﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using SqlKata;
namespace ConsoleApp2
{
    class Program
    {
        static void Main(string[] args)
        {
            var compiler = new SqlKata.Compilers.SqlServerCompiler();
            var query = new Query("abc").WhereIn("Id", new List<int> { 1, 3, 5, 7 });
            query = query.WhereBetween("Date", DateTime.Now.AddDays(-7), DateTime.Now).OrWhere("Id", ">", 100) ;

            var result = compiler.Compile(query);

            var sql = result.ToString();
        }
    }
}
