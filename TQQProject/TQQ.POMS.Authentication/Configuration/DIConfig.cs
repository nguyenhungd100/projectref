﻿using Microsoft.Extensions.DependencyInjection;
using TQQ.POMS.Domain.Services.RolesFacade;
using TQQ.POMS.Domain.Services.RolesFacade.Implementation;
using TQQ.POMS.Domain.Services.UserFacade;
using TQQ.POMS.Domain.Services.UserFacade.Implementation;
using TQQ.POMS.Framework.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TQQ.POMS.Domain.Data;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;

namespace TQQ.POMS.Authentication.Configuration
{
    public static class DIConfigurator
    {
        public static void ConfigDI(this IServiceCollection services, IConfiguration configuration)
        {
            //services
            //    .AddEntityFrameworkSqlServer()
            //    .AddDbContext<DomainContext>((serviceProvider, options) =>
            //                 options.UseSqlServer(configuration.GetConnectionString("DefaultConnection"))
            //                 .UseInternalServiceProvider(serviceProvider));

            //services.AddScoped(typeof(IAppContextFactory<>), typeof(AppContextFactory<>));
            services.AddScoped(typeof(IEntityRepository<,>), typeof(EntityRepository<,>));
            services.AddScoped<IUserService, UserService>();
            services.AddScoped<IRolesService, RolesService>();
        }
    }
}
