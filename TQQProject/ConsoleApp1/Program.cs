﻿using net.sf.mpxj;
using net.sf.mpxj.reader;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class Program
    {
        static IEnumerable<net.sf.mpxj.Task> _taks;
        static void Main(string[] args)
        {
            var inputFile = @"D:\10. VNSolution\08.PCC1\Sources\pcc1-pms\ConsoleApp1\sample.mpp";
            ProjectReader reader = ProjectReaderUtility.getProjectReader(inputFile);
            ProjectFile projectFile = reader.read(inputFile);
            _taks = projectFile.Tasks.Cast<net.sf.mpxj.Task>();

            var taksModels = from t in _taks
                             select new TaskModel
                             {
                                 Id = int.Parse(t.UniqueID.toString()),
                                 Name = t.Name.ToString(),
                                 Days = t.Duration.Duration.ToString(),
                                 DateStart = t.Start.toString(),
                                 DateEnd = t.Finish.toString()
                             };

            //var level1Tasks = new List<net.sf.mpxj.Task>();
            //foreach(var task in _taks)
            //{
            //    if (task.UniqueID.toString() == "0") continue;

            //    // get all level 1 tasks
            //    if (task.ParentTask.UniqueID.toString() == "0")
            //    {
            //        level1Tasks.Add(task);
            //    }
            //    else continue;
            //}
        }
    }

    public class TaskModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Days { get; set; }
        public string DateStart { get; set; }
        public string DateEnd { get; set; }
    }
}
