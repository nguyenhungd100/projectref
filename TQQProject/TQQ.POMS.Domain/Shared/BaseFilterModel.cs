﻿using System;
using System.Collections.Generic;
using System.Text;
using TQQ.POMS.Domain.Services.UserFacade;
using TQQ.POMS.Framework.Data;

namespace TQQ.POMS.Domain.Shared
{
    public class BaseFilterModel
    {
        public string Search { set; get; }

        public PagingInfo Paging { set; get; }

        public string Branch { get; set; }

        public UserStatus? UserStatus { set; get; }
    }
}
