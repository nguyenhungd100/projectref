﻿
using Newtonsoft.Json;
using System;
using System.ComponentModel.DataAnnotations;
using TQQ.POMS.Framework.Formatter;

namespace TQQ.POMS.Domain.Services.UserFacade
{
    /// <summary>
    /// Tài khoản của hệ thống
    /// </summary>
    public class UserModel
    {
        /// <summary>
        /// Mã tài khoản
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Họ tên tài khoản
        /// </summary>
        [MaxLength(250, ErrorMessage = "Họ tên tối đa là 250 ký tự")]
        [MinLength(5, ErrorMessage = "Họ tên tối thiểu là 5 ký tự")]
        public string FullName { get; set; }

        /// <summary>
        /// Ngày sinh
        /// </summary>
        [JsonConverter(typeof(DateTimeConverter))]
        public DateTime? Birthday { set; get; }

        /// <summary>
        /// Mật khẩu
        /// </summary>
        //[Required(ErrorMessage = "Mật khẩu là bắt buộc")]
        public string Password { get; set; }

        /// <summary>
        /// Mật khẩu mới
        /// </summary>
        public string NewPassword { get; set; }

        /// <summary>
        /// Email đăng nhập
        /// </summary>
        [EmailAddress]
        public string Email { get; set; }

        /// <summary>
        /// Giới tính
        /// </summary>
        //public Nullable<bool> Gender { get; set; }

        /// <summary>
        /// Số di động
        /// </summary>
        //[RegularExpression("^(?!0+$)(\\+\\d{1,3}[- ]?)?(?!0+$)\\d{10,15}$", ErrorMessage = "Số điện thoại không hợp lệ")]
        public string Mobile { get; set; }

        /// <summary>
        /// Trạng thái tài khoản
        /// </summary>
        public UserStatus Status { get; set; }

        /// <summary>
        /// Ảnh minh họa dạng base 64
        /// </summary>
        public string Avatar { get; set; }

        /// <summary>
        /// Ngày tạo
        /// </summary>
        public System.DateTime CreatedDate { get; set; }

        /// <summary>
        /// Ngày cập nhật
        /// </summary>
        public Nullable<System.DateTime> UpdatedDate { get; set; }

        /// <summary>
        /// Mã user tạo tài khoản
        /// </summary>
        public int? CreatedBy { get; set; }

        /// <summary>
        /// Mã user cập nhật tài khoản
        /// </summary>
        public Nullable<int> UpdatedBy { get; set; }

        /// <summary>
        /// Vai trò của user
        /// </summary>
        [Required(ErrorMessage = "Vai trò là bắt buộc")]
        public string UserRoles { get; set; }

        /// <summary>
        /// Địa chỉ
        /// </summary>
        public string Address { get; set; }

        /// <summary>
        /// Mã đối tượng sử dụng tài khoản (Staff, Distributor)
        /// </summary>
        public int? ObjectId { set; get; }

        /// <summary>
        /// Loại tài khoản
        /// </summary>
        public AccountType? AccountType { set; get; }

        /// <summary>
        /// Token xác thực tính hợp lệ của tài khoản
        /// </summary>
        public Guid UserModifiedTimeStamp { set; get; }

        /// <summary>
        /// Quyền của user đăng nhập
        /// </summary>
        public string Permissions { get; set; }

        public string RoleNames { get; set; }

        /// <summary>
        /// Tên đăng nhập
        /// </summary>
        //[MaxLength(250, ErrorMessage = "Họ tên tối đa là 250 ký tự")]
        //[MinLength(5, ErrorMessage = "Họ tên tối thiểu là 5 ký tự")]
        //[Required(ErrorMessage = "Tên đăng nhập là bắt buộc")]
        public string UserName { get; set; }

        /// <summary>
        /// Quận Huyện
        /// </summary>
        public int? District { get; set; }

        /// <summary>
        /// Tên Quận Huyện
        /// </summary>
        public string DistrictName { get; set; }

        /// <summary>
        /// Phường Xã
        /// </summary>
        public int? Ward { get; set; }

        /// <summary>
        /// Tên Phường Xã
        /// </summary>
        public string WardName { get; set; }

        /// <summary>
        /// Chi nhánh
        /// </summary>
        [Required(ErrorMessage = "Chi nhánh là bắt buộc")]
        public string Branch { get; set; }

        /// <summary>
        /// Ngôn ngữ
        /// </summary>
        public string Language { get; set; }

        /// <summary>
        /// Ghi chú
        /// </summary>
        public string Note { get; set; }

        /// <summary>
        /// Không xem thông tin của nhân viên khác
        /// </summary>
        public Boolean? IsNotViewOther { get; set; }

        /// <summary>
        /// Cho phép xem thông tin chung
        /// </summary>
        public Boolean? IsAcceptViewGeneralInfo { get; set; }

        //public override bool Valid(ref string message)
        //{
        //    if (String.IsNullOrEmpty(this.Email) || String.IsNullOrEmpty(this.Mobile))
        //    {
        //        message = "Email, Mobile là bắt buộc";
        //        return false;
        //    }
        //    return base.Valid(ref message);
        //}
    }
}



