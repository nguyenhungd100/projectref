
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using TQQ.POMS.Framework.Utils;
using TQQ.POMS.Domain.Data;
using TQQ.POMS.Domain.Data.Entity;
using TQQ.POMS.Framework.Data;
using System.Linq.Expressions;
using Microsoft.EntityFrameworkCore;
using TQQ.POMS.Domain.Shared;

namespace TQQ.POMS.Domain.Services.UserFacade.Implementation
{
    public class UserService : IUserService
    {
        IEntityRepository<User, DomainContext> _userRepository;
        IEntityRepository<UserRole, DomainContext> _userRoleRepository;
        IEntityRepository<Roles, DomainContext> _rolesRoleRepository;

        public UserService(IEntityRepository<User, DomainContext> userRepository,
            IEntityRepository<UserRole, DomainContext> userRoleRepository,
            IEntityRepository<Roles, DomainContext> rolesRoleRepository)
        {
            _userRepository = userRepository;
            _userRoleRepository = userRoleRepository;
            _rolesRoleRepository = rolesRoleRepository;
        }

        public ActionResultType<UserModel> Add(UserModel user)
        {
            if (String.IsNullOrEmpty(user.Password))
            {
                return new ActionResultType<UserModel>()
                {
                    Result = false,
                    Error = "Mật khẩu là bắt buộc"
                };
            }

            if (String.IsNullOrEmpty(user.UserName))
            {
                return new ActionResultType<UserModel>()
                {
                    Result = false,
                    Error = "Tên đăng nhập là bắt buộc"
                };
            }

            user.Status = UserStatus.Actived;
            var exist = _userRepository.Single(c => c.Email == user.Email || c.UserName == user.UserName);
            if (exist != null)
            {
                return new ActionResultType<UserModel>()
                {
                    Result = false,
                    Error = "Tên đang nhập hoặc Email đã tồn tại"
                };
            }

            //add to storage
            user.Password = EncryptUtil.EncryptMD5(user.Password);
            user.CreatedDate = DateTime.Now;
            user.UserModifiedTimeStamp = Guid.NewGuid();

            //check roles exist
            Roles roles = new Roles();
            roles = _rolesRoleRepository.GetById(user.UserRoles);
            if (roles == null)
            {
                return new ActionResultType<UserModel>()
                {
                    Result = false,
                    Error = "Vai trò không tồn tại"
                };
            }

            var entity = user.Map<User>();
            var res = _userRepository.Add(entity);
            var model = res != null ? res.Map<UserModel>() : null;

            //add user role
            if(res != null)
            {
                string roleResult = String.Empty;
                var branch = StringHelper.ParseIds(res.Branch);
                foreach (int item in branch )
                {
                    UserRole userRole = new UserRole();
                    userRole.BranchId = item;
                    userRole.UserId = res.Id;
                    userRole.RoleId = Int32.Parse(res.UserRoles);
                    userRole.Permission = roles.Permissions;
                    var ur = _userRoleRepository.AddAsync(userRole);
                    if(ur == null)
                    {
                        roleResult = roleResult + item + ";";
                    }
                }
                if (!String.IsNullOrEmpty(roleResult))
                {
                    return new ActionResultType<UserModel>()
                    {
                        Result = true,
                        Error = "Thêm vai trò vào chi nhánh " + roleResult + " không thành công."
                    };
                }
               
            }

            return new ActionResultType<UserModel>()
            {
                Result = (res != null ? true : false),
                Data = model
            };
        }

        public async Task<ActionResult> ChangePassword(int userId, string oldPassword, string newPassword)
        {
            //validate
            var user = await _userRepository.GetByIdAsync(userId);

            if (user == null)
                return new ActionResult()
                {
                    Result = false,
                    Error = "User không tồn tại"
                };

            if (user.Password != EncryptUtil.EncryptMD5(oldPassword))
            {
                return new ActionResult()
                {
                    Result = false,
                    Error = "Mật khẩu cũ không đúng"
                };
            }

            //update old password to storage and not change
            user.Password = EncryptUtil.EncryptMD5(newPassword);
            user.UpdatedDate = DateTime.Now;
            //user.UserModifiedTimeStamp = Guid.NewGuid();
            var res = await _userRepository.UpdateAsync(user);

            return new ActionResult()
            {
                Result = res
            };
        }

        public async Task<ActionResult> Delete(int[] Ids)
        {
            var res = await _userRepository.DeleteManyAsync(c => Ids.Contains(c.Id));

            return new ActionResult()
            {
                Result = (res > 0 ? true : false)
            };
        }

        public async Task<ActionResult> DeleteManyAsync(Expression<Func<User, bool>> expression)
        {
            var res = await _userRepository.DeleteManyAsync(expression);

            return new ActionResult()
            {
                Result = (res > 0 ? true : false)
            };
        }

        public async Task<UserModel> GetByIdAsync(int userId)
        {
            var entity = await _userRepository.GetByIdAsync(userId);
            if (entity != null)
                entity.Password = null;
            return entity.Map<UserModel>();
        }

        public UserModel GetById(int userId)
        {
            var entity = _userRepository.GetById(userId);
            if (entity != null)
                entity.Password = null;

            return entity.Map<UserModel>();
        }

        public async Task<List<UserModel>> List(UserFilterModel filter)
        {
            if (filter == null)
                return null;

            using (var db = _userRepository.GetDbContext())
            {
                //var q = db.Users.AsQueryable();
                var q = from user in db.Users
                        join district in db.District on user.District equals district.DistrictId into district
                        from dis in district.DefaultIfEmpty() 
                        join ward in db.Ward on user.Ward equals ward.Id into ward
                        from war in ward.DefaultIfEmpty()
                        join ur in db.UserRole on user.Id equals ur.UserId into userRoles
                        from userRole in userRoles.DefaultIfEmpty()
                        select new {
                            user.Id,
                            user.FullName,
                            user.Birthday,
                            user.Email,
                            user.Mobile,
                            user.Status,
                            user.CreatedDate,
                            //user.UpdatedDate,
                            //user.CreatedBy,
                            //user.UpdatedBy,
                            user.UserRoles,
                            user.Address,
                            user.UserName,
                            user.District,
                            user.Ward,
                            user.Branch,
                            user.Language,
                            user.Note,
                            user.IsNotViewOther,
                            user.IsAcceptViewGeneralInfo,
                            dis.DistrictName,
                            war.WardName,
                            userRole.BranchId,
                            userRole.RoleId
                        };

                //if (filter.AccountType != null)
                //{
                //    var accountType = (int)filter.AccountType;
                //    //q = q.Where(c => c.AccountType == (AccountType)accountType);
                //}

                if (!String.IsNullOrEmpty(filter.Search))
                {
                    q = q.Where(c => c.FullName.Contains(filter.Search) || c.UserName.Contains(filter.Search));
                }

                //if (filter.creatorid != null)
                //{
                //    q = q.where(c => c.createdby == filter.creatorid);
                //}

                //if (filter.updaterid != null)
                //{
                //    q = q.where(c => c.updatedby == filter.updaterid);
                //}

                if (filter.UserStatus != null)
                {
                    q = q.Where(c => c.Status == filter.UserStatus.Value);
                }
                
                if(!String.IsNullOrEmpty(filter.Branch))
                {
                    q = q.Where(c => filter.Branch.Contains(c.BranchId.ToString()));
                }

                if(!String.IsNullOrEmpty(filter.UserRoles))
                {
                    
                    q = q.Where(c => filter.UserRoles.Contains(c.RoleId.ToString()));
                }

                var u = from x in q
                        group q by new
                        {
                            x.Id,
                            x.FullName,
                            x.Birthday,
                            x.Email,
                            x.Mobile,
                            x.Status,
                            x.CreatedDate,
                            x.UserRoles,
                            x.Address,
                            x.UserName,
                            x.District,
                            x.Ward,
                            x.Branch,
                            x.Language,
                            x.Note,
                            x.IsNotViewOther,
                            x.IsAcceptViewGeneralInfo,
                            x.DistrictName,
                            x.WardName,
                        } into b
                        select new
                        {
                            b.Key.Id,
                            b.Key.FullName,
                            b.Key.Birthday,
                            b.Key.Email,
                            b.Key.Mobile,
                            b.Key.Status,
                            b.Key.CreatedDate,
                            b.Key.UserRoles,
                            b.Key.Address,
                            b.Key.UserName,
                            b.Key.District,
                            b.Key.Ward,
                            b.Key.Branch,
                            b.Key.Language,
                            b.Key.Note,
                            b.Key.IsNotViewOther,
                            b.Key.IsAcceptViewGeneralInfo,
                            b.Key.DistrictName,
                            b.Key.WardName,
                        };
                filter.Paging.RowsCount = await u.CountAsync();

                var result = await u.OrderByDescending(c => c.CreatedDate).Skip(filter.Paging.StartRowIndex).Take(filter.Paging.PageSize)
                    .Select(c => new UserModel()
                    {
                        Id = c.Id,
                        FullName = c.FullName,
                        Birthday = c.Birthday,
                        Email = c.Email,
                        Mobile = c.Mobile,
                        Status = (UserStatus)c.Status,
                        CreatedDate = c.CreatedDate,
                        //UpdatedDate = c.UpdatedDate,
                        //CreatedBy = c.CreatedBy.Value,
                        //UpdatedBy = c.UpdatedBy,
                        UserRoles = c.UserRoles,
                        Address = c.Address,
                        UserName = c.UserName,
                        District = c.District,
                        Ward = c.Ward,
                        Branch = c.Branch,
                        Language = c.Language,
                        Note = c.Note,
                        IsNotViewOther = c.IsNotViewOther,
                        IsAcceptViewGeneralInfo = c.IsAcceptViewGeneralInfo,
                        DistrictName = c.DistrictName,
                        WardName = c.WardName
                    }).ToListAsync();

                foreach (var item in result)
                {
                    
                    if (!String.IsNullOrEmpty(item.UserRoles.ToString()) && item.UserRoles.Contains(";"))
                    {
                        var ids = item.UserRoles.Split(new string[] { ";" }, StringSplitOptions.RemoveEmptyEntries).Select(c => Convert.ToInt32(c));
                        var roles = db.Roless.Where(c => ids.Contains(c.Id)).ToList();
                        item.RoleNames = string.Join(", ", roles.Select(c => c.RoleName));
                    }
                }

                return result;
            }//using
        }

        public async Task<ActionResult> ResetPassword(int userId, string newPassword)
        {
            if (String.IsNullOrEmpty(newPassword))
                return new ActionResult()
                {
                    Result = false,
                    Error = "Mật khẩu không hợp lệ"
                };

            //validate
            var staff = await _userRepository.GetByIdAsync(userId);

            if (staff == null)
                return new ActionResult()
                {
                    Result = false,
                    Error = "User không tồn tại"
                };

            //update old password to storage and not change
            staff.Password = EncryptUtil.EncryptMD5(newPassword);
            staff.UpdatedDate = DateTime.Now;
            //staff.UserModifiedTimeStamp = Guid.NewGuid();
            var res = await _userRepository.UpdateAsync(staff);

            return new ActionResult()
            {
                Result = res
            };
        }

        public async Task<UserModel> SingleAsync(Expression<Func<User, bool>> query)
        {
            var res = await _userRepository.SingleAsync(query);
            //if (res != null)
            //    res.Password = null;
            return res.Map<UserModel>();
        }

        public async Task<ActionResult> ToggleLock(int staffId)
        {
            var res = await _userRepository.GetByIdAsync(staffId);
            if (res != null)
            {
                res.Status = res.Status == UserStatus.Actived ? UserStatus.Disabled : UserStatus.Actived;
                await _userRepository.UpdateAsync(res);
                return new ActionResult() { Result = true };
            }
            return new ActionResult() { Result = false, Error = "User không tồn tại" };
        }

        public ActionResult Update(UserModel staff)
        {
            return Update(staff, true);
        }

        public ActionResult Update(UserModel user, bool forceChangeTimestamp)
        {
            var exist = _userRepository.GetById(user.Id);
            if (exist == null)
                return new ActionResult()
                {
                    Result = false,
                    Error = "user không tồn tại"
                };
            

            exist = _userRepository.Single(c => (c.Email == user.Email || c.UserName == user.UserName) && c.Id != user.Id);
            if (exist != null)
            {
                return new ActionResult()
                {
                    Result = false,
                    Error = "Tên đăng nhập hoặc Email đã tồn tại"
                };
            }
            exist = _userRepository.GetById(user.Id);
            //if (user.Password != null)
            //{
            //    if (exist.Password != EncryptUtil.EncryptMD5(user.Password))
            //    {
            //        return new ActionResult()
            //        {
            //            Result = false,
            //            Error = "Mật khẩu cũ không đúng"
            //        };
            //    }
            //    else
            //    {
            //        user.Password = EncryptUtil.EncryptMD5(user.NewPassword);
            //    }
            //}
            //else
            //{
            //    //update old password to storage and not change
            //    user.Password = exist.Password;
            //}

            //update old password to storage and not change
            if (String.IsNullOrEmpty(user.Password))
            {
                user.Password = exist.Password;
            }
            else
            {
                user.Password = EncryptUtil.EncryptMD5(user.Password);
            }
            //user.AccountType = exist.AccountType;
            //user.ObjectId = exist.ObjectId;

            user.UpdatedDate = DateTime.Now;
            if (forceChangeTimestamp == true)
                user.UserModifiedTimeStamp = Guid.NewGuid();

            //update to storage
            var entity = user.Map<User>();
            var res = _userRepository.Update(entity);
            var model = entity.Map<UserModel>();

            return new ActionResult()
            {
                Result = res,
                Data = model
            };
        }

        public ActionResultType<UserModel> VerifyPassword(string userName, string encryptedPassword)
        {
            var user = _userRepository.Single(u => u.UserName == userName || u.Mobile == userName);
            if (user == null)
                return new ActionResultType<UserModel>()
                {
                    Result = false,
                    Error = "User is not exist"
                };

            if (user.Status != UserStatus.Actived)
                return new ActionResultType<UserModel>()
                {
                    Result = false,
                    Error = "User is not activated"
                };

            if (user.Password == encryptedPassword)
            {
                var userModel = user.Map<UserModel>();
                return new ActionResultType<UserModel>()
                {
                    Result = true,
                    Data = userModel
                };
            }

            return new ActionResultType<UserModel>()
            {
                Result = false,
                Error = "Password is not valid"
            };
        }

        public async Task<ActionResult> ChangePassword(ChangePasswordModel changePassword)
        {
            return await ChangePassword(changePassword.UserId, changePassword.OldPassword, changePassword.NewPassword);
        }

        public async Task<ActionResult> ActiveInactiveUser(int userId)
        {
            //validate
            var user = await _userRepository.GetByIdAsync(userId);
            if (user == null)
                return new ActionResult()
                {
                    Result = false,
                    Error = "User không tồn tại"
                };

            var messages = "";
            //active or inactive user
            if (user.Status == UserStatus.Actived)
            {
                user.Status = UserStatus.NotActived;
                messages = "Inactive successfully!";

            }
            else if(user.Status == UserStatus.NotActived)
            {
                user.Status = UserStatus.Actived;
                messages = "Active successfully!";
            }

            user.UpdatedDate = DateTime.Now;
            //user.UserModifiedTimeStamp = Guid.NewGuid();
            var res = await _userRepository.UpdateAsync(user);

            return new ActionResult()
            {
                Result = res,
                Error = messages
            };
        }
    }
}

