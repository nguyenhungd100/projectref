using TQQ.POMS.Framework.Data;

namespace TQQ.POMS.Domain.Services.UserFacade
{
    public class UserFilterModel
    {
        public string Search { set; get; }

        public AccountType? AccountType { set; get; }

        public UserStatus? UserStatus { set; get; }

        public int? CreatorId { set; get; }

        public int? UpdaterId { set; get; }

        public PagingInfo Paging { set; get; }

        public string Branch { get; set; }

        public string UserRoles { get; set; }
    }
}


