﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TQQ.POMS.Domain.Services.UserFacade
{
    /// <summary>
    /// Loại tài khoản
    /// </summary>
    public enum AccountType
    {
        /// <summary>
        /// Nhân viên
        /// </summary>
        Staff = 1,

        /// <summary>
        /// Tài khoản chủ nhà thuốc (Điểm bán)
        /// </summary>
        Distributor = 2,
    }
}
