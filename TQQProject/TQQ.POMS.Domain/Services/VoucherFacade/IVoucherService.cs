using System;
using System.Collections.Generic;
using System.Text;
using TQQ.POMS.Framework.Data;

namespace TQQ.POMS.Domain.Services.VoucherFacade
{
    public interface IVoucherService
    {
        ActionResultType<bool> Delete(int id);
        ActionResultType<VoucherModel> GetById(int id);
        ActionResult Save(VoucherModel model);
        ActionResultType<List<VoucherModel>> FillterModel(FilterVoucherModel filter);
    }
}
