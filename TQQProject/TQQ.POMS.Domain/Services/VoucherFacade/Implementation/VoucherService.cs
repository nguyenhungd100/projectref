using TQQ.POMS.Domain.Data;
using TQQ.POMS.Domain.Data.Entity;
using TQQ.POMS.Framework.Data;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using TQQ.POMS.Framework.Utils;

namespace TQQ.POMS.Domain.Services.VoucherFacade.Implementation
{
    public class VoucherService : IVoucherService
    {
        IEntityRepository<Voucher, DomainContext> _voucherRepository;
         public VoucherService(IEntityRepository<Voucher, DomainContext> voucherRepository)
        {
            _voucherRepository = voucherRepository;
        }

        public ActionResultType<bool> Delete(int id)
        {

            var item = _voucherRepository.GetById(id);
            var res = _voucherRepository.Delete(item);
            return new ActionResultType<bool>() { Result= res};
        }

        public ActionResultType< List<VoucherModel>> FillterModel(FilterVoucherModel filter)
        {
            using (var db = _voucherRepository.GetDbContext())
            {
                var q = from o in db.Voucher
                        select o;
                if (!string.IsNullOrEmpty( filter.Search))
                {
                  //  q = q.Where(c => c.Name.Contains(filter.Search) || c.Code.Contains(filter.Search));
                }
                var list = q.OrderByDescending(c => c.Id).Skip(filter.Paging.StartRowIndex).Take(filter.Paging.PageSize).ToList();
                filter.Paging.RowsCount = q.Count();
                var listModels = list.Map<List<VoucherModel>>();
               
                return new ActionResultType<List<VoucherModel>>() { Result=true, Data= listModels};
            }
        }

        public ActionResultType< VoucherModel> GetById(int id)
        {
            var item = _voucherRepository.GetById(id);
            var itemModel = item.Map<VoucherModel>();
            return new ActionResultType<VoucherModel>() { Result = true, Data= itemModel };
        }

        public ActionResult Save(VoucherModel model)
        {
            
            var item = model.Map<Voucher>();
            if (model.Id==0)
            {
                var res = _voucherRepository.Insert(item);
                return new ActionResult() { Result = res };
            }
            else
            {
                var res = _voucherRepository.Update(item);
                return new ActionResult() { Result = res };
            }
        }
    }
}
