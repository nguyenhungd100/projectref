using System;
using System.Collections.Generic;
using System.Text;

namespace TQQ.POMS.Domain.Services.VoucherFacade
{
    public class VoucherModel
    {

        public Int32 Id { get; set; }
                 
        public string Code { get; set; }
                 
        public string Name { get; set; }
                 
        public decimal ParValue { get; set; }
                 
        public Int32? Status { get; set; }
                 
        public Boolean? IsApplyMulti { get; set; }
                 
        public string GroupProducts { get; set; }
                 
        public decimal Total { get; set; }
                 
        public string Note { get; set; }
                 
        public DateTime? FromDate { get; set; }
                 
        public DateTime? ToDate { get; set; }
                 
        public Boolean? IsEffectiveDate { get; set; }
                 
        public Int32? EffectiveValue { get; set; }
                 
        public Int32? EffectiveType { get; set; }
                 
        public Boolean? IsAllBranch { get; set; }
                 
        public string Branchs { get; set; }
                 
        public Boolean? IsAllCreatedBy { get; set; }
                 
        public string CreatedBy { get; set; }
                 
        public Boolean? IsAllCustomer { get; set; }
                 
        public string Customers { get; set; }
                  
    }
}
