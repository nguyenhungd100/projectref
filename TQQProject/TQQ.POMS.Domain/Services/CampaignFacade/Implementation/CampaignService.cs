using TQQ.POMS.Domain.Data;
using TQQ.POMS.Domain.Data.Entity;
using TQQ.POMS.Framework.Data;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using TQQ.POMS.Framework.Utils;

namespace TQQ.POMS.Domain.Services.CampaignFacade.Implementation
{
    public class CampaignService : ICampaignService
    {
        IEntityRepository<Campaign, DomainContext> _campaignRepository;
         public CampaignService(IEntityRepository<Campaign, DomainContext> campaignRepository)
        {
            _campaignRepository = campaignRepository;
        }

        public ActionResultType<bool> Delete(int id)
        {

            var item = _campaignRepository.GetById(id);
            var res = _campaignRepository.Delete(item);
            return new ActionResultType<bool>() { Result= res};
        }

        public ActionResultType< List<CampaignModel>> FillterModel(FilterCampaignModel filter)
        {
            using (var db = _campaignRepository.GetDbContext())
            {
                var q = from o in db.Campaign
                        select o;
                if (!string.IsNullOrEmpty( filter.Search))
                {
                  //  q = q.Where(c => c.Name.Contains(filter.Search) || c.Code.Contains(filter.Search));
                }
                var list = q.OrderByDescending(c => c.Id).Skip(filter.Paging.StartRowIndex).Take(filter.Paging.PageSize).ToList();
                filter.Paging.RowsCount = q.Count();
                var listModels = list.Map<List<CampaignModel>>();
               
                return new ActionResultType<List<CampaignModel>>() { Result=true, Data= listModels};
            }
        }

        public ActionResultType< CampaignModel> GetById(int id)
        {
            var item = _campaignRepository.GetById(id);
            var itemModel = item.Map<CampaignModel>();
            return new ActionResultType<CampaignModel>() { Result = true, Data= itemModel };
        }

        public ActionResult Save(CampaignModel model)
        {
            
            var item = model.Map<Campaign>();
            if (model.Id==0)
            {
                var res = _campaignRepository.Insert(item);
                return new ActionResult() { Result = res };
            }
            else
            {
                var res = _campaignRepository.Update(item);
                return new ActionResult() { Result = res };
            }
        }
    }
}
