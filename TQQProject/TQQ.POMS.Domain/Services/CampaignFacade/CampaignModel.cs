using System;
using System.Collections.Generic;
using System.Text;

namespace TQQ.POMS.Domain.Services.CampaignFacade
{
    public class CampaignModel
    {

        public Int32 Id { get; set; }
                 
        public string Code { get; set; }
                 
        public string Name { get; set; }
                 
        public Int32? Status { get; set; }
                 
        public Int32? Type { get; set; }
                 
        public Int32? Method { get; set; }
                 
        public DateTime? FromDate { get; set; }
                 
        public DateTime? ToDate { get; set; }
                 
        public string Months { get; set; }
                 
        public string Dates { get; set; }
                 
        public string Days { get; set; }
                 
        public string Hours { get; set; }
                 
        public Boolean? IsApplyBrithday { get; set; }
                 
        public Boolean? IsApplyWeek { get; set; }
                 
        public Boolean? IsApplyMonth { get; set; }
                 
        public Boolean? IsWarning { get; set; }
                 
        public Boolean? IsAllBranch { get; set; }
                 
        public string Branchs { get; set; }
                 
        public Boolean? IsAllSeller { get; set; }
                 
        public string Sellers { get; set; }
                 
        public Boolean? IsAllCustomer { get; set; }
                 
        public string Customers { get; set; }
                 
        public string CreatedBy { get; set; }
                 
        public string Note { get; set; }
                  
    }
}
