using System;
using System.Collections.Generic;
using System.Text;
using TQQ.POMS.Framework.Data;

namespace TQQ.POMS.Domain.Services.CampaignFacade
{
    public interface ICampaignService
    {
        ActionResultType<bool> Delete(int id);
        ActionResultType<CampaignModel> GetById(int id);
        ActionResult Save(CampaignModel model);
        ActionResultType<List<CampaignModel>> FillterModel(FilterCampaignModel filter);
    }
}
