using TQQ.POMS.Domain.Data;
using TQQ.POMS.Domain.Data.Entity;
using TQQ.POMS.Framework.Data;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using TQQ.POMS.Framework.Utils;
using System.Threading.Tasks;

namespace TQQ.POMS.Domain.Services.FunctionsFacade.Implementation
{
    public class FunctionsService : IFunctionsService
    {
        IEntityRepository<Functions, DomainContext> _functionsRepository;
         public FunctionsService(IEntityRepository<Functions, DomainContext> functionsRepository)
        {
            _functionsRepository = functionsRepository;
        }

        public ActionResultType<bool> Delete(int id)
        {

            var item = _functionsRepository.GetById(id);
            var res = _functionsRepository.Delete(item);
            return new ActionResultType<bool>() { Result= res};
        }

        public ActionResultType< List<FunctionsModel>> FillterModel(FilterFunctionsModel filter)
        {
            using (var db = _functionsRepository.GetDbContext())
            {
                var q = from o in db.Functions
                        select o;
                if (!string.IsNullOrEmpty( filter.Search))
                {
                  //  q = q.Where(c => c.Name.Contains(filter.Search) || c.Code.Contains(filter.Search));
                }
                var list = q.OrderByDescending(c => c.FunctionCode).Skip(filter.Paging.StartRowIndex).Take(filter.Paging.PageSize).ToList();
                filter.Paging.RowsCount = q.Count();
                var listModels = list.Map<List<FunctionsModel>>();
               
                return new ActionResultType<List<FunctionsModel>>() { Result=true, Data= listModels};
            }
        }

        public async Task<List<FunctionsModel>> GetAll()
        {
            var res = await _functionsRepository.FetchAsync();
            return res.Map<List<FunctionsModel>>();
        }

        public ActionResultType< FunctionsModel> GetByCode(string code)
        {
            var item = _functionsRepository.SingleAsync(c => c.FunctionCode == code);
            var itemModel = item.Map<FunctionsModel>();
            return new ActionResultType<FunctionsModel>() { Result = true, Data= itemModel };
        }

        //public ActionResult Save(FunctionsModel model)
        //{
            
        //    var item = model.Map<Functions>();
        //    if (model.Id==0)
        //    {
        //        var res = _functionsRepository.Insert(item);
        //        return new ActionResult() { Result = res };
        //    }
        //    else
        //    {
        //        var res = _functionsRepository.Update(item);
        //        return new ActionResult() { Result = res };
        //    }
        //}
    }
}
