using System;
using System.Collections.Generic;
using System.Text;

namespace TQQ.POMS.Domain.Services.FunctionsFacade
{
    public class FunctionsModel
    {

        public string FunctionCode { get; set; }
                 
        public string FunctionName { get; set; }
                  
    }
}
