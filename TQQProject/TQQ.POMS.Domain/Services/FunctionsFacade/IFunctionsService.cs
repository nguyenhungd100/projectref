using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using TQQ.POMS.Framework.Data;

namespace TQQ.POMS.Domain.Services.FunctionsFacade
{
    public interface IFunctionsService
    {
        ActionResultType<bool> Delete(int id);
        ActionResultType<FunctionsModel> GetByCode(string id);
        //ActionResult Save(FunctionsModel model);
        ActionResultType<List<FunctionsModel>> FillterModel(FilterFunctionsModel filter);
        Task<List<FunctionsModel>> GetAll();

    }
}
