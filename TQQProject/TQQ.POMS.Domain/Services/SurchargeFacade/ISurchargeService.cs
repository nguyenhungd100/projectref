using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using TQQ.POMS.Framework.Data;

namespace TQQ.POMS.Domain.Services.SurchargeFacade
{
    public interface ISurchargeService
    {
        ActionResultType<bool> Delete(int id);
        ActionResultType<SurchargeModel> GetById(int id);
        ActionResult Save(SurchargeModel model);
        ActionResultType<List<SurchargeModel>> FillterModel(FilterSurchargeModel filter);
        Task<ActionResult> ActiveInactiveSurcharge(int SurchargeId);
    }
}
