using System;
using System.Collections.Generic;
using System.Text;
using TQQ.POMS.Domain.Services.UserFacade;

namespace TQQ.POMS.Domain.Services.SurchargeFacade
{
    public class SurchargeModel
    {

            public int Id { get; set; }
                 
            public string Code { get; set; }
                 
            public string Type { get; set; }
                 
            public Decimal Value { get; set; }
                 
            public string Branchs { get; set; }
                 
            public Int32? Order { get; set; }
                 
            public Boolean? IsAutoSaveBill { get; set; }
                 
            public Boolean? IsRefund { get; set; }
                 
            public Boolean? IsAllBranch { get; set; }

            public UserStatus Status { get; set; }

            public Nullable<int> ValueType { get; set; }
    }
}
