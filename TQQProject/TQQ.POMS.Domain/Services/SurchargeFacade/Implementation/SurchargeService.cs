﻿using TQQ.POMS.Domain.Data;
using TQQ.POMS.Domain.Data.Entity;
using TQQ.POMS.Framework.Data;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using TQQ.POMS.Framework.Utils;
using TQQ.POMS.Domain.Services.UserFacade;
using System.Threading.Tasks;
using TQQ.POMS.Domain.Shared;

namespace TQQ.POMS.Domain.Services.SurchargeFacade.Implementation
{
    public class SurchargeService : ISurchargeService
    {
        IEntityRepository<Surcharge, DomainContext> _surchargeRepository;
         public SurchargeService(IEntityRepository<Surcharge, DomainContext> surchargeRepository)
        {
            _surchargeRepository = surchargeRepository;
        }

        public async Task<ActionResult> ActiveInactiveSurcharge(int SurchargeId)
        {
            //validate
            var surcharge = _surchargeRepository.GetById(SurchargeId);
            if (surcharge == null)
            {
                return new ActionResult()
                {
                    Result = false,
                    Error = "Thu khác không tồn tại"
                };
            }

            var messages = "";
            //active or inactive surcharge
            if (surcharge.Status == UserStatus.Actived)
            {
                surcharge.Status = UserStatus.NotActived;
                messages = "Inactive successfully!";

            }
            else if (surcharge.Status == UserStatus.NotActived)
            {
                surcharge.Status = UserStatus.Actived;
                messages = "Active successfully!";
            }

            //surcharge.UpdatedDate = DateTime.Now;
            //user.UserModifiedTimeStamp = Guid.NewGuid();
            var res = await _surchargeRepository.UpdateAsync(surcharge);

            return new ActionResult()
            {
                Result = res,
                Error = messages
            };
        }

        public ActionResultType<bool> Delete(int id)
        {

            var item = _surchargeRepository.GetById(id);
            var res = _surchargeRepository.Delete(item);
            return new ActionResultType<bool>() { Result= res};
        }

        public ActionResultType< List<SurchargeModel>> FillterModel(FilterSurchargeModel filter)
        {
            using (var db = _surchargeRepository.GetDbContext())
            {
                var q = from o in db.Surcharge
                        select o;
                if (!string.IsNullOrEmpty( filter.Search))
                {
                    q = q.Where(c => c.Type.Contains(filter.Search));
                }
                if (!string.IsNullOrEmpty(filter.SurchargeCode))
                {
                    q = q.Where(c => c.Code.Contains(filter.SurchargeCode));
                }
                if (!string.IsNullOrEmpty(filter.Branch))
                {
                    var branch = StringHelper.ParseIds(filter.Branch);
                    q = q.Where(c => c.IsAllBranch == true || branch.Any(x => c.Branchs.Contains(x.ToString())));
                }
                if (filter.UserStatus != null)
                {
                    q = q.Where(c => c.Status == filter.UserStatus.Value);
                }
                var list = q.OrderByDescending(c => c.Id).Skip(filter.Paging.StartRowIndex).Take(filter.Paging.PageSize);
                filter.Paging.RowsCount = q.Count();
                var listModels = list.Select(c => new SurchargeModel()
                {
                    Id = c.Id,
                    Code = c.Code,
                    Type = c.Type,
                    Value = c.Value,
                    Branchs = c.Branchs,
                    Order = c.Order,
                    IsAutoSaveBill = c.IsAutoSaveBill,
                    IsRefund = c.IsRefund,
                    IsAllBranch = c.IsAllBranch,
                    Status = (UserStatus)c.Status,
                    ValueType = c.ValueType
                }).ToList();
                GenSurchargeCode();
                return new ActionResultType<List<SurchargeModel>>() { Result=true, Data= listModels };
            }
        }

        public ActionResultType< SurchargeModel> GetById(int id)
        {
            var item = _surchargeRepository.GetById(id);
            var itemModel = item.Map<SurchargeModel>();
            return new ActionResultType<SurchargeModel>() { Result = true, Data= itemModel };
        }

        public ActionResult Save(SurchargeModel model)
        {
            model.Status = UserStatus.Actived;
            if (String.IsNullOrEmpty(model.Code))
            {
                string code = GenSurchargeCode();
                if(code != "")
                {
                    model.Code = code;
                }
                else
                {
                    return new ActionResult()
                    {
                        Result = false,
                        Error = "Có lỗi xảy ra khi tạo mã tự động"
                    };
                }
            }
            var item = model.Map<Surcharge>();
            if (model.Id==0)
            {
                var res = _surchargeRepository.Insert(item);
                return new ActionResult() { Result = res };
            }
            else
            {
                var res = _surchargeRepository.Update(item);
                return new ActionResult() { Result = res };
            }
        }

        public string GenSurchargeCode()
        {
            string result = "";
            using (var db = _surchargeRepository.GetDbContext())
            {
                int number = 0;
                var q = (from sc in db.Surcharge
                        where sc.Code.StartsWith("MTK") && sc.Code.Length == 8
                        orderby sc.Code descending
                        select sc).Where(c => int.TryParse(c.Code.Substring(3,5), out number) == true).FirstOrDefault();
                if(q != null)
                {
                    int num = Int32.Parse(q.Code.Substring(3, 5));
                    string s = "MTK";
                    num = num + 1;
                    result = s + num.ToString().PadLeft(5, '0');
                    
                }
                else
                {
                    result = "MTK00001";
                }
            }
            return result;
        }
    }
}
