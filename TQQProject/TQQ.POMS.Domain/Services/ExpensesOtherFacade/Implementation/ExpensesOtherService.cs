﻿using TQQ.POMS.Domain.Data;
using TQQ.POMS.Domain.Data.Entity;
using TQQ.POMS.Framework.Data;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using TQQ.POMS.Framework.Utils;
using System.Threading.Tasks;
using TQQ.POMS.Domain.Services.UserFacade;
using TQQ.POMS.Domain.Shared;

namespace TQQ.POMS.Domain.Services.ExpensesOtherFacade.Implementation
{
    public class ExpensesOtherService : IExpensesOtherService
    {
        IEntityRepository<ExpensesOther, DomainContext> _expensesOtherRepository;
         public ExpensesOtherService(IEntityRepository<ExpensesOther, DomainContext> expensesOtherRepository)
        {
            _expensesOtherRepository = expensesOtherRepository;
        }

        public async Task<ActionResult> ActiveInactiveExpenseOther(int expenseOtherId)
        {
            //validate
            var expenseOther = _expensesOtherRepository.GetById(expenseOtherId);
            if (expenseOther == null)
            {
                return new ActionResult()
                {
                    Result = false,
                    Error = "Chi phí nhập hàng không tồn tại"
                };
            }

            var messages = "";
            //active or inactive expenseOther
            if (expenseOther.Status == UserStatus.Actived)
            {
                expenseOther.Status = UserStatus.NotActived;
                messages = "Inactive successfully!";

            }
            else if (expenseOther.Status == UserStatus.NotActived)
            {
                expenseOther.Status = UserStatus.Actived;
                messages = "Active successfully!";
            }

            //expenseOther.UpdatedDate = DateTime.Now;
            //user.UserModifiedTimeStamp = Guid.NewGuid();
            var res = await _expensesOtherRepository.UpdateAsync(expenseOther);

            return new ActionResult()
            {
                Result = res,
                Error = messages
            };
        }

        public ActionResultType<bool> Delete(int id)
        {

            var item = _expensesOtherRepository.GetById(id);
            var res = _expensesOtherRepository.Delete(item);
            return new ActionResultType<bool>() { Result= res};
        }

        public ActionResultType< List<ExpensesOtherModel>> FillterModel(FilterExpensesOtherModel filter)
        {
            using (var db = _expensesOtherRepository.GetDbContext())
            {
                var q = from o in db.ExpensesOther
                        select o;
                if (!string.IsNullOrEmpty(filter.Search))
                {
                    q = q.Where(c => c.Name.Contains(filter.Search));
                }
                if (!string.IsNullOrEmpty(filter.code))
                {
                    q = q.Where(c => c.Code.Contains(filter.code));
                }
                if (!string.IsNullOrEmpty(filter.Branch))
                {
                    var branch = StringHelper.ParseIds(filter.Branch);
                    q = q.Where(c => c.IsAllBranch == true || branch.Any(x => c.Branchs.Contains(x.ToString())));
                }
                if (filter.UserStatus != null)
                {
                    q = q.Where(c => c.Status == filter.UserStatus.Value);
                }
                var list = q.OrderByDescending(c => c.Id).Skip(filter.Paging.StartRowIndex).Take(filter.Paging.PageSize).ToList();
                filter.Paging.RowsCount = q.Count();
                var listModels = list.Select(c => new ExpensesOtherModel() {
                    Id = c.Id,
                    Code = c.Code,
                    Name = c.Name,
                    Value = c.Value,
                    ValueType = c.ValueType,
                    Type = c.Type,
                    IsAllBranch = c.IsAllBranch,
                    Branchs = c.Branchs,
                    IsAutoSaveBill = c.IsAutoSaveBill,
                    Status = (UserStatus)c.Status,
                    IsRefund = c.IsRefund
                }).ToList();
               
                return new ActionResultType<List<ExpensesOtherModel>>() { Result=true, Data= listModels};
            }
        }

        public ActionResultType< ExpensesOtherModel> GetById(int id)
        {
            var item = _expensesOtherRepository.GetById(id);
            var itemModel = item.Map<ExpensesOtherModel>();
            return new ActionResultType<ExpensesOtherModel>() { Result = true, Data= itemModel };
        }

        public ActionResult Save(ExpensesOtherModel model)
        {
            model.Status = UserStatus.Actived;
            if (String.IsNullOrEmpty(model.Code))
            {
                string code = GenExpenseOtherCode();
                if (code != "")
                {
                    model.Code = code;
                }
                else
                {
                    return new ActionResult()
                    {
                        Result = false,
                        Error = "Có lỗi xảy ra khi tạo mã tự động"
                    };
                }
            }
            var item = model.Map<ExpensesOther>();
            if (model.Id==0)
            {
                var res = _expensesOtherRepository.Insert(item);
                return new ActionResult() { Result = res };
            }
            else
            {
                var res = _expensesOtherRepository.Update(item);
                return new ActionResult() { Result = res };
            }
        }

        public string GenExpenseOtherCode()
        {
            string result = "";

            using (var db = _expensesOtherRepository.GetDbContext())
            {
                int number = 0;
                var q = (from sc in db.ExpensesOther
                         where sc.Code.StartsWith("CPN") && sc.Code.Length == 8
                         orderby sc.Code descending
                         select sc).Where(c => int.TryParse(c.Code.Substring(3, 5), out number) == true).FirstOrDefault();
                if (q != null)
                {
                    int num = Int32.Parse(q.Code.Substring(3, 5));
                    string s = "MTK";
                    num = num + 1;
                    result = s + num.ToString().PadLeft(5, '0');
                }
                else
                {
                    result = "CPN00001";
                }
            }
            return result;
        }
    }
}
