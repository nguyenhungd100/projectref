using System;
using System.Collections.Generic;
using System.Text;
using TQQ.POMS.Domain.Services.UserFacade;

namespace TQQ.POMS.Domain.Services.ExpensesOtherFacade
{
    public class ExpensesOtherModel
    {

            public int Id { get; set; }
                 
            public string Code { get; set; }
                 
            public string Name { get; set; }
                 
            public decimal Value { get; set; }

            public Nullable<int> ValueType { get; set; }

            public Nullable<int> Type { get; set; }

            public Nullable<bool> IsAllBranch { get; set; }

            public string Branchs { get; set; }

            public Nullable<bool> IsAutoSaveBill { get; set; }

            public Nullable<bool> IsRefund { get; set; }

            public UserStatus Status { get; set; }

    }
}
