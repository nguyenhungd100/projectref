using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using TQQ.POMS.Framework.Data;

namespace TQQ.POMS.Domain.Services.ExpensesOtherFacade
{
    public interface IExpensesOtherService
    {
        ActionResultType<bool> Delete(int id);
        ActionResultType<ExpensesOtherModel> GetById(int id);
        ActionResult Save(ExpensesOtherModel model);
        ActionResultType<List<ExpensesOtherModel>> FillterModel(FilterExpensesOtherModel filter);
        Task<ActionResult> ActiveInactiveExpenseOther(int expenseOtherId);
    }
}
