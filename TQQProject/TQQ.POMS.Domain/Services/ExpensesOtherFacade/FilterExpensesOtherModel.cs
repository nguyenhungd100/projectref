using System;
using System.Collections.Generic;
using System.Text;
using TQQ.POMS.Domain.Shared;
using TQQ.POMS.Framework.Data;

namespace TQQ.POMS.Domain.Services.ExpensesOtherFacade
{
    public class FilterExpensesOtherModel : BaseFilterModel
    {
        public string code { get; set; }
    }
}
