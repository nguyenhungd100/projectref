﻿using AutoMapper;
using AutoMapper.EquivalencyExpression;

using TQQ.POMS.Domain.Data.Entity;
using TQQ.POMS.Domain.Services.BranchFacade;
using TQQ.POMS.Domain.Services.DistrictFacade;
using TQQ.POMS.Domain.Services.RolesFacade;
using TQQ.POMS.Domain.Services.SurchargeFacade;
using TQQ.POMS.Domain.Services.UserFacade;
using TQQ.POMS.Domain.Services.WardFacade;

namespace TQQ.POMS.Domain.Services
{
    public static class DomainMaps
    {
        public static IMapper Mapper { get; set; }

        public static void Config()
        {
            var config = new MapperConfiguration(cfg =>
            {
                cfg.AddCollectionMappers();

                cfg.CreateMap<User, UserModel>();
                cfg.CreateMap<Roles, RolesModel>();
                cfg.CreateMap<District, DistrictModel>();
                cfg.CreateMap<Ward, WardModel>();
                cfg.CreateMap<Branch, BranchModel>();
                cfg.CreateMap<Surcharge, SurchargeModel>();

                // ignore all unmapped properties globally
                //cfg.ForAllMaps((map, exp) => exp.ForAllOtherMembers(opt => opt.Ignore()));
            });
            Mapper = config.CreateMapper();
        }

        public static TTo Map<TTo>(this object entity)
        {
            return Mapper.Map<TTo>(entity);
        }
    }
}
