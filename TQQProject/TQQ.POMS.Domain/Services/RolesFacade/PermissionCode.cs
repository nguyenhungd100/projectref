﻿namespace TQQ.POMS.Domain.Services.RolesFacade
{
    public enum PermissionGroupCode
    {
        SYSTEM = 1,
        PRODUCT = 2,
        EXCHANGE = 3,
        CUSTOMER = 4,
        REPORT = 5,
        CASHFLOW = 6,
        PROMOTION = 7,
        VOUCHER = 8,
    }

    public enum PermissionCode
    {
        // System
        [PermissionDetails(PermissionGroupCode.SYSTEM, "Thiết lập")]
        SETTING = 1,
        [PermissionDetails(PermissionGroupCode.SYSTEM, "Xem DS mẫu in")]
        VIEW_PRINT_TEMPLETES = 2,
        [PermissionDetails(PermissionGroupCode.SYSTEM, "Cập nhật mẫu in")]
        UPDATE_PRINT_TEMPLETES = 3,
        [PermissionDetails(PermissionGroupCode.SYSTEM, "Xem DS người dùng")]
        VIEW_USER = 4,
        [PermissionDetails(PermissionGroupCode.SYSTEM, "Thêm mới người dùng")]
        CREATE_USER = 5,
        [PermissionDetails(PermissionGroupCode.SYSTEM, "Cập nhật người dùng")]
        UPDATE_USER = 6,
        [PermissionDetails(PermissionGroupCode.SYSTEM, "Xóa người dùng")]
        DELETE_USER = 7,
        [PermissionDetails(PermissionGroupCode.SYSTEM, "Xem DS chi nhánh")]
        VIEW_BRANCH = 8,
        [PermissionDetails(PermissionGroupCode.SYSTEM, "Thêm mới chi nhánh")]
        CREATE_BRANCH = 9,
        [PermissionDetails(PermissionGroupCode.SYSTEM, "Cập nhật chi nhánh")]
        UPDATE_BRANCH = 10,
        [PermissionDetails(PermissionGroupCode.SYSTEM, "Xóa chi nhánh")]
        DELETE_BRANCH = 11,
        [PermissionDetails(PermissionGroupCode.SYSTEM, "Xem DS thu khác")]
        VIEW_SURCHARGE = 12,
        [PermissionDetails(PermissionGroupCode.SYSTEM, "Thêm mới thu khác")]
        CREATE_SURCHARGE = 13,
        [PermissionDetails(PermissionGroupCode.SYSTEM, "Cập nhật thu khác")]
        UPDATE_SURCHARGE = 14,
        [PermissionDetails(PermissionGroupCode.SYSTEM, "Xóa thu khác")]
        DELETE_SURCHARGE = 15,
        [PermissionDetails(PermissionGroupCode.SYSTEM, "Xem DS chi phí nhập hàng")]
        VIEW_EXPENSESOTHER = 16,
        [PermissionDetails(PermissionGroupCode.SYSTEM, "Thêm mới chi phí nhập hàng")]
        CREATE_EXPENSESOTHER = 17,
        [PermissionDetails(PermissionGroupCode.SYSTEM, "Cập nhật chi phí nhập hàng")]
        UPDATE_EXPENSESOTHER = 18,
        [PermissionDetails(PermissionGroupCode.SYSTEM, "Xóa chi phí nhập hàng")]
        DELETE_EXPENSESOTHER = 19,
        [PermissionDetails(PermissionGroupCode.SYSTEM, "Lịch sử thao tác")]
        ACTION_HISTORY = 20,
        [PermissionDetails(PermissionGroupCode.SYSTEM, "Tổng quan")]
        OVERVIEW = 21,

        // Product
        [PermissionDetails(PermissionGroupCode.PRODUCT, "Xem DS Danh mục sản phẩm")]
        VIEW_PRODUCTS = 22,
        [PermissionDetails(PermissionGroupCode.PRODUCT, "Thêm mới Danh mục sản phẩm")]
        CREATE_PRODUCTS = 23,
        [PermissionDetails(PermissionGroupCode.PRODUCT, "Cập nhật Danh mục sản phẩm")]
        UPDATE_PRODUCTS = 24,
        [PermissionDetails(PermissionGroupCode.PRODUCT, "Xóa Danh mục sản phẩm")]
        DELETE_PRODUCTS = 25,
        [PermissionDetails(PermissionGroupCode.PRODUCT, "Giá nhập Danh mục sản phẩm")]
        PRICE_PRODUCTS = 26,
        [PermissionDetails(PermissionGroupCode.PRODUCT, "Giá vốn Danh mục sản phẩm")]
        COST_PRODUCTS = 27,
        [PermissionDetails(PermissionGroupCode.PRODUCT, "Import Danh mục sản phẩm")]
        IMPORT_PRODUCTS = 28,
        [PermissionDetails(PermissionGroupCode.PRODUCT, "Xuất file Danh mục sản phẩm")]
        EXPORT_PRODUCTS = 29,


        [PermissionDetails(PermissionGroupCode.PRODUCT, "Xem DS thiết lập giá")]
        VIEW_PRICEBOOK = 30,
        [PermissionDetails(PermissionGroupCode.PRODUCT, "Thêm mới thiết lập giá")]
        CREATE_PRICEBOOK = 31,
        [PermissionDetails(PermissionGroupCode.PRODUCT, "Cập nhật thiết lập giá")]
        UPDATE_PRICEBOOK = 32,
        [PermissionDetails(PermissionGroupCode.PRODUCT, "Xóa thiết lập giá")]
        DELETE_PRICEBOOK = 33,
        [PermissionDetails(PermissionGroupCode.PRODUCT, "Import thiết lập giá")]
        IMPORT_PRICEBOOK = 34,
        [PermissionDetails(PermissionGroupCode.PRODUCT, "Xuất file thiết lập giá")]
        EXPORT_PRICEBOOK = 35,

        [PermissionDetails(PermissionGroupCode.PRODUCT, "Xem DS kiểm kho")]
        VIEW_STOCKTAKES = 36,
        [PermissionDetails(PermissionGroupCode.PRODUCT, "Thêm mới kiểm kho")]
        CREATE_STOCKTAKES = 37,
        [PermissionDetails(PermissionGroupCode.PRODUCT, "Xóa kiểm kho")]
        DELETE_STOCKTAKES = 38,
        [PermissionDetails(PermissionGroupCode.PRODUCT, "Sao chép kiểm kho")]
        COPY_STOCKTAKES = 39,
        [PermissionDetails(PermissionGroupCode.PRODUCT, "Hoàn thành kiểm kho")]
        FINISH_STOCKTAKES = 40,

        [PermissionDetails(PermissionGroupCode.PRODUCT, "Xem DS sản xuất")]
        VIEW_MANUFACTURING = 41,
        [PermissionDetails(PermissionGroupCode.PRODUCT, "Thêm mới sản xuất")]
        CREATE_MANUFACTURING = 42,
        [PermissionDetails(PermissionGroupCode.PRODUCT, "Cập nhật sản xuất")]
        UPDATE_MANUFACTURING = 43,
        [PermissionDetails(PermissionGroupCode.PRODUCT, "Xóa sản xuất")]
        DELETE_MANUFACTURING = 44,
        [PermissionDetails(PermissionGroupCode.PRODUCT, "Xuất file sản xuất")]
        EXPORT_MANUFACTURING = 45,



        //EXCHANGE
        [PermissionDetails(PermissionGroupCode.EXCHANGE, "Xem DS đặt hàng")]
        VIEW_ORDERS = 46,
        [PermissionDetails(PermissionGroupCode.EXCHANGE, "Thêm mới đặt hàng")]
        CREATE_ORDERS = 47,
        [PermissionDetails(PermissionGroupCode.EXCHANGE, "Cập nhật đặt hàng")]
        UPDATE_ORDERS = 48,
        [PermissionDetails(PermissionGroupCode.EXCHANGE, "Xóa đặt hàng")]
        DELETE_ORDERS = 49,
        [PermissionDetails(PermissionGroupCode.EXCHANGE, "In lại đặt hàng")]
        PRINT_ORDERS = 50,
        [PermissionDetails(PermissionGroupCode.EXCHANGE, "Xuất file đặt hàng")]
        EXPORT_ORDERS = 51,
        [PermissionDetails(PermissionGroupCode.EXCHANGE, "Tạo hóa đơn đặt hàng")]
        BILL_ORDERS = 52,
        [PermissionDetails(PermissionGroupCode.EXCHANGE, "Sao chép đặt hàng")]
        COPY_ORDERS = 53,

        [PermissionDetails(PermissionGroupCode.EXCHANGE, "Xem DS hóa đơn")]
        VIEW_INVOICES = 54,
        [PermissionDetails(PermissionGroupCode.EXCHANGE, "Thêm mới hóa đơn")]
        CREATE_INVOICES = 55,
        [PermissionDetails(PermissionGroupCode.EXCHANGE, "Cập nhật hóa đơn")]
        UPDATE_INVOICES = 56,
        [PermissionDetails(PermissionGroupCode.EXCHANGE, "Xóa hóa đơn")]
        DELETE_INVOICES = 57,
        [PermissionDetails(PermissionGroupCode.EXCHANGE, "Xuất file hóa đơn")]
        EXPORT_INVOICES = 58,
        [PermissionDetails(PermissionGroupCode.EXCHANGE, "Xem tồn kho hóa đơn")]
        VIEW_INVENTORY_INVOICES = 59,
        [PermissionDetails(PermissionGroupCode.EXCHANGE, "Thay đổi giá hóa đơn")]
        CHANGE_PRICE_INVOICES = 60,
        [PermissionDetails(PermissionGroupCode.EXCHANGE, "Cập nhật giảm giá hóa đơn")]
        UPDATE_DISCOUNT_INVOICES = 61,
        [PermissionDetails(PermissionGroupCode.EXCHANGE, "Cập nhật người bán hóa đơn")]
        UPDATE_SELLER_INVOICES = 62,
        [PermissionDetails(PermissionGroupCode.EXCHANGE, "In lại hóa đơn")]
        PRINT_INVOICES = 63,
        [PermissionDetails(PermissionGroupCode.EXCHANGE, "Sao chép hóa đơn")]
        COPY_INVOICES = 64,

        [PermissionDetails(PermissionGroupCode.EXCHANGE, "Xem DS trả hàng")]
        VIEW_RETURNS = 65,
        [PermissionDetails(PermissionGroupCode.EXCHANGE, "Thêm mới trả hàng")]
        CREATE_RETURNS = 66,
        [PermissionDetails(PermissionGroupCode.EXCHANGE, "Cập nhật trả hàng")]
        UPDATE_RETURNS = 67,
        [PermissionDetails(PermissionGroupCode.EXCHANGE, "Xóa trả hàng")]
        DELETE_RETURNS = 68,
        [PermissionDetails(PermissionGroupCode.EXCHANGE, "In lại trả hàng")]
        PRINT_RETURNS = 69,
        [PermissionDetails(PermissionGroupCode.EXCHANGE, "Sao chép trả hàng")]
        COPY_RETURNS = 70,
        [PermissionDetails(PermissionGroupCode.EXCHANGE, "Xuất file trả hàng")]
        EXPORT_RETURNS = 71,

        [PermissionDetails(PermissionGroupCode.EXCHANGE, "Xem DS đặt hàng nhập")]
        VIEW_ORDERSUPPLIER = 72,
        [PermissionDetails(PermissionGroupCode.EXCHANGE, "Thêm mới đặt hàng nhập")]
        CREATE_ORDERSUPPLIER = 73,
        [PermissionDetails(PermissionGroupCode.EXCHANGE, "Cập nhật đặt hàng nhập")]
        UPDATE_ORDERSUPPLIER = 74,
        [PermissionDetails(PermissionGroupCode.EXCHANGE, "Xóa đặt hàng nhập")]
        DELETE_ORDERSUPPLIER = 75,
        [PermissionDetails(PermissionGroupCode.EXCHANGE, "In lại đặt hàng nhập")]
        PRINT_ORDERSUPPLIER = 76,
        [PermissionDetails(PermissionGroupCode.EXCHANGE, "Xuất file đặt hàng nhập")]
        EXPORT_ORDERSUPPLIER = 77,
        [PermissionDetails(PermissionGroupCode.EXCHANGE, "Tạo phiếu nhập đặt hàng nhập")]
        CREATE_BILL_ORDERSUPPLIER = 78,
        [PermissionDetails(PermissionGroupCode.EXCHANGE, "Sao chép đặt hàng nhập")]
        COPY_ORDERSUPPLIER = 79,


        [PermissionDetails(PermissionGroupCode.EXCHANGE, "Xem DS nhập hàng")]
        VIEW_PURCHASEORDER = 80,
        [PermissionDetails(PermissionGroupCode.EXCHANGE, "Thêm mới nhập hàng")]
        CREATE_PURCHASEORDER = 81,
        [PermissionDetails(PermissionGroupCode.EXCHANGE, "Cập nhật nhập hàng")]
        UPDATE_PURCHASEORDER = 82,
        [PermissionDetails(PermissionGroupCode.EXCHANGE, "Xóa nhập hàng")]
        DELETE_PURCHASEORDER = 83,
        [PermissionDetails(PermissionGroupCode.EXCHANGE, "Xuất file nhập hàng")]
        EXPORT_PURCHASEORDER = 84,
        [PermissionDetails(PermissionGroupCode.EXCHANGE, "Sao chép nhập hàng")]
        COPY_PURCHASEORDER = 85,

        [PermissionDetails(PermissionGroupCode.EXCHANGE, "Xem DS trả hàng nhập")]
        VIEW_PURCHASERETURNS = 86,
        [PermissionDetails(PermissionGroupCode.EXCHANGE, "Thêm mới trả hàng nhập")]
        CREATE_PURCHASERETURNS = 87,
        [PermissionDetails(PermissionGroupCode.EXCHANGE, "Cập nhật trả hàng nhập")]
        UPDATE_PURCHASERETURNS = 88,
        [PermissionDetails(PermissionGroupCode.EXCHANGE, "Xóa trả hàng nhập")]
        DELETE_PURCHASERETURNS = 89,
        [PermissionDetails(PermissionGroupCode.EXCHANGE, "Sao chép trả hàng nhập")]
        COPY_PURCHASERETURNS = 90,

        [PermissionDetails(PermissionGroupCode.EXCHANGE, "Xem DS chuyển hàng")]
        VIEW_TRANSFERS = 91,
        [PermissionDetails(PermissionGroupCode.EXCHANGE, "Thêm mới chuyển hàng")]
        CREATE_TRANSFERS = 92,
        [PermissionDetails(PermissionGroupCode.EXCHANGE, "Cập nhật chuyển hàng")]
        UPDATE_TRANSFERS = 93,
        [PermissionDetails(PermissionGroupCode.EXCHANGE, "Xóa chuyển hàng")]
        DELETE_TRANSFERS = 94,
        [PermissionDetails(PermissionGroupCode.EXCHANGE, "Xuất file chuyển hàng")]
        EXPORT_TRANSFERS = 95,
        [PermissionDetails(PermissionGroupCode.EXCHANGE, "Sao chép chuyển hàng")]
        COPY_TRANSFERS = 96,

        [PermissionDetails(PermissionGroupCode.EXCHANGE, "Xem DS xuất hủy")]
        VIEW_DAMAGEITEMS = 97,
        [PermissionDetails(PermissionGroupCode.EXCHANGE, "Thêm mới xuất hủy")]
        CREATE_DAMAGEITEMS = 98,
        [PermissionDetails(PermissionGroupCode.EXCHANGE, "Cập nhật xuất hủy")]
        UPDATE_DAMAGEITEMS = 99,
        [PermissionDetails(PermissionGroupCode.EXCHANGE, "Xóa xuất hủy")]
        DELETE_DAMAGEITEMS = 100,
        [PermissionDetails(PermissionGroupCode.EXCHANGE, "Sao chép xuất hủy")]
        COPY_DAMAGEITEMS = 101,
        [PermissionDetails(PermissionGroupCode.EXCHANGE, "Xuất file xuất hủy")]
        EXPORT_DAMAGEITEMS = 102,

        //CUSTOMER
        [PermissionDetails(PermissionGroupCode.CUSTOMER, "Xem DS khách hàng")]
        VIEW_CUSTOMERS = 103,
        [PermissionDetails(PermissionGroupCode.CUSTOMER, "Thêm mới khách hàng")]
        CREATE_CUSTOMERS = 104,
        [PermissionDetails(PermissionGroupCode.CUSTOMER, "Cập nhật khách hàng")]
        UPDATE_CUSTOMERS = 105,
        [PermissionDetails(PermissionGroupCode.CUSTOMER, "Xóa khách hàng")]
        DELETE_CUSTOMERS = 106,
        [PermissionDetails(PermissionGroupCode.CUSTOMER, "Điện thoại khách hàng")]
        PHONE_CUSTOMERS = 107,
        [PermissionDetails(PermissionGroupCode.CUSTOMER, "Import khách hàng")]
        IMPORT_CUSTOMERS = 108,
        [PermissionDetails(PermissionGroupCode.CUSTOMER, "Xuất file khách hàng")]
        EXPORT_CUSTOMERS = 109,
        [PermissionDetails(PermissionGroupCode.CUSTOMER, "Cập nhật nhóm khách hàng")]
        UPDATE_GROUP_CUSTOMERS = 110,

        [PermissionDetails(PermissionGroupCode.CUSTOMER, "Xem DS công nợ KH")]
        VIEW_DEBT_KH = 111,
        [PermissionDetails(PermissionGroupCode.CUSTOMER, "Thêm mới công nợ KH")]
        CREATE_DEBT_KH = 112,
        [PermissionDetails(PermissionGroupCode.CUSTOMER, "Cập nhật công nợ KH")]
        UPDATE_DEBT_KH = 113,
        [PermissionDetails(PermissionGroupCode.CUSTOMER, "Xóa công nợ KH")]
        DELETE_DEBT_KH = 114,

        [PermissionDetails(PermissionGroupCode.CUSTOMER, "Thêm mới thanh toán KH")]
        CREATE_PAY_KH = 115,
        [PermissionDetails(PermissionGroupCode.CUSTOMER, "Cập nhật thanh toán KH")]
        UPDATE_PAY_KH = 116,
        [PermissionDetails(PermissionGroupCode.CUSTOMER, "Xóa thanh toán KH")]
        DELETE_PAY_KH = 117,

        [PermissionDetails(PermissionGroupCode.CUSTOMER, "Xem DS tích điểm KH")]
        VIEW_EARN_POINTS = 118,
        [PermissionDetails(PermissionGroupCode.CUSTOMER, "Cập nhật tích điểm KH")]
        UPDATE_EARN_POINTS = 119,

        [PermissionDetails(PermissionGroupCode.CUSTOMER, "Xem DS nhà cung cấp")]
        VIEW_SUPPLIERS = 120,
        [PermissionDetails(PermissionGroupCode.CUSTOMER, "Thêm mới nhà cung cấp")]
        CREATE_SUPPLIERS = 121,
        [PermissionDetails(PermissionGroupCode.CUSTOMER, "Cập nhật nhà cung cấp")]
        UPDATE_SUPPLIERS = 122,
        [PermissionDetails(PermissionGroupCode.CUSTOMER, "Xóa nhà cung cấp")]
        DELETE_SUPPLIERS = 123,
        [PermissionDetails(PermissionGroupCode.CUSTOMER, "Điện thoại nhà cung cấp")]
        PHONE_SUPPLIERS = 124,
        [PermissionDetails(PermissionGroupCode.CUSTOMER, "Import nhà cung cấp")]
        IMPORT_SUPPLIERS = 125,
        [PermissionDetails(PermissionGroupCode.CUSTOMER, "Xuất file nhà cung cấp")]
        ẼPORT_SUPPLIERS = 126,

        [PermissionDetails(PermissionGroupCode.CUSTOMER, "Xem DS công nợ NCC")]
        VIEW_DEBT_NCC = 127,
        [PermissionDetails(PermissionGroupCode.CUSTOMER, "Thêm mới công nợ NCC")]
        CREATE_DEBT_NCC = 128,
        [PermissionDetails(PermissionGroupCode.CUSTOMER, "Cập nhật công nợ NCC")]
        UPDATE_DEBT_NCC = 129,
        [PermissionDetails(PermissionGroupCode.CUSTOMER, "Xóa công nợ NCC")]
        DELETE_DEBT_NCC = 130,

        [PermissionDetails(PermissionGroupCode.CUSTOMER, "Thêm mới thanh toán NCC, ĐTGH")]
        CREATE_PAY_NCC = 131,
        [PermissionDetails(PermissionGroupCode.CUSTOMER, "Xóa thanh toán NCC, ĐTGH")]
        DELETE_PAY_NCC = 132,
        [PermissionDetails(PermissionGroupCode.CUSTOMER, "Cập nhật thanh toán NCC, ĐTGH")]
        UPDATE_PAY_NCC = 133,

        [PermissionDetails(PermissionGroupCode.CUSTOMER, "Xem DS đối tác giao hàng")]
        VIEW_PARTNERDELIVERY = 134,
        [PermissionDetails(PermissionGroupCode.CUSTOMER, "Thêm mới đối tác giao hàng")]
        CREATE_PARTNERDELIVERY = 135,
        [PermissionDetails(PermissionGroupCode.CUSTOMER, "Cập nhật đối tác giao hàng")]
        UPDATE_PARTNERDELIVERY = 136,
        [PermissionDetails(PermissionGroupCode.CUSTOMER, "Xóa đối tác giao hàng")]
        DELETE_PARTNERDELIVERY = 137,
        [PermissionDetails(PermissionGroupCode.CUSTOMER, "Import đối tác giao hàng")]
        IMPORT_PARTNERDELIVERY = 138,
        [PermissionDetails(PermissionGroupCode.CUSTOMER, "Xuất file đối tác giao hàng")]
        EXPORT_PARTNERDELIVERY = 139,

        [PermissionDetails(PermissionGroupCode.CUSTOMER, "Xem DS công nợ đối tác GH")]
        VIEW_DEBT_DTGH = 140,
        [PermissionDetails(PermissionGroupCode.CUSTOMER, "Thêm mới công nợ đối tác GH")]
        CREATE_DEBT_DTGH = 141,
        [PermissionDetails(PermissionGroupCode.CUSTOMER, "Cập nhật công nợ đối tác GH")]
        UPDATE_DEBT_DTGH = 142,
        [PermissionDetails(PermissionGroupCode.CUSTOMER, "Xóa công nợ đối tác GH")]
        DELETE_DEBT_DTGH = 143,


        //REPORT
        [PermissionDetails(PermissionGroupCode.REPORT, "Tổng hợp cuối ngày")]
        GENERAL_ENDOFDAYREPORT = 144,
        [PermissionDetails(PermissionGroupCode.REPORT, "Hàng hóa cuối ngày")]
        STOCK_ENDOFDAYREPORT = 145,
        [PermissionDetails(PermissionGroupCode.REPORT, "Thu chi cuối ngày")]
        IN_OUT_ENDOFDAYREPORT = 146,
        [PermissionDetails(PermissionGroupCode.REPORT, "Bán hàng cuối ngày")]
        SALE_ENDOFDAYREPORT = 147,


        [PermissionDetails(PermissionGroupCode.REPORT, "Nhân viên bán hàng")]
        EMPLOYEE_SALEREPORT = 148,
        [PermissionDetails(PermissionGroupCode.REPORT, "Lợi nhuận bán hàng")]
        PROFIT_SALEREPORT = 149,
        [PermissionDetails(PermissionGroupCode.REPORT, "Giảm giá hóa đơn bán hàng")]
        DISCOUNT_SALEREPORT = 150,
        [PermissionDetails(PermissionGroupCode.REPORT, "Trả hàng bán hàng")]
        PURCHASERETURNS_SALEREPORT = 151,
        [PermissionDetails(PermissionGroupCode.REPORT, "Thời gian bán hàng")]
        TIME_SALEREPORT = 152,
        [PermissionDetails(PermissionGroupCode.REPORT, "Chi nhánh bán hàng")]
        BRANCH_SALEREPORT = 153,


        [PermissionDetails(PermissionGroupCode.REPORT, "Giao dịch đặt hàng")]
        TRANFER_ORDERREPORT = 154,
        [PermissionDetails(PermissionGroupCode.REPORT, "Hàng hóa đặt hàng")]
        STOCK_ORDERREPORT = 155,


        [PermissionDetails(PermissionGroupCode.REPORT, "Giá trị kho hàng hóa")]
        VALUE_PRODUCTREPORT = 156,
        [PermissionDetails(PermissionGroupCode.REPORT, "Hạn sử dụng hàng hóa")]
        EXPIRE_DATE_PRODUCTREPORT = 157,
        [PermissionDetails(PermissionGroupCode.REPORT, "Khách theo hàng bán hàng hóa")]
        CUSTOMER_PRODUCTREPORT = 158,
        [PermissionDetails(PermissionGroupCode.REPORT, "Xuất hủy hàng hóa")]
        OUT_PRODUCTREPORT = 159,
        [PermissionDetails(PermissionGroupCode.REPORT, "NCC theo hàng nhập hàng hóa")]
        NCC_PRODUCTREPORT = 160,
        [PermissionDetails(PermissionGroupCode.REPORT, "Nhân viên theo hàng bán hàng hóa")]
        EMPLOYEE_PRODUCTREPORT = 161,
        [PermissionDetails(PermissionGroupCode.REPORT, "Xuất nhập tồn hàng hóa")]
        IN_OUT_PRODUCTREPORT = 162,
        [PermissionDetails(PermissionGroupCode.REPORT, "Xuất nhập tồn chi tiết hàng hóa")]
        IN_OUT_DETAIL_PRODUCTREPORT = 163,
        [PermissionDetails(PermissionGroupCode.REPORT, "Lợi nhuận hàng hóa")]
        PROFIT_PRODUCTREPORT = 164,
        [PermissionDetails(PermissionGroupCode.REPORT, "Bán hàng hàng hóa")]
        SALE_PRODUCTREPORT = 165,


        [PermissionDetails(PermissionGroupCode.REPORT, "Công nợ khách hàng")]
        DEBT_CUSTOMERREPORT = 166,
        [PermissionDetails(PermissionGroupCode.REPORT, "Hàng bán theo khách")]
        STOCK_CUSTOMERREPORT = 167,
        [PermissionDetails(PermissionGroupCode.REPORT, "Bán hàng khách hàng")]
        SALE_CUSTOMERREPORT = 168,
        [PermissionDetails(PermissionGroupCode.REPORT, "Lợi nhuận khách hàng")]
        PROFIT_CUSTOMERREPORT = 169,


        [PermissionDetails(PermissionGroupCode.REPORT, "Công nợ nhà cung cấp")]
        DEBT_SUPPLIERREPORT = 170,
        [PermissionDetails(PermissionGroupCode.REPORT, "Hàng nhập theo NCC")]
        STOCK_SUPPLIERREPORT = 171,
        [PermissionDetails(PermissionGroupCode.REPORT, "Nhập hàng nhà cung cấp")]
        IN_SUPPLIERREPORT = 172,


        [PermissionDetails(PermissionGroupCode.REPORT, "Lợi nhuận nhân viên")]
        PROFIT_USERREPORT = 173,
        [PermissionDetails(PermissionGroupCode.REPORT, "Hàng bán theo NV")]
        STOCK_USERREPORT = 174,
        [PermissionDetails(PermissionGroupCode.REPORT, "Bán hàng nhân viên")]
        SALE_USERREPORT = 175,


        [PermissionDetails(PermissionGroupCode.REPORT, "Hàng bán theo kênh bán hàng")]
        STOCK_SALECHANNELREPORT = 176,
        [PermissionDetails(PermissionGroupCode.REPORT, "Lợi nhuận kênh bán hàng")]
        PROFIT_SALECHANNELREPORT = 177,
        [PermissionDetails(PermissionGroupCode.REPORT, "Bán hàng kênh bán hàng")]
        SALE_SALECHANNELREPORT = 178,

        [PermissionDetails(PermissionGroupCode.REPORT, "Tài chính")]
        FINANCE_SALECHANNELREPORT = 179,



        //CASHFLOW

        [PermissionDetails(PermissionGroupCode.CASHFLOW, "Xem DS sổ quỹ")]
        VIEW_CASHFLOW = 180,
        [PermissionDetails(PermissionGroupCode.CASHFLOW, "Thêm mới sổ quỹ")]
        CREATE_CASHFLOW = 181,
        [PermissionDetails(PermissionGroupCode.CASHFLOW, "Cập nhật sổ quỹ")]
        UPDATE_CASHFLOW = 182,
        [PermissionDetails(PermissionGroupCode.CASHFLOW, "Xóa sổ quỹ")]
        DELETE_CASHFLOW = 183,
        [PermissionDetails(PermissionGroupCode.CASHFLOW, "Xuất file sổ quỹ")]
        EXPORT_CASHFLOW = 184,


        //PROMOTION
        [PermissionDetails(PermissionGroupCode.PROMOTION, "Xem DS chương trình khuyến mại")]
        VIEW_PROMOTION = 185,
        [PermissionDetails(PermissionGroupCode.PROMOTION, "Thêm mới chương trình khuyến mại")]
        CREATE_PROMOTION = 186,
        [PermissionDetails(PermissionGroupCode.PROMOTION, "Cập nhật chương trình khuyến mại")]
        UPDATE_PROMOTION = 187,
        [PermissionDetails(PermissionGroupCode.PROMOTION, "Xóa chương trình khuyến mại")]
        DELETE_PROMOTION = 188,



        //VOUCHER
        [PermissionDetails(PermissionGroupCode.VOUCHER, "Xem DS chương trình khuyến mại")]
        VIEW_VOUCHER = 189,
        [PermissionDetails(PermissionGroupCode.VOUCHER, "Thêm mới chương trình khuyến mại")]
        CREATE_VOUCHER = 190,
        [PermissionDetails(PermissionGroupCode.VOUCHER, "Cập nhật chương trình khuyến mại")]
        UPDATE_VOUCHER = 191,
        [PermissionDetails(PermissionGroupCode.VOUCHER, "Xóa chương trình khuyến mại")]
        DELETE_VOUCHER = 192,
        [PermissionDetails(PermissionGroupCode.VOUCHER, "Phát hành chương trình khuyến mại")]
        RELEASE_VOUCHER = 193,
    }
}