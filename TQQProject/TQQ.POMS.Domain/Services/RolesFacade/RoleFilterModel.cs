
using TQQ.POMS.Framework.Data;

namespace TQQ.POMS.Domain.Services.RolesFacade 		
{
	public class RoleFilterModel
	{
		public string Search { set; get; }

        public PagingInfo Paging { set; get; }
	}
}


