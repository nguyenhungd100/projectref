
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using TQQ.POMS.Framework.Utils;
using TQQ.POMS.Domain.Data;
using TQQ.POMS.Domain.Data.Entity;
using TQQ.POMS.Framework.Data;
using System.Linq.Expressions;

namespace TQQ.POMS.Domain.Services.RolesFacade.Implementation
{
    public class RolesService : IRolesService
    {
        IEntityRepository<Roles, DomainContext> _rolesRepository;

        public RolesService(IEntityRepository<Roles, DomainContext> rolesRepository)
        {
            _rolesRepository = rolesRepository;
        }

        public async Task<ActionResult> Add(RolesModel roles)
        {
            //add to storage
            //var entity = roles.CloneToModel<RolesModel, Roles>();
            var entity = DomainMaps.Mapper.Map<Roles>(roles);
            var res = await _rolesRepository.AddAsync(entity);

            return new ActionResult()
            {
                Result = (res != null ? true : false)
            };
        }

        public async Task<ActionResult> Delete(int[] Ids)
        {
            var roleIds = Enum.GetValues(typeof(SystemRoles)).Cast<int>().ToArray();
            if (Ids.Any(c => roleIds.Contains(c)))
            {
                return new ActionResult()
                {
                    Result = false,
                    Error = "Không được xóa các role của hệ thống"
                };
            }

            var res = await _rolesRepository.DeleteManyAsync(c => Ids.Contains(c.Id));

            return new ActionResult()
            {
                Result = (res > 0 ? true : false)
            };
        }

        public async Task<RolesModel> GetById(int rolesId)
        {
            var entity = await _rolesRepository.GetByIdAsync(rolesId);
            return entity.Map<RolesModel>();
        }

        public async Task<List<RolesModel>> List(RoleFilterModel filter)
        {
            var res = await _rolesRepository.FetchAsync(c => (!String.IsNullOrEmpty(filter.Search) ? true : true), c => c.Id, filter.Paging);

            return res.Map<List<RolesModel>>();
        }

        public List<RolesModel> List(Expression<Func<Roles, bool>> query)
        {
            var res = _rolesRepository.Fetch(query);
            return res.Map<List<RolesModel>>();
        }

        public async Task<ActionResult> Update(RolesModel roles)
        {
            var exist = await _rolesRepository.GetByIdAsync(roles.Id);
            if (exist == null)
                return new ActionResult()
                {
                    Result = false,
                    Error = "roles không tồn tại"
                };

            //update to storage
            var entity = roles.Map<Roles>();
            var res = await _rolesRepository.UpdateAsync(entity);

            return new ActionResult()
            {
                Result = res
            };
        }

        public async Task<bool> HasPermission(int[] roleIds, PermissionCode permission)
        {
            var code = $";{permission.GetHashCode()};";
            var roles = await _rolesRepository.FetchAsync(c => roleIds.Contains(c.Id));
            if (roles.Any(c => !String.IsNullOrEmpty(c.Permissions) && c.Permissions.Contains(code)))
            {
                return true;
            }
            return false;
        }

        public static List<PermissionGroup> ListPermissions()
        {
            return PermissionHelper.GetPermissionGroups();
        }

        public async Task<List<RolesModel>> GetAll()
        {
            var res = await _rolesRepository.FetchAsync();

            return res.Map<List<RolesModel>>();
        }
    }
}

