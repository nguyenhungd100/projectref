
using System;
namespace TQQ.POMS.Domain.Services.RolesFacade
{
    public class RolesModel
    {
        public Int32 Id { set; get; }
        public String RoleName { set; get; }
        public String Description { set; get; }
        public String Title { set; get; }
        public String Permissions { set; get; }
    }
}



