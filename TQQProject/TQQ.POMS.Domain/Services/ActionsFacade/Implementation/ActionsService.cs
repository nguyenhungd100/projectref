using TQQ.POMS.Domain.Data;
using TQQ.POMS.Domain.Data.Entity;
using TQQ.POMS.Framework.Data;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using TQQ.POMS.Framework.Utils;
using System.Threading.Tasks;

namespace TQQ.POMS.Domain.Services.ActionsFacade.Implementation
{
    public class ActionsService : IActionsService
    {
        IEntityRepository<Actions, DomainContext> _actionsRepository;
         public ActionsService(IEntityRepository<Actions, DomainContext> actionsRepository)
        {
            _actionsRepository = actionsRepository;
        }

        public ActionResultType<bool> Delete(int id)
        {

            var item = _actionsRepository.GetById(id);
            var res = _actionsRepository.Delete(item);
            return new ActionResultType<bool>() { Result= res};
        }

        public ActionResultType< List<ActionsModel>> FillterModel(FilterActionsModel filter)
        {
            using (var db = _actionsRepository.GetDbContext())
            {
                var q = from o in db.Actions
                        select o;
                if (!string.IsNullOrEmpty( filter.Search))
                {
                  //  q = q.Where(c => c.Name.Contains(filter.Search) || c.Code.Contains(filter.Search));
                }
                var list = q.OrderByDescending(c => c.ActionCode).Skip(filter.Paging.StartRowIndex).Take(filter.Paging.PageSize).ToList();
                filter.Paging.RowsCount = q.Count();
                var listModels = list.Map<List<ActionsModel>>();
               
                return new ActionResultType<List<ActionsModel>>() { Result=true, Data= listModels};
            }
        }

        public async Task<List<ActionsModel>> GetAll()
        {
            var res = await _actionsRepository.FetchAsync();
            return res.Map<List<ActionsModel>>();
        
        }

        public ActionResultType< ActionsModel> GetByCode(string code)
        {
            var item = _actionsRepository.SingleAsync(a => a.ActionCode == code);
            var itemModel = item.Map<ActionsModel>();
            return new ActionResultType<ActionsModel>() { Result = true, Data= itemModel };
        }

        //public ActionResult Save(ActionsModel model)
        //{
            
        //    var item = model.Map<Actions>();
        //    if (model.Id==0)
        //    {
        //        var res = _actionsRepository.Insert(item);
        //        return new ActionResult() { Result = res };
        //    }
        //    else
        //    {
        //        var res = _actionsRepository.Update(item);
        //        return new ActionResult() { Result = res };
        //    }
        //}
    }
}
