using System;
using System.Collections.Generic;
using System.Text;

namespace TQQ.POMS.Domain.Services.ActionsFacade
{
    public class ActionsModel
    {

        public string ActionCode { get; set; }
                 
        public string ActionName { get; set; }
                  
    }
}
