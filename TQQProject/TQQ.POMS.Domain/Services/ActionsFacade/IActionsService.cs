using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using TQQ.POMS.Framework.Data;

namespace TQQ.POMS.Domain.Services.ActionsFacade
{
    public interface IActionsService
    {
        ActionResultType<bool> Delete(int id);
        ActionResultType<ActionsModel> GetByCode(string code);
        //ActionResult Save(ActionsModel model);
        ActionResultType<List<ActionsModel>> FillterModel(FilterActionsModel filter);
        Task<List<ActionsModel>> GetAll();
    }
}
