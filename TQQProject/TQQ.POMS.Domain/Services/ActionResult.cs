﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TQQ.POMS.Domain.Services
{
    public class ActionResult
    {
        public bool Result { set; get; }

        public string Error { set; get; }

        public object Data { set; get; }

        public static implicit operator decimal(ActionResult v)
        {
            throw new NotImplementedException();
        }
    }

    public class ActionResultType<T> : ActionResult
    {
        public new T Data { set; get; }
    }
}
