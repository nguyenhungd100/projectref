﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using TQQ.POMS.Domain.Data;
using TQQ.POMS.Domain.Data.Entity;
using TQQ.POMS.Framework.Data;

namespace TQQ.POMS.Domain.Services.WardFacade.Implementation
{
    public class WardService : IWardService
    {
        IEntityRepository<Ward, DomainContext> _wardRepository;

        public WardService(IEntityRepository<Ward, DomainContext> wardRepository)
        {
            _wardRepository = wardRepository;
        }

        public async Task<List<WardModel>> GetWardByDistrictId(int districtId)
        {
            var res = await _wardRepository.FetchAsync(c => c.DistrictId == districtId);

            return res.Map<List<WardModel>>();
        }
    }
}
