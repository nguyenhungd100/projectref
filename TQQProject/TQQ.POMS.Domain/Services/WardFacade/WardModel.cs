﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TQQ.POMS.Domain.Services.WardFacade
{
    public class WardModel
    {
        public Int32 Id { get; set; }
        public Int32 DistrictId { get; set; }
        public string WardName { get; set; }
    }
}
