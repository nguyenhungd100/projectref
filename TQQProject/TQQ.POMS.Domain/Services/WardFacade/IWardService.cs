﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace TQQ.POMS.Domain.Services.WardFacade
{
    public interface IWardService
    {
        /// <summary>
        /// Lấy danh sách Phường Xã theo Quận Huyện
        /// </summary>
        /// <returns></returns>
        Task<List<WardModel>> GetWardByDistrictId(int districtId);
    }
}
