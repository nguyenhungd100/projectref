
using TQQ.POMS.Framework.Data;

namespace TQQ.POMS.Domain.Services.DepartmentFacade 		
{
	public class DepartmentFilterModel
	{
		public string Search { set; get; }

        public PagingInfo Paging { set; get; }
	}
}


