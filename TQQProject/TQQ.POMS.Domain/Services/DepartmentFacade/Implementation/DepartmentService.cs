
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using TQQ.POMS.Framework.Utils;
using TQQ.POMS.Domain.Data;
using TQQ.POMS.Domain.Data.Entity;
using TQQ.POMS.Framework.Data;
using System.Linq.Expressions;

namespace TQQ.POMS.Domain.Services.DepartmentFacade.Implementation
{
    public class DepartmentService : IDepartmentService
    {
        IEntityRepository<Department, DomainContext> _departmentRepository;

        public DepartmentService(IEntityRepository<Department, DomainContext> departmentRepository)
        {
            _departmentRepository = departmentRepository;
        }

        public async Task<ActionResult> AddAsync(DepartmentModel department)
        {
            //add to storage
            var entity = department.Map<Department>();
            var res = await _departmentRepository.AddAsync(entity);

            return new ActionResult()
            {
                Result = (res != null ? true : false)
            };
        }

        public async Task<ActionResult> DeleteAsync(long[] Ids)
        {
            var res = await _departmentRepository.DeleteManyAsync(c => Ids.Contains(c.Id));

            return new ActionResult()
            {
                Result = (res > 0 ? true : false)
            };
        }

        public async Task<DepartmentModel> GetByIdAsync(long departmentId)
        {
            var entity = await _departmentRepository.GetByIdAsync(departmentId);
            return entity.Map<DepartmentModel>();
        }

        public async Task<List<DepartmentModel>> ListAsync(DepartmentFilterModel filter)
        {
            var res = await _departmentRepository.FetchAsync(c => (!String.IsNullOrEmpty(filter.Search) ? c.Name.Contains(filter.Search) || c.Code.Contains(filter.Search) : true), c => c.Id, filter.Paging);

            return res.Map<List<DepartmentModel>>();
        }

        public ActionResultType<List<DepartmentModel>> List()
        {
            using (var db = _departmentRepository.GetDbContext())
            {
                var q = from o in db.Departments
                        select o;

                var listModels = q.ToList().Map<List<DepartmentModel>>();

                return new ActionResultType<List<DepartmentModel>>() { Result = true, Data = listModels };
            }
        }

        public async Task<DepartmentModel> SingleAsync(Expression<Func<Department, bool>> expression)
        {
            var entity = await _departmentRepository.SingleAsync(expression);
            return entity.Map<DepartmentModel>();
        }

        public async Task<ActionResult> UpdateAsync(DepartmentModel department)
        {
            var exist = await _departmentRepository.GetByIdAsync(department.Id);
            if (exist == null)
                return new ActionResult()
                {
                    Result = false,
                    Error = "Phòng ban không tồn tại"
                };

            //update to storage
            var entity = department.Map<Department>();
            var res = await _departmentRepository.UpdateAsync(entity);

            return new ActionResult()
            {
                Result = res
            };
        }
    }
}

