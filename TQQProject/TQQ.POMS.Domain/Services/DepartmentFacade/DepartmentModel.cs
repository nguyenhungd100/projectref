﻿
using System;
using System.ComponentModel.DataAnnotations;

namespace TQQ.POMS.Domain.Services.DepartmentFacade
{
    public class DepartmentModel
    {
        public long Id { set; get; }

        [MaxLength(10, ErrorMessage = "Mã phòng ban tối đa là 10 ký tự")]
        [MinLength(1, ErrorMessage = "Mã phòng ban tối thiểu là 1 ký tự")]
        [Required(ErrorMessage = "Mã phòng ban là bắt buộc")]
        public string Code { get; set; }

        [MaxLength(250, ErrorMessage = "Tên phòng ban tối đa là 250 ký tự")]
        [MinLength(10, ErrorMessage = "Tên phòng ban tối thiểu là 10 ký tự")]
        [Required(ErrorMessage = "Tên phòng ban là bắt buộc")]
        public string Name { get; set; }
    }
}



