
using TQQ.POMS.Domain.Data.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace TQQ.POMS.Domain.Services.DepartmentFacade
{
    public interface IDepartmentService
    {
        Task<DepartmentModel> SingleAsync(Expression<Func<Department, bool>> expression);

        Task<DepartmentModel> GetByIdAsync(long departmentId);

        Task<ActionResult> AddAsync(DepartmentModel department);

        Task<ActionResult> UpdateAsync(DepartmentModel department);

        Task<ActionResult> DeleteAsync(long[] Ids);

        Task<List<DepartmentModel>> ListAsync(DepartmentFilterModel filter);

        ActionResultType<List<DepartmentModel>> List();
    }
}


