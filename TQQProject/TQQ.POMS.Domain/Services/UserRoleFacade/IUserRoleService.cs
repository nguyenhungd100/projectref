
using TQQ.POMS.Domain.Data.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using TQQ.POMS.Domain.Services.RolesFacade;

namespace TQQ.POMS.Domain.Services.UserRoleFacade 		
{
	public interface IUserRoleService
    {
		Task<UserRoleModel> GetById(int userRoleIds);

        Task<ActionResult> Update(UserRoleModel userRole);

        Task<ActionResult> Delete(int userRoleId);

        Task<UserRoleModel> GetByUserIdAndBranchId(int userId, int BranchId);
        

    }
}


