
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using TQQ.POMS.Framework.Utils;
using TQQ.POMS.Domain.Data;
using TQQ.POMS.Domain.Data.Entity;
using TQQ.POMS.Framework.Data;
using System.Linq.Expressions;
using TQQ.POMS.Domain.Services.RolesFacade;

namespace TQQ.POMS.Domain.Services.UserRoleFacade.Implementation
{
    public class UserRoleService : IUserRoleService
    {
        IEntityRepository<UserRole, DomainContext> _userRoleRepository;

        public UserRoleService(IEntityRepository<UserRole, DomainContext> userRoleRepository)
        {
            _userRoleRepository = userRoleRepository;
        }

        public async Task<ActionResult> Add(UserRoleModel userRole)
        {
            //add to storage
            //var entity = roles.CloneToModel<RolesModel, Roles>();
            var entity = DomainMaps.Mapper.Map<UserRole>(userRole);
            var res = await _userRoleRepository.AddAsync(entity);

            return new ActionResult()
            {
                Result = (res != null ? true : false)
            };
        }

        public async Task<ActionResult> Delete(int userRoleId)
        {
            
            var res = await _userRoleRepository.DeleteManyAsync(c => c.Id == userRoleId);

            return new ActionResult()
            {
                Result = (res > 0 ? true : false)
            };
        }

        public async Task<UserRoleModel> GetById(int userRoleId)
        {
            var entity = await _userRoleRepository.GetByIdAsync(userRoleId);
            return entity.Map<UserRoleModel>();
        } 

        public async Task<ActionResult> Update(UserRoleModel userRole)
        {
            var exist = await _userRoleRepository.GetByIdAsync(userRole.Id);
            
           
            if (exist == null)
            {
                //Check if not exist then add new
                var entity = DomainMaps.Mapper.Map<UserRole>(userRole);
                var res = await _userRoleRepository.AddAsync(entity);
                
                return new ActionResult()
                {
                    Result = (res != null ? true : false)
                };
            }
            else
            {
                //if exist then update
                var entity = userRole.Map<UserRole>();
                var res = await _userRoleRepository.UpdateAsync(entity);

                return new ActionResult()
                {
                    Result = res
                };
            }
            
        }

        public async Task<UserRoleModel> GetByUserIdAndBranchId(int userId, int branchId)
        {
            var entity = await _userRoleRepository.SingleAsync(c => c.UserId == userId && c.BranchId == branchId);
            
            return entity.Map<UserRoleModel>();
        }
    }
}

