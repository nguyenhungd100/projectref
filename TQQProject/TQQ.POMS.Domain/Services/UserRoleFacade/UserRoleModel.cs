﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TQQ.POMS.Domain.Services.UserRoleFacade
{
    public class UserRoleModel
    {
        public Int32 Id { set; get; }
        public Int32 RoleId { set; get; }
        public Int32 UserId { set; get; }
        public Int32 BranchId { set; get; }
        public String Permission { set; get; }
    }
}
