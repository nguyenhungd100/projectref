﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using TQQ.POMS.Domain.Data;
using TQQ.POMS.Domain.Data.Entity;
using TQQ.POMS.Framework.Data;

namespace TQQ.POMS.Domain.Services.DistrictFacade.Implementation
{
    public class DistrictService : IDistrictService
    {
        IEntityRepository<District, DomainContext> _districtRepository;

        public DistrictService(IEntityRepository<District, DomainContext> districtRepository)
        {
            _districtRepository = districtRepository;
        }

        public async Task<List<DistrictModel>> GetAllDistrict()
        {
            var res = await _districtRepository.FetchAsync();

            return res.Map<List<DistrictModel>>();
        }
    }
}
