﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TQQ.POMS.Domain.Services.DistrictFacade
{
    public class DistrictModel
    {
        public Int32 DistrictId { get; set; }
        public string DistrictName { get; set; }
    }
}
