﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace TQQ.POMS.Domain.Services.DistrictFacade
{
    public interface IDistrictService
    {
        /// <summary>
        /// Lấy danh sách Quận Huyện
        /// </summary>
        /// <returns></returns>
        Task<List<DistrictModel>> GetAllDistrict();
    }
}
