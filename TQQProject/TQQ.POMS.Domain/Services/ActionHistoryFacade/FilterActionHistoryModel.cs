using System;
using System.Collections.Generic;
using System.Text;
using TQQ.POMS.Domain.Shared;
using TQQ.POMS.Framework.Data;

namespace TQQ.POMS.Domain.Services.ActionHistoryFacade
{
    public class FilterActionHistoryModel : BaseFilterModel
    {
        public string Account { get; set; }

        public string FunctionCode { get; set; }

        public string ActionCode { get; set; }

        public DateTime FromDate { get; set;}

        public DateTime ToDate { get; set; }
    }
}
