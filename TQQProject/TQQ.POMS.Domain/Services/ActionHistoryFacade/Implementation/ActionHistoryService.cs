using TQQ.POMS.Domain.Data;
using TQQ.POMS.Domain.Data.Entity;
using TQQ.POMS.Framework.Data;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using TQQ.POMS.Framework.Utils;

namespace TQQ.POMS.Domain.Services.ActionHistoryFacade.Implementation
{
    public class ActionHistoryService : IActionHistoryService
    {
        IEntityRepository<ActionHistory, DomainContext> _actionHistoryRepository;
         public ActionHistoryService(IEntityRepository<ActionHistory, DomainContext> actionHistoryRepository)
        {
            _actionHistoryRepository = actionHistoryRepository;
        }

        public ActionResultType<bool> Delete(int id)
        {

            var item = _actionHistoryRepository.GetById(id);
            var res = _actionHistoryRepository.Delete(item);
            return new ActionResultType<bool>() { Result= res};
        }

        public ActionResultType< List<ActionHistoryModel>> FillterModel(FilterActionHistoryModel filter)
        {
            using (var db = _actionHistoryRepository.GetDbContext())
            {
                var q = from o in db.ActionHistory
                        select o;
                if (!string.IsNullOrEmpty( filter.Search))
                {
                    q = q.Where(c => c.Description.Contains(filter.Search));
                }
                if (!string.IsNullOrEmpty(filter.Account))
                {
                    q = q.Where(c => filter.Search.Contains( c.Account));
                }
                if (!string.IsNullOrEmpty(filter.FunctionCode))
                {
                    q = q.Where(c => filter.FunctionCode.Contains(c.FunctionCode));
                }
                if (!string.IsNullOrEmpty(filter.Branch))
                {
                    q = q.Where(c => filter.Branch.Contains(c.BranchId.ToString()));
                }
                if (!string.IsNullOrEmpty(filter.ActionCode))
                {
                    q = q.Where(c => filter.ActionCode.Contains(c.ActionCode));
                }
                if(!string.IsNullOrEmpty(filter.FromDate + "") && !string.IsNullOrEmpty(filter.ToDate + ""))
                {
                    q = q.Where(c => filter.FromDate <= c.CreatedDate && filter.ToDate >= c.CreatedDate);
                }
                var list = q.OrderByDescending(c => c.Id).Skip(filter.Paging.StartRowIndex).Take(filter.Paging.PageSize).ToList();
                filter.Paging.RowsCount = q.Count();
                var listModels = list.Map<List<ActionHistoryModel>>();
               
                return new ActionResultType<List<ActionHistoryModel>>() { Result=true, Data= listModels};
            }
        }

        public ActionResultType< ActionHistoryModel> GetById(int id)
        {
            var item = _actionHistoryRepository.GetById(id);
            var itemModel = item.Map<ActionHistoryModel>();
            return new ActionResultType<ActionHistoryModel>() { Result = true, Data= itemModel };
        }

        public ActionResult Save(ActionHistoryModel model)
        {
            
            var item = model.Map<ActionHistory>();
            if (model.Id==0)
            {
                var res = _actionHistoryRepository.Insert(item);
                return new ActionResult() { Result = res };
            }
            else
            {
                var res = _actionHistoryRepository.Update(item);
                return new ActionResult() { Result = res };
            }
        }
    }
}
