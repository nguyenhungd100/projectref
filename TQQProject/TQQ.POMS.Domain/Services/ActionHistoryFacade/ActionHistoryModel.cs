using System;
using System.Collections.Generic;
using System.Text;

namespace TQQ.POMS.Domain.Services.ActionHistoryFacade
{
    public class ActionHistoryModel
    {

        public Int32 Id { get; set; }
                 
        public string Account { get; set; }
                 
        public string FunctionCode { get; set; }
                 
        public DateTime? CreatedDate { get; set; }
                 
        public string Description { get; set; }
                 
        public string ActionCode { get; set; }
                 
        public Int32? BranchId { get; set; }
                  
    }
}
