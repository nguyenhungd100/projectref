using System;
using System.Collections.Generic;
using System.Text;
using TQQ.POMS.Framework.Data;

namespace TQQ.POMS.Domain.Services.ActionHistoryFacade
{
    public interface IActionHistoryService
    {
        ActionResultType<bool> Delete(int id);
        ActionResultType<ActionHistoryModel> GetById(int id);
        ActionResult Save(ActionHistoryModel model);
        ActionResultType<List<ActionHistoryModel>> FillterModel(FilterActionHistoryModel filter);
    }
}
