using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using TQQ.POMS.Framework.Data;

namespace TQQ.POMS.Domain.Services.TimeAccessFacade
{
    public interface ITimeAccessService
    {
        ActionResultType<bool> Delete(int id);
        ActionResultType<TimeAccessModel> GetById(int id);
        ActionResult Save(TimeAccessModel model);
        ActionResultType<List<TimeAccessModel>> FillterModel(FilterTimeAccessModel filter);

        ActionResult SaveAll(List<TimeAccessModel> ls);
        ActionResultType<List<TimeAccessModel>> GetListByUserId(int userId);
    }
}
