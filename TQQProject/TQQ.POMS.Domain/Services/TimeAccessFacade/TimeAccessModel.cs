using System;
using System.Collections.Generic;
using System.Text;

namespace TQQ.POMS.Domain.Services.TimeAccessFacade
{
    public class TimeAccessModel
    {

        public Int32 Id { get; set; }
                 
        public Boolean? IsChild { get; set; }
                 
        public Int32? Order { get; set; }
                 
        public Int32? UserId { get; set; }
                 
        public string DayOfWeek { get; set; }
                 
        public string Name { get; set; }
                 
        public Boolean? IsActive { get; set; }

        public string From { get; set; }

        public string To { get; set; }

    }
}
