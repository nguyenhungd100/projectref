using TQQ.POMS.Domain.Data;
using TQQ.POMS.Domain.Data.Entity;
using TQQ.POMS.Framework.Data;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using TQQ.POMS.Framework.Utils;
using System.Threading.Tasks;

namespace TQQ.POMS.Domain.Services.TimeAccessFacade.Implementation
{
    public class TimeAccessService : ITimeAccessService
    {
        IEntityRepository<TimeAccess, DomainContext> _TimeAccessRepository;
         public TimeAccessService(IEntityRepository<TimeAccess, DomainContext> TimeAccessRepository)
        {
            _TimeAccessRepository = TimeAccessRepository;
        }

        public ActionResultType<bool> Delete(int id)
        {

            var item = _TimeAccessRepository.GetById(id);
            var res = _TimeAccessRepository.Delete(item);
            return new ActionResultType<bool>() { Result= res};
        }

        public ActionResultType< List<TimeAccessModel>> FillterModel(FilterTimeAccessModel filter)
        {
            using (var db = _TimeAccessRepository.GetDbContext())
            {
                var q = from o in db.TimeAccess
                        select o;
                if (!string.IsNullOrEmpty( filter.Search))
                {
                  //  q = q.Where(c => c.Name.Contains(filter.Search) || c.Code.Contains(filter.Search));
                }
                var list = q.OrderByDescending(c => c.Id).Skip(filter.Paging.StartRowIndex).Take(filter.Paging.PageSize).ToList();
                filter.Paging.RowsCount = q.Count();
                var listModels = list.Map<List<TimeAccessModel>>();
               
                return new ActionResultType<List<TimeAccessModel>>() { Result=true, Data= listModels};
            }
        }

        public ActionResultType< TimeAccessModel> GetById(int id)
        {
            var item = _TimeAccessRepository.GetById(id);
            var itemModel = item.Map<TimeAccessModel>();
            return new ActionResultType<TimeAccessModel>() { Result = true, Data= itemModel };
        }

        public ActionResult Save(TimeAccessModel model)
        {
            
            var item = model.Map<TimeAccess>();
            if (model.Id==0)
            {
                var res = _TimeAccessRepository.Insert(item);
                return new ActionResult() { Result = res };
            }
            else
            {
                var res = _TimeAccessRepository.Update(item);
                return new ActionResult() { Result = res };
            }
        }

        public ActionResult SaveAll(List<TimeAccessModel> ls)
        {
            
            var timeAccess = ls.Map<List<TimeAccess>>();
            var res = false;
            foreach(TimeAccess item in timeAccess)
            {
                if(item.Id == 0)
                {
                    res =  _TimeAccessRepository.Insert(item);
                }
                else
                {
                    res =  _TimeAccessRepository.Update(item);
                }
            }

            return new ActionResult()
            {
                Result = res
            };
        }

        public ActionResultType<List<TimeAccessModel>> GetListByUserId(int userId)
        {
            using (var db = _TimeAccessRepository.GetDbContext())
            {
                var q = from o in db.TimeAccess
                        select o;

                var list = q.Where(c => c.UserId == userId);

                var listModels = list.Map<List<TimeAccessModel>>();

                return new ActionResultType<List<TimeAccessModel>>() { Result = true, Data = listModels };
            }
        }

    }
}
