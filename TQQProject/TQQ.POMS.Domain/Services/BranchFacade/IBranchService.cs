using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using TQQ.POMS.Framework.Data;

namespace TQQ.POMS.Domain.Services.BranchFacade
{
    public interface IBranchService
    {
        ActionResultType<bool> Delete(int id);
        ActionResultType<BranchModel> GetById(int id);
        ActionResult Save(BranchModel model);
        ActionResultType<List<BranchModel>> FillterModel(FilterBranchModel filter);
        ActionResultType<List<BranchModel>> GetBranchByCurrentUser(int userId);
        ActionResultType<List<BranchModel>> GetAllBranch();

    }
}
