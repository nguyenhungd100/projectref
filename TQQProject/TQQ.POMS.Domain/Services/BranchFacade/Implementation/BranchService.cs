using TQQ.POMS.Domain.Data;
using TQQ.POMS.Domain.Data.Entity;
using TQQ.POMS.Framework.Data;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using TQQ.POMS.Framework.Utils;
using TQQ.POMS.Domain.Services.UserFacade;
using System.Threading.Tasks;

namespace TQQ.POMS.Domain.Services.BranchFacade.Implementation
{
    public class BranchService : IBranchService
    {
        IEntityRepository<Branch, DomainContext> _branchRepository;
         public BranchService(IEntityRepository<Branch, DomainContext> branchRepository)
        {
            _branchRepository = branchRepository;
        }

        public ActionResultType<bool> Delete(int id)
        {

            var item = _branchRepository.GetById(id);
            var res = _branchRepository.Delete(item);
            return new ActionResultType<bool>() { Result= res};
        }

        public ActionResultType< List<BranchModel>> FillterModel(FilterBranchModel filter)
        {
            using (var db = _branchRepository.GetDbContext())
            {
                var q = from branch in (from o in db.Branchs
                        join district in db.District on o.DistrictId equals district.DistrictId into district
                        from dis in district.DefaultIfEmpty()
                        join ward in db.Ward on o.WardId equals ward.Id into ward
                        from war in ward.DefaultIfEmpty()
                        join u in db.UserRole on o.Id equals u.BranchId into userRole
                        from userR in userRole.DefaultIfEmpty()
                        select new
                        {
                            o.Id,
                            o.BranchName,
                            o.Address,
                            o.Mobile,
                            o.Email,
                            o.DistrictId,
                            o.WardId,
                            o.Status,
                            dis.DistrictName,
                            war.WardName,
                            userR.UserId
                        })
                        group branch by new
                        {
                            branch.Id,
                            branch.BranchName,
                            branch.Address,
                            branch.Mobile,
                            branch.Email,
                            branch.DistrictId,
                            branch.WardId,
                            branch.Status,
                            branch.DistrictName,
                            branch.WardName

                        } into b
                        select new
                        {
                            b.Key.Id,
                            b.Key.BranchName,
                            b.Key.Address,
                            b.Key.Mobile,
                            b.Key.Email,
                            b.Key.DistrictId,
                            b.Key.WardId,
                            b.Key.Status,
                            b.Key.DistrictName,
                            b.Key.WardName,
                            NumberUser = b.Count(c => c.UserId != null)
                        };
                if (!string.IsNullOrEmpty( filter.Search))
                {
                    q = q.Where(c => c.BranchName.Contains(filter.Search));
                }
                if (filter.UserStatus != null)
                {
                    q = q.Where(c => c.Status == filter.UserStatus.Value);
                }
                var list = q.OrderByDescending(c => c.Id).Skip(filter.Paging.StartRowIndex).Take(filter.Paging.PageSize);
                filter.Paging.RowsCount = q.Count();
                var listModels = list.Select(c => new BranchModel()
                {
                    Id = c.Id,
                    BranchName = c.BranchName,
                    Address = c.Address,
                    Mobile = c.Mobile,
                    DistrictId = c.DistrictId,
                    WardId = c.WardId,
                    Email = c.Email,
                    Status = (UserStatus)c.Status,
                    DistrictName = c.DistrictName,
                    WardName = c.WardName,
                    NumberUser = c.NumberUser
                }).ToList();

                return new ActionResultType<List<BranchModel>>() { Result=true, Data= listModels};
            }
        }

        public ActionResultType<List<BranchModel>> GetAllBranch()
        {
            var res = _branchRepository.FetchAsync(b => b.Status == UserStatus.Actived);
            var list = res.Map<List<BranchModel>>();
            return new ActionResultType<List<BranchModel>>() { Result = true, Data = list };
        }

        public ActionResultType<List<BranchModel>> GetBranchByCurrentUser(int userId)
        {
            using (var db = _branchRepository.GetDbContext())
            {
                var q = from o in db.Branchs
                        join userRole in db.UserRole on o.Id equals userRole.BranchId into userrole
                        from user in userrole.DefaultIfEmpty()
                        select new
                        {
                            o.Id,
                            o.BranchName,
                            o.Address,
                            o.Mobile,
                            o.Email,
                            o.DistrictId,
                            o.WardId,
                            o.Status,
                            user.UserId
                        };
                
                q = q.Where(c => c.UserId == userId && c.Status == UserStatus.Actived);
                var list = q.OrderByDescending(c => c.Id);
                var listModels = list.Select( c => new BranchModel()
                {
                    Id = c.Id,
                    BranchName = c.BranchName,
                    Address = c.Address,
                    Mobile = c.Mobile,
                    DistrictId = c.DistrictId,
                    WardId = c.WardId,
                    Status = (UserStatus)c.Status
                }).ToList();
               
                return new ActionResultType<List<BranchModel>>() { Result=true, Data= listModels};
            }
        }

        public ActionResultType<BranchModel> GetById(int id)
        {
            var item = _branchRepository.GetById(id);
            var itemModel = item.Map<BranchModel>();
            return new ActionResultType<BranchModel>() { Result = true, Data= itemModel };
        }

        public ActionResult Save(BranchModel model)
        {
            
            var item = model.Map<Branch>();
            if (model.Id==0)
            {
                item.Status = UserStatus.Actived;
                var res = _branchRepository.Insert(item);
                return new ActionResult() { Result = res };
            }
            else
            {
                var res = _branchRepository.Update(item);
                return new ActionResult() { Result = res };
            }
        }
    }
}
