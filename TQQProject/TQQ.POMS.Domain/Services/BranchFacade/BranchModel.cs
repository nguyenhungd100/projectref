﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;
using TQQ.POMS.Domain.Services.UserFacade;

namespace TQQ.POMS.Domain.Services.BranchFacade
{
    public class BranchModel
    {

        public Int32 Id { get; set; }

        [Required(ErrorMessage = "Tên chi nhánh là bắt buộc")]
        public string BranchName { get; set; }
                 
        public string Address { get; set; }
                 
        public string Mobile { get; set; }
                 
        public string Email { get; set; }
                 
        public Int32? DistrictId { get; set; }
                 
        public Int32? WardId { get; set; }
                 
        public UserStatus Status { get; set; }

        public string DistrictName { get; set; }

        public string WardName { get; set; }

        public int NumberUser { get; set; }
    }
}
