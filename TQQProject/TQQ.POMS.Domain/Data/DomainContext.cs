namespace TQQ.POMS.Domain.Data
{
    using System;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;
    using TQQ.POMS.Domain.Data.Entity;
    using Microsoft.EntityFrameworkCore;
    using Microsoft.Extensions.Configuration;
    using System.IO;

    public partial class DomainContext : DbContext
    {
        public DomainContext()
        {
        }

        public DomainContext(DbContextOptions<DomainContext> options) : base(options)
        {
            // Database.SetInitializer<DomainContext>(null);
        }

        public static string ConnectionString { get; private set; }

        public virtual DbSet<Department> Departments { get; set; }
        public virtual DbSet<Roles> Roless { get; set; }
        public virtual DbSet<User> Users { get; set; }
        public virtual DbSet<Branch> Branchs { get; set; }
        public virtual DbSet<District> District { get; set; }
        public virtual DbSet<Ward> Ward { get; set; }
        public virtual DbSet<UserRole> UserRole { get; set; }
        public virtual DbSet<Surcharge> Surcharge { get; set; }
        public virtual DbSet<ExpensesOther> ExpensesOther { get; set; }
        public virtual DbSet<ActionHistory> ActionHistory { get; set; }
        public virtual DbSet<Functions> Functions { get; set; }
        public virtual DbSet<Actions> Actions { get; set; }
        public virtual DbSet<Campaign> Campaign { get; set; }
        public virtual DbSet<Voucher> Voucher { get; set; }
        public virtual DbSet<TimeAccess> TimeAccess { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Department>().ToTable("Departments");
            modelBuilder.Entity<Roles>().ToTable("T_Roles");
            modelBuilder.Entity<User>().ToTable("T_Users");
            modelBuilder.Entity<District>().ToTable("T_Districts");
            modelBuilder.Entity<Ward>().ToTable("T_Wards");
            modelBuilder.Entity<UserRole>().ToTable("T_UserRole");
            modelBuilder.Entity<Branch>().ToTable("T_Branchs");
            modelBuilder.Entity<Surcharge>().ToTable("T_Surcharges");
            modelBuilder.Entity<ExpensesOther>().ToTable("T_ExpensesOther");
            modelBuilder.Entity<ActionHistory>().ToTable("T_ActionHistory");
            modelBuilder.Entity<Functions>().ToTable("T_Functions");
            modelBuilder.Entity<Actions>().ToTable("T_Actions");
            modelBuilder.Entity<Campaign>().ToTable("T_Campaign");
            modelBuilder.Entity<Voucher>().ToTable("T_Voucher");
            modelBuilder.Entity<TimeAccess>().ToTable("T_TimeAccess");

        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!String.IsNullOrEmpty(ConnectionString))
            {
                optionsBuilder.UseSqlServer(ConnectionString);
                return;
            }

            if (!optionsBuilder.IsConfigured)
            {
                IConfigurationBuilder builder = new ConfigurationBuilder()
                   .SetBasePath(Directory.GetCurrentDirectory())
                   .AddJsonFile("appsettings.json");

                IConfigurationRoot configuration = builder.Build();
                var env = configuration.GetValue<string>("EnvironmentName");

                configuration = builder.AddJsonFile($"appsettings.{env}.json", optional: true).Build();

                ConnectionString = configuration.GetConnectionString("DefaultConnection");
                optionsBuilder.UseSqlServer(ConnectionString);
            }
        }
    }
}
