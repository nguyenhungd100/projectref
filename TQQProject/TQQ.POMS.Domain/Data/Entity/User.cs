﻿/// <summary>
/// Tài khoản của nhân viên trong hệ thống
/// </summary>

namespace TQQ.POMS.Domain.Data.Entity
{
    using TQQ.POMS.Domain.Services.UserFacade;
    using System;
    using System.Collections.Generic;

    public partial class User
    {
        public int Id { get; set; }
        public string FullName { get; set; }
        public DateTime? Birthday { set; get; }
        public string Password { get; set; }
        public string Email { get; set; }
        //public Nullable<bool> Gender { get; set; }
        public string Mobile { get; set; }
        public UserStatus Status { get; set; }
        public string Avatar { get; set; }
        public DateTime CreatedDate { get; set; }
        public Nullable<System.DateTime> UpdatedDate { get; set; }
        public int? CreatedBy { get; set; }
        public Nullable<int> UpdatedBy { get; set; }
        public string UserRoles { get; set; }
        public string Address { get; set; }
        public string UserName { get; set; }
        public int? District { get; set; }
        public int? Ward { get; set; }
        public string Branch { get; set; }
        public string Language { get; set; }
        public string Note { get; set; }
        public Boolean? IsNotViewOther { get; set; }
        public Boolean? IsAcceptViewGeneralInfo { get; set; }



        //public int? ObjectId { set; get; }
        //public AccountType? AccountType { set; get; }

        //public Guid UserModifiedTimeStamp { set; get; }
    }
}
