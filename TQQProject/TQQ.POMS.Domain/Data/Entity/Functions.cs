﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace TQQ.POMS.Domain.Data.Entity
{
    public class Functions
    {
        [Key]
        public string FunctionCode { get; set; }

        public string FunctionName { get; set; }
    }
}
