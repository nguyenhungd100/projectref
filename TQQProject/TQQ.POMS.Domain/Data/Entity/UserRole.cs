﻿/// <summary>
/// Vai trò của hệ thống
/// </summary>

namespace TQQ.POMS.Domain.Data.Entity
{
    using System;
    using System.Collections.Generic;
    
    public partial class UserRole
    {
        public int Id { get; set; }
        public int? RoleId { get; set; }
        public int? UserId { get; set; }
        public int? BranchId { get; set; }
        public string Permission { get; set; }
    }
}
