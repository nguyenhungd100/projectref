﻿using System;
using System.Collections.Generic;
using System.Text;
using TQQ.POMS.Domain.Services.BranchFacade;
using TQQ.POMS.Domain.Services.UserFacade;

namespace TQQ.POMS.Domain.Data.Entity
{
    public class Branch
    {
        public int Id { get; set; }

        public string BranchName { get; set; }

        public string Address { get; set; }

        public string Mobile { get; set; }

        public string Email { get; set; }

        public int DistrictId { get; set; }

        public int WardId { get; set; }

        public UserStatus Status { get; set; }
        

    }
}
