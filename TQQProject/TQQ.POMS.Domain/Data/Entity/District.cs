﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace TQQ.POMS.Domain.Data.Entity
{
    public partial class District
    {
        
        public int DistrictId { get; set; }
        
        public string DistrictName { get; set; }
    }
}
