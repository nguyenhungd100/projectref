﻿using System;
using System.Collections.Generic;
using System.Text;
using TQQ.POMS.Domain.Services.UserFacade;

namespace TQQ.POMS.Domain.Data.Entity
{
    public class Surcharge
    {
        public int Id { get; set; }

        public String Code { get; set; }

        public String Type { get; set; }

        public Decimal Value { get; set; }

        public String Branchs { get; set; }

        public Nullable<int> Order { get; set; }

        public Nullable<bool> IsAutoSaveBill { get; set; }

        public Nullable<bool> IsRefund { get; set; }

        public Nullable<bool> IsAllBranch { get; set; }

        public UserStatus? Status { get; set; }

        public Nullable<int> ValueType { get; set; }
    }
}
