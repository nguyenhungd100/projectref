﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace TQQ.POMS.Domain.Data.Entity
{
    public partial class Ward
    {
        public int Id { get; set; }

        public int DistrictId { get; set; }
        
        public string WardName { get; set; }
    }
}
