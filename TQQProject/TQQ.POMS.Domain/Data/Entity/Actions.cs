﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace TQQ.POMS.Domain.Data.Entity
{
    public partial class Actions
    {
        [Key]
        public string ActionCode { get; set; }

        public string ActionName { get; set; }
    }
}
