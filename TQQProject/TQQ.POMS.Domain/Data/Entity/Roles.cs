﻿/// <summary>
/// Vai trò của hệ thống
/// </summary>

namespace TQQ.POMS.Domain.Data.Entity
{
    using System;
    using System.Collections.Generic;
    
    public partial class Roles
    {
        public int Id { get; set; }
        public string RoleName { get; set; }
        public string Description { get; set; }
        public string Title { get; set; }
        public string Permissions { get; set; }
    }
}
