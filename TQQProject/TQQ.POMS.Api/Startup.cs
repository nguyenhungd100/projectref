﻿using System;
using System.Collections.Generic;
using System.IO;
using Autofac;
using Autofac.Extensions.DependencyInjection;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Serilog;
using Swashbuckle.AspNetCore.Swagger;
using TQQ.POMS.Api.Authorization;
using TQQ.POMS.Api.Configuration;
using TQQ.POMS.Api.Helpers;

using TQQ.POMS.Domain.Services.RolesFacade;
using TQQ.POMS.Domain.Services.RolesFacade.Implementation;
using TQQ.POMS.Domain.Services;
using TQQ.POMS.Domain.Data;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Antiforgery;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Localization;
using System.Linq;
using Microsoft.AspNetCore.Mvc.ModelBinding.Binders;
using System.Globalization;

namespace TQQ.POMS.Api
{
    public class Startup
    {
        public IConfiguration Configuration { get; }
        private AppSettings appSettings;

        public Startup(IConfiguration configuration, IHostingEnvironment env)
        {
            Configuration = configuration;
            Domain.Services.DomainMaps.Config();

            var builder = new ConfigurationBuilder()
                    .SetBasePath(Directory.GetCurrentDirectory())
                    .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true)
                    .AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: true)
                    .AddEnvironmentVariables();

            Configuration = builder.Build();

            Log.Logger = new LoggerConfiguration()
                        .ReadFrom.Configuration(Configuration)
                        //.WriteTo.Sentry("https://f65f908fde9940f0b2b16c2045643ea9@sentry.io/1260343")
                        .Enrich.FromLogContext()
                        .CreateLogger();
        }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddCors();

            services
                .AddMvc(config =>
                {
                    // Add to global model binders so you don't need to use the [ModelBinder] attribute.
                    config.ModelBinderProviders.Insert(0, new DecimalModelBinderProvider());

                    config.Filters.Add(typeof(GlobalExceptionFilter));
                })
                .SetCompatibilityVersion(CompatibilityVersion.Version_2_1);

            services.AddSwaggerGen(config =>
            {
                config.SwaggerDoc("v1", new Info { Title = "TQQ POMS Api v1", Version = "V1" });
                config.IncludeXmlComments(string.Format("{0}\\TQQ.POMS.Api.xml", AppDomain.CurrentDomain.BaseDirectory));
                config.IncludeXmlComments(string.Format("{0}\\TQQ.POMS.Domain.xml", AppDomain.CurrentDomain.BaseDirectory));

                config.DescribeAllEnumsAsStrings();
                //config.CustomSchemaIds(x => x.FullName);
            });

            // Add our Config object so it can be injected
            var appSettingSection = Configuration.GetSection("AppSettings");
            services.Configure<AppSettings>(appSettingSection);
            appSettings = new AppSettings();
            appSettingSection.Bind(appSettings);

            services.AddAuthentication("Bearer")
                   .AddIdentityServerAuthentication(options =>
                   {
                       options.Authority = appSettings.AuthenticationServer;
                       options.RequireHttpsMetadata = false;
                       options.ApiName = "tqq_api";
                       //options.SupportedTokens = IdentityServer4.AccessTokenValidation.SupportedTokens.Both;
                       //options.ApiSecret = "secret";
                   });

            //Authorize user access
            services.AddAuthorization(options =>
            {
                var permissions = RolesService.ListPermissions();
                var all = new List<Permission>();
                permissions.ForEach(k => all.AddRange(k.Permissions));
                foreach (var permission in all)
                {
                    var policyName = permission.Code.ToString();
                    options.AddPolicy(policyName,
                        policy => policy.Requirements.Add(new PermissionRequirement(permission.Code)));
                }
            });

            //Inject storage user service
            services.ConfigDI(Configuration);

            services.Configure<RequestLocalizationOptions>(options =>
            {
                options.DefaultRequestCulture = new RequestCulture("vi-VN");
                options.SupportedCultures = new List<CultureInfo>
                        {
                            new CultureInfo("vi-VN"),
                            new CultureInfo("en-US")
                        };
                options.SupportedUICultures = new List<CultureInfo>
                        {
                            new CultureInfo("vi"),
                            new CultureInfo("en")
                        };
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            app.AddSentryContext();

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseHsts();
            }

            app.UseCors(builder => builder
                   //.WithOrigins(appSettings.ClientAppRedirectUri)
                   .AllowAnyOrigin()
                   .AllowAnyMethod()
                   .AllowAnyHeader()
                   .AllowCredentials());

            app.UseAuthentication();
            //app.UseHttpsRedirection();

            app.UseRequestLocalization(new RequestLocalizationOptions
            {
                DefaultRequestCulture = new RequestCulture(new CultureInfo("vi-VN")),
                SupportedCultures = new List<CultureInfo>
                        {
                            new CultureInfo("vi-VN"),
                            new CultureInfo("en-US")
                        },
                SupportedUICultures = new List<CultureInfo>
                        {
                            new CultureInfo("vi"),
                            new CultureInfo("en")
                        }
            });

            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=Index}/{id?}");
            });

            //implement docs for api
            app.UseSwagger();
            app.UseSwaggerUI(config =>
            {
                config.SwaggerEndpoint("v1/swagger.json", "TQQ POMS Api v1");
                config.DocExpansion(DocExpansion.None);
                config.ShowExtensions();
            });
        }
    }
}
