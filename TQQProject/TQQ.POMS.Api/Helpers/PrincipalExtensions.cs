﻿using TQQ.POMS.Domain.Services.RolesFacade;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace TQQ.POMS.Api.Helpers
{
    public static class PrincipalExtensions
    {
        public static bool HasPermission(this ClaimsPrincipal claimsPrincipal, PermissionCode permissionCode)
        {
            var permissionsClaims = claimsPrincipal.Claims.FirstOrDefault(c => c.Type == "permissions");
            if (permissionsClaims == null) return false;

            return permissionsClaims.Value.Contains(";" + permissionCode.GetHashCode() + ";");
        }

        public static string GetPermissions(this ClaimsPrincipal claimsPrincipal)
        {
            var permissionsClaims = claimsPrincipal.Claims.FirstOrDefault(c => c.Type == "permissions");
            if (permissionsClaims == null) return string.Empty;

            return permissionsClaims.Value;
        }
    }
}
