﻿using Microsoft.Extensions.DependencyInjection;
using TQQ.POMS.Domain.Services.RolesFacade;
using TQQ.POMS.Domain.Services.RolesFacade.Implementation;
using TQQ.POMS.Domain.Services.UserFacade;
using TQQ.POMS.Domain.Services.UserFacade.Implementation;
using TQQ.POMS.Framework.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TQQ.POMS.Domain.Data;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;

using TQQ.POMS.Domain.Services.DepartmentFacade.Implementation;
using TQQ.POMS.Domain.Services.DepartmentFacade;
using TQQ.POMS.Domain.Services.DistrictFacade;
using TQQ.POMS.Domain.Services.DistrictFacade.Implementation;
using TQQ.POMS.Domain.Services.WardFacade;
using TQQ.POMS.Domain.Services.WardFacade.Implementation;
using TQQ.POMS.Domain.Services.UserRoleFacade;
using TQQ.POMS.Domain.Services.UserRoleFacade.Implementation;
using TQQ.POMS.Domain.Services.BranchFacade.Implementation;
using TQQ.POMS.Domain.Services.BranchFacade;
using TQQ.POMS.Domain.Services.SurchargeFacade;
using TQQ.POMS.Domain.Services.SurchargeFacade.Implementation;
using TQQ.POMS.Domain.Services.ExpensesOtherFacade;
using TQQ.POMS.Domain.Services.ActionHistoryFacade;
using TQQ.POMS.Domain.Services.ExpensesOtherFacade.Implementation;
using TQQ.POMS.Domain.Services.ActionHistoryFacade.Implementation;
using TQQ.POMS.Domain.Services.FunctionsFacade;
using TQQ.POMS.Domain.Services.FunctionsFacade.Implementation;
using TQQ.POMS.Domain.Services.ActionsFacade;
using TQQ.POMS.Domain.Services.ActionsFacade.Implementation;
using TQQ.POMS.Domain.Services.CampaignFacade;
using TQQ.POMS.Domain.Services.CampaignFacade.Implementation;
using TQQ.POMS.Domain.Services.VoucherFacade;
using TQQ.POMS.Domain.Services.VoucherFacade.Implementation;
using TQQ.POMS.Domain.Services.TimeAccessFacade;
using TQQ.POMS.Domain.Services.TimeAccessFacade.Implementation;

namespace TQQ.POMS.Api.Configuration
{
    public static class DIConfigurator
    {
        public static void ConfigDI(this IServiceCollection services, IConfiguration configuration)
        {
            //services
            //    .AddEntityFrameworkSqlServer()
            //    .AddDbContext<DomainContext>((serviceProvider, options) =>
            //                 options.UseSqlServer(configuration.GetConnectionString("DefaultConnection"))
            //                 .UseInternalServiceProvider(serviceProvider), ServiceLifetime.Singleton);

            services.AddScoped(typeof(IEntityRepository<,>), typeof(EntityRepository<,>));


            services.AddScoped<IDepartmentService, DepartmentService>();
            services.AddScoped<IUserService, UserService>();
            services.AddScoped<IRolesService, RolesService>();
            services.AddScoped<IDistrictService, DistrictService>();
            services.AddScoped<IWardService, WardService>();
            services.AddScoped<IUserRoleService, UserRoleService>();
            services.AddScoped<IBranchService, BranchService>();
            services.AddScoped<ISurchargeService, SurchargeService>();
            services.AddScoped<IExpensesOtherService, ExpensesOtherService>();
            services.AddScoped<IActionHistoryService, ActionHistoryService>();
            services.AddScoped<IFunctionsService, FunctionsService>();
            services.AddScoped<IActionsService, ActionsService>();
            services.AddScoped<ICampaignService, CampaignService>();
            services.AddScoped<IVoucherService, VoucherService>();
            services.AddScoped<ITimeAccessService, TimeAccessService>();
        }
    }
}
