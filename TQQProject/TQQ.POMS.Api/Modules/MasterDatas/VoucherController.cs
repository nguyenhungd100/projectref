using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TQQ.POMS.Api.Helpers;

using TQQ.POMS.Framework.Data;
using Microsoft.AspNetCore.Mvc;
using TQQ.POMS.Domain.Services;
using TQQ.POMS.Domain.Services.VoucherFacade;

namespace TQQ.POMS.Api.Modules.Vouchers
{
    public class VoucherController : Controller
    {
        private readonly IVoucherService _voucherService;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="VoucherService"></param>
        public VoucherController(IVoucherService voucherService)
        {
            _voucherService = voucherService;
        }
        /// <summary>
        /// get list
        /// </summary>
        [HttpPost]
        [Route("/api/v1/Voucher/list")]
        public async Task< IActionResult> List([FromBody]FilterVoucherModel filter)
        {
            var res = _voucherService.FillterModel(filter);
            return Json(new ResultBase<PagedList<VoucherModel>>() { success = true, data = new PagedList<VoucherModel>() { list = res.Data, pager = filter.Paging } });
        }
        /// <summary>
        /// Save
        /// </summary>
        [HttpPost]
        [Route("/api/v1/Voucher/save")]
        public async Task<IActionResult> save([FromBody]VoucherModel model)
        {
            if (!ModelState.IsValid)
                return Json(new ResultBase<PagedList<VoucherModel>>() { success = false, message = ModelState.GetErrorsMessage() });
            var res = _voucherService.Save(model);
           
            return Json(new ResultBase<VoucherModel>() { success = res.Result, message = res.Error });

        }
        /// <summary>
        /// Delete
        /// </summary>
        [HttpDelete]
        [Route("/api/v1/Voucher/delete")]
        public async Task<IActionResult> Delete(int id)
        {
            var res = _voucherService.Delete(id);
            return Json(new ResultBase<bool>() {success = res.Result, data = res.Data, message= res.Error });
        }
        /// <summary>
        /// Get by Id
        /// </summary>
        [HttpGet("/api/v1/Voucher/get_by_id")]
        public async Task<IActionResult> GetById(int id)
        {
            var res = _voucherService.GetById(id);
           return Json(new ResultBase<VoucherModel>() {success = res.Result, data = res.Data, message= res.Error });
        }
    }
}