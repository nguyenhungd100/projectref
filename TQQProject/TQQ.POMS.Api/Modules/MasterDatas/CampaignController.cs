using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TQQ.POMS.Api.Helpers;

using TQQ.POMS.Framework.Data;
using Microsoft.AspNetCore.Mvc;
using TQQ.POMS.Domain.Services;
using TQQ.POMS.Domain.Services.CampaignFacade;

namespace TQQ.POMS.Api.Modules.Campaigns
{
    public class CampaignController : Controller
    {
        private readonly ICampaignService _campaignService;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="CampaignService"></param>
        public CampaignController(ICampaignService campaignService)
        {
            _campaignService = campaignService;
        }
        /// <summary>
        /// get list
        /// </summary>
        [HttpPost]
        [Route("/api/v1/Campaign/list")]
        public async Task< IActionResult> List([FromBody]FilterCampaignModel filter)
        {
            var res = _campaignService.FillterModel(filter);
            return Json(new ResultBase<PagedList<CampaignModel>>() { success = true, data = new PagedList<CampaignModel>() { list = res.Data, pager = filter.Paging } });
        }
        /// <summary>
        /// Save
        /// </summary>
        [HttpPost]
        [Route("/api/v1/Campaign/save")]
        public async Task<IActionResult> save([FromBody]CampaignModel model)
        {
            if (!ModelState.IsValid)
                return Json(new ResultBase<PagedList<CampaignModel>>() { success = false, message = ModelState.GetErrorsMessage() });
            var res = _campaignService.Save(model);
           
            return Json(new ResultBase<CampaignModel>() { success = res.Result, message = res.Error });

        }
        /// <summary>
        /// Delete
        /// </summary>
        [HttpDelete]
        [Route("/api/v1/Campaign/delete")]
        public async Task<IActionResult> Delete(int id)
        {
            var res = _campaignService.Delete(id);
            return Json(new ResultBase<bool>() {success = res.Result, data = res.Data, message= res.Error });
        }
        /// <summary>
        /// Get by Id
        /// </summary>
        [HttpGet("/api/v1/Campaign/get_by_id")]
        public async Task<IActionResult> GetById(int id)
        {
            var res = _campaignService.GetById(id);
           return Json(new ResultBase<CampaignModel>() {success = res.Result, data = res.Data, message= res.Error });
        }
    }
}