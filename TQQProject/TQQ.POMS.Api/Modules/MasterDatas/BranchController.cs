using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TQQ.POMS.Api.Helpers;

using TQQ.POMS.Framework.Data;
using Microsoft.AspNetCore.Mvc;
using TQQ.POMS.Domain.Services;
using TQQ.POMS.Domain.Services.BranchFacade;
using Microsoft.AspNetCore.Authorization;

namespace TQQ.POMS.Api.Modules.Branchs
{
    public class BranchController : Controller
    {
        private readonly IBranchService _branchService;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="BranchService"></param>
        public BranchController(IBranchService branchService)
        {
            _branchService = branchService;
        }
        /// <summary>
        /// get list
        /// </summary>
        [HttpPost]
        [Authorize]
        [Route("/api/v1/branch/list")]
        public async Task< IActionResult> List([FromBody]FilterBranchModel filter)
        {
            var res = _branchService.FillterModel(filter);
            return Json(new ResultBase<PagedList<BranchModel>>() { success = true, data = new PagedList<BranchModel>() { list = res.Data, pager = filter.Paging } });
        }
        /// <summary>
        /// Save
        /// </summary>
        [HttpPost]
        [Authorize]
        [Route("/api/v1/branch/save")]
        public async Task<IActionResult> save([FromBody]BranchModel model)
        {
            if (!ModelState.IsValid)
                return Json(new ResultBase<PagedList<BranchModel>>() { success = false, message = ModelState.GetErrorsMessage() });
            var res = _branchService.Save(model);
           
            return Json(new ResultBase<BranchModel>() { success = res.Result, message = res.Error });

        }
        /// <summary>
        /// Delete
        /// </summary>
        [HttpDelete]
        [Route("/api/v1/branch/delete")]
        [Authorize]
        public async Task<IActionResult> Delete(int id)
        {
            var res = _branchService.Delete(id);
            return Json(new ResultBase<bool>() {success = res.Result, data = res.Data, message= res.Error });
        }
        /// <summary>
        /// Get by Id
        /// </summary>
        [HttpGet("/api/v1/branch/get_by_id")]
        [Authorize]
        public async Task<IActionResult> GetById(int id)
        {
            var res = _branchService.GetById(id);
           return Json(new ResultBase<BranchModel>() {success = res.Result, data = res.Data, message= res.Error });
        }
        /// <summary>
        /// get branch by current user
        /// </summary>
        [HttpGet]
        [Authorize]
        [Route("/api/v1/branch/list_by_current_user")]
        public async Task<IActionResult> GetBranchByCurrentUser()
        {
            var userId = HttpContext.User.Identity.GetUserId();
            var res = _branchService.GetBranchByCurrentUser(userId);
            return Json(new ResultBase<PagedList<BranchModel>>() { success = true, data = new PagedList<BranchModel>() { list = res.Data, pager = new PagingInfo() } });
        }

        /// <summary>
        /// get all branch active
        /// </summary>
        [HttpGet]
        [Route("/api/v1/branch/list_all_branch")]
        public async Task<IActionResult> GetAllBranch()
        {
            var res = _branchService.GetAllBranch();
            return Json(new ResultBase<PagedList<BranchModel>>() { success = true, data = new PagedList<BranchModel>() { list = res.Data, pager = new PagingInfo() } });
        }
    }
}