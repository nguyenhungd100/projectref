﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Swashbuckle.AspNetCore.Annotations;
using TQQ.POMS.Domain.Services.DistrictFacade;
using TQQ.POMS.Domain.Services.RolesFacade;
using TQQ.POMS.Domain.Services.UserFacade;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace TQQ.POMS.Api.Modules.MasterDatas
{
    public class DistrictController : Controller
    {
        private readonly IDistrictService _districtService;

        public DistrictController (IDistrictService districtService)
        {
            _districtService = districtService;
        }

        /// <summary>
        /// Get list district
        /// </summary>
        /// <returns></returns>
        [HttpGet("/api/v1/district/get_list")]
        //[Authorize(Policy = nameof(PermissionCode.MANAGE_USERS))]
        [SwaggerResponse((int)HttpStatusCode.OK, Type = typeof(ResultBase<PagedList<DistrictModel>>))]
        [SwaggerResponse((int)System.Net.HttpStatusCode.OK, Type = typeof(ResultBase<>))]
        [SwaggerResponse((int)System.Net.HttpStatusCode.Unauthorized, Type = typeof(ResultBase<>))]
        [SwaggerResponse((int)System.Net.HttpStatusCode.InternalServerError, Type = typeof(ResultBase<>))]
        [SwaggerResponse((int)System.Net.HttpStatusCode.BadRequest, Type = typeof(ResultBase<>))]
        [SwaggerResponse((int)System.Net.HttpStatusCode.Forbidden, Type = typeof(ResultBase<>))]
        public async Task<IActionResult> GetList()
        {
            var result = await _districtService.GetAllDistrict();

            return Json(new ResultBase<PagedList<DistrictModel>>() { success = true, data = new PagedList<DistrictModel>() { list = result} });
        }
    }
}
