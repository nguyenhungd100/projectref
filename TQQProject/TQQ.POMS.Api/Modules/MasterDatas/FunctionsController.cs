using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TQQ.POMS.Api.Helpers;

using TQQ.POMS.Framework.Data;
using Microsoft.AspNetCore.Mvc;
using TQQ.POMS.Domain.Services;
using TQQ.POMS.Domain.Services.FunctionsFacade;

namespace TQQ.POMS.Api.Modules.Functionss
{
    public class FunctionsController : Controller
    {
        private readonly IFunctionsService _functionsService;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="functionsService"></param>
        public FunctionsController(IFunctionsService functionsService)
        {
            _functionsService = functionsService;
        }
        /// <summary>
        /// Get by Id
        /// </summary>
        [HttpGet("/api/v1/functions/get_by_code")]
        public async Task<IActionResult> GetByCode(string code)
        {
            var res = _functionsService.GetByCode(code);
           return Json(new ResultBase<FunctionsModel>() {success = res.Result, data = res.Data, message= res.Error });
        }
        /// <summary>
        /// Get all function
        /// </summary>
        [HttpGet("/api/v1/functions/get_all")]
        public async Task<IActionResult> GetAll()
        {
            var res = await _functionsService.GetAll();
            return Json(new ResultBase<PagedList<FunctionsModel>>() { success = true, data = new PagedList<FunctionsModel>() { list = res } });
        }

    }
}