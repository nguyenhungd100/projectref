using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TQQ.POMS.Api.Helpers;

using TQQ.POMS.Framework.Data;
using Microsoft.AspNetCore.Mvc;
using TQQ.POMS.Domain.Services;
using TQQ.POMS.Domain.Services.SurchargeFacade;
using Microsoft.AspNetCore.Authorization;

namespace TQQ.POMS.Api.Modules.Surcharges
{
    public class SurchargesController : Controller
    {
        private readonly ISurchargeService _surchargeservice;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="Surchargeservice"></param>
        public SurchargesController(ISurchargeService surchargeservice)
        {
            _surchargeservice = surchargeservice;
        }
        /// <summary>
        /// get list
        /// </summary>
        [HttpPost]
        [Route("/api/v1/surcharges/list")]
        [Authorize]
        public async Task< IActionResult> List([FromBody]FilterSurchargeModel filter)
        {
            var res = _surchargeservice.FillterModel(filter);
            return Json(new ResultBase<PagedList<SurchargeModel>>() { success = true, data = new PagedList<SurchargeModel>() { list = res.Data, pager = filter.Paging } });
        }
        /// <summary>
        /// Save
        /// </summary>
        [HttpPost]
        [Authorize]
        [Route("/api/v1/surcharges/save")]
        public async Task<IActionResult> save([FromBody]SurchargeModel model)
        {
            if (!ModelState.IsValid)
                return Json(new ResultBase<PagedList<SurchargeModel>>() { success = false, message = ModelState.GetErrorsMessage() });
            var res = _surchargeservice.Save(model);
           
            return Json(new ResultBase<SurchargeModel>() { success = res.Result, message = res.Error });

        }
        /// <summary>
        /// Delete
        /// </summary>
        [HttpDelete]
        [Authorize]
        [Route("/api/v1/surcharges/delete")]
        public async Task<IActionResult> Delete(int id)
        {
            var res = _surchargeservice.Delete(id);
            return Json(new ResultBase<bool>() {success = res.Result, data = res.Data, message= res.Error });
        }
        /// <summary>
        /// Get by Id
        /// </summary>
        [HttpGet("/api/v1/surcharges/get_by_id")]
        [Authorize]
        public async Task<IActionResult> GetById(int id)
        {
            var res = _surchargeservice.GetById(id);
           return Json(new ResultBase<SurchargeModel>() {success = res.Result, data = res.Data, message= res.Error });
        }


        [HttpPost("/api/v1/surcharges/active_inactive")]
        [Authorize]
        public async Task<IActionResult> ActiveInactiveSurcharge(int surchargeId)
        {
            if (!ModelState.IsValid) return Json(new ResultBase<List<SurchargeModel>>() { success = false, message = ModelState.GetErrorsMessage() });

            var result = await _surchargeservice.ActiveInactiveSurcharge(surchargeId);

            return Json(new ResultBase<bool>() { success = result.Result, message = result.Error });
        }
    }
}