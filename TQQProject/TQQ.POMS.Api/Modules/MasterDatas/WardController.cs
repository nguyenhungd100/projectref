﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Swashbuckle.AspNetCore.Annotations;
using TQQ.POMS.Domain.Services.WardFacade;
using TQQ.POMS.Domain.Services.RolesFacade;
using TQQ.POMS.Domain.Services.UserFacade;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace TQQ.POMS.Api.Modules.MasterDatas
{
    public class WardController : Controller
    {
        private readonly IWardService _wardService;

        public WardController (IWardService wardService)
        {
            _wardService = wardService;
        }

        /// <summary>
        /// Get list Ward by districtId
        /// </summary>
        /// <returns></returns>
        [HttpGet("/api/v1/ward/get_list_ward_by_districtId")]
        //[Authorize(Policy = nameof(PermissionCode.MANAGE_USERS))]
        [SwaggerResponse((int)HttpStatusCode.OK, Type = typeof(ResultBase<PagedList<WardModel>>))]
        [SwaggerResponse((int)System.Net.HttpStatusCode.OK, Type = typeof(ResultBase<>))]
        [SwaggerResponse((int)System.Net.HttpStatusCode.Unauthorized, Type = typeof(ResultBase<>))]
        [SwaggerResponse((int)System.Net.HttpStatusCode.InternalServerError, Type = typeof(ResultBase<>))]
        [SwaggerResponse((int)System.Net.HttpStatusCode.BadRequest, Type = typeof(ResultBase<>))]
        [SwaggerResponse((int)System.Net.HttpStatusCode.Forbidden, Type = typeof(ResultBase<>))]
        public async Task<IActionResult> GetWardByDistrictId(int districtId)
        {
            var result = await _wardService.GetWardByDistrictId(districtId);

            return Json(new ResultBase<PagedList<WardModel>>() { success = true, data = new PagedList<WardModel>() { list = result} });
        }
    }
}
