using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TQQ.POMS.Api.Helpers;

using TQQ.POMS.Framework.Data;
using Microsoft.AspNetCore.Mvc;
using TQQ.POMS.Domain.Services;
using TQQ.POMS.Domain.Services.ActionHistoryFacade;

namespace TQQ.POMS.Api.Modules.ActionHistorys
{
    public class ActionHistoryController : Controller
    {
        private readonly IActionHistoryService _actionHistoryService;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="actionHistoryService"></param>
        public ActionHistoryController(IActionHistoryService actionHistoryService)
        {
            _actionHistoryService = actionHistoryService;
        }
        /// <summary>
        /// get list
        /// </summary>
        [HttpPost]
        [Route("/api/v1/action_history/list")]
        public async Task< IActionResult> List([FromBody]FilterActionHistoryModel filter)
        {
            var res = _actionHistoryService.FillterModel(filter);
            return Json(new ResultBase<PagedList<ActionHistoryModel>>() { success = true, data = new PagedList<ActionHistoryModel>() { list = res.Data, pager = filter.Paging } });
        }
        /// <summary>
        /// Save
        /// </summary>
        [HttpPost]
        [Route("/api/v1/action_history/save")]
        public async Task<IActionResult> save([FromBody]ActionHistoryModel model)
        {
            if (!ModelState.IsValid)
                return Json(new ResultBase<PagedList<ActionHistoryModel>>() { success = false, message = ModelState.GetErrorsMessage() });
            var res = _actionHistoryService.Save(model);
           
            return Json(new ResultBase<ActionHistoryModel>() { success = res.Result, message = res.Error });

        }
        /// <summary>
        /// Delete
        /// </summary>
        [HttpDelete]
        [Route("/api/v1/action_history/delete")]
        public async Task<IActionResult> Delete(int id)
        {
            var res = _actionHistoryService.Delete(id);
            return Json(new ResultBase<bool>() {success = res.Result, data = res.Data, message= res.Error });
        }
        /// <summary>
        /// Get by Id
        /// </summary>
        [HttpGet("/api/v1/action_history/get_by_id")]
        public async Task<IActionResult> GetById(int id)
        {
            var res = _actionHistoryService.GetById(id);
           return Json(new ResultBase<ActionHistoryModel>() {success = res.Result, data = res.Data, message= res.Error });
        }
    }
}