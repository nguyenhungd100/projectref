using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TQQ.POMS.Api.Helpers;

using TQQ.POMS.Framework.Data;
using Microsoft.AspNetCore.Mvc;
using TQQ.POMS.Domain.Services;
using TQQ.POMS.Domain.Services.ActionsFacade;

namespace TQQ.POMS.Api.Modules.Actionss
{
    public class ActionsController : Controller
    {
        private readonly IActionsService _actionsService;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="actionsService"></param>
        public ActionsController(IActionsService actionsService)
        {
            _actionsService = actionsService;
        }
        /// <summary>
        /// Get by Id
        /// </summary>
        [HttpGet("/api/v1/actions/get_by_code")]
        public async Task<IActionResult> GetByCode(string code)
        {
            var res = _actionsService.GetByCode(code);
           return Json(new ResultBase<ActionsModel>() {success = res.Result, data = res.Data, message= res.Error });
        }
        /// <summary>
        /// Get all action
        /// </summary>
        [HttpGet("/api/v1/actions/get_all")]
        public async Task<IActionResult> GetAll()
        {
            var res = await _actionsService.GetAll();
            return Json(new ResultBase<PagedList<ActionsModel>>() { success = true, data = new PagedList<ActionsModel>() { list = res } });
        }
    }
}