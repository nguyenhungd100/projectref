using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TQQ.POMS.Api.Helpers;

using TQQ.POMS.Framework.Data;
using Microsoft.AspNetCore.Mvc;
using TQQ.POMS.Domain.Services;
using TQQ.POMS.Domain.Services.ExpensesOtherFacade;
using Microsoft.AspNetCore.Authorization;

namespace TQQ.POMS.Api.Modules.ExpensesOthers
{
    public class ExpensesOtherController : Controller
    {
        private readonly IExpensesOtherService _expensesOtherService;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="ExpensesOtherService"></param>
        public ExpensesOtherController(IExpensesOtherService expensesOtherService)
        {
            _expensesOtherService = expensesOtherService;
        }
        /// <summary>
        /// get list
        /// </summary>
        [HttpPost]
        [Route("/api/v1/expenses_other/list")]
        [Authorize]
        public async Task< IActionResult> List([FromBody]FilterExpensesOtherModel filter)
        {
            var res = _expensesOtherService.FillterModel(filter);
            return Json(new ResultBase<PagedList<ExpensesOtherModel>>() { success = true, data = new PagedList<ExpensesOtherModel>() { list = res.Data, pager = filter.Paging } });
        }
        /// <summary>
        /// Save
        /// </summary>
        [HttpPost]
        [Route("/api/v1/expenses_other/save")]
        [Authorize]
        public async Task<IActionResult> save([FromBody]ExpensesOtherModel model)
        {
            if (!ModelState.IsValid)
                return Json(new ResultBase<PagedList<ExpensesOtherModel>>() { success = false, message = ModelState.GetErrorsMessage() });
            var res = _expensesOtherService.Save(model);
           
            return Json(new ResultBase<ExpensesOtherModel>() { success = res.Result, message = res.Error });

        }
        /// <summary>
        /// Delete
        /// </summary>
        [HttpDelete]
        [Route("/api/v1/expenses_other/delete")]
        [Authorize]
        public async Task<IActionResult> Delete(int id)
        {
            var res = _expensesOtherService.Delete(id);
            return Json(new ResultBase<bool>() {success = res.Result, data = res.Data, message= res.Error });
        }
        /// <summary>
        /// Get by Id
        /// </summary>
        [HttpGet("/api/v1/expenses_other/get_by_id")]
        [Authorize]
        public async Task<IActionResult> GetById(int id)
        {
            var res = _expensesOtherService.GetById(id);
           return Json(new ResultBase<ExpensesOtherModel>() {success = res.Result, data = res.Data, message= res.Error });
        }

        [HttpPost("/api/v1/expenses_other/active_inactive")]
        [Authorize]
        public async Task<IActionResult> ActiveInactiveExpenseOther(int expenseOtherId)
        {
            if (!ModelState.IsValid) return Json(new ResultBase<List<ExpensesOtherModel>>() { success = false, message = ModelState.GetErrorsMessage() });

            var result = await _expensesOtherService.ActiveInactiveExpenseOther(expenseOtherId);

            return Json(new ResultBase<bool>() { success = result.Result, message = result.Error });
        }
    }
}