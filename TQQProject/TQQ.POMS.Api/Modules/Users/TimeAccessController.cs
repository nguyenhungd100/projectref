using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TQQ.POMS.Api.Helpers;

using TQQ.POMS.Framework.Data;
using Microsoft.AspNetCore.Mvc;
using TQQ.POMS.Domain.Services;
using TQQ.POMS.Domain.Services.TimeAccessFacade;

namespace TQQ.POMS.Api.Modules.TimeAccesss
{
    public class TimeAccessController : Controller
    {
        private readonly ITimeAccessService _TimeAccessService;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="TimeAccessService"></param>
        public TimeAccessController(ITimeAccessService TimeAccessService)
        {
            _TimeAccessService = TimeAccessService;
        }
        /// <summary>
        /// get list
        /// </summary>
        [HttpPost]
        [Route("/api/v1/time_access/list")]
        public async Task< IActionResult> List([FromBody]FilterTimeAccessModel filter)
        {
            var res = _TimeAccessService.FillterModel(filter);
            return Json(new ResultBase<PagedList<TimeAccessModel>>() { success = true, data = new PagedList<TimeAccessModel>() { list = res.Data, pager = filter.Paging } });
        }
        /// <summary>
        /// Save
        /// </summary>
        [HttpPost]
        [Route("/api/v1/time_access/save")]
        public async Task<IActionResult> save([FromBody]List<TimeAccessModel> model)
        {
            if (!ModelState.IsValid)
                return Json(new ResultBase<PagedList<TimeAccessModel>>() { success = false, message = ModelState.GetErrorsMessage() });
            var res = _TimeAccessService.SaveAll(model);
           
            return Json(new ResultBase<List<TimeAccessModel>>() { success = res.Result, message = res.Error });

        }
        /// <summary>
        /// Delete
        /// </summary>
        [HttpDelete]
        [Route("/api/v1/time_access/delete")]
        public async Task<IActionResult> Delete(int id)
        {
            var res = _TimeAccessService.Delete(id);
            return Json(new ResultBase<bool>() {success = res.Result, data = res.Data, message= res.Error });
        }
        /// <summary>
        /// Get by Id
        /// </summary>
        [HttpGet("/api/v1/time_access/get_by_id")]
        public async Task<IActionResult> GetById(int id)
        {
            var res = _TimeAccessService.GetById(id);
           return Json(new ResultBase<TimeAccessModel>() {success = res.Result, data = res.Data, message= res.Error });
        }

        /// <summary>
        /// Get by Id
        /// </summary>
        [HttpGet("/api/v1/time_access/get_by_user_id")]
        public async Task<IActionResult> GetListByUserId(int userId)
        {
            var res = _TimeAccessService.GetListByUserId(userId);
            return Json(new ResultBase<List<TimeAccessModel>>() { success = res.Result, data = res.Data, message = res.Error });
        }
    }
}