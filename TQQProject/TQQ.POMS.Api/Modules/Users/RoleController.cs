﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Swashbuckle.AspNetCore.Annotations;

using TQQ.POMS.Domain.Services.UserFacade;
using TQQ.POMS.Domain.Services.RolesFacade;
using TQQ.POMS.Domain.Services.RolesFacade.Implementation;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace TQQ.POMS.Api.Modules.Users
{
    public class RoleController : Controller
    {
        private readonly IRolesService _rolesService;

        public RoleController(IRolesService rolesService)
        {
            _rolesService = rolesService;
        }

        /// <summary>
        /// Filter roles
        /// </summary>
        /// <returns></returns>
        [HttpPost("/api/v1/roles/list")]
        //[Authorize(Policy = nameof(PermissionCode.MANAGE_ROLES))]
        [SwaggerResponse((int)System.Net.HttpStatusCode.OK, Type = typeof(ResultBase<>))]
        [SwaggerResponse((int)System.Net.HttpStatusCode.Unauthorized, Type = typeof(ResultBase<>))]
        [SwaggerResponse((int)System.Net.HttpStatusCode.InternalServerError, Type = typeof(ResultBase<>))]
        [SwaggerResponse((int)System.Net.HttpStatusCode.BadRequest, Type = typeof(ResultBase<>))]
        [SwaggerResponse((int)System.Net.HttpStatusCode.Forbidden, Type = typeof(ResultBase<>))]
        [SwaggerResponse((int)System.Net.HttpStatusCode.NotFound)]
        [SwaggerResponse((int)System.Net.HttpStatusCode.OK, Type = typeof(ResultBase<List<RolesModel>>))]
        public async Task<IActionResult> FilterRoles([FromBody]TQQ.POMS.Domain.Services.RolesFacade.RoleFilterModel filter)
        {
            var res = await _rolesService.List(filter);

            return Json(new ResultBase<List<RolesModel>>() { success = true, data = res });
        }

        /// <summary>
        /// Get role by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        //[Authorize(Policy = nameof(PermissionCode.MANAGE_ROLES))]
        [Route("/api/v1/roles/get_role")]
        [HttpGet]
        [SwaggerResponse((int)System.Net.HttpStatusCode.OK, Type = typeof(ResultBase<List<RolesModel>>))]
        [SwaggerResponse((int)System.Net.HttpStatusCode.OK, Type = typeof(ResultBase<RolesModel>))]
        [SwaggerResponse((int)System.Net.HttpStatusCode.OK, Type = typeof(ResultBase<>))]
        [SwaggerResponse((int)System.Net.HttpStatusCode.Unauthorized, Type = typeof(ResultBase<>))]
        [SwaggerResponse((int)System.Net.HttpStatusCode.InternalServerError, Type = typeof(ResultBase<>))]
        [SwaggerResponse((int)System.Net.HttpStatusCode.BadRequest, Type = typeof(ResultBase<>))]
        [SwaggerResponse((int)System.Net.HttpStatusCode.Forbidden, Type = typeof(ResultBase<>))]
        [SwaggerResponse((int)System.Net.HttpStatusCode.NotFound)]
        public async Task<IActionResult> GetRole(int id)
        {
            var res = await _rolesService.GetById(id);

            return Json(new ResultBase<RolesModel>() { success = true, data = res });
        }

        /// <summary>
        /// Update roles
        /// </summary>
        /// <param name="role"></param>
        /// <returns></returns>
        //[Authorize(Policy = nameof(PermissionCode.MANAGE_ROLES))]
        [Route("/api/v1/roles/update")]
        [HttpPut]
        [SwaggerResponse((int)System.Net.HttpStatusCode.OK, Type = typeof(ResultBase<RolesModel>))]
        [SwaggerResponse((int)System.Net.HttpStatusCode.OK, Type = typeof(ResultBase<>))]
        [SwaggerResponse((int)System.Net.HttpStatusCode.Unauthorized, Type = typeof(ResultBase<>))]
        [SwaggerResponse((int)System.Net.HttpStatusCode.InternalServerError, Type = typeof(ResultBase<>))]
        [SwaggerResponse((int)System.Net.HttpStatusCode.BadRequest, Type = typeof(ResultBase<>))]
        [SwaggerResponse((int)System.Net.HttpStatusCode.Forbidden, Type = typeof(ResultBase<>))]
        [SwaggerResponse((int)System.Net.HttpStatusCode.NotFound)]
        [SwaggerResponse((int)System.Net.HttpStatusCode.OK, Type = typeof(ResultBase<bool>))]
        public async Task<ActionResult> UpdateAsync([FromBody]RolesModel role)
        {
            var res = await _rolesService.Update(role);

            return Json(new ResultBase<RolesModel>() { success = res.Result, message = res.Error });
        }

        /// <summary>
        /// Get permissions
        /// </summary>
        /// <returns></returns>
        //[Authorize(Policy = nameof(PermissionCode.MANAGE_ROLES))]
        [Route("/api/v1/roles/list_permissions")]
        [HttpGet]
        [SwaggerResponse((int)System.Net.HttpStatusCode.OK, Type = typeof(ResultBase<RolesModel>))]
        [SwaggerResponse((int)System.Net.HttpStatusCode.OK, Type = typeof(ResultBase<>))]
        [SwaggerResponse((int)System.Net.HttpStatusCode.Unauthorized, Type = typeof(ResultBase<>))]
        [SwaggerResponse((int)System.Net.HttpStatusCode.InternalServerError, Type = typeof(ResultBase<>))]
        [SwaggerResponse((int)System.Net.HttpStatusCode.BadRequest, Type = typeof(ResultBase<>))]
        [SwaggerResponse((int)System.Net.HttpStatusCode.Forbidden, Type = typeof(ResultBase<>))]
        [SwaggerResponse((int)System.Net.HttpStatusCode.NotFound)]
        [SwaggerResponse((int)System.Net.HttpStatusCode.OK, Type = typeof(ResultBase<List<PermissionGroup>>))]
        public ActionResult ListPermissions()
        {
            var res = RolesService.ListPermissions();

            return Json(new ResultBase<List<PermissionGroup>>() { success = true, data = res });
        }

        /// <summary>
        /// Get all role
        /// </summary>
        /// <returns></returns>
        //[Authorize(Policy = nameof(PermissionCode.MANAGE_ROLES))]
        [Route("/api/v1/roles/list_all")]
        [HttpGet]
        [SwaggerResponse((int)System.Net.HttpStatusCode.OK, Type = typeof(ResultBase<RolesModel>))]
        [SwaggerResponse((int)System.Net.HttpStatusCode.OK, Type = typeof(ResultBase<>))]
        [SwaggerResponse((int)System.Net.HttpStatusCode.Unauthorized, Type = typeof(ResultBase<>))]
        [SwaggerResponse((int)System.Net.HttpStatusCode.InternalServerError, Type = typeof(ResultBase<>))]
        [SwaggerResponse((int)System.Net.HttpStatusCode.BadRequest, Type = typeof(ResultBase<>))]
        [SwaggerResponse((int)System.Net.HttpStatusCode.Forbidden, Type = typeof(ResultBase<>))]
        [SwaggerResponse((int)System.Net.HttpStatusCode.NotFound)]
        [SwaggerResponse((int)System.Net.HttpStatusCode.OK, Type = typeof(ResultBase<List<RolesModel>>))]
        public async Task<IActionResult> ListAll()
        {
            var res = await _rolesService.GetAll();

            return Json(new ResultBase<List<RolesModel>>() { success = true, data = res });
        }
    }
}
