﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Swashbuckle.AspNetCore.Annotations;

using TQQ.POMS.Domain.Services.UserFacade;
using TQQ.POMS.Domain.Services.RolesFacade;
using TQQ.POMS.Domain.Services.RolesFacade.Implementation;
using TQQ.POMS.Domain.Services.UserRoleFacade;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace TQQ.POMS.Api.Modules.Users
{
    public class UserRoleController : Controller
    {
        private readonly IUserRoleService _userRoleService;

        public UserRoleController(IUserRoleService userRoleService)
        {
            _userRoleService = userRoleService;
        }
        

        /// <summary>
        /// Get user role by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        //[Authorize(Policy = nameof(PermissionCode.MANAGE_ROLES))]
        [Route("/api/v1/user_roles/get_role")]
        [HttpGet]
        [SwaggerResponse((int)System.Net.HttpStatusCode.OK, Type = typeof(ResultBase<List<UserRoleModel>>))]
        [SwaggerResponse((int)System.Net.HttpStatusCode.OK, Type = typeof(ResultBase<UserRoleModel>))]
        [SwaggerResponse((int)System.Net.HttpStatusCode.OK, Type = typeof(ResultBase<>))]
        [SwaggerResponse((int)System.Net.HttpStatusCode.Unauthorized, Type = typeof(ResultBase<>))]
        [SwaggerResponse((int)System.Net.HttpStatusCode.InternalServerError, Type = typeof(ResultBase<>))]
        [SwaggerResponse((int)System.Net.HttpStatusCode.BadRequest, Type = typeof(ResultBase<>))]
        [SwaggerResponse((int)System.Net.HttpStatusCode.Forbidden, Type = typeof(ResultBase<>))]
        [SwaggerResponse((int)System.Net.HttpStatusCode.NotFound)]
        public async Task<IActionResult> GetRole(int id)
        {
            var res = await _userRoleService.GetById(id);

            return Json(new ResultBase<UserRoleModel>() { success = true, data = res });
        }

        /// <summary>
        /// Update user roles
        /// </summary>
        /// <param name="userRole"></param>
        /// <returns></returns>
        //[Authorize(Policy = nameof(PermissionCode.MANAGE_ROLES))]
        [Route("/api/v1/user_roles/update")]
        [HttpPut]
        [SwaggerResponse((int)System.Net.HttpStatusCode.OK, Type = typeof(ResultBase<RolesModel>))]
        [SwaggerResponse((int)System.Net.HttpStatusCode.OK, Type = typeof(ResultBase<>))]
        [SwaggerResponse((int)System.Net.HttpStatusCode.Unauthorized, Type = typeof(ResultBase<>))]
        [SwaggerResponse((int)System.Net.HttpStatusCode.InternalServerError, Type = typeof(ResultBase<>))]
        [SwaggerResponse((int)System.Net.HttpStatusCode.BadRequest, Type = typeof(ResultBase<>))]
        [SwaggerResponse((int)System.Net.HttpStatusCode.Forbidden, Type = typeof(ResultBase<>))]
        [SwaggerResponse((int)System.Net.HttpStatusCode.NotFound)]
        [SwaggerResponse((int)System.Net.HttpStatusCode.OK, Type = typeof(ResultBase<bool>))]
        public async Task<ActionResult> UpdateAsync([FromBody]UserRoleModel UserRole)
        {
            var res = await _userRoleService.Update(UserRole);

            return Json(new ResultBase<UserRoleModel>() { success = res.Result, message = res.Error });
        }

        /// <summary>
        /// Get user role by userId and branchId
        /// </summary>
        /// /// <param name="userId"></param>
        /// /// <param name="branchId"></param>
        /// <returns></returns>
        //[Authorize(Policy = nameof(PermissionCode.MANAGE_ROLES))]
        [Route("/api/v1/user_roles/get_user_role_by_userId_branchId")]
        [HttpGet]
        [SwaggerResponse((int)System.Net.HttpStatusCode.OK, Type = typeof(ResultBase<RolesModel>))]
        [SwaggerResponse((int)System.Net.HttpStatusCode.OK, Type = typeof(ResultBase<>))]
        [SwaggerResponse((int)System.Net.HttpStatusCode.Unauthorized, Type = typeof(ResultBase<>))]
        [SwaggerResponse((int)System.Net.HttpStatusCode.InternalServerError, Type = typeof(ResultBase<>))]
        [SwaggerResponse((int)System.Net.HttpStatusCode.BadRequest, Type = typeof(ResultBase<>))]
        [SwaggerResponse((int)System.Net.HttpStatusCode.Forbidden, Type = typeof(ResultBase<>))]
        [SwaggerResponse((int)System.Net.HttpStatusCode.NotFound)]
        public async Task<ActionResult> GetUserRoleByUserIdBranchId(int userId, int branchId)
        {
            var res = await _userRoleService.GetByUserIdAndBranchId(userId, branchId);
            return Json(new ResultBase<UserRoleModel>() { success = true, data = res });
        }

        /// <summary>
        /// Delete user role
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        //[Authorize(Policy = nameof(PermissionCode.MANAGE_USERS))]
        [HttpDelete("/api/v1/user_roles/delete")]
        [SwaggerResponse((int)System.Net.HttpStatusCode.OK, Type = typeof(ResultBase<bool>))]
        [SwaggerResponse((int)System.Net.HttpStatusCode.OK, Type = typeof(ResultBase<UserRoleModel>))]
        [SwaggerResponse((int)System.Net.HttpStatusCode.OK, Type = typeof(ResultBase<>))]
        [SwaggerResponse((int)System.Net.HttpStatusCode.Unauthorized, Type = typeof(ResultBase<>))]
        [SwaggerResponse((int)System.Net.HttpStatusCode.InternalServerError, Type = typeof(ResultBase<>))]
        [SwaggerResponse((int)System.Net.HttpStatusCode.BadRequest, Type = typeof(ResultBase<>))]
        [SwaggerResponse((int)System.Net.HttpStatusCode.Forbidden, Type = typeof(ResultBase<>))]
        public async Task<IActionResult> RemoveUserRole(int id)
        {
            
            var result = await _userRoleService.Delete(id);

            return Json(new ResultBase<bool>() { success = result.Result, message = result.Error });
        }
    }
}
