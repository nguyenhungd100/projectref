﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TQQ.POMS.Api.Helpers;
using TQQ.POMS.Domain.Services.DepartmentFacade;
using TQQ.POMS.Domain.Services.RolesFacade;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Swashbuckle.AspNetCore.Annotations;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace TQQ.POMS.Api.Modules.Departments
{
    public class DepartmentController : Controller
    {
        private readonly IDepartmentService _departmentService;

        public DepartmentController(IDepartmentService departmentService)
        {
            _departmentService = departmentService;
        }

        ///// <summary>
        ///// List departments
        ///// </summary>
        ///// <returns></returns>
        //[HttpPost("v1/department/list")]
        ////[Authorize(Policy = nameof(PermissionCode.VIEW_ACCOUNTS_CATEGORIES))]
        //[SwaggerResponse((int)System.Net.HttpStatusCode.Unauthorized, Type = typeof(ResultBase<>))]
        //[SwaggerResponse((int)System.Net.HttpStatusCode.InternalServerError, Type = typeof(ResultBase<>))]
        //[SwaggerResponse((int)System.Net.HttpStatusCode.BadRequest, Type = typeof(ResultBase<>))]
        //[SwaggerResponse((int)System.Net.HttpStatusCode.Forbidden, Type = typeof(ResultBase<>))]
        //[SwaggerResponse((int)System.Net.HttpStatusCode.NotFound)]
        //[SwaggerResponse((int)System.Net.HttpStatusCode.OK, Type = typeof(ResultBase<List<DepartmentModel>>))]
        //public async Task<IActionResult> List([FromBody]DepartmentFilterModel filter)
        //{
        //    if (!ModelState.IsValid)
        //        return Json(new ResultBase<bool>() { success = false, message = ModelState.GetErrorsMessage() });

        //    var res = await _departmentService.ListAsync(filter);

        //    return Json(new ResultBase<List<DepartmentModel>>() { success = true, data = res });
        //}

        ///// <summary>
        ///// Get detail department
        ///// </summary>
        ///// <param name="id"></param>
        ///// <returns></returns>
        ////[Authorize(Policy = nameof(PermissionCode.XEM_VAI_TRO))]
        //[Route("v1/department/get_by_id")]
        //[HttpGet]
        //[SwaggerResponse((int)System.Net.HttpStatusCode.OK, Type = typeof(ResultBase<DepartmentModel>))]
        //[SwaggerResponse((int)System.Net.HttpStatusCode.Unauthorized, Type = typeof(ResultBase<>))]
        //[SwaggerResponse((int)System.Net.HttpStatusCode.InternalServerError, Type = typeof(ResultBase<>))]
        //[SwaggerResponse((int)System.Net.HttpStatusCode.BadRequest, Type = typeof(ResultBase<>))]
        //[SwaggerResponse((int)System.Net.HttpStatusCode.Forbidden, Type = typeof(ResultBase<>))]
        //[SwaggerResponse((int)System.Net.HttpStatusCode.NotFound)]
        //public async Task<IActionResult> GetById(long id)
        //{
        //    var res = await _departmentService.GetByIdAsync(id);

        //    return Json(new ResultBase<DepartmentModel>() { success = true, data = res });
        //}

        ///// <summary>
        ///// Add department detail
        ///// </summary>
        ///// <param name="department"></param>
        ///// <returns></returns>
        ////[Authorize(Policy = nameof(PermissionCode.CAP_NHAT_VAI_TRO))]
        //[Route("v1/department/add")]
        //[HttpPost]
        //[SwaggerResponse((int)System.Net.HttpStatusCode.Unauthorized, Type = typeof(ResultBase<>))]
        //[SwaggerResponse((int)System.Net.HttpStatusCode.InternalServerError, Type = typeof(ResultBase<>))]
        //[SwaggerResponse((int)System.Net.HttpStatusCode.BadRequest, Type = typeof(ResultBase<>))]
        //[SwaggerResponse((int)System.Net.HttpStatusCode.Forbidden, Type = typeof(ResultBase<>))]
        //[SwaggerResponse((int)System.Net.HttpStatusCode.NotFound)]
        //[SwaggerResponse((int)System.Net.HttpStatusCode.OK, Type = typeof(ResultBase<bool>))]
        //public async Task<ActionResult> Add([FromBody]DepartmentModel department)
        //{
        //    if (!ModelState.IsValid)
        //        return Json(new ResultBase<bool>() { success = false, message = ModelState.GetErrorsMessage() });

        //    var res = await _departmentService.AddAsync(department);

        //    return Json(new ResultBase<RolesModel>() { success = res.Result, message = res.Error });
        //}

        ///// <summary>
        ///// Update department detail
        ///// </summary>
        ///// <param name="department"></param>
        ///// <returns></returns>
        ////[Authorize(Policy = nameof(PermissionCode.CAP_NHAT_VAI_TRO))]
        //[Route("v1/department/update")]
        //[HttpPut]
        //[SwaggerResponse((int)System.Net.HttpStatusCode.Unauthorized, Type = typeof(ResultBase<>))]
        //[SwaggerResponse((int)System.Net.HttpStatusCode.InternalServerError, Type = typeof(ResultBase<>))]
        //[SwaggerResponse((int)System.Net.HttpStatusCode.BadRequest, Type = typeof(ResultBase<>))]
        //[SwaggerResponse((int)System.Net.HttpStatusCode.Forbidden, Type = typeof(ResultBase<>))]
        //[SwaggerResponse((int)System.Net.HttpStatusCode.NotFound)]
        //[SwaggerResponse((int)System.Net.HttpStatusCode.OK, Type = typeof(ResultBase<bool>))]
        //public async Task<ActionResult> Update([FromBody]DepartmentModel department)
        //{
        //    if (!ModelState.IsValid)
        //        return Json(new ResultBase<bool>() { success = false, message = ModelState.GetErrorsMessage() });

        //    var res = await _departmentService.UpdateAsync(department);

        //    return Json(new ResultBase<RolesModel>() { success = res.Result, message = res.Error });
        //}

        ///// <summary>
        ///// Delete department by id
        ///// </summary>
        ///// <returns></returns>
        ////[Authorize(Policy = nameof(PermissionCode.XEM_VAI_TRO))]
        //[Route("v1/department/delete")]
        //[HttpDelete]
        //[SwaggerResponse((int)System.Net.HttpStatusCode.Unauthorized, Type = typeof(ResultBase<>))]
        //[SwaggerResponse((int)System.Net.HttpStatusCode.InternalServerError, Type = typeof(ResultBase<>))]
        //[SwaggerResponse((int)System.Net.HttpStatusCode.BadRequest, Type = typeof(ResultBase<>))]
        //[SwaggerResponse((int)System.Net.HttpStatusCode.Forbidden, Type = typeof(ResultBase<>))]
        //[SwaggerResponse((int)System.Net.HttpStatusCode.NotFound)]
        //[SwaggerResponse((int)System.Net.HttpStatusCode.OK, Type = typeof(bool))]
        //public async Task<ActionResult> Delete(long departmentId)
        //{
        //    var res = await _departmentService.DeleteAsync(new long[] { departmentId });

        //    return Json(new ResultBase<List<PermissionGroup>>() { success = res.Result, message = res.Error });
        //}


        ///// <summary>
        ///// get list
        ///// </summary>
        //[HttpPost]
        //[Route("v1/department/list_all")]
        ////[Authorize(Policy = nameof(PermissionCode.XEM_VAI_TRO))]
        //[SwaggerResponse((int)System.Net.HttpStatusCode.Unauthorized, Type = typeof(ResultBase<>))]
        //[SwaggerResponse((int)System.Net.HttpStatusCode.InternalServerError, Type = typeof(ResultBase<>))]
        //[SwaggerResponse((int)System.Net.HttpStatusCode.BadRequest, Type = typeof(ResultBase<>))]
        //[SwaggerResponse((int)System.Net.HttpStatusCode.Forbidden, Type = typeof(ResultBase<>))]
        //[SwaggerResponse((int)System.Net.HttpStatusCode.NotFound)]
        //[SwaggerResponse((int)System.Net.HttpStatusCode.OK, Type = typeof(ResultBase<List<DepartmentModel>>))]
        //public async Task<IActionResult> ListAll()
        //{
        //    var res = _departmentService.List();

        //    return Json(new ResultBase<PagedList<DepartmentModel>>() { success = true, data = new PagedList<DepartmentModel>() { list = res.Data, pager = null } });
        //}
    }
}
