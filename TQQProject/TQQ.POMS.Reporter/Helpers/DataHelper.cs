﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;

namespace TQQ.POMS.Reporting.Helpers
{
    public static class DataHelper
    {
        /// <summary>
        /// Clear null value and set default value
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public static List<T> ClearNullValue<T>(this List<T> data)
        {
            foreach (var item in data)
            {
                foreach (PropertyInfo property in typeof(T).GetProperties())
                {
                    Type myType = property.PropertyType;
                    var constructor = myType.GetConstructor(Type.EmptyTypes);
                    if (constructor != null)
                    {
                        // will initialize to a new copy of property type
                        property.SetValue(item, constructor.Invoke(null));
                        // or property.SetValue(model, Activator.CreateInstance(myType));
                    }
                    else
                    {
                        // will initialize to the default value of property type
                        object value;

                        if (myType.IsValueType)
                        {
                            value = Activator.CreateInstance(myType);
                        }
                        else
                        {
                            value = null;
                        }

                        property.SetValue(item, value);
                    }
                }//foreach
            }//foreach

            return data;
        }
    }
}