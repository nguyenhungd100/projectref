﻿using System.Web;
using System.Web.Mvc;

namespace TQQ.POMS.Reporter
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }
}
