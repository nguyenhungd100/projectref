﻿using CrystalDecisions.CrystalReports.Engine;
//using TQQ.POMS.Domain.Data.Entity;
//using TQQ.POMS.Domain.Services.AccountFacade;
//using TQQ.POMS.Domain.Services.AccountFacade.Implementation;
//using TQQ.POMS.Domain.Services.AssetFacade;
//using TQQ.POMS.Domain.Services.AssetFacade.Implementation;
//using TQQ.POMS.Domain.Services.ConfigurationFacade;
//using TQQ.POMS.Domain.Services.ConfigurationFacade.Implementation;
//using TQQ.POMS.Domain.Services.InputIncurredVoucherFacade;
//using TQQ.POMS.Domain.Services.InputIncurredVoucherFacade.Implementation;
using TQQ.POMS.Reporting.Helpers;
using Microsoft.AspNetCore.Cors;
using TQQ.POMS.Reporter.Helpers;
using TQQ.POMS.Reporter.Models;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace TQQ.POMS.Reporting.Controllers
{
    [AllowCrossSite]
    public class ReportController : Controller
    {
        //private readonly IConfigurationService _configService = new ConfigurationService();
        //private readonly IAccountService _accountService = new AccountService();
        //private readonly IInputIncurredVoucherService _incurredVoucherService = new InputIncurredVoucherService();
        //private readonly IAssetService _assetService = new AssetService();

        public ConfigurationModel Configuration { get; }

        public ReportController()
        {
            Configuration = _configService.GetConfig();
        }

        /// <summary>
        /// List balance sheet accounts
        /// </summary>
        /// <returns></returns>
        public async Task<ActionResult> ReportBalanceSheet(string filter)
        {
            //convert string to filter
            var base64EncodedBytes = System.Convert.FromBase64String(filter);
            var unHashFilter = System.Text.Encoding.UTF8.GetString(base64EncodedBytes);
            //var format = "dd/MM/yyyy"; // your datetime format
            var dateTimeConverter = new IsoDateTimeConverter { Culture = new System.Globalization.CultureInfo("vi-VN") };
            var filterModel = JsonConvert.DeserializeObject<AccountBalanceFilterModel>(unHashFilter, dateTimeConverter);

            //handle export
            Account account = null;
            var res = _accountService.ListAccountBalanceSheet(filterModel, out account);

            var rd = new ReportDocument();
            rd.Load(Path.Combine(Server.MapPath("~/Reports"), "BalanceSheetReport.rpt"));

            TextObject companyName = (TextObject)rd.ReportDefinition.ReportObjects["CompanyName"];
            TextObject address = (TextObject)rd.ReportDefinition.ReportObjects["Address"];
            TextObject taxCode = (TextObject)rd.ReportDefinition.ReportObjects["TaxCode"];
            TextObject reportTitle = (TextObject)rd.ReportDefinition.ReportObjects["ReportTitle3"];
            TextObject reportTitle2 = (TextObject)rd.ReportDefinition.ReportObjects["ReportTitle2"];

            companyName.Text = Configuration.CompanyName;
            address.Text = Configuration.Address;
            taxCode.Text = Configuration.TaxCode;

            if (filterModel.AccountId == null)
            {
                reportTitle.Text = "BẢNG CÂN ĐỐI KẾ TOÁN";
                reportTitle2.Text = "";
            }
            else
            {
                reportTitle.Text = "BẢNG TỔNG HỢP - CHI TIẾT TÀI KHOẢN";
                reportTitle2.Text = $"TÀI KHOẢN: {account.Code} - {account.Name.ToUpper()}";
            }

            rd.SetDataSource(res.Select(c => new
            {
                Code = c.Code,
                Name = c.Name,
                LoanAtStartYear = c.LoanAtStartYear.GetValueOrDefault(),
                BalanceAtStartYear = c.BalanceAtStartYear.GetValueOrDefault(),
                IncurredLoan = c.IncurredLoan.GetValueOrDefault(),
                IncurredBalance = c.IncurredBalance.GetValueOrDefault(),
                LoanAtEndYear = c.LoanAtEndYear.GetValueOrDefault(),
                BalanceAtEndYear = c.BalanceAtEndYear.GetValueOrDefault(),
            }));

            Response.Buffer = false;
            Response.ClearContent();
            Response.ClearHeaders();

            Stream stream = rd.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
            stream.Seek(0, SeekOrigin.Begin);

            return File(stream, "application/pdf", "BalanceSheetReport.pdf");
        }

        /// <summary>
        /// Report detail book
        /// </summary>
        /// <returns></returns>
        public async Task<ActionResult> ReportDetailBook(string filter)
        {
            //convert string to filter
            var base64EncodedBytes = System.Convert.FromBase64String(filter);
            var unHashFilter = System.Text.Encoding.UTF8.GetString(base64EncodedBytes);
            //var format = "dd/MM/yyyy"; // your datetime format
            var dateTimeConverter = new IsoDateTimeConverter { Culture = new System.Globalization.CultureInfo("vi-VN") };
            var filterModel = JsonConvert.DeserializeObject<DetailBookReportFilter>(unHashFilter, dateTimeConverter);

            ModelState.Clear();

            if (!TryValidateModel(filterModel))
                return Content(ModelState.GetErrorsMessage());

            var account = _accountService.GetById(filterModel.AccountId);

            if (account != null)
            {
                switch (account.AccountType)
                {
                    case Domain.AccountType.ExportImport:
                    case Domain.AccountType.Inventory:
                        {
                            //return await ReportInventoryDetail(filterModel);
                        }
                    case Domain.AccountType.Normal:
                    case Domain.AccountType.Customer:
                        {
                            //if (account.Code == "335")
                            //{
                            //    return await ReportLoanDetail(filterModel);
                            //}
                            //else
                            //{
                            //    return await ReportNormalAccount(filterModel);
                            //}
                        }
                }//switch

                return Content("Loại tài khoản cần xem không tồn tại");
            }
            else
            {
                return Content("Tài khoản cần xem không tồn tại");
            }
        }

        private async Task<ActionResult> ReportNormalAccount(DetailBookReportFilter filterModel)
        {
            //handle export
            decimal amount = 0;
            decimal lastOverBalance = 0;
            Account account = null;
            AccountDetail detail = null;
            //var res = _incurredVoucherService.ReportLoanDetail(filterModel, out amount, out account, out detail, out lastOverBalance);

            var rd = new ReportDocument();
            rd.Load(Path.Combine(Server.MapPath("~/Reports"), "LoanDetailReport.rpt"));

            TextObject companyName = (TextObject)rd.ReportDefinition.ReportObjects["CompanyName"];
            TextObject address = (TextObject)rd.ReportDefinition.ReportObjects["Address"];
            TextObject taxCode = (TextObject)rd.ReportDefinition.ReportObjects["TaxCode"];
            TextObject reportTime = (TextObject)rd.ReportDefinition.ReportObjects["ReportTime"];
            TextObject ReportTime2 = (TextObject)rd.ReportDefinition.ReportObjects["ReportTime2"];
            TextObject reportTitle = (TextObject)rd.ReportDefinition.ReportObjects["ReportTitle2"];
            TextObject amountTitle = (TextObject)rd.ReportDefinition.ReportObjects["Amount"];
            TextObject lastOverBalanceTitle = (TextObject)rd.ReportDefinition.ReportObjects["LastOverBalance"];
            TextObject totalTitle = (TextObject)rd.ReportDefinition.ReportObjects["TotalTitle"];

            companyName.Text = Configuration.CompanyName;
            address.Text = Configuration.Address;
            taxCode.Text = Configuration.TaxCode;
            reportTime.Text = $"Tháng {filterModel.StartMonth + "-" + filterModel.EndMonth} năm {filterModel.Year}";
            ReportTime2.Text = $"Tháng {filterModel.StartMonth + "-" + filterModel.EndMonth} năm {filterModel.Year}";
            totalTitle.Text = $"Cộng phát sinh tháng {filterModel.StartMonth + "-" + filterModel.EndMonth} năm {filterModel.Year}";
            reportTitle.Text = $"TÀI KHOẢN {account.Code + (detail != null ? $"-{account.Code}{detail.Code}: {detail.Name}" : ": " + account.Name)}";
            amountTitle.Text = $"{ amount.ToString("N2", new CultureInfo("vi")) }";
            lastOverBalanceTitle.Text = $"{ lastOverBalance.ToString("N2", new CultureInfo("vi")) }";

            rd.SetDataSource(res.Select(c => new
            {
                RecordDate = c.RecordDate,
                NumberOfBookVouchers = c.NumberOfBookVouchers,
                DayVoucher = c.DayVoucher,
                NumberDocument = c.NumberDocument,
                Description = c.Description,
                AccountCode = c.AccountCode,
                DetailCode = c.DetailCode,
                LoanIncurred = c.LoanIncurred,
                BalanceIncurred = c.BalanceIncurred,
                Amount = c.Amount
            }));

            Response.Buffer = false;
            Response.ClearContent();
            Response.ClearHeaders();

            Stream stream = rd.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
            stream.Seek(0, SeekOrigin.Begin);

            return File(stream, "application/pdf", "LoanDetailReport.pdf");
        }

        /// <summary>
        /// List loan detail
        /// </summary>
        /// <returns></returns>
        private async Task<ActionResult> ReportLoanDetail(DetailBookReportFilter filterModel)
        {
            ////convert string to filter
            //var base64EncodedBytes = System.Convert.FromBase64String(filter);
            //var unHashFilter = System.Text.Encoding.UTF8.GetString(base64EncodedBytes);
            ////var format = "dd/MM/yyyy"; // your datetime format
            //var dateTimeConverter = new IsoDateTimeConverter { Culture = new System.Globalization.CultureInfo("en-US") };
            //var filterModel = JsonConvert.DeserializeObject<DetailBookReportFilter>(unHashFilter, dateTimeConverter);

            //handle export
            decimal amount = 0;
            decimal lastOverBalance = 0;
            Account account = null;
            AccountDetail detail = null;
            //var res = _incurredVoucherService.ReportLoanDetail(filterModel, out amount, out account, out detail, out lastOverBalance);

            var rd = new ReportDocument();
            rd.Load(Path.Combine(Server.MapPath("~/Reports"), "LoanDetailReport.rpt"));

            TextObject companyName = (TextObject)rd.ReportDefinition.ReportObjects["CompanyName"];
            TextObject address = (TextObject)rd.ReportDefinition.ReportObjects["Address"];
            TextObject taxCode = (TextObject)rd.ReportDefinition.ReportObjects["TaxCode"];
            TextObject reportTime = (TextObject)rd.ReportDefinition.ReportObjects["ReportTime"];
            TextObject ReportTime2 = (TextObject)rd.ReportDefinition.ReportObjects["ReportTime2"];
            TextObject reportTitle = (TextObject)rd.ReportDefinition.ReportObjects["ReportTitle2"];
            TextObject amountTitle = (TextObject)rd.ReportDefinition.ReportObjects["Amount"];
            TextObject lastOverBalanceTitle = (TextObject)rd.ReportDefinition.ReportObjects["LastOverBalance"];
            TextObject totalTitle = (TextObject)rd.ReportDefinition.ReportObjects["TotalTitle"];

            companyName.Text = Configuration.CompanyName;
            address.Text = Configuration.Address;
            taxCode.Text = Configuration.TaxCode;
            reportTime.Text = $"Tháng {filterModel.StartMonth + " - " + filterModel.EndMonth} năm {filterModel.Year}";
            ReportTime2.Text = $"Tháng {filterModel.StartMonth + " - " + filterModel.EndMonth} năm {filterModel.Year}";
            totalTitle.Text = $"Cộng phát sinh tháng {filterModel.StartMonth + " - " + filterModel.EndMonth} năm {filterModel.Year}";
            reportTitle.Text = $"TÀI KHOẢN {account.Code + (detail != null ? $"-{account.Code}{detail.Code}: {detail.Name}" : ": " + account.Name)}";
            amountTitle.Text = $"{ amount.ToString("N2", new CultureInfo("vi")) }";
            lastOverBalanceTitle.Text = $"{ lastOverBalance.ToString("N2", new CultureInfo("vi")) }";

            rd.SetDataSource(res.Select(c => new
            {
                RecordDate = c.RecordDate,
                NumberOfBookVouchers = c.NumberOfBookVouchers,
                DayVoucher = c.DayVoucher,
                NumberDocument = c.NumberDocument,
                Description = c.Description,
                AccountCode = c.AccountCode,
                DetailCode = c.DetailCode,
                LoanIncurred = c.LoanIncurred,
                BalanceIncurred = c.BalanceIncurred,
                Amount = c.Amount
            }));

            Response.Buffer = false;
            Response.ClearContent();
            Response.ClearHeaders();

            Stream stream = rd.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
            stream.Seek(0, SeekOrigin.Begin);

            return File(stream, "application/pdf", "LoanDetailReport.pdf");
        }

        /// <summary>
        /// Report inventory detail
        /// </summary>
        /// <returns></returns>
        private async Task<ActionResult> ReportInventoryDetail(DetailBookReportFilter filterModel)
        {
            ////convert string to filter
            //var base64EncodedBytes = System.Convert.FromBase64String(filter);
            //var unHashFilter = System.Text.Encoding.UTF8.GetString(base64EncodedBytes);
            ////var format = "dd/MM/yyyy"; // your datetime format
            //var dateTimeConverter = new IsoDateTimeConverter { Culture = new System.Globalization.CultureInfo("en-US") };
            //var filterModel = JsonConvert.DeserializeObject<InventoryReportFilter>(unHashFilter, dateTimeConverter);

            //handle export
            AccountModel account = null;
            var res = _accountService.ReportInventoryDetail(filterModel, out account);

            var rd = new ReportDocument();
            rd.Load(Path.Combine(Server.MapPath("~/Reports"), "SummaryInventoryReport.rpt"));

            TextObject companyName = (TextObject)rd.ReportDefinition.ReportObjects["CompanyName"];
            TextObject address = (TextObject)rd.ReportDefinition.ReportObjects["Address"];
            TextObject taxCode = (TextObject)rd.ReportDefinition.ReportObjects["TaxCode"];
            TextObject reportTime = (TextObject)rd.ReportDefinition.ReportObjects["DatePrint"];
            //TextObject ReportTime2 = (TextObject)rd.ReportDefinition.ReportObjects["ReportTime2"];
            TextObject reportTitle = (TextObject)rd.ReportDefinition.ReportObjects["ReportTitle2"];

            companyName.Text = Configuration.CompanyName;
            address.Text = Configuration.Address;
            taxCode.Text = Configuration.TaxCode;
            reportTime.Text = $"Tháng {filterModel.StartMonth + " - " + filterModel.EndMonth} năm {filterModel.Year}";
            //ReportTime2.Text = $"Tháng {filterModel.Month} - {filterModel.Year}";
            reportTitle.Text = $"TÀI KHOẢN: {account.Code + " - " + account.Name}";

            rd.SetDataSource(res.Select(c => new
            {
                Code = c.Code,
                Name = c.Name,
                Unit = c.Unit,
                AmountStart = c.AmountStart,
                AmountEnd = c.AmountEnd,
                AmountImport = c.AmountImport,
                AmountExport = c.AmountExport,
                PriceStart = c.PriceStart,
                PriceEnd = c.PriceEnd,
                PriceExport = c.PriceExport,
                PriceImport = c.PriceImport,
                QuantityStart = c.QuantityStart,
                QuantityEnd = c.QuantityEnd,
                QuantityExport = c.QuantityExport,
                QuantityImport = c.QuantityImport
            }));

            Response.Buffer = false;
            Response.ClearContent();
            Response.ClearHeaders();

            Stream stream = rd.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
            stream.Seek(0, SeekOrigin.Begin);

            return File(stream, "application/pdf", "LoanDetailReport.pdf");
        }

        /// <summary>
        /// Report financial detail
        /// </summary>
        /// <returns></returns>
        public async Task<ActionResult> ReportFinancial(string filter)
        {
            //convert string to filter
            var base64EncodedBytes = System.Convert.FromBase64String(filter);
            var unHashFilter = System.Text.Encoding.UTF8.GetString(base64EncodedBytes);
            //var format = "dd/MM/yyyy"; // your datetime format
            var dateTimeConverter = new IsoDateTimeConverter { Culture = new System.Globalization.CultureInfo("en-US") };
            var filterModel = JsonConvert.DeserializeObject<FinancialReportFilter>(unHashFilter, dateTimeConverter);

            //handle export
            var res = _accountService.ReportFinancial(filterModel);

            var rd = new ReportDocument();
            rd.Load(Path.Combine(Server.MapPath("~/Reports"), "FinancialReport.rpt"));

            TextObject companyName = (TextObject)rd.ReportDefinition.ReportObjects["CompanyName"];
            TextObject address = (TextObject)rd.ReportDefinition.ReportObjects["Address"];
            //TextObject taxCode = (TextObject)rd.ReportDefinition.ReportObjects["TaxCode"];
            TextObject reportTime = (TextObject)rd.ReportDefinition.ReportObjects["DatePrint"];
            //TextObject reportTitle = (TextObject)rd.ReportDefinition.ReportObjects["ReportTitle2"];

            companyName.Text = Configuration.CompanyName;
            address.Text = Configuration.Address;
            //taxCode.Text = Configuration.TaxCode;
            reportTime.Text = $"Năm {filterModel.Year}";

            rd.SetDataSource(res.Select(c => new
            {
                Code = c.Code,
                Target = c.Target,
                Present = c.Present,
                PreviousYear = c.PreviousYear,
                ThisYear = c.ThisYear,
            }));

            Response.Buffer = false;
            Response.ClearContent();
            Response.ClearHeaders();

            Stream stream = rd.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
            stream.Seek(0, SeekOrigin.Begin);

            return File(stream, "application/pdf", "FinancialReport.pdf");
        }

        /// <summary>
        /// Report fixed assets
        /// </summary>
        /// <returns></returns>
        public async Task<ActionResult> ReportFixedAsset(string filter)
        {
            //convert string to filter
            var base64EncodedBytes = System.Convert.FromBase64String(filter);
            var unHashFilter = System.Text.Encoding.UTF8.GetString(base64EncodedBytes);
            //var format = "dd/MM/yyyy"; // your datetime format
            var dateTimeConverter = new IsoDateTimeConverter { Culture = new System.Globalization.CultureInfo("en-US") };
            var filterModel = JsonConvert.DeserializeObject<Domain.Services.AssetFacade.ReportFilterModel>(unHashFilter, dateTimeConverter);

            //handle export
            var res = _assetService.ReportDepreciation(filterModel);

            var rd = new ReportDocument();
            rd.Load(Path.Combine(Server.MapPath("~/Reports"), "FixedAssetDepreciationReport.rpt"));

            TextObject companyName = (TextObject)rd.ReportDefinition.ReportObjects["CompanyName"];
            TextObject address = (TextObject)rd.ReportDefinition.ReportObjects["Address"];
            TextObject taxCode = (TextObject)rd.ReportDefinition.ReportObjects["TaxCode"];
            TextObject reportTime = (TextObject)rd.ReportDefinition.ReportObjects["DatePrint"];
            //TextObject ReportTime2 = (TextObject)rd.ReportDefinition.ReportObjects["ReportTime2"];
            //TextObject reportTitle = (TextObject)rd.ReportDefinition.ReportObjects["ReportTitle2"];

            companyName.Text = Configuration.CompanyName;
            address.Text = Configuration.Address;
            taxCode.Text = Configuration.TaxCode;
            reportTime.Text = $"Tháng {filterModel.Month} năm {filterModel.Year}";
            //ReportTime2.Text = $"Tháng {filterModel.Month} - {filterModel.Year}";
            //reportTitle.Text = $"TÀI KHOẢN: {account.Code + " - " + account.Name}";

            rd.SetDataSource(res.Select(c => new
            {
                Code = c.Code,
                Name = c.Name,
                DepreciationDate = c.DepreciationDate,
                Price = c.Price,
                DepreciationYear = c.DepreciationYear,
                DepreciationMonth = c.DepreciationMonth,
                DepreciationYearQuota = c.DepreciationYearQuota,
                DepreciationMonthQuota = c.DepreciationMonthQuota,
                PreviousAccumulateDepreciationValue = c.PreviousAccumulateDepreciationValue,
                ThisPeriodAccumulateDepreciationValue = c.ThisPeriodAccumulateDepreciationValue,
                AccumulatedDepreciationValue = c.AccumulatedDepreciationValue,
                RemainValue = c.RemainValue
            }));

            Response.Buffer = false;
            Response.ClearContent();
            Response.ClearHeaders();

            Stream stream = rd.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
            stream.Seek(0, SeekOrigin.Begin);

            return File(stream, "application/pdf", "FixedAssetReport.pdf");
        }

        /// <summary>
        /// Report product price
        /// </summary>
        /// <returns></returns>
        public async Task<ActionResult> ReportProductPrice(string filter)
        {
            //convert string to filter
            var base64EncodedBytes = System.Convert.FromBase64String(filter);
            var unHashFilter = System.Text.Encoding.UTF8.GetString(base64EncodedBytes);
            //var format = "dd/MM/yyyy"; // your datetime format
            var dateTimeConverter = new IsoDateTimeConverter { Culture = new System.Globalization.CultureInfo("en-US") };
            var filterModel = JsonConvert.DeserializeObject<Domain.Services.InputIncurredVoucherFacade.ReportFilterModel>(unHashFilter, dateTimeConverter);

            //handle export
            var res = _incurredVoucherService.ReportProductPrice(filterModel);

            var rd = new ReportDocument();
            rd.Load(Path.Combine(Server.MapPath("~/Reports"), "ProductPriceReport.rpt"));

            TextObject companyName = (TextObject)rd.ReportDefinition.ReportObjects["CompanyName"];
            TextObject address = (TextObject)rd.ReportDefinition.ReportObjects["Address"];
            TextObject taxCode = (TextObject)rd.ReportDefinition.ReportObjects["TaxCode"];
            TextObject reportTime = (TextObject)rd.ReportDefinition.ReportObjects["ReportTime"];

            companyName.Text = Configuration.CompanyName;
            address.Text = Configuration.Address;
            taxCode.Text = Configuration.TaxCode;
            reportTime.Text = $"Tháng {filterModel.Month} năm {filterModel.Year}";

            rd.SetDataSource(res.Select(c => new
            {
                c.Code,
                c.Name,
                c.Quantity,
                c.FactoryPrice,
                c.FactoryAmount,
                c.WholePrice,
                c.WholeAmount
            }));

            Response.Buffer = false;
            Response.ClearContent();
            Response.ClearHeaders();

            Stream stream = rd.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
            stream.Seek(0, SeekOrigin.Begin);

            return File(stream, "application/pdf", "ProductPriceReport.pdf");
        }

        /// <summary>
        /// In phiếu thu tiền mặt
        /// </summary>
        /// <returns></returns>
        public async Task<ActionResult> PrintCashReceipt(long id)
        {
            //handle export
            var voucher = await _incurredVoucherService.GetByIdAsync(id);

            var rd = new ReportDocument();
            rd.Load(Path.Combine(Server.MapPath("~/Reports"), "CashReceiptTicket.rpt"));

            TextObject companyName = (TextObject)rd.ReportDefinition.ReportObjects["CompanyName"];
            TextObject address = (TextObject)rd.ReportDefinition.ReportObjects["Address"];
            TextObject taxCode = (TextObject)rd.ReportDefinition.ReportObjects["TaxCode"];
            //TextObject reportTime = (TextObject)rd.ReportDefinition.ReportObjects["ReportTime"];

            companyName.Text = Configuration.CompanyName;
            address.Text = Configuration.Address;
            taxCode.Text = Configuration.TaxCode;

            var items = await _incurredVoucherService.FetchAsync(c => c.DocumentTypeId == voucher.DocumentTypeId && c.NumberDocument == voucher.NumberDocument && c.DayVoucher == voucher.DayVoucher);
            var total = items.Sum(c => c.Amount);
            var reason = string.Join(";", items.Select(c => c.Description));

            var file1 = new CashReceiptPrinter()
            {
                NumberDocument = voucher.NumberDocument,
                FullName = voucher.FullNameVoucher,
                Address = voucher.AddressVoucher,
                Reason = reason,
                Amount = total,
                AmountByWord = total.ToText(),
                IncludeVouchers = voucher.IncludeVouchers,
                Date = String.Format("Ngày {0} tháng {1} năm {2}", voucher.DayVoucher.Day, voucher.DayVoucher.Month, voucher.DayVoucher.Year),
                BalanceAccountCode = voucher.BalanceAccountCode,
                DirectorName = Configuration.DirectorName,
                ChiefAccountName = Configuration.ChiefAccountantName,
                PayerName = voucher.FullNameVoucher,
                TreasurerName = Configuration.Treasurer,
                LabelName = "Liên 1: Lưu"
            };

            var file2 = new CashReceiptPrinter()
            {
                NumberDocument = voucher.NumberDocument,
                FullName = voucher.FullNameVoucher,
                Address = voucher.AddressVoucher,
                Reason = reason,
                Amount = total,
                AmountByWord = total.ToText(),
                IncludeVouchers = voucher.IncludeVouchers,
                Date = String.Format("Ngày {0} tháng {1} năm {2}", voucher.DayVoucher.Day, voucher.DayVoucher.Month, voucher.DayVoucher.Year),
                BalanceAccountCode = voucher.BalanceAccountCode,
                DirectorName = Configuration.DirectorName,
                ChiefAccountName = Configuration.ChiefAccountantName,
                PayerName = voucher.FullNameVoucher,
                TreasurerName = Configuration.Treasurer,
                LabelName = "Liên 2: Giao cho khách hàng"
            };

            rd.SetDataSource(new CashReceiptPrinter[] { file1, file2 });

            Response.Buffer = false;
            Response.ClearContent();
            Response.ClearHeaders();

            Stream stream = rd.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
            stream.Seek(0, SeekOrigin.Begin);

            return File(stream, "application/pdf", "CashReceiptPrinter.pdf");
        }

        /// <summary>
        /// In phiếu xuất kho
        /// </summary>
        /// <returns></returns>
        public async Task<ActionResult> PrintExportStockTicket(long id)
        {
            //handle export
            var voucher = await _incurredVoucherService.GetByIdAsync(id);

            var rd = new ReportDocument();
            rd.Load(Path.Combine(Server.MapPath("~/Reports"), "ExportStockTicket.rpt"));

            TextObject companyName = (TextObject)rd.ReportDefinition.ReportObjects["CompanyName"];
            TextObject address = (TextObject)rd.ReportDefinition.ReportObjects["Address"];
            TextObject taxCode = (TextObject)rd.ReportDefinition.ReportObjects["TaxCode"];
            TextObject dateTime = (TextObject)rd.ReportDefinition.ReportObjects["DateTime"];
            TextObject dateTime2 = (TextObject)rd.ReportDefinition.ReportObjects["DateTime2"];
            TextObject numberDocument = (TextObject)rd.ReportDefinition.ReportObjects["NumberDocument"];
            TextObject debtAccountCode = (TextObject)rd.ReportDefinition.ReportObjects["DebtAccountCode"];
            TextObject balanceAccountCode = (TextObject)rd.ReportDefinition.ReportObjects["BalanceAccountCode"];
            TextObject fullName = (TextObject)rd.ReportDefinition.ReportObjects["FullName"];
            TextObject addressTicket = (TextObject)rd.ReportDefinition.ReportObjects["AddressTicket"];
            TextObject reason = (TextObject)rd.ReportDefinition.ReportObjects["Reason"];
            TextObject amount = (TextObject)rd.ReportDefinition.ReportObjects["Amount"];
            TextObject amountText = (TextObject)rd.ReportDefinition.ReportObjects["AmountText"];
            TextObject stock = (TextObject)rd.ReportDefinition.ReportObjects["Stock"];
            TextObject includeVoucher = (TextObject)rd.ReportDefinition.ReportObjects["IncludeVouchers"];
            TextObject receiver = (TextObject)rd.ReportDefinition.ReportObjects["Receiver"];
            TextObject stocker = (TextObject)rd.ReportDefinition.ReportObjects["Stocker"];
            TextObject chiefAccountant = (TextObject)rd.ReportDefinition.ReportObjects["ChiefAccountant"];
            TextObject director = (TextObject)rd.ReportDefinition.ReportObjects["Director"];

            companyName.Text = Configuration.CompanyName;
            address.Text = Configuration.Address;
            taxCode.Text = Configuration.TaxCode;
            dateTime.Text = $"Ngày {voucher.DayVoucher.Day} tháng {voucher.DayVoucher.Month} năm {voucher.DayVoucher.Year}";
            dateTime2.Text = $"Ngày {voucher.DayVoucher.Day} tháng {voucher.DayVoucher.Month} năm {voucher.DayVoucher.Year}";
            numberDocument.Text = $"Số: {voucher.NumberDocument}";
            debtAccountCode.Text = $"Nợ: {voucher.DebtAccountCode}";
            balanceAccountCode.Text = $"Có: {voucher.BalanceAccountCode}";
            fullName.Text = voucher.FullNameVoucher ?? string.Empty;
            addressTicket.Text = voucher.AddressVoucher ?? string.Empty;
            reason.Text = voucher.Description ?? string.Empty;
            stock.Text = voucher.StockCode ?? string.Empty;
            includeVoucher.Text = voucher.IncludeVouchers ?? string.Empty;
            receiver.Text = voucher.FullNameVoucher;
            stocker.Text = Configuration.Stocker;
            chiefAccountant.Text = Configuration.ChiefAccountantName;
            director.Text = Configuration.DirectorName;

            var items = await _incurredVoucherService.FetchAsync(c => c.DocumentTypeId == voucher.DocumentTypeId && c.NumberDocument == voucher.NumberDocument && c.DayVoucher == voucher.DayVoucher && c.BalanceAccountId == voucher.BalanceAccountId);

            var total = items.Sum(c => c.Amount);
            amount.Text = total.ToString("N2", new CultureInfo("vi-VN"));
            amountText.Text = total.ToText();

            rd.SetDataSource(items.Select(c => new ExportStockTicketModel()
            {
                Amount = c.Amount,
                Code = c.BalanceDetailCode ?? string.Empty,
                Name = c.BalanceDetailName ?? string.Empty,
                Price = c.Price ?? 0,
                Quantity = c.Quantity ?? 0,
                Unit = c.Unit ?? string.Empty
            }));

            Response.Buffer = false;
            Response.ClearContent();
            Response.ClearHeaders();

            Stream stream = rd.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
            stream.Seek(0, SeekOrigin.Begin);

            return File(stream, "application/pdf", "ExportStockTicket.pdf");
        }

        /// <summary>
        /// In phiếu chi tiền mặt
        /// </summary>
        /// <returns></returns>
        public async Task<ActionResult> PrintCashVoucher(long id)
        {
            //handle export
            var voucher = await _incurredVoucherService.GetByIdAsync(id);

            var rd = new ReportDocument();
            rd.Load(Path.Combine(Server.MapPath("~/Reports"), "CashVoucherTicket.rpt"));

            TextObject companyName = (TextObject)rd.ReportDefinition.ReportObjects["CompanyName"];
            TextObject address = (TextObject)rd.ReportDefinition.ReportObjects["Address"];
            TextObject taxCode = (TextObject)rd.ReportDefinition.ReportObjects["TaxCode"];
            //TextObject reportTime = (TextObject)rd.ReportDefinition.ReportObjects["ReportTime"];

            companyName.Text = Configuration.CompanyName;
            address.Text = Configuration.Address;
            taxCode.Text = Configuration.TaxCode;

            var items = await _incurredVoucherService.FetchAsync(c => c.DocumentTypeId == voucher.DocumentTypeId && c.NumberDocument == voucher.NumberDocument && c.DayVoucher == voucher.DayVoucher);
            var total = items.Sum(c => c.Amount);
            var reason = string.Join(";", items.Select(c => c.Description));

            var file1 = new CashReceiptPrinter()
            {
                NumberDocument = voucher.NumberDocument,
                FullName = voucher.FullNameVoucher,
                Address = voucher.AddressVoucher,
                Reason = reason,
                Amount = total,
                AmountByWord = total.ToText(),
                IncludeVouchers = voucher.IncludeVouchers,
                Date = String.Format("Ngày {0} tháng {1} năm {2}", voucher.DayVoucher.Day, voucher.DayVoucher.Month, voucher.DayVoucher.Year),
                BalanceAccountCode = voucher.DebtAccountCode,
                DirectorName = Configuration.DirectorName,
                ChiefAccountName = Configuration.ChiefAccountantName,
                PayerName = voucher.FullNameVoucher,
                TreasurerName = Configuration.Treasurer,
                LabelName = "Liên 1: Lưu"
            };

            var file2 = new CashReceiptPrinter()
            {
                NumberDocument = voucher.NumberDocument,
                FullName = voucher.FullNameVoucher,
                Address = voucher.AddressVoucher,
                Reason = reason,
                Amount = total,
                AmountByWord = total.ToText(),
                IncludeVouchers = voucher.IncludeVouchers,
                Date = String.Format("Ngày {0} tháng {1} năm {2}", voucher.DayVoucher.Day, voucher.DayVoucher.Month, voucher.DayVoucher.Year),
                BalanceAccountCode = voucher.DebtAccountCode,
                DirectorName = Configuration.DirectorName,
                ChiefAccountName = Configuration.ChiefAccountantName,
                PayerName = voucher.FullNameVoucher,
                TreasurerName = Configuration.Treasurer,
                LabelName = "Liên 2: Giao cho khách hàng"
            };

            rd.SetDataSource(new CashReceiptPrinter[] { file1, file2 });

            Response.Buffer = false;
            Response.ClearContent();
            Response.ClearHeaders();

            Stream stream = rd.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
            stream.Seek(0, SeekOrigin.Begin);

            return File(stream, "application/pdf", "CashVoucherPrinter.pdf");
        }

        /// <summary>
        /// In phiếu nhập kho
        /// </summary>
        /// <returns></returns>
        public async Task<ActionResult> PrintImportStockTicket(long id)
        {
            //handle export
            var voucher = await _incurredVoucherService.GetByIdAsync(id);

            var rd = new ReportDocument();
            rd.Load(Path.Combine(Server.MapPath("~/Reports"), "ImportStockTicket.rpt"));

            TextObject companyName = (TextObject)rd.ReportDefinition.ReportObjects["CompanyName"];
            TextObject address = (TextObject)rd.ReportDefinition.ReportObjects["Address"];
            TextObject taxCode = (TextObject)rd.ReportDefinition.ReportObjects["TaxCode"];
            TextObject dateTime = (TextObject)rd.ReportDefinition.ReportObjects["DateTime"];
            TextObject dateTime2 = (TextObject)rd.ReportDefinition.ReportObjects["DateTime2"];
            TextObject numberDocument = (TextObject)rd.ReportDefinition.ReportObjects["NumberDocument"];
            TextObject debtAccountCode = (TextObject)rd.ReportDefinition.ReportObjects["DebtAccountCode"];
            TextObject balanceAccountCode = (TextObject)rd.ReportDefinition.ReportObjects["BalanceAccountCode"];
            TextObject fullName = (TextObject)rd.ReportDefinition.ReportObjects["FullName"];
            TextObject addressTicket = (TextObject)rd.ReportDefinition.ReportObjects["AddressTicket"];
            TextObject reason = (TextObject)rd.ReportDefinition.ReportObjects["Reason"];
            TextObject amount = (TextObject)rd.ReportDefinition.ReportObjects["Amount"];
            TextObject amountText = (TextObject)rd.ReportDefinition.ReportObjects["AmountText"];
            TextObject stock = (TextObject)rd.ReportDefinition.ReportObjects["Stock"];
            TextObject includeVoucher = (TextObject)rd.ReportDefinition.ReportObjects["IncludeVouchers"];
            TextObject receiver = (TextObject)rd.ReportDefinition.ReportObjects["Receiver"];
            TextObject stocker = (TextObject)rd.ReportDefinition.ReportObjects["Stocker"];
            TextObject chargeMaterialName = (TextObject)rd.ReportDefinition.ReportObjects["ChargeMaterialName"];

            companyName.Text = Configuration.CompanyName;
            address.Text = Configuration.Address;
            taxCode.Text = Configuration.TaxCode;
            dateTime.Text = $"Ngày {voucher.DayVoucher.Day} tháng {voucher.DayVoucher.Month} năm {voucher.DayVoucher.Year}";
            dateTime2.Text = $"Ngày {voucher.DayVoucher.Day} tháng {voucher.DayVoucher.Month} năm {voucher.DayVoucher.Year}";
            numberDocument.Text = $"Số: {voucher.NumberDocument}";
            debtAccountCode.Text = $"Nợ: {voucher.DebtAccountCode}";
            balanceAccountCode.Text = $"Có: {voucher.BalanceAccountCode}";
            fullName.Text = voucher.FullNameVoucher ?? string.Empty;
            addressTicket.Text = voucher.AddressVoucher ?? string.Empty;
            reason.Text = voucher.Description ?? string.Empty;
            stock.Text = voucher.StockCode ?? string.Empty;
            includeVoucher.Text = voucher.IncludeVouchers ?? string.Empty;
            receiver.Text = voucher.FullNameVoucher;
            stocker.Text = Configuration.Stocker;
            chargeMaterialName.Text = Configuration.ChargeMaterialName;

            var items = await _incurredVoucherService.FetchAsync(c => c.DocumentTypeId == voucher.DocumentTypeId && c.NumberDocument == voucher.NumberDocument && c.DayVoucher == voucher.DayVoucher && c.DebtAccountId == voucher.DebtAccountId);

            var total = items.Sum(c => c.Amount);
            amount.Text = total.ToString("N2", new CultureInfo("vi-VN"));
            amountText.Text = total.ToText();

            rd.SetDataSource(items.Select(c => new ExportStockTicketModel()
            {
                Amount = c.Amount,
                Code = c.DebtDetailCode ?? string.Empty,
                Name = c.DebtDetailName ?? string.Empty,
                Price = c.Price ?? 0,
                Quantity = c.Quantity ?? 0,
                Unit = c.Unit ?? string.Empty
            }));

            Response.Buffer = false;
            Response.ClearContent();
            Response.ClearHeaders();

            Stream stream = rd.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
            stream.Seek(0, SeekOrigin.Begin);

            return File(stream, "application/pdf", "ImportStockTicket.pdf");
        }

        /// <summary>
        /// In hóa đơn VAT đầu ra
        /// </summary>
        /// <returns></returns>
        public async Task<ActionResult> PrintVATInvoiceTicket(long id)
        {
            //handle export
            var voucher = await _incurredVoucherService.GetByIdAsync(id);

            var rd = new ReportDocument();
            rd.Load(Path.Combine(Server.MapPath("~/Reports"), "VATInvoice.rpt"));

            TextObject companyName = (TextObject)rd.ReportDefinition.ReportObjects["CompanyName"];
            TextObject address = (TextObject)rd.ReportDefinition.ReportObjects["Address"];
            TextObject taxCode = (TextObject)rd.ReportDefinition.ReportObjects["TaxCode"];
            TextObject dateTime = (TextObject)rd.ReportDefinition.ReportObjects["DateTime"];
            TextObject sign = (TextObject)rd.ReportDefinition.ReportObjects["Sign"];
            TextObject billNumber = (TextObject)rd.ReportDefinition.ReportObjects["BillNumber"];
            TextObject buyerName = (TextObject)rd.ReportDefinition.ReportObjects["BuyerName"];
            TextObject buyerTaxCode = (TextObject)rd.ReportDefinition.ReportObjects["BuyerTaxCode"];
            TextObject buyerAddress = (TextObject)rd.ReportDefinition.ReportObjects["BuyerAddress"];
            TextObject payment = (TextObject)rd.ReportDefinition.ReportObjects["Payment"];
            TextObject amount = (TextObject)rd.ReportDefinition.ReportObjects["Amount"];
            TextObject vatValue = (TextObject)rd.ReportDefinition.ReportObjects["VATValue"];
            TextObject vat = (TextObject)rd.ReportDefinition.ReportObjects["VAT"];
            TextObject total = (TextObject)rd.ReportDefinition.ReportObjects["Total"];
            TextObject amountText = (TextObject)rd.ReportDefinition.ReportObjects["AmountText"];
            TextObject director = (TextObject)rd.ReportDefinition.ReportObjects["Director"];

            companyName.Text = Configuration.CompanyName;
            address.Text = Configuration.Address;
            taxCode.Text = Configuration.TaxCode;
            dateTime.Text = $"Ngày {voucher.DayVoucher.Day} tháng {voucher.BillDate.Value.Month} năm {voucher.BillDate.Value.Year}";
            sign.Text = $"Ký hiệu: {voucher.Seri}";
            billNumber.Text = $"Số hóa đơn: {voucher.BillNumber}";
            buyerName.Text = voucher.BusinessCompany ?? string.Empty;
            buyerTaxCode.Text = voucher.TaxNumber ?? string.Empty;
            buyerAddress.Text = voucher.BillAddress ?? string.Empty;
            payment.Text = voucher.KKBS ?? string.Empty;
            director.Text = Configuration.DirectorName;

            //var items = await _incurredVoucherService.FetchAsync(c => c.DocumentTypeId == voucher.DocumentTypeId && c.NumberDocument == voucher.NumberDocument && c.DayVoucher == voucher.DayVoucher && c.DebtAccountId == voucher.DebtAccountId);

            var vatValueData = 0;
            switch (voucher.Bill)
            {
                case "R01":
                case "R02":
                case "R05": vatValueData = 0; break;
                case "R03": vatValueData = 5; break;
                case "R04": vatValueData = 10; break;
            }

            amount.Text = voucher.Amount.ToString("N2", new CultureInfo("vi-VN"));
            vatValue.Text = $"{vatValueData} %";
            vat.Text = vatValueData > 0 ? (vatValueData * voucher.Amount / 100).ToString("N2", new CultureInfo("vi-VN")) : "";
            var sum = (voucher.Amount + vatValueData * voucher.Amount / 100);
            total.Text = sum.ToString("N2", new CultureInfo("vi-VN"));
            amountText.Text = sum.ToText();

            var list = new List<VATInvoiceModel>();
            for (var i = 0; i < 0; i++)
            {
                list.Add(new VATInvoiceModel()
                {
                    Amount = 0,
                    Unit = string.Empty,
                    Name = string.Empty,
                    Price = 0,
                    Quantity = 0,
                });
            }

            list.Insert(0, new VATInvoiceModel()
            {
                Amount = voucher.Amount,
                Unit = string.Empty,
                Name = voucher.BillProduct ?? string.Empty,
                Price = voucher.Price ?? 0,
                Quantity = voucher.Quantity ?? 0,
            });

            rd.SetDataSource(list);

            Response.Buffer = false;
            Response.ClearContent();
            Response.ClearHeaders();

            Stream stream = rd.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
            stream.Seek(0, SeekOrigin.Begin);

            return File(stream, "application/pdf", "VATInvoice.pdf");
        }


        /// <summary>
        /// In phiếu xuất kho bán hàng
        /// </summary>
        /// <returns></returns>
        public async Task<ActionResult> PrintExportPriceTicket(long id)
        {
            //handle export
            var voucher = await _incurredVoucherService.GetByIdAsync(id);

            var rd = new ReportDocument();
            rd.Load(Path.Combine(Server.MapPath("~/Reports"), "ExportPriceTicket.rpt"));

            TextObject companyName = (TextObject)rd.ReportDefinition.ReportObjects["CompanyName"];
            TextObject address = (TextObject)rd.ReportDefinition.ReportObjects["Address"];
            TextObject taxCode = (TextObject)rd.ReportDefinition.ReportObjects["TaxCode"];
            TextObject dateTime = (TextObject)rd.ReportDefinition.ReportObjects["DateTime"];
            TextObject dateTime2 = (TextObject)rd.ReportDefinition.ReportObjects["DateTime2"];
            //TextObject sign = (TextObject)rd.ReportDefinition.ReportObjects["Sign"];
            TextObject billNumber = (TextObject)rd.ReportDefinition.ReportObjects["BillNumber"];
            TextObject buyerName = (TextObject)rd.ReportDefinition.ReportObjects["BuyerName"];
            TextObject buyerTaxCode = (TextObject)rd.ReportDefinition.ReportObjects["BuyerTaxCode"];
            TextObject buyerAddress = (TextObject)rd.ReportDefinition.ReportObjects["BuyerAddress"];
            //TextObject payment = (TextObject)rd.ReportDefinition.ReportObjects["Payment"];
            TextObject amount = (TextObject)rd.ReportDefinition.ReportObjects["Amount"];
            //TextObject vatValue = (TextObject)rd.ReportDefinition.ReportObjects["VATValue"];
            TextObject vat = (TextObject)rd.ReportDefinition.ReportObjects["VAT"];
            TextObject total = (TextObject)rd.ReportDefinition.ReportObjects["Total"];
            TextObject amountText = (TextObject)rd.ReportDefinition.ReportObjects["AmountText"];
            TextObject chargeMaterialName = (TextObject)rd.ReportDefinition.ReportObjects["ChargeMaterialName"];

            companyName.Text = Configuration.CompanyName;
            address.Text = Configuration.Address;
            taxCode.Text = Configuration.TaxCode;
            dateTime.Text = $"Ngày {voucher.DayVoucher.Day} tháng {voucher.BillDate.Value.Month} năm {voucher.BillDate.Value.Year}";
            dateTime2.Text = dateTime.Text;
            //sign.Text = $"Ký hiệu: {voucher.Seri}";
            billNumber.Text = $"Số HĐ: {voucher.BillNumber}";
            buyerName.Text = voucher.BusinessCompany ?? string.Empty;
            buyerTaxCode.Text = voucher.TaxNumber ?? string.Empty;
            buyerAddress.Text = voucher.BillAddress ?? string.Empty;
            //payment.Text = voucher.KKBS ?? string.Empty;
            chargeMaterialName.Text = Configuration.ChargeMaterialName;

            //var items = await _incurredVoucherService.FetchAsync(c => c.DocumentTypeId == voucher.DocumentTypeId && c.NumberDocument == voucher.NumberDocument && c.DayVoucher == voucher.DayVoucher && c.DebtAccountId == voucher.DebtAccountId);

            var vatValueData = 0;
            switch (voucher.Bill)
            {
                case "R01":
                case "R02":
                case "R05": vatValueData = 0; break;
                case "R03": vatValueData = 5; break;
                case "R04": vatValueData = 10; break;
            }

            amount.Text = voucher.Amount.ToString("N2", new CultureInfo("vi-VN"));
            //vatValue.Text = $"{vatValueData} %";
            vat.Text = vatValueData > 0 ? (vatValueData * voucher.Amount / 100).ToString("N2", new CultureInfo("vi-VN")) : "";
            var sum = (voucher.Amount + vatValueData * voucher.Amount / 100);
            total.Text = sum.ToString("N2", new CultureInfo("vi-VN"));
            amountText.Text = sum.ToText();

            var list = new List<VATInvoiceModel>();
            for (var i = 0; i < 0; i++)
            {
                list.Add(new VATInvoiceModel()
                {
                    Amount = 0,
                    Unit = string.Empty,
                    Name = string.Empty,
                    Price = 0,
                    Quantity = 0,
                });
            }

            list.Insert(0, new VATInvoiceModel()
            {
                Amount = voucher.Amount,
                Unit = string.Empty,
                Name = voucher.BillProduct ?? string.Empty,
                Price = voucher.Price ?? 0,
                Quantity = voucher.Quantity ?? 0,
            });

            rd.SetDataSource(list);

            Response.Buffer = false;
            Response.ClearContent();
            Response.ClearHeaders();

            Stream stream = rd.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
            stream.Seek(0, SeekOrigin.Begin);

            return File(stream, "application/pdf", "ExportPriceTicket.pdf");
        }

        public ActionResult Index()
        {
            return Content("Reporting service");
        }
    }
}