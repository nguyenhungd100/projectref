﻿using TQQ.POMS.Domain.ModelBinders;
using Newtonsoft.Json;
using System;
using System.ComponentModel.DataAnnotations;

namespace TQQ.POMS.Domain.Services.InputIncurredVoucherFacade
{
    public class AccountInventoryModel
    {
        public string Code { set; get; }
        public string Name { set; get; }
        public string Unit { set; get; }

        /// <summary>
        /// Số lượng tồn đầu kỳ
        /// </summary>
        public decimal QuantityStart { set; get; }

        /// <summary>
        /// Giá tồn đầu kỳ
        /// </summary>
        public decimal PriceStart { set; get; }

        /// <summary>
        /// Giá tồn đầu kỳ
        /// </summary>
        public decimal AmountStart { set; get; }

        /// <summary>
        /// Số lượng nhập trong kỳ
        /// </summary>
        public decimal QuantityImport { set; get; }

        /// <summary>
        /// Giá nhập trong kỳ
        /// </summary>
        public decimal PriceImport { set; get; }

        /// <summary>
        /// Giá xuất trong kỳ
        /// </summary>
        public decimal AmountExport { set; get; }

        /// <summary>
        /// Số lượng xuất trong kỳ
        /// </summary>
        public decimal QuantityExport { set; get; }

        /// <summary>
        /// Giá xuất trong kỳ
        /// </summary>
        public decimal PriceExport { set; get; }

        /// <summary>
        /// Giá nhập trong kỳ
        /// </summary>
        public decimal AmountImport { set; get; }

        /// <summary>
        /// Số lượng tồn cuối kỳ
        /// </summary>
        public decimal QuantityEnd { set; get; }

        /// <summary>
        /// Giá tồn cuối kỳ
        /// </summary>
        public decimal PriceEnd { set; get; }

        /// <summary>
        /// Giá tồn cuối kỳ
        /// </summary>
        public decimal AmountEnd { set; get; }
    }
}