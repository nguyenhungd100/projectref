﻿using TQQ.POMS.Domain.Validators;
using Newtonsoft.Json;
using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace TQQ.POMS.Reporter.Models
{
    public class AccountBalanceModel
    {
        public long Id { set; get; }

        /// <summary>
        /// Mã TK
        /// </summary>
        [MaxLength(6, ErrorMessage = "Mã tài khoản tối đa là 6 ký tự")]
        [MinLength(3, ErrorMessage = "Mã tài khoản tối thiểu là 3 ký tự")]
        [Required(ErrorMessage = "Mã tài khoản là bắt buộc")]
        public string Code { get; set; }

        /// <summary>
        /// Năm làm việc
        /// </summary>
        [Range(1000, 9999, ErrorMessage = "Năm làm việc không hợp lệ. Giá trị hợp lệ là 1000 đến 9999")]
        [Required(ErrorMessage = "Năm làm việc là bắt buộc")]
        public int Year { get; set; }

        /// <summary>
        /// Tên tài khoản
        /// </summary>
        [MaxLength(250, ErrorMessage = "Tên tài khoản tối đa là 250 ký tự")]
        [MinLength(3, ErrorMessage = "Tên tài khoản tối thiểu là 3 ký tự")]
        [Required(ErrorMessage = "Tên tài khoản là bắt buộc")]
        public string Name { get; set; }

        /// <summary>
        /// Dư nợ đầu năm
        /// </summary>                
        [JsonConverter(typeof(TQQ.POMS.Domain.ModelBinders.DecimalConverter))]
        public decimal LoanAtStartYear { set; get; }

        /// <summary>
        /// Dư có đầu năm
        /// </summary>
        [JsonConverter(typeof(TQQ.POMS.Domain.ModelBinders.DecimalConverter))]
        public decimal BalanceAtStartYear { set; get; }

        /// <summary>
        /// PS Nợ
        /// </summary>
        [JsonConverter(typeof(TQQ.POMS.Domain.ModelBinders.DecimalConverter))]
        public decimal IncurredLoan { set; get; }

        /// <summary>
        /// PS Có
        /// </summary>
        [JsonConverter(typeof(TQQ.POMS.Domain.ModelBinders.DecimalConverter))]
        public decimal IncurredBalance { set; get; }

        /// <summary>
        /// Dư nợ cuối năm
        /// </summary>
        [JsonConverter(typeof(TQQ.POMS.Domain.ModelBinders.DecimalConverter))]
        public decimal LoanAtEndYear { set; get; }

        /// <summary>
        /// Dư có cuối năm
        /// </summary>
        [JsonConverter(typeof(TQQ.POMS.Domain.ModelBinders.DecimalConverter))]
        public decimal BalanceAtEndYear { set; get; }
    }
}



