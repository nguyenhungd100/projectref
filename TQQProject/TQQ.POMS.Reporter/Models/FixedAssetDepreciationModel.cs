﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TQQ.POMS.Reporter.Models
{
    public class FixedAssetDepreciationModel
    {
        /// <summary>
        /// Mã ts
        /// </summary>
        public string Code { set; get; }

        /// <summary>
        /// Tên TS
        /// </summary>
        public string Name { set; get; }

        /// <summary>
        /// Nguyên giá
        /// </summary>
        public decimal Price { set; get; }

        /// <summary>
        /// Ngày tính khấu hao
        /// </summary>
        public DateTime DepreciationDate { set; get; }

        /// <summary>
        /// Năm khấu hao
        /// </summary>
        public decimal DepreciationYear { set; get; }

        /// <summary>
        /// Tháng khấu hao
        /// </summary>
        public decimal DepreciationMonth { set; get; }

        /// <summary>
        /// Mức khấu hao năm
        /// </summary>
        public decimal DepreciationYearQuota { set; get; }

        /// <summary>
        /// Mức khấu hao tháng
        /// </summary>
        public decimal DepreciationMonthQuota { set; get; }

        /// <summary>
        /// Giá trị KH lũy kế kỳ trước
        /// </summary>
        public decimal PreviousAccumulateDepreciationValue { set; get; }

        /// <summary>
        /// Giá trị KH lũy kế kỳ này
        /// </summary>
        public decimal ThisPeriodAccumulateDepreciationValue { set; get; }

        /// <summary>
        /// Giá trị KH lũy kế
        /// </summary>
        public decimal AccumulatedDepreciationValue { set; get; }

        /// <summary>
        /// Giá trị còn lại
        /// </summary>
        public decimal RemainValue { set; get; }
    }
}