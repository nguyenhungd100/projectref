﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TQQ.POMS.Reporter.Models
{
    public class LoanDetailReportModel
    {
        public DateTime RecordDate { set; get; }
        public string NumberOfBookVouchers { set; get; }
        public DateTime DayVoucher { set; get; }
        public string NumberDocument { set; get; }
        public string Description { set; get; }

        /// <summary>
        /// Tài khoản đối ứng
        /// </summary>
        public string AccountCode { set; get; }

        /// <summary>
        /// Mã Chi tiết
        /// </summary>
        public string DetailCode { set; get; }

        /// <summary>
        /// Nợ phát sinh
        /// </summary>
        public decimal LoanIncurred { set; get; }

        /// <summary>
        /// Có phát sinh
        /// </summary>
        public decimal BalanceIncurred { set; get; }

        /// <summary>
        /// Số dư bên có
        /// </summary>
        public decimal Amount { set; get; }
    }
}