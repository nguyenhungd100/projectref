﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TQQ.POMS.Reporter.Models
{
    public class CashReceiptPrinter
    {
        /// <summary>
        /// Số chứng từ
        /// </summary>
        public string NumberDocument { set; get; }

        public string FullName { set; get; }

        public string Address { set; get; }

        public string Reason { set; get; }

        public decimal Amount { set; get; }

        public string AmountByWord { set; get; }

        public string IncludeVouchers { set; get; }

        public string Date { set; get; }

        public string BalanceAccountCode { set; get; }

        public string DirectorName { set; get; }

        public string ChiefAccountName { set; get; }

        public string PayerName { set; get; }

        public string TreasurerName { set; get; }

        /// <summary>
        /// Niên 1,2
        /// </summary>
        public string LabelName { set; get; }
    }
}