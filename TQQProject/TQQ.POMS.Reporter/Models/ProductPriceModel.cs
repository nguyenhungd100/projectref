﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TQQ.POMS.Reporter.Models
{
    public class ProductPriceModel
    {
        public string Code { set; get; }

        public string Name { set; get; }

        public decimal Quantity { set; get; }

        public decimal FactoryPrice { set; get; }

        public decimal FactoryAmount { set; get; }

        public decimal WholePrice { set; get; }

        public decimal WholeAmount { set; get; }
    }
}