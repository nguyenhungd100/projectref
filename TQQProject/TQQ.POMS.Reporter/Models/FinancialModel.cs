﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TQQ.POMS.Reporter.Models
{
    public class FinancialModel
    {
        /// <summary>
        /// Chỉ tiêu tài chính
        /// </summary>
        public string Target { set; get; }

        public string Code { set; get; }

        /// <summary>
        /// Thuyết minh
        /// </summary>
        public string Present { set; get; }

        public decimal ThisYear { set; get; }

        public decimal PreviousYear { set; get; }
    }
}