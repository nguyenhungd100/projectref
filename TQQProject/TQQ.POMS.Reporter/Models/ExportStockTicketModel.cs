﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TQQ.POMS.Reporter.Models
{
    public class ExportStockTicketModel
    {
        public string Code { set; get; }
        public string Name { set; get; }
        public string Unit { set; get; }
        public decimal Quantity { set; get; }
        public decimal Price { set; get; }
        public decimal Amount { set; get; }
    }
}