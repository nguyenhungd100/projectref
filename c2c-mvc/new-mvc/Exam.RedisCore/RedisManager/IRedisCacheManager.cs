﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace Exam.RedisCore.RedisManager
{
    public interface IRedisCacheManager<T> where T : class, new()
    {
        T Get<T>(string id);

        IQueryable<T> GetAll<T>();

        IQueryable<T> GetAll<T>(string hash, string value, Expression<Func<T, bool>> filter);

        IQueryable<T> GetAll<T>(string hash, string value);

        void Set<T>(T item);

        void Set<T>(T item, string hash, string value, string keyName);

        void Set<T>(T item, List<string> hash, List<string> value, string keyName);

        void SetAll<T>(List<T> listItems);

        void SetAll<T>(List<T> list, string hash, string value, string keyName);

        void SetAll<T>(List<T> list, List<string> hash, List<string> value, string keyName);

        void Delete<T>(T item);

        void DeleteAll<T>(T item);

        long PublishMessage(string channel, object item);
    }
}
