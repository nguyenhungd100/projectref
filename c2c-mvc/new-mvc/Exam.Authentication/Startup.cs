﻿using Exam.Authentication.IdentityServer;
using Exam.CoreData;
using Exam.CoreData.Repository;
using Exam.CoreData.Repository.Implement;
using Exam.Services.RoleFacade;
using Exam.Services.RoleFacade.Implement;
using Exam.Services.UserFacade;
using Exam.Services.UserFacade.Implement;
using IdentityServer4.Services;
using IdentityServer4.Validation;
using Microsoft.AspNetCore.Antiforgery;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Serilog;
using Services.UserFacade;
using Services.UserFacade.Implement;
using System.IdentityModel.Tokens.Jwt;
using System.IO;
using System.Security.Cryptography.X509Certificates;

namespace Exam.Authentication
{
    public class Startup
    {
        public IConfiguration _configuration { get; private set; }
        public IHostingEnvironment _environment { get; private set; }
        public Startup(IConfiguration configuration, IHostingEnvironment environment)
        {
            _configuration = configuration;
            _environment = environment;

            var builder = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true)
                .AddJsonFile($"appsettings.{environment.EnvironmentName}.json", optional: true)
                .AddEnvironmentVariables();

            _configuration = builder.Build();

            Log.Logger = new LoggerConfiguration()
                        .ReadFrom.Configuration(_configuration)
                        .Enrich.FromLogContext()
                        .CreateLogger();
        }



        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc(options =>
            {
                options.Filters.Add(new AutoValidateAntiforgeryTokenAttribute());
            })/*.SetCompatibilityVersion(CompatibilityVersion.Version_2_2)*/;
            services.AddOptions();
            var appSettingSection = _configuration.GetSection("AppSettings");
            services.Configure<AppSettings>(appSettingSection);
            var appSettings = new AppSettings();
            appSettingSection.Bind(appSettings);

            services.AddDbContext<ExamDBContext>(
                option => option.UseSqlServer(_configuration.GetConnectionString("DefaultConnection")));

            services.AddScoped(typeof(IRepository<,>), typeof(Repository<,>));
            services.AddScoped<IAuthorizationService, AuthorizationService>();
            services.AddScoped<IRoleService, RoleService>();
            services.AddScoped<IUserService, UserService>();



            var cert = new X509Certificate2(Path.Combine(_environment.ContentRootPath, "IdentityServer", "idsrv3test.pfx"), "idsrv3test");
            services.AddIdentityServer()
                    .AddSigningCredential(cert)
                    .AddInMemoryIdentityResources(ConfigIdentityServer.GetIdentityResources())
                    .AddInMemoryApiResources(ConfigIdentityServer.GetApiResources())
                    .AddInMemoryClients(ConfigIdentityServer.GetClients(appSettings))
                    .AddProfileService<ProfileService>();

            // services.ConfigDI(_configuration);
            services.AddTransient<IResourceOwnerPasswordValidator, ResourceOwnerPasswordValidator>();
            services.AddTransient<IProfileService, ProfileService>();

            services.AddCors();
            services.AddAntiforgery(c => c.HeaderName = "X-XSRF-TOKEN");


        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, IAntiforgery antiforgery)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                //app.UseHsts();
                app.UseExceptionHandler("/Error");
            }
            app.UseStaticFiles();
            app.UseIdentityServer();
            JwtSecurityTokenHandler.DefaultInboundClaimTypeMap.Clear();
            //app.UseHttpsRedirection();
            app.UseCors(builder => builder.AllowAnyOrigin()
                .AllowAnyMethod()
                .AllowAnyHeader()
                .AllowCredentials());

            app.Use(next => context =>
            {
                string path = context.Request.Path.Value;
                var tokens = antiforgery.GetAndStoreTokens(context);
                context.Response.Cookies.Append("XSRF-TOKEN", tokens.RequestToken, new CookieOptions() { HttpOnly = false });
                return next(context);
            });

            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=Index}/{id?}"
                    );
            });
        }
    }
}
