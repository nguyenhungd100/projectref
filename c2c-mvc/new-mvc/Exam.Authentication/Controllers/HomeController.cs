﻿using Exam.Services.RoleFacade;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Exam.Authentication.Controllers
{
    public class HomeController : Controller
    {
        private readonly IRoleService _roleService;


        public HomeController(IRoleService roleService)
        {
            _roleService = roleService;
        }
        // GET: api/<controller>
        [HttpGet("/")]
        public string Index()
        {
            return "Authen Server";
        }

        // GET api/<controller>/5
        [HttpGet("/Error")]
        public string Get(int id)
        {
            return "Internal Server Error";
        }


    }
}
