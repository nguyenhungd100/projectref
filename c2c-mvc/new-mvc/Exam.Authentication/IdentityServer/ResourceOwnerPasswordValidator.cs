﻿using Exam.CoreData.Models.Users;
using Exam.Services.RoleFacade;
using Exam.Services.UserFacade;
using IdentityServer4.Models;
using IdentityServer4.Validation;
using System;
using System.Threading.Tasks;

namespace Exam.Authentication.IdentityServer
{
    public class ResourceOwnerPasswordValidator : IResourceOwnerPasswordValidator
    {
        private readonly IAuthorizationService _authorizationService;
        private readonly IRoleService _roleService;

        public ResourceOwnerPasswordValidator(IAuthorizationService authorizationService, IRoleService roleService)
        {
            _authorizationService = authorizationService;
            _roleService = roleService;
        }

        public async Task ValidateAsync(ResourceOwnerPasswordValidationContext context)
        {
            try
            {
                UserModel userModel = null;
                var result = _authorizationService.VerifyPassword(context.UserName, context.Password, ref userModel);
                if (result)
                {
                    if (userModel != null)
                    {
                        context.Result = new GrantValidationResult(
                            subject: userModel.Id.ToString(),
                            authenticationMethod: "custom",
                            claims: ConfigIdentityServer.GetUserClaims(userModel, _roleService));
                    }
                    // var userModel = _authorizationService.GetUser(context.UserName);
                    else context.Result = new GrantValidationResult(TokenRequestErrors.InvalidGrant, "User does not exist.");
                }
                else
                {
                    context.Result = new GrantValidationResult(TokenRequestErrors.InvalidGrant, "Incorrect password");
                }
            }
            catch (Exception ex)
            {
                context.Result = new GrantValidationResult(TokenRequestErrors.InvalidRequest, ex.Message);
            }
        }
    }
}
