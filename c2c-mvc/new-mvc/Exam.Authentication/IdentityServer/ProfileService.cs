﻿using Exam.CoreData.Enums;
using Exam.Services.RoleFacade;
using Exam.Services.UserFacade;
using IdentityServer4.Models;
using IdentityServer4.Services;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace Exam.Authentication.IdentityServer
{
    public class ProfileService : IProfileService
    {
        private readonly IAuthorizationService _authorizationService;
        private readonly IRoleService _roleService;

        public ProfileService(IAuthorizationService authorizationService, IRoleService roleService)
        {
            _authorizationService = authorizationService;
            _roleService = roleService;
        }

        public async Task GetProfileDataAsync(ProfileDataRequestContext context)
        {
            try
            {
                if (!string.IsNullOrEmpty(context.Subject.Identity.Name))
                {
                    var user = await _authorizationService.GetByIdAsync(Convert.ToInt32(context.Subject.Identity.Name));

                    if (user != null)
                    {
                        var claims = ConfigIdentityServer.GetUserClaims(user, _roleService);
                        context.IssuedClaims = claims.Where(c => context.RequestedClaimTypes.Contains(c.Type)).ToList();
                    }
                }
                else
                {
                    var userId = context.Subject.Claims.FirstOrDefault(c => c.Type == "sub");
                    if (!string.IsNullOrEmpty(userId?.Value) && long.Parse(userId.Value) > 0)
                    {
                        var user = await _authorizationService.GetByIdAsync(int.Parse(userId.Value));

                        if (user != null)
                        {
                            var claims = ConfigIdentityServer.GetUserClaims(user, _roleService);
                            context.IssuedClaims = claims.ToList();
                        }
                    }
                }
            }
            catch (Exception)
            {

                throw;
            }
        }

        public async Task IsActiveAsync(IsActiveContext context)
        {
            try
            {
                var userId = context.Subject.Claims.FirstOrDefault(c => c.Type == "user_id");
                if (!string.IsNullOrEmpty(userId?.Value) && int.Parse(userId.Value) > 0)
                {
                    var user = await _authorizationService.GetByIdAsync(int.Parse(userId.Value));
                    if (user != null)
                    {
                        context.IsActive = user.Status == UserStatus.Actived ? true : false;
                    }
                }
            }
            catch (Exception ex)
            {

                throw;
            }
        }
    }
}
