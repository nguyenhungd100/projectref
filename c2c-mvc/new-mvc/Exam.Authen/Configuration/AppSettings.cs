﻿namespace Exam.Authen.Configuration
{
    public class AppSettings
    {
        public string ApiServer { get; set; }

        public string AuthenticationServer { get; set; }

        public string[] ClientAppRedirectUri { get; set; }
    }
}
