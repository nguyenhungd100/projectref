﻿using Exam.CoreData;
using Exam.CoreData.Data.Entities;
using Exam.CoreData.Entities;
using Exam.CoreData.Enums;
using Exam.CoreData.Models;
using Exam.CoreData.Models.Images;
using Exam.CoreData.Models.PagingInfo;
using Exam.CoreData.Models.Posts;
using Exam.CoreData.Repository;
using Exam.Libraries.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace Exam.Services.PostFacade.Implement
{
    public class PostService : IPostService
    {
        private readonly IRepository<Post, ExamDBContext> _postRepository;
        private readonly IRepository<PostImage, ExamDBContext> _postImageRepository;
        private readonly IRepository<User, ExamDBContext> _userRepository;

        public PostService(IRepository<Post, ExamDBContext> postRepository,
            IRepository<PostImage, ExamDBContext> postImageRepository,
            IRepository<User, ExamDBContext> userRepository)
        {
            _postRepository = postRepository;
            _postImageRepository = postImageRepository;
            _userRepository = userRepository;
        }

        public BaseSearchResult<PostModel> SearchPost(SearchPostModel model)
        {
            model.SortDesc = true;
            Expression<Func<Post, dynamic>> expressionOrder = c => c.CreatedDate;
            if (model.OrderByLowestPrice == true)
            {
                model.SortDesc = false;
                expressionOrder = c => c.Price;
            }

            Expression<Func<Post, bool>> expressionWhere = c => c.Id > 0;

            //if (model.ObjectOrientedId.HasValue)
            //{
            //    expressionWhere = c => c.ObjectOrientedId == model.ObjectOrientedId.Value;
            //}


            var result = new BaseSearchResult<PostModel>();

            var entities = _postRepository.FinAllPaging(
                new SearchModel { PageIndex = model.PageIndex, PageSize = model.PageSize },
                c => c.Id > 0, null
                );
            return new BaseSearchResult<PostModel>
            {
                Records = entities.Records?.CloneToListModels<Post, PostModel>(),
                TotalRecord = entities.TotalRecord,
                PageIndex = model.PageIndex,
                PageSize = model.PageSize
            };
        }

        public PostModel GetPostById(int id)
        {
            var result = new PostModel();
            var post = _postRepository.FindById(id);
            if (post == null) throw new ServiceException("Tin bài không tồn tại");
            result = post?.CloneToModel<Post, PostModel>();
            var userCreated = _userRepository.FindById(result.CreatedBy);
            result.MobileUser = string.IsNullOrEmpty(userCreated.Mobile) ? userCreated.Email : userCreated.Mobile;
            result.PostImages = from p in _postImageRepository.FindAll(c => c.PostId == post.Id)
                                select new PostImageModel
                                {
                                    Id = p.Id,
                                    StringBase64 = p.StringBase64,
                                    CreatedDate = p.CreatedDate,
                                    Description = p.Description
                                };
            return result;
        }

        private bool UpdatePostImage(int postId, IEnumerable<PostImageModel> postImageModels)
        {
            var result = true;
            var images = _postImageRepository.FindAll(c => c.PostId == postId);
            var imageIds = images.Select(c => c.Id);

            foreach (var imageId in imageIds)
            {
                if (!postImageModels.Select(c => c.Id).Contains(imageId))
                {
                    result = _postImageRepository.Delete(images.First(a => a.Id == imageId));
                    if (!result)
                        return result;
                }
            }
            var addImages = postImageModels.Where(c => c.StringBase64.Length > 0);
            foreach (var item in addImages)
            {
                result = _postImageRepository.Insert(new PostImage
                {
                    StringBase64 = item.StringBase64,
                    CreatedDate = item.CreatedDate,
                    Description = item.Description
                });
                if (!result)
                    return result;
            }
            return result;
        }


        public bool SavePost(SavePostModel model)
        {
            var result = false;
            using (var context = _postRepository.GetDBContext())
            {
                using (var trans = context.Database.BeginTransaction())
                {
                    try
                    {
                        var entity = model?.CloneToModel<SavePostModel, Post>();
                        entity.CreatedDate = DateTime.UtcNow;
                        if (model.Id.HasValue)
                        {
                            var exist = _postRepository.FindById(model.Id);
                            if (exist == null)
                            {
                                throw new ServiceException("Tin bài không tồn tại");
                            }
                            exist.CategoryId = model.CategoryId;
                            exist.Name = model.Name;
                            exist.Description = model.Description;
                            exist.Content = model.Content;
                            exist.Status = model.Status;
                            result = _postRepository.Update(exist);
                            if (result)
                            {
                                result = UpdatePostImage(model.Id.Value, model.PostImages);
                            }
                            else return false;
                        }
                        else
                        {
                            entity.CreatedBy = model.CreatedBy;
                            result = _postRepository.Insert(entity);

                            if (result)
                            {
                                foreach (var item in model.PostImages)
                                {
                                    result = _postImageRepository.Insert(
                                        new PostImage
                                        {
                                            PostId = entity.Id,
                                            Description = item.Description,
                                            CreatedDate = DateTime.UtcNow,
                                            StringBase64 = item.StringBase64
                                        });
                                    if (!result)
                                        break;
                                }
                            }
                            else
                            {
                                trans.Rollback();
                                return false;
                            }
                        }
                        if (result)
                        {
                            context.SaveChanges();
                            trans.Commit();
                        }
                    }
                    catch (Exception ex)
                    {
                        trans.Rollback();
                        throw;
                    }
                }
            }
            return result;
        }

        public bool DeleteManyPost(DeleteGenericModel model)
        {
            var result = false;
            if (model.Ids.Count() == 0)
                throw new ServiceException("Please select at least 1 department");
            using (var context = _postRepository.GetDBContext())
            {
                using (var trans = context.Database.BeginTransaction())
                {
                    try
                    {
                        foreach (var item in model.Ids)
                        {
                            result = _postRepository.Delete(new Post { Id = item });
                            if (!result)
                            {
                                trans.Rollback();
                                break;
                            }
                        }
                        if (result) trans.Commit();
                    }
                    catch (Exception)
                    {
                        trans.Rollback();
                    }
                }
            }
            return result;
        }

        public bool ApprovePost(int id, int approver)
        {
            var post = _postRepository.FindById(id);
            if (post == null)
                throw new ServiceException("Bài đăng không tồn tại");
            post.ApproverId = approver;
            post.ApproveDate = DateTime.UtcNow;
            post.Status = PostStatus.Approve;
            return _postRepository.Update(post);
        }

        public bool RejectPost(int id, int approver)
        {
            var post = _postRepository.FindById(id);
            if (post == null)
                throw new ServiceException("Bài đăng không tồn tại");
            post.ApproverId = approver;
            post.ApproveDate = DateTime.UtcNow;
            post.Status = PostStatus.Reject;
            return _postRepository.Update(post);
        }
    }
}
