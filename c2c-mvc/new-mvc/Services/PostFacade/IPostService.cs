﻿using Exam.CoreData.Models;
using Exam.CoreData.Models.PagingInfo;
using Exam.CoreData.Models.Posts;

namespace Exam.Services.PostFacade
{
    public interface IPostService
    {
        BaseSearchResult<PostModel> SearchPost(SearchPostModel model);
        PostModel GetPostById(int id);
        bool SavePost(SavePostModel model);
        bool DeleteManyPost(DeleteGenericModel model);
        bool ApprovePost(int id, int approver);
        bool RejectPost(int id, int approver);
    }
}
