﻿namespace Exam.Services.RoleFacade
{
    public enum PermissionGroupCode
    {
        SYSTEM = 1,
        NONE
    }

    public enum PermissionCode
    {
        [PermissionDetail(PermissionGroupCode.SYSTEM, "Quản lý người dùng")]
        MANAGER_USERS = 1,
        [PermissionDetail(PermissionGroupCode.SYSTEM, "Quản lý vai trò")]
        MANAGER_ROLES = 5,

        [PermissionDetail(PermissionGroupCode.NONE, "Đăng tin bài")]
        CREATE_POST = 50,

        [PermissionDetail(PermissionGroupCode.NONE, "Duyệt tin bài")]
        APPROVEPOST = 101,
        [PermissionDetail(PermissionGroupCode.NONE, "Từ chối tin bài")]
        REJECTPOST = 105,
    }
}
