﻿using Exam.CoreData.Models.Roles;
using System.Collections.Generic;

namespace Exam.Services.RoleFacade
{
    public interface IRoleService
    {
        List<RoleModel> List(int[] roleIds);
        List<PermissionGroup> ListPermission();
    }
}
