﻿using Exam.CoreData;
using Exam.CoreData.Data.Entities;
using Exam.CoreData.Models.Roles;
using Exam.CoreData.Repository;
using Exam.Libraries.Utils;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Exam.Services.RoleFacade.Implement
{
    public class RoleService : IRoleService
    {
        private readonly IRepository<Role, ExamDBContext> _roleRepository;

        public RoleService()
        {
        }

        public RoleService(IRepository<Role, ExamDBContext> roleRepository)
        {
            _roleRepository = roleRepository;
        }

        public List<RoleModel> List(int[] roleIds)
        {
            var role = _roleRepository.FindAll(c => roleIds.Contains(c.Id));
            return role.ToList().CloneToListModels<Role, RoleModel>();
        }

        public List<PermissionGroup> ListPermission()
        {
            var groups = new List<PermissionGroup>();
            var groupCodes = Enum.GetValues(typeof(PermissionGroupCode)).Cast<PermissionGroupCode>().ToList();
            foreach (var groupCode in groupCodes)
            {
                groups.Add(new PermissionGroup
                {
                    GroupId = (int)groupCode,
                    Name = groupCode.ToString()
                });
            }

            var permissions = new List<Permission>();
            foreach (var p in Enum.GetValues(typeof(PermissionCode)).Cast<PermissionCode>())
            {
                var attr = p.GetAttributeOfType<PermissionDetailAttribute>();
                permissions.Add(new Permission
                {
                    Code = p,
                    Name = attr.Name,
                    GroupId = (int)attr.GroupCode
                });
            }

            foreach (var group in groups)
            {
                var groupPermissions = permissions.Where(c => c.GroupId == group.GroupId);
                group.Permissions = new List<Permission>();
                group.Permissions.AddRange(groupPermissions);
            }
            return groups;
        }
    }
}
