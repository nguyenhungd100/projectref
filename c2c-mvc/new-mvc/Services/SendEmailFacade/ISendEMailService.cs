﻿namespace Exam.Services.SendEmailFacade
{
    public interface ISendEmailService
    {
        bool SendOTPVerifyAccount(string email, string code);
    }
}
