﻿using Exam.CoreData;
using Exam.CoreData.Data.Entities;
using Exam.CoreData.Models.Districts;
using Exam.CoreData.Models.Provinces;
using Exam.CoreData.Repository;
using Exam.Libraries.Utils;
using Microsoft.AspNetCore.Http;
using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Exam.Services.ProvinceAndDistrictFacade.Implemenet
{
    public class ProvinceAndDistrictService : IProvinceAndDistrictService
    {
        private readonly IRepository<Province, ExamDBContext> _provinceRepository;
        private readonly IRepository<District, ExamDBContext> _districtRepository;

        public ProvinceAndDistrictService(IRepository<Province, ExamDBContext> provinceRepository,
            IRepository<District, ExamDBContext> districtRepository)
        {
            _provinceRepository = provinceRepository;
            _districtRepository = districtRepository;
        }

        private ExcelRange ColumExcelContainString(ExcelWorksheet worksheet, int row, string inputString)
        {
            return worksheet.Cells[row, worksheet.Cells.First(c => c.Text.ToString().Contains(inputString)).Start.Column];
        }

        public bool ImportDistrict(IFormFile file)
        {
            var stream = file.OpenReadStream();
            var package = new ExcelPackage(stream);
            var worksheet = package.Workbook.Worksheets[0];

            int totalRows = worksheet.Dimension.End.Row;

            var provinces = _provinceRepository.FindAll();
            int id = 1;
            var entities = new List<District>();
            for (int i = 2; i <= totalRows; i++)
            {
                var entity = new District();
                if (ColumExcelContainString(worksheet, i, "Mã").Text.Length > 0)
                {
                    entity.Id = int.Parse(ColumExcelContainString(worksheet, i, "Mã").Text.ToString());
                }
                if (ColumExcelContainString(worksheet, i, "Tên").Text.Length > 0)
                {
                    entity.Name = ColumExcelContainString(worksheet, i, "Tên").Text.ToString();
                }
                if (ColumExcelContainString(worksheet, i, "Cấp").Text.Length > 0)
                {
                    entity.Level = ColumExcelContainString(worksheet, i, "Cấp").Text.ToString();
                }
                if (ColumExcelContainString(worksheet, i, "Tỉnh / Thành Phố").Text.Length > 0)
                {
                    var nameProvince = ColumExcelContainString(worksheet, i, "Tỉnh / Thành Phố").Text.ToString();
                    if (provinces.Select(c => c.Name).Contains(nameProvince))
                    {
                        var provinceChosen = provinces.FirstOrDefault(c => c.Name == nameProvince);
                        entity.ProvinceId = provinceChosen.Id;
                    }
                }
                entity.Id = id;
                entity.SortOrder = entity.Id;
                entities.Add(entity);
                id++;
            }
            foreach (var item in entities)
            {
                _districtRepository.Insert(item);
            }
            return true;
        }

        public bool ImportProvinces(IFormFile file)
        {
            var stream = file.OpenReadStream();
            var package = new ExcelPackage(stream);
            var worksheet = package.Workbook.Worksheets[0];

            int totalRows = worksheet.Dimension.End.Row;

            int id = 1;
            var entities = new List<Province>();
            for (int i = 2; i <= totalRows; i++)
            {
                var entity = new Province();
                if (ColumExcelContainString(worksheet, i, "Mã").Text.Length > 0)
                {
                    entity.Id = int.Parse(ColumExcelContainString(worksheet, i, "Mã").Text.ToString());
                }
                if (ColumExcelContainString(worksheet, i, "Tên").Text.Length > 0)
                {
                    entity.Name = ColumExcelContainString(worksheet, i, "Tên").Text.ToString();
                }
                if (ColumExcelContainString(worksheet, i, "Cấp").Text.Length > 0)
                {
                    entity.Level = ColumExcelContainString(worksheet, i, "Cấp").Text.ToString();
                }
                entity.Id = id;
                entity.SortOrder = entity.Id;
                entities.Add(entity);
                id++;
            }
            foreach (var item in entities)
            {
                _provinceRepository.Insert(item);
            }
            return true;
        }

        public IEnumerable<ProvinceModel> GetAllProvinces()
        {
            var provinces = _provinceRepository.FindAll();
            return provinces.ToList()?.CloneToListModels<Province, ProvinceModel>();
        }

        public IEnumerable<DistrictModel> GetAllDistricts()
        {
            var districts = _districtRepository.FindAll();
            return districts.ToList()?.CloneToListModels<District, DistrictModel>();
        }

        public IEnumerable<DistrictModel> GetDistrictGroupsWithProvince(int provinceId)
        {
            var province = _provinceRepository.FindById(provinceId);
            if (province == null)
            {
                throw new ServiceException("Tỉnh thành này không tồn tại");
            }
            var districs = _districtRepository.FindAll(c => c.ProvinceId == provinceId);
            return from d in districs
                   select new DistrictModel
                   {
                       Id = d.Id,
                       Name = d.Name,
                       ProvinceId = province.Id,
                       ProvinceName = province.Name,
                       Level = d.Level,
                       SortOrder = d.SortOrder
                   };

        }
    }
}
