﻿using Exam.CoreData.Models.Districts;
using Exam.CoreData.Models.Provinces;
using Microsoft.AspNetCore.Http;
using System.Collections.Generic;

namespace Exam.Services.ProvinceAndDistrictFacade
{
    public interface IProvinceAndDistrictService
    {
        bool ImportDistrict(IFormFile file);
        bool ImportProvinces(IFormFile file);
        IEnumerable<ProvinceModel> GetAllProvinces();
        IEnumerable<DistrictModel> GetAllDistricts();
        IEnumerable<DistrictModel> GetDistrictGroupsWithProvince(int provinceId);
    }
}
