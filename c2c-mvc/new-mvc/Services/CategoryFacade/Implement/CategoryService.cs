﻿using Exam.CoreData;
using Exam.CoreData.Data.Entities;
using Exam.CoreData.Models.Categories;
using Exam.CoreData.Models.ObjectOrienteds;
using Exam.CoreData.Repository;
using Exam.Libraries.Utils;
using System.Collections.Generic;
using System.Linq;

namespace Exam.Services.CategoryFacade.Implement
{
    public class CategoryService : ICategoryService
    {
        private readonly IRepository<Category, ExamDBContext> _categoryRepository;
        private readonly IRepository<ObjectType, ExamDBContext> _objectTypeRepository;

        public CategoryService(IRepository<Category, ExamDBContext> categoryRepository,
            IRepository<ObjectType, ExamDBContext> objectTypeRepository)
        {
            _categoryRepository = categoryRepository;
            _objectTypeRepository = objectTypeRepository;
        }

        public IEnumerable<CategoryModel> GetCategories()
        {
            var result = new List<CategoryModel>();
            var parents = _categoryRepository.FindAll(c => c.ParentId == null);
            var parentIds = parents.Select(c => c.Id).ToList();
            var childrens = _categoryRepository.FindAll(c => c.ParentId != null && parentIds.Contains(c.ParentId.Value));
            return (from p in parents
                    select new CategoryModel
                    {
                        Id = p.Id,
                        Name = p.Name,
                        Description = p.Description,
                        ParentId = null,
                        DisplayOrder = p.DisplayOrder,
                        Status = p.Status,
                        CategoryChildrens = childrens.Where(c => c.ParentId == p.Id).ToList()
                          ?.CloneToListModels<Category, CategoryModel>()
                    }).OrderBy(c => c.DisplayOrder);
        }

        public IEnumerable<ObjectTypeModel> GetObjectTypes(int categoryId)
        {
            var objects = _objectTypeRepository.FindAll(c => c.CategoryId == categoryId)?.OrderBy(c => c.DisplayOrder);
            foreach (var item in objects)
            {
                yield return new ObjectTypeModel
                {
                    Id = item.Id,
                    Name = item.Name,
                    CategoryId = item.CategoryId,
                    DisplayOrder = item.DisplayOrder,
                };
            }

        }
    }
}
