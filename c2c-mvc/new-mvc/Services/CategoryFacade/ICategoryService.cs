﻿using Exam.CoreData.Models.Categories;
using Exam.CoreData.Models.ObjectOrienteds;
using System.Collections.Generic;

namespace Exam.Services.CategoryFacade
{
    public interface ICategoryService
    {
        IEnumerable<CategoryModel> GetCategories();
        IEnumerable<ObjectTypeModel> GetObjectTypes(int categoryId);
    }
}
