﻿using Exam.CoreData;
using Exam.CoreData.Data.Entities;
using Exam.CoreData.Entities;
using Exam.CoreData.Enums;
using Exam.CoreData.Models.Users;
using Exam.CoreData.Repository;
using Exam.Libraries.Utils;
using Exam.Services;
using Exam.Services.SendEmailFacade;
using Microsoft.Extensions.Caching.Memory;
using System;
using System.Linq;

namespace Services.UserFacade.Implement
{
    public class UserService : IUserService
    {
        private readonly IMemoryCache _cache;
        private readonly IRepository<User, ExamDBContext> _userRepository;
        private readonly IRepository<Role, ExamDBContext> _roleRepository;
        private readonly ISendEmailService _sendMailService;

        public UserService(IMemoryCache cache,
            IRepository<User, ExamDBContext> userRepository,
            IRepository<Role, ExamDBContext> roleRepository,
            ISendEmailService sendMailService)
        {
            _cache = cache;
            _userRepository = userRepository;
            _roleRepository = roleRepository;
            _sendMailService = sendMailService;
        }

        public UserModel GetById(int userId)
        {
            var result = new UserModel();
            var entity = _userRepository.FindById(userId);
            var roleAll = entity.UserRoles.Split(new string[] { ";" }, StringSplitOptions.RemoveEmptyEntries)
                    .Select(c => Convert.ToInt32(c)).ToList();
            var roleMin = 100000;
            if (roleAll.Count() > 0)
                roleMin = roleAll.Min();
            var role = _roleRepository.FindById(roleMin);
            result = entity?.CloneToModel<User, UserModel>();
            if (role != null)
            {
                result.DisplayOrder = role.DisplayOrder ?? 100000;
            }
            return result;
        }

        public bool CreateUserByAdmin(CreateUserModel model)
        {
            if (_userRepository.FirstOrDefault(c => c.Mobile == model.Mobile) != null)
            {
                throw new ServiceException("Số điện thoại đã tồn tại");
            }

            if (_userRepository.FirstOrDefault(c => c.Email == model.Email) != null)
            {
                throw new ServiceException("Email đã tồn tại");
            }

            var user = new User
            {
                FullName = model.FullName,
                Password = EncryptHelper.HassPasswordBCrypt(model.Password),
                Mobile = model.Mobile,
                Email = model.Email,
                Gender = model.Gender,
                Status = UserStatus.Actived,
                UserRoles = model.UserRoles,
                DisplayOrder = 1,
            };
            return _userRepository.Insert(user);
        }

        public bool LockAccount(int userId)
        {
            var userExist = _userRepository.FindById(userId);
            if (userExist == null)
                throw new ServiceException("Tài khoản không tồn tại");
            userExist.Status = UserStatus.Disabled;
            return _userRepository.Update(userExist);
        }

        public bool CreateUserSide(CreateUserModel model)
        {
            var result = false;
            using (var context = _userRepository.GetDBContext())
            {
                using (var trans = context.Database.BeginTransaction())
                {
                    try
                    {
                        if (_userRepository.FirstOrDefault(c => c.Mobile == model.Mobile) != null)
                        {
                            throw new ServiceException("Số điện thoại đã tồn tại");
                        }

                        if (_userRepository.FirstOrDefault(c => c.Email == model.Email) != null)
                        {
                            throw new ServiceException("Email đã tồn tại");
                        }

                        var user = new User
                        {
                            FullName = model.FullName,
                            Password = EncryptHelper.HassPasswordBCrypt(model.Password),
                            Mobile = model.Mobile,
                            Email = model.Email,
                            Gender = model.Gender,
                            Status = UserStatus.Actived,
                            UserRoles = model.UserRoles,
                            DisplayOrder = 1,
                        };
                        result = _userRepository.Insert(user);
                        //if (result) result = SendOTPLoginToEmail(user.Email);

                        context.SaveChanges();
                        trans.Commit();
                    }
                    catch (Exception ex)
                    {
                        trans.Rollback();
                        throw;
                    }
                }
            }
            return result;
        }

        private bool SendOTPLoginToEmail(string email)
        {
            var result = false;
            var random = new Random();
            var code = random.Next(111111, 999999).ToString("D6");
            var statusSend = _sendMailService.SendOTPVerifyAccount(email, code);
            if (statusSend)
            {
                var cacheEntry = new MemoryCacheEntryOptions()
                    .SetSlidingExpiration(TimeSpan.FromMinutes(5));
                var otp = _cache.Set(code, email, cacheEntry);
                return result = true;
            }
            return result;
        }

        private bool VerifyOTPLogin(string email, string otp)
        {
            var userCache = _cache.Get(otp);
            if (userCache == null)
                throw new ServiceException("Mã xác nhận không tồn tại hoặc quá hạn 5 phút");
            else
            {
                if (userCache.ToString() == email)
                    return true;
                else
                    throw new ServiceException("Mã xác nhận không đúng");
            }
        }

        public bool ActiveAccount(VerifyOTPModel model)
        {
            var user = _userRepository.FirstOrDefault(c => c.Email == model.Email);
            if (user != null)
            {
                if (user.Status == UserStatus.Actived)
                {
                    throw new ServiceException("Tài khoản đã được kích hoạt");
                }
                if (VerifyOTPLogin(user.Email, model.OTP))
                {
                    user.Status = UserStatus.Actived;
                    return _userRepository.Update(user);
                }
                else throw new ServiceException("Tài khoản chưa được xác thực");
            }
            else throw new ServiceException("Tài khoản chưa đăng ký");
        }
    }
}
