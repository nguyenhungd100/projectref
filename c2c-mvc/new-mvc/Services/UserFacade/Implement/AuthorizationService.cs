﻿using Exam.CoreData;
using Exam.CoreData.Entities;
using Exam.CoreData.Enums;
using Exam.CoreData.Models.Users;
using Exam.CoreData.Repository;
using Exam.Libraries.Utils;
using System.Threading.Tasks;

namespace Exam.Services.UserFacade.Implement
{
    public class AuthorizationService : IAuthorizationService
    {
        private readonly IRepository<User, ExamDBContext> _userRepository;

        public AuthorizationService(IRepository<User, ExamDBContext> userRepository)
        {
            _userRepository = userRepository;
        }

        public async Task<UserModel> GetByIdAsync(int userId)
        {
            var entity = await _userRepository.FindByIdAsync(userId);
            return await Task.FromResult(entity.CloneToModel<User, UserModel>());
        }

        public UserModel GetUser(string userName)
        {
            var entity = _userRepository.FirstOrDefault(c => c.Email == userName);
            return entity.CloneToModel<User, UserModel>();
        }

        public bool VerifyPassword(string userName, string password, ref UserModel userModel)
        {
            var user = _userRepository.FirstOrDefault(c => c.FullName == userName || c.Email == userName);
            if (user == null || user.Status != UserStatus.Actived) return false;
            if (EncryptHelper.VerifyPasswordBCrypt(password, user.Password))
            {
                userModel = user.CloneToModel<User, UserModel>();
                return true;
            }
            userModel = null;
            return false;
        }
    }
}
