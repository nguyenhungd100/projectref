﻿using Exam.CoreData.Models.Users;

namespace Services.UserFacade
{
    public interface IUserService
    {
        UserModel GetById(int userId);
        bool CreateUserByAdmin(CreateUserModel model);
        bool LockAccount(int userId);
        bool CreateUserSide(CreateUserModel model);
        bool ActiveAccount(VerifyOTPModel model);
    }
}
