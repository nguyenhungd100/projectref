﻿using Exam.CoreData.Models.Users;
using System.Threading.Tasks;

namespace Exam.Services.UserFacade
{
    public interface IAuthorizationService
    {
        Task<UserModel> GetByIdAsync(int userId);
        bool VerifyPassword(string userName, string password, ref UserModel userModel);
        UserModel GetUser(string userName);
    }
}
