﻿using Exam.CoreData.Models.Accounts;

namespace Exam.Services.AccountFacade
{
    public interface IAccountService
    {
        AccountModel GetById(string userName);

        bool CheckLogin(AccountModel model);
    }
}
