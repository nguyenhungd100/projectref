﻿using Exam.CoreData;
using Exam.CoreData.Data.Entities;
using Exam.CoreData.Repository;

namespace Exam.Services.ProductFacade.Implement
{
    public class ProductService : IProductService
    {
        private readonly IRepository<Product, ExamDBContext> _productRepository;

        public ProductService(IRepository<Product, ExamDBContext> productRepository)
        {
            _productRepository = productRepository;
        }
    }
}
