﻿namespace Exam.CoreData.MvcModels.Tags
{
    public class TagModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int Type { get; set; }
    }
}
