﻿using System.Collections.Generic;

namespace Exam.CoreData.Models.Departments
{
    public class DeleteManyDepartmentModel
    {
        public List<int> Ids { get; set; }
    }
}
