﻿using System;

namespace Exam.CoreData.Models.Images
{
    public class PostImageModel
    {
        public int Id { get; set; }
        public string StringBase64 { get; set; }
        public DateTime CreatedDate { get; set; }
        public string Description { get; set; }
        public string Mime { get; set; }
    }
}
