﻿using Exam.CoreData.Enums;
using Newtonsoft.Json;

namespace Exam.CoreData.Models.Users
{
    public class UserModel
    {
        public int Id { get; set; }

        public string FullName { get; set; }

        public string Email { get; set; }

        public UserStatus Status { get; set; }

        public string Mobile { get; set; }

        public int? Gender { get; set; }

        public string UserRoles { get; set; }

        public string Permissions { get; set; }

        public int? DisplayOrder { get; set; }

        [JsonIgnore]
        public bool IsUserSide { get; set; }

    }
}
