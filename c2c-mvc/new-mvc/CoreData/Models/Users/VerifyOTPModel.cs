﻿using System.ComponentModel.DataAnnotations;

namespace Exam.CoreData.Models.Users
{
    public class VerifyOTPModel
    {
        [Required]
        public string Email { get; set; }
        public string OTP { get; set; }
    }
}
