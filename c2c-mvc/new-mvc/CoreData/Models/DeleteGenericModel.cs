﻿using System.Collections.Generic;

namespace Exam.CoreData.Models
{
    public class DeleteGenericModel
    {
        public List<int> Ids { get; set; }
    }
}
