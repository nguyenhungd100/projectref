﻿using Exam.CoreData.Enums;
using Exam.CoreData.Models.Images;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace Exam.CoreData.Models.Posts
{
    public class SavePostModel
    {
        public int? Id { get; set; }
        public int CategoryId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string Content { get; set; }

        public decimal Price { get; set; }
        public int DistrictId { get; set; }
        public int ProvinceId { get; set; }
        [JsonIgnore]
        public int CreatedBy { get; set; }
        [JsonIgnore]
        public DateTime CreatedDate { get; set; }
        [JsonIgnore]
        public int? ApproverId { get; set; }
        [JsonIgnore]
        public DateTime? ApproveDate { get; set; }
        public PostStatus Status { get; set; }
        public IEnumerable<PostImageModel> PostImages { get; set; }
    }
}
