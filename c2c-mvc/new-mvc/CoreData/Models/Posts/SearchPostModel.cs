﻿using Exam.CoreData.Models.PagingInfo;

namespace Exam.CoreData.Models.Posts
{
    public class SearchPostModel : BaseSearchModel
    {
        public int CategoryId { get; set; }

        public bool OrderByLowestPrice { get; set; } = false;

        //public int? ObjectOrientedId { get; set; }
    }
}
