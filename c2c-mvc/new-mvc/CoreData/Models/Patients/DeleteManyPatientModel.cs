﻿using System.Collections.Generic;

namespace Exam.CoreData.Models.Patients
{
    public class DeleteManyPatientModel
    {
        public List<string> Ids { get; set; }
    }
}
