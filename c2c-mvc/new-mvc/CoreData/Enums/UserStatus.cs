﻿namespace Exam.CoreData.Enums
{
    public enum UserStatus
    {
        NotActived = 1,

        Actived = 2,

        Disabled = 3
    }
}
