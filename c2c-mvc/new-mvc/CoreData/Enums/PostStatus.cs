﻿namespace Exam.CoreData.Enums
{
    public enum PostStatus
    {
        Init = 1,
        Approve = 5,
        Reject = 10
    }
}
