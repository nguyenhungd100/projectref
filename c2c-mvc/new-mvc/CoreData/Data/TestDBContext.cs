﻿using Exam.CoreData.Data.Entities2;
using Microsoft.EntityFrameworkCore;

namespace Exam.CoreData.Data
{
    public partial class TestDBContext : DbContext
    {
        public TestDBContext(DbContextOptions<TestDBContext> options) : base(options)
        {
        }
        public virtual DbSet<TestTable2> TestTables { get; set; }



        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {

            modelBuilder.Entity<TestTable2>(entity => entity.ToTable("TestTable1"));

            base.OnModelCreating(modelBuilder);
            //modelBuilder.Entity<User>(entity => { entity.ToTable("Users", "OtherSchema"); });
        }
    }
}
