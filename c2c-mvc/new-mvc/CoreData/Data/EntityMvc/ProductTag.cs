﻿namespace Exam.CoreData.Data.EntityMvc
{
    public class ProductTag : BaseEntity
    {
        public int ProductId { get; set; }
        public int TagId { get; set; }
    }
}
