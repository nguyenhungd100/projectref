﻿using Exam.CoreData.Enums.MvcStatus;
using System;

namespace Exam.CoreData.Data.EntityMvc
{
    public class ProductCategory : BaseEntity
    {
        public string Name { get; set; }
        public string Alias { get; set; }
        public int? ParentId { get; set; }
        public string Seo { get; set; }
        public DateTime? CreatedDate { get; set; }
        public int DisplayOrder { get; set; }
        public ProductCategoryStatus Status { get; set; }
    }
}
