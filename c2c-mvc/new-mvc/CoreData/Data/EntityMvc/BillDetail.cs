﻿namespace Exam.CoreData.Data.EntityMvc
{
    public class BillDetail : BaseEntity
    {
        public int BillId { get; set; }
        public int ProductId { get; set; }
        public int Quantity { get; set; }
        public decimal Price { get; set; }
    }
}
