﻿namespace Exam.CoreData.Data.EntityMvc
{
    public class Role : BaseEntity
    {
        public string RoleName { get; set; }

        public string Title { get; set; }

        public string Description { get; set; }

        public string Permissions { get; set; }

        public int? DisplayOrder { get; set; }
    }
}
