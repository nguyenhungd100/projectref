﻿namespace Exam.CoreData.Data.EntityMvc
{
    public class BlogTag : BaseEntity
    {
        public int BlogId { get; set; }
        public int TagId { get; set; }
    }
}
