﻿namespace Exam.CoreData.Data.EntityMvc
{
    public class Tag : BaseEntity
    {
        public string Name { get; set; }
        public int Type { get; set; }
    }
}
