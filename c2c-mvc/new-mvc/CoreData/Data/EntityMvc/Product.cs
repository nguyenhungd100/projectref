﻿using Exam.CoreData.Enums.MvcStatus;
using System;

namespace Exam.CoreData.Data.EntityMvc
{
    public class Product : BaseEntity
    {
        public int CategoryId { get; set; }
        public string Name { get; set; }
        public string Alias { get; set; }
        public decimal? Price { get; set; }
        public decimal? PromotionPrice { get; set; }
        public int? Quantity { get; set; }
        public string Description { get; set; }
        public string Content { get; set; }
        public string HotFlag { get; set; }
        public string NewFlg { get; set; }
        public string Image { get; set; }
        public DateTime CreatedDate { get; set; }
        public ProductStatus Status { get; set; }
    }
}
