﻿using Exam.CoreData.Enums.MvcStatus;

namespace Exam.CoreData.Data.EntityMvc
{
    public class Blog : BaseEntity
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Seo { get; set; }
        public string Description { get; set; }
        public string Content { get; set; }
        public string Image { get; set; }
        public BlogStatus Status { get; set; }
    }
}
