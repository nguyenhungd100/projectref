﻿using Exam.CoreData.Enums.MvcStatus;
using System;

namespace Exam.CoreData.Data.EntityMvc
{
    public class Bill
    {
        public int CustomerId { get; set; }
        public string CustomerName { get; set; }
        public string CustomerPhone { get; set; }
        public string CustomerEmail { get; set; }
        public DateTime BillDate { get; set; }
        public BillStatus Status { get; set; }
    }
}
