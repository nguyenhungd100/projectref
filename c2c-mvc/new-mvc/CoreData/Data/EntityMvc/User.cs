﻿using Exam.CoreData.Enums;

namespace Exam.CoreData.Data.EntityMvc
{
    public class User : BaseEntity
    {
        public string FullName { get; set; }

        public string Email { get; set; }

        public UserStatus Status { get; set; }

        public string Password { get; set; }

        public string Mobile { get; set; }

        public int? Gender { get; set; }

        public string UserRoles { get; set; }

        public int? DisplayOrder { get; set; }

    }
}
