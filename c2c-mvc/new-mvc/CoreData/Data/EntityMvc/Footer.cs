﻿namespace Exam.CoreData.Data.EntityMvc
{
    public class Footer : BaseEntity
    {
        public string Content { get; set; }
    }
}
