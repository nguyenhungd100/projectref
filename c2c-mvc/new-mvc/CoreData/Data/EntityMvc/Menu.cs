﻿using Exam.CoreData.Enums.MvcStatus;

namespace Exam.CoreData.Data.EntityMvc
{
    public class Menu : BaseEntity
    {
        public int Name { get; set; }
        public string Url { get; set; }
        public string Css { get; set; }
        public int? ParentId { get; set; }
        public int DisplayOrder { get; set; }
        public MenuStatus Status { get; set; }
    }
}
