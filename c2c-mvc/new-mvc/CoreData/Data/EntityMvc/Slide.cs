﻿using Exam.CoreData.Enums.MvcStatus;

namespace Exam.CoreData.Data.EntityMvc
{
    public class Slide : BaseEntity
    {
        public string Name { get; set; }
        public string Url { get; set; }
        public string Image { get; set; }
        public string Text { get; set; }
        public int DisplayOrder { get; set; }
        public SlideStatus Status { get; set; }
    }
}
