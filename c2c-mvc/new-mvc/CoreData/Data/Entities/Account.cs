﻿using System.ComponentModel.DataAnnotations;

namespace Exam.CoreData.Data.Entities
{
    public class Account
    {
        [Key]
        public string UserName { get; set; }

        public string Password { get; set; }
    }
}
