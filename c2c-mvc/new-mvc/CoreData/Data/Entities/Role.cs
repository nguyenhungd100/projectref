﻿namespace Exam.CoreData.Data.Entities
{
    public class Role
    {
        public int Id { get; set; }

        public string RoleName { get; set; }

        public string Title { get; set; }

        public string Description { get; set; }

        public string Permissions { get; set; }

        public int? DisplayOrder { get; set; }
    }
}
