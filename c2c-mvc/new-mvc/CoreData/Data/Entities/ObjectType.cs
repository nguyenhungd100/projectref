﻿using Exam.CoreData.Entities;

namespace Exam.CoreData.Data.Entities
{
    public class ObjectType : BaseEntity
    {
        public int CategoryId { get; set; }

        public string Name { get; set; }
        public int DisplayOrder { get; set; }
    }
}
