﻿namespace Exam.CoreData.Data.Entities
{
    public class Province
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string Level { get; set; }

        public int SortOrder { get; set; }
    }
}
