﻿using Exam.CoreData.Entities;
using System;

namespace Exam.CoreData.Data.Entities
{
    public class PostImage : BaseEntity
    {
        public int PostId { get; set; }
        public string Description { get; set; }
        public DateTime CreatedDate { get; set; }
        public string StringBase64 { get; set; }
    }
}
