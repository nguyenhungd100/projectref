﻿using Exam.CoreData.Entities;

namespace Exam.CoreData.Data.Entities
{
    public class ImageLibrary : BaseEntity
    {
        public string UrlData { get; set; }
        public string Mime { get; set; }
    }
}
