﻿using System.ComponentModel.DataAnnotations;

namespace Exam.CoreData.Data.Entities2
{
    public class TestTable2
    {
        [Key]
        public long Id { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }
    }
}
