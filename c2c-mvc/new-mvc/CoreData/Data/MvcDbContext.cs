﻿using Exam.CoreData.Data.EntityMvc;
using Microsoft.EntityFrameworkCore;

namespace Exam.CoreData.Data
{
    public partial class MvcDbContext : DbContext
    {
        public MvcDbContext(DbContextOptions<MvcDbContext> options) : base(options)
        {
        }
        public virtual DbSet<BillDetail> BillDetails { get; set; }
        public virtual DbSet<Bill> Bills { get; set; }
        public virtual DbSet<Blog> Blogs { get; set; }
        public virtual DbSet<BlogTag> BlogTags { get; set; }
        public virtual DbSet<Footer> Footers { get; set; }
        public virtual DbSet<Menu> Menus { get; set; }
        public virtual DbSet<ProductCategory> ProductCategories { get; set; }
        public virtual DbSet<Product> Products { get; set; }
        public virtual DbSet<ProductTag> ProductTags { get; set; }
        public virtual DbSet<Role> Roles { get; set; }
        public virtual DbSet<Slide> Slides { get; set; }
        public virtual DbSet<Tag> Tags { get; set; }
        public virtual DbSet<User> Users { get; set; }


        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<BillDetail>(entity => entity.ToTable("BillDetails"));
            modelBuilder.Entity<Bill>(entity => entity.ToTable("Bills"));
            modelBuilder.Entity<Blog>(entity => entity.ToTable("Blogs"));
            modelBuilder.Entity<BlogTag>(entity => entity.ToTable("BlogTags"));
            modelBuilder.Entity<Footer>(entity => entity.ToTable("Footers"));
            modelBuilder.Entity<Menu>(entity => entity.ToTable("Menus"));
            modelBuilder.Entity<ProductCategory>(entity => entity.ToTable("ProductCategories"));
            modelBuilder.Entity<Product>(entity => entity.ToTable("Products"));
            modelBuilder.Entity<ProductTag>(entity => entity.ToTable("ProductTags"));
            modelBuilder.Entity<Role>(entity => entity.ToTable("Roles"));
            modelBuilder.Entity<Slide>(entity => entity.ToTable("Slides"));
            modelBuilder.Entity<Tag>(entity => entity.ToTable("Tags"));
            modelBuilder.Entity<User>(entity => entity.ToTable("Users"));
            base.OnModelCreating(modelBuilder);
        }
    }
}

