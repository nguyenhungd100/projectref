﻿using Exam.CoreData.Data.Entities;
using Exam.CoreData.Entities;
using Microsoft.EntityFrameworkCore;

namespace Exam.CoreData
{
    public partial class ExamDBContext : DbContext
    {
        public ExamDBContext(DbContextOptions<ExamDBContext> options) : base(options)
        {
            // EntityFrameworkManager.ContextFactory = context => new ExamDBContext(options);
        }

        public virtual DbSet<User> Users { get; set; }
        public virtual DbSet<Role> Roles { get; set; }
        public virtual DbSet<District> Districts { get; set; }
        public virtual DbSet<Province> Provinces { get; set; }
        public virtual DbSet<Category> Categories { get; set; }
        public virtual DbSet<ImageLibrary> ImageLibraries { get; set; }
        public virtual DbSet<PostImage> PostImages { get; set; }
        public virtual DbSet<ObjectType> ObjectTypes { get; set; }
        public virtual DbSet<Post> Posts { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {


            modelBuilder.Entity<User>(entity => entity.ToTable("Users"));
            modelBuilder.Entity<Role>(entity => entity.ToTable("Roles"));
            modelBuilder.Entity<Province>(entity => entity.ToTable("Provinces"));
            modelBuilder.Entity<District>(entity => entity.ToTable("Districts"));
            modelBuilder.Entity<Category>(entity => entity.ToTable("Categories"));
            modelBuilder.Entity<ImageLibrary>(entity => entity.ToTable("ImageLibraries"));
            modelBuilder.Entity<PostImage>(entity => entity.ToTable("PostImages"));
            modelBuilder.Entity<ObjectType>(entity => entity.ToTable("ObjectTypes"));
            modelBuilder.Entity<Post>(entity => entity.ToTable("Posts"));
            base.OnModelCreating(modelBuilder);
        }
    }
}
