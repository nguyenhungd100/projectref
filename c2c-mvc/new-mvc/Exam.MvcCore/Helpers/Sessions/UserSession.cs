﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Exam.MvcCore.Helpers.Sessions
{
    public class UserSession
    {
        public string UserName { get; set; }
    }
}
