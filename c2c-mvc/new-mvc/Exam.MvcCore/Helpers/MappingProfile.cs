﻿using AutoMapper;
using Exam.CoreData.Data.EntityMvc;
using Exam.CoreData.MvcModels.Tags;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Exam.MvcCore.Helpers
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            CreateMap<TagModel, Tag>();
        }

    }
}
