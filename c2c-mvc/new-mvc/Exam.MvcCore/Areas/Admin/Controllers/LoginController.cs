﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Exam.CoreData.Models.Accounts;
using Exam.MvcCore.Helpers.Sessions;
using Exam.Services.AccountFacade;
using Microsoft.AspNetCore.Mvc;

namespace Exam.MvcCore.Areas.Admin.Controllers
{
    public class LoginController : Controller
    {
        private readonly IAccountService _accountService;
        private readonly ISessionHelper _sessionHelper;

        public LoginController(IAccountService accountService,
            ISessionHelper sessionHelper)
        {
            _accountService = accountService;
            _sessionHelper = sessionHelper;
        }

        [HttpGet]
        public IActionResult Index()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Index(AccountModel model)
        {
            //if (!ModelState.IsValid) throw new Exception(ModelState.GetErrorsMessage());
            var checkLogin = _accountService.CheckLogin(model);
            if (checkLogin)
            {
                _sessionHelper.SetSession("loginSession", new UserSession { UserName = model.UserName });
                return RedirectToAction("Index", "Home");
            }
            else
            {
                ModelState.AddModelError("", "Tên đăng nhập hoặc mật khẩu không tồn tại");
            }
            var ses = _sessionHelper.GetSession<UserSession>("loginSession");
            return View();
        }
    }
}