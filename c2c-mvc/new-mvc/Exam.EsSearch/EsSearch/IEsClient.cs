﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace Exam.EsSearch.EsSearch
{
    public interface IESClient<T> where T : class
    {
        bool Create(T document);
        Task<bool> CreateAsync(T document);
        bool CreateMany(IEnumerable<T> documents);
        Task<bool> CreateManyAsync(IEnumerable<T> documents);
        bool CreateIndexSettings(string fieldNameAnalyzer, IEnumerable<string> filterAnalyzer = null);
        bool CreateIndexSettingsAsync(string fieldNameAnalyzer, IEnumerable<string> filterAnalyzer = null);



        bool Update<T>(long id, dynamic updateDocument);
        Task<bool> UpdateAsync<T>(long id, dynamic updateDocument) where T : class;


        bool DeleteIndex(string nameIndex);
        bool DeleteMapping(T document);
        bool DeleteMapping<T>(int id) where T : class;
        bool DeleteMapping(IEnumerable<T> documents);
        bool DeleteMappingMany(IEnumerable<int> ids);

        T Get(T document);
        T Get(string id);
        T Get(long id);

    }
}
