﻿using Elasticsearch.Net;
using Nest;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Exam.EsSearch.EsSearch
{
    public sealed class ElasticContext<T> : ElasticClient, IESClient<T>,
        IDisposable where T : class
    {
        public ElasticContext()
        {

        }

        static ElasticContext()
        {
            GetSettings();
        }

        static ConnectionSettings GetSettings()
        {
            var connectionString = "http://localhost:9200/";
            var requestTimeout = 5;
            var nodes = new Uri[]
            {
                new Uri(connectionString)
            };

            var pool = new StaticConnectionPool(nodes);
            return new ConnectionSettings(pool).RequestTimeout(TimeSpan.FromSeconds(requestTimeout));

        }

        public bool Create(T document)
        {
            try
            {
                var stringId = typeof(T).Name.ToLower();
                var index = Index(document, i => i.Index(stringId).Refresh(Refresh.True));
                return index.IsValid;
            }
            catch
            {
                return false;
            }
        }

        public async Task<bool> CreateAsync(T document)
        {
            try
            {
                var stringId = typeof(T).Name;
                var index = await IndexAsync(document, i => i.Index(stringId).Refresh(Refresh.True));
                return index.IsValid;
            }
            catch
            {
                return false;
            }
        }

        public bool CreateMany(IEnumerable<T> documents)
        {
            try
            {
                var stringId = typeof(T).Name;
                var bulkIndexer = new BulkDescriptor();
                bulkIndexer.IndexMany(documents, (d, doc) => d.Document(doc).Index(stringId));

                var index = Bulk(bulkIndexer.Refresh(Refresh.True));
                return index.IsValid;
            }
            catch
            {
                return false;
            }
        }

        public async Task<bool> CreateManyAsync(IEnumerable<T> documents)
        {
            try
            {
                var stringId = typeof(T).Name;
                var bulkIndexer = new BulkDescriptor();
                bulkIndexer.IndexMany(documents, (d, doc) => d.Document(doc).Index(stringId));

                var index = await BulkAsync(bulkIndexer.Refresh(Refresh.True));
                return index.IsValid;
            }
            catch
            {
                return false;
            }
        }

        public bool CreateIndexSettings(string fieldNameAnalyzer, IEnumerable<string> filterAnalyzer = null)
        {
            throw new NotImplementedException();
        }

        public bool CreateIndexSettingsAsync(string fieldNameAnalyzer, IEnumerable<string> filterAnalyzer = null)
        {
            throw new NotImplementedException();
        }

        public bool DeleteIndex(string nameIndex)
        {
            return DeleteIndex(nameIndex);
        }

        public bool DeleteMapping(T document)
        {
            try
            {
                var stringId = typeof(T).Name;
                var response = Delete<T>(document, c => c.Index(stringId)
                    .Refresh(Refresh.True));
                return response.IsValid;
            }
            catch
            {
                return false;
            }
        }

        public bool DeleteMapping<T>(int id) where T : class
        {
            try
            {
                var stringId = typeof(T).Name.ToLower();
                var response = Delete<T>(id, i => i.Index(stringId).Refresh(Elasticsearch.Net.Refresh.True));
                return response.IsValid;
            }
            catch
            {
                return false;
            }
        }

        public bool DeleteMapping(IEnumerable<T> documents)
        {
            throw new NotImplementedException();
        }

        public bool DeleteMappingMany(IEnumerable<int> ids)
        {
            throw new NotImplementedException();
        }

        public bool Update(long id, dynamic updateDocument)
        {
            try
            {
                var stringId = typeof(T).Name;
                //var response = Update<T, dynamic>(id, c => c.Index(stringId)
                //    .Doc(updateDocument)
                //    .Refresh(Refresh.True));
                //return response.IsValid;
                return true;
            }
            catch
            {
                return false;
            }
        }

        public async Task<bool> UpdateAsync<T>(long id, dynamic updateDocument) where T : class
        {
            try
            {
                var stringId = typeof(T).Name;
                ///var response = await UpdateAsync<T, dynamic>(id, u => u.Index(stringId).Doc(updateDocument).Refresh(Elasticsearch.Net.Refresh.True));

                return true;
            }
            catch
            {
                return false;
            }
        }

        public void Dispose()
        {
            GC.SuppressFinalize(this);
        }

        public bool Update<T>(long id, dynamic updateDocument)
        {
            throw new NotImplementedException();
        }

        public new T Get(T document)
        {
            try
            {
                var stringId = typeof(T).Name.ToLower();
                var response = Get<T>(document, c => c.Index(stringId).Refresh());
                if (response.IsValid)
                {
                    return response.Source;
                }
                return default(T);
            }
            catch (Exception)
            {

                return default(T);
            }
        }

        public new T Get(string id)
        {
            try
            {
                var stringId = typeof(T).Name.ToLower();
                var response = Get<T>(id, c => c.Index(stringId).Refresh());
                if (response.IsValid)
                {
                    return response.Source;
                }
                return default(T);
            }
            catch (Exception)
            {

                return default(T);
            }
        }

        public new T Get(long id)
        {
            try
            {
                var stringId = typeof(T).Name.ToLower();
                var response = Get<T>(id, c => c.Index(stringId).Refresh());
                if (response.IsValid)
                {
                    return response.Source;
                }
                return default(T);
            }
            catch (Exception)
            {

                return default(T);
            }
        }
    }
}
