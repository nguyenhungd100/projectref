﻿using AutoMapper;
using Exam.CoreData.Data.EntityMvc;
using Exam.CoreData.MvcModels.Tags;

namespace MvcCore.Helpers
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            CreateMap<TagModel, Tag>();
        }

    }
}
