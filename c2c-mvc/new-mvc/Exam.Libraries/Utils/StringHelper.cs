﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Exam.Libraries.Utils
{
    public static class StringHelper
    {
        public static string[] RemoveDuplicates(this string[] s)
        {
            HashSet<string> set = new HashSet<string>();
            string[] result = new string[set.Count];
            set.CopyTo(result);
            return result;
        }

        public static int[] RemoveDuplicates(this int[] s)
        {
            var set = new HashSet<int>();
            var result = new int[set.Count];
            set.CopyTo(result);
            return result;
        }

        public static int[] ParseIds(this string stringFormat)
        {
            if (string.IsNullOrEmpty(stringFormat) || !stringFormat.Contains(";")) return null;

            var ids = stringFormat.Split(new string[] { ";" }, StringSplitOptions.RemoveEmptyEntries)
                .Select(c => Convert.ToInt32(c)).ToArray();

            return ids;
        }
    }
}
