﻿using IdGen;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace Exam.Libraries.Utils
{
    public class IdPartModel
    {
        public string Bits { get; set; }
        public TimeSpan Times { get; set; }
        public short GeneratorId { get; set; }
        public long Sequence { get; set; }
        public DateTime Date { get; set; }
    }

    public static class IdParts
    {

        private static object _syncLock = new object();

        public static long AutogenId(int generatorId)
        {
            var generator = new IdGenerator(generatorId);
            return generator.CreateId();
        }

        public static IEnumerable<long> AutogenId(int generatorId, int count)
        {
            lock (_syncLock)
            {
                //if (count > 4096) throw new Exception("Sequence overflow. Refusing to generate id for rest of tick");
                var generators = (IEnumerable)new IdGenerator(generatorId)/*.GetEnumerator()*/;
                // Interlocked.Increment(ref safeInstanceCount);
                var ids = generators.OfType<long>().Take(count).ToArray();
                return ids;
            }
        }

        public static IdPartModel DecomposeIdPart(long id)
        {
            var generator = new IdGenerator(1);
            var epoc = generator.Epoch.LocalDateTime;

            var bytes = BitConverter.GetBytes(id).Reverse();
            var bits = string.Concat(bytes.Select(c => Convert.ToString((byte)c, 2).PadLeft(8, '0')));
            var timespan = TimeSpan.FromMilliseconds(Convert.ToInt64(bits.Substring(0, 42), 2));

            return new IdPartModel
            {
                Bits = bits,
                GeneratorId = Convert.ToInt16(bits.Substring(42, 10), 2),
                Sequence = Convert.ToInt64(bits.Substring(52, 12), 2),
                Times = timespan,
                Date = new DateTime(timespan.Ticks + epoc.Ticks)
            };
        }

        public static IdPartModel DecomposeIdPartInsta(long id)
        {
            try
            {
                var epocNew = new DateTime(2019, 1, 1);

                var bytes = BitConverter.GetBytes(id).Reverse();
                var bits = string.Concat(bytes.Select(c => Convert.ToString((byte)c, 2).PadLeft(8, '0')));

                var timespan = TimeSpan.FromSeconds(Convert.ToInt64(bits.Substring(0, 41), 2));

                var totalMiliSeconds = (long)timespan.TotalSeconds * 1000 + (epocNew.Ticks / 10000);

                var da = new DateTime(totalMiliSeconds * 10000);

                return new IdPartModel
                {
                    Bits = bits,
                    GeneratorId = Convert.ToInt16(bits.Substring(41, 13), 2),
                    Sequence = Convert.ToInt64(bits.Substring(54, 10), 2),
                    Times = timespan,
                    Date = new DateTime(totalMiliSeconds * 10000)
                };
            }
            catch (Exception)
            {
                throw;
            }

        }
    }
}
