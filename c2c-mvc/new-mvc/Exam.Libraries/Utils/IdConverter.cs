﻿using System;

namespace Exam.Libraries.Utils
{
    public static class IdConverter
    {
        public static ulong Base64ToLong(string id)
        {
            var bytes = Convert.FromBase64String(id);
            Array.Reverse(bytes);
            return BitConverter.ToUInt64(bytes, 0);
        }

        public static string ULongToString(ulong id)
        {
            var bytes = BitConverter.GetBytes(id);
            Array.Reverse(bytes);
            return Convert.ToBase64String(bytes);
        }
    }
}
