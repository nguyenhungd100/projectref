﻿using Exam.CoreData.Repository;
using Exam.CoreData.Repository.Implement;
using Exam.EsSearch.EsSearch;
using Exam.Services.CategoryFacade;
using Exam.Services.CategoryFacade.Implement;
using Exam.Services.PostFacade;
using Exam.Services.PostFacade.Implement;
using Exam.Services.ProvinceAndDistrictFacade;
using Exam.Services.ProvinceAndDistrictFacade.Implemenet;
using Exam.Services.RoleFacade;
using Exam.Services.RoleFacade.Implement;
using Exam.Services.SendEmailFacade;
using Exam.Services.SendEmailFacade.Implement;
using Microsoft.Extensions.DependencyInjection;
using Services.UserFacade;
using Services.UserFacade.Implement;
using ServiceStack.Redis;

namespace Exam.Api.Configuration
{
    public static class DIConfiguration
    {
        public static void ConfigDI(this IServiceCollection services, AppSettings appSettings)
        {
            services.AddScoped(typeof(IRepository<,>), typeof(Repository<,>));
            services.AddTransient<IRoleService, RoleService>();
            services.AddTransient<IUserService, UserService>();
            services.AddTransient<IProvinceAndDistrictService, ProvinceAndDistrictService>();
            services.AddTransient<ICategoryService, CategoryService>();
            services.AddTransient<ISendEmailService, SendEmailService>();
            services.AddTransient<IPostService, PostService>();

            services.AddTransient<ISendEmailService>(s => new SendEmailService(appSettings.EmailAddress, appSettings.EmailPass));
            services.AddScoped<IRedisClient, RedisClient>();
            services.AddScoped(typeof(IESClient<>), typeof(ElasticContext<>));
        }
    }
}
