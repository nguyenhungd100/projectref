﻿namespace Exam.Api.Configuration
{
    public class AppSettings
    {
        public string ApiServer { get; set; }

        public string AuthenticationServer { get; set; }

        public string[] ClientAppRedirectUri { get; set; }

        public string EmailAddress { get; set; }

        public string EmailPass { get; set; }
    }
}
