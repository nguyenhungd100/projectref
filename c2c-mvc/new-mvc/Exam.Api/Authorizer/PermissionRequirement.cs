﻿using Exam.Services.RoleFacade;
using Microsoft.AspNetCore.Authorization;
using System.Linq;
using System.Threading.Tasks;

namespace Exam.Api.Authorizer
{
    public class PermissionRequirement : AuthorizationHandler<PermissionRequirement>, IAuthorizationRequirement
    {
        private readonly PermissionCode _permission;

        public PermissionRequirement(PermissionCode permission)
        {
            _permission = permission;
        }

        protected override async Task HandleRequirementAsync(AuthorizationHandlerContext context, PermissionRequirement requirement)
        {
            if (context.User == null)
            {
                context.Fail();
                return;
            }
            var permissions = context.User.Claims.FirstOrDefault(c => c.Type == "permissions");
            if (permissions == null)
            {
                context.Fail();
                return;
            }

            bool hasPermission = permissions.Value.Contains(";" + requirement._permission.GetHashCode() + ";");
            if (hasPermission)
            {
                context.Succeed(requirement);
            }
            else context.Fail();
        }
    }
}
