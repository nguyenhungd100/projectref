﻿using Microsoft.AspNetCore.Mvc;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Exam.Api.Controllers
{
    [Route("/")]
    public class HomeController : Controller
    {
        // GET: api/<controller>
        [HttpGet]
        public string Get()
        {
            return "Api Server";
        }
    }
}
