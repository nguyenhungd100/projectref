﻿using Exam.Api.Helpers;
using Exam.CoreData.Models;
using Exam.CoreData.Models.PagingInfo;
using Exam.CoreData.Models.Posts;
using Exam.Services.PostFacade;
using Exam.Services.RoleFacade;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Swashbuckle.AspNetCore.Annotations;
using System;
using System.Net;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Exam.Api.Controllers
{
    [Produces("application/json")]
    [Route("api/[controller]")]
    public class PostController : Controller
    {
        private readonly IPostService _postService;

        public PostController(IPostService postService)
        {
            _postService = postService;
        }

        /// <summary>
        /// Tìm kiếm tin bài
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost("search")]
        [Authorize]
        [SwaggerResponse((int)System.Net.HttpStatusCode.OK, Type = typeof(BaseSearchResult<PostModel>))]
        public IActionResult SearchPost([FromBody] SearchPostModel model)
        {
            var result = _postService.SearchPost(model);
            return Json(new { success = true, data = result });
        }

        /// <summary>
        /// Lấy thông tin tin bài theo mã
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("info/{id}")]
        //[Authorize]
        [SwaggerResponse((int)System.Net.HttpStatusCode.OK, Type = typeof(PostModel))]
        public IActionResult GetPostById(int id)
        {
            var result = _postService.GetPostById(id);
            return Json(new { success = true, data = result });
        }

        /// <summary>
        /// Cập nhật tin bài hoặc tạo tin bài
        /// </summary>
        /// <param name="model"></param>
        [HttpPost("save")]
        [Authorize]
        [SwaggerResponse((int)HttpStatusCode.OK, Type = typeof(bool))]
        public IActionResult SavePost([FromBody] SavePostModel model)
        {
            model.CreatedBy = HttpContext.User.Identity.GetUserId();
            var result = _postService.SavePost(model);
            return Json(new { success = result });
        }

        /// <summary>
        /// Xóa nhiều tin bài
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpDelete("delete_many")]
        [SwaggerResponse((int)HttpStatusCode.OK, Type = typeof(bool))]
        public IActionResult DeleteManyPost([FromBody] DeleteGenericModel model)
        {
            var result = _postService.DeleteManyPost(model);
            return Json(new { success = result });
        }

        /// <summary>
        /// Duyệt tin bài
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost("approve_post/{id}")]
        [Authorize]
        [SwaggerResponse((int)HttpStatusCode.OK, Type = typeof(bool))]
        public IActionResult ApprovePost(int id)
        {
            var approver = HttpContext.User.Identity.GetUserId();
            if (HttpContext.User.HasPermission(PermissionCode.APPROVEPOST))
            {
                var result = _postService.ApprovePost(id, approver);
                return Json(new { success = result });
            }
            else throw new Exception("Bạn không có quyền thực hiện tính năng này");
        }

        /// <summary>
        /// Từ chối tin bài
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost("reject_post/{id}")]
        [Authorize]
        [SwaggerResponse((int)HttpStatusCode.OK, Type = typeof(bool))]
        public IActionResult RejectPost(int id)
        {
            var approver = HttpContext.User.Identity.GetUserId();
            if (HttpContext.User.HasPermission(PermissionCode.REJECTPOST))
            {
                var result = _postService.RejectPost(id, approver);
                return Json(new { success = result });
            }
            else throw new Exception("Bạn không có quyền thực hiện tính năng này");
        }
    }
}
