﻿using Exam.CoreData.Models.Categories;
using Exam.CoreData.Models.ObjectOrienteds;
using Exam.Services.CategoryFacade;
using Microsoft.AspNetCore.Mvc;
using Swashbuckle.AspNetCore.Annotations;
using System.Collections.Generic;
using System.Net;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Exam.Api.Controllers
{
    [Route("api/[controller]")]
    public class CategoryController : Controller
    {
        private readonly ICategoryService _categoryService;

        public CategoryController(ICategoryService categoryService)
        {
            _categoryService = categoryService;
        }

        /// <summary>
        /// Danh sách danh mục
        /// </summary>
        /// <returns></returns>
        [HttpGet("categories")]
        //[Authorize]
        [SwaggerResponse((int)HttpStatusCode.OK, Type = typeof(IEnumerable<CategoryModel>))]
        public IActionResult GetCategories()
        {
            var result = _categoryService.GetCategories();
            return Json(new { success = true, data = result });
        }

        /// <summary>
        /// Lấy tất cả danh sách đối tượng bài đăng theo từng danh mục
        /// </summary>
        [HttpGet("object_types/{categoryId}")]
        //[Authorize]
        [SwaggerResponse((int)HttpStatusCode.OK, Type = typeof(IEnumerable<ObjectTypeModel>))]
        public IActionResult GetObjectTypes(int categoryId)
        {
            var result = _categoryService.GetObjectTypes(categoryId);
            return Json(new { success = true, data = result });
        }
    }
}
