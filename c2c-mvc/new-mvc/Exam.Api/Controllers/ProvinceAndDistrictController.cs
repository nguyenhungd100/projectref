﻿using Exam.Services.ProvinceAndDistrictFacade;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Exam.Api.Controllers
{
    [Route("api/[controller]")]
    public class ProvinceAndDistrictController : Controller
    {
        private readonly IProvinceAndDistrictService _provinceAndDistrictService;

        public ProvinceAndDistrictController(IProvinceAndDistrictService provinceAndDistrictService)
        {
            _provinceAndDistrictService = provinceAndDistrictService;
        }

        /// <summary>
        /// Get all provinces
        /// </summary>
        /// <returns></returns>
        [HttpGet("provinces")]
        [Authorize]
        public IActionResult GetAllProvinces()
        {
            var result = _provinceAndDistrictService.GetAllProvinces();
            return Json(new { success = true, data = result });
        }

        /// <summary>
        /// Get all districts
        /// </summary>
        /// <returns></returns>
        [HttpGet("districts")]
        [Authorize]
        public IActionResult GetAllDistricts()
        {
            var result = _provinceAndDistrictService.GetAllDistricts();
            return Json(new { success = true, data = result });
        }

        [HttpGet("district_groups/{provinceId}")]
        public IActionResult GetDistrictGroupsWithProvince(int provinceId)
        {
            var result = _provinceAndDistrictService.GetDistrictGroupsWithProvince(provinceId);
            return Json(new { success = true, data = result });
        }

        /// <summary>
        /// Import info districts by excel
        /// </summary>
        /// <param name="file"></param>
        /// <returns></returns>
        [HttpPost("import_district")]
        //[Authorize]
        public IActionResult ImportDistrict(IFormFile file)
        {
            var result = _provinceAndDistrictService.ImportDistrict(file);
            return Json(new { success = result });
        }

        /// <summary>
        /// Import info provinces by excel
        /// </summary>
        /// <param name="file"></param>
        /// <returns></returns>
        [HttpPost("import_provinces")]
        //[Authorize]
        public IActionResult ImportProvinces(IFormFile file)
        {
            var result = _provinceAndDistrictService.ImportProvinces(file);
            return Json(new { success = result });
        }

    }
}
