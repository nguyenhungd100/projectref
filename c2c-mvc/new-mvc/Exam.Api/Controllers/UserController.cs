﻿using Exam.Api.Helpers;
using Exam.CoreData.Models.Users;
using Exam.Libraries.Utils;
using Exam.Services.RoleFacade;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Services.UserFacade;
using Swashbuckle.AspNetCore.Annotations;
using System;
using System.Net;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Exam.Api.Controllers
{
    [Route("api/[controller]")]
    public class UserController : Controller
    {
        private readonly IUserService _userService;

        public UserController(IUserService userService)
        {
            _userService = userService;
        }

        [HttpGet("my_profile")]
        [Authorize]
        [SwaggerResponse((int)HttpStatusCode.OK, Type = typeof(ResultBase<UserModel>))]
        public IActionResult GetMyProfile()
        {
            var userId = HttpContext.User.Identity.GetUserId();
            var userModel = _userService.GetById(userId);
            var permissions = HttpContext.User.GetPermissions();
            userModel.Permissions = permissions;

            return Json(new { success = true, data = userModel });
        }

        /// <summary>
        /// Tạo tài khoản từ phía quản trị cho nhân viên hệ thống
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [Authorize(Policy = nameof(PermissionCode.MANAGER_USERS))]
        [HttpPost("create_user_by_admin")]
        [SwaggerResponse((int)HttpStatusCode.OK, Type = typeof(ResultBase<bool>))]
        public IActionResult CreateUserByAdmin([FromBody]CreateUserModel model)
        {
            var result = _userService.CreateUserByAdmin(model);
            return Json(new { success = result });
        }

        /// <summary>
        /// Tạo tài khoản từ phía người dùng
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost("create_user_side")]
        [SwaggerResponse((int)HttpStatusCode.OK, Type = typeof(ResultBase<bool>))]
        public IActionResult CreateUserSide([FromBody]CreateUserModel model)
        {
            var x = EncryptHelper.Encrypt("1581310025", true);
            model.IsUserSide = true;
            var result = _userService.CreateUserSide(model);
            return Json(new { success = result });
        }

        /// <summary>
        /// Xác thực mã OTP đăng ký tài khoản bằng gmail
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost("verify_otp_login")]
        [SwaggerResponse((int)HttpStatusCode.OK, Type = typeof(ResultBase<bool>))]
        public IActionResult VerifyOTPLogin([FromBody]VerifyOTPModel model)
        {
            if (!ModelState.IsValid) throw new Exception(ModelState.GetErrorsMessage());
            var result = _userService.ActiveAccount(model);
            return Json(new { success = result });
        }
        //[Authorize(Policy = nameof(PermissionCode.MANAGER_USERS))]
        //[HttpPut("update_user")]
        //[SwaggerResponse((int)HttpStatusCode.OK, Type = typeof(ResultBase<bool>))]
        //public IActionResult UpdateUser(int id, [FromBody]string value)
        //{
        //    var result = _userService.CreateUser(model);
        //    return Json(new { success = result });
        //}

        [Authorize(Policy = nameof(PermissionCode.MANAGER_USERS))]
        [HttpPut("lock_account/{userId}")]
        [SwaggerResponse((int)HttpStatusCode.OK, Type = typeof(ResultBase<bool>))]
        public IActionResult LockAccount(int userId)
        {
            var result = _userService.LockAccount(userId);
            return Json(new { success = result });
        }

    }
}
