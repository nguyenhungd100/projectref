﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using Exam.Api.Authorizer;
using Exam.Api.Configuration;
using Exam.Api.Helpers;
using Exam.CoreData;
using Exam.CoreData.Data;
using Exam.Libraries.Utils;
using Exam.Services.RoleFacade;
using Exam.Services.RoleFacade.Implement;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Cors.Internal;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Serilog;
using ServiceStack.Redis;
using Swashbuckle.AspNetCore.Swagger;
using Swashbuckle.AspNetCore.SwaggerGen;
using Swashbuckle.AspNetCore.SwaggerUI;

namespace ExamProject
{
    public class Startup
    {
        public Startup(IHostingEnvironment env, IConfiguration configuration)
        {
            _configuration = configuration;

            var builder = new ConfigurationBuilder()
                .SetBasePath(env.ContentRootPath)
                .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true)
                .AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: true)
                .AddEnvironmentVariables();
            _configuration = builder.Build();

            Log.Logger = new LoggerConfiguration()
                        .ReadFrom.Configuration(_configuration)
                        .Enrich.FromLogContext()
                        .CreateLogger();
        }

        public IConfiguration _configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            // Add framework services.
            services.AddMvc(config=> {
                config.Filters.Add(typeof(GlobalExceptionFilter));
            });
            services.AddSwaggerGen(config =>
            {
                config.SwaggerDoc("v1", new Info { Title = "ExamProject v1", Version = "V1" });
                config.IncludeXmlComments(string.Format("{0}\\Exam.Api.xml", AppDomain.CurrentDomain.BaseDirectory));
                config.IncludeXmlComments(string.Format("{0}\\Exam.CoreData.xml", AppDomain.CurrentDomain.BaseDirectory));

                config.DescribeAllEnumsAsStrings();
                config.AddSecurityDefinition("Bearer", new ApiKeyScheme()
                {
                    Description = "JWT Authorization header {token}",
                    Name = "Authorization",
                    In = "header",
                    Type = "apiKey"
                });
                config.AddSecurityRequirement(new Dictionary<string, IEnumerable<string>>
                {
                    {"Bearer", new string[]{} },
                    {"Basic", new string[]{} }
                });
            });

            var appSettingSection = _configuration.GetSection("AppSettings");
            services.Configure<AppSettings>(appSettingSection);
            var appSettings = new AppSettings();
            appSettingSection.Bind(appSettings);

            services.AddDbContext<ExamDBContext>(
                option => option
                            //.UseLazyLoadingProxies()
                            .UseSqlServer(EncryptHelper.Decrypt(_configuration.GetConnectionString("DefaultConnection"), true)));
          
            services.AddCors();
            DIConfiguration.ConfigDI(services, appSettings);
           
            services.AddCors(options =>
            {
                options.AddPolicy("AllowAll",
                    builder =>
                    {
                        builder
                        .AllowAnyOrigin()
                        .AllowAnyMethod()
                        .AllowAnyHeader()
                        .AllowCredentials();
                    });
            });

            services.Configure<MvcOptions>(options => {
                options.Filters.Add(new CorsAuthorizationFilterFactory("AllowAll"));
            });          

            services.AddAuthentication("Bearer")
                .AddIdentityServerAuthentication(options =>
                {
                    options.Authority = appSettings.AuthenticationServer;

                    options.RequireHttpsMetadata = false;
                    options.ApiName = "exam_api";
                });

            services.AddAuthorization(options =>
            {
                IRoleService roleService = new RoleService();
                var permissions = roleService.ListPermission();
                var all = new List<Permission>();                                                                                                                   
                permissions.ForEach(c => all.AddRange(c.Permissions));
                foreach (var permission in all)
                {
                    var policyName = permission.Code.ToString();
                    options.AddPolicy(policyName, p => p.Requirements.Add(new PermissionRequirement(permission.Code)));
                }
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory)
        {
            //loggerFactory.AddConsole(Configuration.GetSection("Logging"));
            //loggerFactory.AddDebug();

            //if (env.IsDevelopment())
            //{
            //    app.UseDeveloperExceptionPage();
            //}
            //else
            //{
            //    app.UseHsts();
            //}
            app.UseAuthentication();
            app.UseMvc(routes => 
            {
                routes.MapRoute(
                        name: "default",
                        template: "{controller=Home}/{action=Index}/{id?}"
                    );
            });
            
            app.UseSwagger();
            app.UseSwaggerUI(config => {
                config.SwaggerEndpoint("v1/swagger.json", "ExamProject Api");
                config.DocExpansion(DocExpansion.None);
                config.ShowExtensions();
            });
            app.UseCors("AllowAll");

            app.Use((context, next) =>
            {
                context.Response.OnStarting(() =>
                {
                    context.Response.Headers.Remove("X-Powered-By");
                    return Task.CompletedTask;
                });
                return next();
            });
        }
    }  
}
