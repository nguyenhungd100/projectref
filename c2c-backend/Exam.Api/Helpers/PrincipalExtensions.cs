﻿using Exam.Services.RoleFacade;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace Exam.Api.Helpers
{
    public static class PrincipalExtensions
    {
        public static bool HasPermission(this ClaimsPrincipal claimsPrincipal, PermissionCode permissionCode)
        {
            var permissionClaims = claimsPrincipal.Claims.FirstOrDefault(c => c.Type == "permissions");
            if (permissionClaims == null) return false;
            return permissionClaims.Value.Contains(";" + permissionCode.GetHashCode() + ";");
        }

        public static string GetPermissions(this ClaimsPrincipal claimsPrincipal)
        {
            var permissionClaims = claimsPrincipal.Claims.FirstOrDefault(c=>c.Type == "permissions");
            if (permissionClaims == null) return string.Empty;
            return permissionClaims.Value;
        }
    }
}
