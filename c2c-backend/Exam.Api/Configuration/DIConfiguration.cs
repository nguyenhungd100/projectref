﻿using Exam.CoreData.Repository;
using Exam.CoreData.Repository.Implement;
using Exam.Services.CategoryFacade;
using Exam.Services.CategoryFacade.Implement;
using Exam.Services.CommentFacade;
using Exam.Services.CommentFacade.Implement;
using Exam.Services.ImageFacade;
using Exam.Services.ImageFacade.Implement;
using Exam.Services.LiveStreamFacade;
using Exam.Services.LiveStreamFacade.Implement;
using Exam.Services.NotifyFacade;
using Exam.Services.NotifyFacade.Implement;
using Exam.Services.PostFacade;
using Exam.Services.PostFacade.Implement;
using Exam.Services.ProvinceAndDistrictFacade;
using Exam.Services.ProvinceAndDistrictFacade.Implemenet;
using Exam.Services.RoleFacade;
using Exam.Services.RoleFacade.Implement;
using Exam.Services.SendEmailFacade;
using Exam.Services.SendEmailFacade.Implement;
using Exam.Services.SpecificationFacade;
using Exam.Services.SpecificationFacade.Implement;
using Exam.Services.StatisticFacade;
using Exam.Services.StatisticFacade.Implement;
using Exam.Services.TransactionFacade;
using Exam.Services.TransactionFacade.Implement;
using Microsoft.Extensions.DependencyInjection;
using Services.UserFacade;
using Services.UserFacade.Implement;
using ServiceStack.Redis;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Exam.Api.Configuration
{
    public static class DIConfiguration
    {
        public static void ConfigDI(this IServiceCollection services, AppSettings appSettings)
        {
            services.AddScoped(typeof(IRepository<,>), typeof(Repository<,>));
            services.AddTransient<IRoleService, RoleService>();
            services.AddTransient<IUserService, UserService>();
            services.AddTransient<IProvinceAndDistrictService, ProvinceAndDistrictService>();
            services.AddTransient<ICategoryService, CategoryService>();
            services.AddTransient<ISendEmailService, SendEmailService>();
            services.AddTransient<IPostService, PostService>();
            services.AddTransient<ITransactionService, TransactionService>();
            services.AddTransient<ISpecificationService, SpecificationService>();
            services.AddTransient<IImageService, ImageService>();
            services.AddTransient<ICommentService, CommentService>();
            services.AddTransient<IStatisticService, StatisticService>();
            services.AddTransient<ILiveStreamService, LiveStreamService>();
            services.AddTransient<ISendEmailService>(s=> new SendEmailService(appSettings.EmailAddress, appSettings.EmailPass));
            services.AddTransient<IPaymentService>(s => new PaymentService(
                 appSettings.PaymentChosen.Email,
                appSettings.PaymentChosen.MerChantId,
                appSettings.PaymentChosen.MerChantPass,
                appSettings.PaymentChosen.ReturnUrl,
                appSettings.PaymentChosen.CancelUrl));
            services.AddTransient<INotifyService>(s => new NotifyService(appSettings.FirebaseUrl));
        }
    }
}
