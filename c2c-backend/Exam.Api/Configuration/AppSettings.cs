﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Exam.Api.Configuration
{
    public class AppSettings
    {
        public string ApiServer { get; set; }

        public string AuthenticationServer { get; set; }

        public string[] ClientAppRedirectUri { get; set; }

        public string FirebaseUrl { get; set; }

        public string EmailAddress { get; set; }

        public string EmailPass { get; set; }

        public PaymentInfo[] Payments { get; set; }

        public PaymentInfo PaymentChosen 
        { 
            get
            {
                return Payments.FirstOrDefault(c => c.Id == PaymentChosenId);
            }
        }

        public int PaymentChosenId { get; set; }
    }

    public class PaymentInfo
    {
        public int Id { get; set; }

        public string CancelUrl { get; set; }

        public string ReturnUrl { get; set; }

        public string Email { get; set; }

        public string MerChantId { get; set; }

        public string MerChantPass { get; set; }
    }
}
