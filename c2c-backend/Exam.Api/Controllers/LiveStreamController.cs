﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Exam.Api.Helpers;
using Exam.Services.LiveStreamFacade;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Swashbuckle.AspNetCore.Annotations;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Exam.Api.Controllers
{
    [Route("api/[controller]")]
    public class LiveStreamController : Controller
    {
        private readonly ILiveStreamService _liveStreamService;

        public LiveStreamController(ILiveStreamService liveStreamService)
        {
            _liveStreamService = liveStreamService;
        }

        //[HttpPost("create_live")]
        //[Authorize]
        //[SwaggerResponse((int)HttpStatusCode.OK, Type = typeof(bool))]
        //public IActionResult CreateStream([FromBody] Create)
        //{
        //    var userId = HttpContext.User.Identity.GetUserId();
        //    var result = _liveStreamService.CreateStream(userId);
        //    return Json(new { success = result });
        //}
        
    }
}
