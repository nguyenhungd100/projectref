﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Exam.CoreData.Models.Statistics;
using Exam.Services.StatisticFacade;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Swashbuckle.AspNetCore.Annotations;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Exam.Api.Controllers
{
    [Route("api/[controller]")]
    public class StatisticController : Controller
    {
        private readonly IStatisticService _statisticService;

        public StatisticController(IStatisticService statisticService)
        {
            _statisticService = statisticService;
        }

        /// <summary>
        /// Thống kê chung
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost("general")]
        [Authorize]
        [SwaggerResponse((int)System.Net.HttpStatusCode.OK, Type = typeof(StatisticResultModel))]
        public IActionResult StatisticGeneral([FromBody]StatisticGeneralModel model)
        {
            var result = _statisticService.StatisticGeneral(model);
            return Json(new { success = true, data = result });
        }

        /// <summary>
        /// Thống kê doanh thu
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost("revenue")]
        [Authorize]
        [SwaggerResponse((int)System.Net.HttpStatusCode.OK, Type = typeof(StatisticRevenueModel))]
        public IActionResult StatisticRevenue([FromBody]StatisticGeneralModel model)
        {
            var result = _statisticService.StatisticRevenue(model);
            return Json(new { success = true, data = result });
        }


    }
}
