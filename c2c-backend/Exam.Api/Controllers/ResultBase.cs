﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Exam.Api.Controllers
{
    public class ResultBase<T>
    {
        public bool success { get; set; }

        public T data { get; set; }

        public string message { get; set; }
    }
}
