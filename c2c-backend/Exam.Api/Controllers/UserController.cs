﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Exam.Api.Helpers;
using Exam.CoreData.Models.PagingInfo;
using Exam.CoreData.Models.Users;
using Exam.Libraries.Utils;
using Exam.Services.RoleFacade;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Services.UserFacade;
using Swashbuckle.AspNetCore.Annotations;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Exam.Api.Controllers
{
    [Route("api/[controller]")]
    public class UserController : Controller
    {
        private readonly IUserService _userService;

        public UserController(IUserService userService)
        {
            _userService = userService;
        }

        /// <summary>
        /// Tìm kiếm user - 1
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost("search")]
        [Authorize]
        [SwaggerResponse((int)System.Net.HttpStatusCode.OK, Type = typeof(BaseSearchResult<UserModel>))]
        public IActionResult SearchUser([FromBody] SearchUserModel model)
        {
            var result = _userService.SearchUser(model);
            return Json(new { success = true, data = result });
        }


        [HttpGet("my_profile")]
        [Authorize]
        [SwaggerResponse((int)HttpStatusCode.OK, Type = typeof(ResultBase<UserModel>))]
        public IActionResult GetMyProfile()
        {
            var userId = HttpContext.User.Identity.GetUserId();
            var userModel = _userService.GetById(userId);
            var permissions = HttpContext.User.GetPermissions();
            userModel.Permissions = permissions;

            return Json(new { success = true, data = userModel });
        }

        /// <summary>
        /// Tạo tài khoản từ phía quản trị cho nhân viên hệ thống
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [Authorize(Policy = nameof(PermissionCode.MANAGER_USERS))]
        [HttpPost("create_user_on_cms")]
        [SwaggerResponse((int)HttpStatusCode.OK, Type = typeof(ResultBase<bool>))]
        public IActionResult CreateUserByAdmin([FromBody]CreateUserModel model)
        {
            model.PersonAdd = HttpContext.User.Identity.GetUserId();
            var result = _userService.CreateUserByAdmin(model);
            return Json(new { success = result.Item1, data = result.Item2 });
        }

        /// <summary>
        /// Tạo tài khoản từ phía người dùng
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost("create_user_side")]
        [SwaggerResponse((int)HttpStatusCode.OK, Type = typeof(ResultBase<bool>))]
        public IActionResult CreateUserSide([FromBody]CreateUserModel model)
        {
            model.IsUserSide = true;
            var result = _userService.CreateUserSide(model);
            return Json(new { success = result });
        }

        /// <summary>
        /// Xác thực mã OTP đăng ký tài khoản bằng gmail
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost("verify_otp_login")]
        [SwaggerResponse((int)HttpStatusCode.OK, Type = typeof(ResultBase<bool>))]
        public IActionResult VerifyOTPLogin([FromBody]VerifyOTPModel model)
        {
            if (!ModelState.IsValid) throw new Exception(ModelState.GetErrorsMessage());
            var result = _userService.ActiveAccount(model);
            return Json(new { success = result.Item1, data = result.Item2  });
        }

        /// <summary>
        /// Gửi lại mã OTP đăng ký tài khoản bằng gmail
        /// </summary>
        /// <param name="email"></param>
        /// <returns></returns>
        [HttpGet("resend_otp_login/{email}")]
        [SwaggerResponse((int)HttpStatusCode.OK, Type = typeof(ResultBase<bool>))]
        public IActionResult ResendOtpLogin(string email)
        {
            var result = _userService.ResendOtpLogin(email);
            return Json(new { success = result });
        }
        //[Authorize(Policy = nameof(PermissionCode.MANAGER_USERS))]
        //[HttpPut("update_user")]
        //[SwaggerResponse((int)HttpStatusCode.OK, Type = typeof(ResultBase<bool>))]
        //public IActionResult UpdateUser(int id, [FromBody]string value)
        //{
        //    var result = _userService.CreateUser(model);
        //    return Json(new { success = result });
        //}

        /// <summary>
        /// Khóa tài khoản
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        [Authorize(Policy = nameof(PermissionCode.MANAGER_USERS))]
        [HttpPut("lock_account/{userId}")]
        [SwaggerResponse((int)HttpStatusCode.OK, Type = typeof(ResultBase<bool>))]
        public IActionResult LockAccount(int userId)
        {
            var result = _userService.LockAccount(userId);
            return Json(new { success = result });
        }

        /// <summary>
        /// Cập nhật thông tin tài khoản
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [Authorize]
        [HttpPut("update_my_profile")]
        [SwaggerResponse((int)HttpStatusCode.OK, Type = typeof(ResultBase<bool>))]
        public IActionResult UpdateMyProfile([FromBody] UpdateMyProfileModel model)
        {
            var userCurrent = HttpContext.User.Identity.GetUserId();
            if (userCurrent != model.Id)
            {
                throw new Exception("Bạn không có quyền thực hiện tính năng này");
            }
            var result = _userService.UpdateMyProfile(model);
            return Json(new { success = result });
        }

        [HttpGet("test_gen/{code}")]
        [SwaggerResponse((int)HttpStatusCode.OK, Type = typeof(ResultBase<bool>))]
        public string TestGen(string code)
        {
            return EncryptHelper.HassPasswordBCrypt(code);
        }

        [HttpPost("ec")]
        [SwaggerResponse((int)HttpStatusCode.OK, Type = typeof(ResultBase<bool>))]
        public string EncryptString([FromForm]string code)
        {
            return EncryptHelper.Encrypt(code);
        }

        [HttpPost("dc")]
        [SwaggerResponse((int)HttpStatusCode.OK, Type = typeof(ResultBase<bool>))]
        public string DecryptString([FromForm]string code)
        {
            return EncryptHelper.Decrypt(code, true);
        }

        /// <summary>
        /// Thay đổi mật khẩu
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [Authorize]
        [HttpPost("change_pass")]
        [SwaggerResponse((int)HttpStatusCode.OK, Type = typeof(ResultBase<bool>))]
        public IActionResult ChangePassword([FromBody] ChangePasswordModel model)
        {            
            var result = _userService.ChangePassword(model);
            return Json(new { success = result });
        }

        [HttpGet("export_to_excel")]
        [SwaggerResponse((int)HttpStatusCode.OK, Type = typeof(ResultBase<Stream>))]
        public IActionResult ExportToExcel()
        {
            var result = _userService.ExportToExcel();

            var type = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
            //var contentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
            return File(result, type, "hehe.xlsx");
        }
    }
}
