﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Exam.CoreData.Models.Images;
using Exam.Services.ImageFacade.Implement;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Swashbuckle.AspNetCore.Annotations;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Exam.Api.Controllers
{
    [Route("api/[controller]")]
    public class ImageController : Controller
    {
        private readonly IImageService _imageService;

        public ImageController(IImageService imageService)
        {
            _imageService = imageService;
        }

        /// <summary>
        /// Lấy ảnh theo id
        /// </summary>
        /// <param name="imageId"></param>
        /// <param name="width"></param>
        /// <param name="height"></param>
        /// <returns></returns>
        [HttpGet("{imageId}/{width}/{height}")]
        [SwaggerResponse((int)HttpStatusCode.OK, Type = typeof(FileResult))]
        public async Task<FileResult> GetImageById(string imageId, int width, int height)
        {
            var result = await _imageService.GetImageByIdAsync(imageId, width, height);
            return File(result.data, result.contentType, result.fileName);
        }


        /// <summary>
        /// Thêm mới ảnh đại diện hoặc ảnh trang cửa hàng
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost("save")]
        [Authorize]
        [SwaggerResponse((int)HttpStatusCode.OK, Type = typeof(bool))]
        public IActionResult SaveImage(SaveImageLibraryModel model)
        {
            var result = _imageService.SaveImage(model);
            return Json(new { success = !string.IsNullOrEmpty(result) ? true : false, data = result });
        }

    }
}
