﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Exam.Services.ProvinceAndDistrictFacade;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Exam.Api.Controllers
{
    [Route("api/[controller]")]
    public class ProvinceAndDistrictController : Controller
    {
        private readonly IProvinceAndDistrictService _provinceAndDistrictService;

        public ProvinceAndDistrictController(IProvinceAndDistrictService provinceAndDistrictService)
        {
            _provinceAndDistrictService = provinceAndDistrictService;
        }

        /// <summary>
        /// Lấy tất cả các tỉnh thành
        /// </summary>
        /// <returns></returns>
        [HttpGet("provinces")]
        [Authorize]        
        public IActionResult GetAllProvinces()
        {
            var result = _provinceAndDistrictService.GetAllProvinces();
            return Json(new { success = true, data = result });
        }

        /// <summary>
        /// Lấy tất cả các quận huyện
        /// </summary>
        /// <returns></returns>
        [HttpGet("districts")]
        [Authorize]
        public IActionResult GetAllDistricts()
        {
            var result = _provinceAndDistrictService.GetAllDistricts();
            return Json(new { success = true, data = result });
        }

        /// <summary>
        /// Lấy danh sách quận huyện theo mã tỉnh thành
        /// </summary>
        /// <param name="provinceId"></param>
        /// <returns></returns>
        [HttpGet("district_groups/{provinceId}")]
        public IActionResult GetDistrictGroupsWithProvince(int provinceId)
        {
            var result = _provinceAndDistrictService.GetDistrictGroupsWithProvince(provinceId);
            return Json(new { success = true, data = result });
        }

        /// <summary>
        /// Thêm quận huyện bằng file excel
        /// </summary>
        /// <param name="file"></param>
        /// <returns></returns>
        [HttpPost("import_district")]
        //[Authorize]
        public IActionResult ImportDistrict(IFormFile file)
        {
            var result = _provinceAndDistrictService.ImportDistrict(file);
            return Json(new { success = result });
        }

        /// <summary>
        /// Thêm tỉnh thành bằng file excel
        /// </summary>
        /// <param name="file"></param>
        /// <returns></returns>
        [HttpPost("import_provinces")]
        //[Authorize]
        public IActionResult ImportProvinces(IFormFile file)
        {
            var result = _provinceAndDistrictService.ImportProvinces(file);
            return Json(new { success = result });
        }

    }
}
