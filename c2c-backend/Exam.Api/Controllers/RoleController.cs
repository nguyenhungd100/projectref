﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Exam.CoreData.Models.Roles;
using Exam.Services.RoleFacade;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Swashbuckle.AspNetCore.Annotations;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Exam.Api.Controllers
{
    [Route("api/[controller]")]
    public class RoleController : Controller
    {
        private readonly IRoleService _roleService;

        public RoleController(IRoleService roleService)
        {
            _roleService = roleService;
        }

        /// <summary>
        /// Tìm kiếm vai trò
        /// </summary>
        /// <returns></returns>
        [HttpPost("search")]
        [Authorize(Policy = nameof(PermissionCode.MANAGER_ROLES))]
        [SwaggerResponse((int)System.Net.HttpStatusCode.OK, Type = typeof(RoleFilterResult))]
        public ActionResult FilterRoles([FromBody]RoleFilterModel roleFilterModel)
        {
            var res = _roleService.Filter(roleFilterModel);
            return Json(new { success = true, data = res });
        }

        /// <summary>
        /// Lưu quyền người dùng theo vai trò
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost("save")]
        [Authorize(Policy = nameof(PermissionCode.MANAGER_ROLES))]
        [SwaggerResponse((int)System.Net.HttpStatusCode.OK, Type = typeof(bool))]
        public ActionResult SaveRole([FromBody]SaveRoleModel model)
        {
            var res = _roleService.SaveRole(model);
            return Json(new { success = res });
        }

        /// <summary>
        /// Lấy mã code role
        /// </summary>
        /// <returns></returns>
        [HttpGet("codes")]
        [Authorize]
        [SwaggerResponse((int)System.Net.HttpStatusCode.OK, Type = typeof(IEnumerable<RoleCodeModel>))]
        public ActionResult GetCodeRoles()
        {
            var res = _roleService.GetCodeRoles();
            return Json(new { success = true , data = res});
        }
    }
}
