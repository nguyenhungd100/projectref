﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Exam.Api.Helpers;
using Exam.CoreData.Models.TransactionHistories;
using Exam.Services.TransactionFacade;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Swashbuckle.AspNetCore.Annotations;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Exam.Api.Controllers
{
    [Route("api/[controller]")]
    public class TransactionController : Controller
    {
        private readonly ITransactionService _transactionService;

        public TransactionController(ITransactionService transactionService)
        {
            _transactionService = transactionService;
        }

        #region Transaction History
        /// <summary>
        /// Lấy lịch sử giao dịch theo người dùng
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        [HttpGet("transactions/{userid}")]
        [Authorize]
        [SwaggerResponse((int)HttpStatusCode.OK, Type = typeof(IEnumerable<TransactionHistoryModel>))]
        public IActionResult GetTransactionByUserId(int userId)
        {
            var result = _transactionService.GetTransactionByUserId(userId);
            return Json(new { success = true, data = result });
        }

        [HttpPost("get_check_out_url")]
        [Authorize]
        [SwaggerResponse((int)HttpStatusCode.OK, Type = typeof(IEnumerable<TransactionHistoryModel>))]
        public IActionResult GetCheckOutUrl([FromBody] CheckOutModel model)
        {
            model.UserId = HttpContext.User.Identity.GetUserId();
            var result = _transactionService.GetCheckOutUrl(model);
            return Json(new { success = true, data = result });
        }

        [HttpPost("verify_check_out")]
        [Authorize]
        [SwaggerResponse((int)HttpStatusCode.OK, Type = typeof(bool))]
        public IActionResult VerifyCheckOut([FromBody] VerifyCheckOutModel model)
        {
            model.UserId = HttpContext.User.Identity.GetUserId();
            var result = _transactionService.VerifyCheckOut(model);
            return Json(new { success = result });
        }
        #endregion

    }
}
