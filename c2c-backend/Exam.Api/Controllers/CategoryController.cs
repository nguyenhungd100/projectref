﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Exam.Api.Helpers;
using Exam.CoreData.Models.AssignCategoryManager;
using Exam.CoreData.Models.Categories;
using Exam.CoreData.Models.ObjectOrienteds;
using Exam.Services.CategoryFacade;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Swashbuckle.AspNetCore.Annotations;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Exam.Api.Controllers
{
    [Route("api/[controller]")]
    public class CategoryController : Controller
    {
        private readonly ICategoryService _categoryService;

        public CategoryController(ICategoryService categoryService)
        {
            _categoryService = categoryService;
        }

        #region Quản lý danh mục
        /// <summary>
        /// Danh sách danh mục
        /// </summary>
        /// <returns></returns>
        [HttpGet("categories")]
        //[Authorize]
        [SwaggerResponse((int)HttpStatusCode.OK, Type = typeof(IEnumerable<CategoryModel>))]
        public IActionResult GetCategories()
        {
            var result = _categoryService.GetCategories();
            return Json(new { success = true, data = result });
        }

        /// <summary>
        /// Lấy tất cả danh sách đối tượng bài đăng theo từng danh mục
        /// </summary>
        [HttpGet("object_types/{categoryId}")]
        //[Authorize]
        [SwaggerResponse((int)HttpStatusCode.OK, Type = typeof(IEnumerable<ObjectTypeModel>))]
        public IActionResult GetObjectTypes(int categoryId)
        {
            var result = _categoryService.GetObjectTypes(categoryId);
            return Json(new { success = true, data = result });
        }
        #endregion

        #region Quản lý phân giao
        /// <summary>
        /// Lấy thông tin người được phân giao quản lý danh mục
        /// </summary>
        /// <param name="categoryid"></param>
        /// <returns></returns>
        [HttpGet("user_assigned_by_category/{categoryid}")]
        [Authorize]
        [SwaggerResponse((int)HttpStatusCode.OK, Type = typeof(InfoUserAssignedModel))]
        public IActionResult GetUserAssignedByCategory(int categoryid)
        {
            var result = _categoryService.GetUserAssignedByCategory(categoryid);
            return Json(new { success = true, data = result });
        }
        
        /// <summary>
        /// Phân giao quản lý danh mục 
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost("assign_category_manager")]
        [Authorize]
        [SwaggerResponse((int)HttpStatusCode.OK, Type = typeof(bool))]
        public IActionResult AssignCategoryManager([FromBody] AssignCategoryManagerModel model)
        {
            model.AssignByUser = HttpContext.User.Identity.GetUserId();
            var result = _categoryService.AssignCategoryManager(model);
            return Json(new { success = result });
        }

        /// <summary>
        /// Thu hồi phân giao danh mục
        /// </summary>
        /// <param name="userAssignedId"></param>
        /// <param name="categoryId"></param>
        /// <returns></returns>
        [HttpPost("revocation_assign_category/{userAssignedId}/{categoryId}")]
        [Authorize]
        [SwaggerResponse((int)HttpStatusCode.OK, Type = typeof(bool))]
        public IActionResult RevocationAssignCategory(int userAssignedId, int categoryId)
        {
            var result = _categoryService.RevocationAssignCategory(userAssignedId, categoryId);
            return Json(new { success = result });
        }

        /// <summary>
        /// Staging
        /// </summary>
        /// <returns></returns>
        [HttpPost("deploy_staging")]
        //[Authorize]
        [SwaggerResponse((int)HttpStatusCode.OK, Type = typeof(string))]
        public string DeployStaging()
        {
            return "staging1222";
        }

        /// <summary>
        /// Production
        /// </summary>
        /// <returns></returns>
        [HttpPost("production_staging")]
        //[Authorize]
        [SwaggerResponse((int)HttpStatusCode.OK, Type = typeof(string))]
        public string DeployProduction()
        {
            var a = 5;
            return "production1222";
        }
        #endregion


    }
}
