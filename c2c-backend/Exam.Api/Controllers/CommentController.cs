﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Exam.Api.Helpers;
using Exam.CoreData.Models.Comments;
using Exam.CoreData.Models.PagingInfo;
using Exam.Services.CommentFacade;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Swashbuckle.AspNetCore.Annotations;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Exam.Api.Controllers
{
    [Route("api/[controller]")]
    public class CommentController : Controller
    {
        private readonly ICommentService _commentService;

        public CommentController(ICommentService commentService)
        {
            _commentService = commentService;
        }

        /// <summary>
        /// Tìm kiếm comment - 1
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost("get_comments")]
        //[Authorize]
        [SwaggerResponse((int)System.Net.HttpStatusCode.OK, Type = typeof(IEnumerable<CommentModel>))]
        public IActionResult SearchComment([FromBody] SearchCommentModel model)
        {
            var result = _commentService.SearchComment(model);
            return Json(new { success = true, data = result });
        }

        /// <summary>
        /// Cập nhật comment
        /// </summary>
        /// <param name="model"></param>
        [HttpPost("save")]
        [Authorize]
        [SwaggerResponse((int)HttpStatusCode.OK, Type = typeof(bool))]
        public IActionResult SavePost([FromBody] SaveCommentModel model)
        {
            model.CreatedBy = HttpContext.User.Identity.GetUserId();
            var result = _commentService.SaveComment(model);
            return Json(new { success = result });
        }

        /// <summary>
        /// Xóa comment
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete("delete/{id}")]
        [Authorize]
        [SwaggerResponse((int)HttpStatusCode.OK, Type = typeof(bool))]
        public IActionResult DeleteComment(int id)
        {
            var userId = HttpContext.User.Identity.GetUserId();
            var result = _commentService.DeleteComment(id, userId);
            return Json(new { success = result });
        }

        /// <summary>
        /// Like comment
        /// </summary> 
        /// <param name="id"></param>
        /// <param name="status"></param>
        /// <returns></returns>
        [HttpPost("like/{id}/{status}")]
        [Authorize]
        [SwaggerResponse((int)HttpStatusCode.OK, Type = typeof(bool))]
        public IActionResult LikeComment(int id, bool status = true)
        {
            var userId = HttpContext.User.Identity.GetUserId();
            var result = _commentService.LikeComment(id, userId, status);
            return Json(new { success = result });
        }

        /// <summary>
        /// Dislike comment
        /// </summary>
        /// <param name="id"></param>
        /// <param name="status"></param>
        /// <returns></returns>
        [HttpPost("dislike/{id}/{status}")]
        [Authorize]
        [SwaggerResponse((int)HttpStatusCode.OK, Type = typeof(bool))]
        public IActionResult DisLikeComment(int id, bool status = true)
        {
            var userId = HttpContext.User.Identity.GetUserId();
            var result = _commentService.DisLikeComment(id, userId, status);
            return Json(new { success = result });
        }
    }
}
