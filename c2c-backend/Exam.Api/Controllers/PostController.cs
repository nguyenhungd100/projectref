﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Exam.Api.Helpers;
using Exam.CoreData.Models;
using Exam.CoreData.Models.Images;
using Exam.CoreData.Models.PagingInfo;
using Exam.CoreData.Models.Posts;
using Exam.CoreData.Models.ShopPages;
using Exam.CoreData.Models.Specifications;
using Exam.Libraries.Utils;
using Exam.Services.PostFacade;
using Exam.Services.RoleFacade;
using Exam.Services.SpecificationFacade;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Services.UserFacade;
using Swashbuckle.AspNetCore.Annotations;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Exam.Api.Controllers
{
    [Produces("application/json")]
    [Route("api/[controller]")]
    public class PostController : Controller
    {
        private readonly IPostService _postService;
        private readonly ISpecificationService _specificationService;
        private readonly IUserService _userService;

        public PostController(IPostService postService,
            ISpecificationService specificationService,
            IUserService userService)
        {
            _postService = postService;
            _specificationService = specificationService;
            _userService = userService;
        }

        #region Post
        /// <summary>
        /// Tìm kiếm tin bài - 1
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost("search")]
        //[Authorize]
        [SwaggerResponse((int)System.Net.HttpStatusCode.OK, Type = typeof(BaseSearchResult<PostModel>))]
        public IActionResult SearchPost([FromBody] SearchPostModel model)
        {
            var kk = EncryptHelper.Encrypt("150210502019", true);
            var result = _postService.SearchPost(model);
            return Json(new { success = true, data = result });
        }

        /// <summary>
        /// Lưu tin quan tâm
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost("caring")]
        [Authorize]
        [SwaggerResponse((int)System.Net.HttpStatusCode.OK, Type = typeof(BaseSearchResult<PostModel>))]
        public IActionResult CaringPost([FromBody]CaringPostModel model)
        {
            model.UserId = HttpContext.User.Identity.GetUserId();
            var result = _postService.CaringPost(model);
            return Json(new { success = result });
        }

        /// <summary>
        /// Lấy thông tin tin bài theo mã - 1
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("info/{id}")]
        //[Authorize]
        [SwaggerResponse((int)System.Net.HttpStatusCode.OK, Type = typeof(PostModel))]
        public IActionResult GetPostById(int id)
        {
            var result = _postService.GetPostById(id);
            return Json(new { success = true, data = result });
        }

        /// <summary>
        /// Cập nhật tin bài hoặc tạo tin bài - 1
        /// </summary>
        /// <param name="model"></param>
        [HttpPost("save")]
        [Authorize]
        [SwaggerResponse((int)HttpStatusCode.OK, Type = typeof(bool))]
        public IActionResult SavePost([FromBody] SavePostModel model)
        {
            var isFreePost = false;
            if (HttpContext.User.HasPermission(PermissionCode.CREATE_FREE_POST))
            {
                isFreePost = true;
            }
            model.CreatedBy = HttpContext.User.Identity.GetUserId();
            var result = _postService.SavePost(model, isFreePost);
            return Json(new { success = result });
        }

        /// <summary>
        /// Xóa nhiều tin bài - 1 
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpDelete("delete_many")]
        [SwaggerResponse((int)HttpStatusCode.OK, Type = typeof(bool))]
        public IActionResult DeleteManyPost([FromBody] DeleteGenericModel model)
        {
            var result = _postService.DeleteManyPost(model);
            return Json(new { success = result });
        }

        /// <summary>
        /// Duyệt tin bài - 1
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost("approve_post/{id}")]
        [Authorize]
        [SwaggerResponse((int)HttpStatusCode.OK, Type = typeof(bool))]
        public IActionResult ApprovePost(int id)
        {
            var approver = HttpContext.User.Identity.GetUserId();
            if (HttpContext.User.HasPermission(PermissionCode.APPROVEPOST))
            {
                var result = _postService.ApprovePost(id, approver);
                return Json(new { success = result });
            }
            else throw new Exception("Bạn không có quyền thực hiện tính năng này");
        }

        /// <summary>
        /// Từ chối tin bài - 1 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost("reject_post/{id}")]
        [Authorize]
        [SwaggerResponse((int)HttpStatusCode.OK, Type = typeof(bool))]
        public IActionResult RejectPost(int id)
        {
            var approver = HttpContext.User.Identity.GetUserId();
            if (HttpContext.User.HasPermission(PermissionCode.REJECTPOST))
            {
                var result = _postService.RejectPost(id, approver);
                return Json(new { success = result });
            }
            else throw new Exception("Bạn không có quyền thực hiện tính năng này");
        }
        #endregion

        #region Ảnh tin bài      
        /// <summary>
        /// Lấy ảnh tin bài dựa theo id ảnh - 2
        /// </summary>
        /// <param name="imageId"></param>
        /// <param name="width"></param>
        /// <param name="height"></param>
        /// <returns></returns>
        [HttpGet("post_image/{imageId}/{width}/{height}")]
        [SwaggerResponse((int)HttpStatusCode.OK, Type = typeof(FileResult))]
        public async Task<FileResult> GetPostImageByImageIdAsync(string imageId, int width, int height)
        {
            var result = await _postService.GetPostImageByImageIdAsync(imageId, width, height);
            return File(result.data, result.contentType, result.fileName);
        }

        /// <summary>
        /// Lấy danh sách id ảnh tin bài - 2
        /// </summary>
        /// <param name="postId"></param>
        /// <returns></returns>
        [HttpGet("post_image_ids/{postId}")]
        [SwaggerResponse((int)HttpStatusCode.OK, Type = typeof(IEnumerable<Guid>))]
        public async Task<IActionResult> GetPostImageIdsAsync(int postId)
        {
            var result = await _postService.GetPostImageIdsAsync(postId);
            return Json(new { success = true, data = result });
        }

        /// <summary>
        /// Thêm mới ảnh tin bài - 2
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost("post_image/save")]
        //[Authorize]
        [SwaggerResponse((int)HttpStatusCode.OK, Type = typeof(bool))]
        public IActionResult SavePostImage(SavePostImageModel model)
        {
            var result = _postService.SavePostImage(model);
            return Json(new { success = !string.IsNullOrEmpty(result) ? true : false, data = result });
        }

        /// <summary>
        /// Xóa ảnh tin bài - 2
        /// </summary>
        /// <returns></returns>
        [HttpPost("post_image/delete/{imageId}")]
        [Authorize]
        [SwaggerResponse((int)HttpStatusCode.OK, Type = typeof(bool))]
        public async Task<IActionResult> DeletePostImageAsync(string imageId)
        {
            var result = await _postService.DeletePostImageAsync(imageId);
            return Json(new { success = result });
        }
        #endregion

        #region Shop Page      
        /// <summary>
        /// Lấy danh sách shop page - 3
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost("shop_pages/search")]
        // [Authorize]
        [SwaggerResponse((int)HttpStatusCode.OK, Type = typeof(BaseSearchResult<ShopPageModel>))]
        public IActionResult SearchShopPage([FromBody] SearchShopPageModel model)
        {
            var result = _postService.SearchShopPage(model);
            return Json(new { success = true, data = result });
        }

        /// <summary>
        /// Lấy shop page theo id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("shop_pages/{id}")]
        [SwaggerResponse((int)HttpStatusCode.OK, Type = typeof(ShopPageModel))]
        public IActionResult GetShopPageById(int id)
        {
            var result = _postService.GetShopPageById(id);
            return Json(new { success = true, data = result });

        }

        /// <summary>
        /// Cập nhật shop page - 3
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost("shop_pages/save")]
        [SwaggerResponse((int)HttpStatusCode.OK, Type = typeof(int))]
        public IActionResult SaveShopPage([FromBody] SaveShopPageModel model)
        {
            var isFreeCreateShop = false;
            if (HttpContext.User.HasPermission(PermissionCode.CREATE_FREE_SHOP_PAGE))
            {
                isFreeCreateShop = true;
            }
            var userCurrent = HttpContext.User.Identity.GetUserId();
            var result = _postService.SaveShopPage(model, userCurrent, isFreeCreateShop);
            return Json(new { success = true, data = result });

        }
        #endregion

        #region Specification
        /// <summary>
        /// Tìm kiếm thông số kỹ thuật - 4
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost("spec/search")]
        [SwaggerResponse((int)System.Net.HttpStatusCode.OK, Type = typeof(BaseSearchResult<SpecificationModel>))]
        public IActionResult SearchSpecification([FromBody] SearchSpecificationModel model)
        {
            var result = _specificationService.SearchSpecification(model);
            return Json(new { success = true, data = result });
        }


        /// <summary>
        /// Cập nhật thông số kỹ thuật - 4
        /// </summary>
        /// <param name="model"></param>
        [HttpPost("spec/save")]
        [Authorize]
        [SwaggerResponse((int)HttpStatusCode.OK, Type = typeof(bool))]
        public IActionResult SaveSpecfication([FromBody] SaveSpecificationModel model)
        {
            var result = _specificationService.SaveSpecfication(model);
            return Json(new { success = result });
        }

        /// <summary>
        /// Lấy thông số kỹ thuật theo danh mục - 4
        /// </summary>
        /// <param name="categoryid"></param>
        /// <returns></returns>
        [HttpGet("spec/get_by_categoryid/{categoryid}")]
        [SwaggerResponse((int)HttpStatusCode.OK, Type = typeof(IEnumerable<SpecificationModel>))]
        public IActionResult GetSpecficationByCategoryId(int categoryid)
        {
            var result = _specificationService.GetSpecficationByCategoryId(categoryid);
            return Json(new { success = true, data = result });
        }
        #endregion

        #region
        /// <summary>
        /// Lưu điểm đánh giá - 5
        /// </summary>
        /// <param name="score"></param>
        /// <param name="postId"></param>
        /// <returns></returns>
        [HttpPost("favorite/save/{postId}/{score}")]
        [Authorize]
        [SwaggerResponse((int)HttpStatusCode.OK, Type = typeof(bool))]
        public IActionResult SaveFavorite(int score, int postId)
        {
            var userId = HttpContext.User.Identity.GetUserId();
            var result = _postService.SaveFavorite(score, postId, userId);
            return Json(new { success = result });
        }


        /// <summary>
        /// Danh sách tố cáo - 6
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost("get_denounce_list")]
        [Authorize]
        [SwaggerResponse((int)HttpStatusCode.OK, Type = typeof(BaseSearchResult<DenouncePostModel>))]
        public IActionResult GetDenounce([FromBody] SearchDenounceModel model)
        {
            var result = _postService.GetDenouncePost(model);
            return Json(new { success = result });
        }

        /// <summary>
        /// Tố cáo tin bài - 6
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost("denounce")]
        [Authorize]
        [SwaggerResponse((int)HttpStatusCode.OK, Type = typeof(bool))]
        public IActionResult DenouncePost([FromBody] DenounceModel model)
        {
            model.UserId = HttpContext.User.Identity.GetUserId();
            var result = _postService.DenouncePost(model);
            return Json(new { success = result });
        }

        /// <summary>
        /// Khóa tin bài - 7
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost("lock")]
        [Authorize]
        [SwaggerResponse((int)HttpStatusCode.OK, Type = typeof(bool))]
        public IActionResult LockPost([FromBody] LockPostModel model)
        {
            var result = _postService.LockPost(model);
            return Json(new { success = result });
        }
        #endregion
    }
}
