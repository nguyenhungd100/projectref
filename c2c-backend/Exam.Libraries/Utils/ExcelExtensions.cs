﻿
using OfficeOpenXml;
using OfficeOpenXml.Style;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;

namespace Exam.Libraries.Utils
{
    public class ExcelBindingInfo
    {
        public ExcelBindingInfo()
        {

        }

        public Dictionary<string, List<(string columnValue, string title, int width)>> Rows { get; set; }

        public List<int> RowsWidth { get; set; }

        public int RowsHeight { get; set; }

        public Font Font { get; set; } = new Font("Times New Roman", FontSize);

        public static int FontSize { get; set; } = 13;
    }

    public class ExcelExtensions
    {
        public void SetBackGround(ExcelWorksheet worksheet, ExcelBindingInfo inputData)
        {
            worksheet.Cells.Style.WrapText = true;
            using (var range = worksheet.Cells["A:O"])
            {
                range.Style.Font.SetFromFont(inputData.Font);
            }

            //range header
            using (var range = worksheet.Cells[1, 1, 1 + inputData.Rows.Count(), inputData.Rows.Values.Count])
            {
                range.Style.Font.SetFromFont(inputData.Font);
                range.Style.Font.Bold = true;
                range.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                range.Style.VerticalAlignment = ExcelVerticalAlignment.Center;
            }

            //range content
            using (var range = worksheet.Cells[2, 1, 1 + inputData.Rows.Count(), inputData.Rows.Values.Count])
            {
                range.Style.Font.SetFromFont(inputData.Font);
                range.Style.Font.Bold = false;
                range.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                range.Style.VerticalAlignment = ExcelVerticalAlignment.Center;
            }

            int i = 2;
            foreach (var item in inputData.Rows.Values.FirstOrDefault())
            {
                worksheet.Cells[1, i].Value = item.title;
                worksheet.Column(i).Width = item.width;
                i++;
            }
            worksheet.View.FreezePanes(2, 1 + inputData.Rows.Count());
        }

        public MemoryStream ExportDataToExcel(ExcelBindingInfo inputData)
        {
            var stream = new MemoryStream();
            using (var excelPackage = new ExcelPackage(stream))
            {
                var worksheet = excelPackage.Workbook.Worksheets.Add("List Users");
                worksheet.DefaultRowHeight = inputData.RowsHeight;
                SetBackGround(worksheet, inputData);

                int i = 2;
                foreach (var row in inputData.Rows)
                {

                    int j = 1;
                    worksheet.Cells[i, 1].Value = i - 1;
                    foreach (var column in row.Value)
                    {
                        worksheet.Cells[i, 1 + j].Value = column.columnValue;
                        j++;
                    }
                    j = 0;
                    i++;
                }
                i = 0;
                stream = new MemoryStream(excelPackage.GetAsByteArray());
            }
            return stream;
        }


    }
}
