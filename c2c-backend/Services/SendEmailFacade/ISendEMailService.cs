﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Exam.Services.SendEmailFacade
{
    public interface ISendEmailService
    {
        bool SendOTPVerifyAccount(string email, string code);
    }
}
