﻿using Exam.Libraries.Utils;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Mail;
using System.Text;

namespace Exam.Services.SendEmailFacade.Implement
{
    public class SendEmailService : ISendEmailService
    {
        private readonly string _emailAdress;
        private readonly string _emailPass;

        public SendEmailService(string emailAdress, string emailPass)
        {
            _emailAdress = emailAdress;
            _emailPass = emailPass;
        }

        public bool SendOTPVerifyAccount(string email, string otp)
        {
            var pass = EncryptHelper.Decrypt(_emailPass, true);
            var result = false;
            try
            {
                var strBuilder = new StringBuilder();

                strBuilder.Append("Mã xác nhận tài khoản " + email + " của bạn là: " + otp + " ( có hiệu lực trong vòng 5 phút)");

                MailMessage msg = new MailMessage(_emailAdress, email)
                {
                    Subject = "Mã xác nhận đăng nhập Chợ điện tử C2C",
                    Body = strBuilder.ToString(),
                    IsBodyHtml = true,
                };


                SmtpClient smt = new SmtpClient("smtp.gmail.com", 587)
                {
                    UseDefaultCredentials = false,
                    EnableSsl = true,
                    Credentials = new NetworkCredential(_emailAdress, pass)                   
                };

                smt.Send(msg);

                result = true;
            }
            catch (Exception ex)
            {

                throw ex;
            }

            return result;
        }
    }
}
