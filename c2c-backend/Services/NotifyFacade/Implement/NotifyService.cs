﻿using Exam.Libraries.Utils;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using OfficeOpenXml.FormulaParsing.Excel.Functions.Math;
using System;
using System.Collections.Generic;
using System.Text;

namespace Exam.Services.NotifyFacade.Implement
{
    public class NotifyService : INotifyService
    {
        private readonly string _fireBaseUrl;
        private readonly FireBaseDB _fireBaseDB;
        public NotifyService(string fireBaseUrl)
        {
            _fireBaseUrl = EncryptHelper.Decrypt(fireBaseUrl, true);
            _fireBaseDB = new FireBaseDB(_fireBaseUrl);

        }
        public readonly ILogger _logger;
        public bool AddNotification(List<int> userIds, object data)
        {
            var response = new FirebaseResponse();
            var result = false;

            foreach (var userId in userIds)
            {
                FireBaseDB fireBaseDBInfoProject = _fireBaseDB.NodePath("notifications/u-" + userId);
                FirebaseResponse addProjectUserResponse = fireBaseDBInfoProject.Post(JsonConvert.SerializeObject(data, Formatting.None,
                    new JsonSerializerSettings
                    {
                        ContractResolver = new CamelCasePropertyNamesContractResolver()
                    }));
                result = addProjectUserResponse._success;

            }
            return result;
        }

        public bool AddLiveStream(object data)
        {
            var result = false;
            FireBaseDB fireBaseDBInfoProject = _fireBaseDB.NodePath("lives");
            FirebaseResponse addProjectUserResponse = fireBaseDBInfoProject.Post(JsonConvert.SerializeObject(data, Formatting.None,
                new JsonSerializerSettings
                {
                    ContractResolver = new CamelCasePropertyNamesContractResolver()
                }));
            result = addProjectUserResponse._success;
            return result;
        }

        public bool AddMessage(List<int> userIds, object data)
        {
            var response = new FirebaseResponse();
            var result = false;

            foreach (var userId in userIds)
            {
                FireBaseDB fireBaseDBInfoProject = _fireBaseDB.NodePath("Converation/messages");
                FirebaseResponse addProjectUserResponse = fireBaseDBInfoProject.Post(JsonConvert.SerializeObject(data, Formatting.None,
                    new JsonSerializerSettings
                    {
                        ContractResolver = new CamelCasePropertyNamesContractResolver()
                    }));
                result = addProjectUserResponse._success;
                var k = addProjectUserResponse._jsonContent;
            }
            return result;
        }
    }
}
