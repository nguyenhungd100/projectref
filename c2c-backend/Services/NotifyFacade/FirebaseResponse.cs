﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;

namespace Exam.Services.NotifyFacade
{
    public class FirebaseResponse
    {

        public bool _success { get; set; }
        public string _jsonContent { get; set; }
        public string _errorMessage { get; set; }
        public HttpResponseMessage _httpResponse { get; set; }
        public FirebaseResponse()
        {
        }

        public FirebaseResponse(bool success, string errorMessage, HttpResponseMessage httpResponse = null, string jsonContent = null)
        {
            _success = success;
            _jsonContent = jsonContent;
            _errorMessage = errorMessage;
            _httpResponse = httpResponse;
        }
    }
}
