﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;

namespace Exam.Services.NotifyFacade
{
    public class FireBaseDB
    {
        private string RootNode { set; get; }
        public FireBaseDB(string baseURL)
        {
            this.RootNode = baseURL;
        }

        public FireBaseDB(string baseURL, string pathToJSONKey, params string[] scopes)
        {
            this.RootNode = baseURL;
        }

        public FireBaseDB Node(string node)
        {
            if (node.Contains("/"))
            {
                throw new FormatException("Không được chứa ký tự '/', sử dung NodePath để thay thế ");
            }

            return new FireBaseDB(this.RootNode + '/' + node);
        }

        public FireBaseDB NodePath(string nodePath)
        {
            return new FireBaseDB(this.RootNode + '/' + nodePath);
        }

        public FirebaseResponse Get()
        {
            return new FirebaseRequest(HttpMethod.Get, this.RootNode).Execute();
        }

        public FirebaseResponse Put(string jsonData)
        {
            return new FirebaseRequest(HttpMethod.Put, this.RootNode, jsonData).Execute();
        }

        public FirebaseResponse Post(string jsonData)
        {
            return new FirebaseRequest(HttpMethod.Post, this.RootNode, jsonData).Execute();
        }

        public FirebaseResponse Patch(string jsonData)
        {
            return new FirebaseRequest(new HttpMethod("PATCH"), this.RootNode, jsonData).Execute();
        }

        public FirebaseResponse Delete()
        {
            return new FirebaseRequest(HttpMethod.Delete, this.RootNode).Execute();
        }

        public override string ToString()
        {
            return this.RootNode;
        }
    }
}
