﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;

namespace Exam.Services.NotifyFacade
{
    public class FirebaseRequest
    {
        private const string JSON_SUFFIX = ".json";
        private HttpMethod Method { set; get; }
        private string JSON { set; get; }
        private string Uri { set; get; }


        public FirebaseRequest(HttpMethod method, string uri, string jsonString = null)
        {
            this.Method = method;
            this.JSON = jsonString;
            if (uri.Replace("/", string.Empty).EndsWith("firebaseio.com"))
            {
                this.Uri = uri + '/' + JSON_SUFFIX;
            }
            else
            {
                this.Uri = uri + JSON_SUFFIX;
            }
        }

        public FirebaseResponse Execute()
        {
            Uri requestURI;
            if (FirebaseHelper.ValidateURI(this.Uri))
            {
                requestURI = new Uri(this.Uri);
            }
            else
            {
                return new FirebaseResponse(false, "Provided Firebase path is not a valid HTTP/S URL");
            }

            string json = null;
            if (this.JSON != null)
            {
                if (!FirebaseHelper.TryParseJSON(this.JSON, out json))
                {
                    return new FirebaseResponse(false, string.Format("Invalid JSON : {0}", json));
                }
            }

            var response = FirebaseHelper.RequestHelper(this.Method, requestURI, json);
            response.Wait();
            var k = response.Result.Content.ReadAsStringAsync();
            var result = response.Result;

            if (!result.IsSuccessStatusCode)
            {
                response = FirebaseHelper.RequestHelper(this.Method, requestURI, json);
                response.Wait();
                result = response.Result;
            }

            var firebaseResponse = new FirebaseResponse()
            {
                _httpResponse = result,
                _errorMessage = result.StatusCode.ToString() + " : " + result.ReasonPhrase,
                _success = response.Result.IsSuccessStatusCode
            };

            if (this.Method.Equals(HttpMethod.Get))
            {
                var content = result.Content.ReadAsStringAsync();
                content.Wait();
                firebaseResponse._jsonContent = content.Result;
            }

            return firebaseResponse;
        }
    }
}
