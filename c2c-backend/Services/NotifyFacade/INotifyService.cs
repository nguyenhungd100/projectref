﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Exam.Services.NotifyFacade
{
    public interface INotifyService
    {
        bool AddNotification(List<int> userIds, object data);

        bool AddLiveStream(object data);

        bool AddMessage(List<int> userIds, object data);
    }
}
