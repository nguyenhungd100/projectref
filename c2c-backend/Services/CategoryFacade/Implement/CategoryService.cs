﻿using Exam.CoreData;
using Exam.CoreData.Data.Entities;
using Exam.CoreData.Entities;
using Exam.CoreData.Models.AssignCategoryManager;
using Exam.CoreData.Models.Categories;
using Exam.CoreData.Models.ObjectOrienteds;
using Exam.CoreData.Models.PagingInfo;
using Exam.CoreData.Repository;
using Exam.Libraries.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Exam.Services.CategoryFacade.Implement
{
    public class CategoryService : ICategoryService
    {
        private readonly IRepository<Category, ExamDBContext> _categoryRepository;
        private readonly IRepository<ObjectType, ExamDBContext> _objectTypeRepository;
        private readonly IRepository<AssignCategoryManager, ExamDBContext> _assignCategoryManagerRepository;
        private readonly IRepository<User, ExamDBContext> _userRepository;

        public CategoryService(IRepository<Category, ExamDBContext> categoryRepository,
            IRepository<ObjectType, ExamDBContext> objectTypeRepository,
            IRepository<AssignCategoryManager, ExamDBContext> assignCategoryManagerRepository,
            IRepository<User, ExamDBContext> userRepository)
        {
            _categoryRepository = categoryRepository;
            _objectTypeRepository = objectTypeRepository;
            _assignCategoryManagerRepository = assignCategoryManagerRepository;
            _userRepository = userRepository;
        }
        
        public IEnumerable<CategoryModel> GetCategories()
        {
            var result = new List<CategoryModel>();
            var parents = _categoryRepository.FindAll(c=>c.ParentId == null);
            var parentIds = parents.Select(c => c.Id).ToList();
            var childrens = _categoryRepository.FindAll(c => c.ParentId != null && parentIds.Contains(c.ParentId.Value));

            return (from p in parents
                   select new CategoryModel
                   {
                       Id = p.Id,
                       Name = p.Name,
                       Description = p.Description,
                       ParentId = null,
                       DisplayOrder = p.DisplayOrder,
                       Status = p.Status,
                       CategoryChildrens = childrens.Where(c => c.ParentId == p.Id).ToList()?.CloneToListModels<Category, CategoryModel>()
                   }).OrderBy(c=>c.DisplayOrder);
        }

        public IEnumerable<ObjectTypeModel> GetObjectTypes(int categoryId)
        {
            var objects = _objectTypeRepository.FindAll(c => c.CategoryId == categoryId)?.OrderBy(c=>c.DisplayOrder);

            foreach (var item in objects)
            {
                yield return new ObjectTypeModel
                {
                    Id = item.Id,
                    Name = item.Name,
                    CategoryId = item.CategoryId,
                    DisplayOrder = item.DisplayOrder,
                };
            }           
        }

        public bool AssignCategoryManager(AssignCategoryManagerModel model)
        {
            var result = false;
            var existAssign = _assignCategoryManagerRepository.FindAll(c => c.CategoryId == model.CategoryId).FirstOrDefault();
            if (existAssign == null)
            {
                result = _assignCategoryManagerRepository.Insert(new AssignCategoryManager
                {
                    CategoryId = model.CategoryId,
                    UserId = model.UserId,
                    AssignTime = model.AssignTime,
                    AssignByUser = model.AssignByUser
                });
            }
            else
            {
                result = _assignCategoryManagerRepository.Update(new AssignCategoryManager
                {
                    Id = existAssign.Id,
                    CategoryId = model.CategoryId,
                    UserId = model.UserId,
                    AssignTime = model.AssignTime,
                    AssignByUser = model.AssignByUser
                });
            }
            return result;
        }

        public InfoUserAssignedModel GetUserAssignedByCategory(int categoryid)
        {
            var result = new InfoUserAssignedModel();
            var category = _categoryRepository.FindById(categoryid);
            if (category != null)
            {
                result.Category = category.CloneToModel<Category, CategoryModel>();
                var assignExist = _assignCategoryManagerRepository.FindAll(c => c.CategoryId == category.Id).FirstOrDefault();
                if (assignExist != null && assignExist.UserId.HasValue)
                {
                    var userAssigned = _userRepository.FindById(assignExist.UserId.Value);
                    if (userAssigned!= null)
                    {
                        result.UserAssignedEmail = userAssigned.Email;
                        result.UserAssignedMobile = userAssigned.Mobile;
                        result.UserAssignedName = userAssigned.FullName;
                        result.UserAssignedId = userAssigned.Id;
                    }
                }
            }
            return result;
        }

        public bool RevocationAssignCategory(int userAssignedId, int categoryId)
        {
            var result = false;
            var assignExist = _assignCategoryManagerRepository.FindAll(c => c.CategoryId == categoryId
                && c.UserId == userAssignedId).FirstOrDefault();
            if (assignExist == null)
            {
                throw new ServiceException("Tài khoản này chưa được phân giao guản lý cho danh mục đã chọn");
            }
            result = _assignCategoryManagerRepository.Delete(assignExist);
            return result;
        }
    }
}
