﻿using Exam.CoreData;
using Exam.CoreData.Data.Entities;
using Exam.CoreData.Models.AssignCategoryManager;
using Exam.CoreData.Models.Categories;
using Exam.CoreData.Models.ObjectOrienteds;
using Exam.CoreData.Models.PagingInfo;
using Exam.CoreData.Repository;
using System;
using System.Collections.Generic;
using System.Text;

namespace Exam.Services.CategoryFacade
{
    public interface ICategoryService
    {
        IEnumerable<CategoryModel> GetCategories();
        IEnumerable<ObjectTypeModel> GetObjectTypes(int categoryId);
        bool AssignCategoryManager(AssignCategoryManagerModel model);
        InfoUserAssignedModel GetUserAssignedByCategory(int categoryid);
        bool RevocationAssignCategory(int userAssignedId, int categoryId);
    }
}
