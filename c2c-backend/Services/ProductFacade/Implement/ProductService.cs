﻿using Exam.CoreData;
using Exam.CoreData.Data.Entities;
using Exam.CoreData.Repository;
using System;
using System.Collections.Generic;
using System.Text;

namespace Exam.Services.ProductFacade.Implement
{
    public class ProductService : IProductService
    {
        private readonly IRepository<Product, ExamDBContext> _productRepository;

        public ProductService(IRepository<Product, ExamDBContext> productRepository)
        {
            _productRepository = productRepository;
        }
    }
}
