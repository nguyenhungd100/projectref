﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Exam.Services.RoleFacade
{
    [AttributeUsage(AttributeTargets.All, Inherited = false, AllowMultiple = false)]
    public class PermissionDetailAttribute : Attribute
    {
        private readonly PermissionGroupCode _groupCode;
        private readonly string _name;

        public PermissionDetailAttribute(PermissionGroupCode groupCode, string name)
        {
            _groupCode = groupCode;
            _name = name;
        }

        public virtual string Name => _name;

        public virtual PermissionGroupCode GroupCode => _groupCode;
    }
}
