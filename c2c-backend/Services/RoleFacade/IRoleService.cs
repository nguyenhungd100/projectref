﻿using Exam.CoreData.Models.PagingInfo;
using Exam.CoreData.Models.Roles;
using System;
using System.Collections.Generic;
using System.Text;

namespace Exam.Services.RoleFacade
{
    public interface IRoleService
    {
        List<RoleModel> List(int[] roleIds);
        List<PermissionGroup> ListPermission();
        RoleFilterResult Filter(RoleFilterModel roleFilterModel);
        bool SaveRole(SaveRoleModel model);
        IEnumerable<RoleCodeModel> GetCodeRoles();
    }
}
