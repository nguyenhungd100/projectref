﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using Exam.CoreData;
using Exam.CoreData.Data.Entities;
using Exam.CoreData.Models.PagingInfo;
using Exam.CoreData.Models.Roles;
using Exam.CoreData.Repository;
using Exam.Libraries.Utils;

namespace Exam.Services.RoleFacade.Implement
{
    public class RoleService : IRoleService
    {
        private readonly IRepository<Role, ExamDBContext> _roleRepository;

        public RoleService()
        {
        }

        public RoleService(IRepository<Role, ExamDBContext> roleRepository)
        {
            _roleRepository = roleRepository;
        }

        public RoleFilterResult Filter(RoleFilterModel roleFilterModel)
        {
            var result = new RoleFilterResult();
            Expression<Func<Role, bool>> expression = c => c.Id > 0;
            if (!string.IsNullOrEmpty(roleFilterModel.RoleName))
            {
                expression = expression.And(c => c.RoleName.Contains(roleFilterModel.RoleName));
            }
            var entities = _roleRepository.FinAllPaging(new SearchModel
            {
                PageIndex = roleFilterModel.PageIndex,
                PageSize = roleFilterModel.PageSize
            },
                expression);
            result.Records = entities.Records.CloneToListModels<Role, RoleModel>();
            foreach (var record in result.Records)
            {
                if (!string.IsNullOrEmpty(record.Permissions))
                {
                    foreach (var code in Enum.GetValues(typeof(PermissionCode)).Cast<PermissionCode>())
                    {
                        var permissionIds = record.Permissions.Split(new string[] { ";" }, StringSplitOptions.RemoveEmptyEntries)
                            .Select(c => Convert.ToInt32(c)).ToList();
                        if (permissionIds.Contains((int)code))
                        {
                            record.PermissionInfo.Add((int)code, true);
                        }
                        else
                            record.PermissionInfo.Add((int)code, false);
                    }
                }
            }
            foreach (var code in Enum.GetValues(typeof(PermissionCode)).Cast<PermissionCode>())
            {
                var attr = code.GetAttributeOfType<PermissionDetailAttribute>();
                result.PermissionTitle.Add((int)code, attr.Name);
            }

            return new RoleFilterResult
            {
                Records = result.Records,
                TotalRecord = entities.TotalRecord,
                PageIndex = roleFilterModel.PageIndex,
                PageSize = roleFilterModel.PageSize,
                PermissionTitle = result.PermissionTitle
            };
        }

        public IEnumerable<RoleCodeModel> GetCodeRoles()
        {
            var roles = _roleRepository.FindAll();
            foreach (var role in roles)
            {
                yield return new RoleCodeModel
                {
                    RoleName = role.RoleName,
                    Code = role.Id
                };
            }
        }

        public List<RoleModel> List(int[] roleIds)
        {
            var role = _roleRepository.FindAll(c => roleIds.Contains(c.Id));
            return role.ToList().CloneToListModels<Role, RoleModel>();
        }

        public List<PermissionGroup> ListPermission()
        {
            var groups = new List<PermissionGroup>();
            var groupCodes = Enum.GetValues(typeof(PermissionGroupCode)).Cast<PermissionGroupCode>().ToList();
            foreach (var groupCode in groupCodes)
            {
                groups.Add(new PermissionGroup
                {
                    GroupId = (int)groupCode,
                    Name = groupCode.ToString()
                });
            }

            var permissions = new List<Permission>();
            foreach (var p in Enum.GetValues(typeof(PermissionCode)).Cast<PermissionCode>())
            {
                var attr = p.GetAttributeOfType<PermissionDetailAttribute>();
                permissions.Add(new Permission
                {
                    Code = p,
                    Name = attr.Name,
                    GroupId = (int)attr.GroupCode
                });
            }

            foreach (var group in groups)
            {
                var groupPermissions = permissions.Where(c => c.GroupId == group.GroupId);
                group.Permissions = new List<Permission>();
                group.Permissions.AddRange(groupPermissions);
            }
            return groups;
        }

        public bool SaveRole(SaveRoleModel model)
        {
            var result = false;
            var roleCheckIds = model.SaveRoleInfos.Select(c => c.RoleId);
            var roles = _roleRepository.FindAll(c => roleCheckIds.Contains(c.Id));
            foreach (var roleInfo in model.SaveRoleInfos)
            {
                var role = roles.FirstOrDefault(c => c.Id == roleInfo.RoleId);
                if (role != null)
                {
                    var permissionExistIds = role.Permissions.Split(new string[] {";"}, StringSplitOptions.RemoveEmptyEntries)
                       .Select(c => Convert.ToInt32(c)).ToList();
                    var permissionNew = new List<int>();
                    foreach (var permission in roleInfo.Data)
                    {
                        if (permission.Value == true)
                        {
                            permissionExistIds.Add(permission.Key);
                        }
                        else
                        {
                            var permissionExcept = new List<int> { permission.Key };
                            permissionExistIds = permissionExistIds.Except(permissionExcept).ToList();
                        }
                    }
                    
                    role.Permissions = string.Join(";", permissionExistIds?.Distinct());
                    if (string.IsNullOrEmpty(role.Permissions))
                    {
                        role.Permissions = ";";
                    }
                    else
                    {
                        role.Permissions = ";" + role.Permissions + ";";
                    }
                    result = _roleRepository.Update(role);
                }
            }
            return result;
            
        }
    }
}
