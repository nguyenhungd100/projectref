﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Exam.Services.RoleFacade
{
    public enum PermissionGroupCode
    {
        SYSTEM = 1,
        POST = 2
    }

    public enum PermissionCode
    {
        [PermissionDetail(PermissionGroupCode.SYSTEM, "Quản lý người dùng")]
        MANAGER_USERS = 1,
        [PermissionDetail(PermissionGroupCode.SYSTEM, "Quản lý vai trò")]
        MANAGER_ROLES = 5,

        [PermissionDetail(PermissionGroupCode.POST, "Đăng tin bài")]
        CREATE_POST = 50,
        [PermissionDetail(PermissionGroupCode.POST, "Đăng tin miễn phí")]
        CREATE_FREE_POST = 51,

        [PermissionDetail(PermissionGroupCode.POST, "Tạo cửa hàng")]
        CREATE_SHOP_PAGE = 70,
        [PermissionDetail(PermissionGroupCode.POST, "Tạo cửa hàng miễn phí")]
        CREATE_FREE_SHOP_PAGE = 71,

        [PermissionDetail(PermissionGroupCode.POST, "Duyệt tin bài")]
        APPROVEPOST = 101,
        [PermissionDetail(PermissionGroupCode.POST, "Từ chối tin bài")]
        REJECTPOST = 105,
        [PermissionDetail(PermissionGroupCode.POST, "Khóa tin bài")]
        LOCKPOST = 110,


    }
}
