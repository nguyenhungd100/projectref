﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using Exam.CoreData;
using Exam.CoreData.Data.Entities;
using Exam.CoreData.Entities;
using Exam.CoreData.Enums;
using Exam.CoreData.Models.Notifications;
using Exam.CoreData.Models.PagingInfo;
using Exam.CoreData.Models.Users;
using Exam.CoreData.Models.Wallets;
using Exam.CoreData.Repository;
using Exam.Libraries.Utils;
using Exam.Services;
using Exam.Services.NotifyFacade;
using Exam.Services.SendEmailFacade;
using Microsoft.Extensions.Caching.Memory;

namespace Services.UserFacade.Implement
{
    public class UserService : IUserService
    {
        private readonly IMemoryCache _cache;
        private readonly IRepository<User, ExamDBContext> _userRepository;
        private readonly IRepository<Role, ExamDBContext> _roleRepository;
        private readonly ISendEmailService _sendMailService;
        private readonly IRepository<Wallet, ExamDBContext> _walletRepository;
        private readonly IRepository<TransactionHistory, ExamDBContext> _transactionHistoryRepository;
        private readonly IRepository<ShopPage, ExamDBContext> _shopPageRepository;
        private readonly IRepository<Category, ExamDBContext> _categoryRepository;
        private readonly INotifyService _notifyService;

        public UserService(IMemoryCache cache,
            IRepository<User, ExamDBContext> userRepository,
            IRepository<Role, ExamDBContext> roleRepository,
            ISendEmailService sendMailService,
            IRepository<Wallet, ExamDBContext> walletRepository,
            IRepository<TransactionHistory, ExamDBContext> transactionHistoryRepository,
            IRepository<ShopPage, ExamDBContext> shopPageRepository,
            IRepository<Category, ExamDBContext> categoryRepository,
            INotifyService notifyService
            )
        {
            _cache = cache;
            _userRepository = userRepository;
            _roleRepository = roleRepository;
            _sendMailService = sendMailService;
            _walletRepository = walletRepository;
            _transactionHistoryRepository = transactionHistoryRepository;
            _shopPageRepository = shopPageRepository;
            _categoryRepository = categoryRepository;
            _notifyService = notifyService;
        }

        public BaseSearchResult<UserModel> SearchUser(SearchUserModel model)
        {
            model.SortDesc = true;
            Expression<Func<User, bool>> expressionWhere = c => c.Id > 0;
            //Theo email, tên hay mobile
            if (!string.IsNullOrEmpty(model.TextSearch))
            {
                expressionWhere = expressionWhere.And(a => a.FullName.Contains(model.TextSearch)
                    || a.Email.Contains(model.TextSearch)
                    || a.Mobile.Contains(model.TextSearch));
            }
            //Theo trạng thái
            if (model.Status.HasValue)
            {
                expressionWhere = expressionWhere.And(b => b.Status == model.Status.Value);
            }

            var result = new BaseSearchResult<UserModel>();

            var entities = _userRepository.FinAllPaging(
                    new SearchModel { PageIndex = model.PageIndex, PageSize = model.PageSize },
                    expressionWhere, null
                );

            var userResults = entities.Records?.CloneToListModels<User, UserModel>();

            return new BaseSearchResult<UserModel>
            {
                Records = userResults,
                TotalRecord = entities.TotalRecord,
                PageIndex = model.PageIndex,
                PageSize = model.PageSize
            };
        }

        public UserModel GetById(int userId)
        {
            var result = new UserModel();
            var entity = _userRepository.FindById(userId);
            var roleAll = entity.UserRoles.Split(new string[] { ";" }, StringSplitOptions.RemoveEmptyEntries)
                    .Select(c => Convert.ToInt32(c)).ToList();
            var roleMin = 100000;
            if (roleAll.Count() > 0)
                roleMin = roleAll.Min();
            var role = _roleRepository.FindById(roleMin);
            result = entity?.CloneToModel<User, UserModel>();
            if (role != null)
            {
                result.DisplayOrder = role.DisplayOrder ?? 100000;
            }
            //var walletInUse = _walletRepository.FirstOrDefault(c => c.UserId == userId);
            //if (walletInUse != null)
            //{
            //    result.Wallet = walletInUse.CloneToModel<Wallet, WalletModel>();
            //}
            var transactionHistory = _transactionHistoryRepository.FindAll(c => c.UserId == userId);
            if (transactionHistory.Count() > 0)
            {
                var recharge = transactionHistory.Where(a => a.TransactionType == TransactionType.Recharge
                        && a.Status == TransactionStatus.Success)
                    .Sum(c => c.TransactionAmount);

                var deduct = transactionHistory.Where(a => a.TransactionType == TransactionType.Deduct
                        && a.Status == TransactionStatus.Success)
                    .Sum(c => c.TransactionAmount);
                result.AssetsTotal = recharge - deduct;
            }
            if (result.ShopPageId.HasValue)
            {

            }
            var shopPage = _shopPageRepository.FirstOrDefault(c => c.CreatedBy == userId);
            if (shopPage != null)
            {
                result.ShopPageId = shopPage.Id;
                result.ShopPageName = shopPage.Name;
                var category = _categoryRepository.FindById(shopPage.CategoryId);
                result.CategoryId = category.Id;
                result.CategoryName = category.Name;
            }
            return result;
        }

        public (bool, User) CreateUserByAdmin(CreateUserModel model)
        {
            if (_userRepository.FirstOrDefault(c => c.Mobile == model.Mobile) != null && !string.IsNullOrEmpty(model.Mobile))
            {
                throw new ServiceException("Số điện thoại đã tồn tại");
            }

            if (_userRepository.FirstOrDefault(c => c.Email == model.Email) != null && !string.IsNullOrEmpty(model.Email))
            {
                throw new ServiceException("Email đã tồn tại");
            }

            var user = new User
            {
                FullName = model.FullName,
                Password = EncryptHelper.HassPasswordBCrypt(model.Password),
                Mobile = model.Mobile,
                Email = model.Email,
                Gender = model.Gender,
                Status = UserStatus.Actived,
                UserRoles = ";",
                DisplayOrder = 1,
                OnCms = !model.IsUserSide,
                DateOfBirth = model.DateOfBirth,

            };
            var result = _userRepository.Insert(user);
            if (result && !user.OnCms.Value)
            {
                result = _transactionHistoryRepository.Insert(new TransactionHistory
                {
                    UserId = user.Id,
                    TransactionAmount = 10000,
                    TransactionTime = DateTime.Now,
                    TransactionType = TransactionType.Recharge,
                    Status = TransactionStatus.Success
                });
            }

            if (result)
            {
                var userAdd = _userRepository.FindById(model.PersonAdd);
                var listUserIds = new List<int> { user.Id, model.PersonAdd };
                var k = _notifyService.AddNotification(listUserIds, new AccountNotifyModel
                {
                    Type = EventType.AddUser,
                    PersonAddedId = user.Id,
                    PersonAddedName = user.FullName,
                    PersonAddId = userAdd.Id,
                    PersonAddName = userAdd.FullName,
                });
                return (true, user);
            }
            else
                return (false, null);
        }

        public bool LockAccount(int userId)
        {
            var userExist = _userRepository.FindById(userId);
            if (userExist == null)
                throw new ServiceException("Tài khoản không tồn tại");
            userExist.Status = UserStatus.Disabled;
            return _userRepository.Update(userExist);
        }

        public void CreateNotifyFromBoPhanHoTro(int bphtId, int newUserId)
        {
            var userIds = new List<int> { newUserId };
            _notifyService.AddMessage(userIds, new 
            {
                content = "Bộ phận hỗ trợ khách hàng xin kính chào quý khách.",
                sender = new { 
                    id = bphtId,
                    imageId = "74e06a49-f3bf-4e3d-bf52-f025fcda254f",
                    name = "Bộ phận hỗ trợ"
                },
                senderId = bphtId
            });
        }

        public bool CreateUserSide(CreateUserModel model)
        {          
            var result = false;
            using (var context = _userRepository.GetDBContext())
            {
                using (var trans = context.Database.BeginTransaction())
                {
                    try
                    {
                        if (_userRepository.FirstOrDefault(c => c.Mobile == model.Mobile) != null && !string.IsNullOrEmpty(model.Mobile))
                        {
                            throw new ServiceException("Số điện thoại đã tồn tại");
                        }

                        if (_userRepository.FirstOrDefault(c => c.Email == model.Email) != null && !string.IsNullOrEmpty(model.Email))
                        {
                            throw new ServiceException("Email đã tồn tại");
                        }

                        var user = new User
                        {
                            FullName = model.FullName,
                            Password = EncryptHelper.HassPasswordBCrypt(model.Password),
                            Mobile = model.Mobile,
                            Email = model.Email,
                            Gender = model.Gender,
                            Status = UserStatus.NotActived,
                            UserRoles = ";2;",
                            DisplayOrder = 1,
                            OnCms = false,
                            DateOfBirth = model.DateOfBirth
                        };
                        result = _userRepository.Insert(user);
                        if (result)
                        {
                            _transactionHistoryRepository.Insert(new TransactionHistory
                            {
                                UserId = user.Id,
                                TransactionAmount = 10000,
                                TransactionTime = DateTime.Now,
                                TransactionType = TransactionType.Recharge,
                                Status = TransactionStatus.Success
                            });
                            result = SendOTPLoginToEmail(user.Email);
                            //if (result)
                            //{
                            //    CreateNotifyFromBoPhanHoTro(16, user.Id);
                            //}
                        };

                        context.SaveChanges();
                        trans.Commit();
                    }
                    catch (Exception ex)
                    {
                        trans.Rollback();
                        throw;
                    }
                }
            }
            return result;
        }

        private bool SendOTPLoginToEmail(string email)
        {
            var result = false;
            var random = new Random();
            var code = random.Next(111111, 999999).ToString("D6");
            var statusSend = _sendMailService.SendOTPVerifyAccount(email, code);
            if (statusSend)
            {
                var cacheEntry = new MemoryCacheEntryOptions()
                    .SetSlidingExpiration(TimeSpan.FromMinutes(5));
                var otp = _cache.Set(email, code, cacheEntry);
                return result = true;
            }
            return result;
        }

        private bool VerifyOTPLogin(string email, string otp)
        {
            var userCache = _cache.Get(email);
            if (userCache == null)
                throw new ServiceException("Mã xác nhận không tồn tại hoặc quá hạn 5 phút");
            else
            {
                if (userCache.ToString() == otp)
                    return true;
                else
                    throw new ServiceException("Mã xác nhận không đúng");
            }
        }

        public (bool, User) ActiveAccount(VerifyOTPModel model)
        {
            var result = false;
            var user = _userRepository.FirstOrDefault(c => c.Email == model.Email);
            if (user != null)
            {
                if (user.Status == UserStatus.Actived)
                {
                    throw new ServiceException("Tài khoản đã được kích hoạt");
                }
                if (VerifyOTPLogin(user.Email, model.OTP))
                {
                    user.Status = UserStatus.Actived;
                    result = _userRepository.Update(user);
                    if (result)
                    {
                        return (true, user);
                    }
                    else
                        return (false, null);
                }
                else throw new ServiceException("Tài khoản chưa được xác thực");
            }
            else throw new ServiceException("Tài khoản chưa đăng ký");
        }

        public bool ResendOtpLogin(string email)
        {
            return SendOTPLoginToEmail(email);
        }

        public bool UpdateMyProfile(UpdateMyProfileModel model)
        {
            var exist = _userRepository.FindById(model.Id);
            exist.FullName = model.FullName;
            exist.Email = model.Email;
            exist.Mobile = model.Mobile;
            exist.Gender = model.Gender;
            exist.DateOfBirth = model.DateOfBirth;
            return _userRepository.Update(exist);
        }

        public bool ChangePassword(ChangePasswordModel model)
        {
            var result = false;
            var userExist = _userRepository.FirstOrDefault(c => c.Email == model.Email);
            if (userExist == null)
            {
                throw new ServiceException("Tài khoản không tồn tại");
            }
            userExist.Password = EncryptHelper.HassPasswordBCrypt(model.Password);
            result = _userRepository.Update(userExist);
            return result;
        }

        public List<UserModel> GetUsersHasPermissions(int permissionId)
        {
            var result = new List<UserModel>();
            var roles = _roleRepository.FindAll(c => c.Permissions.Contains(";" + permissionId.ToString() + ";"));
            var roleIds = roles.Select(a => a.Id);

            var users = new List<User>();
            foreach (var roleId in roleIds)
            {
                var userIns = _userRepository.FindAll(a => a.UserRoles.Contains(";"+roleId+";"));
                users.AddRange(userIns);
            }
            foreach (var user in users)
            {
                if (!result.Any(a=>a.Id == user.Id))
                {
                    result.Add(user.CloneToModel<User, UserModel>());
                }
            }
            
            return result;
        }

        public MemoryStream ExportToExcel()
        {
            var users = _userRepository.FindAll().ToList();
            var rowData = new Dictionary<string, List<(string, string, int)>>();
            foreach (var user in users)
            {
                rowData.Add(user.Id.ToString(), new List<(string, string,int)> {
                    (user.FullName, "Họ tên", 50),
                    (user.Email,"Email" ,40),
                    (user.Mobile,"SĐT",30),
                    (user.DateOfBirth?.ToString("dd/MM/yyyy"),"Ngày sinh",30 )
                });
            }
            
            return new ExcelExtensions().ExportDataToExcel(new ExcelBindingInfo { Rows = rowData, RowsHeight = 20 });
           
        }
    }
}
