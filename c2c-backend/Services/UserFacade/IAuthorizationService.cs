﻿using Exam.CoreData.Enums;
using Exam.CoreData.Models.Users;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Exam.Services.UserFacade
{
    public interface IAuthorizationService
    {
        Task<UserModel> GetByIdAsync(int userId);
        VerifyLoginMessageEnum VerifyPassword(string userName, string password, ref UserModel userModel);
        UserModel GetUser(string userName);
    }
}
