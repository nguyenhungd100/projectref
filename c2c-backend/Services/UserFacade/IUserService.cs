﻿using Exam.CoreData.Entities;
using Exam.CoreData.Models.PagingInfo;
using Exam.CoreData.Models.Users;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace Services.UserFacade
{
    public interface IUserService
    {
        BaseSearchResult<UserModel> SearchUser(SearchUserModel model);
        UserModel GetById(int userId);
        (bool, User) CreateUserByAdmin(CreateUserModel model);
        bool LockAccount(int userId);
        bool CreateUserSide(CreateUserModel model);
        (bool, User) ActiveAccount(VerifyOTPModel model);
        bool ResendOtpLogin(string email);
        bool UpdateMyProfile(UpdateMyProfileModel model);
        bool ChangePassword(ChangePasswordModel model);
        List<UserModel> GetUsersHasPermissions(int permissionId);
        MemoryStream ExportToExcel();
    }
}
