﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Exam.CoreData.Models.Images;

namespace Exam.Services.ImageFacade.Implement
{
    public interface IImageService
    {
        Task<(byte[] data, string contentType, string fileName)> GetImageByIdAsync(string imageId, int width, int height);
        string SaveImage(SaveImageLibraryModel model);
    }
}
