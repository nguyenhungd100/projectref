﻿using Exam.CoreData;
using Exam.CoreData.Data.Entities;
using Exam.CoreData.Entities;
using Exam.CoreData.Models.Images;
using Exam.CoreData.Repository;
using Exam.Libraries.Utils;
using Exam.Services.ImageFacade.Implement;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading.Tasks;

namespace Exam.Services.ImageFacade
{
    public class ImageService : IImageService
    {
        private readonly IRepository<User, ExamDBContext> _userRepository;
        private readonly IRepository<ShopPage, ExamDBContext> _shopPageRepository;
        private readonly IRepository<ImageLibrary, ExamDBContext> _imageLibraryRepository;

        public ImageService(IRepository<User, ExamDBContext> userRepository,
            IRepository<ShopPage, ExamDBContext> shopPageRepository,
            IRepository<ImageLibrary, ExamDBContext> imageLibraryRepository)
        {
            _userRepository = userRepository;
            _shopPageRepository = shopPageRepository;
            _imageLibraryRepository = imageLibraryRepository;
        }

        public async Task<(byte[] data, string contentType, string fileName)> GetImageByIdAsync(string imageId, int width, int height)
        {
            if (!string.IsNullOrEmpty(imageId))
            {
                var imageIdParse = Guid.Parse(imageId);
                var postImage = await _imageLibraryRepository.FirstOrDefaultAsync(c => c.Id == imageIdParse);
                if (postImage != null)
                {
                    using (var ms = new MemoryStream(postImage.Data))
                    {
                        var image = System.Drawing.Image.FromStream(ms);
                        var bitmapConvert = ImageHelper.ResizeImageByteArray(image, width, height);

                        return (ImageHelper.ConvertBitMapToByteArray(bitmapConvert), postImage.Mime, postImage.Description);
                    };
                }
            }
            return (null, null, null);
        }

        public string SaveImage(SaveImageLibraryModel model)
        {
            var result = string.Empty;

            var contentType = model.Data.ContentType;
            if (!string.Equals(contentType, "image/jpg", StringComparison.OrdinalIgnoreCase) &&
                !string.Equals(contentType, "image/jpeg", StringComparison.OrdinalIgnoreCase) &&
                !string.Equals(contentType, "image/pjpeg", StringComparison.OrdinalIgnoreCase) &&
                !string.Equals(contentType, "image/gif", StringComparison.OrdinalIgnoreCase) &&
                !string.Equals(contentType, "image/x-png", StringComparison.OrdinalIgnoreCase) &&
                !string.Equals(contentType, "image/png", StringComparison.OrdinalIgnoreCase))
            {
                throw new ServiceException("Yêu cầu nhập đúng file ảnh !");
            }
            
            using (var ms = new MemoryStream())
            {
                byte[] fileByteImage = new byte[model.Data.Length];
                model.Data.CopyTo(ms);
                fileByteImage = ms.ToArray();
                using (var bitmap = new System.Drawing.Bitmap(model.Data.OpenReadStream()))
                {
                    var entity = new ImageLibrary
                    {
                        Id = Guid.NewGuid(),
                        Data = fileByteImage,
                        Description = model.Description,
                        CreatedDate = DateTime.Now,
                        Mime = model.Data.ContentType,
                        Size = (int)model.Data.Length / 1024,
                        Width = bitmap.Width,
                        Height = bitmap.Height
                    };
                    var status = _imageLibraryRepository.Insert(entity);
                    if (status)
                    {
                        result = entity.Id.ToString();
                    }

                    if (model.ShopPageId.HasValue)
                    {
                        var shopPage = _shopPageRepository.FindById(model.ShopPageId.Value);
                        if (shopPage == null)
                        {
                            throw new ServiceException("Cửa hàng không tồn tại");
                        }
                        shopPage.ImageId = entity.Id;
                        _shopPageRepository.Update(shopPage);
                    }

                    if (model.UserId.HasValue)
                    {
                        var user = _userRepository.FindById(model.UserId.Value);
                        if (user == null)
                        {
                            throw new ServiceException("Tài khoản không tồn tại");
                        }
                        user.ImageId = entity.Id;
                        _userRepository.Update(user);
                    }
                }
            }
            return result;
        }
    }
}
