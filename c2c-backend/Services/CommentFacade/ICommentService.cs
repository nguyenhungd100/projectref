﻿using System;
using System.Collections.Generic;
using System.Text;
using Exam.CoreData.Models.Comments;
using Exam.CoreData.Models.PagingInfo;

namespace Exam.Services.CommentFacade
{
    public interface ICommentService
    {
        IEnumerable<CommentModel> SearchComment(SearchCommentModel model);
        bool SaveComment(SaveCommentModel model);
        bool DeleteComment(int id, int userId);
        bool LikeComment(int id, int userId, bool status);
        bool DisLikeComment(int id, int userId, bool status);
    }
}
