﻿using Exam.CoreData;
using Exam.CoreData.Data.Entities;
using Exam.CoreData.Entities;
using Exam.CoreData.Enums;
using Exam.CoreData.Models.Comments;
using Exam.CoreData.Models.PagingInfo;
using Exam.CoreData.Repository;
using Exam.Libraries.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;

namespace Exam.Services.CommentFacade.Implement
{
    public class CommentService : ICommentService
    {
        private readonly IRepository<Comment, ExamDBContext> _commentRepository;
        private readonly IRepository<User, ExamDBContext> _userRepository;
        private readonly IRepository<CommentUser, ExamDBContext> _commentUserRepository;

        public CommentService(IRepository<Comment, ExamDBContext> commentRepository,
            IRepository<User, ExamDBContext> userRepository,
            IRepository<CommentUser, ExamDBContext> commentUserRepository)
        {
            _commentRepository = commentRepository;
            _userRepository = userRepository;
            _commentUserRepository = commentUserRepository;
        }

        public IEnumerable<CommentModel> SearchComment(SearchCommentModel model)
        {
            Expression<Func<Comment, bool>> expressionWhere = c => c.Status == CommentStatus.Active;
            //Theo bài đăng
            if (model.PostId.HasValue)
            {
                expressionWhere = expressionWhere.And(a => a.PostId == model.PostId.Value);
            }
            var entities = _commentRepository.FindAll(expressionWhere).ToList();
            var commentResults = entities?.CloneToListModels<Comment, CommentModel>();

            if (entities.Count() > 0)
            {
                var createdUserIds = entities.Select(c => c.CreatedBy);
                var createdUsers = _userRepository.FindAll(c => createdUserIds.Contains(c.Id));

                //Lấy xác định được like, dislike comment nếu user đang đăng nhập
                var commentUsers = new List<CommentUser>();
                var commentResultIds = commentResults.Select(c => c.Id);
                if (commentResultIds.Count() > 0 && model.UserCurrentId.HasValue)
                {
                    commentUsers = _commentUserRepository.FindAll(c => commentResultIds.Contains(c.CommentId)).ToList();
                }

                var commentForUserCurrent = commentUsers.Where(c => c.UserId == model.UserCurrentId);
                foreach (var comment in commentResults)
                {
                    var createdUser = createdUsers.FirstOrDefault(c => c.Id == comment.CreatedBy);
                    if (createdUser != null)
                    {
                        comment.CreatedByEmail = createdUser.Email;
                        comment.CreatedByName = createdUser.FullName;
                        comment.CreatedMobile = createdUser.Mobile;
                        comment.IsOfUserCurrent = (model.UserCurrentId.HasValue && comment.CreatedBy == model.UserCurrentId.Value) ? true : false;
                    }
                    var commentForUser = commentForUserCurrent.FirstOrDefault(c => c.CommentId == comment.Id);

                    //Xác định có like hay dislike với người đang đăng nhập
                    if (commentForUser != null)
                    {
                        comment.IsLike = commentForUser.IsLike;
                        comment.IsDislike = commentForUser.IsDisLike;
                    }

                    comment.LikeCount = commentUsers.Count(c => c.IsLike && c.CommentId == comment.Id);
                    comment.DislikeCount = commentUsers.Count(c => c.IsDisLike && c.CommentId == comment.Id);
                }
            }

            var parentFirsts = commentResults.Where(c => c.ParentId == null).ToList();
            foreach (var parent in parentFirsts)
            {
                parent.CommentChildrens = GetCommentChildrens(parent.Id, commentResults);
            }
            return parentFirsts;
        }

        public List<CommentModel> GetCommentChildrens(int parentId, List<CommentModel> commentAll)
        {
            var childrens = commentAll.Where(c => c.ParentId == parentId).ToList();
            if (childrens.Count() > 0)
            {
                foreach (var child in childrens)
                {
                    child.CommentChildrens = GetCommentChildrens(child.Id, commentAll);
                }
            }
            return childrens;
        }

        public bool SaveComment(SaveCommentModel model)
        {
            var result = false;
            if (string.IsNullOrEmpty(model.Content))
            {
                throw new ServiceException("Yêu cầu nhập nội dung bình luận");
            }
            if (model.Id > 0)
            {
                var commentExist = _commentRepository.FindById(model.Id);
                if (commentExist == null)
                {
                    throw new ServiceException("Bình luận này không tồn tại");
                }
                if (commentExist.CreatedBy != model.CreatedBy)
                {
                    throw new ServiceException("Bạn không có quyền sửa bình luận của người khác");
                }
                commentExist.Content = model.Content;
                result = _commentRepository.Update(commentExist);
            }
            else
            {
                result = _commentRepository.Insert(new Comment
                {
                    Content = model.Content,
                    PostId = model.PostId,
                    CreatedDate = DateTime.UtcNow,
                    CreatedBy = model.CreatedBy,
                    ParentId = model.ParentId,
                    Status = CommentStatus.Active
                });
            }
            return result;
        }

        public bool DeleteComment(int id, int userId)
        {
            var result = false;
            var commentExist = _commentRepository.FindById(id);
            if (commentExist.CreatedBy != userId)
            {
                throw new ServiceException("Bạn không có quyền xóa bình luận của người khác");
            }
            if (commentExist == null)
            {
                throw new ServiceException("Bình luận không tồn tại");
            }
            if (commentExist.Status == CommentStatus.Deleted)
            {
                throw new ServiceException("Bình luận đã được xóa trước đó");
            }
            commentExist.Status = CommentStatus.Deleted;
            result = _commentRepository.Update(commentExist);
            return result;
        }

        public bool LikeComment(int id, int userId, bool status)
        {
            var result = false;
            var commentExist = _commentRepository.FindById(id);
            if (commentExist == null)
            {
                throw new ServiceException("Không tồn tại bình luận này");
            }

            var commentUserExist = _commentUserRepository.FirstOrDefault(c => c.CommentId == id && c.UserId == userId);
            if (commentUserExist == null)
            {
                result = _commentUserRepository.Insert(new CommentUser
                {
                    UserId = userId,
                    CommentId = id,
                    IsLike = status,
                    IsDisLike = false,
                });
            }
            else
            {
                commentUserExist.IsLike = status;
                commentUserExist.IsDisLike = false;
                result = _commentUserRepository.Update(commentUserExist);
            }
            return result;
        }

        public bool DisLikeComment(int id, int userId, bool status)
        {
            var result = false;
            var commentExist = _commentRepository.FindById(id);
            if (commentExist == null)
            {
                throw new ServiceException("Không tồn tại bình luận này");
            }

            var commentUserExist = _commentUserRepository.FirstOrDefault(c => c.CommentId == id && c.UserId == userId);
            if (commentUserExist == null)
            {
                result = _commentUserRepository.Insert(new CommentUser
                {
                    UserId = userId,
                    CommentId = id,
                    IsLike = false,
                    IsDisLike = status,
                });
                if (result)
                {
                    result = _commentRepository.Update(commentExist);
                }
            }
            else
            {
                commentUserExist.IsLike = false;
                commentUserExist.IsDisLike = status;
                result = _commentUserRepository.Update(commentUserExist);
            }

            return result;
        }
    }
}
