﻿using System;
using System.Collections.Generic;
using System.Text;
using Exam.CoreData.Models.PagingInfo;
using Exam.CoreData.Models.Specifications;

namespace Exam.Services.SpecificationFacade
{
    public interface ISpecificationService
    {
        BaseSearchResult<SpecificationModel> SearchSpecification(SearchSpecificationModel model);
        bool SaveSpecfication(SaveSpecificationModel model);
        IEnumerable<SpecificationModel> GetSpecficationByCategoryId(int categoryid);
    }
}
