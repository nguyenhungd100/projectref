﻿using Exam.CoreData;
using Exam.CoreData.Data.Entities;
using Exam.CoreData.Models.PagingInfo;
using Exam.CoreData.Models.Specifications;
using Exam.CoreData.Repository;
using Exam.Libraries.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;

namespace Exam.Services.SpecificationFacade.Implement
{
    public class SpecificationService : ISpecificationService
    {
        private readonly IRepository<Specification, ExamDBContext> _specificationRepository;

        public SpecificationService(IRepository<Specification, ExamDBContext> specificationRepository)
        {
            _specificationRepository = specificationRepository;
        }    

        public BaseSearchResult<SpecificationModel> SearchSpecification(SearchSpecificationModel model)
        {

            model.SortDesc = true;
            Expression<Func<Post, dynamic>> expressionOrder = c => c.CreatedDate;

            Expression<Func<Post, bool>> expressionWhere = c => c.Id > 0;

            if (model.CategoryId.HasValue)
            {
                expressionWhere = c => c.CategoryId == model.CategoryId.Value;
            }

            var result = new BaseSearchResult<SpecificationModel>();

            var entities = _specificationRepository.FinAllPaging(
                new SearchModel { PageIndex = model.PageIndex, PageSize = model.PageSize },
                c => c.Id > 0, null
                );
            return new BaseSearchResult<SpecificationModel>
            {
                Records = entities.Records?.CloneToListModels<Specification, SpecificationModel>(),
                TotalRecord = entities.TotalRecord,
                PageIndex = model.PageIndex,
                PageSize = model.PageSize
            };
        }

        public bool SaveSpecfication(SaveSpecificationModel model)
        {
            var result = false;
            if (model.Id > 0)
            {
                var specExist = _specificationRepository.FindById(model.Id);
                if (specExist == null)
                {
                    throw new ServiceException("Thông số kỹ thuật không tồn tại !");
                }
                result = _specificationRepository.Update(new Specification
                {
                    Id = model.Id,
                    Name = model.Name,
                    Type = model.Type,
                    CatetgoryId = model.CatetgoryId,
                    DisplayOrder = model.DisplayOrder
                });
            }
            else
            {
                result = _specificationRepository.Insert(new Specification
                {
                    Name = model.Name,
                    Type = model.Type,
                    CatetgoryId = model.CatetgoryId,
                    DisplayOrder = model.DisplayOrder
                });
            }
            return result;
        }

        public IEnumerable<SpecificationModel> GetSpecficationByCategoryId(int categoryid)
        {
            return _specificationRepository.FindAll(c => c.CatetgoryId == categoryid)
                ?.ToList()?.CloneToListModels<Specification, SpecificationModel>();
        }
    }
}
