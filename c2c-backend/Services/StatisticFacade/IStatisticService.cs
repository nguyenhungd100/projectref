﻿using System;
using System.Collections.Generic;
using System.Text;
using Exam.CoreData.Models.Statistics;

namespace Exam.Services.StatisticFacade
{
    public interface IStatisticService
    {
        StatisticResultModel StatisticGeneral(StatisticGeneralModel model);
        StatisticRevenueModel StatisticRevenue(StatisticGeneralModel model);
    }
}
