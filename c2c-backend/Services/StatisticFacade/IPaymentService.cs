﻿using Exam.Services.TransactionFacade;
using System;
using System.Collections.Generic;
using System.Text;

namespace Exam.Services.StatisticFacade
{
    public interface IPaymentService
    {
        ResponseInfo SendRequestIntegratedPayment(string bankCode, string buyer_fullname,
            string buyer_email, string buyer_mobile, string order_code, string v);
    }
}
