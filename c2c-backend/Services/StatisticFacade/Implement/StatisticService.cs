﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Exam.CoreData;
using Exam.CoreData.Data.Entities;
using Exam.CoreData.Models.Statistics;
using Exam.CoreData.Repository;

namespace Exam.Services.StatisticFacade.Implement
{
    public class StatisticService : IStatisticService
    {
        private readonly IRepository<ShopPage, ExamDBContext> _shopPageRepository;
        private readonly IRepository<Post, ExamDBContext> _postRepository;
        private readonly IRepository<TransactionHistory, ExamDBContext> _transactionHistoryRepository;

        public StatisticService(IRepository<ShopPage, ExamDBContext> shopPageRepository,
            IRepository<Post, ExamDBContext> postRepository,
            IRepository<TransactionHistory, ExamDBContext> transactionHistoryRepository
            )
        {
            _shopPageRepository = shopPageRepository;
            _postRepository = postRepository;
            _transactionHistoryRepository = transactionHistoryRepository;
        }

        public StatisticResultModel StatisticGeneral(StatisticGeneralModel model)
        {
            var result = new StatisticResultModel();
            var months = new int[] { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12 };
            var posts = _postRepository.FindAll(c => c.CreatedDate.Year == model.Year);
            var shopPages = _shopPageRepository.FindAll(c => c.CreatedDate.Year == model.Year);
            foreach (var month in months)
            {
                result.StatisticInfos.Add(new StatisticInfo
                {
                    Month = month,
                    NumberOfPosts = posts.Where(c => c.CreatedDate.Month == month).Count(),
                    NumberOfShopCreated = shopPages.Where(c => c.CreatedDate.Month == month).Count()
                });
            }
            result.TotalNumberOfPosts = result.StatisticInfos.Select(c => c.NumberOfPosts).Sum();
            result.TotalNumberOfShops = result.StatisticInfos.Select(c => c.NumberOfShopCreated).Sum();
            return result;
        }

        public StatisticRevenueModel StatisticRevenue(StatisticGeneralModel model)
        {
            var result = new StatisticRevenueModel();
            var months = new int[] { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12 };
            var transactions = _transactionHistoryRepository.FindAll(c => c.Status == TransactionStatus.Success);
            foreach (var month in months)
            {
                var revenueInMonth = transactions.Where(c => c.TransactionTime.Month == month 
                    && c.TransactionType == TransactionType.Recharge)?.Sum(c => c.TransactionAmount);

                result.StatisticRevenueInfos.Add(new StatisticRevenueInfo
                {
                    Month = month,
                    Revenue = revenueInMonth.HasValue ? revenueInMonth.Value : 0
                });
            }
            return result;
        }
    }
}
