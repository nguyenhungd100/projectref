﻿using Exam.Libraries.Utils;
using Exam.Services.TransactionFacade;
using System;
using System.Collections.Generic;
using System.Text;

namespace Exam.Services.StatisticFacade.Implement
{
    public class PaymentService : IPaymentService
    {
        private readonly string _email;
        private readonly string _merChantId;
        private readonly string _merChantPass;
        private readonly string _cancel_Url;
        private readonly string _return_Url;



        public PaymentService(string email, string merChantId, string merChantPass, string return_url, string cancel_url)
        {
            _email = email;
            _merChantId = merChantId;
            _merChantPass = EncryptHelper.Decrypt(merChantPass, true);
            _cancel_Url = cancel_url;
            _return_Url = return_url;
        }

        public ResponseInfo SendRequestIntegratedPayment(string bankCode, string totalAmount, string buyer_fullname,
            string buyer_email, string buyer_mobile, string order_code )
        {
            var info = new RequestInfo
            {
                Merchant_id = _merChantId,
                Merchant_password = _merChantPass,
                Receiver_email = _email,

                cur_code = "vnd",
                bank_code = bankCode,
                Total_amount = totalAmount,

                fee_shipping = "0",
                Discount_amount = "0",
                order_description = "Thanh toán đơn hàng tại C2C",
                return_url = _return_Url,
                cancel_url = _cancel_Url,

                Buyer_fullname = buyer_fullname,
                Buyer_email = buyer_email,
                Buyer_mobile = buyer_mobile,
                Order_code = order_code
            };
            APICheckoutV3 objNLChecout = new APICheckoutV3();
            ResponseInfo result = objNLChecout.GetUrlCheckout(info, "ATM_ONLINE");
            return result;
        }
    }
}
