﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Exam.CoreData.Models;
using Exam.CoreData.Models.Images;
using Exam.CoreData.Models.PagingInfo;
using Exam.CoreData.Models.Posts;
using Exam.CoreData.Models.ShopPages;

namespace Exam.Services.PostFacade
{
    public interface IPostService
    {
        BaseSearchResult<PostModel> SearchPost(SearchPostModel model);
        bool CaringPost(CaringPostModel model);
        PostModel GetPostById(int id);
        bool SavePost(SavePostModel model, bool isFreePost);
        bool DeleteManyPost(DeleteGenericModel model);      
        bool ApprovePost(int id, int approver);
        bool RejectPost(int id, int approver);
        Task<IEnumerable<Guid>> GetPostImageIdsAsync(int postId);
        Task<(byte[] data, string contentType, string fileName)> GetPostImageByImageIdAsync(string imageId, int width, int height);
        string SavePostImage(SavePostImageModel model);
        Task<bool> DeletePostImageAsync(string imageId);
        BaseSearchResult<ShopPageModel> SearchShopPage(SearchShopPageModel model);
        ShopPageModel GetShopPageById(int id);

        int SaveShopPage(SaveShopPageModel model, int userCurrent, bool isFreeCreateShop);
        bool SaveFavorite(int score, int postId,int userId);
        BaseSearchResult<DenouncePostModel> GetDenouncePost(SearchDenounceModel model);

        bool DenouncePost(DenounceModel model);
        bool LockPost(LockPostModel model);
    }
}
