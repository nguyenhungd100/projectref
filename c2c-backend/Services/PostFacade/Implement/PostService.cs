﻿using Exam.CoreData;
using Exam.CoreData.Data.Entities;
using Exam.CoreData.Entities;
using Exam.CoreData.Enums;
using Exam.CoreData.Models;
using Exam.CoreData.Models.Images;
using Exam.CoreData.Models.Notifications;
using Exam.CoreData.Models.PagingInfo;
using Exam.CoreData.Models.Posts;
using Exam.CoreData.Models.ShopPages;
using Exam.CoreData.Repository;
using Exam.Libraries.Utils;
using Exam.Services.NotifyFacade;
using Exam.Services.RoleFacade;
using Newtonsoft.Json;
using Services.UserFacade;
using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Exam.Services.PostFacade.Implement
{
    public class PostService : IPostService
    {
        private readonly IRepository<Post, ExamDBContext> _postRepository;
        private readonly IRepository<ImageLibrary, ExamDBContext> _postImageRepository;
        private readonly IRepository<User, ExamDBContext> _userRepository;
        private readonly IRepository<Specification, ExamDBContext> _specificationRepository;
        private readonly IRepository<PostSpecification, ExamDBContext> _postSpecRepository;
        private readonly IRepository<ShopPage, ExamDBContext> _shopPageRepository;
        private readonly IRepository<Province, ExamDBContext> _provinceRepository;
        private readonly IRepository<District, ExamDBContext> _districtRepository;
        private readonly IRepository<Category, ExamDBContext> _categoryRepository;
        private readonly IRepository<Favorite, ExamDBContext> _favoriteRepository;
        private readonly IRepository<Denounce, ExamDBContext> _denountRepository;
        private readonly IRepository<TransactionHistory, ExamDBContext> _transactionRepository;
        private readonly IUserService _userService;
        private readonly INotifyService _notifyService;

        public PostService(IRepository<Post, ExamDBContext> postRepository,
            IRepository<ImageLibrary, ExamDBContext> postImageRepository,
            IRepository<User, ExamDBContext> userRepository,
            IRepository<Specification, ExamDBContext> specificationRepository,
            IRepository<PostSpecification, ExamDBContext> postSpecRepository,
            IRepository<ShopPage, ExamDBContext> shopPageRepository,
            IRepository<Province, ExamDBContext> provinceRepository,
            IRepository<District, ExamDBContext> districtRepository,
            IRepository<Category, ExamDBContext> categoryRepository,
            IRepository<Favorite, ExamDBContext> favoriteRepository,
            IRepository<Denounce, ExamDBContext> denountRepository,
            IRepository<TransactionHistory, ExamDBContext> transactionRepository,
            IUserService userService,
            INotifyService notifyService)
        {
            _postRepository = postRepository;
            _postImageRepository = postImageRepository;
            _userRepository = userRepository;
            _specificationRepository = specificationRepository;
            _postSpecRepository = postSpecRepository;
            _shopPageRepository = shopPageRepository;
            _provinceRepository = provinceRepository;
            _districtRepository = districtRepository;
            _categoryRepository = categoryRepository;
            _favoriteRepository = favoriteRepository;
            _denountRepository = denountRepository;
            _transactionRepository = transactionRepository;
            _userService = userService;
            _notifyService = notifyService;
        }

        #region Post
        public BaseSearchResult<PostModel> SearchPost(SearchPostModel model)
        {
            model.SortDesc = true;
            Expression<Func<Post, bool>> expressionWhere = c => c.Id > 0;
            //Theo danh mục
            if (model.CategoryId.HasValue)
            {
                expressionWhere = expressionWhere.And(a => a.CategoryId == model.CategoryId.Value);
            }
            //Theo trạng thái
            if (model.Status.HasValue)
            {
                expressionWhere = expressionWhere.And(b => b.Status == model.Status.Value);
            }
            //Theo người tạo
            if (model.CreatedBy.HasValue)
            {
                expressionWhere = expressionWhere.And(c => c.CreatedBy == model.CreatedBy.Value);
            }
            //Theo chuyên trang cá nhân
            if (!string.IsNullOrEmpty(model.ShopPageId))
            {
                expressionWhere = expressionWhere.And(d => d.ShopPageId == int.Parse(model.ShopPageId));
            }
            //Theo ngày tạo
            if (model.TimeMax.HasValue && model.TimeMin.HasValue)
            {
                expressionWhere = expressionWhere.And(e => e.CreatedDate > model.TimeMin.Value
                    && e.CreatedDate < model.TimeMax.Value);
            }

            if (model.PriceMin.HasValue && model.PriceMin.Value > 0)
            {
                expressionWhere = expressionWhere.And(e => e.Price >= model.PriceMin.Value);
            }

            if (model.PriceMax.HasValue && model.PriceMax.Value > 0)
            {
                expressionWhere = expressionWhere.And(e => e.Price <= model.PriceMax.Value);
            }

            if (model.ProvinceId.HasValue)
            {
                expressionWhere = expressionWhere.And(e => e.ProvinceId == model.ProvinceId.Value);
            }

            //Lấy danh sách tin quan tâm của người đang đăng nhập
            var postCareIds = new List<int>();
            if (model.UserCurrent.HasValue && model.UserCurrent.Value > 0)
            {
                var user = _userRepository.FindById(model.UserCurrent.Value);
                if (!string.IsNullOrEmpty(user.PostIdCares))
                {
                    postCareIds = JsonConvert.DeserializeObject<List<int>>(user?.PostIdCares);
                }
                if (model.IsGetListCare)
                {
                    expressionWhere = expressionWhere.And(f => postCareIds.Contains(f.Id));
                }
            }

            var result = new BaseSearchResult<PostModel>();

            var entities = _postRepository.FinAllPaging(
                    new SearchModel { PageIndex = model.PageIndex, PageSize = model.PageSize },
                    expressionWhere,
                    model.OrderByLowestPrice.HasValue ? new OrderByExpression<Post, decimal>(u => u.Price, model.OrderByLowestPrice == true ? false : true) : null,
                    new OrderByExpression<Post, DateTime>(u => u.CreatedDate, true)

                );
            var postResults = entities.Records?.CloneToListModels<Post, PostModel>();

            if (entities.Records.Count() > 0)
            {
                var approverIds = entities.Records.Select(c => c.ApproverId);
                var createdUserIds = entities.Records.Select(c => c.CreatedBy);

                var approver = _userRepository.FindAll(c => approverIds.Contains(c.Id));
                var createdUser = _userRepository.FindAll(c => createdUserIds.Contains(c.Id));

                var provinceIds = entities.Records.Select(c => c.ProvinceId);
                var provinces = _provinceRepository.FindAll(c => provinceIds.Contains(c.Id));

                var districIds = entities.Records.Select(c => c.DistrictId);
                var districts = _districtRepository.FindAll(c => districIds.Contains(c.Id));

                var catogories = _categoryRepository.FindAll();
                var shopPageCheckIds = entities.Records.Select(c => c.ShopPageId);
                var shopPages = _shopPageRepository.FindAll(c => shopPageCheckIds.Contains(c.Id));

                foreach (var post in postResults)
                {
                    post.ApproverName = approver.FirstOrDefault(c => c.Id == post.ApproverId)?.FullName;
                    post.CreatedUserName = createdUser.FirstOrDefault(c => c.Id == post.CreatedBy)?.FullName;
                    post.ProvinceName = provinces.FirstOrDefault(c => c.Id == post.ProvinceId)?.Name;
                    post.DistrictName = districts.FirstOrDefault(c => c.Id == post.DistrictId)?.Name;
                    post.ListImageIds = JsonConvert.DeserializeObject<List<string>>(post.ImageIds)?.Select(Guid.Parse).ToList();
                    post.IsCare = postCareIds.Count() > 0 && postCareIds.Contains(post.Id) ? true : false;
                    post.CategoryName = catogories.FirstOrDefault(c => c.Id == post.CategoryId)?.Name;
                    if (post.ShopPageId.HasValue)
                    {
                        post.ShopPageName = shopPages.FirstOrDefault(c => c.Id == post.ShopPageId.Value)?.Name;
                    }
                }
            }
            return new BaseSearchResult<PostModel>
            {
                Records = postResults,
                TotalRecord = entities.TotalRecord,
                PageIndex = model.PageIndex,
                PageSize = model.PageSize
            };
        }

        public bool CaringPost(CaringPostModel model)
        {
            var result = false;
            var user = _userRepository.FindById(model.UserId);
            if (!string.IsNullOrEmpty(user.PostIdCares))
            {
                var postCares = JsonConvert.DeserializeObject<List<int>>(user.PostIdCares);
                if (model.IsCaring)
                {
                    postCares.Add(model.PostId);
                }
                else
                    postCares = postCares.Where(c => c != model.PostId).ToList();
                user.PostIdCares = JsonConvert.SerializeObject(postCares?.Distinct());
                result = _userRepository.Update(user);
            }
            else
            {
                if (model.IsCaring)
                {
                    user.PostIdCares = JsonConvert.SerializeObject(new int[] { model.PostId });
                }
                result = _userRepository.Update(user);
            }
            return result;
        }

        public PostModel GetPostById(int id)
        {
            var result = new PostModel();
            var post = _postRepository.FindById(id);
            if (post == null) throw new ServiceException("Tin bài không tồn tại");
            result = post?.CloneToModel<Post, PostModel>();
            var userCreated = _userRepository.FindById(result.CreatedBy);
            var shopPage = _shopPageRepository.FirstOrDefault(a => a.CreatedBy == userCreated.Id);
            if (shopPage != null)
            {
                result.ShopPageIdInUser = shopPage.Id;
                result.ShopPageNameInUser = shopPage.Name;
            }
            if (userCreated != null)
            {
                result.MobileUser = userCreated.Mobile;
                result.CreatedUserName = userCreated.FullName;
                result.CreatedUserEmail = userCreated.Email;
                result.CreatedImageId = userCreated?.ImageId;
                //
            }
            if (result.ApproverId.HasValue)
            {
                result.ApproverName = _userRepository.FindById(result.ApproverId)?.FullName;
            }
            var province = _provinceRepository.FindById(post.ProvinceId);
            result.ProvinceName = province?.Name;
            var district = _districtRepository.FindById(post.DistrictId);
            result.DistrictName = district?.Name;

            result.ListImageIds = JsonConvert.DeserializeObject<List<string>>(post.ImageIds)?.Select(Guid.Parse).ToList();

            var favorites = _favoriteRepository.FindAll(c => c.PostId == id);
            result.NumberOfReviewers = favorites.Count();
            if (favorites.Count() > 0)
            {
                result.AverageScore = favorites.Sum(c => c.Score) / (float)favorites.Count();
            }
            return result;
        }

        private bool SaveSpecPost(int postId, IEnumerable<PostSpecDetail> postSpecs)
        {
            var result = false;
            var specByPostIds = _postSpecRepository.FindAll(c => c.PostId == postId);
            var spectExistIds = specByPostIds.Select(c => c.SpecificationId);

            foreach (var spectExistId in spectExistIds)
            {
                if (!postSpecs.Select(c => c.SpecificationId).Contains(spectExistId))
                {
                    result = _postSpecRepository.Delete(specByPostIds.First(a => a.Id == spectExistId && a.PostId == postId));
                    if (!result)
                        return result;
                }
            }

            var specChosenIds = postSpecs.Select(c => c.SpecificationId);
            var postSpecChosens = _postSpecRepository.FindAll(c => specChosenIds.Contains(c.SpecificationId) && c.PostId == postId);
            foreach (var postSpec in postSpecs)
            {
                if (!postSpecChosens.Select(c => c.SpecificationId).Contains(postSpec.SpecificationId))
                {
                    result = _postSpecRepository.Insert(new PostSpecification
                    {
                        PostId = postId,
                        SpecificationId = postSpec.SpecificationId,
                        Data = postSpec.Data
                    });
                }
                else
                {
                    var specExist = specByPostIds.FirstOrDefault(c => c.PostId == postId && c.SpecificationId == postSpec.SpecificationId);
                    specExist.Data = postSpec.Data;
                    result = _postSpecRepository.Update(specExist);
                }
                if (!result) break;
            }
            return result;
        }

        public (bool, bool isNotEnough) PayForCreatedPost(int userId)
        {
            var result = false;
            var notEnough = false;
            var user = _userRepository.FindById(userId);
            if (user != null)
            {
                decimal remain = 0;
                var transactionHistory = _transactionRepository.FindAll(c => c.UserId == userId);
                if (transactionHistory.Count() > 0)
                {
                    var recharge = transactionHistory.Where(a => a.TransactionType == TransactionType.Recharge
                            && a.Status == TransactionStatus.Success)
                        .Sum(c => c.TransactionAmount);

                    var deduct = transactionHistory.Where(a => a.TransactionType == TransactionType.Deduct
                            && a.Status == TransactionStatus.Success)
                        .Sum(c => c.TransactionAmount);
                    remain = recharge - deduct;
                }

                
                try
                {
                    if (remain < 1000)
                    {
                        throw new ServiceException("Bạn không đủ 1000 đ để đăng bài. Vui lòng nạp thêm tiền vào tài khoản");
                    }
                    result = _transactionRepository.Insert(new TransactionHistory
                    {
                        UserId = userId,
                        TransactionAmount = 1000,
                        TransactionTime = DateTime.Now,
                        TransactionType = TransactionType.Deduct,
                        Status = TransactionStatus.Success
                    });
                }
                catch (Exception ex)
                {
                    notEnough = true;
                }

                
            }
            return (result, notEnough);
        }

        public bool SavePost(SavePostModel model, bool isFreePost)
        {
            var result = false;
            var notEnoughMoney = false;
            var messageNotMoney = string.Empty;
            if (model.ImageIds == null || model.ImageIds.Count() == 0)
            {
                throw new ServiceException("Yêu cầu chọn ảnh bài đăng !");
            }

            if (!model.CategoryId.HasValue)
            {
                throw new ServiceException("Yêu cầu chọn danh mục");
            }

            if (!model.Price.HasValue)
            {
                throw new ServiceException("Yêu cầu nhập giá sản phẩm");
            }

            using (var context = _postRepository.GetDBContext())
            {
                using (var trans = context.Database.BeginTransaction())
                {
                    try
                    {
                        var imageIds = string.Empty;
                        if (model != null)
                        {
                            var entity = new Post
                            {
                                Id = model.Id.HasValue ? model.Id.Value : 0,
                                CategoryId = model.CategoryId.Value,
                                Name = model.Name,
                                Description = model.Description,
                                Content = model.Content,
                                Price = model.Price.Value,
                                DistrictId = model.DistrictId,
                                ProvinceId = model.ProvinceId,
                                CreatedBy = model.CreatedBy,
                            };
                            if (!string.IsNullOrEmpty(model.ShopPageId))
                            {
                                entity.ShopPageId = int.Parse(model.ShopPageId);
                            }
                            if (model.ImageIds.Count() > 0)
                            {
                                imageIds = JsonConvert.SerializeObject(model.ImageIds);
                            }
                            else throw new ServiceException("Yêu cầu chọn ảnh bài đăng");
                            if (model.Id.HasValue && model.Id.Value > 0)
                            {
                                var exist = _postRepository.FindById(model.Id);
                                if (exist == null)
                                {
                                    throw new ServiceException("Tin bài không tồn tại");
                                }
                                exist.CategoryId = model.CategoryId.Value;
                                exist.Price = model.Price.Value;
                                exist.Name = model.Name;
                                exist.Description = model.Description;
                                exist.Content = model.Content;
                                if (!string.IsNullOrEmpty(model.ShopPageId))
                                {
                                    exist.ShopPageId = int.Parse(model.ShopPageId);
                                }
                                exist.ImageIds = imageIds;
                                result = _postRepository.Update(exist);
                                if (result)
                                {
                                    //if (result) result = SaveSpecPost(model.Id.Value, model.PostSpecDetails);
                                }
                            }
                            else
                            {
                                entity.Status = PostStatus.Init;
                                entity.CreatedDate = DateTime.UtcNow;
                                entity.ImageIds = imageIds;
                                if (!isFreePost)
                                {
                                    var pay = PayForCreatedPost(entity.CreatedBy);
                                    notEnoughMoney = pay.isNotEnough;
                                    if (notEnoughMoney == true)
                                    {
                                        notEnoughMoney = true;
                                        throw new ServiceException("Bạn không đủ 1000 đ để đăng bài. Vui lòng nạp thêm tiền vào tài khoản");
                                    }
                                    if (pay.Item1 == true && notEnoughMoney == false)
                                    {
                                        result = _postRepository.Insert(entity);
                                    }
                                                                  
                                }

                                else result = _postRepository.Insert(entity);

                                if (result)
                                {
                                    var users = _userService.GetUsersHasPermissions((int)PermissionCode.APPROVEPOST);
                                    var userIds = users.Select(a => a.Id).ToList();
                                    var category = _categoryRepository.FindById(model.CategoryId);
                                    var createdUser = _userRepository.FindById(model.CreatedBy);
                                    var k = _notifyService.AddNotification(userIds, new CreatePostNotifyModel
                                    {
                                        Type = EventType.CreatePost,
                                        CategoryId = model.CategoryId.Value,
                                        CategoryName = category?.Name,
                                        CreatedBy = model.CreatedBy,
                                        CreatedName = !string.IsNullOrEmpty(createdUser.FullName) ? createdUser.FullName : createdUser.Email,
                                        PostId = entity.Id,
                                        Title = entity.Name,
                                        ShopPageId = entity.ShopPageId,
                                        ShopPageName = entity.ShopPageId.HasValue ? _shopPageRepository.FindById(entity.ShopPageId.Value)?.Name : string.Empty,
                                        Content = $"{(!string.IsNullOrEmpty(createdUser.FullName) ? createdUser.FullName : createdUser.Email)} đã đăng 1 tin trong danh mục {category.Name}."

                                    });
                                    //if (result) result = SaveSpecPost(entity.Id, model.PostSpecDetails);
                                }
                            }
                            if (result)
                            {
                                context.SaveChanges();
                                trans.Commit();
                            }
                            else
                            {
                                trans.Rollback();
                                return false;
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        if (notEnoughMoney)
                        {
                            messageNotMoney = ex.Message.ToString();
                        }
                        trans.Rollback();
                    }
                }
            }
            if (!string.IsNullOrEmpty(messageNotMoney))
            {
                throw new ServiceException(messageNotMoney);
            }
            return result;
        }

        public bool DeleteManyPost(DeleteGenericModel model)
        {
            var result = false;
            if (model.Ids.Count() == 0)
                throw new ServiceException("Hãy chọn ít nhất 1 bài để xóa");
            using (var context = _postRepository.GetDBContext())
            {
                using (var trans = context.Database.BeginTransaction())
                {
                    try
                    {
                        foreach (var item in model.Ids)
                        {
                            result = _postRepository.Delete(new Post { Id = item });
                            if (!result)
                            {
                                trans.Rollback();
                                break;
                            }
                        }
                        if (result) trans.Commit();
                    }
                    catch (Exception)
                    {
                        trans.Rollback();
                    }
                }
            }
            return result;
        }

        public bool ApprovePost(int id, int approveId)
        {
            var post = _postRepository.FindById(id);
            if (post == null)
                throw new ServiceException("Bài đăng không tồn tại");
            post.ApproverId = approveId;
            post.ApproveDate = DateTime.UtcNow;
            post.Status = PostStatus.Approve;
            var result = _postRepository.Update(post);
            var approver = _userRepository.FindById(approveId);
            if (result)
            {

                var userReceivers = new List<int> { post.CreatedBy };
                _notifyService.AddNotification(userReceivers, new ApprovePostEventModel
                {
                    Type = EventType.ApprovePost,
                    Content = $"{(!string.IsNullOrEmpty(approver.FullName) ? approver.FullName : approver.Email)} đã phê duyệt bài đăng: {post.Name} của bạn"
                });
            }
            return result;
        }

        public bool RejectPost(int id, int approverId)
        {
            var post = _postRepository.FindById(id);
            if (post == null)
                throw new ServiceException("Bài đăng không tồn tại");
            post.ApproverId = approverId;
            post.ApproveDate = DateTime.UtcNow;
            post.Status = PostStatus.Reject;
            var result = _postRepository.Update(post);
            var approver = _userRepository.FindById(approverId);
            if (result)
            {

                var userReceivers = new List<int> { post.CreatedBy };
                _notifyService.AddNotification(userReceivers, new ApprovePostEventModel
                {
                    Type = EventType.RejectPost,
                    Content = $"{(!string.IsNullOrEmpty(approver.FullName) ? approver.FullName : approver.Email)} đã từ chối bài đăng: {post.Name} của bạn"
                }); ;
            }
            return result;
        }
        #endregion

        #region Ảnh tin bài    
        public async Task<IEnumerable<Guid>> GetPostImageIdsAsync(int postId)
        {
            var result = new List<Guid>();
            var post = await _postRepository.FindByIdAsync(postId);
            if (!string.IsNullOrEmpty(post.ImageIds))
            {
                var imageIds = JsonConvert.DeserializeObject<List<string>>(post.ImageIds);
                result = imageIds.Select(Guid.Parse).ToList();
            }
            return result;
        }

        public async Task<(byte[] data, string contentType, string fileName)> GetPostImageByImageIdAsync(string imageId, int width, int height)
        {
            if (!string.IsNullOrEmpty(imageId))
            {
                var imageIdParse = Guid.Parse(imageId);
                var postImage = await _postImageRepository.FirstOrDefaultAsync(c => c.Id == imageIdParse);
                if (postImage != null)
                {
                    using (var ms = new MemoryStream(postImage.Data))
                    {
                        var image = System.Drawing.Image.FromStream(ms);
                        var bitmapConvert = ImageHelper.ResizeImageByteArray(image, width, height);

                        return (ImageHelper.ConvertBitMapToByteArray(bitmapConvert), postImage.Mime, postImage.Description);
                    };
                }
            }
            return (null, null, null);
        }
        public string SavePostImage(SavePostImageModel model)
        {
            var result = string.Empty;

            var contentType = model.Data.ContentType;
            if (!string.Equals(contentType, "image/jpg", StringComparison.OrdinalIgnoreCase) &&
                !string.Equals(contentType, "image/jpeg", StringComparison.OrdinalIgnoreCase) &&
                !string.Equals(contentType, "image/pjpeg", StringComparison.OrdinalIgnoreCase) &&
                !string.Equals(contentType, "image/gif", StringComparison.OrdinalIgnoreCase) &&
                !string.Equals(contentType, "image/x-png", StringComparison.OrdinalIgnoreCase) &&
                !string.Equals(contentType, "image/png", StringComparison.OrdinalIgnoreCase))
            {
                throw new ServiceException("Yêu cầu nhập đúng file ảnh !");
            }

            using (var ms = new MemoryStream())
            {
                byte[] fileByteImage = new byte[model.Data.Length];
                model.Data.CopyTo(ms);
                fileByteImage = ms.ToArray();
                using (var bitmap = new System.Drawing.Bitmap(model.Data.OpenReadStream()))
                {
                    var entity = new ImageLibrary
                    {
                        Id = Guid.NewGuid(),
                        Data = fileByteImage,
                        Description = model.Description,
                        CreatedDate = DateTime.Now,
                        Mime = model.Data.ContentType,
                        Size = (int)model.Data.Length / 1024,
                        Width = bitmap.Width,
                        Height = bitmap.Height
                    };
                    var status = _postImageRepository.Insert(entity);
                    if (status)
                    {
                        result = entity.Id.ToString();
                    }
                }
            }
            return result;
        }
        public async Task<bool> DeletePostImageAsync(string imageId)
        {
            if (!string.IsNullOrEmpty(imageId))
            {
                throw new ServiceException("Ảnh không được rỗng");
            }
            var imageIdParse = Guid.Parse(imageId);
            var image = await _postImageRepository.FindByIdAsync(imageIdParse);
            if (image == null)
            {
                throw new ServiceException("Ảnh không được rỗng");
            }
            return await _postImageRepository.DeleteAsync(image);
        }
        #endregion

        public BaseSearchResult<ShopPageModel> SearchShopPage(SearchShopPageModel model)
        {
            Expression<Func<ShopPage, bool>> expressionWhere = c => c.Id > 0;
            if (model.CategoryId.HasValue)
            {
                expressionWhere = expressionWhere.And(a => a.CategoryId == model.CategoryId.Value);
            }

            if (model.UserId.HasValue)
            {
                expressionWhere = expressionWhere.And(b => b.CreatedBy == model.UserId.Value);
            }

            var result = new BaseSearchResult<ShopPageModel>();

            var entities = _shopPageRepository.FinAllPaging(
                new SearchModel { PageIndex = model.PageIndex, PageSize = model.PageSize },
                 expressionWhere, null
                );
            var shopPageResults = entities.Records?.CloneToListModels<ShopPage, ShopPageModel>();
            var createdUserIds = shopPageResults.Select(c => c.CreatedBy);
            var createdUsers = _userRepository.FindAll(c => createdUserIds.Contains(c.Id));
            foreach (var shopPage in shopPageResults)
            {
                shopPage.CreatedUserName = createdUsers?.FirstOrDefault(c => c.Id == shopPage.CreatedBy)?.FullName;
            }

            return new BaseSearchResult<ShopPageModel>
            {
                Records = shopPageResults,
                TotalRecord = entities.TotalRecord,
                PageIndex = model.PageIndex,
                PageSize = model.PageSize
            };
        }

        public bool PayForCreatedShopPage(int userId)
        {
            var result = false;
            var user = _userRepository.FindById(userId);
            if (user != null)
            {
                decimal remain = 0;
                var transactionHistory = _transactionRepository.FindAll(c => c.UserId == userId);
                if (transactionHistory.Count() > 0)
                {
                    var recharge = transactionHistory.Where(a => a.TransactionType == TransactionType.Recharge
                            && a.Status == TransactionStatus.Success)
                        .Sum(c => c.TransactionAmount);

                    var deduct = transactionHistory.Where(a => a.TransactionType == TransactionType.Deduct
                            && a.Status == TransactionStatus.Success)
                        .Sum(c => c.TransactionAmount);
                    remain = recharge - deduct;
                }

                if (remain < 5000)
                {
                    throw new ServiceException("Bạn không đủ 5000 đ để tạo cửa hàng. Vui lòng nạp thêm tiền vào tài khoản");
                }
                result = _transactionRepository.Insert(new TransactionHistory
                {
                    UserId = userId,
                    TransactionAmount = 5000,
                    TransactionTime = DateTime.Now,
                    TransactionType = TransactionType.Deduct,
                    Status = TransactionStatus.Success
                });
            }
            return result;
        }

        public int SaveShopPage(SaveShopPageModel model, int userCurrent, bool isFreeCreateShop)
        {
            var result = false;
            if (model.Id > 0)
            {
                var shopPageExist = _shopPageRepository.FindById(model.Id);
                if (shopPageExist.CreatedBy != userCurrent)
                {
                    throw new ServiceException("Bạn không có quyền chỉnh sửa chuyên trang của người khác");
                }
                shopPageExist.Name = model.Name;
                shopPageExist.CategoryId = model.CategoryId;
                shopPageExist.Description = model.Description;
                result = _shopPageRepository.Update(shopPageExist);
                return shopPageExist.Id;
            }
            else
            {
                if (isFreeCreateShop)
                {
                    var addModel = new ShopPage
                    {
                        CreatedBy = userCurrent,
                        CreatedDate = DateTime.Now,
                        Name = model.Name,
                        CategoryId = model.CategoryId,
                        Description = model.Description
                    };
                    result = _shopPageRepository.Insert(addModel);
                    return addModel.Id;
                }
                else
                {
                    if (PayForCreatedShopPage(userCurrent))
                    {
                        var addModel = new ShopPage
                        {
                            CreatedBy = userCurrent,
                            CreatedDate = DateTime.Now,
                            Name = model.Name,
                            CategoryId = model.CategoryId
                        };
                        result = _shopPageRepository.Insert(addModel);
                        return addModel.Id;
                    }
                    else
                    {
                        return 0;
                    }
                }
            }
        }

        public ShopPageModel GetShopPageById(int id)
        {
            var exist = _shopPageRepository.FindById(id);
            if (exist == null)
            {
                throw new ServiceException("Cửa hàng không tồn tại");
            }
            return exist.CloneToModel<ShopPage, ShopPageModel>();
        }

        public bool SaveFavorite(int score, int postId, int userId)
        {
            var result = false;
            var postExist = _postRepository.FindById(postId);
            if (postExist == null)
            {
                throw new ServiceException("Bài đăng không tồn tại");
            }
            var existFavorite = _favoriteRepository.FindAll(c => c.PostId == postId && c.UserId == userId)?.FirstOrDefault();
            if (existFavorite == null)
            {
                result = _favoriteRepository.Insert(new Favorite
                {
                    PostId = postId,
                    UserId = userId,
                    Score = score
                });
            }
            else
            {
                existFavorite.Score = score;
                result = _favoriteRepository.Update(existFavorite);
            }
            return result;
        }

        public BaseSearchResult<DenouncePostModel> GetDenouncePost(SearchDenounceModel model)
        {
            model.SortDesc = true;
            Expression<Func<Denounce, bool>> expressionWhere = c => c.Id > 0;

            //Theo ngày tạo
            if (model.TimeMax.HasValue && model.TimeMin.HasValue)
            {
                expressionWhere = expressionWhere.And(b => b.CreatedDate > model.TimeMin.Value
                    && b.CreatedDate < model.TimeMax.Value);
            }

            var result = new BaseSearchResult<DenouncePostModel>();

            var entities = _denountRepository.FinAllPaging(
                    new SearchModel { PageIndex = model.PageIndex, PageSize = model.PageSize },
                    expressionWhere,
                    new OrderByExpression<Denounce, DateTime>(u => u.CreatedDate)
                );

            var denounces = entities.Records?.CloneToListModels<Denounce, DenouncePostModel>();

            var postCheckIds = denounces.Select(c => c.PostId);
            var userIds = denounces.Select(c => c.UserId);
            var posts = _postRepository.FindAll(c => postCheckIds.Contains(c.Id));
            var users = _userRepository.FindAll(c => userIds.Contains(c.Id));

            foreach (var denounce in denounces)
            {
                denounce.PostName = posts.FirstOrDefault(c => c.Id == denounce.PostId)?.Name;
                denounce.UserName = users.FirstOrDefault(c => c.Id == denounce.UserId)?.FullName;
            }
            return new BaseSearchResult<DenouncePostModel>
            {
                Records = denounces,
                TotalRecord = entities.TotalRecord,
                PageIndex = model.PageIndex,
                PageSize = model.PageSize
            };
        }

        public bool DenouncePost(DenounceModel model)
        {
            var post = _postRepository.FindById(model.PostId);
            if (post == null)
            {
                throw new ServiceException("Bài đăng không tồn tại");
            }

            if (string.IsNullOrEmpty(model.Reason))
            {
                throw new ServiceException("Bạn chưa nhập lý do tố cáo");
            }

            var denounce = _denountRepository.FirstOrDefault(c => c.PostId == model.PostId && c.UserId == model.UserId);

            if (denounce == null)
            {
                return _denountRepository.Insert(new Denounce
                {
                    PostId = model.PostId,
                    UserId = model.UserId,
                    Reason = model.Reason,
                    CreatedDate = DateTime.UtcNow
                });
            }
            else
            {
                denounce.Reason = model.Reason;
                denounce.CreatedDate = DateTime.UtcNow;
                return _denountRepository.Update(denounce);
            }
        }

        public bool LockPost(LockPostModel model)
        {
            var result = false;
            var post = _postRepository.FindById(model.PostId);
            if (post == null)
            {
                throw new ServiceException("Bài đăng không tồn tại");
            }
            if (model.IsLock)
            {
                post.Status = PostStatus.Lock;
            }
            else post.Status = PostStatus.Approve;
            _postRepository.Update(post);
            return result;
        }
    }
}
