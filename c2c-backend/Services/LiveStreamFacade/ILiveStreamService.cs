﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Exam.Services.LiveStreamFacade
{
    public interface ILiveStreamService
    {
        bool CreateStream(int userId);
    }
}
