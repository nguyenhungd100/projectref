﻿using Exam.CoreData;
using Exam.CoreData.Data.Entities;
using Exam.CoreData.Entities;
using Exam.CoreData.Repository;
using Exam.Services.NotifyFacade;
using System;
using System.Collections.Generic;
using System.Text;

namespace Exam.Services.LiveStreamFacade.Implement
{
    public class LiveStreamService : ILiveStreamService
    {
        private readonly IRepository<LiveStream, ExamDBContext> _liveStreamRepository;
        private readonly INotifyService _notifyService;
        private readonly IRepository<User, ExamDBContext> _userRepository;

        public LiveStreamService(IRepository<LiveStream, ExamDBContext> liveStreamRepository,
            INotifyService notifyService,
            IRepository<User, ExamDBContext> userRepository)
        {
            _liveStreamRepository = liveStreamRepository;
            _notifyService = notifyService;
            _userRepository = userRepository;
        }

        public bool CreateStream(int userId)
        {
            var result = false;
            var user = _userRepository.FindById(userId);
            if (user == null || user?.Status != CoreData.Enums.UserStatus.Actived)
            {
                throw new ServiceException("Tài khoản không hợp lệ, vui lòng liên hệ ban quản trị");
            }
            var liveStream = new LiveStream
            {
                CreatedBy = userId,
                StreamId = $"{userId}**{DateTime.Now.Day}**{DateTime.Now.Month}**{DateTime.Now.Year}",
                CreatedDate = DateTime.Now,
            };
            result = _liveStreamRepository.Insert(liveStream);
            if (result)
            {
                result = _notifyService.AddLiveStream(new { 
                    createdBy = userId,
                    createdUserName = string.IsNullOrEmpty(user.FullName)? user.FullName : user.Email,
                    streamId = liveStream.StreamId, 
                    date = liveStream.CreatedDate 
                });
            }
            return result;
        }
    }
}
