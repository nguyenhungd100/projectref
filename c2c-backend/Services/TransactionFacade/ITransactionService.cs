﻿using Exam.CoreData.Models.TransactionHistories;
using System;
using System.Collections.Generic;
using System.Text;

namespace Exam.Services.TransactionFacade
{
    public interface ITransactionService
    {
        List<TransactionHistoryModel> GetTransactionByUserId(int userId);
        string GetCheckOutUrl(CheckOutModel model);
        bool VerifyCheckOut(VerifyCheckOutModel model);
    }
}
