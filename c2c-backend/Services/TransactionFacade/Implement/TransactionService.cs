﻿using Exam.CoreData;
using Exam.CoreData.Data;
using Exam.CoreData.Data.Entities;
using Exam.CoreData.Entities;
using Exam.CoreData.Enums;
using Exam.CoreData.Models.TransactionHistories;
using Exam.CoreData.Repository;
using Exam.Libraries.Utils;
using Exam.Services.StatisticFacade;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;

namespace Exam.Services.TransactionFacade.Implement
{
    public class TransactionService : ITransactionService
    {
        private readonly IRepository<Wallet, ExamDBContext> _walletRepository;
        private readonly IRepository<TransactionHistory, ExamDBContext> _transactionHistoryRepository;
        private readonly IRepository<User, ExamDBContext> _userRepository;
        private readonly IPaymentService _paymentService;

        public TransactionService(IRepository<Wallet, ExamDBContext> walletRepository,
            IRepository<TransactionHistory, ExamDBContext> transactionHistoryRepository,
            IRepository<User, ExamDBContext> userRepository,
            IPaymentService paymentService)
        {
            _walletRepository = walletRepository;
            _transactionHistoryRepository = transactionHistoryRepository;
            _userRepository = userRepository;
            _paymentService = paymentService;
        }

        public List<TransactionHistoryModel> GetTransactionByUserId(int userId)
        {
            var userExist = _userRepository.FindById(userId);
            if (userExist == null)
                throw new ServiceException("Tài khoản không tồn tại");
            if (userExist.Status == CoreData.Enums.UserStatus.Disabled)
            {
                throw new ServiceException("Tài khoản đã bị khóa");
            }
            var transactions = _transactionHistoryRepository.FindAll(c => c.UserId == userId && c.Status == TransactionStatus.Success)
                ?.OrderByDescending(a => a.TransactionTime);
            return transactions.ToList()?.CloneToListModels<TransactionHistory, TransactionHistoryModel>();
        }

        public string Payment()
        {
            using (var httpClient = new HttpClient())
            {
                var data = httpClient.GetAsync("");
            }
            return "";
        }

        public string GetCheckOutUrl(CheckOutModel model)
        {
            var user = _userRepository.FindById(model.UserId);
            if (user == null)
            {
                throw new ServiceException("Tài khoản không tồn tại");
            }

            string totalAmount = string.Empty;
            OrderCodeInfo orderCode;
            Enum.TryParse(model.OrderCode, out orderCode);
            switch (orderCode)
            {
                case OrderCodeInfo.Value2000:
                    totalAmount = "2000";
                    break;
                case OrderCodeInfo.Value5000:
                    totalAmount = "5000";
                    break;
                case OrderCodeInfo.Value10000:
                    totalAmount = "10000";
                    break;
                case OrderCodeInfo.Value20000:
                    totalAmount = "20000";
                    break;
                default:
                    break;
            }
            
            var transaction = new TransactionHistory
            {
                UserId = model.UserId,
                TransactionAmount = decimal.Parse(totalAmount),
                TransactionTime = DateTime.Now,
                TransactionType = TransactionType.Recharge,
                Status = TransactionStatus.WaitPay
            };
            var success = _transactionHistoryRepository.Insert(transaction);
                    
            var urlRedirect = string.Empty;

            if (success)
            {
                var resultPayment = _paymentService.SendRequestIntegratedPayment(model.BankCode, totalAmount, user.FullName, user.Email, user.Mobile, transaction.Id.ToString());
                
                if (resultPayment.Error_code == "00")
                {
                    urlRedirect = resultPayment.Checkout_url;
                    var t2 = resultPayment.Description;
                }
            }
       
            return urlRedirect;

        }

        public bool VerifyCheckOut(VerifyCheckOutModel model)
        {
            var result = false;
            var transaction = _transactionHistoryRepository.FindById(model.OrderCode);
            if (transaction == null)
            {
                throw new ServiceException("Giao dịch không tồn tại");
            }
            if (model.UserId != transaction.UserId)
            {
                throw new ServiceException("Bạn không phải là chủ giao dịch này");
            }
            if (!(transaction.TransactionType == TransactionType.Recharge
                && transaction.Status == TransactionStatus.WaitPay))
            {
                throw new ServiceException("Giao dịch không ở trạng thái chờ xác thực");
            }
            transaction.Status = TransactionStatus.Success;
            result = _transactionHistoryRepository.Update(transaction);
            return result;
        }
    }
}
