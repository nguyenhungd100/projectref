﻿using Exam.CoreData.Data.Entities;
using Exam.CoreData.Models.PagingInfo;
using Exam.Libraries.Utils;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Exam.CoreData.Repository
{
    public interface IRepository<T, TC> where T : class 
        where TC : DbContext
    {
        TC GetDBContext();

        T FindById(object id);

        Task<T> FindByIdAsync(object id);

        T FirstOrDefault();

        T FirstOrDefault(Expression<Func<T, bool>> expression);

        Task<T> FirstOrDefaultAsync(Expression<Func<T, bool>> expression);

        T SingleOrDefault();

        T SingleOrDefault(Expression<Func<T, bool>> expression);

        bool Any(Expression<Func<T, bool>> expression);

        IEnumerable<T> FindAll();

        Task<IEnumerable<T>> FindAllAsync();

        IEnumerable<T> FindAll(Expression<Func<T, bool>> expression);

        Task<IEnumerable<T>> FindAllAsync(Expression<Func<T, bool>> expression);

        BaseSearchResult<T> FinAllPaging(SearchModel search, Expression<Func<T, bool>> expression, params IOrderByExpression<T>[] orderByExpressions);

        Task<bool> InsertAsync(T entity);

        bool Insert(T entity);

        Task<bool> BulkInsertAsync(IEnumerable<T> entities);

        bool Update(T entity);

        bool Delete(T entity);
        Task<bool> DeleteAsync(T entity);
    }
}
