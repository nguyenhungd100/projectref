﻿using Exam.CoreData.Models.PagingInfo;
using Exam.Libraries.Utils;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Exam.CoreData.Repository.Implement
{
    public class Repository<T, TC> : IRepository<T, TC> 
        where T : class, new()
        where TC : DbContext
    {
        private readonly TC _context;

        public Repository(TC context)
        {
            _context = context;
        }

        public TC GetDBContext()
        {
            return _context;
        }

        public T FindById(object id)
        {
            return _context.Set<T>().Find(id);
        }

        public async Task<T> FindByIdAsync(object id)
        {
            return await _context.Set<T>().FindAsync(id);
        }

        public T FirstOrDefault()
        {
            return _context.Set<T>().FirstOrDefault();
        }

        public T FirstOrDefault(Expression<Func<T, bool>> expression)
        {
            return _context.Set<T>().FirstOrDefault(expression);
        }

        public async Task<T> FirstOrDefaultAsync(Expression<Func<T, bool>> expression)
        {
            return await _context.Set<T>().FirstOrDefaultAsync(expression);
        }

        public T SingleOrDefault()
        {
            return _context.Set<T>().SingleOrDefault();
        }

        public T SingleOrDefault(Expression<Func<T, bool>> expression)
        {
            return _context.Set<T>().SingleOrDefault(expression);
        }

        public bool Any(Expression<Func<T, bool>> expression)
        {
            return _context.Set<T>().Any(expression);
        }

        public IEnumerable<T> FindAll()
        {
            return _context.Set<T>().ToList();
        }

        public async Task<IEnumerable<T>> FindAllAsync()
        {
            return await _context.Set<T>().ToListAsync();
        }

        public IEnumerable<T> FindAll(Expression<Func<T, bool>> expression)
        {
            return _context.Set<T>().Where(expression).ToList();
        }

        public async Task<IEnumerable<T>> FindAllAsync(Expression<Func<T, bool>> expression)
        {
            return await _context.Set<T>().Where(expression).ToListAsync();
        }

        public IQueryable<TEntity> ApplyOrderBy<TEntity>(IQueryable<TEntity> query,
         params IOrderByExpression<TEntity>[] orderByExpressions)
         where TEntity : class
        {
            if (orderByExpressions == null)
                return query;

            IOrderedQueryable<TEntity> output = null;

            foreach (var orderByExpression in orderByExpressions)
            {
                if (orderByExpression != null)
                {
                    if (output == null)
                        output = orderByExpression.ApplyOrderBy(query);
                    else
                        output = orderByExpression.ApplyThenBy(output);
                }              
            }

            return output ?? query;
        }

        public BaseSearchResult<T> FinAllPaging(SearchModel search, Expression<Func<T, bool>> expression,
             params IOrderByExpression<T>[] orderByExpressions)
        {
            var result = new BaseSearchResult<T>();
            var list = new List<T>();

            list = ApplyOrderBy<T>(_context.Set<T>().Where(expression), orderByExpressions)?.ToList();

            result.TotalRecord = list.Count();
            result.Records = list.Skip(search.PageSize * (search.PageIndex - 1)).Take(search.PageSize).ToList();
            result.PageIndex = search.PageIndex;
            result.PageSize = search.PageSize;
            return result;
        }

        public async Task<bool> InsertAsync(T entity)
        {
            var a = await _context.Set<T>().AddAsync(entity);
            return await SaveChangeAsync();
        }

        public bool Insert(T entity)
        {
            _context.Set<T>().Add(entity);
            return SaveChange();
        }

        public async Task<bool> BulkInsertAsync(IEnumerable<T> entities)
        {
            //await _context.BulkInsertAsync(entities);
            return true;
        }
        
        private async Task<bool> SaveChangeAsync()
        {
            return await _context.SaveChangesAsync() > 0 ? true : false;
        }

        public bool Update(T entity)
        {
            _context.Set<T>().Update(entity);
            return SaveChange();
        }

        public bool Delete(T entity)
        {
            _context.Attach(entity);
            _context.Set<T>().Remove(entity);
            return SaveChange();
        }

        public async Task<bool> DeleteAsync(T entity)
        {
            _context.Attach(entity);
            _context.Set<T>().Remove(entity);
            return await SaveChangeAsync();
        }

        public bool SaveChange()
        {
            return _context.SaveChanges() > 0 ? true : false;
        }

        
    }
}
