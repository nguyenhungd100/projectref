﻿using Exam.CoreData.Data;
using Exam.CoreData.Data.Entities;
using Exam.CoreData.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;
using System;
using System.Collections.Generic;
using System.Text;

namespace Exam.CoreData
{
    public partial class ExamDBContext : DbContext
    {
        public ExamDBContext(DbContextOptions<ExamDBContext> options) : base(options)
        {
           // EntityFrameworkManager.ContextFactory = context => new ExamDBContext(options);
        }

        public virtual DbSet<User> Users { get; set; }
        public virtual DbSet<Role> Roles { get; set; }
        public virtual DbSet<District> Districts { get; set; }
        public virtual DbSet<Province> Provinces { get; set; }
        public virtual DbSet<Category> Categories { get; set; }
        public virtual DbSet<ImageLibrary> ImageLibraries { get; set; }
        public virtual DbSet<ObjectType> ObjectTypes { get; set; }
        public virtual DbSet<Post> Posts { get; set; }
        public virtual DbSet<Specification> Specifications { get; set; }
        public virtual DbSet<PostSpecification> PostSpecifications { get; set; }
        public virtual DbSet<Wallet> Wallets { get; set; }
        public virtual DbSet<TransactionHistory> TransactionHistories { get; set; }
        public virtual DbSet<ShopPage> ShopPages { get; set; }
        public virtual DbSet<Comment> Comments { get; set; }
        public virtual DbSet<CommentUser> CommentUsers { get; set; }
        public virtual DbSet<Favorite> Favorites { get; set; }
        public virtual DbSet<Denounce> Denounces { get; set; }

        public virtual DbSet<LiveStream> LiveStreams { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<User>(entity => entity.ToTable("Users"));
            modelBuilder.Entity<Role>(entity => entity.ToTable("Roles"));
            modelBuilder.Entity<Province>(entity => entity.ToTable("Provinces"));
            modelBuilder.Entity<District>(entity => entity.ToTable("Districts"));
            modelBuilder.Entity<Category>(entity => entity.ToTable("Categories"));
            modelBuilder.Entity<ImageLibrary>(entity => entity.ToTable("ImageLibraries"));
            modelBuilder.Entity<ObjectType>(entity => entity.ToTable("ObjectTypes"));
            modelBuilder.Entity<Post>(entity => entity.ToTable("Posts"));
            modelBuilder.Entity<Specification>(entity => entity.ToTable("Specifications"));
            modelBuilder.Entity<PostSpecification>(entity => entity.ToTable("PostSpecifications"));
            modelBuilder.Entity<Wallet>(entity => entity.ToTable("Wallets"));
            modelBuilder.Entity<TransactionHistory>(entity => entity.ToTable("TransactionHistories"));
            modelBuilder.Entity<ShopPage>(entity => entity.ToTable("ShopPages"));
            modelBuilder.Entity<Comment>(entity => entity.ToTable("Comments"));
            modelBuilder.Entity<CommentUser>(entity => entity.ToTable("CommentUsers"));
            modelBuilder.Entity<Favorite>(entity => entity.ToTable("Favorites"));
            modelBuilder.Entity<Denounce>(entity => entity.ToTable("Denounces"));
            modelBuilder.Entity<LiveStream>(entity => entity.ToTable("LiveStreams"));
            base.OnModelCreating(modelBuilder);
        }
    }
}
