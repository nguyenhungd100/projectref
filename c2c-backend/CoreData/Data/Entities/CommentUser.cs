﻿using Exam.CoreData.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Exam.CoreData.Data.Entities
{
    public class CommentUser : BaseEntity
    {
        public int CommentId { get; set; }
        public int UserId { get; set; }
        public bool IsLike { get; set; }
        public bool IsDisLike { get; set; }

    }
}
