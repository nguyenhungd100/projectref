﻿using Exam.CoreData.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Exam.CoreData.Data.Entities
{
    public class District 
    {
        public int Id { get; set; }

        public int ProvinceId { get; set; }

        public string Name { get; set; }

        public string Level { get; set; }

        public int SortOrder { get; set; }
    }
}
