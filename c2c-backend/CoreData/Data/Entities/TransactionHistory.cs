﻿using Exam.CoreData.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace Exam.CoreData.Data.Entities
{
    public class TransactionHistory : BaseEntity
    {
        public int UserId { get; set; }

        public decimal TransactionAmount { get; set; }

        public DateTime TransactionTime { get; set; }

        public TransactionType TransactionType { get; set; }

        public TransactionStatus Status { get; set; }
    }

    public enum TransactionType
    {
        [Description("Nạp tiền")]
        Recharge = 1,

        [Description("Trừ tiền")]
        Deduct = 5,
    }

    public enum TransactionStatus
    {
        [Description("Chờ xử lý")]
        WaitPay = 1,
        [Description("Thành công")]
        Success = 5
    }
}
