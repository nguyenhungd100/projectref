﻿using Exam.CoreData.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Exam.CoreData.Entities
{
    public class User : BaseEntity
    {
        public string FullName { get; set; }

        public string Email { get; set; }

        public UserStatus Status { get; set; }

        public string Password { get; set; }

        public string Mobile { get; set; }

        public int? Gender { get; set; }

        public string UserRoles { get; set; }

        public int? DisplayOrder { get; set; }

        public string PostIdCares { get; set; }

        public bool? OnCms { get; set; }

        public DateTime? DateOfBirth { get; set; }
        public Guid? ImageId { get; set; }
    }
}
