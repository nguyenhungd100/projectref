﻿using Exam.CoreData.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Exam.CoreData.Data.Entities
{
    public class Wallet : BaseEntity
    {
        public int UserId { get; set; }

        public decimal Coins { get; set; }

        public DateTime LastTransactionTime { get; set; }

        public WalletStatus Status { get; set; }
    }

    public enum WalletStatus
    {
        Open = 1,
        Closed = 5
    }
}
