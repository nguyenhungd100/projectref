﻿using Exam.CoreData.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Exam.CoreData.Data.Entities
{
    public class ObjectType : BaseEntity
    {
        public int CategoryId { get; set; }

        public string Name { get; set; }
        public int DisplayOrder { get; set; }
    }
}
