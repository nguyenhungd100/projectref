﻿using Exam.CoreData.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Exam.CoreData.Data.Entities
{
    public class Denounce : BaseEntity
    {
        public int PostId { get; set; }

        public int UserId { get; set; }

        public string Reason { get; set; }

        public DateTime CreatedDate { get; set; }

    }
}
