﻿using Exam.CoreData.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Exam.CoreData.Data.Entities
{
    public class PostSpecification : BaseEntity
    {
        public int PostId { get; set; }

        public int SpecificationId { get; set; }

        public string Data { get; set; }
    }
}
