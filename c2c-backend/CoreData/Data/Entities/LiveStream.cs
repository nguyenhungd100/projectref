﻿using Exam.CoreData.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Exam.CoreData.Data.Entities
{
    public class LiveStream : BaseEntity
    {
        public string StreamId { get; set; }

        public int CreatedBy { get; set; }

        public string LinkVideo { get; set; }

        public DateTime CreatedDate { get; set; }

    }
}
