﻿using Exam.CoreData.Entities;
using Exam.CoreData.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace Exam.CoreData.Data.Entities
{
    public class Comment : BaseEntity
    {
        public string Content { get; set; }

        public int PostId { get; set; }

        public DateTime CreatedDate { get; set; }

        public int CreatedBy { get; set; }

        public int? ParentId { get; set; }

        public CommentStatus Status { get; set; }
    }

    

}
