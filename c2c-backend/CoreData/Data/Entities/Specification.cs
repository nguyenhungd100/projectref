﻿using Exam.CoreData.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Exam.CoreData.Data.Entities
{
    public class Specification : BaseEntity
    {
        public int CatetgoryId { get; set; }

        public string Name { get; set; }

        public SpecificationType Type { get; set; }

        public int DisplayOrder { get; set; }
    }

    public enum SpecificationType
    {
        IsNum = 1,
        IsText = 2
    }
}
