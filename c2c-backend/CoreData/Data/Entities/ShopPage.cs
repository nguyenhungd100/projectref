﻿using Exam.CoreData.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Exam.CoreData.Data.Entities
{
    public class ShopPage : BaseEntity
    {
        public int CategoryId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public DateTime CreatedDate { get; set; }
        public int CreatedBy { get; set; }
        public int? ApprovedBy { get; set; }
        public Guid? ImageId { get; set; }
    }
}
