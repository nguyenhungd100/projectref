﻿using Exam.CoreData.Entities;
using Exam.CoreData.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace Exam.CoreData.Data.Entities
{
    public class Post : BaseEntity
    {
        public int CategoryId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string Content { get; set; }
        public int? ApproverId { get; set; }
        public int DistrictId { get; set; }
        public int ProvinceId { get; set; }
        public int CreatedBy { get; set; }
        public decimal Price { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime? ApproveDate { get; set; }
        public int? ShopPageId { get; set; }
        public string ImageIds { get; set; }
        public PostStatus Status { get; set; }
    }

   
}
