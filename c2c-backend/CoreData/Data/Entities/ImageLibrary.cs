﻿using Exam.CoreData.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Exam.CoreData.Data.Entities
{
    public class ImageLibrary
    {
        public Guid Id { get; set; }
        public byte[] Data { get; set; }
        public int? Width { get; set; }
        public int? Height { get; set; }
        public string Description { get; set; }
        public string Mime { get; set; }
        public int Size { get; set; }
        public DateTime? CreatedDate { get; set; }
    }
}
