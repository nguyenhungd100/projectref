﻿using Exam.CoreData.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Exam.CoreData.Data.Entities
{
    public class AssignCategoryManager : BaseEntity
    {
        public int CategoryId { get; set; }
        public int? UserId { get; set; }
        public DateTime AssignTime { get; set; }
        public int AssignByUser { get; set; }
    }
}
