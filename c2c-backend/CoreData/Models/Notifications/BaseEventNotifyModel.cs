﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Exam.CoreData.Models.Notifications
{
    public class BaseEventNotifyModel
    {
        public DateTime CreatedOn { get; set; } = DateTime.Now;
        public EventType Type { get; set; } = EventType.System;
    }

    public enum EventType : int
    {
        System = 1,
        CreatePost = 2,
        ApprovePost = 3,
        RejectPost = 4,
        AddUser = 5,
    }
}
