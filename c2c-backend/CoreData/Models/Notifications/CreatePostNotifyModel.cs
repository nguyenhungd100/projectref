﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Exam.CoreData.Models.Notifications
{
    public class CreatePostNotifyModel : BaseEventNotifyModel
    {
        public int CreatedBy { get; set; }

        public string CreatedName { get; set; }

        public int PostId { get; set; }

        public string Title { get; set; }

        public int CategoryId { get; set; }

        public string CategoryName { get; set; }

        public int? ShopPageId { get; set; }

        public string ShopPageName { get; set; }

        public string Content { get; set; }
    }
}
