﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Exam.CoreData.Models.Notifications
{
    public class ApprovePostEventModel : BaseEventNotifyModel
    {
        public string Content { get; set; }

    }
}
