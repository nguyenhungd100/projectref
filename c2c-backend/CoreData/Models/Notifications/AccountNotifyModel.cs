﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Exam.CoreData.Models.Notifications
{
    public class AccountNotifyModel : BaseEventNotifyModel
    {
        public int PersonAddedId { get; set; }
        public string PersonAddedName { get; set; }
        public int PersonAddId { get; set; }
        public string PersonAddName { get; set; }
        public string Role { get; set; }
    }
}
