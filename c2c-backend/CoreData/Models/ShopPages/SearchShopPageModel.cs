﻿using Exam.CoreData.Models.PagingInfo;
using System;
using System.Collections.Generic;
using System.Text;

namespace Exam.CoreData.Models.ShopPages
{
    public class SearchShopPageModel : BaseSearchModel
    {
        public int? CategoryId { get; set; }
        public int? UserId { get; set; }
    }
}
