﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Exam.CoreData.Models.ShopPages
{
    public class ShopPageModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }

        public DateTime CreatedDate { get; set; }
        public int CreatedBy { get; set; }
        public string CreatedUserName { get; set; }
        public int? ApprovedBy { get; set; }
        public Guid? ImageId { get; set; }
    }
}
