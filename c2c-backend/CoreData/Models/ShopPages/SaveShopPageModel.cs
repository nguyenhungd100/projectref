﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Exam.CoreData.Models.ShopPages
{
    public class SaveShopPageModel
    {
        public int Id { get; set; }
        public int CategoryId { get; set; }
        public string Name { get; set; }
        public int? BackgroundImageId { get; set; }

        public string Description { get; set; }
    }
}
