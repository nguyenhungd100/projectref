﻿using Exam.CoreData.Models.PagingInfo;
using System;
using System.Collections.Generic;
using System.Text;

namespace Exam.CoreData.Models.Roles
{
    public class RoleFilterResult : BaseSearchResult<RoleModel>
    {
        public IDictionary<int, string> PermissionTitle { get; set; } = new Dictionary<int, string>();
    }
}
