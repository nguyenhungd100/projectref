﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Exam.CoreData.Models.Roles
{
    public class RoleCodeModel
    {
        public string RoleName { get; set; }

        public int Code { get; set; }
    }
}
