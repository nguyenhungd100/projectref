﻿using Exam.CoreData.Models.PagingInfo;
using System;
using System.Collections.Generic;
using System.Text;

namespace Exam.CoreData.Models.Roles
{
    public class RoleFilterModel : BaseSearchModel
    {
        public string RoleName { get; set; }
    }
}
