﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Exam.CoreData.Models.Roles
{
    public class SaveRoleModel
    {
        public IEnumerable<SaveRoleInfo> SaveRoleInfos { get; set; } = new List<SaveRoleInfo>();
    }

    public class SaveRoleInfo 
    {
        public int RoleId { get; set; }
        public IDictionary<int, bool> Data { get; set; } = new Dictionary<int, bool>();
    }

}
