﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Exam.CoreData.Models.Roles
{
    public class RoleModel
    {
        public int Id { get; set; }

        public string RoleName { get; set; }

        public string Title { get; set; }

        public string Description { get; set; }

        public string Permissions { get; set; }

        public IDictionary<int, bool> PermissionInfo { get; set; } = new Dictionary<int, bool>();

        public int? DisplayOrder { get; set; }
    }
}
