﻿using Exam.CoreData.Data.Entities;
using Exam.CoreData.Enums;
using Exam.CoreData.Models.Images;
using System;
using System.Collections.Generic;
using System.Text;

namespace Exam.CoreData.Models.Posts
{
    public class PostModel
    {
        public int Id { get; set; }
        public int CategoryId { get; set; }
        public string CategoryName { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string Content { get; set; }
        public int? ApproverId { get; set; }
        public string ApproverName { get; set; }
        public int DistrictId { get; set; }
        public string DistrictName { get; set; }
        public int ProvinceId { get; set; }
        public string ProvinceName { get; set; }
        public int CreatedBy { get; set; }
        public string CreatedUserName { get; set; }
        public string CreatedUserEmail { get; set; }
        public Guid? CreatedImageId { get; set; }
        public decimal Price { get; set; }
        public string MobileUser { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime? ApproveDate { get; set; }
        public IEnumerable<Guid> ListImageIds { get; set; }
        public string ImageIds { get; set; }

        public int? ShopPageId { get; set; }

        public string ShopPageName { get; set; }
        public PostStatus Status { get; set; }
        public bool IsCare { get; set; } = false;

        public float AverageScore { get; set; } = 0;

        public int NumberOfReviewers { get; set; } = 0;

        public int? ShopPageIdInUser { get; set; }

        public string ShopPageNameInUser { get; set; }
    }
}
