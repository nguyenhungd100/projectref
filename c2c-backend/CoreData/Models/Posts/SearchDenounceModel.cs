﻿using Exam.CoreData.Models.PagingInfo;
using System;
using System.Collections.Generic;
using System.Text;

namespace Exam.CoreData.Models.Posts
{
    public class SearchDenounceModel : BaseSearchModel
    {
        public DateTime? TimeMin { get; set; }

        public DateTime? TimeMax { get; set; }
    }
}
