﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Exam.CoreData.Models.Posts
{
    public class DenouncePostModel
    {
        public int Id { get; set; }
        public int PostId { get; set; }

        public string PostName { get; set; }

        public int UserId { get; set; }

        public string UserName { get; set; }

        public string Reason { get; set; }

        public DateTime CreatedDate { get; set; }
    }
}
