﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace Exam.CoreData.Models.Posts
{
    public class DenounceModel
    {
        public int PostId { get; set; }

        [JsonIgnore]
        public int UserId { get; set; }

        public string Reason { get; set; }

    }
}
