﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Exam.CoreData.Models.Posts
{
    public class LockPostModel
    {
        public int PostId { get; set; }

        public bool IsLock { get; set; } = true;
    }
}
