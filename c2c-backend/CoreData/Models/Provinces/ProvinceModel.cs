﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Exam.CoreData.Models.Provinces
{
    public class ProvinceModel
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string Level { get; set; }

        public int SortOrder { get; set; }
    }
}
