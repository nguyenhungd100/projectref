﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace Exam.CoreData.Models.ObjectOrienteds
{
    public class ObjectTypeModel
    {
        public int Id { get; set; }
        public int CategoryId { get; set; }
        public string Name { get; set; }
        [JsonIgnore]
        public int DisplayOrder { get; set; }
    }
}
