﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Exam.CoreData.Models.Districts
{
    public class DistrictModel
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public int ProvinceId { get; set; }

        public string ProvinceName { get; set; }

        public string Level { get; set; }

        public int SortOrder { get; set; }
    }
}
