﻿using Exam.CoreData.Models.Categories;
using System;
using System.Collections.Generic;
using System.Text;

namespace Exam.CoreData.Models.AssignCategoryManager
{
    public class InfoUserAssignedModel
    {
        public CategoryModel Category { get; set; }

        public int UserAssignedId { get; set; }

        public string UserAssignedName { get; set; }

        public string UserAssignedEmail { get; set; }

        public string UserAssignedMobile { get; set; }
    }
}
