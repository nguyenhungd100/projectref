﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace Exam.CoreData.Models.AssignCategoryManager
{
    public class AssignCategoryManagerModel
    {
        [JsonIgnore]
        public int Id { get; set; }
        public int CategoryId { get; set; }
        public int? UserId { get; set; }
        [JsonIgnore]
        public DateTime AssignTime { get; set; }
        [JsonIgnore]
        public int AssignByUser { get; set; }
    }
}
