﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace Exam.CoreData.Models.Comments
{
    public class SaveCommentModel
    {
        public int Id { get; set; }

        public string Content { get; set; }

        public int PostId { get; set; }
        [JsonIgnore]
        public DateTime CreatedDate { get; set; }
        
        [JsonIgnore]
        public int CreatedBy { get; set; }

        public int? ParentId { get; set; }

    }
}
