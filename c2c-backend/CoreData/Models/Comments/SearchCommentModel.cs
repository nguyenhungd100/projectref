﻿using Exam.CoreData.Models.PagingInfo;
using System;
using System.Collections.Generic;
using System.Text;

namespace Exam.CoreData.Models.Comments
{
    public class SearchCommentModel
    {
        public int? PostId { get; set; }

        public int? UserCurrentId { get; set; }
    }
}
