﻿using Exam.CoreData.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace Exam.CoreData.Models.Comments
{
    public class CommentModel
    {
        public int Id { get; set; }

        public string Content { get; set; }

        public int PostId { get; set; }

        public DateTime CreatedDate { get; set; }

        public int CreatedBy { get; set; }

        public string CreatedByName { get; set; }

        public string CreatedByEmail { get; set; }

        public string CreatedMobile { get; set; }

        public int? ParentId { get; set; }

        public Int64 LikeCount { get; set; } = 0;

        public Int64 DislikeCount { get; set; } = 0;

        public CommentStatus Status { get; set; }

        public bool IsOfUserCurrent { get; set; } = false;

        public bool IsLike { get; set; } = false;

        public bool IsDislike { get; set; } = false;

        public IEnumerable<CommentModel> CommentChildrens { get; set; }
    }
}
