﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Exam.CoreData.Models.Statistics
{
    public class StatisticResultModel
    {
        public List<StatisticInfo> StatisticInfos { get; set; } = new List<StatisticInfo>();

        public Int64 TotalNumberOfPosts { get; set; } = 0;

        public Int64 TotalNumberOfShops { get; set; } = 0;
    }

    public class StatisticInfo 
    {
        public int Month { get; set; }

        public Int64 NumberOfPosts { get; set; } = 0;

        public Int64 NumberOfShopCreated { get; set; } = 0;


    }

    public class StatisticRevenueModel
    {
        public List<StatisticRevenueInfo> StatisticRevenueInfos { get; set; } = new List<StatisticRevenueInfo>();
    }

    public class StatisticRevenueInfo 
    {
        public int Month { get; set; }

        public decimal Revenue { get; set; } = 0;
    }

}
