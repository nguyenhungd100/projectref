﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Exam.CoreData.Models.Categories
{
    public class CategoryModel
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        public int? ParentId { get; set; }

        public int? DisplayOrder { get; set; }

        public bool? Status { get; set; }

        public IEnumerable<CategoryModel> CategoryChildrens { get; set; }
    }
}
