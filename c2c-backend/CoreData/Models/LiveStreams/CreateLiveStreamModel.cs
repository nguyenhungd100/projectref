﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Exam.CoreData.Models.LiveStreams
{
    public class CreateLiveStreamModel
    {
        public int UserId { get; set; }

        public string Title { get; set; }
    }
}
