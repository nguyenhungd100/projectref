﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Text;

namespace Exam.CoreData.Models.Images
{
    public class ImageLibraryModel
    {
        public Guid Id { get; set; }
        public byte[] Data { get; set; }
        public int? Width { get; set; }
        public int? Height { get; set; }
        public string Description { get; set; }
        public string Mime { get; set; }
        public int Size { get; set; }
        public DateTime? CreatedDate { get; set; }
    }

    public  class SaveImageLibraryModel
    {
        public int? UserId { get; set; }
        public int? ShopPageId { get; set; }
        public string Description { get; set; }
        public IFormFile Data { get; set; }
    }
}
