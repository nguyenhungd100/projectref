﻿using Exam.CoreData.Data.Entities;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace Exam.CoreData.Models.Wallets
{
    public class WalletModel
    {
        [JsonIgnore]
        public int Id { get; set; }
        public int UserId { get; set; }

        public decimal Coins { get; set; }

        public DateTime LastTransactionTime { get; set; }

        public WalletStatus Status { get; set; }
    }
}
