﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Exam.CoreData.Models.Users
{
    public class ChangePasswordModel
    {
        public string Email { get; set; }

        public string Password { get; set; }
    }
}
