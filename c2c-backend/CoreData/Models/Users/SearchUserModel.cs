﻿using Exam.CoreData.Enums;
using Exam.CoreData.Models.PagingInfo;
using System;
using System.Collections.Generic;
using System.Text;

namespace Exam.CoreData.Models.Users
{
    public class SearchUserModel : BaseSearchModel
    {
        public string TextSearch { get; set; }

        public UserStatus? Status { get; set; }
    }
}
