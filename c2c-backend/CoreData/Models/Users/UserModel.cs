﻿using Exam.CoreData.Enums;
using Exam.CoreData.Models.Wallets;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace Exam.CoreData.Models.Users
{
    public class UserModel
    {
        public int Id { get; set; }

        public string FullName { get; set; }

        public string Email { get; set; }

        public UserStatus Status { get; set; }

        public string Mobile { get; set; }

        public int? Gender { get; set; }

        public string UserRoles { get; set; }

        public string Permissions { get; set; }

        public int? DisplayOrder { get; set; }

        [JsonIgnore]
        public bool IsUserSide { get; set; }

        public bool? OnCms { get; set; }

        public WalletModel Wallet { get; set; }

        public decimal AssetsTotal { get; set; } = 0;

        public DateTime? DateOfBirth { get; set; }

        public Guid? ImageId { get; set; }

        public int? CategoryId { get; set; }

        public string CategoryName { get; set; }

        public int? ShopPageId { get; set; }

        public string ShopPageName { get; set; }
    }
}
