﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Exam.CoreData.Models.Users
{
    public class UpdateMyProfileModel
    {
        public int Id { get; set; }

        public string FullName { get; set; }

        public string Email { get; set; }

        public string Mobile { get; set; }

        public int? Gender { get; set; }

        public DateTime? DateOfBirth { get; set; }

    }
}
