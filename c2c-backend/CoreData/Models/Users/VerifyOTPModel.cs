﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Exam.CoreData.Models.Users
{
    public class VerifyOTPModel
    {
        [Required]
        public string Email { get; set; }
        public string OTP { get; set; }
    }
}
