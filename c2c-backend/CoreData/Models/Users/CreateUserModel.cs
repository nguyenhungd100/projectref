﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace Exam.CoreData.Models.Users
{
    public class CreateUserModel
    {
        public string FullName { get; set; }

        public string Email { get; set; }

        public string Password { get; set; }

        public string Mobile { get; set; }

        public int? Gender { get; set; }

        public string UserRoles { get; set; }

        public bool IsUserSide { get; set; } = true;

        public DateTime? DateOfBirth { get; set; }

        [JsonIgnore]
        public int PersonAdd { get; set; }

    }
}
