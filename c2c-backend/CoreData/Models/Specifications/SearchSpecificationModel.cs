﻿using Exam.CoreData.Models.PagingInfo;
using System;
using System.Collections.Generic;
using System.Text;

namespace Exam.CoreData.Models.Specifications
{
    public class SearchSpecificationModel : BaseSearchModel
    {
        public int? CategoryId { get; set; }
    }    
}
