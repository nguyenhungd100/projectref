﻿using Exam.CoreData.Data.Entities;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace Exam.CoreData.Models.Specifications
{
    public class SpecificationModel
    {
        public int CatetgoryId { get; set; }

        public string Name { get; set; }

        [JsonIgnore]
        public SpecificationType Type { get; set; }

        public string TypeDescription 
        {
            get { return Type.ToString(); }
        }

        public int DisplayOrder { get; set; }
    }
}
