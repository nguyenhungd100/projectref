﻿using Exam.CoreData.Data.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Exam.CoreData.Models.Specifications
{
    public class SaveSpecificationModel
    {
        public int Id { get; set; }

        public int CatetgoryId { get; set; }

        public string Name { get; set; }

        public SpecificationType Type { get; set; }

        public int DisplayOrder { get; set; }
    }
}
