﻿using Exam.CoreData.Data.Entities;
using Exam.Libraries.Utils;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace Exam.CoreData.Models.TransactionHistories
{
    public class TransactionHistoryModel
    {
        [JsonIgnore]
        public int Id { get; set; }

        public int UserId { get; set; }

        public decimal TransactionAmount { get; set; }

        public DateTime TransactionTime { get; set; }

        public TransactionType TransactionType { get; set; }

        public string MessageType
        {
            get
            {
                return EnumHelper.GetDescription(TransactionType);
            }
        }
        public TransactionStatus Status { get; set; }

        public string MessageStatus
        {
            get
            {
                return EnumHelper.GetDescription(Status);
            }
        }
    }
}
