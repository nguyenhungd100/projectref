﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace Exam.CoreData.Models.TransactionHistories
{
    public class VerifyCheckOutModel
    {
        public int OrderCode { get; set; }

        [JsonIgnore]
        public int UserId { get; set; }
    }
}
