﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace Exam.CoreData.Models.TransactionHistories
{
    public class CheckOutModel
    {
        public string BankCode { get; set; }

        public string OrderCode { get; set; }

        [JsonIgnore]
        public int UserId { get; set; }
    }
}
