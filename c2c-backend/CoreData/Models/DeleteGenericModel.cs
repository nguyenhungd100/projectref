﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Exam.CoreData.Models
{
    public class DeleteGenericModel
    {
        public List<int> Ids { get; set; }
    }
}
