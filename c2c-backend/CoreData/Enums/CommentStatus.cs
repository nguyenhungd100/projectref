﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Exam.CoreData.Enums
{
    public enum CommentStatus
    {
        Active = 1,
        Deleted = 10
    }
}
