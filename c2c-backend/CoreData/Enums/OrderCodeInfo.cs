﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Exam.CoreData.Enums
{
    public enum OrderCodeInfo
    {
        Value2000 = 2,

        Value5000 = 5,
        Value10000 =10,
        Value20000 = 20
    }
}
