﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Exam.CoreData.Enums
{
    public enum PostStatus
    {
        Init = 1,
        Approve = 5,
        Reject = 10,
        Lock = 15
    }
}
