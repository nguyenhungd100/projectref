﻿using Exam.CoreData.Enums;
using Exam.CoreData.Models.Users;
using Exam.Libraries.Utils;
using Exam.Services.RoleFacade;
using Exam.Services.UserFacade;
using IdentityServer4.Models;
using IdentityServer4.Validation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Exam.Authentication.IdentityServer
{
    

    public class ResourceOwnerPasswordValidator : IResourceOwnerPasswordValidator
    {
        private readonly IAuthorizationService _authorizationService;
        private readonly IRoleService _roleService;

        public ResourceOwnerPasswordValidator(IAuthorizationService authorizationService, IRoleService roleService)
        {
            _authorizationService = authorizationService;
            _roleService = roleService;
        }

        public async Task ValidateAsync(ResourceOwnerPasswordValidationContext context)
        {
            try
            {
                UserModel userModel = null;
                var result = _authorizationService.VerifyPassword(context.UserName, context.Password, ref userModel);
                if (result == VerifyLoginMessageEnum.IncorrectEmail)
                {
                    context.Result = new GrantValidationResult(TokenRequestErrors.InvalidGrant, "Tài khoản không tồn tại !");
                }
                if (result == VerifyLoginMessageEnum.NotActived)
                {
                    context.Result = new GrantValidationResult(TokenRequestErrors.InvalidGrant, "Tài khoản chưa được kích hoạt. Vui lòng liên hệ ban quản trị !");
                }
                if (result == VerifyLoginMessageEnum.IncorectPass)
                {
                    context.Result = new GrantValidationResult(TokenRequestErrors.InvalidGrant, "Sai mật khẩu !");
                }
                if (result == VerifyLoginMessageEnum.Success)
                {
                    if (userModel != null)
                    {
                        var clientName = context.Request.Raw["client_type"];
                        context.Result = new GrantValidationResult(
                            subject: userModel.Id.ToString(),
                            authenticationMethod: "custom",
                            claims: ConfigIdentityServer.GetUserClaims(userModel, _roleService));
                    }
                    // var userModel = _authorizationService.GetUser(context.UserName);
                    else context.Result = new GrantValidationResult(TokenRequestErrors.InvalidGrant, "Lỗi hệ thống !");
                }                 
            }
            catch (Exception ex)
            {
                context.Result = new GrantValidationResult(TokenRequestErrors.InvalidRequest, ex.Message);
            }
        }
    }
}
