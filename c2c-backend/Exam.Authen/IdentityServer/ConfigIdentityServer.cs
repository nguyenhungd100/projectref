﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Exam.Authen.IdentityServer
{
    public class ConfigIdentityServer
    {
        //public static IEnumerable<IdentityResource> GetIdentityResources()
        //{
        //    return new List<IdentityResource>
        //    {
        //        new IdentityResources.OpenId(),
        //        new IdentityResources.Profile()
        //    };
        //}

        //public static IEnumerable<ApiResource> GetApiResources()
        //{
        //    return new List<ApiResource>
        //    {
        //        new ApiResource("exam_api", "Exam Api")
        //    };
        //}

        //public static IEnumerable<Client> GetClients(AppSettings appSettings)
        //{
        //    var uris = appSettings.ClientAppRedirectUri;

        //    return new List<Client>
        //    {
        //        new Client
        //        {
        //            ClientId = "client",
        //            AllowedGrantTypes = GrantTypes.ClientCredentials,
        //            ClientSecrets =
        //            {
        //                new Secret("m9wfTF5ZXCvL9u5AOIXBK1zWe9Kd9Kzb".Sha256())
        //            },
        //            AllowedScopes = { "exam_api"}
        //        },

        //        new Client
        //        {
        //            ClientId = "ro.client",
        //            AllowedGrantTypes = GrantTypes.ResourceOwnerPasswordAndClientCredentials,
        //            RedirectUris = new List<string>(uris),
        //            ClientSecrets =
        //            {
        //                new Secret("secret".Sha256())
        //            },
        //            AllowedScopes =
        //            {
        //                "exam_api",
        //                "openid",
        //                "profile",
        //                "offline_access"
        //            },
        //            AllowOfflineAccess = true,
        //            Enabled = true,
        //            AllowAccessTokensViaBrowser = true,
        //            AllowedCorsOrigins = new List<string>(uris),
        //            AccessTokenType = AccessTokenType.Jwt,
        //            IdentityTokenLifetime = 3000,
        //            AccessTokenLifetime = 3600 * 24 * 30,
        //            AuthorizationCodeLifetime = 300
        //        }
        //    };
        //}

        //public static Claim[] GetUserClaims(UserModel userModel, IRoleService roleService)
        //{
        //    var result = string.Empty;
        //    if (!string.IsNullOrEmpty(userModel.UserRoles))
        //    {
        //        var roleIds = userModel.UserRoles.ParseIds();
        //        if (roleIds.Length > 0)
        //        {
        //            var roles = roleService.List(roleIds);
        //            var permissions = new List<int>();
        //            foreach (var role in roles)
        //            {
        //                if (!string.IsNullOrEmpty(role.Permissions))
        //                {
        //                    roleIds = role.Permissions.ParseIds();
        //                    permissions.AddRange(roleIds);
        //                }
        //            }
        //            var res = permissions.ToArray().RemoveDuplicates();
        //            result = string.Join(";", res);
        //            if (!string.IsNullOrEmpty(result))
        //                result = ";" + result + ";";
        //        }
        //    }
        //    return new Claim[]
        //    {
        //        new Claim("user_id", userModel.Id.ToString() ?? ""),
        //        new Claim(JwtClaimTypes.Name, userModel.FullName ?? string.Empty),
        //        new Claim(JwtClaimTypes.Email, userModel.Email ?? string.Empty),
        //        new Claim("permissions", result)
        //    };
        //}
    }
}
