﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Exam.CoreData.Data.Entities
{
    public class TestTable1
    {
        [Key]
        public long Id { get; set; }

        public string Name { get; set; }
    }
}
