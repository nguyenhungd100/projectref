﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Exam.CoreData.Models.Patients
{
    public class DeleteManyPatientModel
    {
        public List<string> Ids { get; set; }
    }
}
