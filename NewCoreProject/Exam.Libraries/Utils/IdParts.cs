﻿using IdGen;
using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Exam.Libraries.Utils
{
    public class IdPartModel
    {
        [JsonIgnore]
        public string Bits { get; set; }
        [JsonIgnore]
        public TimeSpan Times { get; set; }
        public short GeneratorId { get; set; }
        public long Sequence { get; set; }
        public DateTime Date { get; set; }
    }

    public static class IdParts
    {
       public static long AutogenId(int generatorId)
        {
            var generator = new IdGenerator(generatorId);
            return generator.CreateId();
        }

        public static IEnumerable<long> AutogenId(int generatorId, int count)
        {
            var generator = (IEnumerable)new IdGenerator(generatorId).GetEnumerator();
            var ids = generator.OfType<long>().Take(count).ToArray();
            return ids;
        }

        public static IdPartModel DecomposeIdPart(long id)
        {
            var generator = new IdGenerator(1);
            var epoc = generator.Epoch.LocalDateTime;

            var bytes = BitConverter.GetBytes(id).Reverse();
            var bits3 = string.Concat(bytes.Select(c => Convert.ToString((byte)c, 2)));
            var bits = string.Concat(bytes.Select(c => Convert.ToString((byte)c, 2).PadLeft(8, '0')));
            var timespan = TimeSpan.FromMilliseconds(Convert.ToUInt64(bits.Substring(0, 42), 2));

            return new IdPartModel
            {
                Bits = bits,
                GeneratorId = Convert.ToInt16(bits.Substring(42, 10), 2),
                Sequence = Convert.ToInt64(bits.Substring(52, 12), 2),
                Times = timespan,
                Date = new DateTime(timespan.Ticks + epoc.Ticks)
            };
        }

        public static IdPartModel DecomposeIdPartInsta(long id)
        {
            
            var bytes = BitConverter.GetBytes(id).Reverse();
            var bits = string.Concat(bytes.Select(c => Convert.ToString((byte)c, 2).PadLeft(8, '0')));
           // var bits = string.Concat(bytes.Select(c => Convert.ToString((byte)c, 2)));
            var timespan = TimeSpan.FromMilliseconds(Convert.ToUInt64(bits.Substring(0, 41), 2));
            var epocNew = new DateTime(1970,1,1);

            var da = new DateTime(timespan.Ticks + epocNew.Ticks);
            
            var a = Convert.ToInt16(bits.Substring(41, 13), 2);
            var b = Convert.ToInt64(bits.Substring(54, 10), 2);
            var e = timespan;
            //var d = new DateTime(timespan.Ticks + epoc.Ticks);
            return new IdPartModel
            {
                Bits = bits,
                GeneratorId = Convert.ToInt16(bits.Substring(41, 13), 2),
                Sequence = Convert.ToInt64(bits.Substring(54, 10), 2),
                Times = timespan,
                //Date = new DateTime(timespan.Ticks + epoc.Ticks)
            };
        }

    }
}
