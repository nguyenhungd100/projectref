﻿using ChannelVN.IMS2.WebApi.Config;
using ChannelVN.IMS2.WebApi.Env;
using ChannelVN.IMS2.WebApi.Filters.ActionFilters;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpOverrides;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Cors.Internal;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace ChannelVN.IMS2.WebApi
{
    public partial class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;

            // Bind the configuration information to common appsettings object.
            Configuration.LoadAppSettings();
        }

        public IConfiguration Configuration { get; set; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            ConfigureJwtAuthService(services);

            // Add service and create Policy with options
            services.AddCors(options =>
            {
                options.AddPolicy("CorsPolicy",
                builder =>
                {
                    builder
                    .AllowAnyOrigin() //.WithOrigins("http://localhost", "http://127.0.0.1")
                    .AllowAnyMethod()
                    .AllowAnyHeader()
                    .AllowCredentials();
                });
            });

            services.AddHttpsRedirection(options =>
            {
                options.HttpsPort = 443;
            });

            services.AddResponseCompression(options =>
            {
                options.EnableForHttps = true;
                options.Providers.Add<Microsoft.AspNetCore.ResponseCompression.GzipCompressionProvider>();
            });
            
            //services.AddHostedService<StreamService>();
            //services.AddHostedService<ConsumeScopedServiceHostedService>();
            //services.AddScoped<IScopedProcessingService, ScopedProcessingService>();

            services.AddMvc(
                options =>
                {
                    options.Filters.Add(new CorsAuthorizationFilterFactory("CorsPolicy"));

                    options.ModelBinderProviders.Insert(0, new DateTimeModelBinderProvider());

                    options.Filters.Add(new ValidateModelAttribute());
                    options.Filters.Add(new Filters.ExceptionFilters.UncatchExceptionAttribute());
                })
                .AddJsonOptions(options =>
                {
                    options.SerializerSettings.DateFormatString = "yyyy-MM-ddTHH:mm:ssZ";
                    //var resolver = options.SerializerSettings.ContractResolver;
                    //if (resolver != null)
                    //    (resolver as DefaultContractResolver).NamingStrategy = null;
                })
                .SetCompatibilityVersion(CompatibilityVersion.Version_2_1);
            
            services.AddDistributedMemoryCache();

            services.AddSession(options =>
            {
                var appsettings = Foundation.Common.Configuration.AppSettings.Current;

                options.IdleTimeout = System.TimeSpan.FromMinutes(appsettings.ServiceConfiguration.TokenExpiresIn);
                options.Cookie.Name = ".Oauth.Session";
                options.Cookie.HttpOnly = true;
                options.Cookie.IsEssential = true;
            });

            services.ConfigDI();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment() || env.IsTest())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseHsts();
            }
            //app.UseHttpsRedirection();

            app.UseCookiePolicy();
            app.UseSession();
            app.UseAuthentication();
            app.UseResponseCompression();
            app.UseXssProtection();
            app.UseStaticFiles();

            app.UseMvc();
            app.UseForwardedHeaders(new ForwardedHeadersOptions
            {
                ForwardedHeaders = ForwardedHeaders.XForwardedFor |
                ForwardedHeaders.XForwardedProto
            });
        }
    }
}
