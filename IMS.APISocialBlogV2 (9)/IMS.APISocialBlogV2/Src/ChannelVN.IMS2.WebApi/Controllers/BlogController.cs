﻿using ChannelVN.IMS2.Core.Entities.Base;
using ChannelVN.IMS2.Core.Models.Models.Blogs;
using ChannelVN.IMS2.Core.Models.Models.TestTables;
using ChannelVN.IMS2.Core.Services.BlogFacade;
using ChannelVN.IMS2.Foundation.Common;
using ChannelVN.IMS2.Foundation.Common.Configuration;
using ChannelVN.IMS2.Foundation.Logging;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net.Http;
using System.Security.Claims;
using System.Threading.Tasks;

namespace ChannelVN.IMS2.WebApi.Controllers
{
    [Route("api/[controller]")]
    public class BlogController : AbstractController
    {
        private readonly IBlogService _blogService;

        public BlogController(IBlogService blogService)
        {
            _blogService = blogService;
        }

        /// <summary>
        /// Get by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("getbyid/{id}")]
        public IActionResult GetById(string id)
        {

            var listTask = new List<long>();
            for (int i = 0; i < 1000000; i++)
            {
                listTask.Add(Task.Run(()=>IdParts.AutogenIdWithCodeGen()).Result);
            }

            var query = listTask.GroupBy(x => x)
              .Where(g => g.Count() > 1)
              .Select(y => new { Element = y.Key, Counter = y.Count() })
              .ToList();





            //var result = _blogService.GetById(id);

            return Json(new { success = true, data = listTask });
        }

        /// <summary>
        /// Search
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost("search")]
        public IActionResult SearchTest([FromBody] SearchTestModel model)
        {
            var result = _blogService.SearchTest(model);
            return Json(new { success = true, data = result });
        }

        /// <summary>
        /// Test add One
        /// </summary>
        /// <returns></returns>
        /// <param name="model"></param>
        [HttpPost("create")]
        public IActionResult AddOneTest([FromBody] TestTableModel model)
        {
            var result = _blogService.AddOneTest(model);
            return Json(new { success = result });
        }

        /// <summary>
        /// Test add Many
        /// </summary>
        /// <returns></returns>
        [HttpPost("create-many")]
        public async Task<IActionResult> AddManyTest(/*[FromBody] IEnumerable<TestTable1Model> models*/)
        {
            var models = new List<TestTableModel>();
            var result = await _blogService.AddManyTest(models);
            return Json(new { success = result });
        }

        /// <summary>
        /// Update test record
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPut("update")]
        public IActionResult UpdateOne([FromBody] TestTableModel model)
        {
            var result = _blogService.UpdateOne(model);
            return Json(new { success = result });
        }

        /// <summary>
        /// Delete test record
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpDelete("delete")]
        public IActionResult DeleteOne([FromBody] TestTableModel model)
        {
            var result = _blogService.DeleteOne(model);
            return Json(new { success = result });
        }

        /// <summary>
        /// Test Decode
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("testdecode/{id}")]
        public IActionResult TestDecode(string id)
        {
            var result = _blogService.TestDecode(id);
            return Json(new { success = true, data = result });
        }

        [HttpGet("bulkcopy")]
        public IActionResult BulkCopy()
        {
            var result = _blogService.BulkCopy();
            return Json(new { success = true });
        }

        [HttpGet("autogen")]
        public IActionResult AutoGen()
        {
            var result = _blogService.AutoGen();
            return Json(new { success = true, data = result });
        }
    }
}
