﻿using Microsoft.Extensions.DependencyInjection;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using System.Threading.Tasks;
using Microsoft.IdentityModel.Tokens;
using System;
using ChannelVN.IMS2.Foundation.Security.Bearer.Helpers;
using ChannelVN.IMS2.Foundation.Logging;
using Microsoft.AspNetCore.Http;
using ChannelVN.IMS2.Foundation.Common.Configuration;
using System.Linq;

namespace ChannelVN.IMS2.WebApi
{
    public partial class Startup
    {
        private void ConfigureJwtAuthService(IServiceCollection services)
        {
            var appSettings = AppSettings.Current;

            services.AddAuthentication(options =>
            {
                options.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                options.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
            })
            .AddJwtBearer(options =>
            {
                options.TokenValidationParameters = new TokenValidationParameters
                {
                    // Validate the JWT Issuer (iss) claim  
                    ValidateIssuer = true,
                    ValidIssuer = appSettings.ServiceConfiguration.Namespace,

                    // Validate the JWT Audience (aud) claim  
                    ValidateAudience = true,
                    //ValidAudience = "Any",

                    // Validate the token expiry  
                    ValidateLifetime = true,

                    // The signing key must match!  
                    ValidateIssuerSigningKey = true,
                    IssuerSigningKey = JwtSecurityKey.Create(appSettings.ChannelConfiguration.SecretKey),

                    // If you want to allow a certain amount of clock drift, set that here
                    ClockSkew = TimeSpan.Zero
                };

                options.Events = new JwtBearerEvents
                {
                    OnMessageReceived = context =>
                    {
                        var value = context.Request.Headers["Authorization"].ToString();
                        if (!string.IsNullOrEmpty(value) && value.ToLower().Contains("bearer"))
                        {
                            value = value.Trim().Substring(6).Trim();
                        }
                        if (!string.IsNullOrEmpty(value))
                        {
                            context.Token = value;
                            // Bind auth info to httpcontext.users.claims
                            Task.Run(() =>
                            {
                                return Foundation.Security.SecureParser.Parse(value);
                            }).ContinueWith(authInfo =>
                            {
                                Foundation.Security.SecureParser.BindClaims(context.HttpContext.User, authInfo.Result);
                                return authInfo.Result;
                            }).ContinueWith(authInfo =>
                            {
                                context.Options.TokenValidationParameters.ValidIssuer = authInfo.Result.iss;
                                context.Options.TokenValidationParameters.ValidAudience = authInfo.Result.aud;
                            }).Wait();
                        }

                        return Task.CompletedTask;
                    },
                    OnTokenValidated = context =>
                    {
                        //Logger.Debug("OnTokenValidated: " + context.SecurityToken);
                        return Task.CompletedTask;
                    },
                    OnAuthenticationFailed = context =>
                    {
                        Logger.Error("OnAuthenticationFailed: " + context.Exception.Message);
                        return Task.CompletedTask;
                    },
                    OnChallenge = context =>
                    {
                        // Skip the default logic.
                        context.HandleResponse();
                        context.Response.ContentType = "application/json";
                        context.Response.StatusCode = 401;

                        var payload = Foundation.Common.Json.Stringify(new
                        {
                            message = "Authorization has been denied for this request."
                            //error = "invalid_secret",
                            //error_description = context.Exception.Message
                        });
                        context.Response.WriteAsync(payload.ToString()).Wait();

                        Logger.Error("OnChallenge: " + context.AuthenticateFailure);
                        return Task.CompletedTask;
                    }
                };
            });
        }
    }
}