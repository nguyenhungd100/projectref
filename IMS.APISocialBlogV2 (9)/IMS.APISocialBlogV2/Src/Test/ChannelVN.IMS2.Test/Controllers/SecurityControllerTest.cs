﻿using ChannelVN.IMS2.Core.Entities.Security;
using ChannelVN.IMS2.WebApi.Controllers;
using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.TestHost;
using System.Net.Http;
using ChannelVN.IMS2.WebApi;
using Xunit;
using System.Threading.Tasks;
using ChannelVN.IMS2.Foundation.Common;

namespace ChannelVN.IMS2.Test.Controllers
{
    public class SecurityControllerTest
    {
        private readonly string tokenValue = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxNzA2NzA0NTUiLCJqdGkiOiJmZTBiYTJjNy00M2Y3LTRkZWMtYjI3ZS1mNzk3NDcwN2E0MWIiLCJleHAiOjE1NzU3NzM5NTQsImlzcyI6IlZjY29ycCIsImF1ZCI6IkFueSJ9.zfBHp5Nm96Bi23G4XnGpQQFZSLkD8Q27JrxHq_JxmrA";
        private readonly string[] args;
        private readonly TestServer _server;
        private readonly HttpClient _client;

        public SecurityControllerTest()
        {
            _server = new TestServer(Program.CreateWebHostBuilder(args));
            _client = _server.CreateClient();
            _client.DefaultRequestHeaders.Add("Authorization", tokenValue);
        }

        public static IEnumerable<object[]> AccountTestData =>
        new List<object[]>
        {
            new object[] { new Account {
                Avatar="avatar_url",
                Banner="banner_url",
                Id=1234567872,
                Description="description",
                Email="email@gov.vn",
                FullName="FullName",
                IsFullPermission=false,
                IsSystem=false,
                LoginedDate=DateTime.Now,
                Mobile="0986095508",
                OtpSecretKey="123",
                Password="123",
                UserName="UserName",
                VerifiedBy="Aloso",
                VerifiedDate=DateTime.Now
            },true},
            new object[] { new Account {
                Avatar="avatar_url",
                Banner="banner_url",
                Id=0,
                Description="description",
                Email="email@gov.vn",
                FullName="FullName",
                IsFullPermission=false,
                IsSystem=false,
                LoginedDate=DateTime.Now,
                Mobile="0986095508",
                OtpSecretKey="123",
                Password="123",
                UserName="UserName",
                VerifiedBy="Aloso",
                VerifiedDate=DateTime.Now
            },true}
        };

        [Theory]
        //[ClassData(typeof(CalculatorTestData))]
        [MemberData(nameof(AccountTestData))]
        public void AddTestAsync(Account data1,bool data2)
        {
            var account = Utility.ConvertToKeyValuePair(data1);

            var content = new FormUrlEncodedContent(account);
            Assert.True(true == data2);
            //var response = await _client.PostAsync("/api/account/add", content);
            //response.EnsureSuccessStatusCode();

            //var responseString = await response.Content.ReadAsStringAsync();

            //var okResult = Json.Parse<ActionResponse<object>>(responseString);
            //Assert.IsType<ActionResponse<object>>(okResult);
            //Assert.True(okResult.Success == data2);
        }
    }

    
}
