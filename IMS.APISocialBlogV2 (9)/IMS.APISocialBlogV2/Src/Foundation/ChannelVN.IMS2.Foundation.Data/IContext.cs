﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ChannelVN.IMS2.Foundation.Data
{
    public interface IContext
    {
        /// <summary>
        /// Đối tượng quản lý tham số chung
        /// </summary>
        ContextOptions Options { get; set; }

    }
}
