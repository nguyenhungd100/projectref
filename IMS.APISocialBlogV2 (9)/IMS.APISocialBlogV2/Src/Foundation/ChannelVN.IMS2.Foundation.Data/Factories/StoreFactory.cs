﻿using System.Collections.Generic;

namespace ChannelVN.IMS2.Foundation.Data.Factories
{
    public static class StoreFactory
    {
        /// <summary>
        /// Danh sách các thể hiện của đối tượng kho lưu
        /// </summary>
        private static IDictionary<string, object> _stores 
            = new Dictionary<string, object>();

        /// <summary>
        /// Hàm khởi tạo kho lưu
        /// </summary>
        /// <typeparam name="TStore"></typeparam>
        /// <returns></returns>
        public static TStore Create<TStore>() where TStore : class, IStore, new()
        {
            var storeName = typeof(TStore)?.FullName;
            if (!_stores.ContainsKey(storeName))
            {
                _stores[storeName] = new TStore();
            }
            return _stores[storeName] as TStore;
        }
    }
}