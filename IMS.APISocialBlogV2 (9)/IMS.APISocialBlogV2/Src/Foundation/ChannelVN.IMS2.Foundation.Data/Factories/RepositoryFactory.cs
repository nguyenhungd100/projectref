﻿using System.Collections.Generic;

namespace ChannelVN.IMS2.Foundation.Data.Factories
{
    public static class RepositoryFactory
    {
        /// <summary>
        /// Danh sách các thể hiện của đối tượng kho nghiệp vụ
        /// </summary>
        private static IDictionary<string, object> _repositories = new Dictionary<string, object>();

        /// <summary>
        /// Hàm khởi tạo kho nghiệp vụ
        /// </summary>
        /// <typeparam name="TRepository"></typeparam>
        /// <returns></returns>
        public static TRepository Create<TRepository>() where TRepository : class, IRepository, new()
        {
            var repositoryName = typeof(TRepository)?.FullName;
            if (!_repositories.ContainsKey(repositoryName))
            {
                _repositories[repositoryName] = new TRepository();
            }
            return _repositories[repositoryName] as TRepository;
        }
    }
}
