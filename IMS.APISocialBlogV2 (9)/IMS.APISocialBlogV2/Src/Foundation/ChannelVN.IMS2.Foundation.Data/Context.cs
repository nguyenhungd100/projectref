﻿using ChannelVN.IMS2.Foundation.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ChannelVN.IMS2.Foundation.Data
{
    public abstract class Context : IContext, IDisposable
    {
        // implement MySql/MsSql connector

        public Context()
            : this(new ContextOptions())
        {
        }

        public Context(ContextOptions options) => Options = options;

        public virtual void Dispose()
        {
            //throw new NotImplementedException();
        }
    
        public enum DatabaseType
        {
            MsSql = 1,
            RedisCache = 5,
            ElasticSearch = 10
        }

        /// <summary>
        /// Đối tượng quản lý tham số chung
        /// </summary>
        public ContextOptions Options { get; set; }

        protected virtual IEnumerable<Dictionary<string, object>> DbCollections 
            => (IEnumerable<Dictionary<string, object>>)Options.GetValue("ConnectionStrings");

        protected virtual (string connectionStr, int DbCode) GetConnectionStrFromId(DatabaseType dbType, long? id = null)
        {
            if (id.HasValue)
            {
                var seperate = IdParts.SeparateInfoDb(ChosenDbFromId(id.Value, dbType));
                return (seperate.connectionStr, seperate.dbCode);
            }
            else
            { 
                var auto = AutoRandomConnection();
                var seperate = IdParts.SeparateInfoDb(auto);
                return (seperate.connectionStr, seperate.dbCode);
            }           
        }

        protected virtual Dictionary<string, object> ChosenDbFromId(long id, DatabaseType dbType)
        {
            var res = new Dictionary<string, object>();
            var shardId = 0;

            var idParts = IdParts.DecomposeIdPartWithCodeGen(id);
            switch (dbType)
            {
                case DatabaseType.MsSql:
                    shardId = idParts.ShardIdSql;
                   
                    break;
                case DatabaseType.RedisCache:
                    shardId = idParts.ShardIdCache;
                    break;
                case DatabaseType.ElasticSearch:
                    shardId = idParts.ShardIdEs;
                    break;
                default:
                    break;
            }
            res = DbCollections.FirstOrDefault(c => c["DbCode"].ToString() == shardId.ToString());
            return res;
        }

        protected virtual Dictionary<string, object> AutoRandomConnection()
        {
            var rd = new Random();
            var randomDb = DbCollections.ElementAt(index: rd.Next(DbCollections.Count() - 1));
            return randomDb;
        }

      
    }
}
