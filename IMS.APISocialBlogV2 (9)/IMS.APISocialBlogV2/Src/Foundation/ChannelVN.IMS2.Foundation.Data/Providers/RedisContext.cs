﻿using ChannelVN.IMS2.Foundation.Common;
using StackExchange.Redis;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace ChannelVN.IMS2.Foundation.Data.Providers
{
    sealed class RedisConnectionManager
    {
        /// <summary>
        /// 
        /// </summary>
        private static Dictionary<string, Lazy<ConnectionMultiplexer>> _lazyConnection
            = new Dictionary<string, Lazy<ConnectionMultiplexer>>();

        /// <summary>
        /// 
        /// </summary>
        public RedisConnectionManager()
        {
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="connectionKey"></param>
        /// <param name="connectionHandler"></param>
        public RedisConnectionManager(string connectionKey, Func<ConnectionMultiplexer> connectionHandler)
            => Connect(connectionKey, connectionHandler);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="connectionKey"></param>
        /// <param name="connectionHandler"></param>
        /// <returns></returns>
        public RedisConnectionManager Connect(string connectionKey, Func<ConnectionMultiplexer> connectionHandler)
        {
            if (!_lazyConnection.ContainsKey(connectionKey))
            {
                _lazyConnection.Add(connectionKey, new Lazy<ConnectionMultiplexer>(connectionHandler));
            }
            return this;
        }

        public Lazy<ConnectionMultiplexer> this[string connectionKey]
            => _lazyConnection.GetValueOrDefault(connectionKey);
    }

    public sealed class RedisContext : Context, IDatabase
    {
        private static readonly object _synRoot = new object();
        private RedisConnectionManager _connectionManager = new RedisConnectionManager();

        #region Constructor

        public RedisContext()
        {
        }

        public RedisContext(ContextOptions options)
            : base(options)
        {
        }
        #endregion

        #region Properties

        public string ConnectionString
        {
            get
            {
                return (string)(Options.GetValue("ConnectionString") ?? string.Empty);
            }
        }

        public char SeparateChar
        {
            get
            {
                return (char)(Options.GetValue("SeparateChar") ?? ':');
            }
        }

        public string Namespace
        {
            get
            {
                return ((string)(Options.GetValue("Namespace") ?? string.Empty)).Trim();
            }
            set
            {
                if (!Options.ContainsKey("Namespace"))
                {
                    Options.Add("Namespace", value);
                }
                else
                {
                    Options["Namespace"] = value;
                }
            }
        }


        #region Private methods

        private IConnectionMultiplexer GetConnection(long? id = null)
        {
            var connectionStr = string.Empty;
            try
            {               
                if (id.HasValue)
                    connectionStr = GetConnectionStrFromId( DatabaseType.RedisCache,id.Value).connectionStr;
                else connectionStr = GetConnectionStrFromId(DatabaseType.RedisCache).connectionStr; ;
                _connectionManager.Connect(connectionStr, () => ConnectionMultiplexer.Connect(Parse(connectionStr)));

                //{
                //    if (Multiplexer == null || !Multiplexer.IsConnected)
                //    {
                //        lock (_synRoot)
                //        {
                //            if (Multiplexer == null || !Multiplexer.IsConnected)
                //            {
                //                _connectionManager.Connect(ConnectionString, () => ConnectionMultiplexer.Connect(Parse(ConnectionString)));
                //            }
                //        }
                //    }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return _connectionManager[connectionStr]?.Value;
        }

        public RedisKey GetRedisKeySpecified<T>(long id) where T : class
        {
            return ObjectHelper.GetTableName<T>() + ":" + id.ToString();
        }

        public RedisKey GenerateRedisKey<T>() where T : class
        {
           // var random = IdParts.SeparateInfoDb(AutoRandomConnection());
            var id = IdParts.AutogenIdWithCodeGen();
            return GetRedisKeySpecified<T>(id);
        }

        public (string tableName, long id) SeparateRedisKey(RedisKey key)
        {
            var separate = key.ToString().Split(":");
            if (separate.Count() == 2)
            {
                return (separate[0], long.Parse(separate[1]));
            }
            else
                return (null, long.Parse(separate[0]));
        }


        private ConfigurationOptions Parse(string connectionString, bool ignoreUnknown = true)
        {
            try
            {
                var options = ConfigurationOptions.Parse(connectionString, ignoreUnknown);

                options.AbortOnConnectFail = false;
                //options.ClientName = "DESKTOP-PC893";
                //options.EndPoints.Add("localhost", 6379);
                //options.Ssl = true;
                //options.Password = "";
                //options.AllowAdmin = true;
                //options.KeepAlive = 30;
                options.ConnectRetry = 8;
                options.ConnectTimeout = 100000;
                options.SyncTimeout = 100000;

                return options;
            }
            catch
            {
                return new ConfigurationOptions();
            }
        }

        private byte[] GetBytes(string value)
        {
            if (String.IsNullOrEmpty(value))
            {
                return null;
            }
            return Encoding.ASCII.GetBytes(value);
        }
        #endregion

        #region Public methods

        public string NameOf(Object entity)
        {
            var name = "";
            if (entity != null)
            {
                if (String.IsNullOrEmpty(Namespace))
                    name = entity.GetType().Name.ToLower();
                else
                    name = $"{Namespace}{SeparateChar}{entity.GetType().Name}".ToLower();
            }
            return name;
        }

        public string NameOf(params string[] values)
        {
            var name = "";
            if (null != values)
            {
                var strValue = String.Join(SeparateChar, values);
                if (!String.IsNullOrEmpty(strValue))
                {
                    if (String.IsNullOrEmpty(Namespace))
                        name = strValue.ToLower();
                    else
                        name = $"{Namespace}{SeparateChar}{strValue}".ToLower();
                }
            }
            return name;
        }
        public string NameOf<T>()
        {
            var name = "";
            if (null != typeof(T).Name)
            {
                var strValue = String.Join(SeparateChar, typeof(T).Name);
                if (!String.IsNullOrEmpty(strValue))
                {
                    if (String.IsNullOrEmpty(Namespace))
                        name = strValue.ToLower();
                    else
                        name = $"{Namespace}{SeparateChar}{strValue}".ToLower();
                }
            }
            return name;
        }


        #endregion

        #region IDisposable implementation

        public override void Dispose()
        {
            //if ((Multiplexer != null) && Multiplexer.IsConnected)
            //{
            //    Multiplexer.Close(false);
            //    Multiplexer.Dispose();
            //}
        }
        #endregion

        #region IDatabase implementation

        public int Database => throw new NotImplementedException();

        public IConnectionMultiplexer Multiplexer
            => _connectionManager[ConnectionString]?.Value;

        public IBatch CreateBatch(object asyncState = null)
        {
            IBatch returnValue;
            var conn = GetConnection();
            {
                var db = conn.GetDatabase();
                returnValue = db.CreateBatch(asyncState);
            }
            return returnValue;
        }

        public ITransaction CreateTransaction(object asyncState = null)
        {
            ITransaction returnValue;
            var conn = GetConnection();
            {
                var db = conn.GetDatabase();
                returnValue = db.CreateTransaction(asyncState);
            }
            return returnValue;
        }

        public RedisValue DebugObject(RedisKey key, CommandFlags flags = CommandFlags.None)
        {
            RedisValue returnValue;
            var conn = GetConnection(SeparateRedisKey(key).id);
            {
                var db = conn.GetDatabase();
                returnValue = db.DebugObject(NameOf(key), flags);
            }
            return returnValue;
        }

        public Task<RedisValue> DebugObjectAsync(RedisKey key, CommandFlags flags = CommandFlags.None)
        {
            Task<RedisValue> returnValue;
            var conn = GetConnection(SeparateRedisKey(key).id);
            {
                var db = conn.GetDatabase();
                returnValue = db.DebugObjectAsync(NameOf(key), flags);
            }
            return returnValue;
        }

        public RedisResult Execute(string command, params object[] args)
        {
            RedisResult returnValue;
            var conn = GetConnection();
            {
                var db = conn.GetDatabase();
                returnValue = db.Execute(command, args);
            }
            return returnValue;
        }

        public RedisResult Execute(string command, ICollection<object> args, CommandFlags flags = CommandFlags.None)
        {
            RedisResult returnValue;
            var conn = GetConnection();
            {
                var db = conn.GetDatabase();
                returnValue = db.Execute(command, args, flags);
            }
            return returnValue;
        }

        public Task<RedisResult> ExecuteAsync(string command, params object[] args)
        {
            Task<RedisResult> returnValue;
            var conn = GetConnection();
            {
                var db = conn.GetDatabase();
                returnValue = db.ExecuteAsync(command, args);
            }
            return returnValue;
        }

        public Task<RedisResult> ExecuteAsync(string command, ICollection<object> args, CommandFlags flags = CommandFlags.None)
        {
            Task<RedisResult> returnValue;
            var conn = GetConnection();
            {
                var db = conn.GetDatabase();
                returnValue = db.ExecuteAsync(command, args, flags);
            }
            return returnValue;
        }

        public bool GeoAdd(RedisKey key, double longitude, double latitude, RedisValue member, CommandFlags flags = CommandFlags.None)
        {
            bool returnValue;
            var conn = GetConnection();
            {
                var db = conn.GetDatabase();
                returnValue = db.GeoAdd(NameOf(key), longitude, latitude, member, flags);
            }
            return returnValue;
        }

        public bool GeoAdd(RedisKey key, GeoEntry value, CommandFlags flags = CommandFlags.None)
        {
            bool returnValue;
            var conn = GetConnection();
            {
                var db = conn.GetDatabase();
                returnValue = db.GeoAdd(NameOf(key), value, flags);
            }
            return returnValue;
        }

        public long GeoAdd(RedisKey key, GeoEntry[] values, CommandFlags flags = CommandFlags.None)
        {
            long returnValue;
            var conn = GetConnection();
            {
                var db = conn.GetDatabase();
                returnValue = db.GeoAdd(NameOf(key), values, flags);
            }
            return returnValue;
        }

        public Task<bool> GeoAddAsync(RedisKey key, double longitude, double latitude, RedisValue member, CommandFlags flags = CommandFlags.None)
        {
            Task<bool> returnValue;
            var conn = GetConnection();
            {
                var db = conn.GetDatabase();
                returnValue = db.GeoAddAsync(NameOf(key), longitude, latitude, member, flags);
            }
            return returnValue;
        }

        public Task<bool> GeoAddAsync(RedisKey key, GeoEntry value, CommandFlags flags = CommandFlags.None)
        {
            Task<bool> returnValue;
            var conn = GetConnection();
            {
                var db = conn.GetDatabase();
                returnValue = db.GeoAddAsync(NameOf(key), value, flags);
            }
            return returnValue;
        }

        public Task<long> GeoAddAsync(RedisKey key, GeoEntry[] values, CommandFlags flags = CommandFlags.None)
        {
            Task<long> returnValue;
            var conn = GetConnection();
            {
                var db = conn.GetDatabase();
                returnValue = db.GeoAddAsync(NameOf(key), values, flags);
            }
            return returnValue;
        }

        public double? GeoDistance(RedisKey key, RedisValue member1, RedisValue member2, GeoUnit unit = GeoUnit.Meters, CommandFlags flags = CommandFlags.None)
        {
            double? returnValue;
            var conn = GetConnection(SeparateRedisKey(key).id);
            {
                var db = conn.GetDatabase();
                returnValue = db.GeoDistance(NameOf(key), member1, member2, unit, flags);
            }
            return returnValue;
        }

        public Task<double?> GeoDistanceAsync(RedisKey key, RedisValue member1, RedisValue member2, GeoUnit unit = GeoUnit.Meters, CommandFlags flags = CommandFlags.None)
        {
            Task<double?> returnValue;
            var conn = GetConnection(SeparateRedisKey(key).id);
            {
                var db = conn.GetDatabase();
                returnValue = db.GeoDistanceAsync(NameOf(key), member1, member2, unit, flags);
            }
            return returnValue;
        }

        public string[] GeoHash(RedisKey key, RedisValue[] members, CommandFlags flags = CommandFlags.None)
        {
            string[] returnValue;
            var conn = GetConnection(SeparateRedisKey(key).id);
            {
                var db = conn.GetDatabase();
                returnValue = db.GeoHash(NameOf(key), members, flags);
            }
            return returnValue;
        }

        public string GeoHash(RedisKey key, RedisValue member, CommandFlags flags = CommandFlags.None)
        {
            string returnValue;
            var conn = GetConnection(SeparateRedisKey(key).id);
            {
                var db = conn.GetDatabase();
                returnValue = db.GeoHash(NameOf(key), member, flags);
            }
            return returnValue;
        }

        public Task<string[]> GeoHashAsync(RedisKey key, RedisValue[] members, CommandFlags flags = CommandFlags.None)
        {
            Task<string[]> returnValue;
            var conn = GetConnection(SeparateRedisKey(key).id);
            {
                var db = conn.GetDatabase();
                returnValue = db.GeoHashAsync(NameOf(key), members, flags);
            }
            return returnValue;
        }

        public Task<string> GeoHashAsync(RedisKey key, RedisValue member, CommandFlags flags = CommandFlags.None)
        {
            Task<string> returnValue;
            var conn = GetConnection(SeparateRedisKey(key).id);
            {
                var db = conn.GetDatabase();
                returnValue = db.GeoHashAsync(NameOf(key), member, flags);
            }
            return returnValue;
        }

        public GeoPosition?[] GeoPosition(RedisKey key, RedisValue[] members, CommandFlags flags = CommandFlags.None)
        {
            GeoPosition?[] returnValue;
            var conn = GetConnection(SeparateRedisKey(key).id);
            {
                var db = conn.GetDatabase();
                returnValue = db.GeoPosition(NameOf(key), members, flags);
            }
            return returnValue;
        }

        public GeoPosition? GeoPosition(RedisKey key, RedisValue member, CommandFlags flags = CommandFlags.None)
        {
            GeoPosition? returnValue;
            var conn = GetConnection(SeparateRedisKey(key).id);
            {
                var db = conn.GetDatabase();
                returnValue = db.GeoPosition(NameOf(key), member, flags);
            }
            return returnValue;
        }

        public Task<GeoPosition?[]> GeoPositionAsync(RedisKey key, RedisValue[] members, CommandFlags flags = CommandFlags.None)
        {
            Task<GeoPosition?[]> returnValue;
            var conn = GetConnection(SeparateRedisKey(key).id);
            {
                var db = conn.GetDatabase();
                returnValue = db.GeoPositionAsync(NameOf(key), members, flags);
            }
            return returnValue;
        }

        public Task<GeoPosition?> GeoPositionAsync(RedisKey key, RedisValue member, CommandFlags flags = CommandFlags.None)
        {
            Task<GeoPosition?> returnValue;
            var conn = GetConnection(SeparateRedisKey(key).id);
            {
                var db = conn.GetDatabase();
                returnValue = db.GeoPositionAsync(NameOf(key), member, flags);
            }
            return returnValue;
        }

        public GeoRadiusResult[] GeoRadius(RedisKey key, RedisValue member, double radius, GeoUnit unit = GeoUnit.Meters, int count = -1, Order? order = null, GeoRadiusOptions options = GeoRadiusOptions.Default, CommandFlags flags = CommandFlags.None)
        {
            GeoRadiusResult[] returnValue;
            var conn = GetConnection(SeparateRedisKey(key).id);
            {
                var db = conn.GetDatabase();
                returnValue = db.GeoRadius(NameOf(key), member, radius, unit, count, order, options, flags);
            }
            return returnValue;
        }

        public GeoRadiusResult[] GeoRadius(RedisKey key, double longitude, double latitude, double radius, GeoUnit unit = GeoUnit.Meters, int count = -1, Order? order = null, GeoRadiusOptions options = GeoRadiusOptions.Default, CommandFlags flags = CommandFlags.None)
        {
            GeoRadiusResult[] returnValue;
            var conn = GetConnection(SeparateRedisKey(key).id);
            {
                var db = conn.GetDatabase();
                returnValue = db.GeoRadius(NameOf(key), longitude, latitude, radius, unit, count, order, options, flags);
            }
            return returnValue;
        }

        public Task<GeoRadiusResult[]> GeoRadiusAsync(RedisKey key, RedisValue member, double radius, GeoUnit unit = GeoUnit.Meters, int count = -1, Order? order = null, GeoRadiusOptions options = GeoRadiusOptions.Default, CommandFlags flags = CommandFlags.None)
        {
            Task<GeoRadiusResult[]> returnValue;
            var conn = GetConnection(SeparateRedisKey(key).id);
            {
                var db = conn.GetDatabase();
                returnValue = db.GeoRadiusAsync(NameOf(key), member, radius, unit, count, order, options, flags);
            }
            return returnValue;
        }

        public Task<GeoRadiusResult[]> GeoRadiusAsync(RedisKey key, double longitude, double latitude, double radius, GeoUnit unit = GeoUnit.Meters, int count = -1, Order? order = null, GeoRadiusOptions options = GeoRadiusOptions.Default, CommandFlags flags = CommandFlags.None)
        {
            Task<GeoRadiusResult[]> returnValue;
            var conn = GetConnection(SeparateRedisKey(key).id);
            {
                var db = conn.GetDatabase();
                returnValue = db.GeoRadiusAsync(NameOf(key), longitude, latitude, radius, unit, count, order, options, flags);
            }
            return returnValue;
        }

        public bool GeoRemove(RedisKey key, RedisValue member, CommandFlags flags = CommandFlags.None)
        {
            bool returnValue;
            var conn = GetConnection(SeparateRedisKey(key).id);
            {
                var db = conn.GetDatabase();
                returnValue = db.GeoRemove(NameOf(key), member, flags);
            }
            return returnValue;
        }

        public Task<bool> GeoRemoveAsync(RedisKey key, RedisValue member, CommandFlags flags = CommandFlags.None)
        {
            Task<bool> returnValue;
            var conn = GetConnection(SeparateRedisKey(key).id);
            {
                var db = conn.GetDatabase();
                returnValue = db.GeoRemoveAsync(NameOf(key), member, flags);
            }
            return returnValue;
        }

        public long HashDecrement(RedisKey key, RedisValue hashField, long value = 1, CommandFlags flags = CommandFlags.None)
        {
            long returnValue;
            var conn = GetConnection(SeparateRedisKey(key).id);
            {
                var db = conn.GetDatabase();
                returnValue = db.HashDecrement(NameOf(key), hashField, value, flags);
            }
            return returnValue;
        }

        public double HashDecrement(RedisKey key, RedisValue hashField, double value, CommandFlags flags = CommandFlags.None)
        {
            double returnValue;
            var conn = GetConnection(SeparateRedisKey(key).id);
            {
                var db = conn.GetDatabase();
                returnValue = db.HashDecrement(NameOf(key), hashField, value, flags);
            }
            return returnValue;
        }

        public Task<long> HashDecrementAsync(RedisKey key, RedisValue hashField, long value = 1, CommandFlags flags = CommandFlags.None)
        {
            Task<long> returnValue;
            var conn = GetConnection(SeparateRedisKey(key).id);
            {
                var db = conn.GetDatabase();
                returnValue = db.HashDecrementAsync(NameOf(key), hashField, value, flags);
            }
            return returnValue;
        }

        public Task<double> HashDecrementAsync(RedisKey key, RedisValue hashField, double value, CommandFlags flags = CommandFlags.None)
        {
            Task<double> returnValue;
            var conn = GetConnection(SeparateRedisKey(key).id);
            {
                var db = conn.GetDatabase();
                returnValue = db.HashDecrementAsync(NameOf(key), hashField, value, flags);
            }
            return returnValue;
        }

        public bool HashDelete(RedisKey key, RedisValue hashField, CommandFlags flags = CommandFlags.None)
        {
            bool returnValue;
            var conn = GetConnection(SeparateRedisKey(key).id);
            {
                var db = conn.GetDatabase();
                returnValue = db.HashDelete(NameOf(key), hashField, flags);
            }
            return returnValue;
        }

        public long HashDelete(RedisKey key, RedisValue[] hashFields, CommandFlags flags = CommandFlags.None)
        {
            long returnValue;
            var conn = GetConnection(SeparateRedisKey(key).id);
            {
                var db = conn.GetDatabase();
                returnValue = db.HashDelete(NameOf(key), hashFields, flags);
            }
            return returnValue;
        }

        public Task<bool> HashDeleteAsync(RedisKey key, RedisValue hashField, CommandFlags flags = CommandFlags.None)
        {
            Task<bool> returnValue;
            var conn = GetConnection(SeparateRedisKey(key).id);
            {
                var db = conn.GetDatabase();
                returnValue = db.HashDeleteAsync(NameOf(key), hashField, flags);
            }
            return returnValue;
        }

        public Task<long> HashDeleteAsync(RedisKey key, RedisValue[] hashFields, CommandFlags flags = CommandFlags.None)
        {
            Task<long> returnValue;
            var conn = GetConnection(SeparateRedisKey(key).id);
            {
                var db = conn.GetDatabase();
                returnValue = db.HashDeleteAsync(NameOf(key), hashFields, flags);
            }
            return returnValue;
        }

        public bool HashExists(RedisKey key, RedisValue hashField, CommandFlags flags = CommandFlags.None)
        {
            bool returnValue;
            var conn = GetConnection(SeparateRedisKey(key).id);
            {
                var db = conn.GetDatabase();
                returnValue = db.HashExists(NameOf(key), hashField, flags);
            }
            return returnValue;
        }

        public Task<bool> HashExistsAsync(RedisKey key, RedisValue hashField, CommandFlags flags = CommandFlags.None)
        {
            Task<bool> returnValue;
            var conn = GetConnection(SeparateRedisKey(key).id);
            {
                var db = conn.GetDatabase();
                returnValue = db.HashExistsAsync(NameOf(key), hashField, flags);
            }
            return returnValue;
        }

        public RedisValue HashGet(RedisKey key, RedisValue hashField, CommandFlags flags = CommandFlags.None)
        {
            RedisValue returnValue;
            var conn = GetConnection(SeparateRedisKey(key).id);
            {
                var db = conn.GetDatabase();
                returnValue = db.HashGet(NameOf(key), hashField, flags);
            }
            return returnValue;
        }

        public RedisValue[] HashGet(RedisKey key, RedisValue[] hashFields, CommandFlags flags = CommandFlags.None)
        {
            RedisValue[] returnValue;
            var conn = GetConnection(SeparateRedisKey(key).id);
            {
                var db = conn.GetDatabase();
                returnValue = db.HashGet(NameOf(key), hashFields, flags);
            }
            return returnValue;
        }

        public HashEntry[] HashGetAll(RedisKey key, CommandFlags flags = CommandFlags.None)
        {
            HashEntry[] returnValue;
            var conn = GetConnection(SeparateRedisKey(key).id);
            {
                var db = conn.GetDatabase();
                returnValue = db.HashGetAll(NameOf(key), flags);
            }
            return returnValue;
        }

        public Task<HashEntry[]> HashGetAllAsync(RedisKey key, CommandFlags flags = CommandFlags.None)
        {
            Task<HashEntry[]> returnValue;
            var conn = GetConnection(SeparateRedisKey(key).id);
            {
                var db = conn.GetDatabase();
                returnValue = db.HashGetAllAsync(NameOf(key), flags);
            }
            return returnValue;
        }

        public Task<RedisValue> HashGetAsync(RedisKey key, RedisValue hashField, CommandFlags flags = CommandFlags.None)
        {
            Task<RedisValue> returnValue;
            var conn = GetConnection(SeparateRedisKey(key).id);
            {
                var db = conn.GetDatabase();
                returnValue = db.HashGetAsync(NameOf(key), hashField, flags);
            }
            return returnValue;
        }

        public Task<RedisValue> HashGetAsync_Test(RedisKey key, RedisValue hashField, CommandFlags flags = CommandFlags.None)
        {
            Task<RedisValue> returnValue;
            var conn = GetConnection(SeparateRedisKey(key).id);
            {
                var db = conn.GetDatabase();
                returnValue = db.HashGetAsync(key, hashField, flags);
            }
            return returnValue;
        }

        public Task<RedisValue[]> HashGetAsync(RedisKey key, RedisValue[] hashFields, CommandFlags flags = CommandFlags.None)
        {
            Task<RedisValue[]> returnValue;
            var conn = GetConnection(SeparateRedisKey(key).id);
            {
                var db = conn.GetDatabase();
                returnValue = db.HashGetAsync(NameOf(key), hashFields, flags);
            }
            return returnValue;
        }

        public long HashIncrement(RedisKey key, RedisValue hashField, long value = 1, CommandFlags flags = CommandFlags.None)
        {
            long returnValue;
            var conn = GetConnection(SeparateRedisKey(key).id);
            {
                var db = conn.GetDatabase();
                returnValue = db.HashIncrement(NameOf(key), hashField, value, flags);
            }
            return returnValue;
        }

        public double HashIncrement(RedisKey key, RedisValue hashField, double value, CommandFlags flags = CommandFlags.None)
        {
            double returnValue;
            var conn = GetConnection(SeparateRedisKey(key).id);
            {
                var db = conn.GetDatabase();
                returnValue = db.HashIncrement(NameOf(key), hashField, value, flags);
            }
            return returnValue;
        }

        public Task<long> HashIncrementAsync(RedisKey key, RedisValue hashField, long value = 1, CommandFlags flags = CommandFlags.None)
        {
            Task<long> returnValue;
            var conn = GetConnection(SeparateRedisKey(key).id);
            {
                var db = conn.GetDatabase();
                returnValue = db.HashIncrementAsync(NameOf(key), hashField, value, flags);
            }
            return returnValue;
        }

        public Task<double> HashIncrementAsync(RedisKey key, RedisValue hashField, double value, CommandFlags flags = CommandFlags.None)
        {
            Task<double> returnValue;
            var conn = GetConnection(SeparateRedisKey(key).id);
            {
                var db = conn.GetDatabase();
                returnValue = db.HashIncrementAsync(NameOf(key), hashField, value, flags);
            }
            return returnValue;
        }

        public RedisValue[] HashKeys(RedisKey key, CommandFlags flags = CommandFlags.None)
        {
            RedisValue[] returnValue;
            var conn = GetConnection(SeparateRedisKey(key).id);
            {
                var db = conn.GetDatabase();
                returnValue = db.HashKeys(NameOf(key), flags);
            }
            return returnValue;
        }

        public Task<RedisValue[]> HashKeysAsync(RedisKey key, CommandFlags flags = CommandFlags.None)
        {
            Task<RedisValue[]> returnValue;
            var conn = GetConnection(SeparateRedisKey(key).id);
            {
                var db = conn.GetDatabase();
                returnValue = db.HashKeysAsync(NameOf(key), flags);
            }
            return returnValue;
        }

        public long HashLength(RedisKey key, CommandFlags flags = CommandFlags.None)
        {
            long returnValue;
            var conn = GetConnection(SeparateRedisKey(key).id);
            {
                var db = conn.GetDatabase();
                returnValue = db.HashLength(NameOf(key), flags);
            }
            return returnValue;
        }

        public Task<long> HashLengthAsync(RedisKey key, CommandFlags flags = CommandFlags.None)
        {
            Task<long> returnValue;
            var conn = GetConnection(SeparateRedisKey(key).id);
            {
                var db = conn.GetDatabase();
                returnValue = db.HashLengthAsync(NameOf(key), flags);
            }
            return returnValue;
        }

        public IEnumerable<HashEntry> HashScan(RedisKey key, RedisValue pattern, int pageSize, CommandFlags flags)
        {
            IEnumerable<HashEntry> returnValue;
            var conn = GetConnection(SeparateRedisKey(key).id);
            {
                var db = conn.GetDatabase();
                returnValue = db.HashScan(NameOf(key), pattern, pageSize, flags);
            }
            return returnValue;
        }

        public IEnumerable<HashEntry> HashScan(RedisKey key, RedisValue pattern = default(RedisValue), int pageSize = 10, long cursor = 0, int pageOffset = 0, CommandFlags flags = CommandFlags.None)
        {
            IEnumerable<HashEntry> returnValue;
            var conn = GetConnection(SeparateRedisKey(key).id);
            {
                var db = conn.GetDatabase();
                returnValue = db.HashScan(NameOf(key), pattern, pageSize, cursor, pageOffset, flags);
            }
            return returnValue;
        }

        public void HashSet(RedisKey key, HashEntry[] hashFields, CommandFlags flags = CommandFlags.None)
        {
            var conn = GetConnection();
            {
                var db = conn.GetDatabase();
                db.HashSet(NameOf(key), hashFields, flags);
            }
        }

        public bool HashSet(RedisKey key, RedisValue hashField, RedisValue value, When when = When.Always, CommandFlags flags = CommandFlags.None)
        {
            bool returnValue;
            var conn = GetConnection();
            {
                var db = conn.GetDatabase();
                returnValue = db.HashSet(NameOf(key), hashField, value, when, flags);
            }
            return returnValue;
        }

        public Task HashSetAsync(RedisKey key, HashEntry[] hashFields, CommandFlags flags = CommandFlags.None)
        {
            Task returnValue;
            var conn = GetConnection();
            {
                var db = conn.GetDatabase();
                returnValue = db.HashSetAsync(NameOf(key), hashFields, flags);
            }
            return returnValue;
        }

        public Task<bool> HashSetAsync(RedisKey key, RedisValue hashField, RedisValue value, When when = When.Always, CommandFlags flags = CommandFlags.None)
        {
            try
            {
                Task<bool> returnValue;
                var conn = GetConnection();
                {
                    var db = conn.GetDatabase();
                    returnValue = Task.FromResult<bool>(db.HashSet(NameOf(key), hashField, value, when, flags));
                }
                return returnValue;
            }
            catch (Exception)
            {
                return Task.FromResult<bool>(false);
            }

        }

        public RedisValue[] HashValues(RedisKey key, CommandFlags flags = CommandFlags.None)
        {
            RedisValue[] returnValue;
            var conn = GetConnection(SeparateRedisKey(key).id);
            {
                var db = conn.GetDatabase();
                returnValue = db.HashValues(NameOf(key), flags);
            }
            return returnValue;
        }

        public Task<RedisValue[]> HashValuesAsync(RedisKey key, CommandFlags flags = CommandFlags.None)
        {
            Task<RedisValue[]> returnValue;
            var conn = GetConnection(SeparateRedisKey(key).id);
            {
                var db = conn.GetDatabase();
                returnValue = db.HashValuesAsync(NameOf(key), flags);
            }
            return returnValue;
        }

        public bool HyperLogLogAdd(RedisKey key, RedisValue value, CommandFlags flags = CommandFlags.None)
        {
            bool returnValue;
            var conn = GetConnection();
            {
                var db = conn.GetDatabase();
                returnValue = db.HyperLogLogAdd(NameOf(key), value, flags);
            }
            return returnValue;
        }

        public bool HyperLogLogAdd(RedisKey key, RedisValue[] values, CommandFlags flags = CommandFlags.None)
        {
            bool returnValue;
            var conn = GetConnection();
            {
                var db = conn.GetDatabase();
                returnValue = db.HyperLogLogAdd(NameOf(key), values, flags);
            }
            return returnValue;
        }

        public Task<bool> HyperLogLogAddAsync(RedisKey key, RedisValue value, CommandFlags flags = CommandFlags.None)
        {
            Task<bool> returnValue;
            var conn = GetConnection();
            {
                var db = conn.GetDatabase();
                returnValue = db.HyperLogLogAddAsync(NameOf(key), value, flags);
            }
            return returnValue;
        }

        public Task<bool> HyperLogLogAddAsync(RedisKey key, RedisValue[] values, CommandFlags flags = CommandFlags.None)
        {
            Task<bool> returnValue;
            var conn = GetConnection();
            {
                var db = conn.GetDatabase();
                returnValue = db.HyperLogLogAddAsync(NameOf(key), values, flags);
            }
            return returnValue;
        }

        public long HyperLogLogLength(RedisKey key, CommandFlags flags = CommandFlags.None)
        {
            long returnValue;
            var conn = GetConnection();
            {
                var db = conn.GetDatabase();
                returnValue = db.HyperLogLogLength(NameOf(key), flags);
            }
            return returnValue;
        }

        public long HyperLogLogLength(RedisKey[] keys, CommandFlags flags = CommandFlags.None)
        {
            long returnValue;
            if (keys != null)
            {
                for (var i = 0; i < keys.Length; ++i)
                {
                    keys[i] = NameOf(keys[i]);
                }
            }
            var conn = GetConnection();
            {
                var db = conn.GetDatabase();
                returnValue = db.HyperLogLogLength(keys, flags);
            }
            return returnValue;
        }

        public Task<long> HyperLogLogLengthAsync(RedisKey key, CommandFlags flags = CommandFlags.None)
        {
            Task<long> returnValue;
            var conn = GetConnection();
            {
                var db = conn.GetDatabase();
                returnValue = db.HyperLogLogLengthAsync(NameOf(key), flags);
            }
            return returnValue;
        }

        public Task<long> HyperLogLogLengthAsync(RedisKey[] keys, CommandFlags flags = CommandFlags.None)
        {
            Task<long> returnValue;
            if (keys != null)
            {
                for (var i = 0; i < keys.Length; ++i)
                {
                    keys[i] = NameOf(keys[i]);
                }
            }
            var conn = GetConnection();
            {
                var db = conn.GetDatabase();
                returnValue = db.HyperLogLogLengthAsync(keys, flags);
            }
            return returnValue;
        }

        public void HyperLogLogMerge(RedisKey destination, RedisKey first, RedisKey second, CommandFlags flags = CommandFlags.None)
        {
            var conn = GetConnection();
            {
                var db = conn.GetDatabase();
                db.HyperLogLogMerge(NameOf(destination), NameOf(first), NameOf(second), flags);
            }
        }

        public void HyperLogLogMerge(RedisKey destination, RedisKey[] sourceKeys, CommandFlags flags = CommandFlags.None)
        {
            if (sourceKeys != null)
            {
                for (var i = 0; i < sourceKeys.Length; ++i)
                {
                    sourceKeys[i] = NameOf(sourceKeys[i]);
                }
            }
            var conn = GetConnection();
            {
                var db = conn.GetDatabase();
                db.HyperLogLogMerge(NameOf(destination), sourceKeys, flags);
            }
        }

        public Task HyperLogLogMergeAsync(RedisKey destination, RedisKey first, RedisKey second, CommandFlags flags = CommandFlags.None)
        {
            Task returnValue;
            var conn = GetConnection();
            {
                var db = conn.GetDatabase();
                returnValue = db.HyperLogLogMergeAsync(NameOf(destination), NameOf(first), NameOf(second), flags);
            }
            return returnValue;
        }

        public Task HyperLogLogMergeAsync(RedisKey destination, RedisKey[] sourceKeys, CommandFlags flags = CommandFlags.None)
        {
            Task returnValue;
            if (sourceKeys != null)
            {
                for (var i = 0; i < sourceKeys.Length; ++i)
                {
                    sourceKeys[i] = NameOf(sourceKeys[i]);
                }
            }
            var conn = GetConnection();
            {
                var db = conn.GetDatabase();
                returnValue = db.HyperLogLogMergeAsync(NameOf(destination), sourceKeys, flags);
            }
            return returnValue;
        }

        public EndPoint IdentifyEndpoint(RedisKey key = default(RedisKey), CommandFlags flags = CommandFlags.None)
        {
            EndPoint returnValue;
            var conn = GetConnection(SeparateRedisKey(key).id);
            {
                var db = conn.GetDatabase();
                returnValue = db.IdentifyEndpoint(NameOf(key), flags);
            }
            return returnValue;
        }

        public Task<EndPoint> IdentifyEndpointAsync(RedisKey key = default(RedisKey), CommandFlags flags = CommandFlags.None)
        {
            Task<EndPoint> returnValue;
            var conn = GetConnection(SeparateRedisKey(key).id);
            {
                var db = conn.GetDatabase();
                returnValue = db.IdentifyEndpointAsync(NameOf(key), flags);
            }
            return returnValue;
        }

        public bool IsConnected(RedisKey key, CommandFlags flags = CommandFlags.None)
        {
            bool returnValue;
            var conn = GetConnection(SeparateRedisKey(key).id);
            {
                var db = conn.GetDatabase();
                returnValue = db.IsConnected(NameOf(key), flags);
            }
            return returnValue;
        }

        public bool KeyDelete(RedisKey key, CommandFlags flags = CommandFlags.None)
        {
            bool returnValue;
            var conn = GetConnection(SeparateRedisKey(key).id);
            {
                var db = conn.GetDatabase();
                returnValue = db.KeyDelete(NameOf(key), flags);
            }
            return returnValue;
        }

        public long KeyDelete(RedisKey[] keys, CommandFlags flags = CommandFlags.None)
        {
            long returnValue;
            if (keys != null)
            {
                for (var i = 0; i < keys.Length; ++i)
                {
                    keys[i] = NameOf(keys[i]);
                }
            }
            var conn = GetConnection();
            {
                var db = conn.GetDatabase();
                returnValue = db.KeyDelete(keys, flags);
            }
            return returnValue;
        }

        public Task<bool> KeyDeleteAsync(RedisKey key, CommandFlags flags = CommandFlags.None)
        {
            Task<bool> returnValue;
            var conn = GetConnection(SeparateRedisKey(key).id);
            {
                var db = conn.GetDatabase();
                returnValue = db.KeyDeleteAsync(NameOf(key), flags);
            }
            return returnValue;
        }

        public Task<long> KeyDeleteAsync(RedisKey[] keys, CommandFlags flags = CommandFlags.None)
        {
            Task<long> returnValue;
            if (keys != null)
            {
                for (var i = 0; i < keys.Length; ++i)
                {
                    keys[i] = NameOf(keys[i]);
                }
            }
            var conn = GetConnection();
            {
                var db = conn.GetDatabase();
                returnValue = db.KeyDeleteAsync(keys, flags);
            }
            return returnValue;
        }

        public byte[] KeyDump(RedisKey key, CommandFlags flags = CommandFlags.None)
        {
            byte[] returnValue;
            var conn = GetConnection(SeparateRedisKey(key).id);
            {
                var db = conn.GetDatabase();
                returnValue = db.KeyDump(NameOf(key), flags);
            }
            return returnValue;
        }

        public Task<byte[]> KeyDumpAsync(RedisKey key, CommandFlags flags = CommandFlags.None)
        {
            Task<byte[]> returnValue;
            var conn = GetConnection(SeparateRedisKey(key).id);
            {
                var db = conn.GetDatabase();
                returnValue = db.KeyDumpAsync(NameOf(key), flags);
            }
            return returnValue;
        }

        public bool KeyExists(RedisKey key, CommandFlags flags = CommandFlags.None)
        {
            bool returnValue;
            var conn = GetConnection();
            {
                var db = conn.GetDatabase();
                returnValue = db.KeyExists(NameOf(key), flags);
            }
            return returnValue;
        }

        public Task<bool> KeyExistsAsync(RedisKey key, CommandFlags flags = CommandFlags.None)
        {
            Task<bool> returnValue;
            var conn = GetConnection(SeparateRedisKey(key).id);
            {
                var db = conn.GetDatabase();
                returnValue = db.KeyExistsAsync(NameOf(key), flags);
            }
            return returnValue;
        }

        public bool KeyExpire(RedisKey key, TimeSpan? expiry, CommandFlags flags = CommandFlags.None)
        {
            bool returnValue;
            var conn = GetConnection(SeparateRedisKey(key).id);
            {
                var db = conn.GetDatabase();
                returnValue = db.KeyExpire(NameOf(key), expiry, flags);
            }
            return returnValue;
        }

        public bool KeyExpire(RedisKey key, DateTime? expiry, CommandFlags flags = CommandFlags.None)
        {
            bool returnValue;
            var conn = GetConnection(SeparateRedisKey(key).id);
            {
                var db = conn.GetDatabase();
                returnValue = db.KeyExpire(NameOf(key), expiry, flags);
            }
            return returnValue;
        }

        public Task<bool> KeyExpireAsync(RedisKey key, TimeSpan? expiry, CommandFlags flags = CommandFlags.None)
        {
            Task<bool> returnValue;
            var conn = GetConnection(SeparateRedisKey(key).id);
            {
                var db = conn.GetDatabase();
                returnValue = db.KeyExpireAsync(NameOf(key), expiry, flags);
            }
            return returnValue;
        }

        public Task<bool> KeyExpireAsync(RedisKey key, DateTime? expiry, CommandFlags flags = CommandFlags.None)
        {
            Task<bool> returnValue;
            var conn = GetConnection(SeparateRedisKey(key).id);
            {
                var db = conn.GetDatabase();
                returnValue = db.KeyExpireAsync(NameOf(key), expiry, flags);
            }
            return returnValue;
        }

        public void KeyMigrate(RedisKey key, EndPoint toServer, int toDatabase = 0, int timeoutMilliseconds = 0, MigrateOptions migrateOptions = MigrateOptions.None, CommandFlags flags = CommandFlags.None)
        {
            var conn = GetConnection(SeparateRedisKey(key).id);
            {
                var db = conn.GetDatabase();
                db.KeyMigrate(NameOf(key), toServer, toDatabase, timeoutMilliseconds, migrateOptions, flags);
            }
        }

        public Task KeyMigrateAsync(RedisKey key, EndPoint toServer, int toDatabase = 0, int timeoutMilliseconds = 0, MigrateOptions migrateOptions = MigrateOptions.None, CommandFlags flags = CommandFlags.None)
        {
            Task returnValue;
            var conn = GetConnection(SeparateRedisKey(key).id);
            {
                var db = conn.GetDatabase();
                returnValue = db.KeyMigrateAsync(NameOf(key), toServer, toDatabase, timeoutMilliseconds, migrateOptions, flags);
            }
            return returnValue;
        }

        public bool KeyMove(RedisKey key, int database, CommandFlags flags = CommandFlags.None)
        {
            bool returnValue;
            var conn = GetConnection(SeparateRedisKey(key).id);
            {
                var db = conn.GetDatabase();
                returnValue = db.KeyMove(NameOf(key), database, flags);
            }
            return returnValue;
        }

        public Task<bool> KeyMoveAsync(RedisKey key, int database, CommandFlags flags = CommandFlags.None)
        {
            Task<bool> returnValue;
            var conn = GetConnection(SeparateRedisKey(key).id);
            {
                var db = conn.GetDatabase();
                returnValue = db.KeyMoveAsync(NameOf(key), database, flags);
            }
            return returnValue;
        }

        public bool KeyPersist(RedisKey key, CommandFlags flags = CommandFlags.None)
        {
            bool returnValue;
            var conn = GetConnection(SeparateRedisKey(key).id);
            {
                var db = conn.GetDatabase();
                returnValue = db.KeyPersist(NameOf(key), flags);
            }
            return returnValue;
        }

        public Task<bool> KeyPersistAsync(RedisKey key, CommandFlags flags = CommandFlags.None)
        {
            Task<bool> returnValue;
            var conn = GetConnection(SeparateRedisKey(key).id);
            {
                var db = conn.GetDatabase();
                returnValue = db.KeyPersistAsync(NameOf(key), flags);
            }
            return returnValue;
        }

        public RedisKey KeyRandom(CommandFlags flags = CommandFlags.None)
        {
            RedisKey returnValue;
            var conn = GetConnection();
            {
                var db = conn.GetDatabase();
                returnValue = NameOf(db.KeyRandom(flags));
            }
            return returnValue;
        }

        public Task<RedisKey> KeyRandomAsync(CommandFlags flags = CommandFlags.None)
        {
            Task<RedisKey> returnValue;
            var conn = GetConnection();
            {
                var db = conn.GetDatabase();
                returnValue = db.KeyRandomAsync(flags);
            }
            return returnValue.ContinueWith(key => (RedisKey)NameOf(key.Result));
        }

        public bool KeyRename(RedisKey key, RedisKey newKey, When when = When.Always, CommandFlags flags = CommandFlags.None)
        {
            bool returnValue;
            var conn = GetConnection(SeparateRedisKey(key).id);
            {
                var db = conn.GetDatabase();
                returnValue = db.KeyRename(NameOf(key), NameOf(newKey), when, flags);
            }
            return returnValue;
        }

        public Task<bool> KeyRenameAsync(RedisKey key, RedisKey newKey, When when = When.Always, CommandFlags flags = CommandFlags.None)
        {
            Task<bool> returnValue;
            var conn = GetConnection(SeparateRedisKey(key).id);
            {
                var db = conn.GetDatabase();
                returnValue = db.KeyRenameAsync(NameOf(key), NameOf(newKey), when, flags);
            }
            return returnValue;
        }

        public void KeyRestore(RedisKey key, byte[] value, TimeSpan? expiry = null, CommandFlags flags = CommandFlags.None)
        {
            var conn = GetConnection(SeparateRedisKey(key).id);
            {
                var db = conn.GetDatabase();
                db.KeyRestore(NameOf(key), value, expiry, flags);
            }
        }

        public Task KeyRestoreAsync(RedisKey key, byte[] value, TimeSpan? expiry = null, CommandFlags flags = CommandFlags.None)
        {
            Task returnValue;
            var conn = GetConnection(SeparateRedisKey(key).id);
            {
                var db = conn.GetDatabase();
                returnValue = db.KeyRestoreAsync(NameOf(key), value, expiry, flags);
            }
            return returnValue;
        }

        public TimeSpan? KeyTimeToLive(RedisKey key, CommandFlags flags = CommandFlags.None)
        {
            TimeSpan? returnValue;
            var conn = GetConnection(SeparateRedisKey(key).id);
            {
                var db = conn.GetDatabase();
                returnValue = db.KeyTimeToLive(NameOf(key), flags);
            }
            return returnValue;
        }

        public Task<TimeSpan?> KeyTimeToLiveAsync(RedisKey key, CommandFlags flags = CommandFlags.None)
        {
            Task<TimeSpan?> returnValue;
            var conn = GetConnection(SeparateRedisKey(key).id);
            {
                var db = conn.GetDatabase();
                returnValue = db.KeyTimeToLiveAsync(NameOf(key), flags);
            }
            return returnValue;
        }

        public RedisType KeyType(RedisKey key, CommandFlags flags = CommandFlags.None)
        {
            RedisType returnValue;
            var conn = GetConnection(SeparateRedisKey(key).id);
            {
                var db = conn.GetDatabase();
                returnValue = db.KeyType(NameOf(key), flags);
            }
            return returnValue;
        }

        public Task<RedisType> KeyTypeAsync(RedisKey key, CommandFlags flags = CommandFlags.None)
        {
            Task<RedisType> returnValue;
            var conn = GetConnection(SeparateRedisKey(key).id);
            {
                var db = conn.GetDatabase();
                returnValue = db.KeyTypeAsync(NameOf(key), flags);
            }
            return returnValue;
        }

        public RedisValue ListGetByIndex(RedisKey key, long index, CommandFlags flags = CommandFlags.None)
        {
            RedisValue returnValue;
            var conn = GetConnection(SeparateRedisKey(key).id);
            {
                var db = conn.GetDatabase();
                returnValue = db.ListGetByIndex(NameOf(key), index, flags);
            }
            return returnValue;
        }

        public Task<RedisValue> ListGetByIndexAsync(RedisKey key, long index, CommandFlags flags = CommandFlags.None)
        {
            Task<RedisValue> returnValue;
            var conn = GetConnection(SeparateRedisKey(key).id);
            {
                var db = conn.GetDatabase();
                returnValue = db.ListGetByIndexAsync(NameOf(key), index, flags);
            }
            return returnValue;
        }

        public long ListInsertAfter(RedisKey key, RedisValue pivot, RedisValue value, CommandFlags flags = CommandFlags.None)
        {
            long returnValue;
            var conn = GetConnection(SeparateRedisKey(key).id);
            {
                var db = conn.GetDatabase();
                returnValue = db.ListInsertAfter(NameOf(key), pivot, value, flags);
            }
            return returnValue;
        }

        public Task<long> ListInsertAfterAsync(RedisKey key, RedisValue pivot, RedisValue value, CommandFlags flags = CommandFlags.None)
        {
            Task<long> returnValue;
            var conn = GetConnection(SeparateRedisKey(key).id);
            {
                var db = conn.GetDatabase();
                returnValue = db.ListInsertAfterAsync(NameOf(key), pivot, value, flags);
            }
            return returnValue;
        }

        public long ListInsertBefore(RedisKey key, RedisValue pivot, RedisValue value, CommandFlags flags = CommandFlags.None)
        {
            long returnValue;
            var conn = GetConnection(SeparateRedisKey(key).id);
            {
                var db = conn.GetDatabase();
                returnValue = db.ListInsertBefore(NameOf(key), pivot, value, flags);
            }
            return returnValue;
        }

        public Task<long> ListInsertBeforeAsync(RedisKey key, RedisValue pivot, RedisValue value, CommandFlags flags = CommandFlags.None)
        {
            Task<long> returnValue;
            var conn = GetConnection(SeparateRedisKey(key).id);
            {
                var db = conn.GetDatabase();
                returnValue = db.ListInsertBeforeAsync(NameOf(key), pivot, value, flags);
            }
            return returnValue;
        }

        public RedisValue ListLeftPop(RedisKey key, CommandFlags flags = CommandFlags.None)
        {
            RedisValue returnValue;
            var conn = GetConnection(SeparateRedisKey(key).id);
            {
                var db = conn.GetDatabase();
                returnValue = db.ListLeftPop(NameOf(key), flags);
            }
            return returnValue;
        }

        public Task<RedisValue> ListLeftPopAsync(RedisKey key, CommandFlags flags = CommandFlags.None)
        {
            Task<RedisValue> returnValue;
            var conn = GetConnection(SeparateRedisKey(key).id);
            {
                var db = conn.GetDatabase();
                returnValue = db.ListLeftPopAsync(NameOf(key), flags);
            }
            return returnValue;
        }

        public long ListLeftPush(RedisKey key, RedisValue value, When when = When.Always, CommandFlags flags = CommandFlags.None)
        {
            long returnValue;
            var conn = GetConnection(SeparateRedisKey(key).id);
            {
                var db = conn.GetDatabase();
                returnValue = db.ListLeftPush(NameOf(key), value, when, flags);
            }
            return returnValue;
        }

        public long ListLeftPush(RedisKey key, RedisValue[] values, CommandFlags flags = CommandFlags.None)
        {
            long returnValue;
            var conn = GetConnection(SeparateRedisKey(key).id);
            {
                var db = conn.GetDatabase();
                returnValue = db.ListLeftPush(NameOf(key), values, flags);
            }
            return returnValue;
        }

        public Task<long> ListLeftPushAsync(RedisKey key, RedisValue value, When when = When.Always, CommandFlags flags = CommandFlags.None)
        {
            Task<long> returnValue;
            var conn = GetConnection(SeparateRedisKey(key).id);
            {
                var db = conn.GetDatabase();
                returnValue = db.ListLeftPushAsync(NameOf(key), value, when, flags);
            }
            return returnValue;
        }

        public Task<long> ListLeftPushAsync(RedisKey key, RedisValue[] values, CommandFlags flags = CommandFlags.None)
        {
            Task<long> returnValue;
            var conn = GetConnection(SeparateRedisKey(key).id);
            {
                var db = conn.GetDatabase();
                returnValue = db.ListLeftPushAsync(NameOf(key), values, flags);
            }
            return returnValue;
        }

        public long ListLength(RedisKey key, CommandFlags flags = CommandFlags.None)
        {
            long returnValue;
            var conn = GetConnection(SeparateRedisKey(key).id);
            {
                var db = conn.GetDatabase();
                returnValue = db.ListLength(NameOf(key), flags);
            }
            return returnValue;
        }

        public Task<long> ListLengthAsync(RedisKey key, CommandFlags flags = CommandFlags.None)
        {
            Task<long> returnValue;
            var conn = GetConnection(SeparateRedisKey(key).id);
            {
                var db = conn.GetDatabase();
                returnValue = db.ListLengthAsync(NameOf(key), flags);
            }
            return returnValue;
        }

        public RedisValue[] ListRange(RedisKey key, long start = 0, long stop = -1, CommandFlags flags = CommandFlags.None)
        {
            RedisValue[] returnValue;
            var conn = GetConnection(SeparateRedisKey(key).id);
            {
                var db = conn.GetDatabase();
                returnValue = db.ListRange(NameOf(key), start, stop, flags);
            }
            return returnValue;
        }

        public Task<RedisValue[]> ListRangeAsync(RedisKey key, long start = 0, long stop = -1, CommandFlags flags = CommandFlags.None)
        {
            Task<RedisValue[]> returnValue;
            var conn = GetConnection(SeparateRedisKey(key).id);
            {
                var db = conn.GetDatabase();
                returnValue = db.ListRangeAsync(NameOf(key), start, stop, flags);
            }
            return returnValue;
        }

        public long ListRemove(RedisKey key, RedisValue value, long count = 0, CommandFlags flags = CommandFlags.None)
        {
            long returnValue;
            var conn = GetConnection(SeparateRedisKey(key).id);
            {
                var db = conn.GetDatabase();
                returnValue = db.ListRemove(NameOf(key), value, count, flags);
            }
            return returnValue;
        }

        public Task<long> ListRemoveAsync(RedisKey key, RedisValue value, long count = 0, CommandFlags flags = CommandFlags.None)
        {
            Task<long> returnValue;
            var conn = GetConnection(SeparateRedisKey(key).id);
            {
                var db = conn.GetDatabase();
                returnValue = db.ListRemoveAsync(NameOf(key), value, count, flags);
            }
            return returnValue;
        }

        public RedisValue ListRightPop(RedisKey key, CommandFlags flags = CommandFlags.None)
        {
            RedisValue returnValue;
            var conn = GetConnection(SeparateRedisKey(key).id);
            {
                var db = conn.GetDatabase();
                returnValue = db.ListRightPop(NameOf(key), flags);
            }
            return returnValue;
        }

        public Task<RedisValue> ListRightPopAsync(RedisKey key, CommandFlags flags = CommandFlags.None)
        {
            Task<RedisValue> returnValue;
            var conn = GetConnection(SeparateRedisKey(key).id);
            {
                var db = conn.GetDatabase();
                returnValue = db.ListRightPopAsync(NameOf(key), flags);
            }
            return returnValue;
        }

        public RedisValue ListRightPopLeftPush(RedisKey source, RedisKey destination, CommandFlags flags = CommandFlags.None)
        {
            RedisValue returnValue;
            var conn = GetConnection(SeparateRedisKey(source).id);
            {
                var db = conn.GetDatabase();
                returnValue = db.ListRightPopLeftPush(NameOf(source), NameOf(destination), flags);
            }
            return returnValue;
        }

        public Task<RedisValue> ListRightPopLeftPushAsync(RedisKey source, RedisKey destination, CommandFlags flags = CommandFlags.None)
        {
            Task<RedisValue> returnValue;
            var conn = GetConnection(SeparateRedisKey(source).id);
            {
                var db = conn.GetDatabase();
                returnValue = db.ListRightPopLeftPushAsync(NameOf(source), NameOf(destination), flags);
            }
            return returnValue;
        }

        public long ListRightPush(RedisKey key, RedisValue value, When when = When.Always, CommandFlags flags = CommandFlags.None)
        {
            long returnValue;
            var conn = GetConnection(SeparateRedisKey(key).id);
            {
                var db = conn.GetDatabase();
                returnValue = db.ListRightPush(NameOf(key), value, when, flags);
            }
            return returnValue;
        }

        public long ListRightPush(RedisKey key, RedisValue[] values, CommandFlags flags = CommandFlags.None)
        {
            long returnValue;
            var conn = GetConnection(SeparateRedisKey(key).id);
            {
                var db = conn.GetDatabase();
                returnValue = db.ListRightPush(NameOf(key), values, flags);
            }
            return returnValue;
        }

        public Task<long> ListRightPushAsync(RedisKey key, RedisValue value, When when = When.Always, CommandFlags flags = CommandFlags.None)
        {
            Task<long> returnValue;
            var conn = GetConnection(SeparateRedisKey(key).id);
            {
                var db = conn.GetDatabase();
                returnValue = db.ListRightPushAsync(NameOf(key), value, when, flags);
            }
            return returnValue;
        }

        public Task<long> ListRightPushAsync(RedisKey key, RedisValue[] values, CommandFlags flags = CommandFlags.None)
        {
            Task<long> returnValue;
            var conn = GetConnection(SeparateRedisKey(key).id);
            {
                var db = conn.GetDatabase();
                returnValue = db.ListRightPushAsync(NameOf(key), values, flags);
            }
            return returnValue;
        }

        public void ListSetByIndex(RedisKey key, long index, RedisValue value, CommandFlags flags = CommandFlags.None)
        {
            var conn = GetConnection();
            {
                var db = conn.GetDatabase();
                db.ListSetByIndex(NameOf(key), index, value, flags);
            }
        }

        public Task ListSetByIndexAsync(RedisKey key, long index, RedisValue value, CommandFlags flags = CommandFlags.None)
        {
            Task returnValue;
            var conn = GetConnection();
            {
                var db = conn.GetDatabase();
                returnValue = db.ListSetByIndexAsync(NameOf(key), index, value, flags);
            }
            return returnValue;
        }

        public void ListTrim(RedisKey key, long start, long stop, CommandFlags flags = CommandFlags.None)
        {
            var conn = GetConnection();
            {
                var db = conn.GetDatabase();
                db.ListTrim(NameOf(key), start, stop, flags);
            }
        }

        public Task ListTrimAsync(RedisKey key, long start, long stop, CommandFlags flags = CommandFlags.None)
        {
            Task returnValue;
            var conn = GetConnection();
            {
                var db = conn.GetDatabase();
                returnValue = db.ListTrimAsync(NameOf(key), start, stop, flags);
            }
            return returnValue;
        }

        public bool LockExtend(RedisKey key, RedisValue value, TimeSpan expiry, CommandFlags flags = CommandFlags.None)
        {
            bool returnValue;
            var conn = GetConnection(SeparateRedisKey(key).id);
            {
                var db = conn.GetDatabase();
                returnValue = db.LockExtend(NameOf(key), value, expiry, flags);
            }
            return returnValue;
        }

        public Task<bool> LockExtendAsync(RedisKey key, RedisValue value, TimeSpan expiry, CommandFlags flags = CommandFlags.None)
        {
            Task<bool> returnValue;
            var conn = GetConnection(SeparateRedisKey(key).id);
            {
                var db = conn.GetDatabase();
                returnValue = db.LockExtendAsync(NameOf(key), value, expiry, flags);
            }
            return returnValue;
        }

        public RedisValue LockQuery(RedisKey key, CommandFlags flags = CommandFlags.None)
        {
            RedisValue returnValue;
            var conn = GetConnection(SeparateRedisKey(key).id);
            {
                var db = conn.GetDatabase();
                returnValue = db.LockQuery(NameOf(key), flags);
            }
            return returnValue;
        }

        public Task<RedisValue> LockQueryAsync(RedisKey key, CommandFlags flags = CommandFlags.None)
        {
            Task<RedisValue> returnValue;
            var conn = GetConnection(SeparateRedisKey(key).id);
            {
                var db = conn.GetDatabase();
                returnValue = db.LockQueryAsync(NameOf(key), flags);
            }
            return returnValue;
        }

        public bool LockRelease(RedisKey key, RedisValue value, CommandFlags flags = CommandFlags.None)
        {
            bool returnValue;
            var conn = GetConnection(SeparateRedisKey(key).id);
            {
                var db = conn.GetDatabase();
                returnValue = db.LockRelease(NameOf(key), value, flags);
            }
            return returnValue;
        }

        public Task<bool> LockReleaseAsync(RedisKey key, RedisValue value, CommandFlags flags = CommandFlags.None)
        {
            Task<bool> returnValue;
            var conn = GetConnection(SeparateRedisKey(key).id);
            {
                var db = conn.GetDatabase();
                returnValue = db.LockReleaseAsync(NameOf(key), value, flags);
            }
            return returnValue;
        }

        public bool LockTake(RedisKey key, RedisValue value, TimeSpan expiry, CommandFlags flags = CommandFlags.None)
        {
            bool returnValue;
            var conn = GetConnection(SeparateRedisKey(key).id);
            {
                var db = conn.GetDatabase();
                returnValue = db.LockTake(NameOf(key), value, expiry, flags);
            }
            return returnValue;
        }

        public Task<bool> LockTakeAsync(RedisKey key, RedisValue value, TimeSpan expiry, CommandFlags flags = CommandFlags.None)
        {
            Task<bool> returnValue;
            var conn = GetConnection(SeparateRedisKey(key).id);
            {
                var db = conn.GetDatabase();
                returnValue = db.LockTakeAsync(NameOf(key), value, expiry, flags);
            }
            return returnValue;
        }

        public TimeSpan Ping(CommandFlags flags = CommandFlags.None)
        {
            TimeSpan returnValue;
            var conn = GetConnection();
            {
                var db = conn.GetDatabase();
                returnValue = db.Ping(flags);
            }
            return returnValue;
        }

        public Task<TimeSpan> PingAsync(CommandFlags flags = CommandFlags.None)
        {
            Task<TimeSpan> returnValue;
            var conn = GetConnection();
            {
                var db = conn.GetDatabase();
                returnValue = db.PingAsync(flags);
            }
            return returnValue;
        }

        public long Publish(RedisChannel channel, RedisValue message, CommandFlags flags = CommandFlags.None)
        {
            long returnValue;
            var conn = GetConnection();
            {
                var db = conn.GetDatabase();
                returnValue = db.Publish(channel, message, flags);
            }
            return returnValue;
        }

        public Task<long> PublishAsync(RedisChannel channel, RedisValue message, CommandFlags flags = CommandFlags.None)
        {
            Task<long> returnValue;
            var conn = GetConnection();
            {
                var db = conn.GetDatabase();
                returnValue = db.PublishAsync(channel, message, flags);
            }
            return returnValue;
        }

        public RedisResult ScriptEvaluate(string script, RedisKey[] keys = null, RedisValue[] values = null, CommandFlags flags = CommandFlags.None)
        {
            RedisResult returnValue;
            if (keys != null)
            {
                for (var i = 0; i < keys.Length; ++i)
                {
                    keys[i] = NameOf(keys[i]);
                }
            }
            var conn = GetConnection();
            {
                var db = conn.GetDatabase();
                returnValue = db.ScriptEvaluate(script, keys, values, flags);
            }
            return returnValue;
        }

        public RedisResult ScriptEvaluate(byte[] hash, RedisKey[] keys = null, RedisValue[] values = null, CommandFlags flags = CommandFlags.None)
        {
            RedisResult returnValue;
            if (keys != null)
            {
                for (var i = 0; i < keys.Length; ++i)
                {
                    keys[i] = NameOf(keys[i]);
                }
            }
            var conn = GetConnection();
            {
                var db = conn.GetDatabase();
                returnValue = db.ScriptEvaluate(hash, keys, values, flags);
            }
            return returnValue;
        }

        public RedisResult ScriptEvaluate(LuaScript script, object parameters = null, CommandFlags flags = CommandFlags.None)
        {
            RedisResult returnValue;
            var conn = GetConnection();
            {
                var db = conn.GetDatabase();
                returnValue = db.ScriptEvaluate(script, parameters, flags);
            }
            return returnValue;
        }

        public RedisResult ScriptEvaluate(LoadedLuaScript script, object parameters = null, CommandFlags flags = CommandFlags.None)
        {
            RedisResult returnValue;
            var conn = GetConnection();
            {
                var db = conn.GetDatabase();
                returnValue = db.ScriptEvaluate(script, parameters, flags);
            }
            return returnValue;
        }

        public Task<RedisResult> ScriptEvaluateAsync(string script, RedisKey[] keys = null, RedisValue[] values = null, CommandFlags flags = CommandFlags.None)
        {
            Task<RedisResult> returnValue;
            if (keys != null)
            {
                for (var i = 0; i < keys.Length; ++i)
                {
                    keys[i] = NameOf(keys[i]);
                }
            }
            var conn = GetConnection();
            {
                var db = conn.GetDatabase();
                returnValue = db.ScriptEvaluateAsync(script, keys, values, flags);
            }
            return returnValue;
        }

        public Task<RedisResult> ScriptEvaluateAsync(byte[] hash, RedisKey[] keys = null, RedisValue[] values = null, CommandFlags flags = CommandFlags.None)
        {
            Task<RedisResult> returnValue;
            if (keys != null)
            {
                for (var i = 0; i < keys.Length; ++i)
                {
                    keys[i] = NameOf(keys[i]);
                }
            }
            var conn = GetConnection();
            {
                var db = conn.GetDatabase();
                returnValue = db.ScriptEvaluateAsync(hash, keys, values, flags);
            }
            return returnValue;
        }

        public Task<RedisResult> ScriptEvaluateAsync(LuaScript script, object parameters = null, CommandFlags flags = CommandFlags.None)
        {
            Task<RedisResult> returnValue;
            var conn = GetConnection();
            {
                var db = conn.GetDatabase();
                returnValue = db.ScriptEvaluateAsync(script, parameters, flags);
            }
            return returnValue;
        }

        public Task<RedisResult> ScriptEvaluateAsync(LoadedLuaScript script, object parameters = null, CommandFlags flags = CommandFlags.None)
        {
            Task<RedisResult> returnValue;
            var conn = GetConnection();
            {
                var db = conn.GetDatabase();
                returnValue = db.ScriptEvaluateAsync(script, parameters, flags);
            }
            return returnValue;
        }

        public bool SetAdd(RedisKey key, RedisValue value, CommandFlags flags = CommandFlags.None)
        {
            bool returnValue;
            var conn = GetConnection();
            {
                var db = conn.GetDatabase();
                returnValue = db.SetAdd(NameOf(key), value, flags);
            }
            return returnValue;
        }

        public long SetAdd(RedisKey key, RedisValue[] values, CommandFlags flags = CommandFlags.None)
        {
            long returnValue;
            var conn = GetConnection();
            {
                var db = conn.GetDatabase();
                returnValue = db.SetAdd(NameOf(key), values, flags);
            }
            return returnValue;
        }

        public Task<bool> SetAddAsync(RedisKey key, RedisValue value, CommandFlags flags = CommandFlags.None)
        {
            Task<bool> returnValue;
            var conn = GetConnection();
            {
                var db = conn.GetDatabase();
                returnValue = db.SetAddAsync(NameOf(key), value, flags);
            }
            return returnValue;
        }

        public Task<long> SetAddAsync(RedisKey key, RedisValue[] values, CommandFlags flags = CommandFlags.None)
        {
            Task<long> returnValue;
            var conn = GetConnection();
            {
                var db = conn.GetDatabase();
                returnValue = db.SetAddAsync(NameOf(key), values, flags);
            }
            return returnValue;
        }

        public RedisValue[] SetCombine(SetOperation operation, RedisKey first, RedisKey second, CommandFlags flags = CommandFlags.None)
        {
            RedisValue[] returnValue;
            var conn = GetConnection();
            {
                var db = conn.GetDatabase();
                returnValue = db.SetCombine(operation, NameOf(first), NameOf(second), flags);
            }
            return returnValue;
        }

        public RedisValue[] SetCombine(SetOperation operation, RedisKey[] keys, CommandFlags flags = CommandFlags.None)
        {
            RedisValue[] returnValue;
            if (keys != null)
            {
                for (var i = 0; i < keys.Length; ++i)
                {
                    keys[i] = NameOf(keys[i]);
                }
            }
            var conn = GetConnection();
            {
                var db = conn.GetDatabase();
                returnValue = db.SetCombine(operation, keys, flags);
            }
            return returnValue;
        }

        public long SetCombineAndStore(SetOperation operation, RedisKey destination, RedisKey first, RedisKey second, CommandFlags flags = CommandFlags.None)
        {
            long returnValue;
            var conn = GetConnection();
            {
                var db = conn.GetDatabase();
                returnValue = db.SetCombineAndStore(operation, NameOf(destination), NameOf(first), NameOf(second), flags);
            }
            return returnValue;
        }

        public long SetCombineAndStore(SetOperation operation, RedisKey destination, RedisKey[] keys, CommandFlags flags = CommandFlags.None)
        {
            long returnValue;
            if (keys != null)
            {
                for (var i = 0; i < keys.Length; ++i)
                {
                    keys[i] = NameOf(keys[i]);
                }
            }
            var conn = GetConnection();
            {
                var db = conn.GetDatabase();
                returnValue = db.SetCombineAndStore(operation, NameOf(destination), keys, flags);
            }
            return returnValue;
        }

        public Task<long> SetCombineAndStoreAsync(SetOperation operation, RedisKey destination, RedisKey first, RedisKey second, CommandFlags flags = CommandFlags.None)
        {
            Task<long> returnValue;
            var conn = GetConnection();
            {
                var db = conn.GetDatabase();
                returnValue = db.SetCombineAndStoreAsync(operation, NameOf(destination), NameOf(first), NameOf(second), flags);
            }
            return returnValue;
        }

        public Task<long> SetCombineAndStoreAsync(SetOperation operation, RedisKey destination, RedisKey[] keys, CommandFlags flags = CommandFlags.None)
        {
            Task<long> returnValue;
            if (keys != null)
            {
                for (var i = 0; i < keys.Length; ++i)
                {
                    keys[i] = NameOf(keys[i]);
                }
            }
            var conn = GetConnection();
            {
                var db = conn.GetDatabase();
                returnValue = db.SetCombineAndStoreAsync(operation, NameOf(destination), keys, flags);
            }
            return returnValue;
        }

        public Task<RedisValue[]> SetCombineAsync(SetOperation operation, RedisKey first, RedisKey second, CommandFlags flags = CommandFlags.None)
        {
            Task<RedisValue[]> returnValue;
            var conn = GetConnection();
            {
                var db = conn.GetDatabase();
                returnValue = db.SetCombineAsync(operation, NameOf(first), NameOf(second), flags);
            }
            return returnValue;
        }

        public Task<RedisValue[]> SetCombineAsync(SetOperation operation, RedisKey[] keys, CommandFlags flags = CommandFlags.None)
        {
            Task<RedisValue[]> returnValue;
            if (keys != null)
            {
                for (var i = 0; i < keys.Length; ++i)
                {
                    keys[i] = NameOf(keys[i]);
                }
            }
            var conn = GetConnection();
            {
                var db = conn.GetDatabase();
                returnValue = db.SetCombineAsync(operation, keys, flags);
            }
            return returnValue;
        }

        public bool SetContains(RedisKey key, RedisValue value, CommandFlags flags = CommandFlags.None)
        {
            bool returnValue;
            var conn = GetConnection();
            {
                var db = conn.GetDatabase();
                returnValue = db.SetContains(NameOf(key), value, flags);
            }
            return returnValue;
        }

        public Task<bool> SetContainsAsync(RedisKey key, RedisValue value, CommandFlags flags = CommandFlags.None)
        {
            Task<bool> returnValue;
            var conn = GetConnection();
            {
                var db = conn.GetDatabase();
                returnValue = db.SetContainsAsync(NameOf(key), value, flags);
            }
            return returnValue;
        }

        public long SetLength(RedisKey key, CommandFlags flags = CommandFlags.None)
        {
            long returnValue;
            var conn = GetConnection();
            {
                var db = conn.GetDatabase();
                returnValue = db.SetLength(NameOf(key), flags);
            }
            return returnValue;
        }

        public Task<long> SetLengthAsync(RedisKey key, CommandFlags flags = CommandFlags.None)
        {
            Task<long> returnValue;
            var conn = GetConnection();
            {
                var db = conn.GetDatabase();
                returnValue = db.SetLengthAsync(NameOf(key), flags);
            }
            return returnValue;
        }

        public RedisValue[] SetMembers(RedisKey key, CommandFlags flags = CommandFlags.None)
        {
            RedisValue[] returnValue;
            var conn = GetConnection();
            {
                var db = conn.GetDatabase();
                returnValue = db.SetMembers(NameOf(key), flags);
            }
            return returnValue;
        }

        public Task<RedisValue[]> SetMembersAsync(RedisKey key, CommandFlags flags = CommandFlags.None)
        {
            Task<RedisValue[]> returnValue;
            var conn = GetConnection();
            {
                var db = conn.GetDatabase();
                returnValue = db.SetMembersAsync(NameOf(key), flags);
            }
            return returnValue;
        }

        public bool SetMove(RedisKey source, RedisKey destination, RedisValue value, CommandFlags flags = CommandFlags.None)
        {
            bool returnValue;
            var conn = GetConnection();
            {
                var db = conn.GetDatabase();
                returnValue = db.SetMove(NameOf(source), NameOf(destination), value, flags);
            }
            return returnValue;
        }

        public Task<bool> SetMoveAsync(RedisKey source, RedisKey destination, RedisValue value, CommandFlags flags = CommandFlags.None)
        {
            Task<bool> returnValue;
            var conn = GetConnection();
            {
                var db = conn.GetDatabase();
                returnValue = db.SetMoveAsync(NameOf(source), NameOf(destination), value, flags);
            }
            return returnValue;
        }

        public RedisValue SetPop(RedisKey key, CommandFlags flags = CommandFlags.None)
        {
            RedisValue returnValue;
            var conn = GetConnection();
            {
                var db = conn.GetDatabase();
                returnValue = db.SetPop(NameOf(key), flags);
            }
            return returnValue;
        }

        public Task<RedisValue> SetPopAsync(RedisKey key, CommandFlags flags = CommandFlags.None)
        {
            Task<RedisValue> returnValue;
            var conn = GetConnection();
            {
                var db = conn.GetDatabase();
                returnValue = db.SetPopAsync(NameOf(key), flags);
            }
            return returnValue;
        }

        public RedisValue SetRandomMember(RedisKey key, CommandFlags flags = CommandFlags.None)
        {
            RedisValue returnValue;
            var conn = GetConnection();
            {
                var db = conn.GetDatabase();
                returnValue = db.SetRandomMember(NameOf(key), flags);
            }
            return returnValue;
        }

        public Task<RedisValue> SetRandomMemberAsync(RedisKey key, CommandFlags flags = CommandFlags.None)
        {
            Task<RedisValue> returnValue;
            var conn = GetConnection();
            {
                var db = conn.GetDatabase();
                returnValue = db.SetRandomMemberAsync(NameOf(key), flags);
            }
            return returnValue;
        }

        public RedisValue[] SetRandomMembers(RedisKey key, long count, CommandFlags flags = CommandFlags.None)
        {
            RedisValue[] returnValue;
            var conn = GetConnection();
            {
                var db = conn.GetDatabase();
                returnValue = db.SetRandomMembers(NameOf(key), count, flags);
            }
            return returnValue;
        }

        public Task<RedisValue[]> SetRandomMembersAsync(RedisKey key, long count, CommandFlags flags = CommandFlags.None)
        {
            Task<RedisValue[]> returnValue;
            var conn = GetConnection();
            {
                var db = conn.GetDatabase();
                returnValue = db.SetRandomMembersAsync(NameOf(key), count, flags);
            }
            return returnValue;
        }

        public bool SetRemove(RedisKey key, RedisValue value, CommandFlags flags = CommandFlags.None)
        {
            bool returnValue;
            var conn = GetConnection();
            {
                var db = conn.GetDatabase();
                returnValue = db.SetRemove(NameOf(key), value, flags);
            }
            return returnValue;
        }

        public long SetRemove(RedisKey key, RedisValue[] values, CommandFlags flags = CommandFlags.None)
        {
            long returnValue;
            var conn = GetConnection();
            {
                var db = conn.GetDatabase();
                returnValue = db.SetRemove(NameOf(key), values, flags);
            }
            return returnValue;
        }

        public Task<bool> SetRemoveAsync(RedisKey key, RedisValue value, CommandFlags flags = CommandFlags.None)
        {
            Task<bool> returnValue;
            var conn = GetConnection();
            {
                var db = conn.GetDatabase();
                returnValue = db.SetRemoveAsync(NameOf(key), value, flags);
            }
            return returnValue;
        }

        public Task<long> SetRemoveAsync(RedisKey key, RedisValue[] values, CommandFlags flags = CommandFlags.None)
        {
            Task<long> returnValue;
            var conn = GetConnection();
            {
                var db = conn.GetDatabase();
                returnValue = db.SetRemoveAsync(NameOf(key), values, flags);
            }
            return returnValue;
        }

        public IEnumerable<RedisValue> SetScan(RedisKey key, RedisValue pattern, int pageSize, CommandFlags flags)
        {
            IEnumerable<RedisValue> returnValue;
            var conn = GetConnection();
            {
                var db = conn.GetDatabase();
                returnValue = db.SetScan(NameOf(key), pattern, pageSize, flags);
            }
            return returnValue;
        }

        public IEnumerable<RedisValue> SetScan(RedisKey key, RedisValue pattern = default(RedisValue), int pageSize = 10, long cursor = 0, int pageOffset = 0, CommandFlags flags = CommandFlags.None)
        {
            IEnumerable<RedisValue> returnValue;
            var conn = GetConnection();
            {
                var db = conn.GetDatabase();
                returnValue = db.SetScan(NameOf(key), pattern, pageSize, cursor, pageOffset, flags);
            }
            return returnValue;
        }

        public RedisValue[] Sort(RedisKey key, long skip = 0, long take = -1, Order order = Order.Ascending, SortType sortType = SortType.Numeric, RedisValue by = default(RedisValue), RedisValue[] get = null, CommandFlags flags = CommandFlags.None)
        {
            RedisValue[] returnValue;
            var conn = GetConnection();
            {
                var db = conn.GetDatabase();
                returnValue = db.Sort(NameOf(key), skip, take, order, sortType, by, get, flags);
            }
            return returnValue;
        }

        public long SortAndStore(RedisKey destination, RedisKey key, long skip = 0, long take = -1, Order order = Order.Ascending, SortType sortType = SortType.Numeric, RedisValue by = default(RedisValue), RedisValue[] get = null, CommandFlags flags = CommandFlags.None)
        {
            long returnValue;
            var conn = GetConnection();
            {
                var db = conn.GetDatabase();
                returnValue = db.SortAndStore(NameOf(destination), NameOf(key), skip, take, order, sortType, by, get, flags);
            }
            return returnValue;
        }

        public Task<long> SortAndStoreAsync(RedisKey destination, RedisKey key, long skip = 0, long take = -1, Order order = Order.Ascending, SortType sortType = SortType.Numeric, RedisValue by = default(RedisValue), RedisValue[] get = null, CommandFlags flags = CommandFlags.None)
        {
            Task<long> returnValue;
            var conn = GetConnection();
            {
                var db = conn.GetDatabase();
                returnValue = db.SortAndStoreAsync(NameOf(destination), NameOf(key), skip, take, order, sortType, by, get, flags);
            }
            return returnValue;
        }

        public Task<RedisValue[]> SortAsync(RedisKey key, long skip = 0, long take = -1, Order order = Order.Ascending, SortType sortType = SortType.Numeric, RedisValue by = default(RedisValue), RedisValue[] get = null, CommandFlags flags = CommandFlags.None)
        {
            Task<RedisValue[]> returnValue;
            var conn = GetConnection();
            {
                var db = conn.GetDatabase();
                returnValue = db.SortAsync(NameOf(key), skip, take, order, sortType, by, get, flags);
            }
            return returnValue;
        }

        public bool SortedSetAdd(RedisKey key, RedisValue member, double score, CommandFlags flags)
        {
            bool returnValue;
            var conn = GetConnection();
            {
                var db = conn.GetDatabase();
                returnValue = db.SortedSetAdd(NameOf(key), member, score, flags);
            }
            return returnValue;
        }

        public bool SortedSetAdd(RedisKey key, RedisValue member, double score, When when = When.Always, CommandFlags flags = CommandFlags.None)
        {
            bool returnValue;
            var conn = GetConnection();
            {
                var db = conn.GetDatabase();
                returnValue = db.SortedSetAdd(NameOf(key), member, score, when, flags);
            }
            return returnValue;
        }

        public long SortedSetAdd(RedisKey key, SortedSetEntry[] values, CommandFlags flags)
        {
            long returnValue;
            var conn = GetConnection();
            {
                var db = conn.GetDatabase();
                returnValue = db.SortedSetAdd(NameOf(key), values, flags);
            }
            return returnValue;
        }

        public long SortedSetAdd(RedisKey key, SortedSetEntry[] values, When when = When.Always, CommandFlags flags = CommandFlags.None)
        {
            long returnValue;
            var conn = GetConnection();
            {
                var db = conn.GetDatabase();
                returnValue = db.SortedSetAdd(NameOf(key), values, when, flags);
            }
            return returnValue;
        }

        public Task<bool> SortedSetAddAsync(RedisKey key, RedisValue member, double score, CommandFlags flags)
        {
            Task<bool> returnValue;
            var conn = GetConnection();
            {
                var db = conn.GetDatabase();
                returnValue = db.SortedSetAddAsync(NameOf(key), member, score, flags);
            }
            return returnValue;
        }

        public Task<bool> SortedSetAddAsync(RedisKey key, RedisValue member, double score, When when = When.Always, CommandFlags flags = CommandFlags.None)
        {
            Task<bool> returnValue;
            var conn = GetConnection();
            {
                var db = conn.GetDatabase();
                returnValue = db.SortedSetAddAsync(NameOf(key), member, score, when, flags);
            }
            return returnValue;
        }

        public Task<long> SortedSetAddAsync(RedisKey key, SortedSetEntry[] values, CommandFlags flags)
        {
            Task<long> returnValue;
            var conn = GetConnection();
            {
                var db = conn.GetDatabase();
                returnValue = db.SortedSetAddAsync(NameOf(key), values, flags);
            }
            return returnValue;
        }

        public Task<long> SortedSetAddAsync(RedisKey key, SortedSetEntry[] values, When when = When.Always, CommandFlags flags = CommandFlags.None)
        {
            Task<long> returnValue;
            var conn = GetConnection();
            {
                var db = conn.GetDatabase();
                returnValue = db.SortedSetAddAsync(NameOf(key), values, when, flags);
            }
            return returnValue;
        }

        public long SortedSetCombineAndStore(SetOperation operation, RedisKey destination, RedisKey first, RedisKey second, Aggregate aggregate = Aggregate.Sum, CommandFlags flags = CommandFlags.None)
        {
            long returnValue;
            var conn = GetConnection();
            {
                var db = conn.GetDatabase();
                returnValue = db.SortedSetCombineAndStore(operation, NameOf(destination), NameOf(first), NameOf(second), aggregate, flags);
            }
            return returnValue;
        }

        public long SortedSetCombineAndStore(SetOperation operation, RedisKey destination, RedisKey[] keys, double[] weights = null, Aggregate aggregate = Aggregate.Sum, CommandFlags flags = CommandFlags.None)
        {
            long returnValue;
            if (keys != null)
            {
                for (var i = 0; i < keys.Length; ++i)
                {
                    keys[i] = NameOf(keys[i]);
                }
            }
            var conn = GetConnection();
            {
                var db = conn.GetDatabase();
                returnValue = db.SortedSetCombineAndStore(operation, NameOf(destination), keys, weights, aggregate, flags);
            }
            return returnValue;
        }

        public Task<long> SortedSetCombineAndStoreAsync(SetOperation operation, RedisKey destination, RedisKey first, RedisKey second, Aggregate aggregate = Aggregate.Sum, CommandFlags flags = CommandFlags.None)
        {
            Task<long> returnValue;
            var conn = GetConnection();
            {
                var db = conn.GetDatabase();
                returnValue = db.SortedSetCombineAndStoreAsync(operation, NameOf(destination), NameOf(first), NameOf(second), aggregate, flags);
            }
            return returnValue;
        }

        public Task<long> SortedSetCombineAndStoreAsync(SetOperation operation, RedisKey destination, RedisKey[] keys, double[] weights = null, Aggregate aggregate = Aggregate.Sum, CommandFlags flags = CommandFlags.None)
        {
            Task<long> returnValue;
            if (keys != null)
            {
                for (var i = 0; i < keys.Length; ++i)
                {
                    keys[i] = NameOf(keys[i]);
                }
            }
            var conn = GetConnection();
            {
                var db = conn.GetDatabase();
                returnValue = db.SortedSetCombineAndStoreAsync(operation, NameOf(destination), keys, weights, aggregate, flags);
            }
            return returnValue;
        }

        public double SortedSetDecrement(RedisKey key, RedisValue member, double value, CommandFlags flags = CommandFlags.None)
        {
            double returnValue;
            var conn = GetConnection();
            {
                var db = conn.GetDatabase();
                returnValue = db.SortedSetDecrement(NameOf(key), member, value, flags);
            }
            return returnValue;
        }

        public Task<double> SortedSetDecrementAsync(RedisKey key, RedisValue member, double value, CommandFlags flags = CommandFlags.None)
        {
            Task<double> returnValue;
            var conn = GetConnection();
            {
                var db = conn.GetDatabase();
                returnValue = db.SortedSetDecrementAsync(NameOf(key), member, value, flags);
            }
            return returnValue;
        }

        public double SortedSetIncrement(RedisKey key, RedisValue member, double value, CommandFlags flags = CommandFlags.None)
        {
            double returnValue;
            var conn = GetConnection();
            {
                var db = conn.GetDatabase();
                returnValue = db.SortedSetIncrement(NameOf(key), member, value, flags);
            }
            return returnValue;
        }

        public Task<double> SortedSetIncrementAsync(RedisKey key, RedisValue member, double value, CommandFlags flags = CommandFlags.None)
        {
            Task<double> returnValue;
            var conn = GetConnection();
            {
                var db = conn.GetDatabase();
                returnValue = db.SortedSetIncrementAsync(NameOf(key), member, value, flags);
            }
            return returnValue;
        }

        public long SortedSetLength(RedisKey key, double min = double.NegativeInfinity, double max = double.PositiveInfinity, Exclude exclude = Exclude.None, CommandFlags flags = CommandFlags.None)
        {
            long returnValue;
            var conn = GetConnection();
            {
                var db = conn.GetDatabase();
                returnValue = db.SortedSetLength(NameOf(key), min, max, exclude, flags);
            }
            return returnValue;
        }

        public Task<long> SortedSetLengthAsync(RedisKey key, double min = double.NegativeInfinity, double max = double.PositiveInfinity, Exclude exclude = Exclude.None, CommandFlags flags = CommandFlags.None)
        {
            Task<long> returnValue;
            var conn = GetConnection();
            {
                var db = conn.GetDatabase();
                returnValue = db.SortedSetLengthAsync(NameOf(key), min, max, exclude, flags);
            }
            return returnValue;
        }

        public long SortedSetLengthByValue(RedisKey key, RedisValue min, RedisValue max, Exclude exclude = Exclude.None, CommandFlags flags = CommandFlags.None)
        {
            long returnValue;
            var conn = GetConnection();
            {
                var db = conn.GetDatabase();
                returnValue = db.SortedSetLengthByValue(NameOf(key), min, max, exclude, flags);
            }
            return returnValue;
        }

        public Task<long> SortedSetLengthByValueAsync(RedisKey key, RedisValue min, RedisValue max, Exclude exclude = Exclude.None, CommandFlags flags = CommandFlags.None)
        {
            Task<long> returnValue;
            var conn = GetConnection();
            {
                var db = conn.GetDatabase();
                returnValue = db.SortedSetLengthByValueAsync(NameOf(key), min, max, exclude, flags);
            }
            return returnValue;
        }

        public RedisValue[] SortedSetRangeByRank(RedisKey key, long start = 0, long stop = -1, Order order = Order.Ascending, CommandFlags flags = CommandFlags.None)
        {
            RedisValue[] returnValue;
            var conn = GetConnection();
            {
                var db = conn.GetDatabase();
                returnValue = db.SortedSetRangeByRank(NameOf(key), start, stop, order, flags);
            }
            return returnValue;
        }

        public Task<RedisValue[]> SortedSetRangeByRankAsync(RedisKey key, long start = 0, long stop = -1, Order order = Order.Ascending, CommandFlags flags = CommandFlags.None)
        {
            Task<RedisValue[]> returnValue;
            var conn = GetConnection();
            {
                var db = conn.GetDatabase();
                returnValue = db.SortedSetRangeByRankAsync(NameOf(key), start, stop, order, flags);
            }
            return returnValue;
        }

        public SortedSetEntry[] SortedSetRangeByRankWithScores(RedisKey key, long start = 0, long stop = -1, Order order = Order.Ascending, CommandFlags flags = CommandFlags.None)
        {
            SortedSetEntry[] returnValue;
            var conn = GetConnection();
            {
                var db = conn.GetDatabase();
                returnValue = db.SortedSetRangeByRankWithScores(NameOf(key), start, stop, order, flags);
            }
            return returnValue;
        }

        public Task<SortedSetEntry[]> SortedSetRangeByRankWithScoresAsync(RedisKey key, long start = 0, long stop = -1, Order order = Order.Ascending, CommandFlags flags = CommandFlags.None)
        {
            Task<SortedSetEntry[]> returnValue;
            var conn = GetConnection();
            {
                var db = conn.GetDatabase();
                returnValue = db.SortedSetRangeByRankWithScoresAsync(NameOf(key), start, stop, order, flags);
            }
            return returnValue;
        }

        public RedisValue[] SortedSetRangeByScore(RedisKey key, double start = double.NegativeInfinity, double stop = double.PositiveInfinity, Exclude exclude = Exclude.None, Order order = Order.Ascending, long skip = 0, long take = -1, CommandFlags flags = CommandFlags.None)
        {
            RedisValue[] returnValue;
            var conn = GetConnection();
            {
                var db = conn.GetDatabase();
                returnValue = db.SortedSetRangeByScore(NameOf(key), start, stop, exclude, order, skip, take, flags);
            }
            return returnValue;
        }

        public Task<RedisValue[]> SortedSetRangeByScoreAsync(RedisKey key, double start = double.NegativeInfinity, double stop = double.PositiveInfinity, Exclude exclude = Exclude.None, Order order = Order.Ascending, long skip = 0, long take = -1, CommandFlags flags = CommandFlags.None)
        {
            Task<RedisValue[]> returnValue;
            var conn = GetConnection();
            {
                var db = conn.GetDatabase();
                returnValue = db.SortedSetRangeByScoreAsync(NameOf(key), start, stop, exclude, order, skip, take, flags);
            }
            return returnValue;
        }

        public SortedSetEntry[] SortedSetRangeByScoreWithScores(RedisKey key, double start = double.NegativeInfinity, double stop = double.PositiveInfinity, Exclude exclude = Exclude.None, Order order = Order.Ascending, long skip = 0, long take = -1, CommandFlags flags = CommandFlags.None)
        {
            SortedSetEntry[] returnValue;
            var conn = GetConnection();
            {
                var db = conn.GetDatabase();
                returnValue = db.SortedSetRangeByScoreWithScores(NameOf(key), start, stop, exclude, order, skip, take, flags);
            }
            return returnValue;
        }

        public Task<SortedSetEntry[]> SortedSetRangeByScoreWithScoresAsync(RedisKey key, double start = double.NegativeInfinity, double stop = double.PositiveInfinity, Exclude exclude = Exclude.None, Order order = Order.Ascending, long skip = 0, long take = -1, CommandFlags flags = CommandFlags.None)
        {
            Task<SortedSetEntry[]> returnValue;
            var conn = GetConnection();
            {
                var db = conn.GetDatabase();
                returnValue = db.SortedSetRangeByScoreWithScoresAsync(NameOf(key), start, stop, exclude, order, skip, take, flags);
            }
            return returnValue;
        }

        public RedisValue[] SortedSetRangeByValue(RedisKey key, RedisValue min = default(RedisValue), RedisValue max = default(RedisValue), Exclude exclude = Exclude.None, long skip = 0, long take = -1, CommandFlags flags = CommandFlags.None)
        {
            RedisValue[] returnValue;
            var conn = GetConnection();
            {
                var db = conn.GetDatabase();
                returnValue = db.SortedSetRangeByValue(NameOf(key), min, max, exclude, skip, take, flags);
            }
            return returnValue;
        }

        public Task<RedisValue[]> SortedSetRangeByValueAsync(RedisKey key, RedisValue min = default(RedisValue), RedisValue max = default(RedisValue), Exclude exclude = Exclude.None, long skip = 0, long take = -1, CommandFlags flags = CommandFlags.None)
        {
            Task<RedisValue[]> returnValue;
            var conn = GetConnection();
            {
                var db = conn.GetDatabase();
                returnValue = db.SortedSetRangeByValueAsync(NameOf(key), min, max, exclude, skip, take, flags);
            }
            return returnValue;
        }

        public long? SortedSetRank(RedisKey key, RedisValue member, Order order = Order.Ascending, CommandFlags flags = CommandFlags.None)
        {
            long? returnValue;
            var conn = GetConnection();
            {
                var db = conn.GetDatabase();
                returnValue = db.SortedSetRank(NameOf(key), member, order, flags);
            }
            return returnValue;
        }

        public Task<long?> SortedSetRankAsync(RedisKey key, RedisValue member, Order order = Order.Ascending, CommandFlags flags = CommandFlags.None)
        {
            Task<long?> returnValue;
            var conn = GetConnection();
            {
                var db = conn.GetDatabase();
                returnValue = db.SortedSetRankAsync(NameOf(key), member, order, flags);
            }
            return returnValue;
        }

        public bool SortedSetRemove(RedisKey key, RedisValue member, CommandFlags flags = CommandFlags.None)
        {
            bool returnValue;
            var conn = GetConnection();
            {
                var db = conn.GetDatabase();
                returnValue = db.SortedSetRemove(NameOf(key), member, flags);
            }
            return returnValue;
        }

        public long SortedSetRemove(RedisKey key, RedisValue[] members, CommandFlags flags = CommandFlags.None)
        {
            long returnValue;
            var conn = GetConnection();
            {
                var db = conn.GetDatabase();
                returnValue = db.SortedSetRemove(NameOf(key), members, flags);
            }
            return returnValue;
        }

        public Task<bool> SortedSetRemoveAsync(RedisKey key, RedisValue member, CommandFlags flags = CommandFlags.None)
        {
            Task<bool> returnValue;
            var conn = GetConnection();
            {
                var db = conn.GetDatabase();
                returnValue = db.SortedSetRemoveAsync(NameOf(key), member, flags);
            }
            return returnValue;
        }

        public Task<long> SortedSetRemoveAsync(RedisKey key, RedisValue[] members, CommandFlags flags = CommandFlags.None)
        {
            Task<long> returnValue;
            var conn = GetConnection();
            {
                var db = conn.GetDatabase();
                returnValue = db.SortedSetRemoveAsync(NameOf(key), members, flags);
            }
            return returnValue;
        }

        public long SortedSetRemoveRangeByRank(RedisKey key, long start, long stop, CommandFlags flags = CommandFlags.None)
        {
            long returnValue;
            var conn = GetConnection();
            {
                var db = conn.GetDatabase();
                returnValue = db.SortedSetRemoveRangeByRank(NameOf(key), start, stop, flags);
            }
            return returnValue;
        }

        public Task<long> SortedSetRemoveRangeByRankAsync(RedisKey key, long start, long stop, CommandFlags flags = CommandFlags.None)
        {
            Task<long> returnValue;
            var conn = GetConnection();
            {
                var db = conn.GetDatabase();
                returnValue = db.SortedSetRemoveRangeByRankAsync(NameOf(key), start, stop, flags);
            }
            return returnValue;
        }

        public long SortedSetRemoveRangeByScore(RedisKey key, double start, double stop, Exclude exclude = Exclude.None, CommandFlags flags = CommandFlags.None)
        {
            long returnValue;
            var conn = GetConnection();
            {
                var db = conn.GetDatabase();
                returnValue = db.SortedSetRemoveRangeByScore(NameOf(key), start, stop, exclude, flags);
            }
            return returnValue;
        }

        public Task<long> SortedSetRemoveRangeByScoreAsync(RedisKey key, double start, double stop, Exclude exclude = Exclude.None, CommandFlags flags = CommandFlags.None)
        {
            Task<long> returnValue;
            var conn = GetConnection();
            {
                var db = conn.GetDatabase();
                returnValue = db.SortedSetRemoveRangeByScoreAsync(NameOf(key), start, stop, exclude, flags);
            }
            return returnValue;
        }

        public long SortedSetRemoveRangeByValue(RedisKey key, RedisValue min, RedisValue max, Exclude exclude = Exclude.None, CommandFlags flags = CommandFlags.None)
        {
            long returnValue;
            var conn = GetConnection();
            {
                var db = conn.GetDatabase();
                returnValue = db.SortedSetRemoveRangeByValue(NameOf(key), min, max, exclude, flags);
            }
            return returnValue;
        }

        public Task<long> SortedSetRemoveRangeByValueAsync(RedisKey key, RedisValue min, RedisValue max, Exclude exclude = Exclude.None, CommandFlags flags = CommandFlags.None)
        {
            Task<long> returnValue;
            var conn = GetConnection();
            {
                var db = conn.GetDatabase();
                returnValue = db.SortedSetRemoveRangeByValueAsync(NameOf(key), min, max, exclude, flags);
            }
            return returnValue;
        }

        public IEnumerable<SortedSetEntry> SortedSetScan(RedisKey key, RedisValue pattern, int pageSize, CommandFlags flags)
        {
            IEnumerable<SortedSetEntry> returnValue;
            var conn = GetConnection();
            {
                var db = conn.GetDatabase();
                returnValue = db.SortedSetScan(NameOf(key), pattern, pageSize, flags);
            }
            return returnValue;
        }

        public IEnumerable<SortedSetEntry> SortedSetScan(RedisKey key, RedisValue pattern = default(RedisValue), int pageSize = 10, long cursor = 0, int pageOffset = 0, CommandFlags flags = CommandFlags.None)
        {
            IEnumerable<SortedSetEntry> returnValue;
            var conn = GetConnection();
            {
                var db = conn.GetDatabase();
                returnValue = db.SortedSetScan(NameOf(key), pattern, pageSize, cursor, pageOffset, flags);
            }
            return returnValue;
        }

        public double? SortedSetScore(RedisKey key, RedisValue member, CommandFlags flags = CommandFlags.None)
        {
            double? returnValue;
            var conn = GetConnection();
            {
                var db = conn.GetDatabase();
                returnValue = db.SortedSetScore(NameOf(key), member, flags);
            }
            return returnValue;
        }

        public Task<double?> SortedSetScoreAsync(RedisKey key, RedisValue member, CommandFlags flags = CommandFlags.None)
        {
            Task<double?> returnValue;
            var conn = GetConnection();
            {
                var db = conn.GetDatabase();
                returnValue = db.SortedSetScoreAsync(NameOf(key), member, flags);
            }
            return returnValue;
        }

        public long StringAppend(RedisKey key, RedisValue value, CommandFlags flags = CommandFlags.None)
        {
            long returnValue;
            var conn = GetConnection();
            {
                var db = conn.GetDatabase();
                returnValue = db.StringAppend(NameOf(key), value, flags);
            }
            return returnValue;
        }

        public Task<long> StringAppendAsync(RedisKey key, RedisValue value, CommandFlags flags = CommandFlags.None)
        {
            Task<long> returnValue;
            var conn = GetConnection();
            {
                var db = conn.GetDatabase();
                returnValue = db.StringAppendAsync(NameOf(key), value, flags);
            }
            return returnValue;
        }

        public long StringBitCount(RedisKey key, long start = 0, long end = -1, CommandFlags flags = CommandFlags.None)
        {
            long returnValue;
            var conn = GetConnection();
            {
                var db = conn.GetDatabase();
                returnValue = db.StringBitCount(NameOf(key), start, end, flags);
            }
            return returnValue;
        }

        public Task<long> StringBitCountAsync(RedisKey key, long start = 0, long end = -1, CommandFlags flags = CommandFlags.None)
        {
            Task<long> returnValue;
            var conn = GetConnection();
            {
                var db = conn.GetDatabase();
                returnValue = db.StringBitCountAsync(NameOf(key), start, end, flags);
            }
            return returnValue;
        }

        public long StringBitOperation(Bitwise operation, RedisKey destination, RedisKey first, RedisKey second = default(RedisKey), CommandFlags flags = CommandFlags.None)
        {
            long returnValue;
            var conn = GetConnection();
            {
                var db = conn.GetDatabase();
                returnValue = db.StringBitOperation(operation, NameOf(destination), NameOf(first), NameOf(second), flags);
            }
            return returnValue;
        }

        public long StringBitOperation(Bitwise operation, RedisKey destination, RedisKey[] keys, CommandFlags flags = CommandFlags.None)
        {
            long returnValue;
            if (keys != null)
            {
                for (var i = 0; i < keys.Length; ++i)
                {
                    keys[i] = NameOf(keys[i]);
                }
            }
            var conn = GetConnection();
            {
                var db = conn.GetDatabase();
                returnValue = db.StringBitOperation(operation, NameOf(destination), keys, flags);
            }
            return returnValue;
        }

        public Task<long> StringBitOperationAsync(Bitwise operation, RedisKey destination, RedisKey first, RedisKey second = default(RedisKey), CommandFlags flags = CommandFlags.None)
        {
            Task<long> returnValue;
            var conn = GetConnection();
            {
                var db = conn.GetDatabase();
                returnValue = db.StringBitOperationAsync(operation, NameOf(destination), NameOf(first), NameOf(second), flags);
            }
            return returnValue;
        }

        public Task<long> StringBitOperationAsync(Bitwise operation, RedisKey destination, RedisKey[] keys, CommandFlags flags = CommandFlags.None)
        {
            Task<long> returnValue;
            if (keys != null)
            {
                for (var i = 0; i < keys.Length; ++i)
                {
                    keys[i] = NameOf(keys[i]);
                }
            }
            var conn = GetConnection();
            {
                var db = conn.GetDatabase();
                returnValue = db.StringBitOperationAsync(operation, NameOf(destination), keys, flags);
            }
            return returnValue;
        }

        public long StringBitPosition(RedisKey key, bool bit, long start = 0, long end = -1, CommandFlags flags = CommandFlags.None)
        {
            long returnValue;
            var conn = GetConnection();
            {
                var db = conn.GetDatabase();
                returnValue = db.StringBitPosition(NameOf(key), bit, start, end, flags);
            }
            return returnValue;
        }

        public Task<long> StringBitPositionAsync(RedisKey key, bool bit, long start = 0, long end = -1, CommandFlags flags = CommandFlags.None)
        {
            Task<long> returnValue;
            var conn = GetConnection();
            {
                var db = conn.GetDatabase();
                returnValue = db.StringBitPositionAsync(NameOf(key), bit, start, end, flags);
            }
            return returnValue;
        }

        public long StringDecrement(RedisKey key, long value = 1, CommandFlags flags = CommandFlags.None)
        {
            long returnValue;
            var conn = GetConnection();
            {
                var db = conn.GetDatabase();
                returnValue = db.StringDecrement(NameOf(key), value, flags);
            }
            return returnValue;
        }

        public double StringDecrement(RedisKey key, double value, CommandFlags flags = CommandFlags.None)
        {
            double returnValue;
            var conn = GetConnection();
            {
                var db = conn.GetDatabase();
                returnValue = db.StringDecrement(NameOf(key), value, flags);
            }
            return returnValue;
        }

        public Task<long> StringDecrementAsync(RedisKey key, long value = 1, CommandFlags flags = CommandFlags.None)
        {
            Task<long> returnValue;
            var conn = GetConnection();
            {
                var db = conn.GetDatabase();
                returnValue = db.StringDecrementAsync(NameOf(key), value, flags);
            }
            return returnValue;
        }

        public Task<double> StringDecrementAsync(RedisKey key, double value, CommandFlags flags = CommandFlags.None)
        {
            Task<double> returnValue;
            var conn = GetConnection();
            {
                var db = conn.GetDatabase();
                returnValue = db.StringDecrementAsync(NameOf(key), value, flags);
            }
            return returnValue;
        }

        public RedisValue StringGet(RedisKey key, CommandFlags flags = CommandFlags.None)
        {
            RedisValue returnValue;
            var conn = GetConnection();
            {
                var db = conn.GetDatabase();
                returnValue = db.StringGet(NameOf(key), flags);
            }
            return returnValue;
        }

        public RedisValue[] StringGet(RedisKey[] keys, CommandFlags flags = CommandFlags.None)
        {
            RedisValue[] returnValue;
            if (keys != null)
            {
                for (var i = 0; i < keys.Length; ++i)
                {
                    keys[i] = NameOf(keys[i]);
                }
            }
            var conn = GetConnection();
            {
                var db = conn.GetDatabase();
                returnValue = db.StringGet(keys, flags);
            }
            return returnValue;
        }

        public Task<RedisValue> StringGetAsync(RedisKey key, CommandFlags flags = CommandFlags.None)
        {
            Task<RedisValue> returnValue;
            var conn = GetConnection();
            {
                var db = conn.GetDatabase();
                returnValue = db.StringGetAsync(NameOf(key), flags);
            }
            return returnValue;
        }

        public Task<RedisValue[]> StringGetAsync(RedisKey[] keys, CommandFlags flags = CommandFlags.None)
        {
            Task<RedisValue[]> returnValue;
            if (keys != null)
            {
                for (var i = 0; i < keys.Length; ++i)
                {
                    keys[i] = NameOf(keys[i]);
                }
            }
            var conn = GetConnection();
            {
                var db = conn.GetDatabase();
                returnValue = db.StringGetAsync(keys, flags);
            }
            return returnValue;
        }

        public bool StringGetBit(RedisKey key, long offset, CommandFlags flags = CommandFlags.None)
        {
            bool returnValue;
            var conn = GetConnection();
            {
                var db = conn.GetDatabase();
                returnValue = db.StringGetBit(NameOf(key), offset, flags);
            }
            return returnValue;
        }

        public Task<bool> StringGetBitAsync(RedisKey key, long offset, CommandFlags flags = CommandFlags.None)
        {
            Task<bool> returnValue;
            var conn = GetConnection();
            {
                var db = conn.GetDatabase();
                returnValue = db.StringGetBitAsync(NameOf(key), offset, flags);
            }
            return returnValue;
        }

        public RedisValue StringGetRange(RedisKey key, long start, long end, CommandFlags flags = CommandFlags.None)
        {
            RedisValue returnValue;
            var conn = GetConnection();
            {
                var db = conn.GetDatabase();
                returnValue = db.StringGetRange(NameOf(key), start, end, flags);
            }
            return returnValue;
        }

        public Task<RedisValue> StringGetRangeAsync(RedisKey key, long start, long end, CommandFlags flags = CommandFlags.None)
        {
            Task<RedisValue> returnValue;
            var conn = GetConnection();
            {
                var db = conn.GetDatabase();
                returnValue = db.StringGetRangeAsync(NameOf(key), start, end, flags);
            }
            return returnValue;
        }

        public RedisValue StringGetSet(RedisKey key, RedisValue value, CommandFlags flags = CommandFlags.None)
        {
            RedisValue returnValue;
            var conn = GetConnection();
            {
                var db = conn.GetDatabase();
                returnValue = db.StringGetSet(NameOf(key), value, flags);
            }
            return returnValue;
        }

        public Task<RedisValue> StringGetSetAsync(RedisKey key, RedisValue value, CommandFlags flags = CommandFlags.None)
        {
            Task<RedisValue> returnValue;
            var conn = GetConnection();
            {
                var db = conn.GetDatabase();
                returnValue = db.StringGetSetAsync(NameOf(key), value, flags);
            }
            return returnValue;
        }

        public RedisValueWithExpiry StringGetWithExpiry(RedisKey key, CommandFlags flags = CommandFlags.None)
        {
            RedisValueWithExpiry returnValue;
            var conn = GetConnection();
            {
                var db = conn.GetDatabase();
                returnValue = db.StringGetWithExpiry(NameOf(key), flags);
            }
            return returnValue;
        }

        public Task<RedisValueWithExpiry> StringGetWithExpiryAsync(RedisKey key, CommandFlags flags = CommandFlags.None)
        {
            Task<RedisValueWithExpiry> returnValue;
            var conn = GetConnection();
            {
                var db = conn.GetDatabase();
                returnValue = db.StringGetWithExpiryAsync(NameOf(key), flags);
            }
            return returnValue;
        }

        public long StringIncrement(RedisKey key, long value = 1, CommandFlags flags = CommandFlags.None)
        {
            long returnValue;
            var conn = GetConnection();
            {
                var db = conn.GetDatabase();
                returnValue = db.StringIncrement(NameOf(key), value, flags);
            }
            return returnValue;
        }

        public double StringIncrement(RedisKey key, double value, CommandFlags flags = CommandFlags.None)
        {
            double returnValue;
            var conn = GetConnection();
            {
                var db = conn.GetDatabase();
                returnValue = db.StringIncrement(NameOf(key), value, flags);
            }
            return returnValue;
        }

        public Task<long> StringIncrementAsync(RedisKey key, long value = 1, CommandFlags flags = CommandFlags.None)
        {
            Task<long> returnValue;
            var conn = GetConnection();
            {
                var db = conn.GetDatabase();
                returnValue = db.StringIncrementAsync(NameOf(key), value, flags);
            }
            return returnValue;
        }

        public Task<double> StringIncrementAsync(RedisKey key, double value, CommandFlags flags = CommandFlags.None)
        {
            Task<double> returnValue;
            var conn = GetConnection();
            {
                var db = conn.GetDatabase();
                returnValue = db.StringIncrementAsync(NameOf(key), value, flags);
            }
            return returnValue;
        }

        public long StringLength(RedisKey key, CommandFlags flags = CommandFlags.None)
        {
            long returnValue;
            var conn = GetConnection();
            {
                var db = conn.GetDatabase();
                returnValue = db.StringLength(NameOf(key), flags);
            }
            return returnValue;
        }

        public Task<long> StringLengthAsync(RedisKey key, CommandFlags flags = CommandFlags.None)
        {
            Task<long> returnValue;
            var conn = GetConnection();
            {
                var db = conn.GetDatabase();
                returnValue = db.StringLengthAsync(NameOf(key), flags);
            }
            return returnValue;
        }

        public bool StringSet(RedisKey key, RedisValue value, TimeSpan? expiry = null, When when = When.Always, CommandFlags flags = CommandFlags.None)
        {
            bool returnValue;
            var conn = GetConnection();
            {
                var db = conn.GetDatabase();
                returnValue = db.StringSet(NameOf(key), value, expiry, when, flags);
            }
            return returnValue;
        }

        public bool StringSet(KeyValuePair<RedisKey, RedisValue>[] values, When when = When.Always, CommandFlags flags = CommandFlags.None)
        {
            bool returnValue = false;
            if (values != null)
            {
                var temp = new KeyValuePair<RedisKey, RedisValue>[values.Length];
                for (var i = 0; i < temp.Length; ++i)
                {
                    temp[i] = new KeyValuePair<RedisKey, RedisValue>(NameOf(values[i].Key), values[i].Value);
                }
                var conn = GetConnection();
                {
                    var db = conn.GetDatabase();
                    returnValue = db.StringSet(temp, when, flags);
                }
            }
            return returnValue;
        }

        public Task<bool> StringSetAsync(RedisKey key, RedisValue value, TimeSpan? expiry = null, When when = When.Always, CommandFlags flags = CommandFlags.None)
        {
            Task<bool> returnValue;
            var conn = GetConnection();
            {
                var db = conn.GetDatabase();
                returnValue = db.StringSetAsync(NameOf(key), value, expiry, when, flags);
            }
            return returnValue;
        }

        public Task<bool> StringSetAsync(KeyValuePair<RedisKey, RedisValue>[] values, When when = When.Always, CommandFlags flags = CommandFlags.None)
        {
            var returnValue = Task.FromResult(false);
            if (values != null)
            {
                var temp = new KeyValuePair<RedisKey, RedisValue>[values.Length];
                for (var i = 0; i < temp.Length; ++i)
                {
                    temp[i] = new KeyValuePair<RedisKey, RedisValue>(NameOf(values[i].Key), values[i].Value);
                }
                var conn = GetConnection();
                {
                    var db = conn.GetDatabase();
                    returnValue = db.StringSetAsync(temp, when, flags);
                }
            }
            return returnValue;
        }

        public bool StringSetBit(RedisKey key, long offset, bool bit, CommandFlags flags = CommandFlags.None)
        {
            bool returnValue;
            var conn = GetConnection();
            {
                var db = conn.GetDatabase();
                returnValue = db.StringSetBit(NameOf(key), offset, bit, flags);
            }
            return returnValue;
        }

        public Task<bool> StringSetBitAsync(RedisKey key, long offset, bool bit, CommandFlags flags = CommandFlags.None)
        {
            Task<bool> returnValue;
            var conn = GetConnection();
            {
                var db = conn.GetDatabase();
                returnValue = db.StringSetBitAsync(NameOf(key), offset, bit, flags);
            }
            return returnValue;
        }

        public RedisValue StringSetRange(RedisKey key, long offset, RedisValue value, CommandFlags flags = CommandFlags.None)
        {
            RedisValue returnValue;
            var conn = GetConnection();
            {
                var db = conn.GetDatabase();
                returnValue = db.StringSetRange(NameOf(key), offset, value, flags);
            }
            return returnValue;
        }

        public Task<RedisValue> StringSetRangeAsync(RedisKey key, long offset, RedisValue value, CommandFlags flags = CommandFlags.None)
        {
            Task<RedisValue> returnValue;
            var conn = GetConnection();
            {
                var db = conn.GetDatabase();
                returnValue = db.StringSetRangeAsync(NameOf(key), offset, value, flags);
            }
            return returnValue;
        }

        public bool TryWait(Task task)
        {
            bool returnValue;
            var conn = GetConnection();
            {
                var db = conn.GetDatabase();
                returnValue = db.TryWait(task);
            }
            return returnValue;
        }

        public void Wait(Task task)
        {
            var conn = GetConnection();
            {
                var db = conn.GetDatabase();
                db.Wait(task);
            }
        }

        public T Wait<T>(Task<T> task)
        {
            T returnValue;
            var conn = GetConnection();
            {
                var db = conn.GetDatabase();
                returnValue = db.Wait(task);
            }
            return returnValue;
        }

        public void WaitAll(params Task[] tasks)
        {
            var conn = GetConnection();
            {
                var db = conn.GetDatabase();
                db.WaitAll(tasks);
            }
        }
        #endregion

        #region MULTI/EXEC

        public bool Multi(Action<ITransaction> handler, CommandFlags flags = CommandFlags.None)
        {
            bool returnValue = false;
            try
            {
                var conn = GetConnection();
                {
                    var db = conn.GetDatabase();
                    bool fn(ITransaction transaction)
                    {
                        try
                        {
                            handler(transaction);
                            return transaction.Execute(flags);
                        }
                        catch (Exception ex)
                        {
                            throw ex;
                        }
                    }
                    returnValue = fn(db.CreateTransaction());
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return returnValue;
        }

        public Task<bool> MultiAsync(Action<ITransaction> handler, CommandFlags flags = CommandFlags.None)
        {
            var returnValue = Task.FromResult(false);
            try
            {
                var conn = GetConnection();
                {
                    var db = conn.GetDatabase();
                    Task<bool> fn(ITransaction transaction)
                    {
                        try
                        {
                            handler(transaction);
                            return transaction.ExecuteAsync(flags);
                        }
                        catch
                        {
                            return returnValue;
                        }
                    }
                    returnValue = fn(db.CreateTransaction());
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return returnValue;
        }

        public bool Exec(Action<IDatabase> handler)
        {
            bool returnValue = false;
            try
            {
                var conn = GetConnection();
                {
                    bool fn(IDatabase database)
                    {
                        try
                        {
                            handler(database);
                        }
                        catch
                        {
                            return returnValue;
                        }
                        return true;
                    }
                    returnValue = fn(conn.GetDatabase());
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return returnValue;
        }

        public Task ExecAsync(Func<IDatabase, Task> handler)
        {
            var returnValue = default(Task);
            try
            {
                var conn = GetConnection();
                {
                    Task fn(IDatabase database)
                    {
                        try
                        {
                            return handler(database);
                        }
                        catch
                        {
                            return returnValue;
                        }
                    }
                    returnValue = fn(conn.GetDatabase());
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return returnValue;
        }

        public T Exec<T>(Func<IDatabase, T> handler)
        {
            var returnValue = default(T);
            try
            {
                var conn = GetConnection();
                {
                    T fn(IDatabase database)
                    {
                        try
                        {
                            return handler(database);
                        }
                        catch
                        {
                            return returnValue;
                        }
                    }
                    returnValue = fn(conn.GetDatabase());
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return returnValue;
        }

        public async Task<T> ExecAsync<T>(Func<IDatabase, Task<T>> handler)
        {
            var returnValue = default(T);
            try
            {
                var conn = GetConnection();
                {
                    Task<T> fn(IDatabase database)
                    {
                        try
                        {
                            return handler(database);
                        }
                        catch
                        {
                            return Task.FromResult(default(T));
                        }
                    }
                    returnValue = await fn(conn.GetDatabase());
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return returnValue;
        }
        #endregion

        #region Extenstion methods

        #region STRING

        public bool StringSetEx<T>(RedisKey key, T document, TimeSpan? expiry = null, When when = When.Always, CommandFlags flags = CommandFlags.None) where T : class, new()
        {
            return StringSet(key, Json.Stringify(document), expiry = null, when, flags);
        }

        public bool StringSetEx<T>(RedisKey key, IList<T> documents, TimeSpan? expiry = null, When when = When.Always, CommandFlags flags = CommandFlags.None) where T : class, new()
        {
            return StringSet(key, Json.Stringify(documents), expiry = null, when, flags);
        }

        public T StringGetEx<T>(RedisKey key, CommandFlags flags = CommandFlags.None) where T : class, new()
        {
            return Json.Parse<T>(StringGet(key, flags));
        }

        public T[] StringGetEx<T>(RedisKey[] keys, CommandFlags flags = CommandFlags.None) where T : class, new()
        {
            var returnValue = new T[] { };
            var values = StringGet(keys, flags);
            if (values != null)
            {
                returnValue = new T[values.Length];
                for (var i = 0; i < returnValue.Length; ++i)
                {
                    returnValue[i] = Json.Parse<T>(values[i]);
                }
            }
            return returnValue;
        }
        #endregion

        #region HASH

        public bool HashSetEx<T>(RedisKey key, RedisValue hashField, T document, When when = When.Always, CommandFlags flags = CommandFlags.None) where T : class, new()
        {
            return HashSet(key, hashField, Json.Stringify(document), when, flags);
        }
        public async Task<bool> HashSetExAsync<T>(RedisKey key, RedisValue hashField, T document, When when = When.Always, CommandFlags flags = CommandFlags.None) where T : class, new()
        {

            return await HashSetAsync(key, hashField, Json.Stringify(document), when, flags);
        }
        public bool HashSetEx<T>(RedisValue hashField, T document, When when = When.Always, CommandFlags flags = CommandFlags.None) where T : class, new()
        {
            return HashSetEx<T>(typeof(T).Name, hashField, document, when, flags);
        }
        public async Task<bool> HashSetExAsync<T>(RedisValue hashField, T document, When when = When.Always, CommandFlags flags = CommandFlags.None) where T : class, new()
        {
            return await HashSetExAsync<T>(typeof(T).Name, hashField, document, when, flags);
        }

        public void HashSetEx<T>(RedisKey key, KeyValuePair<RedisValue, T>[] hashFields, CommandFlags flags = CommandFlags.None) where T : class, new()
        {
            if (hashFields != null)
            {
                var temp = new HashEntry[hashFields.Length];
                for (var i = 0; i < temp.Length; ++i)
                {
                    temp[i] = new HashEntry(hashFields[i].Key, Json.Stringify(hashFields[i].Value));
                }
                HashSet(key, temp, flags);
            }
        }

        public void HashSetEx<T>(KeyValuePair<RedisValue, T>[] hashFields, CommandFlags flags = CommandFlags.None) where T : class, new()
        {
            HashSetEx<T>(typeof(T).Name, hashFields, flags);
        }

        public async Task<bool> HashSetMultExAsync<T>(IEnumerable<KeyValuePair<string, T>> data) where T : class, new()
        {
            if (data == null) return false;
            var hashId = typeof(T).Name;
            var meta = new List<HashEntry>();
            meta.AddRange(data.Select(s => new HashEntry(s.Key, Json.Stringify(s.Value))).ToList());
            //HashSet(NameOf(hashId), meta.ToArray());
            await HashSetAsync(hashId, meta.ToArray());
            return true;
        }

        public T HashGetEx<T>(RedisKey key, RedisValue hashField, CommandFlags flags = CommandFlags.None) where T : class, new()
        {
            return Json.Parse<T>(HashGet(key, hashField, flags));
        }
        public async Task<T> HashGetExAsync<T>(RedisKey key, RedisValue hashField, CommandFlags flags = CommandFlags.None) where T : class, new()
        {
            return Json.Parse<T>(await HashGetAsync(key, hashField, flags));
        }

        public T HashGetEx<T>(RedisValue hashField, CommandFlags flags = CommandFlags.None) where T : class, new()
        {
            return HashGetEx<T>(typeof(T).Name, hashField, flags);
        }
        public Task<T> HashGetExAsync<T>(RedisValue hashField, CommandFlags flags = CommandFlags.None) where T : class, new()
        {
            return HashGetExAsync<T>(typeof(T).Name, hashField, flags);
        }

        public T[] HashGetEx<T>(RedisKey key, RedisValue[] hashFields, CommandFlags flags = CommandFlags.None) where T : class, new()
        {
            var returnValue = new T[] { };
            var values = HashGet(key, hashFields, flags);
            if (values != null)
            {
                returnValue = new T[values.Length];
                for (var i = 0; i < returnValue.Length; ++i)
                {
                    returnValue[i] = Json.Parse<T>(values[i]);
                }
            }
            return returnValue;
        }

        public T[] HashGetEx<T>(RedisValue[] hashFields, CommandFlags flags = CommandFlags.None) where T : class, new()
        {
            return HashGetEx<T>(typeof(T).Name, hashFields, flags);
        }

        public KeyValuePair<RedisValue, T>[] HashGetAllEx<T>(RedisKey key, CommandFlags flags = CommandFlags.None) where T : class, new()
        {
            var returnValue = new KeyValuePair<RedisValue, T>[] { };
            var entries = HashGetAll(key, flags);
            if (entries != null)
            {
                returnValue = new KeyValuePair<RedisValue, T>[entries.Length];
                for (var i = 0; i < returnValue.Length; ++i)
                {
                    returnValue[i] = new KeyValuePair<RedisValue, T>(entries[i].Name, Json.Parse<T>(entries[i].Value));
                }
            }
            return returnValue;
        }

        public KeyValuePair<RedisValue, T>[] HashGetAllEx<T>(CommandFlags flags = CommandFlags.None) where T : class, new()
        {
            return HashGetAllEx<T>(typeof(T).Name, flags);
        }

        public T[] HashGetAllExx<T>(RedisKey key, CommandFlags flags = CommandFlags.None) where T : class, new()
        {
            var returnValue = new T[] { };
            var entries = HashGetAll(key, flags);
            if (entries != null)
            {
                returnValue = new T[entries.Length];
                for (var i = 0; i < returnValue.Length; ++i)
                {
                    returnValue[i] = Json.Parse<T>(entries[i].Value);
                }
            }
            return returnValue;
        }

        public T[] HashGetAllExx<T>(CommandFlags flags = CommandFlags.None) where T : class, new()
        {
            return HashGetAllExx<T>(typeof(T).Name, flags);
        }

        public async Task<List<T>> HashGetAllExxAsync<T>(RedisKey key, CommandFlags flags = CommandFlags.None) where T : class, new()
        {
            var returnValue = new List<T>();
            var entries = (await HashGetAllAsync(key, flags)).ToList();
            if (entries != null)
            {
                returnValue.AddRange(entries.Where(w => w.Value.HasValue).Select(s => Json.Parse<T>(s.Value)).ToList());
            }
            return returnValue;
        }

        public async Task<List<T>> HashGetAllExxAsync<T>(CommandFlags flags = CommandFlags.None) where T : class, new()
        {
            return await HashGetAllExxAsync<T>(typeof(T).Name, flags);
        }

        public async Task<List<T>> GetsFromHashExAsync<T>(string hashId, List<RedisValue> keys, CommandFlags flags = CommandFlags.None) where T : class, new()
        {
            var results = new List<T>();

            var valueReplies = (await HashGetAsync(hashId, keys.ToArray(), flags)).ToList();
            results.AddRange(valueReplies.Where(w => w.HasValue).Select(s => Json.Parse<T>(s)).ToList());

            return results;
        }

        public List<T> GetsFromHash<T>(string hashId, List<RedisValue> keys) where T : class, new()
        {
            var results = new List<T>();

            var valueReplies = (HashGet(hashId, keys.ToArray())).ToList();
            results.AddRange(valueReplies.Where(w => w.HasValue).Select(s => Json.Parse<T>(s)).ToList());

            return results;
        }
        public async Task<List<T>> GetsFromHashExAsync<T>(List<string> keys, CommandFlags flags = CommandFlags.None) where T : class, new()
        {
            var hashId = typeof(T).Name;
            var valueReplies = new List<RedisValue>();
            valueReplies.AddRange(keys.Select(s => (RedisValue)s));

            return await GetsFromHashExAsync<T>(hashId, valueReplies, flags);
        }

        public List<T> GetsFromHash<T>(List<string> keys) where T : class, new()
        {
            var hashId = typeof(T).Name;
            var valueReplies = new List<RedisValue>();
            valueReplies.AddRange(keys.Select(s => (RedisValue)s));

            return GetsFromHash<T>(hashId, valueReplies);
        }
        #endregion

        #region SORTED SET

        public bool SortedSetAddEx<T>(RedisKey key, T document, double score, CommandFlags flags = CommandFlags.None) where T : class, new()
        {
            return SortedSetAdd(key, Json.Stringify(document), score, flags);
        }

        public bool SortedSetAddEx<T>(T document, double score, CommandFlags flags = CommandFlags.None) where T : class, new()
        {
            return SortedSetAddEx<T>(typeof(T).Name, document, score, flags);
        }

        public T[] SortedSetRangeByScoreEx<T>(RedisKey key, double start = double.NegativeInfinity, double stop = double.PositiveInfinity, Exclude exclude = Exclude.None, Order order = Order.Ascending, long skip = 0, long take = -1, CommandFlags flags = CommandFlags.None) where T : class, new()
        {
            var returnValue = new T[] { };
            var members = SortedSetRangeByScore(key, start, stop, exclude, order, skip, take, flags);
            if (members != null)
            {
                returnValue = new T[members.Length];
                for (var i = 0; i < returnValue.Length; ++i)
                {
                    returnValue[i] = Json.Parse<T>(members[i]);
                }
            }
            return returnValue;
        }
        public T[] SortedSetRangeByScoreEx<T>(double start = double.NegativeInfinity, double stop = double.PositiveInfinity, Exclude exclude = Exclude.None, Order order = Order.Ascending, long skip = 0, long take = -1, CommandFlags flags = CommandFlags.None) where T : class, new()
        {
            return SortedSetRangeByScoreEx<T>(typeof(T).Name, start, stop, exclude, order, skip, take, flags);
        }
        #endregion

        #endregion

        #region Supported in Version 2.0.601

        public Lease<byte> HashGetLease(RedisKey key, RedisValue hashField, CommandFlags flags = CommandFlags.None)
        {
            throw new NotImplementedException();
        }

        public long KeyExists(RedisKey[] keys, CommandFlags flags = CommandFlags.None)
        {
            throw new NotImplementedException();
        }

        public TimeSpan? KeyIdleTime(RedisKey key, CommandFlags flags = CommandFlags.None)
        {
            throw new NotImplementedException();
        }

        public RedisValue[] SetPop(RedisKey key, long count, CommandFlags flags = CommandFlags.None)
        {
            throw new NotImplementedException();
        }

        public RedisValue[] SortedSetRangeByValue(RedisKey key, RedisValue min = default(RedisValue), RedisValue max = default(RedisValue), Exclude exclude = Exclude.None, Order order = Order.Ascending, long skip = 0, long take = -1, CommandFlags flags = CommandFlags.None)
        {
            throw new NotImplementedException();
        }

        public SortedSetEntry? SortedSetPop(RedisKey key, Order order = Order.Ascending, CommandFlags flags = CommandFlags.None)
        {
            throw new NotImplementedException();
        }

        public SortedSetEntry[] SortedSetPop(RedisKey key, long count, Order order = Order.Ascending, CommandFlags flags = CommandFlags.None)
        {
            throw new NotImplementedException();
        }

        public long StreamAcknowledge(RedisKey key, RedisValue groupName, RedisValue messageId, CommandFlags flags = CommandFlags.None)
        {
            throw new NotImplementedException();
        }

        public long StreamAcknowledge(RedisKey key, RedisValue groupName, RedisValue[] messageIds, CommandFlags flags = CommandFlags.None)
        {
            throw new NotImplementedException();
        }

        public RedisValue StreamAdd(RedisKey key, RedisValue streamField, RedisValue streamValue, RedisValue? messageId = null, int? maxLength = null, bool useApproximateMaxLength = false, CommandFlags flags = CommandFlags.None)
        {
            throw new NotImplementedException();
        }

        public RedisValue StreamAdd(RedisKey key, NameValueEntry[] streamPairs, RedisValue? messageId = null, int? maxLength = null, bool useApproximateMaxLength = false, CommandFlags flags = CommandFlags.None)
        {
            throw new NotImplementedException();
        }

        public StreamEntry[] StreamClaim(RedisKey key, RedisValue consumerGroup, RedisValue claimingConsumer, long minIdleTimeInMs, RedisValue[] messageIds, CommandFlags flags = CommandFlags.None)
        {
            throw new NotImplementedException();
        }

        public RedisValue[] StreamClaimIdsOnly(RedisKey key, RedisValue consumerGroup, RedisValue claimingConsumer, long minIdleTimeInMs, RedisValue[] messageIds, CommandFlags flags = CommandFlags.None)
        {
            throw new NotImplementedException();
        }

        public bool StreamConsumerGroupSetPosition(RedisKey key, RedisValue groupName, RedisValue position, CommandFlags flags = CommandFlags.None)
        {
            throw new NotImplementedException();
        }

        public StreamConsumerInfo[] StreamConsumerInfo(RedisKey key, RedisValue groupName, CommandFlags flags = CommandFlags.None)
        {
            throw new NotImplementedException();
        }

        public bool StreamCreateConsumerGroup(RedisKey key, RedisValue groupName, RedisValue? position = null, CommandFlags flags = CommandFlags.None)
        {
            throw new NotImplementedException();
        }

        public long StreamDelete(RedisKey key, RedisValue[] messageIds, CommandFlags flags = CommandFlags.None)
        {
            throw new NotImplementedException();
        }

        public long StreamDeleteConsumer(RedisKey key, RedisValue groupName, RedisValue consumerName, CommandFlags flags = CommandFlags.None)
        {
            throw new NotImplementedException();
        }

        public bool StreamDeleteConsumerGroup(RedisKey key, RedisValue groupName, CommandFlags flags = CommandFlags.None)
        {
            throw new NotImplementedException();
        }

        public StreamGroupInfo[] StreamGroupInfo(RedisKey key, CommandFlags flags = CommandFlags.None)
        {
            throw new NotImplementedException();
        }

        public StreamInfo StreamInfo(RedisKey key, CommandFlags flags = CommandFlags.None)
        {
            throw new NotImplementedException();
        }

        public long StreamLength(RedisKey key, CommandFlags flags = CommandFlags.None)
        {
            throw new NotImplementedException();
        }

        public StreamPendingInfo StreamPending(RedisKey key, RedisValue groupName, CommandFlags flags = CommandFlags.None)
        {
            throw new NotImplementedException();
        }

        public StreamPendingMessageInfo[] StreamPendingMessages(RedisKey key, RedisValue groupName, int count, RedisValue consumerName, RedisValue? minId = null, RedisValue? maxId = null, CommandFlags flags = CommandFlags.None)
        {
            throw new NotImplementedException();
        }

        public StreamEntry[] StreamRange(RedisKey key, RedisValue? minId = null, RedisValue? maxId = null, int? count = null, Order messageOrder = Order.Ascending, CommandFlags flags = CommandFlags.None)
        {
            throw new NotImplementedException();
        }

        public StreamEntry[] StreamRead(RedisKey key, RedisValue position, int? count = null, CommandFlags flags = CommandFlags.None)
        {
            throw new NotImplementedException();
        }

        public RedisStream[] StreamRead(StreamPosition[] streamPositions, int? countPerStream = null, CommandFlags flags = CommandFlags.None)
        {
            throw new NotImplementedException();
        }

        public StreamEntry[] StreamReadGroup(RedisKey key, RedisValue groupName, RedisValue consumerName, RedisValue? position = null, int? count = null, CommandFlags flags = CommandFlags.None)
        {
            throw new NotImplementedException();
        }

        public RedisStream[] StreamReadGroup(StreamPosition[] streamPositions, RedisValue groupName, RedisValue consumerName, int? countPerStream = null, CommandFlags flags = CommandFlags.None)
        {
            throw new NotImplementedException();
        }

        public long StreamTrim(RedisKey key, int maxLength, bool useApproximateMaxLength = false, CommandFlags flags = CommandFlags.None)
        {
            throw new NotImplementedException();
        }

        public Lease<byte> StringGetLease(RedisKey key, CommandFlags flags = CommandFlags.None)
        {
            throw new NotImplementedException();
        }

        public Task<Lease<byte>> HashGetLeaseAsync(RedisKey key, RedisValue hashField, CommandFlags flags = CommandFlags.None)
        {
            throw new NotImplementedException();
        }

        public Task<long> KeyExistsAsync(RedisKey[] keys, CommandFlags flags = CommandFlags.None)
        {
            throw new NotImplementedException();
        }

        public Task<TimeSpan?> KeyIdleTimeAsync(RedisKey key, CommandFlags flags = CommandFlags.None)
        {
            throw new NotImplementedException();
        }

        public Task<RedisValue[]> SetPopAsync(RedisKey key, long count, CommandFlags flags = CommandFlags.None)
        {
            throw new NotImplementedException();
        }

        public Task<RedisValue[]> SortedSetRangeByValueAsync(RedisKey key, RedisValue min = default(RedisValue), RedisValue max = default(RedisValue), Exclude exclude = Exclude.None, Order order = Order.Ascending, long skip = 0, long take = -1, CommandFlags flags = CommandFlags.None)
        {
            throw new NotImplementedException();
        }

        public Task<SortedSetEntry?> SortedSetPopAsync(RedisKey key, Order order = Order.Ascending, CommandFlags flags = CommandFlags.None)
        {
            throw new NotImplementedException();
        }

        public Task<SortedSetEntry[]> SortedSetPopAsync(RedisKey key, long count, Order order = Order.Ascending, CommandFlags flags = CommandFlags.None)
        {
            throw new NotImplementedException();
        }

        public Task<long> StreamAcknowledgeAsync(RedisKey key, RedisValue groupName, RedisValue messageId, CommandFlags flags = CommandFlags.None)
        {
            throw new NotImplementedException();
        }

        public Task<long> StreamAcknowledgeAsync(RedisKey key, RedisValue groupName, RedisValue[] messageIds, CommandFlags flags = CommandFlags.None)
        {
            throw new NotImplementedException();
        }

        public Task<RedisValue> StreamAddAsync(RedisKey key, RedisValue streamField, RedisValue streamValue, RedisValue? messageId = null, int? maxLength = null, bool useApproximateMaxLength = false, CommandFlags flags = CommandFlags.None)
        {
            throw new NotImplementedException();
        }

        public Task<RedisValue> StreamAddAsync(RedisKey key, NameValueEntry[] streamPairs, RedisValue? messageId = null, int? maxLength = null, bool useApproximateMaxLength = false, CommandFlags flags = CommandFlags.None)
        {
            throw new NotImplementedException();
        }

        public Task<StreamEntry[]> StreamClaimAsync(RedisKey key, RedisValue consumerGroup, RedisValue claimingConsumer, long minIdleTimeInMs, RedisValue[] messageIds, CommandFlags flags = CommandFlags.None)
        {
            throw new NotImplementedException();
        }

        public Task<RedisValue[]> StreamClaimIdsOnlyAsync(RedisKey key, RedisValue consumerGroup, RedisValue claimingConsumer, long minIdleTimeInMs, RedisValue[] messageIds, CommandFlags flags = CommandFlags.None)
        {
            throw new NotImplementedException();
        }

        public Task<bool> StreamConsumerGroupSetPositionAsync(RedisKey key, RedisValue groupName, RedisValue position, CommandFlags flags = CommandFlags.None)
        {
            throw new NotImplementedException();
        }

        public Task<StreamConsumerInfo[]> StreamConsumerInfoAsync(RedisKey key, RedisValue groupName, CommandFlags flags = CommandFlags.None)
        {
            throw new NotImplementedException();
        }

        public Task<bool> StreamCreateConsumerGroupAsync(RedisKey key, RedisValue groupName, RedisValue? position = null, CommandFlags flags = CommandFlags.None)
        {
            throw new NotImplementedException();
        }

        public Task<long> StreamDeleteAsync(RedisKey key, RedisValue[] messageIds, CommandFlags flags = CommandFlags.None)
        {
            throw new NotImplementedException();
        }

        public Task<long> StreamDeleteConsumerAsync(RedisKey key, RedisValue groupName, RedisValue consumerName, CommandFlags flags = CommandFlags.None)
        {
            throw new NotImplementedException();
        }

        public Task<bool> StreamDeleteConsumerGroupAsync(RedisKey key, RedisValue groupName, CommandFlags flags = CommandFlags.None)
        {
            throw new NotImplementedException();
        }

        public Task<StreamGroupInfo[]> StreamGroupInfoAsync(RedisKey key, CommandFlags flags = CommandFlags.None)
        {
            throw new NotImplementedException();
        }

        public Task<StreamInfo> StreamInfoAsync(RedisKey key, CommandFlags flags = CommandFlags.None)
        {
            throw new NotImplementedException();
        }

        public Task<long> StreamLengthAsync(RedisKey key, CommandFlags flags = CommandFlags.None)
        {
            throw new NotImplementedException();
        }

        public Task<StreamPendingInfo> StreamPendingAsync(RedisKey key, RedisValue groupName, CommandFlags flags = CommandFlags.None)
        {
            throw new NotImplementedException();
        }

        public Task<StreamPendingMessageInfo[]> StreamPendingMessagesAsync(RedisKey key, RedisValue groupName, int count, RedisValue consumerName, RedisValue? minId = null, RedisValue? maxId = null, CommandFlags flags = CommandFlags.None)
        {
            throw new NotImplementedException();
        }

        public Task<StreamEntry[]> StreamRangeAsync(RedisKey key, RedisValue? minId = null, RedisValue? maxId = null, int? count = null, Order messageOrder = Order.Ascending, CommandFlags flags = CommandFlags.None)
        {
            throw new NotImplementedException();
        }

        public Task<StreamEntry[]> StreamReadAsync(RedisKey key, RedisValue position, int? count = null, CommandFlags flags = CommandFlags.None)
        {
            throw new NotImplementedException();
        }

        public Task<RedisStream[]> StreamReadAsync(StreamPosition[] streamPositions, int? countPerStream = null, CommandFlags flags = CommandFlags.None)
        {
            throw new NotImplementedException();
        }

        public Task<StreamEntry[]> StreamReadGroupAsync(RedisKey key, RedisValue groupName, RedisValue consumerName, RedisValue? position = null, int? count = null, CommandFlags flags = CommandFlags.None)
        {
            throw new NotImplementedException();
        }

        public Task<RedisStream[]> StreamReadGroupAsync(StreamPosition[] streamPositions, RedisValue groupName, RedisValue consumerName, int? countPerStream = null, CommandFlags flags = CommandFlags.None)
        {
            throw new NotImplementedException();
        }

        public Task<long> StreamTrimAsync(RedisKey key, int maxLength, bool useApproximateMaxLength = false, CommandFlags flags = CommandFlags.None)
        {
            throw new NotImplementedException();
        }

        public Task<Lease<byte>> StringGetLeaseAsync(RedisKey key, CommandFlags flags = CommandFlags.None)
        {
            throw new NotImplementedException();
        }
        #endregion
    }
}
#endregion