﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ChannelVN.IMS2.Foundation.Data.Providers.SqlGenerator
{
    public class SqlQuery
    {
        public SqlQuery()
        {
            SqlBuilder = new StringBuilder();
        }

        public SqlQuery(object param): this()
        {
            Param = param;
        }

        public StringBuilder SqlBuilder { get; }

        public object Param { get; private set; }

        public string GetSql()
        {
            return SqlBuilder.ToString().Trim();
        }

        public void SetParam(object param)
        {
            Param = param;
        }
    }
}
