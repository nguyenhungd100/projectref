﻿using System;
using System.Collections;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Reflection;
using System.Text;

namespace ChannelVN.IMS2.Foundation.Data.Providers.SqlGenerator
{
    internal static class TypeExtensions
    {
        private static readonly ConcurrentDictionary<Type, PropertyInfo[]> _reflectionPropertyCache = new ConcurrentDictionary<Type, PropertyInfo[]>();

        public static PropertyInfo[] FindClassProperties(this Type objectType)
        {
            if (_reflectionPropertyCache.ContainsKey(objectType))
                return _reflectionPropertyCache[objectType];

            var result = objectType.GetProperties().ToArray();
            _reflectionPropertyCache.TryAdd(objectType, result);
            return result;
        }

        public static bool IsValueType(this Type type)
        {
            return type.IsValueType;
                
        }

        public static Dictionary<string, object> GetKeyValueAnnotation(object instance)
        {
            var result = new Dictionary<string, object>();
            var properties = instance.GetType().GetProperties();
            foreach (PropertyInfo property in properties)
            {
                var attribute = Attribute.GetCustomAttribute(property, typeof(KeyAttribute)) as KeyAttribute;

                if (attribute != null)
                {
                    object val = property.GetValue(instance);
                    result.Add(property.Name,val);
                }
            }
            return result;
            
        }
    }
}
