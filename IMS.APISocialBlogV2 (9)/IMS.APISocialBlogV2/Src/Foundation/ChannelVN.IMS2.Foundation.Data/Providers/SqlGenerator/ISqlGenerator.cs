﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Reflection;
using System.Text;

namespace ChannelVN.IMS2.Foundation.Data.Providers.SqlGenerator
{
    public interface ISqlGenerator<TEntity> where TEntity : class
    {
        /// <summary>
        /// All original properties
        /// </summary>
        PropertyInfo[] AllProperties { get; }

        SqlPropertyMetadata[] KeySqlProperties { get; }

        SqlPropertyMetadata[] SqlProperties { get; }


        string TableName { get; }

        string TableSchema { get; }



        /// <summary>
        /// Get Sql for Select by id Query
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        SqlQuery GetSelectById(object id);

        /// <summary>
        /// Get Sql for Insert Query
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        SqlQuery GetInsert(TEntity entity);

        /// <summary>
        /// Get Sql for Update Query
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        SqlQuery GetUpdate(TEntity entity);

        /// <summary>
        /// Get Sql for Delete Query
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        SqlQuery GetDelete(TEntity entity);
    }
}
