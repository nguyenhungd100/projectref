﻿using ChannelVN.IMS2.Foundation.Data.Providers.SqlGenerator;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Text;

namespace ChannelVN.IMS2.Foundation.Data.Providers.SqlGenerator
{
    public class SqlGenerator<TEntity> : ISqlGenerator<TEntity> where TEntity : class
    {

        public SqlGenerator()
        {
            InitProperties();
            InitConfig();
        }

        public PropertyInfo[] AllProperties { get; protected set; }

        public string TableName { get; protected set; }

        public string TableSchema { get; protected set; }

        public SqlPropertyMetadata[] KeySqlProperties { get; protected set; }

        public SqlPropertyMetadata[] SqlProperties { get; protected set; }

        private void InitProperties()
        {
            var entityType = typeof(TEntity);
            var entityTypeInfo = entityType.GetTypeInfo();
            var tableAttribute = entityTypeInfo.GetCustomAttribute<TableAttribute>();

            TableName = tableAttribute != null ? tableAttribute.Name : entityTypeInfo.Name;
            TableSchema = tableAttribute != null ? tableAttribute.Schema : string.Empty;
            AllProperties = entityType.FindClassProperties().Where(q => q.CanWrite).ToArray();

            var props = AllProperties.Where(ExpressionHelper.GetPrimitivePropertiesPredicate()).ToArray();
            SqlProperties = props.Where(p => !p.GetCustomAttributes<NotMappedAttribute>().Any()).Select(p => new SqlPropertyMetadata(p)).ToArray();
            KeySqlProperties = props.Where(p => p.GetCustomAttributes<KeyAttribute>().Any()).Select(p => new SqlPropertyMetadata(p)).ToArray();
        }

        private void InitConfig()
        {
            TableName = GetTableNameWithSchemaPrefix(TableName, TableSchema, "[", "]");
            foreach (var propertyMetadata in SqlProperties)
                propertyMetadata.ColumnName = "[" + propertyMetadata.ColumnName + "]";

            foreach (var propertyMetadata in KeySqlProperties)
                propertyMetadata.ColumnName = "[" + propertyMetadata.ColumnName + "]";
        }

        private static string GetTableNameWithSchemaPrefix(string tableName, string tableSchema, string startQuotationMark = "", string endQuotationMark = "")
        {
            return !string.IsNullOrEmpty(tableSchema)
                ? startQuotationMark + tableSchema + endQuotationMark + "." + startQuotationMark + tableName + endQuotationMark
                : startQuotationMark + tableName + endQuotationMark;
        }

        private SqlQuery InitBuilderSelect(bool firstOnly)
        {
            var query = new SqlQuery();
            query.SqlBuilder.Append("SELECT " + (firstOnly ? "TOP 1 " : "") + GetFieldsSelect(TableName, SqlProperties));
            return query;
        }

        private static string GetFieldsSelect(string tableName, IEnumerable<SqlPropertyMetadata> properties)
        {
            string ProjectionFunction(SqlPropertyMetadata p)
            {
                return !string.IsNullOrEmpty(p.Alias)
                    ? tableName + "." + p.ColumnName + " AS " + p.PropertyName
                    : tableName + "." + p.ColumnName;
            }

            return string.Join(", ", properties.Select(ProjectionFunction));
        }

        public SqlQuery GetSelectById(object id)
        {
            var sqlQuery = InitBuilderSelect(true);
            var keyProperty = KeySqlProperties[0];
            sqlQuery.SqlBuilder.Append(" FROM " + TableName + " ");
            IDictionary<string, object> dictionary = new Dictionary<string, object>
            {
                {keyProperty.PropertyName, id }
            };

            sqlQuery.SqlBuilder.Append("WHERE " + TableName + "." + keyProperty.ColumnName + " = @" + keyProperty.PropertyName + " ");
            sqlQuery.SetParam(dictionary);
            return sqlQuery;
        }

        public SqlQuery GetSelectAll(Expression<Func<TEntity, bool>> predicate)
        {
            return GetSelect(predicate, false);
        }

        private SqlQuery GetSelect(Expression<Func<TEntity, bool>> predicate, bool firstOnly)
        {
            var sqlQuery = InitBuilderSelect(firstOnly);
            sqlQuery.SqlBuilder.Append(" FROM " + TableName + " ");
            AppendWherePredicateQuery(sqlQuery, predicate);
            if (firstOnly)
            {
                sqlQuery.SqlBuilder.Append("LIMIT 1");
            }
            return sqlQuery;
        }

        private void AppendWherePredicateQuery(SqlQuery sqlQuery, Expression<Func<TEntity, bool>> predicate)
        {
            IDictionary<string, object> dictionaryParams = new Dictionary<string, object>();
            if (predicate != null)
            {
                var queryProperties = new List<QueryParameter>();

                //Todo:
            }
        }

        public SqlQuery GetInsert(TEntity entity)
        {
            //var key = KeySqlProperties[0];
            //var properties = SqlProperties.Where(p => !p.PropertyName.Equals(key.PropertyName));
            var query = new SqlQuery(entity);
            query.SqlBuilder.Append(
                "INSERT INTO " + TableName
                + " (" + string.Join(", ", SqlProperties.Select(p => p.ColumnName)) + ")"
                + " VALUES (" + string.Join(", ", SqlProperties.Select(p => "@" + p.PropertyName)) + ")");
            return query;
        }

        public SqlQuery GetUpdate(TEntity entity)
        {
            var properties = SqlProperties.Where(p => !KeySqlProperties.Any(k => k.PropertyName.Equals(p.PropertyName, StringComparison.OrdinalIgnoreCase)) && !p.IgnoreUpdate).ToArray();
            if (!properties.Any())
                throw new ArgumentException("Can't update without [Key]");
            var query = new SqlQuery(entity);
            query.SqlBuilder.Append("UPDATE " + TableName + " SET " + string.Join(", ", properties.Select(p => p.ColumnName + " = @" + p.PropertyName))
                + " WHERE " + string.Join(" AND ", KeySqlProperties.Where(p => !p.IgnoreUpdate).Select(p => p.ColumnName + " = @" + p.PropertyName)));
            query.SetParam(entity);
            return query;
        }

        public SqlQuery GetDelete(TEntity entity)
        {
            var sqlQuery = new SqlQuery();
            var whereSql = " WHERE " + string.Join(" AND ", KeySqlProperties.Select(p => TableName + "." + p.ColumnName + " = @" + p.PropertyName));
            sqlQuery.SqlBuilder.Append("DELETE FROM " + TableName + whereSql);
            sqlQuery.SetParam(entity);
            return sqlQuery;
        }
    }
}
