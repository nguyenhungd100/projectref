﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ChannelVN.IMS2.Foundation.Data.Providers.SqlGenerator
{
    internal class QueryParameter
    {
        internal QueryParameter(string linkingOperator, string propertyName, object propertyValue, string queryOperator)
        {
            LinkingOperator = linkingOperator;
            PropertyName = propertyName;
            PropertyValue = propertyValue;
            QueryOperator = queryOperator;
        }

        public string LinkingOperator { get; set; }

        public string PropertyName { get; set; }

        public object PropertyValue { get; set; }

        public string QueryOperator { get; set; }
    }
}
