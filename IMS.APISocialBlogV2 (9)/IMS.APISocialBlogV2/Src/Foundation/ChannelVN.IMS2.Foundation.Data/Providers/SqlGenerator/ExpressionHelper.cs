﻿using System;
using System.Collections.Generic;
using System.Reflection;
using System.Text;

namespace ChannelVN.IMS2.Foundation.Data.Providers.SqlGenerator
{
    public  static class ExpressionHelper
    {
        public static Func<PropertyInfo,bool> GetPrimitivePropertiesPredicate()
        {
            return p => p.CanWrite && (p.PropertyType.IsValueType() || p.PropertyType == typeof(string) || p.PropertyType == typeof(byte[]));
        }

    }
}
