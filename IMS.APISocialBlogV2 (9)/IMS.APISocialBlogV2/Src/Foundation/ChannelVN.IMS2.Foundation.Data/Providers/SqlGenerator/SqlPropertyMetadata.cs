﻿using ChannelVN.IMS2.Foundation.Data.Providers.SqlGenerator;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Reflection;
using System.Text;

namespace ChannelVN.IMS2.Foundation.Data.Providers.SqlGenerator
{
    public class SqlPropertyMetadata
    {
        public SqlPropertyMetadata(PropertyInfo propertyInfo)
        {
            PropertyInfo = propertyInfo;
            var alias = PropertyInfo.GetCustomAttribute<ColumnAttribute>();
            if (!string.IsNullOrEmpty(alias?.Name))
            {
                Alias = alias.Name;
                ColumnName = Alias;
            }
            else ColumnName = PropertyInfo.Name;

            var ignoreUpdate = PropertyInfo.GetCustomAttribute<IgnoreUpdateAttribute>();
            if (ignoreUpdate != null)
                IgnoreUpdate = true;
            

        }

        public PropertyInfo PropertyInfo { get; set; }

        public string Alias { get; set; }

        public string ColumnName { get; set; }

        public bool IgnoreUpdate { get; set; }

        public virtual string PropertyName => PropertyInfo.Name;


    }
}
