﻿using ChannelVN.IMS2.Foundation.Common;
using ChannelVN.IMS2.Foundation.Common.DbHelper;
using ChannelVN.IMS2.Foundation.Data.Providers.SqlClient;
using ChannelVN.IMS2.Foundation.Data.Providers.SqlGenerator;
using Dapper;
using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Dynamic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using System.Threading.Tasks.Dataflow;

namespace ChannelVN.IMS2.Foundation.Data.Providers
{
    public sealed class MssqlContext : Context
    {
        private IDbConnection _connection = null;
        private IDbTransaction _transaction = null;

        #region Constructor
        public MssqlContext()
            : this(new ContextOptions())
        {
        }

        public MssqlContext(ContextOptions options)
            : base(options)
        {
        }
        #endregion

        #region Properties
        #endregion

        #region Private methods
        /// <summary>
        /// For checking the type of the connection database.
        /// </summary>
        private bool IsAsync { get; }


        /// <summary>
        /// Initializes the database connection.
        /// </summary>
        private void GetConnection(long? id)
        {
            var connectionStr = string.Empty;
            if (id.HasValue)
                connectionStr = GetConnectionStrFromId(DatabaseType.MsSql, id.Value).connectionStr;
            else connectionStr = GetConnectionStrFromId(DatabaseType.MsSql).connectionStr;
            if (string.IsNullOrEmpty(connectionStr))
            {
                throw new ArgumentNullException("The connection string must be not empty.");
            }
            else
            {
                try
                {
                    _connection = new SqlConnection(connectionStr);
                    _connection.Open();
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
        }


        /// <summary>
        /// Initializes the database connection.
        /// </summary>
        private void GetConnection(Dictionary<string,object> infoDb)
        {
            var connectionStr = string.Empty;
             connectionStr = IdParts.SeparateInfoDb(infoDb).connectionStr;
            if (string.IsNullOrEmpty(connectionStr))
            {
                throw new ArgumentNullException("The connection string must be not empty.");
            }
            else
            {
                try
                {
                    _connection = new SqlConnection(connectionStr);
                    _connection.Open();
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
        }



        #endregion


        /// <summary>
        /// Creates <see cref="System.Data.IDataReader"/> for the specified DB command.
        /// </summary>
        /// <param name="command">The <see cref="System.Data.IDbCommand"/> object.</param>
        /// <returns>A reference to the <see cref="System.Data.IDataReader"/> object.</returns>
        private IDataReader ExecuteReader(IDbCommand command)
        {
            var res = command.ExecuteReader();
            res.Close();
            return res;
        }

        /// <summary>
        /// Map records from the DataReader
        /// </summary>
        /// <param name="command">The <see cref="System.Data.IDbCommand"/> command.</param>
        /// <returns>Entity of records.</returns>
        private T Get<T>(IDbCommand command)
        {
            T returnValue = default(T);
            using (IDataReader reader = ExecuteReader(command))
            {
                returnValue = MapRecord<T>(reader);
            }
            return returnValue;
        }

        /// <summary>
        /// Map records from the DataReader
        /// </summary>
        /// <param name="reader">The <see cref="System.Data.IDataReader"/> object.</param>
        /// <returns>Entity of records.</returns>
        private T MapRecord<T>(IDataReader reader)
        {
            return EntityMapper.FillObject<T>(reader);
        }

        /// <summary>
        /// Creates and returns a new <see cref="System.Data.IDbCommand"/> object.
        /// </summary>
        /// <param name="sqlText">The text of the query.</param>
        /// <returns>An <see cref="System.Data.IDbCommand"/> object.</returns>
        private IDbCommand CreateCommand(string sqlText)
        {
            return CreateCommand(sqlText, false);
        }

        /// <summary>
        /// Creates and returns a new <see cref="System.Data.IDbCommand"/> object.
        /// </summary>
        /// <param name="sqlText">The text of the query.</param>
        /// <param name="procedure">Specifies whether the sqlText parameter is 
        /// the name of a stored procedure.</param>
        /// <returns>An <see cref="System.Data.IDbCommand"/> object.</returns>
        private IDbCommand CreateCommand(string sqlText, bool procedure)
        {
            IDbCommand cmd = _connection.CreateCommand();
            cmd.CommandText = sqlText;
            cmd.Transaction = _transaction;
            if (procedure)
            {
                cmd.CommandType = CommandType.StoredProcedure;
            }
            return cmd;
        }

        /// <summary>
        /// Creates and returns a new <see cref="System.Data.IDbCommand"/> object.
        /// </summary>
        /// <param name="commandType"></param>
        /// <param name="commandText"></param>
        /// <param name="parameters"></param>
        /// <returns>An <see cref="System.Data.IDbCommand"/> object.</returns>
        private IDbCommand CreateCommand(CommandType commandType, string commandText, params SqlParameter[] parameters)
        {
            IDbCommand cmd = _connection.CreateCommand();
            cmd.CommandType = commandType;
            cmd.CommandText = commandText;
            cmd.Transaction = _transaction;
            foreach (var parameter in parameters)
            {
                AddParameter(cmd, parameter.ParameterName, parameter.Value, parameter.Direction);
            }
            return cmd;
        }

        /// <summary>
        /// Adds a new parameter to the specified command. It is not recommended that 
        /// you use this method directly from your custom code. Instead use the 
        /// <c>AddParameter</c> method of the CmsMainDbBase classes.
        /// </summary>
        /// <param name="cmd">The <see cref="System.Data.IDbCommand"/> object to add the parameter to.</param>
        /// <param name="paramName">The name of the parameter.</param>
        /// <param name="value">The value of the parameter.</param>
        /// <param name="paraDirection">The direction of the parameter.</param>
        /// <returns>A reference to the added parameter.</returns>
        private IDbDataParameter AddParameter(IDbCommand cmd, string paramName, object value, ParameterDirection paraDirection)
        {
            IDbDataParameter parameter = cmd.CreateParameter();
            parameter.ParameterName = CreateCollectionParameterName(paramName);
            if (value is DateTime)
            {
                parameter.Value = (DateTime.MinValue == Utility.ConvertToDateTime(value) ? DBNull.Value : value);
            }
            else
            {
                parameter.Value = (value ?? DBNull.Value);
            }
            parameter.Direction = paraDirection;
            cmd.Parameters.Add(parameter);
            return parameter;
        }

        /// <summary>
        /// Adds a new parameter to the specified command. It is not recommended that 
        /// you use this method directly from your custom code. Instead use the 
        /// <c>AddParameter</c> method of the CmsMainDbBase classes.
        /// </summary>
        /// <param name="cmd">The <see cref="System.Data.IDbCommand"/> object to add the parameter to.</param>
        /// <param name="paramName">The name of the parameter.</param>
        /// <param name="value">The value of the parameter.</param>
        /// <returns>A reference to the added parameter.</returns>
        private IDbDataParameter AddParameter(IDbCommand cmd, string paramName, object value)
        {
            IDbDataParameter parameter = cmd.CreateParameter();
            parameter.ParameterName = CreateCollectionParameterName(paramName);
            if (value is DateTime)
            {
                parameter.Value = (DateTime.MinValue == Utility.ConvertToDateTime(value) ? DBNull.Value : value);
            }
            else
            {
                parameter.Value = (value ?? DBNull.Value);
            }
            cmd.Parameters.Add(parameter);
            return parameter;
        }

        /// <summary>
        /// Creates a .Net data provider specific name that is used by the 
        /// <see cref="AddParameter"/> method.
        /// </summary>
        /// <param name="baseParamName">The base name of the parameter.</param>
        /// <returns>The full data provider specific parameter name.</returns>
        private string CreateCollectionParameterName(string baseParamName)
        {
            return "@" + baseParamName;

        }

        private class SqlGen<TEntity> : IDisposable where TEntity : class
        {
            public SqlGen()
            {
                SqlGenerator = new SqlGenerator<TEntity>();
            }
            public ISqlGenerator<TEntity> SqlGenerator { get; }

            public void Dispose()
            {
                GC.SuppressFinalize(SqlGenerator);
                GC.SuppressFinalize(this);
            }
        }

        public (IEnumerable<T> recordRes, long totalCount) FindAll<T>(string sqlCount, string sqlQuery) where T : class
        {
            var recordRes = new List<T>();
            long totalCount = 0;
            var result = (recordRes, totalCount);

            var buffer = new BufferBlock<Dictionary<string, object>>();
            foreach (var db in DbCollections)
            {
                buffer.Post(db);
            }

            Task<(IEnumerable<T>, long)> GetDataFromDb()
            {
                var dbInfo = buffer.Receive();
                GetConnection(dbInfo);

                var queryMultip = _connection.QueryMultiple(sqlCount + ";" + sqlQuery);

                var count = queryMultip.ReadFirst<long>();
                var entities = queryMultip.Read<T>();
                return Task.FromResult((entities, count));
            }

            var tasks = DbCollections.Select(o => GetDataFromDb()).ToList();
            foreach (var task in tasks)
            {
                if (task.IsCompletedSuccessfully)
                {
                    var re = task.Result.Item1;
                    result.recordRes.AddRange(task.Result.Item1);
                    result.totalCount += task.Result.Item2;
                }
            }
            return result;
        }

        public T FindById<T>(long id) where T : class
        {
            T returnValue = default(T);
            using (var sqlGen = new SqlGen<T>())
            {
                var key = sqlGen.SqlGenerator.KeySqlProperties.FirstOrDefault().PropertyName;
                var queryResult = sqlGen.SqlGenerator.GetSelectById(id);
                GetConnection(id);
                returnValue = _connection.Query<T>(new CommandDefinition(queryResult.GetSql(), new { key, id })).FirstOrDefault();
                //var cmd = _connection.ExecuteReader($"SELECT * FROM TestTable where Id = {id}");
            }
            return returnValue;
        }

      
        public bool Insert<T>(T instance, long id) where T : class
        {
            var result = false;
            using (var sqlGen = new SqlGen<T>())
            {
                var sqlQuery = sqlGen.SqlGenerator.GetInsert(instance);
                GetConnection(ChosenDbFromId(id, DatabaseType.MsSql));
                using (var cmd = this.CreateCommand(CommandType.Text, sqlQuery.GetSql()))
                {
                    var keyValues = Utility.ConvertToKeyValuePair(sqlQuery.Param);
                    foreach (var item in keyValues)
                    {
                        if (item.Key.Equals(sqlGen.SqlGenerator.KeySqlProperties[0].PropertyName))
                        {
                            AddParameter(cmd, item.Key, id);
                        }
                        else
                            AddParameter(cmd, item.Key, item.Value);
                    }
                    result = cmd.ExecuteNonQuery() > 0 ? true : false;
                }
            }
            return result;
        }
       
        public bool Update<T>(T instance, long id) where T : class
        {
            var result = false;
            using (var sqlGen = new SqlGen<T>())
            {
                var sqlQuery = sqlGen.SqlGenerator.GetUpdate(instance);
                GetConnection(id);

                using (var cmd = this.CreateCommand(CommandType.Text, sqlQuery.GetSql()))
                {
                    var keyValues = Utility.ConvertToKeyValuePair(sqlQuery.Param);
                    foreach (var item in keyValues)
                    {
                        AddParameter(cmd, item.Key, item.Value);
                    }
                    result = cmd.ExecuteNonQuery() > 0 ? true : false;
                }
            }
            return result;
        }

        public bool Delete<T>(T instance) where T : class
        {
            var result = false;
            using (var sqlGen = new SqlGen<T>())
            {
                var keyValue = TypeExtensions.GetKeyValueAnnotation(instance).FirstOrDefault();
                if (!keyValue.Equals(default(KeyValuePair<string, object>)))
                {
                    GetConnection(long.Parse(keyValue.Value.ToString()));
                    var sqlQuery = sqlGen.SqlGenerator.GetDelete(instance);

                    using (var cmd = this.CreateCommand(CommandType.Text, sqlQuery.GetSql(),
                    parameters: new SqlParameter[] { new SqlParameter(keyValue.Key, keyValue.Value) }))
                    {
                        try
                        {
                            result = cmd.ExecuteNonQuery() > 0 ? true : false;
                        }
                        catch (Exception ex)
                        {

                            throw;
                        }
                        
                    }
                }
            }
            return result;
        }

        public async Task<bool> BulkInsert<T>(IEnumerable<T> instance) where T : class
        {
            var tasks = new List<Task>();
            var random = AutoRandomConnection();
            GetConnection(random);
            var dbCode = IdParts.SeparateInfoDb(random);
            var ids = IdParts.AutogenIdWithCodeGen(instance.Count());
            var buffer = new BufferBlock<long>();
            foreach (var id in ids)
            {
                buffer.Post(id);
            }

            async Task<bool> AddAsync<T>(T entity) where T : class
            {
                using (var sqlGen = new SqlGen<T>())
                {
                    var sqlQuery = sqlGen.SqlGenerator.GetInsert(entity);
                    using (var cmd = this.CreateCommand(CommandType.Text, sqlQuery.GetSql()))
                    {
                        var keyValues = Utility.ConvertToKeyValuePair(sqlQuery.Param);
                        foreach (var item in keyValues)
                        {
                            if (item.Key.Equals(sqlGen.SqlGenerator.KeySqlProperties[0].PropertyName))
                            {
                                AddParameter(cmd, item.Key, await buffer.ReceiveAsync());
                            }
                            else
                                AddParameter(cmd, item.Key, item.Value);
                        }
                        return await ExecuteNonQueryAsync(cmd) > 0 ? true : false;
                    }
                }
            }
            Parallel.ForEach(instance, async item => tasks.Add(Task.FromResult(await AddAsync(item))));

            return await Task.FromResult(Task.WhenAll(tasks).IsCompletedSuccessfully);
        }

        #region IDisposable implementation

        /// <summary>
        /// Rolls back any pending transactions and closes the DB connection.
        /// </summary>
        public override void Dispose()
        {
            //Close();
        }
        #endregion

        //#region Public methods







        /// <summary>
        /// Returns a SQL statement parameter name that is specific for the data provider.
        /// For example it returns ? for OleDb provider, or @paramName for MS SQL provider.
        /// </summary>
        /// <param name="paramName">The data provider neutral SQL parameter name.</param>
        /// <returns>The SQL statement parameter name.</returns>
        private string CreateSqlParameterName(string paramName)
        {
            return "@" + paramName;
        }

        private Task<SqlDataReader> ExecuteReaderAsync(IDbCommand command)
        {
            return (command as SqlCommand)?.ExecuteReaderAsync();
        }

        /// <summary>
        /// Creates <see cref="System.Data.IDataReader"/> for the specified DB command.
        /// </summary>
        /// <param name="commandType"></param>
        /// <param name="commandText"></param>
        /// <param name="parameters"></param>
        /// <returns>A reference to the <see cref="System.Data.IDataReader"/> object.</returns>
        private IDataReader ExecuteReader(CommandType commandType, string commandText, params SqlParameter[] parameters)
        {
            var command = CreateCommand(commandType, commandText, parameters);
            return command.ExecuteReader();
        }

        private Task<SqlDataReader> ExecuteReaderAsync(CommandType commandType, string commandText, params SqlParameter[] parameters)
        {
            var command = CreateCommand(commandType, commandText, parameters) as SqlCommand;
            return command.ExecuteReaderAsync();
        }

        /// <summary>
        /// Creates <see cref="System.Data.IDataReader"/> for the specified DB command.
        /// </summary>
        /// <param name="command">The <see cref="System.Data.IDbCommand"/> object.</param>
        /// <returns>A reference to the <see cref="System.Data.IDataReader"/> object.</returns>
        private int ExecuteNonQuery(IDbCommand command)
        {
            return command.ExecuteNonQuery();
        }

        private Task<int> ExecuteNonQueryAsync(IDbCommand command)
        {
            return (command as SqlCommand)?.ExecuteNonQueryAsync();
        }

        /// <summary>
        /// Creates <see cref="System.Data.IDataReader"/> for the specified DB command.
        /// </summary>
        /// <param name="commandType"></param>
        /// <param name="commandText"></param>
        /// <param name="parameters"></param>
        /// <returns>A reference to the <see cref="System.Data.IDataReader"/> object.</returns>
        private int ExecuteNonQuery(CommandType commandType, string commandText, params SqlParameter[] parameters)
        {
            var command = CreateCommand(commandType, commandText, parameters);
            return command.ExecuteNonQuery();
        }

        private Task<int> ExecuteNonQueryAsync(CommandType commandType, string commandText, params SqlParameter[] parameters)
        {
            var command = CreateCommand(commandType, commandText, parameters) as SqlCommand;
            return command.ExecuteNonQueryAsync();
        }

        private async Task<T> GetAsync<T>(IDbCommand command)
        {
            T returnValue = default(T);
            using (DbDataReader reader = await ExecuteReaderAsync(command as SqlCommand))
            {
                returnValue = MapRecord<T>(reader);
            }
            return returnValue;
        }

        /// <summary>
        /// Map records from the DataReader
        /// </summary>
        /// <param name="reader">The <see cref="System.Data.IDataReader"/> object.</param>
        /// <returns>List entity of records.</returns>
        private List<T> MapRecords<T>(IDataReader reader)
        {
            return EntityMapper.FillCollection<T>(reader);
        }

        /// <summary>
        /// Map records from the DataReader
        /// </summary>
        /// <param name="command">The <see cref="System.Data.IDbCommand"/> command.</param>
        /// <returns>List entity of records.</returns>
        private List<T> GetList<T>(IDbCommand command)
        {
            List<T> returnValue = new List<T>();
            using (IDataReader reader = ExecuteReader(command))
            {
                returnValue = MapRecords<T>(reader);
            }
            return returnValue;
        }

        private async Task<List<T>> GetListAsync<T>(IDbCommand command)
        {
            List<T> returnValue = new List<T>();
            using (DbDataReader reader = await ExecuteReaderAsync(command as SqlCommand))
            {
                returnValue = MapRecords<T>(reader);
            }
            return returnValue;
        }

        /// <summary>
        /// Map records from the DataReader
        /// </summary>
        /// <param name="command">The <see cref="System.Data.IDbCommand"/> command.</param>
        /// <returns>List entity of records.</returns>
        private List<T> GetListGenericType<T>(IDbCommand command)
        {
            List<T> returnValue = new List<T>();
            using (IDataReader reader = ExecuteReader(command))
            {
                while (reader.Read())
                {
                    returnValue.Add((T)reader[0]);
                }
            }
            return returnValue;
        }

        private async Task<List<T>> GetListGenericTypeAsync<T>(IDbCommand command)
        {
            List<T> returnValue = new List<T>();
            using (DbDataReader reader = await ExecuteReaderAsync(command as SqlCommand))
            {
                while (await reader.ReadAsync())
                {
                    returnValue.Add((T)reader[0]);
                }
            }
            return returnValue;
        }

        private object GetFirstDataRecord(IDbCommand command)
        {
            object returnValue = null;
            using (IDataReader reader = ExecuteReader(command))
            {
                if (reader.Read())
                {
                    returnValue = reader[0];
                }
            }
            return returnValue;
        }

        private async Task<object> GetFirstDataRecordAsync(IDbCommand command)
        {
            object returnValue = null;
            using (DbDataReader reader = await ExecuteReaderAsync(command as SqlCommand))
            {
                if (await reader.ReadAsync())
                {
                    returnValue = reader[0];
                }
            }
            return returnValue;
        }

        /// <summary>
        /// Gets <see cref="System.Data.IDbConnection"/> associated with this object.
        /// </summary>
        /// <value>A reference to the <see cref="System.Data.IDbConnection"/> object.</value>
        private IDbConnection Connection
        {
            get { return _connection; }
        }

        /// <summary>
        /// Begins a new database transaction.
        /// </summary>
        /// <seealso cref="CommitTransaction"/>
        /// <seealso cref="RollbackTransaction"/>
        /// <returns>An object representing the new transaction.</returns>
        private IDbTransaction BeginTransaction()
        {
            CheckTransactionState(false);
            _transaction = _connection.BeginTransaction();
            return _transaction;
        }

        /// <summary>
        /// Begins a new database transaction with the specified 
        /// transaction isolation level.
        /// <seealso cref="CommitTransaction"/>
        /// <seealso cref="RollbackTransaction"/>
        /// </summary>
        /// <param name="isolationLevel">The transaction isolation level.</param>
        /// <returns>An object representing the new transaction.</returns>
        private IDbTransaction BeginTransaction(IsolationLevel isolationLevel)
        {
            CheckTransactionState(false);
            _transaction = _connection.BeginTransaction(isolationLevel);
            return _transaction;
        }

        /// <summary>
        /// Commits the current database transaction.
        /// <seealso cref="BeginTransaction"/>
        /// <seealso cref="RollbackTransaction"/>
        /// </summary>
        private void CommitTransaction()
        {
            CheckTransactionState(true);
            _transaction.Commit();
            _transaction = null;
        }

        /// <summary>
        /// Rolls back the current transaction from a pending state.
        /// <seealso cref="BeginTransaction"/>
        /// <seealso cref="CommitTransaction"/>
        /// </summary>
        private void RollbackTransaction()
        {
            CheckTransactionState(true);
            _transaction.Rollback();
            _transaction = null;
        }

        /// <summary>
        /// Checks the state of the current transaction
        /// </summary>
        /// <param name="mustBeOpen"></param>
        private void CheckTransactionState(bool mustBeOpen)
        {
            if (mustBeOpen)
            {
                if (null == _transaction)
                    throw new InvalidOperationException("Transaction is not open.");
            }
            else
            {
                if (null != _transaction)
                    throw new InvalidOperationException("Transaction is already open.");
            }
        }

        private object GetParameterValueFromCommand(IDbCommand command, int paramterIndex)
        {
            var parameter = command.Parameters[paramterIndex] as SqlParameter;
            return parameter?.Value;
        }

        /// <summary>
        /// Rolls back any pending transactions and closes the DB connection.
        /// An application can call the <c>Close</c> method more than
        /// one time without generating an exception.
        /// </summary>
        private void Close()
        {
            if (_connection != null && _connection.State != ConnectionState.Closed)
            {
                _connection.Close();
                _connection = null;
            }
        }


        public void InsertDataUsingSqlBulkCopy<T>(IEnumerable<T> entities, string tableName)
        {
            using (var bulk = new SqlBulkCopy(""))
            {
                using (var dt = DataTableHelper.ConvertToDataTable(entities))
                {
                    dt.Columns.RemoveAt(0);
                    bulk.DestinationTableName = tableName;
                    bulk.ColumnMappings.Add("Name", "Name");
                    bulk.BulkCopyTimeout = 360;
                    bulk.WriteToServer(dt);
                }
            }
        }

        //public void InsertDataUsingSqlBulkCopy<T>(IEnumerable<T> entities) where T : class
        //{
        //    var dbCode = AutoRandomConnection();

        //    using (var bulk = new SqlBulkCopy(_connection.ConnectionString))
        //    {
        //        using (var dt = DataTableHelper.ConvertToDataTable(entities))
        //        {
        //            try
        //            {
        //                dt.Columns[0].ColumnName = IdParts.AutogenIdWithCodeGen(dbCode);
        //                bulk.DestinationTableName = "TestTable";
        //                bulk.ColumnMappings.Add("Id", "Id");
        //                bulk.ColumnMappings.Add("Name", "Name");
        //                bulk.BulkCopyTimeout = 360;
        //                bulk.WriteToServer(dt);
        //            }
        //            catch (Exception ex)
        //            {

        //                throw;
        //            }

        //        }
        //    }
    }
}
