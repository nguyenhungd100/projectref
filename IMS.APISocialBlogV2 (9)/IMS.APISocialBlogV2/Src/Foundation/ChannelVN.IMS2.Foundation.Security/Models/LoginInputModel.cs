﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ChannelVN.IMS2.Foundation.Security.Models
{
    public class LoginInputModel
    {
        public string Username { get; set; }
        public string Password { get; set; }
    }
}
