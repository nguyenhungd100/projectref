﻿using Microsoft.IdentityModel.Tokens;
using System.Text;

namespace ChannelVN.IMS2.Foundation.Security.Bearer.Helpers
{
    public static class JwtSecurityKey
    {
        public static SymmetricSecurityKey Create(string secret)
        {
            return new SymmetricSecurityKey(Encoding.ASCII.GetBytes(secret));
        }
    }
}
