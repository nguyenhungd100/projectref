﻿using System.Collections.Generic;

namespace ChannelVN.IMS2.Foundation.Common
{
    public class PagingDataResult<T>
    {
        public int Total { get; set; }
        public List<T> Data { get; set; }
    }
}
