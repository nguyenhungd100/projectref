﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Reflection;
using System.Text.RegularExpressions;

namespace ChannelVN.IMS2.Foundation.Common
{
    public static class Utility
    {
        const string UniChars = "àáảãạâầấẩẫậăằắẳẵặèéẻẽẹêềếểễệđìíỉĩịòóỏõọôồốổỗộơờớởỡợùúủũụưừứửữựỳýỷỹỵÀÁẢÃẠÂẦẤẨẪẬĂẰẮẲẴẶÈÉẺẼẸÊỀẾỂỄỆĐÌÍỈĨỊÒÓỎÕỌÔỒỐỔỖỘƠỜỚỞỠỢÙÚỦŨỤƯỪỨỬỮỰỲÝỶỸỴÂĂĐÔƠƯ";
        const string UnsignedChars = "aaaaaaaaaaaaaaaaaeeeeeeeeeeediiiiiooooooooooooooooouuuuuuuuuuuyyyyyAAAAAAAAAAAAAAAAAEEEEEEEEEEEDIIIIIOOOOOOOOOOOOOOOOOUUUUUUUUUUUYYYYYAADOOU";

        public static string UnicodeToUnsignedAndDash(string s)
        {
            const string strChar = "abcdefghijklmnopqrstxyzuvxw0123456789/- ";
            s = UnicodeToUnsigned(s.ToLower().Trim());
            var sReturn = "";
            for (int i = 0; i < s.Length; i++)
            {
                if (strChar.IndexOf(s[i]) > -1)
                {
                    if (s[i] != ' ')
                        sReturn += s[i];
                    else if (i > 0 && s[i - 1] != ' ' && s[i - 1] != '-')
                        sReturn += "-";
                }
            }
            return sReturn.Replace("--", "-").Replace("/", "-");
        }

        public static string UnicodeToUnsigned(string s)
        {
            var retVal = "";
            for (int i = 0; i < s.Length; i++)
            {
                var pos = UniChars.IndexOf(s[i].ToString());
                if (pos >= 0)
                    retVal += UnsignedChars[pos];
                else
                    retVal += s[i];
            }
            return retVal;
        }

        public static bool PropertyExists(dynamic obj, string name)
        {
            if (obj == null) return false;
            if (obj is IDictionary<string, Newtonsoft.Json.Linq.JToken> jdict)
            {
                return jdict.ContainsKey(name);
            }
            if (obj is IDictionary<string, object> odict)
            {
                return odict.ContainsKey(name);
            }
            return obj.GetType().GetProperty(name) != null;
        }

        public static bool IsValidEmail(string email)
        {
            if (string.IsNullOrEmpty(email))
            {
                return false;
            }
            const string pattern = "^([0-9a-zA-Z]([-.\\w]*[0-9a-zA-Z])*@([0-9a-zA-Z][-\\w]*[0-9a-zA-Z]\\.)+[a-zA-Z]{2,9})$";
            return Regex.IsMatch(email, pattern);
        }

        public static string RemoveStrHtmlTags(string inputString)
        {
            var input = inputString.Trim();
            if (input != "")
            {
                input = Regex.Replace(input, @"<(.|\n)*?>", string.Empty);
            }
            return input;
        }

        public static string ReplaceSpecialCharater(string inputString)
        {
            inputString = inputString.Trim();
            inputString = inputString.Replace(@"\", @"\\");
            inputString = inputString.Replace("\"", "&quot;");
            inputString = inputString.Replace("“", "&ldquo;");
            inputString = inputString.Replace("”", "&rdquo;");
            inputString = inputString.Replace("‘", "&lsquo;");
            inputString = inputString.Replace("’", "&rsquo;");
            inputString = inputString.Replace("'", "&#39;");
            return inputString;
        }

        public static string WrapString(string inputString, int length)
        {
            var str = string.Empty;
            var startIndex = 0;
            for (var i = (int)Math.Ceiling(inputString.Length / ((double)length)); (startIndex < inputString.Length) && (i > 1); i--)
            {
                str = str + inputString.Substring(startIndex, length) + ' ';
                startIndex += length;
            }
            return (str + inputString.Substring(startIndex));
        }

        public static double DateDiff(DateTime dateFrom, DateTime dateTo, string instant)
        {
            var diff = dateTo - dateFrom;
            double result = 0;
            switch (instant.ToLower())
            {
                case "d":
                    result = diff.TotalDays;
                    break;
                case "h":
                    result = diff.TotalHours;
                    break;
                case "m":
                    result = diff.TotalMinutes;
                    break;
                case "s":
                    result = diff.TotalSeconds;
                    break;
            }
            return result;
        }

        public static int CountWords(string stringInput)
        {
            if (string.IsNullOrEmpty(stringInput)) return 0;
            // Replaces all HTML tags
            stringInput = RemoveStrHtmlTags(stringInput);
            // Counts all words
            var collection = Regex.Matches(stringInput, @"[\S]+");
            // Returns number words.
            return collection.Count;
        }

        #region Convert Data

        public static string ConvertToString(object value)
        {
            return null == value ? string.Empty : value.ToString();
        }

        public static int ConvertToInt(object value)
        {
            int returnValue;
            if (null == value || !int.TryParse(value.ToString(), out returnValue))
            {
                returnValue = 0;
            }
            return returnValue;
        }

        public static long ConvertToLong(object value)
        {
            long returnValue;
            if (null == value || !long.TryParse(value.ToString(), out returnValue))
            {
                returnValue = 0;
            }
            return returnValue;
        }

        public static short ConvertToShort(object value)
        {
            short returnValue;
            if (null == value || !short.TryParse(value.ToString(), out returnValue))
            {
                returnValue = 0;
            }
            return returnValue;
        }

        public static decimal ConvertToDecimal(object value)
        {
            decimal returnValue;
            if (null == value || !decimal.TryParse(value.ToString(), out returnValue))
            {
                returnValue = 0;
            }
            return returnValue;
        }

        public static double ConvertToDouble(object value)
        {
            double returnValue;
            if (null == value || !double.TryParse(value.ToString(), out returnValue))
            {
                returnValue = 0;
            }
            return returnValue;
        }

        public static DateTime ConvertToDateTime(object value)
        {
            DateTime returnValue;

            if (null == value || !DateTime.TryParse(value.ToString(), out returnValue))
            {
                returnValue = DateTime.MinValue;
            }
            return returnValue;
        }

        public static DateTime ConvertToDateTimeDefaultDateNow(object value)
        {
            DateTime returnValue;

            if (null == value || !DateTime.TryParse(value.ToString(), out returnValue))
            {
                returnValue = DateTime.Now;
            }
            return returnValue;
        }

        public static TimeSpan ConvertToTimeSpan(object value)
        {
            TimeSpan timeSpan;
            if (null == value || !TimeSpan.TryParse(value.ToString(), out timeSpan))
            {
                timeSpan = TimeSpan.MinValue;
            }
            return timeSpan;

        }

        public static long ConvertToTimestamp(DateTime dateTime)
        {
            var timeSpan = dateTime - new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
            return (long)timeSpan.TotalMilliseconds;
        }

        public static long ConvertToTimestamp10(this DateTime dateTime)
        {
            var timeSpan = (dateTime - new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc));
            return (long)timeSpan.TotalSeconds;
        }

        public static bool ConvertToBoolean(object value)
        {
            bool returnValue;
            if (null == value || !bool.TryParse(value.ToString(), out returnValue))
            {
                returnValue = false;
            }
            return returnValue;
        }

        public static byte ConvertToByte(object value)
        {
            byte returnValue;
            if (null == value || !byte.TryParse(value.ToString(), out returnValue))
            {
                returnValue = 0;
            }
            return returnValue;
        }

        public static double DateTimeToUnixTimestamp(DateTime dateTime, long precision = TimeSpan.TicksPerSecond)
        {
            var unixEpoch = new DateTime(1970, 1, 1, 0, 0, 0, 0, DateTimeKind.Utc);
            var unixTimestampInTicks = (dateTime.ToUniversalTime() - unixEpoch).Ticks;
            return (double)unixTimestampInTicks / precision;
        }

        public static DateTime UnixTimestampToDateTime(double unixTime, long precision = TimeSpan.TicksPerSecond)
        {
            var unixEpoch = new DateTime(1970, 1, 1, 0, 0, 0, 0, DateTimeKind.Utc);
            var unixTimestampInTicks = (long)(unixTime * precision);
            return new DateTime(unixEpoch.Ticks + unixTimestampInTicks, DateTimeKind.Utc);
        }

        public static string Base64Decode(string encodedValue)
        {
            var encodedBytes = FromBase64String(encodedValue);
            return System.Text.Encoding.UTF8.GetString(encodedBytes);
        }

        private static byte[] FromBase64String(string input)
        {
            string working = input.Replace('-', '+').Replace('_', '/');
            while (working.Length % 3 != 0)
            {
                working += '=';
            }
            try
            {
                return Convert.FromBase64String(working);
            }
            catch
            {
                // .Net core 2.1 bug
                // https://github.com/dotnet/corefx/issues/30793
                try
                {
                    return Convert.FromBase64String(input.Replace('-', '+').Replace('_', '/'));
                }
                catch { }
                try
                {
                    return Convert.FromBase64String(input.Replace('-', '+').Replace('_', '/') + "=");
                }
                catch { }
                try
                {
                    return Convert.FromBase64String(input.Replace('-', '+').Replace('_', '/') + "==");
                }
                catch { }

                return null;
            }
        }

        public static IEnumerable<DateTime> EachDay(DateTime from, DateTime thru)
        {
            for (var day = from.Date; day.Date <= thru.Date; day = day.AddDays(1))
                yield return day;
        }

        public static DateTime? ConvertSecondsToDate(double? seconds)
        {
            if (seconds == null||seconds==0) return null;
            DateTime start = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Unspecified);
            DateTime date =DateTime.SpecifyKind(start.AddSeconds(seconds??0),DateTimeKind.Unspecified);
            return date;
        }

        public static bool ValidateStringIds(string strIds)
        {
            var returnValue = true;
            if (string.IsNullOrEmpty(strIds)) return returnValue;
            try
            {
                var arrId = strIds.Split(",");
                foreach(var id in arrId)
                {
                    if(!long.TryParse(id,out long result))
                    {
                        returnValue = false;
                    }
                }
            }catch(Exception) {
                returnValue = false;
            }
            return returnValue;
        }
        #endregion

        #region Object to KeyValuePair
        public static List<KeyValuePair<string, string>> ConvertToKeyValuePair(this object me)
        {
            List<KeyValuePair<string, string>> result = new List<KeyValuePair<string, string>>();
            try
            {
                foreach (var property in me.GetType().GetProperties())
                {
                    result.Add(new KeyValuePair<string, string>(property.Name, property.GetValue(me)?.ToString()));
                }
            }
            catch (Exception ex) {}
            return result;
        }
        #endregion
        
        #region Clonable

        public static object CloneObject(object objSource)
        {
            //Get the type of source object and create a new instance of that type
            var typeSource = objSource.GetType();
            var objTarget = Activator.CreateInstance(typeSource);

            //Get all the properties of source object type
            var propertyInfo = typeSource.GetProperties(BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance);

            //Assign all source property to taget object 's properties
            foreach (PropertyInfo property in propertyInfo)
            {
                //Check whether property can be written to
                if (property.CanWrite)
                {
                    //Check whether property type is value type, enum or string type
                    if (property.PropertyType.IsValueType || property.PropertyType.IsEnum || property.PropertyType.Equals(typeof(System.String)))
                    {
                        property.SetValue(objTarget, property.GetValue(objSource, null), null);
                    }
                    //else property type is object/complex types, so need to recursively call this method until the end of the tree is reached
                    else
                    {
                        var objPropertyValue = property.GetValue(objSource, null);
                        if (objPropertyValue == null)
                        {
                            property.SetValue(objTarget, null, null);
                        }
                        else
                        {
                            property.SetValue(objTarget, CloneObject(objPropertyValue), null);
                        }
                    }
                }
            }
            return objTarget;
        }
        #endregion

        public static IEnumerable<TValue> RandomValues<TKey, TValue>(IDictionary<TKey, TValue> dict)
        {
            Random rand = new Random();
            List<TValue> values = Enumerable.ToList(dict.Values);
            int size = dict.Count;
            while (true)
            {
                yield return values[rand.Next(size)];
            }
        }
    }
}
