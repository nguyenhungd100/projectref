﻿using System.Collections.Generic;

namespace ChannelVN.IMS2.Foundation.Common.Configuration
{
    public sealed class ServiceConfiguration
    {
        public IDictionary<string, ClientRegistry> ClientRegistry { get; set; }

        public string Namespace { get; set; }
        public string Passcode { get; set; }
        public int TokenExpiresIn { get; set; }
    }

    public sealed class ClientRegistry
    {
        /// <summary>
        /// Namespace
        /// Eg. thandong, tamtay, ...
        /// </summary>
        public string Issuer { get; set; }
        /// <summary>
        /// Username
        /// Eg. admin, test, vanlt, ...
        /// </summary>
        public string Subject { get; set; }
        /// <summary>
        /// Three partners or consumers
        /// Eg.  
        /// </summary>
        public string Audience { get; set; }
        /// <summary>
        /// Expire time (in minutes)
        /// Eg. 120 => 2 minutes
        /// </summary>
        public int Expiry { get; set; }
        /// <summary>
        /// Identity provider
        /// Eg. JCHome, linhvanlung.vn
        /// </summary>
        public string IdentityProvider { get; set; }
        /// <summary>
        /// User Authorization
        /// Eg. 
        /// roles: ["Admin","Supervisor"], 
        /// permissions: ["1","2","3"]
        /// ==> "aut": ["roles:Admin,Supervisor","permissions:1,2,3"]
        /// </summary>
        public IDictionary<string, string[]> Claims { get; set; }

        /// <summary>
        /// Constructor
        /// </summary>
        public ClientRegistry()
        {
            Audience = "Any";
            Claims = new Dictionary<string, string[]>();
        }
    }
}
