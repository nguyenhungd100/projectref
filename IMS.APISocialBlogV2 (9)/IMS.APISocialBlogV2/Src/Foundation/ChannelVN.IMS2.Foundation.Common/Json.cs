﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using Newtonsoft.Json.Linq;
using System;

namespace ChannelVN.IMS2.Foundation.Common
{
    public static class Json
    {
        public static string Stringify(object @object, string dateTimeFormat)
        {
            try
            {
                return JsonConvert.SerializeObject(@object, new IsoDateTimeConverter { DateTimeFormat = dateTimeFormat });
            }
            catch
            {
                return string.Empty;
            }
        }

        public static string Stringify(object @object)
        {
            try
            {
                return JsonConvert.SerializeObject(@object, MicrosoftDateFormatSettings);
            }
            catch
            {
                return string.Empty;
            }
        }

        public static T Parse<T>(string jsonString)
        {
            try
            {
                return JsonConvert.DeserializeObject<T>(jsonString, MicrosoftDateFormatSettings);
            }
            catch(Exception ex)
            {
                return default(T);
            }
        }

        public static T Parse<T>(string jsonString, string dateTimeFormat)
        {
            try
            {
                return JsonConvert.DeserializeObject<T>(jsonString, new IsoDateTimeConverter { DateTimeFormat = dateTimeFormat });
            }
            catch
            {
                return default(T);
            }
        }

        private static readonly JsonSerializerSettings MicrosoftDateFormatSettings = new JsonSerializerSettings
        {
            DateFormatHandling = DateFormatHandling.MicrosoftDateFormat
        };

        public static dynamic ConvertToObject(string content)
        {
            try
            {
                return JObject.Parse(content);
            }
            catch
            {
                return new JObject();
            }
        }

        public static dynamic ConvertToObject(object content)
        {
            try
            {
                return JObject.FromObject(content);
            }
            catch
            {
                return new JObject();
            }
        }

        public static dynamic ConvertToArray(string content)
        {
            try
            {
                return JArray.Parse(content);
            }
            catch
            {
                return new JArray();
            }
        }

        public static dynamic ConvertToArray(object content)
        {
            try
            {
                return JArray.FromObject(content);
            }
            catch
            {
                return new JArray();
            }
        }

        public static bool HasProperty(dynamic @object, string propertyName)
        {
            return (@object as JObject).ContainsKey(propertyName);
        }
    }
}
