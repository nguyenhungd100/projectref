﻿using ChannelVN.IMS2.Foundation.Common.Configuration;
using IdGen;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ChannelVN.IMS2.Foundation.Common
{
    public class IdPartModel
    {
        public string Bits { get; set; }
        public TimeSpan Times { get; set; }
        public short ShardId { get; set; }

        public int ShardIdSql { get; set; }

        public int ShardIdCache { get; set; }

        public int ShardIdEs { get; set; }

        public long Sequence { get; set; }
        public DateTime Date { get; set; }
    }


    public static class IdParts
    {
        private static DateTime epoch = new DateTime(2019, 1, 1);
        private static MaskConfig mark = new MaskConfig(41, 9, 13);
        private static Dictionary<int, IdGenerator> _generators = new Dictionary<int, IdGenerator>();
        public static long AutogenIdWithCodeGen()
        {
            var generatorId = CombineShardId().shardId;
            if (!_generators.ContainsKey(generatorId))
            {
                var generator = new IdGenerator(generatorId, epoch, mark);
                _generators.Add(generatorId, generator);
            }
            return _generators[generatorId].CreateId();
        }


        public static IEnumerable<long> AutogenIdWithCodeGen(int count)
        {
            var epoch = new DateTime(2029, 1, 1);
            var mark = new MaskConfig(41, 4, 18);
            var generatorId = CombineShardId().shardId;
            var generators = (IEnumerable)new IdGenerator(generatorId, epoch, mark).GetEnumerator();
            var ids = generators.OfType<long>().Take(count).ToArray();
            return ids;
        }

        public static IdPartModel DecomposeIdPartWithCodeGen(long id)
        {
            var result = new IdPartModel();
            try
            {
                var generator = new IdGenerator(1);
                var epoc = new DateTime(2019, 1, 1);

                var bytes = BitConverter.GetBytes(id).Reverse();
                var bits = string.Concat(bytes.Select(c => Convert.ToString((byte)c, 2).PadLeft(8, '0')));
                var timespan = TimeSpan.FromMilliseconds(Convert.ToInt64(bits.Substring(0, 42), 2));


                result.Bits = bits;
                result.ShardId = Convert.ToInt16(bits.Substring(42, 9), 2);
                result.Sequence = Convert.ToInt64(bits.Substring(50, 13), 2);
                result.Times = timespan;
                result.Date = new DateTime(timespan.Ticks + epoc.Ticks);

                result.ShardIdSql = SeperateShardId(result.ShardId).shardIdSql;
                result.ShardIdEs = SeperateShardId(result.ShardId).shardIdEs;
                result.ShardIdCache = SeperateShardId(result.ShardId).shardIdCache;
            }
            catch (Exception ex)
            {
                throw;
            }
            return result;
        }

        public static (int shardId, Dictionary<string, object> autoSql,
            Dictionary<string, object> autoCache,
            Dictionary<string, object> autoSearch)
            CombineShardId()
        {
            var rd = new Random();
            var cmsMainDb = AppSettings.Current.ChannelConfiguration.ConnectionStrings.CmsMainDb;
            var autoSql = cmsMainDb.ElementAt(index: rd.Next(cmsMainDb.Count() - 1));
            var shardIdSql = SeparateInfoDb(autoSql).dbCode;

            var cmsCacheDb = AppSettings.Current.ChannelConfiguration.ConnectionStrings.CmsMainCacheDb;
            var autoCache = cmsCacheDb.ElementAt(index: rd.Next(cmsCacheDb.Count() - 1));
            var shardIdCache = SeparateInfoDb(autoCache).dbCode;

            var cmsSearchDb = AppSettings.Current.ChannelConfiguration.ConnectionStrings.CmsMainCacheDb;
            var autoSearch = cmsSearchDb.ElementAt(index: rd.Next(cmsSearchDb.Count() - 1));
            var shardIdSearch = SeparateInfoDb(autoSearch).dbCode;


            var bitSql = Convert.ToString(shardIdSql, 2).PadLeft(3, '0'); ;
            var bitCache = Convert.ToString(shardIdCache, 2).PadLeft(3, '0');
            var bitEs = Convert.ToString(shardIdSearch, 2).PadLeft(3, '0');
            var strBuilder = new StringBuilder();

            strBuilder.Append(bitSql);
            strBuilder.Append(bitCache);
            strBuilder.Append(bitEs);

            return (Convert.ToInt32(strBuilder.ToString(), 2), autoSql, autoCache, autoSearch);
        }

        public static (int shardIdSql, int shardIdCache, int shardIdEs) SeperateShardId(int input)
        {
            var bits = Convert.ToString(input, 2).PadLeft(9, '0');
            var bitSql = bits.Substring(0, 3);
            var bitCache = bits.Substring(3, 3);
            var bitEs = bits.Substring(6, 3);
            return (Convert.ToInt32(bitSql.ToString(), 2), Convert.ToInt32(bitCache.ToString(), 2), Convert.ToInt32(bitEs.ToString(), 2));
        }

        public static (string connectionStr, int dbCode) SeparateInfoDb(Dictionary<string, object> infoDb)
        {
            if (infoDb != null)
            {
                return (infoDb.FirstOrDefault(c => c.Key == "ConnectionString").Value.ToString(),
                    int.Parse(infoDb.FirstOrDefault(c => c.Key == "DbCode").Value.ToString()));
            }
            else return (null, 0);
        }



        public static IdPartModel DecomposeIdPartWithDbGen(long id)
        {
            try
            {
                var epocNew = new DateTime(2019, 1, 1);

                var bytes = BitConverter.GetBytes(id).Reverse();
                var bits = string.Concat(bytes.Select(c => Convert.ToString((byte)c, 2).PadLeft(8, '0')));

                var timespan = TimeSpan.FromSeconds(Convert.ToInt64(bits.Substring(0, 41), 2));

                var totalMiliSeconds = (long)timespan.TotalSeconds * 1000 + (epocNew.Ticks / 10000);

                var da = new DateTime(totalMiliSeconds * 10000);

                return new IdPartModel
                {
                    Bits = bits,
                    //ShardId = Convert.ToInt16(bits.Substring(41, 4), 2),
                    Sequence = Convert.ToInt64(bits.Substring(45, 19), 2),
                    Times = timespan,
                    Date = new DateTime(totalMiliSeconds * 10000)
                };
            }
            catch (Exception)
            {
                throw;
            }
        }

        //public static IdPartModel DecomposeIdPartInsta(long id)
        //{
        //    try
        //    {
        //        var epocNew = new DateTime(2019, 1, 1);

        //        var bytes = BitConverter.GetBytes(id).Reverse();
        //        var bits = string.Concat(bytes.Select(c => Convert.ToString((byte)c, 2).PadLeft(8, '0')));

        //        var timespan = TimeSpan.FromSeconds(Convert.ToInt64(bits.Substring(0, 41), 2));

        //        var totalMiliSeconds = (long)timespan.TotalSeconds * 1000 + (epocNew.Ticks / 10000);

        //        var da = new DateTime(totalMiliSeconds * 10000);

        //        return new IdPartModel
        //        {
        //            Bits = bits,
        //            ShardId = Convert.ToInt16(bits.Substring(41, 13), 2),
        //            Sequence = Convert.ToInt64(bits.Substring(54, 10), 2),
        //            Times = timespan,
        //            Date = new DateTime(totalMiliSeconds * 10000)
        //        };
        //    }
        //    catch (Exception)
        //    {

        //        throw;
        //    }
        //}



        //public static int CombileGeneratorId(/*IEnumerable<IEnumerable<Dictionary<string, object>>> dbCollectors*/)
        //{
        //    var cmsMainDb = AppSettings.Current.ChannelConfiguration.ConnectionStrings.CmsMainDb;
        //    var cmsCache = AppSettings.Current.ChannelConfiguration.ConnectionStrings.CmsMainCacheDb;
        //    var cmsSearch = AppSettings.Current.ChannelConfiguration.ConnectionStrings.cms;
        //    var generatorId = connectionStrings[new Random().Next(connectionStrings.Length)].ShardId;
        //    var autoSql = 1;
        //    var autoEs = 2;
        //    var autoCache = 7;
        //    var autoOther = 5;

        //    var shardIdSql = Convert.ToString(autoSql, 2).PadLeft(3, '0');
        //    var shardIdCache = Convert.ToString(autoCache, 2).PadLeft(3, '0');
        //    var shardIdEs = Convert.ToString(autoEs, 2).PadLeft(3, '0');
        //    var shardOther = Convert.ToString(autoOther, 2).PadLeft(3, '0');
        //    var shardIdTotal = new StringBuilder();

        //    shardIdTotal.Append(shardIdSql);
        //    shardIdTotal.Append(shardIdCache);
        //    shardIdTotal.Append(shardIdEs);
        //    shardIdTotal.Append(shardOther);
        //    return Convert.ToInt32(shardIdTotal.ToString(), 2); 
        //}

    }
}
