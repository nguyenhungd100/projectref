﻿using ChannelVN.IMS2.Foundation.Common.Configuration;
using ChannelVN.IMS2.Foundation.Common.DbHelper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ChannelVN.IMS2.Foundation.Common
{
    public static class SwitchDbHelper
    {
        public static DbCode GetDbCodeFromId(long id)
        {
            var res = string.Empty;
            var idParts = IdParts.DecomposeIdPartWithCodeGen(id);
            return (DbCode)idParts.ShardId;
        }

        public static string ChosenConnection(this Dictionary<string, string> connectionStrs, DbCode code)
        {
            var res = string.Empty;
            var nameDb = Enum.GetName(typeof(DbCode), code);
            if (connectionStrs.ContainsKey(nameDb))
            {
                res = connectionStrs.FirstOrDefault(c => c.Key == nameDb).Value;
            }
            return res;
        }

        
    }
}
