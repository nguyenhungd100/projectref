﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ChannelVN.IMS2.Foundation.Common
{
    public static class EnumHelper
    {
        public static T GetAttributeOfType<T>(this Enum enumVal) where T : Attribute
        {
            var type = enumVal.GetType();
            var memInfo = type.GetMember(enumVal.ToString());
            var attribute = memInfo[0].GetCustomAttributes(typeof(T), false);
            return (attribute.Length > 0) ? (T)attribute[0] : null;
        }
    }
}
