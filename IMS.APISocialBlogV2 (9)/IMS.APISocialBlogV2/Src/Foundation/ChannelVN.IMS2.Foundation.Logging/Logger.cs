﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using ChannelVN.IMS2.Foundation.Common;
using Serilog;
using Serilog.Core;
using Serilog.Events;

namespace ChannelVN.IMS2.Foundation.Logging
{
    //Using Serilog.AspNetCore
    //Must be install the following NuGet package into your app.
    //
    //Install-Package Serilog.AspNetCore -DependencyVersion Highest
    //Install-Package Serilog.Sinks.RollingFile
    //Install-Package Serilog.Sinks.Async
    //Install-Package Serilog.Sinks.Console
    //Install-Package Serilog.Enrichers.Environment
    //Install-Package Serilog.Enrichers.Thread
    //Install-Package Serilog.Settings.Configuration    > If read from configuration in appSettings.json file
    public class Logger //: Serilog.ILogger
    {
        //Create(options =>
        //{
        //    options.ReadFrom.Configuration(hostingContext.Configuration);
        //});
        public static void Create(Action<LoggerConfiguration> configureLogger)
        {
            var options = new LoggerConfiguration();
            configureLogger(options);
            Log.Logger = options.CreateLogger();
        }

        //File: appSettings.json
        //
        //"Serilog": {
        //"Using": [ "Serilog.Sinks.RollingFile", "Serilog.Sinks.Async" ],
        //"MinimumLevel": {
        //  "Default": "Information",
        //  "Override": {
        //    "Microsoft": "Warning",
        //    "System": "Warning"
        //  }
        //},
        //"WriteTo": [
        //  {
        //    "Name": "Async",
        //    "Args": {
        //      "configure": [
        //        {
        //          "Name": "RollingFile",
        //          "Args": { "pathFormat": "Logs/log-{Date}.log" }
        //        }
        //      ]
        //    }
        //  }
        //],
        //"Enrich": [ "FromLogContext", "WithMachineName", "WithThreadId" ],
        //"Properties": {
        //  "Application": "ChannelVN.IMS2.WebApi"
        //}
        //-------------------------------------------------------------------------------------------
        //
        //At the time of this writing, Serilog's Rolling File sink supports 3 (three) specifiers:
        //
        //  1. {Date}, which is formatted as yyyyMMdd
        //  2. {Hour}, which is formatted as yyyyMMddHH
        //  3. {HalfHour}, which is formatted as yyyyMMddHHmm
        //-------------------------------------------------------------------------------------------
        public static void Create(bool writeToFile = true, bool isDebug = false, bool fileBatch = false)
        {
            Create(options =>
            {
                options
                    .MinimumLevel.Verbose()
                    .MinimumLevel.Override("Microsoft", LogEventLevel.Warning)
                    .MinimumLevel.Override("System", LogEventLevel.Warning)
                    .Enrich.FromLogContext()
                    .Enrich.WithMachineName()
                    .Enrich.WithThreadId()
                    .Enrich.WithProperty("Application", "ChannelVN.IMS2.WebApi");
                if (writeToFile)
                {
                    options
                        .WriteTo.Logger(l => l.Filter.ByIncludingOnly(e => e.Level == LogEventLevel.Verbose).WriteTo.Async(a => a.RollingFile("Logs/log-{Date}-verbose.log", shared: true)))
                        .WriteTo.Logger(l => l.Filter.ByIncludingOnly(e => e.Level == LogEventLevel.Debug).WriteTo.Async(a => a.RollingFile("Logs/log-{Date}-debug.log", shared: true)))
                        .WriteTo.Logger(l => l.Filter.ByIncludingOnly(e => e.Level == LogEventLevel.Information).WriteTo.Async(a => a.RollingFile("Logs/log-{Date}-info.log", shared: true)))
                        .WriteTo.Logger(l => l.Filter.ByIncludingOnly(e => e.Level == LogEventLevel.Warning).WriteTo.Async(a => a.RollingFile("Logs/log-{Date}-warning.log", shared: true)))
                        .WriteTo.Logger(l => l.Filter.ByIncludingOnly(e => e.Level == LogEventLevel.Error).WriteTo.Async(a => a.RollingFile("Logs/log-{Date}-error.log", shared: true)))
                        .WriteTo.Logger(l => l.Filter.ByIncludingOnly(e => e.Level == LogEventLevel.Fatal).WriteTo.Async(a => a.RollingFile("Logs/log-{Date}-fatal.log", shared: true)));
                    if (fileBatch)
                    {
                        options.WriteTo.Async(a => a.RollingFile("Logs/log-{Date}.log", shared: true));
                    }
                }
                if (isDebug)
                {
                    options.WriteTo.Console();
                }
            });
        }

        public static void Dispose()
        { 
            Log.CloseAndFlush();
        }

        public static bool BindMessageTemplate(string messageTemplate, object[] propertyValues, out MessageTemplate parsedTemplate, out IEnumerable<LogEventProperty> boundProperties)
            => Log.BindMessageTemplate(messageTemplate, propertyValues, out parsedTemplate, out boundProperties);

        public static bool BindProperty(string propertyName, object value, bool destructureObjects, out LogEventProperty property)
            => Log.BindProperty(propertyName, value, destructureObjects, out property);

        public static void Debug(string messageTemplate)
        { 
            Log.Debug(messageTemplate);
        }

        public static void Debug<T>(string messageTemplate, T propertyValue)
            => Log.Debug<T>(messageTemplate, propertyValue);

        public static void Debug<T0, T1>(string messageTemplate, T0 propertyValue0, T1 propertyValue1)
            => Log.Debug<T0, T1>(messageTemplate, propertyValue0, propertyValue1);

        public static void Debug<T0, T1, T2>(string messageTemplate, T0 propertyValue0, T1 propertyValue1, T2 propertyValue2)
            => Log.Debug<T0, T1, T2>(messageTemplate, propertyValue0, propertyValue1, propertyValue2);

        public static void Debug(string messageTemplate, params object[] propertyValues)
            => Log.Debug(messageTemplate, propertyValues);

        public static void Debug(Exception exception, string messageTemplate)
        {
            Log.Debug(exception, messageTemplate);
        }            

        public static void Debug<T>(Exception exception, string messageTemplate, T propertyValue)
            => Log.Debug<T>(exception, messageTemplate, propertyValue);

        public static void Debug<T0, T1>(Exception exception, string messageTemplate, T0 propertyValue0, T1 propertyValue1)
            => Log.Debug<T0, T1>(exception, messageTemplate, propertyValue0, propertyValue1);

        public static void Debug<T0, T1, T2>(Exception exception, string messageTemplate, T0 propertyValue0, T1 propertyValue1, T2 propertyValue2)
            => Log.Debug<T0, T1, T2>(exception, messageTemplate, propertyValue0, propertyValue1, propertyValue2);

        public static void Debug(Exception exception, string messageTemplate, params object[] propertyValues)
            => Log.Debug(exception, messageTemplate, propertyValues);

        public static void Error(string messageTemplate)
        {
            Log.Error(messageTemplate);
        }
        public static void Error<T>(string messageTemplate, T propertyValue)
        { 
            Log.Error<T>(messageTemplate, propertyValue);
        }

        public static void Error<T0, T1>(string messageTemplate, T0 propertyValue0, T1 propertyValue1)
        { 
            Log.Error<T0, T1>(messageTemplate, propertyValue0, propertyValue1);
        }

        public static void Error<T0, T1, T2>(string messageTemplate, T0 propertyValue0, T1 propertyValue1, T2 propertyValue2)
        { 
            Log.Error<T0, T1, T2>(messageTemplate, propertyValue0, propertyValue1, propertyValue2);
        }

        public static void Error(string messageTemplate, params object[] propertyValues)
        { 
            Log.Error(messageTemplate, propertyValues);
        }

        public static void Error(Exception exception, string messageTemplate)
        {
            Log.Error(exception, messageTemplate);
        }

        public static void Error<T>(Exception exception, string messageTemplate, T propertyValue)
        { 
            Log.Error<T>(exception, messageTemplate, propertyValue);
        }

        public static void Error<T0, T1>(Exception exception, string messageTemplate, T0 propertyValue0, T1 propertyValue1)
        { 
            Log.Error<T0, T1>(exception, messageTemplate, propertyValue0, propertyValue1);
        }

        public static void Error<T0, T1, T2>(Exception exception, string messageTemplate, T0 propertyValue0, T1 propertyValue1, T2 propertyValue2)
        { 
            Log.Error<T0, T1, T2>(exception, messageTemplate, propertyValue0, propertyValue1, propertyValue2);
        }

        public static void Error(Exception exception, string messageTemplate, params object[] propertyValues)
        { 
            Log.Error(exception, messageTemplate, propertyValues);
        }

        public static void Fatal(string messageTemplate)
        { 
            Log.Fatal(messageTemplate);
        }

        public static void Fatal<T>(string messageTemplate, T propertyValue)
        { 
            Log.Fatal<T>(messageTemplate, propertyValue);
        }

        public static void Fatal<T0, T1>(string messageTemplate, T0 propertyValue0, T1 propertyValue1)
        { 
            Log.Fatal<T0, T1>(messageTemplate, propertyValue0, propertyValue1);
        }

        public static void Fatal<T0, T1, T2>(string messageTemplate, T0 propertyValue0, T1 propertyValue1, T2 propertyValue2)
        { 
            Log.Fatal<T0, T1, T2>(messageTemplate, propertyValue0, propertyValue1, propertyValue2);
        }

        public static void Fatal(string messageTemplate, params object[] propertyValues)
        { 
            Log.Fatal(messageTemplate, propertyValues);
        }

        public static void Fatal(Exception exception, string messageTemplate)
        { 
            Log.Fatal(exception, messageTemplate);
        }

        public static void Fatal<T>(Exception exception, string messageTemplate, T propertyValue)
        { 
            Log.Fatal<T>(exception, messageTemplate, propertyValue);
        }

        public static void Fatal<T0, T1>(Exception exception, string messageTemplate, T0 propertyValue0, T1 propertyValue1)
        { 
            Log.Fatal<T0, T1>(exception, messageTemplate, propertyValue0, propertyValue1);
        }

        public static void Fatal<T0, T1, T2>(Exception exception, string messageTemplate, T0 propertyValue0, T1 propertyValue1, T2 propertyValue2)
        { 
            Log.Fatal<T0, T1, T2>(exception, messageTemplate, propertyValue0, propertyValue1, propertyValue2);
        }

        public static void Fatal(Exception exception, string messageTemplate, params object[] propertyValues)
        {
            Log.Fatal(exception, messageTemplate, propertyValues);
        }
           
        public static ILogger ForContext(ILogEventEnricher enricher)
            => Log.ForContext(enricher);

        public static ILogger ForContext(IEnumerable<ILogEventEnricher> enrichers)
            => Log.ForContext(enrichers.Cast<ILogEventEnricher>().ToArray());

        public static ILogger ForContext(string propertyName, object value, bool destructureObjects = false)
            => Log.ForContext(propertyName, value, destructureObjects);

        public static ILogger ForContext<TSource>()
            => Log.ForContext<TSource>();

        public static ILogger ForContext(Type source)
            => Log.ForContext(source);

        public static void Information(string messageTemplate)
        { 
            Log.Information(messageTemplate);
        }

        public static void Information<T>(string messageTemplate, T propertyValue)
            => Log.Information<T>(messageTemplate, propertyValue);

        public static void Information<T0, T1>(string messageTemplate, T0 propertyValue0, T1 propertyValue1)
            => Log.Information<T0, T1>(messageTemplate, propertyValue0, propertyValue1);

        public static void Information<T0, T1, T2>(string messageTemplate, T0 propertyValue0, T1 propertyValue1, T2 propertyValue2)
            => Log.Information<T0, T1, T2>(messageTemplate, propertyValue0, propertyValue1, propertyValue2);

        public static void Information(string messageTemplate, params object[] propertyValues)
            => Log.Information(messageTemplate, propertyValues);

        public static void Information(Exception exception, string messageTemplate)
        { 
            Log.Information(exception, messageTemplate);
        }

        public static void Information<T>(Exception exception, string messageTemplate, T propertyValue)
            => Log.Information<T>(exception, messageTemplate, propertyValue);

        public static void Information<T0, T1>(Exception exception, string messageTemplate, T0 propertyValue0, T1 propertyValue1)
            => Log.Information<T0, T1>(exception, messageTemplate, propertyValue0, propertyValue1);

        public static void Information<T0, T1, T2>(Exception exception, string messageTemplate, T0 propertyValue0, T1 propertyValue1, T2 propertyValue2)
            => Log.Information<T0, T1, T2>(exception, messageTemplate, propertyValue0, propertyValue1, propertyValue2);

        public static void Information(Exception exception, string messageTemplate, params object[] propertyValues)
            => Log.Information(exception, messageTemplate, propertyValues);

        public static bool IsEnabled(LogEventLevel level)
            => Log.IsEnabled(level);

        public static void Verbose(string messageTemplate)
            => Log.Verbose(messageTemplate);

        public static void Verbose<T>(string messageTemplate, T propertyValue)
            => Log.Verbose<T>(messageTemplate, propertyValue);

        public static void Verbose<T0, T1>(string messageTemplate, T0 propertyValue0, T1 propertyValue1)
            => Log.Verbose<T0, T1>(messageTemplate, propertyValue0, propertyValue1);

        public static void Verbose<T0, T1, T2>(string messageTemplate, T0 propertyValue0, T1 propertyValue1, T2 propertyValue2)
            => Log.Verbose<T0, T1, T2>(messageTemplate, propertyValue0, propertyValue1, propertyValue2);

        public static void Verbose(string messageTemplate, params object[] propertyValues)
            => Log.Verbose(messageTemplate, propertyValues);

        public static void Verbose(Exception exception, string messageTemplate)
            => Log.Verbose(exception, messageTemplate);

        public static void Verbose<T>(Exception exception, string messageTemplate, T propertyValue)
            => Log.Verbose<T>(exception, messageTemplate, propertyValue);

        public static void Verbose<T0, T1>(Exception exception, string messageTemplate, T0 propertyValue0, T1 propertyValue1)
            => Log.Verbose<T0, T1>(exception, messageTemplate, propertyValue0, propertyValue1);

        public static void Verbose<T0, T1, T2>(Exception exception, string messageTemplate, T0 propertyValue0, T1 propertyValue1, T2 propertyValue2)
            => Log.Verbose<T0, T1, T2>(exception, messageTemplate, propertyValue0, propertyValue1, propertyValue2);

        public static void Verbose(Exception exception, string messageTemplate, params object[] propertyValues)
            => Log.Verbose(exception, messageTemplate, propertyValues);

        public static void Warning(string messageTemplate)
        { 
            Log.Warning(messageTemplate);
        }

        public static void Warning<T>(string messageTemplate, T propertyValue)
            => Log.Warning<T>(messageTemplate, propertyValue);

        public static void Warning<T0, T1>(string messageTemplate, T0 propertyValue0, T1 propertyValue1)
            => Log.Warning<T0, T1>(messageTemplate, propertyValue0, propertyValue1);

        public static void Warning<T0, T1, T2>(string messageTemplate, T0 propertyValue0, T1 propertyValue1, T2 propertyValue2)
            => Log.Warning<T0, T1, T2>(messageTemplate, propertyValue0, propertyValue1, propertyValue2);

        public static void Warning(string messageTemplate, params object[] propertyValues)
            => Log.Warning(messageTemplate, propertyValues);

        public static void Warning(Exception exception, string messageTemplate)
        { 
            Log.Warning(exception, messageTemplate);
        }

        public static void Warning<T>(Exception exception, string messageTemplate, T propertyValue)
            => Log.Warning<T>(exception, messageTemplate, propertyValue);

        public static void Warning<T0, T1>(Exception exception, string messageTemplate, T0 propertyValue0, T1 propertyValue1)
            => Log.Warning<T0, T1>(exception, messageTemplate, propertyValue0, propertyValue1);

        public static void Warning<T0, T1, T2>(Exception exception, string messageTemplate, T0 propertyValue0, T1 propertyValue1, T2 propertyValue2)
            => Log.Warning<T0, T1, T2>(exception, messageTemplate, propertyValue0, propertyValue1, propertyValue2);

        public static void Warning(Exception exception, string messageTemplate, params object[] propertyValues)
            => Log.Warning(exception, messageTemplate, propertyValues);

        public static void Write(LogEvent logEvent)
            => Log.Write(logEvent);

        public static void Write(LogEventLevel level, string messageTemplate)
            => Log.Write(level, messageTemplate);

        public static void Write<T>(LogEventLevel level, string messageTemplate, T propertyValue)
            => Log.Write<T>(level, messageTemplate, propertyValue);

        public static void Write<T0, T1>(LogEventLevel level, string messageTemplate, T0 propertyValue0, T1 propertyValue1)
            => Log.Write<T0, T1>(level, messageTemplate, propertyValue0, propertyValue1);

        public static void Write<T0, T1, T2>(LogEventLevel level, string messageTemplate, T0 propertyValue0, T1 propertyValue1, T2 propertyValue2)
            => Log.Write<T0, T1, T2>(level, messageTemplate, propertyValue0, propertyValue1, propertyValue2);

        public static void Write(LogEventLevel level, string messageTemplate, params object[] propertyValues)
            => Log.Write(level, messageTemplate, propertyValues);

        public static void Write(LogEventLevel level, Exception exception, string messageTemplate)
            => Log.Write(level, exception, messageTemplate);

        public static void Write<T>(LogEventLevel level, Exception exception, string messageTemplate, T propertyValue)
            => Log.Write<T>(level, exception, messageTemplate, propertyValue);

        public static void Write<T0, T1>(LogEventLevel level, Exception exception, string messageTemplate, T0 propertyValue0, T1 propertyValue1)
            => Log.Write<T0, T1>(level, exception, messageTemplate, propertyValue0, propertyValue1);

        public static void Write<T0, T1, T2>(LogEventLevel level, Exception exception, string messageTemplate, T0 propertyValue0, T1 propertyValue1, T2 propertyValue2)
            => Log.Write<T0, T1, T2>(level, exception, messageTemplate, propertyValue0, propertyValue1, propertyValue2);

        public static void Write(LogEventLevel level, Exception exception, string messageTemplate, params object[] propertyValues)
            => Log.Write(level, exception, messageTemplate, propertyValues);
    }
}
