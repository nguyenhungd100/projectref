﻿using ChannelVN.IMS2.Foundation.Toolkit.Messaging.ReserveStream;
using System.Collections.Generic;
using System.Text.RegularExpressions;

namespace ChannelVN.IMS2.Foundation.Toolkit.Messaging.ExchangeQueue
{
    public class Exchange<TPayload>
    {
        private Dictionary<string, Stream<TPayload>> _streams = null;
        private ExchangeType _type = ExchangeType.Direct;

        public Exchange(Dictionary<string, Stream<TPayload>> streams, EQOptions options = null)
        {
            _streams = streams;
            _type = options == null ? ExchangeType.Direct : options.Type;
        }

        public List<Stream<TPayload>> GetReserveStream(ExchangeFilter filterCondition)
        {
            List<Stream<TPayload>> matchedStreams = new List<Stream<TPayload>>();
            switch (_type)
            {
                case ExchangeType.Direct:
                    {
                        var stream = _streams.GetValueOrDefault(filterCondition.Match);
                        if (stream != null)
                        {
                            matchedStreams.Add(stream);
                        }
                        
                        break;
                    }
                case ExchangeType.Fanout:
                    {
                        var streams = _streams.Values;
                        if (streams != null)
                        {
                            matchedStreams = new List<Stream<TPayload>>(streams);
                        }

                        break;
                    }
                case ExchangeType.Topic:
                    {
                        var rgx = new Regex(filterCondition.Match, RegexOptions.IgnoreCase);
                        foreach (var key in _streams.Keys)
                        {
                            var matches = rgx.Matches(key);
                            if (matches.Count > 0)
                            {
                                var stream = _streams.GetValueOrDefault(key);
                                if (stream != null)
                                {
                                    matchedStreams.Add(stream);
                                }
                            }
                        }
                        break;
                    }
            }
            return matchedStreams;
        }
    }
}
