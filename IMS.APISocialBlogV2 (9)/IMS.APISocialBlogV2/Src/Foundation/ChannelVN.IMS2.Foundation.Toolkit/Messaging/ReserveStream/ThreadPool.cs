﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ChannelVN.IMS2.Foundation.Toolkit.Messaging.ReserveStream
{
    internal class ThreadPool<TPayload>
    {
        private int DefaultWorkers => Environment.ProcessorCount;
        public int LimitPoolThread { get; private set; }

        private Thread<TPayload>[] _threads = null;
        private bool _nextFlag = false;
        private ThreadPoolState _state = new ThreadPoolState(true);

        public ThreadPool(Stream<TPayload> stream)
        {
            LimitPoolThread = stream.Options.LIMIT_POOL_THREAD > 0 ? stream.Options.LIMIT_POOL_THREAD : DefaultWorkers;
            
            _threads = new Thread<TPayload>[LimitPoolThread];
            for (var i = 0; i < LimitPoolThread; i++)
            {
                _threads[i] = new Thread<TPayload>(i + 1, stream);
            }
        }

        public void GetFreeThread(Action<Thread<TPayload>> callback)
        {
            for (var i = 0; i < _threads.Length; ++i)
            {
                if (!_threads[i].IsBusy)
                {
                    callback(_threads[i]);
                    break;
                }
            }
        }

        public void Run(ThreadPoolState state = null)
        {
            _nextFlag = true;
            _state = state ?? _state;
            DoJob();
        }

        private void DoJob()
        {
            if (!_state.IsRunnable)
            {
                return;
            }
            GetFreeThread(thread => {
                if (thread != null)
                {
                    thread.IsBusy = true;
                    thread.Stream.Count(length => {
                        _nextFlag = (length > 0);

                        thread.Execute(/*next*/() => {
                            thread.IsBusy = false;
                            if (_nextFlag)
                            {
                                DoJob();
                            }
                        });
                    });
                }
            });
        }
    }
}
