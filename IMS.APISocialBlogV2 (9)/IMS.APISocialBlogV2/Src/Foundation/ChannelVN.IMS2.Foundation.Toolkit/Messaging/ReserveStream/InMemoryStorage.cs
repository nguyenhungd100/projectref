﻿using System;
using System.Collections.Generic;

namespace ChannelVN.IMS2.Foundation.Toolkit.Messaging.ReserveStream
{
    public class InMemoryStorage<TPayload> : Storage<TPayload>
    {
        public Queue<Element<TPayload>> Elements { get; private set; }
        public Queue<Element<TPayload>> RecoveryElements { get; private set; }

        public InMemoryStorage() : this(new StorageOptions())
        {
        }

        public InMemoryStorage(StorageOptions options) : base(options)
        {
            Elements = new Queue<Element<TPayload>>();
            RecoveryElements = new Queue<Element<TPayload>>();
        }

        public override void Push(Element<TPayload> element, Action<Exception, long> callback)
        {
            lock (Elements)
            {
                Elements.Enqueue(element);
            }
            callback(null, Elements.Count);
        }

        public override void Pop(Action<Exception, Element<TPayload>> callback)
        {
            Element<TPayload> entry = null;
            lock (Elements)
            {
                entry = Elements.Dequeue();
            }
            callback(null, entry);
        }

        public override void Count(Action<Exception, long> callback)
        {
            callback(null, Elements.Count);
        }

        public override void Backup(Element<TPayload> element, Action<Exception, long> callback)
        {
            RecoveryElements.Enqueue(element);
            callback(null, RecoveryElements.Count);
        }

        public override void Restore(Action<Exception, Element<TPayload>> callback)
        {
            Element<TPayload> entry = null;
            while (RecoveryElements.Count > 0)
            {
                entry = RecoveryElements.Dequeue();
                Push(entry, (err, length) => callback(err, entry));
            }
        }
    }
}
