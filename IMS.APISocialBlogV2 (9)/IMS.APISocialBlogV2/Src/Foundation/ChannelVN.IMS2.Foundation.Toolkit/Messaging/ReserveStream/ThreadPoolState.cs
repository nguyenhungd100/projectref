﻿namespace ChannelVN.IMS2.Foundation.Toolkit.Messaging.ReserveStream
{
    public class ThreadPoolState
    {
        public bool IsRunnable { get; set; }

        public ThreadPoolState(bool state)
            => IsRunnable = state;
    }
}
