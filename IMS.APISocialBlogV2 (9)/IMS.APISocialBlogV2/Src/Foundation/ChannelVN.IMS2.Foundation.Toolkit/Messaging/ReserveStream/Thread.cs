﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ChannelVN.IMS2.Foundation.Toolkit.Messaging.ReserveStream
{
    internal class Thread<TPayload>
    {
        private readonly int _id;

        public Stream<TPayload> Stream { get; private set; }
        public bool IsBusy { get; set; }

        public Thread(Stream<TPayload> stream) : this(0, stream)
        {
        }

        public Thread(int id, Stream<TPayload> stream)
        {
            IsBusy = false;
            _id = id;
            Stream = stream;
        }

        public void Execute(Action next)
        {
            try
            {
                new System.Threading.Thread(() =>
                {
                    Stream.GetFreeElement(element =>
                    {
                        if (element != null)
                        {
                            element.Pid = _id;
                            Stream.Handler(element, /*ack*/err =>
                            {
                                if (err == null)
                                {
                                    next();
                                }
                                else
                                {
                                    if ((Stream.Options.Retry == 0) || (element.Retry < Stream.Options.Retry))
                                    {
                                        element.Retry += 1;
                                        Stream.AddElement(element, (addErr, effected) =>
                                        {
                                            next();
                                        });
                                    }
                                    else if (Stream.Options.Recovery && (element.Retry >= Stream.Options.Retry))
                                    {
                                        element.Retry = 0; //reset
                                        Stream.Backup(element, (bkErr, effected) =>
                                        {
                                            next();
                                        });
                                    }
                                    else
                                    {
                                        next();
                                    }
                                }
                            });
                        }
                        else
                        {
                            next();
                        }
                    });
                }).Start();
            }
            catch (Exception err)
            {
                next();
                Stream.Error(err);
            }
        }
    }
}
