﻿using System;

namespace ChannelVN.IMS2.Foundation.Toolkit.Messaging.ReserveStream
{
    public abstract class Storage<TPayload>
    {
        /// <summary>
        /// The optional parameters.
        /// </summary>
        public StorageOptions Options { get; private set; }

        /// <summary>
        /// The constructor of the storage.
        /// </summary>
        /// <param name="options"></param>
        public Storage(StorageOptions options)
            => Options = options;

        /// <summary>
        /// For push an emlement to the stream.
        /// </summary>
        /// <param name="element"></param>
        /// <param name="callback"></param>
        public abstract void Push(Element<TPayload> element, Action<Exception, long> callback);
        
        /// <summary>
        /// For pop an element from the stream.
        /// </summary>
        /// <param name="callback"></param>
        public abstract void Pop(Action<Exception, Element<TPayload>> callback);

        /// <summary>
        /// For get the total of elements in the stream.
        /// </summary>
        /// <param name="callback"></param>
        public virtual void Count(Action<Exception, long> callback)
        {
            callback(null, 0);
        }

        /// <summary>
        /// For backup an element data to sencond stream.
        /// </summary>
        /// <param name="element"></param>
        /// <param name="callback"></param>
        public virtual void Backup(Element<TPayload> element, Action<Exception, long> callback)
        {
            callback(null, 0);
        }

        /// <summary>
        /// For restore some elements inside the second stream.
        /// </summary>
        /// <param name="callback"></param>
        public virtual void Restore(Action<Exception, Element<TPayload>> callback)
        {
            callback(null, null);
        }
    }
}
