﻿using System;
using System.Threading.Tasks;

namespace ChannelVN.IMS2.Foundation.Toolkit.Messaging.ReserveStream
{
    public class Stream<TPayload>
    {
        public string Id { get; }
        public StreamOptions Options { get; }
        public Action<Element<TPayload>, Action<Exception>> Handler { get; private set; }

        private long _currentTotal = 0;
        private long _seriNum = 0;

        private Storage<TPayload> _store = null;
        private ThreadPool<TPayload> _pool = null;

        public event EventHandler<ErrorEventArgs> OnError;
        public event EventHandler<RaiseEventArgs<TPayload>> OnRaise;
        public event EventHandler<DrainEventArgs> OnDrain;
        public event EventHandler<DoneEventArgs> OnDone;
        public event EventHandler<StartEventArgs> OnStart;
        public event EventHandler<StopEventArgs> OnStop;

        public Stream(StreamOptions options) : this(Guid.NewGuid().ToString().Replace("-", ""), options)
        {
        }

        public Stream(string id, StreamOptions options)
        {
            Id = id;
            Options = options;

            SetHandler(null);

            switch (Options.Mode)
            {
                case StorageMode.Redis:
                    {
                        _store = new Databases.RedisStorage<TPayload>(new StorageOptions
                        {
                            Stream = Id,
                            Engine = Options.Engine,
                            PrefixStream = Options.PrefixStream,
                            ExpiresIn = Options.ExpiresIn,
                            SeparateChar = Options.SeparateChar
                        });
                        break;
                    }
                case StorageMode.Memory:
                default:
                    {
                        _store = new InMemoryStorage<TPayload>(new StorageOptions { });
                        break;
                    }
            }
            Count(length => _currentTotal += length);

            _pool = new ThreadPool<TPayload>(this);
            _pool.Run();
        }

        public Stream<TPayload> AddElement(Element<TPayload> element, Action<Exception, int> callback = null)
        {
            try
            {
                Increment();

                if (element != null)
                    element.Id = _seriNum;

                _store.Push(element, (err, length) =>
                {
                    if (err != null)
                    {
                        FnError(err);
                        callback?.Invoke(err, 0);
                    }
                    else
                    {
                        FnRaise(element);

                        callback?.Invoke(null, 1);
                        _pool.Run();
                    }
                });
            }
            catch (Exception err)
            {
                FnError(err);
                callback?.Invoke(err, 0);
            }
            return this;
        }

        public Stream<TPayload> AddElement(TPayload element, Action<Exception, int> callback = null)
        {
            return AddElement(new Element<TPayload>(0, element), callback);
        }

        public void GetFreeElement(Action<Element<TPayload>> callback)
        {
            try
            {
                _store.Pop((err, element) =>
                {
                    if (err != null)
                    {
                        callback(null);
                    }
                    else
                    {
                        Decrement();
                        callback(element);
                    }
                });
            }
            catch
            {
                callback(null);
            }
        }

        public void Count(Action<long> callback)
        {
            _store.Count((err, length) =>
            {
                if (err != null)
                {
                    callback(0);
                }
                else
                {
                    callback(length);
                }
            });
        }

        public Stream<TPayload> Backup(Element<TPayload> element, Action<Exception, int> callback = null)
        {
            try
            {
                _store.Backup(element, (err, length) =>
                {
                    if (err != null)
                    {
                        FnError(err);
                        callback?.Invoke(err, 0);
                    }
                    else
                    {
                        //FnRaise(element);

                        callback?.Invoke(null, 1);
                        _pool.Run();
                    }
                });
            }
            catch (Exception err)
            {
                FnError(err);
                callback?.Invoke(err, 0);
            }
            return this;
        }

        //public Stream<TPayload> Restore(Action<Exception, Element<TPayload>> callback = null)
        //{
        //    try
        //    {
        //        if (!Options.Recovery)
        //        {
        //            throw new Exception("The recovery mode is turn off.");
        //        }
        //        _store.Restore((err, element) =>
        //        {
        //            if (err != null)
        //            {
        //                FnError(err);
        //                callback?.Invoke(err, null);
        //            }
        //            else if (element != null)
        //            {
        //                FnRaise(element);

        //                callback?.Invoke(null, element);
        //                _pool.Run();
        //            }
        //        });
        //    }
        //    catch (Exception err)
        //    {
        //        FnError(err);
        //        callback?.Invoke(err, null);
        //    }
        //    return this;
        //}

        public Task Restore(Action<Exception, Element<TPayload>> callback = null)
        {
            try
            {
                if (!Options.Recovery)
                {
                    throw new Exception("The recovery mode is turn off.");
                }
                _store.Restore((err, element) =>
                {
                    if (err != null)
                    {
                        FnError(err);
                        callback?.Invoke(err, null);
                    }
                    else if (element != null)
                    {
                        FnRaise(element);

                        callback?.Invoke(null, element);
                        _pool.Run();
                    }
                });
            }
            catch (Exception err)
            {
                FnError(err);
                callback?.Invoke(err, null);
            }
            return Task.CompletedTask;
        }

        public void SetHandler(Action<Element<TPayload>, Action<Exception>> handler)
            => Handler = handler ?? ((msg, ack) => { });

        public Stream<TPayload> Map(Action<Element<TPayload>, long> callback)
        {
            return this;
        }

        public void Abort(ThreadPoolState state = null)
            => _pool.Run(new ThreadPoolState(state == null ? false : state.IsRunnable));

        private long Increment()
        {
            ++_currentTotal;
            if (_currentTotal > Options.LIMIT_STREAM)
            {
                throw new Exception("Too large the limit stream.");
            }
            _seriNum = (_seriNum++ % Options.LIMIT_STREAM) + 1;
            return _seriNum;
        }

        private void Decrement()
        {
            if (_currentTotal > 0)
            {
                --_currentTotal;
                if (_currentTotal == 0)
                {
                    FnDrain();
                }
            }
        }

        internal void Error(Exception e)
        {
            FnError(e);
        }

        protected virtual void FnError(Exception e)
            => OnError?.Invoke(this, new ErrorEventArgs(e));

        protected virtual void FnRaise(Element<TPayload> e)
            => OnRaise?.Invoke(this, new RaiseEventArgs<TPayload>(e));

        protected virtual void FnDrain()
            => OnDrain?.Invoke(this, new DrainEventArgs());

        protected virtual void FnDone(/*response*/)
            => OnDone?.Invoke(this, new DoneEventArgs());

        protected virtual void FnStart()
            => OnStart?.Invoke(this, new StartEventArgs());

        protected virtual void FnStop(Element<TPayload> e)
            => OnStop?.Invoke(this, new StopEventArgs());
    }
}
