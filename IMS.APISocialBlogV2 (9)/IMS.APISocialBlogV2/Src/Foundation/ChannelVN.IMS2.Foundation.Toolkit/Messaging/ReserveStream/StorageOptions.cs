﻿namespace ChannelVN.IMS2.Foundation.Toolkit.Messaging.ReserveStream
{
    public class StorageOptions
    {
        public StorageEngine Engine { get; set; }
        public int ExpiresIn { get; set; }
        internal string Stream { get; set; }
        public string PrefixStream { get; set; }
        public char SeparateChar { get; set; }

        public StorageOptions()
        {
        }
    }
}
