﻿using ChannelVN.IMS2.Core.Entities.Base.Test;
using System;
using System.Collections.Generic;
using System.Text;

namespace ChannelVN.IMS2.Core.Models.CacheStore.BlogFacade
{
    public interface IBlogCacheStore
    {
        TestTable GetById(string id);
        bool AddOneTest(TestTable model);
    }
}
