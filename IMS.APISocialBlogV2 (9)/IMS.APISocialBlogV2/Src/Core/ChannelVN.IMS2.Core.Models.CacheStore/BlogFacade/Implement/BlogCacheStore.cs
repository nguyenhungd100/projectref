﻿using System;
using System.Collections.Generic;
using System.Text;
using ChannelVN.IMS2.Core.Entities.Base.Test;
using ChannelVN.IMS2.Foundation.Logging;
using Newtonsoft.Json;
using StackExchange.Redis;

namespace ChannelVN.IMS2.Core.Models.CacheStore.BlogFacade.Implement
{
    public class BlogCacheStore : CmsMainCacheStore, IBlogCacheStore
    {
        public BlogCacheStore()
        {

        }
        public bool AddOneTest(TestTable model)
        {
            var result = false;
            using (var context = Current.GetContext())
            {
                try
                {
                    var genkey = context.GenerateRedisKey<TestTable>();
                    result = context.SetAdd(genkey, JsonConvert.SerializeObject(model));
                }
                catch (Exception ex)
                {

                    throw;
                }
            }
            return result;
        }
        public TestTable GetById(string id)
        {
            var result = new TestTable();
            using (var context = Current.GetContext())
            {
                try
                {
                    var idParse = long.Parse(id);
                    var redisKey = context.GetRedisKeySpecified<TestTable>(idParse);
                    result = JsonConvert.DeserializeObject<TestTable>(context.DebugObject(redisKey));
                    
                }
                catch (Exception ex)
                {
                    Logger.Error(ex, ex.Message);
                }
            }
            return result;
        }
    }
}
