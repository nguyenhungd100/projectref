﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using ChannelVN.IMS2.Core.Entities.Base;
using ChannelVN.IMS2.Core.Entities.Base.Test;
using ChannelVN.IMS2.Core.Entities.ErrorCode;
using ChannelVN.IMS2.Core.Models.CacheStore.Blog;
using ChannelVN.IMS2.Core.Models.CacheStore.BlogFacade;
using ChannelVN.IMS2.Core.Models.DataStore.Blog;
using ChannelVN.IMS2.Core.Models.DataStore.BlogFacade;
using ChannelVN.IMS2.Core.Models.Models.Blogs;
using ChannelVN.IMS2.Core.Models.Models.TestTables;
using ChannelVN.IMS2.Core.Models.SearchStore.Blog;
using ChannelVN.IMS2.Core.Models.SearchStore.BlogFacade;
using ChannelVN.IMS2.Foundation.Common;
using ChannelVN.IMS2.Foundation.Data;
using ChannelVN.IMS2.Foundation.Data.Providers;
using ChannelVN.IMS2.Foundation.Logging;
using static ChannelVN.IMS2.Core.Entities.ErrorCode.ErrorMapping;

namespace ChannelVN.IMS2.Core.Repositories.BlogRepos
{
    public interface IBlogRepository
    {
        TestTable GetById(string id);
        SearchTestResult SearchTest(SearchTestModel model);
        bool AddOneTest(TestTable model);
        bool UpdateOne(TestTable testTable);
        bool DeleteOne(TestTable testTable);



        Task<ErrorMapping.ErrorCodes> AddBlogAsync(BlogModel model);
        IEnumerable<BlogModel> GetBlog();
        Task<bool> AddManyTest(IEnumerable<TestTableModel> models);
        bool BulkCopy();
        bool TestGenId();                   
    }

    public class BlogRepository : IBlogRepository
    {
        private readonly IBlogDataStore _blogDataStore;

        public BlogRepository(IBlogDataStore blogDataStore)
        {
            _blogDataStore = blogDataStore;
        }

        public TestTable GetById(string id)
        {
            return _blogDataStore.GetById(id);
        }

        public SearchTestResult SearchTest(SearchTestModel model)
        {
            return _blogDataStore.SearchTest(model);
        }

        public bool AddOneTest(TestTable model)
        {
            var result = false;
            try
            {
                result = _blogDataStore.AddOneTest(model);
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.ToString());
                throw;
            }                    
            return result;
        }

        public async Task<bool> AddManyTest(IEnumerable<TestTableModel> models)
        {
            var x = new List<TestTable>();
            return await _blogDataStore.AddManyTest(x);
        }

        public bool UpdateOne(TestTable testTable)
        {
            return _blogDataStore.UpdateOne(testTable);
        }

        public bool DeleteOne(TestTable testTable)
        {
            return _blogDataStore.DeleteOne(testTable);
        }

        
        



















        
       
        public async Task<ErrorMapping.ErrorCodes> AddBlogAsync(BlogModel model)
        {

            var returnValue = ErrorCodes.BusinessError;
            try
            {
                var result =  BlogDataStore.FillAll();
                //if (result)
                //{
                //    returnValue = ErrorCodes.Success;
                //}
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
                returnValue = ErrorCodes.Exception;
            }
            
            //var returnValue = ErrorCodes.BusinessError;
            //try
            //{
            //    var blog = new Blog();
            //    var x = Mapper<Blog>.Map<BlogModel>(model, blog);
                //var result = await BlogDataStore.AddAsync(blog);
            //    if (result)
            //    {
            //        returnValue = ErrorCodes.Success;
            //    }
            //}
            //catch (Exception ex)
            //{
            //    Logger.Error(ex, ex.Message);
            //    returnValue = ErrorCodes.Exception;
            //}
            return returnValue;
        }

        public IEnumerable<BlogModel> GetBlog()
        {
            var result = BlogDataStore.FillAll();
            
            return null;
        }

        bool IBlogRepository.TestGenId()
        {
            throw new NotImplementedException();
        }

        public bool BulkCopy()
        {
            return _blogDataStore.BulkCopy();
        }

      
    }
}
