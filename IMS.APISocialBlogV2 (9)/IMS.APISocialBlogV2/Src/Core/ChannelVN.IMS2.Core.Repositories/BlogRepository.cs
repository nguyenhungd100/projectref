﻿using ChannelVN.IMS2.Core.Entities.Base;
using ChannelVN.IMS2.Core.Models.CacheStore.Blog;
using ChannelVN.IMS2.Foundation.Data;
using ChannelVN.IMS2.Foundation.Logging;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using static ChannelVN.IMS2.Core.Entities.ErrorCode.ErrorMapping;

namespace ChannelVN.IMS2.Core.Repositories
{
    public class BlogRepository : Repository
    {
        public static async Task<ErrorCodes> AddAsync(Blog data)
        {
            var returnValue = ErrorCodes.BusinessError;
            try
            {
                var result = await BlogCacheStore.AddAsync(data);
                if (result)
                {
                    returnValue = ErrorCodes.Success;

                    //TODO somethings
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
                returnValue = ErrorCodes.Exception;
            }
            return returnValue;
        }
    }
}
