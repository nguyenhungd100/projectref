﻿using ChannelVN.IMS2.Core.Entities.Base;
using ChannelVN.IMS2.Core.Models.CacheStore.Security;
using ChannelVN.IMS2.Core.Models.DataStore.Security;
using ChannelVN.IMS2.Foundation.Data;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace ChannelVN.IMS2.Core.Repositories
{
    public class SecurityRepository : Repository
    {
        /// <summary>
        /// 
        /// </summary>
        /// <remarks>
        /// Ưu tiên lấy dữ liệu từ Cached. Trường hợp không 
        /// có xuống lấy SQL trả về, đồng thời lưu cached sử dụng lại.
        /// </remarks>
        /// <param name="username"></param>
        /// <returns></returns>
        public static async Task<Account> GetAccountByUsernameAsync(string username)
        {
            var userInfo = await AccountCacheStore.GetUserByUsernameAsync(username);

            if(userInfo == null)
            {
                userInfo = await AccountDataStore.GetUserByUsernameAsync(username);
                if (userInfo != null)
                {
                    await AccountCacheStore.AddAsync(userInfo);
                }
            }

            return userInfo;
        }
    }
}
