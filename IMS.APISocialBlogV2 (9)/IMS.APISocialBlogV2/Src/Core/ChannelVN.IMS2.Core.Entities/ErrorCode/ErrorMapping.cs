﻿using ChannelVN.IMS2.Foundation.Common;

namespace ChannelVN.IMS2.Core.Entities.ErrorCode
{
    public class ErrorMapping : ErrorMappingBase<ErrorMapping.ErrorCodes>
    {
        protected override void InitErrorMapping()
        {
            #region General errors

            InnerHashtable[ErrorCodes.Success] = "Xử lý thành công";
            InnerHashtable[ErrorCodes.UnknowError] = "Lỗi không xác định";
            InnerHashtable[ErrorCodes.Exception] = "Lỗi trong quá trình xử lý";
            InnerHashtable[ErrorCodes.BusinessError] = "Lỗi nghiệp vụ";
            InnerHashtable[ErrorCodes.InvalidRequest] = "Yêu cầu không hợp lệ";
            InnerHashtable[ErrorCodes.TimeoutSession] = "Phiên làm việc của bạn đã hết.";
            InnerHashtable[ErrorCodes.UpdateError] = "Cập nhật không thành công.";
            InnerHashtable[ErrorCodes.AddError] = "Thêm thất bại.";
            #endregion

            #region Account
            InnerHashtable[ErrorCodes.AccountNotExits] = "Account không tồn tại.";
            InnerHashtable[ErrorCodes.AccountExits] = "Account đã tồn tại.";
            InnerHashtable[ErrorCodes.AccountStatusNotExits] = "Trạng thái Account không tồn tại.";

            #endregion

            #region News
            InnerHashtable[ErrorCodes.NewsNotExits] = "Tin bài không tồn tại.";
            InnerHashtable[ErrorCodes.YouAccountLimitQuote] = "Tài khoản của bạn đã vượt quá hạn mức trong tuần.";
            InnerHashtable[ErrorCodes.NewsNotExits] = "Tin bài không tồn tại.";

            #endregion

            #region VideoChannel
            InnerHashtable[ErrorCodes.VideoChannelNotExist] = "Kênh video không tồn tại.";

            #endregion

            #region Video
            InnerHashtable[ErrorCodes.VideoNotExist] = "Video không tồn tại.";
            InnerHashtable[ErrorCodes.VideoPermissionInvalid] = "Bạn không có quyền chỉnh sửa video này.";
            InnerHashtable[ErrorCodes.VideoKeyIsRequired] = "Key video không được để trống.";
            InnerHashtable[ErrorCodes.VideoNameIsRequired] = "Tên video không được để trống.";
            InnerHashtable[ErrorCodes.VideoHtmlCodeIsRequired] = "HtmlCode không được để trống.";
            InnerHashtable[ErrorCodes.VideoAddError] = "Lỗi hệ thống khi tạo Video.";
            InnerHashtable[ErrorCodes.VideoActionInvalid] = "Trạng thái không hợp lệ.";
            InnerHashtable[ErrorCodes.VideoFileNameIsRequired] = "Tên file video không được để trống";

            #endregion

            InnerHashtable[ErrorCodes.PermissionInvalid] = "Bạn không có quyền sản xuất.";
            InnerHashtable[ErrorCodes.DataNotExist] = "Dữ liệu không tồn tại.";
            InnerHashtable[ErrorCodes.DataInvalid] = "Dữ liệu không hợp lệ.";
            InnerHashtable[ErrorCodes.AddOfficerError] = "Không tạo được page.";
        }

        public enum ErrorCodes
        {
            #region General errors

            Success = ErrorCodeBase.Success,
            UnknowError = ErrorCodeBase.UnknowError,
            Exception = ErrorCodeBase.Exception,
            BusinessError = ErrorCodeBase.BusinessError,
            InvalidRequest = ErrorCodeBase.InvalidRequest,
            TimeoutSession = ErrorCodeBase.TimeoutSession,
            UpdateError = ErrorCodeBase.UpdateError,
            AddError = ErrorCodeBase.AddError,
            #endregion

            #region Account errors
            AccountNotExits = 101,
            AccountExits = 102,

            AccountStatusNotExits = 103,
            #endregion

            #region News errors
            NewsNotExits = 1001,
            YouAccountLimitQuote = 1002,
            #endregion

            #region VideoChannel
            VideoChannelNotExist = 2001,
            #endregion

            #region Video
            VideoNotExist = 3001,
            VideoPermissionInvalid = 3002,
            VideoKeyIsRequired = 3003,
            VideoNameIsRequired = 3004,
            VideoHtmlCodeIsRequired = 3005,
            VideoFileNameIsRequired = 3006,
            VideoAddError = 3007,
            VideoActionInvalid = 3008,
            #endregion

            PermissionInvalid = 4001,
            DataNotExist = 4002,
            DataInvalid = 4003,
            AddOfficerError = 4004
        }

        public static ErrorMapping Current
        {
            get { return _current ?? (_current = new ErrorMapping()); }
        }

        private static ErrorMapping _current;
    }
}
