﻿using ChannelVN.IMS2.Foundation.Data;
using System;

namespace ChannelVN.IMS2.Core.Entities.Base
{
    public class Template : Entity
    {
        public long Id { get; set; }
        public string Title { get; set; }
        public string Avatar { get; set; }
        public string CreatedBy { get; set; }

        private DateTime? _CreatedDate { get; set; }
        public DateTime? CreatedDate
        {
            get
            {
                return _CreatedDate;
            }
            set
            {
                _CreatedDate = value != null ? DateTime.SpecifyKind(value.Value, DateTimeKind.Unspecified) : value;
            }
        }

        public string ModifiedBy { get; set; }

        private DateTime? _ModifiedDate { get; set; }
        public DateTime? ModifiedDate
        {
            get
            {
                return _ModifiedDate;
            }
            set
            {
                _ModifiedDate = value != null ? DateTime.SpecifyKind(value.Value, DateTimeKind.Unspecified) : value;
            }
        }

        public int Status { get; set; }
        public string Description { get; set; }
        public int Priority { get; set; }
        public int CategoryId { get; set; }
        public string MetaData { get; set; }
    }
}
