﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace ChannelVN.IMS2.Core.Entities.Base.Test
{
    [Table("TestTable")]
    public class TestTable
    {
        [Key]
        public long Id { get; set; }

        public string Name { get; set; }

        public TestTableStats? Status { get; set; }
    }

    public enum TestTableStats
    {
        Init =1 ,
        Pending = 5
    }
}
