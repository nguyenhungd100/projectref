﻿namespace ChannelVN.IMS2.Core.Entities.Base
{
    public enum NewsType
    {
        Article = 1,
        Video = 2,
        PlayList = 3,
        MediaUnit = 4,
        Post = 5,
        Photo = 6,
        ShareLink = 7,
        Album = 8,
        Beam = 9
    }
}
