﻿namespace ChannelVN.IMS2.Core.Entities.Plugins
{
    public class NewsLinkInfo
    {
        public string title { get; set; }

        public string byline { get; set; }

        public string dir { get; set; }

        public string content { get; set; }

        public string textContent { get; set; }

        public int? length { get; set; }

        public string excerpt { get; set; }

        public string url { get; set; }

        public string domain { get; set; }

        public string avatar { get; set; }

        public string logo { get; set; }
    }
}
