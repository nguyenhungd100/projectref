﻿using ChannelVN.IMS2.Foundation.Common;
using ChannelVN.IMS2.Foundation.Common.Configuration;
using ChannelVN.IMS2.Foundation.Common.DbHelper;
using ChannelVN.IMS2.Foundation.Data;
using ChannelVN.IMS2.Foundation.Data.Providers;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ChannelVN.IMS2.Core.Models
{
    public class CmsMainDataStore : Store<MssqlContext, CmsMainDataStore>
    {
        public CmsMainDataStore()
        {
            Configuration.Add(STO_NAMESPACE, AppSettings.Current.ServiceConfiguration.Namespace);
            Configuration.Add(STO_CONNECTIONSTRING, AppSettings.Current.ChannelConfiguration.ConnectionStrings.CmsMainDb);
        }

        static CmsMainDataStore()
        {
            if (Current == null)
            {

                Current = new CmsMainDataStore();
            }
        }

        protected static CmsMainDataStore Current { get; } 



        //private CmsMainDataStore(DbCode code)
        //{
        //    Configuration.Add(STO_NAMESPACE, AppSettings.Current.ServiceConfiguration.Namespace);
        //    Configuration.Add(STO_CONNECTIONSTRING, SwitchDbHelper.ChosenConnection(ConnectionStrs, code));

        //}    

        //private Dictionary<string, string> ConnectionStrs = AppSettings.Current.ChannelConfiguration.ConnectionStrings.CmsMainDb;

        //protected static CmsMainDataStore FromDb(DbCode code) => new CmsMainDataStore(code);


    }
}
