﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ChannelVN.IMS2.Core.Models.Models.Blogs
{
    public class BlogModel : NewsModel
    {
        public string Cover { get; set; }
        public string CoverDesc { get; set; }
        public string Title { get; set; }
        public string Sapo { get; set; }
        public string SubTitle { get; set; }
        public string Avatar1 { get; set; }
        public string Avatar2 { get; set; }
        public string Avatar3 { get; set; }
        public long? TemplateId { get; set; }
        public string Body { get; set; }
        public string BodyMeta { get; set; }
        private DateTime? _ApprovedDate { get; set; }
        public DateTime? ApprovedDate
        {
            get
            {
                return _ApprovedDate;
            }
            set
            {
                _ApprovedDate = value != null ? DateTime.SpecifyKind(value.Value, DateTimeKind.Unspecified) : value;
            }
        }
        public string ApprovedBy { get; set; }
        public long? InstantViewId { get; set; }
        public bool IsInstantView { get; set; }
        public string InstantViewUrl { get; set; }
    }
}
