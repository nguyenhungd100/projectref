﻿using ChannelVN.IMS2.Core.Models.Models.TestTables;
using System;
using System.Collections.Generic;
using System.Text;

namespace ChannelVN.IMS2.Core.Models.Models.Blogs
{
    public class SearchTestResult : BaseSearchResult<TestTableModel>
    {
        public SearchTestResult(int pageIndex, int pageSize)
        {
            PageIndex = pageIndex;
            PageSize = pageSize;
        }
    }
}
