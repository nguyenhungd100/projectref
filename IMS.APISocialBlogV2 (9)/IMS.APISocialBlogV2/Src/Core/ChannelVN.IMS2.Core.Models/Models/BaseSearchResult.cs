﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ChannelVN.IMS2.Core.Models.Models
{
    public class BaseSearchResult<T> where T : class
    {
        public IEnumerable<T> Records { get; set; }

        public long TotalRecord { get; set; }
        

        public int PageIndex { get; set; }

        public int PageSize { get; set; }

        public int PageCount
        {
            get
            {
                return (int) (this.TotalRecord / this.PageSize + (this.TotalRecord % this.PageSize > 0 ? 1 : 0));
            }
        }
    }
}
