﻿using ChannelVN.IMS2.Foundation.Common.Configuration;
using ChannelVN.IMS2.Foundation.Data;
using ChannelVN.IMS2.Foundation.Data.Providers;

namespace ChannelVN.IMS2.Core.Models
{
    public class CmsMainSearchStore : Store<ElasticContext, CmsMainSearchStore>
    {
        public CmsMainSearchStore()
        {
            Configuration.Add(STO_CONNECTIONSTRING, AppSettings.Current.ChannelConfiguration.ConnectionStrings.CmsMainSearchDb);
            Configuration.Add(STO_NAMESPACE, string.Format("{0}_{1}", AppSettings.Current.Application.Name, AppSettings.Current.ServiceConfiguration.Namespace));
        }

        static CmsMainSearchStore()
        {
            if (Current == null)
            {
                Current = new CmsMainSearchStore();
            }
        }

        public static CmsMainSearchStore Current { get; }
    }
}
