﻿using ChannelVN.IMS2.Foundation.Common.Configuration;
using ChannelVN.IMS2.Foundation.Common.DbHelper;
using ChannelVN.IMS2.Foundation.Data;
using ChannelVN.IMS2.Foundation.Data.Providers;

namespace ChannelVN.IMS2.Core.Models
{
    public class CmsMainCacheStore : Store<RedisContext, CmsMainCacheStore>
    {
        public CmsMainCacheStore()
        {
            Configuration.Add(STO_CONNECTIONSTRING, AppSettings.Current.ChannelConfiguration.ConnectionStrings.CmsMainCacheDb);
            Configuration.Add(STO_NAMESPACE, string.Format("{0}:{1}", AppSettings.Current.Application.Name, AppSettings.Current.ServiceConfiguration.Namespace));

        }



        static CmsMainCacheStore()
        {
            if(Current == null)
            {
                Current = new CmsMainCacheStore();
            }
        }
       
        protected static CmsMainCacheStore Current { get; }
    }
}
