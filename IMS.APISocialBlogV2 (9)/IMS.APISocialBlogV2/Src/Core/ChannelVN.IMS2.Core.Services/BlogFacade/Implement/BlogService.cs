﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ChannelVN.IMS2.Core.Entities.Base;
using ChannelVN.IMS2.Core.Entities.Base.Test;
using ChannelVN.IMS2.Core.Entities.ErrorCode;
using ChannelVN.IMS2.Core.Models.Models.Blogs;
using ChannelVN.IMS2.Core.Models.Models.TestTables;
using ChannelVN.IMS2.Core.Repositories.BlogRepos;
using ChannelVN.IMS2.Foundation.Common;

namespace ChannelVN.IMS2.Core.Services.BlogFacade.Implement
{
    public class BlogService : IBlogService
    {
        private readonly IBlogRepository _blogRepository;

        public BlogService(IBlogRepository blogRepository)
        {
            _blogRepository = blogRepository;
        }

        public TestTableModel GetById(string id)
        {
            return Mapper<TestTableModel>.Map<TestTable>(_blogRepository.GetById(id), new TestTableModel());
        }

        public SearchTestResult SearchTest(SearchTestModel model)
        {           
            return _blogRepository.SearchTest(model);          
        }

        public bool AddOneTest(TestTableModel model)
        {
            var res = _blogRepository.AddOneTest(model.CloneToModel<TestTableModel,TestTable>());
            return res;
        }

        public async Task<bool> AddManyTest(IEnumerable<TestTableModel> models)
        {
            var res = await _blogRepository.AddManyTest(models);
            return res;
        }

        public bool UpdateOne(TestTableModel model)
        {
            return _blogRepository.UpdateOne(Mapper<TestTable>.Map<TestTableModel>(model, new TestTable()));
        }

        public bool DeleteOne(TestTableModel model)
        {
            return _blogRepository.DeleteOne(Mapper<TestTable>.Map<TestTableModel>(model, new TestTable()));
        }

        




        public object AutoGen()
        {
            return IdParts.AutogenIdWithCodeGen(5);
        }

        public bool BulkCopy()
        {
            return _blogRepository.BulkCopy();
        }
     
        public IEnumerable<BlogModel> GetBlog()
        {
            return _blogRepository.GetBlog();
        }
       
        
        
        public IdPartModel TestDecode(string id)
        {
            var longParsed = long.Parse(id);
            return IdParts.DecomposeIdPartWithCodeGen(longParsed);
        }

       
    }
}
