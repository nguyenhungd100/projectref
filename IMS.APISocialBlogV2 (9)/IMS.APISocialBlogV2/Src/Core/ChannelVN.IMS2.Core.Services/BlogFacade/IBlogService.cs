﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using ChannelVN.IMS2.Core.Entities.ErrorCode;
using ChannelVN.IMS2.Core.Models.Models.Blogs;
using ChannelVN.IMS2.Core.Models.Models.TestTables;
using ChannelVN.IMS2.Foundation.Common;

namespace ChannelVN.IMS2.Core.Services.BlogFacade
{
    public interface IBlogService
    {
        TestTableModel GetById(string id);
        SearchTestResult SearchTest(SearchTestModel model);
        bool AddOneTest(TestTableModel model);
        Task<bool> AddManyTest(IEnumerable<TestTableModel> models);
        bool UpdateOne(TestTableModel model);
        bool DeleteOne(TestTableModel model);
          


        
        IdPartModel TestDecode(string id);
        bool BulkCopy();  
        object AutoGen();

        
    }
}
