﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ChannelVN.IMS2.Core.Models.DataStore.SQLScripts
{
    internal class Account
    {
        public const string SP_ACCOUNT_GET_BY_USERNAME = @"
        SELECT [Id]
            ,[Mobile]
            ,[Email]
            ,[UserName]
            ,[Password]
            ,[Type]
            ,[Class]
            ,[FullName]
            ,[Avatar]
            ,[Banner]
            ,[VerifiedBy]
            ,[VerifiedDate]
            ,[Status]
            ,[IsSystem]
            ,[IsFullPermission]
            ,[OpenId]
            ,[OtpSecretKey]
            ,[CreatedBy]
            ,[CreatedDate]
            ,[LoginedDate]
            ,[Description]
        FROM [dbo].[Account]
        WHERE [UserName] = @UserName";
    }
}
