﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using ChannelVN.IMS2.Core.Entities.Base;
using ChannelVN.IMS2.Core.Entities.Base.Test;
using ChannelVN.IMS2.Core.Models.Models.Blogs;
using ChannelVN.IMS2.Core.Models.Models.TestTables;

namespace ChannelVN.IMS2.Core.Models.DataStore.BlogFacade
{
    public interface IBlogDataStore
    {
        TestTable GetById(string id);
        SearchTestResult SearchTest(SearchTestModel model);
        bool AddOneTest(TestTable model);
        Task<bool> AddManyTest(IEnumerable<TestTable> models);
        bool UpdateOne(TestTable testTable);
        bool DeleteOne(TestTable testTable);

        bool BulkCopy();
        
    }
}
