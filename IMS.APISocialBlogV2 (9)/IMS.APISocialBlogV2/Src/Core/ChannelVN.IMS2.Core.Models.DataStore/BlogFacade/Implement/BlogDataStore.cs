﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using ChannelVN.IMS2.Core.Entities.Base;
using ChannelVN.IMS2.Core.Entities.Base.Test;
using ChannelVN.IMS2.Core.Models.Models.Blogs;
using ChannelVN.IMS2.Core.Models.Models.TestTables;
using ChannelVN.IMS2.Foundation.Common;
using ChannelVN.IMS2.Foundation.Common.Configuration;
using ChannelVN.IMS2.Foundation.Common.DbHelper;

namespace ChannelVN.IMS2.Core.Models.DataStore.BlogFacade.Implement
{
    public class BlogDataStore : CmsMainDataStore, IBlogDataStore
    {
        public BlogDataStore()
        {
        }


        public TestTable GetById(string id)
        {
            var idParse = long.Parse(id);
            var res = new TestTable();

            using (var context = Current.GetContext())
            {
                res = context.FindById<TestTable>(long.Parse(id));
            }
            return res;
        }

        public SearchTestResult SearchTest(Models.Blogs.SearchTestModel model)
        {
            var res = new SearchTestResult(model.PageIndex, model.PageSize);
            using (var context = Current.GetContext())
            {
                var entity = new TestTable();
                var compiler = new SqlKata.Compilers.SqlServerCompiler();
                var query = new SqlKata.Query(nameof(TestTable));

                if (!string.IsNullOrEmpty(model.Name))
                    query = query.OrWhereContains(entity.GetMemberName(t => t.Name), "'+N'" + model.Name + "'+'", true);
                if (model.Status.HasValue)               
                    query = query.Where(entity.GetMemberName(t => t.Status), model.Status);

                var queryCount = query.Clone();
                var sqlCount = compiler.Compile(queryCount.Select(entity.GetMemberName(t => t.Id)).AsCount()).ToString();

                if (model.SortDesc.HasValue && !string.IsNullOrEmpty(model.SortBy))
                {
                    if (model.SortDesc.Value)
                    {
                        query = query.OrderByDesc(model.SortBy);
                    }
                    else
                        query = query.OrderBy(model.SortBy);
                }
                
                query = query.Skip(model.PageSize * (model.PageIndex - 1)).Take(model.PageSize);
                var sqlQuery = compiler.Compile(query).ToString();
                var contextResult =  context.FindAll<TestTable>(sqlCount, sqlQuery);
                res.Records = contextResult.recordRes.ToList().CloneToListModels<TestTable, TestTableModel>();
                res.TotalRecord = contextResult.totalCount;
                return res;
            }
        }

        public bool AddOneTest(TestTable model)
        {
            var result = false;
            var randomId = IdParts.AutogenIdWithCodeGen();
            using (var context = Current.GetContext())
            {
                
                result = context.Insert(model, randomId);
            }
            return result;
        }

        public async Task<bool> AddManyTest(IEnumerable<TestTable> models)
        {
            var result = false;
            var sample = CreateSampleModel(1000);
            using (var context = Current.GetContext())
            {
                result = await context.BulkInsert(sample);
            }
            return result;
        }

        public bool UpdateOne(TestTable testTable)
        {
            var res = false;
            using (var context = Current.GetContext())
            {
                res = context.Update(testTable, testTable.Id);
            }
            return res;
        }

        public bool DeleteOne(TestTable testTable)
        {
            var res = false;
            using (var context = Current.GetContext())
            {
                res = context.Delete(testTable);
            }
            return res;
        }












        public bool BulkCopy()
        {
            throw new NotImplementedException();
        }




        //public TestTable GetById(string id)
        //{
        //    var idParse = long.Parse(id);
        //    var shardId = IdParts.DecomposeIdPartNew(idParse)?.ShardId;
        //    var dbCode = SwitchDbHelper.GetDbCodeFromId(idParse);
        //    TestTable GenerateCommand(string tableName, DbCode code)
        //    {
        //        using (var context = FromDb(code).GetContext())
        //        {
        //            var res1 = new TestTable();
        //            var cmd = context.CreateCommand($"Select * from {tableName} where id = {idParse}");
        //            var entity = context.Get<TestTable>(cmd);
        //            return entity;
        //        }
        //    }

        //    var t1 = Task.Run(() => GenerateCommand("TableTest", dbCode));
        //    //var t2 = Task.Run(() => GenerateCommand("TableTest21", DbCode.CmsMainDb2));
        //    //var t3 = Task.Run(() => GenerateCommand("TableTest31", DbCode.CmsMainDb3));
        //    //var t4 = Task.Run(() => GenerateCommand("TableTest41", DbCode.CmsMainDb4));

        //    var executeTask = Task.Run(() => Task.WhenAll(t1));
        //    return executeTask.Result.FirstOrDefault(c => c != null);
        //}

        //public async Task<bool> TestAddManyWhenGenIdAsync(IEnumerable<TestTableModel> models)
        //{
        //    bool GenerateCommand(string tableName, DbCode code)
        //    {
        //        using (var context = FromDb(code).GetContext())
        //        {                   
        //            var sqlText = new StringBuilder();
        //            var sqlLast = new StringBuilder();
        //            for (int i = 0; i < 1; i++)
        //            {
        //                sqlText.Append($"Insert into {tableName}(Name) values ");
        //                var allValues = new List<string>();
        //                for (int j = 0; j < 1000; j++)
        //                {
        //                    allValues.Add($"('Lần thứ{ j * i + j}')");
        //                }
        //                sqlText.AppendJoin(',', allValues.ToArray());
        //                sqlLast.AppendJoin(" go ", sqlText.ToString());
        //            }
        //            var cmd = context.CreateCommand(sqlText.ToString());
        //            cmd.CommandTimeout = 360;
        //            return context.ExecuteNonQuery(cmd) > 0 ? true : false;
        //        }              
        //    }

        //    var t1 = Task.Run(() => GenerateCommand("TableTest", DbCode.CmsMainDb1));
        //    var t2 = Task.Run(() => GenerateCommand("TableTest", DbCode.CmsMainDb2));
        //    var t3 = Task.Run(() => GenerateCommand("TableTest", DbCode.CmsMainDb3));
        //    var t4 = Task.Run(() => GenerateCommand("TableTest", DbCode.CmsMainDb4));

        //    var executeTask = await Task.WhenAll(t1, t2, t3, t4);
        //    return /*(executeTask.Any(c => c == false)) ? false : true;*/ true;
        //}

        //public bool TestAddWhenGenId(TestTableModel model)
        //{
        //    using (var context = FromDb(DbCode.CmsMainDb1).GetContext())
        //    {
        //        var sqlText = new StringBuilder();
        //        var sqlLast = new StringBuilder();
        //        for (int i = 0; i < 1; i++)
        //        {
        //            sqlText.Append($"Insert into TableTest(Name) values ");
        //            var allValues = new List<string>();
        //            for (int j = 0; j < 1000; j++)
        //            {
        //                allValues.Add($"('Lần thứ{ j * i + j}')");
        //            }
        //            sqlText.AppendJoin(',', allValues.ToArray());
        //            sqlLast.AppendJoin(" go ", sqlText.ToString());
        //        }
        //        var cmd = context.CreateCommand(sqlText.ToString());
        //        cmd.CommandTimeout = 360;
        //        return context.ExecuteNonQuery(cmd) > 0 ? true : false;
        //    }            
        //}


        //public bool TestAddManyInOneTable()
        //{
        //    using (var context = FromDb(DbCode.CmsMainDb1).GetContext())
        //    {
        //        var sqlText = new StringBuilder();
        //        var sqlLast = new StringBuilder();
        //        for (int i = 0; i < 10; i++)
        //        {
        //            sqlText.Append($"Insert into TableTest(Name) values ");
        //            var allValues = new List<string>();
        //            for (int j = 0; j < 1000; j++)
        //            {
        //                allValues.Add($"('Lần thứ{ j * i + j}')");
        //            }
        //            sqlText.AppendJoin(',', allValues.ToArray());
        //            sqlLast.AppendJoin(" go ", sqlText.ToString());
        //        }
        //        var cmd = context.CreateCommand(sqlText.ToString());
        //        return context.ExecuteNonQuery(cmd) > 0 ? true : false;
        //    }
        //}


        //public bool BulkCopy()
        //{
        //    var entities = CreateSampleModel(1000000);
        //    //using (var context = Current.GetContext())
        //    //{
        //    //    context.InsertDataUsingSqlBulkCopy<TestTable1>(entities, "TableTest11");
        //    //}
        //    return true;
        //}

        private static IEnumerable<TestTable> CreateSampleModel(int count)
        {
            return Enumerable.Range(0, count)
                .Select(i => new TestTable
                {
                    Name = "Lần" + i

                }); ;

        }
    }
}
