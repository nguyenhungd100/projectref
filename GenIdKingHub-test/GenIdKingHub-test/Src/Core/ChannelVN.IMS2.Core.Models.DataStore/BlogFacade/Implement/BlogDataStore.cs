﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ChannelVN.IMS2.Core.Entities.Base.Test;
using ChannelVN.IMS2.Core.Models.Models.TestTables;
using ChannelVN.IMS2.Foundation.Common;
using ChannelVN.IMS2.Foundation.Common.Configuration;
using ChannelVN.IMS2.Foundation.Common.DbHelper;

namespace ChannelVN.IMS2.Core.Models.DataStore.BlogFacade.Implement
{
    public class BlogDataStore : CmsMainDataStore, IBlogDataStore
    {
        public BlogDataStore()
        {
        }

        public TestTable GetById(string id)
        {
            var idParse = long.Parse(id);
            var res = new TestTable();
            using (var context = Current.GetContext())
            {
                
            }
            //var dbCode = SwitchDbHelper.GetDbCodeFromId(idParse);
            //TestTable GenerateCommand(string tableName, DbCode code)
            //{
            //    using (var context = FromDb(code).GetContext())
            //    {
            //        var cmd = context.CreateCommand($"Select * from  {tableName} where id ={idParse}");
            //        var entity = context.Get<TestTable>(cmd);
            //        return entity;
            //    }
            //}
            //return GenerateCommand("TableTest", dbCode);
            return res;
        }

        public Task<bool> TestAddManyWhenGenIdAsync(IEnumerable<TestTableModel> models)
        {
            throw new NotImplementedException();
        }

        public bool TestAddWhenGenId(TestTableModel model)
        {
            throw new NotImplementedException();
        }
        //public async Task<bool> TestAddManyWhenGenIdAsync(IEnumerable<TestTableModel> models)
        //{
        //    bool GenerateCommand(string tableName, DbCode code)
        //    {               
        //        using (var context = FromDb(code).GetContext())
        //        {                   
        //            var sqlText = new StringBuilder();
        //            var sqlLast = new StringBuilder();
        //            for (int i = 0; i < 10; i++)
        //            {
        //                sqlText.Append($"Insert into {tableName}(Name) values ");
        //                var allValues = new List<string>();
        //                for (int j = 0; j < 1000; j++)
        //                {
        //                    allValues.Add($"('Lần thứ{ j * i + j}')");
        //                }
        //                sqlText.AppendJoin(',', allValues.ToArray());
        //                sqlLast.AppendJoin(" go ", sqlText.ToString());
        //            }
        //            var cmd = context.CreateCommand(sqlText.ToString());
        //            cmd.CommandTimeout = 360;
        //            return context.ExecuteNonQuery(cmd) > 0 ? true : false;
        //        }
        //    }
        //    var t1 = Task.Run(() => GenerateCommand("TableTest",DbCode.CmsMainDb1));
        //    var t2 = Task.Run(() => GenerateCommand("TableTest",DbCode.CmsMainDb2));
        //    var t3 = Task.Run(() => GenerateCommand("TableTest",DbCode.CmsMainDb3));
        //    var t4 = Task.Run(() => GenerateCommand("TableTest", DbCode.CmsMainDb4));

        //    var executeTask = await Task.WhenAll(t1, t2, t3, t4);
        //    return (executeTask.Any(c => c == false)) ? false : true;
        //}

        //public bool TestAddWhenGenId(TestTableModel model)
        //{
        //    using (var context = FromDb(DbCode.CmsMainDb1).GetContext())
        //    {
        //        var sqlText = new StringBuilder();
        //        var sqlLast = new StringBuilder();
        //        for (int i = 0; i < 20; i++)
        //        {
        //            sqlText.Append($"Insert into TableTest(Name) values ");
        //            var allValues = new List<string>();
        //            for (int j = 0; j < 1000; j++)
        //            {
        //                allValues.Add($"('Lần thứ{ j * i + j}')");
        //            }
        //            sqlText.AppendJoin(',', allValues.ToArray());
        //            sqlLast.AppendJoin(" go ", sqlText.ToString());
        //        }
        //        var cmd = context.CreateCommand(sqlText.ToString());
        //        cmd.CommandTimeout = 360;
        //        return context.ExecuteNonQuery(cmd) > 0 ? true : false;
        //    }       
        //}


        //public bool TestAddManyInOneTable()
        //{
        //    using (var context = FromDb(DbCode.CmsMainDb1).GetContext())
        //    {
        //        var sqlText = new StringBuilder();
        //        var sqlLast = new StringBuilder();
        //        for (int i = 0; i < 10; i++)
        //        {
        //            sqlText.Append($"Insert into TableTest11(Name) values ");
        //            var allValues = new List<string>();
        //            for (int j = 0; j < 1000; j++)
        //            {
        //                allValues.Add($"('Lần thứ{ j * i + j}')");
        //            }
        //            sqlText.AppendJoin(',', allValues.ToArray());
        //            sqlLast.AppendJoin(" go ", sqlText.ToString());
        //        }
        //        var cmd = context.CreateCommand(sqlText.ToString());
        //        return context.ExecuteNonQuery(cmd) > 0 ? true : false;
        //    }
        //}


    }
}
