﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using ChannelVN.IMS2.Core.Entities.Base.Test;
using ChannelVN.IMS2.Core.Models.Models.TestTables;

namespace ChannelVN.IMS2.Core.Models.DataStore.BlogFacade
{
    public interface IBlogDataStore
    {
        Task<bool> TestAddManyWhenGenIdAsync(IEnumerable<TestTableModel> models);
        bool TestAddWhenGenId(TestTableModel model);
        TestTable GetById(string id);
    }
}
