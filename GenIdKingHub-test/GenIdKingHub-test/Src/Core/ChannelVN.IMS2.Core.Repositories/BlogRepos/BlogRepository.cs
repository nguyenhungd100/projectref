﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using ChannelVN.IMS2.Core.Entities.Base;
using ChannelVN.IMS2.Core.Entities.Base.Test;
using ChannelVN.IMS2.Core.Entities.ErrorCode;
using ChannelVN.IMS2.Core.Models.CacheStore.Blog;
using ChannelVN.IMS2.Core.Models.DataStore.Blog;
using ChannelVN.IMS2.Core.Models.DataStore.BlogFacade;
using ChannelVN.IMS2.Core.Models.Models.Blogs;
using ChannelVN.IMS2.Core.Models.Models.TestTables;
using ChannelVN.IMS2.Core.Models.SearchStore.Blog;
using ChannelVN.IMS2.Foundation.Common;
using ChannelVN.IMS2.Foundation.Logging;
using static ChannelVN.IMS2.Core.Entities.ErrorCode.ErrorMapping;

namespace ChannelVN.IMS2.Core.Repositories.BlogRepos
{
    public interface IBlogRepository
    {
        Task<ErrorMapping.ErrorCodes> AddBlogAsync(BlogModel model);
        bool AddBlog(BlogModel blog);
        IEnumerable<BlogModel> GetBlog();

        bool TestAddManyWhenGenId(IEnumerable<TestTableModel> models);
        bool TestAddWhenGenId(TestTableModel model);
        bool TestGenId();
        TestTable GetById(string id);
    }

    public class BlogRepository : IBlogRepository
    {
        private readonly IBlogDataStore _blogDataStore;

        public BlogRepository(IBlogDataStore blogDataStore)
        {
            _blogDataStore = blogDataStore;
        }


        public bool TestAddManyWhenGenId(IEnumerable<TestTableModel> models)
        {
            return _blogDataStore.TestAddManyWhenGenIdAsync(models).Result;
        }

        public bool TestAddWhenGenId(TestTableModel model)
        {
            return _blogDataStore.TestAddWhenGenId(model);
        }

        public async Task<ErrorMapping.ErrorCodes> AddBlogAsync(BlogModel model)
        {

            var returnValue = ErrorCodes.BusinessError;
            try
            {
                var result = BlogDataStore.FillAll();
                //if (result)
                //{
                //    returnValue = ErrorCodes.Success;
                //}
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
                returnValue = ErrorCodes.Exception;
            }

            //var returnValue = ErrorCodes.BusinessError;
            //try
            //{
            //    var blog = new Blog();
            //    var x = Mapper<Blog>.Map<BlogModel>(model, blog);
            //var result = await BlogDataStore.AddAsync(blog);
            //    if (result)
            //    {
            //        returnValue = ErrorCodes.Success;
            //    }
            //}
            //catch (Exception ex)
            //{
            //    Logger.Error(ex, ex.Message);
            //    returnValue = ErrorCodes.Exception;
            //}
            return returnValue;
        }

        public IEnumerable<BlogModel> GetBlog()
        {
            var result = BlogDataStore.FillAll();

            return null;
        }

        bool IBlogRepository.TestGenId()
        {
            throw new NotImplementedException();
        }

        public TestTable GetById(string id)
        {
            return _blogDataStore.GetById(id);
        }

        public bool AddBlog(BlogModel blog)
        {
            throw new NotImplementedException();
        }
    }
}
