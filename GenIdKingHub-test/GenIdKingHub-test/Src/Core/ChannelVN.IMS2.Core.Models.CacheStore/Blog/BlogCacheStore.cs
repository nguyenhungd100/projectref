﻿using ChannelVN.IMS2.Foundation.Logging;
using ChannelVN.IMS2.Core.Entities.Base;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using StackExchange.Redis;

namespace ChannelVN.IMS2.Core.Models.CacheStore.Blog
{
    public class BlogCacheStore : CmsMainCacheStore
    {
        public static Task<bool> AddAsync(Entities.Base.Blog  data)
        {
            var returnValue = Task.FromResult(false);

            using (var context = Current.GetContext())
            {
                try
                {
                    returnValue = context.MultiAsync((trans) =>
                    {
                        
                    });
                }
                catch (Exception ex)
                {
                    Logger.Error(ex, ex.Message);
                }
            }
            return returnValue;
        }

        public static IEnumerable<Entities.Base.Blog> GetAll()
        {
            using (var context = Current.GetContext())
            {
                
            }
            return null;
        }

        public static bool AddRedis(Entities.Base.Blog blog)
        {
            using (var context = Current.GetContext())
            {
                var x = context.SetAdd(blog.Id.ToString(),  JsonConvert.SerializeObject(blog) );
                return x;
            }
        }

        public static Entities.Base.Blog GetRedis(string id)
        {
            using (var context = Current.GetContext())
            {
                //id = "imsblogv2:vivavietnam:" + id;
                //var t = context.HashDelete("","");
                var k = context.Execute($"SPOP {id}");
                var x =JsonConvert.DeserializeObject<Entities.Base.Blog>(k.ToString());
                return x;
            }
        }
    }
}
