﻿using ChannelVN.IMS2.Core.Entities.Base;
using ChannelVN.IMS2.Foundation.Common;
using ChannelVN.IMS2.Foundation.Logging;
using StackExchange.Redis;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace ChannelVN.IMS2.Core.Models.CacheStore.Security
{
    public class AccountCacheStore : CmsMainCacheStore
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="username"></param>
        /// <returns></returns>
        public static async Task<Account> GetUserByUsernameAsync(string username)
        {
            var returnValue = default(Account);

            using (var context = Current.GetContext())
            {
                try
                {
                    var findAccountByUsernameScript = "local userId = redis.call('zscore', '" + context.NameOf("usermeta") + "', '{0}'); return redis.call('hget', '" + context.NameOf(typeof(Account).Name) + "', userId);";

                    var script = string.Format(findAccountByUsernameScript, username ?? "");
                    var value = await context.ScriptEvaluateAsync(script, null, null, CommandFlags.PreferSlave);

                    if (value != null && !value.IsNull)
                    {
                        returnValue = Json.Parse<Account>(value.ToString());
                    }
                }
                catch (Exception ex)
                {
                    Logger.Error(ex, ex.Message);
                }
            }
            return returnValue;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public static Task<bool> AddAsync(Account data)
        {
            var returnValue = Task.FromResult(false);

            using (var context = Current.GetContext())
            {
                try
                {
                    returnValue = context.MultiAsync((trans) =>
                    {
                        trans.HashSetAsync(context.NameOf(data.GetType().Name), data.Id, Json.Stringify(data));
                        trans.SortedSetAddAsync(context.NameOf(IndexKeys.KeyCreatedDate<Account>()), data.Id, (data.CreatedDate ?? DateTime.Now).ConvertToTimestamp10());
                    });
                }
                catch (Exception ex)
                {
                    Logger.Error(ex, ex.Message);
                }
            }
            return returnValue;
        }
    }
}
