﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ChannelVN.IMS2.Core.Models.CacheStore
{
    internal class IndexKeys
    {
        const string INDEX_PREFIX = "index";
        const string INDEX_SEPARATE_CHAR = ":";

        public static string KeyCreatedDate<T>() 
            => string.Join(INDEX_SEPARATE_CHAR, INDEX_PREFIX, typeof(T).Name.ToLower(), "createddate");
    }
}
