﻿namespace ChannelVN.IMS2.Core.Entities.Base
{
    public enum AccountType : byte
    {
        ReadOnly = 0,
        Personal = 1,
        Official = 2
    }
}
