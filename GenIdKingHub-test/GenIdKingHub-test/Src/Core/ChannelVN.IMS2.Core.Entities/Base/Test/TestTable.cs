﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ChannelVN.IMS2.Core.Entities.Base.Test
{
    public class TestTable
    {
        public long Id { get; set; }

        public string Name { get; set; }
    }
}
