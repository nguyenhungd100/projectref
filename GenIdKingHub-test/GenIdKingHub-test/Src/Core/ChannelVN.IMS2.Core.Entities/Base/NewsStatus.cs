﻿namespace ChannelVN.IMS2.Core.Entities.Base
{
    public enum NewsStatus
    {
        Draft = 1,
        Published = 2,
        Archive = 3,
        Remove = 4,
        Approve = 5
    }
}
