﻿namespace ChannelVN.IMS2.Core.Entities.Base
{
    public enum TemplateStatus
    {
        All = -1,
        UnActive = 0,
        Active = 1
    }
}
