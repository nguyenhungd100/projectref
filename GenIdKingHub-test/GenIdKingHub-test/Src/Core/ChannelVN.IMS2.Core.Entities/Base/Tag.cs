﻿using ChannelVN.IMS2.Foundation.Data;
using System;
using System.Collections.Generic;
using System.Text;

namespace ChannelVN.IMS2.Core.Entities.Base
{
    public class Tag : Entity
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public string UnsignName { get; set; }
        public bool? IsHotTag { get; set; }
        public string Url { get; set; }
        public string Description { get; set; }
        public long? Priority { get; set; }
        public int? Status { get; set; }
        public long? ParentId { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
    }
}
