﻿namespace ChannelVN.IMS2.Core.Entities.Newsletters.Base
{
    public enum BlogTemplate
    {
        SizeS = 1,
        SizeM = 2,
        SizeL = 3,
        Magazine = 4,
        MiniMagazine = 5,
        PhotoStory = 6
    }
}