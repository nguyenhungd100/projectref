﻿namespace ChannelVN.IMS2.Core.Entities.Base
{
    public enum NewsDataType
    {
        Webview = 0,
        Native = 1,
        AMP = 2,
        InstantView = 3
    }
}
