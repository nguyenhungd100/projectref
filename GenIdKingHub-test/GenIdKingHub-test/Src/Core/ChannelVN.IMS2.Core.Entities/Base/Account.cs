﻿using ChannelVN.IMS2.Foundation.Data;
using System;
using System.Collections.Generic;
using System.Text;

namespace ChannelVN.IMS2.Core.Entities.Base
{
    public class Account : Entity
    {
        public long Id { get; set; }
        public string EncryptId { get { return Id.ToString(); } }
        public string Mobile { get; set; }
        public string Email { get; set; }
        public string UserName { get; set; }

        public string Password { get; set; }
        public byte? Type { get; set; }
        public byte? Class { get; set; }
        public string FullName { get; set; }

        public string Avatar { get; set; }

        public string Banner { get; set; }
        public string VerifiedBy { get; set; }
        private DateTime? _VerifiedDate { get; set; }
        public DateTime? VerifiedDate
        {
            get
            {
                return _VerifiedDate;
            }
            set
            {
                _VerifiedDate = value != null ? DateTime.SpecifyKind(value.Value, DateTimeKind.Unspecified) : value;
            }
        }
        public int? Status { get; set; }

        public bool? IsSystem { get; set; }

        public bool? IsFullPermission { get; set; }

        public string OtpSecretKey { get; set; }
        private DateTime? _CreatedDate { get; set; }
        public DateTime? CreatedDate
        {
            get
            {
                return _CreatedDate;
            }
            set
            {
                _CreatedDate = value != null ? DateTime.SpecifyKind(value.Value, DateTimeKind.Unspecified) : value;
            }
        }
        private DateTime? _LoginedDate { get; set; }
        public DateTime? LoginedDate
        {
            get
            {
                return _LoginedDate;
            }
            set
            {
                _LoginedDate = value != null ? DateTime.SpecifyKind(value.Value, DateTimeKind.Unspecified) : value;
            }
        }

        public string Description { get; set; }
    }
}
