﻿namespace ChannelVN.IMS2.Core.Entities.Base
{
    public enum AccountStatus
    {
        /// <summary>
        /// Chờ duyệt
        /// </summary>
        InActived = 0,
        /// <summary>
        /// Đã duyệt
        /// </summary>
        Actived = 1,
        /// <summary>
        /// Khóa thao tác
        /// </summary>
        Locked = 2
    }
}
