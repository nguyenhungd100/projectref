﻿
namespace ChannelVN.IMS2.Core.Entities
{
    public enum LogContentType
    {
        Article = 1,
        Video = 2,
        PlayList = 3,
        MediaUnit = 4,
        Post = 5,
        Photo = 6,
        ShareLink = 7,
        Album = 8,
        Beam = 9,
        Blog = 10,
        Board = 20,
        Account = 21,
        Distribution = 22,
        Tag = 23,
        Template = 24
    }
}
