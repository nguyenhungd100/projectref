﻿using ChannelVN.IMS2.Foundation.Common;
using ChannelVN.IMS2.Foundation.Common.Configuration;
using ChannelVN.IMS2.Foundation.Common.DbHelper;
using ChannelVN.IMS2.Foundation.Data;
using ChannelVN.IMS2.Foundation.Data.Providers;
using System.Collections.Generic;
using System.Linq;

namespace ChannelVN.IMS2.Core.Models
{
    public class CmsMainDataStore : Store<MssqlContext, CmsMainDataStore>
    {
        public CmsMainDataStore()
        {
            Configuration.Add(STO_NAMESPACE, AppSettings.Current.ServiceConfiguration.Namespace);
            Configuration.Add(STO_CONNECTIONSTRINGS, AppSettings.Current.ChannelConfiguration.ConnectionStrings.CmsMainDb);

        }      

        static CmsMainDataStore()
        {
            if (Current == null)
            {
                Current = new CmsMainDataStore();
            }
        }
        protected static CmsMainDataStore Current { get; }
        //public static CmsMainDataStore FromDb(DbCode code) => new CmsMainDataStore(code);

        


    }
}
