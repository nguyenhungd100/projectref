﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ChannelVN.IMS2.Core.Models.Models.TestTables
{
    public class TestTableModel
    {
        public long Id { get; set; }

        public string Name { get; set; }
    }
}
