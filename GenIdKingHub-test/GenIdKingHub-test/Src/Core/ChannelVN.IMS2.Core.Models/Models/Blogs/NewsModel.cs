﻿using ChannelVN.IMS2.Core.Entities.Base;
using System;
using System.Collections.Generic;
using System.Text;

namespace ChannelVN.IMS2.Core.Models.Models.Blogs
{
    public class NewsModel
    {
        public long Id { get; set; }
        public int? Type { get; set; }
        public string Url { get; set; }
        public string OriginalUrl { get; set; }
        public int? Status { get; set; }
        public string Author { get; set; }
        public long? CategoryId { get; set; }

        private DateTime? _CreatedDate { get; set; }
        public DateTime? CreatedDate
        {
            get
            {
                return _CreatedDate;
            }
            set
            {
                _CreatedDate = value != null ? DateTime.SpecifyKind(value.Value, DateTimeKind.Unspecified) : value;
            }
        }

        public string CreatedBy { get; set; }
        public string Location { get; set; }
        public int? PublishMode { get; set; }
        public string PublishData { get; set; }

        private DateTime? _DistributionDate { get; set; }
        public DateTime? DistributionDate
        {
            get
            {
                return _DistributionDate;
            }
            set
            {
                _DistributionDate = value != null ? DateTime.SpecifyKind(value.Value, DateTimeKind.Unspecified) : value;
            }
        }

        private DateTime? _ModifiedDate { get; set; }
        public DateTime? ModifiedDate
        {
            get
            {
                return _ModifiedDate;
            }
            set
            {
                _ModifiedDate = value != null ? DateTime.SpecifyKind(value.Value, DateTimeKind.Unspecified) : value;
            }
        }

        public string ModifiedBy { get; set; }
        public long? DistributorId { get; set; }
        public string Tags { get; set; }
        public int? CardType { get; set; }

        public NewsSeoMeta SeoMeta { get; set; }
    }
}
