﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using ChannelVN.IMS2.Core.Entities.ErrorCode;
using ChannelVN.IMS2.Core.Models.Models.Blogs;
using ChannelVN.IMS2.Core.Models.Models.TestTables;
using ChannelVN.IMS2.Foundation.Common;

namespace ChannelVN.IMS2.Core.Services.BlogFacade
{
    public interface IBlogService
    {   
        bool TestAddWhenGenId(TestTableModel model);
        bool TestAddManyWhenGenId(IEnumerable<TestTableModel> models);
        IdPartModel TestDecode(string id);
        TestTableModel GetById(string id);
        bool AddBlog(BlogModel blog);
    }
}
