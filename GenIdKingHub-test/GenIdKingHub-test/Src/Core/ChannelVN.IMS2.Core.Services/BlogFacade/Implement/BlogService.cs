﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using ChannelVN.IMS2.Core.Entities.Base;
using ChannelVN.IMS2.Core.Entities.Base.Test;
using ChannelVN.IMS2.Core.Entities.ErrorCode;
using ChannelVN.IMS2.Core.Models.Models.Blogs;
using ChannelVN.IMS2.Core.Models.Models.TestTables;
using ChannelVN.IMS2.Core.Repositories.BlogRepos;
using ChannelVN.IMS2.Foundation.Common;

namespace ChannelVN.IMS2.Core.Services.BlogFacade.Implement
{
    public class BlogService : IBlogService
    {
        private readonly IBlogRepository _blogRepository;

        public BlogService(IBlogRepository blogRepository)
        {
            _blogRepository = blogRepository;
        }

        public bool AddBlog(BlogModel blog)
        {
            return _blogRepository.AddBlog(blog);
        }

        public async Task<ErrorMapping.ErrorCodes> AddBlogAsync(BlogModel model)
        {
            return await _blogRepository.AddBlogAsync(model);
        }

        public IEnumerable<BlogModel> GetBlog()
        {
            return _blogRepository.GetBlog();
        }

        public TestTableModel GetById(string id)
        {
            return Mapper<TestTableModel>.Map<TestTable>(_blogRepository.GetById(id), new TestTableModel());
        }

        public bool TestAddManyWhenGenId(IEnumerable<TestTableModel> models)
        {
            var res = _blogRepository.TestAddManyWhenGenId(models);
            return res;
        }

        public bool TestAddWhenGenId(TestTableModel model)
        {
            var res = _blogRepository.TestAddWhenGenId(model);
            return res;
        }

        public IdPartModel TestDecode(string id)
        {
            var longParsed = long.Parse(id);
            return IdParts.DecomposeIdPartNew(longParsed);
        }
    }
}
