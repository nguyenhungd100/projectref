﻿using ChannelVN.IMS2.Core.Entities.Base;
using ChannelVN.IMS2.Core.Repositories;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace ChannelVN.IMS2.Core.Services
{
    public static class SecurityService
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="username"></param>
        /// <returns></returns>
        public static Task<Account> GetByUsernameAsync(string username)
        {
            return SecurityRepository.GetAccountByUsernameAsync(username);
        }
    }
}
