﻿using ChannelVN.IMS2.Core.Entities.Base;
using ChannelVN.IMS2.Core.Entities.ErrorCode;
using ChannelVN.IMS2.Core.Repositories;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace ChannelVN.IMS2.Core.Services
{
    public static class BlogService
    {
        public static Task<ErrorMapping.ErrorCodes> AddAsync(Blog data)
        {
            return BlogRepository.AddAsync(data);
        }


    }
}
