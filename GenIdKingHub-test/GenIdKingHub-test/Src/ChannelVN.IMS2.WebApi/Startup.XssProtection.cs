﻿using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;
using System.Text.Encodings.Web;
using System.Text.Unicode;

namespace ChannelVN.IMS2.WebApi
{
    public static class XssProtectionExtension
    {
        public static void UseXssProtection(this IApplicationBuilder app)
        {
            app.Use(async (context, next) =>
            {
                context.Response.Headers.Add("X-Xss-Protection", "1");
                await next();
            });
        }

        public static void AddHtmlEncode(this IServiceCollection services)
        {
            services.AddSingleton(HtmlEncoder.Create(allowedRanges: new[] {
                UnicodeRanges.BasicLatin,
                UnicodeRanges.CjkUnifiedIdeographs
            }));
        }
    }
}