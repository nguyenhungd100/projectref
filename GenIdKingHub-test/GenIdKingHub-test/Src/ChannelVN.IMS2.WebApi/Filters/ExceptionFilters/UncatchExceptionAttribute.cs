﻿using ChannelVN.IMS2.Foundation.Logging;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.Extensions.Logging;
using System;

namespace ChannelVN.IMS2.WebApi.Filters.ExceptionFilters
{
    public class UncatchExceptionAttribute : IExceptionFilter
    {
        public void OnException(ExceptionContext context)
        {
            Logger.Error<Exception>("OnException {@Ex}", context.Exception);
        }
    }
}
