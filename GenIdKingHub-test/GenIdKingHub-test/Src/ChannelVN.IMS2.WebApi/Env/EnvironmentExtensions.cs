﻿using Microsoft.AspNetCore.Hosting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ChannelVN.IMS2.WebApi.Env
{
    public static class EnvironmentExtensions
    {
        #region Standard Environment Supported by .NET Core Framework

        //public static bool IsDevelopment(this IHostingEnvironment env)
        //{
        //    return env.IsEnvironment(EnvironmentNames.Development);
        //}

        //public static bool IsProduction(this IHostingEnvironment env)
        //{
        //    return env.IsEnvironment(EnvironmentNames.Production);
        //}

        //public static bool IsStaging(this IHostingEnvironment env)
        //{
        //    return env.IsEnvironment(EnvironmentNames.Staging);
        //}

        #endregion

        #region For Extentions

        public static bool IsTest(this IHostingEnvironment env)
        {
            return env.IsEnvironment(EnvironmentNames.Test);
        }

        #endregion
    }
}
