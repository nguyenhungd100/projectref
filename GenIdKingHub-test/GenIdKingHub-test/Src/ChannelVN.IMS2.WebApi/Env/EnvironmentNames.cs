﻿namespace ChannelVN.IMS2.WebApi.Env
{
    public static class EnvironmentNames
    {
        public const string Development = "Development";
        public const string Production = "Production";
        public const string Staging = "Staging";
        public const string Test = "Test";
    }
}
