﻿using ChannelVN.IMS2.Core.Entities.Base;
using ChannelVN.IMS2.Core.Models.Models.Blogs;
using ChannelVN.IMS2.Core.Models.Models.TestTables;
using ChannelVN.IMS2.Core.Services.BlogFacade;
using ChannelVN.IMS2.Foundation.Common;
using ChannelVN.IMS2.Foundation.Common.Configuration;
using ChannelVN.IMS2.Foundation.Logging;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net.Http;
using System.Security.Claims;
using System.Threading.Tasks;

namespace ChannelVN.IMS2.WebApi.Controllers
{
    [Route("api/[controller]")]
    public class BlogController : AbstractController
    {
        private readonly IBlogService _blogService;

        public BlogController(IBlogService blogService)
        {
            _blogService = blogService;
        }
        /// <summary>
        /// Thêm và Update News
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///
        ///     POST api/blog/save
        ///     {
        ///        "title": "Test tiêu đề bài viết",
        ///        "officerId": "123456"
        ///     }
        ///
        /// </remarks>  
        /// <param name="news"></param>
        /// <param name="officerId"> id tài khoản officer</param>
        /// <returns>
        /// A newly created TodoItem
        /// </returns>
        /// <response code="401">Authorization has been denied for this request</response>
        /// <response code="404">Not Found</response>
        /// <response code="405">Invalid input</response>
        [HttpPost, Route("save")]
        public IActionResult SaveAsync([FromForm]Blog news, [FromForm]long? officerId)
        {
            return Ok(ActionResponse.CreateErrorResponse("Not implementation!"));
        }


        [HttpGet("getbyid/{id}")]
        public IActionResult GetById(string id)
        {
            var result = _blogService.GetById(id);
            return Json(new { success = true, data = result });
        }

        /// <summary>
        /// Test add One
        /// </summary>
        /// <returns></returns>
        [HttpGet("testadd")]
        public IActionResult TestAddWhenGenId(/*[FromBody] TestTable1Model model*/)
        {
            var model = new TestTableModel();
            var result = _blogService.TestAddWhenGenId(model);
            return Json(new { success = result });
        }

        /// <summary>
        /// Test add Many
        /// </summary>
        /// <returns></returns>
        [HttpGet("testaddmany")]
        public IActionResult TestAddManyWhenGenId(/*[FromBody] IEnumerable<TestTable1Model> models*/)
        {
            var models = new List<TestTableModel>();
            var result = _blogService.TestAddManyWhenGenId(models);
            return Json(new { success = result });
        }

        /// <summary>
        /// Test Decode
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("testdecode/{id}")]
        public IActionResult TestDecode(string id)
        {
            var result = _blogService.TestDecode(id);
            return Json(new { success = true, data = result });
        }
     
        /// <summary>
        /// Add blog
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost("add")]
        public IActionResult AddBlog(BlogModel model)
        {
            var result = _blogService.AddBlog(model);
            return Json(new { success = result});
        }
    }
}
