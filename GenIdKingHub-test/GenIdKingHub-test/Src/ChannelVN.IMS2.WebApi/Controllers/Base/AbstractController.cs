﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace ChannelVN.IMS2.WebApi.Controllers
{
    [Authorize]
    [ApiController]
    public abstract class AbstractController : Controller
    {
        public AbstractController() 
        {
        }
    }
}
