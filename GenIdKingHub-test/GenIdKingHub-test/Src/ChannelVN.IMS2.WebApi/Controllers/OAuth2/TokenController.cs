﻿using ChannelVN.IMS2.Core.Services;
using ChannelVN.IMS2.Foundation.Common;
using ChannelVN.IMS2.Foundation.Common.Configuration;
using ChannelVN.IMS2.Foundation.Logging;
using ChannelVN.IMS2.Foundation.Security.Bearer.Helpers;
using ChannelVN.IMS2.Foundation.Security.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Globalization;
using System.Net.Http;
using System.Threading.Tasks;

namespace ChannelVN.IMS2.WebApi.Controllers.OAuth2
{
    [Produces("application/json")]
    //[Route("oauth2")]
    [AllowAnonymous]
    public class TokenController : Controller
    {
        /// <summary>
        /// Support case grant_type=password
        /// </summary>
        /// <param name="inputModel"> Require {username, password}</param>
        /// <returns></returns>
        [HttpPost, Route("oauth2/token")]
        public IActionResult Index([FromForm]LoginInputModel inputModel)
        {
            var appSettings = AppSettings.Current;
            var clientRegistry = appSettings.ServiceConfiguration.ClientRegistry;

            var t = !clientRegistry.Keys.Contains(inputModel.Password);

            if (string.IsNullOrEmpty(inputModel.Username)
                || string.IsNullOrEmpty(inputModel.Password)
                || !clientRegistry.Keys.Contains(inputModel.Password))
            {
                return Unauthorized();
            }

            var clientInfo = clientRegistry[inputModel.Password];

            var issuer = clientInfo.Issuer;
            var audience = clientInfo.Audience;
            var expiresIn = clientInfo.Expiry;

            if (string.IsNullOrEmpty(issuer))
            {
                issuer = appSettings.ServiceConfiguration.Namespace;
            }
            if (!string.IsNullOrEmpty(clientInfo.Subject))
            {
                inputModel.Username = clientInfo.Subject;
            }
            if (expiresIn <= 0)
            {
                expiresIn = appSettings.ServiceConfiguration.TokenExpiresIn;
            }

            var tokenBuilder = new JwtTokenBuilder()
                                .AddSecurityKey(JwtSecurityKey.Create(appSettings.ChannelConfiguration.SecretKey))
                                .AddSubject(inputModel.Username.ToLower())
                                .AddIssuer(issuer)
                                .AddAudience(audience)
                                .AddExpiry(expiresIn);

            if (clientInfo.Claims.Count > 0)
            {
                //var claims = new System.Collections.Generic.List<string>();
                //foreach (var keyType in clientInfo.Claims.Keys)
                //{
                //    claims.Add(string.Join(':', keyType.ToLower(), string.Join(',', clientInfo.Claims?[keyType])));
                //}
                tokenBuilder.AddClaim("aut", Foundation.Common.Json.Stringify(clientInfo.Claims)); //string.Join(';', claims.ToArray()));
            }

            var token = tokenBuilder.Build();
            return Ok(token);
        }

        /// <summary>
        /// Support case grant_type=client_credentials
        /// </summary>
        /// <param name="inputModel">Require {username, password, Headers["X-Api-Key"]}</param>
        /// <returns></returns>
        [HttpPost, Route("login")]
        public async Task<IActionResult> Login([FromForm]LoginInputModel inputModel)
        {
            var apiKey = string.Empty;
            foreach (var key in HttpContext.Request.Headers?.Keys)
            {
                if (key.ToLower().Equals(Foundation.Security.SecureParser.XApiKey.ToLower()))
                {
                    apiKey = HttpContext.Request.Headers?[key];
                    break;
                }
            }
            var appSettings = AppSettings.Current;
            var clientRegistry = appSettings.ServiceConfiguration.ClientRegistry;

            if (string.IsNullOrEmpty(inputModel.Username)
                || string.IsNullOrEmpty(inputModel.Password)
                || !clientRegistry.Keys.Contains(apiKey))
            {
                return Unauthorized();
            }

            var userInfo = await SecurityService.GetByUsernameAsync(inputModel.Username.ToLower());
            if (userInfo != null)
            {
                if (Encryption.Md5(inputModel.Password ?? "").Equals(userInfo.Password))
                {
                    var clientInfo = clientRegistry[apiKey];

                    var issuer = clientInfo.Issuer;
                    var audience = clientInfo.Audience;
                    var expiresIn = clientInfo.Expiry;

                    if (string.IsNullOrEmpty(issuer))
                    {
                        issuer = appSettings.ServiceConfiguration.Namespace;
                    }
                    if (expiresIn <= 0)
                    {
                        expiresIn = appSettings.ServiceConfiguration.TokenExpiresIn;
                    }

                    var tokenBuilder = new JwtTokenBuilder()
                                        .AddSecurityKey(JwtSecurityKey.Create(appSettings.ChannelConfiguration.SecretKey))
                                        .AddSubject(userInfo.Id.ToString())
                                        .AddIssuer(issuer)
                                        .AddAudience(audience)
                                        .AddExpiry(expiresIn);

                    if (clientInfo.Claims.Count > 0)
                    {
                        //var claims = new List<string>();
                        //foreach (var keyType in userInfo.Claims.Keys)
                        //{
                        //    claims.Add(string.Join(':', keyType.ToLower(), string.Join(',', userInfo.Claims?[keyType])));
                        //}
                        tokenBuilder.AddClaim("aut", Foundation.Common.Json.Stringify(clientInfo.Claims)); //string.Join(';', claims.ToArray()));
                    }
                    return Ok(tokenBuilder.Build());
                }
                else
                {
                    return Ok(ActionResponse<string>.CreateErrorResponse("Username or Password is wrong!"));
                }
            }
            else
            {
                return Ok(ActionResponse<string>.CreateErrorResponse("No users exist!"));
            }
        }
    }
}