﻿using System;
using System.IO;
using System.Reflection;
using System.Net;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Serilog;
using ChannelVN.IMS2.Foundation.Logging;
using ChannelVN.IMS2.WebApi.Env;
using ChannelVN.IMS2.Foundation.Common.Configuration;

namespace ChannelVN.IMS2.WebApi
{
#pragma warning disable CS1591
    public class Program
    {
        public static void Main(string[] args)
        {
            try
            {
                Logger.Create();

                CreateWebHostBuilder(args).Build().Run();
            }
            catch (Exception ex)
            {
                Logger.Fatal(ex, "Web Host terminated unexpectedly");
            }
            finally
            {
                Logger.Dispose();
            }
        }

        public static IWebHostBuilder CreateWebHostBuilder(string[] args)
        {
            return WebHost.CreateDefaultBuilder(args)
                .UseKestrel(options =>
                {
                    options.Listen(IPAddress.Any, AppSettings.Current.Server.Port);
                })
                
                .UseContentRoot(Directory.GetCurrentDirectory())
                .ConfigureAppConfiguration((hostingContext, config) =>
                {
                    var env = hostingContext.HostingEnvironment;

                    var builder = new ConfigurationBuilder()
                        .SetBasePath(Directory.GetCurrentDirectory())
                        .AddJsonFile($"appsettings.json", optional: true, reloadOnChange: true)
                        .AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: true, reloadOnChange: true);

                    var settings = new AppSettings();
                    builder.Build().Bind(settings);

                    config.AddJsonFile(
                        Path.Combine("Config", "servicesettings.json"), optional: true, reloadOnChange: true);

                    if (string.IsNullOrEmpty(settings?.ServiceConfiguration?.Namespace))
                    {
                        var channelConfigs = Directory.EnumerateDirectories("Config");
                        if (channelConfigs != null)
                        {
                            foreach (var channelConfig in channelConfigs)
                            {
                                config.AddJsonFile(
                                    Path.Combine(channelConfig, $"servicesettings.{env.EnvironmentName}.json"), optional: true, reloadOnChange: true);
                            }
                        }
                    }
                    else
                    {
                        config.AddJsonFile(
                            Path.Combine("Config", settings?.ServiceConfiguration?.Namespace, $"servicesettings.{env.EnvironmentName}.json"),
                            optional: true, reloadOnChange: true);
                    }

                    if (env.IsDevelopment() && env.IsTest())
                    {
                        var appAssembly = Assembly.Load(new AssemblyName(env.ApplicationName));
                        if (appAssembly != null)
                        {
                            config.AddUserSecrets(appAssembly, optional: true);
                        }
                    }
                    config.AddEnvironmentVariables();

                    if (args != null)
                    {
                        config.AddCommandLine(args);
                    }
                })
                .UseIISIntegration()
                .UseStartup<Startup>()
                .UseSerilog();
        }
    }
#pragma warning restore CS1591
}