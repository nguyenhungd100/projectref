﻿using ChannelVN.IMS2.Core.Models.DataStore.BlogFacade;
using ChannelVN.IMS2.Core.Models.DataStore.BlogFacade.Implement;
using ChannelVN.IMS2.Core.Repositories.BlogRepos;
using ChannelVN.IMS2.Core.Services.BlogFacade;
using ChannelVN.IMS2.Core.Services.BlogFacade.Implement;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ChannelVN.IMS2.WebApi.Config
{
    public static class DIConfiguration
    {
        public static void ConfigDI(this IServiceCollection services)
        {
            services.AddTransient<IBlogService, BlogService>();
            services.AddScoped<IBlogRepository, BlogRepository>();
            services.AddScoped<IBlogDataStore, BlogDataStore>();
        }
    }
}
