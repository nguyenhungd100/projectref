﻿using ChannelVN.IMS2.Foundation.Common;
using System.IdentityModel.Tokens.Jwt;

namespace ChannelVN.IMS2.Foundation.Security.Bearer.Helpers
{
    public sealed class JwtToken
    {
        private JwtSecurityToken token;

        internal JwtToken(JwtSecurityToken token)
        {
            this.token = token;
        }

        public string access_token => new JwtSecurityTokenHandler().WriteToken(token);
        public string token_type => "bearer";
        public double expires_in => Utility.DateTimeToUnixTimestamp(token.ValidTo);
    }
}
