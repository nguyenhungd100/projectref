﻿using ChannelVN.IMS2.Foundation.Common;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;

namespace ChannelVN.IMS2.Foundation.Security
{
    public sealed class SecureParser
    {
        /// <summary>
        /// headers["x-api-key"]
        /// </summary>
        public const string XApiKey = "X-Api-Key";

        /// <summary>
        /// 
        /// </summary>
        /// <param name="tokenValue"></param>
        /// <returns></returns>
        public static dynamic Parse(string tokenValue)
        {
            try
            {
                var tokenParts = tokenValue.Split(new char[] { '.' }, StringSplitOptions.RemoveEmptyEntries);
                var payload = Utility.Base64Decode(tokenParts[1]);
                return Json.ConvertToObject(payload);
            }
            catch
            {
                return new { };
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="user"></param>
        /// <param name="payload"></param>
        public static void BindClaims(ClaimsPrincipal user, dynamic payload)
        {
            var claims = new List<Claim>();
            if (Json.HasProperty(payload, "sub"))
            {
                var sub = Convert.ToString(payload.sub);

                claims.Add(new Claim(JwtRegisteredClaimNames.Sub, sub));
                claims.Add(new Claim(ClaimTypes.NameIdentifier, sub));
            }
            if (Json.HasProperty(payload, "iss"))
            {
                claims.Add(new Claim(JwtRegisteredClaimNames.Iss, Convert.ToString(payload.iss)));
            }
            if (Json.HasProperty(payload, "iat"))
            {
                claims.Add(new Claim(JwtRegisteredClaimNames.Iat, Convert.ToString(payload.iat)));
            }
            if (Json.HasProperty(payload, "jti"))
            {
                claims.Add(new Claim(JwtRegisteredClaimNames.Jti, Convert.ToString(payload.jti)));
            }
            if (Json.HasProperty(payload, "aud"))
            {
                claims.Add(new Claim(JwtRegisteredClaimNames.Aud, Convert.ToString(payload.aud)));
            }
            if (Json.HasProperty(payload, "aut"))
            {
                claims.Add(new Claim(JwtRegisteredClaimNames.AuthTime, Convert.ToString(payload.aut)));
            }
            if (Json.HasProperty(payload, "exp"))
            {
                claims.Add(new Claim(JwtRegisteredClaimNames.Exp, Convert.ToString(payload.exp)));
            }
            if (Json.HasProperty(payload, "idp"))
            {
                claims.Add(new Claim("idp", Convert.ToString(payload.idp)));
            }

            var identity = (ClaimsIdentity)user.Identity;
            identity.AddClaims(claims);
        }
    }
}