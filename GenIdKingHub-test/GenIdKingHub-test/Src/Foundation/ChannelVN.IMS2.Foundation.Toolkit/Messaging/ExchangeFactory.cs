﻿using ChannelVN.IMS2.Foundation.Common.Configuration;
using ChannelVN.IMS2.Foundation.Logging;
using ChannelVN.IMS2.Foundation.Toolkit.Messaging.ExchangeQueue;
using ChannelVN.IMS2.Foundation.Toolkit.Messaging.ReserveStream;
using System;

namespace ChannelVN.IMS2.Foundation.Toolkit.Messaging
{
    public class ExchangeFactory
    {
        /// <summary>
        /// Check If not exists than to create a global object.
        /// </summary>
        static ExchangeFactory()
        {
            if (_instance == null)
            {
                var connectString = AppSettings.Current.ChannelConfiguration.ConnectionStrings.CmsQueueDb;
                if (string.IsNullOrEmpty(connectString))
                {
                   // connectString = AppSettings.Current.ChannelConfiguration.ConnectionStrings.CmsMainCacheDb;
                }

                _instance = new EQ<object>(new EQOptions
                {
                    Type = ExchangeType.Direct,
                    Mode = StorageMode.Redis,
                    Engine = StorageEngine.Parse(connectString),
                    Retry = 3,
                    LIMIT_POOL_THREAD = (Environment.ProcessorCount < 1 ? 1 : Environment.ProcessorCount),
                    LIMIT_STREAM = 4294967296
                });

                _instance.OnError += (sender, e) =>
                {
                    Logger.Error($"OnError.e => {e.Error.Message}");
                };
            }
        }

        /// <summary>
        /// The queue object.
        /// </summary>
        private static EQ<object> _instance = null;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="config"></param>
        /// <param name="handler"></param>
        /// <returns></returns>
        public static Stream<object> CreateStream(EQConfiguration config, Action<Element<object>, Action<Exception>> handler)
        {
            var stream = _instance.CreateStream(config, handler);

            stream.OnRaise += (sender, e) =>
            {
                Logger.Information($"OnRaise.e => {e.Element.Id}");
            };
            stream.OnError += (sender, e) =>
            {
                Logger.Error($"OnError.e => {e.Error.Message}");
            };
            return stream;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="topic"></param>
        public static Stream<object> GetStream(string topic)
        {
            return _instance.GetStream(topic);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="message"></param>
        /// <param name="callback"></param>
        public static void Send(EQMessage<object> message, Action<Exception, int> callback = null)
        {
            _instance.Send(message, callback);
        }
    }
}
