﻿namespace ChannelVN.IMS2.Foundation.Toolkit.Messaging.ReserveStream
{
    public enum StorageMode
    {
        /// <summary>
        /// Default to store elements in RAM memory
        /// </summary>
        Memory = 0,
        /// <summary>
        /// Permiss to store elements in Redis provider
        /// </summary>
        Redis = 1
    }
}
