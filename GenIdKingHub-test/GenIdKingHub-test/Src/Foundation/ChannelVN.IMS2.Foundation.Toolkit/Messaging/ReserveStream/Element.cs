﻿using System;

namespace ChannelVN.IMS2.Foundation.Toolkit.Messaging.ReserveStream
{
    public class Element<TPayload>
    {
        public long Id { get; set; }
        public TPayload Payload { get; set; }
        public int Retry { get; set; }
        public long Pid { get; set; }
        public long Time { get; set; }

        public Element(long id, TPayload payload)
        {
            Id = id;
            Payload = payload;
            Retry = 1;
            Pid = 0;
            Time = (long)Common.Utility.DateTimeToUnixTimestamp(DateTime.UtcNow);
        }
    }
}
