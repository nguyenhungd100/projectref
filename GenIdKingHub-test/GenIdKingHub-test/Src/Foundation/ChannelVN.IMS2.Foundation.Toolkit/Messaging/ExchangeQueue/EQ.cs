﻿using ChannelVN.IMS2.Foundation.Toolkit.Messaging.ReserveStream;
using System;
using System.Collections.Generic;

namespace ChannelVN.IMS2.Foundation.Toolkit.Messaging.ExchangeQueue
{
    public class EQ<TPayload>
    {
        public EQOptions Options { get; private set; }

        private Dictionary<string, Stream<TPayload>> _stream = null;
        private Exchange<TPayload> _exchange = null;

        public event EventHandler<ErrorEventArgs> OnError;
        public event EventHandler<StartEventArgs> OnStart;
        public event EventHandler<StopEventArgs> OnStop;

        public EQ(EQOptions options = null)
        {
            Options = options ?? (new EQOptions());
            _stream = new Dictionary<string, Stream<TPayload>>();
            _exchange = new Exchange<TPayload>(_stream, Options);
        }

        public Stream<TPayload> CreateStream(EQConfiguration config, Action<Element<TPayload>, Action<Exception>> handler)
        {
            if (handler == null)
            {
                throw new Exception("You should come with a handler function.");
            }
            if (config == null)
            {
                throw new Exception("Require config must be an object.");
            }

            if (string.IsNullOrEmpty(config.Topic))
            {
                throw new Exception("Require config.Topic must be not empty.");
            }
            else
            {
                var streamId = config.Topic;
                var stream = _stream.GetValueOrDefault(streamId);

                if (stream == null)
                {
                    var options = Options.Clone() as EQOptions;
                    if(config.LIMIT_POOL_THREAD.HasValue)
                    {
                        options.LIMIT_POOL_THREAD = config.LIMIT_POOL_THREAD.Value;
                    }
                    if (config.LIMIT_STREAM.HasValue)
                    {
                        options.LIMIT_STREAM = config.LIMIT_STREAM.Value;
                    }
                    if (config.Retry.HasValue)
                    {
                        options.Retry = config.Retry.Value;
                    }
                    if (config.Recovery.HasValue)
                    {
                        options.Recovery = config.Recovery.Value;
                    }

                    stream = new Stream<TPayload>(streamId, options);
                    stream.OnError += OnError;
                    stream.OnStart += OnStart;
                    stream.OnStop += OnStop;

                    _stream.Add(streamId, stream);
                }
                stream.SetHandler(handler);
            }
            return _stream[config.Topic];
        }
        
        public void Send(EQMessage<TPayload> message, Action<Exception, int> callback = null)
        {
            var streams = _exchange.GetReserveStream(new ExchangeFilter {
                Match = message.Headers.Topic
            });

            if(streams?.Count > 0)
            {
                foreach(var stream in streams)
                {
                    stream.AddElement(message.Body, callback);
                }
            }
            else if(!string.IsNullOrEmpty(message.Headers.Group))
            {
                var group = _stream.GetValueOrDefault(message.Headers.Group);

                Stream<TPayload> stream = null;
                if(group != null)
                {
                    var options = group.Options;
                    stream = CreateStream(new EQConfiguration { Topic = message.Headers.Topic }, group.Handler);
                }

                if(stream != null)
                {
                    stream.AddElement(message.Body, callback);
                }
            }
            else
            {
                callback?.Invoke(new Exception("The stream is not exists!"), 0);
            }
        }

        public Stream<TPayload> GetStream(string topic)
            => _stream.GetValueOrDefault(topic);

        protected virtual void FnError(Exception e)
            => OnError?.Invoke(this, new ErrorEventArgs(e));

        protected virtual void FnStart()
            => OnStart?.Invoke(this, new StartEventArgs());

        protected virtual void FnStop(Element<TPayload> e)
            => OnStop?.Invoke(this, new StopEventArgs());
    }
}
