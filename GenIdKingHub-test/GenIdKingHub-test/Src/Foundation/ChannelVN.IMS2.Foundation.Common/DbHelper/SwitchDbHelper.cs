﻿using ChannelVN.IMS2.Foundation.Common.Configuration;
using ChannelVN.IMS2.Foundation.Common.DbHelper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ChannelVN.IMS2.Foundation.Common
{
    public static class SwitchDbHelper
    {       
        public static DbCode GetDbCodeFromId(long id)
        {
            var res = string.Empty;
            var idParts = IdParts.DecomposeIdPartNew(id);
            return (DbCode)idParts.ShardId;
        }

        

        public static string ChosenConnection(Dictionary<string, string> connectionStrs, DbCode code)
        {
            var res = string.Empty;
            var nameDb = Enum.GetName(typeof(DbCode), code);
            if (connectionStrs.ContainsKey(nameDb))
            {
                res = connectionStrs.FirstOrDefault(c => c.Key == nameDb).Value;
            }
            return res;
        }

        public static Dictionary<string,object> ChosenDbFromId(this IEnumerable<Dictionary<string,object>> dbCollection, long id)
        {
            var res = new Dictionary<string, object>();
            var idparts = IdParts.DecomposeIdPartNew(id);
            if (dbCollection.Count()!=0)
            {
                res = dbCollection.FirstOrDefault(c => int.Parse(c["DbCode"].ToString()) == idparts.ShardId);
            }
            return res;
        }
    } 
}
