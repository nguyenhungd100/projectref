﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ChannelVN.IMS2.Foundation.Common.DbHelper
{
    [AttributeUsage(AttributeTargets.All, Inherited = true, AllowMultiple = false)]
    public class DbDetaiAttribute : Attribute
    {
        private readonly DbGroupCode group;
        private readonly string name;

        public DbDetaiAttribute(DbGroupCode group, string name)
        {
            this.group = group;
            this.name = name;
        }

        public virtual string Name { get { return name; } }
        public virtual DbGroupCode Group { get { return group; } }
    }
}
