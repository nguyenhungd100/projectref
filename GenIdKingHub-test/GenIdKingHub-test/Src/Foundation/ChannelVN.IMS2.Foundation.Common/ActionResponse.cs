﻿using System;

namespace ChannelVN.IMS2.Foundation.Common
{
    public class ActionResponse: ActionResponse<string>
    {
        public ActionResponse() : base()
        {
            Data = "";
        }
        
        new public static ActionResponse CreateErrorResponse(int errorCode, string message = "")
        {
            return new ActionResponse
            {
                Success = false,
                Message = (string.IsNullOrEmpty(message) ? "Error!" : message),
                ErrorCode = errorCode,
                Data = ""
            };
        }

        new public static ActionResponse CreateErrorResponse(string message)
        {
            return CreateErrorResponse(9997, message);
        }

        new public static ActionResponse CreateErrorResponse(Exception ex)
        {
            return CreateErrorResponse(9998, ex.Message);
        }

        new public static ActionResponse CreateErrorResponseForInvalidRequest()
        {
            return CreateErrorResponse(9996, "Invalid request!");
        }

        new public static ActionResponse CreateSuccessResponse(string data, string message = "")
        {
            return new ActionResponse
            {
                Success = true,
                Message = string.IsNullOrEmpty(message) ? "Success!" : message,
                ErrorCode = 0,
                Data = data
            };
        }

        new public static ActionResponse CreateSuccessResponse(string message)
        {
            return new ActionResponse
            {
                Success = true,
                Message = string.IsNullOrEmpty(message) ? "Success!" : message,
                ErrorCode = 0
            };
        }

        new public static ActionResponse CreateSuccessResponse()
        {
            return CreateSuccessResponse("Success!");
        }
    }
}
