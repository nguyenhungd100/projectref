﻿using IdGen;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ChannelVN.IMS2.Foundation.Common
{
    public class IdPartModel
    {
        public string Bits { get; set; }
        public TimeSpan Times { get; set; }
        public short ShardId { get; set; }
        public long Sequence { get; set; }
        public DateTime Date { get; set; }
    }

    public static class IdParts
    {
        private static object _syncLock = new object();

        public static long AutogenIdTwitter(int generatorId)
        {
            var mark = new MaskConfig(41, 4, 19);
            var generator = new IdGenerator(generatorId, mark);
            return generator.CreateId();
        }

        public static IEnumerable<long> AutogenIdTwitter(int generatorId, int count)
        {
            var epoch = new DateTime(2019, 1, 1);
            var mark = new MaskConfig(41, 4, 18);
            var generators = (IEnumerable)new IdGenerator(generatorId).GetEnumerator();
            var ids = generators.OfType<long>().Take(count).ToArray();
            return ids;
        }

        public static IdPartModel DecomposeIdPartTwitter(long id)
        {
            try
            {
                var epoc = new DateTime(2019, 1, 1);
                var bytes = BitConverter.GetBytes(id).Reverse();
                var bits = string.Concat(bytes.Select(c => Convert.ToString((byte)c, 2).PadLeft(8, '0')));
                var timespan = TimeSpan.FromMilliseconds(Convert.ToInt64(bits.Substring(0, 42), 2));

                return new IdPartModel
                {
                    Bits = bits,
                    ShardId = Convert.ToInt16(bits.Substring(42, 4), 2),
                    Sequence = Convert.ToInt64(bits.Substring(46, 18), 2),
                    Times = timespan,
                    Date = new DateTime(timespan.Ticks + epoc.Ticks)
                };
            }
            catch (Exception)
            {
                throw;
            }            
        }

        public static IdPartModel DecomposeIdPartInsta(long id)
        {
            try
            {
                var epocNew = new DateTime(2019, 1, 1);
                var bytes = BitConverter.GetBytes(id).Reverse();
                var bits = string.Concat(bytes.Select(c => Convert.ToString((byte)c, 2).PadLeft(8, '0')));
                var timespan = TimeSpan.FromSeconds(Convert.ToInt64(bits.Substring(0, 41), 2));
                var totalMiliSeconds = (long)timespan.TotalSeconds * 1000 + (epocNew.Ticks / 10000);

                return new IdPartModel
                {
                    Bits = bits,
                    ShardId = Convert.ToInt16(bits.Substring(41, 13), 2),
                    Sequence = Convert.ToInt64(bits.Substring(54, 10), 2),
                    Times = timespan,
                    Date = new DateTime(totalMiliSeconds * 10000)
                };
            }
            catch (Exception)
            {
                throw;
            }
        }

        public static IdPartModel DecomposeIdPartNew(long id)
        {
            try
            {
                var epocNew = new DateTime(2019, 1, 1);
                var bytes = BitConverter.GetBytes(id).Reverse();
                var bits = string.Concat(bytes.Select(c => Convert.ToString((byte)c, 2).PadLeft(8, '0')));
                var timespan = TimeSpan.FromSeconds(Convert.ToInt64(bits.Substring(0, 41), 2));
                var totalMiliSeconds = (long)timespan.TotalSeconds * 1000 + (epocNew.Ticks / 10000);

                return new IdPartModel
                {
                    Bits = bits,
                    ShardId = Convert.ToInt16(bits.Substring(41, 4), 2),
                    Sequence = Convert.ToInt64(bits.Substring(45, 19), 2),
                    Times = timespan,
                    Date = new DateTime(totalMiliSeconds * 10000)
                };
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
