﻿namespace ChannelVN.IMS2.Foundation.Common.Configuration
{
    public sealed class AppSettings
    {
        public string AllowedHosts { get; set; }
        public Application Application { get; set; }
        public Server Server { get; set; }

        public ServiceConfiguration ServiceConfiguration { get; set; }
        public ChannelConfiguration ChannelConfiguration { get; set; }

        public AppSettings()
        {
            Current = this;
        }

        public static AppSettings Current { get; set; }
    }

    public sealed class Application
    {
        public string Name { get; set; }
    }

    public sealed class Server
    {
        public string IPAddress { get; set; }
        public int Port { get; set; }
    }
}
