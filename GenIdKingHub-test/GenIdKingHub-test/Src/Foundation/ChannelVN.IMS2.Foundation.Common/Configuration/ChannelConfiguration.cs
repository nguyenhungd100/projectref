﻿using System.Collections;
using System.Collections.Generic;

namespace ChannelVN.IMS2.Foundation.Common.Configuration
{
    public sealed class ChannelConfiguration
    {
        public string SecretKey { get; set; }
        public ConnectionStrings ConnectionStrings { get; set; }

        public LogActionSettings LogActionSettings { get; set; }
        public NewsVersionSettings NewsVersionSettings { get; set; }
     
        public EditorSettings EditorSettings { get; set; }
        public LoginSettings LoginSettings { get; set; }
        public MappUserIDSettings MappUserIDSettings { get; set; }
    }

    public sealed class ConnectionStrings
    {
        public IEnumerable<Dictionary<string, object>> CmsMainDb { get; set; }
        public Dictionary<string, string> CmsMainCacheDb { get; set; }
        public Dictionary<string, string> CmsMainSearchDb { get; set; }
        public string CmsQueueDb { get; set; }
    }

    public sealed class LogActionSettings
    {
        public int ApiEnabled { get; set; }
        public string ApiUrl { get; set; }
        public string ApiChannel { get; set; }
        public string ApiSecretKey { get; set; }
    }

    public sealed class NewsVersionSettings
    {
        public int ApiEnabled { get; set; }
        public string ApiUrl { get; set; }
        public string ApiChannel { get; set; }
        public string ApiSecretKey { get; set; }
    }

    public sealed class EditorSettings
    {
        public string ApiUrl { get; set; }
    }

    public sealed class LoginSettings
    {
        public string ApiUrl { get; set; }
    }

    public sealed class MappUserIDSettings
    {
        public string ApiUrl { get; set; }
    }
}

