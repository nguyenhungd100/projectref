﻿using System;

namespace ChannelVN.IMS2.Foundation.Common
{
    public static class Generator
    {
        public static long NewsId()
        {
            return KeySimple(ref seedIdNews, ref shareId);
        }

        public static long DistributionId()
        {
            return KeySimple(ref seedIdDistribution, ref shareId);
        }

        public static long TagId()
        {
            return KeySimple(ref seedIdTag, ref shareId);
        }

        public static long CategoryId()
        {
            return KeySimple(ref seedIdCategory, ref shareId);
        }

        public static long ItemStreamDistributionId()
        {
            return KeySimple(ref seedIdItemStream, ref shareId);
        }

        public static long VideoId()
        {
            return KeySimple(ref seedIdVideo, ref shareId);
        }

        public static long PlaylistId()
        {
            return KeySimple(ref seedIdPlaylist, ref shareId);
        }
        public static long BoardId()
        {
            return KeySimple(ref seedIdBoard, ref shareId);
        }
        public static long SorterSetSearchId()
        {
            return KeySimple(ref seedSorterSetSearch, ref shareId);
        }

        #region Private Methods

        private static long seedIdDistribution = 0;
        private static long seedIdNews = 0;
        private static long seedIdTag = 0;
        private static long seedIdCategory = 0;
        private static long seedIdItemStream = 0;
        private static long seedIdVideo = 0;
        private static long seedIdPlaylist = 0;
        private static long seedIdBoard = 0;
        private static long seedSorterSetSearch = 0;

        private static long shareId = 0;

        private static object _syncLock = new object();

        /// <summary>
        /// Ham ho tro sinh Key dua theo thoi gian va so tu tang.
        /// </summary>
        /// <param name="seed"></param>
        /// <returns></returns>
        private static long KeySimple(ref long seed, ref long seedDB)
        {
            lock (_syncLock)
            {
                var unixTimeNow = Utility.DateTimeToUnixTimestamp(DateTime.UtcNow, TimeSpan.TicksPerMillisecond);
                var nid = string.Format("{0}{1}{2:0000}", (long)Math.Round(unixTimeNow), ++seedDB % 10, ++seed % 10000);
                return long.Parse(nid);
            }
        }

        public static IdInfor DecodeId(long id)
        {
            var strId = id.ToString();
            return new IdInfor
            {
                Time = long.Parse(strId.Substring(0, 13)),
                ShareId = int.Parse(strId.Substring(13, 1)),
                Seed = int.Parse(strId.Substring(14, 4))
            };
        }
        #endregion
    }

    public class IdInfor {
        public long Time { get; set; }
        public int ShareId { get; set; }
        public int Seed { get; set; }
    }
}
