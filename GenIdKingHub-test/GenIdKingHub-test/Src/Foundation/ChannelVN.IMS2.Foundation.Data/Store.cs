﻿using ChannelVN.IMS2.Foundation.Common.DbHelper;
using System;

namespace ChannelVN.IMS2.Foundation.Data
{
    public abstract class Store<TContext, TStore> : IStore where TContext : class, IContext, new() where TStore : IStore, new()
    {
        /// <summary>
        /// Khởi tạo đối tượng kho lưu
        /// </summary>
        public Store() => Configuration = new ContextOptions();

        /// <summary>
        /// Nhận về một đối tượng lệnh để có thể tiến hành khai thác dữ liệu
        /// </summary>
        /// <returns></returns>
        public virtual TContext GetContext()
        {
            return Activator.CreateInstance(typeof(TContext), Configuration) as TContext;
        }

        /// <summary>
        /// Cho phép khởi tạo nhanh một đối tượng kho lưu toàn cục.
        /// </summary>
        public static TStore NewStore
        {
            get { return new TStore(); }
        }

        /// <summary>
        /// Đối tượng quản lý cấu hình truy xuất dữ liệu
        /// </summary>
        public ContextOptions Configuration { get; private set; }

        /// <summary>
        /// Ký hiệu quy ước cho tên chuỗi kết nối
        /// </summary>
        public const string STO_CONNECTIONSTRINGS = "ConnectionStrings";

        /// <summary>
        /// Ký hiệu quy ước cho tên vùng dữ liệu làm việc
        /// </summary>
        public const string STO_NAMESPACE = "Namespace";

        public DbCode DbCode { get; set; }


    }
}
