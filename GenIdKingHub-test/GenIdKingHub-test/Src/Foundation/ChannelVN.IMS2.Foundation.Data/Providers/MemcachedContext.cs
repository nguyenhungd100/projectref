﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ChannelVN.IMS2.Foundation.Data.Providers
{
    public sealed class MemcachedContext : Context
    {
        #region Constructor

        public MemcachedContext() 
        {
        }

        public MemcachedContext(ContextOptions options)
            : base(options)
        {
        }
        #endregion
    }
}
