﻿using ChannelVN.IMS2.Foundation.Common;
using ChannelVN.IMS2.Foundation.Data.Providers.SqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace ChannelVN.IMS2.Foundation.Data.Providers
{
    public sealed class MssqlContext : Context
    {
        private IDbConnection _connection = null;
        private IDbTransaction _transaction = null;

        #region Constructor

        public MssqlContext()
            : this(new ContextOptions())
        {
        }

        public MssqlContext(ContextOptions options)
            : base(options)
        {
            //GetConnection();
            var a = SwitchDbHelper.ChosenDbFromId(dbCollection: this.DbCollections, 546456465465);
            var connectionStr = a.GetValueOrDefault("ConnectionString");
        }
        #endregion

        public void ChangeConnection(string connectionStr)
        {
            Options["ConnectionString"] = connectionStr;
            GetConnection();
            if (string.IsNullOrEmpty(connectionStr))
            {
                throw new ArgumentNullException("The connection string must be not empty.");
            }

            try
            {
                _connection = new SqlConnection(ConnectionString);
                _connection.Open();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private IEnumerable<Dictionary<string,object>> DbCollections
        {
            get
            {
                return (IEnumerable<Dictionary<string, object>>)Options.GetValue("ConnectionStrings");
            }
        }
        #region Properties

        public string ConnectionString
        {
            get
            {
                //return (string)(Options.GetValue("ConnectionString")?? string.Empty);
                return null;
            }
        }
        #endregion

        #region Private methods

        /// <summary>
        /// For checking the type of the connection database.
        /// </summary>
        public bool IsAsync { get; }

        /// <summary>
        /// Initializes the database connection.
        /// </summary>
        private void GetConnection()
        {
            if (string.IsNullOrEmpty(ConnectionString))
            {
                throw new ArgumentNullException("The connection string must be not empty.");
            }
            try
            {
                //if (IsAsync)
                //{
                //    strConnection = new SqlConnectionStringBuilder(ConnectionString)
                //    {
                //        AsynchronousProcessing = true
                //    }.ToString();
                //}
                if (_connection == null || _connection.State != ConnectionState.Open)
                {
                    _connection = new SqlConnection(ConnectionString);
                    _connection.Open();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region IDisposable implementation

        /// <summary>
        /// Rolls back any pending transactions and closes the DB connection.
        /// </summary>
        public override void Dispose()
        {
            Close();
        }
        #endregion

        #region Public methods

        /// <summary>
        /// Creates and returns a new <see cref="System.Data.IDbCommand"/> object.
        /// </summary>
        /// <param name="sqlText">The text of the query.</param>
        /// <returns>An <see cref="System.Data.IDbCommand"/> object.</returns>
        public IDbCommand CreateCommand(string sqlText)
        {
            return CreateCommand(sqlText, false);
        }

        /// <summary>
        /// Creates and returns a new <see cref="System.Data.IDbCommand"/> object.
        /// </summary>
        /// <param name="sqlText">The text of the query.</param>
        /// <param name="procedure">Specifies whether the sqlText parameter is 
        /// the name of a stored procedure.</param>
        /// <returns>An <see cref="System.Data.IDbCommand"/> object.</returns>
        public IDbCommand CreateCommand(string sqlText, bool procedure)
        {
            IDbCommand cmd = _connection.CreateCommand();
            cmd.CommandText = sqlText;
            cmd.Transaction = _transaction;
            if (procedure)
            {
                cmd.CommandType = CommandType.StoredProcedure;
            }
            return cmd;
        }

        /// <summary>
        /// Creates and returns a new <see cref="System.Data.IDbCommand"/> object.
        /// </summary>
        /// <param name="commandType"></param>
        /// <param name="commandText"></param>
        /// <param name="parameters"></param>
        /// <returns>An <see cref="System.Data.IDbCommand"/> object.</returns>
        public IDbCommand CreateCommand(CommandType commandType, string commandText, params SqlParameter[] parameters)
        {
            IDbCommand cmd = _connection.CreateCommand();
            cmd.CommandType = commandType;
            cmd.CommandText = commandText;
            cmd.Transaction = _transaction;
            foreach (var parameter in parameters)
            {
                AddParameter(cmd, parameter.ParameterName, parameter.Value, parameter.Direction);
            }
            return cmd;
        }

        /// <summary>
        /// Adds a new parameter to the specified command. It is not recommended that 
        /// you use this method directly from your custom code. Instead use the 
        /// <c>AddParameter</c> method of the CmsMainDbBase classes.
        /// </summary>
        /// <param name="cmd">The <see cref="System.Data.IDbCommand"/> object to add the parameter to.</param>
        /// <param name="paramName">The name of the parameter.</param>
        /// <param name="value">The value of the parameter.</param>
        /// <returns>A reference to the added parameter.</returns>
        public IDbDataParameter AddParameter(IDbCommand cmd, string paramName, object value)
        {
            IDbDataParameter parameter = cmd.CreateParameter();
            parameter.ParameterName = CreateCollectionParameterName(paramName);
            if (value is DateTime)
            {
                parameter.Value = (DateTime.MinValue == Utility.ConvertToDateTime(value) ? DBNull.Value : value);
            }
            else
            {
                parameter.Value = (value ?? DBNull.Value);
            }
            cmd.Parameters.Add(parameter);
            return parameter;
        }

        /// <summary>
        /// Adds a new parameter to the specified command. It is not recommended that 
        /// you use this method directly from your custom code. Instead use the 
        /// <c>AddParameter</c> method of the CmsMainDbBase classes.
        /// </summary>
        /// <param name="cmd">The <see cref="System.Data.IDbCommand"/> object to add the parameter to.</param>
        /// <param name="paramName">The name of the parameter.</param>
        /// <param name="value">The value of the parameter.</param>
        /// <param name="paraDirection">The direction of the parameter.</param>
        /// <returns>A reference to the added parameter.</returns>
        public IDbDataParameter AddParameter(IDbCommand cmd, string paramName, object value, ParameterDirection paraDirection)
        {
            IDbDataParameter parameter = cmd.CreateParameter();
            parameter.ParameterName = CreateCollectionParameterName(paramName);
            if (value is DateTime)
            {
                parameter.Value = (DateTime.MinValue == Utility.ConvertToDateTime(value) ? DBNull.Value : value);
            }
            else
            {
                parameter.Value = (value ?? DBNull.Value);
            }
            parameter.Direction = paraDirection;
            cmd.Parameters.Add(parameter);
            return parameter;
        }

        /// <summary>
        /// Returns a SQL statement parameter name that is specific for the data provider.
        /// For example it returns ? for OleDb provider, or @paramName for MS SQL provider.
        /// </summary>
        /// <param name="paramName">The data provider neutral SQL parameter name.</param>
        /// <returns>The SQL statement parameter name.</returns>
        private string CreateSqlParameterName(string paramName)
        {
            return "@" + paramName;
        }

        /// <summary>
        /// Creates a .Net data provider specific name that is used by the 
        /// <see cref="AddParameter"/> method.
        /// </summary>
        /// <param name="baseParamName">The base name of the parameter.</param>
        /// <returns>The full data provider specific parameter name.</returns>
        private string CreateCollectionParameterName(string baseParamName)
        {
            return "@" + baseParamName;
        }

        /// <summary>
        /// Creates <see cref="System.Data.IDataReader"/> for the specified DB command.
        /// </summary>
        /// <param name="command">The <see cref="System.Data.IDbCommand"/> object.</param>
        /// <returns>A reference to the <see cref="System.Data.IDataReader"/> object.</returns>
        public IDataReader ExecuteReader(IDbCommand command)
        {
            return command.ExecuteReader();
        }

        public Task<SqlDataReader> ExecuteReaderAsync(IDbCommand command)
        {
            return (command as SqlCommand)?.ExecuteReaderAsync();
        }

        /// <summary>
        /// Creates <see cref="System.Data.IDataReader"/> for the specified DB command.
        /// </summary>
        /// <param name="commandType"></param>
        /// <param name="commandText"></param>
        /// <param name="parameters"></param>
        /// <returns>A reference to the <see cref="System.Data.IDataReader"/> object.</returns>
        public IDataReader ExecuteReader(CommandType commandType, string commandText, params SqlParameter[] parameters)
        {
            var command = CreateCommand(commandType, commandText, parameters);
            return command.ExecuteReader();
        }

        public Task<SqlDataReader> ExecuteReaderAsync(CommandType commandType, string commandText, params SqlParameter[] parameters)
        {
            var command = CreateCommand(commandType, commandText, parameters) as SqlCommand;
            return command.ExecuteReaderAsync();
        }

        /// <summary>
        /// Creates <see cref="System.Data.IDataReader"/> for the specified DB command.
        /// </summary>
        /// <param name="command">The <see cref="System.Data.IDbCommand"/> object.</param>
        /// <returns>A reference to the <see cref="System.Data.IDataReader"/> object.</returns>
        public int ExecuteNonQuery(IDbCommand command)
        {
            return command.ExecuteNonQuery();
        }

        public Task<int> ExecuteNonQueryAsync(IDbCommand command)
        {
            return (command as SqlCommand)?.ExecuteNonQueryAsync();
        }

        /// <summary>
        /// Creates <see cref="System.Data.IDataReader"/> for the specified DB command.
        /// </summary>
        /// <param name="commandType"></param>
        /// <param name="commandText"></param>
        /// <param name="parameters"></param>
        /// <returns>A reference to the <see cref="System.Data.IDataReader"/> object.</returns>
        public int ExecuteNonQuery(CommandType commandType, string commandText, params SqlParameter[] parameters)
        {
            var command = CreateCommand(commandType, commandText, parameters);
            return command.ExecuteNonQuery();
        }

        public Task<int> ExecuteNonQueryAsync(CommandType commandType, string commandText, params SqlParameter[] parameters)
        {
            var command = CreateCommand(commandType, commandText, parameters) as SqlCommand;
            return command.ExecuteNonQueryAsync();
        }

        /// <summary>
        /// Map records from the DataReader
        /// </summary>
        /// <param name="reader">The <see cref="System.Data.IDataReader"/> object.</param>
        /// <returns>Entity of records.</returns>
        private T MapRecord<T>(IDataReader reader)
        {
            return EntityMapper.FillObject<T>(reader);
        }

        /// <summary>
        /// Map records from the DataReader
        /// </summary>
        /// <param name="command">The <see cref="System.Data.IDbCommand"/> command.</param>
        /// <returns>Entity of records.</returns>
        public T Get<T>(IDbCommand command)
        {
            T returnValue = default(T);
            using (IDataReader reader = ExecuteReader(command))
            {
                returnValue = MapRecord<T>(reader);
            }
            return returnValue;
        }

        public async Task<T> GetAsync<T>(IDbCommand command)
        {
            T returnValue = default(T);
            using (DbDataReader reader = await ExecuteReaderAsync(command as SqlCommand))
            {
                returnValue = MapRecord<T>(reader);
            }
            return returnValue;
        }

        /// <summary>
        /// Map records from the DataReader
        /// </summary>
        /// <param name="reader">The <see cref="System.Data.IDataReader"/> object.</param>
        /// <returns>List entity of records.</returns>
        private List<T> MapRecords<T>(IDataReader reader)
        {
            return EntityMapper.FillCollection<T>(reader);
        }

        /// <summary>
        /// Map records from the DataReader
        /// </summary>
        /// <param name="command">The <see cref="System.Data.IDbCommand"/> command.</param>
        /// <returns>List entity of records.</returns>
        public List<T> GetList<T>(IDbCommand command)
        {
            List<T> returnValue = new List<T>();
            using (IDataReader reader = ExecuteReader(command))
            {
                returnValue = MapRecords<T>(reader);
            }
            return returnValue;
        }

        public async Task<List<T>> GetListAsync<T>(IDbCommand command)
        {
            List<T> returnValue = new List<T>();
            using (DbDataReader reader = await ExecuteReaderAsync(command as SqlCommand))
            {
                returnValue = MapRecords<T>(reader);
            }
            return returnValue;
        }

        /// <summary>
        /// Map records from the DataReader
        /// </summary>
        /// <param name="command">The <see cref="System.Data.IDbCommand"/> command.</param>
        /// <returns>List entity of records.</returns>
        public List<T> GetListGenericType<T>(IDbCommand command)
        {
            List<T> returnValue = new List<T>();
            using (IDataReader reader = ExecuteReader(command))
            {
                while (reader.Read())
                {
                    returnValue.Add((T)reader[0]);
                }
            }
            return returnValue;
        }

        public async Task<List<T>> GetListGenericTypeAsync<T>(IDbCommand command)
        {
            List<T> returnValue = new List<T>();
            using (DbDataReader reader = await ExecuteReaderAsync(command as SqlCommand))
            {
                while (await reader.ReadAsync())
                {
                    returnValue.Add((T)reader[0]);
                }
            }
            return returnValue;
        }

        public object GetFirstDataRecord(IDbCommand command)
        {
            object returnValue = null;
            using (IDataReader reader = ExecuteReader(command))
            {
                if (reader.Read())
                {
                    returnValue = reader[0];
                }
            }
            return returnValue;
        }

        public async Task<object> GetFirstDataRecordAsync(IDbCommand command)
        {
            object returnValue = null;
            using (DbDataReader reader = await ExecuteReaderAsync(command as SqlCommand))
            {
                if (await reader.ReadAsync())
                {
                    returnValue = reader[0];
                }
            }
            return returnValue;
        }

        /// <summary>
        /// Gets <see cref="System.Data.IDbConnection"/> associated with this object.
        /// </summary>
        /// <value>A reference to the <see cref="System.Data.IDbConnection"/> object.</value>
        public IDbConnection Connection
        {
            get { return _connection; }
        }

        /// <summary>
        /// Begins a new database transaction.
        /// </summary>
        /// <seealso cref="CommitTransaction"/>
        /// <seealso cref="RollbackTransaction"/>
        /// <returns>An object representing the new transaction.</returns>
        public IDbTransaction BeginTransaction()
        {
            CheckTransactionState(false);
            _transaction = _connection.BeginTransaction();
            return _transaction;
        }

        /// <summary>
        /// Begins a new database transaction with the specified 
        /// transaction isolation level.
        /// <seealso cref="CommitTransaction"/>
        /// <seealso cref="RollbackTransaction"/>
        /// </summary>
        /// <param name="isolationLevel">The transaction isolation level.</param>
        /// <returns>An object representing the new transaction.</returns>
        public IDbTransaction BeginTransaction(IsolationLevel isolationLevel)
        {
            CheckTransactionState(false);
            _transaction = _connection.BeginTransaction(isolationLevel);
            return _transaction;
        }

        /// <summary>
        /// Commits the current database transaction.
        /// <seealso cref="BeginTransaction"/>
        /// <seealso cref="RollbackTransaction"/>
        /// </summary>
        public void CommitTransaction()
        {
            CheckTransactionState(true);
            _transaction.Commit();
            _transaction = null;
        }

        /// <summary>
        /// Rolls back the current transaction from a pending state.
        /// <seealso cref="BeginTransaction"/>
        /// <seealso cref="CommitTransaction"/>
        /// </summary>
        public void RollbackTransaction()
        {
            CheckTransactionState(true);
            _transaction.Rollback();
            _transaction = null;
        }

        /// <summary>
        /// Checks the state of the current transaction
        /// </summary>
        /// <param name="mustBeOpen"></param>
        private void CheckTransactionState(bool mustBeOpen)
        {
            if (mustBeOpen)
            {
                if (null == _transaction)
                    throw new InvalidOperationException("Transaction is not open.");
            }
            else
            {
                if (null != _transaction)
                    throw new InvalidOperationException("Transaction is already open.");
            }
        }

        public object GetParameterValueFromCommand(IDbCommand command, int paramterIndex)
        {
            var parameter = command.Parameters[paramterIndex] as SqlParameter;
            return parameter?.Value;
        }

        /// <summary>
        /// Rolls back any pending transactions and closes the DB connection.
        /// An application can call the <c>Close</c> method more than
        /// one time without generating an exception.
        /// </summary>
        public void Close()
        {
            if (_connection != null && _connection.State != ConnectionState.Closed)
            {
                _connection.Close();
                _connection = null;
            }
        }
        #endregion
    }
}
