﻿using System.Collections.Generic;

namespace ChannelVN.IMS2.Foundation.Data.Providers.ESClient
{
    public class ESClientEntity
    {
        public class SearchResult<T>
        {
            public long Total { get; set; }
            public int Page { get; set; }
            public IEnumerable<T> Data { get; set; }
            public long ElapsedMilliseconds { get; set; }
        }

        public class CountStatus
        {
            public string Key { get; set; }
            public long? DocCount { get; set; }
        }

        public class CountDateStatus
        {
            public string Key { get; set; }
            public int DocCount1 { get; set; }
            public int DocCount2 { get; set; }
        }
    }
}
