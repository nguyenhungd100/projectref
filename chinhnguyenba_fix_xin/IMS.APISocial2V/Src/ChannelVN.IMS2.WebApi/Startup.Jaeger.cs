﻿using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using OpenTracing;
using OpenTracing.Util;
using ChannelVN.IMS2.Foundation.Common.Configuration;

namespace ChannelVN.IMS2.WebApi
{
    public partial class Startup
    {
        private void ConfigureJaeger(IServiceCollection services)
        {
            var appSettings = AppSettings.Current;

            if (appSettings.EnableTracing)
            {
                services.AddSingleton<ITracer>(serviceProvider =>
                {
                    ILoggerFactory loggerFactory = serviceProvider.GetRequiredService<ILoggerFactory>();

                    var jagerConfig = Configuration.GetSection("Jaeger");
                    var tracer = Jaeger.Configuration.FromIConfiguration(loggerFactory, jagerConfig).GetTracer();

                    // Allows code that can't use DI to also access the tracer.
                    GlobalTracer.Register(tracer);

                    return tracer;
                });

                services.AddOpenTracing();
            }
        }
    }
}