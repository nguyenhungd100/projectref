﻿(function () {    
    var title = document.getElementsByTagName('title')[0];
    title.text = "KingHub API ChannelVN";

    var icon = document.getElementsByTagName('link');
    for (let i = 0; i < icon.length; i++) {
        if (icon[i].getAttribute('rel') === 'icon')
            icon[i].href = "/swagger-ui/icon.png";
    }    
})();