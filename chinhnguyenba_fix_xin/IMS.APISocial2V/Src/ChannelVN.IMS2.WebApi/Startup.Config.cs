﻿using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Configuration;

namespace ChannelVN.IMS2.WebApi
{
    public static class ConfigurationExtension
    {
        public static void LoadAppSettings(this IConfiguration configuration)
        {
            var appsettings = Foundation.Common.Configuration.AppSettings.Current;
            configuration.Bind(appsettings);
        }
    }
}