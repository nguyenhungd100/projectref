﻿using ChannelVN.IMS2.Foundation.Logging;
using Microsoft.AspNetCore.Mvc.Filters;

namespace ChannelVN.IMS2.WebApi.Filters.ExceptionFilters
{
    public class UncatchExceptionAttribute : IExceptionFilter
    {
        public void OnException(ExceptionContext context)
        {
            Logger.Error("OnException {@Ex}", context.Exception);
        }
    }
}
