﻿using ChannelVN.IMS2.Foundation.Common;
using ChannelVN.IMS2.Foundation.Common.Configuration;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Controllers;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Web;

namespace ChannelVN.IMS2.WebApi.Filters.ActionFilters
{
    public class ValidateModelAttribute : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext context)
        {
            if (!context.ModelState.IsValid)
            {
                context.Result = new ValidationFailedResult(context.ModelState);
            }

            base.OnActionExecuting(context);
        }

        public override void OnActionExecuted(ActionExecutedContext context)
        {
            if (context.Result is UnauthorizedResult)
            {
                context.Result = new ValidationUnauthorizedResult((context.Result as StatusCodeResult).StatusCode);
            }

            base.OnActionExecuted(context);
        }
    }

    #region ValidationUnauthorizedResult

    internal class ValidationUnauthorizedResult : ObjectResult
    {
        public string Message { get; }

        public ValidationUnauthorizedResult(int statusCode)
            : base(new
            {
                message = "The secret key are invalid."
            })
        {
            StatusCode = statusCode;
        }
    }
    #endregion

    #region ValidationFailedResult

    internal class ValidationFailedResult : ObjectResult
    {
        public ValidationFailedResult(ModelStateDictionary modelState)
            : base(new ValidationResultModel(modelState))
        {
            StatusCode = StatusCodes.Status200OK;
        }
    }

    internal class ValidationResultModel
    {
        public string Message { get; }

        public List<ValidationError> Errors { get; }

        public ValidationResultModel(ModelStateDictionary modelState)
        {
            Message = "Validation failed";
            Errors = modelState.Keys
                    .SelectMany(key => modelState[key].Errors.Select(x => new ValidationError(key, x.ErrorMessage)))
                    .ToList();
        }
    }

    internal class ValidationError
    {
        [Newtonsoft.Json.JsonProperty(NullValueHandling = Newtonsoft.Json.NullValueHandling.Ignore)]
        public string Field { get; }

        public string Message { get; }

        public ValidationError(string field, string message)
        {
            Field = field != string.Empty ? field : null;
            Message = message;
        }
    }
    #endregion

    #region Custome DateTime Model

    public class DateTimeModelBinder : IModelBinder
    {
        public Task BindModelAsync(ModelBindingContext bindingContext)
        {
            if (bindingContext == null)
            {
                throw new ArgumentNullException(nameof(bindingContext));
            }

            if (bindingContext.ModelType != typeof(DateTime?))
            {
                return Task.CompletedTask;
            }

            var modelName = GetModelName(bindingContext);

            var valueProviderResult = bindingContext.ValueProvider.GetValue(modelName);
            if (valueProviderResult == ValueProviderResult.None)
            {
                return Task.CompletedTask;
            }

            bindingContext.ModelState.SetModelValue(modelName, valueProviderResult);

            var dateToParse = valueProviderResult.FirstValue?.Trim();

            if (string.IsNullOrEmpty(dateToParse))
            {
                return Task.CompletedTask;
            }

            var dateTime = ParseDate(bindingContext, dateToParse);

            bindingContext.Result = ModelBindingResult.Success(dateTime);

            return Task.CompletedTask;
        }

        private DateTime? ParseDate(ModelBindingContext bindingContext, string dateToParse)
        {
            var attribute = GetDateTimeModelBinderAttribute(bindingContext);
            var dateFormat = attribute?.DateFormat;

            if (string.IsNullOrEmpty(dateFormat))
            {
                return ParseDateTime(dateToParse);
            }

            return ParseDateTime(dateToParse, new string[] { dateFormat });
        }

        private DateTimeModelBinderAttribute GetDateTimeModelBinderAttribute(ModelBindingContext bindingContext)
        {
            var modelName = GetModelName(bindingContext);

            var paramDescriptor = bindingContext.ActionContext.ActionDescriptor.Parameters
                .Where(x => x.ParameterType == typeof(DateTime?))
                .Where((x) =>
                {
                    var paramModelName = x.BindingInfo?.BinderModelName ?? x.Name;
                    return paramModelName.Equals(modelName);
                })
                .FirstOrDefault();

            var ctrlParamDescriptor = paramDescriptor as ControllerParameterDescriptor;
            if (ctrlParamDescriptor == null)
            {
                return null;
            }

            var attribute = ctrlParamDescriptor.ParameterInfo
                .GetCustomAttributes(typeof(DateTimeModelBinderAttribute), false)
                .FirstOrDefault();

            return (DateTimeModelBinderAttribute)attribute;
        }

        private string GetModelName(ModelBindingContext bindingContext)
        {
            if (!string.IsNullOrEmpty(bindingContext.BinderModelName))
            {
                return bindingContext.BinderModelName;
            }

            return bindingContext.ModelName;
        }

        public DateTime? ParseDateTime(
            string dateToParse,
            string[] formats = null,
            IFormatProvider provider = null,
            DateTimeStyles styles = DateTimeStyles.AssumeLocal)
        {
            var CUSTOM_DATE_FORMATS = new string[]
                {
                "dd-MM-yyyy",
                "dd-MM-yyyy HH:mm",
                "dd-MM-yyyy HH:mm:ss",
                "dd/MM/yyyy",
                "dd/MM/yyyy HH:mm",
                "yyyy-MM-ddTHH:mm:ssZ"
                };

            if (formats == null)
            {
                formats = CUSTOM_DATE_FORMATS;
            }

            DateTime validDate;

            foreach (var format in formats)
            {
                if (format.EndsWith("Z") || format.EndsWith("z"))
                {
                    if (DateTime.TryParseExact(dateToParse,
                        format,
                        provider?? CultureInfo.CurrentCulture, // ?? CultureInfo.InvariantCulture,
                        DateTimeStyles.AdjustToUniversal,
                        out validDate))
                    {
                        return validDate;
                    }
                }

                if (DateTime.TryParseExact(dateToParse,
                    format,
                    provider, // ?? CultureInfo.InvariantCulture,
                    styles,
                    out validDate))
                {
                    return validDate;
                }
            }

            return null;
        }
    }

    public class DateTimeModelBinderAttribute : ModelBinderAttribute
    {
        public string DateFormat { get; set; }

        public DateTimeModelBinderAttribute()
            : base(typeof(DateTimeModelBinder))
        {
        }
    }

    public class DateTimeModelBinderProvider : IModelBinderProvider
    {
        public IModelBinder GetBinder(ModelBinderProviderContext context)
        {
            if (context == null)
            {
                throw new ArgumentNullException(nameof(context));
            }

            if (context.Metadata.ModelType == typeof(DateTime?))
            {
                return new DateTimeModelBinder();
            }

            return null;
        }
    }
    #endregion

    public class StringModelBinder : IModelBinder
    {
        private List<string> FieldSpecial = new List<string>() { "body" };
        public Task BindModelAsync(ModelBindingContext bindingContext)
        {
            if (bindingContext == null)
            {
                throw new ArgumentNullException(nameof(bindingContext));
            }

            if (bindingContext.ModelType == typeof(string))
            {
                var modelName = GetModelName(bindingContext);

                var valueProviderResult = bindingContext.ValueProvider.GetValue(modelName);
                if (valueProviderResult == ValueProviderResult.None)
                {
                    return Task.CompletedTask;
                }

                bindingContext.ModelState.SetModelValue(modelName, valueProviderResult);

                var strHtml = valueProviderResult.FirstValue;

                if (string.IsNullOrEmpty(strHtml))
                {
                    return Task.CompletedTask;
                }

                //var str = string.Empty;

                //str = strHtml;// strHtml.RemoveScriptTag()?.Decoder()?.RemoveScriptTag();
                //if (!FieldSpecial.Contains(bindingContext.FieldName.ToLower()))
                //{
                //    str = strHtml.RemoveStrHtmlTags()?.Decoder()?.RemoveStrHtmlTags();
                //}
                //else
                //{
                //    str = strHtml.RemoveScriptTag()?.Decoder()?.RemoveScriptTag();
                //}

                bindingContext.Result = ModelBindingResult.Success(strHtml?.Trim());
            }

            return Task.CompletedTask;
            //if (bindingContext == null)
            //{
            //    throw new ArgumentNullException(nameof(bindingContext));
            //}

            //if (bindingContext.ModelType == typeof(string))
            //{
            //    var modelName = GetModelName(bindingContext);

            //    var valueProviderResult = bindingContext.ValueProvider.GetValue(modelName);
            //    if (valueProviderResult == ValueProviderResult.None)
            //    {
            //        return Task.CompletedTask;
            //    }

            //    bindingContext.ModelState.SetModelValue(modelName, valueProviderResult);

            //    var strHtml = valueProviderResult.FirstValue;

            //    if (string.IsNullOrEmpty(strHtml))
            //    {
            //        return Task.CompletedTask;
            //    }

            //    //var a = bindingContext.ActionContext.HttpContext.Request.Path;
            //    var str = strHtml?.Decoder();
            //    var specialProp = AppSettings.Current.ChannelConfiguration.RemoveXSSConfig.SpecialPropertyValidate;
            //    var prop = specialProp?.FirstOrDefault(p => p.Name.ToLower().Equals(bindingContext.FieldName.ToLower()));
            //    if (prop == null)
            //    {
            //        str = strHtml.ClearAllHtml();
            //    }
            //    else
            //    {
            //        if (prop.Type.Equals("json"))
            //        {
            //            var obj = Newtonsoft.Json.JsonConvert.DeserializeObject(str);
            //            str = Json.Stringify(Utility.ClearObjectJson(obj));
            //        }
            //        if (prop.Type.Equals("text"))
            //        {
            //            str = str.ClearSpecialField();
            //        }
            //    }

            //    bindingContext.Result = ModelBindingResult.Success(str);
            //}

            //return Task.CompletedTask;
        }

        private string GetModelName(ModelBindingContext bindingContext)
        {
            if (!string.IsNullOrEmpty(bindingContext.BinderModelName))
            {
                return bindingContext.BinderModelName;
            }

            return bindingContext.ModelName;
        }
    }
    public class StringModelBinderProvider : IModelBinderProvider
    {
        public IModelBinder GetBinder(ModelBinderProviderContext context)
        {
            if (context == null)
            {
                throw new ArgumentNullException(nameof(context));
            }

            if (context.Metadata.ModelType == typeof(string))
            {
                return new StringModelBinder();
            }

            return null;
        }
    }
}
