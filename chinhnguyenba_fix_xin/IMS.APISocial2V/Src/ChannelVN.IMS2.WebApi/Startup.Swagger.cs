﻿using Microsoft.Extensions.DependencyInjection;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using System;
using System.Linq;
using Swashbuckle.AspNetCore.Swagger;
using System.Collections.Generic;
using System.Reflection;
using System.IO;

namespace ChannelVN.IMS2.WebApi
{
    public partial class Startup
    {
        private void ConfigureSwaggerUI(IServiceCollection services)
        {
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new Info {
                    Version = "v1",
                    Title = "KingHub API ChannelVN",
                    //Description = ".NET Core Web API",
                    //TermsOfService = "KingHub",
                    Contact = new Contact
                    {
                        Name = "CHINHNB",
                        Email = "chinhnguyenba@channelvn.net",
                        Url = "https://www.facebook.com/bachinh.vn"
                    },
                    //License = new License
                    //{
                    //    Name = "License",
                    //    Url = "https://www.facebook.com/bachinh.vn"
                    //}
                });
                c.AddSecurityDefinition(
                    JwtBearerDefaults.AuthenticationScheme,
                    new ApiKeyScheme
                    {
                        In = "Header",
                        Description = "Create and Copy token into input value",
                        Name = "Authorization",
                        Type = "apiKey"
                    });
                c.AddSecurityRequirement(new Dictionary<string, IEnumerable<string>> {
                    { "Bearer", Enumerable.Empty<string>() },
                });                
                // Set the comments path for the Swagger JSON and UI.
                var xmlFile = $"{Assembly.GetExecutingAssembly().GetName().Name}.xml";
                var xmlPath = Path.Combine(AppContext.BaseDirectory, xmlFile);
                c.IncludeXmlComments(xmlPath);
            });
        }
    }
}