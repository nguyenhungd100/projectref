﻿using ChannelVN.IMS2.Core.Entities.NewsOnHome;
using ChannelVN.IMS2.Core.Services;
using ChannelVN.IMS2.Foundation.Common;
using ChannelVN.IMS2.Foundation.Common.Configuration;
using ChannelVN.IMS2.Foundation.Logging;
using ChannelVN.IMS2.Foundation.Security;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Security.Claims;
using System.Threading.Tasks;

namespace ChannelVN.IMS2.WebApi.Controllers
{
    [Route("api/[controller]")]
    public class NewsOnHomeController : BaseController
    {
        /// <summary>
        /// Tìm kiếm bài post
        /// </summary>
        /// <param name="body"></param>
        /// <param name="officerId"></param>
        /// <returns></returns>
        /// <response code="401">Authorization has been denied for this request</response>
        /// <response code="404">Not Found</response>
        /// <response code="405">Invalid input</response>
        // POST api/news/search => getway index 47
        [HttpPost, Route("search")]
        public async Task<IActionResult> SearchAsync([FromForm]NewsOnHomeSearchInputModel body, [FromForm]long officerId)
        {
            try
            {
                string sessionId = HttpContext?.Request?.Headers?["session-id"];
                if (string.IsNullOrEmpty(sessionId))
                {
                    return Ok(ActionResponse<string>.CreateErrorResponse("session-id not empty"));
                }

                if (body == null)
                {
                    return Ok(ActionResponse<string>.CreateSuccessResponse("Không tìm thấy bài viết nào."));
                }
                var keyword = body.keyword;
                var pageIndex = body.pageIndex.HasValue ? (body.pageIndex < 1 ? 1 : body.pageIndex) : 1;
                var pageSize = body.pageSize.HasValue ? (body.pageSize < 1 ? 10 : body.pageSize) : 10;

                var fromDate = body.fromDate.HasValue ? body.fromDate.Value.AddHours(0).AddMinutes(0).AddSeconds(0) : DateTime.MinValue;
                var toDate = body.toDate.HasValue ? body.toDate.Value.AddHours(23).AddMinutes(59).AddSeconds(59) : DateTime.MaxValue;

                var status = body.status.HasValue ? body.status : 0;
                var officerClass = body.officerClass.HasValue ? body.officerClass : 0;

                long.TryParse(User.FindFirst(ClaimTypes.NameIdentifier)?.Value, out long accountId);

                var result = await NewsOnHomeService.SearchAsync(keyword, officerId, accountId, status, officerClass, pageIndex.Value, pageSize.Value, body.orderBy, fromDate, toDate, sessionId);
                
                return Ok(ActionResponse<PagingDataResult<NewsOnHomeSearch>>.CreateSuccessResponse(result));
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
                return Ok(ActionResponse<string>.CreateErrorResponse(ex.Message));
            }
        }

        /// <summary>
        /// đề xuất tin on home
        /// </summary>
        /// <param name="body"></param>
        /// <returns></returns>
        /// <response code="401">Authorization has been denied for this request</response>
        /// <response code="404">Not Found</response>
        /// <response code="405">Invalid input</response>
        // POST api/news/search => getway index 47
        [HttpPost, Route("offer")]
        public async Task<IActionResult> OfferOnHomeAsync([FromForm]NewsOnHome body)
        {
            try
            {
                string sessionId = HttpContext?.Request?.Headers?["session-id"];
                //if (string.IsNullOrEmpty(sessionId))
                //{
                //    return Ok(ActionResponse<string>.CreateErrorResponse("session-id not empty"));
                //}

                if (body == null || (body != null && body.NewsId <= 0))
                {
                    return Ok(ActionResponse<string>.CreateErrorResponse("Không tìm thấy bài viết được đề xuất."));
                }
                if (body.OfficerId <= 0)
                {
                    return Ok(ActionResponse<string>.CreateErrorResponse("Không tìm thấy page."));
                }
                long.TryParse(User.FindFirst(ClaimTypes.NameIdentifier)?.Value, out long accountId);
                body.Id = body.NewsId;
                body.OrderedDate = DateTime.Now;
                body.OrderedBy = accountId.ToString();
                body.Status = (int)EnumStatusNewsOnHome.OrderOnHome;

                var result = await NewsOnHomeService.AddAsync(body, sessionId, accountId);

                return Ok(ActionResponse<string>.CreateSuccessResponse("Success"));
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
                return Ok(ActionResponse<string>.CreateErrorResponse(ex.Message));
            }
        }

        [HttpPost, Route("acceptOffer")]
        public async Task<IActionResult> AcceptOfferOnHomeAsync([FromForm]NewsOnHome body)
        {
            try
            {
                string sessionId = HttpContext?.Request?.Headers?["session-id"];
                //if (string.IsNullOrEmpty(sessionId))
                //{
                //    return Ok(ActionResponse<string>.CreateErrorResponse("session-id not empty"));
                //}

                if (body == null || (body!=null && body.NewsId<=0))
                {
                    return Ok(ActionResponse<string>.CreateErrorResponse("Không tìm thấy bài viết được đề xuất."));
                }
                if (body.OfficerId <= 0)
                {
                    return Ok(ActionResponse<string>.CreateErrorResponse("Không tìm thấy page."));
                }
                long.TryParse(User.FindFirst(ClaimTypes.NameIdentifier)?.Value, out long accountId);

                body.Status = (int)EnumStatusNewsOnHome.AcceptedOnHome;
                body.AcceptedBy = accountId.ToString();
                body.AcceptedDate = DateTime.Now;

                var result = await NewsOnHomeService.UpdateAsync(body, sessionId, accountId);

                return Ok(ActionResponse<string>.CreateSuccessResponse("Success"));
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
                return Ok(ActionResponse<string>.CreateErrorResponse(ex.Message));
            }
        }

        [HttpPost, Route("cancelOffer")]
        public async Task<IActionResult> CancelOfferOnHomeAsync([FromForm]NewsOnHome body)
        {
            try
            {
                string sessionId = HttpContext?.Request?.Headers?["session-id"];
                //if (string.IsNullOrEmpty(sessionId))
                //{
                //    return Ok(ActionResponse<string>.CreateErrorResponse("session-id not empty"));
                //}
                if (body == null || (body != null && body.NewsId <= 0))
                {
                    return Ok(ActionResponse<string>.CreateErrorResponse("Không tìm thấy bài viết được đề xuất."));
                }
                if (body.OfficerId <= 0)
                {
                    return Ok(ActionResponse<string>.CreateErrorResponse("Không tìm thấy page."));
                }
                long.TryParse(User.FindFirst(ClaimTypes.NameIdentifier)?.Value, out long accountId);

                body.Status = (int)EnumStatusNewsOnHome.CanceledOnHome;
                body.CanceledBy = accountId.ToString();
                body.CanceledDate = DateTime.Now;

                var result = await NewsOnHomeService.UpdateAsync(body, sessionId, accountId);

                return Ok(ActionResponse<string>.CreateSuccessResponse("Success"));
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
                return Ok(ActionResponse<string>.CreateErrorResponse(ex.Message));
            }
        }

        [AllowAnonymous]
        [HttpPost, Route("appbotaccept")]
        public async Task<IActionResult> AppBotAcceptOnHomeAsync([FromForm]NewsOnHome body, [FromForm]long telegramId, [FromForm]string action, [FromForm]string note)
        {
            try
            {
                var apiKey = string.Empty;
                foreach (var key in HttpContext.Request.Headers?.Keys)
                {
                    if (key.ToLower().Equals(SecureParser.XApiKey.ToLower()))
                    {
                        apiKey = HttpContext.Request.Headers?[key];
                        break;
                    }
                }                
                var clientRegistry = AppSettings.Current.ServiceConfiguration.ClientRegistry;
                if (!clientRegistry.Keys.Contains(apiKey))
                {
                    return Unauthorized();
                }

                if (body == null || (body != null && body.NewsId <= 0))
                {
                    return Ok(ActionResponse<string>.CreateErrorResponse("Không tìm thấy bài viết được đề xuất."));
                }
                if (body.OfficerId <= 0)
                {
                    return Ok(ActionResponse<string>.CreateErrorResponse("Không tìm thấy page."));
                }
                if (telegramId <= 0)
                {
                    return Ok(ActionResponse<string>.CreateErrorResponse("Không tìm thấy thông tin telegramId."));
                }

                //check quyền account on page
                //var listPage= await AccountAssignmentService.ListPageAsync(accountId, body.OfficerId, "", 1, 1000);
                //if(listPage!=null && listPage.Data!=null && listPage.Data.Count <= 0)
                //{
                //    return Ok(ActionResponse<string>.CreateErrorResponse("Account không có quyền thao tác trên page này."));
                //}
                var accountId = telegramId;

                if (action.ToLower().Equals("accept"))
                {
                    body.Status = (int)EnumStatusNewsOnHome.AcceptedOnHome;
                    body.AcceptedBy = accountId.ToString();
                    body.AcceptedDate = DateTime.Now;
                }
                else if (action.ToLower().Equals("cancel"))
                {
                    body.Status = (int)EnumStatusNewsOnHome.CanceledOnHome;
                    body.CanceledBy = accountId.ToString();
                    body.CanceledDate = DateTime.Now;
                }
                else
                    return Ok(ActionResponse<string>.CreateErrorResponse("Action không được hỗ trợ."));
                
                var result = await NewsOnHomeService.UpdateAsync(body, "", accountId);
                if(result)
                    return Ok(ActionResponse<string>.CreateSuccessResponse("Success"));

                return Ok(ActionResponse<string>.CreateErrorResponse("Không tìm thấy thông tin bài post."));
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
                return Ok(ActionResponse<string>.CreateErrorResponse(ex.Message));
            }
        }

        [AllowAnonymous]
        [HttpPost, Route("appbot_register_telegram")]
        public async Task<IActionResult> AppBotRegisterTelegramAsync([FromForm]string mobile, [FromForm]long telegramId)
        {
            try
            {
                var apiKey = string.Empty;
                foreach (var key in HttpContext.Request.Headers?.Keys)
                {
                    if (key.ToLower().Equals(SecureParser.XApiKey.ToLower()))
                    {
                        apiKey = HttpContext.Request.Headers?[key];
                        break;
                    }
                }
                var clientRegistry = AppSettings.Current.ServiceConfiguration.ClientRegistry;
                if (!clientRegistry.Keys.Contains(apiKey))
                {
                    return Unauthorized();
                }

                if (string.IsNullOrEmpty(mobile))
                {
                    return Ok(ActionResponse<string>.CreateErrorResponse("Không tìm thấy thông tin mobile."));
                }
                if (telegramId <= 0)
                {
                    return Ok(ActionResponse<string>.CreateErrorResponse("Không tìm thấy thông tin telegramId."));
                }                               

                var result = await SecurityService.UpdateAccountOpentIdAsync(mobile, telegramId);
                if (result==Core.Entities.ErrorCode.ErrorMapping.ErrorCodes.Success)
                    return Ok(ActionResponse<string>.CreateSuccessResponse("Success"));

                return Ok(ActionResponse<string>.CreateErrorResponse("Lỗi khi đăng ký tài khoản telegram."));
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
                return Ok(ActionResponse<string>.CreateErrorResponse(ex.Message));
            }
        }


        [HttpPost, Route("active_job_late_approve")]
        public IActionResult ActiveAuto()
        {
            NewsOnHomeService.JobWarningLateApproveSetting();
            return Ok(ActionResponse<string>.CreateSuccessResponse("success"));
        }
    }
}
