﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using ChannelVN.IMS2.Core.Entities;
using ChannelVN.IMS2.Core.Services;
using ChannelVN.IMS2.Foundation.Common;
using ChannelVN.IMS2.Foundation.Logging;
using Microsoft.AspNetCore.Mvc;

namespace ChannelVN.IMS2.WebApi.Controllers
{
    [Route("api/[controller]")]
    public class LogActionController : BaseController
    {
        /// <summary>
        /// Search log action
        /// </summary>
        /// <param name="keyword"></param>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <param name="sourceId"></param>
        /// <param name="actionType"></param>
        /// <param name="type"></param>
        /// <param name="dateFrom"></param>
        /// <param name="dateTo"></param>
        /// <param name="objectId"></param>
        /// <returns></returns>
        /// <response code="401">Authorization has been denied for this request</response>
        /// <response code="404">Not Found</response>
        /// <response code="405">Invalid input</response>
        // POST api/album/update_status       
        [HttpPost, Route("search")]
        public async Task<IActionResult> SearchAsync([FromForm]string keyword, [FromForm] int pageIndex, [FromForm] int pageSize, [FromForm]long objectId,
            [FromForm]string sourceId, [FromForm] ActionType? actionType, [FromForm] LogContentType? type, [FromForm] DateTime? dateFrom, [FromForm] DateTime? dateTo)
        {
            try
            {
                long.TryParse(User.FindFirst(ClaimTypes.NameIdentifier)?.Value, out long userId);
                if (string.IsNullOrEmpty(sourceId)) sourceId = userId.ToString();
                var _action = new List<int>();
                if (actionType.HasValue)
                {
                    _action.Add((int)actionType.Value);
                }

                var _type = new List<int>();
                if (type.HasValue)
                {
                    _type.Add((int)type.Value);
                }
                
                pageIndex = pageIndex > 0 ? pageIndex : 1;
                pageSize = pageSize > 0 ? pageSize : 20;
                var result = await LogActionService.SearchLog(keyword, pageIndex, pageSize, objectId, sourceId, _action.ToArray(), _type.ToArray(), dateFrom ?? DateTime.MinValue, dateTo ?? DateTime.Now);
                return Ok(ActionResponse<List<LogActionEntity>>.CreateSuccessResponse(result));
            }
            catch (Exception ex)
            {
                return Ok(ActionResponse<string>.CreateErrorResponse(ex.Message));
            }
        }


        [HttpPost, Route("search_fixed")]
        public async Task<IActionResult> SearchAsync2([FromForm]string keyword, [FromForm] int pageIndex, [FromForm] int pageSize, [FromForm]long objectId,
            [FromForm]string sourceId, [FromForm] ActionType? actionType, [FromForm] LogContentType? type, [FromForm] DateTime? dateFrom, [FromForm] DateTime? dateTo)
        {
            try
            {
                // fix all content type, action type
                long.TryParse(User.FindFirst(ClaimTypes.NameIdentifier)?.Value, out long userId);
                if (string.IsNullOrEmpty(sourceId)) sourceId = userId.ToString();
                pageIndex = pageIndex > 0 ? pageIndex : 1;
                pageSize = pageSize > 0 ? pageSize : 20;

                var actionTypes = new int[] {
                    (int)ActionType.Insert,
                }.ToArray();

                var types = new int[] {
                    (int)LogContentType.Photo,
                    (int)LogContentType.Album,
                }.ToArray();

                var result = await LogActionService.SearchLog(keyword, pageIndex, pageSize, objectId, sourceId, actionTypes, types, dateFrom ?? DateTime.MinValue, dateTo ?? DateTime.Now);
                var resultFilter = result.Where(c => types.Contains(c.Type) && actionTypes.Contains(c.ActionTypeDetail)).ToList();
                return Ok(ActionResponse<List<LogActionEntity>>.CreateSuccessResponse(resultFilter));
            }
            catch (Exception ex)
            {
                return Ok(ActionResponse<string>.CreateErrorResponse(ex.Message));
            }
        }
    }
}