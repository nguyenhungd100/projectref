﻿using System;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using ChannelVN.IMS2.Core.Entities;
using ChannelVN.IMS2.Core.Entities.Beam;
using ChannelVN.IMS2.Core.Entities.Distribution;
using ChannelVN.IMS2.Core.Entities.Media;
using ChannelVN.IMS2.Core.Entities.Newsletters;
using ChannelVN.IMS2.Core.Entities.Security;
using ChannelVN.IMS2.Core.Repositories.Constants;
using ChannelVN.IMS2.Core.Repositories.Functions;
using ChannelVN.IMS2.Core.Services;
using ChannelVN.IMS2.Foundation.Common;
using ChannelVN.IMS2.Foundation.Common.Configuration;
using ChannelVN.IMS2.Foundation.Logging;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using static ChannelVN.IMS2.Core.Entities.ErrorCode.ErrorMapping;

namespace ChannelVN.IMS2.WebApi.Controllers
{
    [Route("api/[controller]")]
    public class BeamController : BaseController
    {
        /// <summary>
        /// Thêm mới hoặc update Chùm tin
        /// </summary>
        /// <param name="beam"></param>
        /// <param name="officerId"></param>
        /// <param name="articleInBeam"></param>
        /// <returns></returns>
        /// <response code="401">Authorization has been denied for this request</response>
        /// <response code="404">Not Found</response>
        /// <response code="405">Invalid input</response>
        // POST api/beam/add => getway index 84
        [HttpPost, Route("save")]
        public async Task<IActionResult> SaveAsync([FromForm]Beam beam, [FromForm]ArticleInBeam[] articleInBeam, [FromForm]long officerId)
        {
            try
            {
                string sessionId = HttpContext?.Request?.Headers?["session-id"];
                if (string.IsNullOrEmpty(sessionId))
                {
                    return Ok(ActionResponse<string>.CreateErrorResponse("session-id not empty"));
                }
                var clientIP = HttpContext.GetClientIP();

                var result = ErrorCodes.BusinessError;
                if (beam == null)
                {
                    return Ok(ActionResponse<string>.CreateErrorResponse("Object nulll."));
                }

                if (!(officerId > 0))
                {
                    return Ok(ActionResponse<string>.CreateErrorResponse("OfficerId invalid."));
                }

                if (string.IsNullOrEmpty(beam.Name))
                    return Ok(ActionResponse<string>.CreateErrorResponse("Tên chùm tin trống."));

                if (!Utility.ValidateStringIds(beam.Tags))
                {
                    return Ok(ActionResponse<string>.CreateErrorResponse("Tags không hợp lệ."));
                }

                var validPublishData = await Core.Services.Functions.Function.ValidatePublishData(beam.PublishData, (int)CardType.ListNews);
                if (!validPublishData)
                {
                    return Ok(ActionResponse<string>.CreateErrorResponse("PublishData invalid."));
                }

                if (long.TryParse(User.FindFirst(ClaimTypes.NameIdentifier)?.Value, out long userId))
                {
                    if (beam.Id > 0)
                    {
                        beam.ModifiedDate = DateTime.Now;
                        beam.PublishMode = (int)NewsPublishMode.Public;
                        beam.ModifiedBy = userId.ToString();
                        var url = Utility.UnicodeToUnsignedAndDash(beam.Name ?? "");
                        beam.UnsignName = url;
                        beam.Url = string.Format(AppSettings.Current.ChannelConfiguration.CommonSettings.UrlFormatPlayList, url, beam.Id);
                        beam.CardType = (int)CardType.ListNews;
                        //Get PhotoInAlbum in publishData
                        var listItem = Foundation.Common.Json.Parse<PublishData>(beam.PublishData);
                        articleInBeam = listItem?.Items.Select(p => new ArticleInBeam
                        {
                            BeamId = beam.Id,
                            ArticleId = long.Parse(p.Id),
                            PublishedDate = DateTime.Now
                        }).ToArray();

                        result = await BeamService.UpdateAsync(beam, articleInBeam, clientIP, officerId, sessionId);
                        if (result == ErrorCodes.Success)
                        {
                            //insert log
                            await Function.AddToQueue(ActionName.InsertLog, TopicName.LOGACTION, new LogActionEntity
                            {
                                ObjectId = beam.Id.ToString(),
                                Account = userId.ToString(),
                                CreatedDate = DateTime.SpecifyKind(DateTime.Now, DateTimeKind.Unspecified),
                                Ip = clientIP,
                                Message = "Success.",
                                Title = beam.Name,
                                SourceId = officerId.ToString(),
                                ActionTypeDetail = (int)ActionType.Update,
                                ActionText = ActionType.Update.ToString(),
                                Type = (int)LogContentType.Beam
                            });
                        }
                    }
                    else
                    {
                        var beamSearch = Mapper<BeamSearch>.Map(beam, new BeamSearch());
                        beamSearch.Id = Generator.NewsId();
                        beam.Id = beamSearch.Id;
                        beamSearch.Type = (int)NewsType.Beam;
                        beamSearch.CardType = (int)CardType.ListNews;
                        beamSearch.Status = (int)NewsStatus.Draft;
                        beamSearch.CreatedDate = DateTime.Now;
                        beamSearch.PublishMode = (int)NewsPublishMode.Public;
                        beamSearch.DistributorId = beamSearch.DistributorId.HasValue ? beamSearch.DistributorId : AppSettings.Current.Distribution.Automation.DistributorId;

                        beamSearch.NewsInAccount = new[] {
                            new NewsInAccount
                            {
                                AccountId = officerId,
                                NewsId = beamSearch.Id,
                                PublishedDate = DateTime.Now,
                                PublishedType = (int)PublishedType.Post
                            }
                        };
                        beamSearch.CreatedBy = userId.ToString();

                        var url = Utility.UnicodeToUnsignedAndDash(beam.Name ?? "");
                        beamSearch.UnsignName = url;
                        beamSearch.Url = string.Format(AppSettings.Current.ChannelConfiguration.CommonSettings.UrlFormatPlayList, url, beamSearch.Id);

                        //Get PhotoInAlbum in publishData
                        var listItem = Foundation.Common.Json.Parse<PublishData>(beam.PublishData);
                        beamSearch.ArticleInBeam = listItem?.Items.Select(p => new ArticleInBeam
                        {
                            BeamId = beam.Id,
                            ArticleId = long.Parse(p.Id),
                            PublishedDate = DateTime.Now
                        }).ToArray();

                        result = await BeamService.AddAsync(beamSearch, clientIP);
                        if (result == ErrorCodes.Success)
                        {
                            //insert log
                            await Function.AddToQueue(ActionName.InsertLog, TopicName.LOGACTION, new LogActionEntity
                            {
                                ObjectId = beamSearch.Id.ToString(),
                                Account = userId.ToString(),
                                CreatedDate = DateTime.SpecifyKind(DateTime.Now, DateTimeKind.Unspecified),
                                Ip = clientIP,
                                Message = "Success.",
                                Title = beamSearch.Name,
                                SourceId = officerId.ToString(),
                                ActionTypeDetail = (int)ActionType.Insert,
                                ActionText = ActionType.Insert.ToString(),
                                Type = (int)LogContentType.Beam
                            });
                        }
                    }
                }
                else
                {
                    result = ErrorCodes.PermissionInvalid;
                }
                if (result == ErrorCodes.Success)
                {
                    return Ok(ActionResponse<object>.CreateSuccessResponse(beam.EncryptId, "success"));
                }
                else
                {
                    return Ok(ActionResponse<string>.CreateErrorResponse(Current[result]));
                }
            }
            catch (Exception ex)
            {
                return Ok(ActionResponse<string>.CreateErrorResponse(ex.Message));
            }
        }

        /// <summary>
        /// Đưa chùm tin vào lưu trữ
        /// </summary>
        /// <param name="beam"></param>
        /// <param name="officerId"></param>
        /// <param name="articleInBeam"></param>
        /// <returns></returns>
        /// <response code="401">Authorization has been denied for this request</response>
        /// <response code="404">Not Found</response>
        /// <response code="405">Invalid input</response>
        // POST api/beam/add => getway index 84
        [HttpPost, Route("archive")]
        public async Task<IActionResult> ArchiveAsync([FromForm]Beam beam, [FromForm]ArticleInBeam[] articleInBeam, [FromForm]long officerId)
        {
            try
            {
                string sessionId = HttpContext?.Request?.Headers?["session-id"];
                if (string.IsNullOrEmpty(sessionId))
                {
                    return Ok(ActionResponse<string>.CreateErrorResponse("session-id not empty"));
                }
                var clientIP = HttpContext.GetClientIP();

                var result = ErrorCodes.BusinessError;                
                if (beam == null)
                {
                    return Ok(ActionResponse<string>.CreateErrorResponse("Object nulll."));
                }
                if (string.IsNullOrEmpty(beam.Name))
                    return Ok(ActionResponse<string>.CreateErrorResponse("Tên chùm tin trống."));
                if (!Utility.ValidateStringIds(beam.Tags))
                {
                    return Ok(ActionResponse<string>.CreateErrorResponse("Tags không hợp lệ."));
                }
                if (long.TryParse(User.FindFirst(ClaimTypes.NameIdentifier)?.Value, out long userId))
                {
                    if (beam.Id > 0)
                    {
                        beam.ModifiedDate = DateTime.Now;
                        beam.PublishMode = (int)NewsPublishMode.Public;
                        beam.ModifiedBy = userId.ToString();
                        var url = Utility.UnicodeToUnsignedAndDash(beam.Name ?? "");
                        beam.UnsignName = url;
                        beam.Url = string.Format(AppSettings.Current.ChannelConfiguration.CommonSettings.UrlFormatPlayList, url, beam.Id);

                        //Get PhotoInAlbum in publishData
                        var listItem = Foundation.Common.Json.Parse<PublishData>(beam.PublishData);
                        articleInBeam = listItem?.Items.Select(p => new ArticleInBeam
                        {
                            BeamId = beam.Id,
                            ArticleId = long.Parse(p.Id),
                            PublishedDate = DateTime.Now
                        }).ToArray();

                        result = await BeamService.UpdateAsync(beam, articleInBeam, clientIP, officerId, sessionId);
                        if (result == ErrorCodes.Success)
                        {
                            //insert log
                            await Function.AddToQueue(ActionName.InsertLog, TopicName.LOGACTION, new LogActionEntity
                            {
                                ObjectId = beam.Id.ToString(),
                                Account = userId.ToString(),
                                CreatedDate = DateTime.SpecifyKind(DateTime.Now, DateTimeKind.Unspecified),
                                Ip = clientIP,
                                Message = "Success.",
                                Title = beam.Name,
                                SourceId = officerId.ToString(),
                                ActionTypeDetail = (int)ActionType.Update,
                                ActionText = ActionType.Update.ToString(),
                                Type = (int)LogContentType.Beam
                            });
                        }
                    }
                    else
                    {
                        var beamSearch = Mapper<BeamSearch>.Map(beam, new BeamSearch());
                        beamSearch.Id = Generator.NewsId();
                        beam.Id = beamSearch.Id;
                        beamSearch.Type = (int)NewsType.Beam;
                        beamSearch.CardType = (int)CardType.ListNews;
                        beamSearch.Status = (int)NewsStatus.Archive;
                        beamSearch.CreatedDate = DateTime.Now;
                        beamSearch.PublishMode = (int)NewsPublishMode.Public;
                        beamSearch.DistributorId = beamSearch.DistributorId.HasValue ? beamSearch.DistributorId : AppSettings.Current.Distribution.Automation.DistributorId;
                        var checkOfficialAccount = await SecurityService.CheckAccountMember(officerId, userId);
                        beamSearch.NewsInAccount = new[] {
                                new NewsInAccount
                                {
                                    AccountId = checkOfficialAccount ? officerId : userId,
                                    NewsId = beamSearch.Id,
                                    PublishedDate = DateTime.Now,
                                    PublishedType = (int)PublishedType.Post
                                }
                            };
                        beamSearch.CreatedBy = userId.ToString();
                        var url = Utility.UnicodeToUnsignedAndDash(beam.Name ?? "");
                        beamSearch.UnsignName = url;
                        beamSearch.Url = string.Format(AppSettings.Current.ChannelConfiguration.CommonSettings.UrlFormatPlayList, url, beamSearch.Id);

                        //Get PhotoInAlbum in publishData
                        var listItem = Foundation.Common.Json.Parse<PublishData>(beam.PublishData);
                        beamSearch.ArticleInBeam = listItem?.Items.Select(p => new ArticleInBeam
                        {
                            BeamId = beamSearch.Id,
                            ArticleId = long.Parse(p.Id),
                            PublishedDate = DateTime.Now
                        }).ToArray();

                        result = await BeamService.AddAsync(beamSearch, clientIP);
                        if (result == ErrorCodes.Success)
                        {
                            //insert log
                            await Function.AddToQueue(ActionName.InsertLog, TopicName.LOGACTION, new LogActionEntity
                            {
                                ObjectId = beamSearch.Id.ToString(),
                                Account = userId.ToString(),
                                CreatedDate = DateTime.SpecifyKind(DateTime.Now, DateTimeKind.Unspecified),
                                Ip = clientIP,
                                Message = "Success.",
                                Title = beamSearch.Name,
                                SourceId = officerId.ToString(),
                                ActionTypeDetail = (int)ActionType.Insert,
                                ActionText = ActionType.Insert.ToString(),
                                Type = (int)LogContentType.Beam
                            });
                        }
                    }
                }
                else
                {
                    result = ErrorCodes.PermissionInvalid;
                }

                if (result == ErrorCodes.Success)
                {
                    return Ok(ActionResponse<object>.CreateSuccessResponse(beam.EncryptId, "success"));
                }
                else
                {
                    return Ok(ActionResponse<string>.CreateErrorResponse(Current[result]));
                }
            }
            catch (Exception ex)
            {
                return Ok(ActionResponse<string>.CreateErrorResponse(ex.Message));
            }
        }

        /// <summary>
        /// Cập nhận trạng thái
        /// </summary>
        /// <param name="id"></param>
        /// <param name="action"></param>
        /// <param name="officerId"></param>
        /// <returns></returns>
        /// <response code="401">Authorization has been denied for this request</response>
        /// <response code="404">Not Found</response>
        /// <response code="405">Invalid input</response>
        // POST api/article/update_status       
        [HttpPost, Route("update_status")]
        public async Task<IActionResult> UpdateStatusV2Async([FromForm]long id, [FromForm]NewsStatus action, [FromForm] long officerId)
        {
            try
            {
                string sessionId = HttpContext?.Request?.Headers?["session-id"];
                if (string.IsNullOrEmpty(sessionId))
                {
                    return Ok(ActionResponse<string>.CreateErrorResponse("session-id not empty"));
                }
                var clientIP = HttpContext.GetClientIP();

                var result = ErrorCodes.BusinessError;                
                long.TryParse(User.FindFirst(ClaimTypes.NameIdentifier)?.Value, out long userId);

                result = await BeamService.UpdateStatusV2Async(id, action, userId, clientIP, sessionId);
                if (result == ErrorCodes.Success)
                {
                    //insert log
                    await Function.AddToQueue(ActionName.InsertLog, TopicName.LOGACTION, new LogActionEntity
                    {
                        ObjectId = id.ToString(),
                        Account = userId.ToString(),
                        CreatedDate = DateTime.SpecifyKind(DateTime.Now, DateTimeKind.Unspecified),
                        Ip = clientIP,
                        Message = "Success.",
                        Title = "",
                        SourceId = officerId.ToString(),
                        ActionTypeDetail = (int)ActionType.UpdateStatus,
                        ActionText = ActionType.UpdateStatus.ToString(),
                        Type = (int)LogContentType.Beam
                    });
                    return Ok(ActionResponse<object>.CreateSuccessResponse(id.ToString(), "success"));
                }
                return Ok(ActionResponse<string>.CreateErrorResponse(Current[result]));
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
                return Ok(ActionResponse<string>.CreateErrorResponse(ex.Message));
            }
        }
        /// <summary>
        /// Lấy thông tin chi tiết Chùm tin
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        /// <response code="401">Authorization has been denied for this request</response>
        /// <response code="404">Not Found</response>
        /// <response code="405">Invalid input</response>
        // POST api/beam/get_by_id        
        [HttpPost, Route("get_by_id")]
        public async Task<IActionResult> GetByIdAsync([FromForm] long id)
        {
            try
            {
                var result = await BeamService.GetBeamSearchByIdAsync(id);
                if (result != null && result.Id > 0) return Ok(ActionResponse<BeamSearch>.CreateSuccessResponse(result));
                return Ok(ActionResponse<string>.CreateErrorResponse("Beam not exist."));
            }
            catch (Exception ex)
            {
                return Ok(ActionResponse<string>.CreateErrorResponse(ex.Message));
            }
        }

        /// <summary>
        /// Tìm kiếm chùm tin
        /// </summary>
        /// <param name="searchEntity"></param>
        /// <returns></returns>
        /// <response code="401">Authorization has been denied for this request</response>
        /// <response code="404">Not Found</response>
        /// <response code="405">Invalid input</response>
        // POST api/beam/search
        [HttpPost, Route("search")]
        public async Task<IActionResult> SearchAsync([FromForm]SearchNews searchEntity)
        {
            try
            {
                if (searchEntity == null)
                {
                    return Ok(ActionResponse<BeamSearch>.CreateSuccessResponse("Không tìm thấy chùm tin nào."));
                }
                searchEntity.PageIndex = searchEntity.PageIndex < 1 ? 1 : searchEntity.PageIndex;
                searchEntity.PageSize = searchEntity.PageSize < 1 ? 20 : searchEntity.PageSize;
                searchEntity.FromDate = searchEntity.FromDate.HasValue ? searchEntity.FromDate.Value.AddHours(0).AddMinutes(0).AddSeconds(0) : DateTime.MinValue;
                searchEntity.ToDate = searchEntity.ToDate.HasValue ? searchEntity.ToDate.Value.AddHours(23).AddMinutes(59).AddSeconds(59) : DateTime.MaxValue;

                long.TryParse(User.FindFirst(ClaimTypes.NameIdentifier)?.Value, out long userId);

                var result = await BeamService.SearchAsync(searchEntity, userId);
                return Ok(ActionResponse<PagingDataResult<BeamSearchReturn>>.CreateSuccessResponse(result));
            }
            catch (Exception ex)
            {
                return Ok(ActionResponse<string>.CreateErrorResponse(ex.Message));
            }
        }

        /// <summary>
        /// Lấy danh sách chùm tin đang chờ duyệt
        /// </summary>
        /// <param name="searchEntity"></param>
        /// <returns></returns>
        /// <response code="401">Authorization has been denied for this request</response>
        /// <response code="404">Not Found</response>
        /// <response code="405">Invalid input</response>
        [HttpPost, Route("get_list_approve")]
        public async Task<IActionResult> GetListApproveAsync([FromForm]SearchNews searchEntity)
        {
            try
            {
                if (searchEntity == null)
                {
                    return Ok(ActionResponse<string>.CreateSuccessResponse("Không tìm thấy chùm tin nào."));
                }
                if (searchEntity.OfficerId <= 0)
                {
                    return Ok(ActionResponse<string>.CreateErrorResponse("OfficerId không hợp lệ."));
                }

                searchEntity.Status = ((int)NewsStatus.Approve).ToString();
                searchEntity.PageIndex = searchEntity.PageIndex < 1 ? 1 : searchEntity.PageIndex;
                searchEntity.PageSize = searchEntity.PageSize < 1 ? 20 : searchEntity.PageSize;
                searchEntity.FromDate = searchEntity.FromDate.HasValue ? searchEntity.FromDate.Value.AddHours(0).AddMinutes(0).AddSeconds(0) : DateTime.MinValue;
                searchEntity.ToDate = searchEntity.ToDate.HasValue ? searchEntity.ToDate.Value.AddHours(23).AddMinutes(59).AddSeconds(59) : DateTime.Now;
                long.TryParse(User.FindFirst(ClaimTypes.NameIdentifier)?.Value, out long accountId);

                var result = await BeamService.GetListApproveAsync(searchEntity, accountId);

                return Ok(ActionResponse<PagingDataResult<BeamSearch>>.CreateSuccessResponse(result));
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
                return Ok(ActionResponse<string>.CreateErrorResponse(ex.Message));
            }
        }

        /// <summary>
        /// Danh sách Chùm tin liên quan
        /// </summary>
        /// <param name="beamId"></param>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        /// <response code="401">Authorization has been denied for this request</response>
        /// <response code="404">Not Found</response>
        /// <response code="405">Invalid input</response>
        // POST api/beam/get_list_relation
        [HttpPost, Route("get_list_relation")]
        public async Task<IActionResult> GetListRelationAsync([FromForm]long beamId, [FromForm]int? pageIndex, [FromForm] int? pageSize)
        {
            try
            {
                try
                {
                    if (beamId == 0)
                    {
                        return Ok(ActionResponse<BeamSearch>.CreateSuccessResponse("Chùm tin không tồn tại."));
                    }
                    pageIndex = pageIndex < 1 ? 1 : pageIndex;
                    pageSize = pageSize < 1 ? 20 : pageSize;
                    var beam = await BeamService.GetByIdAsync(beamId);
                    if (beam.BeamRelation != null && beam.BeamRelation.Length > 0)
                    {
                        var result = await BeamService.GetListRelationAsync(beam.BeamRelation, pageIndex, pageSize);
                        return Ok(ActionResponse<PagingDataResult<BeamSearch>>.CreateSuccessResponse(result));
                    }
                    return Ok(ActionResponse<PagingDataResult<BeamSearch>>.CreateSuccessResponse(new PagingDataResult<BeamSearch>()));
                }
                catch (Exception ex)
                {
                    return Ok(ActionResponse<string>.CreateErrorResponse(ex.Message));
                }
            }
            catch (Exception ex)
            {
                return Ok(ActionResponse<string>.CreateErrorResponse(ex.Message));
            }
        }

        /// <summary>
        /// Số lượng Chùm tin theo người dùng
        /// </summary>
        /// <param name="status"></param>
        /// <param name="officialId"></param>
        /// <returns></returns>
        /// <response code="401">Authorization has been denied for this request</response>
        /// <response code="404">Not Found</response>
        /// <response code="405">Invalid input</response>
        // POST api/beam/get_count_by_status
        [HttpPost, Route("get_count_by_status")]
        public async Task<IActionResult> GetCountByStatusAsync([FromForm]NewsStatus status, [FromForm] long? officialId)
        {
            try
            {
                long.TryParse(User.FindFirst(ClaimTypes.NameIdentifier)?.Value, out long userId);

                var result = await BeamService.GetCountByStatusAsync((int)status, officialId ?? 0, userId);
                return Ok(ActionResponse<object>.CreateSuccessResponse(new { Count = result.ToString() }));
            }
            catch (Exception ex)
            {
                return Ok(ActionResponse<string>.CreateErrorResponse(ex.Message));
            }
        }

        /// <summary>
        /// Danh sách Tin trong chùm tin
        /// </summary>
        /// <param name="beamId"></param>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        /// <response code="401">Authorization has been denied for this request</response>
        /// <response code="404">Not Found</response>
        /// <response code="405">Invalid input</response>
        // POST api/beam/article_in_beam => getway index 93
        [HttpPost, Route("article_in_beam")]
        public async Task<IActionResult> ArticleInBeamAsync([FromForm]long beamId, int? pageIndex, int? pageSize)
        {
            try
            {
                pageIndex = pageIndex == null || pageIndex < 1 ? 1 : pageIndex;
                pageSize = pageSize == null || pageSize < 1 ? 20 : pageSize;
                var result = await BeamService.ArticleInBeamAsync(beamId, pageIndex, pageSize);
                return Ok(ActionResponse<PagingDataResult<NewsAll>>.CreateSuccessResponse(result));
            }
            catch (Exception ex)
            {
                return Ok(ActionResponse<string>.CreateErrorResponse(ex.Message));
            }
        }

        /// <summary>
        /// Tìm kiếm bài viết để add vào Chùm tin
        /// </summary>
        /// <param name="searchEntity"></param>
        /// <returns></returns>
        /// <response code="401">Authorization has been denied for this request</response>
        /// <response code="404">Not Found</response>
        /// <response code="405">Invalid input</response>
        // POST api/video/search => getway index 71
        [HttpPost, Route("search_article")]
        public async Task<IActionResult> SearchNewsAsync([FromForm]SearchNews searchEntity)
        {
            try
            {
                if (searchEntity == null)
                {
                    return Ok(ActionResponse<ArticleSearchReturn>.CreateSuccessResponse("Không tìm thấy bài viết nào."));
                }
                searchEntity.PageIndex = searchEntity.PageIndex < 1 ? 1 : searchEntity.PageIndex;
                searchEntity.PageSize = searchEntity.PageSize < 1 ? 20 : searchEntity.PageSize;

                searchEntity.FromDate = searchEntity.FromDate.HasValue ? searchEntity.FromDate.Value.AddHours(0).AddMinutes(0).AddSeconds(0) : DateTime.MinValue;
                searchEntity.ToDate = searchEntity.ToDate.HasValue ? searchEntity.ToDate.Value.AddHours(23).AddMinutes(59).AddSeconds(59) : DateTime.Now;

                searchEntity.Status = ((int)NewsStatus.Published).ToString();
                long.TryParse(User.FindFirst(ClaimTypes.NameIdentifier)?.Value, out long userId);

                var result = await BeamService.SearchNewsAsync(searchEntity, userId);

                return Ok(ActionResponse<PagingDataResult<NewsAll>>.CreateSuccessResponse(result));
            }
            catch (Exception ex)
            {
                return Ok(ActionResponse<string>.CreateErrorResponse(ex.Message));
            }
        }
        /// <summary>
        /// Add vào bảng tin
        /// </summary>
        /// <param name="mediaId"></param>
        /// <param name="boardIds"></param>
        /// <param name="officerId"></param>
        /// <returns></returns>
        /// <response code="401">Authorization has been denied for this request</response>
        /// <response code="404">Not Found</response>
        /// <response code="405">Invalid input</response>
        // POST api/videoplaylist/get_video_by_playlist_id => getway index 93
        [HttpPost, Route("add_to_board")]
        public async Task<IActionResult> AddToBoardAsync([FromForm]long mediaId, [FromForm] long[] boardIds, [FromForm]long officerId)
        {
            try
            {
                var clientIP = HttpContext.GetClientIP();
                long.TryParse(User.FindFirst(ClaimTypes.NameIdentifier)?.Value, out long userId);
                var dataDb = await BoardService.GetListByIdAsync(boardIds);
                if (dataDb == null)
                {
                    return Ok(ActionResponse<string>.CreateErrorResponse("Bảng tin không tồn tại."));
                }
                else
                {
                    if (!await BoardService.CheckBoardOfUser(officerId, userId, boardIds))
                    {
                        return Ok(ActionResponse<string>.CreateErrorResponse("Bạn không có quyền chỉnh sửa Bảng tin."));
                    }
                }
                dataDb.ForEach(p =>
                {
                    p.LastInsertedDate = DateTime.Now;
                    p.ModifiedBy = userId.ToString();
                    p.ModifiedDate = DateTime.Now;
                });
                var result = await BoardService.InsertMediaAsync(mediaId, dataDb);
                if (result)
                {
                    foreach (var boardId in boardIds)
                    {
                        await Function.AddToQueue(ActionName.AddNewsToBoard, TopicName.PUSHBOARDTOCHANNEL, new QueueData<long, string, long, long>
                        {
                            Data1 = boardId,
                            Data2 = mediaId.ToString(),
                            Data3 = officerId,
                            Data4 = userId
                        });
                    }                    
                    //insert log
                    await Function.AddToQueue(ActionName.InsertLog, TopicName.LOGACTION, new LogActionEntity
                    {
                        ObjectId = mediaId.ToString(),
                        Account = userId.ToString(),
                        CreatedDate = DateTime.SpecifyKind(DateTime.Now, DateTimeKind.Unspecified),
                        Ip = clientIP,
                        Message = "Success.",
                        Title = "",
                        SourceId = officerId.ToString(),
                        ActionTypeDetail = (int)ActionType.AddMediaToBoard,
                        ActionText = ActionType.AddMediaToBoard.ToString(),
                        Type = (int)LogContentType.Beam
                    });
                    return Ok(ActionResponse<string>.CreateSuccessResponse("Add beam success."));
                }
                return Ok(ActionResponse<string>.CreateErrorResponse("Add beam unsuccess."));
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
                return Ok(ActionResponse<string>.CreateErrorResponse(ex.Message));
            }
        }

        private async Task<string> CreatePublishData(string caption, ArticleInBeam[] articleInBeam)
        {
            var listArticle = await ArticleService.GetListByIdsAsync(articleInBeam?.Select(p => p.EncryptArticleId)?.ToList());
            var obj = new
            {
                title = caption,
                items = listArticle?.Select(p => new
                {
                    id = p.Id,
                    title = p.Title,

                })
            };
            return "";
        }
    }
}