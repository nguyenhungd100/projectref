﻿using System;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using ChannelVN.IMS2.Core.Entities;
using ChannelVN.IMS2.Core.Entities.Board;
using ChannelVN.IMS2.Core.Repositories.Constants;
using ChannelVN.IMS2.Core.Repositories.Functions;
using ChannelVN.IMS2.Core.Services;
using ChannelVN.IMS2.Foundation.Common;
using ChannelVN.IMS2.Foundation.Common.Configuration;
using ChannelVN.IMS2.Foundation.Logging;
using Microsoft.AspNetCore.Mvc;
using static ChannelVN.IMS2.Core.Entities.ErrorCode.ErrorMapping;

namespace ChannelVN.IMS2.WebApi.Controllers
{
    [Route("api/[controller]")]
    public class BoardController : BaseController
    {
        /// <summary>
        /// Thêm mới bảng tin
        /// </summary>
        /// <param name="board"></param>
        /// <param name="officerId"></param>
        /// <returns></returns>
        /// <response code="401">Authorization has been denied for this request</response>
        /// <response code="404">Not Found</response>
        /// <response code="405">Invalid input</response>
        [HttpPost, Route("add")]
        public async Task<IActionResult> AddAsync([FromForm]Board board, [FromForm]long officerId)
        {
            try
            {
                var result = ErrorCodes.BusinessError;
                var clientIP = string.IsNullOrEmpty(HttpContext?.Connection?.RemoteIpAddress?.ToString()) ? "192.168.1.1" : HttpContext?.Connection?.RemoteIpAddress?.ToString();
                if (board == null)
                {
                    return Ok(ActionResponse<string>.CreateErrorResponse("Object null."));
                }
                if (string.IsNullOrEmpty(board.Name))
                    return Ok(ActionResponse<string>.CreateErrorResponse("Tên kênh không được trống."));
                if (!string.IsNullOrEmpty(board.RelationBoard) && !Utility.ValidateStringIds(board.RelationBoard))
                    return Ok(ActionResponse<string>.CreateErrorResponse("Trường bảng tin liên quan không hợp lệ."));

                var boardSearch = Mapper<BoardSearch>.Map(board, new BoardSearch());
                boardSearch.Id = Generator.BoardId();
                board.Id = boardSearch.Id;
                boardSearch.Status = (int)BoardStatus.Actived;
                boardSearch.CreatedDate = DateTime.Now;
                if (long.TryParse(User.FindFirst(ClaimTypes.NameIdentifier)?.Value, out long userId))
                {
                    #region Old
                    //var listMember = await SecurityService.ListMembersAsync(officerId);
                    //var owner = listMember?.Where(p => p.Role == Core.Entities.Security.AccountMemberRole.Owner).FirstOrDefault();
                    //if (owner != null)
                    //{
                    //boardSearch.CreatedBy = officerId.ToString();
                    //var url = Utility.UnicodeToUnsignedAndDash(boardSearch.Name ?? "");
                    //boardSearch.UnsignName = url;
                    //boardSearch.Url = string.Format(AppSettings.Current.ChannelConfiguration.CommonSettings.UrlFormatPlayList, url, boardSearch.Id);

                    //result = await BoardService.AddAsync(boardSearch, userId, owner.MemberId, clientIP);
                    //if (result == ErrorCodes.Success)
                    //{
                    //    //insert log
                    //    await Function.AddToQueue(ActionName.InsertLog, TopicName.LOGACTION, new LogActionEntity
                    //    {
                    //        ObjectId = boardSearch.Id.ToString(),
                    //        Account = userId.ToString(),
                    //        CreatedDate = DateTime.SpecifyKind(DateTime.Now, DateTimeKind.Unspecified),
                    //        Ip = clientIP,
                    //        Message = "Success.",
                    //        Title = boardSearch.Name,
                    //        SourceId = boardSearch.CreatedBy,
                    //        ActionTypeDetail = (int)ActionType.Insert,
                    //        ActionText = ActionType.Insert.ToString(),
                    //        Type = (int)LogContentType.Board
                    //    });
                    //    return Ok(ActionResponse<object>.CreateSuccessResponse(boardSearch.EncryptId, "success"));
                    //}
                    //else
                    //{
                    //    return Ok(ActionResponse<string>.CreateErrorResponse(Current[result]));
                    //}
                    //}
                    //else
                    //{
                    //    return Ok(ActionResponse<string>.CreateErrorResponse(Current[ErrorCodes.DataInvalid]));
                    //}
                    #endregion
                    boardSearch.CreatedBy = officerId.ToString();
                    var url = Utility.UnicodeToUnsignedAndDash(boardSearch.Name ?? "");
                    boardSearch.UnsignName = url;
                    boardSearch.Url = string.Format(AppSettings.Current.ChannelConfiguration.CommonSettings.UrlFormatPlayList, url, boardSearch.Id);

                    result = await BoardService.AddAsync(boardSearch, userId, clientIP);
                    if (result == ErrorCodes.Success)
                    {
                        //insert log
                        await Function.AddToQueue(ActionName.InsertLog, TopicName.LOGACTION, new LogActionEntity
                        {
                            ObjectId = boardSearch.Id.ToString(),
                            Account = userId.ToString(),
                            CreatedDate = DateTime.SpecifyKind(DateTime.Now, DateTimeKind.Unspecified),
                            Ip = clientIP,
                            Message = "Success.",
                            Title = boardSearch.Name,
                            SourceId = officerId.ToString(),
                            ActionTypeDetail = (int)ActionType.Insert,
                            ActionText = ActionType.Insert.ToString(),
                            Type = (int)LogContentType.Board
                        });
                        return Ok(ActionResponse<object>.CreateSuccessResponse(boardSearch.EncryptId, "success"));
                    }
                    else
                    {
                        return Ok(ActionResponse<string>.CreateErrorResponse(Current[result]));
                    }
                }
                else
                {
                    return Ok(ActionResponse<string>.CreateErrorResponse(Current[result]));
                }
            }
            catch (Exception ex)
            {
                return Ok(ActionResponse<string>.CreateErrorResponse(ex.Message));
            }
        }

        /// <summary>
        /// Update bảng tin
        /// </summary>
        /// <param name="board"></param>
        /// <param name="officerId"></param>
        /// <returns></returns>
        /// <response code="401">Authorization has been denied for this request</response>
        /// <response code="404">Not Found</response>
        /// <response code="405">Invalid input</response>
        [HttpPost, Route("update")]
        public async Task<IActionResult> UpdateAsync([FromForm]Board board, [FromForm]long officerId)
        {
            try
            {
                var clientIP = string.IsNullOrEmpty(HttpContext?.Connection?.RemoteIpAddress?.ToString()) ? "192.168.1.1" : HttpContext?.Connection?.RemoteIpAddress?.ToString();

                if (board == null)
                    return Ok(ActionResponse<string>.CreateErrorResponse("Object null."));
                if (string.IsNullOrEmpty(board.Name))
                    return Ok(ActionResponse<string>.CreateErrorResponse("Tên kênh không được trống."));
                if (!string.IsNullOrEmpty(board.RelationBoard) && !Utility.ValidateStringIds(board.RelationBoard))
                    return Ok(ActionResponse<string>.CreateErrorResponse("Trường bảng tin liên quan không hợp lệ."));

                if (long.TryParse(User.FindFirst(ClaimTypes.NameIdentifier)?.Value, out long userId))
                {
                    board.ModifiedBy = userId.ToString();
                    board.ModifiedDate = DateTime.Now;

                    var url = Utility.UnicodeToUnsignedAndDash(board.Name ?? "");
                    board.UnsignName = url;
                    board.Url = string.Format(AppSettings.Current.ChannelConfiguration.CommonSettings.UrlFormatPlayList, url, board.Id);

                    var result = await BoardService.UpdateAsync(board, userId, officerId, clientIP);
                    if (result == ErrorCodes.Success)
                    {
                        //insert log
                        await Function.AddToQueue(ActionName.InsertLog, TopicName.LOGACTION, new LogActionEntity
                        {
                            ObjectId = board.Id.ToString(),
                            Account = userId.ToString(),
                            CreatedDate = DateTime.SpecifyKind(DateTime.Now, DateTimeKind.Unspecified),
                            Ip = clientIP,
                            Message = "Success.",
                            Title = board.Name,
                            SourceId = officerId.ToString(),
                            ActionTypeDetail = (int)ActionType.Update,
                            ActionText = ActionType.Update.ToString(),
                            Type = (int)LogContentType.Board
                        });
                        return Ok(ActionResponse<object>.CreateSuccessResponse(board.EncryptId, "success"));
                    }
                    else
                    {
                        return Ok(ActionResponse<string>.CreateErrorResponse(Current[result]));
                    }
                }
                else
                {
                    return Ok(ActionResponse<string>.CreateErrorResponse("Thông tin tài khoản không hợp lệ."));
                }
            }
            catch (Exception ex)
            {
                return Ok(ActionResponse<string>.CreateErrorResponse(ex.Message));
            }
        }

        /// <summary>
        /// Update trạng thái bảng tin
        /// </summary>
        /// <param name="id"></param>
        /// <param name="status"></param>
        /// <param name="officerId"></param>
        /// <returns></returns>
        /// <response code="401">Authorization has been denied for this request</response>
        /// <response code="404">Not Found</response>
        /// <response code="405">Invalid input</response>
        [HttpPost, Route("update_status")]
        public async Task<IActionResult> UpdateStatusAsync([FromForm]long id, [FromForm]BoardStatus status, [FromForm]long officerId)
        {
            try
            {
                var clientIP = string.IsNullOrEmpty(HttpContext?.Connection?.RemoteIpAddress?.ToString()) ? "192.168.1.1" : HttpContext?.Connection?.RemoteIpAddress?.ToString();
                long.TryParse(User.FindFirst(ClaimTypes.NameIdentifier)?.Value, out long userId);

                if (!(await BoardService.CheckBoardOfUser(officerId, userId, id)))
                    return Ok(ActionResponse<string>.CreateErrorResponse("Bạn không có quyền, bảng tin này không phải của bạn."));

                var result = await BoardService.UpdateStatusAsync(id, status, userId, clientIP);
                if (result == ErrorCodes.Success)
                {
                    //insert log
                    await Function.AddToQueue(ActionName.InsertLog, TopicName.LOGACTION, new LogActionEntity
                    {
                        ObjectId = id.ToString(),
                        Account = userId.ToString(),
                        CreatedDate = DateTime.SpecifyKind(DateTime.Now, DateTimeKind.Unspecified),
                        Ip = clientIP,
                        Message = "Success.",
                        Title = null,
                        SourceId = officerId.ToString(),
                        ActionTypeDetail = (int)ActionType.UpdateStatus,
                        ActionText = ActionType.UpdateStatus.ToString(),
                        Type = (int)LogContentType.Board
                    });
                    return Ok(ActionResponse<string>.CreateSuccessResponse(id.ToString(), "success"));
                }
                return Ok(ActionResponse<string>.CreateErrorResponse(Current[result]));
            }
            catch (Exception ex)
            {
                return Ok(ActionResponse<string>.CreateErrorResponse(ex.Message));
            }
        }

        /// <summary>
        /// Add tin vào bảng tin
        /// </summary>
        /// <param name="boardId"></param>
        /// <param name="newsIds"></param>
        /// <param name="officerId"></param>
        /// <returns></returns>
        /// <response code="401">Authorization has been denied for this request</response>
        /// <response code="404">Not Found</response>
        /// <response code="405">Invalid input</response>
        [HttpPost, Route("add_news")]
        public async Task<IActionResult> AddNewsAsync([FromForm]long boardId, [FromForm]string newsIds, [FromForm]long officerId)
        {
            try
            {
                var clientIP = string.IsNullOrEmpty(HttpContext?.Connection?.RemoteIpAddress?.ToString()) ? "192.168.1.1" : HttpContext?.Connection?.RemoteIpAddress?.ToString();
                if (string.IsNullOrEmpty(newsIds))
                    return Ok(ActionResponse<string>.CreateErrorResponse("NewsIds is null."));
                if (!Utility.ValidateStringIds(newsIds))
                    return Ok(ActionResponse<string>.CreateErrorResponse("NewsIds is invalid."));
                if (!long.TryParse(User.FindFirst(ClaimTypes.NameIdentifier)?.Value, out long userId))
                {
                    return Ok(ActionResponse<string>.CreateErrorResponse("Account invalid."));
                }
                if (!(await BoardService.CheckBoardOfUser(officerId, userId, boardId)))
                    return Ok(ActionResponse<string>.CreateErrorResponse("Bạn không có quyền, bảng tin này không phải của bạn."));
                var result = await BoardService.AddNewsAsync(boardId, newsIds, officerId, userId, clientIP);// officerId ?? 0);
                if (result)
                {
                    await Function.AddToQueue(ActionName.InsertLog, TopicName.LOGACTION, new LogActionEntity
                    {
                        ObjectId = boardId.ToString(),
                        Account = userId.ToString(),
                        CreatedDate = DateTime.SpecifyKind(DateTime.Now, DateTimeKind.Unspecified),
                        Ip = clientIP,
                        Message = "Success.",
                        Title = null,
                        SourceId = officerId.ToString(),
                        ActionTypeDetail = (int)ActionType.AddMediaToBoard,
                        ActionText = ActionType.AddMediaToBoard.ToString(),
                        Type = (int)LogContentType.Board
                    });
                    return Ok(ActionResponse<string>.CreateSuccessResponse(newsIds, "Success."));
                }                    
                else return Ok(ActionResponse<string>.CreateErrorResponse("Thêm mới không thành công."));
            }
            catch (Exception ex)
            {
                return Ok(ActionResponse<string>.CreateErrorResponse(ex.Message));
            }
        }

        /// <summary>
        /// Delete tin trong bảng tin
        /// </summary>
        /// <param name="boardId"></param>
        /// <param name="newsIds"></param>
        /// <param name="officerId"></param>
        /// <returns></returns>\
        /// <response code="401">Authorization has been denied for this request</response>
        /// <response code="404">Not Found</response>
        /// <response code="405">Invalid input</response>
        [HttpPost, Route("delete_news")]
        public async Task<IActionResult> DeleteNewsAsync([FromForm]long officerId, [FromForm]long boardId, [FromForm]string newsIds)
        {
            try
            {
                var clientIP = string.IsNullOrEmpty(HttpContext?.Connection?.RemoteIpAddress?.ToString()) ? "192.168.1.1" : HttpContext?.Connection?.RemoteIpAddress?.ToString();
                if (!long.TryParse(User.FindFirst(ClaimTypes.NameIdentifier)?.Value, out long userId))
                {
                    return Ok(ActionResponse<string>.CreateErrorResponse("Account invalid."));
                }
                if (!Utility.ValidateStringIds(newsIds))
                {
                    return Ok(ActionResponse<string>.CreateErrorResponse("NewsIds invalid."));
                }
                if (!(await BoardService.CheckBoardOfUser(officerId, userId, boardId)))
                    return Ok(ActionResponse<string>.CreateErrorResponse("Bạn không có quyền, bảng tin này không phải của bạn."));

                var result = await BoardService.DeleteNewsAsync(officerId, userId, boardId, newsIds, clientIP);

                // Thiếu thông tin Log Action( ActionType = DeleteNews)
                if (result)
                {
                    await Function.AddToQueue(ActionName.InsertLog, TopicName.LOGACTION, new LogActionEntity
                    {
                        ObjectId = boardId.ToString(),
                        Account = userId.ToString(),
                        CreatedDate = DateTime.SpecifyKind(DateTime.Now, DateTimeKind.Unspecified),
                        Ip = clientIP,
                        Message = "Success.",
                        Title = null,
                        SourceId = officerId.ToString(),
                        ActionTypeDetail = (int)ActionType.DeleteMediaFromBoard,
                        ActionText = ActionType.DeleteMediaFromBoard.ToString(),
                        Type = (int)LogContentType.Board
                    });
                    return Ok(ActionResponse<string>.CreateSuccessResponse(newsIds.ToString(), "Success."));
                }
                else
                    return Ok(ActionResponse<string>.CreateErrorResponse("Xáo thất bại."));
            }
            catch (Exception ex)
            {
                return Ok(ActionResponse<string>.CreateErrorResponse(ex.Message));
            }
        }

        /// <summary>
        /// Danh sách tin trong bảng tin
        /// </summary>
        /// <param name="boardId"></param>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <param name="officerId"></param>
        /// <returns></returns>
        /// <response code="401">Authorization has been denied for this request</response>
        /// <response code="404">Not Found</response>
        /// <response code="405">Invalid input</response>
        [HttpPost, Route("news_in_board")]
        public async Task<IActionResult> NewsInBoardAsync([FromForm]long? officerId, [FromForm]long boardId, [FromForm]int? pageIndex, [FromForm]int? pageSize)
        {
            try
            {
                var result = await BoardService.NewsInBoardAsync(officerId ?? 0, boardId, pageIndex, pageSize);
                return Ok(ActionResponse<PagingDataResult<NewsAllSearch>>.CreateSuccessResponse(result, "Success"));
            }
            catch (Exception ex)
            {
                return Ok(ActionResponse<string>.CreateErrorResponse(ex.Message));
            }
        }

        /// <summary>
        /// Tìm kiếm bảng tin
        /// </summary>
        /// <param name="search"></param>
        /// <returns></returns>
        /// <response code="401">Authorization has been denied for this request</response>
        /// <response code="404">Not Found</response>
        /// <response code="405">Invalid input</response>
        [HttpPost, Route("search")]
        public async Task<IActionResult> SearchAsync([FromForm]SearchBoard search)
        {
            try
            {
                if (!long.TryParse(User.FindFirst(ClaimTypes.NameIdentifier)?.Value, out long userId))
                {
                    return Ok(ActionResponse<string>.CreateErrorResponse("Account invalid."));
                }
                if (search == null)
                {
                    return Ok(ActionResponse<string>.CreateErrorResponse("Input invalid."));
                }
                search.FromDate = search.FromDate.HasValue ? DateTime.SpecifyKind(search.FromDate.Value, DateTimeKind.Unspecified) : DateTime.SpecifyKind(DateTime.MinValue, DateTimeKind.Unspecified);
                search.ToDate = search.ToDate.HasValue ? DateTime.SpecifyKind(search.ToDate.Value, DateTimeKind.Unspecified) : DateTime.SpecifyKind(DateTime.Now, DateTimeKind.Unspecified);

                search.FromDate = search.FromDate.Value.AddHours(0).AddMinutes(0).AddSeconds(0);
                search.ToDate = search.ToDate.Value.AddHours(23).AddMinutes(59).AddSeconds(59);
                search.PageIndex = search.PageIndex.HasValue ? search.PageIndex : 1;
                search.PageSize = search.PageSize.HasValue ? search.PageSize : 20;

                var result = await BoardService.SearchAsync(search, userId);
                return Ok(ActionResponse<PagingDataResult<BoardSearch>>.CreateSuccessResponse(result, "Success"));
            }
            catch (Exception ex)
            {
                return Ok(ActionResponse<string>.CreateErrorResponse(ex.Message));
            }
        }
    }
}