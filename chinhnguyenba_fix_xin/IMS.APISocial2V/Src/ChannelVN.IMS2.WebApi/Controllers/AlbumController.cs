﻿using ChannelVN.IMS2.Core.Entities;
using ChannelVN.IMS2.Core.Entities.Album;
using ChannelVN.IMS2.Core.Entities.Distribution;
using ChannelVN.IMS2.Core.Entities.Media;
using ChannelVN.IMS2.Core.Entities.Photo;
using ChannelVN.IMS2.Core.Entities.Security;
using ChannelVN.IMS2.Core.Repositories.Constants;
using ChannelVN.IMS2.Core.Repositories.Functions;
using ChannelVN.IMS2.Core.Services;
using ChannelVN.IMS2.Foundation.Common;
using ChannelVN.IMS2.Foundation.Common.Configuration;
using ChannelVN.IMS2.Foundation.Logging;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using static ChannelVN.IMS2.Core.Entities.ErrorCode.ErrorMapping;

namespace ChannelVN.IMS2.WebApi.Controllers
{
    [Route("api/[controller]")]
    public class AlbumController : BaseController
    {
        /// <summary>
        /// Lưu album
        /// </summary>
        /// <param name="albumData"></param>
        /// <param name="OfficerId"></param>
        /// <param name="isGallery"></param>
        /// <param name="cardType"></param>
        /// <returns></returns>
        /// <response code="401">Authorization has been denied for this request</response>
        /// <response code="404">Not Found</response>
        /// <response code="405">Invalid input</response>
        // POST api/album/save        
        [HttpPost, Route("save")]
        public async Task<IActionResult> Save([FromForm]AlbumSearch albumData, [FromForm]long OfficerId, [FromForm]bool isGallery = false,[FromForm]CardType cardType = CardType.Gallery1)
        {
            try
            {
                string sessionId = HttpContext?.Request?.Headers?["session-id"];
                if (string.IsNullOrEmpty(sessionId))
                {
                    return Ok(ActionResponse<string>.CreateErrorResponse("session-id not empty"));
                }
                var clientIP = HttpContext.GetClientIP();

                var result = ErrorCodes.Exception;
                var validateResult = ValidateAlbum(albumData);
                if (validateResult?.Success == false)
                {
                    return Ok(ActionResponse<string>.CreateErrorResponse(validateResult.Message));
                }

                if (!Utility.ValidateStringIds(albumData.Tags))
                {
                    return Ok(ActionResponse<string>.CreateErrorResponse("Tags không hợp lệ."));
                }

                var validPublishData = await Core.Services.Functions.Function.ValidatePublishData(albumData.PublishData, (int)CardType.Album);
                if (!validPublishData)
                {
                    return Ok(ActionResponse<string>.CreateErrorResponse("PublishData invalid."));
                }

                if (long.TryParse(User.FindFirst(ClaimTypes.NameIdentifier)?.Value, out long accountId))
                {
                    if (albumData.Id > 0)
                    {                        
                        var listItem = Foundation.Common.Json.Parse<PublishData>(albumData.PublishData);
                        albumData.PhotoInAlbum = listItem?.Items.Select(p => new PhotoInAlbum
                        {
                            AlbumId = albumData.Id,
                            PhotoId = long.Parse(p.Id),
                            PublishedDate = DateTime.Now
                        }).ToArray();

                        //update
                        albumData.ModifiedDate = DateTime.Now;
                        albumData.ModifiedBy = accountId + "";
                        albumData.CardType = (int)CardType.Album;
                        albumData.UnsignName = Utility.UnicodeToUnsignedAndDash(albumData.Name ?? "");

                        result = await AlbumService.UpdateAsync(albumData, clientIP, OfficerId, sessionId);
                        //Insert log action
                        if (result == ErrorCodes.Success)
                        {
                            await Function.AddToQueue(ActionName.InsertLog, TopicName.LOGACTION, new LogActionEntity
                            {
                                ObjectId = albumData.Id.ToString(),
                                Account = accountId.ToString(),
                                CreatedDate = DateTime.SpecifyKind(DateTime.Now, DateTimeKind.Unspecified),
                                Ip = clientIP,
                                Message = "Success.",
                                Title = albumData.Name,
                                SourceId = OfficerId.ToString(),
                                ActionTypeDetail = (int)ActionType.Update,
                                ActionText = ActionType.Update.ToString(),
                                Type = (int)LogContentType.Album
                            });
                        }
                    }
                    else
                    {
                        if (albumData.TemplateId == null || albumData.TemplateId <= 0) return Ok(ActionResponse<bool>.CreateErrorResponse("Chưa chọn template."));
                        //insert
                        albumData.Id = Generator.NewsId();
                        albumData.CardType = (int)CardType.Album;
                        albumData.Type = (int)NewsType.Album;
                        albumData.PublishMode = (int)NewsPublishMode.Public;
                        albumData.Status = (int)NewsStatus.Draft;
                        albumData.CreatedDate = DateTime.Now;
                        albumData.CreatedBy = accountId + "";
                        albumData.DistributorId = albumData.DistributorId.HasValue ? albumData.DistributorId : AppSettings.Current.Distribution.Automation.DistributorId;
                        albumData.UnsignName = Utility.UnicodeToUnsignedAndDash(albumData.Name ?? "");
                        albumData.LastInsertedDate = DateTime.Now;

                        if (long.TryParse(User.FindFirst(ClaimTypes.NameIdentifier)?.Value, out long userId))
                        {
                            albumData.NewsInAccount = new[] {
                                new NewsInAccount
                                {
                                    AccountId = OfficerId,
                                    NewsId = albumData.Id,
                                    PublishedDate = DateTime.Now,
                                    PublishedType = (int)NewsPublishedType.Post
                                }
                            };
                            //Get PhotoInAlbum in publishData
                            var listItem = Foundation.Common.Json.Parse<PublishData>(albumData.PublishData);
                            albumData.PhotoInAlbum = listItem?.Items.Select(p => new PhotoInAlbum
                            {
                                AlbumId = albumData.Id,
                                PhotoId = long.Parse(p.Id),
                                PublishedDate = DateTime.Now
                            }).ToArray();

                            result = await AlbumService.AddAsync(albumData, clientIP);
                            if (result == ErrorCodes.Success)
                            {
                                await Function.AddToQueue(ActionName.InsertLog, TopicName.LOGACTION, new LogActionEntity
                                {
                                    ObjectId = albumData.Id.ToString(),
                                    Account = accountId.ToString(),
                                    CreatedDate = DateTime.SpecifyKind(DateTime.Now, DateTimeKind.Unspecified),
                                    Ip = clientIP,
                                    Message = "Success.",
                                    Title = albumData.Name,
                                    SourceId = OfficerId.ToString(),
                                    ActionTypeDetail = (int)ActionType.Insert,
                                    ActionText = ActionType.Insert.ToString(),
                                    Type = (int)LogContentType.Album
                                });
                            }
                        }
                    }
                }
                else
                {
                    return Ok(ActionResponse<string>.CreateErrorResponse("Thông tin tài khoản ko hợp lệ."));
                }

                if (result == ErrorCodes.Success)
                {
                    return Ok(ActionResponse<string>.CreateSuccessResponse(albumData.Id.ToString(), "Success."));
                }
                else
                {
                    return Ok(ActionResponse<string>.CreateErrorResponse(Current[result]));
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
                return Ok(ActionResponse<string>.CreateErrorResponse(ex.Message));
            }
        }

        /// <summary>
        /// Lưu trữ album
        /// </summary>
        /// <param name="albumData"></param>
        /// <param name="OfficerId"></param>
        /// <returns></returns>
        /// <response code="401">Authorization has been denied for this request</response>
        /// <response code="404">Not Found</response>
        /// <response code="405">Invalid input</response>
        // POST api/album/save        
        [HttpPost, Route("archive")]
        public async Task<IActionResult> ArchiveAsync([FromForm]AlbumSearch albumData, [FromForm]long OfficerId)
        {
            try
            {
                string sessionId = HttpContext?.Request?.Headers?["session-id"];
                if (string.IsNullOrEmpty(sessionId))
                {
                    return Ok(ActionResponse<string>.CreateErrorResponse("session-id not empty"));
                }
                var clientIP = HttpContext.GetClientIP();

                var returnValue = ErrorCodes.Exception;
                var validateResult = ValidateAlbum(albumData);
                if (validateResult?.Success == false)
                {
                    return Ok(ActionResponse<string>.CreateErrorResponse(validateResult.Message));
                }
                if (!Utility.ValidateStringIds(albumData.Tags))
                {
                    return Ok(ActionResponse<string>.CreateErrorResponse("Tags không hợp lệ."));
                }

                if (long.TryParse(User.FindFirst(ClaimTypes.NameIdentifier)?.Value, out long accountId))
                {
                    if (albumData.Id > 0)
                    {
                        //Get PhotoInAlbum in publishData
                        var listItem = Foundation.Common.Json.Parse<PublishData>(albumData.PublishData);
                        albumData.PhotoInAlbum = listItem?.Items.Select(p => new PhotoInAlbum
                        {
                            AlbumId = albumData.Id,
                            PhotoId = long.Parse(p.Id),
                            PublishedDate = DateTime.Now
                        }).ToArray();

                        //update
                        albumData.Url = string.Format(AppSettings.Current.ChannelConfiguration.CommonSettings.UrlFormatMediaUnit, Utility.UnicodeToUnsignedAndDash(albumData.Name ?? ""), albumData.Id);
                        albumData.ModifiedDate = DateTime.Now;
                        albumData.ModifiedBy = accountId + "";
                        albumData.CardType = (int)CardType.Album;
                        albumData.UnsignName = Utility.UnicodeToUnsignedAndDash(albumData.Name ?? "");

                        returnValue = await AlbumService.UpdateAsync(albumData, clientIP, OfficerId, sessionId);
                        //Insert log action
                        if (returnValue == ErrorCodes.Success)
                        {
                            await Function.AddToQueue(ActionName.InsertLog, TopicName.LOGACTION, new LogActionEntity
                            {
                                ObjectId = albumData.Id.ToString(),
                                Account = accountId.ToString(),
                                CreatedDate = DateTime.SpecifyKind(DateTime.Now, DateTimeKind.Unspecified),
                                Ip = clientIP,
                                Message = "Success.",
                                Title = albumData.Name,
                                SourceId = OfficerId.ToString(),
                                ActionTypeDetail = (int)ActionType.Update,
                                ActionText = ActionType.Update.ToString(),
                                Type = (int)LogContentType.Album
                            });
                        }
                    }
                    else
                    {
                        if (albumData.TemplateId <= 0) return Ok(ActionResponse<bool>.CreateErrorResponse("Chưa chọn template."));
                        //insert
                        albumData.Id = Generator.NewsId();
                        albumData.Url = string.Format(AppSettings.Current.ChannelConfiguration.CommonSettings.UrlFormatMediaUnit, Utility.UnicodeToUnsignedAndDash(albumData.Name ?? ""), albumData.Id);
                        if (string.IsNullOrEmpty(albumData.OriginalUrl))
                            albumData.OriginalUrl = albumData.Url;
                        albumData.CardType = (int)CardType.Album;
                        albumData.Type = (int)NewsType.Album;
                        albumData.PublishMode = (int)NewsPublishMode.Public;
                        albumData.Status = (int)NewsStatus.Archive;
                        albumData.CreatedDate = DateTime.Now;
                        albumData.CreatedBy = accountId + "";
                        albumData.DistributorId = albumData.DistributorId.HasValue ? albumData.DistributorId : AppSettings.Current.Distribution.Automation.DistributorId;
                        albumData.DistributionDate = albumData.DistributionDate.HasValue ? albumData.DistributionDate : DateTime.Now;
                        albumData.UnsignName = Utility.UnicodeToUnsignedAndDash(albumData.Name ?? "");
                        albumData.LastInsertedDate = DateTime.Now;

                        albumData.NewsInAccount = new[] {
                            new NewsInAccount
                            {
                                AccountId  = OfficerId,
                                NewsId = albumData.Id,
                                PublishedDate = DateTime.Now,
                                PublishedType = (int)NewsPublishedType.Post
                            }
                        };

                        //Get PhotoInAlbum in publishData
                        var listItem = Foundation.Common.Json.Parse<PublishData>(albumData.PublishData);
                        albumData.PhotoInAlbum = listItem?.Items.Select(p => new PhotoInAlbum
                        {
                            AlbumId = albumData.Id,
                            PhotoId = long.Parse(p.Id),
                            PublishedDate = DateTime.Now
                        }).ToArray();

                        returnValue = await AlbumService.AddAsync(albumData, clientIP);
                        if (returnValue == ErrorCodes.Success)
                        {
                            await Function.AddToQueue(ActionName.InsertLog, TopicName.LOGACTION, new LogActionEntity
                            {
                                ObjectId = albumData.Id.ToString(),
                                Account = accountId.ToString(),
                                CreatedDate = DateTime.SpecifyKind(DateTime.Now, DateTimeKind.Unspecified),
                                Ip = clientIP,
                                Message = "Success.",
                                Title = albumData.Name,
                                SourceId = OfficerId.ToString(),
                                ActionTypeDetail = (int)ActionType.Insert,
                                ActionText = ActionType.Insert.ToString(),
                                Type = (int)LogContentType.Album
                            });
                        }
                    }
                }
                else
                {
                    returnValue = ErrorCodes.PermissionInvalid;
                }

                if (returnValue == ErrorCodes.Success)
                {
                    return Ok(ActionResponse<object>.CreateSuccessResponse(albumData.Id.ToString(), "Success."));
                }
                else
                {
                    return Ok(ActionResponse<string>.CreateErrorResponse(Current[returnValue]));
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
                return Ok(ActionResponse<string>.CreateErrorResponse(ex.Message));
            }
        }
        private ActionResponse<string> ValidateAlbum(AlbumSearch Album)
        {
            try
            {
                if (Album == null)
                {
                    return ActionResponse<string>.CreateErrorResponse("Params not valid.");
                }
                if (!Utility.ValidateStringIds(Album.Tags))
                    return ActionResponse<string>.CreateErrorResponse("Tags không hợp lệ.");
                if (string.IsNullOrEmpty(Album.PublishData) || !Album.PublishData.Contains("items"))
                    return ActionResponse<string>.CreateErrorResponse("Định dạng PublishData không đúng!");

                return ActionResponse<string>.CreateSuccessResponse("Success.");
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
                return ActionResponse<string>.CreateErrorResponse("Params not valid.");
            }
        }

        /// <summary>
        /// Cập nhận trạng thái
        /// </summary>
        /// <param name="id"></param>
        /// <param name="action"></param>
        /// <param name="boardIds"></param>
        /// <param name="officerId"></param>
        /// <returns></returns>
        /// <response code="401">Authorization has been denied for this request</response>
        /// <response code="404">Not Found</response>
        /// <response code="405">Invalid input</response>
        // POST api/article/update_status       
        [HttpPost, Route("update_status")]
        public async Task<IActionResult> UpdateStatusV2Async([FromForm]long id, [FromForm]NewsStatus action, [FromForm]string boardIds, [FromForm] long officerId)
        {
            try
            {
                string sessionId = HttpContext?.Request?.Headers?["session-id"];
                if (string.IsNullOrEmpty(sessionId))
                {
                    return Ok(ActionResponse<string>.CreateErrorResponse("session-id not empty"));
                }
                var clientIP = HttpContext.GetClientIP();

                var result = ErrorCodes.BusinessError;
                long.TryParse(User.FindFirst(ClaimTypes.NameIdentifier)?.Value, out long userId);

                if (!string.IsNullOrEmpty(boardIds) && !Utility.ValidateStringIds(boardIds))
                {
                    return Ok(ActionResponse<string>.CreateErrorResponse("BoardIds không hợp lệ."));
                }

                result = await AlbumService.UpdateStatusV2Async(id, action, userId, clientIP, sessionId);
                if (result == ErrorCodes.Success)
                {
                    //insert log
                    await Function.AddToQueue(ActionName.InsertLog, TopicName.LOGACTION, new LogActionEntity
                    {
                        ObjectId = id.ToString(),
                        Account = userId.ToString(),
                        CreatedDate = DateTime.SpecifyKind(DateTime.Now, DateTimeKind.Unspecified),
                        Ip = clientIP,
                        Message = "Success.",
                        Title = "",
                        SourceId = officerId.ToString(),
                        ActionTypeDetail = (int)ActionType.UpdateStatus,
                        ActionText = ActionType.UpdateStatus.ToString(),
                        Type = (int)LogContentType.Album
                    });
                    return Ok(ActionResponse<object>.CreateSuccessResponse(id.ToString(), "success"));
                }
                return Ok(ActionResponse<string>.CreateErrorResponse(Current[result]));
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
                return Ok(ActionResponse<string>.CreateErrorResponse(ex.Message));
            }
        }

        /// <summary>
        /// Lấy thông tin chi tiết album
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        /// <response code="401">Authorization has been denied for this request</response>
        /// <response code="404">Not Found</response>
        /// <response code="405">Invalid input</response>
        // POST api/album/get_by_id
        [HttpPost, Route("get_by_id")]
        public async Task<IActionResult> GetById([FromForm]long Id)
        {
            try
            {
                var result = await AlbumService.GetByIdAsync(Id);
                if (result != null && result.Id > 0) return Ok(ActionResponse<AlbumSearch>.CreateSuccessResponse(result));
                return Ok(ActionResponse<string>.CreateErrorResponse("Album not exist"));
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
                return Ok(ActionResponse<string>.CreateErrorResponse(ex.Message));
            }
        }

        /// <summary>
        /// Search album
        /// </summary>
        /// <param name="search"></param>
        /// <returns></returns>
        /// <response code="401">Authorization has been denied for this request</response>
        /// <response code="404">Not Found</response>
        /// <response code="405">Invalid input</response>
        // POST api/album/search        
        [HttpPost, Route("search")]
        public async Task<IActionResult> SearchAsync([FromForm] SearchNews search)
        {
            try
            {
                if (search == null)
                {
                    return Ok(ActionResponse<string>.CreateErrorResponse("Object search null."));
                }

                search.PageIndex = search.PageIndex == null || search.PageIndex < 1 ? 1 : search.PageIndex;
                search.PageSize = search.PageSize == null || search.PageSize < 1 ? 20 : search.PageSize;
                search.FromDate = search.FromDate.HasValue ? search.FromDate.Value.AddHours(0).AddMinutes(0).AddSeconds(0) : DateTime.MinValue;
                search.ToDate = search.ToDate.HasValue ? search.ToDate.Value.AddHours(23).AddMinutes(59).AddSeconds(59) : DateTime.MaxValue;
                long.TryParse(User.FindFirst(ClaimTypes.NameIdentifier)?.Value, out long accountId);
                
                var result = await AlbumService.SearchAsync(search, accountId);
                if (result != null)
                {
                    return Ok(ActionResponse<PagingDataResult<AlbumSearchReturn>>.CreateSuccessResponse(result));
                }

                return Ok(ActionResponse<PagingDataResult<AlbumSearchReturn>>.CreateSuccessResponse(new PagingDataResult<AlbumSearchReturn>()));
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
                return Ok(ActionResponse<string>.CreateErrorResponse(ex.Message));
            }
        }

        /// <summary>
        /// Lấy danh sách album đang chờ duyệt
        /// </summary>
        /// <param name="searchEntity"></param>
        /// <returns></returns>
        /// <response code="401">Authorization has been denied for this request</response>
        /// <response code="404">Not Found</response>
        /// <response code="405">Invalid input</response>
        [HttpPost, Route("get_list_approve")]
        public async Task<IActionResult> GetListApproveAsync([FromForm]SearchNews searchEntity)
        {
            try
            {
                if (searchEntity == null)
                {
                    return Ok(ActionResponse<string>.CreateSuccessResponse("Không tìm thấy album nào."));
                }
                if (searchEntity.OfficerId <= 0)
                {
                    return Ok(ActionResponse<string>.CreateErrorResponse("OfficerId không hợp lệ."));
                }

                searchEntity.Status = ((int)NewsStatus.Approve).ToString();
                searchEntity.PageIndex = searchEntity.PageIndex < 1 ? 1 : searchEntity.PageIndex;
                searchEntity.PageSize = searchEntity.PageSize < 1 ? 20 : searchEntity.PageSize;
                searchEntity.FromDate = searchEntity.FromDate.HasValue ? searchEntity.FromDate.Value.AddHours(0).AddMinutes(0).AddSeconds(0) : DateTime.MinValue;
                searchEntity.ToDate = searchEntity.ToDate.HasValue ? searchEntity.ToDate.Value.AddHours(23).AddMinutes(59).AddSeconds(59) : DateTime.Now;
                long.TryParse(User.FindFirst(ClaimTypes.NameIdentifier)?.Value, out long accountId);

                var result = await AlbumService.GetListApproveAsync(searchEntity, accountId);

                return Ok(ActionResponse<PagingDataResult<AlbumSearch>>.CreateSuccessResponse(result));
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
                return Ok(ActionResponse<string>.CreateErrorResponse(ex.Message));
            }
        }

        /// <summary>
        /// Danh sách photo trong album
        /// </summary>
        /// <param name="albumId"></param>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        /// <response code="401">Authorization has been denied for this request</response>
        /// <response code="404">Not Found</response>
        /// <response code="405">Invalid input</response>
        // POST api/videoplaylist/get_video_by_playlist_id => getway index 93
        [HttpPost, Route("photo_in_album")]
        public async Task<IActionResult> PhotoInAlbumIdAsync([FromForm]long albumId, [FromForm] int? pageIndex, [FromForm] int? pageSize)
        {
            try
            {
                pageIndex = pageIndex == null || pageIndex < 1 ? 1 : pageIndex;
                pageSize = pageSize == null || pageSize < 1 ? 20 : pageSize;
                var result = await AlbumService.PhotoInAlbumIdAsync(albumId, pageIndex, pageSize);
                return Ok(ActionResponse<PagingDataResult<PhotoUnit>>.CreateSuccessResponse(result));
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
                return Ok(ActionResponse<string>.CreateErrorResponse(ex.Message));
            }
        }

        /// <summary>
        /// Add vào bảng tin
        /// </summary>
        /// <param name="mediaId"></param>
        /// <param name="boardIds"></param>
        /// <param name="officerId"></param>
        /// <returns></returns>
        /// <response code="401">Authorization has been denied for this request</response>
        /// <response code="404">Not Found</response>
        /// <response code="405">Invalid input</response>
        // POST api/videoplaylist/get_video_by_playlist_id => getway index 93
        [HttpPost, Route("add_to_board")]
        public async Task<IActionResult> AddToBoardAsync([FromForm]long mediaId, [FromForm] long[] boardIds, [FromForm]long officerId)
        {
            try
            {
                var clientIP = HttpContext.GetClientIP();
                long.TryParse(User.FindFirst(ClaimTypes.NameIdentifier)?.Value, out long userId);

                var dataDb = await BoardService.GetListByIdAsync(boardIds);
                if (dataDb == null)
                {
                    return Ok(ActionResponse<string>.CreateErrorResponse("Bảng tin không tồn tại."));
                }
                else
                {
                    if (!(await BoardService.CheckBoardOfUser(officerId, userId, boardIds)))
                    {
                        return Ok(ActionResponse<string>.CreateErrorResponse("Bạn không có quyền chỉnh sửa Bảng tin."));
                    }
                }

                dataDb.ForEach(p =>
                {
                    p.LastInsertedDate = DateTime.Now;
                    p.ModifiedBy = userId.ToString();
                    p.ModifiedDate = DateTime.Now;
                });

                var result = await BoardService.InsertMediaAsync(mediaId, dataDb);
                if (result)
                {
                    foreach (var boardId in boardIds)
                    {
                        await Function.AddToQueue(ActionName.AddNewsToBoard, TopicName.PUSHBOARDTOCHANNEL, new QueueData<long, string, long, long>
                        {
                            Data1 = boardId,
                            Data2 = mediaId.ToString(),
                            Data3 = officerId,
                            Data4 = userId
                        });
                    }
                    
                    //insert log
                    await Function.AddToQueue(ActionName.InsertLog, TopicName.LOGACTION, new LogActionEntity
                    {
                        ObjectId = mediaId.ToString(),
                        Account = userId.ToString(),
                        CreatedDate = DateTime.SpecifyKind(DateTime.Now, DateTimeKind.Unspecified),
                        Ip = clientIP,
                        Message = "Success.",
                        Title = "",
                        SourceId = officerId.ToString(),
                        ActionTypeDetail = (int)ActionType.AddMediaToBoard,
                        ActionText = ActionType.AddMediaToBoard.ToString(),
                        Type = (int)LogContentType.Album
                    });
                    return Ok(ActionResponse<string>.CreateSuccessResponse("Add album success."));
                }
                return Ok(ActionResponse<string>.CreateErrorResponse("Add album unsuccess."));
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
                return Ok(ActionResponse<string>.CreateErrorResponse(ex.Message));
            }
        }
    }
}
