﻿using ChannelVN.IMS2.Core.Entities;
using ChannelVN.IMS2.Core.Entities.Media;
using ChannelVN.IMS2.Core.Entities.Posts;
using ChannelVN.IMS2.Core.Entities.Security;
using ChannelVN.IMS2.Core.Repositories.Constants;
using ChannelVN.IMS2.Core.Repositories.Functions;
using ChannelVN.IMS2.Core.Services;
using ChannelVN.IMS2.Foundation.Common;
using ChannelVN.IMS2.Foundation.Common.Configuration;
using ChannelVN.IMS2.Foundation.Logging;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using static ChannelVN.IMS2.Core.Entities.ErrorCode.ErrorMapping;

namespace ChannelVN.IMS2.WebApi.Controllers
{
    [Route("api/[controller]")]
    public class PostController : BaseController
    {
        /// <summary>
        /// Thêm mới hoặc Update Post
        /// </summary>
        /// <param name="post"></param>
        /// <param name="officerId"></param>
        /// <returns></returns>
        /// <response code="401">Authorization has been denied for this request</response>
        /// <response code="404">Not Found</response>
        /// <response code="405">Invalid input</response>
        // POST api/post/save => getway index 67
        [HttpPost, Route("save")]
        public async Task<IActionResult> SaveAsync([FromForm]Post post, [FromForm]long officerId)
        {
            try
            {
                string sessionId = HttpContext?.Request?.Headers?["session-id"];
                if (string.IsNullOrEmpty(sessionId))
                {
                    return Ok(ActionResponse<string>.CreateErrorResponse("session-id not empty"));
                }
                var clientIP = HttpContext.GetClientIP();

                var result = ErrorCodes.BusinessError;                
                var validateResult = ValidatePost(post);
                if (validateResult?.Success == false)
                {
                    return Ok(ActionResponse<string>.CreateErrorResponse(validateResult.Message));
                }

                var validPublishData = await Core.Services.Functions.Function.ValidatePublishData(post.PublishData, (int)CardType.Text);
                if (!validPublishData)
                {
                    return Ok(ActionResponse<string>.CreateErrorResponse("PublishData invalid."));
                }

                if (long.TryParse(User.FindFirst(ClaimTypes.NameIdentifier)?.Value, out long userId))
                {
                    if (post.Id > 0)
                    {
                        post.ModifiedDate = DateTime.Now;
                        post.CategoryId = post.CategoryId ?? 0;
                        post.ModifiedBy = userId.ToString();
                        post.Url = string.Format(AppSettings.Current.ChannelConfiguration.CommonSettings.UrlFormatVideo, Utility.UnicodeToUnsignedAndDash(post.GetType().Name), post.Id);
                        if (string.IsNullOrEmpty(post.OriginalUrl))
                            post.OriginalUrl = post.Url;

                        result = await PostService.UpdateAsync(post, officerId, clientIP, sessionId);

                        if (result == ErrorCodes.Success)
                        {
                            //insert log
                            await Function.AddToQueue(ActionName.InsertLog, TopicName.LOGACTION, new LogActionEntity
                            {
                                ObjectId = post.Id.ToString(),
                                Account = userId.ToString(),
                                CreatedDate = DateTime.SpecifyKind(DateTime.Now, DateTimeKind.Unspecified),
                                Ip = clientIP,
                                Message = "Success.",
                                Title = post.Caption,
                                SourceId = officerId.ToString(),
                                ActionTypeDetail = (int)ActionType.Update,
                                ActionText = ActionType.Update.ToString(),
                                Type = (int)LogContentType.Post
                            });
                        }
                    }
                    else
                    {
                        var postSearch = Mapper<PostSearch>.Map(post, new PostSearch());
                        postSearch.Type = (int)NewsType.Post;
                        postSearch.CardType = (int)CardType.Text;
                        postSearch.Id = Generator.NewsId();
                        post.Id = postSearch.Id;
                        postSearch.Status = (int)NewsStatus.Draft;
                        postSearch.CreatedDate = DateTime.Now;
                        postSearch.CategoryId = post.CategoryId ?? 0;
                        postSearch.PublishMode = (int)NewsPublishMode.Public;
                        postSearch.DistributorId = postSearch.DistributorId.HasValue ? postSearch.DistributorId : AppSettings.Current.Distribution.Automation.DistributorId;

                        postSearch.CreatedBy = userId.ToString();
                        postSearch.NewsInAccount = new[] {
                            new NewsInAccount
                            {
                                AccountId = officerId,
                                NewsId = postSearch.Id,
                                PublishedDate = DateTime.Now,
                                PublishedType = (int)NewsPublishedType.Post
                            }
                        };
                        postSearch.Url = string.Format(AppSettings.Current.ChannelConfiguration.CommonSettings.UrlFormatVideo, Utility.UnicodeToUnsignedAndDash(post.GetType().Name), postSearch.Id);
                        if (string.IsNullOrEmpty(postSearch.OriginalUrl))
                            postSearch.OriginalUrl = postSearch.Url;

                        result = await PostService.AddAsync(postSearch, clientIP);
                        if (result == ErrorCodes.Success)
                        {
                            //insert log
                            await Function.AddToQueue(ActionName.InsertLog, TopicName.LOGACTION, new LogActionEntity
                            {
                                ObjectId = postSearch.Id.ToString(),
                                Account = userId.ToString(),
                                CreatedDate = DateTime.SpecifyKind(DateTime.Now, DateTimeKind.Unspecified),
                                Ip = clientIP,
                                Message = "Success.",
                                Title = postSearch.Caption,
                                SourceId = officerId.ToString(),
                                ActionTypeDetail = (int)ActionType.Insert,
                                ActionText = ActionType.Insert.ToString(),
                                Type = (int)LogContentType.Post
                            });
                        }
                    }
                }
                else
                {
                    result = ErrorCodes.PermissionInvalid;
                }

                if (result == ErrorCodes.Success)
                {
                    return Ok(ActionResponse<object>.CreateSuccessResponse(post.EncryptId, "success"));
                }
                else
                {
                    return Ok(ActionResponse<string>.CreateErrorResponse(Current[result]));
                }
            }
            catch (Exception ex)
            {
                return Ok(ActionResponse<string>.CreateErrorResponse((int)ErrorCodes.Exception, ex.Message));
            }

        }

        /// <summary>
        /// Lưu trữ Post
        /// </summary>
        /// <param name="post"></param>
        /// <param name="officerId"></param>
        /// <returns></returns>
        /// <response code="401">Authorization has been denied for this request</response>
        /// <response code="404">Not Found</response>
        /// <response code="405">Invalid input</response>
        // POST api/post/save => getway index 67
        [HttpPost, Route("archive")]
        public async Task<IActionResult> ArchiveAsync([FromForm]Post post, [FromForm]long officerId)
        {
            try
            {
                string sessionId = HttpContext?.Request?.Headers?["session-id"];
                if (string.IsNullOrEmpty(sessionId))
                {
                    return Ok(ActionResponse<string>.CreateErrorResponse("session-id not empty"));
                }

                var result = ErrorCodes.BusinessError;

                var clientIP = string.IsNullOrEmpty(HttpContext?.Connection?.RemoteIpAddress?.ToString()) ? "192.168.1.1" : HttpContext?.Connection?.RemoteIpAddress?.ToString();
                var validateResult = ValidatePost(post);
                if (validateResult?.Success == false)
                {
                    return Ok(ActionResponse<string>.CreateErrorResponse(validateResult.Message));
                }
                if (long.TryParse(User.FindFirst(ClaimTypes.NameIdentifier)?.Value, out long userId))
                {
                    if (post.Id > 0)
                    {
                        post.ModifiedDate = DateTime.Now;
                        post.CategoryId = post.CategoryId ?? 0;
                        post.ModifiedBy = userId.ToString();
                        post.Url = string.Format(AppSettings.Current.ChannelConfiguration.CommonSettings.UrlFormatVideo, Utility.UnicodeToUnsignedAndDash(post.GetType().Name), post.Id);
                        if (string.IsNullOrEmpty(post.OriginalUrl))
                            post.OriginalUrl = post.Url;

                        result = await PostService.UpdateAsync(post, officerId, clientIP, sessionId);
                        if (result == ErrorCodes.Success)
                        {
                            //insert log
                            await Function.AddToQueue(ActionName.InsertLog, TopicName.LOGACTION, new LogActionEntity
                            {
                                ObjectId = post.Id.ToString(),
                                Account = userId.ToString(),
                                CreatedDate = DateTime.SpecifyKind(DateTime.Now, DateTimeKind.Unspecified),
                                Ip = clientIP,
                                Message = "Success.",
                                Title = post.Caption,
                                SourceId = post.ModifiedBy,
                                ActionTypeDetail = (int)ActionType.Update,
                                ActionText = ActionType.Update.ToString(),
                                Type = (int)LogContentType.Post
                            });
                        }
                    }
                    else
                    {
                        var postSearch = Mapper<PostSearch>.Map(post, new PostSearch());
                        postSearch.Type = (int)NewsType.Post;
                        postSearch.CardType = (int)CardType.Text;
                        postSearch.Id = Generator.NewsId();
                        post.Id = postSearch.Id;
                        postSearch.Status = (int)NewsStatus.Archive;
                        postSearch.CreatedDate = DateTime.Now;
                        postSearch.CategoryId = post.CategoryId ?? 0;
                        postSearch.PublishMode = (int)NewsPublishMode.Public;
                        postSearch.DistributorId = postSearch.DistributorId.HasValue ? postSearch.DistributorId : AppSettings.Current.Distribution.Automation.DistributorId;

                        postSearch.CreatedBy = userId.ToString();
                        postSearch.NewsInAccount = new[] {
                            new NewsInAccount
                            {
                                AccountId = officerId,
                                NewsId=postSearch.Id,
                                PublishedDate=DateTime.Now,
                                PublishedType=(int)NewsPublishedType.Post
                            }
                        };
                        postSearch.Url = string.Format(AppSettings.Current.ChannelConfiguration.CommonSettings.UrlFormatVideo, Utility.UnicodeToUnsignedAndDash(post.GetType().Name), postSearch.Id);
                        if (string.IsNullOrEmpty(postSearch.OriginalUrl))
                            postSearch.OriginalUrl = postSearch.Url;

                        result = await PostService.AddAsync(postSearch, clientIP);
                        if (result == ErrorCodes.Success)
                        {
                            //insert log
                            await Function.AddToQueue(ActionName.InsertLog, TopicName.LOGACTION, new LogActionEntity
                            {
                                ObjectId = postSearch.Id.ToString(),
                                Account = userId.ToString(),
                                CreatedDate = DateTime.SpecifyKind(DateTime.Now, DateTimeKind.Unspecified),
                                Ip = clientIP,
                                Message = "Success.",
                                Title = postSearch.Caption,
                                SourceId = postSearch.CreatedBy,
                                ActionTypeDetail = (int)ActionType.Insert,
                                ActionText = ActionType.Insert.ToString(),
                                Type = (int)LogContentType.Post
                            });
                        }
                    }
                }
                else
                {
                    result = ErrorCodes.PermissionInvalid;
                }

                if (result == ErrorCodes.Success)
                {
                    return Ok(ActionResponse<object>.CreateSuccessResponse(post.EncryptId, "success"));
                }
                else
                {
                    return Ok(ActionResponse<string>.CreateErrorResponse(Current[result]));
                }
            }
            catch (Exception ex)
            {
                return Ok(ActionResponse<string>.CreateErrorResponse((int)ErrorCodes.Exception, ex.Message));
            }
        }

        /// <summary>
        /// Lấy thông tin chi tiết của Post
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        /// <response code="401">Authorization has been denied for this request</response>
        /// <response code="404">Not Found</response>
        /// <response code="405">Invalid input</response>
        // POST api/video/get_by_id        
        [HttpPost, Route("get_by_id")]
        public async Task<IActionResult> GetByIdAsync([FromForm]long id)
        {
            try
            {
                var result = await PostService.GetPostSearchByIdAsync(id);
                if (result != null && result.Id > 0) return Ok(ActionResponse<PostSearch>.CreateSuccessResponse(result));
                return Ok(ActionResponse<string>.CreateErrorResponse("Post not exist."));
            }
            catch (Exception ex)
            {
                return Ok(ActionResponse<string>.CreateErrorResponse(ex.Message));
            }
        }

        /// <summary>
        /// Cập nhận trạng thái
        /// </summary>
        /// <param name="id"></param>
        /// <param name="action"></param>
        /// <param name="officerId"></param>
        /// <returns></returns>
        /// <response code="401">Authorization has been denied for this request</response>
        /// <response code="404">Not Found</response>
        /// <response code="405">Invalid input</response>
        // POST api/article/update_status       
        [HttpPost, Route("update_status")]
        public async Task<IActionResult> UpdateStatusV2Async([FromForm]long id, [FromForm]NewsStatus action, [FromForm] long officerId)
        {
            try
            {
                string sessionId = HttpContext.GetSessionId();
                if (string.IsNullOrEmpty(sessionId))
                {
                    return Ok(ActionResponse<string>.CreateErrorResponse("session-id not empty"));
                }
                var clientIP = HttpContext.GetClientIP();

                var result = ErrorCodes.BusinessError;
                long.TryParse(User.FindFirst(ClaimTypes.NameIdentifier)?.Value, out long userId);
                result = await PostService.UpdateStatusV2Async(id, action, userId, clientIP, sessionId);
                if (result == ErrorCodes.Success)
                {
                    //insert log
                    await Function.AddToQueue(ActionName.InsertLog, TopicName.LOGACTION, new LogActionEntity
                    {
                        ObjectId = id.ToString(),
                        Account = userId.ToString(),
                        CreatedDate = DateTime.SpecifyKind(DateTime.Now, DateTimeKind.Unspecified),
                        Ip = clientIP,
                        Message = "Success.",
                        Title = "",
                        SourceId = officerId.ToString(),
                        ActionTypeDetail = (int)ActionType.UpdateStatus,
                        ActionText = ActionType.UpdateStatus.ToString(),
                        Type = (int)LogContentType.Post
                    });
                    return Ok(ActionResponse<object>.CreateSuccessResponse(id.ToString(), "success"));
                }
                return Ok(ActionResponse<string>.CreateErrorResponse(Current[result]));
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
                return Ok(ActionResponse<string>.CreateErrorResponse(ex.Message));
            }
        }

        /// <summary>
        /// Tìm kiếm Post
        /// </summary>
        /// <param name="searchEntity"></param>
        /// <returns></returns>
        /// <response code="401">Authorization has been denied for this request</response>
        /// <response code="404">Not Found</response>
        /// <response code="405">Invalid input</response>
        // POST api/post/search => getway index 71
        [HttpPost, Route("search")]
        public async Task<IActionResult> SearchAsync([FromForm]SearchNews searchEntity)
        {
            try
            {
                if (searchEntity == null)
                {
                    return Ok(ActionResponse<string>.CreateSuccessResponse("Không tìm thấy bài viết nào."));
                }
                searchEntity.PageIndex = searchEntity.PageIndex < 1 ? 1 : searchEntity.PageIndex;
                searchEntity.PageSize = searchEntity.PageSize < 1 ? 20 : searchEntity.PageSize;
                searchEntity.FromDate = searchEntity.FromDate.HasValue ? searchEntity.FromDate.Value.AddHours(0).AddMinutes(0).AddSeconds(0) : DateTime.MinValue;
                searchEntity.ToDate = searchEntity.ToDate.HasValue ? searchEntity.ToDate.Value.AddHours(23).AddMinutes(59).AddSeconds(59) : DateTime.MaxValue;
                long.TryParse(User.FindFirst(ClaimTypes.NameIdentifier)?.Value, out long userId);

                var result = await PostService.SearchAsync(searchEntity, userId);
                return Ok(ActionResponse<PagingDataResult<PostSearchReturn>>.CreateSuccessResponse(result));
            }
            catch (Exception ex)
            {
                return Ok(ActionResponse<string>.CreateErrorResponse(ex.Message));
            }
        }

        /// <summary>
        /// Lấy danh sách post đang chờ duyệt
        /// </summary>
        /// <param name="searchEntity"></param>
        /// <returns></returns>
        /// <response code="401">Authorization has been denied for this request</response>
        /// <response code="404">Not Found</response>
        /// <response code="405">Invalid input</response>
        [HttpPost, Route("get_list_approve")]
        public async Task<IActionResult> GetListApproveAsync([FromForm]SearchNews searchEntity)
        {
            try
            {
                if (searchEntity == null)
                {
                    return Ok(ActionResponse<string>.CreateSuccessResponse("Không tìm thấy post nào."));
                }
                if (searchEntity.OfficerId <= 0)
                {
                    return Ok(ActionResponse<string>.CreateErrorResponse("OfficerId không hợp lệ."));
                }

                searchEntity.Status = ((int)NewsStatus.Approve).ToString();
                searchEntity.PageIndex = searchEntity.PageIndex < 1 ? 1 : searchEntity.PageIndex;
                searchEntity.PageSize = searchEntity.PageSize < 1 ? 20 : searchEntity.PageSize;
                searchEntity.FromDate = searchEntity.FromDate.HasValue ? searchEntity.FromDate.Value.AddHours(0).AddMinutes(0).AddSeconds(0) : DateTime.MinValue;
                searchEntity.ToDate = searchEntity.ToDate.HasValue ? searchEntity.ToDate.Value.AddHours(23).AddMinutes(59).AddSeconds(59) : DateTime.Now;
                long.TryParse(User.FindFirst(ClaimTypes.NameIdentifier)?.Value, out long accountId);

                var result = await PostService.GetListApproveAsync(searchEntity, accountId);

                return Ok(ActionResponse<PagingDataResult<Post>>.CreateSuccessResponse(result));
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
                return Ok(ActionResponse<string>.CreateErrorResponse(ex.Message));
            }
        }

        private ActionResponse<string> ValidatePost(Post post)
        {
            if (post == null)
            {
                return ActionResponse<string>.CreateErrorResponse("Không có post.");
            }
            if (string.IsNullOrEmpty(post.Caption))
            {
                return ActionResponse<string>.CreateErrorResponse("Nội dung không được để trống.");
            }
            if (string.IsNullOrEmpty(post.PublishData))
                return ActionResponse<string>.CreateErrorResponse("Nội dung không được để trống.");// 
            if (!Utility.ValidateStringIds(post.Tags))
                return ActionResponse<string>.CreateErrorResponse("Tags không hợp lệ.");// 
            return ActionResponse<string>.CreateSuccessResponse(Current[ErrorCodes.Success]);// "Pass :)");
        }

        /// <summary>
        /// Add vào bảng tin
        /// </summary>
        /// <param name="mediaId"></param>
        /// <param name="boardIds"></param>
        /// <param name="officerId"></param>
        /// <returns></returns>
        /// <response code="401">Authorization has been denied for this request</response>
        /// <response code="404">Not Found</response>
        /// <response code="405">Invalid input</response>
        // POST api/videoplaylist/get_video_by_playlist_id => getway index 93
        [HttpPost, Route("add_to_board")]
        public async Task<IActionResult> AddToBoardAsync([FromForm]long mediaId, [FromForm] long[] boardIds, [FromForm]long officerId)
        {
            try
            {
                var clientIP = string.IsNullOrEmpty(HttpContext?.Connection?.RemoteIpAddress?.ToString()) ? "192.168.1.1" : HttpContext?.Connection?.RemoteIpAddress?.ToString();
                long.TryParse(User.FindFirst(ClaimTypes.NameIdentifier)?.Value, out long userId);
                var dataDb = await BoardService.GetListByIdAsync(boardIds);
                if (dataDb == null)
                {
                    return Ok(ActionResponse<string>.CreateErrorResponse("Bảng tin không tồn tại."));
                }
                else
                {
                    if (!(await BoardService.CheckBoardOfUser(officerId, userId, boardIds)))
                    {
                        return Ok(ActionResponse<string>.CreateErrorResponse("Bạn không có quyền chỉnh sửa Bảng tin."));
                    }
                }

                dataDb.ForEach(p =>
                {
                    p.LastInsertedDate = DateTime.Now;
                    p.ModifiedBy = userId.ToString();
                    p.ModifiedDate = DateTime.Now;
                });
                var result = await BoardService.InsertMediaAsync(mediaId, dataDb);
                if (result)
                {
                    //share to app
                    foreach (var boardId in boardIds)
                    {
                        await Function.AddToQueue(ActionName.AddNewsToBoard, TopicName.PUSHBOARDTOCHANNEL, new QueueData<long, string, long, long>
                        {
                            Data1 = boardId,
                            Data2 = mediaId.ToString(),
                            Data3 = officerId,
                            Data4 = userId
                        });
                    }
                    
                    //insert log
                    await Function.AddToQueue(ActionName.InsertLog, TopicName.LOGACTION, new LogActionEntity
                    {
                        ObjectId = mediaId.ToString(),
                        Account = userId.ToString(),
                        CreatedDate = DateTime.SpecifyKind(DateTime.Now, DateTimeKind.Unspecified),
                        Ip = clientIP,
                        Message = "Success.",
                        Title = "",
                        SourceId = officerId.ToString(),
                        ActionTypeDetail = (int)ActionType.AddMediaToBoard,
                        ActionText = ActionType.AddMediaToBoard.ToString(),
                        Type = (int)LogContentType.Post
                    });
                    return Ok(ActionResponse<string>.CreateSuccessResponse("Success."));
                }
                return Ok(ActionResponse<string>.CreateErrorResponse("Lỗi nghiệp vụ."));
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
                return Ok(ActionResponse<string>.CreateErrorResponse(ex.Message));
            }
        }
    }
}
