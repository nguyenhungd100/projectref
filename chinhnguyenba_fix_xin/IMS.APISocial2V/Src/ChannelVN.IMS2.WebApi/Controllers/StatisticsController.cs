﻿using ChannelVN.IMS2.Core.Entities.Statistic.Rearch.Request;
using ChannelVN.IMS2.Core.Entities.Statistic.Rearch.Response;
using ChannelVN.IMS2.Core.Entities.Statistic.Video;
using ChannelVN.IMS2.Core.Entities.Statistics;
using ChannelVN.IMS2.Core.Services;
using ChannelVN.IMS2.Foundation.Common;
using ChannelVN.IMS2.Foundation.Common.Configuration;
using ChannelVN.IMS2.Foundation.Logging;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace ChannelVN.IMS2.WebApi.Controllers
{
    [Route("api/[controller]")]
    public class StatisticsController : BaseController
    {
        /// <summary>
        /// Thống kê số lượng bài xuất bản của một Page
        /// </summary>
        /// <param name="search"></param>
        /// <returns></returns>
        /// <response code="401">Authorization has been denied for this request</response>
        /// <response code="404">Not Found</response>
        /// <response code="405">Invalid input</response>
        //[DisableCors]
        [AllowAnonymous]
        [HttpPost, Route("publish_news_count")]
        public async Task<IActionResult> PublishNewsCountAsync([FromForm]GetNewsCount search)
        {
            try
            {
                if (search.FromDate == null)
                {
                    return Ok(ActionResponse<string>.CreateErrorResponse("Bạn phải nhập \"từ ngày\"."));
                }

                search.FromDate = search.FromDate.HasValue ? search.FromDate.Value.AddHours(0).AddMinutes(0).AddSeconds(0) : DateTime.MinValue;
                search.ToDate = search.ToDate.HasValue ? search.ToDate.Value.AddHours(23).AddMinutes(59).AddSeconds(59) : DateTime.Now;

                var result = await StatisticsService.PublishNewsCountAsync(search);
                return Ok(ActionResponse<Dictionary<DateTime, NewsCount2[]>>.CreateSuccessResponse(result));
            }
            catch(Exception ex)
            {
                Logger.Error(ex, ex.Message);
                return Ok(ActionResponse<string>.CreateErrorResponse(ex.Message));
            }
            
        }

        /// <summary>
        /// Thống kê số lượng bài xuất bản của một Page
        /// </summary>
        /// <param name="search"></param>
        /// <returns></returns>
        /// <response code="401">Authorization has been denied for this request</response>
        /// <response code="404">Not Found</response>
        /// <response code="405">Invalid input</response>
        [AllowAnonymous]
        [HttpPost, Route("publish_news_count_of_user_on_page")]
        public async Task<IActionResult> PublishNewsCountOfUserOnPageAsync([FromForm]GetNewsCountOfUser search)
        {
            try
            {
                if (search.FromDate == null)
                {
                    return Ok(ActionResponse<string>.CreateErrorResponse("Bạn phải nhập \"từ ngày\"."));
                }

                //if (long.TryParse(User.FindFirst(ClaimTypes.NameIdentifier)?.Value, out long accountId))
                //{
                //    if (search.AccountId == null || search.AccountId == 0) search.AccountId = accountId;
                //}

                search.FromDate = search.FromDate.HasValue ? search.FromDate.Value.AddHours(0).AddMinutes(0).AddSeconds(0) : DateTime.MinValue;
                search.ToDate = search.ToDate.HasValue ? search.ToDate.Value.AddHours(23).AddMinutes(59).AddSeconds(59) : DateTime.Now;

                var result = await StatisticsService.PublishNewsCountOfUserOnPageAsync(search);
                return Ok(ActionResponse<Dictionary<DateTime, NewsCount[]>>.CreateSuccessResponse(result));
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
                return Ok(ActionResponse<string>.CreateErrorResponse(ex.Message));
            }

        }

        /// <summary>
        /// Danh sách Page
        /// </summary>
        /// <param name="search"></param>
        /// <returns></returns>
        /// <response code="401">Authorization has been denied for this request</response>
        /// <response code="404">Not Found</response>
        /// <response code="405">Invalid input</response>
        // POST api/post/save => getway index 67
        [AllowAnonymous]
        [HttpPost, Route("list_page")]
        public async Task<IActionResult> ListPageAsync([FromForm]SearchPage search)
        {
            try
            {
                if (search == null)
                {
                    return Ok(ActionResponse<string>.CreateErrorResponse("Input invalid."));
                }

                search.PageIndex = search.PageIndex < 1 ? 1 : search.PageIndex;
                search.PageSize = search.PageSize < 1 ? 20 : search.PageSize;

                var result = await StatisticsService.ListPageAsync(search);
                return Ok(ActionResponse<PagingDataResult<object>>.CreateSuccessResponse(result));
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
                return Ok(ActionResponse<string>.CreateErrorResponse(ex.Message));
            }

        }

        /// <summary>
        /// Thống kê lượt follower page
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        /// <response code="401">Authorization has been denied for this request</response>
        /// <response code="404">Not Found</response>
        /// <response code="405">Invalid input</response>
        // POST api/post/save => getway index 67
        [HttpPost, Route("get_followers_on_page")]
        public async Task<IActionResult> GetFollowersOnPagePAsync([FromForm]FollowerRequestDataModel request)
        {
            try
            {
                //if (long.TryParse(User.FindFirst(ClaimTypes.NameIdentifier)?.Value, out long accountId))
                //{
                //    if (search.AccountId == null || search.AccountId == 0) search.AccountId = accountId;
                //}

                request.StartDate = request.StartDate.AddHours(0).AddMinutes(0).AddSeconds(0);
                request.EndDate = request.EndDate.AddHours(23).AddMinutes(59).AddSeconds(59);

                var result = await StatisticsService.GetFollowersOnPagePAsync(request);
                if (result != null && result.Success)
                {
                    return Ok(ActionResponse<Dictionary<DateTime, long>>.CreateSuccessResponse(result.Data));
                }
                else
                {
                    return Ok(ActionResponse<string>.CreateErrorResponse(result?.Message));
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
                return Ok(ActionResponse<string>.CreateErrorResponse(ex.Message));
            }

        }

        /// <summary>
        /// Thống kê lượt tương tác trên page/post
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        /// <response code="401">Authorization has been denied for this request</response>
        /// <response code="404">Not Found</response>
        /// <response code="405">Invalid input</response>
        // POST api/post/save => getway index 67
        [HttpPost, Route("get_interactions_on_page_or_post")]
        public async Task<IActionResult> GetInteractionsOnPageOrPostPAsync([FromForm]InteractionsRequestDataModel request)
        {
            try
            {
                //if (long.TryParse(User.FindFirst(ClaimTypes.NameIdentifier)?.Value, out long accountId))
                //{
                //    if (search.AccountId == null || search.AccountId == 0) search.AccountId = accountId;
                //}

                request.StartDate = request.StartDate.AddHours(0).AddMinutes(0).AddSeconds(0);
                request.EndDate = request.EndDate.AddHours(23).AddMinutes(59).AddSeconds(59);

                var result = await StatisticsService.GetInteractionsOnPageOrPostPAsync(request);
                return Ok(ActionResponse<Dictionary<DateTime, GetInteractionsResponseDataModel>>.CreateSuccessResponse(result));
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
                return Ok(ActionResponse<string>.CreateErrorResponse(ex.Message));
            }

        }

        /// <summary>
        /// Thống kê lượt tương tác trên page/post
        /// </summary>
        /// <param name="ids"></param>
        /// <param name="fromDate"></param>
        /// <param name="toDate"></param>
        /// <returns></returns>
        /// <response code="401">Authorization has been denied for this request</response>
        /// <response code="404">Not Found</response>
        /// <response code="405">Invalid input</response>
        // POST api/post/save => getway index 67
        [HttpGet, Route("get_interactions_on_post")]
        public async Task<IActionResult> GetInteractionsOnPostPAsync([FromQuery]string ids,[FromQuery]DateTime? fromDate, [FromQuery]DateTime? toDate)
        {
            try
            {
                if(string.IsNullOrEmpty(ids) || !fromDate.HasValue || !toDate.HasValue)
                {
                    return Ok(ActionResponse<string>.CreateErrorResponse("Input invalid."));
                }
                //if (long.TryParse(User.FindFirst(ClaimTypes.NameIdentifier)?.Value, out long accountId))
                //{
                //    if (search.AccountId == null || search.AccountId == 0) search.AccountId = accountId;
                //}

                //request.StartDate = request.StartDate.AddHours(0).AddMinutes(0).AddSeconds(0);
                //request.EndDate = request.EndDate.AddHours(23).AddMinutes(59).AddSeconds(59);

                //var result = await StatisticsService.GetInteractionsOnPageOrPostPAsync(request);


                var data = new Dictionary<string, Dictionary<string, object>>();

                foreach(var id in ids.Split(","))
                {
                    data.Add(id, new Dictionary<string, object>()
                    {
                        {
                                DateTime.Now.AddDays(-2).ToString("yyyy-MM-dd"), new {
                                    Comment = 12,
                                    Date = DateTime.Now,
                                    Like = 13,
                                    Post = 14,
                                    Repost = 15,
                                    Send = 16,
                                    Token = 17,
                                    Reach = 18
                                }
                         },
                         {
                                DateTime.Now.AddDays(-1).ToString("yyyy-MM-dd"), new {
                                    Comment = 12,
                                    Date = DateTime.Now,
                                    Like = 13,
                                    Post = 14,
                                    Repost = 15,
                                    Send = 16,
                                    Token = 17,
                                    Reach = 18
                                }
                         },
                         {
                                DateTime.Now.ToString("yyyy-MM-dd"), new {
                                    Comment = 12,
                                    Date = DateTime.Now,
                                    Like = 13,
                                    Post = 14,
                                    Repost = 15,
                                    Send = 16,
                                    Token = 17,
                                    Reach = 18
                                }
                         }
                    });
                }

                return Ok(ActionResponse<Dictionary<string, Dictionary<string, object>>>.CreateSuccessResponse(data));
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
                return Ok(ActionResponse<string>.CreateErrorResponse(ex.Message));
            }

        }

        /// <summary>
        /// Thống kê lượt share post của 1 post
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        /// <response code="401">Authorization has been denied for this request</response>
        /// <response code="404">Not Found</response>
        /// <response code="405">Invalid input</response>
        // POST api/post/save => getway index 67
        [HttpPost, Route("get_post_count_of_post")]
        public async Task<IActionResult> GetPostCountOfPostPAsync([FromForm]FollowerRequestDataModel request)
        {
            try
            {
                //if (long.TryParse(User.FindFirst(ClaimTypes.NameIdentifier)?.Value, out long accountId))
                //{
                //    if (search.AccountId == null || search.AccountId == 0) search.AccountId = accountId;
                //}

                request.StartDate = request.StartDate.AddHours(0).AddMinutes(0).AddSeconds(0);
                request.EndDate = request.EndDate.AddHours(23).AddMinutes(59).AddSeconds(59);

                var result = await StatisticsService.GetPostCountOfPostPAsync(request);
                return Ok(ActionResponse<Dictionary<DateTime, int>>.CreateSuccessResponse(result));
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
                return Ok(ActionResponse<string>.CreateErrorResponse(ex.Message));
            }

        }

        /// <summary>
        /// Thống kê lượt share post của tất cả posts của 1 page
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        /// <response code="401">Authorization has been denied for this request</response>
        /// <response code="404">Not Found</response>
        /// <response code="405">Invalid input</response>
        // POST api/post/save => getway index 67
        [HttpPost, Route("get_post_count_of_all_post_of_page")]
        public async Task<IActionResult> GetPostCountOfAllPostOfPageAsync([FromForm]FollowerRequestDataModel request)
        {
            try
            {
                //if (long.TryParse(User.FindFirst(ClaimTypes.NameIdentifier)?.Value, out long accountId))
                //{
                //    if (search.AccountId == null || search.AccountId == 0) search.AccountId = accountId;
                //}

                request.StartDate = request.StartDate.AddHours(0).AddMinutes(0).AddSeconds(0);
                request.EndDate = request.EndDate.AddHours(23).AddMinutes(59).AddSeconds(59);

                var result = await StatisticsService.GetPostCountOfAllPostOfPageAsync(request);
                return Ok(ActionResponse<Dictionary<DateTime, int>>.CreateSuccessResponse(result));
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
                return Ok(ActionResponse<string>.CreateErrorResponse(ex.Message));
            }

        }

        /// <summary>
        /// Thống kê lượt repost của tất cả posts của 1 page
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        /// <response code="401">Authorization has been denied for this request</response>
        /// <response code="404">Not Found</response>
        /// <response code="405">Invalid input</response>
        // POST api/post/save => getway index 67
        [HttpPost, Route("get_repost_count_of_all_post_of_page")]
        public async Task<IActionResult> GetRepostCountOfAllPostOfPageAsync([FromForm]FollowerRequestDataModel request)
        {
            try
            {
                //if (long.TryParse(User.FindFirst(ClaimTypes.NameIdentifier)?.Value, out long accountId))
                //{
                //    if (search.AccountId == null || search.AccountId == 0) search.AccountId = accountId;
                //}

                request.StartDate = request.StartDate.AddHours(0).AddMinutes(0).AddSeconds(0);
                request.EndDate = request.EndDate.AddHours(23).AddMinutes(59).AddSeconds(59);

                var result = await StatisticsService.GetRepostCountOfAllPostOfPageAsync(request);
                return Ok(ActionResponse<Dictionary<DateTime, int>>.CreateSuccessResponse(result));
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
                return Ok(ActionResponse<string>.CreateErrorResponse(ex.Message));
            }

        }


        #region http://cmsanalytics.admicro.vn/analytics/vda/api-doc/pv/index.html
        /// <summary>
        /// Thống kê User overview
        /// </summary>
        /// <param name="pageIds"></param>
        /// <param name="fromDate"></param>
        /// <param name="toDate"></param>
        /// <returns></returns>
        /// <response code="401">Authorization has been denied for this request</response>
        /// <response code="404">Not Found</response>
        /// <response code="405">Invalid input</response>
        // POST api/post/save => getway index 67
        [HttpGet, Route("user_overview")]
        public async Task<IActionResult> GetUserOverviewAsync([FromQuery]string pageIds, [FromQuery]string fromDate, [FromQuery]string toDate)
        {
            try
            {
                //if (long.TryParse(User.FindFirst(ClaimTypes.NameIdentifier)?.Value, out long accountId))
                //{
                //    if (search.AccountId == null || search.AccountId == 0) search.AccountId = accountId;
                //}

                var dateFormat = AppSettings.Current.ChannelConfiguration.DateFormat;

                var _fromDate = Utility.ConvertToDateTime(fromDate, dateFormat);
                var _toDate = Utility.ConvertToDateTime(toDate, dateFormat);
                _fromDate = _fromDate != null ? _fromDate.Value.AddHours(0).AddMinutes(0).AddSeconds(0) : DateTime.Today;
                _toDate = _toDate != null ? _toDate.Value.AddHours(23).AddMinutes(59).AddSeconds(59) : DateTime.Now;

                if (_fromDate > _toDate || string.IsNullOrEmpty(pageIds))
                {
                    return Ok(ActionResponse<string>.CreateErrorResponse("input invalid."));
                }
                //var duration = Enumerable.Range(0, 1 + _toDate.Value.Subtract(_fromDate.Value).Days).Select(offset => _fromDate.Value.AddDays(offset)).ToArray();
                var returnValue = await StatisticsService.GetUserOverviewAsync(pageIds, _fromDate.Value, _toDate.Value);
                //var result = ids.Split(",").ToDictionary(p => p, p =>;

                return Ok(ActionResponse<object>.CreateSuccessResponse(returnValue));
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
                return Ok(ActionResponse<string>.CreateErrorResponse(ex.Message));
            }
        }
        /// <summary>
        /// Thống kê tương tác theo các loại nội dung
        /// </summary>
        /// <param name="pageId"></param>
        /// <param name="fromDate"></param>
        /// <param name="toDate"></param>
        /// <returns></returns>
        /// <response code="401">Authorization has been denied for this request</response>
        /// <response code="404">Not Found</response>
        /// <response code="405">Invalid input</response>
        // POST api/post/save => getway index 67
        [HttpGet, Route("page/action_by_content_type")]
        public async Task<IActionResult> GetActionByContentTypeAsync([FromQuery]long pageId, [FromQuery]string fromDate, [FromQuery]string toDate)
        {
            try
            {
                //if (long.TryParse(User.FindFirst(ClaimTypes.NameIdentifier)?.Value, out long accountId))
                //{
                //    if (search.AccountId == null || search.AccountId == 0) search.AccountId = accountId;
                //}

                var dateFormat = AppSettings.Current.ChannelConfiguration.DateFormat;

                var _fromDate = Utility.ConvertToDateTime(fromDate, dateFormat);
                var _toDate = Utility.ConvertToDateTime(toDate, dateFormat);
                _fromDate = _fromDate != null ? _fromDate.Value.AddHours(0).AddMinutes(0).AddSeconds(0) : DateTime.Today;
                _toDate = _toDate != null ? _toDate.Value.AddHours(23).AddMinutes(59).AddSeconds(59) : DateTime.Now;

                if (_fromDate > _toDate)
                {
                    return Ok(ActionResponse<string>.CreateErrorResponse("input invalid."));
                }
                //var duration = Enumerable.Range(0, 1 + _toDate.Value.Subtract(_fromDate.Value).Days).Select(offset => _fromDate.Value.AddDays(offset)).ToArray();
                var returnValue = await StatisticsService.GetActionByContentTypeAsync(pageId, _fromDate.Value, _toDate.Value);
                //var result = ids.Split(",").ToDictionary(p => p, p =>;

                return Ok(ActionResponse<object>.CreateSuccessResponse(returnValue));
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
                return Ok(ActionResponse<string>.CreateErrorResponse(ex.Message));
            }
        }
        /// <summary>
        /// Thống kê lượt xem profile
        /// </summary>
        /// <param name="pageId"></param>
        /// <param name="fromDate"></param>
        /// <param name="toDate"></param>
        /// <returns></returns>
        /// <response code="401">Authorization has been denied for this request</response>
        /// <response code="404">Not Found</response>
        /// <response code="405">Invalid input</response>
        // POST api/post/save => getway index 67
        [HttpGet, Route("page/viewprofile")]
        public async Task<IActionResult> GetViewProfileAsync([FromQuery]long pageId, [FromQuery]string fromDate, [FromQuery]string toDate)
        {
            try
            {
                //if (long.TryParse(User.FindFirst(ClaimTypes.NameIdentifier)?.Value, out long accountId))
                //{
                //    if (search.AccountId == null || search.AccountId == 0) search.AccountId = accountId;
                //}

                var dateFormat = AppSettings.Current.ChannelConfiguration.DateFormat;

                var _fromDate = Utility.ConvertToDateTime(fromDate, dateFormat);
                var _toDate = Utility.ConvertToDateTime(toDate, dateFormat);
                _fromDate = _fromDate != null ? _fromDate.Value.AddHours(0).AddMinutes(0).AddSeconds(0) : DateTime.Today;
                _toDate = _toDate != null ? _toDate.Value.AddHours(23).AddMinutes(59).AddSeconds(59) : DateTime.Now;

                if (_fromDate > _toDate)
                {
                    return Ok(ActionResponse<string>.CreateErrorResponse("input invalid."));
                }
                //var duration = Enumerable.Range(0, 1 + _toDate.Value.Subtract(_fromDate.Value).Days).Select(offset => _fromDate.Value.AddDays(offset)).ToArray();
                var returnValue = await StatisticsService.GetViewProfileAsync(pageId, _fromDate.Value, _toDate.Value);
                //var result = ids.Split(",").ToDictionary(p => p, p =>;

                return Ok(ActionResponse<object>.CreateSuccessResponse(returnValue));
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
                return Ok(ActionResponse<string>.CreateErrorResponse(ex.Message));
            }
        }

        /// <summary>
        /// Thống kê lượt người xem profile theo độ tuổi
        /// </summary>
        /// <param name="pageId"></param>
        /// <param name="fromDate"></param>
        /// <param name="toDate"></param>
        /// <returns></returns>
        /// <response code="401">Authorization has been denied for this request</response>
        /// <response code="404">Not Found</response>
        /// <response code="405">Invalid input</response>
        // POST api/post/save => getway index 67
        [HttpGet, Route("page/viewprofile/age")]
        public async Task<IActionResult> GetViewProfileByAgeAsync([FromQuery]long pageId, [FromQuery]string fromDate, [FromQuery]string toDate)
        {
            try
            {
                //if (long.TryParse(User.FindFirst(ClaimTypes.NameIdentifier)?.Value, out long accountId))
                //{
                //    if (search.AccountId == null || search.AccountId == 0) search.AccountId = accountId;
                //}

                var dateFormat = AppSettings.Current.ChannelConfiguration.DateFormat;

                var _fromDate = Utility.ConvertToDateTime(fromDate, dateFormat);
                var _toDate = Utility.ConvertToDateTime(toDate, dateFormat);
                _fromDate = _fromDate != null ? _fromDate.Value.AddHours(0).AddMinutes(0).AddSeconds(0) : DateTime.Today;
                _toDate = _toDate != null ? _toDate.Value.AddHours(23).AddMinutes(59).AddSeconds(59) : DateTime.Now;

                if (_fromDate > _toDate)
                {
                    return Ok(ActionResponse<string>.CreateErrorResponse("input invalid."));
                }
                //var duration = Enumerable.Range(0, 1 + _toDate.Value.Subtract(_fromDate.Value).Days).Select(offset => _fromDate.Value.AddDays(offset)).ToArray();
                var returnValue = await StatisticsService.GetViewProfileByAgeAsync(pageId, _fromDate.Value, _toDate.Value);
                //var result = ids.Split(",").ToDictionary(p => p, p =>;

                return Ok(ActionResponse<object>.CreateSuccessResponse(returnValue));
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
                return Ok(ActionResponse<string>.CreateErrorResponse(ex.Message));
            }
        }

        /// <summary>
        /// Thống kê lượt người xem profile theo giới tính
        /// </summary>
        /// <param name="pageId"></param>
        /// <param name="fromDate"></param>
        /// <param name="toDate"></param>
        /// <returns></returns>
        /// <response code="401">Authorization has been denied for this request</response>
        /// <response code="404">Not Found</response>
        /// <response code="405">Invalid input</response>
        // POST api/post/save => getway index 67
        [HttpGet, Route("page/viewprofile/gender")]
        public async Task<IActionResult> GetViewProfileByGenderAsync([FromQuery]long pageId, [FromQuery]string fromDate, [FromQuery]string toDate)
        {
            try
            {
                //if (long.TryParse(User.FindFirst(ClaimTypes.NameIdentifier)?.Value, out long accountId))
                //{
                //    if (search.AccountId == null || search.AccountId == 0) search.AccountId = accountId;
                //}

                var dateFormat = AppSettings.Current.ChannelConfiguration.DateFormat;

                var _fromDate = Utility.ConvertToDateTime(fromDate, dateFormat);
                var _toDate = Utility.ConvertToDateTime(toDate, dateFormat);
                _fromDate = _fromDate != null ? _fromDate.Value.AddHours(0).AddMinutes(0).AddSeconds(0) : DateTime.Today;
                _toDate = _toDate != null ? _toDate.Value.AddHours(23).AddMinutes(59).AddSeconds(59) : DateTime.Now;

                if (_fromDate > _toDate)
                {
                    return Ok(ActionResponse<string>.CreateErrorResponse("input invalid."));
                }
                //var duration = Enumerable.Range(0, 1 + _toDate.Value.Subtract(_fromDate.Value).Days).Select(offset => _fromDate.Value.AddDays(offset)).ToArray();
                var returnValue = await StatisticsService.GetViewProfileByGenderAsync(pageId, _fromDate.Value, _toDate.Value);
                //var result = ids.Split(",").ToDictionary(p => p, p =>;

                return Ok(ActionResponse<object>.CreateSuccessResponse(returnValue));
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
                return Ok(ActionResponse<string>.CreateErrorResponse(ex.Message));
            }
        }

        /// <summary>
        /// Thống kê lượt người xem profile theo thiết bị
        /// </summary>
        /// <param name="pageId"></param>
        /// <param name="fromDate"></param>
        /// <param name="toDate"></param>
        /// <returns></returns>
        /// <response code="401">Authorization has been denied for this request</response>
        /// <response code="404">Not Found</response>
        /// <response code="405">Invalid input</response>
        // POST api/post/save => getway index 67
        [HttpGet, Route("page/viewprofile/device")]
        public async Task<IActionResult> GetViewProfileByDeviceAsync([FromQuery]long pageId, [FromQuery]string fromDate, [FromQuery]string toDate)
        {
            try
            {
                //if (long.TryParse(User.FindFirst(ClaimTypes.NameIdentifier)?.Value, out long accountId))
                //{
                //    if (search.AccountId == null || search.AccountId == 0) search.AccountId = accountId;
                //}

                var dateFormat = AppSettings.Current.ChannelConfiguration.DateFormat;

                var _fromDate = Utility.ConvertToDateTime(fromDate, dateFormat);
                var _toDate = Utility.ConvertToDateTime(toDate, dateFormat);
                _fromDate = _fromDate != null ? _fromDate.Value.AddHours(0).AddMinutes(0).AddSeconds(0) : DateTime.Today;
                _toDate = _toDate != null ? _toDate.Value.AddHours(23).AddMinutes(59).AddSeconds(59) : DateTime.Now;

                if (_fromDate > _toDate)
                {
                    return Ok(ActionResponse<string>.CreateErrorResponse("input invalid."));
                }
                //var duration = Enumerable.Range(0, 1 + _toDate.Value.Subtract(_fromDate.Value).Days).Select(offset => _fromDate.Value.AddDays(offset)).ToArray();
                var returnValue = await StatisticsService.GetViewProfileByDeviceAsync(pageId, _fromDate.Value, _toDate.Value);
                //var result = ids.Split(",").ToDictionary(p => p, p =>;

                return Ok(ActionResponse<object>.CreateSuccessResponse(returnValue));
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
                return Ok(ActionResponse<string>.CreateErrorResponse(ex.Message));
            }
        }

        /// <summary>
        /// Thống kê lượt người xem profile theo vị trí địa lý
        /// </summary>
        /// <param name="pageId"></param>
        /// <param name="fromDate"></param>
        /// <param name="toDate"></param>
        /// <returns></returns>
        /// <response code="401">Authorization has been denied for this request</response>
        /// <response code="404">Not Found</response>
        /// <response code="405">Invalid input</response>
        // POST api/post/save => getway index 67
        [HttpGet, Route("page/viewprofile/location")]
        public async Task<IActionResult> GetViewProfileByLocationAsync([FromQuery]long pageId, [FromQuery]string fromDate, [FromQuery]string toDate)
        {
            try
            {
                //if (long.TryParse(User.FindFirst(ClaimTypes.NameIdentifier)?.Value, out long accountId))
                //{
                //    if (search.AccountId == null || search.AccountId == 0) search.AccountId = accountId;
                //}

                var dateFormat = AppSettings.Current.ChannelConfiguration.DateFormat;

                var _fromDate = Utility.ConvertToDateTime(fromDate, dateFormat);
                var _toDate = Utility.ConvertToDateTime(toDate, dateFormat);
                _fromDate = _fromDate != null ? _fromDate.Value.AddHours(0).AddMinutes(0).AddSeconds(0) : DateTime.Today;
                _toDate = _toDate != null ? _toDate.Value.AddHours(23).AddMinutes(59).AddSeconds(59) : DateTime.Now;

                if (_fromDate > _toDate)
                {
                    return Ok(ActionResponse<string>.CreateErrorResponse("input invalid."));
                }
                //var duration = Enumerable.Range(0, 1 + _toDate.Value.Subtract(_fromDate.Value).Days).Select(offset => _fromDate.Value.AddDays(offset)).ToArray();
                var returnValue = await StatisticsService.GetViewProfileByLocationAsync(pageId, _fromDate.Value, _toDate.Value);
                //var result = ids.Split(",").ToDictionary(p => p, p =>;

                return Ok(ActionResponse<object>.CreateSuccessResponse(returnValue));
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
                return Ok(ActionResponse<string>.CreateErrorResponse(ex.Message));
            }
        }

        /// <summary>
        /// Thống kê lượng follow page
        /// </summary>
        /// <param name="pageIds"></param>
        /// <param name="fromDate"></param>
        /// <param name="toDate"></param>
        /// <returns></returns>
        /// <response code="401">Authorization has been denied for this request</response>
        /// <response code="404">Not Found</response>
        /// <response code="405">Invalid input</response>
        // POST api/post/save => getway index 67
        [HttpGet, Route("page/follow")]
        public async Task<IActionResult> GetFollowPageAsync([FromQuery]string pageIds, [FromQuery]string fromDate, [FromQuery]string toDate)
        {
            try
            {
                //if (long.TryParse(User.FindFirst(ClaimTypes.NameIdentifier)?.Value, out long accountId))
                //{
                //    if (search.AccountId == null || search.AccountId == 0) search.AccountId = accountId;
                //}

                var dateFormat = AppSettings.Current.ChannelConfiguration.DateFormat;

                var _fromDate = Utility.ConvertToDateTime(fromDate, dateFormat);
                var _toDate = Utility.ConvertToDateTime(toDate, dateFormat);
                _fromDate = _fromDate != null ? _fromDate.Value.AddHours(0).AddMinutes(0).AddSeconds(0) : DateTime.Today;
                _toDate = _toDate != null ? _toDate.Value.AddHours(23).AddMinutes(59).AddSeconds(59) : DateTime.Now;

                if (_fromDate > _toDate)
                {
                    return Ok(ActionResponse<string>.CreateErrorResponse("input invalid."));
                }
                //var duration = Enumerable.Range(0, 1 + _toDate.Value.Subtract(_fromDate.Value).Days).Select(offset => _fromDate.Value.AddDays(offset)).ToArray();
                var returnValue = await StatisticsService.GetFollowPageAsync(pageIds, _fromDate.Value, _toDate.Value);
                //var result = ids.Split(",").ToDictionary(p => p, p =>;

                return Ok(ActionResponse<object>.CreateSuccessResponse(returnValue));
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
                return Ok(ActionResponse<string>.CreateErrorResponse(ex.Message));
            }
        }
        #endregion

        #region http://cmsanalytics.admicro.vn/analytics/vda/api-doc/ims/index.html#api-APP_VIDEO_IMS-l_y_ch__s__video_theo_ng_y
        /// <summary>
        /// Get view video theo ngày
        /// </summary>
        /// <param name="ids"></param>
        /// <param name="fromDate"></param>
        /// <param name="toDate"></param>
        /// <returns></returns>
        /// <response code="401">Authorization has been denied for this request</response>
        /// <response code="404">Not Found</response>
        /// <response code="405">Invalid input</response>
        // POST api/post/save => getway index 67
        [HttpGet, Route("video/get_index_video_by_date")]
        public async Task<IActionResult> GetIndexVideoByDateAsync([FromQuery]string ids, [FromQuery]string fromDate, [FromQuery]string toDate)
        {
            try
            {
                //if (long.TryParse(User.FindFirst(ClaimTypes.NameIdentifier)?.Value, out long accountId))
                //{
                //    if (search.AccountId == null || search.AccountId == 0) search.AccountId = accountId;
                //}

                var dateFormat = AppSettings.Current.ChannelConfiguration.DateFormat;

                var _fromDate = Utility.ConvertToDateTime(fromDate, dateFormat);
                var _toDate = Utility.ConvertToDateTime(toDate, dateFormat);
                _fromDate = _fromDate != null ? _fromDate.Value.AddHours(0).AddMinutes(0).AddSeconds(0) : DateTime.Today;
                _toDate = _toDate != null ? _toDate.Value.AddHours(23).AddMinutes(59).AddSeconds(59) : DateTime.Now;

                if (_fromDate > _toDate || string.IsNullOrEmpty(ids))
                {
                    return Ok(ActionResponse<string>.CreateErrorResponse("input invalid."));
                }
                //var duration = Enumerable.Range(0, 1 + _toDate.Value.Subtract(_fromDate.Value).Days) .Select(offset => _fromDate.Value.AddDays(offset)).ToArray();

                var returnValue = await StatisticsService.GetIndexVideoByDateAsync(ids, _fromDate.Value, _toDate.Value);
                //var result = ids.Split(",").ToDictionary(p => p, p => duration.ToDictionary(d => d.ToString(dateFormat), d => returnValue?.Where(d2 => d2.Key.Equals(p)).First()?.Dates?.Metrics));

                return Ok(ActionResponse<ViewByDate[]>.CreateSuccessResponse(returnValue));
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
                return Ok(ActionResponse<string>.CreateErrorResponse(ex.Message));
            }
        }

        /// <summary>
        /// Tổng hợp các chỉ số video
        /// </summary>
        /// <param name="ids"></param>
        /// <param name="fromDate"></param>
        /// <param name="toDate"></param>
        /// <returns></returns>
        /// <response code="401">Authorization has been denied for this request</response>
        /// <response code="404">Not Found</response>
        /// <response code="405">Invalid input</response>
        // POST api/post/save => getway index 67
        [HttpGet, Route("video/get_index_video")]
        public async Task<IActionResult> GetIndexVideoAsync([FromQuery]string ids, [FromQuery]string fromDate, [FromQuery]string toDate)
        {
            try
            {
                //if (long.TryParse(User.FindFirst(ClaimTypes.NameIdentifier)?.Value, out long accountId))
                //{
                //    if (search.AccountId == null || search.AccountId == 0) search.AccountId = accountId;
                //}

                var dateFormat = AppSettings.Current.ChannelConfiguration.DateFormat;

                var _fromDate = Utility.ConvertToDateTime(fromDate, dateFormat);
                var _toDate = Utility.ConvertToDateTime(toDate, dateFormat);
                _fromDate = _fromDate != null ? _fromDate.Value.AddHours(0).AddMinutes(0).AddSeconds(0) : DateTime.Today;
                _toDate = _toDate != null ? _toDate.Value.AddHours(23).AddMinutes(59).AddSeconds(59) : DateTime.Now;

                if (_fromDate > _toDate || string.IsNullOrEmpty(ids))
                {
                    return Ok(ActionResponse<string>.CreateErrorResponse("input invalid."));
                }
                //var duration = Enumerable.Range(0, 1 + _toDate.Value.Subtract(_fromDate.Value).Days) .Select(offset => _fromDate.Value.AddDays(offset)).ToArray();

                var returnValue = await StatisticsService.GetIndexVideoAsync(ids, _fromDate.Value, _toDate.Value);
                //var result = ids.Split(",").ToDictionary(p => p, p => duration.ToDictionary(d => d.ToString(dateFormat), d => returnValue?.Where(d2 => d2.Key.Equals(p)).First()?.Dates?.Metrics));

                return Ok(ActionResponse<object>.CreateSuccessResponse(returnValue));
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
                return Ok(ActionResponse<string>.CreateErrorResponse(ex.Message));
            }
        }

        /// <summary>
        /// Tổng hợp các chỉ số video
        /// </summary>
        /// <param name="fromDate"></param>
        /// <param name="toDate"></param>
        /// <param name="limit"></param>
        /// <param name="order"></param>
        /// <param name="page"></param>
        /// <param name="pageId"></param>
        /// <returns></returns>
        /// <response code="401">Authorization has been denied for this request</response>
        /// <response code="404">Not Found</response>
        /// <response code="405">Invalid input</response>
        // POST api/post/save => getway index 67
        //http://192.168.23.94:8000/analytics/app_ims_service/top_video
        [HttpGet, Route("video/top")]
        public async Task<IActionResult> TopVideoAsync([FromQuery]long pageId, [FromQuery]string fromDate, [FromQuery]string toDate, [FromQuery]string order, [FromQuery]int? limit, [FromQuery]int? page)
        {
            try
            {
                //if (long.TryParse(User.FindFirst(ClaimTypes.NameIdentifier)?.Value, out long accountId))
                //{
                //    if (search.AccountId == null || search.AccountId == 0) search.AccountId = accountId;
                //}

                var dateFormat = AppSettings.Current.ChannelConfiguration.DateFormat;

                var _fromDate = Utility.ConvertToDateTime(fromDate, dateFormat);
                var _toDate = Utility.ConvertToDateTime(toDate, dateFormat);
                _toDate = _toDate != null ? _toDate.Value.AddHours(23).AddMinutes(59).AddSeconds(59) : DateTime.Today.AddHours(23).AddMinutes(59).AddSeconds(59);
                _fromDate = _fromDate != null ? _fromDate.Value.AddHours(0).AddMinutes(0).AddSeconds(0) : _toDate.Value.AddDays(-7);
                

                var _limit = limit.HasValue ? limit : 10;
                var _page = page.HasValue ? page : 1;
                var _mode = AppSettings.Current.ChannelConfiguration.ViewVideoSettings.Mode;
                if (_fromDate > _toDate || pageId == 0)
                {
                    return Ok(ActionResponse<string>.CreateErrorResponse("input invalid."));
                }
                //var duration = Enumerable.Range(0, 1 + _toDate.Value.Subtract(_fromDate.Value).Days) .Select(offset => _fromDate.Value.AddDays(offset)).ToArray();

                var returnValue = await StatisticsService.TopVideoAsync(pageId, _fromDate.Value, _toDate.Value, _mode, order, _limit.Value, _page.Value);
                //var result = ids.Split(",").ToDictionary(p => p, p => duration.ToDictionary(d => d.ToString(dateFormat), d => returnValue?.Where(d2 => d2.Key.Equals(p)).First()?.Dates?.Metrics));

                return Ok(ActionResponse<object>.CreateSuccessResponse(returnValue));
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
                return Ok(ActionResponse<string>.CreateErrorResponse(ex.Message));
            }
        }

        #endregion

        #region CHINHNB DashBoard
        /// <summary>
        /// Top bài post của page
        /// </summary>
        /// <param name="keyword"></param>
        /// <param name="fromDate"></param>
        /// <param name="toDate"></param>
        /// <param name="limit"></param>
        /// <param name="order"></param>
        /// <param name="page"></param>
        /// <param name="pageId"></param>
        /// <returns></returns>
        /// <response code="401">Authorization has been denied for this request</response>
        /// <response code="404">Not Found</response>
        /// <response code="405">Invalid input</response>        
        // http://192.168.23.94:8000/analytics/app_ims_service/top_post
        [HttpGet, Route("post/top")]
        public async Task<IActionResult> TopPostAsync([FromQuery]long pageId, [FromQuery]DateTime? fromDate, [FromQuery]DateTime? toDate, [FromQuery]OrderEnum order, [FromQuery]int? limit, [FromQuery]int? page)
        {
            try
            {
                var sessionId = HttpContext.GetSessionId();
                var _fromDate = fromDate != null ? fromDate.Value.AddHours(0).AddMinutes(0).AddSeconds(0) : fromDate.Value.AddDays(-7);
                var _toDate = toDate != null ? toDate.Value.AddHours(23).AddMinutes(59).AddSeconds(59) : DateTime.Today.AddHours(23).AddMinutes(59).AddSeconds(59);
                
                var _limit = limit.HasValue ? limit : 20;
                var _page = page.HasValue ? page : 1;
                var _mode = AppSettings.Current.ChannelConfiguration.ViewVideoSettings.Mode;


                if (_fromDate > _toDate || pageId == 0)
                {
                    return Ok(ActionResponse<string>.CreateErrorResponse("input invalid."));
                }

                var returnValue = await StatisticsService.TopPostAsync(pageId, _fromDate, _toDate, _mode, order.ToString(), _limit.Value, _page.Value, sessionId);

                return Ok(ActionResponse<object>.CreateSuccessResponse(returnValue));
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
                return Ok(ActionResponse<string>.CreateErrorResponse(ex.Message));
            }
        }

        // http://192.168.23.94:8000/analytics/app_ims_service/top_account
        [HttpGet, Route("account/top")]
        public async Task<IActionResult> TopAccountAsync([FromQuery]string keyword, [FromQuery]long pageId, [FromQuery]string fromDate, [FromQuery]string toDate, [FromQuery]string order, [FromQuery]int? limit, [FromQuery]int? page)
        {
            try
            {
                var dateFormat = AppSettings.Current.ChannelConfiguration.DateFormat;

                var _fromDate = Utility.ConvertToDateTime(fromDate, dateFormat);
                var _toDate = Utility.ConvertToDateTime(toDate, dateFormat);
                _toDate = _toDate != null ? _toDate.Value.AddHours(23).AddMinutes(59).AddSeconds(59) : DateTime.Today.AddHours(23).AddMinutes(59).AddSeconds(59);
                _fromDate = _fromDate != null ? _fromDate.Value.AddHours(0).AddMinutes(0).AddSeconds(0) : _toDate.Value.AddDays(-7);


                var _limit = limit.HasValue ? limit : 10;
                var _page = page.HasValue ? page : 1;
                var _mode = AppSettings.Current.ChannelConfiguration.ViewVideoSettings.Mode;
                if (_fromDate > _toDate || pageId == 0)
                {
                    return Ok(ActionResponse<string>.CreateErrorResponse("input invalid."));
                }

                //var returnValue = await StatisticsService.TopAccountAsync(pageId, _fromDate.Value, _toDate.Value, _mode, order, _limit.Value, _page.Value, keyword);

                return Ok(ActionResponse<object>.CreateSuccessResponse(new object[]{
                    new {
                        Id = 1234567,
                        Fullname = "account 1",
                        Quantity = 100,
                        Like = 100,
                        Token = 99,
                        Comment = 98,
                        Share = 97,
                        Reach = 96,
                        Send = 95
                    },
                    new {
                        Id = 1234568,
                        Fullname = "account 2",
                        Quantity = 100,
                        Like = 100,
                        Token = 99,
                        Comment = 98,
                        Share = 97,
                        Reach = 96,
                        Send = 95
                    },
                    new {
                        Id = 1234569,
                        Fullname = "account 3",
                        Quantity = 100,
                        Like = 100,
                        Token = 99,
                        Comment = 98,
                        Share = 97,
                        Reach = 96,
                        Send = 95
                    },
                    new {
                        Id = 1234560,
                        Fullname = "account 4",
                        Quantity = 100,
                        Like = 100,
                        Token = 99,
                        Comment = 98,
                        Share = 97,
                        Reach = 96,
                        Send = 95
                    },
                    new {
                        Id = 1234561,
                        Fullname = "account 5",
                        Quantity = 100,
                        Like = 100,
                        Token = 99,
                        Comment = 98,
                        Share = 97,
                        Reach = 96,
                        Send = 95
                    }}));
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
                return Ok(ActionResponse<string>.CreateErrorResponse(ex.Message));
            }
        }

        #endregion
    }
}