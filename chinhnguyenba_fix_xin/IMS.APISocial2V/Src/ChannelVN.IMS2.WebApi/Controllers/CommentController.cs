﻿using System;
using System.Linq;
using System.Net;
using System.Security.Claims;
using System.Threading.Tasks;
using ChannelVN.IMS2.Core.Entities;
using ChannelVN.IMS2.Core.Entities.Comment;
using ChannelVN.IMS2.Core.Entities.Security;
using ChannelVN.IMS2.Core.Repositories.Constants;
using ChannelVN.IMS2.Core.Repositories.Functions;
using ChannelVN.IMS2.Core.Services;
using ChannelVN.IMS2.Foundation.Common;
using ChannelVN.IMS2.Foundation.Logging;
using Microsoft.AspNetCore.Mvc;

namespace ChannelVN.IMS2.WebApi.Controllers
{
    [Route("api/[controller]")]
    public class CommentController : BaseController
    {
        /// <summary>
        /// Lấy danh sách comment của 1 media
        /// </summary>
        /// <param name="requestData"></param>
        /// <returns></returns>
        [HttpGet, Route("get_by_post_id")]
        public async Task<IActionResult> GetByPostIdAsync([FromQuery]RequestDataModel requestData)
        {
            try
            {
                if(requestData == null || !(requestData.PageId > 0) || string.IsNullOrEmpty(requestData.MediaId))
                {
                    return Ok(new ResponseDeleteDataModel<DataModel>
                    {
                        Status = 400,
                        Message = "Input invalid.",
                        Code = (int)System.Net.HttpStatusCode.OK
                    });
                }
                if (long.TryParse(User.FindFirst(ClaimTypes.NameIdentifier)?.Value, out long accountId))
                {
                    var accountMember = await SecurityService.GetAccountMemberByIdAsync(requestData.PageId + IndexKeys.SeparateChar + accountId);

                    if(accountMember!=null && accountMember.Permissions!=null && accountMember.Permissions.Contains(MemberPermission.ManagerComment))
                    {
                        requestData.Limit = requestData.Limit.HasValue ? requestData.Limit : 10;
                        requestData.ChildLimit = requestData.ChildLimit.HasValue ? requestData.ChildLimit : 3;
                        requestData.Type = requestData.Type.HasValue && requestData.Type != 0 ? 1 : 0;

                        var result = await CommentService.GetByPostIdAsync(requestData);
                        return Ok(result);
                    }
                    else
                    {
                        return Ok(new ResponseDeleteDataModel<DataModel>
                        {
                            Status = 403,
                            Message = "permission invalid.",
                            Code = (int)System.Net.HttpStatusCode.OK
                        });
                    }
                }
                else
                {
                    return Ok(new ResponseDeleteDataModel<DataModel>
                    {
                        Status = 401,
                        Message = "account invalid.",
                        Code = (int)System.Net.HttpStatusCode.OK
                    });
                }   
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
                return Ok(new ResponseDeleteDataModel<DataModel>
                {
                    Status = 0,
                    Message = ex.Message,
                    Code = (int)System.Net.HttpStatusCode.OK
                });
            }
        }

        ///// <summary>
        ///// Lấy danh sách comment của 1 media
        ///// </summary>
        ///// <param name="requestData"></param>
        ///// <returns></returns>
        //[HttpGet, Route("detail")]
        //public async Task<IActionResult> GetDetailAsync([FromQuery]RequestDataModel requestData)
        //{
        //    try
        //    {
        //        if (requestData == null || string.IsNullOrEmpty(requestData.CommentId))
        //        {
        //            return Ok(new ResponseDeleteDataModel<DataModel>
        //            {
        //                Status = 0,
        //                Message = "Input invalid.",
        //                Code = (int)System.Net.HttpStatusCode.OK
        //            });
        //        }
        //        if (long.TryParse(User.FindFirst(ClaimTypes.NameIdentifier)?.Value, out long accountId))
        //        {
        //            var pageId = await SecurityService.GetAccountInNewsAsync(requestData.PostId.Value);
        //            var accountMember = await SecurityService.GetAccountMemberByIdAsync(pageId + IndexKeys.SeparateChar + accountId);

        //            if (accountMember != null && accountMember.Permissions != null && accountMember.Permissions.Contains(MemberPermission.ManagerComment))
        //            {
        //                requestData.ChildLimit = requestData.ChildLimit.HasValue ? requestData.ChildLimit : 3;
        //                requestData.Type = requestData.Type.HasValue && requestData.Type != 0 ? 1 : 0;

        //                var result = await CommentService.GetDetailAsync(requestData);
        //                return Ok(result);
        //            }
        //            else
        //            {
        //                return Ok(new ResponseDeleteDataModel<DataModel>
        //                {
        //                    Status = 0,
        //                    Message = "Permission invalid.",
        //                    Code = (int)System.Net.HttpStatusCode.OK
        //                });
        //            }
        //        }
        //        else
        //        {
        //            return Ok(new ResponseDeleteDataModel<DataModel>
        //            {
        //                Status = 0,
        //                Message = "account invalid.",
        //                Code = (int)System.Net.HttpStatusCode.OK
        //            });
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        Logger.Error(ex, ex.Message);
        //        return Ok(new ResponseDeleteDataModel<DataModel>
        //        {
        //            Status = 0,
        //            Message = ex.Message,
        //            Code = (int)System.Net.HttpStatusCode.OK
        //        });
        //    }
        //}

        /// <summary>
        /// Update comment
        /// </summary>
        /// <param name="id"></param>
        /// <param name="jsonContent"></param>
        /// <param name="officerId"></param>
        /// <returns></returns>
        [HttpPut, Route("update")]
        public async Task<IActionResult> UpdateAsync([FromForm]string id, [FromForm]string jsonContent, [FromForm] long officerId)
        {
            var result = default(ResponsePutDataModel<ResultPutDataModel<CommentDataModel>>);
            try
            {
                if (string.IsNullOrEmpty(id) || !(officerId > 0))
                {
                    return Ok(new ResponsePutDataModel<ResultPutDataModel<CommentDataModel>>
                    {
                        Status = 400,
                        Code = (int)HttpStatusCode.OK,
                        Message = "Input invalid."
                    });
                }
                if (long.TryParse(User.FindFirst(ClaimTypes.NameIdentifier)?.Value, out long accountId))
                {
                    var accountMember = await SecurityService.GetAccountMemberByIdAsync(officerId + IndexKeys.SeparateChar + accountId);

                    if (accountMember != null && accountMember.Permissions != null && accountMember.Permissions.Contains(MemberPermission.ManagerComment))
                    {
                        var clientIP = string.IsNullOrEmpty(HttpContext?.Connection?.RemoteIpAddress?.ToString()) ? "192.168.1.1" : HttpContext?.Connection?.RemoteIpAddress?.ToString();

                        result = await CommentService.UpdateAsync(id, jsonContent);

                        if (result != null && result.Code == (int)HttpStatusCode.OK && result.Result != null && result.Result.FirstOrDefault()?.Code == (int)HttpStatusCode.OK)
                        {
                            await Function.AddToQueue(ActionName.InsertLog, TopicName.LOGACTION, new LogActionEntity
                            {
                                ObjectId = id,
                                Account = accountId.ToString(),
                                CreatedDate = DateTime.SpecifyKind(DateTime.Now, DateTimeKind.Unspecified),
                                Ip = clientIP,
                                Message = "Success.",
                                Title = "",
                                SourceId = officerId.ToString(),
                                ActionTypeDetail = (int)ActionType.UpdateComment,
                                ActionText = ActionType.UpdateComment.ToString(),
                                Type = (int)LogContentType.Comment
                            });
                        }
                        return Ok(result);
                    }
                    else
                    {
                        return Ok(new ResponseDeleteDataModel<DataModel>
                        {
                            Status = 403,
                            Message = "permission invalid.",
                            Code = (int)System.Net.HttpStatusCode.OK
                        });
                    }
                }
                else
                {
                    return Ok(new ResponsePutDataModel<ResultPutDataModel<CommentDataModel>>
                    {
                        Status = 401,
                        Code = (int)HttpStatusCode.OK,
                        Message = "Bạn không có quyền chỉnh sửa."
                    });
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
                return Ok(new ResponsePutDataModel<ResultPutDataModel<CommentDataModel>>
                {
                    Status = 0,
                    Code = (int)HttpStatusCode.OK,
                    Message = ex.Message
                });
            }
        }

        /// <summary>
        /// Update comment
        /// </summary>
        /// <param name="mediaId"></param>
        /// <param name="jsonContent"></param>
        /// <param name="officerId"></param>
        /// <param name="parentCommentID"></param>
        /// <param name="role"></param>
        /// <returns></returns>
        [HttpPost, Route("create")]
        public async Task<IActionResult> CreateAsync([FromForm]long mediaId,[FromForm]string parentCommentID, [FromForm]string jsonContent, [FromForm]int? role, [FromForm] long officerId)
        {
            var result = default(ResponseGetDataModel<ResponseData<CommentDataModel>[]>);
            try
            {
                if(mediaId == 0 || string.IsNullOrEmpty(parentCommentID) || string.IsNullOrEmpty(jsonContent) || !(officerId > 0))
                {
                    return Ok(new ResponseGetDataModel<ResponseData<CommentDataModel>[]>
                    {
                        Status = 400,
                        Code = (int)HttpStatusCode.OK,
                        Message = "Input invalid."
                    });
                }

                if (long.TryParse(User.FindFirst(ClaimTypes.NameIdentifier)?.Value, out long accountId))
                {
                    var accountMember = await SecurityService.GetAccountMemberByIdAsync(officerId + IndexKeys.SeparateChar + accountId);

                    if (accountMember != null && accountMember.Permissions != null && accountMember.Permissions.Contains(MemberPermission.ManagerComment))
                    {
                        var clientIP = string.IsNullOrEmpty(HttpContext?.Connection?.RemoteIpAddress?.ToString()) ? "192.168.1.1" : HttpContext?.Connection?.RemoteIpAddress?.ToString();

                        if (role == 1)
                        {
                            result = await CommentService.CreateAsync(mediaId, parentCommentID, jsonContent, accountId);
                        }
                        else
                        {
                            result = await CommentService.CreateAsync(mediaId, parentCommentID, jsonContent, officerId);
                        }

                        if (result != null && result.Code == (int)HttpStatusCode.OK && result.Result != null)
                        {
                            await Function.AddToQueue(ActionName.InsertLog, TopicName.LOGACTION, new LogActionEntity
                            {
                                ObjectId = mediaId.ToString(),
                                Account = accountId.ToString(),
                                CreatedDate = DateTime.SpecifyKind(DateTime.Now, DateTimeKind.Unspecified),
                                Ip = clientIP,
                                Message = "Success.",
                                Title = "",
                                SourceId = officerId.ToString(),
                                ActionTypeDetail = (int)ActionType.CreateComment,
                                ActionText = ActionType.CreateComment.ToString(),
                                Type = (int)LogContentType.Comment
                            });
                        }
                        return Ok(result);
                    }
                    else
                    {
                        return Ok(new ResponseDeleteDataModel<DataModel>
                        {
                            Status = 403,
                            Message = "permission invalid.",
                            Code = (int)System.Net.HttpStatusCode.OK
                        });
                    }
                }
                else
                {
                    return Ok(new ResponseGetDataModel<ResponseData<CommentDataModel>[]>
                    {
                        Status = 401,
                        Code = (int)HttpStatusCode.OK,
                        Message = "Account invalid."
                    });
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
                return Ok(new ResponsePutDataModel<ResultPutDataModel<ResponseData<CommentDataModel>[]>>
                {
                    Status = 0,
                    Code = (int)HttpStatusCode.OK,
                    Message = ex.Message
                });
            }
        }


        /// <summary>
        /// Lấy danh sách comment của 1 media
        /// </summary>
        /// <param name="ids"></param>
        /// <param name="officerId"></param>
        /// <returns></returns>
        [HttpDelete, Route("delete")]
        public async Task<IActionResult> DeleteAsync([FromQuery]string ids, [FromQuery]long officerId)
        {
            var result = default(ResponseDeleteDataModel<ResultDeleteDataModel<DataModel>>);
            try
            {
                if (string.IsNullOrEmpty(ids) || officerId == 0)
                {
                    return Ok(new ResponseDeleteDataModel<ResultDeleteDataModel<DataModel>>
                    {
                        Status = 400,
                        Message = "Input invalid.",
                        Code = (int)System.Net.HttpStatusCode.OK
                    });
                }

                if (long.TryParse(User.FindFirst(ClaimTypes.NameIdentifier)?.Value, out long accountId))
                {
                    var accountMember = await SecurityService.GetAccountMemberByIdAsync(officerId + IndexKeys.SeparateChar + accountId);

                    if (accountMember != null && accountMember.Permissions != null && accountMember.Permissions.Contains(MemberPermission.ManagerComment))
                    {
                        var clientIP = string.IsNullOrEmpty(HttpContext?.Connection?.RemoteIpAddress?.ToString()) ? "192.168.1.1" : HttpContext?.Connection?.RemoteIpAddress?.ToString();

                        result = await CommentService.DeleteAsync(ids, officerId);
                        if (result != null && result.Result != null)
                        {
                            for (var i = 0; i < result.Result.Length; i++)
                            {
                                if (result.Result[i].data.Deleted == true)
                                {
                                    await Function.AddToQueue(ActionName.InsertLog, TopicName.LOGACTION, new LogActionEntity
                                    {
                                        ObjectId = result.Result[i].data.CommentID,
                                        Account = accountId.ToString(),
                                        CreatedDate = DateTime.SpecifyKind(DateTime.Now, DateTimeKind.Unspecified),
                                        Ip = clientIP,
                                        Message = "Success.",
                                        Title = "",
                                        SourceId = officerId.ToString(),
                                        ActionTypeDetail = (int)ActionType.DeleteComment,
                                        ActionText = ActionType.DeleteComment.ToString(),
                                        Type = (int)LogContentType.Comment
                                    });
                                }
                            }
                        }
                        return Ok(result);
                    }
                    else
                    {
                        return Ok(new ResponseDeleteDataModel<ResultDeleteDataModel<DataModel>>
                        {
                            Status = 403,
                            Message = "Permission invalid.",
                            Code = (int)System.Net.HttpStatusCode.OK
                        });
                    }
                }
                else
                {
                    return Ok(new ResponseDeleteDataModel<ResultDeleteDataModel<DataModel>>
                    {
                        Status = 401,
                        Message = "Account invalid.",
                        Code = (int)System.Net.HttpStatusCode.OK
                    });
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
                return Ok(new ResponseDeleteDataModel<ResultDeleteDataModel<DataModel>>
                {
                    Status = 0,
                    Message = ex.Message,
                    Code = (int)System.Net.HttpStatusCode.OK
                });
            }
        }


        /// <summary>
        /// Lấy danh sách comment của 1 media
        /// </summary>
        /// <param name="ids"></param>
        /// <param name="officerId"></param>
        /// <param name="recursive"></param>
        /// <returns></returns>
        [HttpGet, Route("restore")]
        public async Task<IActionResult> RestoreAsync([FromQuery]string ids, [FromQuery]bool recursive, [FromQuery]long officerId)
        {
            var result = default(ResponseGetDataModel<ResponseData<CommentDataModel>[]>);
            try
            {
                if (string.IsNullOrEmpty(ids) || officerId == 0)
                {
                    return Ok(new ResponseGetDataModel<ResponseData<CommentDataModel>[]>
                    {
                        Status = 400,
                        Message = "Input invalid.",
                        Code = (int)System.Net.HttpStatusCode.OK
                    });
                }

                if (long.TryParse(User.FindFirst(ClaimTypes.NameIdentifier)?.Value, out long accountId))
                {
                    var accountMember = await SecurityService.GetAccountMemberByIdAsync(officerId + IndexKeys.SeparateChar + accountId);

                    if (accountMember != null && accountMember.Permissions != null && accountMember.Permissions.Contains(MemberPermission.ManagerComment))
                    {
                        var clientIP = string.IsNullOrEmpty(HttpContext?.Connection?.RemoteIpAddress?.ToString()) ? "192.168.1.1" : HttpContext?.Connection?.RemoteIpAddress?.ToString();

                        result = await CommentService.RestoreAsync(ids, recursive, officerId);
                        if (result != null && result.Result != null)
                        {
                            for (var i = 0; i < result.Result.Length; i++)
                            {
                                if (result.Result[i]?.Code == (int)HttpStatusCode.OK)
                                {
                                    await Function.AddToQueue(ActionName.InsertLog, TopicName.LOGACTION, new LogActionEntity
                                    {
                                        ObjectId = result.Result[i]?.Data?.CommentID,
                                        Account = accountId.ToString(),
                                        CreatedDate = DateTime.SpecifyKind(DateTime.Now, DateTimeKind.Unspecified),
                                        Ip = clientIP,
                                        Message = "Success.",
                                        Title = "",
                                        SourceId = officerId.ToString(),
                                        ActionTypeDetail = (int)ActionType.RestoreComment,
                                        ActionText = ActionType.RestoreComment.ToString(),
                                        Type = (int)LogContentType.Comment
                                    });
                                }
                            }
                        }
                        return Ok(result);
                    }
                    else
                    {
                        return Ok(new ResponseGetDataModel<ResponseData<CommentDataModel>[]>
                        {
                            Status = 403,
                            Message = "Permission invalid.",
                            Code = (int)System.Net.HttpStatusCode.OK
                        });
                    }
                }
                else
                {
                    return Ok(new ResponseGetDataModel<ResponseData<CommentDataModel>[]>
                    {
                        Status = 401,
                        Message = "Account invalid.",
                        Code = (int)System.Net.HttpStatusCode.OK
                    });
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
                return Ok(new ResponseGetDataModel<ResponseData<CommentDataModel>[]>
                {
                    Status = 0,
                    Message = ex.Message,
                    Code = (int)System.Net.HttpStatusCode.OK
                });
            }
        }

        /// <summary>
        /// Lấy danh sách comment của 1 media
        /// </summary>
        /// <param name="search"></param>
        /// <returns></returns>
        [HttpGet, Route("count")]
        public async Task<IActionResult> GetCountAsync([FromQuery]RequestDataModel search)
        {
            var result = default(ResponseGetDataModel<long>);
            try
            {
                if (!(search.PageId > 0))
                {
                    return Ok(new ResponseGetDataModel<long>
                    {
                        Result = 0,
                        Status = 400,
                        Message = "Input invalid.",
                        Code = (int)System.Net.HttpStatusCode.OK
                    });
                }

                if (long.TryParse(User.FindFirst(ClaimTypes.NameIdentifier)?.Value, out long accountId))
                {
                    var accountMember = await SecurityService.GetAccountMemberByIdAsync(search.PageId + IndexKeys.SeparateChar + accountId);

                    if (accountMember != null && accountMember.Permissions != null && accountMember.Permissions.Contains(MemberPermission.ManagerComment))
                    {
                        //if (search.PostId > 0) search.PageId = null;
                        search.Limit = search.Limit.HasValue ? search.Limit : 10;
                        result = await CommentService.GetCountAsync(search);
                    }
                    else
                    {
                        result = new ResponseGetDataModel<long>
                        {
                            Result = 0,
                            Status = 403,
                            Message = "Permission invalid.",
                            Code = (int)System.Net.HttpStatusCode.OK
                        };
                    }
                        
                }
                //Logger.Debug("End:" + DateTime.Now.ToString("HH:mm:ss.fff"));
                return Ok(result);

            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);


                return Ok(new ResponseGetDataModel<long>
                {
                    Result = 0,
                    Status = 0,
                    Message = ex.Message,
                    Code = (int)System.Net.HttpStatusCode.OK
                });
            }
        }

        /// <summary>
        /// Lấy danh sách comment cần duyệt theo Page
        /// </summary>
        /// <param name="search"></param>
        /// <returns></returns>
        [HttpGet, Route("search")]
        public async Task<IActionResult> SearchAsync([FromQuery]RequestDataModel search)
        {
            //Logger.Debug("Start:" + DateTime.Now.ToString("HH:mm:ss.fff"));
            var result = default(ResponseGetDataModel<ResultGetDataModel<CommentDataModel>>);
            try
            {
                if (!(search.PageId > 0))
                {
                    return Ok(new ResponseGetDataModel<ResultGetDataModel<CommentDataModel>>
                    {
                        Status = 400,
                        Message = "Input invalid.",
                        Code = (int)System.Net.HttpStatusCode.OK
                    });
                }

                if (long.TryParse(User.FindFirst(ClaimTypes.NameIdentifier)?.Value, out long accountId))
                {
                    var accountMember = await SecurityService.GetAccountMemberByIdAsync(search.PageId + IndexKeys.SeparateChar + accountId);

                    if (accountMember != null && accountMember.Permissions != null && accountMember.Permissions.Contains(MemberPermission.ManagerComment))
                    {
                        search.Limit = search.Limit.HasValue ? search.Limit : 10;
                        result = await CommentService.SearchAsync(search);
                    }
                    else
                    {
                        result = new ResponseGetDataModel<ResultGetDataModel<CommentDataModel>>
                        {
                            Status = 403,
                            Message = "Permission invalid.",
                            Code = (int)System.Net.HttpStatusCode.OK
                        };
                    }
                }
                else
                {
                    result = new ResponseGetDataModel<ResultGetDataModel<CommentDataModel>>
                    {
                        Status = 401,
                        Message = "Account invalid.",
                        Code = (int)System.Net.HttpStatusCode.OK
                    };
                }
                //Logger.Debug("End:" + DateTime.Now.ToString("HH:mm:ss.fff"));
                return Ok(result);

            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
                return Ok(new ResponseGetDataModel<ResultGetDataModel<CommentDataModel>>
                {
                    Status = 0,
                    Message = ex.Message,
                    Code = (int)System.Net.HttpStatusCode.OK
                });
            }
        }

        /// <summary>
        /// Duyệt comment
        /// </summary>
        /// <param name="id"></param>
        /// <param name="status"></param>
        /// <param name="logID"></param>
        /// <param name="officerId"></param>
        /// <returns></returns>
        [HttpPost, Route("approve")]
        public async Task<IActionResult> ApproveAsync([FromForm]string id, [FromForm]CommentStatus? status, [FromForm]string logID, [FromForm] long officerId)
        {
            var result = default(ResponseGetDataModel<object>);
            try
            {
                var clientIP = string.IsNullOrEmpty(HttpContext?.Connection?.RemoteIpAddress?.ToString()) ? "192.168.1.1" : HttpContext?.Connection?.RemoteIpAddress?.ToString();
                long.TryParse(User.FindFirst(ClaimTypes.NameIdentifier)?.Value, out long userId);

                if (string.IsNullOrEmpty(id) || status == null || !(officerId > 0))
                {
                    return Ok(new ResponseGetDataModel<object>
                    {
                        Status = 400,
                        Message = "Input invalid.",
                        Code = (int)System.Net.HttpStatusCode.OK
                    });
                    //return Ok(ActionResponse<string>.CreateErrorResponse("Input invalid."));
                }


                // Thiếu thông tin Log Action( ActionType = ApproveComment)
                if (long.TryParse(User.FindFirst(ClaimTypes.NameIdentifier)?.Value, out long accountId))
                {
                    var accountMember = await SecurityService.GetAccountMemberByIdAsync(officerId + IndexKeys.SeparateChar + accountId);
                    if (accountMember != null && accountMember.Permissions != null && accountMember.Permissions.Contains(MemberPermission.ManagerComment))
                    {
                        result = await CommentService.ApproveAsync(id, status, logID, accountId);
                        await Function.AddToQueue(ActionName.InsertLog, TopicName.LOGACTION, new LogActionEntity
                        {
                            ObjectId = id,
                            Account = accountId.ToString(),
                            CreatedDate = DateTime.SpecifyKind(DateTime.Now, DateTimeKind.Unspecified),
                            Ip = clientIP,
                            Message = "Success.",
                            Title = "",
                            SourceId = officerId.ToString(),
                            ActionTypeDetail = (int)ActionType.ApproveComment,
                            ActionText = ActionType.ApproveComment.ToString(),
                            Type = (int)LogContentType.Comment
                        });
                    }
                    else
                    {
                        result = new ResponseGetDataModel<object>
                        {
                            Status = 403,
                            Message = "Permission invalid.",
                            Code = (int)System.Net.HttpStatusCode.OK
                        };
                    }
                }
                else
                {
                    result = new ResponseGetDataModel<object>
                    {
                        Status = 401,
                        Message = "Account invalid.",
                        Code = (int)System.Net.HttpStatusCode.OK
                    };
                }
                return Ok(result);

            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
                return Ok(new ResponseGetDataModel<object>
                {
                    Status = 0,
                    Message = ex.Message,
                    Code = (int)System.Net.HttpStatusCode.OK
                });
            }
        }

        /// <summary>
        /// Lấy danh sách member  0: normal, 1: black-list, 2: white-list
        /// </summary>
        /// <param name="search"></param>
        /// <returns></returns>
        [HttpGet, Route("block/search")]
        public async Task<IActionResult> BlockSearchAsync([FromQuery]RequestMemberModel search)
        {
            try
            {
                var result = await CommentService.BlockSearchAsync(search);
                return Ok(result);
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
                return Ok(new ResponseGetDataModel<ResponseData<UserCommentResponse[]>>
                {
                    Code = (int)HttpStatusCode.OK,
                    Status = 0,
                    Message = ex.Message
                });
            }
        }

        /// <summary>
        /// Lấy danh sách member  0: xám, 1: đen, 2: trắng
        /// </summary>
        /// <param name="search"></param>
        /// <returns></returns>
        [HttpGet, Route("classify/search")]
        public async Task<IActionResult> ClassifySearchAsync([FromQuery]RequestMemberModel search)
        {
            try
            {
                var result = await CommentService.ClassifySearchAsync(search);
                return Ok(result);

            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
                return Ok(new ResponseGetDataModel<ResponseData<UserCommentResponse[]>>
                {
                    Code = (int)HttpStatusCode.OK,
                    Status = 0,
                    Message = ex.Message
                });
            }
        }

        /// <summary>
        /// Lấy danh sách member  0 là normal user, 1 là top Fan, 2 là top Contributor
        /// </summary>
        /// <param name="search"></param>
        /// <returns></returns>
        [HttpGet, Route("top_member/search")]
        public async Task<IActionResult> TopMemberSearchAsync([FromQuery]RequestMemberModel search)
        {
            try
            {
                var result = await CommentService.TopMemberSearchAsync(search);
                return Ok(result);

            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
                return Ok(new ResponseGetDataModel<ResponseData<UserCommentResponse[]>>
                {
                    Code = (int)HttpStatusCode.OK,
                    Status = 0,
                    Message = ex.Message
                });
            }
        }

        /// <summary>
        /// Block user theo user-id
        /// Block user theo user-id
        /// </summary>
        /// <param name="search"></param>
        /// <returns></returns>
        //[HttpPost, Route("block/update")]
        //public async Task<IActionResult> BlockUpdateAsync([FromForm]RequestMemberModel search)
        //{
        //    try
        //    {
        //        if (long.TryParse(User.FindFirst(ClaimTypes.NameIdentifier)?.Value, out long accountId))
        //        {
        //            var result = await CommentService.BlockUpdateAsync(search, accountId);
        //            return Ok(result);
        //        }
        //        else
        //        {
        //            return Ok(new ResponseGetDataModel<UserCommentResponse>
        //            {
        //                Code = 401,
        //                Message = "account invalid",
        //                Status = 0
        //            });
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        Logger.Error(ex, ex.Message);
        //        return Ok(new ResponseGetDataModel<UserCommentResponse>
        //        {
        //            Code = (int)System.Net.HttpStatusCode.OK,
        //            Message = ex.Message,
        //            Status = 0
        //        });
        //    }
        //}

        /// <summary>
        /// Update classify member  0: xám, 1: đen, 2: trắng
        /// </summary>
        /// <param name="search"></param>
        /// <returns></returns>
        //[HttpPost, Route("classify/update")]
        //public async Task<IActionResult> ClassifyUpdateAsync([FromForm]RequestMemberModel search)
        //{
        //    try
        //    {
        //        if (long.TryParse(User.FindFirst(ClaimTypes.NameIdentifier)?.Value, out long accountId))
        //        {
        //            var result = await CommentService.ClassifyUpdateAsync(search, accountId);
        //            return Ok(result);
        //        }
        //        else
        //        {
        //            return Ok(new ResponseGetDataModel<UserCommentResponse>
        //            {
        //                Code = 401,
        //                Status = 0,
        //                Message = "account invalid"
        //            });
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        Logger.Error(ex, ex.Message);
        //        return Ok(new ResponseGetDataModel<UserCommentResponse>
        //        {
        //            Code = (int)System.Net.HttpStatusCode.OK,
        //            Status = 0,
        //            Message = ex.Message,
        //        });
        //    }
        //}

        /// <summary>
        /// Update top fan member  0 là normal user, 1 là top Fan, 2 là top Contributor
        /// </summary>
        /// <param name="search"></param>
        /// <returns></returns>
        //[HttpPost, Route("top_member/update")]
        //public async Task<IActionResult> TopMemberUpdateAsync([FromForm]RequestMemberModel search)
        //{
        //    try
        //    {
        //        if (long.TryParse(User.FindFirst(ClaimTypes.NameIdentifier)?.Value, out long accountId))
        //        {
        //            var result = await CommentService.TopMemberUpdateAsync(search, accountId);
        //            return Ok(result);
        //        }
        //        else
        //        {
        //            return Ok(new ResponseGetDataModel<UserCommentResponse>
        //            {
        //                Code = 401,
        //                Status = 0,
        //                Message = "account invalid"
        //            });
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        Logger.Error(ex, ex.Message);
        //        return Ok(new ResponseGetDataModel<UserCommentResponse>
        //        {
        //            Code = (int)System.Net.HttpStatusCode.OK,
        //            Status = 0,
        //            Message = ex.Message,
        //        });
        //    }
        //}

        /// <summary>
        /// Update member theo group
        /// </summary>
        /// <param name="body"></param>
        /// <returns></returns>
        [HttpPost, Route("member/update")]
        public async Task<IActionResult> UpdateMemberCommentAsync([FromForm]RequestMemberModel body)
        {
            try
            {
                if (!string.IsNullOrEmpty("" + body.Group) && body.Group<=0)
                {
                    return Ok(new ResponseGetDataModel<UserCommentResponse>
                    {
                        Code = 400,
                        Message = "Group not found.",
                        Status = 0
                    });
                }
                
                if (!long.TryParse(User.FindFirst(ClaimTypes.NameIdentifier)?.Value, out long accountId))
                {
                    return Ok(new ResponseGetDataModel<UserCommentResponse>
                    {
                        Code = 401,
                        Message = "account invalid",
                        Status = 0
                    });
                }
                var result = new ResponseGetDataModel<UserCommentResponse>();
                switch (body.Group)
                {
                    case (int)EnumUserCommentGroup.Block:
                        result = await CommentService.BlockUpdateAsync(body, accountId);
                        break;
                    case (int)EnumUserCommentGroup.Classify:
                        result = await CommentService.ClassifyUpdateAsync(body, accountId);
                        break;
                    case (int)EnumUserCommentGroup.TopMember:
                        result = await CommentService.TopMemberUpdateAsync(body, accountId);
                        break;
                    default:
                        return Ok(new ResponseGetDataModel<UserCommentResponse>
                        {
                            Code = 400,
                            Message = "Group không được hỗ trợ.",
                            Status = 0
                        });                        
                }                
                return Ok(result);
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
                return Ok(new ResponseGetDataModel<UserCommentResponse>
                {
                    Code = (int)System.Net.HttpStatusCode.OK,
                    Message = ex.Message,
                    Status = 0
                });
            }
        }

        /// <summary>
        /// Lấy danh sách member  by group an status
        /// </summary>
        /// <param name="body"></param>
        /// <returns></returns>
        [HttpGet, Route("member/search")]
        public async Task<IActionResult> SearchMemberCommentAsync([FromQuery]RequestMemberModel body)
        {
            try
            {
                if (body.OfficerId == null || body.OfficerId <= 0)
                {
                    return Ok(new ResponseGetDataModel<UserCommentResponse>
                    {
                        Code = 400,
                        Message = "officer not found.",
                        Status = 0
                    });
                }
                var result = await CommentService.MemberSearchAsync(body);
                return Ok(result);
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
                return Ok(new ResponseGetDataModel<ResponseData<UserCommentResponse[]>>
                {
                    Code = (int)HttpStatusCode.OK,
                    Status = 0,
                    Message = ex.Message
                });
            }
        }
    }
}