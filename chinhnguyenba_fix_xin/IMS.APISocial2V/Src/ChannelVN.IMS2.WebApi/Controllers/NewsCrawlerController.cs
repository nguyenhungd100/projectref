﻿using ChannelVN.IMS2.Core.Entities;
using ChannelVN.IMS2.Core.Entities.ErrorCode;
using ChannelVN.IMS2.Core.Repositories.Constants;
using ChannelVN.IMS2.Core.Repositories.Functions;
using ChannelVN.IMS2.Core.Services;
using ChannelVN.IMS2.Foundation.Common;
using ChannelVN.IMS2.Foundation.Common.Configuration;
using ChannelVN.IMS2.Foundation.Logging;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using static ChannelVN.IMS2.Core.Entities.ErrorCode.ErrorMapping;

namespace ChannelVN.IMS2.WebApi.Controllers
{
    [Route("api/[controller]")]
    public class NewsCrawlerController : BaseController
    {
        /// <summary>
        /// Crawler link
        /// </summary>
        /// <param name="link"></param>
        /// <param name="officerId"></param>
        /// <returns></returns>
        /// <response code="401">Authorization has been denied for this request</response>
        /// <response code="404">Not Found</response>
        /// <response code="405">Invalid input</response>
        [HttpPost, Route("crawler")]
        public async Task<IActionResult> Crawler([FromForm]string link, [FromForm]long officerId)
        {
            try
            {
                var clientIP = string.IsNullOrEmpty(HttpContext?.Connection?.RemoteIpAddress?.ToString()) ? "192.168.1.1" : HttpContext?.Connection?.RemoteIpAddress?.ToString();

                if (string.IsNullOrEmpty(link))
                {
                    return Ok(ActionResponse<string>.CreateErrorResponse("link not found."));
                }
                long.TryParse(User.FindFirst(ClaimTypes.NameIdentifier)?.Value, out long userId);


                var result = await NewsCrawlerService.Crawler(link, userId.ToString(), officerId);
                if (result == ErrorCodes.Success)
                {
                    //insert log
                    await Function.AddToQueue(ActionName.InsertLog, TopicName.LOGACTION, new LogActionEntity
                    {
                        ObjectId = link,
                        Account = userId.ToString(),
                        CreatedDate = DateTime.SpecifyKind(DateTime.Now, DateTimeKind.Unspecified),
                        Ip = clientIP,
                        Message = "Success.",
                        Title = link,
                        SourceId = officerId.ToString(),
                        ActionTypeDetail = (int)ActionType.Insert,
                        ActionText = ActionType.Insert.ToString(),
                        Type = (int)LogContentType.NewsCrawler
                    });
                    return Ok(ActionResponse<string>.CreateSuccessResponse(link, "success"));
                }
                return Ok(ActionResponse<string>.CreateErrorResponse(Current[result].ToString()));

            }
            catch (Exception ex)
            {
                return Ok(ActionResponse<string>.CreateErrorResponse(ex.Message));
            }

        }

        /// <summary>
        /// Search NewsCrawler
        /// </summary>
        /// <param name="body">obj newscrawlerserach</param> 
        /// <param name="officerId">obj newscrawlerserach</param> 
        /// <returns></returns>
        /// <response code="401">Authorization has been denied for this request</response>
        /// <response code="404">Not Found</response>
        /// <response code="405">Invalid input</response>
        // POST api/newscrawler/search  => getway index 98   
        [HttpPost, Route("search")]
        public async Task<IActionResult> Search([FromForm]NewsCrawlerSearchInputModel body, [FromForm]long officerId)
        {
            try
            {
                var keyword = body.keyword;
                var pageIndex = body.pageIndex.HasValue ? (body.pageIndex < 1 ? 1 : body.pageIndex) : 1;
                var pageSize = body.pageSize.HasValue ? (body.pageSize < 1 ? 10 : body.pageSize) : 10;

                var fromDate = body.fromDate.HasValue ? body.fromDate.Value.AddHours(0).AddMinutes(0).AddSeconds(0) : DateTime.MinValue;
                var toDate = body.toDate.HasValue ? body.toDate.Value.AddHours(23).AddMinutes(59).AddSeconds(59) : DateTime.MaxValue;

                var status = body.status.HasValue ? body.status : 0;

                var result = await NewsCrawlerService.SearchAsync(keyword, officerId, status, pageIndex.Value, pageSize.Value, body.orderBy, fromDate, toDate);

                return Ok(ActionResponse<PagingDataResult<NewsCrawler>>.CreateSuccessResponse(result));
            }
            catch (Exception ex)
            {
                return Ok(ActionResponse<string>.CreateErrorResponse(ex.Message));
            }

        }

        /// <summary>
        /// Set Schedule by id
        /// </summary>
        /// <param name="body"></param>        
        /// <returns></returns>
        /// <response code="401">Authorization has been denied for this request</response>
        /// <response code="404">Not Found</response>
        /// <response code="405">Invalid input</response>
        // POST api/newscrawler/set_schedule_by_id        
        [HttpPost, Route("set_schedule_by_id")]
        public async Task<IActionResult> SetScheduleById([FromForm]NewsCrawler body)
        {
            try
            {
                string sessionId = HttpContext.GetSessionId();
                if (string.IsNullOrEmpty(sessionId))
                {
                    return Ok(ActionResponse<string>.CreateErrorResponse("session-id not empty"));
                }
                var clientIP = HttpContext.GetClientIP();

                long.TryParse(User.FindFirst(ClaimTypes.NameIdentifier)?.Value, out long userId);
                if (body.Id <= 0)
                {
                    return Ok(ActionResponse<Tag>.CreateErrorResponse("id not empty."));
                }
                if (body.ScheduleDate == DateTime.MinValue)
                {
                    return Ok(ActionResponse<Tag>.CreateErrorResponse("ScheduleDate not invalid."));
                }
                var status = (int)EnumStatusNewsCrawler.Publish;
                var objNew = new NewsCrawler
                {
                    Id = body.Id,
                    ScheduleDate = body.ScheduleDate,
                    ScheduleBy = userId.ToString(),
                    Status = status
                };

                var result = await NewsCrawlerService.SetScheduleByIdAsync(objNew, userId, clientIP, sessionId);

                if (result == ErrorCodes.Success)
                {
                    //insert log
                    await Function.AddToQueue(ActionName.InsertLog, TopicName.LOGACTION, new LogActionEntity
                    {
                        ObjectId = body.Id.ToString(),
                        Account = userId.ToString(),
                        CreatedDate = DateTime.SpecifyKind(DateTime.Now, DateTimeKind.Unspecified),
                        Ip = clientIP,
                        Message = "Success.",
                        Title = body.ScheduleDate.ToString(),
                        SourceId = body.Id.ToString(),
                        ActionTypeDetail = (int)ActionType.Update,
                        ActionText = ActionType.Update.ToString(),
                        Type = (int)LogContentType.NewsCrawler
                    });
                    return Ok(ActionResponse<string>.CreateSuccessResponse(body.Id.ToString(), "success"));
                }
                return Ok(ActionResponse<string>.CreateErrorResponse(result.ToString()));
            }
            catch (Exception ex)
            {
                return Ok(ActionResponse<string>.CreateErrorResponse(ex.Message));
            }

        }

        /// <summary>
        /// Remove by id
        /// </summary>
        /// <param name="body"></param>
        /// <param name="officerId"></param>        
        /// <returns></returns>
        /// <response code="401">Authorization has been denied for this request</response>
        /// <response code="404">Not Found</response>
        /// <response code="405">Invalid input</response>
        // POST api/newscrawler/set_schedule_by_id        
        [HttpPost, Route("remove_by_id")]
        public async Task<IActionResult> RemoveById([FromForm]NewsCrawler body, [FromForm] long? officerId)
        {
            try
            {
                var clientIP = string.IsNullOrEmpty(HttpContext?.Connection?.RemoteIpAddress?.ToString()) ? "192.168.1.1" : HttpContext?.Connection?.RemoteIpAddress?.ToString();
                long.TryParse(User.FindFirst(ClaimTypes.NameIdentifier)?.Value, out long userId);
                if (body.Id <= 0)
                {
                    return Ok(ActionResponse<Tag>.CreateErrorResponse("id not empty."));
                }

                var status = (int)EnumStatusNewsCrawler.Delete;
                var objNew = new NewsCrawler
                {
                    Id = body.Id,
                    Status = status
                };

                var result = await NewsCrawlerService.RemoveAsync(objNew);

                if (result)
                {
                    //insert log
                    await Function.AddToQueue(ActionName.InsertLog, TopicName.LOGACTION, new LogActionEntity
                    {
                        ObjectId = body.Id.ToString(),
                        Account = userId.ToString(),
                        CreatedDate = DateTime.SpecifyKind(DateTime.Now, DateTimeKind.Unspecified),
                        Ip = clientIP,
                        Message = "Success.",
                        Title = "Remove",
                        SourceId = officerId.ToString(),
                        ActionTypeDetail = (int)ActionType.Update,
                        ActionText = ActionType.Update.ToString(),
                        Type = (int)LogContentType.NewsCrawler
                    });
                    return Ok(ActionResponse<string>.CreateSuccessResponse(body.Id.ToString(), "success"));
                }
                return Ok(ActionResponse<bool>.CreateErrorResponse("Lỗi không tìm thấy bài viết cần xóa."));
            }
            catch (Exception ex)
            {
                return Ok(ActionResponse<string>.CreateErrorResponse(ex.Message));
            }

        }

        /// <summary>
        /// Add tag
        /// </summary>
        /// <param name="tag"></param>
        /// <param name="officerId"></param>
        /// <returns></returns>
        /// <response code="401">Authorization has been denied for this request</response>
        /// <response code="404">Not Found</response>
        /// <response code="405">Invalid input</response>
        [HttpPost, Route("add")]
        public async Task<IActionResult> Add([FromForm]Tag tag, [FromForm] long officerId)
        {
            try
            {
                var clientIP = string.IsNullOrEmpty(HttpContext?.Connection?.RemoteIpAddress?.ToString()) ? "192.168.1.1" : HttpContext?.Connection?.RemoteIpAddress?.ToString();

                if (tag == null)
                {
                    return Ok(ActionResponse<Tag>.CreateErrorResponse("Tag không tồn tại."));
                }

                if (string.IsNullOrEmpty(tag.Name?.Trim())) return Ok(ActionResponse<Tag>.CreateErrorResponse("Tag name không được để trống."));
                tag.Id = Generator.TagId();
                tag.CreatedDate = DateTime.Now;
                tag.ParentId = tag.ParentId ?? 0;
                if (long.TryParse(User.FindFirst(ClaimTypes.NameIdentifier)?.Value, out long userId))
                {
                    tag.CreatedBy = userId.ToString();
                }
                tag.Status = (int)TagStatus.Actived;
                tag.IsHotTag = tag.IsHotTag ?? false;
                var url = Utility.UnicodeToUnsignedAndDash(tag.Name?.Substring(0, 50));
                tag.UnsignName = url;
                tag.Url = string.Format(AppSettings.Current.ChannelConfiguration.CommonSettings.UrlFormatTag, url);

                var tagDb = await TagService.GetByNameAsync(tag.Name);
                if (tagDb == null || tagDb.Id == 0)
                {
                    var result = await TagService.AddAsync(tag);
                    if (result)
                    {
                        //insert log
                        await Function.AddToQueue(ActionName.InsertLog, TopicName.LOGACTION, new LogActionEntity
                        {
                            ObjectId = tag.Id.ToString(),
                            Account = userId.ToString(),
                            CreatedDate = DateTime.SpecifyKind(DateTime.Now, DateTimeKind.Unspecified),
                            Ip = clientIP,
                            Message = "Success.",
                            Title = tag.Name,
                            SourceId = officerId.ToString(),
                            ActionTypeDetail = (int)ActionType.Insert,
                            ActionText = ActionType.Insert.ToString(),
                            Type = (int)LogContentType.Tag
                        });
                        return Ok(ActionResponse<object>.CreateSuccessResponse(tag.EncryptId, "success"));
                    }
                    return Ok(ActionResponse<string>.CreateErrorResponse("Lỗi hệ thống khi tạo tag."));
                }
                else
                {
                    return Ok(ActionResponse<object>.CreateSuccessResponse(tagDb.EncryptId, "success"));
                }
            }
            catch (Exception ex)
            {
                return Ok(ActionResponse<string>.CreateErrorResponse(ex.Message));
            }

        }

        /// <summary>
        /// Update tag
        /// </summary>
        /// <param name="tag"></param>
        /// <param name="officerId"></param>
        /// <returns></returns>
        /// <response code="401">Authorization has been denied for this request</response>
        /// <response code="404">Not Found</response>
        /// <response code="405">Invalid input</response>
        //POST api/tag/update
        [HttpPost, Route("update")]
        public async Task<IActionResult> Update([FromForm]Tag tag, [FromForm] long officerId)
        {
            try
            {
                var clientIP = string.IsNullOrEmpty(HttpContext?.Connection?.RemoteIpAddress?.ToString()) ? "192.168.1.1" : HttpContext?.Connection?.RemoteIpAddress?.ToString();
                long.TryParse(User.FindFirst(ClaimTypes.NameIdentifier)?.Value, out long userId);
                if (tag == null)
                {
                    return Ok(ActionResponse<Tag>.CreateErrorResponse("Tag không tồn tại."));
                }
                var arrStatus = Enum.GetValues(typeof(TagStatus)) as int[];
                if (tag.Status == null || (tag.Status != null && arrStatus != null && !arrStatus.Contains(tag.Status ?? -1)))
                {
                    return Ok(ActionResponse<string>.CreateErrorResponse("Trạng thái tag không hợp lệ."));
                }
                var url = Utility.UnicodeToUnsignedAndDash(tag.Name);
                tag.UnsignName = url;
                tag.Url = string.Format(AppSettings.Current.ChannelConfiguration.CommonSettings.UrlFormatTag, url);

                var result = await TagService.UpdateAsync(tag);

                if (result)
                {
                    //insert log
                    await Function.AddToQueue(ActionName.InsertLog, TopicName.LOGACTION, new LogActionEntity
                    {
                        ObjectId = tag.Id.ToString(),
                        Account = userId.ToString(),
                        CreatedDate = DateTime.SpecifyKind(DateTime.Now, DateTimeKind.Unspecified),
                        Ip = clientIP,
                        Message = "Success.",
                        Title = tag.Name,
                        SourceId = officerId.ToString(),
                        ActionTypeDetail = (int)ActionType.Update,
                        ActionText = ActionType.Update.ToString(),
                        Type = (int)LogContentType.Tag
                    });
                    return Ok(ActionResponse<object>.CreateSuccessResponse(tag.EncryptId, "success"));
                }
                return Ok(ActionResponse<bool>.CreateErrorResponse("Lỗi cập nhật tag video."));
            }
            catch (Exception ex)
            {
                return Ok(ActionResponse<string>.CreateErrorResponse(ex.Message));
            }

        }

        /// <summary>
        /// Get tag by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        /// <response code="401">Authorization has been denied for this request</response>
        /// <response code="404">Not Found</response>
        /// <response code="405">Invalid input</response>
        // POST api/newscrawler/get_by_id        
        [HttpPost, Route("get_by_id")]
        public async Task<IActionResult> GetById([FromForm]long id)
        {
            try
            {
                var result = await NewsCrawlerService.GetByIdAsync(id);

                return Ok(ActionResponse<NewsCrawler>.CreateSuccessResponse(result));
            }
            catch (Exception ex)
            {
                return Ok(ActionResponse<string>.CreateErrorResponse(ex.Message));
            }
        }


        /// <summary>
        /// Search crawler config
        /// </summary>
        /// <param name="body"></param>
        /// <returns></returns>
        /// <response code="401">Authorization has been denied for this request</response>
        /// <response code="404">Not Found</response>
        /// <response code="405">Invalid input</response>
        // POST api/video/search => getway index 71
        [HttpPost, Route("search_config")]
        public async Task<IActionResult> SearchConfigAsync([FromForm]NewsCrawlerConfigSearchModel body)
        {
            try
            {
                var keyword = body.keyword;
                var pageIndex = body.pageIndex.HasValue ? (body.pageIndex < 1 ? 1 : body.pageIndex) : 1;
                var pageSize = body.pageSize.HasValue ? (body.pageSize < 1 ? 10 : body.pageSize) : 10;

                var fromDate = body.fromDate.HasValue ? body.fromDate.Value.AddHours(0).AddMinutes(0).AddSeconds(0) : DateTime.MinValue;
                var toDate = body.toDate.HasValue ? body.toDate.Value.AddHours(23).AddMinutes(59).AddSeconds(59) : DateTime.MaxValue;

                var status = body.status.HasValue ? body.status : 1;

                var result = await NewsCrawlerService.SearchConfigAsync(keyword, body.officerId, body.status, pageIndex.Value, pageSize.Value, body.orderBy, fromDate, toDate);

                return Ok(ActionResponse<PagingDataResult<NewCrawlerConfigurationSearch>>.CreateSuccessResponse(result));
            }
            catch (Exception ex)
            {
                return Ok(ActionResponse<string>.CreateErrorResponse(ex.Message));
            }
        }

        /// <summary>
        /// Add new crawler config
        /// </summary>
        /// <param name="crawlerConfig"></param>
        /// <returns></returns>
        /// <response code="401">Authorization has been denied for this request</response>
        /// <response code="404">Not Found</response>
        /// <response code="405">Invalid input</response>
        [HttpPost, Route("save_config")]
        public async Task<IActionResult> SaveConfigAsync([FromForm] NewCrawlerConfigurationSearch crawlerConfig)
        {
            try
            {
                var result = ErrorCodes.BusinessError;
                var clientIP = HttpContext.GetClientIP();
                var validateResult = ValidateNewCrawlerConfig(crawlerConfig);
                if (validateResult?.Success == false)
                {
                    return Ok(ActionResponse<string>.CreateErrorResponse(validateResult.Message));
                }

                if (long.TryParse(User.FindFirst(ClaimTypes.NameIdentifier)?.Value, out long userId))
                {
                    if (crawlerConfig.Id > 0)
                    {                                            
                        result = await NewsCrawlerService.UpdateConfigAsync(crawlerConfig, clientIP);
                        if (result == ErrorCodes.Success)
                        {
                            await Function.AddToQueue(ActionName.InsertLog, TopicName.LOGACTION, new LogActionEntity
                            {
                                ObjectId = crawlerConfig.Id.ToString(),
                                Account = userId.ToString(),
                                CreatedDate = DateTime.SpecifyKind(DateTime.Now, DateTimeKind.Unspecified),
                                Ip = clientIP,
                                Message = "Success.",
                                Title = "",
                                SourceId = crawlerConfig.OfficerId.ToString(),
                                ActionTypeDetail = (int)ActionType.Update,
                                ActionText = ActionType.Update.ToString(),
                                Type = (int)LogContentType.NewsCrawlerConfig
                            });
                        }
                    }
                    else
                    {
                        if (string.IsNullOrEmpty(crawlerConfig.Source))
                        {
                            return Ok(ActionResponse<string>.CreateErrorResponse("Link Source is not null or empty..."));
                        }
                        var configSearch = Mapper<NewCrawlerConfigurationSearch>.Map(crawlerConfig, new NewCrawlerConfigurationSearch());
                        crawlerConfig.Id = Generator.NewsId();
                        crawlerConfig.CreatedBy = userId.ToString();
                        crawlerConfig.CreatedDate = DateTime.Now;
                        crawlerConfig.IntervalTime = crawlerConfig.IntervalTime ?? 0;
                        result = await NewsCrawlerService.AddConfigAsync(crawlerConfig, clientIP);
                        if (result == ErrorCodes.Success)
                        {
                            await Function.AddToQueue(ActionName.InsertLog, TopicName.LOGACTION, new LogActionEntity
                            {
                                ObjectId = configSearch.Id.ToString(),
                                Account = userId.ToString(),
                                CreatedDate = DateTime.SpecifyKind(DateTime.Now, DateTimeKind.Unspecified),
                                Ip = clientIP,
                                Message = "Success.",
                                Title = "",
                                SourceId = crawlerConfig.OfficerId.ToString(),
                                ActionTypeDetail = (int)ActionType.Update,
                                ActionText = ActionType.Insert.ToString(),
                                Type = (int)LogContentType.NewsCrawlerConfig
                            });
                        }
                    };
                }
                if (result == ErrorCodes.Success)
                {
                    return Ok(ActionResponse<object>.CreateSuccessResponse(crawlerConfig.Id.ToString(), "success"));
                }
                else
                {
                    return Ok(ActionResponse<string>.CreateErrorResponse(Current[result]));
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
                return Ok(ActionResponse<string>.CreateErrorResponse((int)ErrorCodes.Exception, ex.Message));
            }
        }

        private ActionResponse<string> ValidateNewCrawlerConfig(NewCrawlerConfigurationSearch crawlerConfig)
        {
            try
            {              
                if (crawlerConfig == null)
                {
                    return ActionResponse<string>.CreateErrorResponse("Crawler config not found.");
                }

                return ActionResponse<string>.CreateSuccessResponse(Current[ErrorCodes.Success]);
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
                return ActionResponse<string>.CreateErrorResponse("Exception...");
            }
        }


        /// <summary>
        /// Remove config by id
        /// </summary>
        /// <param name="body"></param>
        /// <param name="officerId"></param>        
        /// <returns></returns>
        /// <response code="401">Authorization has been denied for this request</response>
        /// <response code="404">Not Found</response>
        /// <response code="405">Invalid input</response>
        // POST api/newscrawler/set_schedule_by_id        
        [HttpPost, Route("remove_config_by_id")]
        public async Task<IActionResult> RemoveConfigById([FromForm]NewCrawlerConfigurationSearch body, [FromForm] long? officerId)
        {
            try
            {
                var clientIP = string.IsNullOrEmpty(HttpContext?.Connection?.RemoteIpAddress?.ToString()) ? "192.168.1.1" : HttpContext?.Connection?.RemoteIpAddress?.ToString();
                long.TryParse(User.FindFirst(ClaimTypes.NameIdentifier)?.Value, out long userId);
                if (body.Id <= 0)
                {
                    return Ok(ActionResponse<string>.CreateErrorResponse("id not empty."));
                }

                var status = EnumStatusCrawlerConfig.Delete;
                var objNew = new NewCrawlerConfigurationSearch
                {
                    Id = body.Id,
                    Status = status
                };

                var result = await NewsCrawlerService.RemoveConfigAsync(objNew);

                if (result)
                {
                    //insert log
                    await Function.AddToQueue(ActionName.InsertLog, TopicName.LOGACTION, new LogActionEntity
                    {
                        ObjectId = body.Id.ToString(),
                        Account = userId.ToString(),
                        CreatedDate = DateTime.SpecifyKind(DateTime.Now, DateTimeKind.Unspecified),
                        Ip = clientIP,
                        Message = "Success.",
                        Title = "Remove",
                        SourceId = officerId.ToString(),
                        ActionTypeDetail = (int)ActionType.Update,
                        ActionText = ActionType.Update.ToString(),
                        Type = (int)LogContentType.NewsCrawlerConfig
                    });
                    return Ok(ActionResponse<string>.CreateSuccessResponse(body.Id.ToString(), "success"));
                }
                return Ok(ActionResponse<bool>.CreateErrorResponse("Lỗi không tìm thấy cấu hình crawler cần xóa."));
            }
            catch (Exception ex)
            {
                return Ok(ActionResponse<string>.CreateErrorResponse(ex.Message));
            }

        }

        [HttpPost, Route("active_auto")]
        public IActionResult ActiveAuto()
        {
            NewsCrawlerService.JobAutoNewsCrawlerSetting();
            return Ok(ActionResponse<string>.CreateSuccessResponse("success"));
        }
    }
}
