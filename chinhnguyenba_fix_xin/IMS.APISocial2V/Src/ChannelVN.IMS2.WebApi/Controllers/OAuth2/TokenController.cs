﻿using ChannelVN.IMS2.Core.Entities.Security;
using ChannelVN.IMS2.Core.Repositories.Constants;
using ChannelVN.IMS2.Core.Repositories.Functions;
using ChannelVN.IMS2.Core.Services;
using ChannelVN.IMS2.Foundation.Common;
using ChannelVN.IMS2.Foundation.Common.Configuration;
using ChannelVN.IMS2.Foundation.Logging;
using ChannelVN.IMS2.Foundation.Security;
using ChannelVN.IMS2.Foundation.Security.Bearer.Helpers;
using ChannelVN.IMS2.Foundation.Security.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Linq;
using System.Threading.Tasks;
using static ChannelVN.IMS2.Core.Entities.ErrorCode.ErrorMapping;

namespace ChannelVN.IMS2.WebApi.Controllers.OAuth2
{
    [Produces("application/json")]
    [Route("oauth2")]
    [Authorize]
    public class TokenController : Controller
    {
        /// <summary>
        /// Support case grant_type=password
        /// </summary>
        /// <param name="inputModel"> Require {username, password}</param>
        /// <returns></returns>
        [HttpPost, Route("token")]
        [AllowAnonymous]
        public IActionResult Index([FromForm]LoginInputModel inputModel)
        {
            //Logger.Debug(Foundation.Common.Json.Stringify(inputModel));
            var appSettings = AppSettings.Current;
            var clientRegistry = appSettings.ServiceConfiguration.ClientRegistry;

            if (string.IsNullOrEmpty(inputModel.Username)
                || string.IsNullOrEmpty(inputModel.Password)
                || !clientRegistry.Keys.Contains(inputModel.Password))
            {
                return Unauthorized();
            }

            var clientInfo = clientRegistry[inputModel.Password];

            var issuer = clientInfo.Issuer;
            var audience = clientInfo.Audience;
            var expiresIn = clientInfo.Expiry;

            if (string.IsNullOrEmpty(issuer))
            {
                issuer = appSettings.ServiceConfiguration.Namespace;
            }
            if (!string.IsNullOrEmpty(clientInfo.Subject))
            {
                inputModel.Username = clientInfo.Subject;
            }
            if (expiresIn <= 0)
            {
                expiresIn = appSettings.ServiceConfiguration.TokenExpiresIn;
            }

            var tokenBuilder = new JwtTokenBuilder()
                                .AddSecurityKey(JwtSecurityKey.Create(appSettings.ChannelConfiguration.SecretKey))
                                .AddSubject(inputModel.Username.ToLower())
                                .AddIssuer(issuer)
                                .AddAudience(audience)
                                .AddExpiry(expiresIn);

            if (clientInfo.Claims.Count > 0)
            {
                //var claims = new System.Collections.Generic.List<string>();
                //foreach (var keyType in clientInfo.Claims.Keys)
                //{
                //    claims.Add(string.Join(':', keyType.ToLower(), string.Join(',', clientInfo.Claims?[keyType])));
                //}
                tokenBuilder.AddClaim("aut", Foundation.Common.Json.Stringify(clientInfo.Claims)); //string.Join(';', claims.ToArray()));
            }

            var token = tokenBuilder.Build();
            return Ok(token);
        }

        /// <summary>
        /// Support case grant_type=client_credentials
        /// </summary>
        /// <param name="inputModel">Require {username, password, Headers["X-Api-Key"]}</param>
        /// <returns></returns>
        [HttpPost, Route("login")]
        [AllowAnonymous]
        public async Task<IActionResult> Login([FromForm]LoginInputModel inputModel)
        {
            var apiKey = string.Empty;
            foreach (var key in HttpContext.Request.Headers?.Keys)
            {
                if (key.ToLower().Equals(Foundation.Security.SecureParser.XApiKey.ToLower()))
                {
                    apiKey = HttpContext.Request.Headers?[key];
                    break;
                }
            }
            var appSettings = AppSettings.Current;
            var clientRegistry = appSettings.ServiceConfiguration.ClientRegistry;

            if (string.IsNullOrEmpty(inputModel.Username)
                || string.IsNullOrEmpty(inputModel.Password)
                || !clientRegistry.Keys.Contains(apiKey))
            {
                return Unauthorized();
            }

            var userInfo = await SecurityService.GetByUsernameAsync(inputModel.Username.ToLower());
            if (userInfo != null)
            {
                if (Encryption.Md5(inputModel.Password ?? "").Equals(userInfo.Password))
                {
                    var clientInfo = clientRegistry[apiKey];

                    var issuer = clientInfo.Issuer;
                    var audience = clientInfo.Audience;
                    var expiresIn = clientInfo.Expiry;

                    if (string.IsNullOrEmpty(issuer))
                    {
                        issuer = appSettings.ServiceConfiguration.Namespace;
                    }
                    if (expiresIn <= 0)
                    {
                        expiresIn = appSettings.ServiceConfiguration.TokenExpiresIn;
                    }

                    var tokenBuilder = new JwtTokenBuilder()
                                        .AddSecurityKey(JwtSecurityKey.Create(appSettings.ChannelConfiguration.SecretKey))
                                        .AddSubject(userInfo.Id.ToString())
                                        .AddIssuer(issuer)
                                        .AddAudience(audience)
                                        .AddExpiry(expiresIn);

                    if (clientInfo.Claims.Count > 0)
                    {
                        //var claims = new List<string>();
                        //foreach (var keyType in userInfo.Claims.Keys)
                        //{
                        //    claims.Add(string.Join(':', keyType.ToLower(), string.Join(',', userInfo.Claims?[keyType])));
                        //}
                        tokenBuilder.AddClaim("aut", Foundation.Common.Json.Stringify(clientInfo.Claims)); //string.Join(';', claims.ToArray()));
                    }
                    return Ok(tokenBuilder.Build());
                }
                else
                {
                    return Ok(ActionResponse<string>.CreateErrorResponse("Username or Password is wrong!"));
                }
            }
            else
            {
                return Ok(ActionResponse<string>.CreateErrorResponse("No users exist!"));
            }
        }


        /// <summary>
        /// Logout
        /// </summary>
        /// <returns></returns>
        [HttpPost, Route("logout")]
        public async Task<IActionResult> Logout()
        {
            var result = false;
            Task<bool> logoutAsync()
            {
                var token = HttpContext.Request.Headers["Authorization"].ToString();
                if (!string.IsNullOrEmpty(token) && token.ToLower().Contains("bearer"))
                {
                    token = token.Trim().Substring(6).Trim();
                }
                BlackListToken.AddElement(token);
                result = true;
                return Task.FromResult(true);
            }

            await Task.WhenAll(Task.FromResult(await logoutAsync()));

            if (result)
                return Ok(ActionResponse<string>.CreateSuccessResponse("logout success."));
            else
                return Ok(ActionResponse<string>.CreateErrorResponse("logout false."));
        }

        /// <summary>
        /// Support case grant_type=client_credentials
        /// </summary>
        /// <param name="kingHubId">Require {username, password, Headers["X-Api-Key"]}</param>
        /// <param name="password"></param>
        /// <returns></returns>
        [AllowAnonymous]
        [HttpPost, Route("login_with_kinghubid")]
        public async Task<IActionResult> LoginWithKingHubId([FromForm]long kingHubId, [FromForm]string password)
        {
            var appSettings = AppSettings.Current;
            var clientRegistry = appSettings.ServiceConfiguration.ClientRegistry;

            if (kingHubId == 0
                || string.IsNullOrEmpty(password)
                || !clientRegistry.Keys.Contains(password))
            {
                return Ok(ActionResponse<string>.CreateErrorResponse("Secretkey invalid."));
            }

            var userInfo = await SecurityService.GetByIdAsync(kingHubId);
            if (userInfo != null)
            {
                var clientInfo = clientRegistry[password];

                var issuer = clientInfo.Issuer;
                var audience = clientInfo.Audience;
                var expiresIn = clientInfo.Expiry;

                if (string.IsNullOrEmpty(issuer))
                {
                    issuer = appSettings.ServiceConfiguration.Namespace;
                }
                if (expiresIn <= 0)
                {
                    expiresIn = appSettings.ServiceConfiguration.TokenExpiresIn;
                }

                var tokenBuilder = new JwtTokenBuilder()
                .AddSecurityKey(JwtSecurityKey.Create(appSettings.ChannelConfiguration.SecretKey))
                .AddSubject(userInfo.Id.ToString())
                .AddIssuer(issuer)
                .AddAudience(audience)
                .AddExpiry(expiresIn);

                if (clientInfo.Claims.Count > 0)
                {
                    tokenBuilder.AddClaim("aut", Foundation.Common.Json.Stringify(clientInfo.Claims)); //string.Join(';', claims.ToArray()));
                }
                return Ok(ActionResponse<object>.CreateSuccessResponse(tokenBuilder.Build()));
            }
            else
            {
                return Ok(ActionResponse<string>.CreateErrorResponse("No users exist!"));
            }
        }

        /// <summary>
        /// Kiểm tra tài khoản đã tồn tại trong hệ thống hay chưa qua số điện thoại
        /// </summary>
        /// <param name="mobile"> Require {mobile}</param>
        /// <param name="password"> Require {password}</param>
        /// <returns></returns>
        [AllowAnonymous]
        [HttpPost, Route("check_user_by_mobile")]
        public async Task<IActionResult> CheckMobileAsync([FromForm]string mobile, string password)
        {
            var appSettings = AppSettings.Current;
            var clientRegistry = appSettings.ServiceConfiguration.ClientRegistry;

            if (string.IsNullOrEmpty(mobile)
                || string.IsNullOrEmpty(password)
                || !clientRegistry.Keys.Contains(password))
            {
                return Unauthorized();
            }

            var result = await SecurityService.CheckMobileAsync(mobile);
             return Ok(ActionResponse<long?>.CreateSuccessResponse(result));
        }

        /// <summary>
        /// Support case using with a gateway
        /// </summary>
        /// <param name="inputModel">Require {username, password, Headers["X-Api-Key"]}</param>
        /// <returns></returns>
        [AllowAnonymous]
        [HttpPost, Route("v1/auth")]
        public async Task<IActionResult> Auth([FromForm]LoginInputModel inputModel)
        {
            try
            {
                var apiKey = string.Empty;
                foreach (var key in HttpContext.Request.Headers?.Keys)
                {
                    if (key.ToLower().Equals(Foundation.Security.SecureParser.XApiKey.ToLower()))
                    {
                        apiKey = HttpContext.Request.Headers?[key];
                        break;
                    }
                }
                var appSettings = AppSettings.Current;
                var clientRegistry = appSettings.ServiceConfiguration.ClientRegistry;

                if (string.IsNullOrEmpty(apiKey))
                {
                    if (string.IsNullOrEmpty(inputModel.Username)
                            || string.IsNullOrEmpty(inputModel.Password)
                            || !clientRegistry.Keys.Contains(inputModel.Password))
                    {
                        return Unauthorized();
                    }
                    var clientInfo = clientRegistry[inputModel.Password];

                    var issuer = clientInfo.Issuer;
                    var audience = clientInfo.Audience;
                    var expiresIn = clientInfo.Expiry;

                    if (string.IsNullOrEmpty(issuer))
                    {
                        issuer = appSettings.ServiceConfiguration.Namespace;
                    }
                    if (!string.IsNullOrEmpty(clientInfo.Subject))
                    {
                        inputModel.Username = clientInfo.Subject;
                    }
                    if (expiresIn <= 0)
                    {
                        expiresIn = appSettings.ServiceConfiguration.TokenExpiresIn;
                    }
                    return Ok(new
                    {
                        sub = inputModel.Username.ToLower(),
                        iss = issuer,
                        aud = audience,
                        exp = expiresIn,
                        jti = Guid.NewGuid(),
                        idp = AppSettings.Current.Application.Id,
                        aut = clientInfo.Claims,
                        iat = DateTime.Now
                    });
                }
                else
                {
                    if (string.IsNullOrEmpty(inputModel.Username)
                            || string.IsNullOrEmpty(inputModel.Password)
                            || !clientRegistry.Keys.Contains(apiKey))
                    {
                        return Unauthorized();
                    }
                    var userInfo = await SecurityService.GetByUsernameAsync(inputModel.Username);

                    if (userInfo != null)
                    {
                        if (Encryption.Md5(inputModel.Password ?? "").Equals(userInfo.Password))
                        {
                            var clientInfo = clientRegistry[apiKey];

                            var issuer = clientInfo.Issuer;
                            var audience = clientInfo.Audience;
                            var expiresIn = clientInfo.Expiry;

                            if (string.IsNullOrEmpty(issuer))
                            {
                                issuer = appSettings.ServiceConfiguration.Namespace;
                            }
                            if (expiresIn <= 0)
                            {
                                expiresIn = appSettings.ServiceConfiguration.TokenExpiresIn;
                            }

                            return Ok(new
                            {
                                sub = userInfo.Id.ToString(), // inputModel.Username,
                                iss = issuer,
                                aud = audience,
                                exp = expiresIn,
                                jti = Guid.NewGuid(),
                                idp = AppSettings.Current.Application.Id,
                                aut = clientInfo.Claims,
                                //new Dictionary<object, object>() {
                                //            { "Roles", new []{ "Users" } },
                                //            { "Permissions",new []{ "6", "9" } }
                                //        },
                                iat = DateTime.Now
                            });
                        }
                        else
                        {
                            return Ok(ActionResponse<string>.CreateErrorResponse("Username or Password is wrong!"));
                        }
                    }
                    else
                    {
                        return Ok(ActionResponse<string>.CreateErrorResponse("No users exist!"));
                    }
                }
            }
            catch (Exception ex)
            {
                return Ok(ActionResponse<string>.CreateErrorResponse(ex.Message));
            }
        }

        //chinhnb
        [HttpPost, Route("check_login")]
        [AllowAnonymous]
        public async Task<IActionResult> CheckLoginAsync([FromForm]string mobile)
        {
            try
            {                
                if (string.IsNullOrEmpty(mobile))
                {
                    return Ok(ActionResponse<string>.CreateErrorResponse(Current[ErrorCodes.AccountNotExits]));
                }

                var apiKey = string.Empty;
                foreach (var key in HttpContext.Request.Headers?.Keys)
                {
                    if (key.ToLower().Equals(SecureParser.XApiKey.ToLower()))
                    {
                        apiKey = HttpContext.Request.Headers?[key];
                        break;
                    }
                }
                var appSettings = AppSettings.Current;
                var clientRegistry = appSettings.ServiceConfiguration.ClientRegistry;

                if (!clientRegistry.Keys.Contains(apiKey))
                {
                    return Unauthorized();
                }

                var accountId = await SecurityService.CheckMobileAsync(mobile);
                if (accountId > 0)
                {
                    //var account = await SecurityService.GetByIdAsync(accountId.Value);
                    return Ok(ActionResponse<long?>.CreateSuccessResponse(accountId));
                }
                else
                {
                    return Ok(ActionResponse<AccountSearch>.CreateErrorResponse(Current[ErrorCodes.AccountNotExits]));
                    //var account = new Account();
                    ////get kingHub
                    ////var userInfo = await SecurityService.AuthenKinghub(accesstoken);
                    //var arrayUser = await SecurityService.SearchInviteAsync(mobile, 1, 1);

                    //if (arrayUser == null || (arrayUser != null && arrayUser.Count()<=0)) return Ok(ActionResponse<AccountSearch>.CreateErrorResponse(Current[ErrorCodes.AccountNotExits]));

                    //var userInfo = arrayUser.FirstOrDefault();
                    ////Logger.Debug("CHINHNB Kinghub userInfo: ", userInfo);

                    //account.Id = long.Parse(userInfo.User_id);
                    //account.UserName = userInfo.Username?.ToLower();
                    //account.Mobile = mobile;
                    //account.Email = userInfo.Email;
                    //account.FullName = userInfo.Full_name;


                    //account.Password = Encryption.Md5(account.Password ?? "");
                    //account.CreatedDate = DateTime.Now;
                    //account.Type = (int)AccountType.ReadOnly;
                    //account.Class = (int)AccountClass.Normal;
                    //account.Status = (int)AccountStatus.Actived;
                    //account.VerifiedBy = null;
                    //account.VerifiedDate = null;

                    //account.IsSystem = false;
                    //account.IsFullPermission = false;

                    //var erroCode = await SecurityService.AddAsync(account);
                    //if (erroCode == ErrorCodes.Success)
                    //{
                    //    return Ok(ActionResponse<object>.CreateSuccessResponse(new { Id = account.EncryptId }, Current[erroCode]));
                    //}
                    //else
                    //{
                    //    Logger.Sensitive("Thêm mới tài khoản không thành công.", account);
                    //}
                    //return Ok(ActionResponse<string>.CreateErrorResponse(Current[erroCode]));
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
                return Ok(ActionResponse<string>.CreateErrorResponse(ex.Message));
            }
        }
    }
}