﻿using System;
using System.IO;
using System.Net.Http;
using System.Security.Claims;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using ChannelVN.IMS2.Core.Entities;
using ChannelVN.IMS2.Core.Entities.Gallery;
using ChannelVN.IMS2.Core.Entities.Media;
using ChannelVN.IMS2.Core.Repositories.Constants;
using ChannelVN.IMS2.Core.Repositories.Functions;
using ChannelVN.IMS2.Core.Services;
using ChannelVN.IMS2.Foundation.Common;
using ChannelVN.IMS2.Foundation.Common.Configuration;
using ChannelVN.IMS2.Foundation.Logging;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using static ChannelVN.IMS2.Core.Entities.ErrorCode.ErrorMapping;

namespace ChannelVN.IMS2.WebApi.Controllers
{
    [Route("api/[controller]")]
    public class GalleryController : BaseController
    {
      
        /// <summary>
        /// UploadImage
        /// </summary>
        /// <param name="officerId"></param>
        /// <returns></returns>
        [HttpPost, Route("image/upload")]
        public async Task<IActionResult> UploadImageAsync([FromForm] long officerId)
        {
            try
            {
                var clientIP = string.IsNullOrEmpty(HttpContext?.Connection?.RemoteIpAddress?.ToString()) ? "192.168.1.1" : HttpContext?.Connection?.RemoteIpAddress?.ToString();
                long.TryParse(User.FindFirst(ClaimTypes.NameIdentifier)?.Value, out long accountId);
                object result;
                var req = HttpContext.Request;

                // Allows using several time the stream in ASP.Net Core
                req.EnableBuffering();

                // Arguments: Stream, Encoding, detect encoding, buffer size 
                // AND, the most important: keep stream opened

                var content = new FormUrlEncodedContent(HttpContext.Request.Form.ConvertToKeyValuePair());
                // Rewind, so the core is not lost when it looks the body for the request

                Logger.Debug("Upload image =>" + Foundation.Common.Json.Stringify(HttpContext.Request.Form.ConvertToKeyValuePair()));
                // Do whatever work with bodyStr here

                using (var httpClient = new HttpClient())
                {
                    foreach (var item in HttpContext.Request.Headers)
                    {
                        httpClient.DefaultRequestHeaders.Add(item.Key, item.Value.ToString());
                    }
                    var response = await httpClient.PostAsync("http://w-api.lotus.vn/api/v2/ml/web/image/upload/dev", content);
                    if (!response.IsSuccessStatusCode)
                    {
                        response.EnsureSuccessStatusCode();
                    }

                    var astr = await response.Content?.ReadAsStringAsync();
                    //Logger.Debug("GetData2Async=> result:" + astr);
                    result = Foundation.Common.Json.Parse<object>(astr);

                    Logger.Debug("Upload image response =>" + Foundation.Common.Json.Stringify(result));


                    await Function.AddToQueue(ActionName.InsertLog, TopicName.LOGACTION, new LogActionEntity
                    {
                        ObjectId = null,
                        Account = accountId.ToString(),
                        CreatedDate = DateTime.SpecifyKind(DateTime.Now, DateTimeKind.Unspecified),
                        Ip = clientIP,
                        Message = "Success.",
                        Title = null,
                        SourceId = officerId.ToString(),
                        ActionTypeDetail = (int)ActionType.UploadGalleryImage,
                        ActionText = ActionType.UploadGalleryImage.ToString(),
                        Type = (int)LogContentType.Gallery
                    });
                    return Ok(result);
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
                return Ok(ex.Message);
            }
        }

        [HttpGet, Route("get_filename_encrypt")]
        public async Task<IActionResult> GetFileNameEncryptAsync([FromQuery]string filename)
        {
            try
            {
                var resutlEncrypt = string.Empty;
                Task<bool> GenIdAsync()
                {
                    var secret = AppSettings.Current.ChannelConfiguration.SecretKeyFilename;
                    var encoding = new System.Text.ASCIIEncoding();
                    byte[] keyByte = encoding.GetBytes(secret);
                    byte[] messageBytes = encoding.GetBytes(filename);
                    using (var hmacsha256 = new HMACSHA256(keyByte))
                    {
                        byte[] hashmessage = hmacsha256.ComputeHash(messageBytes);
                        resutlEncrypt = Convert.ToBase64String(hashmessage);
                    }
                    return Task.FromResult(true);
                }

                await Task.WhenAll(Task.FromResult(await GenIdAsync()));
                return Ok(ActionResponse<string>.CreateSuccessResponse(resutlEncrypt, "success"));
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
                return Ok(ActionResponse<string>.CreateErrorResponse(ex.Message));
            }
        }


        /// <summary>
        /// Lưu album
        /// </summary>
        /// <param name="galleryData"></param>
        /// <param name="OfficerId"></param>
        /// <returns></returns>
        /// <response code="401">Authorization has been denied for this request</response>
        /// <response code="404">Not Found</response>
        /// <response code="405">Invalid input</response>
        // POST api/album/save        
        [HttpPost, Route("save")]
        public async Task<IActionResult> Save([FromForm]GallerySearch galleryData, [FromForm]long OfficerId)
        {
            try
            {
                string sessionId = HttpContext?.Request?.Headers?["session-id"];
                if (string.IsNullOrEmpty(sessionId))
                {
                    return Ok(ActionResponse<string>.CreateErrorResponse("session-id not empty"));
                }
                var clientIP = HttpContext.GetClientIP();

                var result = ErrorCodes.Exception;
                var validateResult = ValidateGallery(galleryData);
                if (validateResult?.Success == false)
                {
                    return Ok(ActionResponse<string>.CreateErrorResponse(validateResult.Message));
                }

                if (!Utility.ValidateStringIds(galleryData.Tags))
                {
                    return Ok(ActionResponse<string>.CreateErrorResponse("Tags không hợp lệ."));
                }

                var validPublishData = await Core.Services.Functions.Function.ValidatePublishData(galleryData.PublishData, (int)CardType.Gallery1);
                if (!validPublishData)
                {
                    return Ok(ActionResponse<string>.CreateErrorResponse("PublishData invalid."));
                }

                if (long.TryParse(User.FindFirst(ClaimTypes.NameIdentifier)?.Value, out long accountId))
                {
                    if (galleryData.Id > 0)
                    {                        
                        //update
                        galleryData.ModifiedDate = DateTime.Now;
                        galleryData.ModifiedBy = accountId + "";
                        galleryData.CardType = galleryData.CardType == (int)CardType.Gallery1 ? (int)CardType.Gallery1 : (int)CardType.Gallery2;

                        result = await GalleryService.UpdateAsync(galleryData, clientIP, OfficerId, sessionId);
                        //Insert log action
                        if (result == ErrorCodes.Success)
                        {
                            await Function.AddToQueue(ActionName.InsertLog, TopicName.LOGACTION, new LogActionEntity
                            {
                                ObjectId = galleryData.Id.ToString(),
                                Account = accountId.ToString(),
                                CreatedDate = DateTime.SpecifyKind(DateTime.Now, DateTimeKind.Unspecified),
                                Ip = clientIP,
                                Message = "Success.",
                                Title = galleryData.Name,
                                SourceId = OfficerId.ToString(),
                                ActionTypeDetail = (int)ActionType.Update,
                                ActionText = ActionType.Update.ToString(),
                                Type = (int)LogContentType.Gallery
                            });
                        }
                    }
                    else
                    {                        
                        //insert
                        galleryData.Id = Generator.NewsId();                        
                        galleryData.CardType = galleryData.CardType == (int)CardType.Gallery1 ? (int)CardType.Gallery1 : (int)CardType.Gallery2;
                        galleryData.Type = (int)NewsType.Gallery;
                        galleryData.PublishMode = (int)NewsPublishMode.Public;
                        galleryData.Status = (int)NewsStatus.Draft;
                        galleryData.CreatedDate = DateTime.Now;
                        galleryData.CreatedBy = accountId + "";
                        galleryData.DistributorId = galleryData.DistributorId.HasValue ? galleryData.DistributorId : AppSettings.Current.Distribution.Automation.DistributorId;

                        if (long.TryParse(User.FindFirst(ClaimTypes.NameIdentifier)?.Value, out long userId))
                        {
                            galleryData.NewsInAccount = new[] {
                                new NewsInAccount
                                {
                                    AccountId = OfficerId,
                                    NewsId = galleryData.Id,
                                    PublishedDate = DateTime.Now,
                                    PublishedType = (int)NewsPublishedType.Post
                                }
                            };
                            
                            result = await GalleryService.AddAsync(galleryData, clientIP);
                            if (result == ErrorCodes.Success)
                            {
                                await Function.AddToQueue(ActionName.InsertLog, TopicName.LOGACTION, new LogActionEntity
                                {
                                    ObjectId = galleryData.Id.ToString(),
                                    Account = accountId.ToString(),
                                    CreatedDate = DateTime.SpecifyKind(DateTime.Now, DateTimeKind.Unspecified),
                                    Ip = clientIP,
                                    Message = "Success.",
                                    Title = galleryData.Name,
                                    SourceId = OfficerId.ToString(),
                                    ActionTypeDetail = (int)ActionType.Insert,
                                    ActionText = ActionType.Insert.ToString(),
                                    Type = (int)LogContentType.Gallery
                                });
                            }
                        }
                    }
                }
                else
                {
                    return Ok(ActionResponse<string>.CreateErrorResponse("Thông tin tài khoản ko hợp lệ."));
                }

                if (result == ErrorCodes.Success)
                {
                    return Ok(ActionResponse<string>.CreateSuccessResponse(galleryData.Id.ToString(), "Success."));
                }
                else
                {
                    return Ok(ActionResponse<string>.CreateErrorResponse(Current[result]));
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
                return Ok(ActionResponse<string>.CreateErrorResponse(ex.Message));
            }
        }

        /// <summary>
        /// Lưu trữ album
        /// </summary>
        /// <param name="galleryData"></param>
        /// <param name="OfficerId"></param>
        /// <returns></returns>
        /// <response code="401">Authorization has been denied for this request</response>
        /// <response code="404">Not Found</response>
        /// <response code="405">Invalid input</response>
        // POST api/album/save        
        [HttpPost, Route("archive")]
        public async Task<IActionResult> ArchiveAsync([FromForm]GallerySearch galleryData, [FromForm]long OfficerId)
        {
            try
            {
                string sessionId = HttpContext?.Request?.Headers?["session-id"];
                if (string.IsNullOrEmpty(sessionId))
                {
                    return Ok(ActionResponse<string>.CreateErrorResponse("session-id not empty"));
                }
                var clientIP = HttpContext.GetClientIP();

                var returnValue = ErrorCodes.Exception;
                var validateResult = ValidateGallery(galleryData);
                if (validateResult?.Success == false)
                {
                    return Ok(ActionResponse<string>.CreateErrorResponse(validateResult.Message));
                }
                if (!Utility.ValidateStringIds(galleryData.Tags))
                {
                    return Ok(ActionResponse<string>.CreateErrorResponse("Tags không hợp lệ."));
                }

                if (long.TryParse(User.FindFirst(ClaimTypes.NameIdentifier)?.Value, out long accountId))
                {
                    if (galleryData.Id > 0)
                    {
                        //update
                        galleryData.Url = string.Format(AppSettings.Current.ChannelConfiguration.CommonSettings.UrlFormatMediaUnit, Utility.UnicodeToUnsignedAndDash(galleryData.Name ?? ""), galleryData.Id);
                        galleryData.ModifiedDate = DateTime.Now;
                        galleryData.ModifiedBy = accountId + "";
                        galleryData.CardType = galleryData.CardType == (int)CardType.Gallery1 ? (int)CardType.Gallery1 : (int)CardType.Gallery2;

                        returnValue = await GalleryService.UpdateAsync(galleryData, clientIP, OfficerId, sessionId);
                        //Insert log action
                        if (returnValue == ErrorCodes.Success)
                        {
                            await Function.AddToQueue(ActionName.InsertLog, TopicName.LOGACTION, new LogActionEntity
                            {
                                ObjectId = galleryData.Id.ToString(),
                                Account = accountId.ToString(),
                                CreatedDate = DateTime.SpecifyKind(DateTime.Now, DateTimeKind.Unspecified),
                                Ip = clientIP,
                                Message = "Success.",
                                Title = galleryData.Name,
                                SourceId = galleryData.ModifiedBy,
                                ActionTypeDetail = (int)ActionType.Update,
                                ActionText = ActionType.Update.ToString(),
                                Type = (int)LogContentType.Gallery
                            });
                        }
                    }
                    else
                    {                        
                        //insert
                        galleryData.Id = Generator.NewsId();
                        galleryData.Url = string.Format(AppSettings.Current.ChannelConfiguration.CommonSettings.UrlFormatMediaUnit, Utility.UnicodeToUnsignedAndDash(galleryData.Name ?? ""), galleryData.Id);
                        if (string.IsNullOrEmpty(galleryData.OriginalUrl))
                            galleryData.OriginalUrl = galleryData.Url;
                        galleryData.CardType = galleryData.CardType == (int)CardType.Gallery1 ? (int)CardType.Gallery1 : (int)CardType.Gallery2;
                        galleryData.Type = (int)NewsType.Gallery;
                        galleryData.PublishMode = (int)NewsPublishMode.Public;
                        galleryData.Status = (int)NewsStatus.Archive;
                        galleryData.CreatedDate = DateTime.Now;
                        galleryData.CreatedBy = accountId + "";
                        galleryData.DistributorId = galleryData.DistributorId.HasValue ? galleryData.DistributorId : AppSettings.Current.Distribution.Automation.DistributorId;
                        galleryData.DistributionDate = galleryData.DistributionDate.HasValue ? galleryData.DistributionDate : DateTime.Now;

                        galleryData.NewsInAccount = new[] {
                            new NewsInAccount
                            {
                                AccountId  = OfficerId,
                                NewsId = galleryData.Id,
                                PublishedDate = DateTime.Now,
                                PublishedType = (int)NewsPublishedType.Post
                            }
                        };                        

                        returnValue = await GalleryService.AddAsync(galleryData, clientIP);
                        if (returnValue == ErrorCodes.Success)
                        {
                            await Function.AddToQueue(ActionName.InsertLog, TopicName.LOGACTION, new LogActionEntity
                            {
                                ObjectId = galleryData.Id.ToString(),
                                Account = accountId.ToString(),
                                CreatedDate = DateTime.SpecifyKind(DateTime.Now, DateTimeKind.Unspecified),
                                Ip = clientIP,
                                Message = "Success.",
                                Title = galleryData.Name,
                                SourceId = OfficerId.ToString(),
                                ActionTypeDetail = (int)ActionType.Insert,
                                ActionText = ActionType.Insert.ToString(),
                                Type = (int)LogContentType.Gallery
                            });
                        }
                    }
                }
                else
                {
                    returnValue = ErrorCodes.PermissionInvalid;
                }

                if (returnValue == ErrorCodes.Success)
                {
                    return Ok(ActionResponse<object>.CreateSuccessResponse(galleryData.Id.ToString(), "Success."));
                }
                else
                {
                    return Ok(ActionResponse<string>.CreateErrorResponse(Current[returnValue]));
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
                return Ok(ActionResponse<string>.CreateErrorResponse(ex.Message));
            }
        }
        private ActionResponse<string> ValidateGallery(GallerySearch Album)
        {
            try
            {
                if (Album == null)
                {
                    return ActionResponse<string>.CreateErrorResponse("Params not valid.");
                }
                if (!Utility.ValidateStringIds(Album.Tags))
                    return ActionResponse<string>.CreateErrorResponse("Tags không hợp lệ.");
                if (string.IsNullOrEmpty(Album.PublishData) || !Album.PublishData.Contains("items"))
                    return ActionResponse<string>.CreateErrorResponse("Định dạng PublishData không đúng!");

                return ActionResponse<string>.CreateSuccessResponse("Success.");
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
                return ActionResponse<string>.CreateErrorResponse("Params not valid.");
            }
        }

        /// <summary>
        /// Cập nhận trạng thái
        /// </summary>
        /// <param name="id"></param>
        /// <param name="action"></param>
        /// <param name="boardIds"></param>
        /// <returns></returns>
        /// <response code="401">Authorization has been denied for this request</response>
        /// <response code="404">Not Found</response>
        /// <response code="405">Invalid input</response>
        // POST api/article/update_status       
        [HttpPost, Route("update_status")]
        public async Task<IActionResult> UpdateStatusV2Async([FromForm]long id, [FromForm]NewsStatus action, [FromForm]string boardIds)
        {
            try
            {
                string sessionId = HttpContext?.Request?.Headers?["session-id"];
                if (string.IsNullOrEmpty(sessionId))
                {
                    return Ok(ActionResponse<string>.CreateErrorResponse("session-id not empty"));
                }
                var clientIP = HttpContext.GetClientIP();

                var result = ErrorCodes.BusinessError;
                long.TryParse(User.FindFirst(ClaimTypes.NameIdentifier)?.Value, out long userId);
                if (!string.IsNullOrEmpty(boardIds) && !Utility.ValidateStringIds(boardIds))
                {
                    return Ok(ActionResponse<string>.CreateErrorResponse("BoardIds không hợp lệ."));
                }

                result = await GalleryService.UpdateStatusV2Async(id, action, userId, clientIP, sessionId);
                if (result == ErrorCodes.Success)
                {
                    //insert log
                    await Function.AddToQueue(ActionName.InsertLog, TopicName.LOGACTION, new LogActionEntity
                    {
                        ObjectId = id.ToString(),
                        Account = userId.ToString(),
                        CreatedDate = DateTime.SpecifyKind(DateTime.Now, DateTimeKind.Unspecified),
                        Ip = clientIP,
                        Message = "Success.",
                        Title = "",
                        SourceId = userId.ToString(),
                        ActionTypeDetail = (int)ActionType.UpdateStatus,
                        ActionText = ActionType.UpdateStatus.ToString(),
                        Type = (int)LogContentType.Gallery
                    });
                    return Ok(ActionResponse<object>.CreateSuccessResponse(id.ToString(), "success"));
                }
                return Ok(ActionResponse<string>.CreateErrorResponse(Current[result]));
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
                return Ok(ActionResponse<string>.CreateErrorResponse(ex.Message));
            }
        }

        /// <summary>
        /// Lấy thông tin chi tiết album
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        /// <response code="401">Authorization has been denied for this request</response>
        /// <response code="404">Not Found</response>
        /// <response code="405">Invalid input</response>
        // POST api/album/get_by_id
        [HttpPost, Route("get_by_id")]
        public async Task<IActionResult> GetById([FromForm]long Id)
        {
            try
            {
                var result = await GalleryService.GetByIdAsync(Id);
                if (result != null && result.Id > 0) return Ok(ActionResponse<GallerySearch>.CreateSuccessResponse(result));
                return Ok(ActionResponse<string>.CreateErrorResponse("Album not exist"));
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
                return Ok(ActionResponse<string>.CreateErrorResponse(ex.Message));
            }
        }

        /// <summary>
        /// Search album
        /// </summary>
        /// <param name="search"></param>
        /// <returns></returns>
        /// <response code="401">Authorization has been denied for this request</response>
        /// <response code="404">Not Found</response>
        /// <response code="405">Invalid input</response>
        // POST api/album/search        
        [HttpPost, Route("search")]
        public async Task<IActionResult> SearchAsync([FromForm] SearchNews search)
        {
            try
            {
                if (search == null)
                {
                    return Ok(ActionResponse<string>.CreateErrorResponse("Object search null."));
                }

                search.PageIndex = search.PageIndex == null || search.PageIndex < 1 ? 1 : search.PageIndex;
                search.PageSize = search.PageSize == null || search.PageSize < 1 ? 20 : search.PageSize;
                search.FromDate = search.FromDate.HasValue ? search.FromDate.Value.AddHours(0).AddMinutes(0).AddSeconds(0) : DateTime.MinValue;
                search.ToDate = search.ToDate.HasValue ? search.ToDate.Value.AddHours(23).AddMinutes(59).AddSeconds(59) : DateTime.MaxValue;
                long.TryParse(User.FindFirst(ClaimTypes.NameIdentifier)?.Value, out long accountId);

                var result = await GalleryService.SearchAsync(search, accountId);
                if (result != null)
                {
                    return Ok(ActionResponse<PagingDataResult<GallerySearch>>.CreateSuccessResponse(result));
                }

                return Ok(ActionResponse<PagingDataResult<GallerySearch>>.CreateSuccessResponse(new PagingDataResult<GallerySearch>()));
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
                return Ok(ActionResponse<string>.CreateErrorResponse(ex.Message));
            }
        }
    }
}