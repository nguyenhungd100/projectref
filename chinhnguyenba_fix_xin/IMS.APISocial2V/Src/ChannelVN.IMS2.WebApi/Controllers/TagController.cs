﻿using ChannelVN.IMS2.Core.Entities;
using ChannelVN.IMS2.Core.Repositories.Constants;
using ChannelVN.IMS2.Core.Repositories.Functions;
using ChannelVN.IMS2.Core.Services;
using ChannelVN.IMS2.Foundation.Common;
using ChannelVN.IMS2.Foundation.Common.Configuration;
using ChannelVN.IMS2.Foundation.Logging;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace ChannelVN.IMS2.WebApi.Controllers
{
    [Route("api/[controller]")]
    public class TagController : BaseController
    {
        /// <summary>
        /// Add tag
        /// </summary>
        /// <param name="tag"></param>
        /// <param name="officerId"></param>
        /// <returns></returns>
        /// <response code="401">Authorization has been denied for this request</response>
        /// <response code="404">Not Found</response>
        /// <response code="405">Invalid input</response>
        [HttpPost, Route("add")]
        public async Task<IActionResult> Add([FromForm]Tag tag, [FromForm] long officerId)
        {
            try
            {
                var clientIP = string.IsNullOrEmpty(HttpContext?.Connection?.RemoteIpAddress?.ToString()) ? "192.168.1.1" : HttpContext?.Connection?.RemoteIpAddress?.ToString();

                if (tag == null)
                {
                    return Ok(ActionResponse<Tag>.CreateErrorResponse("Tag không tồn tại."));
                }

                if (string.IsNullOrEmpty(tag.Name?.Trim())) return Ok(ActionResponse<Tag>.CreateErrorResponse("Tag name không được để trống."));
                tag.Id = Generator.TagId();
                tag.CreatedDate = DateTime.Now;
                tag.ParentId = tag.ParentId ?? 0;
                if (long.TryParse(User.FindFirst(ClaimTypes.NameIdentifier)?.Value, out long userId))
                {
                    tag.CreatedBy = userId.ToString();
                }
                tag.Status = (int)TagStatus.Actived;
                tag.IsHotTag = tag.IsHotTag ?? false;
                var url = Utility.UnicodeToUnsignedAndDash(tag.Name?.Substring(0, 50));
                tag.UnsignName = url;
                tag.Url = string.Format(AppSettings.Current.ChannelConfiguration.CommonSettings.UrlFormatTag, url);

                var tagDb = await TagService.GetByNameAsync(tag.Name);
                if (tagDb == null || tagDb.Id == 0)
                {
                    var result = await TagService.AddAsync(tag);
                    if (result)
                    {
                        //insert log
                        await Function.AddToQueue(ActionName.InsertLog, TopicName.LOGACTION, new LogActionEntity
                        {
                            ObjectId = tag.Id.ToString(),
                            Account = userId.ToString(),
                            CreatedDate = DateTime.SpecifyKind(DateTime.Now, DateTimeKind.Unspecified),
                            Ip = clientIP,
                            Message = "Success.",
                            Title = tag.Name,
                            SourceId = officerId.ToString(),
                            ActionTypeDetail = (int)ActionType.Insert,
                            ActionText = ActionType.Insert.ToString(),
                            Type = (int)LogContentType.Tag
                        });
                        return Ok(ActionResponse<object>.CreateSuccessResponse(tag.EncryptId, "success"));
                    }
                    return Ok(ActionResponse<string>.CreateErrorResponse("Lỗi hệ thống khi tạo tag."));
                }
                else
                {
                    return Ok(ActionResponse<object>.CreateSuccessResponse(tagDb.EncryptId, "success"));
                }
            }
            catch (Exception ex)
            {
                return Ok(ActionResponse<string>.CreateErrorResponse(ex.Message));
            }

        }

        /// <summary>
        /// Update tag
        /// </summary>
        /// <param name="tag"></param>
        /// <param name="officerId"></param>
        /// <returns></returns>
        /// <response code="401">Authorization has been denied for this request</response>
        /// <response code="404">Not Found</response>
        /// <response code="405">Invalid input</response>
        //POST api/tag/update
        [HttpPost, Route("update")]
        public async Task<IActionResult> Update([FromForm]Tag tag, [FromForm] long officerId)
        {
            try
            {
                var clientIP = string.IsNullOrEmpty(HttpContext?.Connection?.RemoteIpAddress?.ToString()) ? "192.168.1.1" : HttpContext?.Connection?.RemoteIpAddress?.ToString();
                long.TryParse(User.FindFirst(ClaimTypes.NameIdentifier)?.Value, out long userId);
                if (tag == null)
                {
                    return Ok(ActionResponse<Tag>.CreateErrorResponse("Tag không tồn tại."));
                }
                var arrStatus = Enum.GetValues(typeof(TagStatus)) as int[];
                if (tag.Status == null || (tag.Status != null && arrStatus != null && !arrStatus.Contains(tag.Status ?? -1)))
                {
                    return Ok(ActionResponse<string>.CreateErrorResponse("Trạng thái tag không hợp lệ."));
                }
                var url = Utility.UnicodeToUnsignedAndDash(tag.Name);
                tag.UnsignName = url;
                tag.Url = string.Format(AppSettings.Current.ChannelConfiguration.CommonSettings.UrlFormatTag, url);

                var result = await TagService.UpdateAsync(tag);

                if (result)
                {
                    //insert log
                    await Function.AddToQueue(ActionName.InsertLog, TopicName.LOGACTION, new LogActionEntity
                    {
                        ObjectId = tag.Id.ToString(),
                        Account = userId.ToString(),
                        CreatedDate = DateTime.SpecifyKind(DateTime.Now, DateTimeKind.Unspecified),
                        Ip = clientIP,
                        Message = "Success.",
                        Title = tag.Name,
                        SourceId = officerId.ToString(),
                        ActionTypeDetail = (int)ActionType.Update,
                        ActionText = ActionType.Update.ToString(),
                        Type = (int)LogContentType.Tag
                    });
                    return Ok(ActionResponse<object>.CreateSuccessResponse(tag.EncryptId, "success"));
                }
                return Ok(ActionResponse<bool>.CreateErrorResponse("Lỗi cập nhật tag video."));
            }
            catch (Exception ex)
            {
                return Ok(ActionResponse<string>.CreateErrorResponse(ex.Message));
            }

        }

        /// <summary>
        /// Get tag by id
        /// </summary>
        /// <param name="tag"></param>
        /// <returns></returns>
        /// <response code="401">Authorization has been denied for this request</response>
        /// <response code="404">Not Found</response>
        /// <response code="405">Invalid input</response>
        // POST api/tag/get_by_id        
        [HttpPost, Route("get_by_id")]
        public async Task<IActionResult> GetById([FromForm]long id)
        {
            try
            {
                var result = await TagService.GetByIdAsync(id);

                return Ok(ActionResponse<Tag>.CreateSuccessResponse(result));
            }
            catch (Exception ex)
            {
                return Ok(ActionResponse<string>.CreateErrorResponse(ex.Message));
            }

        }

        /// <summary>
        /// Get tag by name
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        /// <response code="401">Authorization has been denied for this request</response>
        /// <response code="404">Not Found</response>
        /// <response code="405">Invalid input</response>
        // POST api/tag/get_by_name        
        [HttpPost, Route("get_by_name")]
        public async Task<IActionResult> GetByName([FromForm]string name)
        {
            try
            {
                var result = await TagService.GetByNameAsync(name);
                return Ok(ActionResponse<Tag>.CreateSuccessResponse(result));
            }
            catch (Exception ex)
            {
                return Ok(ActionResponse<string>.CreateErrorResponse(ex.Message));
            }
        }

        /// <summary>
        /// Search tag
        /// </summary>
        /// <param name="keyword"></param>
        /// <param name="status"></param>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        /// <response code="401">Authorization has been denied for this request</response>
        /// <response code="404">Not Found</response>
        /// <response code="405">Invalid input</response>
        // POST api/tag/search  => getway index 98   
        [HttpPost, Route("search")]
        public async Task<IActionResult> Search([FromForm]string keyword, [FromForm] int? status, [FromForm] int pageIndex, [FromForm] int pageSize)
        {
            try
            {
                pageIndex = pageIndex < 1 ? 1 : pageIndex;
                pageSize = pageSize < 1 ? 10 : pageSize;
                status = status.HasValue ? status : 1;
                var result = await TagService.SearchAsync(keyword, status, pageIndex, pageSize);

                return Ok(ActionResponse<PagingDataResult<Tag>>.CreateSuccessResponse(result));
            }
            catch (Exception ex)
            {
                return Ok(ActionResponse<string>.CreateErrorResponse(ex.Message));
            }

        }
    }
}
