﻿using ChannelVN.IMS2.Core.Entities;
using ChannelVN.IMS2.Core.Entities.Media;
using ChannelVN.IMS2.Core.Repositories.Constants;
using ChannelVN.IMS2.Core.Repositories.Functions;
using ChannelVN.IMS2.Core.Services;
using ChannelVN.IMS2.Foundation.Common;
using ChannelVN.IMS2.Foundation.Logging;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Security.Claims;
using System.Threading.Tasks;

namespace ChannelVN.IMS2.WebApi.Controllers
{
    [Route("api/[controller]")]
    public class TemplateController : BaseController
    {
        /// <summary>
        /// Save template
        /// </summary>
        /// <param name="template"></param>
        /// <param name="officerId"></param>
        /// <returns></returns>
        /// <response code="401">Authorization has been denied for this request</response>
        /// <response code="404">Not Found</response>
        /// <response code="405">Invalid input</response>
        // POST api/template/save        
        [HttpPost, Route("save")]
        public async Task<IActionResult> Save([FromForm]Template template, [FromForm] long officerId)
        {
            try
            {
                var clientIP = string.IsNullOrEmpty(HttpContext?.Connection?.RemoteIpAddress?.ToString()) ? "192.168.1.1" : HttpContext?.Connection?.RemoteIpAddress?.ToString();
                long.TryParse(User.FindFirst(ClaimTypes.NameIdentifier)?.Value, out long accountId);
                if (template == null)
                {
                    return Ok(ActionResponse<string>.CreateErrorResponse("Object null."));
                }

                var result = false;
                if (template.Id > 0)
                {
                    //update
                    template.ModifiedDate = DateTime.Now;
                    template.ModifiedBy = accountId + "";
                    result = await TemplateService.UpdateAsync(template);
                    if (result)
                    {
                        //insert log
                        await Function.AddToQueue(ActionName.InsertLog, TopicName.LOGACTION, new LogActionEntity
                        {
                            ObjectId = template.Id.ToString(),
                            Account = accountId.ToString(),
                            CreatedDate = DateTime.SpecifyKind(DateTime.Now, DateTimeKind.Unspecified),
                            Ip = clientIP,
                            Message = "Success.",
                            Title = template.Title,
                            SourceId = officerId.ToString(),
                            ActionTypeDetail = (int)ActionType.Update,
                            ActionText = ActionType.Update.ToString(),
                            Type = (int)LogContentType.Template
                        });
                    }
                }
                else
                {
                    //insert
                    //template.Id = Generator.TemplateId();
                    template.Status = (int)TemplateStatus.Active;
                    template.CreatedDate = DateTime.Now;
                    template.CreatedBy = accountId + "";

                    result = await TemplateService.AddAsync(template);
                    if (result)
                    {
                        //insert log
                        await Function.AddToQueue(ActionName.InsertLog, TopicName.LOGACTION, new LogActionEntity
                        {
                            ObjectId = template.Id.ToString(),
                            Account = accountId.ToString(),
                            CreatedDate = DateTime.SpecifyKind(DateTime.Now, DateTimeKind.Unspecified),
                            Ip = clientIP,
                            Message = "Success.",
                            Title = template.Title,
                            SourceId = officerId.ToString(),
                            ActionTypeDetail = (int)ActionType.Insert,
                            ActionText = ActionType.Insert.ToString(),
                            Type = (int)LogContentType.Template
                        });
                    }
                }

                if (result)
                    return Ok(ActionResponse<object>.CreateSuccessResponse(template.Id.ToString(), "Success."));

                return Ok(ActionResponse<bool>.CreateErrorResponse("Lỗi tạo khi tạo mẫu."));
            }
            catch (Exception ex)
            {
                return Ok(ActionResponse<string>.CreateErrorResponse(ex.Message));
            }
        }


        /// <summary>
        /// Update trạng thái template
        /// </summary>
        /// <param name="Id"></param>
        /// <param name="Action"></param>
        /// <param name="officerId"></param>
        /// <returns></returns>
        /// <response code="401">Authorization has been denied for this request</response>
        /// <response code="404">Not Found</response>
        /// <response code="405">Invalid input</response>
        // POST api/template/update_status       
        [HttpPost, Route("update_status")]
        public async Task<IActionResult> UpdateStatus([FromForm]int Id, [FromForm]string Action, [FromForm] long officerId)
        {
            try
            {
                var clientIP = string.IsNullOrEmpty(HttpContext?.Connection?.RemoteIpAddress?.ToString()) ? "192.168.1.1" : HttpContext?.Connection?.RemoteIpAddress?.ToString();
                int status;
                if (!string.IsNullOrEmpty(Action) && Enum.IsDefined(typeof(TemplateStatus), Action))
                {
                    status = (int)Enum.Parse(typeof(TemplateStatus), Action);
                }
                else
                {
                    return Ok(ActionResponse<string>.CreateErrorResponse("Trạng thái không hợp lệ."));
                }
                long.TryParse(User.FindFirst(ClaimTypes.NameIdentifier)?.Value, out long userId);

                var result = await TemplateService.UpdateStatusAsync(Id, status, userId);
                if (result)
                {
                    //insert log
                    await Function.AddToQueue(ActionName.InsertLog, TopicName.LOGACTION, new LogActionEntity
                    {
                        ObjectId = Id.ToString(),
                        Account = userId.ToString(),
                        CreatedDate = DateTime.SpecifyKind(DateTime.Now, DateTimeKind.Unspecified),
                        Ip = clientIP,
                        Message = "Success.",
                        Title = null,
                        SourceId = officerId.ToString(),
                        ActionTypeDetail = (int)ActionType.UpdateStatus,
                        ActionText = ActionType.UpdateStatus.ToString(),
                        Type = (int)LogContentType.Template
                    });
                    return Ok(ActionResponse<object>.CreateSuccessResponse(Id.ToString(), "Success."));
                }
                return Ok(ActionResponse<string>.CreateErrorResponse("Error update status."));
            }
            catch (Exception ex)
            {
                return Ok(ActionResponse<string>.CreateErrorResponse(ex.Message));
            }
        }

        /// <summary>
        /// Lấy thông tin chi tiết template
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        /// <response code="401">Authorization has been denied for this request</response>
        /// <response code="404">Not Found</response>
        /// <response code="405">Invalid input</response>
        // POST api/template/get_by_id
        [HttpPost, Route("get_by_id")]
        public async Task<IActionResult> GetById([FromForm]int Id)
        {
            try
            {
                var result = await TemplateService.GetByIdAsync(Id);
                return Ok(ActionResponse<Template>.CreateSuccessResponse(result));
            }
            catch (Exception ex)
            {
                return Ok(ActionResponse<string>.CreateErrorResponse(ex.Message));
            }
        }

        /// <summary>
        /// Tìm kiếm template
        /// </summary>
        /// <param name="Keyword"></param>
        /// <param name="Status"></param>
        /// <param name="PageIndex"></param>
        /// <param name="PageSize"></param>
        /// <param name="CategoryId"></param>
        /// <returns></returns>
        /// <response code="401">Authorization has been denied for this request</response>
        /// <response code="404">Not Found</response>
        /// <response code="405">Invalid input</response>
        // POST api/template/search        
        [HttpPost, Route("search")]
        public async Task<IActionResult> Search([FromForm] string Keyword, [FromForm]int CategoryId, [FromForm] int? Status, [FromForm] int PageIndex, [FromForm] int PageSize)
        {
            try
            {
                PageIndex = PageIndex < 1 ? 1 : PageIndex;
                PageSize = PageSize < 1 ? 20 : PageSize;

                var result = await TemplateService.SearchAsync(Keyword, CategoryId, Status, PageIndex, PageSize);
                if (result != null)
                {
                    return Ok(ActionResponse<PagingDataResult<Template>>.CreateSuccessResponse(result));
                }

                return Ok(ActionResponse<PagingDataResult<Template>>.CreateSuccessResponse(new PagingDataResult<Template>()));
            }
            catch (Exception ex)
            {
                return Ok(ActionResponse<string>.CreateErrorResponse(ex.Message));
            }
        }

        // POST api/template/test     

        [HttpPost, Route("testsearch")]
        [AllowAnonymous]
        public async Task<IActionResult> Test()
        {
            try
            {
                var CategoryId = 1;
                var PageIndex = 1;
                var PageSize = 20;

                var result = await TemplateService.SearchAsync(Uri.EscapeDataString(""), CategoryId, -1, PageIndex, PageSize);
                if (result != null)
                {
                    return Ok(ActionResponse<PagingDataResult<Template>>.CreateSuccessResponse(result));
                }

                return Ok(ActionResponse<PagingDataResult<Template>>.CreateSuccessResponse(new PagingDataResult<Template>()));
            }
            catch (Exception ex)
            {
                return Ok(ActionResponse<string>.CreateErrorResponse(ex.Message));
            }
        }

        // POST api/template/test
        [AllowAnonymous]
        [HttpGet, Route("test_hello")]
        public async Task<IActionResult> TestHell()
        {
            try
            {
                var result = await TemplateService.SearchTestAsync("", null, 1, 20);
                if (result != null)
                {
                    return Ok(ActionResponse<PagingDataResult<Template>>.CreateSuccessResponse(result));
                }

                return Ok(ActionResponse<PagingDataResult<Template>>.CreateSuccessResponse(new PagingDataResult<Template>()));
            }
            catch (Exception ex)
            {
                return Ok(ActionResponse<string>.CreateErrorResponse(ex.Message));
            }
        }

        // POST api/template/test
        [AllowAnonymous]
        [HttpGet, Route("test_hello_nodata")]
        public IActionResult TestHellNoData()
        {
            return Ok(ActionResponse<object>.CreateSuccessResponse("success."));
        }
    }
}
