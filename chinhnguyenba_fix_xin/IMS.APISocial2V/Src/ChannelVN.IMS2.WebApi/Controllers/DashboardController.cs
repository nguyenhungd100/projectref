﻿using System;
using System.Threading.Tasks;
using ChannelVN.IMS2.Core.Entities.DashBoard;
using ChannelVN.IMS2.Core.Services;
using ChannelVN.IMS2.Foundation.Common;
using Microsoft.AspNetCore.Mvc;
using static ChannelVN.IMS2.Core.Entities.ErrorCode.ErrorMapping;

namespace ChannelVN.IMS2.WebApi.Controllers
{

    [Route("api/[controller]")]
    public class DashboardController : BaseController
    {
        /// <summary>
        /// Thống kê số lượng bài theo người dùng
        /// </summary>
        /// <param name="statisticEntity"></param>
        /// <returns></returns>
        /// <response code="401">Authorization has been denied for this request</response>
        /// <response code="404">Not Found</response>
        /// <response code="405">Invalid input</response>
        [HttpPost, Route("get_post_count")]
        public async Task<IActionResult> GetPostCountAsync([FromForm]StatisticQuantity statisticEntity)
        {
            try
            {
                if (statisticEntity != null)
                {
                    statisticEntity.FromDate = statisticEntity.FromDate.HasValue ? DateTime.SpecifyKind(statisticEntity.FromDate.Value, DateTimeKind.Unspecified) : DateTime.SpecifyKind(DateTime.MinValue, DateTimeKind.Unspecified);
                    statisticEntity.ToDate = statisticEntity.ToDate.HasValue ? DateTime.SpecifyKind(statisticEntity.ToDate.Value, DateTimeKind.Unspecified) : DateTime.SpecifyKind(DateTime.Now, DateTimeKind.Unspecified);
                    statisticEntity.FromDate = statisticEntity.FromDate.Value.AddHours(0).AddMinutes(0).AddSeconds(0);
                    statisticEntity.ToDate = statisticEntity.FromDate.Value.AddHours(23).AddMinutes(59).AddSeconds(59);
                    var result = await DashboardService.GetPostCountAsync(statisticEntity);
                    return Ok(ActionResponse<string>.CreateSuccessResponse(result.ToString(), "success"));
                }
                return Ok(ActionResponse<string>.CreateErrorResponse("Input error."));
            }
            catch (Exception ex)
            {
                return Ok(ActionResponse<string>.CreateErrorResponse((int)ErrorCodes.Exception, ex.Message));
            }
        }
    }
}