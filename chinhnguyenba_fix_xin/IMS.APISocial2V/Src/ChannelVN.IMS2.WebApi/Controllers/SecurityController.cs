﻿using ChannelVN.IMS2.Core.Entities;
using ChannelVN.IMS2.Core.Entities.Security;
using ChannelVN.IMS2.Core.Repositories.Constants;
using ChannelVN.IMS2.Core.Repositories.Functions;
using ChannelVN.IMS2.Core.Services;
using ChannelVN.IMS2.Foundation.Common;
using ChannelVN.IMS2.Foundation.Common.Configuration;
using ChannelVN.IMS2.Foundation.Logging;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using static ChannelVN.IMS2.Core.Entities.ErrorCode.ErrorMapping;

namespace ChannelVN.IMS2.WebApi.Controllers
{
    [Route("api/[controller]")]
    public class SecurityController : BaseController
    {        
        /// <summary>
        /// Thêm mới account
        /// </summary>
        /// <param name="account"></param>
        /// <returns></returns>
        /// <response code="401">Authorization has been denied for this request</response>
        /// <response code="404">Not Found</response>
        /// <response code="405">Invalid input</response>
            // POST api/security/account/add        
        [HttpPost, Route("account/add")]
        public async Task<IActionResult> AddAsync([FromForm]Account account)
        {
            try
            {
                //account.Password = Encryption.Md5("12347890");
                if (account == null || account.Id == 0)
                {
                    return Ok(ActionResponse<AccountSearch>.CreateErrorResponse(Current[ErrorCodes.AccountNotExits]));
                }
               
                long.TryParse(User.FindFirst(ClaimTypes.NameIdentifier)?.Value, out long accountId);
                var userCurrent = await SecurityService.GetByIdAsync(accountId);

                Logger.Debug("DataGiangInput=>" + ChannelVN.IMS2.Foundation.Common.Json.Stringify(account));

                var obj = await SecurityService.GetAccountSearchByIdAsync(account.Id);
                if (obj != null && obj.Id > 0)
                {
                    obj.OtpSecretKey = account.OtpSecretKey;
                    obj.FullName = string.IsNullOrEmpty(account.FullName) ? obj.FullName : account.FullName;
                    obj.Email = string.IsNullOrEmpty(account.Email) ? obj.Email : account.Email;
                    obj.Mobile = string.IsNullOrEmpty(account.Mobile) ? obj.Mobile : account.Mobile;

                    await SecurityService.UpdateAccessTokenAsync(obj);

                    return Ok(ActionResponse<object>.CreateSuccessResponse(new { Id = obj.Id.ToString() }, Current[ErrorCodes.Success]));
                }
                else
                {
                    //get kingHub
                    var userInfo = await SecurityService.GetUserInfo(account.Id);

                    if (userInfo == null) return Ok(ActionResponse<AccountSearch>.CreateErrorResponse(Current[ErrorCodes.AccountNotExits]));

                    account.UserName = userInfo.Username?.ToLower();
                    //account.Mobile = userInfo.Phone;
                    //account.Email = userInfo.Email;
                    account.FullName = userInfo.Full_name;


                    account.Password = Encryption.Md5(account.Password ?? "");
                    account.CreatedDate = DateTime.Now;
                    account.Type = (int)AccountType.ReadOnly;
                    account.Class = (int)AccountClass.Normal;
                    account.Status = (int)AccountStatus.Actived;
                    account.VerifiedBy = null;
                    account.VerifiedDate = null;

                    account.IsSystem = (userCurrent != null && (userCurrent.IsSystem == true || userCurrent.IsFullPermission == true)) ? account.IsSystem ?? false : false;
                    account.IsFullPermission = (userCurrent != null && (userCurrent.IsSystem == true || userCurrent.IsFullPermission == true)) ? account.IsFullPermission ?? false : false;

                    var erroCode = await SecurityService.AddAsync(account);
                    if (erroCode == ErrorCodes.Success)
                    {
                        return Ok(ActionResponse<object>.CreateSuccessResponse(new { Id = account.EncryptId }, Current[erroCode]));
                    }
                    else
                    {
                        Logger.Sensitive("Thêm mới tài khoản không thành công.", account);
                    }
                    return Ok(ActionResponse<string>.CreateErrorResponse(Current[erroCode]));
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
                return Ok(ActionResponse<string>.CreateErrorResponse(ex.Message));
            }
        }


        /// <summary>
        /// Thêm mới account
        /// </summary>
        /// <param name="fullName"></param>
        /// <param name="mode"></param>
        /// <param name="commentMode"></param>
        /// <param name="avatar"></param>
        /// <param name="cover"></param>
        /// <param name="crawlerMode"></param>
        /// <param name="crawlerSource"></param>
        /// <param name="labelMode"></param>
        /// <returns></returns>
        /// <response code="401">Authorization has been denied for this request</response>
        /// <response code="404">Not Found</response>
        /// <response code="405">Invalid input</response>
        // POST api/security/account/add        
        [HttpPost, Route("account/add_page")]
        public async Task<IActionResult> AddPageAsync([FromForm]string fullName, [FromForm]string avatar, [FromForm]string cover, [FromForm]CommentMode? commentMode, [FromForm]AccountMode? mode, [FromForm]CrawlerMode? crawlerMode, [FromForm]string crawlerSource, [FromForm]LabelMode? labelMode)
        {
            try
            {
                if(string.IsNullOrEmpty(fullName))
                    return Ok(ActionResponse<string>.CreateErrorResponse("Tên page không được để trống."));

                var sessionid = HttpContext.Request.Headers["sessionId"].ToString();
                long.TryParse(User.FindFirst(ClaimTypes.NameIdentifier)?.Value, out long accountId);

                var currentUser = await SecurityService.GetByIdAsync(accountId);
                if(currentUser!=null && currentUser.Type == (int)AccountType.Personal)
                {
                    var listPage = await SecurityService.GetListPageOwnerAsync(accountId);
                    if(listPage!=null && listPage.Where(p=>p!=null && p.FullName!=null && p.FullName.ToLower().Equals(fullName.ToLower())).Count() > 0)
                    {
                        return Ok(ActionResponse<string>.CreateErrorResponse(Current[ErrorCodes.AddOfficerError]));
                    }
                    var kingHubPage = await SecurityService.CreatedKingHubId(accountId, fullName, avatar, cover, sessionid);

                    Logger.Debug("Thông tin page kinghub=>" + Foundation.Common.Json.Stringify(kingHubPage));

                    if (kingHubPage != null && long.TryParse(kingHubPage.UserID, out long kingHubPageId))
                    {
                        //set role official cho page
                        await Function.AddToQueue(ActionName.ApprovedOnApp, TopicName.SYNC_USERINFO, new QueueData<long, int, object, object>
                        {
                            Data1 = kingHubPageId,
                            Data2 = AppSettings.Current.ChannelConfiguration.ApiUserKingHub.RoleVip
                        });

                        var result = await SecurityService.AddOfficialAsync(new Account()
                        {
                            Id = kingHubPageId,
                            UserName = kingHubPage.Username,
                            FullName = fullName,
                            Avatar = avatar,
                            Banner = cover,
                            Status = currentUser.Class == (int)AccountClass.Waiting && (listPage == null || listPage.Count() ==0) ? (int)AccountStatus.Actived : (int)AccountStatus.UnActived, //AppSettings.Current.ChannelConfiguration.AutoApprovedPage ? (int)AccountStatus.Actived : (int)AccountStatus.UnActived,
                            Type = (int)AccountType.Official,
                            CreatedBy = accountId.ToString(),
                            CreatedDate = DateTime.Now,
                            Mode = (int?)mode,
                            CommentMode = (Byte?)commentMode,
                            CrawlerMode = (Byte?)crawlerMode,
                            CrawlerSource = crawlerSource,
                            Class = currentUser.Class,
                            RelatedId = listPage?.FirstOrDefault()?.Id ?? kingHubPageId
                            //LabelMode = (Byte?)labelMode
                        },
                        new UserProfile()
                        {
                            Id = kingHubPageId,
                            ProfileUrl = kingHubPage.Invite_link// chờ bên kinghub trả về
                        },
                        accountId);

                        if (result == ErrorCodes.Success)
                        {
                            if(currentUser.Class == (int)AccountClass.Waiting)
                            {
                                await Function.AddToQueue(ActionName.SendCrm, TopicName.SYNC_USERINFO, new QueueData<string, Account, List<Account>, object>
                                {
                                    Data1 = currentUser.FullName,
                                    Data2 = currentUser,
                                    Data3 = new List<Account>()
                                    {
                                        new Account()
                                        {
                                            Id = kingHubPageId,
                                            UserName = kingHubPage.Username,
                                            FullName = fullName,
                                            Avatar = avatar,
                                            Banner = cover,
                                            Status = currentUser.Class == (int)AccountClass.Waiting && (listPage == null || listPage.Count() ==0) ? (int)AccountStatus.Actived : (int)AccountStatus.UnActived, 
                                            Type = (int)AccountType.Official,
                                            CreatedBy = accountId.ToString(),
                                            CreatedDate = DateTime.Now,
                                            Mode = (int?)mode,
                                            CommentMode = (Byte?)commentMode,
                                            CrawlerMode = (Byte?)crawlerMode,
                                            CrawlerSource = crawlerSource,
                                            Class = currentUser.Class
                                        }
                                    }
                                });
                            }

                            //await Function.AddToQueue(ActionName.InsertPageOwnerToApp, TopicName.SYNC_USERINFO, new QueueData<long, long, byte?, object>
                            //{
                            //    Data1 = kingHubPageId,
                            //    Data2 = accountId,
                            //    Data3 = currentUser.Class
                            //});

                            await Function.AddToQueue(ActionName.RetryUpdateUserInfoOnApp, TopicName.SYNC_USERINFO, new QueueData<AccountSearch, string, UserProfile, object>
                            {
                                Data1 = new AccountSearch
                                {
                                    Id = kingHubPageId,
                                    UserName = kingHubPage.Username,
                                    FullName = fullName,
                                },
                                Data2 = sessionid,
                                Data3 = null
                            });

                            await Function.AddToQueue(ActionName.SettingCommentOnApp, TopicName.SYNC_USERINFO, new QueueData<long, Byte?, object, object>
                            {
                                Data1 = kingHubPageId,
                                Data2 = (Byte?)commentMode
                            });

                            return Ok(ActionResponse<string>.CreateSuccessResponse(kingHubPageId.ToString(), "pageId:" + kingHubPageId.ToString()));
                        }
                        else
                        {
                            Logger.Error("Lỗi không tạo được page(cms) => userId = " + accountId);
                            return Ok(ActionResponse<string>.CreateErrorResponse(Current[ErrorCodes.AddOfficerError]));
                        }
                    }
                    else
                    {
                        Logger.Sensitive("Tạo page không thành công", new { accountId, fullName, avatar, cover, sessionid });
                        Logger.Error("Lỗi không tạo được page(adtech) => userId = " + accountId);
                        return Ok(ActionResponse<string>.CreateErrorResponse(Current[ErrorCodes.AddOfficerError]));
                    }
                }
                else
                {
                    return Ok(ActionResponse<string>.CreateErrorResponse(Current[ErrorCodes.PermissionInvalid]));
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
                return Ok(ActionResponse<string>.CreateErrorResponse(ex.Message));
            }
        }
        
        /// <summary>
        /// Update account
        /// </summary>
        /// <param name="account"></param>
        /// <param name="profile"></param>
        /// <param name="sessionId"></param>
        /// <returns></returns>
        /// <response code="401">Authorization has been denied for this request</response>
        /// <response code="404">Not Found</response>
        /// <response code="405">Invalid input</response>
        // POST api/account/update        
        [HttpPost, Route("account/update")]
        public async Task<IActionResult> UpdateAsync([FromForm]Account account, [FromForm]UserProfile profile, [FromHeader]string sessionId = null)
        {
            try
            {
                sessionId = HttpContext.Request.Headers["sessionId"].ToString();
                if (account.Mode != null && !Enum.IsDefined(typeof(AccountMode), account.Mode))
                {
                    return Ok(ActionResponse<string>.CreateErrorResponse("Giá trị \"Mode\" không hợp lệ"));
                }

                if (account == null)
                {
                    return Ok(ActionResponse<string>.CreateErrorResponse(Current[ErrorCodes.AccountNotExits]));
                }
                //check quyen du lieu
                long.TryParse(User.FindFirst(ClaimTypes.NameIdentifier)?.Value, out long accountId);

                var accDb = await SecurityService.GetAccountSearchByIdAsync(account.Id);
                if (accDb == null)
                {
                    return Ok(ActionResponse<AccountSearch>.CreateErrorResponse(Current[ErrorCodes.AccountNotExits]));
                }

                var currentAccount = await SecurityService.GetByIdAsync(account.Id);
                if (accountId != account.Id)
                {
                    //var listOwner = await SecurityService.ListOwnerAsync(accountId);

                    var member = await SecurityService.GetAccountMemberByIdAsync(accDb.Id + IndexKeys.SeparateChar + accountId);


                    if (currentAccount == null || member == null || (member.Role != AccountMemberRole.Owner && member.Permissions == null) || (member.Role != AccountMemberRole.Owner && !member.Permissions.Contains(MemberPermission.ConfigPage)))
                    {
                        return Ok(ActionResponse<AccountSearch>.CreateErrorResponse("Bạn không có quyền cập nhật tài khoản này"));
                    }
                }

                accDb = Mapper<AccountSearch>.Map(currentAccount, accDb);
                if (string.IsNullOrEmpty(accDb.UserName))
                {
                    if (string.IsNullOrEmpty(account.UserName))
                    {
                        return Ok(ActionResponse<AccountSearch>.CreateErrorResponse("Username không được để trống."));
                    }
                    var accByUserName = await SecurityService.GetByUsernameAsync(account.UserName);
                    if (accByUserName != null)
                    {
                        return Ok(ActionResponse<AccountSearch>.CreateErrorResponse("Username đã tồn tại."));
                    }
                }

                if (!string.IsNullOrEmpty(accDb.Mobile))
                {
                    account.Mobile = accDb.Mobile.Trim();
                }

                account.UserName = Utility.UnicodeToUnsigned(account.UserName ?? "").ToLower();
                //chi admin moi dc quyen update
                account.IsSystem = (currentAccount.IsSystem == true || currentAccount.IsFullPermission == true) ? account.IsSystem ?? accDb.IsSystem ?? false : accDb.IsSystem ?? false;
                account.IsFullPermission = (currentAccount.IsSystem == true || currentAccount.IsFullPermission == true) ? account.IsFullPermission ?? accDb.IsFullPermission ?? false : accDb.IsFullPermission ?? false;

                var mobileOld = accDb.Mobile;
                var dataUpdate = SetUpdateField(accDb, account);
                var erroCode = await SecurityService.UpdateAsync(dataUpdate, profile, accountId != dataUpdate.Id, mobileOld, sessionId); //accountId != dataUpdate.Id
                if (erroCode == ErrorCodes.Success)
                {
                    return Ok(ActionResponse<object>.CreateSuccessResponse(new { Id = account.EncryptId }, Current[erroCode]));
                }
                return Ok(ActionResponse<string>.CreateErrorResponse(Current[erroCode]));
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
                return Ok(ActionResponse<string>.CreateErrorResponse(ex.Message));
            }
        }

        /// <summary>
        /// Update account type
        /// </summary>
        /// <param name="accountId"></param>
        /// <param name="accountType"></param>
        /// <param name="accountClass"></param>
        /// <returns></returns>
        /// <response code="401">Authorization has been denied for this request</response>
        /// <response code="404">Not Found</response>
        /// <response code="405">Invalid input</response>
        // POST api/security/account/update_type      
        [HttpPost, Route("account/update_type")]
        public async Task<IActionResult> UpdateAccountTypeAsync([FromForm]long accountId, [FromForm] AccountType accountType, [FromForm]AccountClass? accountClass)
        {
            try
            {
                long.TryParse(User.FindFirst(ClaimTypes.NameIdentifier)?.Value, out long userCurrentId);
                var userCurrent = await SecurityService.GetByIdAsync(userCurrentId);
                if (userCurrent.IsSystem == true || userCurrent.IsFullPermission == true)
                {
                    if (accountId == 0)
                    {
                        return Ok(ActionResponse<string>.CreateErrorResponse(Current[ErrorCodes.AccountNotExits]));
                    }

                    var accDb = await SecurityService.GetByIdAsync(accountId);
                    if (accDb == null)
                    {
                        return Ok(ActionResponse<string>.CreateErrorResponse(Current[ErrorCodes.AccountNotExits]));
                    }

                    accDb.Type = (byte)accountType;
                    accDb.Class = (byte?)accountClass ?? (byte?)AccountClass.Waiting;
                    var erroCode = await SecurityService.UpdateAccountTypeAsync(accDb);
                    if (erroCode == ErrorCodes.Success)
                    {
                        if(accountType== AccountType.Personal)
                        {
                            // update role ben app cua member
                            await Function.AddToQueue(ActionName.ApprovedOnApp, TopicName.SYNC_USERINFO, new QueueData<long, int, object, object>
                            {
                                Data1 = accountId,
                                Data2 = AppSettings.Current.ChannelConfiguration.ApiUserKingHub.RoleVip
                            });
                        }
                        else
                        {
                            await Function.AddToQueue(ActionName.ApprovedOnApp, TopicName.SYNC_USERINFO, new QueueData<long, int, object, object>
                            {
                                Data1 = accountId,
                                Data2 = AppSettings.Current.ChannelConfiguration.ApiUserKingHub.RoleNormal
                            });
                        }
                        return Ok(ActionResponse<object>.CreateSuccessResponse(new { Id = accDb.EncryptId }, Current[erroCode]));
                    }
                    return Ok(ActionResponse<string>.CreateErrorResponse(Current[erroCode]));
                }
                else
                {
                    return Ok(ActionResponse<string>.CreateErrorResponse("Bạn không có quyền truy cập....."));
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
                return Ok(ActionResponse<string>.CreateErrorResponse(ex.Message));
            }
        }

       

        /// <summary>
        /// Check account userName
        /// </summary>
        /// <param name="userName"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        /// <response code="401">Authorization has been denied for this request</response>
        /// <response code="404">Not Found</response>
        /// <response code="405">Invalid input</response>
        // POST api/security/account/check_username        
        [HttpPost, Route("account/check_username")]
        public async Task<IActionResult> CheckUserNameAsync([FromForm] string userName, [FromForm] long userId)
        {
            try
            {
                if (string.IsNullOrEmpty(userName))
                {
                    return Ok(ActionResponse<string>.CreateErrorResponse("Username không được để trống."));
                }
                var accByUserName = await SecurityService.GetByUsernameAsync(userName);
                if (accByUserName != null)
                {
                    return Ok(ActionResponse<string>.CreateErrorResponse("Username đã tồn tại."));
                }
                return Ok(ActionResponse<string>.CreateSuccessResponse("Username hợp lệ."));
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
                return Ok(ActionResponse<string>.CreateErrorResponse(ex.Message));
            }
        }


        /// <summary>
        /// Check Page hợp lệ
        /// </summary>
        /// <param name="officerId"></param>
        /// <returns></returns>
        /// <response code="401">Authorization has been denied for this request</response>
        /// <response code="404">Not Found</response>
        /// <response code="405">Invalid input</response>
        // POST api/security/account/check_username        
        [HttpPost, Route("account/check_page_valid")]
        public async Task<IActionResult> CheckPageValidAsync([FromForm] long officerId)
        {
            try
            {
                var userInfo = await SecurityService.GetUserInfo(officerId);
                if (userInfo == null || string.IsNullOrEmpty(userInfo.Full_name) || userInfo.Full_name.Contains("@viva.vn"))
                {
                    return Ok(ActionResponse<DataResponse>.CreateErrorResponse((int)ErrorCodes.DataInvalid, userInfo, "Fullname không hợp lệ."));
                }
                else
                {
                    return Ok(ActionResponse<DataResponse>.CreateSuccessResponse(userInfo, "Page hợp lệ."));
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
                return Ok(ActionResponse<string>.CreateErrorResponse(ex.Message));
            }
        }

        /// <summary>
        /// Lấy thông tin chi tiết account
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        /// <response code="401">Authorization has been denied for this request</response>
        /// <response code="404">Not Found</response>
        /// <response code="405">Invalid input</response>
        // POST api/security/account/get_by_id        
        [HttpPost, Route("account/get_by_id")]
        public async Task<IActionResult> GetByIdAsync([FromForm]long id)
        {
            try
            {
                var result = await SecurityService.GetSimpleAccountAsync(id);
                if (result != null)
                {
                    return Ok(ActionResponse<AccountSimple>.CreateSuccessResponse(result));
                }
                else
                {
                    return Ok(ActionResponse<string>.CreateErrorResponse("Tài khoản không tồn tại."));
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
                return Ok(ActionResponse<string>.CreateErrorResponse(ex.Message));
            }
        }
        
        /// <summary>
        /// Danh sách member trong officer
        /// </summary>
        /// <param name="officerId"></param>
        /// <returns></returns>
        /// <response code="401">Authorization has been denied for this request</response>
        /// <response code="404">Not Found</response>
        /// <response code="405">Invalid input</response>
        // POST api/security/account/get_by_id        
        [HttpPost, Route("account/get_members")]
        public async Task<IActionResult> GetMembers([FromForm]long officerId)
        {
            try
            {
                long.TryParse(User.FindFirst(ClaimTypes.NameIdentifier)?.Value, out long userCurrentId);
                var accCurrent = await SecurityService.GetByIdAsync(userCurrentId);

                var result = await SecurityService.GetMembersAsync(officerId);
                if (result != null && result.Count() > 0)
                {
                    return Ok(ActionResponse<List<AccountMemberInfo>>.CreateSuccessResponse(result?.Where(p=>p.Role != AccountMemberRole.Owner)?.ToList()));
                }
                else
                {
                    return Ok(ActionResponse<string>.CreateErrorResponse("Tài khoản không tồn tại."));
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
                return Ok(ActionResponse<string>.CreateErrorResponse(ex.Message));
            }
        }
        
        /// <summary>
        /// Chi tiết user profile
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        /// <response code="401">Authorization has been denied for this request</response>
        /// <response code="404">Not Found</response>
        /// <response code="405">Invalid input</response>
        // POST api/security/account/get_profile        
        [HttpPost, Route("account/get_profile")]
        public async Task<IActionResult> GetProfileAsync([FromForm]long id)
        {
            try
            {
                long.TryParse(User.FindFirst(ClaimTypes.NameIdentifier)?.Value, out long userCurrentId);
                if (userCurrentId == id)
                {
                    var result = await SecurityService.GetProfileAsync(id);

                    return Ok(ActionResponse<UserProfile>.CreateSuccessResponse(result));
                }
                return Ok(ActionResponse<string>.CreateSuccessResponse("Bạn không có quyền truy cập trang này."));
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
                return Ok(ActionResponse<string>.CreateErrorResponse(ex.Message));
            }
        }

        /// <summary>
        /// Chi tiết account (public)
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        /// <response code="401">Authorization has been denied for this request</response>
        /// <response code="404">Not Found</response>
        /// <response code="405">Invalid input</response>
        // POST api/security/account/public/get_by_id    
        //dùng cho tường
        [HttpPost, Route("account/public/get_by_id")]
        public async Task<IActionResult> Public_GetByIdAsync([FromForm]long id)
        {
            try
            {
                var result = await SecurityService.GetByIdAsync(id);
                if (result != null)
                {
                    var accSimple = new AccountSimple
                    {
                        Id = result.EncryptId,
                        Avatar = result.Avatar,
                        Type = result.Type,
                        Banner = result.Banner,
                        Class = result.Class,
                        Email = result.Email,
                        FullName = result.FullName,
                        Mobile = result.Mobile,
                        Status = result.Status,
                        UserName = result.UserName
                    };
                    return Ok(ActionResponse<AccountSimple>.CreateSuccessResponse(accSimple));
                }
                else
                {
                    return Ok(ActionResponse<string>.CreateErrorResponse("Tài khoản không tồn tại"));
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
                return Ok(ActionResponse<string>.CreateErrorResponse(ex.Message));
            }
        }

        /// <summary>
        /// Tìm kiếm account
        /// </summary>
        /// <param name="search"></param>
        /// <returns></returns>
        /// <response code="401">Authorization has been denied for this request</response>
        /// <response code="404">Not Found</response>
        /// <response code="405">Invalid input</response>
        // POST api/security/account/search      
        [HttpPost, Route("account/search")]
        public async Task<IActionResult> SearchAsync([FromForm]SearchAccountEntity search)
        {
            try
            {
                search.PageIndex = search.PageIndex == null || search.PageIndex < 1 ? 1 : search.PageIndex;
                search.PageSize = search.PageSize == null || search.PageSize < 1 ? 10 : search.PageSize;
                search.Role = null;
                var result = await SecurityService.SearchAsync(search);
                var returnValue = new PagingDataResult<AccountSimple>();
                if (result != null && result.Data != null)
                {
                    returnValue.Total = result.Total;
                    returnValue.Data = result.Data?.Select(p => new AccountSimple()
                    {
                        Id = p.EncryptId,
                        Avatar = p.Avatar,
                        Banner = p.Banner,
                        Class = p.Class,
                        Email = p.Email,
                        FullName = p.FullName,
                        Mobile = p.Mobile,
                        Status = p.Status,
                        Type = p.Type,
                        UserName = p.UserName,
                        CreatedBy = p.CreatedBy
                    })?.ToList();
                }
                return Ok(ActionResponse<PagingDataResult<AccountSimple>>.CreateSuccessResponse(returnValue));
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
                return Ok(ActionResponse<string>.CreateErrorResponse(ex.Message));
            }
        }

        /// <summary>
        /// Tìm kiếm account để invite
        /// </summary>
        /// <param name="search"></param>
        /// <returns></returns>
        /// <response code="401">Authorization has been denied for this request</response>
        /// <response code="404">Not Found</response>
        /// <response code="405">Invalid input</response>
        // POST api/security/account/search      
        [HttpPost, Route("account/search_invite")]
        public async Task<IActionResult> SearchInviteAsync([FromForm]SearchAccountEntity search)
        {
            try
            {
                var page = search.PageIndex == null || search.PageIndex < 1 ? 1 : search.PageIndex;
                var limit = search.PageSize == null || search.PageSize < 1 ? 10 : search.PageSize;

                var result = await SecurityService.SearchInviteAsync(search.Keyword, page??1, limit??10);
                var returnValue = default(object[]);
                if (result != null && result.Count() > 0)
                {
                    returnValue = result.Select(p => new
                    {
                        Id = p.User_id,
                        Avatar = p.Avatar,
                        BirthDay =  DateTime.TryParseExact(p.Birthday, AppSettings.Current.ChannelConfiguration.BirthdayFormat, CultureInfo.InvariantCulture,DateTimeStyles.None, out DateTime birthday) ? birthday : default(DateTime?),
                        Email = p.Email,
                        FullName = p.Full_name,
                        Mobile = p.Phone
                    })?.ToArray();
                }
                return Ok(ActionResponse<object[]>.CreateSuccessResponse(returnValue));
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
                return Ok(ActionResponse<string>.CreateErrorResponse(ex.Message));
            }
        }


        /// <summary>
        /// Deactive page
        /// </summary>
        /// <param name="officerId"></param>
        /// <returns></returns>
        /// <response code="401">Authorization has been denied for this request</response>
        /// <response code="404">Not Found</response>
        /// <response code="405">Invalid input</response>
        // POST api/security/account/update_status     
        [HttpPost, Route("deactive_page")]
        public async Task<IActionResult> DeactivePageAsync([FromForm]long officerId)
        {
            try
            {
                long.TryParse(User.FindFirst(ClaimTypes.NameIdentifier)?.Value, out long accountId);
                var userCurrent = await SecurityService.GetByIdAsync(accountId);

                var erroCode = await SecurityService.UpdateStatusAsync(officerId, userCurrent, AccountStatus.UnActived);

                if (erroCode == ErrorCodes.Success)
                {
                    var page = await SecurityService.GetByIdAsync(officerId);
                    if (page != null)
                    {
                        if(long.TryParse(page.CreatedBy, out long ownerId))
                        {
                            var owner = await SecurityService.GetByIdAsync(ownerId);
                            if (owner != null)
                            {
                                var listPageOwner = await SecurityService.GetListPageOwnerAsync(owner.Id);
                                await Function.AddToQueue(ActionName.SendCrm, TopicName.SYNC_USERINFO, new QueueData<string, Account, List<Account>, object>
                                {
                                    Data1 = userCurrent.FullName,
                                    Data2 = owner,
                                    Data3 = listPageOwner?.Select(p=>Mapper<Account>.Map(p, new Account()))?.ToList()
                                });
                            }
                        }
                    }
                    

                    return Ok(ActionResponse<object>.CreateSuccessResponse(new { OfficerId = officerId.ToString() }, Current[erroCode]));
                }
                else
                {
                    return Ok(ActionResponse<string>.CreateErrorResponse(Current[erroCode]));
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
                return Ok(ActionResponse<string>.CreateErrorResponse(ex.Message));
            }
        }

        /// <summary>
        /// Active page
        /// </summary>
        /// <param name="officerId"></param>
        /// <param name="accountClass"></param>
        /// <returns></returns>
        /// <response code="401">Authorization has been denied for this request</response>
        /// <response code="404">Not Found</response>
        /// <response code="405">Invalid input</response>
        // POST api/security/account/update_status     
        [HttpPost, Route("active_page")]
        public async Task<IActionResult> ActiveAsync([FromForm]long officerId,[FromForm]AccountClass? accountClass)
        {
            try
            {
                long.TryParse(User.FindFirst(ClaimTypes.NameIdentifier)?.Value, out long accountId);
                var userCurrent = await SecurityService.GetByIdAsync(accountId);

                if (userCurrent != null)
                {
                    var erroCode = await SecurityService.UpdateStatusAsync(officerId, userCurrent, AccountStatus.Actived, accountClass);
                    
                    if (erroCode == ErrorCodes.Success)
                    {
                        var page = await SecurityService.GetByIdAsync(officerId);
                        if (page != null)
                        {
                            await Function.AddToQueue(ActionName.UpdatePageTypeToApp, TopicName.SYNC_USERINFO, new QueueData<long, long, byte?, object>
                            {
                                Data1 = officerId,
                                Data2 = accountId,
                                Data3 = page.Class
                            });

                            if (long.TryParse(page.CreatedBy, out long ownerId))
                            {
                                var owner = await SecurityService.GetByIdAsync(ownerId);
                                if (owner != null)
                                {
                                    var listPageOwner = await SecurityService.GetListPageOwnerAsync(owner.Id);
                                    await Function.AddToQueue(ActionName.SendCrm, TopicName.SYNC_USERINFO, new QueueData<string, Account, List<Account>, object>
                                    {
                                        Data1 = userCurrent.FullName,
                                        Data2 = owner,
                                        Data3 = listPageOwner?.Select(p => Mapper<Account>.Map(p, new Account()))?.ToList()
                                    });
                                }
                            }
                        }

                        return Ok(ActionResponse<object>.CreateSuccessResponse(new { OfficerId = officerId.ToString() }, Current[erroCode]));
                    }
                    else
                    {
                        return Ok(ActionResponse<string>.CreateErrorResponse(Current[erroCode]));
                    }
                }
                else
                {
                    return Ok(ActionResponse<string>.CreateErrorResponse("Bạn không có quyền."));
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
                return Ok(ActionResponse<string>.CreateErrorResponse(ex.Message));
            }
        }

        /// <summary>
        /// Update label type
        /// </summary>
        /// <param name="officerId"></param>
        /// <param name="labelType"></param>
        /// <returns></returns>
        /// <response code="401">Authorization has been denied for this request</response>
        /// <response code="404">Not Found</response>
        /// <response code="405">Invalid input</response>
        // POST api/security/account/update_status     
        [HttpPost, Route("update_label_type")]
        public async Task<IActionResult> ConfigLabelTypeAsync([FromForm]long officerId, [FromForm]int? labelType)
        {
            try
            {
                long.TryParse(User.FindFirst(ClaimTypes.NameIdentifier)?.Value, out long accountId);
                var userCurrent = await SecurityService.GetByIdAsync(accountId);

                if (userCurrent != null)
                {
                    var erroCode = await SecurityService.ConfigLabelTypeAsync(officerId, userCurrent, labelType);

                    if (erroCode == ErrorCodes.Success)
                    {
                        return Ok(ActionResponse<object>.CreateSuccessResponse(new { OfficerId = officerId.ToString() }, Current[erroCode]));
                    }
                    else
                    {
                        return Ok(ActionResponse<string>.CreateErrorResponse(Current[erroCode]));
                    }
                }
                else
                {
                    return Ok(ActionResponse<string>.CreateErrorResponse("Bạn không có quyền."));
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
                return Ok(ActionResponse<string>.CreateErrorResponse(ex.Message));
            }
        }

        #region AccountMember
        /// <summary>
        /// Thêm mới account member
        /// </summary>
        /// <param name="accountMember"></param>
        /// <param name="account"></param>
        /// <returns></returns>
        /// <response code="401">Authorization has been denied for this request</response>
        /// <response code="404">Not Found</response>
        /// <response code="405">Invalid input</response>
        // POST api/security/accountmember/add       
        [HttpPost, Route("accountmember/add")]
        public async Task<IActionResult> AddAccountMemberAsync([FromForm]AccountMember accountMember,[FromForm]Account account)
        {
            try
            {
                if (accountMember == null)
                {
                    return Ok(ActionResponse<string>.CreateErrorResponse(Current[ErrorCodes.AccountNotExits]));
                }

                if(accountMember.Role == AccountMemberRole.Owner)
                {
                    return Ok(ActionResponse<string>.CreateErrorResponse(Current[ErrorCodes.DataInvalid]));
                }

                long.TryParse(User.FindFirst(ClaimTypes.NameIdentifier)?.Value, out long accountId);
                //Kiểm tra account tổ chức ????
                if (!(accountMember.OfficerId > 0)) return Ok(ActionResponse<string>.CreateErrorResponse("Thông tin tổ chức không đúng."));
                if (!(accountMember.MemberId > 0)) return Ok(ActionResponse<string>.CreateErrorResponse("Thông tin thành viên không đúng."));

                var officerAcc = await SecurityService.GetAccountSearchByIdAsync(accountMember.OfficerId);
                if (officerAcc == null || !(officerAcc.Id > 0))
                {
                    return Ok(ActionResponse<string>.CreateErrorResponse(Current[ErrorCodes.AccountNotExits]));
                }

                if (officerAcc.AccountMember?.Where(p => p.MemberId == accountMember.MemberId).Count() > 0)
                {
                    return Ok(ActionResponse<string>.CreateErrorResponse(Current[ErrorCodes.AccountExits]));
                }

                //Check quyen
                var currentMember = officerAcc.AccountMember?.Where(p => p.MemberId == accountId)?.FirstOrDefault();
                if (currentMember.IsLocked == true || currentMember?.Permissions == null || !currentMember.Permissions.Contains(MemberPermission.ManagerMember))
                {
                    return Ok(ActionResponse<string>.CreateErrorResponse("Bạn không có quyền thêm thành viên."));
                }

                if (currentMember.Level > accountMember.Level)
                {
                    return Ok(ActionResponse<string>.CreateErrorResponse("Bạn không có quyền thêm thành viên cấp cao hơn."));
                }

                accountMember.JoinedDate = DateTime.Now;
                accountMember.Role = accountMember.Role.HasValue ? accountMember.Role : AccountMemberRole.Poster;

                officerAcc.AccountMember = officerAcc.AccountMember == null ? new AccountMember[] { accountMember } : officerAcc.AccountMember.Append(accountMember).ToArray();

                var accountMemberDb = await SecurityService.GetByIdAsync(accountMember.MemberId);
                if(accountMemberDb == null || accountMemberDb.Id == 0)
                {
                    var resultAdd =  await SecurityService.AddAsync(new Account
                    {
                        Id = accountMember.MemberId,
                        FullName = account?.FullName,
                        UserName = account?.UserName,
                        Mobile = account?.Mobile,
                        Avatar = account?.Avatar,
                        Email = account?.Email,
                        Status = (int)AccountStatus.Actived,
                        Type = (int)AccountType.ReadOnly
                    });

                    if(resultAdd == ErrorCodes.Success)
                    {
                        var erroCode = await SecurityService.AddAccountMemberAsync(accountMember, officerAcc);
                        if (erroCode == ErrorCodes.Success)
                        {
                            // update lại role bên app của member để member có thể login app mà không cần invite code
                            await Function.AddToQueue(ActionName.ApprovedOnApp, TopicName.SYNC_USERINFO, new QueueData<long, int, object, object>
                            {
                                Data1 = accountMember.MemberId,
                                Data2 = AppSettings.Current.ChannelConfiguration.ApiUserKingHub.RoleVip
                            });

                            return Ok(ActionResponse<object>.CreateSuccessResponse(new { OfficerId = accountMember.EncryptOfficerId, MemberId = accountMember.EncryptMemberId }, Current[erroCode]));
                        }
                        return Ok(ActionResponse<string>.CreateErrorResponse(Current[erroCode]));
                    }
                    else
                    {
                        return Ok(ActionResponse<string>.CreateErrorResponse(Current[resultAdd]));
                    }
                }
                else
                {
                    var erroCode = await SecurityService.AddAccountMemberAsync(accountMember, officerAcc);
                    if (erroCode == ErrorCodes.Success)
                    {
                        // update lại role bên app của member để member có thể login app mà không cần invite code
                        await Function.AddToQueue(ActionName.ApprovedOnApp, TopicName.SYNC_USERINFO, new QueueData<long, int, object, object>
                        {
                            Data1 = accountMember.MemberId,
                            Data2 = AppSettings.Current.ChannelConfiguration.ApiUserKingHub.RoleVip
                        });

                        return Ok(ActionResponse<object>.CreateSuccessResponse(new { OfficerId = accountMember.EncryptOfficerId, MemberId = accountMember.EncryptMemberId }, Current[erroCode]));
                    }
                    return Ok(ActionResponse<string>.CreateErrorResponse(Current[erroCode]));
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
                return Ok(ActionResponse<string>.CreateErrorResponse(ex.Message));
            }
        }

        /// <summary>
        /// Update account member
        /// </summary>
        /// <param name="accountMember"></param>
        /// <returns></returns>
        /// <response code="401">Authorization has been denied for this request</response>
        /// <response code="404">Not Found</response>
        /// <response code="405">Invalid input</response>
        // POST api/security/accountmember/update        
        [HttpPost, Route("accountmember/update")]
        public async Task<IActionResult> UpdateAccountMemberAsync([FromForm]AccountMember accountMember)
        {
            try
            {
                long.TryParse(User.FindFirst(ClaimTypes.NameIdentifier)?.Value, out long accountId);

                if (accountMember == null || accountMember.OfficerId == 0 || accountMember.MemberId == 0)
                {
                    return Ok(ActionResponse<string>.CreateErrorResponse(Current[ErrorCodes.AccountNotExits]));
                }

                if(accountMember.Role == null) return Ok(ActionResponse<string>.CreateErrorResponse("Bạn chưa chọn vai trò cho thành viên."));

                var officialAccount = await SecurityService.GetAccountSearchByIdAsync(accountMember.OfficerId);

                var currentMeber = officialAccount?.AccountMember?.Where(p=>p.MemberId == accountId).FirstOrDefault();
                var member = officialAccount?.AccountMember?.Where(p => p.MemberId == accountMember.MemberId).FirstOrDefault();



                if (currentMeber == null || member == null || accountMember.Role == AccountMemberRole.Owner)
                {
                    return Ok(ActionResponse<string>.CreateErrorResponse(Current[ErrorCodes.DataInvalid]));
                }

                if (member.Role == AccountMemberRole.Owner)
                {
                    return Ok(ActionResponse<string>.CreateErrorResponse("Bạn không được update quyền tài khoản owner."));
                }

                if (currentMeber.Level >= member.Level)
                {
                    return Ok(ActionResponse<string>.CreateErrorResponse("Bạn không có quyền chỉnh sửa thành viên cấp cao hơn."));
                }

                if (currentMeber.IsLocked == false && currentMeber.Permissions != null && currentMeber.Permissions.Contains(MemberPermission.ManagerMember))
                {
                    member.Permissions = accountMember.Permissions;
                    member.Role = accountMember.Role;

                    var erroCode = await SecurityService.UpdateAccountMemberAsync(member);
                    if (erroCode == ErrorCodes.Success)
                        return Ok(ActionResponse<object>.CreateSuccessResponse(new { OfficerId = accountMember.EncryptOfficerId, MemberId = accountMember.EncryptMemberId }, Current[erroCode]));
                    return Ok(ActionResponse<string>.CreateErrorResponse(Current[erroCode]));
                }
                else
                {
                    return Ok(ActionResponse<string>.CreateErrorResponse("Bạn không được update quyền tài khoản."));
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
                return Ok(ActionResponse<string>.CreateErrorResponse(ex.Message));
            }
        }

        /// <summary>
        /// Remove account member
        /// </summary>
        /// <param name="officerId"></param>
        /// <param name="memberId"></param>
        /// <returns></returns>
        /// <response code="401">Authorization has been denied for this request</response>
        /// <response code="404">Not Found</response>
        /// <response code="405">Invalid input</response>
        // POST api/security/accountmember/remove_member       
        [HttpPost, Route("accountmember/remove_member")]
        public async Task<IActionResult> RemoveMemberAsync([FromForm]long officerId, [FromForm]long memberId)
        {
            try
            {
                long.TryParse(User.FindFirst(ClaimTypes.NameIdentifier)?.Value, out long accountId);
               
                if(officerId > 0)
                {
                    var listOwner = await SecurityService.ListOwnerAsync(accountId);
                    var listMember = await SecurityService.GetMembersAsync(officerId);

                    var currentMember = listMember?.Where(p => p != null && p.MemberId == accountId).FirstOrDefault();
                    var member = listMember?.Where(p => p != null && p.MemberId == memberId).FirstOrDefault();

                    if (currentMember.IsLocked && currentMember == null || !currentMember.Permissions.Contains(MemberPermission.ManagerMember) || currentMember.Role == member?.Role || member.Role == AccountMemberRole.Owner)
                    {
                        return Ok(ActionResponse<string>.CreateErrorResponse("Bạn không có quyền xóa thành viên."));
                    }

                    if (currentMember.Level >= member.Level)
                    {
                        return Ok(ActionResponse<string>.CreateErrorResponse("Bạn không được xóa tài khoản này."));
                    }

                    if (listMember != null && listMember.Where(p => p.MemberId == memberId)?.Count() > 0)
                    {
                        var returnValue = await SecurityService.RemoveMemberAsync(officerId, memberId);
                        if (returnValue)
                        {
                            // update lại role bên app của member.
                            //await Function.AddToQueue(ActionName.ApprovedOnApp, TopicName.SYNC_USERINFO, new QueueData<long, int, object, object>
                            //{
                            //    Data1 = memberId,
                            //    Data2 = AppSettings.Current.ChannelConfiguration.ApiUserKingHub.RoleNormal
                            //});

                            return Ok(ActionResponse<string>.CreateSuccessResponse("Xóa thành công."));
                        }
                        else
                        {
                            return Ok(ActionResponse<string>.CreateErrorResponse("Xóa thất bại."));
                        }
                    }
                    else
                    {
                        return Ok(ActionResponse<string>.CreateErrorResponse("Tài khoản member không tồn tại."));
                    }
                }
                else
                {
                    var returnValue = await SecurityService.RemoveMemberOnAllPageAsync(accountId, memberId);
                    if (returnValue == ErrorCodes.Success)
                    {
                        // update lại role bên app của member.
                        //await Function.AddToQueue(ActionName.ApprovedOnApp, TopicName.SYNC_USERINFO, new QueueData<long, int, object, object>
                        //{
                        //    Data1 = memberId,
                        //    Data2 = AppSettings.Current.ChannelConfiguration.ApiUserKingHub.RoleNormal
                        //});

                        return Ok(ActionResponse<string>.CreateSuccessResponse("Xóa thành công."));
                    }
                    else
                    {
                        return Ok(ActionResponse<string>.CreateErrorResponse(Current[returnValue]));
                    }
                } 
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
                return Ok(ActionResponse<string>.CreateErrorResponse(ex.Message));
            }
        }

        /// <summary>
        /// Khóa account member
        /// </summary>
        /// <param name="officerId"></param>
        /// <param name="memberId"></param>
        /// <returns></returns>
        /// <response code="401">Authorization has been denied for this request</response>
        /// <response code="404">Not Found</response>
        /// <response code="405">Invalid input</response>
        // POST api/security/accountmember/remove_member       
        [HttpPost, Route("accountmember/lock_member")]
        public async Task<IActionResult> LockMemberAsync([FromForm]long officerId, [FromForm]long memberId)
        {
            try
            {
                long.TryParse(User.FindFirst(ClaimTypes.NameIdentifier)?.Value, out long accountId);

                if (officerId > 0)
                {
                    var listOwner = await SecurityService.ListOwnerAsync(accountId);
                    var listMember = await SecurityService.GetMembersAsync(officerId);

                    var currentMember = listMember?.Where(p => p != null && p.MemberId == accountId).FirstOrDefault();
                    var member = listMember?.Where(p => p != null && p.MemberId == memberId).FirstOrDefault();

                    if (currentMember.IsLocked || currentMember == null || !currentMember.Permissions.Contains(MemberPermission.ManagerMember) || member.Role == AccountMemberRole.Owner
                        || currentMember.Level >= member.Level)
                    {
                        return Ok(ActionResponse<string>.CreateErrorResponse("Bạn không có quyền khóa thành viên này."));
                    }

                    if (listMember != null && listMember.Where(p => p.MemberId == memberId)?.Count() > 0)
                    {
                        var returnValue = await SecurityService.LockMemberAsync(officerId, memberId);
                        if (returnValue)
                        {
                            // update lại role bên app của member.
                            //await Function.AddToQueue(ActionName.ApprovedOnApp, TopicName.SYNC_USERINFO, new QueueData<long, int, object, object>
                            //{
                            //    Data1 = memberId,
                            //    Data2 = AppSettings.Current.ChannelConfiguration.ApiUserKingHub.RoleNormal
                            //});

                            return Ok(ActionResponse<string>.CreateSuccessResponse("Khóa thành công."));
                        }
                        else
                        {
                            return Ok(ActionResponse<string>.CreateErrorResponse("Khóa thất bại."));
                        }
                    }
                    else
                    {
                        return Ok(ActionResponse<string>.CreateErrorResponse("Tài khoản member không tồn tại."));
                    }
                }
                else
                {
                    var returnValue = await SecurityService.LockOrUnlockMemberOnAllPageAsync(accountId, memberId, 1);
                    if (returnValue == ErrorCodes.Success)
                    {
                        // update lại role bên app của member.
                        //await Function.AddToQueue(ActionName.ApprovedOnApp, TopicName.SYNC_USERINFO, new QueueData<long, int, object, object>
                        //{
                        //    Data1 = memberId,
                        //    Data2 = AppSettings.Current.ChannelConfiguration.ApiUserKingHub.RoleNormal
                        //});

                        return Ok(ActionResponse<string>.CreateSuccessResponse("Khóa thành công."));
                    }
                    else
                    {
                        return Ok(ActionResponse<string>.CreateErrorResponse(Current[returnValue]));
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
                return Ok(ActionResponse<string>.CreateErrorResponse(ex.Message));
            }
        }

        /// <summary>
        /// Mở khóa account member
        /// </summary>
        /// <param name="officerId"></param>
        /// <param name="memberId"></param>
        /// <returns></returns>
        /// <response code="401">Authorization has been denied for this request</response>
        /// <response code="404">Not Found</response>
        /// <response code="405">Invalid input</response>
        // POST api/security/accountmember/remove_member       
        [HttpPost, Route("accountmember/unlock_member")]
        public async Task<IActionResult> UnLockMemberAsync([FromForm]long officerId, [FromForm]long memberId)
        {
            try
            {
                long.TryParse(User.FindFirst(ClaimTypes.NameIdentifier)?.Value, out long accountId);

                if (officerId > 0)
                {
                    var listOwner = await SecurityService.ListOwnerAsync(accountId);
                    var listMember = await SecurityService.GetMembersAsync(officerId);

                    var currentMember = listMember?.Where(p => p != null && p.MemberId == accountId).FirstOrDefault();
                    var member = listMember?.Where(p => p != null && p.MemberId == memberId).FirstOrDefault();

                    if (currentMember.IsLocked || currentMember == null || !currentMember.Permissions.Contains(MemberPermission.ManagerMember) || member.Role == AccountMemberRole.Owner
                        || currentMember.Level >= member.Level)
                    {
                        return Ok(ActionResponse<string>.CreateErrorResponse("Bạn không có quyền mở khóa thành viên này."));
                    }

                    if (listMember != null && listMember.Where(p => p.MemberId == memberId)?.Count() > 0)
                    {
                        var returnValue = await SecurityService.UnLockMemberAsync(officerId, memberId);
                        if (returnValue)
                        {
                            // update lại role bên app của member.
                            //await Function.AddToQueue(ActionName.ApprovedOnApp, TopicName.SYNC_USERINFO, new QueueData<long, int, object, object>
                            //{
                            //    Data1 = memberId,
                            //    Data2 = AppSettings.Current.ChannelConfiguration.ApiUserKingHub.RoleNormal
                            //});

                            return Ok(ActionResponse<string>.CreateSuccessResponse("Mở khóa thành công."));
                        }
                        else
                        {
                            return Ok(ActionResponse<string>.CreateErrorResponse("Mở khóa thất bại."));
                        }
                    }
                    else
                    {
                        return Ok(ActionResponse<string>.CreateErrorResponse("Tài khoản member không tồn tại."));
                    }
                }
                else
                {
                    var returnValue = await SecurityService.LockOrUnlockMemberOnAllPageAsync(accountId, memberId, 0);
                    if (returnValue == ErrorCodes.Success)
                    {
                        // update lại role bên app của member.
                        //await Function.AddToQueue(ActionName.ApprovedOnApp, TopicName.SYNC_USERINFO, new QueueData<long, int, object, object>
                        //{
                        //    Data1 = memberId,
                        //    Data2 = AppSettings.Current.ChannelConfiguration.ApiUserKingHub.RoleNormal
                        //});

                        return Ok(ActionResponse<string>.CreateSuccessResponse("Mở khóa thành công."));
                    }
                    else
                    {
                        return Ok(ActionResponse<string>.CreateErrorResponse(Current[returnValue]));
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
                return Ok(ActionResponse<string>.CreateErrorResponse(ex.Message));
            }
        }

        /// <summary>
        /// Get member trong officer
        /// </summary>
        /// <param name="officerId"></param>
        /// <returns></returns>
        /// <response code="401">Authorization has been denied for this request</response>
        /// <response code="404">Not Found</response>
        /// <response code="405">Invalid input</response>
        // POST api/security/account/get_by_id        
        [HttpPost, Route("accountmember/get_current_member")]
        public async Task<IActionResult> GetMember([FromForm]long officerId)
        {
            try
            {
                long.TryParse(User.FindFirst(ClaimTypes.NameIdentifier)?.Value, out long userCurrentId);
                var accMemberCurrent = await SecurityService.GetAccountMemberByIdAsync(officerId + IndexKeys.SeparateChar + userCurrentId);

                if (accMemberCurrent != null)
                {
                    return Ok(ActionResponse<object>.CreateSuccessResponse(new
                    {
                        accMemberCurrent.OfficerId,
                        accMemberCurrent.EncryptOfficerId,
                        accMemberCurrent.MemberId,
                        accMemberCurrent.EncryptMemberId,
                        accMemberCurrent.JoinedDate,
                        accMemberCurrent.Role,
                        accMemberCurrent.RoleName,
                        accMemberCurrent.Permissions
                    }));
                }
                else
                {
                    return Ok(ActionResponse<string>.CreateErrorResponse("Tài khoản không tồn tại."));
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
                return Ok(ActionResponse<string>.CreateErrorResponse(ex.Message));
            }
        }

        /// <summary>
        /// Search account member
        /// </summary>
        /// <param name="officerId"></param>
        /// <param name="role"></param>
        /// <param name="keyWord"></param>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        /// <response code="401">Authorization has been denied for this request</response>
        /// <response code="404">Not Found</response>
        /// <response code="405">Invalid input</response>
        // POST api/security/accountmember/search
        [HttpPost, Route("accountmember/search")]
        public async Task<IActionResult> SearchAccountMemberAsync([FromForm]long? officerId, [FromForm] AccountMemberRole? role, [FromForm]string keyWord, [FromForm] int? pageIndex, [FromForm] int? pageSize)
        {
            try
            {
                //var officerAccount = default(AccountSearch);
                long.TryParse(User.FindFirst(ClaimTypes.NameIdentifier)?.Value, out long accountId);

                var currentAccount = await SecurityService.GetAccountSearchByIdAsync(accountId);
                if (currentAccount == null) return Unauthorized();

                var _pageIndex = pageIndex > 0 ? pageIndex.Value : 1;
                var _pageSize = pageSize > 0 ? pageSize.Value : 20;

                var listMemberInfo = await SecurityService.GetMembersAsync(officerId, accountId, role, keyWord, _pageIndex, _pageSize);
                return Ok(ActionResponse<PagingDataResult<AccountMemberInfo>>.CreateSuccessResponse(listMemberInfo));
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
                return Ok(ActionResponse<string>.CreateErrorResponse(ex.Message));
            }
        }

        /// <summary>
        /// Danh sách tổ chức của user
        /// </summary>
        /// <param name="keyword"></param>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        /// <response code="401">Authorization has been denied for this request</response>
        /// <response code="404">Not Found</response>
        /// <response code="405">Invalid input</response>
        // POST api/security/accountmember/search
        [HttpPost, Route("accountmember/my_official")]
        public async Task<IActionResult> MyOfficialAsync([FromForm]string keyword, [FromForm] int? pageIndex, [FromForm] int? pageSize)
        {
            try
            {
                var _pageIndex = pageIndex < 1 ? 1 : pageIndex??1;
                var _pageSize = pageSize < 1 ? 10 : pageSize??10;
                long.TryParse(User.FindFirst(ClaimTypes.NameIdentifier)?.Value, out long userId);
                var result = await SecurityService.MyOfficialAsync(keyword, userId, _pageIndex, _pageSize);
                return Ok(ActionResponse<PagingDataResult<OfficialAccount>>.CreateSuccessResponse(result));
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
                return Ok(ActionResponse<string>.CreateErrorResponse(ex.Message));
            }
        }

        /// <summary>
        /// Danh sách Page
        /// </summary>
        /// <param name="keyword"></param>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        /// <response code="401">Authorization has been denied for this request</response>
        /// <response code="404">Not Found</response>
        /// <response code="405">Invalid input</response>
        // POST api/security/accountmember/search
        [HttpPost, Route("accountmember/my_pages")]
        public async Task<IActionResult> MyPagesAsync([FromForm]string keyword, [FromForm] int? pageIndex, [FromForm] int? pageSize)
        {
            try
            {
                long.TryParse(User.FindFirst(ClaimTypes.NameIdentifier)?.Value, out long userId);

                var search = new SearchAccountEntity
                {
                    Keyword = keyword,
                    Type = AccountType.Official,
                    PageIndex = pageIndex < 1 ? 1 : pageIndex ?? 1,
                    PageSize = pageSize < 1 ? 10 : pageSize ?? 10,
                    CreatedBy = userId
                };

                var result = await SecurityService.MyPagesAsync(search);
                return Ok(ActionResponse<PagingDataResult<OfficialAccount>>.CreateSuccessResponse(result));
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
                return Ok(ActionResponse<string>.CreateErrorResponse(ex.Message));
            }
        }

        /// <summary>
        /// Danh sách Page
        /// </summary>
        /// <param name="officerId"></param>
        /// <param name="memberId"></param>
        /// <param name="isEnable"></param>
        /// <returns></returns>
        /// <response code="401">Authorization has been denied for this request</response>
        /// <response code="404">Not Found</response>
        /// <response code="405">Invalid input</response>
        // POST api/security/accountmember/search
        [HttpPost, Route("accountmember/set_profile_is_page_on_app")]
        public async Task<IActionResult> SetProfileIsPageOnAppAsync([FromForm]long officerId, [FromForm] long memberId, [FromForm]bool isEnable)
        {
            var result = ErrorCodes.UnknowError;
            try
            {
                if(long.TryParse(User.FindFirst(ClaimTypes.NameIdentifier)?.Value, out long userId)){
                    result = await SecurityService.SetProfileIsPageOnAppAsync(officerId, userId, memberId, isEnable);
                    if(result == ErrorCodes.Success)
                    {
                        return Ok(ActionResponse<string>.CreateSuccessResponse(Current[result]));
                    }
                    else
                    {
                        return Ok(ActionResponse<string>.CreateErrorResponse(Current[result]));
                    }
                }
                else
                {
                    return Ok(ActionResponse<string>.CreateErrorResponse("Thông tin đăng nhập không đúng."));
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
                return Ok(ActionResponse<string>.CreateErrorResponse(ex.Message));
            }
        }

        /// <summary>
        /// Danh sách Page
        /// </summary>
        /// <param name="keyword"></param>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <param name="status"></param>
        /// <returns></returns>
        /// <response code="401">Authorization has been denied for this request</response>
        /// <response code="404">Not Found</response>
        /// <response code="405">Invalid input</response>
        // POST api/security/accountmember/search
        [HttpPost, Route("list_all_page")]
        public async Task<IActionResult> ListAllPageAsync([FromForm]string keyword,[FromForm]AccountStatus? status, [FromForm] int? pageIndex, [FromForm] int? pageSize)
        {
            try
            {
                if(long.TryParse(User.FindFirst(ClaimTypes.NameIdentifier)?.Value, out long userId))
                {
                    var currentAccount = await SecurityService.GetByIdAsync(userId);
                    if(currentAccount !=null && currentAccount.IsSystem == true && currentAccount.IsFullPermission == true)
                    {
                        var _pageIndex = pageIndex < 1 ? 1 : pageIndex ?? 1;
                        var _pageSize = pageSize < 1 ? 10 : pageSize ?? 10;

                        var search = new SearchAccountEntity
                        {
                            Keyword = keyword,
                            PageIndex = _pageIndex,
                            PageSize = _pageSize,
                            Status = status,
                            Type = AccountType.Official
                        };

                        var result = await SecurityService.SearchAsync(search);
                        var returnValue = new PagingDataResult<AccountSimple>();
                        if (result != null && result.Data != null)
                        {
                            returnValue.Total = result.Total;
                            returnValue.Data = result.Data?.Select(p => new AccountSimple()
                            {
                                Id = p.EncryptId,
                                Avatar = p.Avatar,
                                Banner = p.Banner,
                                Class = p.Class,
                                Email = p.Email,
                                FullName = p.FullName,
                                Mobile = p.Mobile,
                                Status = p.Status,
                                Type = p.Type,
                                UserName = p.UserName
                            })?.ToList();
                        }
                        return Ok(ActionResponse<PagingDataResult<AccountSimple>>.CreateSuccessResponse(returnValue));
                    }
                }
                return Ok(ActionResponse<string>.CreateErrorResponse("Bạn không có quyền truy cập."));
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
                return Ok(ActionResponse<string>.CreateErrorResponse(ex.Message));
            }
        }

        /// <summary>
        /// Danh sách người duyệt
        /// </summary>
        /// <param name="officerId"></param>
        /// <returns></returns>
        /// <response code="401">Authorization has been denied for this request</response>
        /// <response code="404">Not Found</response>
        /// <response code="405">Invalid input</response>
        // POST api/security/accountmember/search
        [HttpPost, Route("accountmember/approvers")]
        public async Task<IActionResult> ApproversAsync([FromForm]long officerId)
        {
            try
            {
                long.TryParse(User.FindFirst(ClaimTypes.NameIdentifier)?.Value, out long userId);


                if (await SecurityService.CheckAccountMember(officerId, userId))
                {
                    var result = await SecurityService.ApproversAsync(officerId);
                    return Ok(ActionResponse<List<AccountMemberInfo>>.CreateSuccessResponse(result));
                }
                else
                {
                    return Ok(ActionResponse<string>.CreateErrorResponse("Thông tin tổ chức không chính xác."));
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
                return Ok(ActionResponse<string>.CreateErrorResponse(ex.Message));
            }
        }


        /// <summary>
        /// Change Page Owner
        /// </summary>
        /// <param name="officerId"></param>
        /// <param name="newOwnerId"></param>
        /// <param name="oldOwnerId"></param>
        /// <returns></returns>
        /// <response code="401">Authorization has been denied for this request</response>
        /// <response code="404">Not Found</response>
        /// <response code="405">Invalid input</response>
        // POST api/security/accountmember/search
        [HttpPost, Route("accountmember/change_owner")]
        public async Task<IActionResult> ChangeOwnerAsync([FromForm]long officerId, [FromForm]long oldOwnerId, [FromForm]long newOwnerId )
        {
            try
            {
                if(long.TryParse(User.FindFirst(ClaimTypes.NameIdentifier)?.Value, out long userId))
                {
                    var result = await SecurityService.ChangeOwnerAsync(officerId, oldOwnerId, newOwnerId, userId);
                    return Ok(ActionResponse<string>.CreateSuccessResponse(Current[result]));
                }
                else
                {
                    return Ok(ActionResponse<string>.CreateErrorResponse("Thông đăng nhập không chính xác."));
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
                return Ok(ActionResponse<string>.CreateErrorResponse(ex.Message));
            }
        }

        /// <summary>
        /// Approved user on App
        /// </summary>
        /// <param name="userIds"></param>
        /// <returns></returns>
        /// <response code="401">Authorization has been denied for this request</response>
        /// <response code="404">Not Found</response>
        /// <response code="405">Invalid input</response>
        // POST api/security/accountmember/search
        [HttpPost, Route("accountmember/approved_on_app")]
        public async Task<IActionResult> ApprovedOnAppAsync([FromForm]string userIds)
        {
            try
            {
                var result = true;
                var list = userIds.Split(",");
                int index = 0;
                for(int i =0; i<list.Length; i++)
                {
                    Console.WriteLine(index++.ToString() + ":" + list[i]);
                    if (long.TryParse(list[i].Trim(), out long rs))
                    {
                        result = await SecurityService.ApprovedOnAppAsync(rs, AppSettings.Current.ChannelConfiguration.ApiUserKingHub.RoleVip);
                    }
                    else
                    {
                        Console.WriteLine(list[i] + "=> Loi");
                    }
                    
                    await Task.Delay(10);
                    if (result == false) break;
                }
                if (result)
                {
                    return Ok(ActionResponse<string>.CreateSuccessResponse("Update thành công."));
                }
                else
                {
                    return Ok(ActionResponse<string>.CreateErrorResponse("Update không thành công."));
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
                return Ok(ActionResponse<string>.CreateErrorResponse(ex.Message));
            }
        }

        private AccountSearch SetUpdateField(AccountSearch accountDb, Account account)
        {
            accountDb.Mobile = account.Mobile;
            accountDb.Email = account.Email;
            accountDb.UserName = account.UserName;
            accountDb.FullName = account.FullName;
            accountDb.Avatar = account.Avatar;
            accountDb.Banner = account.Banner;
            accountDb.IsSystem = account.IsSystem;
            accountDb.IsFullPermission = account.IsFullPermission;
            accountDb.OtpSecretKey = account.OtpSecretKey;
            accountDb.LoginedDate = account.LoginedDate;
            accountDb.Description = account.Description;
            accountDb.Mode = account.Mode;
            accountDb.CommentMode = account.CommentMode;
            accountDb.CrawlerMode = account.CrawlerMode;
            accountDb.CrawlerSource = account.CrawlerSource;
            //accountDb.LabelMode = account.LabelMode;
            return accountDb;
        }
        #endregion

        /// <summary>
        /// Get list AccountClass
        /// </summary>
        /// <param name="pageId"></param>
        /// <returns></returns>
        /// <response code="401">Authorization has been denied for this request</response>
        /// <response code="404">Not Found</response>
        /// <response code="405">Invalid input</response>
        // POST api/security/accountmember/search
        [HttpGet, Route("account/class")]
        public async Task<IActionResult> GetOwnerIdAsync([FromQuery]long pageId)
        {
            try
            {
                var accountClass = Enum.GetValues(typeof(AccountClass))
               .Cast<AccountClass>()
               .ToDictionary(t => (int)t, t => t.ToString());
                var accountClassType = Enum.GetValues(typeof(AccountClassType))
               .Cast<AccountClassType>()
               .ToDictionary(t => (int)t, t => t.ToString());


                return Ok(ActionResponse<object>.CreateSuccessResponse(new { AccountClassDef.DictAccountClass,
                    AccountClass = accountClass,
                    AccountClassType = accountClassType
                }, "Success."));
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
                return Ok(ActionResponse<string>.CreateErrorResponse(ex.Message));
            }
        }
    }
}
