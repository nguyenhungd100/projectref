﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using ChannelVN.IMS2.Core.Entities.DataNewsJson;
using ChannelVN.IMS2.Core.Entities.ErrorCode;
using ChannelVN.IMS2.Core.Services;
using ChannelVN.IMS2.Foundation.Common;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace ChannelVN.IMS2.WebApi.Controllers
{
    [Route("api/[controller]")]
    public class PluginsController : BaseController
    {
        /// <summary>
        /// Crawler Link
        /// </summary>
        /// <param name="link"></param>
        /// <param name="officerId"></param>
        [HttpPost, Route("crawler_link")]
        public async Task<IActionResult> CrawlerLinkAsync([FromForm]string link, [FromForm] long officerId)
        {
            try
            {
                var result = await PluginsService.CrawlerLinkAsync(link);             
                return Ok(ActionResponse<DataNewsJson>.CreateSuccessResponse(result.Data));            
            }
            catch (Exception ex)
            {
                return Ok(ActionResponse<string>.CreateErrorResponse(ex.Message));
            }
        }
    }
}