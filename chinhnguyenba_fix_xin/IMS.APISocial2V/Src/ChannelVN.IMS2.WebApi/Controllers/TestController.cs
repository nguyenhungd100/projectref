﻿using System;
using System.Collections.Generic;
using ChannelVN.IMS2.Foundation.Common;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Newtonsoft.Json.Schema;
using Newtonsoft.Json.Serialization;

namespace ChannelVN.IMS2.WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [AllowAnonymous]
    public class TestController : ControllerBase
    {
        [HttpGet, Route("test")]
        public IActionResult Test([FromQuery]string schemaJson,[FromQuery]string jsonData)
        {
            // Example adapted from 
            // https://www.newtonsoft.com/jsonschema/help/html/JsonValidatingWriterAndSerializer.htm
            // by James Newton-King

            var msg = new List<string>();

            var schema = JSchema.Parse(schemaJson);
            var data = Foundation.Common.Json.Parse<object>(jsonData);

            var settings = new JsonSerializerSettings { ContractResolver = new CamelCasePropertyNamesContractResolver() };
            var isValid = JsonExtensions.TestValidate(data, schema, (o, a) =>
            {
                Console.WriteLine(a.Message);
                msg.Add(a.Message);
            }, settings);
            // Prints Array item count 5 exceeds maximum count of 3. Path 'hobbies'.

            Console.WriteLine("isValid = {0}", isValid);
            // Prints isValid = False
            return Ok(new
            {
                isValid,
                msg
            });
            //return Ok("");
        }
    }
}