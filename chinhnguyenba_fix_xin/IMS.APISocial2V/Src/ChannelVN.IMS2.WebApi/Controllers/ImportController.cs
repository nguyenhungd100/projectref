﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ChannelVN.IMS2.Foundation.Common;
using ChannelVN.IMS2.Foundation.Logging;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace ChannelVN.IMS2.WebApi.Controllers
{
    [Route("api/[controller]")]
    public class ImportController : BaseController
    {
        /// <summary>
        /// Lưu album
        /// </summary>
        /// <param name="text"></param>
        /// <returns></returns>
        /// <response code="401">Authorization has been denied for this request</response>
        /// <response code="404">Not Found</response>
        /// <response code="405">Invalid input</response>
        // POST api/album/save        
        [HttpPost]
        public async Task<IActionResult> Save([FromForm]string text)
        {
            try
            {
                //var result = ErrorCodes.Exception;

                //var clientIP = string.IsNullOrEmpty(HttpContext?.Connection?.RemoteIpAddress?.ToString()) ? "192.168.1.1" : HttpContext?.Connection?.RemoteIpAddress?.ToString();

                //var validateResult = ValidateAlbum(albumData);
                //if (validateResult?.Success == false)
                //{
                //    return Ok(ActionResponse<string>.CreateErrorResponse(validateResult.Message));
                //}

                //if (!Utility.ValidateStringIds(albumData.Tags))
                //{
                //    return Ok(ActionResponse<string>.CreateErrorResponse("Tags không hợp lệ."));
                //}

                //var validPublishData = await Core.Services.Functions.Function.ValidatePublishData(albumData.PublishData, (int)CardType.Album);
                //if (!validPublishData)
                //{
                //    return Ok(ActionResponse<string>.CreateErrorResponse("PublishData invalid."));
                //}

                //if (long.TryParse(User.FindFirst(ClaimTypes.NameIdentifier)?.Value, out long accountId))
                //{
                //    if (albumData.Id > 0)
                //    {
                //        //Get PhotoInAlbum in publishData
                //        var listItem = Foundation.Common.Json.Parse<PublishData>(albumData.PublishData);
                //        albumData.PhotoInAlbum = listItem?.Items.Select(p => new PhotoInAlbum
                //        {
                //            AlbumId = albumData.Id,
                //            PhotoId = long.Parse(p.Id),
                //            PublishedDate = DateTime.Now
                //        }).ToArray();

                //        //update
                //        albumData.ModifiedDate = DateTime.Now;
                //        albumData.ModifiedBy = accountId + "";
                //        albumData.CardType = (int)CardType.Album;
                //        albumData.UnsignName = Utility.UnicodeToUnsignedAndDash(albumData.Name ?? "");

                //        result = await AlbumService.UpdateAsync(albumData, clientIP, OfficerId);
                //        //Insert log action
                //        if (result == ErrorCodes.Success)
                //        {
                //            await Function.AddToQueue(ActionName.InsertLog, TopicName.LOGACTION, new LogActionEntity
                //            {
                //                ObjectId = albumData.Id.ToString(),
                //                Account = accountId.ToString(),
                //                CreatedDate = DateTime.SpecifyKind(DateTime.Now, DateTimeKind.Unspecified),
                //                Ip = clientIP,
                //                Message = "Success.",
                //                Title = albumData.Name,
                //                SourceId = albumData.ModifiedBy,
                //                ActionTypeDetail = (int)ActionType.Update,
                //                ActionText = ActionType.Update.ToString(),
                //                Type = (int)LogContentType.Album
                //            });
                //        }
                //    }
                //    else
                //    {
                //        if (albumData.TemplateId == null || albumData.TemplateId <= 0) return Ok(ActionResponse<bool>.CreateErrorResponse("Chưa chọn template."));
                //        //insert
                //        albumData.Id = Generator.NewsId();
                //        albumData.CardType = (int)CardType.Album;
                //        albumData.Type = (int)NewsType.Album;
                //        albumData.PublishMode = (int)NewsPublishMode.Public;
                //        albumData.Status = (int)NewsStatus.Draft;
                //        albumData.CreatedDate = DateTime.Now;
                //        albumData.CreatedBy = accountId + "";
                //        albumData.DistributorId = albumData.DistributorId.HasValue ? albumData.DistributorId : AppSettings.Current.Distribution.Automation.DistributorId;
                //        albumData.UnsignName = Utility.UnicodeToUnsignedAndDash(albumData.Name ?? "");
                //        albumData.LastInsertedDate = DateTime.Now;

                //        if (long.TryParse(User.FindFirst(ClaimTypes.NameIdentifier)?.Value, out long userId))
                //        {
                //            albumData.NewsInAccount = new[] {
                //                new NewsInAccount
                //                {
                //                    AccountId = OfficerId,
                //                    NewsId = albumData.Id,
                //                    PublishedDate = DateTime.Now,
                //                    PublishedType = (int)NewsPublishedType.Post
                //                }
                //            };
                //            //Get PhotoInAlbum in publishData
                //            var listItem = Foundation.Common.Json.Parse<PublishData>(albumData.PublishData);
                //            albumData.PhotoInAlbum = listItem?.Items.Select(p => new PhotoInAlbum
                //            {
                //                AlbumId = albumData.Id,
                //                PhotoId = long.Parse(p.Id),
                //                PublishedDate = DateTime.Now
                //            }).ToArray();

                //            result = await AlbumService.AddAsync(albumData, clientIP);
                //            if (result == ErrorCodes.Success)
                //            {
                //                await Function.AddToQueue(ActionName.InsertLog, TopicName.LOGACTION, new LogActionEntity
                //                {
                //                    ObjectId = albumData.Id.ToString(),
                //                    Account = accountId.ToString(),
                //                    CreatedDate = DateTime.SpecifyKind(DateTime.Now, DateTimeKind.Unspecified),
                //                    Ip = clientIP,
                //                    Message = "Success.",
                //                    Title = albumData.Name,
                //                    SourceId = albumData.CreatedBy,
                //                    ActionTypeDetail = (int)ActionType.Insert,
                //                    ActionText = ActionType.Insert.ToString(),
                //                    Type = (int)LogContentType.Album
                //                });
                //            }
                //        }
                //    }
                //}
                //else
                //{
                //    return Ok(ActionResponse<string>.CreateErrorResponse("Thông tin tài khoản ko hợp lệ."));
                //}

                //if (result == ErrorCodes.Success)
                //{
                //    return Ok(ActionResponse<string>.CreateSuccessResponse(albumData.Id.ToString(), "Success."));
                //}
                //else
                //{
                //    return Ok(ActionResponse<string>.CreateErrorResponse(Current[result]));
                //}
                return Ok(ActionResponse<string>.CreateErrorResponse(""));
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
                return Ok(ActionResponse<string>.CreateErrorResponse(ex.Message));
            }
        }
    }
}