﻿using ChannelVN.IMS2.Core.Entities;
using ChannelVN.IMS2.Core.Entities.Distribution;
using ChannelVN.IMS2.Core.Repositories.Constants;
using ChannelVN.IMS2.Core.Repositories.Functions;
using ChannelVN.IMS2.Core.Services;
using ChannelVN.IMS2.Foundation.Common;
using ChannelVN.IMS2.Foundation.Common.Configuration;
using ChannelVN.IMS2.Foundation.Logging;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace ChannelVN.IMS2.WebApi.Controllers
{
    [Route("api/[controller]")]
    public class DistributionController : BaseController
    {
        /// <summary>
        /// Thêm mới hoặc update thông tin kênh
        /// </summary>
        /// <param name="distribution"></param>
        /// <param name="officerId"></param>
        /// <returns></returns>
        /// <response code="401">Authorization has been denied for this request</response>
        /// <response code="404">Not Found</response>
        /// <response code="405">Invalid input</response>
        // POST api/distribution/save  => getway index 23      
        [HttpPost, Route("save")]
        public async Task<IActionResult> Save([FromForm]Distributor distribution, [FromForm] long officerId)
        {
            try
            {
                var clientIP = string.IsNullOrEmpty(HttpContext?.Connection?.RemoteIpAddress?.ToString()) ? "192.168.1.1" : HttpContext?.Connection?.RemoteIpAddress?.ToString();
                long.TryParse(User.FindFirst(ClaimTypes.NameIdentifier)?.Value, out long accountId);
                if (distribution == null)
                {
                    return Ok(ActionResponse<string>.CreateErrorResponse("Kênh phân phối không tồn tại."));
                }
                if (!string.IsNullOrEmpty(distribution.Config.UnitTime) && distribution.Config.UnitTime.Trim().Length > 1)
                {
                    return Ok(ActionResponse<string>.CreateErrorResponse("ConfigUnitTime phải là kiểu \"Char?\""));
                }

                distribution.Name = distribution.Name?.Trim();
                distribution.Status = (int)DistribusionStatus.Accept;
                distribution.Config.QuotaNormal = distribution.Config.QuotaNormal ?? 5;
                distribution.Config.QuotaProfessional = distribution.Config.QuotaProfessional ?? 0;
                distribution.Config.UnitTime = distribution.Config.UnitTime?.Trim() ?? "D";
                distribution.Config.UnitAmount = distribution.Config.UnitAmount ?? 7;

                var result = false;
                if (distribution.Id > 0)
                {
                    var arrStatus = Enum.GetValues(typeof(DistribusionStatus)) as int[];
                    if (distribution.Status == null || (distribution.Status != null && arrStatus != null && !arrStatus.Contains(distribution.Status ?? -1)))
                    {
                        return Ok(ActionResponse<string>.CreateErrorResponse("Trạng thái video không hợp lệ."));
                    }
                    result = await DistributionService.UpdateAsync(distribution);
                    if (result)
                    {
                        //insert log
                        await Function.AddToQueue(ActionName.InsertLog, TopicName.LOGACTION, new LogActionEntity
                        {
                            ObjectId = distribution.Id.ToString(),
                            Account = accountId.ToString(),
                            CreatedDate = DateTime.SpecifyKind(DateTime.Now, DateTimeKind.Unspecified),
                            Ip = clientIP,
                            Message = "Success.",
                            Title = distribution.Name,
                            SourceId = officerId.ToString(),
                            ActionTypeDetail = (int)ActionType.Update,
                            ActionText = ActionType.Update.ToString(),
                            Type = (int)LogContentType.Distribution
                        });
                    }
                }
                else
                {

                    distribution.Id = Generator.DistributionId();
                    distribution.CreatedDate = DateTime.Now;
                    distribution.CreatedBy = accountId.ToString();//acc.UserName;

                    result = await DistributionService.AddAsync(distribution);
                    if (result)
                    {
                        //insert log
                        await Function.AddToQueue(ActionName.InsertLog, TopicName.LOGACTION, new LogActionEntity
                        {
                            ObjectId = distribution.Id.ToString(),
                            Account = accountId.ToString(),
                            CreatedDate = DateTime.SpecifyKind(DateTime.Now, DateTimeKind.Unspecified),
                            Ip = clientIP,
                            Message = "Success.",
                            Title = distribution.Name,
                            SourceId = officerId.ToString(),
                            ActionTypeDetail = (int)ActionType.Insert,
                            ActionText = ActionType.Insert.ToString(),
                            Type = (int)LogContentType.Distribution
                        });
                    }
                }

                if (result)
                    return Ok(ActionResponse<object>.CreateSuccessResponse(distribution.Id.ToString(), "Success."));

                return Ok(ActionResponse<bool>.CreateErrorResponse("Lỗi tạo kênh phân phối."));
            }
            catch (Exception ex)
            {
                return Ok(ActionResponse<string>.CreateErrorResponse(ex.Message));
            }
        }

        /// <summary>
        /// Lấy thông tin chi tiết kênh
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        /// <response code="401">Authorization has been denied for this request</response>
        /// <response code="404">Not Found</response>
        /// <response code="405">Invalid input</response>
        [HttpPost, Route("get_by_id")]
        public async Task<IActionResult> GetById([FromForm]long id)
        {
            try
            {
                var result = await DistributionService.GetByIdAsync(id);
                return Ok(ActionResponse<Distributor>.CreateSuccessResponse(result));
            }
            catch (Exception ex)
            {
                return Ok(ActionResponse<string>.CreateErrorResponse(ex.Message));
            }
        }

        /// <summary>
        /// Lấy thông tin chi tiết kênh (publish)
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        /// <response code="401">Authorization has been denied for this request</response>
        /// <response code="404">Not Found</response>
        /// <response code="405">Invalid input</response>
        // POST api/distribution/public/get_by_id
        [HttpPost, Route("public/get_by_id")]
        public async Task<IActionResult> GetByIdPublic([FromForm]long id)
        {
            try
            {
                var result = await DistributionService.GetByIdAsync(id);

                if (result != null)
                {
                    var configVideo = Foundation.Common.Json.Parse<Dictionary<string, string>>(result.Config.ApiVideo);
                    configVideo.TryGetValue("Type", out string type);
                    return Ok(ActionResponse<DistributionSimple>.CreateSuccessResponse(new DistributionSimple()
                    {
                        Id = result.Id,
                        ConfigCode = result.Code,
                        ConfigQuotaNormal = result.Config.QuotaNormal,
                        ConfigQuotaProfessional = result.Config.QuotaProfessional,
                        ConfigUnitAmount = result.Config.UnitAmount,
                        ConfigUnitTime = result.Config.UnitTime,
                        Description = result.Description,
                        Name = result.Name,
                        Type = type,
                        Priority = result.Priority,
                        Status = result.Status
                    }));
                }
                return Ok(ActionResponse<string>.CreateErrorResponse("Kênh không tồn tại"));
            }
            catch (Exception ex)
            {
                return Ok(ActionResponse<string>.CreateErrorResponse(ex.Message));
            }
        }

        /// <summary>
        /// Tìm kiếm kênh 
        /// </summary>
        /// <param name="keyword"></param>
        /// <param name="status"></param>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        /// <response code="401">Authorization has been denied for this request</response>
        /// <response code="404">Not Found</response>
        /// <response code="405">Invalid input</response>
        // POST api/distribution/search        
        [HttpPost, Route("search")]
        public async Task<IActionResult> Search([FromForm] string keyword, [FromForm] int? status, [FromForm] int pageIndex, [FromForm] int pageSize)
        {
            try
            {
                pageIndex = pageIndex < 1 ? 1 : pageIndex;
                pageSize = pageSize < 1 ? 10 : pageSize;

                var result = await DistributionService.SearchAsync(keyword, status, pageIndex, pageSize);
                if (result != null)
                {
                    return Ok(ActionResponse<PagingDataResult<DistributionSimple>>.CreateSuccessResponse(new PagingDataResult<DistributionSimple>()
                    {
                        Total = result.Total,
                        Data = result.Data.Where(w => w.Id != AppSettings.Current.Distribution.Automation.DistributorId).Select(p => new DistributionSimple()
                        {
                            Id = p.Id,
                            ConfigCode = p.Code,
                            ConfigQuotaNormal = p.Config.QuotaNormal,
                            ConfigQuotaProfessional = p.Config.QuotaProfessional,
                            ConfigUnitAmount = p.Config.UnitAmount,
                            ConfigUnitTime = p.Config.UnitTime,
                            Description = p.Description,
                            Name = p.Name,
                            Type = Foundation.Common.Json.Parse<Dictionary<string, string>>(p.Config.ApiVideo)?.Where(c => "Type".Equals(c.Key))?.FirstOrDefault().Value,
                            Priority = p.Priority,
                            Status = p.Status
                        }).ToList()
                    }));
                }
                return Ok(ActionResponse<PagingDataResult<DistributionSimple>>.CreateSuccessResponse(new PagingDataResult<DistributionSimple>()));
            }
            catch (Exception ex)
            {
                return Ok(ActionResponse<string>.CreateErrorResponse(ex.Message));
            }
        }

        /// <summary>
        /// Danh sách Zone video theo kênh
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        /// <response code="401">Authorization has been denied for this request</response>
        /// <response code="404">Not Found</response>
        /// <response code="405">Invalid input</response>
        // POST api/distribution/get_zone_video
        [HttpPost, Route("get_zone_video")]
        public async Task<IActionResult> GetZoneVideoById([FromForm]long id)
        {
            try
            {
                var result = await DistributionService.GetByIdAsync(id);
                if (result != null && !string.IsNullOrEmpty(result.Config.ApiZoneVideo))
                {
                    var data = await DistributionService.GetZoneVideoAsync(result.Config.ApiZoneVideo);

                    return Ok(ActionResponse<List<ZoneVideo>>.CreateSuccessResponse(data));
                }

                return Ok(ActionResponse<string>.CreateErrorResponse("Không tìm thấy kênh phân phối or ApiZoneVideo không tồn tại."));
            }
            catch (Exception ex)
            {
                return Ok(ActionResponse<string>.CreateErrorResponse(ex.Message));
            }
        }

        /// <summary>
        /// Danh sách Playlist theo kênh
        /// </summary>
        /// <param name="id"></param>
        /// <param name="keyword"></param>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        /// <response code="401">Authorization has been denied for this request</response>
        /// <response code="404">Not Found</response>
        /// <response code="405">Invalid input</response>
        // POST api/distribution/search_playlist => getway index 29
        [HttpPost, Route("search_playlist")]
        public async Task<IActionResult> GetVideoPlaylistById([FromForm]long id, [FromForm]string keyword, [FromForm]int pageIndex, [FromForm]int pageSize)
        {
            try
            {
                pageIndex = pageIndex < 1 ? 1 : pageIndex;
                pageSize = pageSize < 1 ? 10 : pageSize;

                var result = await DistributionService.GetByIdAsync(id);
                if (result != null && !string.IsNullOrEmpty(result.Config.ApiVideoPlaylist))
                {
                    var data = await DistributionService.GetVideoPlaylistAsync(result.Config.ApiVideoPlaylist, keyword, pageIndex, pageSize);
                    if (data == null)
                    {
                        data = new ResultVideoPlayListPagingData();
                    }

                    return Ok(ActionResponse<ResultVideoPlayListPagingData>.CreateSuccessResponse(data));
                }

                return Ok(ActionResponse<string>.CreateErrorResponse("Không tìm thấy kênh phân phối or ApiVideoPlaylist không tồn tại."));
            }
            catch (Exception ex)
            {
                return Ok(ActionResponse<string>.CreateErrorResponse(ex.Message));
            }
        }

        /// <summary>
        /// Phân phối tin
        /// </summary>
        /// <param name="PayloadData"></param>
        /// <param name="officerId"></param>
        /// <returns></returns>
        /// <response code="401">Authorization has been denied for this request</response>
        /// <response code="404">Not Found</response>
        /// <response code="405">Invalid input</response>
        // POST api/distribution/share
        [HttpPost, Route("share")]
        public async Task<IActionResult> Share([FromForm] string PayloadData, [FromForm] long officerId)
        {
            try
            {
                var clientIP = string.IsNullOrEmpty(HttpContext?.Connection?.RemoteIpAddress?.ToString()) ? "192.168.1.1" : HttpContext?.Connection?.RemoteIpAddress?.ToString();
                var playloadData = Foundation.Common.Json.Parse<PayloadDataShare>(PayloadData);
                var validateResult = ValidateShare(playloadData);
                if (validateResult.Success == false) return Ok(ActionResponse<string>.CreateErrorResponse(validateResult.Message));

                long.TryParse(User.FindFirst(ClaimTypes.NameIdentifier)?.Value, out long accountId);
                var account = await SecurityService.GetAccountSearchByIdAsync(accountId);

                var distribution = await DistributionService.GetByIdAsync(playloadData.DistributionId);
                if (distribution == null)
                {
                    return Ok(ActionResponse<string>.CreateErrorResponse("Kênh phân phối không tồn tại."));
                }
                else
                {
                    var configVideoObj = default(Dictionary<string, string>);
                    switch (playloadData.DistributionId == AppSettings.Current.Distribution.Automation.DistributorId)
                    {
                        case true:
                            configVideoObj = Foundation.Common.Json.Parse<Dictionary<string, string>>(distribution.Config.ApiMediaUnit);
                            if (configVideoObj == null || (configVideoObj != null && string.IsNullOrEmpty(configVideoObj["ApiUrl"])))
                                return Ok(ActionResponse<string>.CreateErrorResponse("Cấu hình kênh chưa đầy đủ."));
                            break;
                        default:
                            configVideoObj = Foundation.Common.Json.Parse<Dictionary<string, string>>(distribution.Config.ApiVideo);
                            if (configVideoObj == null || (configVideoObj != null && string.IsNullOrEmpty(configVideoObj["ApiUrl"])))
                                return Ok(ActionResponse<string>.CreateErrorResponse("Cấu hình kênh chưa đầy đủ."));
                            break;
                    }


                    var configChannelType = string.Empty;
                    configVideoObj.TryGetValue("Type", out configChannelType);
                    if (((int)DistributionType.Pegax).ToString().Equals(configChannelType))
                    {
                        foreach (var meta in playloadData.MetaDataShared)
                        {
                            meta.ZoneId = 0;
                        }
                    }
                }

                var itemStreamDistribution = new ItemStreamDistribution()
                {
                    Id = Generator.ItemStreamDistributionId(),
                    PublishStatus = (int)DistributionPublishStatus.Send,
                    CreatedDate = DateTime.Now,
                    ItemStreamTempId = playloadData.TemplateId,
                    Title = playloadData.Title,
                    MetaData = Foundation.Common.Json.Stringify(playloadData.MetaDataShared),
                    DistributorId = new[] { distribution.Id },
                    Type = playloadData.Type
                };

                var result = await DistributionService.ShareAsync(account, itemStreamDistribution, distribution, playloadData, clientIP);
                if (result)
                {
                    await Function.AddToQueue(ActionName.InsertLog, TopicName.LOGACTION, new LogActionEntity
                    {
                        ObjectId = distribution.Id.ToString(),
                        Account = accountId.ToString(),
                        CreatedDate = DateTime.SpecifyKind(DateTime.Now, DateTimeKind.Unspecified),
                        Ip = clientIP,
                        Message = "Success.",
                        Title = distribution.Name,
                        SourceId = officerId.ToString(),
                        ActionTypeDetail = (int)ActionType.Share,
                        ActionText = ActionType.Share.ToString(),
                        Type = (int)LogContentType.Distribution
                    });

                    return Ok(ActionResponse<object>.CreateSuccessResponse(itemStreamDistribution.Id.ToString(), "Success."));
                }
                return Ok(ActionResponse<string>.CreateErrorResponse("Phân phối gặp lỗi."));
            }
            catch (Exception ex)
            {
                return Ok(ActionResponse<string>.CreateErrorResponse(ex.Message));
            }
        }
        private ActionResponse<string> ValidateShare(PayloadDataShare playloadData)
        {
            if (playloadData?.DistributionId <= 0 && playloadData?.MetaDataShared?.Count == 0)
            {
                return ActionResponse<string>.CreateErrorResponse("không tìm thấy kênh && đối tượng phân phối (DistributionId?).");
            }
            if (string.IsNullOrEmpty(playloadData?.Title))
                return ActionResponse<string>.CreateErrorResponse("Tiêu đề không được để trống (Title?).");
            if (playloadData?.TemplateId < 0)
                return ActionResponse<string>.CreateErrorResponse("Chưa có mẫu (TemplateId?).");
            if (playloadData.Type < 0)
                return ActionResponse<string>.CreateErrorResponse("Không tìm thấy loại đối tượng (Type?).");

            return ActionResponse<string>.CreateSuccessResponse("Pass:).");
        }
    }
}
