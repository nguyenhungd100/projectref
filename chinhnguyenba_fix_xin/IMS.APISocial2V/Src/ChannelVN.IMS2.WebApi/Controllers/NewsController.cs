﻿using ChannelVN.IMS2.Core.Entities;
using ChannelVN.IMS2.Core.Repositories.Constants;
using ChannelVN.IMS2.Core.Repositories.Functions;
using ChannelVN.IMS2.Core.Services;
using ChannelVN.IMS2.Foundation.Common;
using ChannelVN.IMS2.Foundation.Logging;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace ChannelVN.IMS2.WebApi.Controllers
{
    [Route("api/[controller]")]
    public class NewsController : BaseController
    {
        /// <summary>
        /// Tìm kiếm tin
        /// </summary>
        /// <param name="search"></param>
        /// <returns></returns>
        /// <response code="401">Authorization has been denied for this request</response>
        /// <response code="404">Not Found</response>
        /// <response code="405">Invalid input</response>
        // POST api/news/search => getway index 47
        [HttpPost, Route("search")]
        public async Task<IActionResult> SearchAsync([FromForm]SearchNews search)
        {
            try
            {
                if (search == null)
                {
                    return Ok(ActionResponse<string>.CreateSuccessResponse("Không tìm thấy bài viết nào."));
                }
                search.PageIndex = search.PageIndex < 1 ? 1 : search.PageIndex;
                search.PageSize = search.PageSize < 1 ? 20 : search.PageSize;
                search.FromDate = search.FromDate.HasValue ? search.FromDate.Value.AddHours(0).AddMinutes(0).AddSeconds(0) : DateTime.MinValue;
                search.ToDate = search.ToDate.HasValue ? search.ToDate.Value.AddHours(23).AddMinutes(59).AddSeconds(59) : DateTime.MaxValue;
                long.TryParse(User.FindFirst(ClaimTypes.NameIdentifier)?.Value, out long accountId);

                var result = await NewsService.SearchAsync(search, accountId);
                return Ok(ActionResponse<PagingDataResult<NewsAllSearch>>.CreateSuccessResponse(result));
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
                return Ok(ActionResponse<string>.CreateErrorResponse(ex.Message));
            }
        }

        /// <summary>
        /// Tin trên page
        /// </summary>
        /// <param name="search"></param>
        /// <returns></returns>
        /// <response code="401">Authorization has been denied for this request</response>
        /// <response code="404">Not Found</response>
        /// <response code="405">Invalid input</response>
        // POST api/news/search => getway index 47
        [HttpPost, Route("news_on_page")]
        public async Task<IActionResult> NewsOnPageAsync([FromForm]SearchNews search)
        {
            try
            {
                if (search == null)
                {
                    return Ok(ActionResponse<string>.CreateSuccessResponse("Không tìm thấy bài viết nào."));
                }
                search.Status = ((int)NewsStatus.Published).ToString();
                search.PageIndex = search.PageIndex < 1 ? 1 : search.PageIndex;
                search.PageSize = search.PageSize < 1 ? 20 : search.PageSize;
                search.FromDate = search.FromDate.HasValue ? search.FromDate.Value.AddHours(0).AddMinutes(0).AddSeconds(0) : DateTime.MinValue;
                search.ToDate = search.ToDate.HasValue ? search.ToDate.Value.AddHours(23).AddMinutes(59).AddSeconds(59) : DateTime.MaxValue;
                long.TryParse(User.FindFirst(ClaimTypes.NameIdentifier)?.Value, out long accountId);

                var result = await NewsService.NewsOnPageAsync(search, accountId);
                return Ok(ActionResponse<PagingDataResult<NewsAllSearch>>.CreateSuccessResponse(result));
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
                return Ok(ActionResponse<string>.CreateErrorResponse(ex.Message));
            }
        }

        /// <summary>
        /// Danh sách tin đã phân phối
        /// </summary>
        /// <param name="search"></param>
        /// <returns></returns>
        /// <response code="401">Authorization has been denied for this request</response>
        /// <response code="404">Not Found</response>
        /// <response code="405">Invalid input</response>
        // POST api/news/search => getway index 47
        [HttpPost, Route("get_list_distribution")]
        public async Task<IActionResult> GetlistDistribution([FromForm]SearchNews search)
        {
            try
            {
                if (search == null)
                {
                    return Ok(ActionResponse<string>.CreateSuccessResponse("Không tìm thấy bài viết nào."));
                }
                search.PageIndex = search.PageIndex == null || search.PageIndex < 1 ? 1 : search.PageIndex;
                search.PageSize = search.PageSize == null || search.PageSize < 1 ? 20 : search.PageSize;
                search.FromDate = search.FromDate.HasValue ? search.FromDate.Value.AddHours(0).AddMinutes(0).AddSeconds(0) : DateTime.MinValue;
                search.ToDate = search.ToDate.HasValue ? search.ToDate.Value.AddHours(23).AddMinutes(59).AddSeconds(59) : DateTime.MaxValue;

                long.TryParse(User.FindFirst(ClaimTypes.NameIdentifier)?.Value, out long accountId);

                var result = await NewsService.SearchAsync(search, accountId, true);

                return Ok(ActionResponse<PagingDataResult<NewsAllSearch>>.CreateSuccessResponse(result));
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
                return Ok(ActionResponse<string>.CreateErrorResponse(ex.Message));
            }
        }
        /// <summary>
        /// Thay đổi giờ xuất bản
        /// </summary>
        /// <param name="newsId"></param>
        /// <param name="distributionDate"></param>
        /// <param name="officerId"></param>
        /// <returns></returns>
        /// <response code="401">Authorization has been denied for this request</response>
        /// <response code="404">Not Found</response>
        /// <response code="405">Invalid input</response>
        // POST api/news/search => getway index 47
        [HttpPost, Route("change_distribution_date")]
        public async Task<IActionResult> ChangeDistributionDateAsync([FromForm]long newsId, [FromForm]DateTime? distributionDate, [FromForm] long officerId)
        {
            try
            {
                var clientIP = string.IsNullOrEmpty(HttpContext?.Connection?.RemoteIpAddress?.ToString()) ? "192.168.1.1" : HttpContext?.Connection?.RemoteIpAddress?.ToString();
                long.TryParse(User.FindFirst(ClaimTypes.NameIdentifier)?.Value, out long accountId);
                
                if(distributionDate == null || distributionDate < DateTime.Now)
                {
                    distributionDate = DateTime.Now;
                }

                var result = await NewsService.ChangeDistributionDateAsync(newsId, distributionDate.Value, accountId, clientIP);

                // Thiếu thông tin Log Action( ActionType = ChangeDistributionDate)
                if (result)
                {
                    await Function.AddToQueue(ActionName.InsertLog, TopicName.LOGACTION, new LogActionEntity
                    {
                        ObjectId = newsId.ToString(),
                        Account = accountId.ToString(),
                        CreatedDate = DateTime.SpecifyKind(DateTime.Now, DateTimeKind.Unspecified),
                        Ip = clientIP,
                        Message = "Success.",
                        Title = "",
                        SourceId = officerId.ToString(),
                        ActionTypeDetail = (int)ActionType.ChangeDistributionDate,
                        ActionText = ActionType.ChangeDistributionDate.ToString(),
                        Type = (int)LogContentType.News
                    });
                    return Ok(ActionResponse<string>.CreateSuccessResponse("Cập nhật ngày xuất bản thành công."));
                }
                else
                {
                    return Ok(ActionResponse<string>.CreateErrorResponse("Cập nhật ngày xuất bản không thành công."));
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
                return Ok(ActionResponse<string>.CreateErrorResponse(ex.Message));
            }
        }

        /// <summary>
        /// Get list trạng thái NewsOnHome
        /// </summary>
        /// <param name="ids"></param>
        /// <returns></returns>
        /// <response code="401">Authorization has been denied for this request</response>
        /// <response code="404">Not Found</response>
        /// <response code="405">Invalid input</response>
        // POST api/news/search => getway index 47
        [HttpGet, Route("news_on_home/status")]
        public async Task<IActionResult> GetNewsOnHomeStatusAsync([FromQuery]string ids)
        {
            try
            {
                var clientIP = string.IsNullOrEmpty(HttpContext?.Connection?.RemoteIpAddress?.ToString()) ? "192.168.1.1" : HttpContext?.Connection?.RemoteIpAddress?.ToString();
                if(long.TryParse(User.FindFirst(ClaimTypes.NameIdentifier)?.Value, out long accountId))
                {
                    if (string.IsNullOrEmpty(ids))
                    {
                        return Ok(ActionResponse<string>.CreateErrorResponse("Input invalid."));
                    }
                    var listId = ids.Split(",").Select(p => long.Parse(p));

                    var result = await NewsService.GetNewsOnHomeStatusAsync(listId);
                    return Ok(ActionResponse<object>.CreateSuccessResponse(result));
                }
                else
                {
                    return Ok(ActionResponse<string>.CreateErrorResponse("account invalid."));
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
                return Ok(ActionResponse<string>.CreateErrorResponse(ex.Message));
            }
        }
    }
}
