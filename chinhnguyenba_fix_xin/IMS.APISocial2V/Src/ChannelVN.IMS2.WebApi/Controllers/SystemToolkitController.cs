﻿using ChannelVN.IMS2.Core.Entities;
using ChannelVN.IMS2.Core.Entities.Comment;
using ChannelVN.IMS2.Core.Entities.Security;
using ChannelVN.IMS2.Core.Repositories.Constants;
using ChannelVN.IMS2.Core.Repositories.Functions;
using ChannelVN.IMS2.Core.Services;
using ChannelVN.IMS2.Core.Services.VietId;
using ChannelVN.IMS2.Foundation.Common;
using ChannelVN.IMS2.Foundation.Common.Configuration;
using ChannelVN.IMS2.Foundation.Logging;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using static ChannelVN.IMS2.Core.Entities.ErrorCode.ErrorMapping;

namespace ChannelVN.IMS2.WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class SystemToolkitController : BaseController
    {
        /// <summary>
        /// Tìm kiếm account
        /// </summary>
        /// <param name="account"></param>
        /// <param name="profile"></param>
        /// <returns></returns>
        /// <response code="401">Authorization has been denied for this request</response>
        /// <response code="404">Not Found</response>
        /// <response code="405">Invalid input</response>
        // POST api/security/account/search      
        [HttpPost, Route("account/add_or_update")]
        public async Task<IActionResult> AddOrUpdateAsync([FromForm]Account account, [FromForm]UserProfile profile)
        {
            try
            {
                if (string.IsNullOrEmpty(account?.Mobile))
                {
                    return Ok(ActionResponse<string>.CreateErrorResponse("mobile is not null."));
                }

                var sessionId = HttpContext.Request.Headers["sessionId"].ToString();

                long.TryParse(User.FindFirst(ClaimTypes.NameIdentifier)?.Value, out long userCurrentId);
                var userCurrent = await SecurityService.GetByIdAsync(userCurrentId);

                if (userCurrent?.IsSystem == true || userCurrent?.IsFullPermission == true || userCurrent?.Levels?.FirstOrDefault(p => p == EnumAccountLevels.ManagePublisher) != null || userCurrent?.Levels?.FirstOrDefault(p => p == EnumAccountLevels.CMSAdmin) != null)
                {
                    var check = await SecurityService.CheckMobileAsync(account.Mobile);

                    if (check > 0)
                    {
                        var acc = await SecurityService.GetAccountSearchByIdAsync(check.Value);

                        if (acc != null)
                        {
                            account.UserName = string.IsNullOrEmpty(account.UserName) ? acc.UserName : account.UserName?.ToLower();
                            account.Password = Encryption.Md5(account.Password ?? "");
                            account.Type = account.Type ?? acc.Type;
                            account.Class = account.Class ?? acc.Class;
                            account.Status = account.Status ?? acc.Status;
                            account.Mobile = string.IsNullOrEmpty(account.Mobile) ? acc.Mobile : account.Mobile;
                            account.IsSystem = account.IsSystem ?? acc.IsSystem;
                            account.IsFullPermission = account.IsFullPermission ?? acc.IsFullPermission;
                            account.Email = string.IsNullOrEmpty(account.Email) ? acc.Email : account.Email;
                            
                            await SecurityService.UpdateAsync(acc, profile, acc.Type != (Byte?)AccountType.Official, acc.Mobile, acc.Email);

                            return Ok(ActionResponse<string>.CreateSuccessResponse(check.ToString(), Current[ErrorCodes.Success]));
                        }
                    }

                    account.UserName = account.UserName?.ToLower();
                    account.Password = Encryption.Md5(account.Password ?? "");
                    account.CreatedDate = DateTime.Now;
                    account.Type = account.Type ?? (int)AccountType.Personal;
                    account.Class = account.Class ?? (int)AccountClass.Waiting;
                    account.Status = account.Status ?? (int)AccountStatus.Actived;
                    account.VerifiedBy = userCurrentId.ToString();
                    account.VerifiedDate = DateTime.Now;
                    account.IsSystem = null;
                    account.IsFullPermission = null;

                    var result = await SecurityService.CreateByMobileAsync(account);
                    if (result.Item1 == ErrorCodes.Success)
                    {
                        await SecurityService.SaveProfileAsync(profile, result.Item2);
                        return Ok(ActionResponse<string>.CreateSuccessResponse(result.Item2?.Id.ToString(), Current[result.Item1]));
                    }
                    else
                    {
                        return Ok(ActionResponse<string>.CreateErrorResponse(Current[result.Item1]));
                    }
                }
                else
                {
                    return Ok(ActionResponse<string>.CreateErrorResponse("Bạn không có quyền truy cập....."));
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
                return Ok(ActionResponse<string>.CreateErrorResponse(ex.Message));
            }
        }

        /// <summary>
        /// Approved user on App
        /// </summary>
        /// <param name="pageId"></param>
        /// <returns></returns>
        /// <response code="401">Authorization has been denied for this request</response>
        /// <response code="404">Not Found</response>
        /// <response code="405">Invalid input</response>
        // POST api/security/accountmember/search
        [HttpGet, Route("page/ownerid")]
        public async Task<IActionResult> GetOwnerIdAsync([FromQuery]long pageId)
        {
            try
            {
                var accountPage = await SecurityService.GetByIdAsync(pageId);
                if (accountPage != null)
                {
                    if (long.TryParse(accountPage.CreatedBy, out long ownerId))
                    {
                        return Ok(ActionResponse<long>.CreateSuccessResponse(ownerId));
                    }
                    else
                    {
                        return Ok(ActionResponse<string>.CreateErrorResponse("not exist page owner."));
                    }
                }
                else
                {
                    return Ok(ActionResponse<string>.CreateErrorResponse("not exist page."));
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
                return Ok(ActionResponse<string>.CreateErrorResponse(ex.Message));
            }
        }

        [HttpGet, Route("get_sessionid")]
        public async Task<IActionResult> GetSessionIdAsync([FromQuery]string mobile)
        {
            try
            {
                long.TryParse(User.FindFirst(ClaimTypes.NameIdentifier)?.Value, out long userCurrentId);
                var userCurrent = await SecurityService.GetByIdAsync(userCurrentId);

                if (userCurrent?.IsSystem == true || userCurrent?.IsFullPermission == true || userCurrent?.Levels?.FirstOrDefault(p => p == EnumAccountLevels.ManagePublisher) != null || userCurrent?.Levels?.FirstOrDefault(p => p == EnumAccountLevels.CMSAdmin) != null)
                {
                    var accesstoken = await DataVietIdService.GetAccessToken(mobile);
                    if (accesstoken?.Datas != null)
                    {
                        var result = await DataVietIdService.GetSessionId(accesstoken.Datas.AccessToken);
                        if (!string.IsNullOrEmpty(result?.data?.sessionId))
                        {
                            return Ok(ActionResponse<object>.CreateSuccessResponse(result, "success."));
                        }
                        else
                        {
                            return Ok(ActionResponse<string>.CreateErrorResponse("Không lấy được sessionid."));
                        }
                    }
                    else
                    {
                        return Ok(ActionResponse<string>.CreateErrorResponse("Không lấy được accesstoken."));
                    }
                }
                else
                {
                    return Ok(ActionResponse<string>.CreateErrorResponse("Bạn không có quyền."));
                }
            }
            catch (Exception ex)
            {
                return Ok(ActionResponse<string>.CreateErrorResponse(ex.Message));
            }
        }

        [HttpPost, Route("get_user_info_on_app")]
        public async Task<IActionResult> GetUserInfoOnAppAsync([FromForm]long userId)
        {
            try
            {
                var url = AppSettings.Current.ChannelConfiguration.ApiUserKingHub.UrlGetUserInfo;
                var body = ChannelVN.IMS2.Foundation.Common.Json.Stringify(new
                {
                    user_id = userId
                });
                var result = await CommentService.HttpClientPostDataAsync3<UserInfoResponse[]>(url, body);
                return Ok(ActionResponse<object>.CreateSuccessResponse(result, "success."));
            }
            catch (Exception ex)
            {
                return Ok(ActionResponse<string>.CreateErrorResponse(ex.Message));
            }
        }

        [HttpPost, Route("get_user_info_on_app_v2")]
        public async Task<IActionResult> GetUserInfoOnAppV2Async([FromForm]long userId)
        {
            try
            {
                var result = await SecurityService.GetUserInfoV2(userId);
                return Ok(ActionResponse<object>.CreateSuccessResponse(result, "success."));
            }
            catch (Exception ex)
            {
                return Ok(ActionResponse<string>.CreateErrorResponse(ex.Message));
            }
        }

        [HttpPost, Route("restore_job_queue")]
        public async Task<IActionResult> RestoreJobQueue([FromForm]string KeyQueue, [FromHeader]string key)
        {
            try
            {
                await SystemService.RestoreJobQueue(KeyQueue);
                return Ok(ActionResponse<string>.CreateSuccessResponse("Running..."));
            }
            catch (Exception ex)
            {
                return Ok(ActionResponse<string>.CreateErrorResponse(ex.Message));
            }
        }

        [HttpPost, Route("init_sql_from_resdis")]
        public async Task<IActionResult> InitSQLDataFromRedis([FromForm]string tableName, [FromHeader]string key)
        {
            try
            {
                //if (apiKey.Equals(key))
                //{
                //    await SystemService.InitSQLDataFromRedis(tableName);
                //}
                //await SystemService.InitSQLDataFromRedis(tableName);
                return Ok(ActionResponse<string>.CreateSuccessResponse("Running..."));
            }
            catch (Exception ex)
            {
                return Ok(ActionResponse<string>.CreateErrorResponse(ex.Message));
            }
        }

        [HttpPost, Route("push_to_app")]
        public async Task<IActionResult> PushToAppAsync([FromForm]string tableName, [FromForm]string ids, [FromHeader]string key)
        {
            try
            {
                var clientIP = string.IsNullOrEmpty(HttpContext?.Connection?.RemoteIpAddress?.ToString()) ? "192.168.1.1" : HttpContext?.Connection?.RemoteIpAddress?.ToString();
                var result = await SystemService.PushToAppAsync(tableName, ids, clientIP);

                return Ok(ActionResponse<string>.CreateSuccessResponse("Running..."));
            }
            catch (Exception ex)
            {
                return Ok(ActionResponse<string>.CreateErrorResponse(ex.Message));
            }
        }

        [HttpPost, Route("add_to_app")]
        public async Task<IActionResult> AddToAppAsync([FromForm]string tableName, [FromForm]string ids, [FromHeader]string key)
        {
            try
            {
                var clientIP = string.IsNullOrEmpty(HttpContext?.Connection?.RemoteIpAddress?.ToString()) ? "192.168.1.1" : HttpContext?.Connection?.RemoteIpAddress?.ToString();
                var result = await SystemService.AddToAppAsync(tableName, ids, clientIP);

                return Ok(ActionResponse<string>.CreateSuccessResponse("Running..."));
            }
            catch (Exception ex)
            {
                return Ok(ActionResponse<string>.CreateErrorResponse(ex.Message));
            }
        }

        [HttpPost, Route("push_drop_heart")]
        public async Task<IActionResult> PushDropHeartAsync([FromForm]string tableName, [FromForm]string ids, [FromHeader]string key)
        {
            var returnValue = false;
            try
            {
                long.TryParse(User.FindFirst(ClaimTypes.NameIdentifier)?.Value, out long userCurrentId);
                var userCurrent = await SecurityService.GetByIdAsync(userCurrentId);

                if (userCurrent?.IsSystem == true || userCurrent?.IsFullPermission == true)
                {
                    var clientIP = string.IsNullOrEmpty(HttpContext?.Connection?.RemoteIpAddress?.ToString()) ? "192.168.1.1" : HttpContext?.Connection?.RemoteIpAddress?.ToString();
                    returnValue = await SystemService.PushDropHeartAsync(tableName, ids, clientIP);
                }
                else
                {
                    return Ok(ActionResponse<string>.CreateErrorResponse("Bạn không có quyền truy cập."));
                }

                return Ok(ActionResponse<bool>.CreateSuccessResponse(returnValue, "Running..."));
            }
            catch (Exception ex)
            {
                return Ok(ActionResponse<string>.CreateErrorResponse(ex.Message));
            }
        }

        [HttpPost, Route("push_page_owner_to_adtech")]
        public async Task<IActionResult> PushPageOwnerToAdtechAsync([FromHeader]string key)
        {
            try
            {
                long.TryParse(User.FindFirst(ClaimTypes.NameIdentifier)?.Value, out long userCurrentId);
                var userCurrent = await SecurityService.GetByIdAsync(userCurrentId);

                if (userCurrent?.IsSystem == true || userCurrent?.IsFullPermission == true)
                {
                    //var result = await SystemService.PushPageOwnerToAdtechAsync(0, 0);
                }
                else
                {
                    return Ok(ActionResponse<string>.CreateErrorResponse("Bạn không có quyền truy cập."));
                }
                //if (apiKey.Equals(key))
                //{
                //    var result = await SystemService.PushPageOwnerToAdtechAsync(0, 0);
                //}
                //var result = await SystemService.PushPageOwnerToAdtechAsync(0, 0, 0);
                return Ok(ActionResponse<string>.CreateSuccessResponse("Running..."));
            }
            catch (Exception ex)
            {
                return Ok(ActionResponse<string>.CreateErrorResponse(ex.Message));
            }
        }

        [HttpPost, Route("execute_query")]
        public async Task<IActionResult> ExecuteQueryAsync([FromForm]string query, [FromHeader]string key)
        {
            try
            {
                var result = false;
                long.TryParse(User.FindFirst(ClaimTypes.NameIdentifier)?.Value, out long userCurrentId);
                var userCurrent = await SecurityService.GetByIdAsync(userCurrentId);

                if (userCurrent?.IsSystem == true || userCurrent?.IsFullPermission == true)
                {
                    result = await SystemService.ExecuteQueryAsync(query);
                }
                else
                {
                    return Ok(ActionResponse<string>.CreateErrorResponse("Bạn không có quyền truy cập."));
                }
                return Ok(ActionResponse<bool>.CreateSuccessResponse(result));
            }
            catch (Exception ex)
            {
                return Ok(ActionResponse<string>.CreateErrorResponse(ex.Message));
            }
        }

        [HttpGet, Route("list_queue")]
        public async Task<IActionResult> ListQueueAsync([FromQuery]string index, [FromQuery]long start, [FromQuery]long stop, [FromHeader]string key)
        {
            try
            {
                long.TryParse(User.FindFirst(ClaimTypes.NameIdentifier)?.Value, out long userCurrentId);
                var userCurrent = await SecurityService.GetByIdAsync(userCurrentId);

                if (userCurrent?.IsSystem == true || userCurrent?.IsFullPermission == true)
                {
                    var result = await SystemService.ListQueueAsync(index, start, stop);
                    return Ok(ActionResponse<string[]>.CreateSuccessResponse(result?.Select(p => (string)p).ToArray()));
                }
                else
                {
                    return Ok(ActionResponse<string>.CreateErrorResponse("Bạn không có quyền truy cập."));
                }
            }
            catch (Exception ex)
            {
                return Ok(ActionResponse<string>.CreateErrorResponse(ex.Message));
            }
        }

        [HttpGet, Route("get_by_query")]
        public async Task<IActionResult> GetAsync([FromQuery]string query, [FromHeader]string key)
        {
            try
            {
                long.TryParse(User.FindFirst(ClaimTypes.NameIdentifier)?.Value, out long userCurrentId);
                var userCurrent = await SecurityService.GetByIdAsync(userCurrentId);

                if (userCurrent?.IsSystem == true || userCurrent?.IsFullPermission == true)
                {
                    var result = await SystemService.GetAsync(query);
                    return Ok(ActionResponse<object>.CreateSuccessResponse(result, "success."));
                }
                else
                {
                    return Ok(ActionResponse<string>.CreateErrorResponse("Bạn không có quyền truy cập."));
                }
            }
            catch (Exception ex)
            {
                return Ok(ActionResponse<string>.CreateErrorResponse(ex.Message));
            }
        }

        [HttpPost, Route("update_owner_page")]
        public async Task<IActionResult> UpdateOwnerPageAsync([FromForm]string query, [FromHeader]string key)
        {
            try
            {
                long.TryParse(User.FindFirst(ClaimTypes.NameIdentifier)?.Value, out long userCurrentId);
                var userCurrent = await SecurityService.GetByIdAsync(userCurrentId);

                if (userCurrent?.IsSystem == true || userCurrent?.IsFullPermission == true)
                {
                    var result = await SystemService.UpdateOwnerPageAsync(query);
                    return Ok(ActionResponse<bool>.CreateSuccessResponse(result, "success."));
                }
                else
                {
                    return Ok(ActionResponse<string>.CreateErrorResponse("Bạn không có quyền truy cập."));
                }
            }
            catch (Exception ex)
            {
                return Ok(ActionResponse<string>.CreateErrorResponse(ex.Message));
            }
        }

        [HttpPost("sendtest-crm")]
        public async Task<IActionResult> SendTestCRMAsync()
        {
            try
            {
                long.TryParse(User.FindFirst(ClaimTypes.NameIdentifier)?.Value, out long userCurrentId);
                var userCurrent = await SecurityService.GetByIdAsync(userCurrentId);

                if (userCurrent?.IsSystem == true || userCurrent?.IsFullPermission == true)
                {
                    var listAccount = await SecurityService.GetAllAccountAsync();
                    foreach (var item in listAccount)
                    {
                        await Task.Run(async () =>
                        {

                            await Function.AddToQueue(ActionName.SendCrm, TopicName.SYNC_USERINFO, new QueueData<string, Account, List<Account>, object>
                            {
                                Data1 = item.Value.Account?.FullName,
                                Data2 = item.Value.Account,
                                Data3 = item.Value.Pages
                            });

                            Logger.Debug("sendtest-crm =>Id=" + item.Key + ", mobile =" + item.Value.Account.Mobile + ", email=" + item.Value.Account.Email + ", Type=" + (AccountType)item.Value.Account.Type + ", PageIds =" + string.Join(",", item.Value.Pages?.Select(p => p.Id)));
                            //await SecurityService.SendCRMAsync(item.Value.Account.Id, "", item.Value.Account.FullName, item.Value.Account.Mobile, item.Value.Account.Email, (AccountType?)item.Value.Account.Type, item.Value.Pages);
                        });
                    }
                    return Ok(ActionResponse<string>.CreateSuccessResponse("Runing..."));
                }
                else
                {
                    return Ok(ActionResponse<string>.CreateErrorResponse("Bạn không có quyền truy cập."));
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
                return Ok(ActionResponse<string>.CreateErrorResponse(ex.Message));
            }
        }


        [HttpPost("update_page_info")]
        public async Task<IActionResult> UpdatePageInfoAsync([FromForm]string pageIds)
        {
            try
            {
                var sessionId = HttpContext.Request.Headers["session-id"].ToString();
                long.TryParse(User.FindFirst(ClaimTypes.NameIdentifier)?.Value, out long userCurrentId);
                var userCurrent = await SecurityService.GetByIdAsync(userCurrentId);

                if (userCurrent?.IsSystem == true || userCurrent?.IsFullPermission == true)
                {
                    if (string.IsNullOrEmpty(pageIds))
                    {
                        var listData = await SecurityService.GetAllPageAsync();
                        if (listData != null)
                        {
                            foreach(var data in listData)
                            {
                                if (data.Value != null)
                                {
                                    await Function.AddToQueue(ActionName.UpdateUserInfoOnApp, TopicName.SYNC_USERINFO, new QueueData<AccountSearch, string, UserProfile, object>
                                    {
                                        Data1 = Mapper<AccountSearch>.Map(data.Value.Account,new AccountSearch()),
                                        Data2 = sessionId,
                                        Data3 = data.Value.Profile
                                    });
                                }
                            }
                        }
                    }
                    else
                    {
                        return Ok(ActionResponse<string>.CreateErrorResponse("chua lam."));
                    }

                    return Ok(ActionResponse<bool>.CreateSuccessResponse(false, "success."));
                }
                else
                {
                    return Ok(ActionResponse<string>.CreateErrorResponse("Bạn không có quyền truy cập."));
                }
            }
            catch (Exception ex)
            {
                return Ok(ActionResponse<string>.CreateErrorResponse(ex.Message));
            }
        }

        /// <summary>
        /// Tìm kiếm tin
        /// </summary>
        /// <param name="search"></param>
        /// <returns></returns>
        /// <response code="401">Authorization has been denied for this request</response>
        /// <response code="404">Not Found</response>
        /// <response code="405">Invalid input</response>
        // POST api/news/search => getway index 47
        [HttpPost, Route("search_check_post")]
        public async Task<IActionResult> SearchCheckPostAsync([FromForm]SearchNews search)
        {
            try
            {
                if (search == null)
                {
                    return Ok(ActionResponse<string>.CreateSuccessResponse("Không tìm thấy bài viết nào."));
                }
                search.PageIndex = search.PageIndex < 1 ? 1 : search.PageIndex;
                search.PageSize = search.PageSize < 1 ? 20 : search.PageSize;
                search.FromDate = search.FromDate.HasValue ? search.FromDate.Value.AddHours(0).AddMinutes(0).AddSeconds(0) : DateTime.MinValue;
                search.ToDate = search.ToDate.HasValue ? search.ToDate.Value.AddHours(23).AddMinutes(59).AddSeconds(59) : DateTime.MaxValue;

                var result = await SystemToolkitService.SearchCheckPostAsync(search);
                return Ok(ActionResponse<PagingDataResult<NewsAllSearch>>.CreateSuccessResponse(result));
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
                return Ok(ActionResponse<string>.CreateErrorResponse(ex.Message));
            }
        }

        #region sync key redis: imssocialv2:[namespace]:index:account:emails, imssocialv2:[namespace]:index:account:phonenumbers

        [HttpPost, Route("init_redis_keymobile_from_db")]
        public async Task<IActionResult> InitRedisKeyMobileAndEmailFromDB([FromForm]string key)
        {
            try
            {               
                await SystemToolkitService.InitRedisKeyMobileFromDB(key);
                return Ok(ActionResponse<string>.CreateSuccessResponse("Running..."));
            }
            catch (Exception ex)
            {
                return Ok(ActionResponse<string>.CreateErrorResponse(ex.Message));
            }
        }

        [HttpPost, Route("delete_redis_by_key")]
        public async Task<IActionResult> DeleteRedisByKey([FromForm]string key)
        {
            try
            {
                await SystemToolkitService.DeleteRedisByKey(key);
                return Ok(ActionResponse<string>.CreateSuccessResponse("Running..."));
            }
            catch (Exception ex)
            {
                return Ok(ActionResponse<string>.CreateErrorResponse(ex.Message));
            }
        }

        [HttpPost, Route("set_account_system")]
        public async Task<IActionResult> SetAccountSystemAsync([FromForm]long accountId, [FromForm]bool isSystem, [FromForm]bool isFullPermission,[FromForm]string password)
        {
            try
            {
                var account = await SecurityService.GetAccountSearchByIdAsync(accountId);
                if (account != null && account.Type != (byte?)AccountType.Official)
                {
                    account.IsSystem = isSystem;
                    account.IsFullPermission = isFullPermission;
                    account.Password = Encryption.Md5(password);
                    var profile = await SecurityService.GetProfileAsync(account.Id);

                    var result = await SecurityService.UpdateAsync(account, profile, false, "", "");
                    if(result == ErrorCodes.Success)
                    {
                        return Ok(ActionResponse<AccountSearch>.CreateSuccessResponse(account));
                    }
                    else
                    {
                        return Ok(ActionResponse<string>.CreateErrorResponse("Set account system error."));
                    }
                }
                else
                {
                    return Ok(ActionResponse<string>.CreateErrorResponse("Account not exist."));
                }
            }
            catch (Exception ex)
            {
                return Ok(ActionResponse<string>.CreateErrorResponse(ex.Message));
            }
        }

        [HttpPost, Route("push_page_owner_and_type_to_adtech")]
        public async Task<IActionResult> PushPageOwnerAndTypeToAdtechAsync([FromForm]string pageIds)
        {
            try
            {
                long.TryParse(User.FindFirst(ClaimTypes.NameIdentifier)?.Value, out long userCurrentId);
                var userCurrent = await SecurityService.GetByIdAsync(userCurrentId);

                var result = await SystemToolkitService.PushPageOwnerAndTypeToAdtechAsync(pageIds);

                //if (userCurrent?.IsSystem == true || userCurrent?.IsFullPermission == true)
                //{

                //}
                //else
                //{
                //    return Ok(ActionResponse<string>.CreateErrorResponse("Bạn không có quyền truy cập."));
                //}
                return Ok(ActionResponse<string>.CreateSuccessResponse("Number page:"+ result));
            }
            catch (Exception ex)
            {
                return Ok(ActionResponse<string>.CreateErrorResponse(ex.Message));
            }
        }

        [HttpPost, Route("push_board_to_adtech")]
        public async Task<IActionResult> PushBoardToAdtechAsync([FromForm]string boardIds)
        {
            try
            {
                var clientIP = HttpContext.GetClientIP();
                long.TryParse(User.FindFirst(ClaimTypes.NameIdentifier)?.Value, out long userCurrentId);
                var userCurrent = await SecurityService.GetByIdAsync(userCurrentId);

                var result = await SystemToolkitService.PushBoardToAdtechAsync(boardIds, clientIP);

                //if (userCurrent?.IsSystem == true || userCurrent?.IsFullPermission == true)
                //{

                //}
                //else
                //{
                //    return Ok(ActionResponse<string>.CreateErrorResponse("Bạn không có quyền truy cập."));
                //}
                return Ok(ActionResponse<string>.CreateSuccessResponse("Running..."));
            }
            catch (Exception ex)
            {
                return Ok(ActionResponse<string>.CreateErrorResponse(ex.Message));
            }
        }
        #endregion
    }
}