﻿using ChannelVN.IMS2.Core.Entities;
using ChannelVN.IMS2.Core.Entities.Distribution;
using ChannelVN.IMS2.Core.Entities.Media;
using ChannelVN.IMS2.Core.Entities.Newsletters;
using ChannelVN.IMS2.Core.Entities.Security;
using ChannelVN.IMS2.Core.Entities.Videos;
using ChannelVN.IMS2.Core.Repositories.Constants;
using ChannelVN.IMS2.Core.Repositories.Functions;
using ChannelVN.IMS2.Core.Services;
using ChannelVN.IMS2.Foundation.Common;
using ChannelVN.IMS2.Foundation.Common.Configuration;
using ChannelVN.IMS2.Foundation.Logging;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using static ChannelVN.IMS2.Core.Entities.ErrorCode.ErrorMapping;

namespace ChannelVN.IMS2.WebApi.Controllers
{
    [Route("api/[controller]")]
    public class PlaylistController : BaseController
    {
        /// <summary>
        /// Thêm mới hoặc update Playlist
        /// </summary>
        /// <param name="videoPlaylist"></param>
        /// <param name="officerId"></param>
        /// <param name="videoInPlaylist"></param>
        /// <returns></returns>
        /// <response code="401">Authorization has been denied for this request</response>
        /// <response code="404">Not Found</response>
        /// <response code="405">Invalid input</response>
        // POST api/videoplaylist/add => getway index 84
        [HttpPost, Route("save")]
        public async Task<IActionResult> SaveAsync([FromForm]VideoPlaylist videoPlaylist, [FromForm]VideoInPlaylist[] videoInPlaylist, [FromForm]long officerId)
        {
            try
            {
                string sessionId = HttpContext.GetSessionId();
                if (string.IsNullOrEmpty(sessionId))
                {
                    return Ok(ActionResponse<string>.CreateErrorResponse("session-id not empty"));
                }
                var clientIP = HttpContext.GetClientIP();

                var result = ErrorCodes.BusinessError;                

                if (videoPlaylist == null)
                {
                    return Ok(ActionResponse<string>.CreateErrorResponse("Object nulll."));
                }

                if (!Utility.ValidateStringIds(videoPlaylist.Tags))
                {
                    return Ok(ActionResponse<string>.CreateErrorResponse("Tags không hợp lệ."));
                }

                var validPublishData = await Core.Services.Functions.Function.ValidatePublishData(videoPlaylist.PublishData, (int)CardType.PlaylistVideo);
                if (!validPublishData)
                {
                    return Ok(ActionResponse<string>.CreateErrorResponse("PublishData invalid."));
                }

                if (long.TryParse(User.FindFirst(ClaimTypes.NameIdentifier)?.Value, out long userId))
                {
                    if (videoPlaylist.Id > 0)
                    {
                        videoPlaylist.ModifiedDate = DateTime.Now;
                        videoPlaylist.PublishMode = (int)NewsPublishMode.Public;
                        videoPlaylist.ModifiedBy = userId.ToString();

                        var url = Utility.UnicodeToUnsignedAndDash(videoPlaylist.Name ?? "");
                        videoPlaylist.UnsignName = url;

                        videoPlaylist.Url = string.Format(AppSettings.Current.ChannelConfiguration.CommonSettings.UrlFormatPlayList, url, videoPlaylist.Id);

                        var videoPlaylistSearch = Mapper<VideoPlaylistSearch>.Map(videoPlaylist, new VideoPlaylistSearch());

                        //Get PhotoInAlbum in publishData
                        var listItem = Foundation.Common.Json.Parse<PublishData>(videoPlaylist.PublishData);
                        videoPlaylistSearch.VideoInPlaylist = listItem?.Items.Select(p => new VideoInPlaylist
                        {
                            PlaylistId = videoPlaylist.Id,
                            VideoId = long.Parse(p.Id),
                            PlayOnTime = DateTime.Now
                        }).ToArray();

                        result = await VideoPlaylistService.UpdateAsync(videoPlaylistSearch, officerId, clientIP, sessionId);
                        if (result == ErrorCodes.Success)
                        {
                            //insert log
                            await Function.AddToQueue(ActionName.InsertLog, TopicName.LOGACTION, new LogActionEntity
                            {
                                ObjectId = videoPlaylistSearch.Id.ToString(),
                                Account = userId.ToString(),
                                CreatedDate = DateTime.SpecifyKind(DateTime.Now, DateTimeKind.Unspecified),
                                Ip = clientIP,
                                Message = "Success.",
                                Title = videoPlaylistSearch.Name,
                                SourceId = officerId.ToString(),
                                ActionTypeDetail = (int)ActionType.Update,
                                ActionText = ActionType.Update.ToString(),
                                Type = (int)LogContentType.PlayList
                            });
                        }
                    }
                    else
                    {
                        var videoPlaylistSearch = Mapper<VideoPlaylistSearch>.Map(videoPlaylist, new VideoPlaylistSearch());
                        videoPlaylistSearch.VideoInPlaylist = videoInPlaylist;
                        videoPlaylistSearch.Id = Generator.NewsId();
                        videoPlaylist.Id = videoPlaylistSearch.Id;
                        videoPlaylistSearch.Type = (int)NewsType.PlayList;
                        videoPlaylistSearch.CardType = (int)CardType.PlaylistVideo;
                        videoPlaylistSearch.Status = (int)NewsStatus.Draft;
                        videoPlaylistSearch.CreatedDate = DateTime.Now;
                        videoPlaylistSearch.PublishMode = (int)NewsPublishMode.Public;
                        videoPlaylistSearch.DistributorId = videoPlaylistSearch.DistributorId.HasValue ? videoPlaylistSearch.DistributorId : AppSettings.Current.Distribution.Automation.DistributorId;

                        videoPlaylistSearch.NewsInAccount = new[] {
                            new NewsInAccount
                            {
                                AccountId = officerId,
                                NewsId = videoPlaylistSearch.Id,
                                PublishedDate = DateTime.Now,
                                PublishedType = (int)PublishedType.Post
                            }
                        };

                        videoPlaylistSearch.CreatedBy = userId.ToString();
                        var url = Utility.UnicodeToUnsignedAndDash(videoPlaylist.Name ?? "");
                        videoPlaylistSearch.UnsignName = url;
                        videoPlaylistSearch.Url = string.Format(AppSettings.Current.ChannelConfiguration.CommonSettings.UrlFormatPlayList, url, videoPlaylistSearch.Id);

                        //Get PhotoInAlbum in publishData
                        var listItem = Foundation.Common.Json.Parse<PublishData>(videoPlaylist.PublishData);
                        videoPlaylistSearch.VideoInPlaylist = listItem?.Items.Select(p => new VideoInPlaylist
                        {
                            PlaylistId = videoPlaylist.Id,
                            VideoId = long.Parse(p.Id),
                            PlayOnTime = DateTime.Now
                        }).ToArray();

                        result = await VideoPlaylistService.AddAsync(videoPlaylistSearch, clientIP);
                        if (result == ErrorCodes.Success)
                        {
                            //insert log
                            await Function.AddToQueue(ActionName.InsertLog, TopicName.LOGACTION, new LogActionEntity
                            {
                                ObjectId = videoPlaylistSearch.Id.ToString(),
                                Account = userId.ToString(),
                                CreatedDate = DateTime.SpecifyKind(DateTime.Now, DateTimeKind.Unspecified),
                                Ip = clientIP,
                                Message = "Success.",
                                Title = videoPlaylistSearch.Name,
                                SourceId = officerId.ToString(),
                                ActionTypeDetail = (int)ActionType.Insert,
                                ActionText = ActionType.Insert.ToString(),
                                Type = (int)LogContentType.PlayList
                            });
                        }
                    }
                }
                else
                {
                    result = ErrorCodes.PermissionInvalid;
                }

                if (result == ErrorCodes.Success)
                {
                    return Ok(ActionResponse<object>.CreateSuccessResponse(videoPlaylist.EncryptId, "success"));
                }
                else
                {
                    return Ok(ActionResponse<string>.CreateErrorResponse(Current[result]));
                }
            }
            catch (Exception ex)
            {
                return Ok(ActionResponse<string>.CreateErrorResponse(ex.Message));
            }
        }
        /// <summary>
        /// Lưu trữ Playlist
        /// </summary>
        /// <param name="videoPlaylist"></param>
        /// <param name="officerId"></param>
        /// <param name="videoInPlaylist"></param>
        /// <returns></returns>
        /// <response code="401">Authorization has been denied for this request</response>
        /// <response code="404">Not Found</response>
        /// <response code="405">Invalid input</response>
        // POST api/videoplaylist/add => getway index 84
        [HttpPost, Route("archive")]
        public async Task<IActionResult> ArchiveAsync([FromForm]VideoPlaylist videoPlaylist, [FromForm]VideoInPlaylist[] videoInPlaylist, [FromForm]long officerId)
        {
            try
            {
                string sessionId = HttpContext.GetSessionId();
                if (string.IsNullOrEmpty(sessionId))
                {
                    return Ok(ActionResponse<string>.CreateErrorResponse("session-id not empty"));
                }
                var clientIP = HttpContext.GetClientIP();

                var result = ErrorCodes.BusinessError;

                if (videoPlaylist == null)
                {
                    return Ok(ActionResponse<string>.CreateErrorResponse("Object nulll."));
                }
                if (!Utility.ValidateStringIds(videoPlaylist.Tags))
                {
                    return Ok(ActionResponse<string>.CreateErrorResponse("Tags không hợp lệ."));
                }
                if (long.TryParse(User.FindFirst(ClaimTypes.NameIdentifier)?.Value, out long userId))
                {
                    if (videoPlaylist.Id > 0)
                    {
                        videoPlaylist.ModifiedDate = DateTime.Now;
                        videoPlaylist.PublishMode = (int)NewsPublishMode.Public;
                        videoPlaylist.ModifiedBy = userId.ToString();
                        var url = Utility.UnicodeToUnsignedAndDash(videoPlaylist.Name ?? "");
                        videoPlaylist.UnsignName = url;
                        videoPlaylist.Url = string.Format(AppSettings.Current.ChannelConfiguration.CommonSettings.UrlFormatPlayList, url, videoPlaylist.Id);
                        var videoPlaylistSearch = Mapper<VideoPlaylistSearch>.Map(videoPlaylist, new VideoPlaylistSearch());

                        //Get PhotoInAlbum in publishData
                        var listItem = Foundation.Common.Json.Parse<PublishData>(videoPlaylist.PublishData);
                        videoPlaylistSearch.VideoInPlaylist = listItem?.Items.Select(p => new VideoInPlaylist
                        {
                            PlaylistId = videoPlaylist.Id,
                            VideoId = long.Parse(p.Id),
                            PlayOnTime = DateTime.Now
                        }).ToArray();

                        result = await VideoPlaylistService.UpdateAsync(videoPlaylistSearch, officerId, clientIP, sessionId);
                        if (result == ErrorCodes.Success)
                        {
                            //insert log
                            await Function.AddToQueue(ActionName.InsertLog, TopicName.LOGACTION, new LogActionEntity
                            {
                                ObjectId = videoPlaylistSearch.Id.ToString(),
                                Account = userId.ToString(),
                                CreatedDate = DateTime.SpecifyKind(DateTime.Now, DateTimeKind.Unspecified),
                                Ip = clientIP,
                                Message = "Success.",
                                Title = videoPlaylistSearch.Name,
                                SourceId = officerId.ToString(),
                                ActionTypeDetail = (int)ActionType.Update,
                                ActionText = ActionType.Update.ToString(),
                                Type = (int)LogContentType.PlayList
                            });
                        }
                    }
                    else
                    {
                        var videoPlaylistSearch = Mapper<VideoPlaylistSearch>.Map(videoPlaylist, new VideoPlaylistSearch());
                        videoPlaylistSearch.VideoInPlaylist = videoInPlaylist;
                        videoPlaylistSearch.Id = Generator.NewsId();
                        videoPlaylist.Id = videoPlaylistSearch.Id;
                        videoPlaylistSearch.Type = (int)NewsType.PlayList;
                        videoPlaylistSearch.CardType = (int)CardType.PlaylistVideo;
                        videoPlaylistSearch.Status = (int)NewsStatus.Archive;
                        videoPlaylistSearch.CreatedDate = DateTime.Now;
                        videoPlaylistSearch.PublishMode = (int)NewsPublishMode.Public;
                        videoPlaylistSearch.DistributorId = videoPlaylistSearch.DistributorId.HasValue ? videoPlaylistSearch.DistributorId : AppSettings.Current.Distribution.Automation.DistributorId;

                        videoPlaylistSearch.NewsInAccount = new[] {
                            new NewsInAccount
                            {
                                AccountId = officerId,
                                NewsId=videoPlaylistSearch.Id,
                                PublishedDate=DateTime.Now,
                                PublishedType=(int)PublishedType.Post
                            }
                        };
                        videoPlaylistSearch.CreatedBy = userId.ToString();
                        var url = Utility.UnicodeToUnsignedAndDash(videoPlaylist.Name ?? "");
                        videoPlaylistSearch.UnsignName = url;
                        videoPlaylistSearch.Url = string.Format(AppSettings.Current.ChannelConfiguration.CommonSettings.UrlFormatPlayList, url, videoPlaylistSearch.Id);

                        //Get PhotoInAlbum in publishData
                        var listItem = Foundation.Common.Json.Parse<PublishData>(videoPlaylist.PublishData);
                        videoPlaylistSearch.VideoInPlaylist = listItem?.Items.Select(p => new VideoInPlaylist
                        {
                            PlaylistId = videoPlaylist.Id,
                            VideoId = long.Parse(p.Id),
                            PlayOnTime = DateTime.Now
                        }).ToArray();

                        result = await VideoPlaylistService.AddAsync(videoPlaylistSearch, clientIP);
                        if (result == ErrorCodes.Success)
                        {
                            //insert log
                            await Function.AddToQueue(ActionName.InsertLog, TopicName.LOGACTION, new LogActionEntity
                            {
                                ObjectId = videoPlaylistSearch.Id.ToString(),
                                Account = userId.ToString(),
                                CreatedDate = DateTime.SpecifyKind(DateTime.Now, DateTimeKind.Unspecified),
                                Ip = clientIP,
                                Message = "Success.",
                                Title = videoPlaylistSearch.Name,
                                SourceId = officerId.ToString(),
                                ActionTypeDetail = (int)ActionType.Insert,
                                ActionText = ActionType.Insert.ToString(),
                                Type = (int)LogContentType.PlayList
                            });
                        }
                    }
                }
                else
                {
                    result = ErrorCodes.PermissionInvalid;
                }

                if (result == ErrorCodes.Success)
                {
                    return Ok(ActionResponse<object>.CreateSuccessResponse(videoPlaylist.EncryptId, "success"));
                }
                else
                {
                    return Ok(ActionResponse<string>.CreateErrorResponse(Current[result]));
                }
            }
            catch (Exception ex)
            {
                return Ok(ActionResponse<string>.CreateErrorResponse(ex.Message));
            }
        }

        /// <summary>
        /// Cập nhận trạng thái
        /// </summary>
        /// <param name="id"></param>
        /// <param name="action"></param>
        /// <param name="officerId"></param>
        /// <returns></returns>
        /// <response code="401">Authorization has been denied for this request</response>
        /// <response code="404">Not Found</response>
        /// <response code="405">Invalid input</response>
        // POST api/article/update_status       
        [HttpPost, Route("update_status")]
        public async Task<IActionResult> UpdateStatusV2Async([FromForm]long id, [FromForm]NewsStatus action, [FromForm] long officerId)
        {
            try
            {
                string sessionId = HttpContext.GetSessionId();
                if (string.IsNullOrEmpty(sessionId))
                {
                    return Ok(ActionResponse<string>.CreateErrorResponse("session-id not empty"));
                }
                var clientIP = HttpContext.GetClientIP();

                var result = ErrorCodes.BusinessError;                

                long.TryParse(User.FindFirst(ClaimTypes.NameIdentifier)?.Value, out long userId);

                result = await VideoPlaylistService.UpdateStatusV2Async(id, action, userId, clientIP, sessionId);
                if (result == ErrorCodes.Success)
                {
                    //insert log
                    await Function.AddToQueue(ActionName.InsertLog, TopicName.LOGACTION, new LogActionEntity
                    {
                        ObjectId = id.ToString(),
                        Account = userId.ToString(),
                        CreatedDate = DateTime.SpecifyKind(DateTime.Now, DateTimeKind.Unspecified),
                        Ip = clientIP,
                        Message = "Success.",
                        Title = "",
                        SourceId = officerId.ToString(),
                        ActionTypeDetail = (int)ActionType.UpdateStatus,
                        ActionText = ActionType.UpdateStatus.ToString(),
                        Type = (int)LogContentType.PlayList
                    });
                    return Ok(ActionResponse<object>.CreateSuccessResponse(id.ToString(), "success"));
                }
                return Ok(ActionResponse<string>.CreateErrorResponse(Current[result]));
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
                return Ok(ActionResponse<string>.CreateErrorResponse(ex.Message));
            }
        }

        /// <summary>
        /// Lấy thông tin chi tiết Playlist
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        /// <response code="401">Authorization has been denied for this request</response>
        /// <response code="404">Not Found</response>
        /// <response code="405">Invalid input</response>
        // POST api/videoplaylist/get_by_id        
        [HttpPost, Route("get_by_id")]
        public async Task<IActionResult> GetByIdAsync([FromForm] long id)
        {
            try
            {
                var result = await VideoPlaylistService.GetVideoPlaylistSearchByIdAsync(id);
                if (result != null && result.Id > 0) return Ok(ActionResponse<VideoPlaylistSearch>.CreateSuccessResponse(result));
                return Ok(ActionResponse<string>.CreateErrorResponse("Playlist not exist."));
            }
            catch (Exception ex)
            {
                return Ok(ActionResponse<string>.CreateErrorResponse(ex.Message));
            }
        }

        /// <summary>
        /// Tìm kiếm Playlist
        /// </summary>
        /// <param name="searchEntity"></param>
        /// <returns></returns>
        /// <response code="401">Authorization has been denied for this request</response>
        /// <response code="404">Not Found</response>
        /// <response code="405">Invalid input</response>
        // POST api/videoplaylist/search
        [HttpPost, Route("search")]
        public async Task<IActionResult> SearchAsync([FromForm]SearchNews searchEntity)
        {
            try
            {
                if (searchEntity == null)
                {
                    return Ok(ActionResponse<VideoPlaylistSearch>.CreateSuccessResponse("Không tìm thấy video playlist nào."));
                }
                searchEntity.PageIndex = searchEntity.PageIndex < 1 ? 1 : searchEntity.PageIndex;
                searchEntity.PageSize = searchEntity.PageSize < 1 ? 20 : searchEntity.PageSize;

                searchEntity.FromDate = searchEntity.FromDate.HasValue ? searchEntity.FromDate.Value.AddHours(0).AddMinutes(0).AddSeconds(0) : DateTime.MinValue;
                searchEntity.ToDate = searchEntity.ToDate.HasValue ? searchEntity.ToDate.Value.AddHours(23).AddMinutes(59).AddSeconds(59) : DateTime.MaxValue;

                long.TryParse(User.FindFirst(ClaimTypes.NameIdentifier)?.Value, out long userId);

                var result = await VideoPlaylistService.SearchAsync(searchEntity, userId);
                return Ok(ActionResponse<PagingDataResult<VideoPlaylistSearchReturn>>.CreateSuccessResponse(result));
            }
            catch (Exception ex)
            {
                return Ok(ActionResponse<string>.CreateErrorResponse(ex.Message));
            }
        }

        /// <summary>
        /// Lấy danh sách playlist đang chờ duyệt
        /// </summary>
        /// <param name="searchEntity"></param>
        /// <returns></returns>
        /// <response code="401">Authorization has been denied for this request</response>
        /// <response code="404">Not Found</response>
        /// <response code="405">Invalid input</response>
        [HttpPost, Route("get_list_approve")]
        public async Task<IActionResult> GetListApproveAsync([FromForm]SearchNews searchEntity)
        {
            try
            {
                if (searchEntity == null)
                {
                    return Ok(ActionResponse<string>.CreateSuccessResponse("Không tìm thấy playlist nào."));
                }
                if (searchEntity.OfficerId <= 0)
                {
                    return Ok(ActionResponse<string>.CreateErrorResponse("OfficerId không hợp lệ."));
                }

                searchEntity.Status = ((int)NewsStatus.Approve).ToString();
                searchEntity.PageIndex = searchEntity.PageIndex < 1 ? 1 : searchEntity.PageIndex;
                searchEntity.PageSize = searchEntity.PageSize < 1 ? 20 : searchEntity.PageSize;
                searchEntity.FromDate = searchEntity.FromDate.HasValue ? searchEntity.FromDate.Value.AddHours(0).AddMinutes(0).AddSeconds(0) : DateTime.MinValue;
                searchEntity.ToDate = searchEntity.ToDate.HasValue ? searchEntity.ToDate.Value.AddHours(23).AddMinutes(59).AddSeconds(59) : DateTime.Now;
                long.TryParse(User.FindFirst(ClaimTypes.NameIdentifier)?.Value, out long accountId);

                var result = await VideoPlaylistService.GetListApproveAsync(searchEntity, accountId);

                return Ok(ActionResponse<PagingDataResult<VideoPlaylistSearch>>.CreateSuccessResponse(result));
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
                return Ok(ActionResponse<string>.CreateErrorResponse(ex.Message));
            }
        }

        /// <summary>
        /// Danh sách Playlist liên quan
        /// </summary>
        /// <param name="videoPlaylistId"></param>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        /// <response code="401">Authorization has been denied for this request</response>
        /// <response code="404">Not Found</response>
        /// <response code="405">Invalid input</response>
        // POST api/videoplaylist/get_list_relation
        [HttpPost, Route("get_list_relation")]
        public async Task<IActionResult> GetListRelationAsync([FromForm]long videoPlaylistId, [FromForm]int? pageIndex, [FromForm] int? pageSize)
        {
            try
            {
                try
                {
                    if (videoPlaylistId == 0)
                    {
                        return Ok(ActionResponse<VideoPlaylistSearch>.CreateSuccessResponse("Playlist không tồn tại."));
                    }
                    pageIndex = pageIndex < 1 ? 1 : pageIndex;
                    pageSize = pageSize < 1 ? 20 : pageSize;
                    var playlist = await VideoPlaylistService.GetByIdAsync(videoPlaylistId);
                    if (playlist.PlaylistRelation != null && playlist.PlaylistRelation.Length > 0)
                    {
                        var result = await VideoPlaylistService.GetListRelationAsync(playlist.PlaylistRelation, pageIndex, pageSize);
                        return Ok(ActionResponse<PagingDataResult<VideoPlaylistSearch>>.CreateSuccessResponse(result));
                    }
                    return Ok(ActionResponse<PagingDataResult<VideoPlaylistSearch>>.CreateSuccessResponse(new PagingDataResult<VideoPlaylistSearch>()));
                }
                catch (Exception ex)
                {
                    return Ok(ActionResponse<string>.CreateErrorResponse(ex.Message));
                }
            }
            catch (Exception ex)
            {
                return Ok(ActionResponse<string>.CreateErrorResponse(ex.Message));
            }
        }

        /// <summary>
        /// Số lượng Playlist theo người dùng
        /// </summary>
        /// <param name="status"></param>
        /// <param name="officialId"></param>
        /// <returns></returns>
        /// <response code="401">Authorization has been denied for this request</response>
        /// <response code="404">Not Found</response>
        /// <response code="405">Invalid input</response>
        // POST api/videoplaylist/get_count_by_status
        [HttpPost, Route("get_count_by_status")]
        public async Task<IActionResult> GetCountByStatusAsync([FromForm]NewsStatus status, [FromForm] long? officialId)
        {
            try
            {
                long.TryParse(User.FindFirst(ClaimTypes.NameIdentifier)?.Value, out long userId);

                var result = await VideoPlaylistService.GetCountByStatusAsync((int)status, officialId ?? 0, userId);
                return Ok(ActionResponse<object>.CreateSuccessResponse(new { Count = result.ToString() }));
            }
            catch (Exception ex)
            {
                return Ok(ActionResponse<string>.CreateErrorResponse(ex.Message));
            }
        }

        /// <summary>
        /// Danh sách video trong playlist
        /// </summary>
        /// <param name="playlistId"></param>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        /// <response code="401">Authorization has been denied for this request</response>
        /// <response code="404">Not Found</response>
        /// <response code="405">Invalid input</response>
        // POST api/videoplaylist/get_video_by_playlist_id => getway index 93
        [HttpPost, Route("get_video_by_playlist_id")]
        public async Task<IActionResult> GetVideoByPlaylistIdAsync([FromForm]long playlistId, [FromForm] int? pageIndex, [FromForm] int? pageSize)
        {
            try
            {
                pageIndex = pageIndex == null || pageIndex < 1 ? 1 : pageIndex;
                pageSize = pageSize == null || pageSize < 1 ? 20 : pageSize;

                var result = await VideoPlaylistService.GetVideoByPlaylistIdAsync(playlistId, pageIndex, pageSize);
                return Ok(ActionResponse<PagingDataResult<Video>>.CreateSuccessResponse(result));
            }
            catch (Exception ex)
            {
                return Ok(ActionResponse<string>.CreateErrorResponse(ex.Message));
            }
        }

        /// <summary>
        /// Tìm kiếm video để add vào playlist
        /// </summary>
        /// <param name="searchEntity"></param>
        /// <returns></returns>
        /// <response code="401">Authorization has been denied for this request</response>
        /// <response code="404">Not Found</response>
        /// <response code="405">Invalid input</response>
        // POST api/video/search => getway index 71
        [HttpPost, Route("search_video")]
        public async Task<IActionResult> SearchVideoAsync([FromForm]SearchNews searchEntity)
        {
            try
            {
                if (searchEntity == null)
                {
                    return Ok(ActionResponse<VideoSearch>.CreateSuccessResponse("Không tìm thấy video nào."));
                }
                searchEntity.PageIndex = searchEntity.PageIndex < 1 ? 1 : searchEntity.PageIndex;
                searchEntity.PageSize = searchEntity.PageSize < 1 ? 20 : searchEntity.PageSize;
                long.TryParse(User.FindFirst(ClaimTypes.NameIdentifier)?.Value, out long userId);
                if (searchEntity.OfficerId == null || searchEntity.OfficerId == 0) searchEntity.OfficerId = userId;
                var result = await VideoPlaylistService.SearchVideoAsync(searchEntity, userId);

                return Ok(ActionResponse<PagingDataResult<VideoInPlaylistSearchReturn>>.CreateSuccessResponse(result));
            }
            catch (Exception ex)
            {
                return Ok(ActionResponse<string>.CreateErrorResponse(ex.Message));
            }
        }

        /// <summary>
        /// Add vào bảng tin
        /// </summary>
        /// <param name="mediaId"></param>
        /// <param name="boardIds"></param>
        /// <param name="officerId"></param>
        /// <returns></returns>
        /// <response code="401">Authorization has been denied for this request</response>
        /// <response code="404">Not Found</response>
        /// <response code="405">Invalid input</response>
        // POST api/videoplaylist/get_video_by_playlist_id => getway index 93
        [HttpPost, Route("add_to_board")]
        public async Task<IActionResult> AddToBoardAsync([FromForm]long mediaId, [FromForm] long[] boardIds, [FromForm]long officerId)
        {
            try
            {
                var clientIP = string.IsNullOrEmpty(HttpContext?.Connection?.RemoteIpAddress?.ToString()) ? "192.168.1.1" : HttpContext?.Connection?.RemoteIpAddress?.ToString();
                long.TryParse(User.FindFirst(ClaimTypes.NameIdentifier)?.Value, out long userId);
                var dataDb = await BoardService.GetListByIdAsync(boardIds);
                if (dataDb == null)
                {
                    return Ok(ActionResponse<string>.CreateErrorResponse("Bảng tin không tồn tại."));
                }
                else
                {
                    if (!(await BoardService.CheckBoardOfUser(officerId, userId, boardIds)))
                    {
                        return Ok(ActionResponse<string>.CreateErrorResponse("Bạn không có quyền chỉnh sửa Bảng tin."));
                    }
                }

                dataDb.ForEach(p =>
                {
                    p.LastInsertedDate = DateTime.Now;
                    p.ModifiedBy = userId.ToString();
                    p.ModifiedDate = DateTime.Now;
                });
                var result = await BoardService.InsertMediaAsync(mediaId, dataDb);
                if (result)
                {
                    //share to app
                    foreach (var boardId in boardIds)
                    {
                        await Function.AddToQueue(ActionName.AddNewsToBoard, TopicName.PUSHBOARDTOCHANNEL, new QueueData<long, string, long, long>
                        {
                            Data1 = boardId,
                            Data2 = mediaId.ToString(),
                            Data3 = officerId,
                            Data4 = userId
                        });
                    }                    

                    //insert log
                    await Function.AddToQueue(ActionName.InsertLog, TopicName.LOGACTION, new LogActionEntity
                    {
                        ObjectId = mediaId.ToString(),
                        Account = userId.ToString(),
                        CreatedDate = DateTime.SpecifyKind(DateTime.Now, DateTimeKind.Unspecified),
                        Ip = clientIP,
                        Message = "Success.",
                        Title = "",
                        SourceId = officerId.ToString(),
                        ActionTypeDetail = (int)ActionType.AddMediaToBoard,
                        ActionText = ActionType.AddMediaToBoard.ToString(),
                        Type = (int)LogContentType.PlayList
                    });
                    return Ok(ActionResponse<string>.CreateSuccessResponse("Add album success."));
                }
                return Ok(ActionResponse<string>.CreateErrorResponse("Add album unsuccess."));
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
                return Ok(ActionResponse<string>.CreateErrorResponse(ex.Message));
            }
        }
    }
}
