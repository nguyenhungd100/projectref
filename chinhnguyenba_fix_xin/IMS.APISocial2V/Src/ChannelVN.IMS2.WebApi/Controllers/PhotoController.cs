﻿using ChannelVN.IMS2.Core.Entities;
using ChannelVN.IMS2.Core.Entities.Distribution;
using ChannelVN.IMS2.Core.Entities.Media;
using ChannelVN.IMS2.Core.Entities.Photo;
using ChannelVN.IMS2.Core.Entities.Security;
using ChannelVN.IMS2.Core.Repositories.Constants;
using ChannelVN.IMS2.Core.Repositories.Functions;
using ChannelVN.IMS2.Core.Services;
using ChannelVN.IMS2.Foundation.Common;
using ChannelVN.IMS2.Foundation.Common.Configuration;
using ChannelVN.IMS2.Foundation.Logging;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using static ChannelVN.IMS2.Core.Entities.ErrorCode.ErrorMapping;

namespace ChannelVN.IMS2.WebApi.Controllers
{
    [Route("api/[controller]")]
    public class PhotoController : BaseController
    {
        /// <summary>
        /// Tạo ảnh đơn
        /// </summary>
        /// <param name="photoData"></param>
        /// <param name="officerId"></param>
        /// <returns></returns>
        // POST api/Photo/save        
        [HttpPost, Route("save")]
        public async Task<IActionResult> Save([FromForm]PhotoUnit photoData, [FromForm]long officerId)
        {
            try
            {
                string sessionId = HttpContext.GetSessionId();
                if (string.IsNullOrEmpty(sessionId))
                {
                    return Ok(ActionResponse<string>.CreateErrorResponse("session-id not empty"));
                }
                var clientIP = HttpContext.GetClientIP();

                var result = ErrorCodes.BusinessError;                
                var validateResult = ValidatePhoto(photoData);
                if (validateResult?.Success == false)
                {
                    return Ok(ActionResponse<string>.CreateErrorResponse(validateResult.Message));
                }

                var validPublishData = await Core.Services.Functions.Function.ValidatePublishData(photoData.PublishData, (int)CardType.Photo);
                if (!validPublishData)
                {
                    return Ok(ActionResponse<string>.CreateErrorResponse("PublishData invalid."));
                }

                if (long.TryParse(User.FindFirst(ClaimTypes.NameIdentifier)?.Value, out long accountId))
                {
                    if (photoData.Id > 0)
                    {
                        //update
                        photoData.CategoryId = photoData.CategoryId ?? 0;
                        photoData.Url = string.Format(AppSettings.Current.ChannelConfiguration.CommonSettings.UrlFormatPhoto, Utility.UnicodeToUnsignedAndDash(photoData.GetType().Name), photoData.Id);
                        photoData.ModifiedDate = DateTime.Now;
                        photoData.ModifiedBy = accountId + "";
                       
                        result = await PhotoService.UpdateAsync(photoData, officerId, clientIP, sessionId);
                        if (result == ErrorCodes.Success)
                        {
                            //insert log
                            await Function.AddToQueue(ActionName.InsertLog, TopicName.LOGACTION, new LogActionEntity
                            {
                                ObjectId = photoData.Id.ToString(),
                                Account = accountId.ToString(),
                                CreatedDate = DateTime.SpecifyKind(DateTime.Now, DateTimeKind.Unspecified),
                                Ip = clientIP,
                                Message = "Success.",
                                Title = photoData.Caption,
                                SourceId = officerId.ToString(),
                                ActionTypeDetail = (int)ActionType.Update,
                                ActionText = ActionType.Update.ToString(),
                                Type = (int)LogContentType.Photo
                            });
                        }
                    }
                    else
                    {
                        var Photo = Mapper<PhotoSearch>.Map(photoData, new PhotoSearch());
                        //insert
                        Photo.Id = Generator.NewsId();
                        photoData.Id = Photo.Id;
                        photoData.CategoryId = photoData.CategoryId ?? 0;
                        Photo.Url = string.Format(AppSettings.Current.ChannelConfiguration.CommonSettings.UrlFormatPhoto, Utility.UnicodeToUnsignedAndDash(Photo.GetType().Name), Photo.Id);
                        if (string.IsNullOrEmpty(Photo.OriginalUrl))
                            Photo.OriginalUrl = Photo.Url;
                        Photo.CardType = (int)CardType.Photo;//Photo.CardType < 0 ? (int)CardType.Photo: Photo.CardType;
                        Photo.Type = (int)NewsType.Photo;
                        Photo.PublishMode = (int)NewsPublishMode.Public;
                        Photo.Status = (int)NewsStatus.Draft;
                        Photo.CreatedDate = DateTime.Now;
                        Photo.CreatedBy = accountId + "";
                        Photo.DistributorId = Photo.DistributorId.HasValue ? Photo.DistributorId : AppSettings.Current.Distribution.Automation.DistributorId;
                        Photo.NewsInAccount = new[] {
                            new NewsInAccount
                            {
                                AccountId = officerId,
                                NewsId=Photo.Id,
                                PublishedDate=DateTime.Now,
                                PublishedType=(int)NewsPublishedType.Post
                            }
                        };

                        result = await PhotoService.AddAsync(Photo, clientIP);
                        if (result == ErrorCodes.Success)
                        {
                            //insert log
                            await Function.AddToQueue(ActionName.InsertLog, TopicName.LOGACTION, new LogActionEntity
                            {
                                ObjectId = Photo.Id.ToString(),
                                Account = accountId.ToString(),
                                CreatedDate = DateTime.SpecifyKind(DateTime.Now, DateTimeKind.Unspecified),
                                Ip = clientIP,
                                Message = "Success.",
                                Title = Photo.Caption,
                                SourceId = officerId.ToString(),
                                ActionTypeDetail = (int)ActionType.Insert,
                                ActionText = ActionType.Insert.ToString(),
                                Type = (int)LogContentType.Photo
                            });
                        }
                    }
                }

                if (result == ErrorCodes.Success)
                {
                    return Ok(ActionResponse<object>.CreateSuccessResponse(photoData.Id.ToString(), "Success."));
                }
                else
                {
                    return Ok(ActionResponse<string>.CreateErrorResponse(Current[result]));
                }
            }
            catch (Exception ex)
            {
                return Ok(ActionResponse<string>.CreateErrorResponse(ex.Message));
            }
        }


        /// <summary>
        /// Tạo ảnh đơn cho album
        /// </summary>
        /// <param name="PhotoData"></param>
        /// <param name="officerId"></param>
        /// <returns></returns>
        // POST api/Photo/save        
        [HttpPost, Route("archive")]
        public async Task<IActionResult> ArchiveAsync([FromForm]PhotoUnit PhotoData, [FromForm]long officerId)
        {
            try
            {
                string sessionId = HttpContext?.Request?.Headers?["session-id"];
                if (string.IsNullOrEmpty(sessionId))
                {
                    return Ok(ActionResponse<string>.CreateErrorResponse("session-id not empty"));
                }
                var clientIP = HttpContext.GetClientIP();

                var result = ErrorCodes.BusinessError;
                var validateResult = ValidatePhoto(PhotoData);
                if (validateResult?.Success == false)
                {
                    return Ok(ActionResponse<string>.CreateErrorResponse(validateResult.Message));
                }

                if (long.TryParse(User.FindFirst(ClaimTypes.NameIdentifier)?.Value, out long accountId))
                {
                    if (PhotoData.Id > 0)
                    {
                        //update
                        PhotoData.Url = string.Format(AppSettings.Current.ChannelConfiguration.CommonSettings.UrlFormatPhoto, Utility.UnicodeToUnsignedAndDash(PhotoData.GetType().Name), PhotoData.Id);
                        PhotoData.ModifiedDate = DateTime.Now;
                        PhotoData.ModifiedBy = accountId + "";

                        result = await PhotoService.UpdateAsync(PhotoData, officerId, clientIP, sessionId);
                        if (result == ErrorCodes.Success)
                        {
                            //insert log
                            await Function.AddToQueue(ActionName.InsertLog, TopicName.LOGACTION, new LogActionEntity
                            {
                                ObjectId = PhotoData.Id.ToString(),
                                Account = accountId.ToString(),
                                CreatedDate = DateTime.SpecifyKind(DateTime.Now, DateTimeKind.Unspecified),
                                Ip = clientIP,
                                Message = "Success.",
                                Title = PhotoData.Caption,
                                SourceId = officerId.ToString(),
                                ActionTypeDetail = (int)ActionType.Update,
                                ActionText = ActionType.Update.ToString(),
                                Type = (int)LogContentType.Photo
                            });
                        }
                    }
                    else
                    {
                        var Photo = Mapper<PhotoSearch>.Map(PhotoData, new PhotoSearch());
                        //insert
                        Photo.Id = Generator.NewsId();
                        PhotoData.Id = Photo.Id;
                        Photo.Url = string.Format(AppSettings.Current.ChannelConfiguration.CommonSettings.UrlFormatPhoto, Utility.UnicodeToUnsignedAndDash(Photo.GetType().Name), Photo.Id);
                        if (string.IsNullOrEmpty(Photo.OriginalUrl))
                            Photo.OriginalUrl = Photo.Url;
                        Photo.CardType = (int)CardType.Photo;
                        Photo.Type = (int)NewsType.Photo;
                        Photo.PublishMode = (int)NewsPublishMode.Public;
                        Photo.Status = (int)NewsStatus.Archive;
                        Photo.CreatedDate = DateTime.Now;
                        Photo.CreatedBy = accountId + "";
                        Photo.DistributorId = Photo.DistributorId.HasValue ? Photo.DistributorId : AppSettings.Current.Distribution.Automation.DistributorId;

                        Photo.NewsInAccount = new[] {
                            new NewsInAccount
                            {
                                AccountId = officerId,
                                NewsId = Photo.Id,
                                PublishedDate = DateTime.Now,
                                PublishedType = (int)NewsPublishedType.Post
                            }
                        };

                        result = await PhotoService.AddAsync(Photo, clientIP);
                        if (result == ErrorCodes.Success)
                        {
                            //insert log
                            await Function.AddToQueue(ActionName.InsertLog, TopicName.LOGACTION, new LogActionEntity
                            {
                                ObjectId = Photo.Id.ToString(),
                                Account = accountId.ToString(),
                                CreatedDate = DateTime.SpecifyKind(DateTime.Now, DateTimeKind.Unspecified),
                                Ip = clientIP,
                                Message = "Success.",
                                Title = Photo.Caption,
                                SourceId = officerId.ToString(),
                                ActionTypeDetail = (int)ActionType.Insert,
                                ActionText = ActionType.Insert.ToString(),
                                Type = (int)LogContentType.Photo
                            });
                        }
                    }
                }

                if (result == ErrorCodes.Success)
                {
                    return Ok(ActionResponse<object>.CreateSuccessResponse(PhotoData.Id.ToString(), "Success."));
                }
                else
                {
                    return Ok(ActionResponse<string>.CreateErrorResponse(Current[result]));
                }
            }
            catch (Exception ex)
            {
                return Ok(ActionResponse<string>.CreateErrorResponse(ex.Message));
            }
        }

        private ActionResponse<string> ValidatePhoto(PhotoUnit media)
        {
            if (media == null)
            {
                return ActionResponse<string>.CreateErrorResponse("Params not valid.");
            }
            if (!Utility.ValidateStringIds(media.Tags))
                return ActionResponse<string>.CreateErrorResponse("Tags không hợp lệ.");
            if (string.IsNullOrEmpty(media.PublishData))
                return ActionResponse<string>.CreateErrorResponse("Định dạng PublishData không đúng!");

            return ActionResponse<string>.CreateSuccessResponse("Success.");
        }

        /// <summary>
        /// Cập nhận trạng thái
        /// </summary>
        /// <param name="id"></param>
        /// <param name="action"></param>
        /// <param name="officerId"></param>
        /// <returns></returns>
        /// <response code="401">Authorization has been denied for this request</response>
        /// <response code="404">Not Found</response>
        /// <response code="405">Invalid input</response>
        // POST api/article/update_status       
        [HttpPost, Route("update_status")]
        public async Task<IActionResult> UpdateStatusV2Async([FromForm]long id, [FromForm]NewsStatus action, [FromForm] long officerId)
        {
            try
            {
                string sessionId = HttpContext.GetSessionId();
                if (string.IsNullOrEmpty(sessionId))
                {
                    return Ok(ActionResponse<string>.CreateErrorResponse("session-id not empty"));
                }
                var clientIP = HttpContext.GetClientIP();

                var result = ErrorCodes.BusinessError;                
                long.TryParse(User.FindFirst(ClaimTypes.NameIdentifier)?.Value, out long userId);
                result = await PhotoService.UpdateStatusV2Async(id, action, userId, clientIP, sessionId);
                if (result == ErrorCodes.Success)
                {
                    //insert log
                    await Function.AddToQueue(ActionName.InsertLog, TopicName.LOGACTION, new LogActionEntity
                    {
                        ObjectId = id.ToString(),
                        Account = userId.ToString(),
                        CreatedDate = DateTime.SpecifyKind(DateTime.Now, DateTimeKind.Unspecified),
                        Ip = clientIP,
                        Message = "Success.",
                        Title = "",
                        SourceId = officerId.ToString(),
                        ActionTypeDetail = (int)ActionType.UpdateStatus,
                        ActionText = ActionType.UpdateStatus.ToString(),
                        Type = (int)LogContentType.Photo
                    });
                    return Ok(ActionResponse<object>.CreateSuccessResponse(id.ToString(), "success"));
                }
                return Ok(ActionResponse<string>.CreateErrorResponse(Current[result]));
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
                return Ok(ActionResponse<string>.CreateErrorResponse(ex.Message));
            }
        }

        /// <summary>
        /// Lấy thông tin chi tiết Photo
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        /// <response code="401">Authorization has been denied for this request</response>
        /// <response code="404">Not Found</response>
        /// <response code="405">Invalid input</response>
        // POST api/Photo/get_by_id
        [HttpPost, Route("get_by_id")]
        public async Task<IActionResult> GetById([FromForm]long Id)
        {
            try
            {
                var result = await PhotoService.GetByIdAsync(Id);
                if (result != null && result.Id > 0) return Ok(ActionResponse<PhotoSearch>.CreateSuccessResponse(result));
                return Ok(ActionResponse<string>.CreateErrorResponse("Photo not exist."));
            }
            catch (Exception ex)
            {
                return Ok(ActionResponse<string>.CreateErrorResponse(ex.Message));
            }
        }

        /// <summary>
        /// Tìm kiếm Photo
        /// </summary>
        /// <param name="search"></param>
        /// <returns></returns>
        /// <response code="401">Authorization has been denied for this request</response>
        /// <response code="404">Not Found</response>
        /// <response code="405">Invalid input</response>
        // POST api/Photo/search        
        [HttpPost, Route("search")]
        public async Task<IActionResult> Search([FromForm] SearchNews search)
        {
            try
            {
                if (search == null)
                {
                    return Ok(ActionResponse<string>.CreateErrorResponse("Object search null."));
                }

                search.PageIndex = search.PageIndex < 1 ? 1 : search.PageIndex;
                search.PageSize = search.PageSize < 1 ? 20 : search.PageSize;
                search.FromDate = search.FromDate.HasValue ? search.FromDate.Value.AddHours(0).AddMinutes(0).AddSeconds(0) : DateTime.MinValue;
                search.ToDate = search.ToDate.HasValue ? search.ToDate.Value.AddHours(23).AddMinutes(59).AddSeconds(59) : DateTime.MaxValue;
                long.TryParse(User.FindFirst(ClaimTypes.NameIdentifier)?.Value, out long accountId);
                if (search.OfficerId == null || search.OfficerId == 0) search.OfficerId = accountId;
                var result = await PhotoService.SearchAsync(search, accountId);
                if (result != null)
                {
                    return Ok(ActionResponse<PagingDataResult<PhotoSearchReturn>>.CreateSuccessResponse(result));
                }

                return Ok(ActionResponse<PagingDataResult<PhotoSearchReturn>>.CreateSuccessResponse(new PagingDataResult<PhotoSearchReturn>()));
            }
            catch (Exception ex)
            {
                return Ok(ActionResponse<string>.CreateErrorResponse(ex.Message));
            }
        }

        /// <summary>
        /// Lấy danh sách photo đang chờ duyệt
        /// </summary>
        /// <param name="searchEntity"></param>
        /// <returns></returns>
        /// <response code="401">Authorization has been denied for this request</response>
        /// <response code="404">Not Found</response>
        /// <response code="405">Invalid input</response>
        [HttpPost, Route("get_list_approve")]
        public async Task<IActionResult> GetListApproveAsync([FromForm]SearchNews searchEntity)
        {
            try
            {
                if (searchEntity == null)
                {
                    return Ok(ActionResponse<string>.CreateSuccessResponse("Không tìm thấy photo nào."));
                }
                if (searchEntity.OfficerId <= 0)
                {
                    return Ok(ActionResponse<string>.CreateErrorResponse("OfficerId không hợp lệ."));
                }

                searchEntity.Status = ((int)NewsStatus.Approve).ToString();
                searchEntity.PageIndex = searchEntity.PageIndex < 1 ? 1 : searchEntity.PageIndex;
                searchEntity.PageSize = searchEntity.PageSize < 1 ? 20 : searchEntity.PageSize;
                searchEntity.FromDate = searchEntity.FromDate.HasValue ? searchEntity.FromDate.Value.AddHours(0).AddMinutes(0).AddSeconds(0) : DateTime.MinValue;
                searchEntity.ToDate = searchEntity.ToDate.HasValue ? searchEntity.ToDate.Value.AddHours(23).AddMinutes(59).AddSeconds(59) : DateTime.Now;
                long.TryParse(User.FindFirst(ClaimTypes.NameIdentifier)?.Value, out long accountId);

                var result = await PhotoService.GetListApproveAsync(searchEntity, accountId);

                return Ok(ActionResponse<PagingDataResult<PhotoUnit>>.CreateSuccessResponse(result));
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
                return Ok(ActionResponse<string>.CreateErrorResponse(ex.Message));
            }
        }

        /// <summary>
        /// Add Photo to Album
        /// </summary>
        /// <param name="photoId"></param>
        /// <param name="albumIds"></param>
        /// <param name="officerId"></param>
        /// <returns></returns>
        /// <response code="401">Authorization has been denied for this request</response>
        /// <response code="404">Not Found</response>
        /// <response code="405">Invalid input</response>
        // POST api/videoplaylist/get_video_by_playlist_id => getway index 93
        [HttpPost, Route("add_to_album")]
        public async Task<IActionResult> AddToAlbumAsync([FromForm]long photoId, [FromForm]long[] albumIds, [FromForm] long officerId)
        {
            try
            {
                var clientIP = string.IsNullOrEmpty(HttpContext?.Connection?.RemoteIpAddress?.ToString()) ? "192.168.1.1" : HttpContext?.Connection?.RemoteIpAddress?.ToString();
                long.TryParse(User.FindFirst(ClaimTypes.NameIdentifier)?.Value, out long userId);
                var photo = await PhotoService.GetByIdAsync(photoId);
                if (photo == null)
                {
                    return Ok(ActionResponse<string>.CreateErrorResponse("Photo không tồn tại."));
                }
                if (!userId.ToString().Equals(photo.CreatedBy))
                {
                    return Ok(ActionResponse<string>.CreateErrorResponse("Photo không phải của bạn."));
                }
                var dataDb = await AlbumService.GetListByIdAsync(albumIds);
                if (dataDb == null)
                {
                    return Ok(ActionResponse<string>.CreateErrorResponse("Album không tồn tại."));
                }
                else
                {
                    if (dataDb?.Where(p => !userId.ToString().Equals(p.CreatedBy))?.Count() > 0)
                    {
                        return Ok(ActionResponse<string>.CreateErrorResponse("Bạn không có quyền chỉnh sửa Album (id = " + (dataDb?.Where(p => !userId.Equals(p.CreatedBy))?.FirstOrDefault()?.Id + ").")));
                    }
                }

                var publishDataPhoto = Foundation.Common.Json.Parse<PublishData>(photo.PublishData);
                dataDb.ForEach(p =>
                {
                    p.LastInsertedDate = DateTime.Now;
                    p.ModifiedBy = userId.ToString();
                    p.ModifiedDate = DateTime.Now;
                    var publishDataAlbum = Foundation.Common.Json.Parse<PublishData>(p.PublishData);
                    if (publishDataAlbum != null && publishDataAlbum.Items != null && publishDataPhoto != null && publishDataPhoto.Items != null && publishDataPhoto.Items.Count() > 0)
                    {
                        var firstItem = publishDataPhoto.Items.FirstOrDefault();
                        if(firstItem != null)
                        {
                            firstItem.Id = photo.Id.ToString();
                            if (publishDataAlbum.Items.Where(w => firstItem.Id.Equals(w.Id?.ToString()))?.Count() == 0)
                            {
                                publishDataAlbum.Items.Add(firstItem);
                                p.PublishData = Foundation.Common.Json.StringifyFirstLowercase(publishDataAlbum);
                                //p.PublishData = Foundation.Common.Json.Stringify(new
                                //{
                                //    title = publishDataAlbum.Title,
                                //    items = publishDataAlbum.Items.Select(s => new
                                //    {
                                //        title = s.Title,
                                //        link = s.Link,
                                //        thumb = s.Thumb,
                                //        width = s.Width,
                                //        height = s.Height,
                                //        content_type = s.ContentType,
                                //        id = s.Id
                                //    })?.ToArray()
                                //});
                            }
                            else
                            {
                                publishDataAlbum.Items.RemoveAll(w => w.Id.Equals(firstItem.Id));
                                publishDataAlbum.Items.Add(firstItem);
                                p.PublishData = Foundation.Common.Json.StringifyFirstLowercase(publishDataAlbum);
                            }
                        }
                    }
                });

                var result = await PhotoService.AddToAlbumAsync(photoId, dataDb);
                if (result)
                {
                    //insert log
                    await Function.AddToQueue(ActionName.InsertLog, TopicName.LOGACTION, new LogActionEntity
                    {
                        ObjectId = photoId.ToString(),
                        Account = userId.ToString(),
                        CreatedDate = DateTime.SpecifyKind(DateTime.Now, DateTimeKind.Unspecified),
                        Ip = clientIP,
                        Message = "Success.",
                        Title = photo.Caption,
                        SourceId = officerId.ToString(),
                        ActionTypeDetail = (int)ActionType.AddPhotoToAlbum,
                        ActionText = ActionType.AddPhotoToAlbum.ToString(),
                        Type = (int)LogContentType.Photo
                    });
                    return Ok(ActionResponse<string>.CreateSuccessResponse("Add photo success."));
                }
                return Ok(ActionResponse<string>.CreateErrorResponse("Add photo unsuccess."));
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
                return Ok(ActionResponse<string>.CreateErrorResponse(ex.Message));
            }
        }

        /// <summary>
        /// Add vào bảng tin
        /// </summary>
        /// <param name="mediaId"></param>
        /// <param name="boardIds"></param>
        /// <param name="officerId"></param>
        /// <returns></returns>
        /// <response code="401">Authorization has been denied for this request</response>
        /// <response code="404">Not Found</response>
        /// <response code="405">Invalid input</response>
        // POST api/videoplaylist/get_video_by_playlist_id => getway index 93
        [HttpPost, Route("add_to_board")]
        public async Task<IActionResult> AddToBoardAsync([FromForm]long mediaId, [FromForm] long[] boardIds, [FromForm]long officerId)
        {
            try
            {
                var clientIP = string.IsNullOrEmpty(HttpContext?.Connection?.RemoteIpAddress?.ToString()) ? "192.168.1.1" : HttpContext?.Connection?.RemoteIpAddress?.ToString();
                long.TryParse(User.FindFirst(ClaimTypes.NameIdentifier)?.Value, out long userId);
                var dataDb = await BoardService.GetListByIdAsync(boardIds);
                if (dataDb == null)
                {
                    return Ok(ActionResponse<string>.CreateErrorResponse("Bảng tin không tồn tại."));
                }
                else
                {
                    if (!(await BoardService.CheckBoardOfUser(officerId, userId, boardIds)))
                    {
                        return Ok(ActionResponse<string>.CreateErrorResponse("Bạn không có quyền chỉnh sửa Bảng tin."));
                    }
                }

                dataDb.ForEach(p =>
                {
                    p.LastInsertedDate = DateTime.Now;
                    p.ModifiedBy = userId.ToString();
                    p.ModifiedDate = DateTime.Now;
                });
                var result = await BoardService.InsertMediaAsync(mediaId, dataDb);
                if (result)
                {
                    foreach (var boardId in boardIds)
                    {
                        await Function.AddToQueue(ActionName.AddNewsToBoard, TopicName.PUSHBOARDTOCHANNEL, new QueueData<long, string, long, long>
                        {
                            Data1 = boardId,
                            Data2 = mediaId.ToString(),
                            Data3 = officerId,
                            Data4 = userId
                        });
                    }
                    
                    //insert log
                    await Function.AddToQueue(ActionName.InsertLog, TopicName.LOGACTION, new LogActionEntity
                    {
                        ObjectId = mediaId.ToString(),
                        Account = userId.ToString(),
                        CreatedDate = DateTime.SpecifyKind(DateTime.Now, DateTimeKind.Unspecified),
                        Ip = clientIP,
                        Message = "Success.",
                        Title = "",
                        SourceId = officerId.ToString(),
                        ActionTypeDetail = (int)ActionType.AddMediaToBoard,
                        ActionText = ActionType.AddMediaToBoard.ToString(),
                        Type = (int)LogContentType.Photo
                    });
                    return Ok(ActionResponse<string>.CreateSuccessResponse("Add album success."));
                }
                return Ok(ActionResponse<string>.CreateErrorResponse("Add album unsuccess."));
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
                return Ok(ActionResponse<string>.CreateErrorResponse(ex.Message));
            }
        }
    }
}
