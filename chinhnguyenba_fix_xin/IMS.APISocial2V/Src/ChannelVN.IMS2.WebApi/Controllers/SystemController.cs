﻿using ChannelVN.IMS2.Core.Entities;
using ChannelVN.IMS2.Core.Entities.AccountAssignment;
using ChannelVN.IMS2.Core.Entities.Comment;
using ChannelVN.IMS2.Core.Entities.Security;
using ChannelVN.IMS2.Core.Entities.Statistics;
using ChannelVN.IMS2.Core.Repositories;
using ChannelVN.IMS2.Core.Repositories.Constants;
using ChannelVN.IMS2.Core.Repositories.Functions;
using ChannelVN.IMS2.Core.Services;
using ChannelVN.IMS2.Core.Services.VietId;
using ChannelVN.IMS2.Foundation.Common;
using ChannelVN.IMS2.Foundation.Common.Configuration;
using ChannelVN.IMS2.Foundation.Logging;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using static ChannelVN.IMS2.Core.Entities.ErrorCode.ErrorMapping;

namespace ChannelVN.IMS2.WebApi.Controllers
{
    [Route("api/[controller]")]
    public class SystemController : BaseController
    {
        /// <summary>
        /// Update Class
        /// </summary>
        /// <param name="pageId"></param>
        /// <param name="class"></param>
        /// <returns></returns>
        /// <response code="401">Authorization has been denied for this request</response>
        /// <response code="404">Not Found</response>
        /// <response code="405">Invalid input</response>
        // POST api/security/account/update_type      
        [HttpPost, Route("page/update_class")]
        public async Task<IActionResult> UpdateClassAsync([FromForm]long pageId, [FromForm]AccountClass? @class)
        {
            try
            {
                if (@class == null)
                {
                    return Ok(ActionResponse<string>.CreateErrorResponse("AccountClasse must not null."));
                }

                long.TryParse(User.FindFirst(ClaimTypes.NameIdentifier)?.Value, out long userCurrentId);
                var userCurrent = await SecurityService.GetByIdAsync(userCurrentId);

                if (userCurrent?.IsSystem == true || userCurrent?.IsFullPermission == true || userCurrent?.Levels?.Where(p => p == EnumAccountLevels.ManagePublisher)?.Count() > 0 || userCurrent?.Levels?.Where(p => p == EnumAccountLevels.CMSAdmin)?.Count() > 0)
                {
                    if (pageId == 0)
                    {
                        return Ok(ActionResponse<string>.CreateErrorResponse(Current[ErrorCodes.AccountNotExits]));
                    }

                    var accDb = await SecurityService.GetByIdAsync(pageId);
                    if (accDb == null)
                    {
                        return Ok(ActionResponse<string>.CreateErrorResponse(Current[ErrorCodes.AccountNotExits]));
                    }

                    if (accDb.Type != (Byte?)AccountType.Official)
                    {
                        return Ok(ActionResponse<string>.CreateErrorResponse("Data invalid."));
                    }

                    accDb.Class = (byte?)@class ?? (byte?)AccountClass.Waiting;
                    var erroCode = await SecurityService.UpdateAccountClassAsync(accDb);
                    if (erroCode == ErrorCodes.Success)
                    {
                        return Ok(ActionResponse<string>.CreateSuccessResponse(Current[erroCode]));
                    }
                    return Ok(ActionResponse<string>.CreateErrorResponse(Current[erroCode]));
                }
                else
                {
                    return Ok(ActionResponse<string>.CreateErrorResponse("Bạn không có quyền truy cập....."));
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
                return Ok(ActionResponse<string>.CreateErrorResponse(ex.Message));
            }
        }

        /// <summary>
        /// Update Class
        /// </summary>
        /// <param name="accountId"></param>
        /// <param name="role"></param>
        /// <returns></returns>
        /// <response code="401">Authorization has been denied for this request</response>
        /// <response code="404">Not Found</response>
        /// <response code="405">Invalid input</response>
        // POST api/security/account/update_type      
        [HttpPost, Route("account/update_role")]
        public async Task<IActionResult> UpdateRoleAsync([FromForm]long accountId, [FromForm]EnumAccountLevels[] role)
        {
            try
            {
                long.TryParse(User.FindFirst(ClaimTypes.NameIdentifier)?.Value, out long userCurrentId);
                var userCurrent = await SecurityService.GetByIdAsync(userCurrentId);

                if (userCurrent?.IsSystem == true || userCurrent?.IsFullPermission == true)
                {
                    if (accountId == 0)
                    {
                        return Ok(ActionResponse<string>.CreateErrorResponse(Current[ErrorCodes.AccountNotExits]));
                    }

                    var accDb = await SecurityService.GetByIdAsync(accountId);
                    if (accDb == null)
                    {
                        return Ok(ActionResponse<string>.CreateErrorResponse(Current[ErrorCodes.AccountNotExits]));
                    }

                    if (accDb.Type == (Byte?)AccountType.Official)
                    {
                        return Ok(ActionResponse<string>.CreateErrorResponse("Data invalid."));
                    }

                    accDb.Levels = role;
                    var erroCode = await SecurityService.UpdateRoleAsync(accDb);
                    if (erroCode == ErrorCodes.Success)
                    {
                        return Ok(ActionResponse<string>.CreateSuccessResponse(Current[erroCode]));
                    }
                    return Ok(ActionResponse<string>.CreateErrorResponse(Current[erroCode]));
                }
                else
                {
                    return Ok(ActionResponse<string>.CreateErrorResponse("Bạn không có quyền truy cập....."));
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
                return Ok(ActionResponse<string>.CreateErrorResponse(ex.Message));
            }
        }

        /// <summary>
        /// Tìm kiếm account
        /// </summary>
        /// <param name="search"></param>
        /// <returns></returns>
        /// <response code="401">Authorization has been denied for this request</response>
        /// <response code="404">Not Found</response>
        /// <response code="405">Invalid input</response>
        // POST api/security/account/search      
        [HttpPost, Route("account/search")]
        public async Task<IActionResult> SearchAsync([FromForm]SearchAccountEntity search)
        {
            try
            {
                long.TryParse(User.FindFirst(ClaimTypes.NameIdentifier)?.Value, out long userCurrentId);
                var userCurrent = await SecurityService.GetByIdAsync(userCurrentId);

                if (userCurrent?.IsSystem == true || userCurrent?.IsFullPermission == true || userCurrent?.Levels?.FirstOrDefault(p=> p == EnumAccountLevels.CMSAdmin) != null)
                {
                    search.PageIndex = search.PageIndex == null || search.PageIndex < 1 ? 1 : search.PageIndex;
                    search.PageSize = search.PageSize == null || search.PageSize < 1 ? 10 : search.PageSize;
                    search.Type = null;

                    var result = await SecurityService.SearchAsync(search);
                    var returnValue = new PagingDataResult<AccountSystemSimple>();
                    if (result != null && result.Data != null)
                    {
                        returnValue.Total = result.Total;
                        returnValue.Data = result.Data?.Select(p => new AccountSystemSimple()
                        {
                            Id = p.EncryptId,
                            Avatar = p.Avatar,
                            Class = p.Class,
                            Email = p.Email,
                            FullName = p.FullName,
                            Mobile = p.Mobile,
                            Status = p.Status,
                            Levels = p.Levels,
                            Type = p.Type,
                            UserName = p.UserName,
                            RestrictClasses = p.RestrictClasses
                        })?.ToList();
                    }
                    return Ok(ActionResponse<PagingDataResult<AccountSystemSimple>>.CreateSuccessResponse(returnValue));
                }
                else
                {
                    return Ok(ActionResponse<string>.CreateErrorResponse("Bạn không có quyền truy cập....."));
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
                return Ok(ActionResponse<string>.CreateErrorResponse(ex.Message));
            }
        }

        /// <summary>
        /// Tìm kiếm account
        /// </summary>
        /// <param name="account"></param>
        /// <returns></returns>
        /// <response code="401">Authorization has been denied for this request</response>
        /// <response code="404">Not Found</response>
        /// <response code="405">Invalid input</response>
        // POST api/security/account/search      
        [HttpPost, Route("account/create_by_mobile")]
        public async Task<IActionResult> CreateAsync([FromForm]Account account)
        {
            try
            {
                if (string.IsNullOrEmpty(account?.Mobile))
                {
                    return Ok(ActionResponse<string>.CreateErrorResponse("mobile is not null."));
                }

                long.TryParse(User.FindFirst(ClaimTypes.NameIdentifier)?.Value, out long userCurrentId);
                var userCurrent = await SecurityService.GetByIdAsync(userCurrentId);

                if (userCurrent?.IsSystem == true || userCurrent?.IsFullPermission == true || userCurrent?.Levels?.FirstOrDefault(p => p == EnumAccountLevels.ManagePublisher) != null || userCurrent?.Levels?.FirstOrDefault(p => p == EnumAccountLevels.CMSAdmin) != null)
                {
                    var check = await SecurityService.CheckMobileAsync(account.Mobile);

                    if (check > 0)
                    {
                        var acc = await SecurityService.GetByIdAsync(check.Value);

                        if (acc != null)
                        {
                            acc.Type = (byte)AccountType.Personal;
                            await SecurityService.UpdateAccountTypeAsync(acc);

                            return Ok(ActionResponse<string>.CreateSuccessResponse(check.ToString(), Current[ErrorCodes.Success]));
                        }
                    }

                    account.UserName = account.UserName?.ToLower();

                    account.Password = Encryption.Md5(account.Password ?? "");
                    account.CreatedDate = DateTime.Now;
                    account.Type = account.Type ?? (int)AccountType.Personal;
                    account.Class = (int)AccountClass.Waiting;
                    account.Status = (int)AccountStatus.Actived;
                    account.VerifiedBy = userCurrentId.ToString();
                    account.VerifiedDate = DateTime.Now;

                    account.IsSystem = null;
                    account.IsFullPermission = null;

                    var result = await SecurityService.CreateByMobileAsync(account);
                    if (result.Item1 == ErrorCodes.Success)
                    {
                        return Ok(ActionResponse<string>.CreateSuccessResponse(result.Item2?.Id.ToString(), Current[result.Item1]));
                    }
                    else
                    {
                        return Ok(ActionResponse<string>.CreateErrorResponse(Current[result.Item1]));
                    }
                }
                else
                {
                    return Ok(ActionResponse<string>.CreateErrorResponse("Bạn không có quyền truy cập....."));
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
                return Ok(ActionResponse<string>.CreateErrorResponse(ex.Message));
            }
        }

        /// <summary>
        /// Thêm mới account
        /// </summary>
        /// <param name="fullName"></param>
        /// <param name="mode"></param>
        /// <param name="commentMode"></param>
        /// <param name="avatar"></param>
        /// <param name="cover"></param>
        /// <param name="crawlerMode"></param>
        /// <param name="crawlerSource"></param>
        /// <param name="labelMode"></param>
        /// <param name="userId"></param>
        /// <param name="class"></param>
        /// <returns></returns>
        /// <response code="401">Authorization has been denied for this request</response>
        /// <response code="404">Not Found</response>
        /// <response code="405">Invalid input</response>
        // POST api/security/account/add        
        [HttpPost, Route("account/add_page")]
        public async Task<IActionResult> AddPageAsync([FromForm]long userId, [FromForm]string fullName, [FromForm]string avatar, [FromForm]string cover, [FromForm]CommentMode? commentMode, [FromForm]AccountMode? mode, [FromForm]CrawlerMode? crawlerMode, [FromForm]string crawlerSource, [FromForm]LabelMode? labelMode, [FromForm]AccountClass @class)
        {
            try
            {
                if (string.IsNullOrEmpty(fullName))
                    return Ok(ActionResponse<string>.CreateErrorResponse("Tên page không được để trống."));

                //var sessionid = HttpContext.Request.Headers["sessionId"].ToString();
                long.TryParse(User.FindFirst(ClaimTypes.NameIdentifier)?.Value, out long accountId);

                var currentUser = await SecurityService.GetByIdAsync(accountId);
                var personal = await SecurityService.GetByIdAsync(userId);
                var listPage = await SecurityService.GetListPageOwnerAsync(userId);

                if (currentUser?.IsSystem == true || currentUser?.IsFullPermission == true) {
                    if (personal != null && !string.IsNullOrEmpty(personal.Mobile) && personal.Type == (int)AccountType.Personal)
                    {
                        if (listPage != null && listPage.Where(p => p != null && p.FullName != null && p.FullName.ToLower().Equals(fullName.ToLower())).Count() > 0)
                        {
                            return Ok(ActionResponse<string>.CreateErrorResponse("Tên page này đã được tạo."));
                        }

                        var accesstoken = await DataVietIdService.GetAccessToken(personal.Mobile);
                        //Logger.Debug("AccessToken =>" + Foundation.Common.Json.Stringify(accesstoken));
                        if (!string.IsNullOrEmpty(accesstoken?.Datas?.AccessToken))
                        {
                            var session = await DataVietIdService.GetSessionId(accesstoken?.Datas?.AccessToken);
                            //Logger.Debug("Sesion =>" + Foundation.Common.Json.Stringify(session));
                            var sessionId = session?.data?.sessionId;
                            if (!string.IsNullOrEmpty(sessionId))
                            {
                                //Logger.Debug("Thông tin page kinghub input=>" + Foundation.Common.Json.Stringify(new { userId, fullName, avatar, cover, sessionId }));
                                var kingHubPage = await SecurityService.CreatedKingHubId(userId, fullName, avatar, cover, sessionId);

                                //Logger.Debug("Thông tin page kinghub=>" + Foundation.Common.Json.Stringify(kingHubPage));

                                if (kingHubPage != null && long.TryParse(kingHubPage.UserID, out long kingHubPageId))
                                {
                                    //set role official cho page
                                    await Function.AddToQueue(ActionName.ApprovedOnApp, TopicName.SYNC_USERINFO, new QueueData<long, int, object, object>
                                    {
                                        Data1 = kingHubPageId,
                                        Data2 = AppSettings.Current.ChannelConfiguration.ApiUserKingHub.RoleVip
                                    });

                                    var result = await SecurityService.AddOfficialAsync(new Account()
                                    {
                                        Id = kingHubPageId,
                                        UserName = kingHubPage.Username,
                                        FullName = fullName,
                                        Avatar = avatar,
                                        Banner = cover,
                                        Status = (int)AccountStatus.Actived,
                                        Type = (int)AccountType.Official,
                                        CreatedBy = userId.ToString(),
                                        CreatedDate = DateTime.Now,
                                        Mode = (int?)mode,
                                        CommentMode = (Byte?)commentMode,
                                        CrawlerMode = (Byte?)crawlerMode,
                                        CrawlerSource = crawlerSource,
                                        Class = @class != AccountClass.Waiting && @class != AccountClass.WaitingPrevCheck && @class != AccountClass.WaitingPostCheck ? (Byte?)AccountClass.Waiting : (Byte?)@class,
                                        RelatedId = listPage?.FirstOrDefault()?.Id ?? kingHubPageId
                                    },
                                    new UserProfile()
                                    {
                                        Id = kingHubPageId,
                                        ProfileUrl = kingHubPage.Invite_link// chờ bên kinghub trả về
                                    },
                                    userId);

                                    if (result == ErrorCodes.Success)
                                    {
                                        var page = new Account()
                                        {
                                            Id = kingHubPageId,
                                            UserName = kingHubPage.Username,
                                            FullName = fullName,
                                            Avatar = avatar,
                                            Banner = cover,
                                            Status = (int)AccountStatus.Actived,
                                            Type = (int)AccountType.Official,
                                            CreatedBy = userId.ToString(),
                                            CreatedDate = DateTime.Now,
                                            Mode = (int?)mode,
                                            CommentMode = (Byte?)commentMode,
                                            CrawlerMode = (Byte?)crawlerMode,
                                            CrawlerSource = crawlerSource,
                                            Class = @class != AccountClass.Waiting && @class != AccountClass.WaitingPrevCheck && @class != AccountClass.WaitingPostCheck ? (Byte?)AccountClass.Waiting : (Byte?)@class,
                                        };

                                        if (personal.Class == (int)AccountClass.Waiting)
                                        {
                                            await Function.AddToQueue(ActionName.SendCrm, TopicName.SYNC_USERINFO, new QueueData<string, Account, List<Account>, object>
                                            {
                                                Data1 = personal.FullName,
                                                Data2 = personal,
                                                Data3 = new List<Account>()
                                                {
                                                    page
                                                }
                                            });
                                        }

                                        //await Function.AddToQueue(ActionName.InsertPageOwnerToApp, TopicName.SYNC_USERINFO, new QueueData<long, long, byte?, object>
                                        //{
                                        //    Data1 = kingHubPageId,
                                        //    Data2 = userId,
                                        //    Data3 = @class != AccountClass.Waiting && @class != AccountClass.WaitingPrevCheck && @class != AccountClass.WaitingPostCheck ? (Byte?)AccountClass.Waiting : (Byte?)@class,
                                        //});

                                        await Function.AddToQueue(ActionName.RetryUpdateUserInfoOnApp, TopicName.SYNC_USERINFO, new QueueData<AccountSearch, string, UserProfile, object>
                                        {
                                            Data1 = new AccountSearch
                                            {
                                                Id = kingHubPageId,
                                                UserName = kingHubPage.Username,
                                                FullName = fullName,
                                            },
                                            Data2 = sessionId,
                                            Data3 = null
                                        });

                                        await Function.AddToQueue(ActionName.SettingCommentOnApp, TopicName.SYNC_USERINFO, new QueueData<long, Byte?, object, object>
                                        {
                                            Data1 = kingHubPageId,
                                            Data2 = (Byte?)commentMode
                                        });

                                        return Ok(ActionResponse<object>.CreateSuccessResponse(page, "Success."));
                                    }
                                    else
                                    {
                                        Logger.Error("Lỗi không tạo được page(cms) => userId = " + userId);
                                        return Ok(ActionResponse<string>.CreateErrorResponse(Current[ErrorCodes.AddOfficerError]));
                                    }
                                }
                                else
                                {
                                    Logger.Sensitive("Tạo page không thành công", new { userId, fullName, avatar, cover, sessionId });
                                    Logger.Error("Lỗi không tạo được page(adtech) => userId = " + userId);
                                    return Ok(ActionResponse<string>.CreateErrorResponse(Current[ErrorCodes.AddOfficerError]));
                                }
                            }
                            else
                            {
                                Logger.Error("Lỗi không lấy được sessionId => userId = " + userId);
                                return Ok(ActionResponse<string>.CreateErrorResponse(Current[ErrorCodes.AddOfficerError]));
                            }
                        }
                        else
                        {
                            Logger.Error("Lỗi không lấy được accesstoken => userId = " + userId);
                            return Ok(ActionResponse<string>.CreateErrorResponse(Current[ErrorCodes.AddOfficerError]));
                        }
                    }
                    else
                    {
                        return Ok(ActionResponse<string>.CreateErrorResponse(Current[ErrorCodes.PermissionInvalid]));
                    }
                }else
                if(currentUser?.Levels?.FirstOrDefault(p => p == EnumAccountLevels.CMSAdmin) != null || (currentUser?.Levels?.FirstOrDefault(p => p == EnumAccountLevels.ManagePublisher) != null && currentUser.RestrictClasses != null && (currentUser.RestrictClasses.Contains(AccountClass.Waiting) || currentUser.RestrictClasses.Contains(AccountClass.WaitingPostCheck) || currentUser.RestrictClasses.Contains(AccountClass.WaitingPrevCheck)) && currentUser.RestrictClasses.Contains(@class)))
                {
                    if (personal != null && !string.IsNullOrEmpty(personal.Mobile) && personal.Type == (int)AccountType.Personal)
                    {
                        if (listPage != null && listPage.Where(p => p != null && p.FullName != null && p.FullName.ToLower().Equals(fullName.ToLower())).Count() > 0)
                        {
                            return Ok(ActionResponse<string>.CreateErrorResponse("Tên page này đã được tạo."));
                        }

                        var accesstoken = await DataVietIdService.GetAccessToken(personal.Mobile);
                        if (!string.IsNullOrEmpty(accesstoken?.Datas?.AccessToken))
                        {
                            var session = await DataVietIdService.GetSessionId(accesstoken?.Datas?.AccessToken);
                            var sessionId = session?.data?.sessionId;
                            if (!string.IsNullOrEmpty(sessionId))
                            {
                                var kingHubPage = await SecurityService.CreatedKingHubId(userId, fullName, avatar, cover, sessionId);

                                if (kingHubPage != null && long.TryParse(kingHubPage.UserID, out long kingHubPageId))
                                {
                                    //set role official cho page
                                    await Function.AddToQueue(ActionName.ApprovedOnApp, TopicName.SYNC_USERINFO, new QueueData<long, int, object, object>
                                    {
                                        Data1 = kingHubPageId,
                                        Data2 = AppSettings.Current.ChannelConfiguration.ApiUserKingHub.RoleVip
                                    });

                                    var result = await SecurityService.AddOfficialAsync(new Account()
                                    {
                                        Id = kingHubPageId,
                                        UserName = kingHubPage.Username,
                                        FullName = fullName,
                                        Avatar = avatar,
                                        Banner = cover,
                                        Status = (int)AccountStatus.Actived,
                                        Type = (int)AccountType.Official,
                                        CreatedBy = userId.ToString(),
                                        CreatedDate = DateTime.Now,
                                        Mode = (int?)mode,
                                        CommentMode = (Byte?)commentMode,
                                        CrawlerMode = (Byte?)crawlerMode,
                                        CrawlerSource = crawlerSource,
                                        Class = (Byte?)@class,
                                        RelatedId = listPage?.FirstOrDefault()?.Id ?? kingHubPageId
                                        //LabelMode = (Byte?)labelMode
                                    },
                                    new UserProfile()
                                    {
                                        Id = kingHubPageId,
                                        ProfileUrl = kingHubPage.Invite_link// chờ bên kinghub trả về
                                    },
                                    userId);

                                    if (result == ErrorCodes.Success)
                                    {
                                        var page = new Account()
                                        {
                                            Id = kingHubPageId,
                                            UserName = kingHubPage.Username,
                                            FullName = fullName,
                                            Avatar = avatar,
                                            Banner = cover,
                                            Status = (int)AccountStatus.Actived,
                                            Type = (int)AccountType.Official,
                                            CreatedBy = userId.ToString(),
                                            CreatedDate = DateTime.Now,
                                            Mode = (int?)mode,
                                            CommentMode = (Byte?)commentMode,
                                            CrawlerMode = (Byte?)crawlerMode,
                                            CrawlerSource = crawlerSource,
                                            Class = (Byte?)@class
                                        };

                                        if (personal.Class == (int)AccountClass.Waiting)
                                        {
                                            await Function.AddToQueue(ActionName.SendCrm, TopicName.SYNC_USERINFO, new QueueData<string, Account, List<Account>, object>
                                            {
                                                Data1 = personal.FullName,
                                                Data2 = personal,
                                                Data3 = new List<Account>()
                                                {
                                                    page
                                                }
                                            });
                                        }

                                        //await Function.AddToQueue(ActionName.InsertPageOwnerToApp, TopicName.SYNC_USERINFO, new QueueData<long, long, byte?, object>
                                        //{
                                        //    Data1 = kingHubPageId,
                                        //    Data2 = userId,
                                        //    Data3 = (Byte?)@class
                                        //});

                                        await Function.AddToQueue(ActionName.RetryUpdateUserInfoOnApp, TopicName.SYNC_USERINFO, new QueueData<AccountSearch, string, UserProfile, object>
                                        {
                                            Data1 = new AccountSearch
                                            {
                                                Id = kingHubPageId,
                                                UserName = kingHubPage.Username,
                                                FullName = fullName,
                                            },
                                            Data2 = sessionId,
                                            Data3 = null
                                        });

                                        await Function.AddToQueue(ActionName.SettingCommentOnApp, TopicName.SYNC_USERINFO, new QueueData<long, Byte?, object, object>
                                        {
                                            Data1 = kingHubPageId,
                                            Data2 = (Byte?)commentMode
                                        });

                                        return Ok(ActionResponse<object>.CreateSuccessResponse(page, "Success."));
                                    }
                                    else
                                    {
                                        Logger.Error("Lỗi không tạo được page(cms) => userId = " + userId);
                                        return Ok(ActionResponse<string>.CreateErrorResponse(Current[ErrorCodes.AddOfficerError]));
                                    }
                                }
                                else
                                {
                                    Logger.Sensitive("Tạo page không thành công", new { userId, fullName, avatar, cover, sessionId });
                                    Logger.Error("Lỗi không tạo được page(adtech) => userId = " + userId);
                                    return Ok(ActionResponse<string>.CreateErrorResponse(Current[ErrorCodes.AddOfficerError]));
                                }
                            }
                            else
                            {
                                Logger.Error("Lỗi không lấy được sessionId => userId = " + userId);
                                return Ok(ActionResponse<string>.CreateErrorResponse(Current[ErrorCodes.AddOfficerError]));
                            }
                        }
                        else
                        {
                            Logger.Error("Lỗi không lấy được accesstoken => userId = " + userId);
                            return Ok(ActionResponse<string>.CreateErrorResponse(Current[ErrorCodes.AddOfficerError]));
                        }
                    }
                    else
                    {
                        return Ok(ActionResponse<string>.CreateErrorResponse(Current[ErrorCodes.PermissionInvalid]));
                    }
                }
                else
                {
                    return Ok(ActionResponse<string>.CreateErrorResponse("Bạn không có quyền truy cập....."));
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
                return Ok(ActionResponse<string>.CreateErrorResponse(ex.Message));
            }
        }

        /// <summary>
        /// Update account
        /// </summary>
        /// <param name="account"></param>
        /// <param name="profile"></param>
        /// <param name="sessionId"></param>
        /// <returns></returns>
        /// <response code="401">Authorization has been denied for this request</response>
        /// <response code="404">Not Found</response>
        /// <response code="405">Invalid input</response>
        // POST api/account/update        
        [HttpPost, Route("account/update")]
        public async Task<IActionResult> UpdateAsync([FromForm]Account account, [FromForm]UserProfile profile, [FromHeader]string sessionId = null)
        {
            try
            {
                sessionId = HttpContext.Request.Headers["sessionId"].ToString();
                if (account.Mode != null && !Enum.IsDefined(typeof(AccountMode), account.Mode))
                {
                    return Ok(ActionResponse<string>.CreateErrorResponse("Giá trị \"Mode\" không hợp lệ"));
                }

                if (account == null)
                {
                    return Ok(ActionResponse<string>.CreateErrorResponse(Current[ErrorCodes.AccountNotExits]));
                }

                //check quyen du lieu
                long.TryParse(User.FindFirst(ClaimTypes.NameIdentifier)?.Value, out long accountId);

                var accDb = await SecurityService.GetAccountSearchByIdAsync(account.Id);
                if (accDb == null)
                {
                    return Ok(ActionResponse<AccountSearch>.CreateErrorResponse(Current[ErrorCodes.AccountNotExits]));
                }

                var currentAccount = await SecurityService.GetByIdAsync(accountId);
                if ((currentAccount?.IsSystem == true && currentAccount?.IsFullPermission == true) || currentAccount?.Levels?.FirstOrDefault(p => p == EnumAccountLevels.CMSAdmin) != null || (currentAccount?.Levels?.FirstOrDefault(p => p == EnumAccountLevels.ManagePublisher) != null &&  currentAccount?.RestrictClasses?.FirstOrDefault(p=>(Byte?)p == accDb.Class) != null))
                {
                    //accDb = Mapper<AccountSearch>.Map(currentAccount, accDb);
                    if (string.IsNullOrEmpty(accDb.UserName))
                    {
                        //if (string.IsNullOrEmpty(account.UserName))
                        //{
                        //    return Ok(ActionResponse<AccountSearch>.CreateErrorResponse("Username không được để trống."));
                        //}
                        var accByUserName = await SecurityService.GetByUsernameAsync(account.UserName);
                        if (accByUserName != null)
                        {
                            return Ok(ActionResponse<AccountSearch>.CreateErrorResponse("Username đã tồn tại."));
                        }
                    }

                    var emailOld = accDb.Email;
                    var mobileOld = accDb.Mobile;

                    if (string.IsNullOrEmpty(account.Mobile))
                    {
                        account.Mobile = accDb.Mobile?.Trim();
                    }

                    account.UserName = Utility.UnicodeToUnsigned(account.UserName ?? "").ToLower();
                    //chi admin moi dc quyen update
                    account.IsSystem = false;
                    account.IsFullPermission = false;

                    var dataUpdate = SetUpdateField(accDb, account);
                    var erroCode = await SecurityService.UpdateAsync(dataUpdate, profile, dataUpdate.Type == (byte?)AccountType.Official, mobileOld, emailOld, sessionId); //accountId != dataUpdate.Id
                    if (erroCode == ErrorCodes.Success)
                    {
                        return Ok(ActionResponse<object>.CreateSuccessResponse(new { Id = account.EncryptId }, Current[erroCode]));
                    }
                    return Ok(ActionResponse<string>.CreateErrorResponse(Current[erroCode]));
                }
                else
                {
                    return Ok(ActionResponse<AccountSearch>.CreateErrorResponse("Bạn không có quyền cập nhật tài khoản này"));
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
                return Ok(ActionResponse<string>.CreateErrorResponse(ex.Message));
            }
        }

        private AccountSearch SetUpdateField(AccountSearch accountDb, Account account)
        {
            accountDb.Email = account.Email;
            accountDb.UserName = account.UserName;
            accountDb.FullName = account.FullName;
            accountDb.Avatar = account.Avatar;
            accountDb.Banner = account.Banner;
            accountDb.IsSystem = account.IsSystem;
            accountDb.IsFullPermission = account.IsFullPermission;
            accountDb.OtpSecretKey = account.OtpSecretKey;
            accountDb.LoginedDate = account.LoginedDate;
            accountDb.Description = account.Description;
            accountDb.Mode = account.Mode;
            accountDb.CommentMode = account.CommentMode;
            accountDb.CrawlerMode = account.CrawlerMode;
            accountDb.CrawlerSource = account.CrawlerSource;
            //accountDb.LabelMode = account.LabelMode;
            return accountDb;
        }

        /// <summary>
        /// Lấy thông tin chi tiết account
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        /// <response code="401">Authorization has been denied for this request</response>
        /// <response code="404">Not Found</response>
        /// <response code="405">Invalid input</response>
        // POST api/security/account/get_by_id        
        [HttpPost, Route("page/get_by_id")]
        public async Task<IActionResult> GetByIdAsync([FromForm]long id)
        {
            try
            {
                long.TryParse(User.FindFirst(ClaimTypes.NameIdentifier)?.Value, out long accountId);

                var currentUser = await SecurityService.GetByIdAsync(accountId);
                if (currentUser?.IsSystem == true || currentUser?.IsFullPermission == true || currentUser?.Levels?.Where(p => p == EnumAccountLevels.ManagePublisher)?.Count() > 0)
                {
                    var result = await SecurityService.GetByIdAsync(id);
                    if (result != null)
                    {
                        return Ok(ActionResponse<Account>.CreateSuccessResponse(result));
                    }
                    else
                    {
                        return Ok(ActionResponse<string>.CreateErrorResponse("Tài khoản không tồn tại."));
                    }
                }
                else
                {
                    return Ok(ActionResponse<string>.CreateErrorResponse("Bạn không có quyền truy cập."));
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
                return Ok(ActionResponse<string>.CreateErrorResponse(ex.Message));
            }
        }
        /// <summary>
        /// Lấy số bài chờ duyệt của page Tạm
        /// </summary>
        /// <param name="ids"></param>
        /// <returns></returns>
        /// <response code="401">Authorization has been denied for this request</response>
        /// <response code="404">Not Found</response>
        /// <response code="405">Invalid input</response>
        // POST api/security/account/get_by_id        
        [HttpGet, Route("news/count_order")]
        public async Task<IActionResult> CountNewsOrderAsync([FromQuery]string ids)
        {
            try
            {
                if (string.IsNullOrEmpty(ids))
                {
                    return Ok(ActionResponse<string>.CreateErrorResponse("input invalid."));
                }

                long.TryParse(User.FindFirst(ClaimTypes.NameIdentifier)?.Value, out long accountId);

                var currentUser = await SecurityService.GetByIdAsync(accountId);
                var result = await SystemService.CountNewsOrderAsync(ids);
                if (result != null)
                {
                    return Ok(ActionResponse<Dictionary<string, Dictionary<int, int>>>.CreateSuccessResponse(result));
                }
                else
                {
                    return Ok(ActionResponse<string>.CreateErrorResponse("Tài khoản không tồn tại."));
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
                return Ok(ActionResponse<string>.CreateErrorResponse(ex.Message));
            }
        }


        /// <summary>
        /// Add ManagerPublisher 
        /// </summary>
        /// <param name="account"></param>
        /// <returns></returns>
        /// <response code="401">Authorization has been denied for this request</response>
        /// <response code="404">Not Found</response>
        /// <response code="405">Invalid input</response>
        // POST api/security/accountmember/update  
        [HttpPost, Route("account/add_or_update_manager_publisher")]
        public async Task<IActionResult> AddOrUpdateManagerPublisherAsync([FromForm]Account account)
        {
            try
            {
                if (account == null)
                {
                    return Ok(ActionResponse<string>.CreateErrorResponse("Input invalid."));
                }
                long.TryParse(User.FindFirst(ClaimTypes.NameIdentifier)?.Value, out long accountId);

                var userCurrent = await SecurityService.GetByIdAsync(accountId);
                if (userCurrent?.IsSystem == true || userCurrent?.IsFullPermission == true || userCurrent?.Levels?.FirstOrDefault(p => p == EnumAccountLevels.CMSAdmin) != null)
                {
                    var acc = await SecurityService.GetByIdAsync(account.Id);
                    if (acc == null || acc.Id == 0)
                    {
                        account.Status = (int)AccountStatus.Actived;
                        account.Type = (int)AccountType.ReadOnly;
                        account.Levels = account.Levels ?? new EnumAccountLevels[] { }; 
                        account.RestrictClasses = account.RestrictClasses ?? new AccountClass[] { };
                        var resultAdd = await SecurityService.AddAsync(account);

                        if (resultAdd == ErrorCodes.Success)
                        {
                            return Ok(ActionResponse<string>.CreateSuccessResponse("Add success"));
                        }
                        else
                        {
                            return Ok(ActionResponse<string>.CreateErrorResponse(Current[resultAdd]));
                        }
                    }
                    else
                    {
                        acc.Levels = account.Levels ?? new EnumAccountLevels[] { };
                        acc.RestrictClasses = account.RestrictClasses ?? new AccountClass[] { };
                        var resultAdd = await SecurityService.UpdateRoleAsync(acc);
                        if (resultAdd == ErrorCodes.Success)
                        {
                            return Ok(ActionResponse<string>.CreateSuccessResponse(Current[resultAdd]));
                        }
                        else
                        {
                            return Ok(ActionResponse<string>.CreateErrorResponse(Current[resultAdd]));
                        }
                    }
                }
                else
                {
                    return Ok(ActionResponse<string>.CreateErrorResponse("Bạn không có quyền truy cập."));
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
                return Ok(ActionResponse<string>.CreateErrorResponse(ex.Message));
            }
        }

        ///// <summary>
        ///// Add ManagerPublisher 
        ///// </summary>
        ///// <param name="id"></param>
        ///// <returns></returns>
        ///// <response code="401">Authorization has been denied for this request</response>
        ///// <response code="404">Not Found</response>
        ///// <response code="405">Invalid input</response>
        //// POST api/security/accountmember/update  
        //[HttpPost, Route("account/add_cms_admin")]
        //public async Task<IActionResult> AddCmsAdminAsync([FromForm]long id)
        //{
        //    try
        //    {
        //        if (id <= 0 )
        //        {
        //            return Ok(ActionResponse<string>.CreateErrorResponse("Input invalid."));
        //        }
        //        long.TryParse(User.FindFirst(ClaimTypes.NameIdentifier)?.Value, out long accountId);

        //        var userCurrent = await SecurityService.GetByIdAsync(accountId);
        //        if (userCurrent?.IsSystem == true || userCurrent?.IsFullPermission == true)
        //        {
        //            var acc = await SecurityService.GetByIdAsync(id);
        //            if (acc != null)
        //            {
        //                var levels = new EnumAccountLevels[] {  };
        //                if (acc.Levels == null){
        //                    ;
        //                acc.Levels = new EnumAccountLevels[] { EnumAccountLevels.CMSAdmin };
        //                acc.RestrictClasses = account.RestrictClasses ?? new AccountClass[] { };
        //                var resultAdd = await SecurityService.UpdateRoleAsync(acc);
        //                if (resultAdd == ErrorCodes.Success)
        //                {
        //                    return Ok(ActionResponse<string>.CreateSuccessResponse(Current[resultAdd]));
        //                }
        //                else
        //                {
        //                    return Ok(ActionResponse<string>.CreateErrorResponse(Current[resultAdd]));
        //                }
        //            }
        //            else
        //            {
                        
        //            }
        //        }
        //        else
        //        {
        //            return Ok(ActionResponse<string>.CreateErrorResponse("Bạn không có quyền truy cập."));
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        Logger.Error(ex, ex.Message);
        //        return Ok(ActionResponse<string>.CreateErrorResponse(ex.Message));
        //    }
        //}

        [HttpGet, Route("page/list_follower")]
        public async Task<IActionResult> ListFollowerAsync([FromQuery]long officerId)
        {
            try
            {
                long.TryParse(User.FindFirst(ClaimTypes.NameIdentifier)?.Value, out long accountId);

                var listAccountAssign = await AccountAssignmentService.SearchAsync(0, officerId, "", 1, 1000);
                var listAccount = await SecurityService.GetListByIdsAsync(listAccountAssign?.Data?.Select(p=>p.AccountId.ToString()).ToList());

                var returnValue = new List<object>();

                if(listAccountAssign?.Data != null)
                {
                    foreach(var item in listAccountAssign.Data)
                    {
                        var account = listAccount?.FirstOrDefault(f => f.Id == item.AccountId);
                        returnValue.Add(new
                        {
                            Id = item.AccountId.ToString(),
                            account?.FullName,
                            account?.Avatar,
                            account?.Mobile,
                            account?.Email,
                            item.AssignedRole,
                            item.AssignedDate,
                            item.AssignedBy
                        });
                    }
                }

                return Ok(ActionResponse<IEnumerable<object>>.CreateSuccessResponse(returnValue, "Success"));
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
                return Ok(ActionResponse<string>.CreateErrorResponse(ex.Message));
            }

        }

        [HttpGet, Route("account/levels")]
        public async Task<IActionResult> GetLevelsAsync()
        {
            try
            {
                long.TryParse(User.FindFirst(ClaimTypes.NameIdentifier)?.Value, out long accountId);

                var account = await SecurityService.GetByIdAsync(accountId);
                var isPageExist = await AccountAssignmentService.IsPageExistAsync(accountId);

                if (account != null)
                {
                    return Ok(ActionResponse<object>.CreateSuccessResponse(new
                    {
                       account.Levels,
                       account.RestrictClasses,
                       isPageExist
                    }, "Success"));
                }
                else
                {
                    return Ok(ActionResponse<string>.CreateErrorResponse("Account not exist."));
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
                return Ok(ActionResponse<string>.CreateErrorResponse(ex.Message));
            }

        }

        [HttpPost, Route("accountassignment/remove")]
        public async Task<IActionResult> RemoveAccountAssignmentAsync([FromForm]long officerId, [FromForm] long accountId)
        {
            try
            {
                long.TryParse(User.FindFirst(ClaimTypes.NameIdentifier)?.Value, out long currentAccountId);

                var result = await AccountAssignmentService.RemoveAsync(new AccountAssignment
                {
                    AccountId = accountId,
                    OfficerId = officerId
                });

                if (result)
                {
                    return Ok(ActionResponse<string>.CreateSuccessResponse("Success"));
                }
                else
                {
                    return Ok(ActionResponse<string>.CreateErrorResponse("Remove error."));
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
                return Ok(ActionResponse<string>.CreateErrorResponse(ex.Message));
            }
        }

        [HttpPost, Route("accountassignment/add")]
        public async Task<IActionResult> AddAccountAssignmentAsync([FromForm]long officerId, [FromForm]Account account, [FromForm]EnumAssignedRole? assignmentRole)
        {
            try
            {
                if (officerId <= 0 || account== null || account.Id <= 0 || assignmentRole == null)
                {
                    return Ok(ActionResponse<string>.CreateErrorResponse("Input invalid."));
                }
                long.TryParse(User.FindFirst(ClaimTypes.NameIdentifier)?.Value, out long currentAccountId);

                var accountAssign = await SecurityService.GetByIdAsync(account.Id);
                if(accountAssign == null || accountAssign.Id == 0)
                {
                    account.Status = (int)AccountStatus.Actived;
                    account.Type = (int)AccountType.ReadOnly;
                    account.IsFullPermission = false;
                    account.IsSystem = false;
                    account.Levels = null;
                    account.RestrictClasses = null;
                    var resultAdd = await SecurityService.AddAsync(account);
                    if (resultAdd == ErrorCodes.Success)
                    {
                        var result = await AccountAssignmentService.AddAsync(new AccountAssignment
                        {
                            AccountId = account.Id,
                            OfficerId = officerId,
                            AssignedBy = currentAccountId.ToString(),
                            AssignedDate = DateTime.Now,
                            AssignedRole = (int)assignmentRole
                        });

                        if (result)
                        {
                            return Ok(ActionResponse<string>.CreateSuccessResponse("Add success"));
                        }
                        else
                        {
                            return Ok(ActionResponse<string>.CreateErrorResponse("Add error."));
                        }
                    }
                    else
                    {
                        return Ok(ActionResponse<string>.CreateErrorResponse("Add error."));
                    }
                }
                else
                {
                    var result = await AccountAssignmentService.AddAsync(new AccountAssignment
                    {
                        AccountId = account.Id,
                        OfficerId = officerId,
                        AssignedBy = currentAccountId.ToString(),
                        AssignedDate = DateTime.Now,
                        AssignedRole = (int)assignmentRole
                    });

                    if (result)
                    {
                        return Ok(ActionResponse<string>.CreateSuccessResponse("Add success"));
                    }
                    else
                    {
                        return Ok(ActionResponse<string>.CreateErrorResponse("Add error."));
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
                return Ok(ActionResponse<string>.CreateErrorResponse(ex.Message));
            }
        }

        [HttpPost, Route("account/remove_manage_partnership")]
        public async Task<IActionResult> RemoveAccountManagerPartnershipAsync([FromForm] long accountId)
        {
            try
            {
                long.TryParse(User.FindFirst(ClaimTypes.NameIdentifier)?.Value, out long currentAccountId);
                var currentUser = await SecurityService.GetByIdAsync(currentAccountId);
                if (currentUser?.IsSystem == true || currentUser?.IsFullPermission == true || currentUser?.Levels?.FirstOrDefault(p=>p == EnumAccountLevels.CMSAdmin) != null)
                {
                    var accountDb = await SecurityService.GetByIdAsync(accountId);
                    accountDb.Levels = new EnumAccountLevels[] { };
                    accountDb.RestrictClasses = new AccountClass[] { };
                    var resultAdd = await SecurityService.UpdateRoleAsync(accountDb);
                    if (resultAdd == ErrorCodes.Success)
                    {
                        return Ok(ActionResponse<string>.CreateSuccessResponse(Current[resultAdd]));
                    }
                    else
                    {
                        return Ok(ActionResponse<string>.CreateErrorResponse(Current[resultAdd]));
                    }
                }
                else
                {
                    return Ok(ActionResponse<string>.CreateErrorResponse("Bạn không có quyền truy cập."));
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
                return Ok(ActionResponse<string>.CreateErrorResponse(ex.Message));
            }
        }

        /// <summary>
        /// Danh sách Page
        /// </summary>
        /// <param name="search"></param>
        /// <returns></returns>
        /// <response code="401">Authorization has been denied for this request</response>
        /// <response code="404">Not Found</response>
        /// <response code="405">Invalid input</response>
        // POST api/post/save => getway index 67
        //[DisableCors]
        [HttpGet, Route("list_page_manager")]
        public async Task<IActionResult> ListPageManagerAsync([FromQuery]SearchPage search)
        {
            try
            {
                if (search == null)
                {
                    return Ok(ActionResponse<string>.CreateErrorResponse("Input invalid."));
                }

                search.Levels = null;
                search.PageIndex = search.PageIndex < 1 ? 1 : search.PageIndex;
                search.PageSize = search.PageSize < 1 ? 20 : search.PageSize;

                long.TryParse(User.FindFirst(ClaimTypes.NameIdentifier)?.Value, out long currentAccountId);

                var account = await SecurityService.GetByIdAsync(currentAccountId);
                if(account.Levels?.FirstOrDefault(p=>p == EnumAccountLevels.CMSAdmin) != null)
                {
                    search.Levels = 1;
                }
                else
                {
                    if (account.RestrictClasses == null || account.RestrictClasses.Count() == 0)
                    {
                        return Ok(ActionResponse<PagingDataResult<object>>.CreateSuccessResponse(new PagingDataResult<object>()));
                    }
                    search.ListClass = account.RestrictClasses;
                }

                var result = await SystemService.ListPageManagerAsync(search);
                return Ok(ActionResponse<PagingDataResult<object>>.CreateSuccessResponse(result));
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
                return Ok(ActionResponse<string>.CreateErrorResponse(ex.Message));
            }
        }

        #region AccountAssignment
        [HttpPost, Route("accountassignment/list_page")]
        public async Task<IActionResult> ListPageAsync([FromForm]AccountAssignmentInput body)
        {
            try
            {
                long.TryParse(User.FindFirst(ClaimTypes.NameIdentifier)?.Value, out long accountId);
                var officerId = body.OfficerId.HasValue ? body.OfficerId.Value : 0;
                var pageIndex = body.PageIndex.HasValue ? (body.PageIndex < 1 ? 1 : body.PageIndex.Value) : 1;
                var pageSize = body.PageSize.HasValue ? (body.PageSize < 1 ? 10 : body.PageSize.Value) : 1000;

                var data = await AccountAssignmentService.ListPageAsync(accountId, officerId, "", pageIndex, pageSize);

                //var data = new PagingDataResult<object>()
                //{
                //    Total = 1,
                //    Data = new List<object>
                //    {
                //        new
                //        {
                //            Id = "10356057984064877",
                //            Avatar="http://kingcontent.mediacdn.vn/2019/6/21/5385014824124278454359792571487573989916672n-1561113192940427015859.jpg",
                //            FullName="MUTEX",
                //            CreatedBy="8357188416404204",
                //            Status=1,
                //            LabelMode="",
                //            PhoneOwner = "0904799093",
                //            OwnerStatus = 1,
                //            Class=3,
                //            AssignedRole=1,
                //            AssignedDate="",
                //            AssignedBy=""
                //        },
                //        new
                //        {
                //            Id = "10356057984064877",
                //            Avatar="http://kingcontent.mediacdn.vn/2019/6/20/246x0w-15610007214741613960242.jpg",
                //            FullName="CafeBiz",
                //            CreatedBy="8357188416404204",
                //            Status=1,
                //            LabelMode="",
                //            PhoneOwner = "0904799093",
                //            OwnerStatus = 1,
                //            Class=1,
                //            AssignedRole=1,
                //            AssignedDate="",
                //            AssignedBy=""
                //        }
                //    }
                //};
                return Ok(ActionResponse<PagingDataResult<object>>.CreateSuccessResponse(data, "Success"));
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
                return Ok(ActionResponse<string>.CreateErrorResponse(ex.Message));
            }

        }

        #endregion

        #region Bizfly
        [HttpPost, Route("page/active_chat")]
        public async Task<IActionResult> ActiveChatAsync([FromForm] long pageId)
        {
            try
            {
                if (pageId <= 0)
                {
                    return Ok(ActionResponse<string>.CreateErrorResponse("Input invalid."));
                }

                long.TryParse(User.FindFirst(ClaimTypes.NameIdentifier)?.Value, out long currentAccountId);
                var currentUser = await SecurityService.GetByIdAsync(currentAccountId);
                if (currentUser?.IsSystem == true || currentUser?.IsFullPermission == true || currentUser?.Levels?.FirstOrDefault(p => p == EnumAccountLevels.CMSAdmin) != null || currentUser?.Levels?.FirstOrDefault(p => p == EnumAccountLevels.ManagePublisher) != null)
                {
                    var resultActive = await SystemService.ActiveChatAsync(pageId);

                    if (resultActive?.status == 1 && resultActive?.successCode == 1)
                    {
                        return Ok(ActionResponse<object>.CreateSuccessResponse(resultActive, "Active success."));
                    }
                    else
                    {
                        return Ok(ActionResponse<object>.CreateErrorResponse(resultActive?.errorCode ?? 997, resultActive, resultActive?.message));
                    }
                }
                else
                {
                    return Ok(ActionResponse<string>.CreateErrorResponse("Bạn không có quyền truy cập."));
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
                return Ok(ActionResponse<string>.CreateErrorResponse(ex.Message));
            }
        }

        [HttpPost, Route("page/deactive_chat")]
        public async Task<IActionResult> DeactiveChatAsync([FromForm] long pageId)
        {
            try
            {
                if (pageId <= 0)
                {
                    return Ok(ActionResponse<string>.CreateErrorResponse("Input invalid."));
                }

                long.TryParse(User.FindFirst(ClaimTypes.NameIdentifier)?.Value, out long currentAccountId);
                var currentUser = await SecurityService.GetByIdAsync(currentAccountId);
                if (currentUser?.IsSystem == true || currentUser?.IsFullPermission == true || currentUser?.Levels?.FirstOrDefault(p => p == EnumAccountLevels.CMSAdmin) != null || currentUser?.Levels?.FirstOrDefault(p => p == EnumAccountLevels.ManagePublisher) != null)
                {
                    var resultDeactive = await SystemService.DeactiveChatAsync(pageId);

                    if (resultDeactive?.status == 1)
                    {
                        return Ok(ActionResponse<object>.CreateSuccessResponse(resultDeactive, "Deactive success."));
                    }
                    else
                    {
                        return Ok(ActionResponse<object>.CreateErrorResponse(resultDeactive?.errorCode ?? 997, resultDeactive, resultDeactive?.message));
                    }
                }
                else
                {
                    return Ok(ActionResponse<string>.CreateErrorResponse("Bạn không có quyền truy cập."));
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
                return Ok(ActionResponse<string>.CreateErrorResponse(ex.Message));
            }
        }

        [HttpPost, Route("chat/add_user_to_page")]
        public async Task<IActionResult> AddUserToPageAsync([FromForm] long pageId,[FromForm]string email,[FromForm]string mobile)
        {
            try
            {
                if ((string.IsNullOrEmpty(email) && string.IsNullOrEmpty(mobile)) || pageId <= 0)
                {
                    return Ok(ActionResponse<string>.CreateErrorResponse("Input invalid."));
                }

                //long.TryParse(User.FindFirst(ClaimTypes.NameIdentifier)?.Value, out long currentAccountId);
                //var currentUser = await SecurityService.GetByIdAsync(currentAccountId);
                //if (currentUser?.IsSystem == true || currentUser?.IsFullPermission == true || currentUser?.Levels?.FirstOrDefault(p => p == EnumAccountLevels.CMSAdmin) != null || currentUser?.Levels?.FirstOrDefault(p => p == EnumAccountLevels.ManagePublisher) != null)
                //{

                //}
                //else
                //{
                //    return Ok(ActionResponse<string>.CreateErrorResponse("Bạn không có quyền truy cập."));
                //}
                var resultAdd = await SystemService.AddUserToPageAsync(pageId, email, mobile);

                if (resultAdd?.status == 1)
                {
                    return Ok(ActionResponse<object>.CreateSuccessResponse(resultAdd, "Add user to page success."));
                }
                else
                {
                    return Ok(ActionResponse<object>.CreateErrorResponse(resultAdd?.errorCode ?? 997, resultAdd, resultAdd?.message));
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
                return Ok(ActionResponse<string>.CreateErrorResponse(ex.Message));
            }
        }

        [HttpPost, Route("page/check_active_chat")]
        public async Task<IActionResult> CheckActiveChatAsync([FromForm]string pageIds)
        {
            try
            {
                if (string.IsNullOrEmpty(pageIds))
                {
                    return Ok(ActionResponse<string>.CreateErrorResponse("Input invalid."));
                }

                long.TryParse(User.FindFirst(ClaimTypes.NameIdentifier)?.Value, out long currentAccountId);

                var resultCheck = await SystemService.CheckActiveChatAsync(pageIds);

                if (resultCheck?.status == 1)
                {
                    return Ok(ActionResponse<object>.CreateSuccessResponse(resultCheck, "success."));
                }
                else
                {
                    return Ok(ActionResponse<object>.CreateErrorResponse(resultCheck?.errorCode ?? 997, resultCheck, resultCheck?.message));
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
                return Ok(ActionResponse<string>.CreateErrorResponse(ex.Message));
            }
        }


        [HttpGet, Route("chat/page_of_user")]
        public async Task<IActionResult> GetPageOfUserAsync([FromQuery]string email,[FromQuery]string mobile)
        {
            try
            {
                if (string.IsNullOrEmpty(email) && string.IsNullOrEmpty(mobile))
                {
                    return Ok(ActionResponse<string>.CreateErrorResponse("Input invalid."));
                }

                long.TryParse(User.FindFirst(ClaimTypes.NameIdentifier)?.Value, out long currentAccountId);
                //var currentUser = await SecurityService.GetByIdAsync(currentAccountId);
                //if (currentUser?.IsSystem == true || currentUser?.IsFullPermission == true || currentUser?.Levels?.FirstOrDefault(p => p == EnumAccountLevels.CMSAdmin) != null || currentUser?.Levels?.FirstOrDefault(p => p == EnumAccountLevels.ManagePublisher) != null)
                //{

                //}
                //else
                //{
                //    return Ok(ActionResponse<string>.CreateErrorResponse("Bạn không có quyền truy cập."));
                //}
                var resultCheck = await SystemService.GetPageOfUserAsync(email, mobile);

                if (resultCheck?.status == 1)
                {
                    return Ok(ActionResponse<object>.CreateSuccessResponse(resultCheck, "Success."));
                }
                else
                {
                    return Ok(ActionResponse<object>.CreateErrorResponse(resultCheck?.errorCode ?? 997, resultCheck, resultCheck?.message));
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
                return Ok(ActionResponse<string>.CreateErrorResponse(ex.Message));
            }
        }
        #endregion
    }
}