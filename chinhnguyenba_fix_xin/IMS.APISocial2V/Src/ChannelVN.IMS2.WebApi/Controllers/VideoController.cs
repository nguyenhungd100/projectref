﻿using ChannelVN.IMS2.Core.Entities;
using ChannelVN.IMS2.Core.Entities.Distribution;
using ChannelVN.IMS2.Core.Entities.Media;
using ChannelVN.IMS2.Core.Entities.Security;
using ChannelVN.IMS2.Core.Entities.Statistic;
using ChannelVN.IMS2.Core.Entities.Videos;
using ChannelVN.IMS2.Core.Repositories.Constants;
using ChannelVN.IMS2.Core.Repositories.Functions;
using ChannelVN.IMS2.Core.Services;
using ChannelVN.IMS2.Foundation.Common;
using ChannelVN.IMS2.Foundation.Common.Configuration;
using ChannelVN.IMS2.Foundation.Logging;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Security.Claims;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using static ChannelVN.IMS2.Core.Entities.ErrorCode.ErrorMapping;

namespace ChannelVN.IMS2.WebApi.Controllers
{
    [Route("api/[controller]")]
    public class VideoController : BaseController
    {
        /// <summary>
        /// Thêm mới hoặc update Video
        /// </summary>
        /// <param name="video"></param>
        /// <param name="officerId"></param>
        /// <returns></returns>
        /// <response code="401">Authorization has been denied for this request</response>
        /// <response code="404">Not Found</response>
        /// <response code="405">Invalid input</response>
        // POST api/video/add => getway index 67
        [HttpPost, Route("save")]
        public async Task<IActionResult> SaveAsync([FromForm]Video video, [FromForm] long officerId)
        {
            try
            {
                string sessionId = HttpContext?.Request?.Headers?["session-id"];
                if (string.IsNullOrEmpty(sessionId))
                {
                    return Ok(ActionResponse<string>.CreateErrorResponse("session-id not empty"));
                }
                var clientIP = HttpContext.GetClientIP();

                var result = ErrorCodes.BusinessError;                
                var validateResult = ValidateVideo(video);
                if (validateResult?.Success == false)
                {
                    return Ok(ActionResponse<string>.CreateErrorResponse(validateResult.Message));
                }

                var validPublishData = await Core.Services.Functions.Function.ValidatePublishData(video.PublishData, (int)CardType.Video);
                if (!validPublishData)
                {
                    return Ok(ActionResponse<string>.CreateErrorResponse("PublishData invalid."));
                }

                if (long.TryParse(User.FindFirst(ClaimTypes.NameIdentifier)?.Value, out long userId))
                {
                    if (video.Id > 0)
                    {
                        video.ModifiedDate = DateTime.Now;
                        video.CategoryId = video.CategoryId ?? 0;
                        video.ModifiedBy = userId.ToString();
                        video.Url = string.Format(AppSettings.Current.ChannelConfiguration.CommonSettings.UrlFormatVideo, Utility.UnicodeToUnsignedAndDash(video.Name ?? ""), video.Id);
                        if (string.IsNullOrEmpty(video.OriginalUrl))
                        {
                            video.OriginalUrl = video.Url;
                        }

                        result = await VideoService.UpdateAsync(video, officerId, clientIP, sessionId);
                        if (result == ErrorCodes.Success)
                        {
                            //insert log
                            await Function.AddToQueue(ActionName.InsertLog, TopicName.LOGACTION, new LogActionEntity
                            {
                                ObjectId = video.Id.ToString(),
                                Account = userId.ToString(),
                                CreatedDate = DateTime.SpecifyKind(DateTime.Now, DateTimeKind.Unspecified),
                                Ip = clientIP,
                                Message = "Success.",
                                Title = video.Name,
                                SourceId = officerId.ToString(),
                                ActionTypeDetail = (int)ActionType.Update,
                                ActionText = ActionType.Update.ToString(),
                                Type = (int)LogContentType.Video
                            });
                        }
                    }
                    else
                    {
                        VideoSearch videoSearch = Mapper<VideoSearch>.Map(video, new VideoSearch());
                        videoSearch.Type = (int)NewsType.Video;
                        videoSearch.CardType = (int)CardType.Video;
                        videoSearch.Id = Generator.NewsId();
                        video.Id = videoSearch.Id;
                        videoSearch.Status = (int)NewsStatus.Draft;
                        videoSearch.CreatedDate = DateTime.Now;
                        videoSearch.CategoryId = video.CategoryId ?? 0;
                        videoSearch.AllowAdv = videoSearch.AllowAdv ?? false;
                        videoSearch.IsRemovedLogo = videoSearch.IsRemovedLogo ?? false;
                        videoSearch.IsConverted = videoSearch.IsConverted ?? false;
                        videoSearch.PublishMode = (int)NewsPublishMode.Public;
                        videoSearch.DistributorId = videoSearch.DistributorId.HasValue ? videoSearch.DistributorId : AppSettings.Current.Distribution.Automation.DistributorId;
                        videoSearch.CreatedBy = userId.ToString();
                        videoSearch.NewsInAccount = new[] {
                            new NewsInAccount
                            {
                                AccountId = officerId,
                                NewsId = videoSearch.Id,
                                PublishedDate = DateTime.Now,
                  /**/              PublishedType = (int)NewsPublishedType.Post
                            }
                        };
                        videoSearch.Url = string.Format(AppSettings.Current.ChannelConfiguration.CommonSettings.UrlFormatVideo, Utility.UnicodeToUnsignedAndDash(videoSearch.Name ?? ""), videoSearch.Id);
                        if (string.IsNullOrEmpty(videoSearch.OriginalUrl))
                        {
                            videoSearch.OriginalUrl = videoSearch.Url;
                        }

                        result = await VideoService.AddAsync(videoSearch, clientIP);
                        if (result == ErrorCodes.Success)
                        {
                            //insert log
                            await Function.AddToQueue(ActionName.InsertLog, TopicName.LOGACTION, new LogActionEntity
                            {
                                ObjectId = videoSearch.Id.ToString(),
                                Account = userId.ToString(),
                                CreatedDate = DateTime.SpecifyKind(DateTime.Now, DateTimeKind.Unspecified),
                                Ip = clientIP,
                                Message = "Success.",
                                Title = videoSearch.Name,
                                SourceId = officerId.ToString(),
                                ActionTypeDetail = (int)ActionType.Insert,
                                ActionText = ActionType.Insert.ToString(),
                                Type = (int)LogContentType.Video
                            });
                        }
                    }
                }
                else
                {
                    result = ErrorCodes.PermissionInvalid;
                }

                if (result == ErrorCodes.Success)
                {
                    return Ok(ActionResponse<object>.CreateSuccessResponse(video.EncryptId, "success"));
                }
                else
                {
                    return Ok(ActionResponse<string>.CreateErrorResponse(Current[result]));
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
                return Ok(ActionResponse<string>.CreateErrorResponse((int)ErrorCodes.Exception, ex.Message));
            }
        }

        /// <summary>
        /// Lưu trữ Video
        /// </summary>
        /// <param name="video"></param>
        /// <param name="officerId"></param>
        /// <returns></returns>
        /// <response code="401">Authorization has been denied for this request</response>
        /// <response code="404">Not Found</response>
        /// <response code="405">Invalid input</response>
        [HttpPost, Route("archive")]
        public async Task<IActionResult> ArchiveAsync([FromForm]Video video, [FromForm] long officerId)
        {
            try
            {
                string sessionId = HttpContext.GetSessionId();
                if (string.IsNullOrEmpty(sessionId))
                {
                    return Ok(ActionResponse<string>.CreateErrorResponse("session-id not empty"));
                }
                var clientIP = HttpContext.GetClientIP();

                var result = ErrorCodes.BusinessError;
                ActionResponse<string> validateResult = ValidateVideo(video);
                if (validateResult?.Success == false)
                {
                    return Ok(ActionResponse<string>.CreateErrorResponse(validateResult.Message));
                }

                if (long.TryParse(User.FindFirst(ClaimTypes.NameIdentifier)?.Value, out long userId))
                {
                    if (video.Id > 0)
                    {
                        video.ModifiedDate = DateTime.Now;
                        video.CategoryId = video.CategoryId ?? 0;
                        video.ModifiedBy = userId.ToString();
                        video.Url = string.Format(AppSettings.Current.ChannelConfiguration.CommonSettings.UrlFormatVideo, Utility.UnicodeToUnsignedAndDash(video.GetType().Name ?? ""), video.Id);
                        if (string.IsNullOrEmpty(video.OriginalUrl))
                        {
                            video.OriginalUrl = video.Url;
                        }

                        result = await VideoService.UpdateAsync(video, officerId, clientIP, sessionId);
                        if (result == ErrorCodes.Success)
                        {
                            //insert log
                            await Function.AddToQueue(ActionName.InsertLog, TopicName.LOGACTION, new LogActionEntity
                            {
                                ObjectId = video.Id.ToString(),
                                Account = userId.ToString(),
                                CreatedDate = DateTime.SpecifyKind(DateTime.Now, DateTimeKind.Unspecified),
                                Ip = clientIP,
                                Message = "Success.",
                                Title = video.Name,
                                SourceId = officerId.ToString(),
                                ActionTypeDetail = (int)ActionType.Update,
                                ActionText = ActionType.Update.ToString(),
                                Type = (int)LogContentType.Video
                            });
                        }
                    }
                    else
                    {
                        VideoSearch videoSearch = Mapper<VideoSearch>.Map(video, new VideoSearch());
                        videoSearch.Type = (int)NewsType.Video;
                        videoSearch.CardType = (int)CardType.Video;
                        videoSearch.Id = Generator.NewsId();
                        video.Id = videoSearch.Id;
                        videoSearch.Status = (int)NewsStatus.Archive;
                        videoSearch.CreatedDate = DateTime.Now;
                        videoSearch.CategoryId = video.CategoryId ?? 0;
                        videoSearch.AllowAdv = videoSearch.AllowAdv ?? false;
                        videoSearch.IsRemovedLogo = videoSearch.IsRemovedLogo ?? false;
                        videoSearch.IsConverted = videoSearch.IsConverted ?? false;
                        videoSearch.PublishMode = (int)NewsPublishMode.Public;
                        videoSearch.DistributorId = videoSearch.DistributorId.HasValue ? videoSearch.DistributorId : AppSettings.Current.Distribution.Automation.DistributorId;
                        videoSearch.CreatedBy = userId.ToString();
                        videoSearch.NewsInAccount = new[] {
                            new NewsInAccount
                            {
                                AccountId = officerId,
                                NewsId = videoSearch.Id,
                                PublishedDate = DateTime.Now,
                                PublishedType = (int)NewsPublishedType.Post
                            }
                        };
                        videoSearch.Url = string.Format(AppSettings.Current.ChannelConfiguration.CommonSettings.UrlFormatVideo, Utility.UnicodeToUnsignedAndDash(videoSearch.GetType().Name ?? ""), videoSearch.Id);
                        if (string.IsNullOrEmpty(videoSearch.OriginalUrl))
                        {
                            videoSearch.OriginalUrl = videoSearch.Url;
                        }

                        result = await VideoService.AddAsync(videoSearch, clientIP);
                        if (result == ErrorCodes.Success)
                        {
                            //insert log
                            await Function.AddToQueue(ActionName.InsertLog, TopicName.LOGACTION, new LogActionEntity
                            {
                                ObjectId = videoSearch.Id.ToString(),
                                Account = userId.ToString(),
                                CreatedDate = DateTime.SpecifyKind(DateTime.Now, DateTimeKind.Unspecified),
                                Ip = clientIP,
                                Message = "Success.",
                                Title = videoSearch.Name,
                                SourceId = officerId.ToString(),
                                ActionTypeDetail = (int)ActionType.Insert,
                                ActionText = ActionType.Insert.ToString(),
                                Type = (int)LogContentType.Video
                            });
                        }
                    }
                }
                else
                {
                    result = ErrorCodes.PermissionInvalid;
                }

                if (result == ErrorCodes.Success)
                {
                    return Ok(ActionResponse<object>.CreateSuccessResponse(video.EncryptId, "success"));
                }
                else
                {
                    return Ok(ActionResponse<string>.CreateErrorResponse(Current[result]));
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
                return Ok(ActionResponse<string>.CreateErrorResponse((int)ErrorCodes.Exception, ex.Message));
            }
        }

        /// <summary>
        /// Lấy thông tin chi tiết Video
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        /// <response code="401">Authorization has been denied for this request</response>
        /// <response code="404">Not Found</response>
        /// <response code="405">Invalid input</response>
        // POST api/video/get_by_id        
        [HttpPost, Route("get_by_id")]
        public async Task<IActionResult> GetByIdAsync([FromForm]long id)
        {
            try
            {
                VideoSearch result = await VideoService.GetVideoSearchByIdAsync(id);
                if (result != null && result.Id > 0) return Ok(ActionResponse<VideoSearch>.CreateSuccessResponse(result));
                return Ok(ActionResponse<string>.CreateErrorResponse("Video not exist."));
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
                return Ok(ActionResponse<string>.CreateErrorResponse(ex.Message));
            }
        }

        /// <summary>
        /// Cập nhận trạng thái
        /// </summary>
        /// <param name="id"></param>
        /// <param name="action"></param>
        /// <param name="officerId"></param>
        /// <returns></returns>
        /// <response code="401">Authorization has been denied for this request</response>
        /// <response code="404">Not Found</response>
        /// <response code="405">Invalid input</response>
        // POST api/article/update_status       
        [HttpPost, Route("update_status")]
        public async Task<IActionResult> UpdateStatusV2Async([FromForm]long id, [FromForm]NewsStatus action, [FromForm] long officerId)
        {
            try
            {
                string sessionId = HttpContext.GetSessionId();
                if (string.IsNullOrEmpty(sessionId))
                {
                    return Ok(ActionResponse<string>.CreateErrorResponse("session-id not empty"));
                }

                var result = ErrorCodes.BusinessError;

                var clientIP = HttpContext.GetClientIP();

                long.TryParse(User.FindFirst(ClaimTypes.NameIdentifier)?.Value, out long userId);

                result = await VideoService.UpdateStatusV2Async(id, action, userId, clientIP, sessionId);
                if (result == ErrorCodes.Success)
                {
                    //insert log
                    await Function.AddToQueue(ActionName.InsertLog, TopicName.LOGACTION, new LogActionEntity
                    {
                        ObjectId = id.ToString(),
                        Account = userId.ToString(),
                        CreatedDate = DateTime.SpecifyKind(DateTime.Now, DateTimeKind.Unspecified),
                        Ip = clientIP,
                        Message = "Success.",
                        Title = "",
                        SourceId = officerId.ToString(),
                        ActionTypeDetail = (int)ActionType.UpdateStatus,
                        ActionText = ActionType.UpdateStatus.ToString(),
                        Type = (int)LogContentType.Video
                    });
                    return Ok(ActionResponse<object>.CreateSuccessResponse(id.ToString(), "success"));
                }
                return Ok(ActionResponse<string>.CreateErrorResponse(Current[result]));
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
                return Ok(ActionResponse<string>.CreateErrorResponse(ex.Message));
            }
        }

        /// <summary>
        /// Tìm kiếm Video
        /// </summary>
        /// <param name="searchEntity"></param>
        /// <returns></returns>
        /// <response code="401">Authorization has been denied for this request</response>
        /// <response code="404">Not Found</response>
        /// <response code="405">Invalid input</response>
        // POST api/video/search => getway index 71
        [HttpPost, Route("search")]
        public async Task<IActionResult> SearchAsync([FromForm]SearchNews searchEntity)
        {
            try
            {
                if (searchEntity == null)
                {
                    return Ok(ActionResponse<string>.CreateSuccessResponse("Không tìm thấy video nào."));
                }
                searchEntity.PageIndex = searchEntity.PageIndex < 1 ? 1 : searchEntity.PageIndex;
                searchEntity.PageSize = searchEntity.PageSize < 1 ? 20 : searchEntity.PageSize;
                searchEntity.FromDate = searchEntity.FromDate.HasValue ? searchEntity.FromDate.Value.AddHours(0).AddMinutes(0).AddSeconds(0) : DateTime.MinValue;
                searchEntity.ToDate = searchEntity.ToDate.HasValue ? searchEntity.ToDate.Value.AddHours(23).AddMinutes(59).AddSeconds(59) : DateTime.MaxValue;

                long.TryParse(User.FindFirst(ClaimTypes.NameIdentifier)?.Value, out long userId);

                PagingDataResult<VideoSearchReturn> result = await VideoService.SearchAsync(searchEntity, userId);
                return Ok(ActionResponse<PagingDataResult<VideoSearchReturn>>.CreateSuccessResponse(result));
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
                return Ok(ActionResponse<string>.CreateErrorResponse(ex.Message));
            }
        }

        /// <summary>
        /// Lấy danh sách video đang chờ duyệt
        /// </summary>
        /// <param name="searchEntity"></param>
        /// <returns></returns>
        /// <response code="401">Authorization has been denied for this request</response>
        /// <response code="404">Not Found</response>
        /// <response code="405">Invalid input</response>
        [HttpPost, Route("get_list_approve")]
        public async Task<IActionResult> GetListApproveAsync([FromForm]SearchNews searchEntity)
        {
            try
            {
                if (searchEntity == null)
                {
                    return Ok(ActionResponse<string>.CreateSuccessResponse("Không tìm thấy sharelink nào."));
                }
                if (searchEntity.OfficerId <= 0)
                {
                    return Ok(ActionResponse<string>.CreateErrorResponse("OfficerId không hợp lệ."));
                }

                searchEntity.Status = ((int)NewsStatus.Approve).ToString();
                searchEntity.PageIndex = searchEntity.PageIndex < 1 ? 1 : searchEntity.PageIndex;
                searchEntity.PageSize = searchEntity.PageSize < 1 ? 20 : searchEntity.PageSize;
                searchEntity.FromDate = searchEntity.FromDate.HasValue ? searchEntity.FromDate.Value.AddHours(0).AddMinutes(0).AddSeconds(0) : DateTime.MinValue;
                searchEntity.ToDate = searchEntity.ToDate.HasValue ? searchEntity.ToDate.Value.AddHours(23).AddMinutes(59).AddSeconds(59) : DateTime.Now;
                long.TryParse(User.FindFirst(ClaimTypes.NameIdentifier)?.Value, out long accountId);

                var result = await VideoService.GetListApproveAsync(searchEntity, accountId);

                return Ok(ActionResponse<PagingDataResult<Video>>.CreateSuccessResponse(result));
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
                return Ok(ActionResponse<string>.CreateErrorResponse(ex.Message));
            }
        }

        /// <summary>
        /// Thống kế số lượng video theo trạng thái
        /// </summary>
        /// <param name="status"></param>
        /// <param name="officialId"></param>
        /// <returns></returns>
        /// <response code="401">Authorization has been denied for this request</response>
        /// <response code="404">Not Found</response>
        /// <response code="405">Invalid input</response>
        // POST api/video/get_count_by_status
        [HttpPost, Route("get_count_by_status")]
        public async Task<IActionResult> GetCountByStatusAsync([FromForm]string status, [FromForm]long? officialId)
        {
            try
            {
                if (string.IsNullOrEmpty(status) || (!Enum.IsDefined(typeof(NewsStatus), status.ToLower())))
                {
                    return Ok(ActionResponse<string>.CreateErrorResponse("Trạng thái video không hợp lệ."));
                }
                long.TryParse(User.FindFirst(ClaimTypes.NameIdentifier)?.Value, out long userId);
                Core.Entities.Security.Account acc = await SecurityService.GetByIdAsync(userId);
                long result = await VideoService.GetCountByStatus(status, officialId ?? 0, acc);
                return Ok(ActionResponse<object>.CreateSuccessResponse(new { Count = result.ToString() }));
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
                return Ok(ActionResponse<string>.CreateErrorResponse(ex.Message));
            }
        }


        /// <summary>
        /// Danh sách video liên quan
        /// </summary>
        /// <param name="videoId"></param>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        /// <response code="401">Authorization has been denied for this request</response>
        /// <response code="404">Not Found</response>
        /// <response code="405">Invalid input</response>
        // POST api/video/get_list_relation
        [HttpPost, Route("get_list_relation")]
        public async Task<IActionResult> GetListRelationAsync([FromForm]long videoId, [FromForm]int? pageIndex, [FromForm] int? pageSize)
        {
            try
            {
                if (videoId == 0)
                {
                    return Ok(ActionResponse<string>.CreateSuccessResponse("Video không tồn tại."));
                }
                pageIndex = pageIndex < 1 ? 1 : pageIndex;
                pageSize = pageSize < 1 ? 20 : pageSize;
                Video video = await VideoService.GetByIdAsync(videoId);
                if (video.VideoRelation != null && video.VideoRelation.Length > 0)
                {
                    PagingDataResult<Video> result = await VideoService.GetListRelationAsync(video.VideoRelation, pageIndex, pageSize);

                    return Ok(ActionResponse<PagingDataResult<Video>>.CreateSuccessResponse(result));
                }
                return Ok(ActionResponse<PagingDataResult<Video>>.CreateSuccessResponse(new PagingDataResult<Video>()));
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
                return Ok(ActionResponse<string>.CreateErrorResponse(ex.Message));
            }
        }
        
        private ActionResponse<string> ValidateVideo(Video video)
        {
            try
            {
                if (video == null)
                {
                    return ActionResponse<string>.CreateErrorResponse("Không có video.");
                }
                if (string.IsNullOrEmpty(video.KeyVideo))
                {
                    return ActionResponse<string>.CreateErrorResponse(Current[ErrorCodes.VideoKeyIsRequired]);// "Key video không được để trống.");
                }
                if (string.IsNullOrEmpty(video.Name))
                    return ActionResponse<string>.CreateErrorResponse(Current[ErrorCodes.VideoNameIsRequired]);// "Tên video không được để trống.");
                if (string.IsNullOrEmpty(video.HtmlCode))
                {
                    return ActionResponse<string>.CreateErrorResponse(Current[ErrorCodes.VideoHtmlCodeIsRequired]);// "HtmlCode không được để trống.");
                }

                if (string.IsNullOrEmpty(video.FileName))
                {
                    return ActionResponse<string>.CreateErrorResponse(Current[ErrorCodes.VideoFileNameIsRequired]);// "FileName không được để trống.");
                }

                if (!Utility.ValidateStringIds(video.Tags))
                {
                    return ActionResponse<string>.CreateErrorResponse("Tags không hợp lệ.");// "FileName không được để trống.");
                }
                return ActionResponse<string>.CreateSuccessResponse(Current[ErrorCodes.Success]);// "Pass :)");
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
                return ActionResponse<string>.CreateErrorResponse("Exception...");
            }
        }

        /// <summary>
        /// add video vào playlist
        /// </summary>
        /// <param name="videoId"></param>
        /// <param name="playlistIds"></param>
        /// <param name="officerId"></param>
        /// <returns></returns>
        /// <response code="401">Authorization has been denied for this request</response>
        /// <response code="404">Not Found</response>
        /// <response code="405">Invalid input</response>
        // POST api/video/search => getway index 71
        [HttpPost, Route("add_to_playlist")]
        public async Task<IActionResult> AddToPlaylistAsync([FromForm]long videoId, [FromForm]long[] playlistIds, [FromForm] long officerId)
        {
            try
            {
                var clientIP = string.IsNullOrEmpty(HttpContext?.Connection?.RemoteIpAddress?.ToString()) ? "192.168.1.1" : HttpContext?.Connection?.RemoteIpAddress?.ToString();
                long.TryParse(User.FindFirst(ClaimTypes.NameIdentifier)?.Value, out long userId);
                var video = await VideoService.GetByIdAsync(videoId);
                if (video == null)
                {
                    return Ok(ActionResponse<string>.CreateErrorResponse("Video không tồn tại."));
                }
                if (!userId.ToString().Equals(video.CreatedBy))
                {
                    return Ok(ActionResponse<string>.CreateErrorResponse("Video không phải của bạn."));
                }
                var dataDb = await VideoPlaylistService.GetListByIdAsync(playlistIds);
                if (dataDb == null || dataDb.Count() == 0)
                {
                    return Ok(ActionResponse<string>.CreateErrorResponse("Playlist không tồn tại."));
                }
                else
                {
                    if (dataDb?.Where(p => !userId.ToString().Equals(p.CreatedBy))?.Count() > 0)
                    {
                        return Ok(ActionResponse<string>.CreateErrorResponse("Bạn không có quyền chỉnh sửa Playlist (id = " + (dataDb?.Where(p => !userId.Equals(p.CreatedBy))?.FirstOrDefault()?.Id + ").")));
                    }
                }

                //build publishData
                var publishDataVideo = Foundation.Common.Json.Parse<PublishData>(video.PublishData);
                dataDb.ForEach(p =>
                {
                    p.LastInsertedDate = DateTime.Now;
                    p.ModifiedBy = userId.ToString();
                    p.ModifiedDate = DateTime.Now;
                    var publishDataPlaylist = Foundation.Common.Json.Parse<PublishData>(p.PublishData);
                    if (publishDataPlaylist != null && publishDataPlaylist.Items != null && publishDataVideo != null && publishDataVideo.Items != null && publishDataVideo.Items.Count() > 0)
                    {
                        var firstItem = publishDataVideo.Items.FirstOrDefault();
                        if (firstItem != null) {
                            firstItem.Id = video.Id.ToString();
                            if (publishDataPlaylist.Items.Where(w => firstItem.Id.Equals(w.Id?.ToString()))?.Count() == 0)
                            {
                                publishDataPlaylist.Items.Add(firstItem);
                                p.PublishData = Foundation.Common.Json.StringifyFirstLowercase(publishDataPlaylist);
                                //p.PublishData = Foundation.Common.Json.Stringify(new
                                //{
                                //    title = publishDataPlaylist.Title,
                                //    items = publishDataPlaylist.Items.Select(s => new
                                //    {
                                //        title = s.Title,
                                //        link = s.Link,
                                //        thumb = s.Thumb,
                                //        duration = s.Duration,
                                //        width = s.Width,
                                //        height = s.Height,
                                //        content_type = s.ContentType,
                                //        type = s.Type,
                                //        sourceIframe = s.SourceIframe,
                                //        id = s.Id
                                //    })?.ToArray()
                                //});
                            }
                            else
                            {
                                publishDataPlaylist.Items.RemoveAll(w => w.Id.Equals(firstItem.Id));
                                publishDataPlaylist.Items.Add(firstItem);
                                p.PublishData = Foundation.Common.Json.StringifyFirstLowercase(publishDataPlaylist);
                                //p.PublishData = Foundation.Common.Json.Stringify(new
                                //{
                                //    title = publishDataPlaylist.Title,
                                //    items = publishDataPlaylist.Items.Select(s => new
                                //    {
                                //        title = s.Title,
                                //        link = s.Link,
                                //        thumb = s.Thumb,
                                //        duration = s.Duration,
                                //        width = s.Width,
                                //        height = s.Height,
                                //        content_type = s.ContentType,
                                //        type = s.Type,
                                //        sourceIframe = s.SourceIframe,
                                //        id = s.Id
                                //    })?.ToArray()
                                //});
                            }
                        }
                    }
                    else
                    {
                        if ((publishDataPlaylist == null || publishDataPlaylist.Items == null) && publishDataVideo != null && publishDataVideo.Items != null && publishDataVideo.Items.Count() > 0)
                        {

                        }
                    }
                });

                var result = await VideoService.AddToPlaylistAsync(videoId, dataDb);
                if (result)
                {
                    //insert log
                    await Function.AddToQueue(ActionName.InsertLog, TopicName.LOGACTION, new LogActionEntity
                    {
                        ObjectId = videoId.ToString(),
                        Account = userId.ToString(),
                        CreatedDate = DateTime.SpecifyKind(DateTime.Now, DateTimeKind.Unspecified),
                        Ip = clientIP,
                        Message = "Success.",
                        Title = video.Name,
                        SourceId = officerId.ToString(),
                        ActionTypeDetail = (int)ActionType.AddVideoToPlaylist,
                        ActionText = ActionType.AddVideoToPlaylist.ToString(),
                        Type = (int)LogContentType.Video
                    });
                    return Ok(ActionResponse<string>.CreateSuccessResponse("Add video success."));
                }
                return Ok(ActionResponse<string>.CreateErrorResponse("Add video unsuccess."));
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
                return Ok(ActionResponse<string>.CreateErrorResponse(ex.Message));
            }
        }
        /// <summary>
        /// Add vào bảng tin
        /// </summary>
        /// <param name="mediaId"></param>
        /// <param name="boardIds"></param>
        /// <param name="officerId"></param>
        /// <returns></returns>
        /// <response code="401">Authorization has been denied for this request</response>
        /// <response code="404">Not Found</response>
        /// <response code="405">Invalid input</response>
        // POST api/videoplaylist/get_video_by_playlist_id => getway index 93
        [HttpPost, Route("add_to_board")]
        public async Task<IActionResult> AddToBoardAsync([FromForm]long mediaId, [FromForm] long[] boardIds, [FromForm]long officerId)
        {
            try
            {
                var clientIP = string.IsNullOrEmpty(HttpContext?.Connection?.RemoteIpAddress?.ToString()) ? "192.168.1.1" : HttpContext?.Connection?.RemoteIpAddress?.ToString();
                long.TryParse(User.FindFirst(ClaimTypes.NameIdentifier)?.Value, out long userId);

                var dataDb = await BoardService.GetListByIdAsync(boardIds);
                if (dataDb == null)
                {
                    return Ok(ActionResponse<string>.CreateErrorResponse("Bảng tin không tồn tại."));
                }
                else
                {
                    if (!(await BoardService.CheckBoardOfUser(officerId, userId, boardIds)))
                    {
                        return Ok(ActionResponse<string>.CreateErrorResponse("Bạn không có quyền chỉnh sửa Bảng tin."));
                    }
                }

                dataDb.ForEach(p =>
                {
                    p.LastInsertedDate = DateTime.Now;
                    p.ModifiedBy = userId.ToString();
                    p.ModifiedDate = DateTime.Now;
                });

                var result = await BoardService.InsertMediaAsync(mediaId, dataDb);
                if (result)
                {
                    foreach (var boardId in boardIds)
                    {
                        await Function.AddToQueue(ActionName.AddNewsToBoard, TopicName.PUSHBOARDTOCHANNEL, new QueueData<long, string, long, long>
                        {
                            Data1 = boardId,
                            Data2 = mediaId.ToString(),
                            Data3 = officerId,
                            Data4 = userId
                        });
                    }
                    
                    //insert log
                    await Function.AddToQueue(ActionName.InsertLog, TopicName.LOGACTION, new LogActionEntity
                    {
                        ObjectId = mediaId.ToString(),
                        Account = userId.ToString(),
                        CreatedDate = DateTime.SpecifyKind(DateTime.Now, DateTimeKind.Unspecified),
                        Ip = clientIP,
                        Message = "Success.",
                        Title = "",
                        SourceId = officerId.ToString(),
                        ActionTypeDetail = (int)ActionType.AddMediaToBoard,
                        ActionText = ActionType.AddMediaToBoard.ToString(),
                        Type = (int)LogContentType.Video
                    });

                    return Ok(ActionResponse<string>.CreateSuccessResponse("Add video success."));
                }
                return Ok(ActionResponse<string>.CreateErrorResponse("Add video unsuccess."));
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
                return Ok(ActionResponse<string>.CreateErrorResponse(ex.Message));
            }
        }
    }
}
