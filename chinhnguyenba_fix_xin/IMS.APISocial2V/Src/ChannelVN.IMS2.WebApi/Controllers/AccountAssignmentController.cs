﻿using ChannelVN.IMS2.Core.Entities.AccountAssignment;
using ChannelVN.IMS2.Core.Services;
using ChannelVN.IMS2.Foundation.Common;
using ChannelVN.IMS2.Foundation.Logging;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Security.Claims;
using System.Threading.Tasks;

namespace ChannelVN.IMS2.WebApi.Controllers
{
    [Route("api/[controller]")]
    public class AccountAssignmentController : BaseController
    {
        /// <summary>
        /// Tìm kiếm bài post
        /// </summary>
        /// <param name="body"></param>        
        /// <returns></returns>
        /// <response code="401">Authorization has been denied for this request</response>
        /// <response code="404">Not Found</response>
        /// <response code="405">Invalid input</response>
        // POST api/news/search => getway index 47
        [HttpPost, Route("search")]
        public async Task<IActionResult> SearchAsync([FromForm]AccountAssignmentInput body)
        {
            try
            {                
                if (body == null)
                {
                    return Ok(ActionResponse<string>.CreateSuccessResponse("Không tìm thấy bài viết nào."));
                }
                var officerId = body.OfficerId.HasValue ? body.OfficerId.Value : 0;
                var pageIndex = body.PageIndex.HasValue ? (body.PageIndex < 1 ? 1 : body.PageIndex.Value) : 1;
                var pageSize = body.PageSize.HasValue ? (body.PageSize < 1 ? 10 : body.PageSize.Value) : 1000;
                long.TryParse(User.FindFirst(ClaimTypes.NameIdentifier)?.Value, out long accountId);

                var result = await AccountAssignmentService.SearchAsync(accountId, officerId,"", pageIndex, pageSize);
                
                return Ok(ActionResponse<PagingDataResult<AccountAssignment>>.CreateSuccessResponse(result));
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
                return Ok(ActionResponse<string>.CreateErrorResponse(ex.Message));
            }
        }

        /// <summary>
        /// đề xuất tin on home
        /// </summary>
        /// <param name="body"></param>
        /// <returns></returns>
        /// <response code="401">Authorization has been denied for this request</response>
        /// <response code="404">Not Found</response>
        /// <response code="405">Invalid input</response>
        // POST api/news/search => getway index 47
        [HttpPost, Route("save")]
        public async Task<IActionResult> SaveAsync([FromForm]AccountAssignment body)
        {
            try
            {
                if (body == null)
                {
                    return Ok(ActionResponse<string>.CreateErrorResponse("Không tìm thấy bài viết được đề xuất."));
                }
                if (body.OfficerId <= 0)
                {
                    return Ok(ActionResponse<string>.CreateErrorResponse("Không tìm thấy page."));
                }
                long.TryParse(User.FindFirst(ClaimTypes.NameIdentifier)?.Value, out long accountId);
                
                body.AssignedDate = DateTime.Now;
                body.AssignedBy = accountId.ToString();

                var result = await AccountAssignmentService.AddAsync(body);

                return Ok(ActionResponse<string>.CreateSuccessResponse("Success"));
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
                return Ok(ActionResponse<string>.CreateErrorResponse(ex.Message));
            }
        }

        [HttpPost, Route("remove")]
        public async Task<IActionResult> RemoveAsync([FromForm]AccountAssignment body)
        {
            try
            {
                if (body == null)
                {
                    return Ok(ActionResponse<string>.CreateErrorResponse("Không tìm thấy bài viết được đề xuất."));
                }
                if (body.OfficerId <= 0)
                {
                    return Ok(ActionResponse<string>.CreateErrorResponse("Không tìm thấy page."));
                }
                long.TryParse(User.FindFirst(ClaimTypes.NameIdentifier)?.Value, out long accountId);
                
                var result = await AccountAssignmentService.RemoveAsync(body);

                return Ok(ActionResponse<string>.CreateSuccessResponse("Success"));
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
                return Ok(ActionResponse<string>.CreateErrorResponse(ex.Message));
            }
        }        
    }
}
