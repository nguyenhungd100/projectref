﻿using ChannelVN.IMS2.Core.Entities;
using ChannelVN.IMS2.Core.Entities.Media;
using ChannelVN.IMS2.Core.Entities.Security;
using ChannelVN.IMS2.Core.Repositories.Constants;
using ChannelVN.IMS2.Core.Repositories.Functions;
using ChannelVN.IMS2.Core.Services;
using ChannelVN.IMS2.Foundation.Common;
using ChannelVN.IMS2.Foundation.Common.Configuration;
using ChannelVN.IMS2.Foundation.Logging;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using static ChannelVN.IMS2.Core.Entities.ErrorCode.ErrorMapping;

namespace ChannelVN.IMS2.WebApi.Controllers
{
    [Route("api/[controller]")]
    public class MediaUnitController : BaseController
    {
        /// <summary>
        /// Thêm mới hoặc update Media unit
        /// </summary>
        /// <param name="mediaUnitData"></param>
        /// <param name="officerId"></param>
        /// <returns></returns>
        /// <response code="401">Authorization has been denied for this request</response>
        /// <response code="404">Not Found</response>
        /// <response code="405">Invalid input</response>
        // POST api/mediaunit/save        
        [HttpPost, Route("save")]
        public async Task<IActionResult> Save([FromForm]MediaUnit mediaUnitData, [FromForm]long officerId)
        {
            try
            {
                string sessionId = HttpContext?.Request?.Headers?["session-id"];
                if (string.IsNullOrEmpty(sessionId))
                {
                    return Ok(ActionResponse<string>.CreateErrorResponse("session-id not empty"));
                }
                var clientIP = HttpContext.GetClientIP();

                var result = ErrorCodes.BusinessError;
                var validateResult = ValidateMediaUnit(mediaUnitData);
                if (validateResult?.Success == false)
                {
                    return Ok(ActionResponse<string>.CreateErrorResponse(validateResult.Message));
                }

                var validPublishData = await Core.Services.Functions.Function.ValidatePublishData(mediaUnitData.PublishData, (int)CardType.VideoAndImage);
                if (!validPublishData)
                {
                    return Ok(ActionResponse<string>.CreateErrorResponse("PublishData invalid."));
                }

                if (long.TryParse(User.FindFirst(ClaimTypes.NameIdentifier)?.Value, out long accountId))
                {
                    if (mediaUnitData.Id > 0)
                    {
                        //update
                        mediaUnitData.Url = string.Format(AppSettings.Current.ChannelConfiguration.CommonSettings.UrlFormatMediaUnit, Utility.UnicodeToUnsignedAndDash(mediaUnitData.GetType().Name), mediaUnitData.Id);
                        mediaUnitData.ModifiedDate = DateTime.Now;
                        mediaUnitData.ModifiedBy = accountId + "";

                        result = await MediaUnitService.UpdateAsync(mediaUnitData, officerId, clientIP, sessionId);
                        if (result == ErrorCodes.Success)
                        {
                            //insert log
                            await Function.AddToQueue(ActionName.InsertLog, TopicName.LOGACTION, new LogActionEntity
                            {
                                ObjectId = mediaUnitData.Id.ToString(),
                                Account = accountId.ToString(),
                                CreatedDate = DateTime.SpecifyKind(DateTime.Now, DateTimeKind.Unspecified),
                                Ip = clientIP,
                                Message = "Success.",
                                Title = mediaUnitData.Caption,
                                SourceId = officerId.ToString(),
                                ActionTypeDetail = (int)ActionType.Update,
                                ActionText = ActionType.Update.ToString(),
                                Type = (int)LogContentType.MediaUnit
                            });
                        }
                    }
                    else
                    {
                        var mediaUnit = Mapper<MediaUnitSearch>.Map(mediaUnitData, new MediaUnitSearch());
                        //insert
                        mediaUnit.Id = Generator.NewsId();
                        mediaUnitData.Id = mediaUnit.Id;
                        mediaUnit.Url = string.Format(AppSettings.Current.ChannelConfiguration.CommonSettings.UrlFormatMediaUnit, Utility.UnicodeToUnsignedAndDash(mediaUnit.GetType().Name), mediaUnit.Id);
                        if (string.IsNullOrEmpty(mediaUnit.OriginalUrl))
                            mediaUnit.OriginalUrl = mediaUnit.Url;
                        mediaUnit.CardType = (int)CardType.VideoAndImage;
                        mediaUnit.Type = (int)NewsType.MediaUnit;
                        mediaUnit.PublishMode = (int)NewsPublishMode.Public;
                        mediaUnit.Status = (int)NewsStatus.Draft;
                        mediaUnit.CreatedDate = DateTime.Now;
                        mediaUnit.CreatedBy = accountId + "";
                        mediaUnit.DistributorId = mediaUnit.DistributorId.HasValue ? mediaUnit.DistributorId : AppSettings.Current.Distribution.Automation.DistributorId;

                        mediaUnit.NewsInAccount = new[] {
                            new NewsInAccount
                            {
                                AccountId = officerId,
                                NewsId = mediaUnit.Id,
                                PublishedDate = DateTime.Now,
                                PublishedType = (int)NewsPublishedType.Post
                            }
                        };

                        result = await MediaUnitService.AddAsync(mediaUnit, clientIP);
                        if (result == ErrorCodes.Success)
                        {
                            //insert log
                            await Function.AddToQueue(ActionName.InsertLog, TopicName.LOGACTION, new LogActionEntity
                            {
                                ObjectId = mediaUnit.Id.ToString(),
                                Account = accountId.ToString(),
                                CreatedDate = DateTime.SpecifyKind(DateTime.Now, DateTimeKind.Unspecified),
                                Ip = clientIP,
                                Message = "Success.",
                                Title = mediaUnit.Caption,
                                SourceId = officerId.ToString(),
                                ActionTypeDetail = (int)ActionType.Insert,
                                ActionText = ActionType.Insert.ToString(),
                                Type = (int)LogContentType.MediaUnit
                            });
                        }
                    }
                }

                if (result == ErrorCodes.Success)
                {
                    return Ok(ActionResponse<object>.CreateSuccessResponse(mediaUnitData.Id.ToString(), "Success."));
                }
                else
                {
                    return Ok(ActionResponse<bool>.CreateErrorResponse(Current[result]));
                }
            }
            catch (Exception ex)
            {
                return Ok(ActionResponse<string>.CreateErrorResponse(ex.Message));
            }
        }

        /// <summary>
        /// Lưu trữ Media unit
        /// </summary>
        /// <param name="mediaUnitData"></param>
        /// <param name="officerId"></param>
        /// <returns></returns>
        /// <response code="401">Authorization has been denied for this request</response>
        /// <response code="404">Not Found</response>
        /// <response code="405">Invalid input</response>
        // POST api/mediaunit/archive        
        [HttpPost, Route("archive")]
        public async Task<IActionResult> ArchiveAsync([FromForm]MediaUnit mediaUnitData, [FromForm]long officerId)
        {
            try
            {
                string sessionId = HttpContext?.Request?.Headers?["session-id"];
                if (string.IsNullOrEmpty(sessionId))
                {
                    return Ok(ActionResponse<string>.CreateErrorResponse("session-id not empty"));
                }
                var clientIP = HttpContext.GetClientIP();

                var result = ErrorCodes.BusinessError;
                var validateResult = ValidateMediaUnit(mediaUnitData);
                if (validateResult?.Success == false)
                {
                    return Ok(ActionResponse<string>.CreateErrorResponse(validateResult.Message));
                }

                if (long.TryParse(User.FindFirst(ClaimTypes.NameIdentifier)?.Value, out long accountId))
                {
                    if (mediaUnitData.Id > 0)
                    {
                        //update
                        mediaUnitData.Url = string.Format(AppSettings.Current.ChannelConfiguration.CommonSettings.UrlFormatMediaUnit, Utility.UnicodeToUnsignedAndDash(mediaUnitData.GetType().Name), mediaUnitData.Id);
                        mediaUnitData.ModifiedDate = DateTime.Now;
                        mediaUnitData.ModifiedBy = accountId + "";

                        result = await MediaUnitService.UpdateAsync(mediaUnitData, officerId, clientIP, sessionId);
                        if (result == ErrorCodes.Success)
                        {
                            //insert log
                            await Function.AddToQueue(ActionName.InsertLog, TopicName.LOGACTION, new LogActionEntity
                            {
                                ObjectId = mediaUnitData.Id.ToString(),
                                Account = accountId.ToString(),
                                CreatedDate = DateTime.SpecifyKind(DateTime.Now, DateTimeKind.Unspecified),
                                Ip = clientIP,
                                Message = "Success.",
                                Title = mediaUnitData.Caption,
                                SourceId = mediaUnitData.ModifiedBy,
                                ActionTypeDetail = (int)ActionType.Update,
                                ActionText = ActionType.Update.ToString(),
                                Type = (int)LogContentType.MediaUnit
                            });
                        }
                    }
                    else
                    {
                        var mediaUnit = Mapper<MediaUnitSearch>.Map(mediaUnitData, new MediaUnitSearch());
                        //insert
                        mediaUnit.Id = Generator.NewsId();
                        mediaUnitData.Id = mediaUnit.Id;
                        mediaUnit.Url = string.Format(AppSettings.Current.ChannelConfiguration.CommonSettings.UrlFormatMediaUnit, Utility.UnicodeToUnsignedAndDash(mediaUnit.GetType().Name), mediaUnit.Id);
                        if (string.IsNullOrEmpty(mediaUnit.OriginalUrl))
                            mediaUnit.OriginalUrl = mediaUnit.Url;
                        mediaUnit.CardType = (int)CardType.VideoAndImage;
                        mediaUnit.Type = (int)NewsType.MediaUnit;
                        mediaUnit.PublishMode = (int)NewsPublishMode.Public;
                        mediaUnit.Status = (int)NewsStatus.Archive;
                        mediaUnit.CreatedDate = DateTime.Now;
                        mediaUnit.CreatedBy = accountId + "";
                        mediaUnit.DistributorId = mediaUnit.DistributorId.HasValue ? mediaUnit.DistributorId : AppSettings.Current.Distribution.Automation.DistributorId;

                        mediaUnit.NewsInAccount = new[] {
                            new NewsInAccount
                            {
                                AccountId = officerId,
                                NewsId=mediaUnit.Id,
                                PublishedDate=DateTime.Now,
                                PublishedType=(int)NewsPublishedType.Post
                            }
                        };

                        result = await MediaUnitService.AddAsync(mediaUnit, clientIP);
                        if (result == ErrorCodes.Success)
                        {
                            //insert log
                            await Function.AddToQueue(ActionName.InsertLog, TopicName.LOGACTION, new LogActionEntity
                            {
                                ObjectId = mediaUnit.Id.ToString(),
                                Account = accountId.ToString(),
                                CreatedDate = DateTime.SpecifyKind(DateTime.Now, DateTimeKind.Unspecified),
                                Ip = clientIP,
                                Message = "Success.",
                                Title = mediaUnit.Caption,
                                SourceId = mediaUnit.CreatedBy,
                                ActionTypeDetail = (int)ActionType.Insert,
                                ActionText = ActionType.Insert.ToString(),
                                Type = (int)LogContentType.MediaUnit
                            });
                        }
                    }
                }

                if (result == ErrorCodes.Success)
                {
                    return Ok(ActionResponse<object>.CreateSuccessResponse(mediaUnitData.Id.ToString(), "Success."));
                }
                else
                {
                    return Ok(ActionResponse<bool>.CreateErrorResponse("Lỗi tạo khi tạo mẫu."));
                }
            }
            catch (Exception ex)
            {
                return Ok(ActionResponse<string>.CreateErrorResponse(ex.Message));
            }
        }

        private ActionResponse<string> ValidateMediaUnit(MediaUnit mediaUnit)
        {
            if (mediaUnit == null)
            {
                return ActionResponse<string>.CreateErrorResponse("Params not valid.");
            }
            if (!Utility.ValidateStringIds(mediaUnit.Tags))
                return ActionResponse<string>.CreateErrorResponse("Tags không hợp lệ.");
            if (string.IsNullOrEmpty(mediaUnit.PublishData) || !mediaUnit.PublishData.Contains("items"))
                return ActionResponse<string>.CreateErrorResponse("Định dạng PublishData không đúng!");

            return ActionResponse<string>.CreateSuccessResponse("Success.");
        }

        /// <summary>
        /// Cập nhận trạng thái
        /// </summary>
        /// <param name="id"></param>
        /// <param name="action"></param>
        /// <param name="officerId"></param>
        /// <returns></returns>
        /// <response code="401">Authorization has been denied for this request</response>
        /// <response code="404">Not Found</response>
        /// <response code="405">Invalid input</response>
        // POST api/article/update_status       
        [HttpPost, Route("update_status")]
        public async Task<IActionResult> UpdateStatusV2Async([FromForm]long id, [FromForm]NewsStatus action, [FromForm] long officerId)
        {
            try
            {
                string sessionId = HttpContext?.Request?.Headers?["session-id"];
                if (string.IsNullOrEmpty(sessionId))
                {
                    return Ok(ActionResponse<string>.CreateErrorResponse("session-id not empty"));
                }
                var clientIP = HttpContext.GetClientIP();

                var result = ErrorCodes.BusinessError;
                long.TryParse(User.FindFirst(ClaimTypes.NameIdentifier)?.Value, out long userId);

                result = await MediaUnitService.UpdateStatusV2Async(id, action, userId, clientIP, sessionId);
                if (result == ErrorCodes.Success)
                {
                    //insert log
                    await Function.AddToQueue(ActionName.InsertLog, TopicName.LOGACTION, new LogActionEntity
                    {
                        ObjectId = id.ToString(),
                        Account = userId.ToString(),
                        CreatedDate = DateTime.SpecifyKind(DateTime.Now, DateTimeKind.Unspecified),
                        Ip = clientIP,
                        Message = "Success.",
                        Title = "",
                        SourceId = officerId.ToString(),
                        ActionTypeDetail = (int)ActionType.UpdateStatus,
                        ActionText = ActionType.UpdateStatus.ToString(),
                        Type = (int)LogContentType.MediaUnit
                    });
                    return Ok(ActionResponse<object>.CreateSuccessResponse(id.ToString(), "success"));
                }
                return Ok(ActionResponse<string>.CreateErrorResponse(Current[result]));
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
                return Ok(ActionResponse<string>.CreateErrorResponse(ex.Message));
            }
        }

        /// <summary>
        /// Lấy thông tin chi tiết Media unit
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        /// <response code="401">Authorization has been denied for this request</response>
        /// <response code="404">Not Found</response>
        /// <response code="405">Invalid input</response>
        // POST api/mediaunit/get_by_id
        [HttpPost, Route("get_by_id")]
        public async Task<IActionResult> GetById([FromForm]long Id)
        {
            try
            {
                var result = await MediaUnitService.GetByIdAsync(Id);
                if (result != null && result.Id > 0) return Ok(ActionResponse<MediaUnitSearch>.CreateSuccessResponse(result));
                return Ok(ActionResponse<string>.CreateErrorResponse("Media not exist"));
            }
            catch (Exception ex)
            {
                return Ok(ActionResponse<string>.CreateErrorResponse(ex.Message));
            }
        }

        /// <summary>
        /// Tìm kiếm Media unit
        /// </summary>
        /// <param name="search"></param>
        /// <returns></returns>
        /// <response code="401">Authorization has been denied for this request</response>
        /// <response code="404">Not Found</response>
        /// <response code="405">Invalid input</response>
        // POST api/mediaunit/search        
        [HttpPost, Route("search")]
        public async Task<IActionResult> Search([FromForm] SearchNews search)
        {
            try
            {
                if (search == null)
                {
                    return Ok(ActionResponse<string>.CreateErrorResponse("Object search null."));
                }

                search.PageIndex = search.PageIndex < 1 ? 1 : search.PageIndex;
                search.PageSize = search.PageSize < 1 ? 20 : search.PageSize;
                search.FromDate = search.FromDate.HasValue ? search.FromDate.Value.AddHours(0).AddMinutes(0).AddSeconds(0) : DateTime.MinValue;
                search.ToDate = search.ToDate.HasValue ? search.ToDate.Value.AddHours(23).AddMinutes(59).AddSeconds(59) : DateTime.MaxValue;
                long.TryParse(User.FindFirst(ClaimTypes.NameIdentifier)?.Value, out long accountId);
                if (search.OfficerId == null || search.OfficerId == 0) search.OfficerId = accountId;
                var result = await MediaUnitService.SearchAsync(search, accountId);
                if (result != null)
                {
                    return Ok(ActionResponse<PagingDataResult<MediaUnitReturn>>.CreateSuccessResponse(result));
                }

                return Ok(ActionResponse<PagingDataResult<MediaUnitReturn>>.CreateSuccessResponse(new PagingDataResult<MediaUnitReturn>()));
            }
            catch (Exception ex)
            {
                return Ok(ActionResponse<string>.CreateErrorResponse(ex.Message));
            }
        }

        /// <summary>
        /// Lấy danh sách media unit đang chờ duyệt
        /// </summary>
        /// <param name="searchEntity"></param>
        /// <returns></returns>
        /// <response code="401">Authorization has been denied for this request</response>
        /// <response code="404">Not Found</response>
        /// <response code="405">Invalid input</response>
        [HttpPost, Route("get_list_approve")]
        public async Task<IActionResult> GetListApproveAsync([FromForm]SearchNews searchEntity)
        {
            try
            {
                if (searchEntity == null)
                {
                    return Ok(ActionResponse<string>.CreateSuccessResponse("Không tìm thấy media unit nào."));
                }
                if (searchEntity.OfficerId <= 0)
                {
                    return Ok(ActionResponse<string>.CreateErrorResponse("OfficerId không hợp lệ."));
                }

                searchEntity.Status = ((int)NewsStatus.Approve).ToString();
                searchEntity.PageIndex = searchEntity.PageIndex < 1 ? 1 : searchEntity.PageIndex;
                searchEntity.PageSize = searchEntity.PageSize < 1 ? 20 : searchEntity.PageSize;
                searchEntity.FromDate = searchEntity.FromDate.HasValue ? searchEntity.FromDate.Value.AddHours(0).AddMinutes(0).AddSeconds(0) : DateTime.MinValue;
                searchEntity.ToDate = searchEntity.ToDate.HasValue ? searchEntity.ToDate.Value.AddHours(23).AddMinutes(59).AddSeconds(59) : DateTime.Now;
                long.TryParse(User.FindFirst(ClaimTypes.NameIdentifier)?.Value, out long accountId);

                var result = await MediaUnitService.GetListApproveAsync(searchEntity, accountId);

                return Ok(ActionResponse<PagingDataResult<MediaUnit>>.CreateSuccessResponse(result));
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
                return Ok(ActionResponse<string>.CreateErrorResponse(ex.Message));
            }
        }

        /// <summary>
        /// Add vào bảng tin
        /// </summary>
        /// <param name="mediaId"></param>
        /// <param name="boardIds"></param>
        ///  <param name="officerId"></param>
        /// <returns></returns>
        /// <response code="401">Authorization has been denied for this request</response>
        /// <response code="404">Not Found</response>
        /// <response code="405">Invalid input</response>
        // POST api/videoplaylist/get_video_by_playlist_id => getway index 93
        [HttpPost, Route("add_to_board")]
        public async Task<IActionResult> AddToBoardAsync([FromForm]long mediaId, [FromForm] long[] boardIds, [FromForm]long officerId)
        {
            try
            {
                var clientIP = string.IsNullOrEmpty(HttpContext?.Connection?.RemoteIpAddress?.ToString()) ? "192.168.1.1" : HttpContext?.Connection?.RemoteIpAddress?.ToString();
                long.TryParse(User.FindFirst(ClaimTypes.NameIdentifier)?.Value, out long userId);
                var dataDb = await BoardService.GetListByIdAsync(boardIds);
                if (dataDb == null)
                {
                    return Ok(ActionResponse<string>.CreateErrorResponse("Bảng tin không tồn tại."));
                }
                else
                {
                    if (!(await BoardService.CheckBoardOfUser(officerId, userId, boardIds)))
                    {
                        return Ok(ActionResponse<string>.CreateErrorResponse("Bạn không có quyền chỉnh sửa Bảng tin."));
                    }
                }

                dataDb.ForEach(p =>
                {
                    p.LastInsertedDate = DateTime.Now;
                    p.ModifiedBy = userId.ToString();
                    p.ModifiedDate = DateTime.Now;
                });
                var result = await BoardService.InsertMediaAsync(mediaId, dataDb);
                if (result)
                {
                    foreach (var boardId in boardIds)
                    {
                        await Function.AddToQueue(ActionName.AddNewsToBoard, TopicName.PUSHBOARDTOCHANNEL, new QueueData<long, string, long, long>
                        {
                            Data1 = boardId,
                            Data2 = mediaId.ToString(),
                            Data3 = officerId,
                            Data4 = userId
                        });
                    }
                    
                    //insert log
                    await Function.AddToQueue(ActionName.InsertLog, TopicName.LOGACTION, new LogActionEntity
                    {
                        ObjectId = mediaId.ToString(),
                        Account = userId.ToString(),
                        CreatedDate = DateTime.SpecifyKind(DateTime.Now, DateTimeKind.Unspecified),
                        Ip = clientIP,
                        Message = "Success.",
                        Title = "",
                        SourceId = officerId.ToString(),
                        ActionTypeDetail = (int)ActionType.AddMediaToBoard,
                        ActionText = ActionType.AddMediaToBoard.ToString(),
                        Type = (int)LogContentType.MediaUnit
                    });
                    return Ok(ActionResponse<string>.CreateSuccessResponse("Add album success."));
                }
                return Ok(ActionResponse<string>.CreateErrorResponse("Add album unsuccess."));
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
                return Ok(ActionResponse<string>.CreateErrorResponse(ex.Message));
            }
        }
    }
}
