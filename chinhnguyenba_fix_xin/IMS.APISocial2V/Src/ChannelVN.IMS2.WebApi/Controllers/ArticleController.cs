﻿using ChannelVN.IMS2.Core.Entities;
using ChannelVN.IMS2.Core.Entities.Distribution;
using ChannelVN.IMS2.Core.Entities.Media;
using ChannelVN.IMS2.Core.Entities.Newsletters;
using ChannelVN.IMS2.Core.Entities.Security;
using ChannelVN.IMS2.Core.Repositories.Constants;
using ChannelVN.IMS2.Core.Repositories.Functions;
using ChannelVN.IMS2.Core.Services;
using ChannelVN.IMS2.Foundation.Common;
using ChannelVN.IMS2.Foundation.Common.Configuration;
using ChannelVN.IMS2.Foundation.Logging;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using static ChannelVN.IMS2.Core.Entities.ErrorCode.ErrorMapping;
using ChannelVN.IMS2.Foundation.Data.Factories;

namespace ChannelVN.IMS2.WebApi.Controllers
{
    [Route("api/[controller]")]
    public class ArticleController : BaseController
    {
        /// <summary>
        /// Thêm và Update News
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///
        ///     POST api/article/save
        ///     {
        ///        "title": "Test tiêu đề tin",
        ///        "officerId": "123456"
        ///     }
        ///
        /// </remarks>  
        /// <param name="news"></param>
        /// <param name="officerId"> id tài khoản office</param>
        /// <returns>A newly created TodoItem</returns>
        /// <response code="401">Authorization has been denied for this request</response>
        /// <response code="404">Not Found</response>
        /// <response code="405">Invalid input</response>
        [HttpPost, Route("save")]
        public async Task<IActionResult> SaveAsync([FromForm]Article news, [FromForm]long officerId)
        {
            try
            {
                string sessionId = HttpContext?.Request?.Headers?["session-id"];
                if (string.IsNullOrEmpty(sessionId))
                {
                    return Ok(ActionResponse<string>.CreateErrorResponse("session-id not empty"));
                }
                var clientIP = HttpContext.GetClientIP();

                if (news == null)
                {
                    return Ok(ActionResponse<string>.CreateErrorResponse(Current[ErrorCodes.NewsNotExits]));
                }

                if (!(officerId > 0))
                {
                    return Ok(ActionResponse<string>.CreateErrorResponse("OfficerId invalid."));
                }

                if (!Utility.ValidateStringIds(news.Tags))
                {
                    return Ok(ActionResponse<string>.CreateErrorResponse("Tags không hợp lệ."));
                }

                int[] newsTemps = Enum.GetValues(typeof(ArticleTemplateId)) as int[];
                if (newsTemps != null && !newsTemps.Contains(news.TemplateId ?? 0))
                {
                    return Ok(ActionResponse<string>.CreateErrorResponse("Bạn chưa chọn Template cho tin."));
                }

                if (string.IsNullOrEmpty(news.Title))
                {
                    return Ok(ActionResponse<string>.CreateErrorResponse("Tiêu đề tin không được để trống."));
                }

                var validPublishData = await Core.Services.Functions.Function.ValidatePublishData(news.PublishData, (int)CardType.Article);
                if (!validPublishData)
                {
                    return Ok(ActionResponse<string>.CreateErrorResponse("PublishData invalid."));
                }

                var result = ErrorCodes.BusinessError;
                if (long.TryParse(User.FindFirst(ClaimTypes.NameIdentifier)?.Value, out long userId))
                {
                    if (news.Id > 0)
                    {
                        news.ModifiedDate = DateTime.Now;
                        news.ModifiedBy = userId.ToString();
                        if (string.IsNullOrEmpty(news.OriginalUrl))
                            news.OriginalUrl = news.Url;
                        
                        result = await ArticleService.UpdateAsync(news, clientIP, officerId, sessionId);
                        if (result == ErrorCodes.Success)
                        {
                            //insert log
                            await Function.AddToQueue(ActionName.InsertLog, TopicName.LOGACTION, new LogActionEntity
                            {
                                ObjectId = news.Id.ToString(),//id đôi tượng mà user thực hiện action trên nó
                                Account = userId.ToString(), // id user thực hiện action
                                CreatedDate = DateTime.SpecifyKind(DateTime.Now, DateTimeKind.Unspecified),
                                Ip = clientIP,// ip
                                Message = "Success.",// message
                                Title = news.Title,// title đối tượng
                                SourceId = officerId.ToString(),//id page
                                ActionTypeDetail = (int)ActionType.Update,
                                ActionText = ActionType.Update.ToString(),
                                Type = (int)LogContentType.Article
                            });
                        }
                    }
                    else
                    {
                        var articleSearch = Mapper<ArticleSearch>.Map(news, new ArticleSearch());
                        articleSearch.Type = (int)NewsType.Article;
                        articleSearch.Id = Generator.NewsId();
                        articleSearch.CardType = (int)CardType.Article;
                        news.Id = articleSearch.Id;
                        articleSearch.CreatedDate = DateTime.Now;
                        articleSearch.Status = (int)NewsStatus.Draft;
                        articleSearch.DistributorId = articleSearch.DistributorId.HasValue ? articleSearch.DistributorId : AppSettings.Current.Distribution.Automation.DistributorId;
                        articleSearch.ApprovedDate = null;
                        articleSearch.NewsInAccount = new[] {
                            new NewsInAccount
                            {
                                AccountId = officerId,
                                NewsId = articleSearch.Id,
                                PublishedDate = DateTime.Now,
                                PublishedType = (int)PublishedType.Post
                            }
                        };
                        articleSearch.CreatedBy = userId.ToString();
                        if (string.IsNullOrEmpty(articleSearch.OriginalUrl))
                            articleSearch.OriginalUrl = articleSearch.Url;

                        result = await ArticleService.AddAsync(articleSearch, clientIP);
                        if (result == ErrorCodes.Success)
                        {
                            //insert log
                            await Function.AddToQueue(ActionName.InsertLog, TopicName.LOGACTION, new LogActionEntity
                            {
                                ObjectId = articleSearch.Id.ToString(),
                                Account = userId.ToString(),
                                CreatedDate = DateTime.SpecifyKind(DateTime.Now, DateTimeKind.Unspecified),
                                Ip = clientIP,
                                Message = "Success.",
                                Title = articleSearch.Title,
                                SourceId = officerId.ToString(),
                                ActionTypeDetail = (int)ActionType.Insert,
                                ActionText = ActionType.Insert.ToString(),
                                Type = (int)LogContentType.Article
                            });
                        }
                    }
                }
                else
                {
                    result = ErrorCodes.PermissionInvalid;
                }

                if (result == ErrorCodes.Success)
                {
                    return Ok(ActionResponse<object>.CreateSuccessResponse(news.EncryptId, "success"));
                }
                else
                {
                    return Ok(ActionResponse<string>.CreateErrorResponse(Current[result]));
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
                return Ok(ActionResponse<string>.CreateErrorResponse(ex.Message));
            }
        }

        /// <summary>
        /// Lưu trữ và Update bài viết
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///
        ///     POST api/article/archive
        ///     {
        ///        "title": "Test tiêu đề tin",
        ///        "officerId": "123456"
        ///     }
        ///
        /// </remarks>  
        /// <param name="news"></param>
        /// <param name="officerId"> id tài khoản office</param>
        /// <returns>A newly created TodoItem</returns>
        /// <response code="401">Authorization has been denied for this request</response>
        /// <response code="404">Not Found</response>
        /// <response code="405">Invalid input</response>
        [HttpPost, Route("archive")]
        public async Task<IActionResult> ArchiveAsync([FromForm]Article news, [FromForm]long officerId)
        {
            try
            {
                string sessionId = HttpContext?.Request?.Headers?["session-id"];
                if (string.IsNullOrEmpty(sessionId))
                {
                    return Ok(ActionResponse<string>.CreateErrorResponse("session-id not empty"));
                }
                var clientIP = HttpContext.GetClientIP();

                var result = ErrorCodes.BusinessError;
                if (news == null)
                {
                    return Ok(ActionResponse<string>.CreateErrorResponse(Current[ErrorCodes.NewsNotExits]));
                }
                if (!Utility.ValidateStringIds(news.Tags))
                {
                    return Ok(ActionResponse<string>.CreateErrorResponse("Tags không hợp lệ."));
                }
                int[] newsTemps = Enum.GetValues(typeof(ArticleTemplateId)) as int[];
                if (newsTemps != null && !newsTemps.Contains(news.TemplateId ?? 0))
                {
                    return Ok(ActionResponse<string>.CreateErrorResponse("Bạn chưa chọn Template cho tin."));
                }
                if (string.IsNullOrEmpty(news.Title))
                {
                    return Ok(ActionResponse<string>.CreateErrorResponse("Tiêu đề tin không được để trống."));
                }

                if (news.Id > 0)
                {
                    news.ModifiedDate = DateTime.Now;
                    if (long.TryParse(User.FindFirst(ClaimTypes.NameIdentifier)?.Value, out long userId))
                    {
                        news.ModifiedBy = userId.ToString();
                        if (string.IsNullOrEmpty(news.OriginalUrl))
                            news.OriginalUrl = news.Url;
                        
                        result = await ArticleService.UpdateAsync(news, clientIP, officerId, sessionId);
                        if (result == ErrorCodes.Success)
                        {
                            //insert log
                            await Function.AddToQueue(ActionName.InsertLog, TopicName.LOGACTION, new LogActionEntity
                            {
                                ObjectId = news.Id.ToString(),
                                Account = userId.ToString(),
                                CreatedDate = DateTime.SpecifyKind(DateTime.Now, DateTimeKind.Unspecified),
                                Ip = clientIP,
                                Message = "Success.",
                                Title = news.Title,
                                SourceId = officerId.ToString(),
                                ActionTypeDetail = (int)ActionType.Update,
                                ActionText = ActionType.Update.ToString(),
                                Type = (int)LogContentType.Article
                            });
                        }
                    }
                    else
                    {
                        result = ErrorCodes.PermissionInvalid;
                    }
                }
                else
                {
                    var articleSearch = Mapper<ArticleSearch>.Map(news, new ArticleSearch());
                    articleSearch.Type = (int)NewsType.Article;
                    articleSearch.Id = Generator.NewsId();
                    articleSearch.CardType = (int)CardType.Article;
                    news.Id = articleSearch.Id;
                    articleSearch.CreatedDate = DateTime.Now;
                    articleSearch.Status = (int)NewsStatus.Archive;
                    articleSearch.DistributorId = articleSearch.DistributorId.HasValue ? articleSearch.DistributorId : AppSettings.Current.Distribution.Automation.DistributorId;
                    articleSearch.ApprovedDate = null;
                    if (long.TryParse(User.FindFirst(ClaimTypes.NameIdentifier)?.Value, out long userId))
                    {
                        articleSearch.NewsInAccount = new[] {
                            new NewsInAccount
                            {
                                AccountId = officerId,
                                NewsId = articleSearch.Id,
                                PublishedDate = DateTime.Now,
                                PublishedType = (int)PublishedType.Post
                            }
                        };
                        articleSearch.CreatedBy = userId.ToString();

                        if (string.IsNullOrEmpty(articleSearch.OriginalUrl))
                            articleSearch.OriginalUrl = articleSearch.Url;

                        result = await ArticleService.AddAsync(articleSearch, clientIP);
                        if (result == ErrorCodes.Success)
                        {
                            //insert log
                            await Function.AddToQueue(ActionName.InsertLog, TopicName.LOGACTION, new LogActionEntity
                            {
                                ObjectId = articleSearch.Id.ToString(),
                                Account = userId.ToString(),
                                CreatedDate = DateTime.SpecifyKind(DateTime.Now, DateTimeKind.Unspecified),
                                Ip = clientIP,
                                Message = "Success.",
                                Title = articleSearch.Title,
                                SourceId = officerId.ToString(),
                                ActionTypeDetail = (int)ActionType.Update,
                                ActionText = ActionType.Update.ToString(),
                                Type = (int)LogContentType.Article
                            });
                        }
                    }
                    else
                    {
                        result = ErrorCodes.PermissionInvalid;
                    }
                }
                if (result == ErrorCodes.Success)
                {
                    return Ok(ActionResponse<object>.CreateSuccessResponse(news.EncryptId, "success"));
                }
                else
                {
                    return Ok(ActionResponse<string>.CreateErrorResponse(Current[result]));
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
                return Ok(ActionResponse<string>.CreateErrorResponse(ex.Message));
            }
        }

        /// <summary>
        /// Cập nhận trạng thái (V2)
        /// </summary>
        /// <param name="id"></param>
        /// <param name="action"></param>
        /// <param name="officerId"></param>
        /// <returns></returns>
        /// <response code="401">Authorization has been denied for this request</response>
        /// <response code="404">Not Found</response>
        /// <response code="405">Invalid input</response>
        // POST api/article/update_status       
        [HttpPost, Route("update_status")]
        public async Task<IActionResult> UpdateStatusV2Async([FromForm]long id, [FromForm]NewsStatus action, [FromForm] long officerId)
        {
            try
            {
                string sessionId = HttpContext?.Request?.Headers?["session-id"];
                if (string.IsNullOrEmpty(sessionId))
                {
                    return Ok(ActionResponse<string>.CreateErrorResponse("session-id not empty"));
                }
                var clientIP = HttpContext.GetClientIP();

                var result = ErrorCodes.BusinessError;                
                long.TryParse(User.FindFirst(ClaimTypes.NameIdentifier)?.Value, out long userId);

                result = await ArticleService.UpdateStatusV2Async(id, action, userId, clientIP, sessionId);
                if (result == ErrorCodes.Success)
                {
                    //insert log
                    await Function.AddToQueue(ActionName.InsertLog, TopicName.LOGACTION, new LogActionEntity
                    {
                        ObjectId = id.ToString(),
                        Account = userId.ToString(),
                        CreatedDate = DateTime.SpecifyKind(DateTime.Now, DateTimeKind.Unspecified),
                        Ip = clientIP,
                        Message = "Success.",
                        Title = "",
                        SourceId = officerId.ToString(),
                        ActionTypeDetail = (int)ActionType.UpdateStatus,
                        ActionText = ActionType.UpdateStatus.ToString(),
                        Type = (int)LogContentType.Article
                    });
                    return Ok(ActionResponse<object>.CreateSuccessResponse(id.ToString(), "success"));
                }
                return Ok(ActionResponse<string>.CreateErrorResponse(Current[result]));
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
                return Ok(ActionResponse<string>.CreateErrorResponse(ex.Message));
            }
        }

        /// <summary>
        /// Lấy thông tin chi tiết bài viết
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        /// <response code="401">Authorization has been denied for this request</response>
        /// <response code="404">Not Found</response>
        /// <response code="405">Invalid input</response>
        // POST api/article/get_by_id        
        [HttpPost, Route("get_by_id")]
        public async Task<IActionResult> GetByIdAsync([FromForm]long id)
        {
            try
            {
                var result = await ArticleService.GetByIdAsync(id);
                if (result != null && result.Id > 0) return Ok(ActionResponse<Article>.CreateSuccessResponse(result));
                return Ok(ActionResponse<string>.CreateErrorResponse("Article not exist."));
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
                return Ok(ActionResponse<string>.CreateErrorResponse(ex.Message));
            }
        }


        /// <summary>
        /// Lấy thông tin chi tiết bài viết (publish)
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        /// <response code="401">Authorization has been denied for this request</response>
        /// <response code="404">Not Found</response>
        /// <response code="405">Invalid input</response>
        //API cho tường
        [HttpPost, Route("public/get_detail_by_id")]
        public async Task<IActionResult> Public_GetDetailByIdAsync([FromForm]long id)
        {
            try
            {
                var consumerName = User.FindFirst(ClaimTypes.NameIdentifier)?.Value;
                AppSettings.Current.ChannelConfiguration.Consumers.TryGetValue(consumerName.ToLower(), out Consumer consumer);
                if (consumer == null || consumer.ConsumerId <= 0 || consumer.Type != 2)
                {
                    return Ok(ActionResponse<string>.CreateErrorResponse("NameSpace: " + consumerName + " không được hỗ trợ."));
                }
                var result = await ArticleService.GetDetailByIdAsync(id);
                if (result != null && result.News != null && result.News.Status == (int)NewsStatus.Published && result.News.DistributorId == consumer.ConsumerId)
                {
                    return Ok(ActionResponse<ArticleDetail>.CreateSuccessResponse(result));
                }
                else
                {
                    return Ok(ActionResponse<string>.CreateErrorResponse("News: \"" + id + "\" chưa được publish."));
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
                return Ok(ActionResponse<string>.CreateErrorResponse(ex.Message));
            }
        }


        /// <summary>
        /// Danh sách bài biết theo user (publish)
        /// </summary>
        /// <param name="UserName"></param>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        /// <response code="401">Authorization has been denied for this request</response>
        /// <response code="404">Not Found</response>
        /// <response code="405">Invalid input</response>
        // POST api/news/list_my_news      
        //API cho tường
        [HttpPost, Route("public/list_news_by_author")]
        public async Task<IActionResult> ListNewsByAuthorAsync([FromForm]string UserName, [FromForm] int pageIndex, [FromForm] int pageSize)
        {
            try
            {
                var consumerName = User.FindFirst(ClaimTypes.NameIdentifier)?.Value;
                AppSettings.Current.ChannelConfiguration.Consumers.TryGetValue(consumerName.ToLower(), out Consumer consumer);
                if (consumer == null || consumer.ConsumerId <= 0 || consumer.Type != 2)
                {
                    return Ok(ActionResponse<string>.CreateErrorResponse("NameSpace: " + consumerName + " không được hỗ trợ."));
                }
                pageIndex = pageIndex < 1 ? 1 : pageIndex;
                pageSize = pageSize < 1 ? 10 : pageSize;

                var currentAcc = await SecurityService.GetByUsernameAsync(UserName);
                if (currentAcc == null)
                {
                    return Ok(ActionResponse<string>.CreateErrorResponse("UserName: " + UserName + " không tồn tại!"));
                }
                var result = await ArticleService.ListNewsByAuthorAsync(currentAcc.Id, consumer.ConsumerId ?? 0, pageIndex, pageSize);
                return Ok(ActionResponse<PagingDataResult<ArticleOnWall>>.CreateSuccessResponse(result)); //AuthorInfo = accInfo,
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
                return Ok(ActionResponse<string>.CreateErrorResponse(ex.Message));
            }

        }

        /// <summary>
        /// Số bài biết theo user name (publish)
        /// </summary>
        /// <param name="UserName"></param>
        /// <returns></returns>
        /// <response code="401">Authorization has been denied for this request</response>
        /// <response code="404">Not Found</response>
        /// <response code="405">Invalid input</response>
        //POST api/news/get_publish_count => getway index 50
        [HttpPost, Route("public/get_news_count")]
        public async Task<IActionResult> GetPublishCountAsync(string UserName)
        {
            try
            {
                var consumerName = User.FindFirst(ClaimTypes.NameIdentifier)?.Value;
                AppSettings.Current.ChannelConfiguration.Consumers.TryGetValue(consumerName.ToLower(), out Consumer consumer);
                if (consumer == null || consumer.ConsumerId <= 0 || consumer.Type != 2)
                {
                    return Ok(ActionResponse<string>.CreateErrorResponse("NameSpace: " + consumerName + " không được hỗ trợ."));
                }
                var currentAcc = await SecurityService.GetByUsernameAsync(UserName);
                if (currentAcc == null)
                {
                    return Ok(ActionResponse<string>.CreateErrorResponse("UserName: " + UserName + " không tồn tại!"));
                }
                var result = await ArticleService.GetPublishCountAsync(currentAcc.Id, (int)NewsStatus.Published, consumer.ConsumerId);

                return Ok(ActionResponse<object>.CreateSuccessResponse(new { Count = result }, "success"));
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
                return Ok(ActionResponse<string>.CreateErrorResponse(ex.Message));
            }
        }


        /// <summary>
        /// Danh sách ItemStreamDistribution (publish)
        /// </summary>
        /// <param name="getNewsByDistributionEntity"></param>
        /// <returns></returns>
        /// <response code="401">Authorization has been denied for this request</response>
        /// <response code="404">Not Found</response>
        /// <response code="405">Invalid input</response>
        // POST api/news/list_news_by_distribution_id
        [HttpPost, Route("public/list_item_stream_by_distribution_id")]
        public async Task<IActionResult> ListItemStreamByDistributionIdAsync([FromForm]GetNewsByDistribution getNewsByDistributionEntity)
        {
            try
            {
                var consumerName = User.FindFirst(ClaimTypes.NameIdentifier)?.Value;
                AppSettings.Current.ChannelConfiguration.Consumers.TryGetValue(consumerName.ToLower(), out Consumer consumer);
                if (consumer != null && consumer.ConsumerId > 0)
                {
                    getNewsByDistributionEntity.DistributionId = consumer.ConsumerId;
                }
                else
                    return Ok(ActionResponse<string>.CreateErrorResponse("NameSpace: " + consumerName + " không được hỗ trợ."));
                if (getNewsByDistributionEntity.PublishStatus == null)
                {
                    getNewsByDistributionEntity.PublishStatus = DistributionPublishStatus.Send;
                }
                getNewsByDistributionEntity.DistributionId = consumer.ConsumerId;
                var result = await ArticleService.ListItemStreamByDistributionAsync(getNewsByDistributionEntity);
                if (result != null && result.Data == null)
                {
                    result.Data = new List<ItemStreamDistribution>();
                }
                return Ok(ActionResponse<PagingDataResult<ItemStreamDistribution>>.CreateSuccessResponse(result));
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
                return Ok(ActionResponse<string>.CreateErrorResponse(ex.Message));
            }
        }

        /// <summary>
        /// Lấy số ItemStreamDistribution (publish)
        /// </summary>
        /// <param name="distributionId"></param>
        /// <param name="publishStatus"></param>
        /// <returns></returns>
        /// <response code="401">Authorization has been denied for this request</response>
        /// <response code="404">Not Found</response>
        /// <response code="405">Invalid input</response>
        //// POST api/news/list_news_by_distribution_id
        [HttpPost, Route("public/get_count_item_stream_by_distribution_id")]
        public async Task<IActionResult> GetCountItemStreamByDistributionIdAsync([FromForm]long? distributionId, [FromForm] DistributionPublishStatus? publishStatus)
        {
            try
            {
                var consumerName = User.FindFirst(ClaimTypes.NameIdentifier)?.Value;
                AppSettings.Current.ChannelConfiguration.Consumers.TryGetValue(consumerName.ToLower(), out Consumer consumer);
                if (consumer != null && consumer.ConsumerId > 0)
                {
                    distributionId = consumer.ConsumerId;
                }
                else
                    return Ok(ActionResponse<string>.CreateErrorResponse("NameSpace: " + consumerName + " không được hỗ trợ."));
                if (publishStatus == null)
                {
                    publishStatus = DistributionPublishStatus.Send;
                }
                var result = await ArticleService.GetCountItemStreamByDistributionAsync(distributionId.Value, (int)publishStatus);
                return Ok(ActionResponse<object>.CreateSuccessResponse(new { Count = result.ToString() }));
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
                return Ok(ActionResponse<string>.CreateErrorResponse(ex.Message));
            }
        }

        /// <summary>
        /// Tìm kiếm Article
        /// </summary>
        /// <param name="search"></param>
        /// <returns></returns>
        /// <response code="401">Authorization has been denied for this request</response>
        /// <response code="404">Not Found</response>
        /// <response code="405">Invalid input</response>
        // POST api/news/search => getway index 47
        [HttpPost, Route("search")]
        public async Task<IActionResult> SearchAsync([FromForm]SearchNews search)
        {
            try
            {
                if (search == null || search.OfficerId == null)
                {
                    return Ok(ActionResponse<ArticleSearch>.CreateSuccessResponse("Không tìm thấy bài viết nào."));
                }
                search.PageIndex = search.PageIndex == null || search.PageIndex < 1 ? 1 : search.PageIndex;
                search.PageSize = search.PageSize == null || search.PageSize < 1 ? 20 : search.PageSize;
                search.FromDate = search.FromDate.HasValue ? search.FromDate.Value.AddHours(0).AddMinutes(0).AddSeconds(0) : DateTime.MinValue;
                search.ToDate = search.ToDate.HasValue ? search.ToDate.Value.AddHours(23).AddMinutes(59).AddSeconds(59) : DateTime.MaxValue;
                long.TryParse(User.FindFirst(ClaimTypes.NameIdentifier)?.Value, out long accountId);

                var result = await ArticleService.SearchAsync(search, accountId);

                return Ok(ActionResponse<PagingDataResult<ArticleSearchReturn>>.CreateSuccessResponse(result));
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
                return Ok(ActionResponse<string>.CreateErrorResponse(ex.Message));
            }
        }

        /// <summary>
        /// Accept  ItemStreamDistribution
        /// </summary>
        /// <param name="distributionId"></param>
        /// <param name="itemStreamId"></param>
        /// <param name="note"></param>
        /// <param name="officerId"></param>
        /// <returns></returns>
        /// <response code="401">Authorization has been denied for this request</response>
        /// <response code="404">Not Found</response>
        /// <response code="405">Invalid input</response>
        // POST api/news/accept        
        [HttpPost, Route("public/accept")]
        public async Task<IActionResult> AcceptAsync([FromForm]long? distributionId, [FromForm] long itemStreamId, [FromForm]string note, [FromForm] long officerId)
        {
            try
            {
                var clientIP = string.IsNullOrEmpty(HttpContext?.Connection?.RemoteIpAddress?.ToString()) ? "192.168.1.1" : HttpContext?.Connection?.RemoteIpAddress?.ToString();
                long.TryParse(User.FindFirst(ClaimTypes.NameIdentifier)?.Value, out long userId);
                //check cho dau IMS
                var consumerName = User.FindFirst(ClaimTypes.NameIdentifier)?.Value;
                AppSettings.Current.ChannelConfiguration.Consumers.TryGetValue(consumerName.ToLower(), out Consumer consumer);
                if (consumer != null && consumer.ConsumerId > 0)
                {
                    distributionId = consumer.ConsumerId;
                }
                else
                    return Ok(ActionResponse<string>.CreateErrorResponse("NameSpace: " + consumerName + " không được hỗ trợ."));
                var itemStream = await ArticleService.GetItemStreamByIdAsync(itemStreamId);
                var isValid = await DistributionService.CheckItemStreamDistributionAsync(distributionId ?? 0, itemStreamId);
                if (itemStream == null || distributionId == null || !isValid)
                {
                    return Ok(ActionResponse<string>.CreateErrorResponse("Không thể Accept được itemStream."));
                }
                if (distributionId.Value <= 0 || itemStreamId <= 0)
                {
                    return Ok(ActionResponse<string>.CreateErrorResponse("Không thể Accept được itemStream."));
                }
                itemStream.PublishStatus = (int)NewsDistributionPublishStatus.Accept;
                itemStream.AcceptedDate = DateTime.Now;
                itemStream.Note = note;
                itemStream.ModifiedDate = DateTime.Now;
                var result = await ArticleService.AcceptAsync(itemStream);

                if (result)
                {
                    await Function.AddToQueue(ActionName.InsertLog, TopicName.LOGACTION, new LogActionEntity
                    {
                        ObjectId = itemStreamId.ToString(),
                        Account = userId.ToString(),
                        CreatedDate = DateTime.SpecifyKind(DateTime.Now, DateTimeKind.Unspecified),
                        Ip = clientIP,
                        Message = "Success.",
                        Title = "",
                        SourceId = officerId.ToString(),
                        ActionTypeDetail = (int)ActionType.AcceptItemStream,
                        ActionText = ActionType.AcceptItemStream.ToString(),
                        Type = (int)LogContentType.Article
                    });
                    return Ok(ActionResponse<object>.CreateSuccessResponse(itemStream.EncryptId, "Accept thành công."));
                }
                return Ok(ActionResponse<string>.CreateErrorResponse("Accept tin bài xảy ra lỗi."));
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
                return Ok(ActionResponse<string>.CreateErrorResponse(ex.Message));
            }
        }

        /// <summary>
        /// Reject  ItemStreamDistribution
        /// </summary>
        /// <param name="distributionId"></param>
        /// <param name="itemStreamId"></param>
        /// <param name="officerId"></param>
        /// <returns></returns>
        /// <response code="401">Authorization has been denied for this request</response>
        /// <response code="404">Not Found</response>
        /// <response code="405">Invalid input</response>
        // POST api/news/reject        
        [HttpPost, Route("public/reject")]
        public async Task<IActionResult> Reject([FromForm]long? distributionId, [FromForm] long itemStreamId, [FromForm] long officerId)
        {
            try
            {
                var clientIP = string.IsNullOrEmpty(HttpContext?.Connection?.RemoteIpAddress?.ToString()) ? "192.168.1.1" : HttpContext?.Connection?.RemoteIpAddress?.ToString();
                long.TryParse(User.FindFirst(ClaimTypes.NameIdentifier)?.Value, out long userId);

                //check cho dau IMS
                var consumerName = User.FindFirst(ClaimTypes.NameIdentifier)?.Value;
                AppSettings.Current.ChannelConfiguration.Consumers.TryGetValue(consumerName.ToLower(), out Consumer consumer);
                if (consumer != null && consumer.ConsumerId > 0)
                {
                    distributionId = consumer.ConsumerId;
                }
                else
                    return Ok(ActionResponse<string>.CreateErrorResponse("NameSpace: " + consumerName + " không được hỗ trợ."));
                var itemStream = await ArticleService.GetItemStreamByIdAsync(itemStreamId);
                var isValid = await DistributionService.CheckItemStreamDistributionAsync(distributionId ?? 0, itemStreamId);
                if (itemStream == null || distributionId == null || isValid)
                {
                    return Ok(ActionResponse<string>.CreateErrorResponse("ItemStream không tồn tại."));
                }
                if (distributionId.Value <= 0 || itemStreamId <= 0)
                {
                    return Ok(ActionResponse<string>.CreateErrorResponse("Không thể Reject được tin bài."));
                }
                var result = await ArticleService.RejectAsync(itemStream);

                if (result)
                {
                    await Function.AddToQueue(ActionName.InsertLog, TopicName.LOGACTION, new LogActionEntity
                    {
                        ObjectId = itemStreamId.ToString(),
                        Account = userId.ToString(),
                        CreatedDate = DateTime.SpecifyKind(DateTime.Now, DateTimeKind.Unspecified),
                        Ip = clientIP,
                        Message = "Success.",
                        Title = "",
                        SourceId = officerId.ToString(),
                        ActionTypeDetail = (int)ActionType.RejectItemStream,
                        ActionText = ActionType.RejectItemStream.ToString(),
                        Type = (int)LogContentType.Article
                    });
                    return Ok(ActionResponse<object>.CreateSuccessResponse(itemStream.EncryptId, "Reject thành công."));
                }
                else
                    return Ok(ActionResponse<string>.CreateErrorResponse("Reject tin bài xảy ra lỗi."));
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
                return Ok(ActionResponse<string>.CreateErrorResponse(ex.Message));
            }
        }

        /// <summary>
        /// Lấy số bài viết
        /// </summary>
        /// <returns></returns>
        /// <response code="401">Authorization has been denied for this request</response>
        /// <response code="404">Not Found</response>
        /// <response code="405">Invalid input</response>
        //POST api/news/get_publish_count => getway index 50
        [HttpPost, Route("get_news_count")]
        public async Task<IActionResult> GetNewsCount()
        {
            try
            {
                long.TryParse(User.FindFirst(ClaimTypes.NameIdentifier)?.Value, out long accountId);
                var result = await ArticleService.GetPublishCountAsync(accountId, -1, null);
                return Ok(ActionResponse<object>.CreateSuccessResponse(new { Count = result }, "success"));
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
                return Ok(ActionResponse<string>.CreateErrorResponse(ex.Message));
            }
        }

        /// <summary>
        /// Lấy danh sách bài viết đang chờ duyệt
        /// </summary>
        /// <param name="searchEntity"></param>
        /// <returns></returns>
        /// <response code="401">Authorization has been denied for this request</response>
        /// <response code="404">Not Found</response>
        /// <response code="405">Invalid input</response>
        [HttpPost, Route("get_list_approve")]
        public async Task<IActionResult> GetListApproveAsync([FromForm]SearchNews searchEntity)
        {
            try
            {
                if (searchEntity == null)
                {
                    return Ok(ActionResponse<string>.CreateSuccessResponse("Không tìm thấy bài viết nào."));
                }
                if (searchEntity.OfficerId <= 0)
                {
                    return Ok(ActionResponse<string>.CreateErrorResponse("OfficerId không hợp lệ."));
                }

                searchEntity.Status = ((int)NewsStatus.Approve).ToString();
                searchEntity.PageIndex = searchEntity.PageIndex < 1 ? 1 : searchEntity.PageIndex;
                searchEntity.PageSize = searchEntity.PageSize < 1 ? 20 : searchEntity.PageSize;
                searchEntity.FromDate = searchEntity.FromDate.HasValue ? searchEntity.FromDate.Value.AddHours(0).AddMinutes(0).AddSeconds(0) : DateTime.MinValue;
                searchEntity.ToDate = searchEntity.ToDate.HasValue ? searchEntity.ToDate.Value.AddHours(23).AddMinutes(59).AddSeconds(59) : DateTime.Now;
                long.TryParse(User.FindFirst(ClaimTypes.NameIdentifier)?.Value, out long accountId);

                var result = await ArticleService.GetListApproveAsync(searchEntity, accountId);

                return Ok(ActionResponse<PagingDataResult<ArticleSearchReturn>>.CreateSuccessResponse(result));
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
                return Ok(ActionResponse<string>.CreateErrorResponse(ex.Message));
            }
        }

        /// <summary>
        /// Add vào bảng tin
        /// </summary>
        /// <param name="mediaId"></param>
        /// <param name="boardIds"></param>
        /// <param name="officerId"></param>
        /// <returns></returns>
        /// <response code="401">Authorization has been denied for this request</response>
        /// <response code="404">Not Found</response>
        /// <response code="405">Invalid input</response>
        // POST api/videoplaylist/get_video_by_playlist_id => getway index 93
        [HttpPost, Route("add_to_board")]
        public async Task<IActionResult> AddToBoardAsync([FromForm]long mediaId, [FromForm] long[] boardIds, [FromForm]long officerId)
        {
            try
            {
                var clientIP = string.IsNullOrEmpty(HttpContext?.Connection?.RemoteIpAddress?.ToString()) ? "192.168.1.1" : HttpContext?.Connection?.RemoteIpAddress?.ToString();
                long.TryParse(User.FindFirst(ClaimTypes.NameIdentifier)?.Value, out long userId);
                var dataDb = await BoardService.GetListByIdAsync(boardIds);
                if (dataDb == null)
                {
                    return Ok(ActionResponse<string>.CreateErrorResponse("Bảng tin không tồn tại."));
                }
                else
                {
                    if (!(await BoardService.CheckBoardOfUser(officerId, userId, boardIds)))
                    {
                        return Ok(ActionResponse<string>.CreateErrorResponse("Bạn không có quyền chỉnh sửa Bảng tin."));
                    }
                }

                dataDb.ForEach(p =>
                {
                    p.LastInsertedDate = DateTime.Now;
                    p.ModifiedBy = userId.ToString();
                    p.ModifiedDate = DateTime.Now;
                });

                var result = await BoardService.InsertMediaAsync(mediaId, dataDb);
                if (result)
                {
                    foreach (var boardId in boardIds)
                    {
                        await Function.AddToQueue(ActionName.AddNewsToBoard, TopicName.PUSHBOARDTOCHANNEL, new QueueData<long, string, long, long>
                        {
                            Data1 = boardId,
                            Data2 = mediaId.ToString(),
                            Data3 = officerId,
                            Data4 = userId
                        });
                    }
                   
                    await Function.AddToQueue(ActionName.InsertLog, TopicName.LOGACTION, new LogActionEntity
                    {
                        ObjectId = mediaId.ToString(),
                        Account = userId.ToString(),
                        CreatedDate = DateTime.SpecifyKind(DateTime.Now, DateTimeKind.Unspecified),
                        Ip = clientIP,
                        Message = "Success.",
                        Title = "",
                        SourceId = officerId.ToString(),
                        ActionTypeDetail = (int)ActionType.AddMediaToBoard,
                        ActionText = ActionType.AddMediaToBoard.ToString(),
                        Type = (int)LogContentType.Article
                    });
                    return Ok(ActionResponse<string>.CreateSuccessResponse("Add album success."));
                }
                else return Ok(ActionResponse<string>.CreateErrorResponse("Add album unsuccess."));
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
                return Ok(ActionResponse<string>.CreateErrorResponse(ex.Message));
            }
        }
    }
}
