﻿
namespace ChannelVN.IMS2.WebApi
{
    public static class HttpContextExtensions
    {
        public static string GetSessionId(this Microsoft.AspNetCore.Http.HttpContext context)
        {
            return context?.Request?.Headers?["session-id"];
        }

        public static string GetClientIP(this Microsoft.AspNetCore.Http.HttpContext context)
        {
            return string.IsNullOrEmpty(context?.Connection?.RemoteIpAddress?.ToString()) ? "0.0.0.0" : context?.Connection?.RemoteIpAddress?.ToString();
        }
    }
}
