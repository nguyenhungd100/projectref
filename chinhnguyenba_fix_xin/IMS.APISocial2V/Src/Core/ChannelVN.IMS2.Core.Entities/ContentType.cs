﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ChannelVN.IMS2.Core.Entities
{
    public enum ContentType
    {
        Video = 1,
        Photo = 2,
        News = 3,
        Autdio = 4,
        Sport = 5,//data bong đá sport 5
        Trending = 6,//data trending
        Image = 7,//ảnh gif
        Linkshare = 8,

        Sticker = 9,
        Blog = 10,
        TextRichMedia = 12,
        Url = 14,
        Quote = 16,
        StrongBox = 17
    }
}
