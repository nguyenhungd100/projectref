﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ChannelVN.IMS2.Core.Entities
{
    public class SearchNews
    {
        public long Id { get; set; }
        public string Keyword { get; set; }
        public NewsType? Type { get; set; }
        public long? DistributionId { get; set; }
        public long? OfficerId { get; set; }
        public int? CategoryId { get; set; }
        public string Status { get; set; }
        public int? PublishMode { get; set; }
        public string Author { get; set; }
        public DateTime? FromDate { get; set; }
        public DateTime? ToDate { get; set; }
        public int? SearchDateBy { get; set; }
        public SearchNewsOrderBy? OrderBy { get; set; }
        public int? PageIndex { get; set; }
        public int? PageSize { get; set; }

    }
}
