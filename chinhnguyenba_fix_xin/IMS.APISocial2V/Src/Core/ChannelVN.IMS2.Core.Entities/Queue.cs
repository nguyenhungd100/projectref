﻿using ChannelVN.IMS2.Foundation.Data;
using System;
using System.Collections.Generic;
using System.Text;

namespace ChannelVN.IMS2.Core.Entities
{
    public class Queue<T> : Entity
    {
        public DateTime? CreatedDate { get; set; }
        public string Action { get; set; }
        public T Data { get; set; }
    }
}
