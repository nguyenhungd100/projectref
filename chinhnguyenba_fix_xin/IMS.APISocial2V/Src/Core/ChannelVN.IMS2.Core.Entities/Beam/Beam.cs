﻿using ChannelVN.IMS2.Foundation.Data;
using Nest;
using System;
using System.Collections.Generic;
using System.Text;

namespace ChannelVN.IMS2.Core.Entities.Beam
{
    public interface IBeamSearch
    {
         string Name { get; set; }
         string UnsignName { get; set; }
         string Description { get; set; }
         DateTime? LastInsertedDate { get; set; }
         string BeamRelation { get; set; }
         string MetaAvatar { get; set; }
         string MetaData { get; set; }
         string Cover { get; set; }
         ArticleInBeam[] ArticleInBeam { get; set; }
    }
    public class Beam: News
    {
        public string Name { get; set; }
        public string UnsignName { get; set; }
        public string Description { get; set; }
        private DateTime? _LastInsertedDate { get; set; }
        public DateTime? LastInsertedDate
        {
            get
            {
                return _LastInsertedDate;
            }
            set
            {
                _LastInsertedDate = value != null ? DateTime.SpecifyKind(value.Value, DateTimeKind.Unspecified) : value;
            }
        }
        public string BeamRelation { get; set; }
        public string MetaAvatar { get; set; }
        public string MetaData { get; set; }
        public string Cover { get; set; }
    }

    public class BeamSearch : NewsSearch
    {
        public string Name { get; set; }
        [Ignore]
        public string UnsignName { get; set; }
        [Ignore]
        public string Description { get; set; }
        [Ignore]
        private DateTime? _LastInsertedDate { get; set; }
        [Ignore]
        public DateTime? LastInsertedDate
        {
            get
            {
                return _LastInsertedDate;
            }
            set
            {
                _LastInsertedDate = value != null ? DateTime.SpecifyKind(value.Value, DateTimeKind.Unspecified) : value;
            }
        }
        [Ignore]
        public string BeamRelation { get; set; }
        [Ignore]
        public string MetaAvatar { get; set; }
        [Ignore]
        public string MetaData { get; set; }
        public string Cover { get; set; }
        [Ignore]
        public ArticleInBeam[] ArticleInBeam { get; set; }
    }
    public class BeamSearchReturn : NewsSearchReturn
    {
        public string Name { get; set; }
        public string Cover { get; set; }
        public ArticleInBeam[] ArticleInBeam { get; set; }
    }

    public class ArticleInBeam
    {
        public long BeamId { get; set; }
        public string EncryptBeamId { get { return BeamId.ToString(); } }
        public long ArticleId { get; set; }
        public string EncryptArticleId { get { return ArticleId.ToString(); } }
        public int? Priority { get; set; }
        public int? DataType { get; set; }
        private DateTime? _PublishedDate { get; set; }
        public DateTime? PublishedDate
        {
            get
            {
                return _PublishedDate;
            }
            set
            {
                _PublishedDate = value != null ? DateTime.SpecifyKind(value.Value, DateTimeKind.Unspecified) : value;
            }
        }
    }

}
