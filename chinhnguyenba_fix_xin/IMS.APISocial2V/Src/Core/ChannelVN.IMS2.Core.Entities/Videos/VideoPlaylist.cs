﻿using Nest;
using System;
using System.Collections.Generic;
using System.Text;

namespace ChannelVN.IMS2.Core.Entities.Videos
{
    public interface IVideoPlaylist
    {
         string Name { get; set; }
         string UnsignName { get; set; }
         string Description { get; set; }
         DateTime? LastInsertedDate { get; set; }
         string Cover { get; set; }
         string IntroClip { get; set; }
         string PlaylistRelation { get; set; }
         string MetaAvatar { get; set; }
         string MetaData { get; set; }
    }
    public interface IVideoPlaylistSearch
    {
        string Name { get; set; }
        string UnsignName { get; set; }
        string Description { get; set; }
        DateTime? LastInsertedDate { get; set; }
        string Cover { get; set; }
        string IntroClip { get; set; }
        string PlaylistRelation { get; set; }
        string MetaAvatar { get; set; }
        string MetaData { get; set; }
        VideoInPlaylist[] VideoInPlaylist { get; set; }
    }

    public class VideoPlaylistSearch: NewsSearch, IVideoPlaylist
    {
        public string Name { get; set; }
        [Ignore]
        public string UnsignName { get; set; }
        [Ignore]
        public string Description { get; set; }
        [Ignore]
        private DateTime? _LastInsertedDate { get; set; }
        [Ignore]
        public DateTime? LastInsertedDate {
            get
            {
                return _LastInsertedDate;
            }
            set
            {
                _LastInsertedDate = value != null ? DateTime.SpecifyKind(value.Value, DateTimeKind.Unspecified) : value;
            }
        }
        [Ignore]
        public string Cover { get; set; }
        [Ignore]
        public string IntroClip { get; set; }
        [Ignore]
        public string PlaylistRelation { get; set; }
        [Ignore]
        public string MetaAvatar { get; set; }
        [Ignore]
        public string MetaData { get; set; }
        [Ignore]
        public VideoInPlaylist[] VideoInPlaylist { get; set; }
    }
    public class VideoPlaylist: News, IVideoPlaylist
    {
        public string Name { get; set; }
        public string UnsignName { get; set; }
        public string Description { get; set; }
        private DateTime? _LastInsertedDate { get; set; }
        public DateTime? LastInsertedDate {
            get
            {
                return _LastInsertedDate;
            }
            set
            {
                _LastInsertedDate = value != null ? DateTime.SpecifyKind(value.Value, DateTimeKind.Unspecified) : value;
            }
        }
        public string Cover { get; set; }
        public string IntroClip { get; set; }
        public string PlaylistRelation { get; set; }
        public string MetaAvatar { get; set; }
        public string MetaData { get; set; }
        //public string VideoIds { get; set; }
    }
    public class VideoPlaylistSearchReturn: NewsSearchReturn
    {
        public string Name { get; set; }
        private DateTime? _LastInsertedDate { get; set; }
        public DateTime? LastInsertedDate {
            get
            {
                return _LastInsertedDate;
            }
            set
            {
                _LastInsertedDate = value != null ? DateTime.SpecifyKind(value.Value, DateTimeKind.Unspecified) : value;
            }
        }
        public string Cover { get; set; }
        public string IntroClip { get; set; }
        public string MetaAvatar { get; set; }
        public VideoInPlaylist[] VideoInPlaylist { get; set; }
        //public string VideoIds { get; set; }
    }

    public class VideoInPlaylistSearchReturn : NewsSearchReturn
    {

        public string Name { get; set; }
        public string Duration { get; set; }
        public string FileName { get; set; }
        public string MetaAvatar { get; set; }
        public string MetaData { get; set; }
        //public string VideoIds { get; set; }
    }
}
