﻿using ChannelVN.IMS2.Foundation.Data;
using Nest;
using System;
using System.Collections.Generic;
using System.Text;

namespace ChannelVN.IMS2.Core.Entities.Videos
{
    public class VideoInPlaylist:Entity
    {
        [Ignore]
        public long PlaylistId { get; set; }
        [Ignore]
        public string EncryptPlaylistId { get { return PlaylistId.ToString(); } }
        public long VideoId { get; set; }
        public string EncryptVideoId { get { return VideoId.ToString(); } }
        [Ignore]
        public int Priority { get; set; }
        [Ignore]
        private DateTime? _PlayOnTime { get; set; }
        [Ignore]
        public DateTime? PlayOnTime {
            get
            {
                return _PlayOnTime;
            }
            set
            {
                _PlayOnTime = value != null ? DateTime.SpecifyKind(value.Value, DateTimeKind.Unspecified) : value;
            }
        }
    }
}
