﻿using Nest;
using System;
using System.Collections.Generic;
using System.Text;

namespace ChannelVN.IMS2.Core.Entities.Videos
{
    public interface IVideo
    {
         string Name { get; set; }
         string UnsignName { get; set; }
         string Description { get; set; }
         string HtmlCode { get; set; }
         string KeyVideo { get; set; }
         DateTime? UploadedDate { get; set; }
         string Source { get; set; }
         string VideoRelation { get; set; }
         string FileName { get; set; }
         string Duration { get; set; }
         string Size { get; set; }
         long? Capacity { get; set; }
         bool? AllowAdv { get; set; }
         bool? IsRemovedLogo { get; set; }
         bool? IsConverted { get; set; }
         string PolicyContentId { get; set; }
         int? OriginalId { get; set; }
         string Trailer { get; set; }
         string MetaAvatar { get; set; }
         string DisplayStyle { get; set; }
         string MetaData { get; set; }
    }
    public class VideoSearch: NewsSearch, IVideo
    {
        
        public string Name { get; set; }
        [Ignore]
        public string UnsignName { get; set; }
        [Ignore]
        public string Description { get; set; }
        [Ignore]
        public string HtmlCode { get; set; }
        [Ignore]
        public string KeyVideo { get; set; }
        [Ignore]
        private DateTime? _UploadedDate { get; set; }
        [Ignore]
        public DateTime? UploadedDate
        {
            get
            {
                return _UploadedDate;
            }
            set
            {
                _UploadedDate = value !=null ? DateTime.SpecifyKind(value.Value, DateTimeKind.Unspecified) : value;
            }
        }
        [Ignore]
        public string Source { get; set; }
        [Ignore]
        public string VideoRelation { get; set; }
        [Ignore]
        public string FileName { get; set; }
        [Ignore]
        public string Duration { get; set; }
        [Ignore]
        public string Size { get; set; }
        [Ignore]
        public long? Capacity { get; set; }
        [Ignore]
        public bool? AllowAdv { get; set; }
        [Ignore]
        public bool? IsRemovedLogo { get; set; }
        [Ignore]
        public bool? IsConverted { get; set; }
        [Ignore]
        public string PolicyContentId { get; set; }
        [Ignore]
        public int? OriginalId { get; set; }
        [Ignore]
        public string Trailer { get; set; }
        [Ignore]
        public string MetaAvatar { get; set; }
        [Ignore]
        public string DisplayStyle { get; set; }
        [Ignore]
        public string MetaData { get; set; }

    }
    public class Video : News, IVideo
    {

        public string Name { get; set; }
        public string UnsignName { get; set; }
        public string Description { get; set; }
        public string HtmlCode { get; set; }
        public string KeyVideo { get; set; }
        private DateTime? _UploadedDate { get; set; }
        public DateTime? UploadedDate
        {
            get
            {
                return _UploadedDate;
            }
            set
            {
                _UploadedDate = value != null ? DateTime.SpecifyKind(value.Value, DateTimeKind.Unspecified) : value;
            }
        }
        public string Source { get; set; }
        public string VideoRelation { get; set; }
        public string FileName { get; set; }
        public string Duration { get; set; }
        public string Size { get; set; }
        public long? Capacity { get; set; }
        public bool? AllowAdv { get; set; }
        public bool? IsRemovedLogo { get; set; }
        public bool? IsConverted { get; set; }
        public string PolicyContentId { get; set; }
        public int? OriginalId { get; set; }
        public string Trailer { get; set; }
        public string MetaAvatar { get; set; }
        public string DisplayStyle { get; set; }
        public string MetaData { get; set; }
    }

    public class VideoSearchReturn : NewsSearchReturn
    {
       
        public string Name { get; set; }
        public string Duration { get; set; }
        public string FileName { get; set; }
        public string MetaAvatar { get; set; }
    }
}
