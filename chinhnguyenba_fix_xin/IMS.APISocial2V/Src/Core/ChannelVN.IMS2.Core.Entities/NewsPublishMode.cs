﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ChannelVN.IMS2.Core.Entities
{
    public enum NewsPublishMode
    {
        Unlished = 1,
        LogginedIn = 2,
        Public = 3
    }
}
