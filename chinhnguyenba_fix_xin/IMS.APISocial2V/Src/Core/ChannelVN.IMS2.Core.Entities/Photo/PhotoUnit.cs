﻿
namespace ChannelVN.IMS2.Core.Entities.Photo
{
    public interface IPhoto
    {
        string Caption { get; set; }
        string Size { get; set; }
        decimal? Capacity { get; set; }
        PhotoType? ContentType { get; set; }
    }
    public class PhotoUnit : News
    {
        public string Caption { get; set; }
        public string Size { get; set; }
        public decimal? Capacity { get; set; }
        public PhotoType? ContentType { get; set; }
    }

    public class PhotoSearch : NewsSearch
    {
        public string Caption { get; set; }
        public string Size { get; set; }
        public decimal? Capacity { get; set; }
        public PhotoType? ContentType { get; set; }
    }

    public class PhotoSearchReturn : NewsSearchReturn
    {
        public string Caption { get; set; }
        public string PublishData { get; set; }
    }

    public enum PhotoType
    {
        Photo = 2,
        Gif = 7
    }
}
