﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ChannelVN.IMS2.Core.Entities.Security
{
    public class SearchAccountResponse
    {
        public int Status { get; set; }
        public string Message { get; set; }
        public int Code { get; set; }
        public SearchAccountData[] Data { get; set; }
    }
}
