﻿using ChannelVN.IMS2.Foundation.Data;
using System;
using System.Collections.Generic;
using System.Text;

namespace ChannelVN.IMS2.Core.Entities.Security
{
    public class AccountSimple : Entity
    {
        public string Id { get; set; }
        public string UserName { get; set; }
        public string FullName { get; set; }
        public string Avatar { get; set; }
        public string Banner { get; set; }
        public string Mobile { get; set; }
        public string Email { get; set; }
        public int? Status { get; set; }
        public int? Type { get; set; }
        public int? Class { get; set; }
        public string Description { get; set; }
        public string VerifiedBy { get; set; }
        public DateTime? VerifiedDate { get; set; }
        //properties of profile
        public string PenName { get; set; }
        public Byte? Gender { get; set; }
        public DateTime? BirthDay { get; set; }

        public string Address { get; set; }
        public string Title { get; set; }
        public string WorkPlace { get; set; }
        public string School { get; set; }
        public string UrlNetwork { get; set; }
        public string UrlSocial { get; set; }
        public string CreatedBy { get; set; }
        public int? Mode { get; set; }
        public Dictionary<string,List<MemberPermission>> Permissions { get; set; }
        public Byte? CommentMode { get; set; }
        public int? LabelMode { get; set; }
        public long? RelatedId { get; set; }
        public Byte? RelatedType { get; set; }
        public string ProfileUrl { get; set; }
        public string DelegatorId { get; set; }
        public string OtpSecretKey { get; set; }
        //public AccountMember[] AccountMember { get; set; }
    }
    public class AccountSystemSimple : Entity
    {
        public string Id { get; set; }
        public string UserName { get; set; }
        public string FullName { get; set; }
        public string Avatar { get; set; }
       
        public string Mobile { get; set; }
        public string Email { get; set; }
        public int? Status { get; set; }
        public EnumAccountLevels[] Levels { get; set; }
        public int? Type { get; set; }
        public int? Class { get; set; }
        public string Description { get; set; }
        public string VerifiedBy { get; set; }
        public DateTime? VerifiedDate { get; set; }
        public AccountClass[] RestrictClasses { get; set; }
    }
}
