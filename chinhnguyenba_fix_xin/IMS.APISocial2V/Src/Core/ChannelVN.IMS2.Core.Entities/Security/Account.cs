﻿using ChannelVN.IMS2.Foundation.Data;
using Nest;
using System;
using System.Runtime.Serialization;

namespace ChannelVN.IMS2.Core.Entities.Security
{
    public class Account : Entity
    {
        public long Id { get; set; }
        public string EncryptId { get { return Id.ToString(); } }
        public string Mobile { get; set; }
        public string Email { get; set; }
        public string UserName { get; set; }

        public string Password { get; set; }
        public byte? Type { get; set; }
        public byte? Class { get; set; }
        public string FullName { get; set; }

        public string Avatar { get; set; }

        public string Banner { get; set; }
        public string VerifiedBy { get; set; }
        private DateTime? _VerifiedDate { get; set; }
        public DateTime? VerifiedDate {
            get
            {
                return _VerifiedDate;
            }
            set
            {
                _VerifiedDate = value != null ? DateTime.SpecifyKind(value.Value, DateTimeKind.Unspecified) : value;
            }
        }
        public int? Status { get; set; }

        public bool? IsSystem { get; set; }

        public bool? IsFullPermission { get; set; }
        public long? OpenId { get; set; }

        public string OtpSecretKey { get; set; }
        public int? Mode { get; set; }
        public string CreatedBy { get; set; }
        private DateTime? _CreatedDate { get; set; }
        public DateTime? CreatedDate {
            get
            {
                return _CreatedDate;
            }
            set
            {
                _CreatedDate = value != null ? DateTime.SpecifyKind(value.Value, DateTimeKind.Unspecified) : value;
            }
        }
        private DateTime? _LoginedDate { get; set; }
        public DateTime? LoginedDate {
            get
            {
                return _LoginedDate;
            }
            set
            {
                _LoginedDate = value != null ? DateTime.SpecifyKind(value.Value, DateTimeKind.Unspecified) : value;
            }
        }

        public string Description { get; set; }
        public Byte? CommentMode { get; set; }
        public Byte? CrawlerMode { get; set; }
        public string CrawlerSource { get; set; }
        public int? LabelMode { get; set; }
        public long? RelatedId { get; set; }
        public Byte? RelatedType { get; set; }
        public long? DelegatorId { get; set; }
        public EnumAccountLevels[] Levels { get; set; }
        public AccountClass[] RestrictClasses { get; set; }
    }
    public class AccountSearch: Entity
    {
        public long Id { get; set; }
        [Ignore]
        public string EncryptId { get { return Id.ToString(); } }
        public string Mobile { get; set; }
        public string Email { get; set; }
        public string UserName { get; set; }
        [Ignore]
        public string Password { get; set; }
        public byte? Type { get; set; }
        public byte? Class { get; set; }        
        public string FullName { get; set; }
        [Ignore]
        public string Avatar { get; set; }
        [Ignore]
        public string Banner { get; set; }
        public string VerifiedBy { get; set; }
        private DateTime? _VerifiedDate { get; set; }
        public DateTime? VerifiedDate {
            get
            {
                return _VerifiedDate;
            }
            set
            {
                _VerifiedDate = value != null ? DateTime.SpecifyKind(value.Value, DateTimeKind.Unspecified) : value;
            }
        }
        public int? Status { get; set; }
        [Ignore]
        public bool? IsSystem { get; set; }
        [Ignore]
        public bool? IsFullPermission { get; set; }
        [Ignore]
        public string OtpSecretKey { get; set; }
        public int? Mode { get; set; }
        public string CreatedBy { get; set; }
        private DateTime? _CreatedDate { get; set; }
        public DateTime? CreatedDate {
            get {
                return _CreatedDate;
            }
            set
            {
                _CreatedDate = value != null ? DateTime.SpecifyKind(value.Value, DateTimeKind.Unspecified) : value;
            }
        }
        private DateTime? _LoginedDate { get; set; }
        [Ignore]
        public DateTime? LoginedDate
        {
            get
            {
                return _LoginedDate;
            }
            set
            {
                _LoginedDate = value != null ? DateTime.SpecifyKind(value.Value, DateTimeKind.Unspecified) : value;
            }
        }
        [Ignore]
        public string Description { get; set; }        
        public Byte?  CommentMode { get; set; }
        public Byte? CrawlerMode { get; set; }
        public string CrawlerSource { get; set; }
        public int? LabelMode { get; set; }
        public long? RelatedId { get; set; }
        public Byte? RelatedType { get; set; }
        [Ignore]
        public long? DelegatorId { get; set; }
        public EnumAccountLevels[] Levels { get; set; }
        
        [Nested]
        public AccountDistribution[] AccountDistribution { get; set; }
        //[Nested]
        [Ignore]
        public AccountMember[] AccountMember { get; set; }
        public AccountClass[] RestrictClasses { get; set; }
    }

    public class PersonalAccount : AccountSearch
    {
        public PersonalAccount()
        {
            Type = (byte)AccountType.Personal;
        }
    }
    [DataContract]
    public class OfficialAccount : Entity
    {
        //public OfficialAccount()
        //{
        //    Type = (byte)AccountType.Official;
        //}
        [DataMember]
        public long Id { get; set; }
        [DataMember]
        public string EncryptId { get { return Id.ToString(); } }
        [DataMember]
        public string Mobile { get; set; }
        [DataMember]
        public string Email { get; set; }
        [DataMember]
        public string UserName { get; set; }

        public string Password { get; set; }
        [DataMember]
        public byte? Type { get; set; }
        [DataMember]
        public byte? Class { get; set; }
        [DataMember]
        public string FullName { get; set; }
        [DataMember]
        public string Avatar { get; set; }
        [DataMember]
        public string Banner { get; set; }
        public string VerifiedBy { get; set; }
        private DateTime? _VerifiedDate { get; set; }
        public DateTime? VerifiedDate
        {
            get
            {
                return _VerifiedDate;
            }
            set
            {
                _VerifiedDate = value != null ? DateTime.SpecifyKind(value.Value, DateTimeKind.Unspecified) : value;
            }
        }
        [DataMember]
        public int? Status { get; set; }

        public bool? IsSystem { get; set; }

        public bool? IsFullPermission { get; set; }

        public string OtpSecretKey { get; set; }
        [DataMember]
        public int? Mode { get; set; }
        public string CreatedBy { get; set; }
        private DateTime? _CreatedDate { get; set; }
        public DateTime? CreatedDate
        {
            get
            {
                return _CreatedDate;
            }
            set
            {
                _CreatedDate = value != null ? DateTime.SpecifyKind(value.Value, DateTimeKind.Unspecified) : value;
            }
        }
        private DateTime? _LoginedDate { get; set; }
        public DateTime? LoginedDate
        {
            get
            {
                return _LoginedDate;
            }
            set
            {
                _LoginedDate = value != null ? DateTime.SpecifyKind(value.Value, DateTimeKind.Unspecified) : value;
            }
        }
        [DataMember]
        public string Description { get; set; }
        [DataMember]
        public bool IsOwner { get; set; }
        [DataMember]
        public CommentMode? CommentMode { get; set; }
        public Byte? CrawlerMode { get; set; }
        public string CrawlerSource { get; set; }
        [DataMember]
        public int? LabelMode { get; set; }
        public long? RelatedId { get; set; }
        public Byte? RelatedType { get; set; }
        [DataMember]
        public string ProfileUrl { get; set; }
        public long? DelegatorId { get; set; }
        //public new string Password { get { return "b9a97e526e1745103df353f42d65fba8"; } }
    }

    public class SearchAccountEntity
    {
        public string Keyword { get; set; }
        public AccountType? Type { get; set; }
        public bool? IsVerify { get; set; }
        public AccountClass? Class { get; set; }
        public AccountStatus? Status { get; set; }
        public EnumAccountLevels? Role { get; set; }
        public long? Id { get; set; }
        public int? OrderBy { get; set; }
        public int? PageIndex { get; set; }
        public int? PageSize { get; set; }
        public long? CreatedBy { get; set; }
    }
}
