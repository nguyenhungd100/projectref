﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace ChannelVN.IMS2.Core.Entities.Security.VietIdModel
{
    public class LoginMobileResponse
    {
        [JsonProperty("meta")]
        public Meta Metas { get; set; }

        [JsonProperty("data")]
        public Data Datas { get; set; }

        public partial class Data
        {
            [JsonProperty("mobile")]
            public string Mobile { get; set; }

            [JsonProperty("call_active")]
            public long CallActive { get; set; }
        }

        public partial class Meta
        {
            [JsonProperty("signal")]
            public long Signal { get; set; }

            [JsonProperty("error_code")]
            public long ErrorCode { get; set; }

            [JsonProperty("message")]
            public string Message { get; set; }
        }
    }
}
