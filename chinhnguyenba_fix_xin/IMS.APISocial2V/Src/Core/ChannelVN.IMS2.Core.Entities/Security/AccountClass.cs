﻿using System.Collections.Generic;

namespace ChannelVN.IMS2.Core.Entities.Security
{
    public enum AccountClass: byte
    {
        Normal = 0,
        News = 1, //báo
        Exbert = 2, // chuyên gia
        Creator = 3, // nhà sản xuất nội dung // group 6
        Community = 4, //cộng đồng 
        KOL = 5, // KOL // group 6
        Star = 6, //Sao // group 6
        PartnerPromotion = 7, //
        Useful = 8,
        WaitingPrevCheck = 253,// tạm tiền kiểm
        WaitingPostCheck = 254,// tạm hậu kiểm
        Waiting = 255 // tạm
    }

    public enum AccountClassType
    {
        //Others = 0,
        //NewsGroup = 1,
        //ExpertGroup = 2,
        //OfficialAccountGroup = 3,
        //PartnerPromotionGroup = 6,
        //UsefulGroup = 7,
        //WaitingPrevCheckGroup = 253,
        //WaitingPostCheckGroup = 254,
        //WaitingGroup = 255
        Others = 0,
        NewsGroup = 1,
        ExpertGroup = 2,
        OfficialAccountGroup = 3,
        PartnerPromotionGroup = 7,
        UsefulGroup = 8,
        WaitingPrevCheckGroup = 253,
        WaitingPostCheckGroup = 254,
        WaitingGroup = 255
    }

    public class AccountClassDef
    {
        public static Dictionary<AccountClass, AccountClassType> DictAccountClass = new Dictionary<AccountClass, AccountClassType>()
        {
            {AccountClass.Normal, AccountClassType.Others},
            {AccountClass.News, AccountClassType.NewsGroup},
            {AccountClass.Exbert, AccountClassType.ExpertGroup},
            {AccountClass.Creator, AccountClassType.OfficialAccountGroup},
            {AccountClass.KOL, AccountClassType.OfficialAccountGroup},
            {AccountClass.Star, AccountClassType.OfficialAccountGroup},
            {AccountClass.PartnerPromotion, AccountClassType.PartnerPromotionGroup},
            {AccountClass.Useful, AccountClassType.UsefulGroup},
            {AccountClass.WaitingPrevCheck, AccountClassType.WaitingPrevCheckGroup},
            {AccountClass.WaitingPostCheck, AccountClassType.WaitingPostCheckGroup},
            {AccountClass.Waiting, AccountClassType.WaitingGroup},
        };
    }
}
