﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ChannelVN.IMS2.Core.Entities.Security.KingHub
{
    public class KinghubApiResponse
    {
        public int Status { get; set; }
        public string Message { get; set; }
        public int Code { get; set; }
        public KinghubApiResponseData Data { get; set; }
    }
    public class KinghubApiResponseData
    {
        public string UserID { get; set; }
        public string SessionId { get; set; }
    }
}
