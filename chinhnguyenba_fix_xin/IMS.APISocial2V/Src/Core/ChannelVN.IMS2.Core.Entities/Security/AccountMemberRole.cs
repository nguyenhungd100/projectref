﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace ChannelVN.IMS2.Core.Entities.Security
{
    public enum AccountMemberRole
    {
        Owner = 0,
        Admin = 1,
        Approver = 2,
        Poster = 3,
        Editor = 4,
        CommentManager = 5,
        ManagerStatistics = 6
    }

    public class MemberRole
    {
        public static Dictionary<AccountMemberRole, List<MemberPermission>> Roles = new Dictionary<AccountMemberRole, List<MemberPermission>>()
        {
            {AccountMemberRole.Owner, Enum.GetValues(typeof(MemberPermission)).Cast<MemberPermission>().ToList()},

            {AccountMemberRole.Admin, Enum.GetValues(typeof(MemberPermission)).Cast<MemberPermission>().ToList()},

            {AccountMemberRole.Approver, new List<MemberPermission>()
            {
                MemberPermission.ApprovePost,
                MemberPermission.CreatePost,

                MemberPermission.StatisticsInsign,
                MemberPermission.StatisticsUser,
                MemberPermission.StatisticsQuantity
            } },
            {AccountMemberRole.Editor, new List<MemberPermission>()
            {
                MemberPermission.ModifyPostOnPage,
                MemberPermission.RemovePostOnPage,
                MemberPermission.CreatePost,

                MemberPermission.StatisticsInsign,
                MemberPermission.StatisticsUser,
                MemberPermission.StatisticsQuantity
            } },
            {AccountMemberRole.Poster, new List<MemberPermission>()
            {
                MemberPermission.CreatePost,

                MemberPermission.StatisticsInsign,
                MemberPermission.StatisticsUser,
                MemberPermission.StatisticsQuantity
            } },
            {AccountMemberRole.CommentManager, new List<MemberPermission>()
            {
                MemberPermission.ManagerComment
            } },
            {AccountMemberRole.ManagerStatistics, new List<MemberPermission>()
            {
                MemberPermission.StatisticsInsign,
                MemberPermission.StatisticsUser,
                MemberPermission.StatisticsQuantity
            } }
        };
    }

    public enum MemberPermission
    {
        ManagerMember = 11, //quản lý member
        ConfigPage = 12, //cấu hình thông tin page

        ModifyPostOnPage = 21,// chỉnh sửa tất cả nội dung
        RemovePostOnPage = 22,// Gỡ tất cả bài đã xuất bản

        CreatePost = 31,// viết bài
        PublishPost = 32,// xuất bản tin của chính mình
        ApprovePost = 33, //Xuất bản hoặc trả lại (chuyển về lưu tạm) bài được nhờ kiểm duyệt

        ManagerComment = 41, // Quản lý comment

        StatisticsInsign = 51, // xem thống kê Insign(Pageviews ,Tương tác bài viết ,Page follow, Reach , Video Analytics, Action on Page, Token, Followers)
        StatisticsUser = 52, // xem thống kê  Độ tuổi/ Giới tính/ Vùng miền/ Thiết bị
        StatisticsQuantity = 53 // thống kê Sản lượng theo các loại hình nội dung, Bảng biểu, so sánh theo các mốc thời gian
    }
}
