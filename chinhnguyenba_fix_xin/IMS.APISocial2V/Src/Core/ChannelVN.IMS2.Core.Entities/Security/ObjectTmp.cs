﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ChannelVN.IMS2.Core.Entities.Security
{
    public class ObjectTmp
    {
        public Account Account { get; set; }
        public UserProfile Profile { get; set; }
        public List<Account> Pages { get; set; }
    }
}
