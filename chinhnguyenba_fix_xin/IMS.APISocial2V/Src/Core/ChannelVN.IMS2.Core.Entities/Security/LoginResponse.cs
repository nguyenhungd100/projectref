﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ChannelVN.IMS2.Core.Entities.Security
{
    public class LoginResponse
    {
        public int? Status { get; set; }
        public string Message { get; set; }
        public int? Code { get; set; }
        public DataResponse Data { get; set; }
    }

    public class DataResponse
    {
        public string User_id { get; set; }
        public string Username { get; set; }
        public string Full_name { get; set; }
        public string Avatar { get; set; }
        public int? Total_Follow { get; set; }
        public int? Total_Follower { get; set; }
        public string Address { get; set; }
        public string City { get; set; }
        public int? Type { get; set; }
        public string Role { get; set; }
        public string Street { get; set; }
        public string Birthday { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public string FbUrl { get; set; }
        //public string Fb_info { get; set; }
        public PermissionInfo Permission_Info { get; set; }
    }

    public class PermissionInfo
    {
        public double Role_Id { get; set; }
        public Permissions[] Permissions { get; set; }
    }
    public class Permissions
    {
        public double Update_Time { get; set; }
        public double Permission_Id { get; set; }
        public string Permission_Name { get; set; }
    }

    public class AccountRespone
    {
        public int? Status { get; set; }
        public string Message { get; set; }
        public int? Code { get; set; }
        public UserResponse Data { get; set; }
    }

    public class UpdateRoleRespone
    {
        public int? Status { get; set; }
        public string Message { get; set; }
        public int? Code { get; set; }
        public object Result { get; set; }
    }

    public class UserResponse
    {
        public string UserID { get; set; }
        public string Username { get; set; }
        public string Invite_link { get; set; }
    }
}
