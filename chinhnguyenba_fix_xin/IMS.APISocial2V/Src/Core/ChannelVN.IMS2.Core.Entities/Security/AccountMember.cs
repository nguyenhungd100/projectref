﻿using ChannelVN.IMS2.Foundation.Data;
using Nest;
using System;

namespace ChannelVN.IMS2.Core.Entities.Security
{
    public class AccountMember:Entity
    {
       
        [Ignore]
        public long OfficerId { get; set; }
        [Ignore]
        public string EncryptOfficerId { get { return OfficerId.ToString(); } }
        public long MemberId { get; set; }
        [Ignore]
        public string EncryptMemberId { get { return MemberId.ToString(); } }
        public AccountMemberRole? Role { get; set; }
        [Ignore]
        public string RoleName { get { return Role!=null ? Enum.GetName(Role.GetType(), Role).ToString() : ""; } }
        private DateTime? _JoinedDate { get; set; }
        public DateTime? JoinedDate
        {
            get
            {
                return _JoinedDate;
            }
            set
            {
                _JoinedDate = value != null ? DateTime.SpecifyKind(value.Value, DateTimeKind.Unspecified) : value;
            }
        }
        public MemberPermission[] Permissions { get; set; }
        private DateTime? _LockedDate { get; set; }
        public DateTime? LockedDate
        {
            get
            {
                return _LockedDate;
            }
            set
            {
                _LockedDate = value != null ? DateTime.SpecifyKind(value.Value, DateTimeKind.Unspecified) : value;
            }
        }
        public bool IsLocked {
            get
            {
                return LockedDate.HasValue;
            }
        }

        public int Level
        {
            get
            {
                switch (Role)
                {
                    case AccountMemberRole.Owner:
                        return 1;
                    case AccountMemberRole.Admin:
                        return 2;
                    default:
                        return 3;
                }
            }
        }
    }
    public class AccountMemberInfo : AccountMember
    {
        public string FullName { get; set; }
        public string Email { get; set; }
        public string Mobile { get; set; }
        public string Avatar { get; set; }
        public long? DelegatorId { get; set; }
    }
}
