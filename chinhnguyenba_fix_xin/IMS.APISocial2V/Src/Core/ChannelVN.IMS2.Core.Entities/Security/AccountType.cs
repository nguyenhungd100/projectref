﻿namespace ChannelVN.IMS2.Core.Entities.Security
{
    public enum AccountType: byte
    {
        ReadOnly = 0,
        Personal = 1,
        Official = 2
    }
}
