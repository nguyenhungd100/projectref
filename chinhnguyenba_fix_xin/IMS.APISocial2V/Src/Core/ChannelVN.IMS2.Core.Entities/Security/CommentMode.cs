﻿namespace ChannelVN.IMS2.Core.Entities.Security
{
    public enum CommentMode
    {
        Disable = 1,
        PreCheck = 2,
        PostCheck = 3
    }
}
