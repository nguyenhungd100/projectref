﻿
namespace ChannelVN.IMS2.Core.Entities.Security
{
    public class QueueData<T1,T2,T3,T4>
    {
        public T1 Data1 { get; set; }
        public T2 Data2 { get; set; }
        public T3 Data3 { get; set; }
        public T4 Data4 { get; set; }        
    }    
}
