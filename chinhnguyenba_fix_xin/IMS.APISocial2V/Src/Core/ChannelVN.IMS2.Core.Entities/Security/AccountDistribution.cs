﻿using ChannelVN.IMS2.Foundation.Data;
using Nest;
using System;
using System.Collections.Generic;
using System.Text;

namespace ChannelVN.IMS2.Core.Entities.Security
{
    public class AccountDistribution:Entity
    {
        [Ignore]
        public long AccountId { get; set; }
        public long DistributorId { get; set; }
        [Ignore]
        public DateTime? RegisteredDate { get; set; }
        [Ignore]
        public Byte? IsPrimary { get; set; }
        [Ignore]
        public int? Quota { get; set; }
        [Ignore]
        public int? Quantity { get; set; }
        [Ignore]
        public DateTime? ModifiedDate { get; set; }
        [Ignore]
        public string ModifiedBy { get; set; }
    }
}
