﻿using ChannelVN.IMS2.Foundation.Data;
using Nest;
using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;

namespace ChannelVN.IMS2.Core.Entities.Security
{
    public class UserProfile:Entity
    {
        public long Id { get; set; }
        public string EncryptId { get { return Id.ToString(); } }
        public string Slogan { get; set; }
        public string ProfileUrl { get; set; }
        public string PenName { get; set; }
        public string City { get; set; }
        public string Address { get; set; }
        public Byte? Gender { get; set; }
        public DateTime? BirthDay { get; set; }
        public string IdNumber { get; set; }
        public DateTime? IssuedDate { get; set; }
        public string IssuedPlace { get; set; }
        public string Title { get; set; }
        public string WorkPlace { get; set; }
        public string School { get; set; }
        public string UrlNetwork { get; set; }
        public string UrlSocial { get; set; }
        public string Scan1 { get; set; }
        public string Scan2 { get; set; }
        public string Scan3 { get; set; }
        public string ReprName { get; set; }
        public string ReprJob { get; set; }
        public string ReprEmail { get; set; }
        public string ReprPhone { get; set; }
        public string ModifiedBy { get; set; }
        public DateTime? ModifiedDate { get; set; }
    }
}
