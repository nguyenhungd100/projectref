﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ChannelVN.IMS2.Core.Entities.Security
{
    public class SearchAccountData
    {
        public string User_id { get; set; }
        public string Username { get; set; }
        public string Full_name { get; set; }
        public string Avatar { get; set; }
        public string Address { get; set; }
        public string Birthday { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
    }
}
