﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ChannelVN.IMS2.Core.Entities.Newsletters
{
    public enum ArticleTemplateId
    {
        SizeS = 1,
        SizeM = 2,
        SizeL = 3,
        Magazine = 4,
        MiniMagazine = 5,
        PhotoStory = 6
    }

}
