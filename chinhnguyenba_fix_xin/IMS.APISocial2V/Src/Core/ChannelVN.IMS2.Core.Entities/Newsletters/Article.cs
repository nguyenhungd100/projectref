﻿using Nest;
using System;
using System.Collections.Generic;
using System.Text;

namespace ChannelVN.IMS2.Core.Entities.Newsletters
{
    public interface IArticle
    {
        string Title { get; set; }
        string Sapo { get; set; }
        string SubTitle { get; set; }
        string Avatar1 { get; set; }
        string Avatar2 { get; set; }
        string Avatar3 { get; set; }
        long? ParentId { get; set; }
        int? TemplateId { get; set; }
        string Body { get; set; }
        string BodyMeta { get; set; }
       
        bool IsInstantView { get; set; }
        string InstantViewUrl { get; set; }
        string NativeContent { get; set; }
    }
    public class ArticleSearch : NewsSearch, IArticle
    {
        public string Title { get; set; }
        [Ignore]
        public string Sapo { get; set; }
        [Ignore]
        public string SubTitle { get; set; }
        [Ignore]
        public string Avatar1 { get; set; }
        [Ignore]
        public string Avatar2 { get; set; }
        [Ignore]
        public string Avatar3 { get; set; }
        [Ignore]
        public long? ParentId { get; set; }
        [Ignore]
        public int? TemplateId { get; set; }
        [Ignore]
        public string Body { get; set; }
        [Ignore]
        public string BodyMeta { get; set; }
        [Ignore]
        public long? InstantViewId { get; set; }
        [Ignore]
        public string EncryptInstantViewId { get
            {
                return InstantViewId?.ToString();
            }
        }
        [Ignore]
        public bool IsInstantView { get; set; }
        [Ignore]
        public string InstantViewUrl { get; set; }
        [Ignore]
        public string NativeContent { get; set; }

    }
    public class Article : News, IArticle
    {
        /// <summary>
        /// Tiêu đề bài viết
        /// </summary>
        public string Title { get; set; }
        public string Sapo { get; set; }
        public string SubTitle { get; set; }
        public string Avatar1 { get; set; }
        public string Avatar2 { get; set; }
        public string Avatar3 { get; set; }
        public long? ParentId { get; set; }
        public int? TemplateId { get; set; }
        public string Body { get; set; }
        public string BodyMeta { get; set; }
        public long? InstantViewId { get; set; }
        public string EncryptInstantViewId
        {
            get
            {
                return InstantViewId?.ToString();
            }
        }
        public bool IsInstantView { get; set; }
        public string InstantViewUrl { get; set; }
        public string NativeContent { get; set; }
    }
    public class ArticleOnWall : NewsOnWall
    {
        public string Title { get; set; }
        public string Sapo { get; set; }
        public string SubTitle { get; set; }
        public string Avatar1 { get; set; }
        public string Avatar2 { get; set; }
        public string Avatar3 { get; set; }
        public long? ParentId { get; set; }
        public int? TemplateId { get; set; }
    }
    public class ArticleSearchReturn : NewsSearchReturn
    {
        public string Title { get; set; }
        public string SubTitle { get; set; }
        public string Avatar1 { get; set; }
        public string Avatar2 { get; set; }
        public string Avatar3 { get; set; }
    }
}
