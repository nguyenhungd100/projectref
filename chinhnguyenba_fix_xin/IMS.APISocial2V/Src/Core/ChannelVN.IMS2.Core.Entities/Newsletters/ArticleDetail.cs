﻿using ChannelVN.IMS2.Foundation.Data;
using System;
using System.Collections.Generic;
using System.Text;

namespace ChannelVN.IMS2.Core.Entities.Newsletters
{
    public class ArticleDetail : Entity
    {
        public ArticleSearch News { get; set; }
        public List<Tag> NewsInTag { get; set; }
        public string AuthorAvatar { get; set; }

    }
}
