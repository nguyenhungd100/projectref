﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ChannelVN.IMS2.Core.Entities.Newsletters
{
    public enum NewsDistributionPublishStatus
    {
        None = 0,
        Accept = 1,
        Reject = 2
    }
}
