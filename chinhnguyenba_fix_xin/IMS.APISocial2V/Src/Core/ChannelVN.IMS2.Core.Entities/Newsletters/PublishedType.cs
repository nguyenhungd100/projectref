﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ChannelVN.IMS2.Core.Entities.Newsletters
{
    public enum PublishedType
    {
        Post = 1,
        Remind = 2
    }
}
