﻿using ChannelVN.IMS2.Core.Entities.Photo;
using ChannelVN.IMS2.Core.Entities.Security;
using ChannelVN.IMS2.Foundation.Data;
using System;
using System.Collections.Generic;

namespace ChannelVN.IMS2.Core.Entities.Distribution
{
    public class PayloadDataShare : Entity
    {        
        public long DistributionId { get; set; }
        public int TemplateId { get; set; }//cardtype
        public string Title { get; set; }//tổng hợp các tiêu đề cho stream search
        public NewsType Type { get; set; }
        public List<MetaDataShared> MetaDataShared { get; set; }
    }

    public class MetaDataShared
    {
        //object Id (news, video, media)
        public long ObjectId { get; set; }//mediaid
        public string Title { get; set; }//title
        public string Avatar { get; set; }
        public int? ZoneId { get; set; }
        public string ZoneName { get; set; }
        public string ChannelId { get; set; }
        private DateTime? _DistributionDate { get; set; }
        public DateTime? DistributionDate {
            get
            {
                return _DistributionDate;
            }
            set
            {
                _DistributionDate = value != null ? DateTime.SpecifyKind(value.Value, DateTimeKind.Unspecified) : value;
            }
        }

        //video cần
        public string Name { get; set; }        
        public string Description { get; set; }        
        public string ZoneRelation { get; set; }//list zone: zone1;zone2
        public string PlaylistRelation { get; set; }//list playlist: playlist1;playlist2        
        
        //news cần
        public string ObjectType { get; set; }
        public int Type { get; set; }
        public string DisplayStyle { get; set; }                
        public string AuthorAvatar { get; set; }//avatar
        public string AuthorName { get; set; }//fullname
        public string AuthorId { get; set; }        
        public string Url { get; set; }

        //mediaunit        
        public int CardType { get; set; }//type của card
        public int Frameid { get; set; }//cách hiểu thị trong khung 1 media
        public string Tags { get; set; }//tags
        public ShopInfo ShopInfo { get; set; }//shopInfo 
        public long UserId { get; set; }//userid
        public string UserName { get; set; }//username
        public string FullName { get; set; }//fullname
        public int IsDeleted { get; set; }//"isdeleted": mặc định là 0 - không xóa
        public List<ItemMediaUnit> ItemMediaUnit { get; set; }//des{}

        //playlist can
        public Cover Cover { get; set; } // ảnh cover
         //add ro Board
        public long[] BoardIds { get; set; }
        public Caption[] CaptionExt { get; set; }
        public CommentMode? CommentMode { get; set; }
        public LabelType? LabelType { get; set; }
    }

    public class ItemMediaUnit
    {
        ////desc =>{}
        public string Id { get; set; }
        public string Title { get; set; }
        //public string Caption { get; set; }
        public string Link { get; set; }

        public string Link_instantview { get; set; }
        //photo,Video,PlaylistVideo,VideoAndImage,PlaylistAudio
        public string Thumb { get; set; }
        //Video,PlaylistVideo,VideoAndImage,VideoRelation 
        public string TypeMedia { get; set; }//"type": "video/mp4"
        //Video,PlaylistVideo,VideoAndImage,PlaylistAudio,VideoRelation 
        public string Duration { get; set; }//"duration": "00:15"
        //Photo,Video,PlaylistVideo,VideoAndImage,VideoRelation 
        public int Height { get; set; }//513
        //Photo,Video,PlaylistVideo,VideoAndImage,VideoRelation 
        public int Width { get; set; }//655
        //Photo,Video,PlaylistVideo,VideoAndImage,Football,PlaylistAudio,ListNews,VideoRelation 

        //Video=1,
        //Photo=2,
        //News=3,
        //Autdio=4,
        //Sport=5,//data bong đá sport 5
        //Trending=6,//data trending
        //Image=7//ảnh gì

        public ContentType? ContentType { get; set; }//content_type 1
        //VideoAndImage,
        public int Position { get; set; }//"postion": 1 
        //VideoAndImage,VideoRelation 
        public int IsPlay { get; set; }//"is_play": 1     
        //TrendingNews,ShareNews 
        public string Source { get; set; }        //sharelink
        //TrendingNews,ListNews 
        public DataType? DataType { get; set; }//0: view app, 1: native app (mới dùng cho chùm tin)
        //dau App chua nhan
        public string Body { get; set; }// Nội dung  bài viết (mới dùng cho chùm tin)
        //ListNews,ShareNews 
        public Image Image { get; set; }
        public DateTime? CreatedDate { get; set; }// Dùng cho item in playlist, album, chùm tin
        ////TrendingNews 
        //public long SourceId { get; set; }
        //public string SourceName { get; set; }
        //public string PublishedDate { get; set; }
        public string Description { get; set; }
        public bool? IsHeartDrop { get; set; }
        public string original_url { get; set; }
    }
    public class Image
    {
        public string Thumb { get; set; }
        public int Width { get; set; }
        public int Height { get; set; }
        //ShareNews 
        public ContentType? ContentType { get; set; }
    }

    public class Cover
    {
        public string Src { get; set; }
        public string Url { get; set; }
        public string Thumb { get; set; }
        public int Width { get; set; }
        public int Height { get; set; }
        public ContentType? ContentType { get; set; }
    }

    public class ShopInfo
    {
        public string Phone { get; set; }
        public string Email { get; set; }
    }
    public class QueueShareData
    {
        public string SessionId { get; set; }
        public PayloadDataShare PlayloadDataShare { get; set; }        
        public Distributor Distributor { get; set; }
    }

    #region PublishData
    public class ItemPublishData
    {
        public string Id { get; set; }
        public string Title { get; set; }
        public ContentType? ContentType { get; set; }
        public ContentType? Content_Type { get; set; }// for old data
        public string Link { get; set; }
        public string Link_instantview { get; set; }
        public string Thumb { get; set; }
        public int Width { get; set; }
        public int Height { get; set; }
        public string Duration { get; set; }
        public string Type { get; set; }
        public int Position { get; set; }
        public int IsPlay { get; set; }
        public Image Image { get; set; }
        public DataType? DataType { get; set; }
        public string Source { get; set; }
        public string Body { get; set; }
        public string SourceIframe { get; set; }
        public string Description { get; set; }
        public bool? IsHeartDrop { get; set; }
        public string original_url { get; set; }
    }

    public class Caption
    {
        public string text { get; set; }
        public string type { get; set; }
        public string link { get; set; }
        public string userID { get; set; }
    }
    public class PublishData
    {
        public string Caption { get; set; }
        public string Title { get; set; }
        public List<ItemPublishData> Items { get; set; }     
        public string Link { get; set; }
        public Caption[] CaptionExt { get; set; }
        public int? TemplateId { get; set; }
        public string CaptionMetaData { get; set; }
    }
    
    #endregion

    #region obj queue share
    public class QueueShareDataUnit<T>
    {
        public List<T> ListMedia { get; set; }
        public ItemStreamDistribution ItemStreamDistribution { get; set; }        
    }
    #endregion
}
