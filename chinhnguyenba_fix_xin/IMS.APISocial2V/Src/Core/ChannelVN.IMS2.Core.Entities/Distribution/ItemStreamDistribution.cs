﻿using ChannelVN.IMS2.Foundation.Data;
using Nest;
using System;

namespace ChannelVN.IMS2.Core.Entities.Distribution
{
    public class ItemStreamDistribution: Entity 
    {
        public long Id { get; set; }
        [Ignore]
        public string EncryptId { get { return Id.ToString(); } }
        public int? PublishStatus { get; set; }
        [Ignore]
        private DateTime? _CreatedDate { get; set; }
        public DateTime? CreatedDate {
            get
            {
                return _CreatedDate;
            }
            set
            {
                _CreatedDate = value != null ? DateTime.SpecifyKind(value.Value, DateTimeKind.Unspecified) : value;
            }
        }        
        public int? ItemStreamTempId { get; set; }
        [Ignore]
        private DateTime? _AcceptedDate { get; set; }
        [Ignore]
        public DateTime? AcceptedDate {
            get
            {
                return _AcceptedDate;
            }
            set
            {
                _AcceptedDate = value != null ? DateTime.SpecifyKind(value.Value, DateTimeKind.Unspecified) : value;
            }
        }
        [Ignore]
        public string Note { get; set; }
        [Ignore]
        private DateTime? _ModifiedDate { get; set; }
        [Ignore]
        public DateTime? ModifiedDate {
            get
            {
                return _ModifiedDate;
            }
            set
            {
                _ModifiedDate = value != null ? DateTime.SpecifyKind(value.Value, DateTimeKind.Unspecified) : value;
            }
        }        
        public string Title { get; set; }
        [Ignore]
        public long[] DistributorId { get; set; }
        public string MetaData { get; set; }
        public NewsType Type { get; set; }
    }
}
