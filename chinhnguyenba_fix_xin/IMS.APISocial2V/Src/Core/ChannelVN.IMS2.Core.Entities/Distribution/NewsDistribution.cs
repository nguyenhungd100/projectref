﻿using ChannelVN.IMS2.Foundation.Data;
using Nest;
using System;
using System.Collections.Generic;
using System.Text;

namespace ChannelVN.IMS2.Core.Entities.Distribution
{
    public class NewsDistribution: Entity
    {
        public long DistributorId { get; set; }
        [Ignore]
        public string EncryptDistributorId { get { return DistributorId.ToString(); } }
        [Ignore]
        public string DistributorName { get; set; }
        [Ignore]
        public long NewsId { get; set; }
        [Ignore]
        public string EncryptNewsId { get { return NewsId.ToString(); } }
        public long ItemStreamId { get; set; }
        [Ignore]
        public string EncryptItemStreamId { get { return ItemStreamId.ToString(); } }
        [Ignore]
        private DateTime? _SharedDate { get; set; }
        public DateTime? SharedDate {
            get
            {
                return _SharedDate;
            }
            set
            {
                _SharedDate = value != null ? DateTime.SpecifyKind(value.Value, DateTimeKind.Unspecified) : value;
            }
        }
        public string SharedZone { get; set; }
        public override string ToString() => DistributorId + "," + NewsId + "," + ItemStreamId + "," + SharedDate?.ToString("yyyy-MM-dd HH:mm:ss.fff")+","+ SharedZone;
    }
}
