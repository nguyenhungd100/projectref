﻿namespace ChannelVN.IMS2.Core.Entities.Distribution
{
    public enum DistributionPublishStatus
    {
        Send = 1,
        Accept = 2,
        Reject = 3
    }
}
