﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ChannelVN.IMS2.Core.Entities.Distribution
{
    public enum ItemStreamDistributionType
    {
        News = 1,
        Video = 2,
        VideoPlayList = 3
    }
}
