﻿using ChannelVN.IMS2.Foundation.Data;
using System;
using System.Collections.Generic;
using System.Text;

namespace ChannelVN.IMS2.Core.Entities.Distribution
{
    public class DistributorConfiguration:Entity
    {
        public int? QuotaNormal { get; set; }
        public int? QuotaProfessional { get; set; }
        public string UnitTime { get; set; }
        public int? UnitAmount { get; set; }
        public string ApiZoneVideo { get; set; }
        public string ApiVideo { get; set; }
        public string ApiVideoPlaylist { get; set; }
        public string ApiMediaUnit { get; set; }
    }
}
