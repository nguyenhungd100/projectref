﻿using ChannelVN.IMS2.Foundation.Data;
using System;
using System.Collections.Generic;
using System.Text;

namespace ChannelVN.IMS2.Core.Entities.Distribution
{
    public class Distributor: Entity
    {
        public long Id { get; set; }
        public string EncryptId { get { return Id.ToString(); } }
        public string Name { get; set; }
        public string Description { get; set; }
        public int? Priority { get; set; }
        public int? Status { get; set; }
        public string CreatedBy { get; set; }
        private DateTime? _CreatedDate { get; set; }
        public DateTime? CreatedDate {
            get
            {
                return _CreatedDate;
            }
            set
            {
                _CreatedDate = value != null ? DateTime.SpecifyKind(value.Value, DateTimeKind.Unspecified) : value;
            }
        }
        public string Code { get; set; }
        public DistributorConfiguration Config { get; set; }
    }

    public class DistributionSimple
    {
        public long Id { get; set; }
        public string EncryptId { get { return Id.ToString(); } }
        public string Name { get; set; }
        public string Type { get; set; }
        public string Description { get; set; }
        public int? Status { get; set; }
        public int? Priority { get; set; }
        public int? ConfigQuotaNormal { get; set; }
        public int? ConfigQuotaProfessional { get; set; }
        public string ConfigUnitTime { get; set; }
        public int? ConfigUnitAmount { get; set; }
        public string ConfigCode { get; set; }
    }

    public enum DistribusionStatus
    {
        Accept = 1,
        Removed = 0
    }

    public class ResultData
    {
        public bool Success { get; set; }
        public int ErrorCode { get; set; }
        public string Message { get; set; }
        public List<ZoneVideo> Data { get; set; }
    }

    public class ResultVideoPlayListData
    {
        public bool Success { get; set; }
        public int ErrorCode { get; set; }
        public string Message { get; set; }
        public ResultVideoPlayListPagingData Data { get; set; }
    }

    public class ResultVideoPlayListPagingData
    {
        public int TotalRow { get; set; }
        public List<PlaylistEntity> PlayLists { get; set; }
    }

    public class PlaylistEntity : Entity
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string Url { get; set; }
        public string Avatar { get; set; }
    }

    public class ZoneVideo : Entity
    {
        public int Id { get; set; }
        public int ParentId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string Url { get; set; }
        public string Avatar { get; set; }
    }

    public class ConfigZoneVideo : Entity
    {
        public string ApiUrl { get; set; }
        public string ApiClient { get; set; }
        public string ApiKey { get; set; }
    }

    public class ConfigVideoPlaylist : Entity
    {
        public string ApiUrl { get; set; }
        public string ApiClient { get; set; }
        public string ApiKey { get; set; }
    }

    public enum DistributionType
    {
        Pegax = 1,
        KingHub=2
    }

    public class VideoShare
    {
        // Video Info
        public int Id { get; set; }
        public int ZoneId { get; set; }
        public string Name { get; set; }
        public string UnsignName { get; set; }
        public string Description { get; set; }    
        public string HtmlCode { get; set; }
        public string Avatar { get; set; }
        public string KeyVideo { get; set; }
        public string Pname { get; set; }
        public int Status { get; set; }    
        public long NewsId { get; set; }
        public int Views { get; set; }
        public int Mode { get; set; }
        public string Tags { get; set; }
        private DateTime _DistributionDate { get; set; }
        public DateTime DistributionDate {
            get
            {
                return _DistributionDate;
            }
            set
            {
                _DistributionDate = DateTime.SpecifyKind(value, DateTimeKind.Unspecified);
            }
        }
        public string CreatedBy { get; set; }
        private DateTime _CreatedDate { get; set; }
        public DateTime CreatedDate {
            get
            {
                return _CreatedDate;
            }
            set
            {
                _CreatedDate = DateTime.SpecifyKind(value, DateTimeKind.Unspecified);
            }
        }
        public string LastModifiedBy { get; set; }
        private DateTime _LastModifiedDate { get; set; }
        public DateTime LastModifiedDate {
            get
            {
                return _LastModifiedDate;
            }
            set
            {
                _LastModifiedDate = DateTime.SpecifyKind(value, DateTimeKind.Unspecified);
            }
        }
        public string PublishBy { get; set; }
        private DateTime _PublishDate { get; set; }
        public DateTime PublishDate {
            get
            {
                return _PublishDate;
            }
            set
            {
                _PublishDate = DateTime.SpecifyKind(value, DateTimeKind.Unspecified);
            }
        }
        public string EditedBy { get; set; }
        private DateTime _EditedDate { get; set; }
        public DateTime EditedDate {
            get
            {
                return _EditedDate;
            }
            set
            {
                _EditedDate = DateTime.SpecifyKind(value, DateTimeKind.Unspecified);
            }
        }
        public string Url { get; set; }    
        public string VideoRelation { get; set; }
        // VideoFileInfo
        public string FileName { get; set; }
        public string Duration { get; set; }
        public string Size { get; set; }
        public int Capacity { get; set; }
        // For advertisement
        public bool AllowAd { get; set; }
        public bool IsRemoveLogo { get; set; }
        public string OriginalUrl { get; set; }    
        public int Type { get; set; }
        public bool IsConverted { get; set; }
        public string AvatarShareFacebook { get; set; }
        public int OriginalId { get; set; }    
        public string Author { get; set; }    
        public bool IsNewProcessed { get; set; }    
        public int ParentId { get; set; }
        public string HashId { get; set; }    
        public string TrailerUrl { get; set; }
        public string MetaAvatar { get; set; }
        public int Priority { get; set; }
        public string Location { get; set; }
        public string PolicyContentId { get; set; }

        //ext
        public int VideoFolderId { get; set; }
        private DateTime _PlayOnTime { get; set; }
        public DateTime PlayOnTime {
            get
            {
                return _PlayOnTime;
            }
            set
            {
                _PlayOnTime = DateTime.SpecifyKind(value, DateTimeKind.Unspecified);
            }
        }    
        public string ZoneName { get; set; }
        public string Source { get; set; }        
        public string GroupName { get; set; }
        public string DisplayStyle { get; set; }
        public string Namespace { get; set; }
    }

    public class PayLoadShareVideo
    {
        public VideoShare Video { get; set; }
        public string TagIdList { get; set; }
        public string PlaylistIdList { get; set; }
        public string ZoneIdList { get; set; }
    }

    public class PayLoadShareVideoEntity
    {
        public List<PayLoadShareVideo> Data { get; set; }
    }
}
