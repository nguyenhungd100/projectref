﻿using ChannelVN.IMS2.Foundation.Data;
using Nest;
using System;
using System.Collections.Generic;
using System.Text;

namespace ChannelVN.IMS2.Core.Entities
{
    public class Tag : Entity
    {
        public long Id { get; set; }
        [Ignore]
        public string EncryptId { get { return Id.ToString(); } }
        public string Name { get; set; }
        [Ignore]
        public string UnsignName { get; set; }
        [Ignore]
        public bool? IsHotTag { get; set; }
        [Ignore]
        public string Url { get; set; }
        [Ignore]
        public string Description { get; set; }
        [Ignore]
        public long? Priority { get; set; }
        [Ignore]
        public int? Status { get; set; }
        public long? ParentId { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }

    }
    public class TagSearch : Tag
    {

    }
}
