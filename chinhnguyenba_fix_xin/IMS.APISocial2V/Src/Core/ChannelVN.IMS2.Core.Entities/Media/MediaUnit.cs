﻿using Nest;
using System;
using System.Collections.Generic;
using System.Text;

namespace ChannelVN.IMS2.Core.Entities.Media
{
    public interface IMediaUnitSearch
    {
         int TemplateId { get; set; }
         string Caption { get; set; }
         bool? AllowSale { get; set; }
         string CallPhone { get; set; }
         string Email { get; set; }
    }
    public class MediaUnit: News
    {
        public int TemplateId { get; set; }
        public string Caption { get; set; }  
        public bool? AllowSale { get; set; }
        public string CallPhone { get; set; }
        public string Email { get; set; }
    }

    public class MediaUnitSearch : NewsSearch
    {
        [Ignore]
        public int TemplateId { get; set; }
        public string Caption { get; set; }
        public bool? AllowSale { get; set; }
        [Ignore]
        public string CallPhone { get; set; }
        [Ignore]
        public string Email { get; set; }
    }

    public class MediaUnitReturn : NewsSearchReturn
    {
        public string Caption { get; set; }
        public string PublishData { get; set; }
    }

    public enum CardType
    {
        Photo = 1,//(chỉ có 1 ảnh)
        Video = 2,
        TrendingNews = 3,
        PlaylistVideo = 4,
        Seribook = 5,
        VideoAndImage = 6,//video ngang và ảnh + video doc và anh (album anh hoăc album ảnh + video)
        Football = 7,//kêt quả bong da
        PlaylistAudio = 8,//playlist audio 
        TextInPhoto = 9,//text trong photo
        ListNews = 10,//list tin tức
        Text = 11,//text
        ReportShare = 12,//report share
        PostShare = 13,//post share
        VideoRelation = 14,//bài video + video liên quan
        ShareNews = 15,//bài viêt share
        Ads = 16,//quảng cáo
        //custom
        Album = 17, //album ảnh
        Article = 18, //Bài viết đơn,
        Gallery1 = 22,
        Gallery2 = 23,
    }

    //public enum ContentType
    //{
    //    Video=1,
    //    Photo=2,
    //    News=3,
    //    Autdio=4,
    //    Sport=5,//data bong đá sport 5
    //    Trending=6,//data trending
    //    Image=7//ảnh gì
    //}
}
