﻿using Nest;
using System;

namespace ChannelVN.IMS2.Core.Entities.Media
{
    public class Template
    {
        public int Id { get; set; }
        public string Title { get; set; }
        [Ignore]
        public string Avatar { get; set; }
        [Ignore]
        public string CreatedBy { get; set; }
        [Ignore]
        private DateTime? _CreatedDate { get; set; }
        public DateTime? CreatedDate {
            get
            {
                return _CreatedDate;
            }
            set
            {
                _CreatedDate = value != null ? DateTime.SpecifyKind(value.Value, DateTimeKind.Unspecified) : value;
            }
        }
        [Ignore]
        public string ModifiedBy { get; set; }
        [Ignore]
        private DateTime? _ModifiedDate { get; set; }
        [Ignore]
        public DateTime? ModifiedDate {
            get
            {
                return _ModifiedDate;
            }
            set
            {
                _ModifiedDate = value != null ? DateTime.SpecifyKind(value.Value, DateTimeKind.Unspecified) : value;
            }
        }
        public int Status { get; set; }
        [Ignore]
        public string Description { get; set; }
        [Ignore]
        public int Priority { get; set; }
        public int CategoryId { get; set; }
        [Ignore]
        public string MetaData { get; set; }
    }

    public enum TemplateStatus
    {
        All=-1,
        UnActive=0,
        Active=1
    }
    public enum TemplateId
    {
        Album2 = 300,
        Album3 = 301,
        Album4 = 302
    }
    public enum TemplateCate
    {
        MediaUnit = 1,
        Album = 2
    }
    public class ResultData
    {
        public bool ReturnValue { get; set; }
        public string Data { get; set; }
    }
}
