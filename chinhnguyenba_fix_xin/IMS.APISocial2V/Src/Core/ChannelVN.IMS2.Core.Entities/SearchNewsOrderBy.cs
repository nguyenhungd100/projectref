﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ChannelVN.IMS2.Core.Entities
{
    public enum  SearchNewsOrderBy
    {
        DescDistributionDate = 0,
        AscDistributionDate = 1,
        DescCreatedDate = 2,
        AscCreatedDate = 3,
        DescModifiedDate = 4,
        AscModifiedDate = 5
    }
}
