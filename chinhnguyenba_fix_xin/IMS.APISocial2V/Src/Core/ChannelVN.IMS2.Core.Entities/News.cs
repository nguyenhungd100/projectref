﻿using ChannelVN.IMS2.Core.Entities.Distribution;
using ChannelVN.IMS2.Core.Entities.Security;
using ChannelVN.IMS2.Foundation.Data;
using Nest;
using System;

namespace ChannelVN.IMS2.Core.Entities
{
    public interface INews
    {
        long Id { get; set; }
        string EncryptId { get; }

        int? Type { get; set; }

        string Url { get; set; }

        string OriginalUrl { get; set; }
        int? Status { get; set; }

        string Author { get; set; }
        long? CategoryId { get; set; }

        DateTime? CreatedDate { get; set; }
        string CreatedBy { get; set; }

        string Location { get; set; }

        int? PublishMode { get; set; }

        string PublishData { get; set; }

        DateTime? DistributionDate { get; set; }

        DateTime? ModifiedDate { get; set; }

        string ModifiedBy { get; set; }
        long? DistributorId { get; set; }

        string Tags { get; set; }
        int? CardType { get; set; }

        DateTime? ApprovedDate { get; set; }
        string ApprovedBy { get; set; }
        CommentMode? CommentMode { get; set; }
        LabelType? LabelType { get; set; } 
    }
    public abstract class News : Entity, INews //model cho Redis
    {
        public long Id { get; set; }
        [Ignore]
        public string EncryptId { get { return Id.ToString(); } }
        [Ignore]
        public int? Type { get; set; }
        [Ignore]
        public string Url { get; set; }
        [Ignore]
        public string OriginalUrl { get; set; }
        public int? Status { get; set; }
        [Ignore]
        public string Author { get; set; }
        public long? CategoryId { get; set; }
        [Ignore]
        private DateTime? _CreatedDate { get; set; }
        public DateTime? CreatedDate
        {
            get
            {
                return _CreatedDate;
            }
            set
            {
                _CreatedDate = value != null ? DateTime.SpecifyKind(value.Value, DateTimeKind.Unspecified) : value;
            }
        }
        public string CreatedBy { get; set; }
        [Ignore]
        public string Location { get; set; }
        [Ignore]
        public int? PublishMode { get; set; }
        [Ignore]
        public string PublishData { get; set; }
        [Ignore]
        private DateTime? _DistributionDate { get; set; }
        public DateTime? DistributionDate
        {
            get
            {
                return _DistributionDate;
            }
            set
            {
                _DistributionDate = value != null ? DateTime.SpecifyKind(value.Value, DateTimeKind.Unspecified) : value;
            }
        }
        [Ignore]
        private DateTime? _ModifiedDate { get; set; }
        [Ignore]
        public DateTime? ModifiedDate
        {
            get
            {
                return _ModifiedDate;
            }
            set
            {
                _ModifiedDate = value != null ? DateTime.SpecifyKind(value.Value, DateTimeKind.Unspecified) : value;
            }
        }
        [Ignore]
        public string ModifiedBy { get; set; }
        public long? DistributorId { get; set; }
        [Ignore]
        public string Tags { get; set; }
        public int? CardType { get; set; }
        [Ignore]
        private DateTime? _ApprovedDate { get; set; }
        public DateTime? ApprovedDate
        {
            get
            {
                return _ApprovedDate;
            }
            set
            {
                _ApprovedDate = value != null ? DateTime.SpecifyKind(value.Value, DateTimeKind.Unspecified) : value;
            }
        }
        public string ApprovedBy { get; set; }
        public CommentMode? CommentMode { get; set; }
        public LabelType? LabelType { get; set; }
    }
    public abstract class NewsSearch : News //model cho ES
    {
        [Nested]
        public NewsSeoMeta[] NewsSeoMeta { get; set; }
        [Nested]
        public NewsInAccount[] NewsInAccount { get; set; }
        [Nested]
        public NewsDistribution[] NewsDistribution { get; set; }
    }
    public abstract class NewsSearchReturn //model trả về khi search
    {
        public long Id { get; set; }
        public string EncryptId { get { return Id.ToString(); } }
        public int? Status { get; set; }
        public string Url { get; set; }
        public string Author { get; set; }
        public long? CategoryId { get; set; }
        private DateTime? _CreatedDate { get; set; }
        public DateTime? CreatedDate
        {
            get
            {
                return _CreatedDate;
            }
            set
            {
                _CreatedDate = value != null ? DateTime.SpecifyKind(value.Value, DateTimeKind.Unspecified) : value;
            }
        }
        public string CreatedBy { get; set; }
        private DateTime? _ModifiedDate { get; set; }
        public DateTime? ModifiedDate
        {
            get
            {
                return _ModifiedDate;
            }
            set
            {
                _ModifiedDate = value != null ? DateTime.SpecifyKind(value.Value, DateTimeKind.Unspecified) : value;
            }
        }
        public string ModifiedBy { get; set; }
        public string Location { get; set; }
        public int? PublishMode { get; set; }
        private DateTime? _DistributionDate { get; set; }
        public DateTime? DistributionDate
        {
            get
            {
                return _DistributionDate;
            }
            set
            {
                _DistributionDate = value != null ? DateTime.SpecifyKind(value.Value, DateTimeKind.Unspecified) : value;
            }
        }
        public string Tags { get; set; }
        public NewsDistribution[] NewsDistribution { get; set; }
    }
    public abstract class NewsOnWall
    {
        public long Id { get; set; }

        public string EncryptId { get { return Id.ToString(); } }

        public int? Type { get; set; }

        public string Url { get; set; }

        public string OriginalUrl { get; set; }
        public string Author { get; set; }
        public long? CategoryId { get; set; }
        private DateTime? _CreatedDate { get; set; }
        public DateTime? CreatedDate
        {
            get
            {
                return _CreatedDate;
            }
            set
            {
                _CreatedDate = value != null ? DateTime.SpecifyKind(value.Value, DateTimeKind.Unspecified) : value;
            }
        }

        public string Location { get; set; }
        private DateTime? _DistributionDate { get; set; }
        public DateTime? DistributionDate
        {
            get
            {
                return _DistributionDate;
            }
            set
            {
                _DistributionDate = value != null ? DateTime.SpecifyKind(value.Value, DateTimeKind.Unspecified) : value;
            }
        }
        private DateTime? _ModifiedDate { get; set; }
        public DateTime? ModifiedDate
        {
            get
            {
                return _ModifiedDate;
            }
            set
            {
                _ModifiedDate = value != null ? DateTime.SpecifyKind(value.Value, DateTimeKind.Unspecified) : value;
            }
        }

        public string Tags { get; set; }

    }
    public class NewsAllSearch
    {
        public long Id { get; set; }
        public string EncryptId { get { return Id.ToString(); } }
        public int? Status { get; set; }
        public int? Type { get; set; }
        public string Keyword { get; set; }
        public int? CardType { get; set; }
        public string CreatedBy { get; set; }
        private DateTime? _DistributionDate { get; set; }
        public DateTime? DistributionDate
        {
            get
            {
                return _DistributionDate;
            }
            set
            {
                _DistributionDate = value != null ? DateTime.SpecifyKind(value.Value, DateTimeKind.Unspecified) : value;
            }
        }
        private DateTime? _CreatedDate { get; set; }
        public DateTime? CreatedDate
        {
            get
            {
                return _CreatedDate;
            }
            set
            {
                _CreatedDate = value != null ? DateTime.SpecifyKind(value.Value, DateTimeKind.Unspecified) : value;
            }
        }
        public long? DistributorId { get; set; }
        public string Avatar { get; set; }
        public string PublishData { get; set; }
        public string Cover { get; set; }
        public CommentMode? CommentMode { get; set; }
        [Nested]
        public NewsInAccount[] NewsInAccount { get; set; }
        [Nested]
        public NewsDistribution[] NewsDistribution { get; set; }
    }
    public class NewsAll//model cho redis
    {
        public long Id { get; set; }
        public string EncryptId { get { return Id.ToString(); } }
        public int? Status { get; set; }
        public int? Type { get; set; }
        public string Keyword { get; set; }
        public int? CardType { get; set; }
        public string CreatedBy { get; set; }
        private DateTime? _DistributionDate { get; set; }
        public DateTime? DistributionDate
        {
            get
            {
                return _DistributionDate;
            }
            set
            {
                _DistributionDate = value != null ? DateTime.SpecifyKind(value.Value, DateTimeKind.Unspecified) : value;
            }
        }
        private DateTime? _CreatedDate { get; set; }
        public DateTime? CreatedDate
        {
            get
            {
                return _CreatedDate;
            }
            set
            {
                _CreatedDate = value != null ? DateTime.SpecifyKind(value.Value, DateTimeKind.Unspecified) : value;
            }
        }
        public long? DistributorId { get; set; }
        public string Avatar { get; set; }
        public string PublishData { get; set; }
        public string Cover { get; set; }
        public CommentMode? CommentMode { get; set; }
    }
}
