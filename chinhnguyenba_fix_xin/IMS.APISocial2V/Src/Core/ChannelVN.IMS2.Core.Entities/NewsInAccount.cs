﻿using ChannelVN.IMS2.Foundation.Data;
using Nest;
using System;
using System.Collections.Generic;
using System.Text;

namespace ChannelVN.IMS2.Core.Entities
{
    public class NewsInAccount:Entity
    {
        public long AccountId { get; set; }
        [Ignore]
        public string EncryptAccountId { get { return AccountId.ToString(); } }
        [Ignore]
        public long NewsId { get; set; }
        [Ignore]
        public string EncryptNewsId { get { return NewsId.ToString(); } }
        [Ignore]
        private DateTime? _PublishedDate { get; set; }
        [Ignore]
        public DateTime? PublishedDate {
            get
            {
                return _PublishedDate;
            }
            set
            {
                _PublishedDate = value != null ? DateTime.SpecifyKind(value.Value, DateTimeKind.Unspecified) : value;
            }
        }
        public int? PublishedType { get; set; }
    }
}
