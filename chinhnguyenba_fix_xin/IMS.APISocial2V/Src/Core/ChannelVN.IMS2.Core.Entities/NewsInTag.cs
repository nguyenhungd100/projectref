﻿using ChannelVN.IMS2.Foundation.Data;
using System;
using System.Collections.Generic;
using System.Text;

namespace ChannelVN.IMS2.Core.Entities
{
    public class NewsInTag:Entity
    {
        public long NewsId { get; set; }
        public long TagId { get; set; }
        public int Priority { get; set; }
        public DateTime? TaggedDate { get; set; }
    }
}
