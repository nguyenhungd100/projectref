﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ChannelVN.IMS2.Core.Entities.Statistics
{
    public enum OrderEnum
    {
        likes = 1,
        token = 2,
        comments = 3,
        shares = 4,
        views = 5,
        send = 6,
        clicks = 7,
        reachs = 8
    }
}
