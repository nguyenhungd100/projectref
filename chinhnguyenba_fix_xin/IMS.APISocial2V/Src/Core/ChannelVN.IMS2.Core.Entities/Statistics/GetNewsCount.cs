﻿using System;
using System.ComponentModel.DataAnnotations;

namespace ChannelVN.IMS2.Core.Entities.Statistics
{
    public class GetNewsCount
    {
        [Required]
        public long? AccountId { get; set; }
        [Required]
        public DateTime? FromDate { get; set; }
        public DateTime? ToDate { get; set; }
        public NewsType2? NewsType { get; set; }
    }

    public class GetNewsCountOfUser
    {
        [Required]
        public long? OfficerId { get; set; }
        public long? AccountId { get; set; }
        [Required]
        public DateTime? FromDate { get; set; }
        public DateTime? ToDate { get; set; }
        public NewsType? NewsType { get; set; }
    }
}
