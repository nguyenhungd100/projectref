﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ChannelVN.IMS2.Core.Entities.Statistics
{
    public class NewsCount
    {
        public NewsType NewsType { get; set; } 
        public long Count { get; set; }
    }

    public class NewsCount2
    {
        public NewsType2 NewsType { get; set; }
        public long Count { get; set; }
    }
}
