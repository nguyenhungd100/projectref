﻿using ChannelVN.IMS2.Core.Entities.Security;
using System;
using System.Collections.Generic;
using System.Text;

namespace ChannelVN.IMS2.Core.Entities.Statistics
{
    public class SearchPage
    {
        public string Keyword { get; set; }
        public AccountClass? Class { get; set; }
        public AccountStatus? Status { get; set; }
        public AccountClass[] ListClass { get; set; }
        public int? Levels { get; set; }
        public int? PageIndex { get; set; }
        public int? PageSize { get; set; }
    }
}
