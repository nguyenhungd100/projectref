﻿
using Nest;
using System;

namespace ChannelVN.IMS2.Core.Entities.Album
{
    public interface IAlbumSearch
    {
         string Name { get; set; }
         string UnsignName { get; set; }
         string Description { get; set; }
         DateTime? LastInsertedDate { get; set; }
         int? TemplateId { get; set; }
         string AlbumRelation { get; set; }
         string MetaAvatar { get; set; }
         string MetaData { get; set; }
         PhotoInAlbum[] PhotoInAlbum { get; set; }
    }
    public class Album: News
    {        
        public string Name { get; set; }
        public string UnsignName { get; set; }
        public string Description { get; set; }        
        private DateTime? _LastInsertedDate { get; set; }
        public DateTime? LastInsertedDate
        {
            get
            {
                return _LastInsertedDate;
            }
            set
            {
                _LastInsertedDate = value != null ? DateTime.SpecifyKind(value.Value, DateTimeKind.Unspecified) : value;
            }
        }
        public int? TemplateId { get; set; }
        public string AlbumRelation { get; set; }
        public string MetaAvatar { get; set; }
        public string MetaData { get; set; }
    }

    public class AlbumSearch : NewsSearch
    {
        public string Name { get; set; }
        [Ignore]
        public string UnsignName { get; set; }
        [Ignore]
        public string Description { get; set; }
        [Ignore]
        private DateTime? _LastInsertedDate { get; set; }
        public DateTime? LastInsertedDate
        {
            get
            {
                return _LastInsertedDate;
            }
            set
            {
                _LastInsertedDate = value != null ? DateTime.SpecifyKind(value.Value, DateTimeKind.Unspecified) : value;
            }
        }
        [Ignore]
        public int? TemplateId { get; set; }
        [Ignore]
        public string AlbumRelation { get; set; }
        [Ignore]
        public string MetaAvatar { get; set; }
        [Ignore]
        public string MetaData { get; set; }
        [Ignore]
        public PhotoInAlbum[] PhotoInAlbum { get; set; }
    }

    public class AlbumSearchReturn : NewsSearchReturn
    {
        public string Name { get; set; }
        public string PublishData { get; set; }
        public PhotoInAlbum[] PhotoInAlbum { get; set; }
        public int? TemplateId { get; set; }
    }

    public class PhotoInAlbum
    {
        public long AlbumId { get; set; }
        public string EncryptAlbumId { get { return AlbumId.ToString(); } }

        public long PhotoId { get; set; }
        public string EncryptPhotoId { get { return PhotoId.ToString(); } }
        public int? Priority { get; set; }
        private DateTime? _PublishedDate { get; set; }
        public DateTime? PublishedDate
        {
            get
            {
                return _PublishedDate;
            }
            set
            {
                _PublishedDate = value != null ? DateTime.SpecifyKind(value.Value, DateTimeKind.Unspecified) : value;
            }
        }
    }
}
