﻿using ChannelVN.IMS2.Foundation.Data;
using Nest;
using System;
using System.Collections.Generic;
using System.Text;

namespace ChannelVN.IMS2.Core.Entities.Gallery
{
    public interface IGallerySearch
    {
        string Name { get; set; }
        string UnsignName { get; set; }
        string Description { get; set; }
        DateTime? LastInsertedDate { get; set; }
        string Cover { get; set; }
        int? TemplateId { get; set; }
        string AlbumRelation { get; set; }
        string MetaAvatar { get; set; }
        string MetaData { get; set; }
    }
    public class Gallery : News
    {
        public string Name { get; set; }
        public string UnsignName { get; set; }
        public string Description { get; set; }
        private DateTime? _LastInsertedDate { get; set; }
        public DateTime? LastInsertedDate
        {
            get
            {
                return _LastInsertedDate;
            }
            set
            {
                _LastInsertedDate = value != null ? DateTime.SpecifyKind(value.Value, DateTimeKind.Unspecified) : value;
            }
        }
        public string Cover { get; set; }
        public int? TemplateId { get; set; }
        public string AlbumRelation { get; set; }
        public string MetaAvatar { get; set; }
        public string MetaData { get; set; }
    }

    public class GallerySearch : NewsSearch
    {
        public string Name { get; set; }
        [Ignore]
        public string UnsignName { get; set; }
        [Ignore]
        public string Description { get; set; }
        [Ignore]
        private DateTime? _LastInsertedDate { get; set; }
        public DateTime? LastInsertedDate
        {
            get
            {
                return _LastInsertedDate;
            }
            set
            {
                _LastInsertedDate = value != null ? DateTime.SpecifyKind(value.Value, DateTimeKind.Unspecified) : value;
            }
        }
        [Ignore]
        public string Cover { get; set; }
        [Ignore]
        public int? TemplateId { get; set; }
        [Ignore]
        public string AlbumRelation { get; set; }
        [Ignore]
        public string MetaAvatar { get; set; }
        [Ignore]
        public string MetaData { get; set; }
    }

}
