﻿using ChannelVN.IMS2.Foundation.Data;
using Nest;
using System;
using System.Collections.Generic;
using System.Text;

namespace ChannelVN.IMS2.Core.Entities
{
    public class NewsCrawlerSearchInputModel : Entity
    {
        public string keyword { get; set; }
        public OrderBy? orderBy { get; set; }
        public int? status { get; set; }
        public DateTime? fromDate { get; set; }
        public DateTime? toDate { get; set; }
        public int? pageIndex { get; set; }
        public int? pageSize { get; set; }
    }

    public enum OrderBy
    {
        CreatedDate_ASC = 1,
        CreatedDate_DESC = 2,
        Schedule_ASC = 3,
        Schedule_DESC = 4,
    }

    public class ScheduleNewsCrawler{}

    public class NewsCrawler: Entity
    {
        public long Id { get; set; }
        public string EncryptId { get { return Id.ToString(); } }
        public string Source { get; set; }
        [Keyword]
        public string Link { get; set; }
        public string Title { get; set; }
        public string Avatar { get; set; }        
        public string Description { get; set; }
        public long OfficerId { get; set; }

        public string CrawledBy { get; set; }
        public DateTime? CrawledDate { get; set; }
        public int Status { get; set; }
        public string ScheduleBy { get; set; }
        public DateTime? ScheduleDate { get; set; }
        public string Sapo { get; set; }   
        public long MediaId { get; set; }
    }

    public class ResponseLinkDataModel
    {
        public List<string> lstLink { get; set; }
    }

    public enum EnumStatusNewsCrawler
    {        
        Default=0,        
        Publish=1,        
        Delete = 2
    }
}
