﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ChannelVN.IMS2.Core.Entities
{
    public class GetNewsByStatus
    {
        public string Status { get; set; }
        public DateTime? FromDate { get; set; }
        public DateTime? ToDate { get; set; }
        public int? PageIndex { get; set; }
        public int? SearchDateBy { get; set; }
        public int? PageSize { get; set; }
        public int? OrderBy { get; set; }
    }
}
