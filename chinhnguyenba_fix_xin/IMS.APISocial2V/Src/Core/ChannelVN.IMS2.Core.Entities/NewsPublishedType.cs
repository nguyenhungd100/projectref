﻿namespace ChannelVN.IMS2.Core.Entities
{
    public enum NewsPublishedType
    {
        Post = 1,
        Remind = 2
    }
}
