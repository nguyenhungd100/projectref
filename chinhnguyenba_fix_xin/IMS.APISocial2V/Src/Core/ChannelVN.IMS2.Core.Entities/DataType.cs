﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ChannelVN.IMS2.Core.Entities
{
    public enum DataType
    {
        Webview = 0,
        Native = 1,
        AMP = 2,
        InstantView = 3
    }
}
