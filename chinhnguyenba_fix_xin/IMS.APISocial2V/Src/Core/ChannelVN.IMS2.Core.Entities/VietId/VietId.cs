﻿using Newtonsoft.Json;

namespace ChannelVN.IMS2.Core.Entities.VietId
{
    public class UserInfoResponse
    {
        [JsonProperty("meta")]
        public Meta Metas { get; set; }

        [JsonProperty("data")]
        public Data Datas { get; set; }

        public partial class Data
        {
            [JsonProperty("id")]
            public string Id { get; set; }

            [JsonProperty("uniqid")]
            public string Uniqid { get; set; }

            [JsonProperty("name")]
            public string Name { get; set; }

            [JsonProperty("mobile")]
            public string Mobile { get; set; }

            [JsonProperty("status")]
            public string Status { get; set; }

            [JsonProperty("gender")]
            public string Gender { get; set; }

            [JsonProperty("fullname")]
            public string Fullname { get; set; }

            [JsonProperty("birthday")]
            public string Birthday { get; set; }

            [JsonProperty("address")]
            public string Address { get; set; }

            [JsonProperty("avatar")]
            public string Avatar { get; set; }

            [JsonProperty("email")]
            public string Email { get; set; }

            [JsonProperty("code_login")]
            public string CodeLogin { get; set; }

            [JsonProperty("change_info")]
            public long ChangeInfo { get; set; }
        }

        public partial class Meta
        {
            [JsonProperty("signal")]
            public long Signal { get; set; }

            [JsonProperty("error_code")]
            public long ErrorCode { get; set; }

            [JsonProperty("message")]
            public string Message { get; set; }
        }
    }

    public class EnterPinResponse
    {
        [JsonProperty("meta")]
        public Meta Metas { get; set; }

        [JsonProperty("data")]
        public Data Datas { get; set; }

        public partial class Data
        {
            [JsonProperty("access_token")]
            public string AccessToken { get; set; }

            [JsonProperty("expires_in")]
            public string ExpiresIn { get; set; }

            [JsonProperty("token_type")]
            public string TokenType { get; set; }

            [JsonProperty("scope")]
            public object Scope { get; set; }
        }

        public partial class Meta
        {
            [JsonProperty("signal")]
            public long Signal { get; set; }

            [JsonProperty("error_code")]
            public long ErrorCode { get; set; }

            [JsonProperty("message")]
            public string Message { get; set; }
        }
    }

    public class KinghubApiResponse
    {
        public int status { get; set; }
        public string message { get; set; }
        public int code { get; set; }
        public KinghubApiResponseData data { get; set; }
    }

    public class KinghubApiResponseData
    {
        public string userID { get; set; }
        public string sessionId { get; set; }
    }
}
