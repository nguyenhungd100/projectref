﻿using ChannelVN.IMS2.Core.Entities.Distribution;
using System;
using System.Collections.Generic;
using System.Text;

namespace ChannelVN.IMS2.Core.Entities
{
    public class GetNewsByDistribution
    {
        public long? DistributionId { get; set; }
        public DistributionPublishStatus? PublishStatus { get; set; }
        public DateTime? FromDate { get; set; }
        public DateTime? ToDate { get; set; }
        public int? PageIndex { get; set; }
        public int? PageSize { get; set; }
    }
}
