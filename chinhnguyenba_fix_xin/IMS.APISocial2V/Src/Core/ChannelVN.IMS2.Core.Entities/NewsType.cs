﻿namespace ChannelVN.IMS2.Core.Entities
{
    public enum NewsType
    {
        Article = 1,
        Video = 2,
        PlayList = 3,
        MediaUnit = 4,
        Post = 5,
        Photo = 6,
        ShareLink = 7,        
        Album =8,
        Beam=9,
        Gallery = 12
        //Gallery1 = 12,
        //Gallery2 = 13
    }

    public enum NewsType2
    {
        Article = 1,
        Video = 2,
        PlayList = 3,
        MediaUnit = 4,
        Post = 5,
        Photo = 6,
        ShareLink = 7,
        Album = 8,
        Beam = 9,
        Board = 99
    }
}
