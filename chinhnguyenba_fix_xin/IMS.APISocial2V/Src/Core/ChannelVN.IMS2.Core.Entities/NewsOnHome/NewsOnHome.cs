﻿using System;
using System.Collections.Generic;

namespace ChannelVN.IMS2.Core.Entities.NewsOnHome
{
    //1=orderOnHome,2=acceptedOnHome,3=canceledOnHome
    public enum EnumStatusNewsOnHome
    {
        OrderOnHome=1,
        AcceptedOnHome=2,
        CanceledOnHome=3
    }

    //0=khong ra home,1=ra home
    public enum EnumStatusKingHub
    {
        NotOnHome = 0,
        OnHome = 1        
    }
    public class NewsOnHome
    {
        public long Id { get; set; }
        public long NewsId { get; set; }
        public long OfficerId { get; set; }
        public byte? OfficerClass { get; set; }
        public int? Type { get; set; }
        public int Status { get; set; }
        public DateTime? OrderedDate { get; set; }
        public string OrderedBy { get; set; }
        public DateTime? AcceptedDate { get; set; }
        public string AcceptedBy { get; set; }
        public DateTime? CanceledDate { get; set; }
        public string CanceledBy { get; set; }
    }

    public class NewsOnHomeSearch
    {        
        public string id { get; set; }
        public string media_id { get; set; }
        public string link_share { get; set; }
        public string title { get; set; }
        public int card_type { get; set; }
        public List<DataItem> data { get; set; }
        public int status { get; set; }
        public byte? officerClass { get; set; }
        public string OfficerId { get; set; }
        public User user { get; set; }
    }
    public class User
    {        
        public string full_name { get; set; }
        public string avatar { get; set; }
        public string id { get; set; }        
    }
    public class DataItem
{
        public int content_type { get; set; }
        public string created_at { get; set; }
        public int height { get; set; }
        public string id { get; set; }
        public string link { get; set; }
        public string thumb { get; set; }
        public string title { get; set; }
        public int width { get; set; }
        public DataItemImage image { get; set; }
    }

    public class DataItemImage{
        public int content_type { get; set; }
        public int height { get; set; }
        public string thumb { get; set; }
        public int width { get; set; }
    }

    public class NewsOnHomeSearchInputModel
    {
        public string keyword { get; set; }
        public OrderBy? orderBy { get; set; }
        public int? status { get; set; }
        public byte? officerClass { get; set; }
        public DateTime? fromDate { get; set; }
        public DateTime? toDate { get; set; }
        public int? pageIndex { get; set; }
        public int? pageSize { get; set; }
    }

    public class NotificationModel
    {
        public long Id { get; set; }
        public string Note { get; set; }        
    }

    public class ListSearchNewsOnHomeCache
    {

    }

    public class ListQueueJobNewsOnHome
    {

    }
}
