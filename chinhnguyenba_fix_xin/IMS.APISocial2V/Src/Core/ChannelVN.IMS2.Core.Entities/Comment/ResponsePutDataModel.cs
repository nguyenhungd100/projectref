﻿namespace ChannelVN.IMS2.Core.Entities.Comment
{
    public class ResponsePutDataModel<T>
    {
        public int? Status { get; set; }
        public int? Code { get; set; }
        public string Message { get; set; }
        public T[] Result { get; set; }
    }

    public class ResultPutDataModel<T>
    {
        public int? Code { get; set; }
        public string Message { get; set; }
        public T Data { get; set; }
    }
}
