﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ChannelVN.IMS2.Core.Entities.Comment
{
    public class RequestMemberModel
    {
        public string RecordID { get; set; }
        public long? OfficerId { get; set; }
        public long? UserID { get; set; }
        public DateTime? LastUpdateTime { get; set; }
        public string LastUpdateBy { get; set; }
        public int? Status { get; set; }
        public string BeforeCursor { get; set; }
        public string AfterCursor { get; set; }
        public DateTime? FromDate { get; set; }
        public DateTime? ToDate { get; set; }
        public int? Limit { get; set; }

        public int Group { get; set; }
    }

    public enum EnumUserCommentGroup
    {
        Block=1,
        Classify=2,
        TopMember=3
    }

    public enum EnumUserCommentGroupBlock
    {
        Normal = 0,
        BlackList = 1,
        WhiteList = 2
    }

    public enum EnumUserCommentGroupClassify
    {
        Grey = 0,
        Black = 1,
        White = 2
    }

    public enum EnumUserCommentGroupTopMember
    {
        Normal = 0,
        TopFan = 1,
        TopContributor = 2
    }
}
