﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ChannelVN.IMS2.Core.Entities.Comment
{
    public class RequestDataModel
    {
        public string Id { get; set; }
        public string MediaId { get; set; }
        public string Media { get { return MediaId; } }
        public string BeforeCursor { get; set; }
        public string AfterCursor { get; set; }
        public int? Limit { get; set; }
        public int? ChildLimit { get; set; }
        public int? Type { get; set; }

        //cho search
        public bool? IsCheck { get; set; }
        public bool? IsDelete { get; set; }
        public string Text { get; set; }
        public long? PostId { get; set; }
        public long? PageId { get; set; }
        public DateTime? FromDate { get; set; }
        public DateTime? ToDate { get; set; }
        public bool? IsParent { get; set; }
        public string CreatedBy { get; set; }

        //Cho detail
        public string CommentId { get; set; }
    }
}
