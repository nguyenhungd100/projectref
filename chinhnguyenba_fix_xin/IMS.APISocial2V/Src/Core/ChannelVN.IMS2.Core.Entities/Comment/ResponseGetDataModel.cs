﻿namespace ChannelVN.IMS2.Core.Entities.Comment
{
    public class ResponseGetDataModel<T>
    {
        public T Result { get; set; }
        public int? Code { get; set; }
        public string Message { get; set; }
        public int? Status { get; set; }
    }

    public class ResponseData<T>
    {
        public int Code { get; set; }
        public T Data { get; set; }
        public string BeforeCursor { get; set; }
        public string AfterCursor { get; set; }
    }

    public class ResponseUserInfoData<T>
    {
        public int Code { get; set; }
        public T Data { get; set; }
        public int Status { get; set; }
        public string Message { get; set; }
    }

    public class UserInfoResponse
    {
        public string user_id { get; set; }
        public string user_name { get; set; }
        public string full_name { get; set; }
        public string avatar { get; set; }
    }

    public class ResultGetDataModel<T>
    {
        //public int? Total { get; set; }
        public bool? HashBefore { get; set; }
        public T[] data { get; set; }
        public string BeforeCursor { get; set; }
        public string AfterCursor { get; set; }
        public int? TotalAll { get; set; }
        public bool? HashAfter { get; set; }
    }

    //public class CommentDataModel
    //{
    //    public long? CreatedAt { get; set; }
    //    public bool? Deleted { get; set; }
    //    public string CreatedBy { get; set; }
    //    public string CommentID { get; set; }
    //    public string ParentCommentID { get; set; }
    //    public string PostID { get; set; }
    //    public CounterDataModel Counter { get; set; }
    //    public UserDataModel User { get; set; }
    //    public ContentDataModel Content { get; set; }
    //    public int UpdatedAt { get; set; }
    //}

    //public class CounterDataModel
    //{
    //    public int NumLike { get; set; }
    //    public int NumChildren { get; set; }
    //}

    //public class UserDataModel
    //{
    //    public string Id { get; set; }
    //    public string Username { get; set; }
    //    public string Full_name { get; set; }
    //    public string Avatar { get; set; }
    //    public string Cover { get; set; }
    //}

    //public class ContentDataModel
    //{
    //    public string Text { get; set; }
    //    public object Media { get; set; }
    //    public ExtensionDataModel Extension { get; set; }
    //}

    //public class ExtensionDataModel
    //{
    //    public object Preview { get; set; }
    //    public Caption[] Status { get; set; }
    //}
}
