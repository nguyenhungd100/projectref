﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ChannelVN.IMS2.Core.Entities.Comment
{
    public class UserCommentResponse
    {
        public string RecordID { get; set; }
        public long? LastUpdateTime { get; set; }
        public string LastUpdateBy { get; set; }
        public string UserID { get; set; }
        public string PageID { get; set; }
        public int? Status { get; set; }
        public string Avatar { get; set; }
        public string FullName { get; set; }
        public string Type { get; set; }
        public bool IsFan { get; set; }
        public DateTime? JoinDate { get; set; }
    }
}
