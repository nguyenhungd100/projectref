﻿using ChannelVN.IMS2.Core.Entities.Distribution;
using ChannelVN.IMS2.Foundation.Common;
using System;
using System.Runtime.Serialization;

namespace ChannelVN.IMS2.Core.Entities.Comment
{
    public class CommentDataModel
    {
        public long CreatedAt { get; set; }
        
        public DateTime? CreatedDate
        {
            get
            {
                return Utility.ConvertSecondsUtcToDate(CreatedAt, DateTimeKind.Local);
            }
        }
        
        public bool? Deleted { get; set; }
        
        public bool? Show { get; set; }
        public string CreatedBy { get; set; }
        
        public string CommentID { get; set; }
        
        //public BasePost BasePost { get; set; }
        
        public string ParentCommentID { get; set; }
        
        public string PostID { get; set; }
        
        public CounterDataModel Counter { get; set; }
        
        public UserDataModel User { get; set; }
        
        public ContentDataModel Content { get; set; }
        
        public long UpdatedAt { get; set; }
        
        public DateTime? UpdatedDate { get
            {
                return Utility.ConvertSecondsUtcToDate(UpdatedAt);
            }
        }

        //new
        public bool? NeedModerate { get; set; }
        public long ApprovedAt { get; set; }
        public int mode { get; set; }
        public WaitingLog WaitingLog { get; set; }
        public Post Post { get; set; }
        public object Children { get; set; }
    }
    public class WaitingLog
    {
        public string Payload { get; set; }
        public string CommentID { get; set; }
        public string LogID { get; set; }
        public string Type { get; set; }
        public string UserID { get; set; }
        public long Timestamp { get; set; }
    }

    public class Post
    {
        public string Media_id { get; set; }
        public int CardType { get; set; }
        public User User { get; set; }
        public string PostName { get; set; }
        public string PublishData { get; set; }
    }

    public class User
    {
        public string Id { get; set; }
    }

    public class CounterDataModel
    {
        public int NumLike { get; set; }
        public int NumChildren { get; set; }
    }

    public class UserDataModel
    {
        public string Id { get; set; }
        public string Username { get; set; }
        public string Full_name { get; set; }
        public string Avatar { get; set; }
        public string Cover { get; set; }
        public object PageUserStatus { get; set; }
    }

    public class ContentDataModel
    {
        public string Text { get; set; }
        public object Media { get; set; }
        public ExtensionDataModel Extension { get; set; }
    }

    public class ExtensionDataModel
    {
        public object Preview { get; set; }
        public Caption[] Status { get; set; }
        public object[] RichMedias { get; set; }
        public object ReplyUser { get; set; }
        public object QuoteComment { get; set; }
    }
}
