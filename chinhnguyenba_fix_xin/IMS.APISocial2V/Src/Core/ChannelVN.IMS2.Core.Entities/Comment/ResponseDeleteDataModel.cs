﻿namespace ChannelVN.IMS2.Core.Entities.Comment
{
    public class ResponseDeleteDataModel<T>
    {
        public T[] Result { get; set; }
        public int? Code { get; set; }
        public string Message { get; set; }
        public int? Status { get; set; }
    }

    public class ResultDeleteDataModel<T>
    {
        public int? Code { get; set; }
        public T data { get; set; }
        public string message { get; set; }
    }

    public class DataModel
    {
        public bool? Deleted { get; set; }
        public string CommentID { get; set; }
        public string Message { get; set; }
    }
}
