﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ChannelVN.IMS2.Core.Entities.Posts
{
    public interface IPost
    {
        string Caption { get; set; }
    }
    public class PostSearch:NewsSearch, IPost
    {
        public string Caption { get; set; }
    }
    public class Post: News, IPost
    {
        public string Caption { get; set; }
    }
    public class PostSearchReturn: NewsSearchReturn
    {
        public string Caption { get; set; }
    }
}
