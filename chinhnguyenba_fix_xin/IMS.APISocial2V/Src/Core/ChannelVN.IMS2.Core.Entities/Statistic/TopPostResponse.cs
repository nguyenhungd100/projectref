﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ChannelVN.IMS2.Core.Entities.Statistic
{
    public class TopPostResponse
    {
        public int total { get; set; }
        public List<TopPostDataResponse> list { get; set; }
    }
    public class TopPostDataResponse
    {
        public string key { get; set; }
        public int value { get; set; }
        public string id { get; set; }
        public string title { get; set; }
        public string avatar { get; set; }
        public object mediaUnitDesc { get; set; }
        public string distibutionDate { get; set; }
    }

    public class TopReachPostResponse
    {
        public TotalReach total { get; set; }
        public List<TopReachPostDataResponse> list { get; set; }
    }
    public class TotalReach
    {
        public int views { get; set; }
        public int clicks { get; set; }
        public int reach { get; set; }
    }
    public class TopReachPostDataResponse
    {
        public string postId { get; set; }
        public int views { get; set; }
        public int clicks { get; set; }
        public int reach { get; set; }
        public string id { get; set; }
        public string title { get; set; }
        public string avatar { get; set; }
        public object mediaUnitDesc { get; set; }
        public string distibutionDate { get; set; }
    }
}
