﻿using System.ComponentModel.DataAnnotations;

namespace ChannelVN.IMS2.Core.Entities.Statistic.Rearch.Request
{
    public class InteractionsRequestDataModel: FollowerRequestDataModel
    {
        [Required]
        public Type Type { get; set; }
    }

    public enum Type
    {
        Post = 1,
        Page = 2
    }
}
