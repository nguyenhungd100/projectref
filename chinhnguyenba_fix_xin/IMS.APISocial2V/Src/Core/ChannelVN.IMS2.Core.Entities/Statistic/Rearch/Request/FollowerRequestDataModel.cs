﻿using System;
using System.ComponentModel.DataAnnotations;

namespace ChannelVN.IMS2.Core.Entities.Statistic.Rearch.Request
{
    public class FollowerRequestDataModel
    {
        [Required]
        public long Id { get; set; }
        [Required]
        public DateTime StartDate { get; set; }
        [Required]
        public DateTime EndDate { get; set; }
    }
}
