﻿using System;

namespace ChannelVN.IMS2.Core.Entities.Statistic.Rearch.Response
{
    public class GetPostResponseDataModel
    {
        public DateTime Date { get; set; }
        public int Post { get; set; }
    }
}
