﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ChannelVN.IMS2.Core.Entities.Statistic.Rearch.Response
{
    public class GetInteractionsResponseDataModel
    {
        public DateTime? Date { get; set; }
        public long? Post { get; set; }
        public long? Repost { get; set; }
        public long? Comment { get; set; }
        public long? Like { get; set; }
        public long? Send { get; set; }
    }
}
