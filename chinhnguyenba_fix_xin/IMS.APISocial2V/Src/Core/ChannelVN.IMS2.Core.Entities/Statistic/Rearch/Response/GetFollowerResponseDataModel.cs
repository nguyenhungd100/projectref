﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ChannelVN.IMS2.Core.Entities.Statistic.Rearch.Response
{
    public class GetFollowerResponseDataModel
    {
        public DateTime? Date { get; set; }
        public long Follower { get; set; }
    }
}
