﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ChannelVN.IMS2.Core.Entities.Statistic.Rearch.Response
{
    public class GetRepostResponseDataModel
    {
        public DateTime Date { get; set; }
        public int Repost { get; set; }
    }
}
