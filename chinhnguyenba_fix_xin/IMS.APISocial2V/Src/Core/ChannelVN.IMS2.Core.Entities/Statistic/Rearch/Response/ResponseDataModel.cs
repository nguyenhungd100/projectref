﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ChannelVN.IMS2.Core.Entities.Statistic.Rearch.Response
{
    public class ResponseDataModel<T>
    {
        public T Result { get; set; }
        public int? Code { get; set; }
        public string Message { get; set; }
        public int? Status { get; set; }
    }
}
