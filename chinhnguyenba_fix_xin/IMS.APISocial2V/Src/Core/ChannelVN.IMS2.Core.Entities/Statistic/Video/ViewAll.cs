﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ChannelVN.IMS2.Core.Entities.Statistic.Video
{
    public class ViewAll
    {
        public ViewInfo[] Videos { get; set; }
    }

    public class ViewInfo
    {
        public string Key { get; set; }
        public string Text { get; set; }
        public int Play { get; set; }
        public int Like { get; set; }
        public int Comment { get; set; }
        public int Share { get; set; }
    }
}
