﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ChannelVN.IMS2.Core.Entities.Statistic.Video
{
    public class ViewByDate
    {
        public string Key { get; set; }
        public string Text { get; set; }
        public InfoByDate[] Dates { get; set; }
    }

    public class InfoByDate
    {
        public string Date { get; set; }
        public Metrics Metrics { get; set; }
    }

    public class Metrics
    {
        public int Play { get; set; }
        public int Like { get; set; }
        public int Comment { get; set; }
        public int Share { get; set; }
    }
}
