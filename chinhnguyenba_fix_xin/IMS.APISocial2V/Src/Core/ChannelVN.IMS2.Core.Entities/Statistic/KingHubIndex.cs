﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ChannelVN.IMS2.Core.Entities.Statistic
{
    public class KingHubIndex
    {
        public string Key { get; set; }
        public string Text { get; set; }
        public int Play { get; set; }
        public int P25 { get; set; }
        public int P50 { get; set; }
        public int P75 { get; set; }
        public int P100 { get; set; }
    }
}
