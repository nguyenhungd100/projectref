﻿using ChannelVN.IMS2.Foundation.Data;
using ChannelVN.IMS2.Foundation.Data.Factories;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace ChannelVN.IMS2.Core.Entities
{
    public class NewsCrawlerConfigSearchModel : Entity
    {
        public string keyword { get; set; }
        public long officerId { get; set; }
        public ConfigOrderBy? orderBy { get; set; }
        public int? status { get; set; }
        public DateTime? fromDate { get; set; }
        public DateTime? toDate { get; set; }
        public int? pageIndex { get; set; }
        public int? pageSize { get; set; }
    }
    public enum ConfigOrderBy
    {
        CreatedDate_ASC = 1,
        CreatedDate_DESC = 2,
        Schedule_ASC = 3,
        Schedule_DESC = 4,
    }

    public class NewsCrawlerConfigSearch : NewCrawlerConfigurationSearch
    {

    }

    public class NewCrawlerConfigurationSearch : Entity
    {
        public long Id { get; set; }

        public long OfficerId { get; set; }
        public string EncryptId { get { return Id.ToString(); } }

        public string Source { get; set; }

        public string CreatedBy { get; set; }

        private DateTime? _CreatedDate { get; set; }

        public DateTime? CreatedDate
        {
            get
            {
                return _CreatedDate;
            }
            set
            {
                _CreatedDate = value != null ? DateTime.SpecifyKind(value.Value, DateTimeKind.Unspecified) : value;
            }
        }

        public PublishModeStatus? PublishMode { get; set; }

        public int? IntervalTime { get; set; }

        public EnumStatusCrawlerConfig? Status { get; set; }

        public DateTime? JobAutoLastDate { get; set; }

        public DateTime? LastScanTime { get; set; }
    }

    public enum EnumStatusCrawlerConfig
    {
        Stop_Run = 0,
        EnableJob = 1,
        Delete = 2
    }

    public enum PublishModeStatus
    {
        ForPublisher = 1,
        AutoPublish = 2,
    }

    public class NewsCrawlerConfigQueue
    {

    }

    public class NewsCrawlerSourceScan
    {

    }

}
