﻿using System;

namespace ChannelVN.IMS2.Core.Entities.DashBoard
{
    public class StatisticQuantity
    {
        public DateTime? FromDate { get; set; }
        public DateTime? ToDate { get; set; }
        public long? AccountId { get; set; }
        public NewsType? Type { get; set; }
        public NewsStatus? Status { get; set; }
        public long? DistributorId { get; set; }
    }
}
