﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ChannelVN.IMS2.Core.Entities
{
    public enum CaptionType
    {
        Text = 1,
        Tag = 2,
        Link = 3,
        HashTag = 4
    }
}
