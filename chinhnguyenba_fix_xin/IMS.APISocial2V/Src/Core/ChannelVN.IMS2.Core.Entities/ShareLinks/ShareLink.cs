﻿using ChannelVN.IMS2.Foundation.Data;
using System;
using System.Collections.Generic;
using System.Text;

namespace ChannelVN.IMS2.Core.Entities.ShareLinks
{
    public interface IShareLink
    {
        string Title { get; set; }
        string Avatar { get; set; }
        string Source { get; set; }
    }
    public class ShareLinkSearch : NewsSearch, IShareLink
    {
        public string Title { get; set; }
        public string Avatar { get; set; }
        public string Source { get; set; }
    }
    public class ShareLink : News, IShareLink
    {
        public string Title { get; set; }
        public string Avatar { get; set; }
        public string Source { get; set; }
    }
    public class ShareLinkSearchReturn : NewsSearchReturn
    {
        public string Title { get; set; }
        public string Avatar { get; set; }
        public string Source { get; set; }
    }
}
