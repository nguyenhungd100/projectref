﻿using System;
using System.Collections.Generic;

namespace ChannelVN.IMS2.Core.Entities.AccountAssignment
{
    //1-quản lý partnership,2-approve post
    public enum EnumAssignedRole
    {
        ManagePartnership = 1,
        ApprovePost = 2
    }
    public class AccountAssignment
    {
        public string Id { get; set; }
        public long AccountId { get; set; }
        public long OfficerId { get; set; }    
        public int AssignedRole { get; set; }        
        public DateTime AssignedDate { get; set; }        
        public string AssignedBy { get; set; }        
    }

    public class AccountAssignmentInput
    {        
        public long? OfficerId { get; set; }
        public int? PageIndex { get; set; }
        public int? PageSize { get; set; }       
    }
}
