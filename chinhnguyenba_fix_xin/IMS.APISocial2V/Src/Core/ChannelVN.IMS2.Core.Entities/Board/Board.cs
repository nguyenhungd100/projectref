﻿using ChannelVN.IMS2.Foundation.Data;
using Nest;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace ChannelVN.IMS2.Core.Entities.Board
{
    public interface IBoard
    {
         long Id { get; set; }
         string Name { get; set; }
         string UnsignName { get; set; }
         string Description { get; set; }
         string Url { get; set; }
         string Avatar { get; set; }
         string Cover { get; set; }
         long? NewsCoverId { get; set; }
         DateTime? CreatedDate { get; set; }
         string CreatedBy { get; set; }
         DateTime? ModifiedDate { get; set; }
         string ModifiedBy { get; set; }
         int? Status { get; set; }
         bool? IsHot { get; set; }
         DateTime? LastInsertedDate { get; set; }
        int ? FollowCount { get; set; }
        long? ParentId { get; set; }
        string RelationBoard { get; set; }
    }

    public class Board: Entity,IBoard
    {
        public long Id { get; set; }
        public string EncryptId { get { return Id.ToString(); } }
        public string Name { get; set; }
        public string UnsignName { get; set; }
        public string Description { get; set; }
        public string Url { get; set; }
        public string Avatar { get; set; }
        public string Cover { get; set; }
        public long? NewsCoverId { get; set; }
        private DateTime? _CreatedDate { get; set; }
        public DateTime? CreatedDate
        {
            get
            {
                return _CreatedDate;
            }
            set
            {
                _CreatedDate = value != null ? DateTime.SpecifyKind(value.Value, DateTimeKind.Unspecified) : value;
            }
        }
        public string CreatedBy { get; set; }
        private DateTime? _ModifiedDate { get; set; }
        public DateTime? ModifiedDate
        {
            get
            {
                return _ModifiedDate;
            }
            set
            {
                _ModifiedDate = value != null ? DateTime.SpecifyKind(value.Value, DateTimeKind.Unspecified) : value;
            }
        }
        public string ModifiedBy { get; set; }
        public int? Status { get; set; }
        public bool? IsHot { get; set; }
        private DateTime? _LastInsertedDate { get; set; }
        public DateTime? LastInsertedDate
        {
            get
            {
                return _LastInsertedDate;
            }
            set
            {
                _LastInsertedDate = value != null ? DateTime.SpecifyKind(value.Value, DateTimeKind.Unspecified) : value;
            }
        }

        public int? FollowCount { get ; set ; }
        public long? ParentId { get; set; }
        public string RelationBoard { get; set; }
    }

    public class BoardSearch : Entity, IBoard
    {

        public long Id { get; set; }
        [Ignore]
        public string EncryptId { get { return Id.ToString(); } }
        public string Name { get; set; }
        [Ignore]
        public string UnsignName { get; set; }
        [Ignore]
        public string Description { get; set; }
        [Ignore]
        public string Url { get; set; }
        [Ignore]
        public string Avatar { get; set; }
        [Ignore]
        public string Cover { get; set; }
        [Ignore]
        public long? NewsCoverId { get; set; }
        [Ignore]
        private DateTime? _CreatedDate { get; set; }
        public DateTime? CreatedDate
        {
            get
            {
                return _CreatedDate;
            }
            set
            {
                _CreatedDate = value != null ? DateTime.SpecifyKind(value.Value, DateTimeKind.Unspecified) : value;
            }
        }
        public string CreatedBy { get; set; }
        [Ignore]
        private DateTime? _ModifiedDate { get; set; }
        public DateTime? ModifiedDate
        {
            get
            {
                return _ModifiedDate;
            }
            set
            {
                _ModifiedDate = value != null ? DateTime.SpecifyKind(value.Value, DateTimeKind.Unspecified) : value;
            }
        }
        public string ModifiedBy { get; set; }
        public int? Status { get; set; }
        public bool? IsHot { get; set; }
        [Ignore]
        private DateTime? _LastInsertedDate { get; set; }
        public DateTime? LastInsertedDate
        {
            get
            {
                return _LastInsertedDate;
            }
            set
            {
                _LastInsertedDate = value != null ? DateTime.SpecifyKind(value.Value, DateTimeKind.Unspecified) : value;
            }
        }
        [Ignore]
        public int? FollowCount { get; set; }
        [Ignore]
        public long? ParentId { get; set; }
        [Ignore]
        public string RelationBoard { get; set; }
        [Ignore]
        public NewsInBoard[] NewsInBoard { get; set; }
    }

    public class BoardSearchReturn: BoardSearch
    {

    }

    public class NewsInBoard
    {
        public long BoardId { get; set; }
        public long NewsId { get; set; }
        private DateTime? _PublishedDate { get; set; }
        public DateTime? PublishedDate {
            get
            {
                return _PublishedDate;
            }
            set
            {
                _PublishedDate = value != null ? DateTime.SpecifyKind(value.Value, DateTimeKind.Unspecified) : value;
            }
        }
    }

    public enum BoardStatus
    {
        Actived=1,
        UnActived=0
    }

    public class SearchBoard
    {
        public string Keyword { get; set; }
        [Required]
        public long OfficerId { get; set; }
        public BoardStatus? Status { get; set; }
        public DateTime? FromDate { get; set; }
        public DateTime? ToDate { get; set; }
        public SearchNewsOrderBy? OrderBy { get; set; }
        public int? PageIndex { get; set; }
        public int? PageSize { get; set; }
    }
}
