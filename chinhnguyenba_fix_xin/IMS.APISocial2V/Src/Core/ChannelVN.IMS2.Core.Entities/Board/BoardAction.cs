﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ChannelVN.IMS2.Core.Entities.Board
{
    /// <summary>
    /// Các action liên quan tới Bảng tin (API name)
    /// </summary>
    public class BoardAction
    {
        public const string Add = "add";
        public const string Update = "update";
        public const string Delete = "delete";
        public const string AddMultiPost = "addmultipost";
        public const string DeleteMultiPost = "deletemultipost";
        public const string UpdateOwner = "updateowner";
        public const string InsertOwner = "insertowner";
        public const string InsertUpdateOwner = "insertupdateowner";
    }
}
