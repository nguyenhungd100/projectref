﻿namespace ChannelVN.IMS2.Core.Entities.Board
{
    public class KingHubBoardResponse<T>
    {
        public Result<T> Result { get; set; }
        public string Message { get; set; }
        public int Status { get; set; }
    }

    public class Result<T>
    {
        public T Data { get; set; }
    }
}
