﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ChannelVN.IMS2.Core.Entities.Board
{
    public class BoardConstant
    {
        public const int AvatarWidth = 450;
        public const int AvatarHeight = 300;
        public const int CoverWidth = 1024;
        public const int CoverHeight = 780;
    }
}
