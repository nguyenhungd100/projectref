﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ChannelVN.IMS2.Core.Entities.Board
{
    public class PushInfo
    {
        public long ObjectId { get; set; }
        public string ModifiedBy { get; set; }
    }
}
