﻿using ChannelVN.IMS2.Core.Entities.Album;
using ChannelVN.IMS2.Foundation.Data.Providers;
using ChannelVN.IMS2.Foundation.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ChannelVN.IMS2.Core.Models.DataStore.Album
{
    public class AlbumDataStore : CmsMainDataStore
    {
        public async Task<bool> AddAsync(AlbumSearch data, bool isQueue = false)
        {
            var returnValue = false;
            try
            {
                using (var _context = GetContext())
                {
                    try
                    {
                        var cmd = _context.CreateCommand(Scripts.Album.Add);

                        _context.AddParameter(cmd, "Id", data.Id);
                        _context.AddParameter(cmd, "Type", data.Type);
                        _context.AddParameter(cmd, "Url", data.Url);
                        _context.AddParameter(cmd, "OriginalUrl", data.OriginalUrl);
                        _context.AddParameter(cmd, "Status", data.Status);
                        _context.AddParameter(cmd, "Author", data.Author);
                        _context.AddParameter(cmd, "CategoryId", data.CategoryId);
                        _context.AddParameter(cmd, "CreatedDate", data.CreatedDate);
                        _context.AddParameter(cmd, "CreatedBy", data.CreatedBy);
                        _context.AddParameter(cmd, "Location", data.Location);
                        _context.AddParameter(cmd, "PublishMode", data.PublishMode);
                        _context.AddParameter(cmd, "PublishData", data.PublishData);
                        _context.AddParameter(cmd, "DistributionDate", data.DistributionDate);
                        _context.AddParameter(cmd, "Tags", data.Tags);
                        _context.AddParameter(cmd, "CardType", data.CardType);
                        _context.AddParameter(cmd, "Name", data.Name);
                        _context.AddParameter(cmd, "UnsignName", data.UnsignName);
                        _context.AddParameter(cmd, "DistributorId", data.DistributorId);
                        _context.AddParameter(cmd, "Description", data.Description);
                        _context.AddParameter(cmd, "LastInsertedDate", data.LastInsertedDate);
                        _context.AddParameter(cmd, "TemplateId", data.TemplateId);
                        _context.AddParameter(cmd, "AlbumRelation", data.AlbumRelation);
                        _context.AddParameter(cmd, "ApprovedBy", data.ApprovedBy);
                        _context.AddParameter(cmd, "MetaAvatar", data.MetaAvatar);
                        _context.AddParameter(cmd, "MetaData", data.MetaData);
                        _context.AddParameter(cmd, "CommentMode", (Byte?)data.CommentMode);
                        _context.AddParameter(cmd, "LabelType", (int?)data.LabelType);
                        //PhotoInAlbum
                        _context.AddParameter(cmd, "PhotoInAlbum", string.Join(",", data.PhotoInAlbum?.Select(p => p.PhotoId + "_" + p.Priority ?? 0 + "_" + (p.PublishedDate ?? DateTime.Now).ToString("yyyy-MM-dd HH:mm:ss.fff")) ?? new string[] { }));
                        //NewsInAccount
                        _context.AddParameter(cmd, "AccountId", data.NewsInAccount[data.NewsInAccount.Length - 1].AccountId);
                        _context.AddParameter(cmd, "PublishedType", data.NewsInAccount[data.NewsInAccount.Length - 1].PublishedType);
                        _context.AddParameter(cmd, "PublishedDate", data.NewsInAccount[data.NewsInAccount.Length - 1].PublishedDate);

                        returnValue = await _context.ExecuteNonQueryAsync(cmd) > 0;
                    }
                    catch(Exception ex)
                    {
                        if (!isQueue) Logger.Sensitive(ex, ex.Message);
                    }
                    finally
                    {
                        _context.Close();
                    }
                }
            }
            catch (Exception ex)
            {
                if (!isQueue) Logger.Sensitive(ex, ex.Message);
            }
            return returnValue;
        }

        public async Task<bool> UpdateAsync(AlbumSearch data, bool isQueue = false)
        {
            var returnValue = false;
            try
            {
                using(var _context= GetContext())
                {
                    try
                    {
                        var cmd = _context.CreateCommand(Scripts.Album.Update);

                        _context.AddParameter(cmd, "Id", data.Id);
                        //_context.AddParameter(cmd, "Type", data.Type);
                        _context.AddParameter(cmd, "Url", data.Url);
                        //_context.AddParameter(cmd, "OriginalUrl", data.OriginalUrl);
                        //_context.AddParameter(cmd, "Status", data.Status);
                        _context.AddParameter(cmd, "Author", data.Author);
                        _context.AddParameter(cmd, "CategoryId", data.CategoryId);
                        _context.AddParameter(cmd, "ModifiedBy", data.ModifiedBy);
                        _context.AddParameter(cmd, "ModifiedDate", data.ModifiedDate);
                        _context.AddParameter(cmd, "Location", data.Location);
                        //_context.AddParameter(cmd, "PublishMode", data.PublishMode);
                        _context.AddParameter(cmd, "PublishData", data.PublishData);
                        _context.AddParameter(cmd, "DistributionDate", data.DistributionDate);
                        _context.AddParameter(cmd, "Tags", data.Tags);
                        _context.AddParameter(cmd, "Name", data.Name);
                        _context.AddParameter(cmd, "UnsignName", data.UnsignName);
                        _context.AddParameter(cmd, "Description", data.Description);
                        _context.AddParameter(cmd, "TemplateId", data.TemplateId);
                        _context.AddParameter(cmd, "AlbumRelation", data.AlbumRelation);
                        _context.AddParameter(cmd, "ApprovedBy", data.ApprovedBy);
                        _context.AddParameter(cmd, "ApprovedDate", data.ApprovedDate);
                        _context.AddParameter(cmd, "MetaAvatar", data.MetaAvatar);
                        _context.AddParameter(cmd, "MetaData", data.MetaData);
                        _context.AddParameter(cmd, "CommentMode", (Byte?)data.CommentMode);
                        _context.AddParameter(cmd, "LabelType", (int?)data.LabelType);
                        _context.AddParameter(cmd, "PhotoInAlbum", string.Join(",", data.PhotoInAlbum?.Select(p => p.PhotoId + "_" + p.Priority ?? 0 + "_" + (p.PublishedDate ?? DateTime.Now).ToString("yyyy-MM-dd HH:mm:ss.fff")) ?? new string[] { }));


                        returnValue = await _context.ExecuteNonQueryAsync(cmd) > 0;
                    }
                    catch(Exception ex)
                    {
                        if (!isQueue) Logger.Sensitive(ex, ex.Message);
                    }
                    finally
                    {
                        _context.Close();
                    }
                }
            }
            catch (Exception ex)
            {
                if (!isQueue) Logger.Sensitive(ex, ex.Message);
            }
            return returnValue;
        }

        public async Task<bool> UpdateStatusAsync(AlbumSearch data, bool isQueue = false)
        {
            var returnValue = false;
            try
            {
                using(var _context = GetContext())
                {
                    try
                    {
                        var cmd = _context.CreateCommand(Scripts.Album.UpdateStatus);

                        _context.AddParameter(cmd, "Id", data.Id);
                        _context.AddParameter(cmd, "Status", data.Status);
                        _context.AddParameter(cmd, "ModifiedBy", data.ModifiedBy);
                        _context.AddParameter(cmd, "ModifiedDate", data.ModifiedDate);
                        _context.AddParameter(cmd, "DistributionDate", data.DistributionDate);
                        _context.AddParameter(cmd, "ApprovedBy", data.ApprovedBy);
                        _context.AddParameter(cmd, "ApprovedDate", data.ApprovedDate);
                        returnValue = await _context.ExecuteNonQueryAsync(cmd) > 0;
                    }
                    catch(Exception ex)
                    {
                        if (!isQueue) Logger.Sensitive(ex, ex.Message);
                    }
                    finally
                    {
                        _context.Close();
                    }
                }
            }
            catch (Exception ex)
            {
                if (!isQueue) Logger.Sensitive(ex, ex.Message);
            }
            return returnValue;
        }

        public async Task<Entities.Album.Album> GetByIdAsync(long id)
        {
            var returnValue = default(Entities.Album.Album);
            try
            {
                using (var _context = GetContext())
                {
                    try
                    {
                        var cmd = _context.CreateCommand(Scripts.Album.GetById);
                        _context.AddParameter(cmd, "Id", id);
                        returnValue = await _context.GetAsync<Entities.Album.Album>(cmd);
                    }
                    catch (Exception ex)
                    {
                        Logger.Sensitive(ex, ex.Message);
                    }
                    finally
                    {
                        _context.Close();
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Sensitive(ex, ex.Message);
            }
            return returnValue;
        }

        public async Task<bool> AddPhotoAsync(long photoId, List<Entities.Album.Album> dataDb, bool isQueue = false)
        {
            var returnValue = false;
            try
            {
                using (var _context = GetContext())
                {
                    try
                    {
                        var cmd = _context.CreateCommand(Scripts.Album.AddPhoto);

                        _context.AddParameter(cmd, "PhotoId", photoId);
                        _context.AddParameter(cmd, "AlbumIds", string.Join(",", dataDb?.Select(p => p.Id.ToString())?.ToArray() ?? new string[] { }));
                        _context.AddParameter(cmd, "ModifiedBy", dataDb?.FirstOrDefault()?.ModifiedBy);
                        _context.AddParameter(cmd, "ModifiedDate", dataDb?.FirstOrDefault()?.ModifiedDate);
                        _context.AddParameter(cmd, "PublishedDate", DateTime.Now);
                        _context.AddParameter(cmd, "LastInsertedDate", dataDb?.FirstOrDefault()?.LastInsertedDate);
                        returnValue = await _context.ExecuteNonQueryAsync(cmd) > 0;
                    }
                    catch (Exception ex)
                    {
                        if (!isQueue) Logger.Sensitive(ex, ex.Message);
                    }
                    finally
                    {
                        _context.Close();
                    }
                }
            }
            catch (Exception ex)
            {
                if (!isQueue) Logger.Sensitive(ex, ex.Message);
            }
            return returnValue;
        }
    }
}
