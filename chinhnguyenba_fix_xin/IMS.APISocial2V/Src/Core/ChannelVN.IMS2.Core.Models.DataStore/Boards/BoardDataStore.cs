﻿using ChannelVN.IMS2.Core.Entities.Board;
using ChannelVN.IMS2.Foundation.Logging;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace ChannelVN.IMS2.Core.Models.DataStore.Boards
{
    public class BoardDataStore : CmsMainDataStore
    {
        public async Task<bool> AddAsync(BoardSearch data, bool isQueue = false)
        {
            var returnValue = false;
            try
            {
                using(var _context = GetContext())
                {
                    try
                    {
                        var cmd = _context.CreateCommand(Scripts.Board.Add);
                        _context.AddParameter(cmd, "Id", data.Id);
                        _context.AddParameter(cmd, "Name", data.Name);
                        _context.AddParameter(cmd, "UnsignName", data.UnsignName);
                        _context.AddParameter(cmd, "Description", data.Description);
                        _context.AddParameter(cmd, "Url", data.Url);
                        _context.AddParameter(cmd, "Avatar", data.Avatar);
                        _context.AddParameter(cmd, "Cover", data.Cover);
                        _context.AddParameter(cmd, "NewsCoverId", data.NewsCoverId);
                        _context.AddParameter(cmd, "CreatedDate", data.CreatedDate);
                        _context.AddParameter(cmd, "CreatedBy", data.CreatedBy);
                        _context.AddParameter(cmd, "Status", data.Status);
                        _context.AddParameter(cmd, "IsHot", data.IsHot);
                        _context.AddParameter(cmd, "FollowCount", data.FollowCount);
                        _context.AddParameter(cmd, "ParentId", data.ParentId);
                        _context.AddParameter(cmd, "RelationBoard", data.RelationBoard);
                        returnValue = await _context.ExecuteNonQueryAsync(cmd as SqlCommand) > 0;
                    }
                    catch (Exception ex)
                    {
                        if (!isQueue) Logger.Sensitive(ex, ex.Message);
                    }
                    finally
                    {
                        _context.Close();
                    }
                }
            }
            catch (Exception ex)
            {
                if (!isQueue) Logger.Sensitive(ex, ex.Message);
            }
            return returnValue;
        }

        public async Task<bool> UpdateAsync(BoardSearch data, bool isQueue = false)
        {
            var returnValue = false;
            try
            {
                using(var _context = GetContext())
                {
                    try
                    {
                        var cmd = _context.CreateCommand(Scripts.Board.Update);
                        _context.AddParameter(cmd, "Id", data.Id);
                        _context.AddParameter(cmd, "Name", data.Name);
                        _context.AddParameter(cmd, "UnsignName", data.UnsignName);
                        _context.AddParameter(cmd, "Description", data.Description);
                        _context.AddParameter(cmd, "Url", data.Url);
                        _context.AddParameter(cmd, "Avatar", data.Avatar);
                        _context.AddParameter(cmd, "Cover", data.Cover);
                        _context.AddParameter(cmd, "NewsCoverId", data.NewsCoverId);
                        _context.AddParameter(cmd, "ModifiedDate", data.ModifiedDate);
                        _context.AddParameter(cmd, "ModifiedBy", data.ModifiedBy);
                        _context.AddParameter(cmd, "Status", data.Status);
                        _context.AddParameter(cmd, "IsHot", data.IsHot);
                        _context.AddParameter(cmd, "FollowCount", data.FollowCount);
                        _context.AddParameter(cmd, "ParentId", data.ParentId);
                        _context.AddParameter(cmd, "RelationBoard", data.RelationBoard);
                        returnValue = await _context.ExecuteNonQueryAsync(cmd) > 0;
                    }
                    catch (Exception ex)
                    {
                        if (!isQueue) Logger.Sensitive(ex, ex.Message);
                    }
                    finally
                    {
                        _context.Close();
                    }
                }
            }
            catch (Exception ex)
            {
                if (!isQueue) Logger.Sensitive(ex, ex.Message);
            }
            return returnValue;
        }
        public async Task<bool> UpdateMediaAsync(IEnumerable<long> listAdd, IEnumerable<long> listDelete, Board board, bool isQueue = false)
        {
            var returnValue = false;
            try
            {
                using(var _context = GetContext())
                {
                    try
                    {
                        var cmd = _context.CreateCommand(Scripts.Board.UpdateMediaAsync);
                        _context.AddParameter(cmd, "Id", board.Id);
                        _context.AddParameter(cmd, "LastInsertedDate", board.LastInsertedDate);
                        _context.AddParameter(cmd, "NewsIdAdd", string.Join(",", listAdd));
                        _context.AddParameter(cmd, "NewsIdDelete", string.Join(",", listDelete));
                        returnValue = await _context.ExecuteNonQueryAsync(cmd) > 0;
                    }
                    catch (Exception ex)
                    {
                        if (!isQueue) Logger.Sensitive(ex, ex.Message);
                    }
                    finally
                    {
                        _context.Close();
                    }
                }
            }
            catch (Exception ex)
            {
                if (!isQueue) Logger.Sensitive(ex, ex.Message);
            }
            return returnValue;
        }

        public async Task<bool> AddNewsAsync(long boardId, string ids, long officerId, bool isQueue = false)
        {
            var returnValue = false;
            try
            {
                using(var _context = GetContext())
                {
                    try
                    {
                        var cmd = _context.CreateCommand(Scripts.Board.AddNewsToBoard);
                        _context.AddParameter(cmd, "Ids", ids);
                        _context.AddParameter(cmd, "BoardId", boardId);
                        _context.AddParameter(cmd, "OfficerId", officerId);
                        _context.AddParameter(cmd, "PublishedDate", DateTime.SpecifyKind(DateTime.Now, DateTimeKind.Unspecified));
                        _context.AddParameter(cmd, "LastInsertedDate", DateTime.SpecifyKind(DateTime.Now, DateTimeKind.Unspecified));
                        returnValue = await _context.ExecuteNonQueryAsync(cmd) > 0;
                    }
                    catch (Exception ex)
                    {
                        if (!isQueue) Logger.Sensitive(ex, ex.Message);
                    }
                    finally
                    {
                        _context.Close();
                    }
                }
            }
            catch (Exception ex)
            {
                if (!isQueue) Logger.Sensitive(ex, ex.Message);
            }
            return returnValue;
        }
        public async Task<bool> DeleteNewsAsync(long officerId, long boardId, string newsIds, bool isQueue = false)
        {
            var returnValue = false;
            try
            {
                using(var _context = GetContext())
                {
                    try
                    {
                        var cmd = _context.CreateCommand(Scripts.Board.DeleteNewsInBoard);
                        _context.AddParameter(cmd, "BoardId", boardId);
                        _context.AddParameter(cmd, "NewsIds", newsIds);
                        returnValue = await _context.ExecuteNonQueryAsync(cmd) > 0;
                        returnValue = true;
                    }
                    catch (Exception ex)
                    {
                        if (!isQueue) Logger.Sensitive(ex, ex.Message);
                    }
                    finally
                    {
                        _context.Close();
                    }
                }
            }
            catch (Exception ex)
            {
                if (!isQueue) Logger.Sensitive(ex, ex.Message);
            }
            return returnValue;
        }

        public async Task<bool> InsertMediaAsync(long mediaId, List<Board> dataDb, bool isQueue = false)
        {
            var returnValue = false;
            try
            {
                using(var _context = GetContext())
                {
                    try
                    {
                        var cmd = _context.CreateCommand(Scripts.Board.InsertMedia);
                        _context.AddParameter(cmd, "MediaId", mediaId);
                        _context.AddParameter(cmd, "BoardIds", string.Join(",", dataDb?.Select(p => p.Id.ToString())?.ToArray() ?? new string[] { }));
                        _context.AddParameter(cmd, "ModifiedBy", dataDb?.FirstOrDefault()?.ModifiedBy);
                        _context.AddParameter(cmd, "ModifiedDate", dataDb?.FirstOrDefault()?.ModifiedDate);
                        _context.AddParameter(cmd, "PublishedDate", DateTime.Now);
                        _context.AddParameter(cmd, "LastInsertedDate", dataDb?.FirstOrDefault()?.LastInsertedDate);
                        returnValue = await _context.ExecuteNonQueryAsync(cmd as SqlCommand) > 0;
                    }
                    catch (Exception ex)
                    {
                        if (!isQueue) Logger.Sensitive(ex, ex.Message);
                    }
                    finally
                    {
                        _context.Close();
                    }
                }
            }
            catch (Exception ex)
            {
                if (!isQueue) Logger.Sensitive(ex, ex.Message);
            }
            return returnValue;
        }
    }
}
