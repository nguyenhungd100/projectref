﻿using ChannelVN.IMS2.Core.Entities.Media;
using ChannelVN.IMS2.Foundation.Common;
using ChannelVN.IMS2.Foundation.Data.Providers;
using ChannelVN.IMS2.Foundation.Logging;
using System;
using System.Data;
using System.Threading.Tasks;

namespace ChannelVN.IMS2.Core.Models.DataStore.Media
{
    public class TemplateDataStore : CmsMainDataStore
    {
        public async Task<ResultData> AddAsync(Template data)
        {
            var returnValue = new ResultData { ReturnValue = false, Data = "" };
            try
            {
                using(var _context = GetContext())
                {
                    try
                    {
                        var cmd = _context.CreateCommand(Scripts.Template.Add);

                        _context.AddParameter(cmd, "Id", data.Id, ParameterDirection.Output);
                        _context.AddParameter(cmd, "Title", data.Title);
                        _context.AddParameter(cmd, "Avatar", data.Avatar);
                        _context.AddParameter(cmd, "MetaData", data.MetaData);
                        _context.AddParameter(cmd, "Description", data.Description);
                        _context.AddParameter(cmd, "Status", data.Status);
                        _context.AddParameter(cmd, "Priority", data.Priority);
                        _context.AddParameter(cmd, "CategoryId", data.CategoryId);
                        _context.AddParameter(cmd, "CreatedBy", data.CreatedBy);
                        _context.AddParameter(cmd, "CreatedDate", data.CreatedDate);

                        var resValue = await _context.ExecuteNonQueryAsync(cmd) > 0;
                        var id = Utility.ConvertToInt(_context.GetParameterValueFromCommand(cmd, 0));

                        returnValue = new ResultData { ReturnValue = resValue, Data = id.ToString() };
                    }
                    catch(Exception ex)
                    {
                        Logger.Sensitive(ex, ex.Message);
                    }
                    finally
                    {
                        _context.Close();
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Sensitive(ex, ex.Message);
            }
            return returnValue;
        }

        public async Task<bool> UpdateAsync(Template data)
        {
            var returnValue = false;
            try
            {
                using(var _context = GetContext())
                {
                    try
                    {
                        var cmd = _context.CreateCommand(Scripts.Template.Update);

                        _context.AddParameter(cmd, "Id", data.Id);
                        _context.AddParameter(cmd, "Title", data.Title);
                        _context.AddParameter(cmd, "Avatar", data.Avatar);
                        _context.AddParameter(cmd, "MetaData", data.MetaData);
                        _context.AddParameter(cmd, "Description", data.Description);
                        _context.AddParameter(cmd, "Priority", data.Priority);
                        _context.AddParameter(cmd, "CategoryId", data.CategoryId);
                        _context.AddParameter(cmd, "ModifiedBy", data.ModifiedBy);
                        _context.AddParameter(cmd, "ModifiedDate", data.ModifiedDate);

                        returnValue = await _context.ExecuteNonQueryAsync(cmd) > 0;
                    }
                    catch (Exception ex)
                    {
                        Logger.Sensitive(ex, ex.Message);
                    }
                    finally
                    {
                        _context.Close();
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Sensitive(ex, ex.Message);
            }
            return returnValue;
        }

        public async Task<bool> UpdateStatusAsync(Template data, bool isQueue = false)
        {
            var returnValue = false;
            try
            {
                using(var _context = GetContext())
                {
                    try
                    {
                        var cmd = _context.CreateCommand(Scripts.Template.UpdateStatus);

                        _context.AddParameter(cmd, "Id", data.Id);
                        _context.AddParameter(cmd, "Status", data.Status);
                        _context.AddParameter(cmd, "ModifiedBy", data.ModifiedBy);
                        _context.AddParameter(cmd, "ModifiedDate", data.ModifiedDate);

                        returnValue = await _context.ExecuteNonQueryAsync(cmd) > 0;
                    }
                    catch (Exception ex)
                    {
                        Logger.Sensitive(ex, ex.Message);
                    }
                    finally
                    {
                        _context.Close();
                    }
                }
            }
            catch (Exception ex)
            {
                if (!isQueue) Logger.Sensitive(ex, ex.Message);
            }
            return returnValue;
        }

        public async Task<Template> GetByIdAsync(long id)
        {
            var returnValue = default(Template);
            try
            {
                using(var _context = GetContext())
                {
                    try
                    {
                        var cmd = _context.CreateCommand(Scripts.Template.GetById);
                        _context.AddParameter(cmd, "Id", id);

                        returnValue = await _context.GetAsync<Template>(cmd);
                    }
                    catch (Exception ex)
                    {
                        Logger.Sensitive(ex, ex.Message);
                    }
                    finally
                    {
                        _context.Close();
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Sensitive(ex, ex.Message);
            }
            return returnValue;
        }
    }
}
