﻿using ChannelVN.IMS2.Foundation.Common;
using ChannelVN.IMS2.Foundation.Data.Providers;
using ChannelVN.IMS2.Foundation.Logging;
using System;
using System.Threading;
using System.Threading.Tasks;
using System.Transactions;

namespace ChannelVN.IMS2.Core.Models.DataStore
{
    public class CategoryDataStore : CmsMainDataStore
    {
        private MssqlContext _context = null;
        public CategoryDataStore()
        {
            _context = GetContext();
        }
        public async Task<bool> AddAsync()
        {
            var returnValue = false;
            try
            {
                var id = Generator.CategoryId();
                using (var _context = GetContext())
                {
                    try
                    {
                        var cmd = _context.CreateCommand(Scripts.Category.Add);
                        
                        _context.AddParameter(cmd, "Id", id);
                        _context.AddParameter(cmd, "Name", "Name-" + id);
                        _context.AddParameter(cmd, "Description", "des");
                        _context.AddParameter(cmd, "Status", 1);
                        _context.AddParameter(cmd, "Priority", 1);
                        _context.AddParameter(cmd, "OfficerId", 0);
                        _context.AddParameter(cmd, "CreatedDate", DateTime.Now);
                        _context.AddParameter(cmd, "CreatedBy", "Aloso");

                        returnValue = await _context.ExecuteNonQueryAsync(cmd) > 0;
                    }
                    catch (Exception ex)
                    {
                        Logger.Error(ex, ex.Message);
                    }
                    finally
                    {
                        _context.Close();
                    }
                }

                //var cmd = context.CreateCommand(Scripts.Category.Add);
                //context.AddParameter(cmd, "Id", id);
                //context.AddParameter(cmd, "Name", "Name-" + id);
                //context.AddParameter(cmd, "Description", "des");
                //context.AddParameter(cmd, "Status", 1);
                //context.AddParameter(cmd, "Priority", 1);
                //context.AddParameter(cmd, "OfficerId", 0);
                //context.AddParameter(cmd, "CreatedDate", DateTime.Now);
                //context.AddParameter(cmd, "CreatedBy", "Aloso");

                //returnValue = await context.ExecuteNonQueryAsync(cmd) > 0;

                //using (TransactionScope scope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
                //{
                //    var trans = Transaction.Current.DependentClone(DependentCloneOption.BlockCommitUntilComplete);
                //    var trans2 = _context.BeginTransaction(trans.IsolationLevel.ToString());
                //    var cmd = _context.CreateCommand(Scripts.Category.Add);

                //    context.AddParameter(cmd, "Id", id);
                //    context.AddParameter(cmd, "Name", "Name-" + id);
                //    context.AddParameter(cmd, "Description", "des");
                //    context.AddParameter(cmd, "Status", 1);
                //    context.AddParameter(cmd, "Priority", 1);
                //    context.AddParameter(cmd, "OfficerId", 0);
                //    context.AddParameter(cmd, "CreatedDate", DateTime.Now);
                //    context.AddParameter(cmd, "CreatedBy", "Aloso");

                //    returnValue = await context.ExecuteNonQueryAsync(cmd) > 0;

                //    if (returnValue) tran.Complete();
                //    tran.Dispose();

                //    scope.Complete();
                //    trans.Dispose();
                //}
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
            }

            //try
            //{
            //    using (TransactionScope scope = new TransactionScope(TransactionScopeAsyncFlowOption.Suppress))
            //    {
            //        // Perform transactional work here.

            //        //Queue work item
            //        //ThreadPool.QueueUserWorkItem(new WaitCallback(WorkerThread), Transaction.Current.DependentClone(DependentCloneOption.BlockCommitUntilComplete));
            //        var dTx = Transaction.Current.DependentClone(DependentCloneOption.BlockCommitUntilComplete);

            //        //Display transaction information
            //        Console.WriteLine("Transaction information:");
            //        Console.WriteLine("ID:             {0}", Transaction.Current.TransactionInformation.LocalIdentifier);
            //        Console.WriteLine("status:         {0}", Transaction.Current.TransactionInformation.Status);
            //        Console.WriteLine("isolationlevel: {0}", Transaction.Current.IsolationLevel);


            //        using (TransactionScope ts = new TransactionScope(dTx, TransactionScopeAsyncFlowOption.Suppress))
            //        {
            //            //Perform transactional work here.
            //            var id = Generator.CategoryId();
            //            var context = GetContext();
            //            var cmd = context.CreateCommand(Scripts.Category.Add);

            //            context.AddParameter(cmd, "Id", id);
            //            context.AddParameter(cmd, "Name", "Name-" + id);
            //            context.AddParameter(cmd, "Description", "des");
            //            context.AddParameter(cmd, "Status", 1);
            //            context.AddParameter(cmd, "Priority", 1);
            //            context.AddParameter(cmd, "OfficerId", 0);
            //            context.AddParameter(cmd, "CreatedDate", DateTime.Now);
            //            context.AddParameter(cmd, "CreatedBy", "Aloso");

            //            returnValue = await context.ExecuteNonQueryAsync(cmd) > 0;
            //            //Call complete on the transaction scope
            //            ts.Complete();
            //        }
            //        dTx.Complete();
                    

            //        //Call Complete on the TransactionScope based on console input
            //        ConsoleKeyInfo c;
            //        while (true)
            //        {
            //            Console.Write("Complete the transaction scope? [Y|N] ");
            //            c = Console.ReadKey();
            //            Console.WriteLine();

            //            if ((c.KeyChar == 'Y') || (c.KeyChar == 'y'))
            //            {
            //                //Call complete on the scope
            //                scope.Complete();
            //                break;
            //            }
            //            else if ((c.KeyChar == 'N') || (c.KeyChar == 'n'))
            //            {
            //                break;
            //            }
            //        }

            //    }
            //}
            //catch (System.Transactions.TransactionException ex)
            //{
            //    Console.WriteLine(ex);
            //}
            //catch
            //{
            //    Console.WriteLine("Cannot complete transaction");
            //    throw;
            //}
            return returnValue;
        }

        private async Task<bool> WorkerThread(object transaction)
        {
            var returnValue = false;
            //try
            //{
            //    //Create a DependentTransaction from the object passed to the WorkerThread
            //    DependentTransaction dTx = (DependentTransaction)transaction;

            //    //Sleep for 1 second to force the worker thread to delay
            //    //Thread.Sleep(1000);

            //    //Pass the DependentTransaction to the scope, so that work done in the scope becomes part of the transaction passed to the worker thread
            //    using (TransactionScope ts = new TransactionScope(dTx))
            //    {
            //        //Perform transactional work here.
            //        var id = Generator.CategoryId();
            //        var context = GetContext();
            //        var cmd = context.CreateCommand(Scripts.Category.Add);

            //        context.AddParameter(cmd, "Id", id);
            //        context.AddParameter(cmd, "Name", "Name-" + id);
            //        context.AddParameter(cmd, "Description", "des");
            //        context.AddParameter(cmd, "Status", 1);
            //        context.AddParameter(cmd, "Priority", 1);
            //        context.AddParameter(cmd, "OfficerId", 0);
            //        context.AddParameter(cmd, "CreatedDate", DateTime.Now);
            //        context.AddParameter(cmd, "CreatedBy", "Aloso");

            //        returnValue = await context.ExecuteNonQueryAsync(cmd) > 0;
            //        //Call complete on the transaction scope
            //        ts.Complete();
            //    }

            //    //Call complete on the dependent transaction
            //    dTx.Complete();
            //}
            //catch(Exception ex)
            //{
            //    Logger.Error(ex, ex.Message);
            //}
            
            return returnValue;
        }
    }
}
