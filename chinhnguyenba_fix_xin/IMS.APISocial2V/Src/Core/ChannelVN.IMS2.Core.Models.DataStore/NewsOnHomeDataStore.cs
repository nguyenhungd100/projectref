﻿using ChannelVN.IMS2.Core.Entities;
using ChannelVN.IMS2.Core.Entities.NewsOnHome;
using ChannelVN.IMS2.Foundation.Data.Providers;
using ChannelVN.IMS2.Foundation.Logging;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Threading.Tasks;

namespace ChannelVN.IMS2.Core.Models.DataStore
{
    public class NewsOnHomeDataStore : CmsMainDataStore
    {
        private MssqlContext _context = null;

        public NewsOnHomeDataStore() => _context = GetContext();

        public async Task<bool> AddAsync(NewsOnHome data, bool isQueue = false)
        {
            var returnValue = false;
            try
            {
                using(var _context = GetContext())
                {
                    try
                    {
                        var cmd = _context.CreateCommand(Scripts.NewsOnHome.Add);
                        _context.AddParameter(cmd, "NewsId", data.NewsId);
                        _context.AddParameter(cmd, "OfficerId", data.OfficerId);
                        _context.AddParameter(cmd, "OfficerClass", data.OfficerClass);
                        _context.AddParameter(cmd, "Type", data.Type);
                        _context.AddParameter(cmd, "Status", data.Status);
                        _context.AddParameter(cmd, "OrderedDate", data.OrderedDate);                        

                        returnValue = await _context.ExecuteNonQueryAsync(cmd as SqlCommand) > 0;                        
                    }
                    catch(Exception ex)
                    {                        
                        if (!isQueue) Logger.Sensitive(ex, ex.Message);
                    }
                    finally
                    {
                        _context.Close();
                    }
                }
            }
            catch (Exception ex)
            {                
                if (!isQueue) Logger.Sensitive(ex, ex.Message);
            }
            return returnValue;
        }
        public async Task<bool> UpdateAsync(NewsOnHome data, bool isQueue = false)
        {
            var returnValue = false;
            try
            {
                using(var _context = GetContext())
                {
                    try
                    {
                        var cmd = _context.CreateCommand(Scripts.NewsOnHome.Update);
                        _context.AddParameter(cmd, "NewsId", data.NewsId);
                        _context.AddParameter(cmd, "Status", data.Status);
                        _context.AddParameter(cmd, "AcceptedBy", data.AcceptedBy);
                        _context.AddParameter(cmd, "AcceptedDate", data.AcceptedDate);
                        _context.AddParameter(cmd, "CanceledBy", data.CanceledBy);
                        _context.AddParameter(cmd, "CanceledDate", data.CanceledDate);

                        returnValue = await _context.ExecuteNonQueryAsync(cmd as SqlCommand) > 0;
                    }
                    catch (Exception ex)
                    {
                        if (!isQueue) Logger.Sensitive(ex, ex.Message);
                    }
                    finally
                    {
                        _context.Close();
                    }
                }
            }
            catch (Exception ex)
            {
                if (!isQueue) Logger.Sensitive(ex, ex.Message);
            }
            return returnValue;
        }                
    }
}
