﻿using ChannelVN.IMS2.Core.Entities;
using ChannelVN.IMS2.Core.Entities.Videos;
using ChannelVN.IMS2.Foundation.Data.Providers;
using ChannelVN.IMS2.Foundation.Logging;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace ChannelVN.IMS2.Core.Models.DataStore.Videos
{
    public class VideoPlaylistDataStore : CmsMainDataStore
    {
        public async Task<bool> AddAsync(VideoPlaylistSearch data, bool isQueue = false)
        {
            var returnValue = false;
            try
            {
                using(var _context = GetContext())
                {
                    try
                    {
                        var cmd = _context.CreateCommand(Scripts.VideoPlaylist.Add);
                        _context.AddParameter(cmd, "Id", data.Id);
                        _context.AddParameter(cmd, "Type", data.Type);
                        _context.AddParameter(cmd, "Url", data.Url);
                        _context.AddParameter(cmd, "OriginalUrl", data.OriginalUrl);
                        _context.AddParameter(cmd, "Status", data.Status);
                        _context.AddParameter(cmd, "Author", data.Author);
                        _context.AddParameter(cmd, "CategoryId", data.CategoryId);
                        _context.AddParameter(cmd, "CreatedDate", data.CreatedDate);
                        _context.AddParameter(cmd, "CreatedBy", data.CreatedBy);
                        _context.AddParameter(cmd, "Location", data.Location);
                        _context.AddParameter(cmd, "PublishMode", data.PublishMode);
                        _context.AddParameter(cmd, "PublishData", data.PublishData);
                        _context.AddParameter(cmd, "DistributionDate", data.DistributionDate);
                        _context.AddParameter(cmd, "DistributorId", data.DistributorId);
                        _context.AddParameter(cmd, "Tags", data.Tags);
                        _context.AddParameter(cmd, "CardType", data.CardType);
                        _context.AddParameter(cmd, "Name", data.Name);
                        _context.AddParameter(cmd, "UnsignName", data.UnsignName);
                        _context.AddParameter(cmd, "Description", data.Description);
                        _context.AddParameter(cmd, "LastInsertedDate", data.LastInsertedDate);
                        _context.AddParameter(cmd, "Cover", data.Cover);
                        _context.AddParameter(cmd, "IntroClip", data.IntroClip);
                        _context.AddParameter(cmd, "PlaylistRelation", data.PlaylistRelation);
                        _context.AddParameter(cmd, "MetaAvatar", data.MetaAvatar);
                        _context.AddParameter(cmd, "MetaData", data.MetaData);
                        _context.AddParameter(cmd, "ApprovedBy", data.ApprovedBy);
                        _context.AddParameter(cmd, "CommentMode", (Byte?)data.CommentMode);

                        //VideoInPlaylist
                        _context.AddParameter(cmd, "VideoInPlaylist", string.Join(",", data.VideoInPlaylist?.Select(p => p.VideoId + "_" + p.Priority ?? 0 + "_" + (p.PlayOnTime ?? DateTime.Now).ToString("yyyy-MM-dd HH:mm:ss.fff")) ?? new string[] { }));

                        _context.AddParameter(cmd, "AccountId", data.NewsInAccount?[data.NewsInAccount.Length - 1]?.AccountId);
                        _context.AddParameter(cmd, "PublishedDate", data.NewsInAccount?[data.NewsInAccount.Length - 1]?.PublishedDate);
                        _context.AddParameter(cmd, "PublishedType", data.NewsInAccount?[data.NewsInAccount.Length - 1]?.PublishedType);
                        returnValue = await _context.ExecuteNonQueryAsync(cmd as SqlCommand) > 0;
                    }
                    catch (Exception ex)
                    {
                        if (!isQueue) Logger.Sensitive(ex, ex.Message);
                    }
                    finally
                    {
                        _context.Close();
                    }
                }
            }
            catch (Exception ex)
            {
                if (!isQueue) Logger.Sensitive(ex, ex.Message);
            }
            return returnValue;
        }

        public async Task<bool> UpdateAsync(VideoPlaylistSearch data, bool isQueue = false)
        {
            var returnValue = false;
            try
            {
                using(var _context = GetContext())
                {
                    try
                    {
                        var cmd = _context.CreateCommand(Scripts.VideoPlaylist.Update);
                        _context.AddParameter(cmd, "Id", data.Id);
                        _context.AddParameter(cmd, "Url", data.Url);
                        _context.AddParameter(cmd, "OriginalUrl", data.OriginalUrl);
                        _context.AddParameter(cmd, "Author", data.Author);
                        _context.AddParameter(cmd, "CategoryId", data.CategoryId);
                        _context.AddParameter(cmd, "Location", data.Location);
                        _context.AddParameter(cmd, "PublishMode", data.PublishMode);
                        _context.AddParameter(cmd, "PublishData", data.PublishData);
                        //_context.AddParameter(cmd, "DistributionDate", data.DistributionDate);
                        _context.AddParameter(cmd, "Tags", data.Tags);
                        _context.AddParameter(cmd, "Name", data.Name);
                        _context.AddParameter(cmd, "UnsignName", data.UnsignName);
                        _context.AddParameter(cmd, "Description", data.Description);
                        //base News                    
                        _context.AddParameter(cmd, "LastInsertedDate", data.LastInsertedDate);
                        _context.AddParameter(cmd, "Cover", data.Cover);
                        _context.AddParameter(cmd, "IntroClip", data.IntroClip);
                        _context.AddParameter(cmd, "PlaylistRelation", data.PlaylistRelation);
                        _context.AddParameter(cmd, "MetaAvatar", data.MetaAvatar);
                        _context.AddParameter(cmd, "MetaData", data.MetaData);
                        _context.AddParameter(cmd, "ApprovedBy", data.ApprovedBy);
                        _context.AddParameter(cmd, "ApprovedDate", data.ApprovedDate);
                        _context.AddParameter(cmd, "CommentMode", (Byte?)data.CommentMode);

                        _context.AddParameter(cmd, "VideoInPlaylist", string.Join(",", data.VideoInPlaylist?.Select(p => p.VideoId + "_" + p.Priority ?? 0 + "_" + (p.PlayOnTime ?? DateTime.Now).ToString("yyyy-MM-dd HH:mm:ss.fff")) ?? new string[] { }));

                        returnValue = await _context.ExecuteNonQueryAsync(cmd) > 0;
                    }
                    catch (Exception ex)
                    {
                        if (!isQueue) Logger.Sensitive(ex, ex.Message);
                    }
                    finally
                    {
                        _context.Close();
                    }
                }
            }
            catch (Exception ex)
            {
                if (!isQueue) Logger.Sensitive(ex, ex.Message);
            }
            return returnValue;
        }

        public async Task<List<VideoInPlaylist>> GetVideoInPlaylistAsync(long id)
        {
            var returnValue = default(List<VideoInPlaylist>);
            try
            {
                using(var _context = GetContext())
                {
                    try
                    {
                        var cmd = _context.CreateCommand(Scripts.VideoPlaylist.GetVideoInPlaylist);
                        _context.AddParameter(cmd, "PlaylistId", id);
                        returnValue = await _context.GetListAsync<VideoInPlaylist>(cmd);
                    }
                    catch(Exception ex)
                    {
                        Logger.Sensitive(ex, ex.Message);
                    }
                    finally
                    {
                        _context.Close();
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Sensitive(ex, ex.Message);
            }
            return returnValue;
        }

        public async Task<ObjectRelation> GetPlaylistRelationAsync(long id)
        {
            var returnValue = new ObjectRelation();
            try
            {
                using(var _context = GetContext())
                {
                    try
                    {
                        var cmd = _context.CreateCommand(Scripts.VideoPlaylist.GetVideoPlaylistRelation);
                        _context.AddParameter(cmd, "Id", id);
                        returnValue = await _context.GetAsync<ObjectRelation>(cmd);
                    }
                    catch (Exception ex)
                    {
                        Logger.Sensitive(ex, ex.Message);
                    }
                    finally
                    {
                        _context.Close();
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Sensitive(ex, ex.Message);
            }
            return returnValue;
        }

        public async Task<List<VideoPlaylistSearch>> InitAllVideoPlaylistAsync(int page, int pageSize, DateTime startDate, DateTime endDate)
        {
            var returnValue = default(List<VideoPlaylistSearch>);
            try
            {
                using(var _context = GetContext())
                {
                    try
                    {
                        var cmd = _context.CreateCommand(Scripts.VideoPlaylist.InitAllVideoPlaylist);
                        _context.AddParameter(cmd, "DateFrom", startDate);
                        _context.AddParameter(cmd, "DateTo", endDate);
                        _context.AddParameter(cmd, "PageIndex", page);
                        _context.AddParameter(cmd, "PageSize", pageSize);
                        returnValue = await _context.GetListAsync<VideoPlaylistSearch>(cmd);
                    }
                    catch (Exception ex)
                    {
                        Logger.Sensitive(ex, ex.Message);
                    }
                    finally
                    {
                        _context.Close();
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Sensitive(ex, ex.Message);
            }
            return returnValue;
        }

        public async Task<int> CountVideoPlaylistAsync()
        {
            var returnValue = new TotalCount();
            try
            {
                using(var _context = GetContext())
                {
                    try
                    {
                        var cmd = _context.CreateCommand(Scripts.VideoPlaylist.CountVideoPlaylist);
                        returnValue = await _context.GetAsync<TotalCount>(cmd);
                    }
                    catch (Exception ex)
                    {
                        Logger.Sensitive(ex, ex.Message);
                    }
                    finally
                    {
                        _context.Close();
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Sensitive(ex, ex.Message);
            }
            return returnValue.Total;
        }

        public async Task<bool> UpdateStatusAsync(VideoPlaylistSearch data, bool isQueue = false)
        {
            var returnValue = false;
            try
            {
                using(var _context = GetContext())
                {
                    try
                    {
                        var cmd = _context.CreateCommand(Scripts.VideoPlaylist.UpdateStatus);
                        _context.AddParameter(cmd, "Id", data.Id);
                        _context.AddParameter(cmd, "Status", data.Status);
                        _context.AddParameter(cmd, "ModifiedBy", data.ModifiedBy);
                        _context.AddParameter(cmd, "ModifiedDate", data.ModifiedDate);
                        _context.AddParameter(cmd, "DistributionDate", data.DistributionDate);
                        _context.AddParameter(cmd, "ApprovedBy", data.ApprovedBy);
                        _context.AddParameter(cmd, "ApprovedDate", data.ApprovedDate);
                        returnValue = await _context.ExecuteNonQueryAsync(cmd) > 0;
                    }
                    catch (Exception ex)
                    {
                        if (!isQueue) Logger.Sensitive(ex, ex.Message);
                    }
                    finally
                    {
                        _context.Close();
                    }
                }
            }
            catch (Exception ex)
            {
                if (!isQueue) Logger.Sensitive(ex, ex.Message);
            }
            return returnValue;
        }

        public async Task<bool> AddVideoAsync(long videoId, List<VideoPlaylist> dataDb, bool isQueue = false)
        {
            var returnValue = false;
            try
            {
                using(var _context = GetContext())
                {
                    try
                    {
                        var cmd = _context.CreateCommand(Scripts.VideoPlaylist.AddVideo);
                        _context.AddParameter(cmd, "VideoId", videoId);
                        _context.AddParameter(cmd, "PlaylistIds", string.Join(",", dataDb?.Select(p => p.Id.ToString())?.ToArray() ?? new string[] { }));
                        _context.AddParameter(cmd, "ModifiedBy", dataDb?.FirstOrDefault()?.ModifiedBy);
                        _context.AddParameter(cmd, "ModifiedDate", dataDb?.FirstOrDefault()?.ModifiedDate);
                        _context.AddParameter(cmd, "PublishedDate", DateTime.Now);
                        _context.AddParameter(cmd, "LastInsertedDate", dataDb?.FirstOrDefault()?.LastInsertedDate);
                        returnValue = await _context.ExecuteNonQueryAsync(cmd) > 0;
                    }
                    catch (Exception ex)
                    {
                        if (!isQueue) Logger.Sensitive(ex, ex.Message);
                    }
                    finally
                    {
                        _context.Close();
                    }
                }
            }
            catch (Exception ex)
            {
                if (!isQueue) Logger.Sensitive(ex, ex.Message);
            }
            return returnValue;
        }
    }
}
