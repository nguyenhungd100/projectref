﻿using ChannelVN.IMS2.Core.Entities;
using ChannelVN.IMS2.Core.Entities.Videos;
using ChannelVN.IMS2.Foundation.Data.Providers;
using ChannelVN.IMS2.Foundation.Logging;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Threading.Tasks;

namespace ChannelVN.IMS2.Core.Models.DataStore.Videos
{
    public class VideoDataStore : CmsMainDataStore
    {
        public async Task<bool> AddAsync(VideoSearch data, bool isQueue = false)
        {
            var returnValue = false;
            try
            {
                using(var _context = GetContext())
                {
                    try
                    {
                        var cmd = _context.CreateCommand(Scripts.Video.Add);
                        _context.AddParameter(cmd, "Id", data.Id);
                        _context.AddParameter(cmd, "Type", data.Type);
                        _context.AddParameter(cmd, "Url", data.Url);
                        _context.AddParameter(cmd, "OriginalUrl", data.OriginalUrl);
                        _context.AddParameter(cmd, "Status", data.Status);
                        _context.AddParameter(cmd, "Author", data.Author);
                        _context.AddParameter(cmd, "CategoryId", data.CategoryId);
                        _context.AddParameter(cmd, "CreatedDate", data.CreatedDate);
                        _context.AddParameter(cmd, "CreatedBy", data.CreatedBy);
                        _context.AddParameter(cmd, "Location", data.Location);
                        _context.AddParameter(cmd, "PublishMode", data.PublishMode);
                        _context.AddParameter(cmd, "PublishData", data.PublishData);
                        _context.AddParameter(cmd, "DistributionDate", data.DistributionDate);
                        _context.AddParameter(cmd, "DistributorId", data.DistributorId);
                        _context.AddParameter(cmd, "Tags", data.Tags);
                        _context.AddParameter(cmd, "CardType", data.CardType);
                        _context.AddParameter(cmd, "Name", data.Name);
                        _context.AddParameter(cmd, "UnsignName", data.UnsignName);
                        _context.AddParameter(cmd, "Description", data.Description);
                        _context.AddParameter(cmd, "HtmlCode", data.HtmlCode);
                        _context.AddParameter(cmd, "KeyVideo", data.KeyVideo);
                        _context.AddParameter(cmd, "UploadedDate", data.UploadedDate);
                        _context.AddParameter(cmd, "Source", data.Source);
                        _context.AddParameter(cmd, "VideoRelation", data.VideoRelation);
                        _context.AddParameter(cmd, "FileName", data.FileName);
                        _context.AddParameter(cmd, "Duration", data.Duration);
                        _context.AddParameter(cmd, "Size", data.Size);
                        _context.AddParameter(cmd, "Capacity", data.Capacity);
                        _context.AddParameter(cmd, "AllowAdv", data.AllowAdv);
                        _context.AddParameter(cmd, "IsRemovedLogo", data.IsRemovedLogo);
                        _context.AddParameter(cmd, "IsConverted", data.IsConverted);
                        _context.AddParameter(cmd, "PolicyContentId", data.PolicyContentId);
                        _context.AddParameter(cmd, "OriginalId", data.OriginalId);
                        _context.AddParameter(cmd, "Trailer", data.Trailer);
                        _context.AddParameter(cmd, "MetaAvatar", data.MetaAvatar);
                        _context.AddParameter(cmd, "DisplayStyle", data.DisplayStyle);
                        _context.AddParameter(cmd, "MetaData", data.MetaData);
                        _context.AddParameter(cmd, "ApprovedBy", data.ApprovedBy);
                        _context.AddParameter(cmd, "CommentMode", (Byte?)data.CommentMode);
                        //NewsInAccount
                        _context.AddParameter(cmd, "AccountId", data.NewsInAccount[data.NewsInAccount.Length - 1].AccountId);
                        _context.AddParameter(cmd, "PublishedType", data.NewsInAccount[data.NewsInAccount.Length - 1].PublishedType);
                        _context.AddParameter(cmd, "PublishedDate", data.NewsInAccount[data.NewsInAccount.Length - 1].PublishedDate);
                        returnValue = await _context.ExecuteNonQueryAsync(cmd as SqlCommand) > 0;
                    }
                    catch(Exception ex)
                    {
                        if (!isQueue) Logger.Sensitive(ex, ex.Message);
                    }
                    finally
                    {
                        _context.Close();
                    }
                }
            }
            catch (Exception ex)
            {
                if (!isQueue) Logger.Sensitive(ex, ex.Message);
            }
            return returnValue;
        }

        public async Task<int> CountVideoAsync()
        {
            var returnValue = new TotalCount();
            try
            {
                using(var _context = GetContext())
                {
                    try
                    {
                        var cmd = _context.CreateCommand(Scripts.Video.CountVideo);
                        returnValue = await _context.GetAsync<TotalCount>(cmd);
                    }
                    catch (Exception ex)
                    {
                        Logger.Sensitive(ex, ex.Message);
                    }
                    finally
                    {
                        _context.Close();
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Sensitive(ex, ex.Message);
            }
            return returnValue.Total;
        }

        public async Task<ObjectRelation> GetVideoRelationAsync(long videoId)
        {
            var returnValue = new ObjectRelation();
            try
            {
                using(var _context = GetContext())
                {
                    try
                    {
                        var cmd = _context.CreateCommand(Scripts.Video.GetVideoRelation);
                        _context.AddParameter(cmd, "Id", videoId);
                        returnValue = await _context.GetAsync<ObjectRelation>(cmd);
                    }
                    catch (Exception ex)
                    {
                        Logger.Sensitive(ex, ex.Message);
                    }
                    finally
                    {
                        _context.Close();
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Sensitive(ex, ex.Message);
            }
            return returnValue;
        }

        public async Task<bool> DeleteDataAsync(long id)
        {
            var returnValue = false;
            try
            {
                using(var _context = GetContext())
                {
                    try
                    {
                        var cmd = _context.CreateCommand(Scripts.Video.DeleteVideoById);
                        _context.AddParameter(cmd, "VideoId", id);
                        returnValue = await _context.ExecuteNonQueryAsync(cmd) > 0;
                    }
                    catch (Exception ex)
                    {
                        Logger.Sensitive(ex, ex.Message);
                    }
                    finally
                    {
                        _context.Close();
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Sensitive(ex, ex.Message);
            }
            return returnValue;
        }

        public async Task<List<VideoSearch>> InitAllVideoAsync(int page, int pageSize, DateTime startDate, DateTime endDate)
        {
            var returnValue = default(List<VideoSearch>);
            try
            {
                using(var _context = GetContext())
                {
                    try
                    {
                        var cmd = _context.CreateCommand(Scripts.Video.InitAllVideo);
                        _context.AddParameter(cmd, "DateFrom", startDate);
                        _context.AddParameter(cmd, "DateTo", endDate);
                        _context.AddParameter(cmd, "PageIndex", page);
                        _context.AddParameter(cmd, "PageSize", pageSize);
                        returnValue = await _context.GetListAsync<VideoSearch>(cmd);
                    }
                    catch (Exception ex)
                    {
                        Logger.Sensitive(ex, ex.Message);
                    }
                    finally
                    {
                        _context.Close();
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Sensitive(ex, ex.Message);
            }
            return returnValue;
        }

        public async Task<bool> DeleteVideoInTagAsync(long id)
        {
            bool returnValue = false;
            try
            {
                using(var _context = GetContext())
                {
                    try
                    {
                        var cmd = _context.CreateCommand(Scripts.Video.DeleteVideoInTag);
                        _context.AddParameter(cmd, "VideoId", id);
                        returnValue = await _context.ExecuteNonQueryAsync(cmd) > 0;
                        returnValue = true;
                    }
                    catch (Exception ex)
                    {
                        Logger.Sensitive(ex, ex.Message);
                    }
                    finally
                    {
                        _context.Close();
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Sensitive(ex, ex.Message);
                returnValue = false;
            }
            return returnValue;
        }

        public async Task<bool> UpdateStatusAsync(VideoSearch data, bool isQueue = false)
        {
            var returnValue = false;
            try
            {
                using(var _context = GetContext())
                {
                    try
                    {
                        var cmd = _context.CreateCommand(Scripts.Video.UpdateStatus);
                        _context.AddParameter(cmd, "Id", data.Id);
                        _context.AddParameter(cmd, "Status", data.Status);
                        _context.AddParameter(cmd, "ModifiedDate", data.ModifiedDate);
                        _context.AddParameter(cmd, "ModifiedBy", data.ModifiedBy);
                        _context.AddParameter(cmd, "DistributionDate", data.DistributionDate);
                        _context.AddParameter(cmd, "ApprovedBy", data.ApprovedBy);
                        _context.AddParameter(cmd, "ApprovedDate", data.ApprovedDate);
                        returnValue = await _context.ExecuteNonQueryAsync(cmd) > 0;
                    }
                    catch (Exception ex)
                    {
                        Logger.Sensitive(ex, ex.Message);
                    }
                    finally
                    {
                        _context.Close();
                    }
                }
            }
            catch (Exception ex)
            {
                if (!isQueue) Logger.Sensitive(ex, ex.Message);
            }
            return returnValue;
        }

        public async Task<bool> UpdateAsync(VideoSearch data, bool isQueue = false)
        {
            var returnValue = false;
            try
            {
                using(var _context = GetContext())
                {
                    try
                    {
                        var cmd = _context.CreateCommand(Scripts.Video.Update);

                        _context.AddParameter(cmd, "Id", data.Id);
                        _context.AddParameter(cmd, "Url", data.Url);
                        _context.AddParameter(cmd, "OriginalUrl", data.OriginalUrl);
                        _context.AddParameter(cmd, "Author", data.Author);
                        _context.AddParameter(cmd, "CategoryId", data.CategoryId);
                        _context.AddParameter(cmd, "Location", data.Location);
                        _context.AddParameter(cmd, "PublishMode", data.PublishMode);
                        _context.AddParameter(cmd, "PublishData", data.PublishData);
                        _context.AddParameter(cmd, "DistributionDate", data.DistributionDate);
                        _context.AddParameter(cmd, "ModifiedDate", data.ModifiedDate);
                        _context.AddParameter(cmd, "ModifiedBy", data.ModifiedBy);
                        _context.AddParameter(cmd, "Tags", data.Tags);
                        _context.AddParameter(cmd, "Name", data.Name);
                        _context.AddParameter(cmd, "UnsignName", data.UnsignName);
                        _context.AddParameter(cmd, "Description", data.Description);
                        _context.AddParameter(cmd, "HtmlCode", data.HtmlCode);
                        _context.AddParameter(cmd, "KeyVideo", data.KeyVideo);
                        _context.AddParameter(cmd, "UploadedDate", data.UploadedDate);
                        _context.AddParameter(cmd, "Source", data.Source);
                        _context.AddParameter(cmd, "VideoRelation", data.VideoRelation);
                        _context.AddParameter(cmd, "FileName", data.FileName);
                        _context.AddParameter(cmd, "Duration", data.Duration);
                        _context.AddParameter(cmd, "Size", data.Size);
                        _context.AddParameter(cmd, "Capacity", data.Capacity);
                        _context.AddParameter(cmd, "AllowAdv", data.AllowAdv);
                        _context.AddParameter(cmd, "IsRemovedLogo", data.IsRemovedLogo);
                        _context.AddParameter(cmd, "IsConverted", data.IsConverted);
                        _context.AddParameter(cmd, "PolicyContentId", data.PolicyContentId);
                        _context.AddParameter(cmd, "OriginalId", data.OriginalId);
                        _context.AddParameter(cmd, "Trailer", data.Trailer);
                        _context.AddParameter(cmd, "MetaAvatar", data.MetaAvatar);
                        _context.AddParameter(cmd, "DisplayStyle", data.DisplayStyle);
                        _context.AddParameter(cmd, "MetaData", data.MetaData);
                        _context.AddParameter(cmd, "ApprovedBy", data.ApprovedBy);
                        _context.AddParameter(cmd, "ApprovedDate", data.ApprovedDate);
                        _context.AddParameter(cmd, "CommentMode", (Byte?)data.CommentMode);

                        returnValue = await _context.ExecuteNonQueryAsync(cmd) > 0;
                    }
                    catch (Exception ex)
                    {
                        if (!isQueue) Logger.Sensitive(ex, ex.Message);
                    }
                    finally
                    {
                        _context.Close();
                    }
                }
            }
            catch (Exception ex)
            {
                if (!isQueue) Logger.Sensitive(ex, ex.Message);
            }
            return returnValue;
        }


        public async Task<ObjectRelation> GetVideoTagAsync(long id)
        {
            var returnValue = new ObjectRelation();
            try
            {
                using(var _context = GetContext())
                {
                    try
                    {
                        var cmd = _context.CreateCommand(Scripts.Video.GetVideoTag);
                        _context.AddParameter(cmd, "Id", id);
                        returnValue = await _context.GetAsync<ObjectRelation>(cmd);
                    }
                    catch (Exception ex)
                    {
                        Logger.Sensitive(ex, ex.Message);
                    }
                    finally
                    {
                        _context.Close();
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Sensitive(ex, ex.Message);
            }
            return returnValue;
        }

        public async Task<bool> PublishAsync(VideoSearch video)
        {
            var returnValue = false;
            try
            {
                using(var _context = GetContext())
                {
                    try
                    {
                        var cmd = _context.CreateCommand(Scripts.Video.Publish);
                        _context.AddParameter(cmd, "Id", video.Id);
                        _context.AddParameter(cmd, "Status", video.Status);
                        _context.AddParameter(cmd, "DistributionDate", video.DistributionDate);
                        returnValue = await _context.ExecuteNonQueryAsync(cmd) > 0;
                    }
                    catch (Exception ex)
                    {
                        Logger.Sensitive(ex, ex.Message);
                    }
                    finally
                    {
                        _context.Close();
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Sensitive(ex, ex.Message);
            }
            return returnValue;
        }
    }
}
