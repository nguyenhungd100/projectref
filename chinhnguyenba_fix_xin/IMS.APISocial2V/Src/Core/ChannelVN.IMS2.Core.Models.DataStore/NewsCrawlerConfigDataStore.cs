﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using ChannelVN.IMS2.Core.Entities;
using ChannelVN.IMS2.Foundation.Logging;
using Serilog;

namespace ChannelVN.IMS2.Core.Models.DataStore
{
    public class NewsCrawlerConfigDataStore : CmsMainDataStore
    {
        public async Task<bool> AddConfigAsync(NewCrawlerConfigurationSearch data, bool isQueue = false)
        {
            var returnValue = false;
            try
            {
                using (var _context = GetContext())
                {
                    try
                    {
                        var cmd = _context.CreateCommand(Scripts.NewsCrawlerConfiguration.Add);

                        _context.AddParameter(cmd, "Id", data.Id);
                        _context.AddParameter(cmd, "OfficerId", data.OfficerId);
                        _context.AddParameter(cmd, "Source", data.Source);
                        _context.AddParameter(cmd, "CreatedBy", data.CreatedBy);
                        _context.AddParameter(cmd, "CreatedDate", data.CreatedDate);
                        _context.AddParameter(cmd, "PublishMode", data.PublishMode);
                        _context.AddParameter(cmd, "IntervalTime", data.IntervalTime);
                        _context.AddParameter(cmd, "Status", data.Status);                 

                        returnValue = await _context.ExecuteNonQueryAsync(cmd) > 0;

                    }
                    catch (Exception ex)
                    {
                        if (!isQueue) Logger.Sensitive(ex, ex.Message);
                    }
                    finally
                    {
                        _context.Close();
                    }
                }
            }
            catch (Exception ex)
            {
                if (!isQueue) Logger.Sensitive(ex, ex.Message);
            }
            return returnValue;
        }

        public async Task<bool> UpdateConfigAsync(NewCrawlerConfigurationSearch data, bool isQueue = false)
        {
            var returnValue = false;
            try
            {
                using (var _context = GetContext())
                {
                    try
                    {
                        var cmd = _context.CreateCommand(Scripts.NewsCrawlerConfiguration.Update);
                        _context.AddParameter(cmd, "Id", data.Id);
                        _context.AddParameter(cmd, "OfficerId", data.OfficerId);
                        _context.AddParameter(cmd, "Source", data.Source);
                        _context.AddParameter(cmd, "CreatedBy", data.CreatedBy);
                        _context.AddParameter(cmd, "CreatedDate", data.CreatedDate);
                        _context.AddParameter(cmd, "PublishMode", data.PublishMode);

                        _context.AddParameter(cmd, "IntervalTime", data.IntervalTime);
                        _context.AddParameter(cmd, "Status", data.Status);
                       
                        returnValue = await _context.ExecuteNonQueryAsync(cmd) > 0;

                    }
                    catch (Exception ex)
                    {
                        if (!isQueue) Logger.Sensitive(ex, ex.Message);
                    }
                    finally
                    {
                        _context.Close();
                    }
                }
            }
            catch (Exception ex)
            {
                if (!isQueue) Logger.Sensitive(ex, ex.Message);
            }
            return returnValue;
        }
    }
}
