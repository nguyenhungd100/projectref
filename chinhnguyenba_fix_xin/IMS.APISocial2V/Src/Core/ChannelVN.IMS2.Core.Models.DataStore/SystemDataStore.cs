﻿using ChannelVN.IMS2.Core.Entities.Security;
using ChannelVN.IMS2.Foundation.Common;
using ChannelVN.IMS2.Foundation.Common.Configuration;
using ChannelVN.IMS2.Foundation.Data.Providers;
using ChannelVN.IMS2.Foundation.Logging;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace ChannelVN.IMS2.Core.Models.DataStore
{
    public class SystemDataStore : CmsMainDataStore
    {
        public async Task<bool> ConvertDataAsync(Account data)
        {
            var returnValue = false;
            try
            {
                using (var _context = GetContext())
                {
                    try
                    {
                        var cmd = _context.CreateCommand(Scripts.System.ConvertData);
                        _context.AddParameter(cmd, "Id", data.Id);
                        _context.AddParameter(cmd, "Mobile", data.Mobile);
                        returnValue = await _context.ExecuteNonQueryAsync(cmd) > 0;
                    }
                    catch (Exception ex)
                    {
                        Logger.Error(ex, ex.Message);
                    }
                    finally
                    {
                        _context.Close();
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
            }
            return returnValue;
        }

        public async Task<bool> InitAll()
        {
            var returnValue = false;
            using (var client = new HttpClient())
            {
                try
                {
                    string apiCache = AppSettings.Current.ChannelConfiguration.ApiInitData + "cache?";
                    string apiSearch = AppSettings.Current.ChannelConfiguration.ApiInitData + "search?";
                    var obj = new
                    {
                        name = "",
                        @namespace = "vccorp",
                        clear_data = 1
                    };
                    var objectKeys = Utility.ConvertToKeyValuePair(obj);

                    var content = new FormUrlEncodedContent(objectKeys);

                    client.Timeout = TimeSpan.FromSeconds(AppSettings.Current.ChannelConfiguration.HttpClientTimeOut);

                    var response = await client.GetAsync(apiCache + content.ReadAsStringAsync());
                    if (!response.IsSuccessStatusCode)
                    {
                        response.EnsureSuccessStatusCode();
                    }
                    if (response?.StatusCode == System.Net.HttpStatusCode.OK)
                    {
                        Logger.Debug("InitAll redis success.");
                        var responseSearch = await client.GetAsync(apiSearch + content.ReadAsStringAsync());
                        if (!responseSearch.IsSuccessStatusCode)
                        {
                            responseSearch.EnsureSuccessStatusCode();
                        }
                        if (responseSearch?.StatusCode == System.Net.HttpStatusCode.OK)
                        {
                            Logger.Debug("InitAll ES success.");
                        }
                        else
                        {
                            Logger.Debug("InitAll ES data.Id unsuccess.");
                        }
                    }
                    else
                    {
                        Logger.Debug("InitAll redis unsuccess.");
                    }
                    returnValue = true;
                }
                catch (Exception ex)
                {
                    Logger.Error(ex, ex.Message);
                }
            }
            return returnValue;
        }

        public async Task<bool> UpdateUserIdAsync(long vietId, long kinghubId)
        {
            var returnValue = false;
            using (var client = new HttpClient())
            {
                try
                {
                    string api = AppSettings.Current.ChannelConfiguration.ApiUpdateUserId + "vietid=" + vietId + "&tokinghubid=" + kinghubId;

                    client.Timeout = TimeSpan.FromSeconds(AppSettings.Current.ChannelConfiguration.HttpClientTimeOut);

                    var response = await client.GetAsync(api);
                    if (!response.IsSuccessStatusCode)
                    {
                        response.EnsureSuccessStatusCode();
                    }
                    if (response?.StatusCode == System.Net.HttpStatusCode.OK)
                    {
                        var result = Json.Parse<string>(await response.Content?.ReadAsStringAsync());
                        if (result != null && result.Trim().Equals("true"))
                        {
                            returnValue = true;
                            Logger.Debug("Update user success.");
                        }
                        Logger.Debug("Update user unsuccess.");
                    }
                    else
                    {
                        Logger.Debug("Update user unsuccess.");
                    }
                }
                catch (Exception ex)
                {
                    Logger.Error(ex, ex.Message);
                }
            }
            return returnValue;
        }

        public async Task<bool> ExecuteQueryAsync(string query)
        {
            var returnValue = false;
            try
            {
                using (var _context = GetContext())
                {
                    try
                    {
                        var cmd = _context.CreateCommand(query);
                        returnValue = await _context.ExecuteNonQueryAsync(cmd) > 0;
                        returnValue = true;
                    }
                    catch (Exception ex)
                    {
                        Logger.Error(ex, ex.Message);
                    }
                    finally
                    {
                        _context.Close();
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
            }
            return returnValue;
        }

        public async Task<object> GetAsync(string query)
        {
            var returnValue = default(object);
            try
            {
                using (var _context = GetContext())
                {
                    try
                    {
                        var cmd = _context.CreateCommand(query);
                        returnValue = await _context.GetAsync(cmd);
                    }
                    catch (Exception ex)
                    {
                        Logger.Error(ex, ex.Message);
                    }
                    finally
                    {
                        _context.Close();
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
            }
            return returnValue;
        }

        public async Task<List<T>> GetListAsync<T>(string query)
        {
            var returnValue = default(List<T>);
            try
            {
                using (var _context = GetContext())
                {
                    try
                    {
                        var cmd = _context.CreateCommand(query);
                        returnValue = await _context.GetListAsync<T>(cmd);
                    }
                    catch (Exception ex)
                    {
                        Logger.Error(ex, ex.Message);
                    }
                    finally
                    {
                        _context.Close();
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
            }
            return returnValue;
        }
    }
}
