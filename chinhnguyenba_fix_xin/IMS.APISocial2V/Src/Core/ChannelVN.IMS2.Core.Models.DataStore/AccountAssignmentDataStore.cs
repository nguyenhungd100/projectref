﻿using ChannelVN.IMS2.Core.Entities.AccountAssignment;
using ChannelVN.IMS2.Foundation.Data.Providers;
using ChannelVN.IMS2.Foundation.Logging;
using System;
using System.Data.SqlClient;
using System.Threading.Tasks;

namespace ChannelVN.IMS2.Core.Models.DataStore
{
    public class AccountAssignmentDataStore : CmsMainDataStore
    {
        private MssqlContext _context = null;

        public AccountAssignmentDataStore() => _context = GetContext();

        public async Task<bool> AddAsync(AccountAssignment data, bool isQueue = false)
        {
            var returnValue = false;
            try
            {
                using(var _context = GetContext())
                {
                    try
                    {
                        var cmd = _context.CreateCommand(Scripts.AccountAssignment.Add);
                        _context.AddParameter(cmd, "AccountId", data.AccountId);
                        _context.AddParameter(cmd, "OfficerId", data.OfficerId);
                        _context.AddParameter(cmd, "AssignedRole", data.AssignedRole);
                        _context.AddParameter(cmd, "AssignedDate", data.AssignedDate);
                        _context.AddParameter(cmd, "AssignedBy", data.AssignedBy);

                        returnValue = await _context.ExecuteNonQueryAsync(cmd as SqlCommand) > 0;                        
                    }
                    catch(Exception ex)
                    {                        
                        if (!isQueue) Logger.Sensitive(ex, ex.Message);
                    }
                    finally
                    {
                        _context.Close();
                    }
                }
            }
            catch (Exception ex)
            {                
                if (!isQueue) Logger.Sensitive(ex, ex.Message);
            }
            return returnValue;
        }
        public async Task<bool> UpdateAsync(AccountAssignment data, bool isQueue = false)
        {
            var returnValue = false;
            try
            {
                using(var _context = GetContext())
                {
                    try
                    {
                        var cmd = _context.CreateCommand(Scripts.AccountAssignment.Update);
                        _context.AddParameter(cmd, "AccountId", data.AccountId);
                        _context.AddParameter(cmd, "OfficerId", data.OfficerId);
                        _context.AddParameter(cmd, "AssignedRole", data.AssignedRole);
                        _context.AddParameter(cmd, "AssignedDate", data.AssignedDate);
                        _context.AddParameter(cmd, "AssignedBy", data.AssignedBy);

                        returnValue = await _context.ExecuteNonQueryAsync(cmd as SqlCommand) > 0;
                    }
                    catch (Exception ex)
                    {
                        if (!isQueue) Logger.Sensitive(ex, ex.Message);
                    }
                    finally
                    {
                        _context.Close();
                    }
                }
            }
            catch (Exception ex)
            {
                if (!isQueue) Logger.Sensitive(ex, ex.Message);
            }
            return returnValue;
        }
        public async Task<bool> RemoveAsync(AccountAssignment data, bool isQueue = false)
        {
            var returnValue = false;
            try
            {
                using (var _context = GetContext())
                {
                    try
                    {
                        var cmd = _context.CreateCommand(Scripts.AccountAssignment.Remove);
                        _context.AddParameter(cmd, "AccountId", data.AccountId);
                        _context.AddParameter(cmd, "OfficerId", data.OfficerId);

                        returnValue = await _context.ExecuteNonQueryAsync(cmd as SqlCommand) > 0;
                    }
                    catch (Exception ex)
                    {
                        if (!isQueue) Logger.Sensitive(ex, ex.Message);
                    }
                    finally
                    {
                        _context.Close();
                    }
                }
            }
            catch (Exception ex)
            {
                if (!isQueue) Logger.Sensitive(ex, ex.Message);
            }
            return returnValue;
        }
    }
}
