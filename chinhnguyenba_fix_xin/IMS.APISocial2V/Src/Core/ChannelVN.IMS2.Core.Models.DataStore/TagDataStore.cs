﻿using ChannelVN.IMS2.Core.Entities;
using ChannelVN.IMS2.Foundation.Data.Providers;
using ChannelVN.IMS2.Foundation.Logging;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Threading.Tasks;

namespace ChannelVN.IMS2.Core.Models.DataStore
{
    public class TagDataStore : CmsMainDataStore
    {
        private MssqlContext _context = null;

        public TagDataStore() => _context = GetContext();

        public async Task<bool> AddAsync(Tag data, bool isQueue = false)
        {
            var returnValue = false;
            try
            {
                using(var _context = GetContext())
                {
                    try
                    {
                        var cmd = _context.CreateCommand(Scripts.VideoTag.Add);
                        _context.AddParameter(cmd, "Id", data.Id);
                        _context.AddParameter(cmd, "ParentId", data.ParentId);
                        _context.AddParameter(cmd, "Name", data.Name);
                        _context.AddParameter(cmd, "UnsignName", data.UnsignName);

                        _context.AddParameter(cmd, "Description", data.Description);
                        _context.AddParameter(cmd, "Url", data.Url);
                        _context.AddParameter(cmd, "IsHotTag", data.IsHotTag);
                        _context.AddParameter(cmd, "CreatedDate", data.CreatedDate);
                        _context.AddParameter(cmd, "CreatedBy", data.CreatedBy);
                        _context.AddParameter(cmd, "Status", data.Status);
                        returnValue = await _context.ExecuteNonQueryAsync(cmd as SqlCommand) > 0;
                    }
                    catch(Exception ex)
                    {
                        if (!isQueue) Logger.Sensitive(ex, ex.Message);
                    }
                    finally
                    {
                        _context.Close();
                    }
                }
            }
            catch (Exception ex)
            {
                if (!isQueue) Logger.Sensitive(ex, ex.Message);
            }
            return returnValue;
        }
        public async Task<bool> UpdateAsync(Tag data, bool isQueue = false)
        {
            var returnValue = false;
            try
            {
                using(var _context = GetContext())
                {
                    try
                    {
                        var cmd = _context.CreateCommand(Scripts.VideoTag.Update);
                        _context.AddParameter(cmd, "Id", data.Id);
                        _context.AddParameter(cmd, "ParentId", data.ParentId);
                        _context.AddParameter(cmd, "Name", data.Name);
                        _context.AddParameter(cmd, "UnsignName", data.UnsignName);

                        _context.AddParameter(cmd, "Description", data.Description);
                        _context.AddParameter(cmd, "Url", data.Url);
                        _context.AddParameter(cmd, "IsHotTag", data.IsHotTag);
                        _context.AddParameter(cmd, "Status", data.Status);
                        returnValue = await _context.ExecuteNonQueryAsync(cmd as SqlCommand) > 0;
                    }
                    catch (Exception ex)
                    {
                        if (!isQueue) Logger.Sensitive(ex, ex.Message);
                    }
                    finally
                    {
                        _context.Close();
                    }
                }
            }
            catch (Exception ex)
            {
                if (!isQueue) Logger.Sensitive(ex, ex.Message);
            }
            return returnValue;
        }


        public async Task<int> CountTagAsync()
        {
            var returnValue = new TotalCount();
            try
            {
                using(var _context = GetContext())
                {
                    try
                    {
                        var cmd = _context.CreateCommand(Scripts.VideoTag.CountTag);
                        returnValue = await _context.GetAsync<TotalCount>(cmd);
                    }
                    catch (Exception ex)
                    {
                        Logger.Sensitive(ex, ex.Message);
                    }
                    finally
                    {
                        _context.Close();
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Sensitive(ex, ex.Message);
            }
            return returnValue.Total;
        }

        public async Task<List<Tag>> InitAllVideoTagAsync(int page, int pageSize, DateTime startDate, DateTime endDate)
        {
            var returnValue = default(List<Tag>);
            try
            {
                using(var _context = GetContext())
                {
                    try
                    {
                        var cmd = _context.CreateCommand(Scripts.VideoTag.InitAllVideoTag);
                        _context.AddParameter(cmd, "DateFrom", startDate);
                        _context.AddParameter(cmd, "DateTo", endDate);
                        _context.AddParameter(cmd, "PageIndex", page);
                        _context.AddParameter(cmd, "PageSize", pageSize);

                        returnValue = await _context.GetListAsync<Tag>(cmd);
                    }
                    catch (Exception ex)
                    {
                        Logger.Sensitive(ex, ex.Message);
                    }
                    finally
                    {
                        _context.Close();
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Sensitive(ex, ex.Message);
            }
            return returnValue;
        }
    }
}
