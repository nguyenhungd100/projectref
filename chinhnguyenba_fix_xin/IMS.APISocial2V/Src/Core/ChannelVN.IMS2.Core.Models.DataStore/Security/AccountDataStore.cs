﻿using ChannelVN.IMS2.Core.Entities;
using ChannelVN.IMS2.Core.Entities.Security;
using ChannelVN.IMS2.Foundation.Data.Providers;
using ChannelVN.IMS2.Foundation.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ChannelVN.IMS2.Core.Models.DataStore.Security
{
    public class AccountDataStore : CmsMainDataStore
    {
        public async Task<bool> AddAsync(Account data, bool isQueue = false)
        {
            var returnValue = false;
            try
            {
                using(var _context = GetContext())
                {
                    try
                    {
                        var cmd = _context.CreateCommand(Scripts.Account.Add);
                        _context.AddParameter(cmd, "Id", data.Id);
                        _context.AddParameter(cmd, "Mobile", data.Mobile);
                        _context.AddParameter(cmd, "Email", data.Email);
                        _context.AddParameter(cmd, "UserName", data.UserName);
                        _context.AddParameter(cmd, "Type", data.Type);
                        _context.AddParameter(cmd, "Class", data.Class);
                        _context.AddParameter(cmd, "FullName", data.FullName);
                        _context.AddParameter(cmd, "Avatar", data.Avatar);
                        _context.AddParameter(cmd, "Banner", data.Banner);
                        _context.AddParameter(cmd, "Status", data.Status);
                        _context.AddParameter(cmd, "IsSystem", data.IsSystem);
                        _context.AddParameter(cmd, "IsFullPermission", data.IsFullPermission);
                        _context.AddParameter(cmd, "OtpSecretKey", data.OtpSecretKey);
                        _context.AddParameter(cmd, "CreatedBy", data.CreatedBy);
                        _context.AddParameter(cmd, "CreatedDate", data.CreatedDate);
                        _context.AddParameter(cmd, "LoginedDate", data.LoginedDate);
                        _context.AddParameter(cmd, "Description", data.Description);
                        
                        _context.AddParameter(cmd, "CommentMode", (Byte?)data.CommentMode);
                        _context.AddParameter(cmd, "CrawlerMode", data.CrawlerMode);
                        _context.AddParameter(cmd, "CrawlerSource", data.CrawlerSource);
                        
                        _context.AddParameter(cmd, "Levels", data.Levels?.Count() > 0 ? string.Join(",", data.Levels.Select(p => (int)p)) : null);
                        _context.AddParameter(cmd, "RestrictClasses", data.Levels?.Count() > 0 ? string.Join(",", data.Levels.Select(p => (int)p)) : null);
                        await _context.ExecuteNonQueryAsync(cmd);
                        returnValue = true;
                    }
                    catch(Exception ex)
                    {
                        if (!isQueue) Logger.Sensitive(ex, ex.Message);
                    }
                    finally
                    {
                        _context.Close();
                    }
                }
            }
            catch (Exception ex)
            {
                if (!isQueue) Logger.Sensitive(ex, ex.Message);
            }
            return returnValue;
        }
        public async Task<bool> AddOfficialAsync(Account data, UserProfile profile, AccountMember accountMember, bool isQueue = false)
        {
            var returnValue = false;
            try
            {
                using(var _context = GetContext())
                {
                    try
                    {
                        var cmd = _context.CreateCommand(Scripts.Account.AddOfficial);
                        _context.AddParameter(cmd, "Id", data.Id);
                        _context.AddParameter(cmd, "Mobile", data.Mobile);
                        _context.AddParameter(cmd, "Email", data.Email);
                        _context.AddParameter(cmd, "UserName", data.UserName);
                        _context.AddParameter(cmd, "Type", data.Type);
                        _context.AddParameter(cmd, "Class", data.Class);
                        _context.AddParameter(cmd, "FullName", data.FullName);
                        _context.AddParameter(cmd, "Avatar", data.Avatar);
                        _context.AddParameter(cmd, "Banner", data.Banner);
                        _context.AddParameter(cmd, "Status", data.Status);
                        _context.AddParameter(cmd, "IsSystem", data.IsSystem);
                        _context.AddParameter(cmd, "IsFullPermission", data.IsFullPermission);
                        _context.AddParameter(cmd, "OtpSecretKey", data.OtpSecretKey);
                        _context.AddParameter(cmd, "CreatedDate", data.CreatedDate);
                        _context.AddParameter(cmd, "CreatedBy", data.CreatedBy);
                        _context.AddParameter(cmd, "LoginedDate", data.LoginedDate);
                        _context.AddParameter(cmd, "Description", data.Description);
                        _context.AddParameter(cmd, "CommentMode", (Byte?)data.CommentMode);
                        _context.AddParameter(cmd, "CrawlerMode", (Byte?)data.CrawlerMode);
                        _context.AddParameter(cmd, "CrawlerSource", data.CrawlerSource);
                        _context.AddParameter(cmd, "RelatedId", data.RelatedId);
                        _context.AddParameter(cmd, "RelatedType", (Byte?)data.RelatedType);
                        //_context.AddParameter(cmd, "LabelMode", data.LabelMode);
                        //profile
                        _context.AddParameter(cmd, "Slogan", profile.Slogan);
                        _context.AddParameter(cmd, "ProfileUrl", profile.ProfileUrl);
                        _context.AddParameter(cmd, "PenName", profile.PenName);
                        _context.AddParameter(cmd, "City", profile.City);
                        _context.AddParameter(cmd, "Address", profile.Address);
                        _context.AddParameter(cmd, "IdNumber", profile.IdNumber);
                        _context.AddParameter(cmd, "IssuedDate", profile.IssuedDate);
                        _context.AddParameter(cmd, "IssuedPlace", profile.IssuedPlace);
                        _context.AddParameter(cmd, "Scan1", profile.Scan1);
                        _context.AddParameter(cmd, "Scan2", profile.Scan2);
                        _context.AddParameter(cmd, "Scan3", profile.Scan3);
                        _context.AddParameter(cmd, "ReprName", profile.ReprName);
                        _context.AddParameter(cmd, "ReprJob", profile.ReprJob);
                        _context.AddParameter(cmd, "ReprEmail", profile.ReprEmail);
                        _context.AddParameter(cmd, "ReprPhone", profile.ReprPhone);
                        _context.AddParameter(cmd, "ModifiedBy", profile.ModifiedBy);
                        _context.AddParameter(cmd, "ModifiedDate", profile.ModifiedDate);
                        //AccountMember
                        _context.AddParameter(cmd, "OfficerId", accountMember.OfficerId);
                        _context.AddParameter(cmd, "MemberId", accountMember.MemberId);
                        _context.AddParameter(cmd, "Role", (int?) accountMember.Role);
                        _context.AddParameter(cmd, "JoinedDate", accountMember.JoinedDate);
                        _context.AddParameter(cmd, "Permissions", string.Join(",", accountMember.Permissions?.Select(p => (int)p) ?? new int[] { }));

                        await _context.ExecuteNonQueryAsync(cmd);
                        returnValue = true;
                    }
                    catch (Exception ex)
                    {
                        if (!isQueue) Logger.Sensitive(ex, ex.Message);
                    }
                    finally
                    {
                        _context.Close();
                    }
                }
            }
            catch (Exception ex)
            {
                if (!isQueue) Logger.Sensitive(ex, ex.Message);
            }
            return returnValue;
        }
        public async Task<bool> SaveProfileAsync(UserProfile data, Account userCurrent, bool isQueue = false)
        {
            var returnValue = false;
            try
            {
                using(var _context = GetContext())
                {
                    try
                    {
                        var cmd = _context.CreateCommand(Scripts.Account.AddProfile);
                        _context.AddParameter(cmd, "Id", data.Id);
                        _context.AddParameter(cmd, "Slogan", data.Slogan);
                        _context.AddParameter(cmd, "ProfileUrl", data.ProfileUrl);
                        _context.AddParameter(cmd, "PenName", data.PenName);
                        _context.AddParameter(cmd, "City", data.City);
                        _context.AddParameter(cmd, "Address", data.Address);
                        _context.AddParameter(cmd, "IdNumber", data.IdNumber);
                        _context.AddParameter(cmd, "IssuedDate", data.IssuedDate);
                        _context.AddParameter(cmd, "IssuedPlace", data.IssuedPlace);
                        _context.AddParameter(cmd, "Scan1", data.Scan1);
                        _context.AddParameter(cmd, "Scan2", data.Scan2);
                        _context.AddParameter(cmd, "Scan3", data.Scan3);
                        _context.AddParameter(cmd, "ReprName", data.ReprName);
                        _context.AddParameter(cmd, "ReprJob", data.ReprJob);
                        _context.AddParameter(cmd, "ReprEmail", data.ReprEmail);
                        _context.AddParameter(cmd, "ReprPhone", data.ReprPhone);
                        _context.AddParameter(cmd, "ModifiedBy", data.ModifiedBy);
                        _context.AddParameter(cmd, "ModifiedDate", data.ModifiedDate);
                        _context.AddParameter(cmd, "RegisterType", 0);
                        _context.AddParameter(cmd, "Type", userCurrent != null ? userCurrent.Type : 0);
                        returnValue = await _context.ExecuteNonQueryAsync(cmd) > 0;
                    }
                    catch (Exception ex)
                    {
                        if (!isQueue) Logger.Sensitive(ex, ex.Message);
                    }
                    finally
                    {
                        _context.Close();
                    }
                }
            }
            catch (Exception ex)
            {
                if (!isQueue) Logger.Sensitive(ex, ex.Message);
            }
            return returnValue;
        }

        public async Task<bool> UpdateAsync(AccountSearch data, UserProfile profile, bool isQueue = false)
        {
            var returnValue = false;
            try
            {
                using(var _context = GetContext())
                {
                    try
                    {
                        var cmd = _context.CreateCommand(Scripts.Account.Update);
                        _context.AddParameter(cmd, "Id", data.Id);
                        _context.AddParameter(cmd, "Mobile", data.Mobile);
                        _context.AddParameter(cmd, "Email", data.Email);
                        _context.AddParameter(cmd, "UserName", data.UserName);
                        _context.AddParameter(cmd, "Password", data.Password);
                        _context.AddParameter(cmd, "Type", data.Type);
                        _context.AddParameter(cmd, "Class", data.Class);
                        _context.AddParameter(cmd, "FullName", data.FullName);
                        _context.AddParameter(cmd, "Avatar", data.Avatar);
                        _context.AddParameter(cmd, "Banner", data.Banner);
                        _context.AddParameter(cmd, "VerifiedBy", data.VerifiedBy);
                        _context.AddParameter(cmd, "VerifiedDate", data.VerifiedDate);
                        _context.AddParameter(cmd, "Status", data.Status);
                        _context.AddParameter(cmd, "IsSystem", data.IsSystem);
                        _context.AddParameter(cmd, "IsFullPermission", data.IsFullPermission);
                        _context.AddParameter(cmd, "OtpSecretKey", data.OtpSecretKey);
                        _context.AddParameter(cmd, "Mode", data.Mode);
                        
                        _context.AddParameter(cmd, "LoginedDate", data.LoginedDate);
                        _context.AddParameter(cmd, "Description", data.Description);
                        _context.AddParameter(cmd, "CommentMode", (Byte?)data.CommentMode);
                        _context.AddParameter(cmd, "CrawlerMode", (Byte?)data.CrawlerMode);
                        _context.AddParameter(cmd, "CrawlerSource", data.CrawlerSource);
                        _context.AddParameter(cmd, "RelatedId", data.RelatedId);
                        _context.AddParameter(cmd, "RelatedType", (Byte?)data.RelatedType);
                        _context.AddParameter(cmd, "LabelMode", (int?)data.LabelMode);

                        _context.AddParameter(cmd, "Levels", data.Levels?.Count() > 0 ? string.Join(",", data.Levels.Select(p => (int)p)) : null);
                        _context.AddParameter(cmd, "RestrictClasses", data.RestrictClasses?.Count() > 0 ? string.Join(",", data.RestrictClasses.Select(p => (int)p)) : null);
                        //profile
                        _context.AddParameter(cmd, "Gender", profile?.Gender);
                        _context.AddParameter(cmd, "BirthDay", profile?.BirthDay);
                        _context.AddParameter(cmd, "PenName", profile?.PenName);

                        _context.AddParameter(cmd, "Title", profile?.Title);
                        _context.AddParameter(cmd, "WorkPlace", profile?.WorkPlace);
                        _context.AddParameter(cmd, "School", profile?.School);
                        _context.AddParameter(cmd, "Address", profile?.Address);
                        _context.AddParameter(cmd, "UrlNetwork", profile?.UrlNetwork);
                        _context.AddParameter(cmd, "UrlSocial", profile?.UrlSocial);
                        returnValue = await _context.ExecuteNonQueryAsync(cmd) > 0;
                    }
                    catch (Exception ex)
                    {
                        if (!isQueue) Logger.Sensitive(ex, ex.Message);
                    }
                    finally
                    {
                        _context.Close();
                    }
                }
            }
            catch (Exception ex)
            {
                if (!isQueue) Logger.Sensitive(ex, ex.Message);
            }
            return returnValue;
        }

        public async Task<bool> ConfigPageAsync(Account officer, bool isQueue = false)
        {
            var returnValue = false;
            try
            {
                using (var _context = GetContext())
                {
                    try
                    {
                        var cmd = _context.CreateCommand(Scripts.Account.ConfigPage);
                        _context.AddParameter(cmd, "Id", officer.Id);
                        _context.AddParameter(cmd, "Mode", officer.Mode);

                        returnValue = await _context.ExecuteNonQueryAsync(cmd) > 0;
                    }
                    catch (Exception ex)
                    {
                        if (!isQueue) Logger.Sensitive(ex, ex.Message);
                    }
                    finally
                    {
                        _context.Close();
                    }
                }
            }
            catch (Exception ex)
            {
                if (!isQueue) Logger.Sensitive(ex, ex.Message);
            }
            return returnValue;
        }

        public async Task<bool> AcceptOrRejectAsync(AccountSearch data, bool isQueue = false)
        {
            var returnValue = false;
            try
            {
                using(var _context = GetContext())
                {
                    try
                    {
                        var cmd = _context.CreateCommand(Scripts.Account.AcceptOrReject);

                        _context.AddParameter(cmd, "VerifiedBy", data.VerifiedBy);
                        _context.AddParameter(cmd, "VerifiedDate", data.VerifiedDate);

                        returnValue = await _context.ExecuteNonQueryAsync(cmd) > 0;
                    }
                    catch (Exception ex)
                    {
                        if (!isQueue) Logger.Sensitive(ex, ex.Message);
                    }
                    finally
                    {
                        _context.Close();
                    }
                }
            }
            catch (Exception ex)
            {
                if (!isQueue) Logger.Sensitive(ex, ex.Message);
            }
            return returnValue;
        }

        public async Task<List<AccountMember>> InitAllAccountMemberAsync(int page, int pageSize, DateTime startDate, DateTime endDate)
        {
            var returnValue = default(List<AccountMember>);
            try
            {
                using(var _context = GetContext())
                {
                    try
                    {
                        var cmd = _context.CreateCommand(Scripts.Account.InitAllAccountMember);
                        _context.AddParameter(cmd, "DateFrom", startDate);
                        _context.AddParameter(cmd, "DateTo", endDate);
                        _context.AddParameter(cmd, "PageIndex", page);
                        _context.AddParameter(cmd, "PageSize", pageSize);
                        returnValue = await _context.GetListAsync<AccountMember>(cmd);
                    }
                    catch (Exception ex)
                    {
                        Logger.Sensitive(ex, ex.Message);
                    }
                    finally
                    {
                        _context.Close();
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Sensitive(ex, ex.Message);
            }
            return returnValue;
        }

        public async Task<List<AccountDistribution>> GetAccountDistributionByAccountIdAsync(long accountId)
        {
            var returnValue = default(List<AccountDistribution>);
            try
            {
                using(var _context = GetContext())
                {
                    try
                    {
                        var cmd = _context.CreateCommand(Scripts.Account.GetAccountDistributionByAccountId);
                        _context.AddParameter(cmd, "AccountId", accountId);
                        returnValue = await _context.GetListAsync<AccountDistribution>(cmd);
                    }
                    catch (Exception ex)
                    {
                        Logger.Sensitive(ex, ex.Message);
                    }
                    finally
                    {
                        _context.Close();
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Sensitive(ex, ex.Message);
            }
            return returnValue;
        }

        public async Task<List<AccountSearch>> InitAllAccountAsync(int page, int pageSize, DateTime startDate, DateTime endDate)
        {
            var returnValue = default(List<AccountSearch>);
            try
            {
                using(var _context = GetContext())
                {
                    try
                    {
                        var cmd = _context.CreateCommand(Scripts.Account.InitAllAccount);
                        _context.AddParameter(cmd, "DateFrom", startDate);
                        _context.AddParameter(cmd, "DateTo", endDate);
                        _context.AddParameter(cmd, "PageIndex", page);
                        _context.AddParameter(cmd, "PageSize", pageSize);
                        returnValue = await _context.GetListAsync<AccountSearch>(cmd);
                    }
                    catch (Exception ex)
                    {
                        Logger.Sensitive(ex, ex.Message);
                    }
                    finally
                    {
                        _context.Close();
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Sensitive(ex, ex.Message);
            }
            return returnValue;
        }

        public async Task<int> CountAccountAsync()
        {
            var returnValue = new TotalCount();
            try
            {
                using(var _context = GetContext())
                {
                    try
                    {
                        var cmd = _context.CreateCommand(Scripts.Account.CountAccount);
                        returnValue = await _context.GetAsync<TotalCount>(cmd);
                    }
                    catch (Exception ex)
                    {
                        Logger.Sensitive(ex, ex.Message);
                    }
                    finally
                    {
                        _context.Close();
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Sensitive(ex, ex.Message);
            }
            return returnValue.Total;
        }

        public async Task<AccountSearch> GetByIdAsync(long id)
        {
            var returnValue = default(AccountSearch);
            try
            {
                using(var _context = GetContext())
                {
                    try
                    {
                        var cmd = _context.CreateCommand(Scripts.Account.GetById);
                        _context.AddParameter(cmd, "Id", id);
                        returnValue = await _context.GetAsync<AccountSearch>(cmd);
                    }
                    catch (Exception ex)
                    {
                        Logger.Sensitive(ex, ex.Message);
                    }
                    finally
                    {
                        _context.Close();
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Sensitive(ex, ex.Message);
            }
            return returnValue;
        }
        
        public async Task<Account> GetByMobileAsync(long id, string mobile)
        {
            var returnValue = default(Account);
            try
            {
                using (var _context = GetContext())
                {
                    try
                    {
                        var cmd = _context.CreateCommand(Scripts.Account.GetByMobile);
                        _context.AddParameter(cmd, "Id", id);
                        _context.AddParameter(cmd, "Mobile", mobile);
                        returnValue = await _context.GetAsync<Account>(cmd);
                    }
                    catch (Exception ex)
                    {
                        Logger.Sensitive(ex, ex.Message);
                    }
                    finally
                    {
                        _context.Close();
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Sensitive(ex, ex.Message);
            }
            return returnValue;
        }

        public async Task<bool> UpdateStatusAsync(AccountSearch account, bool isQueue = false)
        {
            var returnValue = false;
            try
            {
                using(var _context = GetContext())
                {
                    try
                    {
                        var cmd = _context.CreateCommand(Scripts.Account.UpdateStatus);
                        _context.AddParameter(cmd, "Id", account.Id);
                        _context.AddParameter(cmd, "Status", account.Status);
                        _context.AddParameter(cmd, "Class", account.Class);
                        _context.AddParameter(cmd, "LabelMode", account.LabelMode);
                        returnValue = await _context.ExecuteNonQueryAsync(cmd) > 0;
                    }
                    catch (Exception ex)
                    {
                        if (!isQueue) Logger.Sensitive(ex, ex.Message);
                    }
                    finally
                    {
                        _context.Close();
                    }
                }
            }
            catch (Exception ex)
            {
                if (!isQueue) Logger.Sensitive(ex, ex.Message);
            }
            return returnValue;
        }

        public async Task<bool> UpdateAccountTypeAsync(Account accDb, bool isQueue = false)
        {
            var returnValue = false;
            try
            {
                using(var _context = GetContext())
                {
                    try
                    {
                        var cmd = _context.CreateCommand(Scripts.Account.UpdateAccountType);
                        _context.AddParameter(cmd, "Id", accDb.Id);
                        _context.AddParameter(cmd, "Type", accDb.Type);
                        _context.AddParameter(cmd, "Class", accDb.Class);
                        _context.AddParameter(cmd, "Levels", accDb.Levels?.Count() > 0 ? string.Join(",", accDb.Levels.Select(p=>(int)p)) : null);
                        _context.AddParameter(cmd, "RestrictClasses", accDb.RestrictClasses?.Count() > 0 ? string.Join(",", accDb.RestrictClasses.Select(p => (int)p)) : null);
                        returnValue = await _context.ExecuteNonQueryAsync(cmd) > 0;
                    }
                    catch (Exception ex)
                    {
                        Logger.Sensitive(ex, ex.Message);
                    }
                    finally
                    {
                        _context.Close();
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Sensitive(ex, ex.Message);
            }
            return returnValue;
        }

        public async Task<bool> UpdateAccountOpentIdAsync(Account accDb, bool isQueue = false)
        {
            var returnValue = false;
            try
            {
                using (var _context = GetContext())
                {
                    try
                    {
                        var cmd = _context.CreateCommand(Scripts.Account.UpdateAccountOpentId);
                        _context.AddParameter(cmd, "Id", accDb.Id);
                        _context.AddParameter(cmd, "OpenId", accDb.OpenId);                       
                        returnValue = await _context.ExecuteNonQueryAsync(cmd) > 0;
                    }
                    catch (Exception ex)
                    {
                        Logger.Sensitive(ex, ex.Message);
                    }
                    finally
                    {
                        _context.Close();
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Sensitive(ex, ex.Message);
            }
            return returnValue;
        }

        public async Task<bool> AcceptOrRejectOrLockWriterAsync(AccountSearch data)
        {
            var returnValue = false;
            try
            {
                using(var _context = GetContext())
                {
                    try
                    {
                        var cmd = _context.CreateCommand(Scripts.Account.AcceptOrRejectOrLockWriter);
                        _context.AddParameter(cmd, "Status", data.Status);
                        returnValue = await _context.ExecuteNonQueryAsync(cmd) > 0;
                    }
                    catch (Exception ex)
                    {
                        Logger.Sensitive(ex, ex.Message);
                    }
                    finally
                    {
                        _context.Close();
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Sensitive(ex, ex.Message);
            }
            return returnValue;
        }

        public async Task<bool> ActiveAsync(long id)
        {
            var returnValue = false;
            try
            {
                using(var _context = GetContext())
                {
                    try
                    {
                        var cmd = _context.CreateCommand(Scripts.Account.Active);
                        _context.AddParameter(cmd, "UserId", id);
                        _context.AddParameter(cmd, "Status", (int)AccountStatus.Actived);
                        returnValue = await _context.ExecuteNonQueryAsync(cmd) > 0;
                    }
                    catch (Exception ex)
                    {
                        Logger.Sensitive(ex, ex.Message);
                    }
                    finally
                    {
                        _context.Close();
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Sensitive(ex, ex.Message);
            }
            return returnValue;
        }

        public async Task<bool> DeActiveAsync(long id)
        {
            var returnValue = false;
            try
            {
                using(var _context = GetContext())
                {
                    try
                    {
                        var cmd = _context.CreateCommand(Scripts.Account.DeActive);
                        _context.AddParameter(cmd, "UserId", id);
                        _context.AddParameter(cmd, "Status", (int)AccountStatus.UnActived);
                        returnValue = await _context.ExecuteNonQueryAsync(cmd) > 0;
                    }
                    catch (Exception ex)
                    {
                        Logger.Sensitive(ex, ex.Message);
                    }
                    finally
                    {
                        _context.Close();
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Sensitive(ex, ex.Message);
            }
            return returnValue;
        }
        public async Task<List<Account>> GetAllAccountAsync()
        {
            var returnValue = new List<Account>();
            try
            {
                using (var _context = GetContext())
                {
                    try
                    {
                        var cmd = _context.CreateCommand(Scripts.Account.GetAll);
                        returnValue = await _context.GetListAsync<Account>(cmd);
                    }
                    catch (Exception ex)
                    {
                        Logger.Sensitive(ex, ex.Message);
                    }
                    finally
                    {
                        _context.Close();
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Sensitive(ex, ex.Message);
            }
            return returnValue;
        }
        

        #region AccountMember
        public async Task<bool> UpdateAccountMemberAsync(AccountMember accountMember, bool isQueue = false)
        {
            var returnValue = false;
            try
            {
                using(var _context = GetContext())
                {
                    try
                    {
                        var cmd = _context.CreateCommand(Scripts.Account.UpdateAccountMember);
                        _context.AddParameter(cmd, "OfficerId", accountMember.OfficerId);
                        _context.AddParameter(cmd, "MemberId", accountMember.MemberId);
                        _context.AddParameter(cmd, "Role", (int?)accountMember.Role);
                        _context.AddParameter(cmd, "Permissions", string.Join(",", accountMember.Permissions?.Select(p => (int)p) ?? new int[] { }));
                        returnValue = await _context.ExecuteNonQueryAsync(cmd) > 0;
                        returnValue = true;
                    }
                    catch (Exception ex)
                    {
                        if (!isQueue) Logger.Sensitive(ex, ex.Message);
                    }
                    finally
                    {
                        _context.Close();
                    }
                }
            }
            catch (Exception ex)
            {
                if (!isQueue) Logger.Sensitive(ex, ex.Message);
            }
            return returnValue;
        }


        public async Task<bool> AddAccountMemberAsync(AccountMember accountMember, bool isQueue = false)
        {
            var returnValue = false;
            try
            {
                using(var _context = GetContext())
                {
                    try
                    {
                        var cmd = _context.CreateCommand(Scripts.Account.AddAccountMember);
                        _context.AddParameter(cmd, "OfficerId", accountMember.OfficerId);
                        _context.AddParameter(cmd, "MemberId", accountMember.MemberId);
                        _context.AddParameter(cmd, "Role", (int?)accountMember.Role);
                        _context.AddParameter(cmd, "JoinedDate", accountMember.JoinedDate);
                        _context.AddParameter(cmd, "Permissions", string.Join(",", accountMember.Permissions?.Select(p => (int)p) ?? new int[] { }));
                        returnValue = await _context.ExecuteNonQueryAsync(cmd) > 0;
                    }
                    catch (Exception ex)
                    {
                        if (!isQueue) Logger.Sensitive(ex, ex.Message);
                    }
                    finally
                    {
                        _context.Close();
                    }
                }
            }
            catch (Exception ex)
            {
                if (!isQueue) Logger.Sensitive(ex, ex.Message);
            }
            return returnValue;
        }

        public async Task<int> CountAccountMemberAsync()
        {
            var returnValue = new TotalCount();
            try
            {
                using(var _context = GetContext())
                {
                    try
                    {
                        var cmd = _context.CreateCommand(Scripts.Account.CountAccountMember);
                        returnValue = await _context.GetAsync<TotalCount>(cmd);
                    }
                    catch (Exception ex)
                    {
                        Logger.Sensitive(ex, ex.Message);
                    }
                    finally
                    {
                        _context.Close();
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Sensitive(ex, ex.Message);
            }
            return returnValue.Total;
        }

        public async Task<bool> RemoveMemberAsync(long accountId, long memberId, bool isQueue = false)
        {
            var returnValue = false;
            try
            {
                using(var _context = GetContext())
                {
                    try
                    {
                        var cmd = _context.CreateCommand(Scripts.Account.RemoveAccountMember);
                        _context.AddParameter(cmd, "OfficerId", accountId);
                        _context.AddParameter(cmd, "MemberId", memberId);
                        returnValue = await _context.ExecuteNonQueryAsync(cmd) > 0;
                        returnValue = true;
                    }
                    catch (Exception ex)
                    {
                        if (!isQueue) Logger.Sensitive(ex, ex.Message);
                    }
                    finally
                    {
                        _context.Close();
                    }
                }
            }
            catch (Exception ex)
            {
                if (!isQueue) Logger.Sensitive(ex, ex.Message);
            }
            return returnValue;
        }

        public async Task<bool> RemoveMemberOnAllPageAsync(IEnumerable<long> listOfficerId, long memberId, bool isQueue = false)
        {
            var returnValue = false;
            try
            {
                using (var _context = GetContext())
                {
                    try
                    {
                        var cmd = _context.CreateCommand(Scripts.Account.RemoveAccountMemberOnAllPage);
                        _context.AddParameter(cmd, "ListOfficerId", string.Join(",",  listOfficerId));
                        _context.AddParameter(cmd, "MemberId", memberId);
                        returnValue = await _context.ExecuteNonQueryAsync(cmd) > 0;
                        returnValue = true;
                    }
                    catch (Exception ex)
                    {
                        if (!isQueue) Logger.Sensitive(ex, ex.Message);
                    }
                    finally
                    {
                        _context.Close();
                    }
                }
            }
            catch (Exception ex)
            {
                if (!isQueue) Logger.Sensitive(ex, ex.Message);
            }
            return returnValue;
        }

        public async Task<bool> LockOrUnlockMemberAsync(AccountMember data, bool isQueue = false)
        {
            var returnValue = false;
            try
            {
                using (var _context = GetContext())
                {
                    try
                    {
                        var cmd = _context.CreateCommand(Scripts.Account.LockAccountMember);
                        _context.AddParameter(cmd, "OfficerId", data.OfficerId);
                        _context.AddParameter(cmd, "MemberId", data.MemberId);
                        _context.AddParameter(cmd, "LockedDate", data.LockedDate);
                        returnValue = await _context.ExecuteNonQueryAsync(cmd) > 0;
                        returnValue = true;
                    }
                    catch (Exception ex)
                    {
                        if (!isQueue) Logger.Sensitive(ex, ex.Message);
                    }
                    finally
                    {
                        _context.Close();
                    }
                }
            }
            catch (Exception ex)
            {
                if (!isQueue) Logger.Sensitive(ex, ex.Message);
            }
            return returnValue;
        }

        public async Task<bool> LockOrUnlockMemberOnAllPageAsync(AccountMember[] data, bool isQueue = false)
        {
            var returnValue = false;
            try
            {
                using (var _context = GetContext())
                {
                    try
                    {
                        var cmd = _context.CreateCommand(Scripts.Account.LockOrUnlockMemberOnAllPage);
                        _context.AddParameter(cmd, "ListOfficerId", string.Join(",", data?.Select(p=>p.OfficerId)));
                        _context.AddParameter(cmd, "MemberId", data.First()?.MemberId);
                        _context.AddParameter(cmd, "LockedDate", data.First()?.LockedDate);
                        returnValue = await _context.ExecuteNonQueryAsync(cmd) > 0;
                        returnValue = true;
                    }
                    catch (Exception ex)
                    {
                        if (!isQueue) Logger.Sensitive(ex, ex.Message);
                    }
                    finally
                    {
                        _context.Close();
                    }
                }
            }
            catch (Exception ex)
            {
                if (!isQueue) Logger.Sensitive(ex, ex.Message);
            }
            return returnValue;
        }
        
        public async Task<bool> ChangeOwnerAsync(long officerId, long oldOwnerId, long newOwnerId)
        {
            var returnValue = false;
            try
            {
                using (var _context = GetContext())
                {
                    try
                    {
                        var cmd = _context.CreateCommand(Scripts.Account.ChangeOwner);
                        _context.AddParameter(cmd, "OfficerId", officerId);
                        _context.AddParameter(cmd, "OldOwnerId", oldOwnerId);
                        _context.AddParameter(cmd, "NewOwnerId", newOwnerId);
                        _context.AddParameter(cmd, "Role", (int)AccountMemberRole.Owner);
                        _context.AddParameter(cmd, "JoinedDate", DateTime.Now);
                        returnValue = await _context.ExecuteNonQueryAsync(cmd) > 0;
                        returnValue = true;
                    }
                    catch (Exception ex)
                    {
                        Logger.Sensitive(ex, ex.Message);
                    }
                    finally
                    {
                        _context.Close();
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Sensitive(ex, ex.Message);
            }
            return returnValue;
        }

        public async Task<bool> SetProfileIsPageOnAppAsync(Account account)
        {
            var returnValue = false;
            try
            {
                using (var _context = GetContext())
                {
                    try
                    {
                        var cmd = _context.CreateCommand(Scripts.Account.SetProfileIsPageOnApp);
                        _context.AddParameter(cmd, "DelegatorId", account.DelegatorId);
                        _context.AddParameter(cmd, "Id", account.Id);
                        returnValue = await _context.ExecuteNonQueryAsync(cmd) > 0;
                        returnValue = true;
                    }
                    catch (Exception ex)
                    {
                        Logger.Sensitive(ex, ex.Message);
                    }
                    finally
                    {
                        _context.Close();
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Sensitive(ex, ex.Message);
            }
            return returnValue;
        }

        public async Task<bool> UpdateProfileUrlAsync(UserProfile profile)
        {
            var returnValue = false;
            try
            {
                using (var _context = GetContext())
                {
                    try
                    {
                        var cmd = _context.CreateCommand(Scripts.Account.UpdateProfileUrl);
                        _context.AddParameter(cmd, "ProfileUrl", profile.ProfileUrl);
                        _context.AddParameter(cmd, "Id", profile.Id);
                        returnValue = await _context.ExecuteNonQueryAsync(cmd) > 0;
                        returnValue = true;
                    }
                    catch (Exception ex)
                    {
                        Logger.Sensitive(ex, ex.Message);
                    }
                    finally
                    {
                        _context.Close();
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Sensitive(ex, ex.Message);
            }
            return returnValue;
        }

        public async Task<bool> UpdateAccessTokenAsync(Account data)
        {
            var returnValue = false;
            try
            {
                using (var _context = GetContext())
                {
                    try
                    {
                        var cmd = _context.CreateCommand(Scripts.Account.UpdateAccessToken);
                        _context.AddParameter(cmd, "Id", data.Id);
                        _context.AddParameter(cmd, "Mobile", data.Mobile);
                        _context.AddParameter(cmd, "Email", data.Email);
                        _context.AddParameter(cmd, "OtpSecretKey", data.OtpSecretKey);
                        _context.AddParameter(cmd, "FullName", data.FullName);

                        returnValue = await _context.ExecuteNonQueryAsync(cmd) > 0;
                        returnValue = true;
                    }
                    catch (Exception ex)
                    {
                        Logger.Sensitive(ex, ex.Message);
                    }
                    finally
                    {
                        _context.Close();
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Sensitive(ex, ex.Message);
            }
            return returnValue;
        }
        #endregion
    }
}
