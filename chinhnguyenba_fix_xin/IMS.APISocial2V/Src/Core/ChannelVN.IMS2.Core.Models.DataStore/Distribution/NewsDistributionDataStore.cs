﻿using ChannelVN.IMS2.Core.Entities.Distribution;
using ChannelVN.IMS2.Foundation.Data.Providers;
using ChannelVN.IMS2.Foundation.Logging;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace ChannelVN.IMS2.Core.Models.DataStore.Distribution
{
    public class NewsDistributionDataStore:CmsMainDataStore
    {
        public async Task<bool> InitDataAsync(List<NewsDistribution> data)
        {
            var returnValue = false;
            try
            {
                using (var _context = GetContext())
                {
                    try
                    {
                        foreach (var item in data)
                        {
                            var cmd = _context.CreateCommand(Scripts.Distribution.InitNewsDistribution);
                            _context.AddParameter(cmd, "DistributorId", item.DistributorId);
                            _context.AddParameter(cmd, "NewsId", item.NewsId);
                            _context.AddParameter(cmd, "ItemStreamId", item.ItemStreamId);
                            _context.AddParameter(cmd, "SharedDate", item.SharedDate);
                            _context.AddParameter(cmd, "SharedZone", item.SharedZone);
                            returnValue = await _context.ExecuteNonQueryAsync(cmd) > 0;
                        }
                    }
                    catch(Exception ex)
                    {
                        Logger.Sensitive(ex, ex.Message);
                    }
                    finally
                    {
                        _context.Close();
                    }
                }
            }
            catch(Exception ex)
            {
                Logger.Sensitive(ex, ex.Message);
            }
            return returnValue;
        }
    }
}
