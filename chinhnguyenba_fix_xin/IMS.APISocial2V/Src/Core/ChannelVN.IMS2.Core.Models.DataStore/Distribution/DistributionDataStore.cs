﻿using ChannelVN.IMS2.Core.Entities.Album;
using ChannelVN.IMS2.Core.Entities.Distribution;
using ChannelVN.IMS2.Core.Entities.Gallery;
using ChannelVN.IMS2.Core.Entities.Media;
using ChannelVN.IMS2.Core.Entities.Photo;
using ChannelVN.IMS2.Foundation.Data.Providers;
using ChannelVN.IMS2.Foundation.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ChannelVN.IMS2.Core.Models.DataStore.Distribution
{
    public class DistributionDataStore : CmsMainDataStore
    {
        public async Task<bool> AddAsync(Distributor data, bool isQueue = false)
        {
            var returnValue = false;
            try
            {
                using(var _context = GetContext())
                {
                    try
                    {
                        var cmd = _context.CreateCommand(Scripts.Distribution.Add);

                        _context.AddParameter(cmd, "Id", data.Id);
                        _context.AddParameter(cmd, "Name", data.Name);
                        _context.AddParameter(cmd, "Description", data.Description);
                        _context.AddParameter(cmd, "Status", data.Status);
                        _context.AddParameter(cmd, "Priority", data.Priority);
                        _context.AddParameter(cmd, "Code", data.Code);
                        _context.AddParameter(cmd, "CreatedDate", data.CreatedDate);
                        _context.AddParameter(cmd, "CreatedBy", data.CreatedBy);
                        _context.AddParameter(cmd, "QuotaNormal", data.Config.QuotaNormal);
                        _context.AddParameter(cmd, "QuotaProfessional", data.Config.QuotaProfessional);
                        _context.AddParameter(cmd, "UnitTime", data.Config.UnitTime);
                        _context.AddParameter(cmd, "UnitAmount", data.Config.UnitAmount);
                        _context.AddParameter(cmd, "ApiZoneVideo", data.Config.ApiZoneVideo);
                        _context.AddParameter(cmd, "ApiVideo", data.Config.ApiVideo);
                        _context.AddParameter(cmd, "ApiVideoPlaylist", data.Config.ApiVideoPlaylist);
                        _context.AddParameter(cmd, "ApiMediaUnit", data.Config.ApiMediaUnit);

                        returnValue = await _context.ExecuteNonQueryAsync(cmd) > 0;
                    }
                    catch(Exception ex)
                    {
                        if (!isQueue) Logger.Sensitive(ex, ex.Message);
                    }
                    finally
                    {
                        _context.Close();
                    }
                }
            }
            catch (Exception ex)
            {
                if (!isQueue) Logger.Sensitive(ex, ex.Message);
            }
            return returnValue;
        }

        public async Task<bool> UpdateAsync(Distributor data, bool isQueue = false)
        {
            var returnValue = false;
            try
            {
                using(var _context = GetContext())
                {
                    try
                    {
                        var cmd = _context.CreateCommand(Scripts.Distribution.Update);

                        _context.AddParameter(cmd, "Id", data.Id);
                        _context.AddParameter(cmd, "Name", data.Name);
                        _context.AddParameter(cmd, "Description", data.Description);
                        _context.AddParameter(cmd, "Status", data.Status);
                        _context.AddParameter(cmd, "Priority", data.Priority);
                        _context.AddParameter(cmd, "Code", data.Code);
                        _context.AddParameter(cmd, "QuotaNormal", data.Config.QuotaNormal);
                        _context.AddParameter(cmd, "QuotaProfessional", data.Config.QuotaProfessional);
                        _context.AddParameter(cmd, "UnitTime", data.Config.UnitTime);
                        _context.AddParameter(cmd, "UnitAmount", data.Config.UnitAmount);
                        _context.AddParameter(cmd, "ApiZoneVideo", data.Config.ApiZoneVideo);
                        _context.AddParameter(cmd, "ApiVideo", data.Config.ApiVideo);
                        _context.AddParameter(cmd, "ApiVideoPlaylist", data.Config.ApiVideoPlaylist);
                        _context.AddParameter(cmd, "ApiMediaUnit", data.Config.ApiMediaUnit);

                        returnValue = await _context.ExecuteNonQueryAsync(cmd) > 0;
                    }
                    catch (Exception ex)
                    {
                        if (!isQueue) Logger.Sensitive(ex, ex.Message);
                    }
                    finally
                    {
                        _context.Close();
                    }
                }
            }
            catch (Exception ex)
            {
                if (!isQueue) Logger.Sensitive(ex, ex.Message);
            }
            return returnValue;
        }

        public async Task<Distributor> GetByIdAsync(long id)
        {
            var returnValue = default(Distributor);
            try
            {
                using(var _context = GetContext())
                {
                    try
                    {
                        var cmd = _context.CreateCommand(Scripts.Distribution.GetById);
                        _context.AddParameter(cmd, "Id", id);

                        returnValue = await _context.GetAsync<Distributor>(cmd);
                    }
                    catch (Exception ex)
                    {
                        Logger.Sensitive(ex, ex.Message);
                    }
                    finally
                    {
                        _context.Close();
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Sensitive(ex, ex.Message);
            }
            return returnValue;
        }

        public async Task<bool> ShareMediaUnitAsync(ItemStreamDistribution itemStreamDistribution, List<MediaUnitSearch> listMedia, bool isQueue = false)
        {
            var returnValue = false;
            try
            {
                var listNewsDistribution = new List<NewsDistribution>();
                foreach (var media in listMedia)
                {
                    listNewsDistribution.AddRange(media.NewsDistribution);
                }

                using(var _context = GetContext())
                {
                    //_context.BeginTransaction();
                    try
                    {
                        var cmd = _context.CreateCommand(Scripts.Article.SaveItemStream);

                        _context.AddParameter(cmd, "Id", itemStreamDistribution.Id);
                        _context.AddParameter(cmd, "PublishStatus", itemStreamDistribution.PublishStatus);
                        _context.AddParameter(cmd, "CreatedDate", itemStreamDistribution.CreatedDate);
                        _context.AddParameter(cmd, "ModifiedDate", itemStreamDistribution.ModifiedDate);
                        _context.AddParameter(cmd, "ItemStreamTempId", itemStreamDistribution.ItemStreamTempId);
                        _context.AddParameter(cmd, "Note", itemStreamDistribution.Note);
                        _context.AddParameter(cmd, "Title", itemStreamDistribution.Title);
                        _context.AddParameter(cmd, "MetaData", itemStreamDistribution.MetaData);

                        _context.AddParameter(cmd, "ListNewsDistribution", string.Join("_", listNewsDistribution?.Select(p => p.ToString()) ?? new string[] { }));

                        returnValue = await _context.ExecuteNonQueryAsync(cmd) > 0;
                        //if (returnValue)
                        //{
                        //    foreach (var media in listMedia)
                        //    {
                        //        foreach (var newsDistribution in media.NewsDistribution.Where(p => p.ItemStreamId == itemStreamDistribution.Id))
                        //        {
                        //            if (newsDistribution.NewsId > 0)
                        //            {
                        //                var cmd2 = _context.CreateCommand(Scripts.Photo.InsertOrUpdateNewsDistribution);

                        //                _context.AddParameter(cmd2, "DistributorId", newsDistribution.DistributorId);
                        //                _context.AddParameter(cmd2, "NewsId", newsDistribution.NewsId);
                        //                _context.AddParameter(cmd2, "ItemStreamId", newsDistribution.ItemStreamId);
                        //                _context.AddParameter(cmd2, "SharedDate", newsDistribution.SharedDate);
                        //                _context.AddParameter(cmd2, "SharedZone", newsDistribution.SharedZone);
                        //                returnValue = await _context.ExecuteNonQueryAsync(cmd2) > 0;
                        //                if (!returnValue) break;
                        //            }
                        //        }
                        //    }
                        //    if (returnValue)
                        //    {
                        //        _context.CommitTransaction();
                        //    }
                        //    else
                        //    {
                        //        _context.RollbackTransaction();
                        //    }
                        //}
                    }
                    catch (Exception exx)
                    {
                        if (!isQueue) Logger.Sensitive(exx, exx.Message);
                        //_context.RollbackTransaction();
                    }
                    finally
                    {
                        _context.Close();
                    }
                }
            }
            catch (Exception ex)
            {
                if (!isQueue) Logger.Sensitive(ex, ex.Message);
            }
            return returnValue;
        }

        public async Task<bool> ShareAlbumAsync(ItemStreamDistribution itemStreamDistribution, List<AlbumSearch> listMedia, bool isQueue = false)
        {
            var returnValue = false;
            try
            {
                var listNewsDistribution = new List<NewsDistribution>();
                foreach (var media in listMedia)
                {
                    listNewsDistribution.AddRange(media.NewsDistribution);
                }

                using(var _context = GetContext())
                {
                    //_context.BeginTransaction();
                    try
                    {
                        var cmd = _context.CreateCommand(Scripts.Article.SaveItemStream);

                        _context.AddParameter(cmd, "Id", itemStreamDistribution.Id);
                        _context.AddParameter(cmd, "PublishStatus", itemStreamDistribution.PublishStatus);
                        _context.AddParameter(cmd, "CreatedDate", itemStreamDistribution.CreatedDate);
                        _context.AddParameter(cmd, "ModifiedDate", itemStreamDistribution.ModifiedDate);
                        _context.AddParameter(cmd, "ItemStreamTempId", itemStreamDistribution.ItemStreamTempId);
                        _context.AddParameter(cmd, "Note", itemStreamDistribution.Note);
                        _context.AddParameter(cmd, "Title", itemStreamDistribution.Title);
                        _context.AddParameter(cmd, "MetaData", itemStreamDistribution.MetaData);

                        _context.AddParameter(cmd, "ListNewsDistribution", string.Join("_", listNewsDistribution?.Select(p => p.ToString()) ?? new string[] { }));

                        returnValue = await _context.ExecuteNonQueryAsync(cmd) > 0;
                        //if (returnValue)
                        //{
                        //    foreach (var media in listMedia)
                        //    {
                        //        foreach (var newsDistribution in media.NewsDistribution.Where(p => p.ItemStreamId == itemStreamDistribution.Id))
                        //        {
                        //            if (newsDistribution.NewsId > 0)
                        //            {
                        //                var cmd2 = _context.CreateCommand(Scripts.Photo.InsertOrUpdateNewsDistribution);

                        //                _context.AddParameter(cmd2, "DistributorId", newsDistribution.DistributorId);
                        //                _context.AddParameter(cmd2, "NewsId", newsDistribution.NewsId);
                        //                _context.AddParameter(cmd2, "ItemStreamId", newsDistribution.ItemStreamId);
                        //                _context.AddParameter(cmd2, "SharedDate", newsDistribution.SharedDate);
                        //                _context.AddParameter(cmd2, "SharedZone", newsDistribution.SharedZone);
                        //                returnValue = await _context.ExecuteNonQueryAsync(cmd2) > 0;
                        //                if (!returnValue) break;
                        //            }
                        //        }
                        //    }
                        //    if (returnValue)
                        //    {
                        //        _context.CommitTransaction();
                        //    }
                        //    else
                        //    {
                        //        _context.RollbackTransaction();
                        //    }
                        //}
                    }
                    catch (Exception exx)
                    {
                        if (!isQueue) Logger.Sensitive(exx, exx.Message);
                        //_context.RollbackTransaction();
                    }
                    finally
                    {
                        _context.Close();
                    }
                }
            }
            catch (Exception ex)
            {
                if (!isQueue) Logger.Sensitive(ex, ex.Message);
            }
            return returnValue;
        }

        public async Task<bool> ShareGalleryAsync(ItemStreamDistribution itemStreamDistribution, List<GallerySearch> listMedia, bool isQueue = false)
        {
            var returnValue = false;
            try
            {
                var listNewsDistribution = new List<NewsDistribution>();
                foreach (var media in listMedia)
                {
                    listNewsDistribution.AddRange(media.NewsDistribution);
                }

                using (var _context = GetContext())
                {
                    //_context.BeginTransaction();
                    try
                    {
                        var cmd = _context.CreateCommand(Scripts.Article.SaveItemStream);

                        _context.AddParameter(cmd, "Id", itemStreamDistribution.Id);
                        _context.AddParameter(cmd, "PublishStatus", itemStreamDistribution.PublishStatus);
                        _context.AddParameter(cmd, "CreatedDate", itemStreamDistribution.CreatedDate);
                        _context.AddParameter(cmd, "ModifiedDate", itemStreamDistribution.ModifiedDate);
                        _context.AddParameter(cmd, "ItemStreamTempId", itemStreamDistribution.ItemStreamTempId);
                        _context.AddParameter(cmd, "Note", itemStreamDistribution.Note);
                        _context.AddParameter(cmd, "Title", itemStreamDistribution.Title);
                        _context.AddParameter(cmd, "MetaData", itemStreamDistribution.MetaData);

                        _context.AddParameter(cmd, "ListNewsDistribution", string.Join("_", listNewsDistribution?.Select(p => p.ToString()) ?? new string[] { }));

                        returnValue = await _context.ExecuteNonQueryAsync(cmd) > 0;
                        //if (returnValue)
                        //{
                        //    foreach (var media in listMedia)
                        //    {
                        //        foreach (var newsDistribution in media.NewsDistribution.Where(p => p.ItemStreamId == itemStreamDistribution.Id))
                        //        {
                        //            if (newsDistribution.NewsId > 0)
                        //            {
                        //                var cmd2 = _context.CreateCommand(Scripts.Photo.InsertOrUpdateNewsDistribution);

                        //                _context.AddParameter(cmd2, "DistributorId", newsDistribution.DistributorId);
                        //                _context.AddParameter(cmd2, "NewsId", newsDistribution.NewsId);
                        //                _context.AddParameter(cmd2, "ItemStreamId", newsDistribution.ItemStreamId);
                        //                _context.AddParameter(cmd2, "SharedDate", newsDistribution.SharedDate);
                        //                _context.AddParameter(cmd2, "SharedZone", newsDistribution.SharedZone);
                        //                returnValue = await _context.ExecuteNonQueryAsync(cmd2) > 0;
                        //                if (!returnValue) break;
                        //            }
                        //        }
                        //    }
                        //    if (returnValue)
                        //    {
                        //        _context.CommitTransaction();
                        //    }
                        //    else
                        //    {
                        //        _context.RollbackTransaction();
                        //    }
                        //}
                    }
                    catch (Exception exx)
                    {
                        if (!isQueue) Logger.Sensitive(exx, exx.Message);
                        //_context.RollbackTransaction();
                    }
                    finally
                    {
                        _context.Close();
                    }
                }
            }
            catch (Exception ex)
            {
                if (!isQueue) Logger.Sensitive(ex, ex.Message);
            }
            return returnValue;
        }
        public async Task<bool> SharePhotoAsync(ItemStreamDistribution itemStreamDistribution, List<PhotoSearch> listMedia, bool isQueue = false)
        {
            var returnValue = false;
            try
            {
                var listNewsDistribution = new List<NewsDistribution>();
                foreach (var media in listMedia)
                {
                    listNewsDistribution.AddRange(media.NewsDistribution);
                }

                using (var _context = GetContext())
                {
                    //_context.BeginTransaction();
                    try
                    {
                        var cmd = _context.CreateCommand(Scripts.Article.SaveItemStream);

                        _context.AddParameter(cmd, "Id", itemStreamDistribution.Id);
                        _context.AddParameter(cmd, "PublishStatus", itemStreamDistribution.PublishStatus);
                        _context.AddParameter(cmd, "CreatedDate", itemStreamDistribution.CreatedDate);
                        _context.AddParameter(cmd, "ModifiedDate", itemStreamDistribution.ModifiedDate);
                        _context.AddParameter(cmd, "ItemStreamTempId", itemStreamDistribution.ItemStreamTempId);
                        _context.AddParameter(cmd, "Note", itemStreamDistribution.Note);
                        _context.AddParameter(cmd, "Title", itemStreamDistribution.Title);
                        _context.AddParameter(cmd, "MetaData", itemStreamDistribution.MetaData);

                        _context.AddParameter(cmd, "ListNewsDistribution", string.Join("_", listNewsDistribution?.Select(p => p.ToString()) ?? new string[] { }));

                        returnValue = await _context.ExecuteNonQueryAsync(cmd) > 0;
                        //if (returnValue)
                        //{
                        //    foreach (var media in listMedia)
                        //    {
                        //        foreach (var newsDistribution in media.NewsDistribution.Where(p => p.ItemStreamId == itemStreamDistribution.Id))
                        //        {

                        //            if (newsDistribution.NewsId > 0)
                        //            {
                        //                var cmd2 = _context.CreateCommand(Scripts.Photo.InsertOrUpdateNewsDistribution);
                        //                _context.AddParameter(cmd2, "DistributorId", newsDistribution.DistributorId);
                        //                _context.AddParameter(cmd2, "NewsId", newsDistribution.NewsId);
                        //                _context.AddParameter(cmd2, "ItemStreamId", newsDistribution.ItemStreamId);
                        //                _context.AddParameter(cmd2, "SharedDate", newsDistribution.SharedDate);
                        //                _context.AddParameter(cmd2, "SharedZone", newsDistribution.SharedZone);
                        //                returnValue = await _context.ExecuteNonQueryAsync(cmd2) > 0;
                        //                if (!returnValue) break;
                        //            }
                        //        }
                        //    }
                        //    if (returnValue)
                        //    {
                        //        _context.CommitTransaction();
                        //    }
                        //    else
                        //    {
                        //        _context.RollbackTransaction();
                        //    }
                        //}
                    }
                    catch (Exception exx)
                    {
                        if (!isQueue) Logger.Sensitive(exx, exx.Message);
                        //_context.RollbackTransaction();
                    }
                    finally
                    {
                        _context.Close();
                    }
                }
            }
            catch (Exception ex)
            {
                if (!isQueue) Logger.Sensitive(ex, ex.Message);
            }
            return returnValue;
        }

        public async Task<bool> ShareMediaAsync(ItemStreamDistribution itemStreamDistribution, List<NewsDistribution> listNewsDistribution, bool isUpdate = false, bool isQueue = false)
        {
            var returnValue = false;
            try
            {
                using(var _context = GetContext())
                {
                    //_context.BeginTransaction();
                    try
                    {
                        string sqlScript = "";
                        if (isUpdate == false)
                        {
                            sqlScript = Scripts.Article.InsertItemStream;
                        }
                        else
                        {
                            sqlScript = Scripts.Article.UpdateItemStream;
                        }
                        var cmd = _context.CreateCommand(sqlScript);
                        _context.AddParameter(cmd, "Id", itemStreamDistribution.Id);
                        _context.AddParameter(cmd, "Note", itemStreamDistribution.Note);
                        _context.AddParameter(cmd, "PublishStatus", itemStreamDistribution.PublishStatus);
                        _context.AddParameter(cmd, "ItemStreamTempId", itemStreamDistribution.ItemStreamTempId);
                        _context.AddParameter(cmd, "Title", itemStreamDistribution.Title);
                        _context.AddParameter(cmd, "MetaData", itemStreamDistribution.MetaData);
                        _context.AddParameter(cmd, "CreatedDate", itemStreamDistribution.CreatedDate);
                        _context.AddParameter(cmd, "ModifiedDate", itemStreamDistribution.ModifiedDate);

                        _context.AddParameter(cmd, "ListNewsDistribution", string.Join("_", listNewsDistribution?.Select(p => p.ToString()) ?? new string[] { }));

                        returnValue = await _context.ExecuteNonQueryAsync(cmd) > 0;                        
                    }
                    catch (Exception exx)
                    {
                        if (!isQueue) Logger.Sensitive(exx, exx.Message);
                        //_context.RollbackTransaction();
                    }
                    finally
                    {
                        _context.Close();
                    }
                }
            }
            catch (Exception ex)
            {
                if (!isQueue) Logger.Sensitive(ex, ex.Message);
            }
            return returnValue;
        }
    }
}
