﻿using ChannelVN.IMS2.Core.Entities.Photo;
using ChannelVN.IMS2.Foundation.Data.Providers;
using ChannelVN.IMS2.Foundation.Logging;
using System;
using System.Threading.Tasks;

namespace ChannelVN.IMS2.Core.Models.DataStore.Photo
{
    public class PhotoDataStore : CmsMainDataStore
    {
        public async Task<bool> AddAsync(PhotoSearch data, bool isQueue = false)
        {
            var returnValue = false;
            try
            {
                using(var _context = GetContext())
                {
                    try
                    {
                        var cmd = _context.CreateCommand(Scripts.Photo.Add);

                        _context.AddParameter(cmd, "Id", data.Id);
                        _context.AddParameter(cmd, "Type", data.Type);
                        _context.AddParameter(cmd, "Url", data.Url);
                        _context.AddParameter(cmd, "OriginalUrl", data.OriginalUrl);
                        _context.AddParameter(cmd, "Status", data.Status);
                        _context.AddParameter(cmd, "Author", data.Author);
                        _context.AddParameter(cmd, "CategoryId", data.CategoryId);
                        _context.AddParameter(cmd, "CreatedDate", data.CreatedDate);
                        _context.AddParameter(cmd, "CreatedBy", data.CreatedBy);
                        _context.AddParameter(cmd, "Location", data.Location);
                        _context.AddParameter(cmd, "PublishMode", data.PublishMode);
                        _context.AddParameter(cmd, "PublishData", data.PublishData);
                        _context.AddParameter(cmd, "DistributionDate", data.DistributionDate);
                        _context.AddParameter(cmd, "Tags", data.Tags);
                        _context.AddParameter(cmd, "Caption", data.Caption);
                        _context.AddParameter(cmd, "CardType", data.CardType);
                        _context.AddParameter(cmd, "Size", data.Size);
                        _context.AddParameter(cmd, "Capacity", data.Capacity);
                        _context.AddParameter(cmd, "DistributorId", data.DistributorId);
                        _context.AddParameter(cmd, "ApprovedBy", data.ApprovedBy);
                        _context.AddParameter(cmd, "CommentMode", (Byte?)data.CommentMode);
                        _context.AddParameter(cmd, "LabelType", (int?)data.LabelType);
                        //NewsInAccount
                        _context.AddParameter(cmd, "AccountId", data.NewsInAccount[data.NewsInAccount.Length - 1].AccountId);
                        _context.AddParameter(cmd, "PublishedType", data.NewsInAccount[data.NewsInAccount.Length - 1].PublishedType);
                        _context.AddParameter(cmd, "PublishedDate", data.NewsInAccount[data.NewsInAccount.Length - 1].PublishedDate);

                        returnValue = await _context.ExecuteNonQueryAsync(cmd) > 0;

                    }
                    catch(Exception ex)
                    {
                        if (!isQueue) Logger.Sensitive(ex, ex.Message);
                    }
                    finally
                    {
                        _context.Close();
                    }
                }
            }
            catch (Exception ex)
            {
                if (!isQueue) Logger.Sensitive(ex, ex.Message);
            }
            return returnValue;
        }

        public async Task<bool> UpdateAsync(PhotoSearch data, bool isQueue = false)
        {
            var returnValue = false;
            try
            {
                using(var _context = GetContext())
                {
                    try
                    {
                        var cmd = _context.CreateCommand(Scripts.Photo.Update);

                        _context.AddParameter(cmd, "Id", data.Id);
                        _context.AddParameter(cmd, "Url", data.Url);
                        _context.AddParameter(cmd, "Author", data.Author);
                        _context.AddParameter(cmd, "CategoryId", data.CategoryId);
                        _context.AddParameter(cmd, "ModifiedBy", data.ModifiedBy);
                        _context.AddParameter(cmd, "ModifiedDate", data.ModifiedDate);
                        _context.AddParameter(cmd, "Location", data.Location);
                        _context.AddParameter(cmd, "PublishData", data.PublishData);
                        _context.AddParameter(cmd, "DistributionDate", data.DistributionDate);
                        _context.AddParameter(cmd, "Tags", data.Tags);
                        _context.AddParameter(cmd, "Caption", data.Caption);
                        _context.AddParameter(cmd, "Size", data.Size);
                        _context.AddParameter(cmd, "Capacity", data.Capacity);
                        _context.AddParameter(cmd, "ApprovedBy", data.ApprovedBy);
                        _context.AddParameter(cmd, "ApprovedDate", data.ApprovedDate);
                        _context.AddParameter(cmd, "CommentMode", (Byte?)data.CommentMode);
                        _context.AddParameter(cmd, "LabelType", (int?)data.LabelType);

                        returnValue = await _context.ExecuteNonQueryAsync(cmd) > 0;
                    }
                    catch (Exception ex)
                    {
                        if (!isQueue) Logger.Sensitive(ex, ex.Message);
                    }
                    finally
                    {
                        _context.Close();
                    }
                }
            }
            catch (Exception ex)
            {
                if (!isQueue) Logger.Sensitive(ex, ex.Message);
            }
            return returnValue;
        }

        public async Task<bool> UpdateStatusAsync(PhotoSearch data, bool isQueue = false)
        {
            var returnValue = false;
            try
            {
                using(var _context = GetContext())
                {
                    try
                    {
                        var cmd = _context.CreateCommand(Scripts.Photo.UpdateStatus);

                        _context.AddParameter(cmd, "Id", data.Id);
                        _context.AddParameter(cmd, "Status", data.Status);
                        _context.AddParameter(cmd, "ModifiedBy", data.ModifiedBy);
                        _context.AddParameter(cmd, "ModifiedDate", data.ModifiedDate);
                        _context.AddParameter(cmd, "DistributionDate", data.DistributionDate);
                        _context.AddParameter(cmd, "ApprovedBy", data.ApprovedBy);
                        _context.AddParameter(cmd, "ApprovedDate", data.ApprovedDate);
                        returnValue = await _context.ExecuteNonQueryAsync(cmd) > 0;
                    }
                    catch (Exception ex)
                    {
                        if (!isQueue) Logger.Sensitive(ex, ex.Message);
                    }
                    finally
                    {
                        _context.Close();
                    }
                }
            }
            catch (Exception ex)
            {
                if (!isQueue) Logger.Sensitive(ex, ex.Message);
            }
            return returnValue;
        }

        public async Task<PhotoUnit> GetByIdAsync(long id)
        {
            var returnValue = default(PhotoUnit);
            try
            {
                using(var _context = GetContext())
                {
                    try
                    {
                        var cmd = _context.CreateCommand(Scripts.Photo.GetById);
                        _context.AddParameter(cmd, "Id", id);

                        returnValue = await _context.GetAsync<PhotoUnit>(cmd);
                    }
                    catch (Exception ex)
                    {
                        Logger.Sensitive(ex, ex.Message);
                    }
                    finally
                    {
                        _context.Close();
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Sensitive(ex, ex.Message);
            }
            return returnValue;
        }
    }
}
