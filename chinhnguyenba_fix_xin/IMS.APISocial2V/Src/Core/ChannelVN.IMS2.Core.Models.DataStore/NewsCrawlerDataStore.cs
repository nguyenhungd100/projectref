﻿using ChannelVN.IMS2.Core.Entities;
using ChannelVN.IMS2.Foundation.Data.Providers;
using ChannelVN.IMS2.Foundation.Logging;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Threading.Tasks;

namespace ChannelVN.IMS2.Core.Models.DataStore
{
    public class NewsCrawlerDataStore : CmsMainDataStore
    {
        private MssqlContext _context = null;

        public NewsCrawlerDataStore() => _context = GetContext();

        public async Task<bool> AddAsync(List<NewsCrawler> data, bool isQueue = false)
        {
            var returnValue = false;
            try
            {
                using(var _context = GetContext())
                {
                    try
                    {                                         
                        foreach (var item in data) {
                            var cmd = _context.CreateCommand(Scripts.NewsCrawler.Add);
                            _context.AddParameter(cmd, "Id", item.Id);
                            _context.AddParameter(cmd, "Source", item.Source);
                            _context.AddParameter(cmd, "Link", item.Link);
                            _context.AddParameter(cmd, "Title", item.Title);
                            _context.AddParameter(cmd, "Avatar", item.Avatar);
                            _context.AddParameter(cmd, "Description", item.Description);
                            _context.AddParameter(cmd, "CrawledBy", item.CrawledBy);
                            _context.AddParameter(cmd, "CrawledDate", item.CrawledDate);
                            _context.AddParameter(cmd, "Status", item.Status);
                            _context.AddParameter(cmd, "Sapo", item.Sapo);
                            _context.AddParameter(cmd, "OfficerId", item.OfficerId);
                            await _context.ExecuteNonQueryAsync(cmd as SqlCommand);
                        }
                        returnValue = true;
                    }
                    catch(Exception ex)
                    {                        
                        if (!isQueue) Logger.Sensitive(ex, ex.Message);
                    }
                    finally
                    {
                        _context.Close();
                    }
                }
            }
            catch (Exception ex)
            {                
                if (!isQueue) Logger.Sensitive(ex, ex.Message);
            }
            return returnValue;
        }
        public async Task<bool> UpdateAsync(NewsCrawler data, bool isQueue = false)
        {
            var returnValue = false;
            try
            {
                using(var _context = GetContext())
                {
                    try
                    {
                        var cmd = _context.CreateCommand(Scripts.NewsCrawler.Update);
                        _context.AddParameter(cmd, "Id", data.Id);
                        _context.AddParameter(cmd, "Source", data.Source);
                        _context.AddParameter(cmd, "Link", data.Link);
                        _context.AddParameter(cmd, "Title", data.Title);
                        _context.AddParameter(cmd, "Avatar", data.Avatar);
                        _context.AddParameter(cmd, "Description", data.Description);                        
                        _context.AddParameter(cmd, "Status", data.Status);
                        _context.AddParameter(cmd, "Sapo", data.Sapo);
                        _context.AddParameter(cmd, "ScheduleBy", data.ScheduleBy);
                        _context.AddParameter(cmd, "ScheduleDate", data.ScheduleDate);
                        _context.AddParameter(cmd, "MediaId", data.MediaId);

                        returnValue = await _context.ExecuteNonQueryAsync(cmd as SqlCommand) > 0;
                    }
                    catch (Exception ex)
                    {
                        if (!isQueue) Logger.Sensitive(ex, ex.Message);
                    }
                    finally
                    {
                        _context.Close();
                    }
                }
            }
            catch (Exception ex)
            {
                if (!isQueue) Logger.Sensitive(ex, ex.Message);
            }
            return returnValue;
        }

        public async Task<int> CountTagAsync()
        {
            var returnValue = new TotalCount();
            try
            {
                using(var _context = GetContext())
                {
                    try
                    {
                        var cmd = _context.CreateCommand(Scripts.VideoTag.CountTag);
                        returnValue = await _context.GetAsync<TotalCount>(cmd);
                    }
                    catch (Exception ex)
                    {
                        Logger.Sensitive(ex, ex.Message);
                    }
                    finally
                    {
                        _context.Close();
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Sensitive(ex, ex.Message);
            }
            return returnValue.Total;
        }

        public async Task<List<Tag>> InitAllVideoTagAsync(int page, int pageSize, DateTime startDate, DateTime endDate)
        {
            var returnValue = default(List<Tag>);
            try
            {
                using(var _context = GetContext())
                {
                    try
                    {
                        var cmd = _context.CreateCommand(Scripts.VideoTag.InitAllVideoTag);
                        _context.AddParameter(cmd, "DateFrom", startDate);
                        _context.AddParameter(cmd, "DateTo", endDate);
                        _context.AddParameter(cmd, "PageIndex", page);
                        _context.AddParameter(cmd, "PageSize", pageSize);

                        returnValue = await _context.GetListAsync<Tag>(cmd);
                    }
                    catch (Exception ex)
                    {
                        Logger.Sensitive(ex, ex.Message);
                    }
                    finally
                    {
                        _context.Close();
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Sensitive(ex, ex.Message);
            }
            return returnValue;
        }
    }
}
