﻿using ChannelVN.IMS2.Core.Entities;
using ChannelVN.IMS2.Core.Entities.Distribution;
using ChannelVN.IMS2.Core.Entities.Newsletters;
using ChannelVN.IMS2.Foundation.Logging;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ChannelVN.IMS2.Core.Models.DataStore.Articles
{
    public class ArticleDataStore : CmsMainDataStore
    {
        public async Task<bool> AddAsync(ArticleSearch data, bool isQueue = false)
        {
            var returnValue = false;
            try
            {
                using(var _context=GetContext())
                {
                    try
                    {
                        var cmd = _context.CreateCommand(Scripts.Article.Add);

                        _context.AddParameter(cmd, "Id", data.Id);
                        _context.AddParameter(cmd, "Type", data.Type);
                        _context.AddParameter(cmd, "Url", data.Url);
                        _context.AddParameter(cmd, "OriginalUrl", data.OriginalUrl);
                        _context.AddParameter(cmd, "Status", data.Status);
                        _context.AddParameter(cmd, "Author", data.Author);
                        _context.AddParameter(cmd, "CategoryId", data.CategoryId);
                        _context.AddParameter(cmd, "CreatedDate", data.CreatedDate);
                        _context.AddParameter(cmd, "CreatedBy", data.CreatedBy);
                        _context.AddParameter(cmd, "Location", data.Location);
                        _context.AddParameter(cmd, "PublishMode", data.PublishMode);
                        _context.AddParameter(cmd, "PublishData", data.PublishData);
                        _context.AddParameter(cmd, "DistributionDate", data.DistributionDate);
                        _context.AddParameter(cmd, "DistributorId", data.DistributorId);
                        _context.AddParameter(cmd, "ModifiedDate", data.ModifiedDate);
                        _context.AddParameter(cmd, "ModifiedBy", data.ModifiedBy);
                        _context.AddParameter(cmd, "Tags", data.Tags);
                        _context.AddParameter(cmd, "CardType", data.CardType);
                        _context.AddParameter(cmd, "Title", data.Title);
                        _context.AddParameter(cmd, "Sapo", data.Sapo);
                        _context.AddParameter(cmd, "SubTitle", data.SubTitle);
                        _context.AddParameter(cmd, "Avatar1", data.Avatar1);
                        _context.AddParameter(cmd, "Avatar2", data.Avatar2);
                        _context.AddParameter(cmd, "Avatar3", data.Avatar3);
                        _context.AddParameter(cmd, "ParentId", data.ParentId);
                        _context.AddParameter(cmd, "TemplateId", data.TemplateId);
                        _context.AddParameter(cmd, "Body", data.Body);
                        _context.AddParameter(cmd, "BodyMeta", data.BodyMeta);
                        _context.AddParameter(cmd, "ApprovedBy", data.ApprovedBy);
                        _context.AddParameter(cmd, "InstantViewId", data.InstantViewId);
                        _context.AddParameter(cmd, "IsInstantView", data.IsInstantView);
                        _context.AddParameter(cmd, "InstantViewUrl", data.InstantViewUrl);
                        _context.AddParameter(cmd, "CommentMode", (Byte?)data.CommentMode);
                        _context.AddParameter(cmd, "NativeContent", data.NativeContent);

                        //newsseometa
                        //newsinaccount
                        _context.AddParameter(cmd, "AccountId", data.NewsInAccount[0].AccountId);
                        _context.AddParameter(cmd, "PublishedDate", data.NewsInAccount[0].PublishedDate);
                        _context.AddParameter(cmd, "PublishedType", data.NewsInAccount[0].PublishedType);

                        returnValue = await _context.ExecuteNonQueryAsync(cmd) > 0;

                    }
                    catch(Exception ex)
                    {
                        if (!isQueue) Logger.Sensitive(ex, ex.Message);
                    }
                    finally
                    {
                        _context.Close();
                    }
                }
            }
            catch (Exception ex)
            {
                if (!isQueue) Logger.Sensitive(ex, ex.Message);
            }
            return returnValue;
        }

        public async Task<bool> UpdateAsync(ArticleSearch data, bool isQueue = false)
        {
            var returnValue = false;
            try
            {
                using(var _context=GetContext())
                {
                    try
                    {
                        var cmd = _context.CreateCommand(Scripts.Article.Update);

                        _context.AddParameter(cmd, "Id", data.Id);
                        _context.AddParameter(cmd, "Url", data.Url);
                        _context.AddParameter(cmd, "OriginalUrl", data.OriginalUrl);
                        _context.AddParameter(cmd, "Author", data.Author);
                        _context.AddParameter(cmd, "CategoryId", data.CategoryId);
                        _context.AddParameter(cmd, "Location", data.Location);
                        _context.AddParameter(cmd, "PublishMode", data.PublishMode);

                        _context.AddParameter(cmd, "PublishData", data.PublishData);
                        _context.AddParameter(cmd, "DistributionDate", data.DistributionDate);
                        _context.AddParameter(cmd, "ModifiedDate", data.ModifiedDate);
                        _context.AddParameter(cmd, "ModifiedBy", data.ModifiedBy);
                        _context.AddParameter(cmd, "Tags", data.Tags);
                        _context.AddParameter(cmd, "Title", data.Title);
                        _context.AddParameter(cmd, "Sapo", data.Sapo);

                        _context.AddParameter(cmd, "SubTitle", data.SubTitle);
                        _context.AddParameter(cmd, "Avatar1", data.Avatar1);
                        _context.AddParameter(cmd, "Avatar2", data.Avatar2);
                        _context.AddParameter(cmd, "Avatar3", data.Avatar3);
                        _context.AddParameter(cmd, "ParentId", data.ParentId);
                        _context.AddParameter(cmd, "TemplateId", data.TemplateId);
                        _context.AddParameter(cmd, "Body", data.Body);
                        _context.AddParameter(cmd, "BodyMeta", data.BodyMeta);
                        _context.AddParameter(cmd, "ApprovedDate", data.ApprovedDate);
                        _context.AddParameter(cmd, "ApprovedBy", data.ApprovedBy);
                        _context.AddParameter(cmd, "InstantViewId", data.InstantViewId);
                        _context.AddParameter(cmd, "IsInstantView", data.IsInstantView);
                        _context.AddParameter(cmd, "InstantViewUrl", data.InstantViewUrl);
                        _context.AddParameter(cmd, "CommentMode", (Byte?)data.CommentMode);
                        _context.AddParameter(cmd, "NativeContent", data.NativeContent);

                        returnValue = await _context.ExecuteNonQueryAsync(cmd) > 0;

                    }
                    catch (Exception ex)
                    {
                        if (!isQueue) Logger.Sensitive(ex, ex.Message);
                    }
                    finally
                    {
                        _context.Close();
                    }
                }
            }
            catch (Exception ex)
            {
                if (!isQueue) Logger.Sensitive(ex, ex.Message);
            }
            return returnValue;
        }
        public async Task<bool> UpdateStatusAsync(ArticleSearch data, bool isQueue = false)
        {
            var returnValue = false;
            try
            {
                using(var _context=GetContext())
                {
                    try
                    {
                        var cmd = _context.CreateCommand(Scripts.Article.UpdateStatus);

                        _context.AddParameter(cmd, "Id", data.Id);
                        _context.AddParameter(cmd, "Status", data.Status);
                        _context.AddParameter(cmd, "ModifiedDate", data.ModifiedDate);
                        _context.AddParameter(cmd, "ModifiedBy", data.ModifiedBy);
                        _context.AddParameter(cmd, "DistributionDate", data.DistributionDate);
                        _context.AddParameter(cmd, "ApprovedDate", data.ApprovedDate);
                        _context.AddParameter(cmd, "ApprovedBy", data.ApprovedBy);
                        returnValue = await _context.ExecuteNonQueryAsync(cmd) > 0;

                    }
                    catch (Exception ex)
                    {
                        if (!isQueue) Logger.Sensitive(ex, ex.Message);
                    }
                    finally
                    {
                        _context.Close();
                    }
                }
            }
            catch (Exception ex)
            {
                if (!isQueue) Logger.Sensitive(ex, ex.Message);
            }
            return returnValue;
        }
        public async Task<Article> GetByIdAsync(long id)
        {
            var returnValue = default(Article);
            try
            {
                using (var _context = GetContext())
                {
                    try
                    {
                        var cmd = _context.CreateCommand(Scripts.Article.GetById);
                        _context.AddParameter(cmd, "Id", id);
                        returnValue = await _context.GetAsync<Article>(cmd);

                    }
                    catch (Exception ex)
                    {
                        Logger.Sensitive(ex, ex.Message);
                    }
                    finally
                    {
                        _context.Close();
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Sensitive(ex, ex.Message);
            }
            return returnValue;
        }

        //NewsInTag
        public async Task<bool> AddNewsInTagAsync(long newsId, long tagId, int priority)
        {
            var returnValue = false;
            try
            {
                using(var _context=GetContext())
                {
                    try
                    {
                        var cmd = _context.CreateCommand(Scripts.Article.AddNewsInTag);
                        _context.AddParameter(cmd, "NewsId", newsId);
                        _context.AddParameter(cmd, "TagId", tagId);
                        _context.AddParameter(cmd, "Priority", priority);
                        returnValue = await _context.ExecuteNonQueryAsync(cmd) > 0;
                    }
                    catch (Exception ex)
                    {
                        Logger.Sensitive(ex, ex.Message);
                    }
                    finally
                    {
                        _context.Close();
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Sensitive(ex, ex.Message);
            }
            return returnValue;
        }

        public async Task<bool> DeleteNewsInTagAsync(long newsId)
        {
            var returnValue = false;
            try
            {
                using(var _context=GetContext())
                {
                    try
                    {
                        var cmd = _context.CreateCommand(Scripts.Article.DeleteNewsInTag);
                        _context.AddParameter(cmd, "NewsId", newsId);
                        returnValue = await _context.ExecuteNonQueryAsync(cmd) > 0;
                    }
                    catch (Exception ex)
                    {
                        Logger.Sensitive(ex, ex.Message);
                    }
                    finally
                    {
                        _context.Close();
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Sensitive(ex, ex.Message);
            }
            return returnValue;
        }

        public async Task<bool> PublishAsync(ArticleSearch obj)
        {
            var returnValue = false;
            try
            {
                using(var _context=GetContext())
                {
                    try
                    {
                        var cmd = _context.CreateCommand(Scripts.Article.Publish);

                        _context.AddParameter(cmd, "Id", obj.Id);
                        _context.AddParameter(cmd, "Status", (int)NewsStatus.Published);
                        _context.AddParameter(cmd, "CreatedDate", obj.CreatedDate);
                        _context.AddParameter(cmd, "DistributionDate", obj.DistributionDate);
                        _context.AddParameter(cmd, "Url", obj.Url);
                        _context.AddParameter(cmd, "OriginalUrl", obj.OriginalUrl);
                        returnValue = await _context.ExecuteNonQueryAsync(cmd) > 0;
                    }
                    catch (Exception ex)
                    {
                        Logger.Sensitive(ex, ex.Message);
                    }
                    finally
                    {
                        _context.Close();
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Sensitive(ex, ex.Message);
            }
            return returnValue;
        }


        public async Task<bool> AcceptAsync(ItemStreamDistribution itemStreamId, bool isQueue = false)
        {
            var returnValue = false;
            try
            {
                using(var _context=GetContext())
                {
                    try
                    {
                        var cmd = _context.CreateCommand(Scripts.Article.AcceptItemStream);

                        _context.AddParameter(cmd, "Id", itemStreamId.Id);
                        _context.AddParameter(cmd, "PublishStatus", itemStreamId.PublishStatus);
                        _context.AddParameter(cmd, "AcceptedDate", itemStreamId.AcceptedDate);
                        _context.AddParameter(cmd, "Note", itemStreamId.Note);
                        _context.AddParameter(cmd, "ModifiedDate", itemStreamId.ModifiedDate);
                        returnValue = await _context.ExecuteNonQueryAsync(cmd) > 0;
                    }
                    catch (Exception ex)
                    {
                        Logger.Sensitive(ex, ex.Message);
                    }
                    finally
                    {
                        _context.Close();
                    }
                }
            }
            catch (Exception ex)
            {
                if (!isQueue) Logger.Sensitive(ex, ex.Message);
            }
            return returnValue;
        }

        public async Task<bool> RejectAsync(long itemStreamId, bool isQueue = false)
        {
            var returnValue = false;
            try
            {
                using(var _context=GetContext())
                {
                    try
                    {
                        var cmd = _context.CreateCommand(Scripts.Article.RejectItemStream);

                        _context.AddParameter(cmd, "Id", itemStreamId);
                        _context.AddParameter(cmd, "PublishStatus", (int)NewsDistributionPublishStatus.Reject);

                        returnValue = await _context.ExecuteNonQueryAsync(cmd) > 0;
                    }
                    catch (Exception ex)
                    {
                        Logger.Sensitive(ex, ex.Message);
                    }
                    finally
                    {
                        _context.Close();
                    }
                }
            }
            catch (Exception ex)
            {
                if (!isQueue) Logger.Sensitive(ex, ex.Message);
            }
            return returnValue;
        }

        public async Task<int> CountNewsAsync()
        {
            var returnValue = new TotalCount();
            try
            {
                using(var _context=GetContext())
                {
                    try
                    {
                        var cmd = _context.CreateCommand(Scripts.Article.CountNews);
                        returnValue = await _context.GetAsync<TotalCount>(cmd);
                    }
                    catch (Exception ex)
                    {
                        Logger.Sensitive(ex, ex.Message);
                    }
                    finally
                    {
                        _context.Close();
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Sensitive(ex, ex.Message);
            }
            return returnValue.Total;
        }

        public async Task<int> CountItemStreamAsync()
        {
            var returnValue = new TotalCount();
            try
            {
                using(var _context=GetContext())
                {
                    try
                    {
                        var cmd = _context.CreateCommand(Scripts.Article.CountItemStream);
                        returnValue = await _context.GetAsync<TotalCount>(cmd);
                    }
                    catch (Exception ex)
                    {
                        Logger.Sensitive(ex, ex.Message);
                    }
                    finally
                    {
                        _context.Close();
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Sensitive(ex, ex.Message);
            }
            return returnValue.Total;
        }


        public async Task<List<ArticleSearch>> InitAllNewsAsync(int page, int pageSize, DateTime startDate, DateTime endDate)
        {
            var returnValue = default(List<ArticleSearch>);
            try
            {
                using(var _context=GetContext())
                {
                    try
                    {
                        var cmd = _context.CreateCommand(Scripts.Article.InitAllNews);
                        _context.AddParameter(cmd, "DateFrom", startDate);
                        _context.AddParameter(cmd, "DateTo", endDate);
                        _context.AddParameter(cmd, "PageIndex", page);
                        _context.AddParameter(cmd, "PageSize", pageSize);

                        returnValue = await _context.GetListAsync<ArticleSearch>(cmd);
                    }
                    catch (Exception ex)
                    {
                        Logger.Sensitive(ex, ex.Message);
                    }
                    finally
                    {
                        _context.Close();
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Sensitive(ex, ex.Message);
            }
            return returnValue;
        }

        public async Task<List<ItemStreamDistribution>> InitAllItemStreamAsync(int page, int pageSize, DateTime startDate, DateTime endDate)
        {
            var returnValue = default(List<ItemStreamDistribution>);
            try
            {
                using(var _context=GetContext())
                {
                    try
                    {
                        var cmd = _context.CreateCommand(Scripts.Article.InitAllItemStream);
                        _context.AddParameter(cmd, "DateFrom", startDate);
                        _context.AddParameter(cmd, "DateTo", endDate);
                        _context.AddParameter(cmd, "PageIndex", page);
                        _context.AddParameter(cmd, "PageSize", pageSize);

                        returnValue = await _context.GetListAsync<ItemStreamDistribution>(cmd);
                    }
                    catch (Exception ex)
                    {
                        Logger.Sensitive(ex, ex.Message);
                    }
                    finally
                    {
                        _context.Close();
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Sensitive(ex, ex.Message);
            }
            return returnValue;
        }

        public async Task<List<NewsInAccount>> GetNewsInAccountByNewsIdAsync(long newsId)
        {
            var returnValue = default(List<NewsInAccount>);
            try
            {
                using(var _context=GetContext())
                {
                    try
                    {
                        var cmd = _context.CreateCommand(Scripts.Article.GetNewsInAccountByNewsId);
                        _context.AddParameter(cmd, "NewsId", newsId);

                        returnValue = await _context.GetListAsync<NewsInAccount>(cmd);
                    }
                    catch (Exception ex)
                    {
                        Logger.Sensitive(ex, ex.Message);
                    }
                    finally
                    {
                        _context.Close();
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Sensitive(ex, ex.Message);
            }
            return returnValue;
        }
        public async Task<List<NewsDistribution>> GetNewsDistributionByNewsIdAsync(long newsId)
        {
            var returnValue = default(List<NewsDistribution>);
            try
            {
                using(var _context=GetContext())
                {
                    try
                    {
                        var cmd = _context.CreateCommand(Scripts.Article.GetDistributionByNewsId);
                        _context.AddParameter(cmd, "NewsId", newsId);

                        returnValue = await _context.GetListAsync<NewsDistribution>(cmd);
                    }
                    catch (Exception ex)
                    {
                        Logger.Sensitive(ex, ex.Message);
                    }
                    finally
                    {
                        _context.Close();
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Sensitive(ex, ex.Message);
            }
            return returnValue;
        }


        public async Task<List<NewsDistribution>> GetNewsDistributionByItemStreamIdAsync(long itemStreamId)
        {
            var returnValue = default(List<NewsDistribution>);
            try
            {
                using(var _context=GetContext())
                {
                    try
                    {
                        var cmd = _context.CreateCommand(Scripts.Article.GetDistributionByItemStreamId);
                        _context.AddParameter(cmd, "ItemStreamId", itemStreamId);

                        returnValue = await _context.GetListAsync<NewsDistribution>(cmd);
                    }
                    catch (Exception ex)
                    {
                        Logger.Sensitive(ex, ex.Message);
                    }
                    finally
                    {
                        _context.Close();
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Sensitive(ex, ex.Message);
            }
            return returnValue;
        }
    }
}
