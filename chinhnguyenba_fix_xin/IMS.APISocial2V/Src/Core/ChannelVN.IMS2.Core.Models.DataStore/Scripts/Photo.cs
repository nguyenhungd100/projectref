﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ChannelVN.IMS2.Core.Models.DataStore.Scripts
{
    internal class Photo
    {
        public const string Add = @"BEGIN TRANSACTION  ; 
            INSERT INTO [Photo]            
                ([Id]
                  ,[Type]
                  ,[Url]
                  ,[OriginalUrl]
                  ,[Status]
                  ,[Author]
                  ,[CategoryId]
                  ,[CreatedDate]
                  ,[CreatedBy]
                  ,[Location]
                  ,[PublishMode]
                  ,[PublishData]
                  ,[DistributionDate]                  
                  ,[Tags]                  
                  ,[Caption]
                  ,[CardType]
                ,[Size]
                ,[Capacity]
                ,[DistributorId]
                ,[ApprovedBy]
                ,[CommentMode]
                )
            VALUES
                (@Id
                  ,@Type
                  ,CASE WHEN COL_LENGTH('Photo','Url')=-1 THEN @Url ELSE LEFT(@Url,COL_LENGTH('Photo','Url')/2) END
                  ,CASE WHEN COL_LENGTH('Photo','OriginalUrl')=-1 THEN @OriginalUrl ELSE LEFT(@OriginalUrl,COL_LENGTH('Photo','OriginalUrl')/2) END
                  ,@Status
                  ,CASE WHEN COL_LENGTH('Photo','Author')=-1 THEN @Author ELSE LEFT(@Author,COL_LENGTH('Photo','Author')/2) END
                  ,@CategoryId
                  ,@CreatedDate
                  ,@CreatedBy
                  ,CASE WHEN COL_LENGTH('Photo','Location')=-1 THEN @Location ELSE LEFT(@Location,COL_LENGTH('Photo','Location')/2) END
                  ,@PublishMode
                  ,@PublishData
                  ,@DistributionDate                  
                  ,@Tags                  
                  ,@Caption
                  ,@CardType
                ,CASE WHEN COL_LENGTH('Photo','Size')=-1 THEN @Size ELSE LEFT(@Size,COL_LENGTH('Photo','Size')) END
                ,@Capacity
                ,@DistributorId
                ,@ApprovedBy
                ,@CommentMode);

            IF(@@ROWCOUNT<>0) 
                BEGIN
                    INSERT INTO [NewsInAccount]
                    ([AccountId]
                      ,[NewsId]
                      ,[PublishedDate]
                      ,[PublishedType])
                    VALUES
                    (@AccountId
                      ,@Id
                      ,@PublishedDate
                      ,@PublishedType);
                    IF(@@ROWCOUNT<>0)
                        COMMIT;
                    ELSE ROLLBACK;
                END
            ELSE ROLLBACK;
            ";

        public const string Update = @"UPDATE [Photo] 
                  SET [Url]=CASE WHEN COL_LENGTH('Photo','Url')=-1 THEN @Url ELSE LEFT(@Url,COL_LENGTH('Photo','Url')/2) END
                    ,[Author]=CASE WHEN COL_LENGTH('Photo','Author')=-1 THEN @Author ELSE LEFT(@Author,COL_LENGTH('Photo','Author')/2) END                    
                    ,[CategoryId]=@CategoryId
                    ,[Location]=CASE WHEN COL_LENGTH('Photo','Location')=-1 THEN @Location ELSE LEFT(@Location,COL_LENGTH('Photo','Location')/2) END
                    ,[PublishData]=@PublishData
                    ,[DistributionDate]=@DistributionDate
                    ,[ModifiedBy]=@ModifiedBy
                    ,[ModifiedDate]=@ModifiedDate
                    ,[Tags]=@Tags                      
                    ,[Caption]=@Caption 
                    ,[Size]=CASE WHEN COL_LENGTH('Photo','Size')=-1 THEN @Size ELSE LEFT(@Size,COL_LENGTH('Photo','Size')) END 
                    ,[Capacity]=@Capacity 
                    ,[ApprovedBy]=@ApprovedBy
                    ,[ApprovedDate]=@ApprovedDate
                    ,[CommentMode]=@CommentMode
            WHERE [Id]=@Id                
            ";

        public const string UpdateStatus = @"UPDATE [Photo] 
                SET [Status]=@Status,
                    [ModifiedBy]=@ModifiedBy
                   ,[ModifiedDate]=@ModifiedDate
                    ,[DistributionDate]=@DistributionDate
                    ,[ApprovedBy]=@ApprovedBy
                    ,[ApprovedDate]=@ApprovedDate
            WHERE [Id]=@Id ";

        public const string GetById = @"SELECT * FROM [Photo] WHERE [Id]=@Id";

        public const string CountPhoto = @"SELECT count(Id) as Total from Photo ";

        public const string InsertOrUpdateNewsDistribution = @"
            IF EXISTS (SELECT 1 FROM [NewsDistribution] WHERE [NewsId]=@NewsId AND [ItemStreamId]=@ItemStreamId)
            UPDATE [NewsDistribution]
            SET [SharedDate]=@SharedDate,
                [SharedZone]=@SharedZone
            WHERE [DistributorId]=@DistributorId AND [NewsId]=@NewsId AND [ItemStreamId]=@ItemStreamId
            ELSE
            INSERT INTO [NewsDistribution]
                ([DistributorId]
                  ,[NewsId]                               
                  ,[ItemStreamId]
                  ,[SharedDate]
                  ,[SharedZone])
            VALUES
                (@DistributorId
                ,@NewsId                              
                ,@ItemStreamId
                ,@SharedDate
                ,@SharedZone) 
            ";

        public const string InitAllPhoto = @"DECLARE @UpperBand int, @LowerBand int
                                        
                                        SET @LowerBand  = (@PageIndex - 1) * @PageSize
                                        SET @UpperBand  = (@PageIndex * @PageSize)
                                        SELECT * FROM (
                                        SELECT d.* ,
                                        ROW_NUMBER() OVER(ORDER BY d.Id DESC) AS RowNumber 
                                        FROM Photo d 
                                        where (@DateFrom is null and (1=1)) or (@DateFrom is not null and (CONVERT(datetime,d.CreatedDate) >= CONVERT(datetime,@DateFrom) AND CONVERT(datetime,d.CreatedDate)<=CONVERT(datetime,@DateTo)))
                                        ) AS temp
                                        WHERE RowNumber > @LowerBand AND RowNumber <= @UpperBand";
    }
}
