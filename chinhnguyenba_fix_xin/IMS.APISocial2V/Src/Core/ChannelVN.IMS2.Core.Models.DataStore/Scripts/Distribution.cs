
using System;
using System.Collections.Generic;
using System.Text;

namespace ChannelVN.IMS2.Core.Models.DataStore.Scripts
{
    internal class Distribution
    {
        public const string Add = @"INSERT INTO [Distributor]
                ([Id]
                  ,[Name]
                  ,[Description]
                  ,[Status]
                  ,[Priority]                  
                  ,[CreatedDate]
                  ,[CreatedBy]                  
                ,[Code]                
                )
            VALUES
                (@Id
                ,@Name
                ,@Description
                ,@Status
                ,@Priority                
                ,@CreatedDate
                ,@CreatedBy                
                ,@Code
                )

            INSERT INTO [DistributorConfiguration]
                ([Id]                  
                  ,[QuotaNormal]
                  ,[QuotaProfessional]
                  ,[UnitTime]
                  ,[UnitAmount]
                  ,[ApiZoneVideo]
                  ,[ApiVideo]               
                ,[ApiVideoPlaylist]
                ,[ApiMediaUnit]
                )
            VALUES
                (@Id                
                ,@QuotaNormal
                ,@QuotaProfessional
                ,@UnitTime
                ,@UnitAmount
                ,@ApiZoneVideo
                ,@ApiVideo                
                ,@ApiVideoPlaylist
                ,@ApiMediaUnit)
            ";

        public const string Update = @"UPDATE [Distributor] 
                  SET [Name]=@Name
                  ,[Description]=@Description
                  ,[Status]=@Status
                  ,[Priority]=@Priority                  
                    ,[Code]=@Code                    
            WHERE [Id]=@Id  

            UPDATE [DistributorConfiguration] 
                 SET [QuotaNormal]=@QuotaNormal
                    ,[QuotaProfessional]=@QuotaProfessional
                    ,[UnitTime]=@UnitTime
                    ,[UnitAmount]=@UnitAmount
                    ,[ApiZoneVideo]=@ApiZoneVideo
                    ,[ApiVideo]=@ApiVideo                    
                    ,[ApiVideoPlaylist]=@ApiVideoPlaylist
                    ,[ApiMediaUnit]=@ApiMediaUnit
            WHERE [Id]=@Id  
            ";

        public const string GetById = @"SELECT * FROM [Distributor] WHERE [Id]=@Id";

        public const string InitNewsDistribution = @"
            IF EXISTS(SELECT 1 FROM NewsDistribution WHERE [DistributorId]=@DistributorId AND [NewsId]=@NewsId AND [ItemStreamId]=@ItemStreamId)
            BEGIN
                UPDATE [NewsDistribution]
                SET [SharedDate]=@SharedDate
                    ,[SharedZone]=@SharedZone
                WHERE [DistributorId]=@DistributorId AND [NewsId]=@NewsId AND [ItemStreamId]=@ItemStreamId;
            END
            ELSE
                INSERT INTO [NewsDistribution]
                    ([DistributorId]
                      ,[NewsId]                               
                      ,[ItemStreamId]
                      ,[SharedDate]
                      ,[SharedZone])
                VALUES
                    (@DistributorId
                    ,@NewsId                              
                    ,@ItemStreamId
                    ,@SharedDate
                    ,@SharedZone);
                ";
    }
}
