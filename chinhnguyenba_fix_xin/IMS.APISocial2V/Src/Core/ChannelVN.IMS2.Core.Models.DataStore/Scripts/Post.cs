﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ChannelVN.IMS2.Core.Models.DataStore.Scripts
{
    public class Post
    {
        public const string Add = @"
            BEGIN TRANSACTION  ;     
         INSERT INTO [Post](
            [Id]
           ,[Type]
           ,[Url]
           ,[OriginalUrl]
           ,[Status]
           ,[Author]
           ,[CategoryId]
           ,[CreatedDate]
           ,[CreatedBy]
           ,[Location]
           ,[PublishMode]
           ,[PublishData]
           ,[DistributionDate]
            ,[DistributorId]
           ,[Tags]
           ,[CardType]
            ,[Caption]
            ,[ApprovedBy]
            ,[CommentMode]
        ) VALUES (
            @Id
           ,@Type
           ,CASE WHEN COL_LENGTH('Post','Url')=-1 THEN @Url ELSE LEFT(@Url,COL_LENGTH('Post','Url')/2) END
           ,CASE WHEN COL_LENGTH('Post','OriginalUrl')=-1 THEN @OriginalUrl ELSE LEFT(@OriginalUrl,COL_LENGTH('Post','OriginalUrl')/2) END
           ,@Status
           ,CASE WHEN COL_LENGTH('Post','Author')=-1 THEN @Author ELSE LEFT(@Author,COL_LENGTH('Post','Author')/2) END
           ,@CategoryId
           ,@CreatedDate
           ,@CreatedBy
           ,CASE WHEN COL_LENGTH('Post','Location')=-1 THEN @Location ELSE LEFT(@Location,COL_LENGTH('Post','Location')/2) END
           ,@PublishMode
           ,@PublishData
           ,@DistributionDate
           ,@DistributorId
           ,@Tags
           ,@CardType
            ,@Caption
            ,@ApprovedBy
            ,@CommentMode
        );
        
        IF(@@ROWCOUNT<>0) 
                INSERT INTO [NewsInAccount]
                ([AccountId]
                  ,[NewsId]
                  ,[PublishedDate]
                  ,[PublishedType])
                VALUES
                (@AccountId
                  ,@Id
                  ,@PublishedDate
                  ,@PublishedType) ;
        IF(@@ROWCOUNT<>0) 
            BEGIN
                IF(LEN(@Tags)>0)
                    BEGIN
                        INSERT INTO [NewsInTag]
                        ([NewsId]
                        ,[TagId]
                        ,[Priority]
                        ,[TaggedDate])
                        SELECT @Id,tb.value,ROW_NUMBER() OVER (ORDER BY (SELECT null) ),@CreatedDate FROM STRING_SPLIT(@Tags,',') tb;
                        IF(@@ROWCOUNT=0) ROLLBACK;ELSE COMMIT;
                    END;
                ELSE COMMIT;
            END;
        ELSE ROLLBACK;
        ";
        public const string UpdateStatus = @"UPDATE [Post]
           SET [Status]=@Status
                ,[ModifiedDate]=@ModifiedDate
                ,[ModifiedBy]=@ModifiedBy
                ,[DistributionDate]=@DistributionDate
                ,[ApprovedBy]=@ApprovedBy
                ,[ApprovedDate]=@ApprovedDate
           WHERE [Id]=@Id
        ;";
        public const string Update = @"
        BEGIN TRANSACTION;
        UPDATE [Post]
           SET 
            [Url]=CASE WHEN COL_LENGTH('Post','Url')=-1 THEN @Url ELSE LEFT(@Url,COL_LENGTH('Post','Url')/2) END
           ,[OriginalUrl]=CASE WHEN COL_LENGTH('Post','OriginalUrl')=-1 THEN @OriginalUrl ELSE LEFT(@OriginalUrl,COL_LENGTH('Post','OriginalUrl')/2) END
           ,[Author]=CASE WHEN COL_LENGTH('Post','Author')=-1 THEN @Author ELSE LEFT(@Author,COL_LENGTH('Post','Author')/2) END
           ,[CategoryId]=@CategoryId
           ,[Location]=CASE WHEN COL_LENGTH('Post','Location')=-1 THEN @Location ELSE LEFT(@Location,COL_LENGTH('Post','Location')/2) END
           ,[PublishMode]=@PublishMode
           ,[PublishData]=@PublishData
           ,[DistributionDate]=@DistributionDate
           ,[ModifiedDate]=@ModifiedDate
           ,[ModifiedBy]=@ModifiedBy
           ,[Tags]=@Tags
           ,[Caption]=@Caption
           ,[ApprovedBy]=@ApprovedBy
           ,[ApprovedDate]=@ApprovedDate
           ,[CommentMode]=@CommentMode
        WHERE [Id]=@Id;
        IF(@@ROWCOUNT<>0) 
            BEGIN
                DELETE [NewsInTag] WHERE [NewsId]=@Id;
                IF(LEN(@Tags)>0)
                BEGIN
                    INSERT INTO [NewsInTag]
                    ([NewsId]
                    ,[TagId]
                    ,[Priority]
                    ,[TaggedDate])
                    SELECT @Id,tb.value,ROW_NUMBER() OVER (ORDER BY (SELECT null) ),@ModifiedDate FROM STRING_SPLIT(@Tags,',') tb;
                    IF(@@ROWCOUNT<>0) COMMIT; ELSE ROLLBACK;
                END;
                ELSE COMMIT;
            END;
        ELSE ROLLBACK;
        ";
    }
}
