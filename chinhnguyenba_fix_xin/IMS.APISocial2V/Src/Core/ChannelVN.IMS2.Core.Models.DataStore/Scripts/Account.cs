﻿namespace ChannelVN.IMS2.Core.Models.DataStore.Scripts
{
    internal class Account
    {
        public const string Add = @"
            --BEGIN TRANSACTION;
            IF((SELECT count(id) FROM [Account] WHERE [Id] = @Id) = 0)
            INSERT INTO [Account]
                ([Id]
               ,[Mobile]
               ,[Email]
               ,[UserName]
               ,[Type]
               ,[Class]
               ,[FullName]
               ,[Avatar]
               ,[Banner]
               ,[Status]
               ,[IsSystem]
               ,[IsFullPermission]
               ,[OtpSecretKey]
               ,[CreatedBy]
               ,[CreatedDate]
               ,[LoginedDate]
               ,[Description]
               ,[CommentMode]
                ,[CrawlerMode]
                ,[CrawlerSource]
                ,[Levels]
                ,[RestrictClasses]
            )
            VALUES
                (@Id
                ,@Mobile
                ,@Email
                ,@UserName
                ,@Type
                ,@Class
                ,@FullName
                ,@Avatar
                ,@Banner
                ,@Status
                ,@IsSystem
                ,@IsFullPermission
                ,@OtpSecretKey
                ,@CreatedBy
                ,@CreatedDate
                ,@LoginedDate
                ,@Description
                ,@CommentMode
                ,@CrawlerMode
                ,@CrawlerSource
                ,@Levels
                ,@RestrictClasses
                );
            --IF(@@ROWCOUNT>0)
           -- BEGIN
               -- INSERT INTO [AccountDistribution](
                --       [AccountId]
                 --     ,[DistributorId]
                --      ,[RegisteredDate]
                 --     ,[IsPrimary]
                --       ,[Quota]
                --        ,[Quantity]
                 --   ,[RegisteredDate]
                 --  )
               -- VALUES(
                    --   @Id
                    --  ,@DistributorId
                    --  ,@RegisteredDate
                    --  ,@IsPrimary
                     --   ,@Quota
                     --   ,@Quantity
                      --  ,@RegisteredDate
               -- ); 
               -- IF(@@ROWCOUNT>0) COMMIT; ELSE ROLLBACK;
           -- END
            --ELSE ROLLBACK;
            ";

        public const string AddOfficial = @"
           -- BEGIN TRANSACTION;
            IF((SELECT count(id) FROM [Account] WHERE [Id] = @Id)=0)
            BEGIN
            INSERT INTO [Account]
                ([Id]
               ,[Mobile]
               ,[Email]
               ,[UserName]
               ,[Type]
               ,[Class]
               ,[FullName]
               ,[Avatar]
               ,[Banner]
               ,[Status]
               ,[IsSystem]
               ,[IsFullPermission]
               ,[OtpSecretKey]
               ,[CreatedBy]
               ,[CreatedDate]
               ,[LoginedDate]
               ,[Description]
               ,[CommentMode]
               ,[CrawlerMode]
                ,[CrawlerSource]
                ,[RelatedId]
                ,[RelatedType]
               --,[LabelMode]
                )
            VALUES
                (@Id
                ,@Mobile
                ,@Email
                ,@UserName
                ,@Type
                ,@Class
                ,@FullName
                ,@Avatar
                ,@Banner
                ,@Status
                ,@IsSystem
                ,@IsFullPermission
                ,@OtpSecretKey
                ,@CreatedBy
                ,@CreatedDate
                ,@LoginedDate
                ,@Description
                ,@CommentMode
                ,@CrawlerMode
                ,@CrawlerSource
                ,@RelatedId
                ,@RelatedType
                --,@LabelMode
                );
            END;
            IF((SELECT count(OfficerId) FROM [AccountMember] WHERE [OfficerId]=@OfficerId AND [MemberId]=@MemberId)=0)
            BEGIN
                INSERT INTO [AccountMember]
                    ([OfficerId]
                    ,[MemberId]
                    ,[Role]
                    ,[JoinedDate]
                    ,[Permissions])
                VALUES
                    (@OfficerId
                    ,@MemberId
                    ,@Role
                    ,@JoinedDate
                    ,@Permissions);
            END;
            --IF(@@ROWCOUNT>0)
               -- BEGIN
            IF((SELECT count(Id) FROM [UserProfile] WHERE [Id]=@Id)=0)
            BEGIN
                    INSERT INTO [UserProfile](
                    [Id]
                   ,[Slogan]
                   ,[ProfileUrl]
                   ,[PenName]
                   ,[City]
                   ,[Address]
                   ,[IdNumber]
                   ,[IssuedDate]
                   ,[IssuedPlace]
                   ,[Scan1]
                   ,[Scan2]
                   ,[Scan3]
           
                   ,[ReprName]
                   ,[ReprJob]
                   ,[ReprEmail]
                   ,[ReprPhone]
                    ) VALUES(
                        @Id
                        ,@Slogan
                        ,@ProfileUrl
                        ,@PenName
                        ,@City
                        ,@Address
                        ,@IdNumber
                        ,@IssuedDate
                        ,@IssuedPlace
                        ,@Scan1
                        ,@Scan2
                        ,@Scan3
           
                        ,@ReprName
                        ,@ReprJob
                        ,@ReprEmail
                        ,@ReprPhone
                    );
            END;
                    --IF(@@ROWCOUNT>0)
                   -- BEGIN
                        --INSERT INTO [AccountMember]
                               --([OfficerId]
                               --,[MemberId]
                               --,[Role]
                               --,[JoinedDate])
                         --VALUES
                              -- (@OfficerId
                               --,@MemberId
                              -- ,@Role
                               --,@JoinedDate);
                        --IF(@@ROWCOUNT>0) COMMIT;
                        --ELSE ROLLBACK;
                    --END;
                    --ELSE ROLLBACK;
                --END;
            --ELSE ROLLBACK;
            --INSERT INTO [AccountDistribution](
                  -- [AccountId]
                 -- ,[DistributorId]
                  --,[RegisteredDate]
                  --,[IsPrimary]
               -- )
           -- VALUES(
                  -- @Id
                  --,@DistributorId
                  --,@RegisteredDate
                  --,@IsPrimary
                --)
            ";
        public const string RemoveAccountMember = @"
         DELETE [AccountMember] WHERE [OfficerId]=@OfficerId and [MemberId]=@MemberId;
        ";

        public const string RemoveAccountMemberOnAllPage = @"
         DELETE [AccountMember] WHERE Convert(varchar(30), [OfficerId]) IN (@ListOfficerId) and [MemberId]=@MemberId;
        ";

        public const string LockAccountMember = @"
         UPDATE [AccountMember] SET [LockedDate]=@LockedDate WHERE [OfficerId]=@OfficerId and [MemberId]=@MemberId;
        ";

        public const string LockOrUnlockMemberOnAllPage = @"
         UPDATE [AccountMember] SET [LockedDate]=@LockedDate WHERE Convert(varchar(30),[OfficerId]) IN (@ListOfficerId) and [MemberId]=@MemberId;
        ";
        
        public const string AddProfile = @"
        IF((SELECT COUNT(Id) FROM [UserProfile] WHERE [Id]=@Id)=0)
            BEGIN
                INSERT INTO [UserProfile](
                    [Id]
                   ,[Slogan]
                   ,[ProfileUrl]
                   ,[PenName]
                   ,[City]
                   ,[Address]
                   ,[IdNumber]
                   ,[IssuedDate]
                   ,[IssuedPlace]
                   ,[Scan1]
                   ,[Scan2]
                   ,[Scan3]
           
                   ,[ReprName]
                   ,[ReprJob]
                   ,[ReprEmail]
                   ,[ReprPhone]
                ) VALUES(
                    @Id
                    ,@Slogan
                    ,@ProfileUrl
                    ,@PenName
                    ,@City
                    ,@Address
                    ,@IdNumber
                    ,@IssuedDate
                    ,@IssuedPlace
                    ,@Scan1
                    ,@Scan2
                    ,@Scan3
           
                    ,@ReprName
                    ,@ReprJob
                    ,@ReprEmail
                    ,@ReprPhone
                );
                IF(@RegisterType=1)
                    BEGIN
                        UPDATE [Account]
                        SET [Type]=@Type
                        ,[VerifiedBy]=null
                        ,[VerifiedDate]=null
                        WHERE [Id]=@Id;
                    END;
            END;
        ELSE 
            BEGIN
                UPDATE [UserProfile]
                SET 
                    [Slogan]=@Slogan
                   ,[ProfileUrl]=@ProfileUrl
                   ,[PenName]=@PenName
                   ,[City]=@City
                   ,[Address]=@Address
                   ,[IdNumber]=@IdNumber
                   ,[IssuedDate]=@IssuedDate
                   ,[IssuedPlace]=@IssuedPlace
                   ,[Scan1]=@Scan1
                   ,[Scan2]=@Scan2
                   ,[Scan3]=@Scan3
           
                   ,[ReprName]=@ReprName
                   ,[ReprJob]=@ReprJob
                   ,[ReprEmail]=@ReprEmail
                   ,[ReprPhone]=@ReprPhone
                    ,[ModifiedBy]=@ModifiedBy
                    ,[ModifiedDate]=@ModifiedDate
                WHERE [Id]=@Id;
                IF(@RegisterType=1)
                    BEGIN
                        UPDATE [Account]
                        SET [Type]=@Type
                        ,[VerifiedBy]=null
                        ,[VerifiedDate]=null
                        WHERE [Id]=@Id;
                    END;  
            END;";
        public const string Update = @"UPDATE [Account] 
            SET 
               [Mobile]=@Mobile
               ,[Email]=@Email
               ,[UserName]=@UserName
                ,[Password]=@Password
               ,[Type]=@Type
               ,[Class]=@Class
               ,[FullName]=@FullName
               ,[Avatar]=@Avatar
               ,[Banner]=@Banner
                ,[VerifiedBy]=@VerifiedBy
                ,[VerifiedDate]=@VerifiedDate
               ,[Status]=@Status
               ,[IsSystem]=@IsSystem
               ,[IsFullPermission]=@IsFullPermission
               ,[OtpSecretKey]=@OtpSecretKey
               ,[Mode]=@Mode
              
               ,[LoginedDate]=@LoginedDate
               ,[Description]=@Description
               ,[CommentMode]=@CommentMode
                ,[CrawlerMode]=@CrawlerMode
                ,[CrawlerSource]=@CrawlerSource
                ,[RelatedId]=@RelatedId
                ,[RelatedType]=@RelatedType
               --,[LabelMode]=@LabelMode
                ,[Levels]=@Levels
                ,[RestrictClasses]=@RestrictClasses
            WHERE [Id]=@Id;
            IF((SELECT COUNT(Id) FROM [UserProfile] WHERE [Id]=@Id)=0)
                BEGIN
                    INSERT INTO [UserProfile](Id,PenName,Gender,BirthDay)
                    VALUES(@Id,@PenName,@Gender,@BirthDay)
                END;
            ELSE
                BEGIN
                    UPDATE [UserProfile] 
                    SET 
                       [PenName]=@PenName
                       ,[Gender]=@Gender
                       ,[BirthDay]=@BirthDay
                        ,[Address]=@Address
                       ,[Title]=@Title
                       ,[WorkPlace]=@WorkPlace
                        ,[School]=@School
                       ,[UrlNetwork]=@UrlNetwork
                       ,[UrlSocial]=@UrlSocial
                    WHERE [Id]=@Id
                END;
            ";

        public const string ConfigPage = @"UPDATE [Account] SET [Mode] = @Mode WHERE [Id] = @Id;";
        public const string AcceptOrReject = @"UPDATE [Account] 
            SET 
                ,[VerifiedBy]=@VerifiedBy
                ,[VerifiedDate]=@VerifiedDate
            WHERE [Id]=@Id;
            ";

        public const string AcceptOrRejectOrLockWriter = @"
        UPDATE [Account] 
            SET 
            [AccountType]=@AccountType
            ,[RegisterAccountType]=@RegisterAccountType
            ,[RegisterAccountStatus]=@RegisterAccountStatus
            ,[VerifiedBy]=@VerifiedBy
            ,[VerifiedDate]=@VerifiedDate
            ,[Status]=@Status
            WHERE [UserId]=@UserId        
            ";
        public const string AddWriter = @"UPDATE [Account] 
            SET 
            [RegisterAccountType]=@RegisterAccountType
            ,[RegisterAccountStatus]=@RegisterAccountStatus
            ,[FullName]=@FullName
            ,[Email]=@Email
            ,[AccountClass]=@AccountClass
            ,[PenName]=@PenName
            ,[Scan1]=@Scan1
            ,[ModifiedDate]=@ModifiedDate  

            ,[Description]=@Description
            WHERE [UserId]=@UserId
            ";
        public const string UpdateQuota = @"UPDATE [AccountDistribution] 
            SET [Quantity]=@Quantity
            WHERE [AccountId]=@AccountId and [DistributorId]=@DistributorId
        ";
        public const string GetById = @"SELECT * FROM [Account] WHERE [Id]=@Id";
        public const string GetByMobile = @"SELECT TOP 1 * FROM [Account] WHERE [Mobile]=@Mobile AND [Id]<>@Id";
        public const string Active = @"UPDATE [Account] 
                  SET [Status]=@Status                                                  
                  ,[ModifiedDate]=getdate()                 
            WHERE [UserId]=@UserId               
            ";
        public const string UpdateStatus = @"UPDATE [Account] 
                  SET [Status]=@Status, [LabelMode]=@LabelMode, [Class]=@Class    
            WHERE [Id]=@Id               
            ";
        public const string UpdateAccessToken = @"UPDATE [Account] 
                  SET [OtpSecretKey]=@OtpSecretKey,
                    [Mobile]=@Mobile,
                    [Email]=@Email,
                    [FullName]=@FullName
            WHERE [Id]=@Id               
            ";
        
        public const string DeActive = @"UPDATE [Account] 
                  SET [Status]=@Status                                                  
                  ,[ModifiedDate]=getdate()                  
            WHERE [UserId]=@UserId                
            ";
        public const string GetAll = @"SELECT Mobile,Email,Id FROM Account WHERE Type<>2 and Id > 0";
        public const string UpdateAccountType = @"UPDATE [Account] 
                  SET [Type]=@Type ,[Class]=@Class, [Levels]=@Levels ,[RestrictClasses] = @RestrictClasses   
            WHERE [Id]=@Id               
            ";
        public const string UpdateAccountOpentId = @"UPDATE [Account] 
                  SET [OpenId]=@OpenId  
            WHERE [Id]=@Id               
            ";

        public const string CountAccount = @"SELECT count(Id) as Total from Account ";

        public const string InitAllAccount = @"DECLARE @UpperBand int, @LowerBand int
                                        
                                        SET @LowerBand  = (@PageIndex - 1) * @PageSize
                                        SET @UpperBand  = (@PageIndex * @PageSize)
                                        SELECT * FROM (
                                        SELECT a.* ,
                                        ROW_NUMBER() OVER(ORDER BY a.Id DESC) AS RowNumber 
                                        FROM Account a 
                                        where (@DateFrom is null and (1=1)) or (@DateFrom is not null and (CONVERT(datetime,a.CreatedDate) >= CONVERT(datetime,@DateFrom) AND CONVERT(datetime,a.CreatedDate)<=CONVERT(datetime,@DateTo)))
                                        ) AS temp
                                        WHERE RowNumber > @LowerBand AND RowNumber <= @UpperBand";

        public const string GetAccountDistributionByAccountId = @"select * from AccountDistribution where [AccountId]=@AccountId";

        public const string AddAccountMember = @" 
                                INSERT INTO [AccountMember]
                                        ([OfficerId]
                                        ,[MemberId]
                                        ,[Role]
                                        ,[JoinedDate]
                                        ,[Permissions])
                                    VALUES(@OfficerId
                                        ,@MemberId
                                        ,@Role
                                        ,@JoinedDate
                                        ,@Permissions);
                                    ";

        public const string UpdateAccountMember = @"UPDATE [AccountMember]
                                    SET [Role]=@Role
                                    WHERE [OfficerId]=@OfficerId and [MemberId]=@MemberId";

        public const string CountAccountMember = @"SELECT count(OrganizerId) as Total from AccountMember";

        public const string InitAllAccountMember = @"DECLARE @UpperBand int, @LowerBand int
                                        
                                        SET @LowerBand  = (@PageIndex - 1) * @PageSize
                                        SET @UpperBand  = (@PageIndex * @PageSize)
                                        SELECT temp.* FROM (
                                        SELECT a.* ,
                                            ROW_NUMBER() OVER(ORDER BY a.OrganizerId DESC) AS RowNumber 
                                            FROM AccountMember a 
                                            where (@DateFrom is null and (1=1)) or (@DateFrom is not null and (CONVERT(datetime,a.ApprovedDate) 
                                            >= CONVERT(datetime,@DateFrom) AND CONVERT(datetime,a.ApprovedDate)<=CONVERT(datetime,@DateTo)))
                                        ) AS temp
                                        WHERE RowNumber > @LowerBand AND RowNumber <= @UpperBand";

        public const string ChangeOwner = @"UPDATE [Account] SET [CreatedBy] = @NewOwnerId WHERE [Id] = @OfficerId;
            DELETE [AccountMember] WHERE [OfficerId] = @OfficerId AND [MemberId] = @OldOwnerId;
            IF((SELECT 1 FROM AccountMember WHERE [OfficerId]=@OfficerId AND [MemberId]=@NewOwnerId)>0)
                BEGIN
                    UPDATE AccountMember SET [Role] = @Role WHERE [OfficerId] = @OfficerId AND [MemberId] = @NewOwnerId;
                END
            ELSE
                BEGIN
                    INSERT INTO AccountMember(OfficerId,MemberId,Role,JoinedDate) VALUES(@OfficerId,@NewOwnerId,@Role,@JoinedDate);
                END";
        public const string SetProfileIsPageOnApp = @"UPDATE [Account] SET [DelegatorId] = @DelegatorId WHERE [Id]=@Id;";
        public const string UpdateProfileUrl = @"UPDATE [UserProfile] SET [ProfileUrl] = @ProfileUrl WHERE [Id]=@Id;";
    }
}
