﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ChannelVN.IMS2.Core.Models.DataStore.Scripts
{
    public class Board
    {
        public const string Add = @"INSERT INTO [Board]
           ([Id]
           ,[Name]
           ,[UnsignName]
           ,[Description]
           ,[Url]
           ,[Avatar]
           ,[Cover]
           ,[NewsCoverId]
           ,[CreatedDate]
           ,[CreatedBy]
           ,[Status]
           ,[IsHot]
            ,[FollowCount]
            ,[ParentId]
            ,[RelationBoard])
            VALUES
           (@Id
           ,CASE WHEN COL_LENGTH('Board','Name')=-1 THEN @Name ELSE LEFT(@Name,COL_LENGTH('Board','Name')/2) END
           ,CASE WHEN COL_LENGTH('Board','UnsignName')=-1 THEN @UnsignName ELSE LEFT(@UnsignName,COL_LENGTH('Board','UnsignName')) END
           ,CASE WHEN COL_LENGTH('Board','Description')=-1 THEN @Description ELSE LEFT(@Description,COL_LENGTH('Board','Description')/2) END
           ,CASE WHEN COL_LENGTH('Board','Url')=-1 THEN @Url ELSE LEFT(@Url,COL_LENGTH('Board','Url')) END
           ,CASE WHEN COL_LENGTH('Board','Avatar')=-1 THEN @Avatar ELSE LEFT(@Avatar,COL_LENGTH('Board','Avatar')/2) END
           ,CASE WHEN COL_LENGTH('Board','Cover')=-1 THEN @Cover ELSE LEFT(@Cover,COL_LENGTH('Board','Cover')/2) END
           ,@NewsCoverId
           ,@CreatedDate
           ,@CreatedBy
           ,@Status
           ,@IsHot
            ,@FollowCount
            ,@ParentId
            ,@RelationBoard)
        ";
        
        public const string UpdateMediaAsync = @"
         BEGIN TRANSACTION;
         UPDATE [Board]
           SET [LastInsertedDate] = @LastInsertedDate
         WHERE Id=@Id
         IF(@@ROWCOUNT>0)
           IF(LEN(@NewsIdAdd)>0)
                BEGIN
                    INSERT INTO [NewsInBoard](
                        [BoardId]
                        ,[NewsId]
                        ,[PublishedDate]
                    ) SELECT @Id,dt.value,@LastInsertedDate FROM STRING_SPLIT(@NewsIdAdd,',') dt;
                    IF(@@ROWCOUNT>0)
                        IF(LEN(@NewsIdDelete)>0)
                            BEGIN
                                DELETE [NewsInBoard] WHERE [NewsId] IN (SELECT dt.value FROM STRING_SPLIT(@NewsIdDelete,',') dt);
                                IF(@@ROWCOUNT==0) ROLLBACK; ELSE COMMIT;
                            END;
                        ELSE COMMIT;
                    ELSE ROLLBACK;
                END;
            ELSE
                BEGIN
                    IF(LEN(@NewsIdDelete)>0)
                        BEGIN
                            DELETE [NewsInBoard] WHERE [NewsId] IN (SELECT dt.value FROM STRING_SPLIT(@NewsIdDelete,',') dt);
                            IF(@@ROWCOUNT==0) ROLLBACK; ELSE COMMIT;
                        END;
                    ELSE COMMIT;
                END;
        ELSE ROLLBACK;
            
            "
;
        public const string Update = @"UPDATE [Board]
            SET [Name] =CASE WHEN COL_LENGTH('Board','Name')=-1 THEN @Name ELSE LEFT(@Name,COL_LENGTH('Board','Name')/2) END
              ,[UnsignName] = CASE WHEN COL_LENGTH('Board','UnsignName')=-1 THEN @UnsignName ELSE LEFT(@UnsignName,COL_LENGTH('Board','UnsignName')) END
              ,[Description] = CASE WHEN COL_LENGTH('Board','Description')=-1 THEN @Description ELSE LEFT(@Description,COL_LENGTH('Board','Description')/2) END
              ,[Url] = CASE WHEN COL_LENGTH('Board','Url')=-1 THEN @Url ELSE LEFT(@Url,COL_LENGTH('Board','Url')/2) END
              ,[Avatar] = CASE WHEN COL_LENGTH('Board','Avatar')=-1 THEN @Avatar ELSE LEFT(@Avatar,COL_LENGTH('Board','Avatar')/2) END
              ,[Cover] = CASE WHEN COL_LENGTH('Board','Cover')=-1 THEN @Cover ELSE LEFT(@Cover,COL_LENGTH('Board','Cover')/2) END
              ,[NewsCoverId] = @NewsCoverId
              ,[ModifiedDate] = @ModifiedDate
              ,[ModifiedBy] = @ModifiedBy
              ,[Status] = @Status
              ,[IsHot] = @IsHot
              ,[FollowCount]=@FollowCount
              ,[ParentId]=@ParentId
              ,[RelationBoard]=@RelationBoard
            WHERE Id=@Id";
        public const string AddNewsToBoard = @"
        DECLARE @total INT;
		DECLARE @index INT; 
		DECLARE @newsid BIGINT;
		SET @index=1;
		SET @total = (select count(dt.value)  from STRING_SPLIT(@Ids,',') dt);
		WHILE @index <= @total
		BEGIN
			SET @NewsId=(select dt.value from (select value , ROW_NUMBER() OVER (ORDER BY (SELECT null) ) as rownum  from STRING_SPLIT(@Ids,',')) dt where dt.rownum=@index);
		   IF((SELECT COUNT(BoardId) FROM [NewsInBoard] WHERE [BoardId]=@BoardId AND [NewsId] = @NewsId)=0)
                BEGIN
                    INSERT INTO [NewsInBoard]
                   ([BoardId]
                   ,[NewsId]
                    ,[PublishedDate])
                    VALUES
                   (@BoardId
                   ,@NewsId
                    ,@PublishedDate
			       );
                    UPDATE [Board]
                    SET [LastInsertedDate]=@LastInsertedDate
                    WHERE [Id]=@BoardId;
                END;
            ELSE
                BEGIN
                    UPDATE [NewsInBoard] SET [BoardId]=[BoardId] WHERE [BoardId]=@BoardId AND [NewsId] = @NewsId
                END
		   SET @index = @index + 1;
		END;
        ";
        public const string DeleteNewsInBoard = @"
        DELETE [NewsInBoard] WHERE [BoardId]=@BoardId AND [NewsId] IN (SELECT value FROM  STRING_SPLIT(@NewsIds,','));
        ";

        public const string InsertMedia = @"BEGIN TRANSACTION;
        IF(LEN(@BoardIds)>0)
            BEGIN
                UPDATE Board
                SET ModifiedBy = @ModifiedBy
                    ,ModifiedDate = @ModifiedDate
                    ,LastInsertedDate = @LastInsertedDate
                WHERE Id IN (SELECT dt.value FROM STRING_SPLIT(@BoardIds,',') dt);
                IF(@@ROWCOUNT>0)
                    BEGIN
                        INSERT INTO NewsInBoard(BoardId,NewsId,Priority,PublishedDate)
                        SELECT dt.value,@MediaId,0,@PublishedDate
                        FROM STRING_SPLIT(@BoardIds,',') dt 
                        WHERE NOT EXISTS (SELECT * FROM NewsInBoard WHERE NewsId = @MediaId AND BoardId IN (select * from STRING_SPLIT(@BoardIds,',')));
                        IF(@@ROWCOUNT>0) COMMIT; ELSE ROLLBACK;
                    END;
                ELSE ROLLBACK;
            END;
        ELSE
        COMMIT;";
    }
}
