﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ChannelVN.IMS2.Core.Models.DataStore.Scripts
{
    internal class NewsCrawlerConfiguration
    {
        public const string Add = @"     
         INSERT INTO [NewsCrawlerConfiguration](
            [Id]
           ,[OfficerId]
           ,[Source]
           ,[CreatedBy]
           ,[CreatedDate]
           ,[PublishMode]
           ,[IntervalTime]
           ,[Status]           
        ) VALUES (
             @Id
            ,@OfficerId
            ,@Source
            ,@CreatedBy
            ,@CreatedDate
            ,@PublishMode
            ,@IntervalTime
            ,@Status
        );";

        public const string Update = @"UPDATE [NewsCrawlerConfiguration]
                SET [OfficerId]=@OfficerId               
                ,[Source]=@Source
                ,[CreatedBy]=@CreatedBy
                ,[CreatedDate]=@CreatedDate               
                ,[PublishMode]=@PublishMode
                ,[IntervalTime]=@IntervalTime
                ,[Status]=@Status
            WHERE [Id]=@Id";
    }
}
