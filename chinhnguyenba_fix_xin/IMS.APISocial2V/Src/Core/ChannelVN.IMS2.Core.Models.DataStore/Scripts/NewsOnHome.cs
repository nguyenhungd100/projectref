﻿namespace ChannelVN.IMS2.Core.Models.DataStore.Scripts
{
    internal class NewsOnHome
    {
        public const string Add = @"
            IF((SELECT count(NewsId) FROM [NewsOnHome] WHERE [NewsId] = @NewsId) = 0)
            INSERT INTO [NewsOnHome]
            ([NewsId]
              ,[OfficerId]
              ,[Type]
              ,[Status]
              ,[OrderedBy]
              ,[OrderedDate]
              ,[OfficerClass])
            VALUES
                (@NewsId
              ,@OfficerId
              ,@Type
              ,@Status
              ,@OrderedDate
              ,@OrderedBy
              ,@OfficerClass)
            ELSE
            UPDATE [NewsOnHome]
                SET [OrderedDate]=@OrderedDate                  
            WHERE [NewsId] = @NewsId
            ";
        public const string Update = @"UPDATE [NewsOnHome]
                SET [Status]=@Status
                  ,[AcceptedDate]=@AcceptedDate
                  ,[AcceptedBy]=@AcceptedBy
                  ,[CanceledDate]=@CanceledDate
                  ,[CanceledBy]=@CanceledBy
            WHERE [NewsId] = @NewsId
            ";        
    }
}
