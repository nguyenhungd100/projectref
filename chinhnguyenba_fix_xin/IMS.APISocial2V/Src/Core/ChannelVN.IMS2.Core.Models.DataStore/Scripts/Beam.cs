﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ChannelVN.IMS2.Core.Models.DataStore.Scripts
{
    public class Beam
    {
        public const string Test = @"
        INSERT INTO ArticleInBeam(BeamId,ArticleId,Priority,PublishedDate)
        select (select v from (select dt2.value as v,ROW_NUMBER()  OVER (ORDER BY (SELECT null) ) as n from STRING_SPLIT(dt.value,'_') dt2 order by n asc offset 0 ROWS FETCH Next 1 rows only) dt3)
	,(select v from (select dt2.value as v,ROW_NUMBER()  OVER (ORDER BY (SELECT null) ) as n from STRING_SPLIT(dt.value,'_') dt2 order by n asc offset 1 ROWS FETCH Next 1 rows only) dt3)
	,(select v from (select dt2.value as v,ROW_NUMBER()  OVER (ORDER BY (SELECT null) ) as n from STRING_SPLIT(dt.value,'_') dt2 order by n asc offset 2 ROWS FETCH Next 1 rows only) dt3)
	,(select v from (select dt2.value as v,ROW_NUMBER()  OVER (ORDER BY (SELECT null) ) as n from STRING_SPLIT(dt.value,'_') dt2 order by n asc offset 3 ROWS FETCH Next 1 rows only) dt3)
 from STRING_SPLIT(@ArticleIds,',') dt;
        --IF type_id('ArticleInBeam') IS NULL
        --CREATE TYPE ArticleInBeam AS TABLE
        --(
           -- BeamId BigInt 
           -- ,ArticleId BigInt
           -- , Priority Int
          -- , DataType Int
           -- , PublishedDate DateTime
        --);
        --DECLARE @LocationTVP AS ArticleInBeam;  
        --SET @LocationTVP = @ArticleIds;
        --INSERT INTO ArticleInBeam  
          -- (BeamId  
          -- ,ArticleId  
          -- ,Priority  
          -- ,DataType
           -- ,PublishedDate)  
       -- SELECT *  
       -- FROM  @LocationTVP;  
        ";
        public const string Add = @"
        BEGIN TRANSACTION;
        INSERT INTO [Beam]
        (   [Id]
           ,[Type]
           ,[Url]
           ,[OriginalUrl]
           ,[Status]
           ,[Author]
           ,[CategoryId]
           ,[CreatedDate]
           ,[CreatedBy]
           ,[Location]
           ,[PublishMode]
           ,[PublishData]
           ,[DistributionDate]
           ,[DistributorId]
           ,[Tags]
           ,[CardType]
           ,[Name]
           ,[UnsignName]
           ,[Description]
           ,[LastInsertedDate]
           ,[BeamRelation]
           ,[ApprovedBy]
           ,[MetaAvatar]
           ,[MetaData]
           ,[Cover]
           ,[CommentMode]
        ) VAlUES (
            @Id
           ,@Type
           ,CASE WHEN COL_LENGTH('Beam','Url')=-1 THEN @Url ELSE LEFT(@Url,COL_LENGTH('Beam','Url')/2) END
           ,CASE WHEN COL_LENGTH('Beam','OriginalUrl')=-1 THEN @OriginalUrl ELSE LEFT(@OriginalUrl,COL_LENGTH('Beam','OriginalUrl')/2) END
           ,@Status
           ,CASE WHEN COL_LENGTH('Beam','Author')=-1 THEN @Author ELSE LEFT(@Author,COL_LENGTH('Beam','Author')/2) END
           ,@CategoryId
           ,@CreatedDate
           ,@CreatedBy
           ,CASE WHEN COL_LENGTH('Beam','Location')=-1 THEN @Location ELSE LEFT(@Location,COL_LENGTH('Beam','Location')/2) END
           ,@PublishMode
           ,@PublishData
           ,@DistributionDate
           ,@DistributorId
           ,@Tags
           ,@CardType
           ,CASE WHEN COL_LENGTH('Beam','Name')=-1 THEN @Name ELSE LEFT(@Name,COL_LENGTH('Beam','Name')/2) END
           ,CASE WHEN COL_LENGTH('Beam','UnsignName')=-1 THEN @UnsignName ELSE LEFT(@UnsignName,COL_LENGTH('Beam','UnsignName')) END
           ,@Description
           ,@LastInsertedDate
           ,@BeamRelation
           ,@ApprovedBy
           ,@MetaAvatar
           ,@MetaData
           ,CASE WHEN COL_LENGTH('Beam','Cover')=-1 THEN @Cover ELSE LEFT(@Cover,COL_LENGTH('Beam','Cover')/2) END
           ,@CommentMode
        );
        
        IF(@@ROWCOUNT<>0)
        BEGIN
            INSERT INTO [NewsInAccount]
                    ([AccountId]
                      ,[NewsId]
                      ,[PublishedDate]
                      ,[PublishedType])
                    VALUES
                    (@AccountId
                      ,@Id
                      ,@PublishedDate
                      ,@PublishedType);
            IF(@@ROWCOUNT<>0)
            BEGIN
                IF(LEN(@ArticleIds)>0)
                    BEGIN
                        INSERT INTO ArticleInBeam(BeamId,ArticleId,Priority,DataType,PublishedDate)
                        SELECT @Id
                        ,(SELECT v FROM (SELECT dt2.value as v,ROW_NUMBER()  OVER (ORDER BY (SELECT null) ) as n from STRING_SPLIT(dt.value,'_') dt2 order by n asc offset 0 ROWS FETCH Next 1 rows only) dt3)
	                    ,ROW_NUMBER()  OVER (ORDER BY (SELECT null) )
	                    ,(select v from (select dt2.value as v,ROW_NUMBER()  OVER (ORDER BY (SELECT null) ) as n from STRING_SPLIT(dt.value,'_') dt2 order by n asc offset 1 ROWS FETCH Next 1 rows only) dt3)
                    ,(select v from (select dt2.value as v,ROW_NUMBER()  OVER (ORDER BY (SELECT null) ) as n from STRING_SPLIT(dt.value,'_') dt2 order by n asc offset 2 ROWS FETCH Next 1 rows only) dt3)
                     from STRING_SPLIT(@ArticleIds,',') dt;
                        IF(@@ROWCOUNT<>0)
                        BEGIN
                            IF(LEN(@Tags)>0)
                            BEGIN
                                INSERT INTO [NewsInTag]
                                ([NewsId]
                                ,[TagId]
                                ,[Priority]
                                ,[TaggedDate])
                                SELECT @Id,tb.value,ROW_NUMBER() OVER (ORDER BY (SELECT null) ),@CreatedDate FROM STRING_SPLIT(@Tags,',') tb;
                                IF(@@ROWCOUNT=0) ROLLBACK;ELSE COMMIT
                            END;
                            ELSE COMMIT;
                        END;
                        ELSE ROLLBACK;
                    END;
                ELSE 
                BEGIN
                    IF(LEN(@Tags)>0)
                    BEGIN
                        INSERT INTO [NewsInTag]
                        ([NewsId]
                        ,[TagId]
                        ,[Priority]
                        ,[TaggedDate])
                        SELECT @Id,tb.value,ROW_NUMBER() OVER (ORDER BY (SELECT null) ),@CreatedDate FROM STRING_SPLIT(@Tags,',') tb;
                        IF(@@ROWCOUNT=0) ROLLBACK;ELSE COMMIT
                    END;
                    ELSE COMMIT;
                END;
            END;
            ELSE ROLLBACK;
        END;
        ELSE ROLLBACK;
        ";

        public const string Update = @"
        BEGIN TRANSACTION;
        UPDATE [Beam]
        SET
           [Url]=CASE WHEN COL_LENGTH('Beam','Url')=-1 THEN @Url ELSE LEFT(@Url,COL_LENGTH('Beam','Url')/2) END
           ,[OriginalUrl]=CASE WHEN COL_LENGTH('Beam','OriginalUrl')=-1 THEN @OriginalUrl ELSE LEFT(@OriginalUrl,COL_LENGTH('Beam','OriginalUrl')/2) END
           ,[Author]=CASE WHEN COL_LENGTH('Beam','Author')=-1 THEN @Author ELSE LEFT(@Author,COL_LENGTH('Beam','Author')/2) END
           ,[CategoryId]=@CategoryId
           ,[Location]=CASE WHEN COL_LENGTH('Beam','Location')=-1 THEN @Location ELSE LEFT(@Location,COL_LENGTH('Beam','Location')/2) END
           ,[PublishMode]=@PublishMode
           ,[PublishData]=@PublishData
           --,[DistributionDate]=@DistributionDate
           ,[Tags]=@Tags
           ,[Name]=CASE WHEN COL_LENGTH('Beam','Name')=-1 THEN @Name ELSE LEFT(@Name,COL_LENGTH('Beam','Name')/2) END
           ,[UnsignName]=CASE WHEN COL_LENGTH('Beam','UnsignName')=-1 THEN @UnsignName ELSE LEFT(@UnsignName,COL_LENGTH('Beam','UnsignName')) END
           ,[Description]=@Description
           ,[LastInsertedDate]=@LastInsertedDate
           ,[BeamRelation]=@BeamRelation
            ,[ApprovedBy]=@ApprovedBy
            ,[ApprovedDate]=@ApprovedDate
           ,[MetaAvatar]=@MetaAvatar
           ,[MetaData]=@MetaData
            ,[Cover]=CASE WHEN COL_LENGTH('Beam','Cover')=-1 THEN @Cover ELSE LEFT(@Cover,COL_LENGTH('Beam','Cover')/2) END
            ,[CommentMode]=@CommentMode
        WHERE [Id]=@Id;
        
        IF(@@ROWCOUNT<>0)
        BEGIN
            DELETE [ArticleInBeam] WHERE [BeamId]=@Id;
            IF(LEN(@ArticleIds)>0)
                BEGIN
                    
                   INSERT INTO ArticleInBeam(BeamId,ArticleId,Priority,DataType,PublishedDate)
                        SELECT @Id
                        ,(SELECT v FROM (SELECT dt2.value as v,ROW_NUMBER()  OVER (ORDER BY (SELECT null) ) as n from STRING_SPLIT(dt.value,'_') dt2 order by n asc offset 0 ROWS FETCH Next 1 rows only) dt3)
	                    ,ROW_NUMBER()  OVER (ORDER BY (SELECT null) )
	                    ,(select v from (select dt2.value as v,ROW_NUMBER()  OVER (ORDER BY (SELECT null) ) as n from STRING_SPLIT(dt.value,'_') dt2 order by n asc offset 1 ROWS FETCH Next 1 rows only) dt3)
                    ,(select v from (select dt2.value as v,ROW_NUMBER()  OVER (ORDER BY (SELECT null) ) as n from STRING_SPLIT(dt.value,'_') dt2 order by n asc offset 2 ROWS FETCH Next 1 rows only) dt3)
                    from STRING_SPLIT(@ArticleIds,',') dt;
                    
                    IF(@@ROWCOUNT<>0)
                        BEGIN
                            COMMIT;
                        END
                    ELSE ROLLBACK;
                END;
            ELSE 
                COMMIT;
        END;
        ELSE ROLLBACK;
        ";

        public const string CountVideoPlaylist = @"SELECT count(id) as Total FROM [Playlist]";
        public const string InitAllVideoPlaylist = @"DECLARE @UpperBand int, @LowerBand int
                                        
            SET @LowerBand  = (@PageIndex - 1) * @PageSize
            SET @UpperBand  = (@PageIndex * @PageSize)
            SELECT * FROM (
            SELECT 
                 v.*,
            ROW_NUMBER() OVER(ORDER BY v.Id DESC) AS RowNumber 
            FROM [Playlist] v 
            where  (@DateFrom is null and (1=1)) or (@DateFrom is not null and (CONVERT(datetime,v.CreatedDate) >= CONVERT(datetime,@DateFrom) AND CONVERT(datetime,v.CreatedDate)<=CONVERT(datetime,@DateTo)))
            ) AS temp
            WHERE RowNumber > @LowerBand AND RowNumber <= @UpperBand";
        public const string UpdateStatus = @"UPDATE [Beam] 
                SET [Status]=@Status,
                 [ModifiedDate]=@ModifiedDate,
                 [ModifiedBy]=@ModifiedBy,
                [DistributionDate]=@DistributionDate,
                [ApprovedBy]=@ApprovedBy,
                [ApprovedDate]=@ApprovedDate
            WHERE [Id]=@Id ";
        //public const string InitAll= @"DECLARE @UpperBand int, @LowerBand int
        // SET @LowerBand  = (@PageIndex - 1) * @PageSize
        // SET @UpperBand  = (@PageIndex* @PageSize)
        //SELECT* FROM(
        //SELECT
        //    v.[Id]
        //        ,v.[CategoryId]
        //        ,v.[Name]
        //        ,v.[UnsignName]
        //        ,v.[Description]
        //        ,v.[Status]
        //        ,v.[Mode]
        //        ,v.[Avatar]
        //        ,v.[Priority]
        //        ,v.[CreatedDate]
        //        ,v.[CreatedBy]
        //        ,v.[ModifiedDate]
        //        ,v.[ModifiedBy]
        //        ,v.[PublishedDate]
        //        ,v.[PublishedBy]
        //        ,v.[EditedDate]
        //        ,v.[EditedBy]
        //        ,v.[LastInsertedDate]
        //        ,v.[VideoCount]
        //        ,v.[FollowCount]
        //        ,v.[Url]
        //        ,v.[Cover]
        //        ,v.[IntroClip]
        //        ,v.[MetaData]
        //        ,v.[MetaAvatar],
        //ROW_NUMBER() OVER(ORDER BY v.Id DESC) AS RowNumber
        //FROM Video v
        //where(@DateFrom is null and (1=1)) or(@DateFrom is not null and (CONVERT(datetime, v.CreatedDate) >= CONVERT(datetime, @DateFrom) AND CONVERT(datetime, v.CreatedDate)<=CONVERT(datetime, @DateTo)))
        //) AS temp
        //WHERE RowNumber > @LowerBand AND RowNumber <= @UpperBand";
        public const string GetVideoPlaylistRelation = @"SELECT TOP 1 [PlaylistRelation] as Ids FROM [VideoPlaylist] WHERE [Id]=@Id";

        public const string GetVideoInPlaylist = @"SELECT [PlaylistId],[VideoId],[Priority],[PlayOnTime] FROM [VideoInPlaylist] WHERE [PlaylistId]=@PlaylistId ORDER BY [Priority] ASC";
    }
}
