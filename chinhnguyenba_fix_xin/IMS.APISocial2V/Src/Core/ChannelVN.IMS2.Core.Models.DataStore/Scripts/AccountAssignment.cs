﻿namespace ChannelVN.IMS2.Core.Models.DataStore.Scripts
{
    internal class AccountAssignment
    {
        public const string Add = @"
            IF((SELECT count(AccountId) FROM [AccountAssignment] WHERE [AccountId] = @AccountId AND [OfficerId]=@OfficerId) = 0)
            INSERT INTO [AccountAssignment]
            ([AccountId]
              ,[OfficerId]
              ,[AssignedRole]
              ,[AssignedDate]
              ,[AssignedBy])
            VALUES
                (@AccountId
              ,@OfficerId
              ,@AssignedRole
              ,@AssignedDate
              ,@AssignedBy)
            ELSE
            UPDATE [AccountAssignment]
                SET [AssignedDate]=@AssignedDate
                  ,[AssignedBy]=@AssignedBy                       
            WHERE [AccountId] = @AccountId AND [OfficerId]=@OfficerId
            ";
        public const string Update = @"UPDATE [AccountAssignment]
                SET [AssignedRole]=@AssignedRole
                  ,[AssignedDate]=@AssignedDate
                  ,[AssignedBy]=@AssignedBy                       
            WHERE [AccountId] = @AccountId AND [OfficerId]=@OfficerId
            ";
        public const string Remove = @"DELETE [AccountAssignment] WHERE [AccountId] = @AccountId AND [OfficerId]=@OfficerId";
    }
}
