﻿namespace ChannelVN.IMS2.Core.Models.DataStore.Scripts
{
    internal class Category
    {
        public const string Add = @"
            INSERT INTO [Category]
                ([Id]
                  ,[Name]
                  ,[Description]
                  ,[Status]
                  ,[Priority]
                  ,[OfficerId]
                  ,[CreatedDate]
                  ,[CreatedBy])
            VALUES
                (@Id
                ,@Name
                ,@Description
                ,@Status
                ,@Priority
                ,@OfficerId
                ,@CreatedDate
                ,@CreatedBy);
            ";

        public const string Update = @"UPDATE [Category] 
                  SET [Name]=@Name
                  ,[Description]=@Description
                  ,[Status]=@Status
                  ,[Priority]=@Priority                                                
            WHERE [Id]=@Id                
            ";

        public const string GetById = @"SELECT * FROM [Category] WHERE [Id]=@Id";

        public const string CountCategory = @"SELECT count(Id) as Total from Category ";

        public const string InitAllCategory = @"DECLARE @UpperBand int, @LowerBand int
                                        
                                        SET @LowerBand  = (@PageIndex - 1) * @PageSize
                                        SET @UpperBand  = (@PageIndex * @PageSize)
                                        SELECT * FROM (
                                        SELECT c.* ,
                                        ROW_NUMBER() OVER(ORDER BY c.Id DESC) AS RowNumber 
                                        FROM Category c 
                                        where (@DateFrom is null and (1=1)) or (@DateFrom is not null and (CONVERT(datetime,c.CreatedDate) >= CONVERT(datetime,@DateFrom) AND CONVERT(datetime,c.CreatedDate)<=CONVERT(datetime,@DateTo)))
                                        ) AS temp
                                        WHERE RowNumber > @LowerBand AND RowNumber <= @UpperBand";
    }
}
