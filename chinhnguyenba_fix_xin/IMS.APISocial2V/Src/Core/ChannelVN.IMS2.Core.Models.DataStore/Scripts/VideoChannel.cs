﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ChannelVN.IMS2.Core.Models.DataStore.Scripts
{
    internal class VideoChannel
    {
        public const string Add = @"
        BEGIN TRANSACTION;
        INSERT INTO [VideoChannel]
        ([Id]
            ,[CategoryId]
            ,[Name]
            ,[Avatar]
            ,[Priority]
            ,[CreatedBy]
            ,[CreatedDate]
            ,[Url]
            ,[FollowCount]
            ,[VideoCount]
            ,[Rank]
            ,[IntroClip]
            ,[Status]
            ,[Cover]
            ,[Description]
            ,[ChannelRelation]
            ,[MetaData]
            ,[MetaAvatar]
        ) VALUES(@Id
            ,@CategoryId
            ,@Name
            ,@Avatar
            ,@Priority
            ,@CreatedBy
            ,@CreatedDate
            ,@Url
            ,@FollowCount
            ,@VideoCount
            ,@Rank
            ,@IntroClip
            ,@Status
            ,@Cover
            ,@Description
            ,@ChannelRelation
            ,@MetaData
            ,@MetaAvatar
        );
        IF(@@ROWCOUNT<>0)
        IF(LEN(@PlaylistIds)>0)
            BEGIN
                INSERT INTO VideoPlaylistInChannel(PlaylistId,ChannelId)
                SELECT dt.value,@Id  FROM STRING_SPLIT(@PlaylistIds,',') dt;
            END;
        IF(@@ROWCOUNT=0) ROLLBACK;
        ELSE COMMIT;
        ";
        public const string Update = @"
        BEGIN TRANSACTION;
        UPDATE [VideoChannel]
        SET [CategoryId]=@CategoryId
            ,[Name]=@Name
            ,[Avatar]=@Avatar
            ,[Priority]=@Priority
            ,[Url]=@Url
            ,[FollowCount]=@FollowCount
            ,[VideoCount]=@VideoCount
            ,[Rank]=@Rank
            ,[IntroClip]=@IntroClip
            ,[Status]=@Status
            ,[ModifiedBy]=@ModifiedBy
            ,[ModifiedDate]=@ModifiedDate
            ,[Cover]=@Cover
            ,[Description]=@Description
            ,[ChannelRelation]=@ChannelRelation
            ,[MetaData]=@MetaData
            ,[MetaAvatar]=@MetaAvatar
         WHERE [Id]=@Id;
        IF(@@ROWCOUNT<>0)
            BEGIN
                DELETE [VideoPlaylistInChannel] WHERE [ChannelId]=@Id;
                IF(LEN(@PlaylistIds)>0)
                    BEGIN
                        INSERT INTO [VideoPlaylistInChannel]([PlaylistId],[ChannelId])
                        SELECT dt.value,@Id from STRING_SPLIT(@PlaylistIds,',') dt;
                    
                    END;
            END;
        IF(@@ROWCOUNT=0) ROLLBACK;
        ELSE COMMIT;
        ";
        public const string CountVideoChannel = @"SELECT count(id) FROM VideoChannel";

        public const string InitAllVideoChannel = @"DECLARE @UpperBand int, @LowerBand int
                                        
            SET @LowerBand  = (@PageIndex - 1) * @PageSize
            SET @UpperBand  = (@PageIndex * @PageSize)
            SELECT * FROM (
            SELECT 
                 vc.[Id]
                  ,vc.[CategoryId]
                  ,vc.[Name]
                  ,vc.[Avatar]
                  ,vc.[Priority]
                  ,vc.[CreatedBy]
                  ,vc.[CreatedDate]
                  ,vc.[Url]
                  ,vc.[FollowCount]
                  ,vc.[VideoCount]
                  ,vc.[Rank]
                  ,vc.[IntroClip]
                  ,vc.[Status]
                  ,vc.[ModifiedBy]
                  ,vc.[ModifiedDate]
                  ,vc.[Cover]
                  ,vc.[Description]
                  ,vc.[MetaData]
                  ,vc.[MetaAvatar],
            ROW_NUMBER() OVER(ORDER BY vc.Id DESC) AS RowNumber 
            FROM [VideoChannel] vc
            where (@DateFrom is null and (1=1)) or (@DateFrom is not null and (CONVERT(datetime,vc.CreatedDate) >= CONVERT(datetime,@DateFrom) AND CONVERT(datetime,vc.CreatedDate)<=CONVERT(datetime,@DateTo)))
            ) AS temp
            WHERE RowNumber > @LowerBand AND RowNumber <= @UpperBand";

        public const string GetPlaylistInChannel = @"SELECT [PlaylistId] FROM VideoPlaylistInChannel WHERE [ChannelId]=@ChannelId";
        public const string GetRelation = @"SELECT [ChannelRelation] FROM VideoChannel WHERE [Id]=@Id";
    }
}
