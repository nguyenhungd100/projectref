﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ChannelVN.IMS2.Core.Models.DataStore.Scripts
{
    internal class VideoTag
    {
        public const string Add = @"INSERT INTO [VideoTag]
        ([Id]
            ,[ParentId]
            ,[Name]
            ,[UnsignName]
            
            ,[Description]
            ,[Url]
            ,[IsHotTag]
            ,[CreatedDate]
            ,[CreatedBy]
            ,[Status]
        ) VALUES (@Id
            ,@ParentId
            ,@Name
            ,@UnsignName
            
            ,@Description
            ,@Url
            ,@IsHotTag
            ,@CreatedDate
            ,@CreatedBy
            ,@Status
        );";
        public const string Update = @"UPDATE [VideoTag]
            SET [ParentId]=@ParentId
            ,[Name]=@Name
            ,[UnsignName]=@UnsignName
            
            ,[Description]=@Description
            ,[Url]=@Url
            ,[IsHotTag]=@IsHotTag
            ,[Status]=@Status  WHERE [Id]=@Id";
        public const string CountTag = @"SELECT count(Id) as Total FROM [VideoTag]";

        public const string InitAllVideoTag = @"DECLARE @UpperBand int, @LowerBand int
                                        
                                        SET @LowerBand  = (@PageIndex - 1) * @PageSize
                                        SET @UpperBand  = (@PageIndex * @PageSize)
                                        SELECT * FROM (
                                        SELECT t.* ,
                                        ROW_NUMBER() OVER(ORDER BY t.Id DESC) AS RowNumber 
                                        FROM VideoTag t 
                                        where (@DateFrom is null and (1=1)) or (@DateFrom is not null and (CONVERT(datetime,t.CreatedDate) >= CONVERT(datetime,@DateFrom) AND CONVERT(datetime,t.CreatedDate)<=CONVERT(datetime,@DateTo)))
                                        ) AS temp
                                        WHERE RowNumber > @LowerBand AND RowNumber <= @UpperBand";
    }
}
