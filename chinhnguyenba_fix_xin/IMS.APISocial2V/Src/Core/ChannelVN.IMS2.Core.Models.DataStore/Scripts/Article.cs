﻿namespace ChannelVN.IMS2.Core.Models.DataStore.Scripts
{
    internal class Article
    {
        public const string Add = @"
            BEGIN TRANSACTION;     
            INSERT INTO [Article]
                ([Id]
               ,[Type]
               ,[Url]
               ,[OriginalUrl]
               ,[Status]
               ,[Author]
               ,[CategoryId]
               ,[CreatedDate]
               ,[CreatedBy]
               ,[Location]
               ,[PublishMode]
               ,[PublishData]
               ,[DistributionDate]
               ,[ModifiedDate]
               ,[ModifiedBy]
               ,[DistributorId]
               ,[Tags]
                ,[CardType]
               ,[Title]
               ,[Sapo]
               ,[SubTitle]
               ,[Avatar1]
               ,[Avatar2]
               ,[Avatar3]
               ,[ParentId]
               ,[TemplateId]
               ,[Body]
               ,[BodyMeta]
                ,[Approvedby]
                ,[InstantViewId]
                ,[IsInstantView]
                ,[InstantViewUrl]
                ,[CommentMode]
                ,[NativeContent])
            VALUES
                (
                @Id
               ,@Type
               ,CASE WHEN COL_LENGTH('Article','Url')=-1 THEN @Url ELSE LEFT(@Url,COL_LENGTH('Article','Url')/2) END
               ,CASE WHEN COL_LENGTH('Article','OriginalUrl')=-1 THEN @OriginalUrl ELSE LEFT(@OriginalUrl,COL_LENGTH('Article','OriginalUrl')/2) END
               ,@Status
               ,CASE WHEN COL_LENGTH('Article','Author')=-1 THEN @Author ELSE LEFT(@Author,COL_LENGTH('Article','Author')/2) END
               ,@CategoryId
               ,@CreatedDate
               ,@CreatedBy
               ,CASE WHEN COL_LENGTH('Article','Location')=-1 THEN @Location ELSE LEFT(@Location,COL_LENGTH('Article','Location')/2) END
               ,@PublishMode
               ,@PublishData
               ,@DistributionDate
               ,@ModifiedDate
               ,@ModifiedBy
               ,@DistributorId
               ,@Tags
               ,@CardType
               ,CASE WHEN COL_LENGTH('Article','Title')=-1 THEN @Title ELSE LEFT(@Title,COL_LENGTH('Article','Title')/2) END
               ,@Sapo
               ,CASE WHEN COL_LENGTH('Article','SubTitle')=-1 THEN @SubTitle ELSE LEFT(@SubTitle,COL_LENGTH('Article','SubTitle')/2) END
               ,CASE WHEN COL_LENGTH('Article','Avatar1')=-1 THEN @Avatar1 ELSE LEFT(@Avatar1,COL_LENGTH('Article','Avatar1')/2) END
               ,CASE WHEN COL_LENGTH('Article','Avatar2')=-1 THEN @Avatar2 ELSE LEFT(@Avatar2,COL_LENGTH('Article','Avatar2')/2) END
               ,CASE WHEN COL_LENGTH('Article','Avatar3')=-1 THEN @Avatar3 ELSE LEFT(@Avatar3,COL_LENGTH('Article','Avatar3')/2) END
               ,@ParentId
               ,@TemplateId
               ,@Body
               ,@BodyMeta
                ,@ApprovedBy
                ,@InstantViewId
                ,@IsInstantView
                ,CASE WHEN COL_LENGTH('Article','InstantViewUrl')=-1 THEN @InstantViewUrl ELSE LEFT(@InstantViewUrl,COL_LENGTH('Article','InstantViewUrl')/2) END
                ,@CommentMode
                ,@NativeContent
                );
            
               
            IF(@@ROWCOUNT<>0)      
                INSERT INTO [NewsInAccount]
                ([AccountId]
                  ,[NewsId]
                  ,[PublishedDate]
                  ,[PublishedType])
                VALUES
                (@AccountId
                  ,@Id
                  ,getdate()
                  ,@PublishedType) ; 
            IF(@@ROWCOUNT<>0) 
                BEGIN
                    IF(LEN(@Tags)>0)
                        BEGIN
                            INSERT INTO [NewsInTag]
                            ([NewsId]
                            ,[TagId]
                            ,[Priority]
                            ,[TaggedDate])
                            SELECT @Id,tb.value,ROW_NUMBER() OVER (ORDER BY (SELECT null) ),@CreatedDate FROM STRING_SPLIT(@Tags,',') tb;
                            IF(@@ROWCOUNT= 0) ROLLBACK;
                            ELSE COMMIT;
                        END;
                    ELSE COMMIT;
                END;
            ELSE ROLLBACK;
            ";        

        public const string Update = @"
            BEGIN TRANSACTION;
            UPDATE [Article] 
                SET 
                [Url]=CASE WHEN COL_LENGTH('Article','Url')=-1 THEN @Url ELSE LEFT(@Url,COL_LENGTH('Article','Url')/2) END
               ,[OriginalUrl]=CASE WHEN COL_LENGTH('Article','OriginalUrl')=-1 THEN @OriginalUrl ELSE LEFT(@OriginalUrl,COL_LENGTH('Article','OriginalUrl')/2) END
               ,[Author]=CASE WHEN COL_LENGTH('Article','Author')=-1 THEN @Author ELSE LEFT(@Author,COL_LENGTH('Article','Author')/2) END
               ,[CategoryId]=@CategoryId
               ,[Location]=CASE WHEN COL_LENGTH('Article','Location')=-1 THEN @Location ELSE LEFT(@Location,COL_LENGTH('Article','Location')/2) END
               ,[PublishMode]=@PublishMode
               ,[PublishData]=@PublishData
               ,[DistributionDate]=@DistributionDate
               ,[ModifiedDate]=@ModifiedDate
               ,[ModifiedBy]=@ModifiedBy
               ,[Tags]=@Tags
               ,[Title]=CASE WHEN COL_LENGTH('Article','Title')=-1 THEN @Title ELSE LEFT(@Title,COL_LENGTH('Article','Title')/2) END
               ,[Sapo]=@Sapo
               ,[SubTitle]=CASE WHEN COL_LENGTH('Article','SubTitle')=-1 THEN @SubTitle ELSE LEFT(@SubTitle,COL_LENGTH('Article','SubTitle')/2) END
               ,[Avatar1]=CASE WHEN COL_LENGTH('Article','Avatar1')=-1 THEN @Avatar1 ELSE LEFT(@Avatar1,COL_LENGTH('Article','Avatar1')/2) END
               ,[Avatar2]=CASE WHEN COL_LENGTH('Article','Avatar2')=-1 THEN @Avatar2 ELSE LEFT(@Avatar2,COL_LENGTH('Article','Avatar2')/2) END
               ,[Avatar3]=CASE WHEN COL_LENGTH('Article','Avatar3')=-1 THEN @Avatar3 ELSE LEFT(@Avatar3,COL_LENGTH('Article','Avatar3')/2) END
               ,[ParentId]=@ParentId
               ,[TemplateId]=@TemplateId
               ,[Body]=@Body
               ,[BodyMeta]=@BodyMeta
               ,[ApprovedDate]=@ApprovedDate
               ,[ApprovedBy]=@ApprovedBy
                ,[InstantViewId]=@InstantViewId
                ,[IsInstantView] =@IsInstantView
                ,[InstantViewUrl]=CASE WHEN COL_LENGTH('Article','InstantViewUrl')=-1 THEN @InstantViewUrl ELSE LEFT(@InstantViewUrl,COL_LENGTH('Article','InstantViewUrl')/2) END
                ,[CommentMode]=@CommentMode
                ,[NativeContent]=@NativeContent
            WHERE [Id]=@Id  

            IF(@@ROWCOUNT<>0) 
                BEGIN
                    DELETE [NewsInTag] WHERE [NewsId]=@Id;
                    IF(LEN(@Tags)>0)
                    BEGIN
                        INSERT INTO [NewsInTag]
                        ([NewsId]
                        ,[TagId]
                        ,[Priority]
                        ,[TaggedDate])
                        SELECT @Id,tb.value,ROW_NUMBER() OVER (ORDER BY (SELECT null) ),@ModifiedDate FROM STRING_SPLIT(@Tags,',') tb;
                        IF(@@ROWCOUNT<>0) COMMIT; ELSE ROLLBACK;
                    END;
                    ELSE COMMIT;
                END;
            ELSE ROLLBACK;
            ";
        public const string UpdateStatus = @"UPDATE [Article] 
                SET [Status]=@Status,
                 [ModifiedDate]=@ModifiedDate,
                 [ModifiedBy]=@ModifiedBy,
                [DistributionDate]=@DistributionDate,
                [ApprovedDate]=@ApprovedDate,
                [ApprovedBy]=@ApprovedBy
            WHERE [Id]=@Id ";

        public const string GetById = @"SELECT * FROM [Article] WHERE [Id]=@Id";

        //NewsIntag
        public const string AddNewsInTag = @"INSERT INTO [NewsInTag]
                                            ([NewsId],[TagId],[TaggedDate],[Priority]) 
                                            VALUES
                                            (@NewsId,@TagId,getdate(),@Priority)
                                            ";
        public const string DeleteNewsInTag = @"DELETE [NewsInTag] WHERE NewsId=@NewsId";

        public const string Publish = @"UPDATE [News] 
                SET [Status]=@Status               
                ,[ModifiedDate]=@CreatedDate
                ,[DistributionDate]=@DistributionDate
                ,[Url]=@Url
                ,[OriginalUrl]=@OriginalUrl
            WHERE [Id]=@Id ;
            
          -- INSERT INTO [ItemStreamDistribution]
               -- ([Id]
                 -- ,[PublishStatus]                               
                 -- ,[CreatedDate]
                 -- ,[ItemStreamTempId]
                 -- ,[Title]
                  --,[MetaData])
           -- VALUES
                --(@Id
                --,@PublishStatus                              
               -- ,@CreatedDate
                --,@ItemStreamTempId
                --,@Title
                --,@MetaData) 

            --INSERT INTO [NewsDistribution]
                --([DistributorId]
                 -- ,[NewsId]                               
                 -- ,[ItemStreamId]
                  --,[SharedDate])
           -- VALUES
                --(@DistributorId
               -- ,@Id                              
                --,@ItemStreamId
                --,@CreatedDate) 
            ";

        public const string InsertNewsDistribution = @"INSERT INTO [NewsDistribution]
                ([DistributorId]
                  ,[NewsId]                               
                  ,[ItemStreamId]
                  ,[SharedDate]
                  ,[SharedZone])
            VALUES
                (@DistributorId
                ,@NewsId                              
                ,@ItemStreamId
                ,@SharedDate
                ,@SharedZone) 
            ";
        public const string InsertItemStream = @"
            BEGIN TRANSACTION;
            INSERT INTO [ItemStreamDistribution]
                ([Id]
                  ,[PublishStatus]                               
                  ,[CreatedDate]
                  ,[Note]
                  ,[ItemStreamtempId]
                  ,[Title]
                  ,[MetaData])
            VALUES
                (@Id
                ,@PublishStatus                              
                ,@CreatedDate
                ,@Note
                ,@ItemStreamtempId
                ,@Title
                ,@MetaData);
            IF(@@ROWCOUNT>0)
                IF(LEN(@ListNewsDistribution)>0)
                    BEGIN
                        INSERT INTO [NewsDistribution]
                        ([DistributorId]
                          ,[NewsId]                               
                          ,[ItemStreamId]
                          ,[SharedDate]
                          ,[SharedZone])
                        SELECT (SELECT v FROM (SELECT dt2.value as v,ROW_NUMBER()  OVER (ORDER BY (SELECT null) ) as n from STRING_SPLIT(dt.value,',') dt2 order by n asc offset 0 ROWS FETCH Next 1 rows only) dt3)
	                    ,(select v from (select dt2.value as v,ROW_NUMBER()  OVER (ORDER BY (SELECT null) ) as n from STRING_SPLIT(dt.value,',') dt2 order by n asc offset 1 ROWS FETCH Next 1 rows only) dt3)
                        ,(select v from (select dt2.value as v,ROW_NUMBER()  OVER (ORDER BY (SELECT null) ) as n from STRING_SPLIT(dt.value,',') dt2 order by n asc offset 2 ROWS FETCH Next 1 rows only) dt3)
                        ,(select v from (select dt2.value as v,ROW_NUMBER()  OVER (ORDER BY (SELECT null) ) as n from STRING_SPLIT(dt.value,',') dt2 order by n asc offset 3 ROWS FETCH Next 1 rows only) dt3)
                        ,(select v from (select dt2.value as v,ROW_NUMBER()  OVER (ORDER BY (SELECT null) ) as n from STRING_SPLIT(dt.value,',') dt2 order by n asc offset 4 ROWS FETCH Next 1 rows only) dt3)
                        from STRING_SPLIT(@ListNewsDistribution,'_') dt;
                        IF(@@ROWCOUNT>0) COMMIT; ELSE ROLLBACK;
                    END;
                ELSE COMMIT;
            ELSE COMMIT;
            ";

        public const string SaveItemStream = @"
            IF EXISTS (SELECT 1 FROM ItemStreamDistribution WHERE Id=@Id)
            BEGIN
                UPDATE [ItemStreamDistribution]
                    SET 
                    [Note]=@Note
                    ,[PublishStatus]=@PublishStatus
                    ,[ItemStreamTempId]=@ItemStreamTempId
                    ,[Title]=@Title
                    ,[MetaData]=@MetaData
                    ,[ModifiedDate]=@ModifiedDate
                WHERE Id = @Id
            END
            ELSE
            BEGIN
                BEGIN TRANSACTION;
                INSERT INTO [ItemStreamDistribution]
                    ([Id]
                      ,[PublishStatus]                               
                      ,[CreatedDate]
                      ,[Note]
                      ,[ItemStreamtempId]
                      ,[Title]
                      ,[MetaData])
                VALUES
                    (@Id
                    ,@PublishStatus                              
                    ,@CreatedDate
                    ,@Note
                    ,@ItemStreamtempId
                    ,@Title
                    ,@MetaData);
                IF(@@ROWCOUNT>0)
                    IF(LEN(@ListNewsDistribution)>0)
                        BEGIN
                            INSERT INTO [NewsDistribution]
                            ([DistributorId]
                                ,[NewsId]                               
                                ,[ItemStreamId]
                                ,[SharedDate]
                                ,[SharedZone])
                            SELECT (SELECT v FROM (SELECT dt2.value as v,ROW_NUMBER()  OVER (ORDER BY (SELECT null) ) as n from STRING_SPLIT(dt.value,',') dt2 order by n asc offset 0 ROWS FETCH Next 1 rows only) dt3)
	                        ,(select v from (select dt2.value as v,ROW_NUMBER()  OVER (ORDER BY (SELECT null) ) as n from STRING_SPLIT(dt.value,',') dt2 order by n asc offset 1 ROWS FETCH Next 1 rows only) dt3)
                            ,(select v from (select dt2.value as v,ROW_NUMBER()  OVER (ORDER BY (SELECT null) ) as n from STRING_SPLIT(dt.value,',') dt2 order by n asc offset 2 ROWS FETCH Next 1 rows only) dt3)
                            ,(select v from (select dt2.value as v,ROW_NUMBER()  OVER (ORDER BY (SELECT null) ) as n from STRING_SPLIT(dt.value,',') dt2 order by n asc offset 3 ROWS FETCH Next 1 rows only) dt3)
                            ,(select v from (select dt2.value as v,ROW_NUMBER()  OVER (ORDER BY (SELECT null) ) as n from STRING_SPLIT(dt.value,',') dt2 order by n asc offset 4 ROWS FETCH Next 1 rows only) dt3)
                            from STRING_SPLIT(@ListNewsDistribution,'_') dt;
                            IF(@@ROWCOUNT>0) COMMIT; ELSE ROLLBACK;
                        END;
                    ELSE COMMIT;
                ELSE ROLLBACK;
            END
            ";

        public const string UpdateItemStream = @"UPDATE [ItemStreamDistribution]
                SET 
                [Note]=@Note
                ,[PublishStatus]=@PublishStatus
                ,[ItemStreamTempId]=@ItemStreamTempId
                ,[Title]=@Title
                ,[MetaData]=@MetaData
                ,[ModifiedDate]=@ModifiedDate
            WHERE Id = @Id";
        public const string AcceptItemStream = @"UPDATE [ItemStreamDistribution]
                SET [PublishStatus]=@PublishStatus
                ,[AcceptedDate]=@AcceptedDate
                ,[Note]=@Note
                ,[ModifiedDate]=@ModifiedDate
            WHERE Id = @Id                                    
            ";

        public const string RejectItemStream = @"UPDATE [ItemStreamDistribution]
                SET [PublishStatus]=@PublishStatus
            WHERE Id = @Id                                   
            ";

        public const string CountNews = @"SELECT count(Id) as Total from [Article]                                    
            ";
        public const string CountItemStream = @"SELECT count(tbl1.Id) as Total  FROM [ItemStreamDistribution] tbl1
            INNER JOIN [NewsDistribution] tbl2 on tbl1.Id=tbl2.ItemStreamId
            INNER JOIN [News] tbl3 on tbl2.NewsId=tbl3.Id";
        public const string InitAllNews = @"DECLARE @UpperBand int, @LowerBand int
                                        
                                        SET @LowerBand  = (@PageIndex - 1) * @PageSize
                                        SET @UpperBand  = (@PageIndex * @PageSize)
                                        SELECT * FROM (
                                        SELECT n.* , mt.UrlSlug, mt.MetaTitle, mt.MetaDescription, mt.MetaAuthor,
                                        ROW_NUMBER() OVER(ORDER BY n.Id DESC) AS RowNumber 
                                        FROM News n 
                                        left join NewsSeoMeta mt on n.Id=mt.NewsId
                                        where n.Type=1 and (@DateFrom is null and (1=1)) or (@DateFrom is not null 
                                        and (CONVERT(datetime,n.CreatedDate) >= CONVERT(datetime,@DateFrom) 
                                        AND CONVERT(datetime,n.CreatedDate)<=CONVERT(datetime,@DateTo)))
                                        ) AS temp
                                        WHERE RowNumber > @LowerBand AND RowNumber <= @UpperBand";

        public const string InitAllItemStream = @"DECLARE @UpperBand int, @LowerBand int
                                        
                                        SET @LowerBand  = (@PageIndex - 1) * @PageSize
                                        SET @UpperBand  = (@PageIndex * @PageSize)
                                        SELECT * FROM (
                                        SELECT tbl1.* ,ROW_NUMBER() OVER(ORDER BY tbl1.Id DESC) AS RowNumber,tbl3.Type as Type   FROM [ItemStreamDistribution] tbl1
            INNER JOIN [NewsDistribution] tbl2 on tbl1.Id=tbl2.ItemStreamId
            INNER JOIN [News] tbl3 on tbl2.NewsId=tbl3.Id 
                                        where (@DateFrom is null and (1=1)) or (@DateFrom is not null 
                                        and (CONVERT(datetime,tbl1.CreatedDate) >= CONVERT(datetime,@DateFrom) 
                                        AND CONVERT(datetime,tbl1.CreatedDate)<=CONVERT(datetime,@DateTo)))
                                        ) AS temp
                                        WHERE temp.RowNumber > @LowerBand AND temp.RowNumber <= @UpperBand AND temp.Type IN (1,2,3)";

        public const string GetNewsInAccountByNewsId = @"SELECT * FROM [NewsInAccount] WHERE [NewsId]=@NewsId";

        public const string GetDistributionByNewsId = @"SELECT a.DistributorId,a.NewsId,a.ItemStreamId,a.SharedDate,b.PublishStatus,b.ItemStreamTempId,d.Name as DistributionName
                                        FROM [NewsDistribution] a 
                                        INNER JOIN [ItemStreamDistribution] b ON a.ItemStreamId=b.Id
                                        LEFT JOIN [Distributor] d on a.DistributorId=d.Id
                                        WHERE a.NewsId=@NewsId ";

        public const string GetDistributionByItemStreamId = @"SELECT a.DistributorId,a.NewsId,a.ItemStreamId,a.SharedDate
                                        FROM [NewsDistribution] a 
                                        WHERE a.ItemStreamId=@ItemStreamId ";
    }
}
