﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ChannelVN.IMS2.Core.Models.DataStore.Scripts
{
    internal class VideoPlaylist
    {
        public const string Add = @"
        BEGIN TRANSACTION;
        INSERT INTO [Playlist]
        (   [Id]
           ,[Type]
           ,[Url]
           ,[OriginalUrl]
           ,[Status]
           ,[Author]
           ,[CategoryId]
           ,[CreatedDate]
           ,[CreatedBy]
           ,[Location]
           ,[PublishMode]
           ,[PublishData]
           ,[DistributionDate]
           ,[DistributorId]
           ,[Tags]
           ,[CardType]
           ,[Name]
           ,[UnsignName]
           ,[Description]
           ,[LastInsertedDate]
           ,[Cover]
           ,[IntroClip]
           ,[PlaylistRelation]
           ,[MetaAvatar]
           ,[MetaData]
           ,[ApprovedBy]
           ,[CommentMode]
        ) VAlUES (
            @Id
           ,@Type
           ,CASE WHEN COL_LENGTH('Playlist','Url')=-1 THEN @Url ELSE LEFT(@Url,COL_LENGTH('Playlist','Url')/2) END
           ,CASE WHEN COL_LENGTH('Playlist','OriginalUrl')=-1 THEN @OriginalUrl ELSE LEFT(@OriginalUrl,COL_LENGTH('Playlist','OriginalUrl')/2) END
           ,@Status
           ,CASE WHEN COL_LENGTH('Playlist','Author')=-1 THEN @Author ELSE LEFT(@Author,COL_LENGTH('Playlist','Author')/2) END
           ,@CategoryId
           ,@CreatedDate
           ,@CreatedBy
           ,CASE WHEN COL_LENGTH('Playlist','Location')=-1 THEN @Location ELSE LEFT(@Location,COL_LENGTH('Playlist','Location')/2) END
           ,@PublishMode
           ,@PublishData
           ,@DistributionDate
           ,@DistributorId
           ,@Tags
           ,@CardType
           ,CASE WHEN COL_LENGTH('Playlist','Name')=-1 THEN @Name ELSE LEFT(@Name,COL_LENGTH('Playlist','Name')/2) END
           ,CASE WHEN COL_LENGTH('Playlist','UnsignName')=-1 THEN @UnsignName ELSE LEFT(@UnsignName,COL_LENGTH('Playlist','UnsignName')) END
           ,@Description
           ,@LastInsertedDate
           ,CASE WHEN COL_LENGTH('Playlist','Cover')=-1 THEN @Cover ELSE LEFT(@Cover,COL_LENGTH('Playlist','Cover')/2) END
           ,CASE WHEN COL_LENGTH('Playlist','IntroClip')=-1 THEN @IntroClip ELSE LEFT(@IntroClip,COL_LENGTH('Playlist','IntroClip')/2) END
           ,@PlaylistRelation
           ,@MetaAvatar
           ,@MetaData
           ,@ApprovedBy
           ,@CommentMode
        );
        
        IF(@@ROWCOUNT<>0)
        BEGIN
            INSERT INTO [NewsInAccount]
                    ([AccountId]
                      ,[NewsId]
                      ,[PublishedDate]
                      ,[PublishedType])
                    VALUES
                    (@AccountId
                      ,@Id
                      ,@PublishedDate
                      ,@PublishedType);
            IF(@@ROWCOUNT<>0)
            BEGIN
                IF(LEN(@VideoInPlaylist)>0)
                    BEGIN
                        INSERT INTO [VideoInPlaylist]([PlaylistId],[VideoId],[Priority],[PlayOnTime])
                        SELECT @Id
                        ,(SELECT v FROM (SELECT dt2.value as v,ROW_NUMBER()  OVER (ORDER BY (SELECT null) ) as n from STRING_SPLIT(dt.value,'_') dt2 order by n asc offset 0 ROWS FETCH Next 1 rows only) dt3)
	                    ,(select v from (select dt2.value as v,ROW_NUMBER()  OVER (ORDER BY (SELECT null) ) as n from STRING_SPLIT(dt.value,'_') dt2 order by n asc offset 1 ROWS FETCH Next 1 rows only) dt3)
                        ,(select v from (select dt2.value as v,ROW_NUMBER()  OVER (ORDER BY (SELECT null) ) as n from STRING_SPLIT(dt.value,'_') dt2 order by n asc offset 2 ROWS FETCH Next 1 rows only) dt3)
                        from STRING_SPLIT(@VideoInPlaylist,',') dt;

                        IF(@@ROWCOUNT<>0)
                        BEGIN
                            IF(LEN(@Tags)>0)
                            BEGIN
                                INSERT INTO [NewsInTag]
                                ([NewsId]
                                ,[TagId]
                                ,[Priority]
                                ,[TaggedDate])
                                SELECT @Id,tb.value,ROW_NUMBER() OVER (ORDER BY (SELECT null) ),@CreatedDate FROM STRING_SPLIT(@Tags,',') tb;
                                IF(@@ROWCOUNT=0) ROLLBACK;ELSE COMMIT
                            END;
                            ELSE COMMIT;
                        END;
                        ELSE ROLLBACK;
                    END;
                ELSE 
                BEGIN
                    IF(LEN(@Tags)>0)
                    BEGIN
                        INSERT INTO [NewsInTag]
                        ([NewsId]
                        ,[TagId]
                        ,[Priority]
                        ,[TaggedDate])
                        SELECT @Id,tb.value,ROW_NUMBER() OVER (ORDER BY (SELECT null) ),@CreatedDate FROM STRING_SPLIT(@Tags,',') tb;
                        IF(@@ROWCOUNT=0) ROLLBACK;ELSE COMMIT
                    END;
                    ELSE COMMIT;
                END;
            END;
            ELSE ROLLBACK;
        END;
        ELSE ROLLBACK;
        ";

        public const string Update = @"
        BEGIN TRANSACTION;
        UPDATE [Playlist]
        SET
           [Url]=CASE WHEN COL_LENGTH('Playlist','Url')=-1 THEN @Url ELSE LEFT(@Url,COL_LENGTH('Playlist','Url')/2) END
           ,[OriginalUrl]=CASE WHEN COL_LENGTH('Playlist','OriginalUrl')=-1 THEN @OriginalUrl ELSE LEFT(@OriginalUrl,COL_LENGTH('Playlist','OriginalUrl')/2) END
           ,[Author]=CASE WHEN COL_LENGTH('Playlist','Author')=-1 THEN @Author ELSE LEFT(@Author,COL_LENGTH('Playlist','Author')/2) END
           ,[CategoryId]=@CategoryId
           ,[Location]=CASE WHEN COL_LENGTH('Playlist','Location')=-1 THEN @Location ELSE LEFT(@Location,COL_LENGTH('Playlist','Location')/2) END
           ,[PublishMode]=@PublishMode
           ,[PublishData]=@PublishData
           --,[DistributionDate]=@DistributionDate
           ,[Tags]=@Tags
           ,[Name]=CASE WHEN COL_LENGTH('Playlist','Name')=-1 THEN @Name ELSE LEFT(@Name,COL_LENGTH('Playlist','Name')/2) END
           ,[UnsignName]=CASE WHEN COL_LENGTH('Playlist','UnsignName')=-1 THEN @UnsignName ELSE LEFT(@UnsignName,COL_LENGTH('Playlist','UnsignName')) END
           ,[Description]=@Description
           ,[LastInsertedDate]=@LastInsertedDate
           ,[Cover]=CASE WHEN COL_LENGTH('Playlist','Cover')=-1 THEN @Cover ELSE LEFT(@Cover,COL_LENGTH('Playlist','Cover')/2) END
           ,[IntroClip]=CASE WHEN COL_LENGTH('Playlist','IntroClip')=-1 THEN @IntroClip ELSE LEFT(@IntroClip,COL_LENGTH('Playlist','IntroClip')/2) END
           ,[PlaylistRelation]=@PlaylistRelation
           ,[MetaAvatar]=@MetaAvatar
           ,[MetaData]=@MetaData
           ,[ApprovedBy]=@ApprovedBy
           ,[ApprovedDate]=@ApprovedDate
           ,[CommentMode]=@CommentMode
        WHERE [Id]=@Id;
        
        IF(@@ROWCOUNT<>0)
        BEGIN
            DELETE [VideoInPlaylist] WHERE [PlaylistId]=@Id;
            IF(LEN(@VideoInPlaylist)>0)
                BEGIN
                    INSERT INTO [VideoInPlaylist]([PlaylistId],[VideoId],[Priority],[PlayOnTime])
                    SELECT @Id
                    ,(SELECT v FROM (SELECT dt2.value as v,ROW_NUMBER()  OVER (ORDER BY (SELECT null) ) as n from STRING_SPLIT(dt.value,'_') dt2 order by n asc offset 0 ROWS FETCH Next 1 rows only) dt3)
	                ,(select v from (select dt2.value as v,ROW_NUMBER()  OVER (ORDER BY (SELECT null) ) as n from STRING_SPLIT(dt.value,'_') dt2 order by n asc offset 1 ROWS FETCH Next 1 rows only) dt3)
                    ,(select v from (select dt2.value as v,ROW_NUMBER()  OVER (ORDER BY (SELECT null) ) as n from STRING_SPLIT(dt.value,'_') dt2 order by n asc offset 2 ROWS FETCH Next 1 rows only) dt3)
                    from STRING_SPLIT(@VideoInPlaylist,',') dt;
                    
                    IF(@@ROWCOUNT<>0)
                        COMMIT;
                    ELSE ROLLBACK;
                END;
            ELSE 
                COMMIT;
        END;
        ELSE ROLLBACK;
        ";

        public const string CountVideoPlaylist = @"SELECT count(id) as Total FROM [Playlist]";
        public const string InitAllVideoPlaylist = @"DECLARE @UpperBand int, @LowerBand int
                                        
            SET @LowerBand  = (@PageIndex - 1) * @PageSize
            SET @UpperBand  = (@PageIndex * @PageSize)
            SELECT * FROM (
            SELECT 
                 v.*,
            ROW_NUMBER() OVER(ORDER BY v.Id DESC) AS RowNumber 
            FROM [Playlist] v 
            where  (@DateFrom is null and (1=1)) or (@DateFrom is not null and (CONVERT(datetime,v.CreatedDate) >= CONVERT(datetime,@DateFrom) AND CONVERT(datetime,v.CreatedDate)<=CONVERT(datetime,@DateTo)))
            ) AS temp
            WHERE RowNumber > @LowerBand AND RowNumber <= @UpperBand";
        public const string UpdateStatus = @"UPDATE [Playlist] 
                SET [Status]=@Status,
                 [ModifiedDate]=@ModifiedDate,
                 [ModifiedBy]=@ModifiedBy,
                [DistributionDate]=@DistributionDate,
                [ApprovedBy]=@ApprovedBy,
                [ApprovedDate]=@ApprovedDate
            WHERE [Id]=@Id ";

        public const string AddVideo = @"
        BEGIN TRANSACTION;
        IF(LEN(@PlaylistIds)>0)
            BEGIN
                UPDATE Playlist
                SET ModifiedBy = @ModifiedBy
                    ,ModifiedDate = @ModifiedDate
                    ,LastInsertedDate = @LastInsertedDate
                WHERE Id IN (SELECT dt.value FROM STRING_SPLIT(@PlaylistIds,',') dt);
                IF(@@ROWCOUNT>0)
                    BEGIN
                        INSERT INTO VideoInPlaylist(PlaylistId,VideoId,Priority,PlayOnTime)
                        SELECT dt.value,@VideoId,0,@PublishedDate
                        FROM STRING_SPLIT(@PlaylistIds,',') dt;
                        IF(@@ROWCOUNT>0) COMMIT; ELSE ROLLBACK;
                    END;
                ELSE ROLLBACK;
            END;
        ELSE
        COMMIT;
        ";
        //public const string InitAll= @"DECLARE @UpperBand int, @LowerBand int
        // SET @LowerBand  = (@PageIndex - 1) * @PageSize
        // SET @UpperBand  = (@PageIndex* @PageSize)
        //SELECT* FROM(
        //SELECT
        //    v.[Id]
        //        ,v.[CategoryId]
        //        ,v.[Name]
        //        ,v.[UnsignName]
        //        ,v.[Description]
        //        ,v.[Status]
        //        ,v.[Mode]
        //        ,v.[Avatar]
        //        ,v.[Priority]
        //        ,v.[CreatedDate]
        //        ,v.[CreatedBy]
        //        ,v.[ModifiedDate]
        //        ,v.[ModifiedBy]
        //        ,v.[PublishedDate]
        //        ,v.[PublishedBy]
        //        ,v.[EditedDate]
        //        ,v.[EditedBy]
        //        ,v.[LastInsertedDate]
        //        ,v.[VideoCount]
        //        ,v.[FollowCount]
        //        ,v.[Url]
        //        ,v.[Cover]
        //        ,v.[IntroClip]
        //        ,v.[MetaData]
        //        ,v.[MetaAvatar],
        //ROW_NUMBER() OVER(ORDER BY v.Id DESC) AS RowNumber
        //FROM Video v
        //where(@DateFrom is null and (1=1)) or(@DateFrom is not null and (CONVERT(datetime, v.CreatedDate) >= CONVERT(datetime, @DateFrom) AND CONVERT(datetime, v.CreatedDate)<=CONVERT(datetime, @DateTo)))
        //) AS temp
        //WHERE RowNumber > @LowerBand AND RowNumber <= @UpperBand";
        public const string GetVideoPlaylistRelation = @"SELECT TOP 1 [PlaylistRelation] as Ids FROM [VideoPlaylist] WHERE [Id]=@Id";

        public const string GetVideoInPlaylist = @"SELECT [PlaylistId],[VideoId],[Priority],[PlayOnTime] FROM [VideoInPlaylist] WHERE [PlaylistId]=@PlaylistId ORDER BY [Priority] ASC";
    }
}
