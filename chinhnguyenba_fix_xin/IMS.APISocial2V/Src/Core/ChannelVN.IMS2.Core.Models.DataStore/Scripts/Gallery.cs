﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ChannelVN.IMS2.Core.Models.DataStore.Scripts
{
    internal class Gallery
    {
        public const string Add = @"BEGIN TRANSACTION  ; 
            INSERT INTO [Gallery]            
                ([Id]
                  ,[Type]
                  ,[Url]
                  ,[OriginalUrl]
                  ,[Status]
                  ,[Author]
                  ,[CategoryId]
                  ,[CreatedDate]
                  ,[CreatedBy]
                  ,[Location]
                  ,[PublishMode]
                  ,[PublishData]
                  ,[DistributionDate]
                  ,[Tags]                  
                  ,[Name]
                  ,[CardType]
                  ,[DistributorId]
                ,[UnsignName]
                ,[Description]
                ,[TemplateId]
                --,[AlbumRelation]
                ,[ApprovedBy]
                ,[MetaAvatar]
                ,[MetaData]
                ,[LastInsertedDate]
                ,[Cover]
                ,[CommentMode]
                )
            VALUES
                (@Id
                  ,@Type
                  ,CASE WHEN COL_LENGTH('Gallery','Url')=-1 THEN @Url ELSE LEFT(@Url,COL_LENGTH('Gallery','Url')/2) END
                  ,CASE WHEN COL_LENGTH('Gallery','OriginalUrl')=-1 THEN @OriginalUrl ELSE LEFT(@OriginalUrl,COL_LENGTH('Gallery','OriginalUrl')/2) END
                  ,@Status
                  ,CASE WHEN COL_LENGTH('Gallery','Author')=-1 THEN @Author ELSE LEFT(@Author,COL_LENGTH('Gallery','Author')/2) END
                  ,@CategoryId
                  ,@CreatedDate
                  ,@CreatedBy
                  ,CASE WHEN COL_LENGTH('Gallery','Location')=-1 THEN @Location ELSE LEFT(@Location,COL_LENGTH('Gallery','Location')/2) END
                  ,@PublishMode
                  ,@PublishData
                  ,@DistributionDate                  
                  ,@Tags                  
                  ,CASE WHEN COL_LENGTH('Gallery','Name')=-1 THEN @Name ELSE LEFT(@Name,COL_LENGTH('Gallery','Name')/2) END
                  ,@CardType
                  ,@DistributorId
                ,CASE WHEN COL_LENGTH('Gallery','UnsignName')=-1 THEN @UnsignName ELSE LEFT(@UnsignName,COL_LENGTH('Gallery','UnsignName')) END
                ,@Description
                ,@TemplateId
                --,@AlbumRelation
                ,@ApprovedBy
                ,@MetaAvatar
                ,@MetaData
                ,@LastInsertedDate
                ,@Cover
                ,@CommentMode);

            IF(@@ROWCOUNT<>0) 
                BEGIN
                    INSERT INTO [NewsInAccount]
                    ([AccountId]
                      ,[NewsId]
                      ,[PublishedDate]
                      ,[PublishedType])
                    VALUES
                    (@AccountId
                      ,@Id
                      ,@PublishedDate
                      ,@PublishedType);
                    IF(@@ROWCOUNT<>0)
                       COMMIT;
                    ELSE ROLLBACK;
                END
            ELSE ROLLBACK;
            ";

        //public const string Update = @"
        //BEGIN TRANSACTION;    
        //    UPDATE [Gallery] 
        //        SET [Url]= CASE WHEN COL_LENGTH('Gallery','Url')=-1 THEN @Url ELSE LEFT(@Url,COL_LENGTH('Gallery','Url')/2) END
        //        ,[Author]=CASE WHEN COL_LENGTH('Gallery','Author')=-1 THEN @Author ELSE LEFT(@Author,COL_LENGTH('Gallery','Author')/2) END              
        //        ,[CategoryId]=@CategoryId
        //        ,[Location]=CASE WHEN COL_LENGTH('Gallery','Location')=-1 THEN @Location ELSE LEFT(@Location,COL_LENGTH('Gallery','Location')/2) END
        //        ,[PublishData]=@PublishData
        //        ,[DistributionDate]=@DistributionDate
        //        ,[ModifiedBy]=@ModifiedBy
        //        ,[ModifiedDate]=@ModifiedDate
        //        ,[Tags]=@Tags                       
        //        ,[Name]=CASE WHEN COL_LENGTH('Gallery','Name')=-1 THEN @Name ELSE LEFT(@Name,COL_LENGTH('Gallery','Name')/2) END
        //        ,[UnsignName]=CASE WHEN COL_LENGTH('Gallery','UnsignName')=-1 THEN @UnsignName ELSE LEFT(@UnsignName,COL_LENGTH('Gallery','UnsignName')) END
        //        ,[Description]=@Description
        //        ,[Cover]=@Cover
        //        ,[TemplateId]=@TemplateId
        //        --,[AlbumRelation]=@AlbumRelation
        //        ,[ApprovedBy]=@ApprovedBy
        //        ,[ApprovedDate]=@ApprovedDate
        //        ,[MetaAvatar]=@MetaAvatar
        //        ,[MetaData]=@MetaData
        //        ,[CommentMode]=@CommentMode
        //    WHERE [Id]=@Id ;   
        //    IF(@@ROWCOUNT<>0)
        //        COMMIT;
        //    ELSE ROLLBACK;
        //END;";

        public const string Update = @"UPDATE [Gallery] 
                SET [Url]= CASE WHEN COL_LENGTH('Gallery','Url')=-1 THEN @Url ELSE LEFT(@Url,COL_LENGTH('Gallery','Url')/2) END
                ,[Author]=CASE WHEN COL_LENGTH('Gallery','Author')=-1 THEN @Author ELSE LEFT(@Author,COL_LENGTH('Gallery','Author')/2) END              
                ,[CategoryId]=@CategoryId
                ,[Location]=CASE WHEN COL_LENGTH('Gallery','Location')=-1 THEN @Location ELSE LEFT(@Location,COL_LENGTH('Gallery','Location')/2) END
                ,[PublishData]=@PublishData
                ,[DistributionDate]=@DistributionDate
                ,[ModifiedBy]=@ModifiedBy
                ,[ModifiedDate]=@ModifiedDate
                ,[Tags]=@Tags                       
                ,[Name]=CASE WHEN COL_LENGTH('Gallery','Name')=-1 THEN @Name ELSE LEFT(@Name,COL_LENGTH('Gallery','Name')/2) END
                ,[UnsignName]=CASE WHEN COL_LENGTH('Gallery','UnsignName')=-1 THEN @UnsignName ELSE LEFT(@UnsignName,COL_LENGTH('Gallery','UnsignName')) END
                ,[Description]=@Description
                ,[Cover]=@Cover
                ,[TemplateId]=@TemplateId
                --,[AlbumRelation]=@AlbumRelation
                ,[ApprovedBy]=@ApprovedBy
                ,[ApprovedDate]=@ApprovedDate
                ,[MetaAvatar]=@MetaAvatar
                ,[MetaData]=@MetaData
                ,[CommentMode]=@CommentMode
            WHERE [Id]=@Id ;";

        public const string UpdateStatus = @"UPDATE [Gallery] 
                SET [Status]=@Status,
                    [ModifiedBy]=@ModifiedBy
                   ,[ModifiedDate]=@ModifiedDate
                    ,[DistributionDate]=@DistributionDate
                    ,[ApprovedBy]=@ApprovedBy
                    ,[ApprovedDate]=@ApprovedDate
            WHERE [Id]=@Id ";

        public const string AddPhoto = @"
        BEGIN TRANSACTION;
        IF(LEN(@AlbumIds)>0)
            BEGIN
                UPDATE Gallery
                SET ModifiedBy = @ModifiedBy
                    ,ModifiedDate = @ModifiedDate
                    ,LastInsertedDate = @LastInsertedDate
                WHERE Id IN (SELECT dt.value FROM STRING_SPLIT(@AlbumIds,',') dt);
                IF(@@ROWCOUNT>0)
                    BEGIN
                        INSERT INTO PhotoInAlbum(AlbumId,PhotoId,Priority,PublishedDate)
                        SELECT dt.value,@PhotoId,0,@PublishedDate
                        FROM STRING_SPLIT(@AlbumIds,',') dt;
                        IF(@@ROWCOUNT>0) COMMIT; ELSE ROLLBACK;
                    END;
                ELSE ROLLBACK;
            END;
        ELSE
        COMMIT;
        ";
        public const string GetById = @"SELECT * FROM [Gallery] WHERE [Id]=@Id";

        public const string CountAlbum = @"SELECT count(Id) as Total from Gallery ";

        public const string InitAllAlbum = @"DECLARE @UpperBand int, @LowerBand int
                                        
                                        SET @LowerBand  = (@PageIndex - 1) * @PageSize
                                        SET @UpperBand  = (@PageIndex * @PageSize)
                                        SELECT * FROM (
                                        SELECT d.* ,
                                        ROW_NUMBER() OVER(ORDER BY d.Id DESC) AS RowNumber 
                                        FROM Gallery d 
                                        where (@DateFrom is null and (1=1)) or (@DateFrom is not null and (CONVERT(datetime,d.CreatedDate) >= CONVERT(datetime,@DateFrom) AND CONVERT(datetime,d.CreatedDate)<=CONVERT(datetime,@DateTo)))
                                        ) AS temp
                                        WHERE RowNumber > @LowerBand AND RowNumber <= @UpperBand";
    }
}
