﻿namespace ChannelVN.IMS2.Core.Models.DataStore.Scripts
{
    internal class NewsCrawler
    {
        public const string Add = @"INSERT INTO [NewsCrawler]
            ([Id]
              ,[Source]
              ,[Link]
              ,[Title]
              ,[Avatar]
              ,[Description]
              ,[CrawledBy]
              ,[CrawledDate]
              ,[Status]              
              ,[Sapo]
              ,[OfficerId])
            VALUES
                (@Id
                  ,@Source
                  ,@Link
                  ,@Title
                  ,@Avatar
                  ,@Description
                  ,@CrawledBy
                  ,@CrawledDate
                  ,@Status
                  ,@Sapo
                  ,@OfficerId)
            ";
        public const string Update = @"UPDATE [NewsCrawler]
                SET [Source]=@Source
                ,[Link]=@Link
                ,[Title]=@Title
                ,[Avatar]=@Avatar
                ,[Description]=@Description              
                ,[Status]=@Status           
                ,[Sapo]=@Sapo
                ,[ScheduleBy]=@ScheduleBy
                ,[ScheduleDate]=@ScheduleDate
                ,[MediaId]=@MediaId
            WHERE [Id] = @Id
            ";

        public const string GetById = @"SELECT * FROM [Tag] WHERE [Id]=@Id";

        public const string InitAllTag = @"DECLARE @UpperBand int, @LowerBand int
                                        
                                        SET @LowerBand  = (@PageIndex - 1) * @PageSize
                                        SET @UpperBand  = (@PageIndex * @PageSize)
                                        SELECT * FROM (
                                        SELECT t.* ,
                                        ROW_NUMBER() OVER(ORDER BY t.Id DESC) AS RowNumber 
                                        FROM Tag t 
                                        where (@DateFrom is null and (1=1)) or (@DateFrom is not null and (CONVERT(datetime,t.CreatedDate) >= CONVERT(datetime,@DateFrom) AND CONVERT(datetime,t.CreatedDate)<=CONVERT(datetime,@DateTo)))
                                        ) AS temp
                                        WHERE RowNumber > @LowerBand AND RowNumber <= @UpperBand";

        public const string CountTag = @"SELECT count(Id) as Total from Tag ";
    }
}
