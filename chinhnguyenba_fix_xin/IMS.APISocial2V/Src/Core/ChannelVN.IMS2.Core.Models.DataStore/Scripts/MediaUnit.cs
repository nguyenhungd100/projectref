﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ChannelVN.IMS2.Core.Models.DataStore.Scripts
{
    internal class MediaUnit
    {
        public const string Add = @"BEGIN TRANSACTION  ; 
            INSERT INTO [MediaUnit]            
                ([Id]
                  ,[Type]
                  ,[Url]
                  ,[OriginalUrl]
                  ,[Status]
                  ,[Author]
                  ,[CategoryId]
                  ,[CreatedDate]
                  ,[CreatedBy]
                  ,[Location]
                  ,[PublishMode]
                  ,[PublishData]
                  ,[DistributionDate]                  
                  ,[Tags]
                  ,[TemplateId]
                  ,[Caption]
                  ,[CardType]
                  ,[DistributorId]
                  ,[AllowSale]
                  ,[CallPhone]
                  ,[Email]
                  ,[ApprovedBy]
                  ,[CommentMode]
                )
            VALUES
                (@Id
                  ,@Type
                  ,CASE WHEN COL_LENGTH('MediaUnit','Url')=-1 THEN @Url ELSE LEFT(@Url,COL_LENGTH('MediaUnit','Url')/2) END
                  ,CASE WHEN COL_LENGTH('MediaUnit','OriginalUrl')=-1 THEN @OriginalUrl ELSE LEFT(@OriginalUrl,COL_LENGTH('MediaUnit','OriginalUrl')/2) END
                  ,@Status
                  ,CASE WHEN COL_LENGTH('MediaUnit','Author')=-1 THEN @Author ELSE LEFT(@Author,COL_LENGTH('MediaUnit','Author')/2) END
                  ,@CategoryId
                  ,@CreatedDate
                  ,@CreatedBy
                  ,CASE WHEN COL_LENGTH('MediaUnit','Location')=-1 THEN @Location ELSE LEFT(@Location,COL_LENGTH('MediaUnit','Location')/2) END
                  ,@PublishMode
                  ,@PublishData
                  ,@DistributionDate                  
                  ,@Tags
                  ,@TemplateId
                  ,@Caption
                  ,@CardType
                  ,@DistributorId
                  ,@AllowSale
                  ,CASE WHEN COL_LENGTH('MediaUnit','CallPhone')=-1 THEN @CallPhone ELSE LEFT(@CallPhone,COL_LENGTH('MediaUnit','CallPhone')) END
                  ,CASE WHEN COL_LENGTH('MediaUnit','Email')=-1 THEN @Email ELSE LEFT(@Email,COL_LENGTH('MediaUnit','Email')) END
                  ,@ApprovedBy
                  ,@CommentMode);

            IF(@@ROWCOUNT<>0) 
                BEGIN
                    INSERT INTO [NewsInAccount]
                    ([AccountId]
                      ,[NewsId]
                      ,[PublishedDate]
                      ,[PublishedType])
                    VALUES
                    (@AccountId
                      ,@Id
                      ,@PublishedDate
                      ,@PublishedType);
                    IF(@@ROWCOUNT<>0)
                        COMMIT;
                    ELSE ROLLBACK;
                END
            ELSE ROLLBACK;
            ";

        public const string Update = @"UPDATE [MediaUnit] 
                  SET [Url]=CASE WHEN COL_LENGTH('MediaUnit','Url')=-1 THEN @Url ELSE LEFT(@Url,COL_LENGTH('MediaUnit','Url')/2) END
                    ,[Author]=CASE WHEN COL_LENGTH('MediaUnit','Author')=-1 THEN @Author ELSE LEFT(@Author,COL_LENGTH('MediaUnit','Author')/2) END                    
                    ,[CategoryId]=@CategoryId
                    ,[Location]=CASE WHEN COL_LENGTH('MediaUnit','Location')=-1 THEN @Location ELSE LEFT(@Location,COL_LENGTH('MediaUnit','Location')/2) END
                    ,[PublishData]=@PublishData
                    ,[DistributionDate]=@DistributionDate
                    ,[ModifiedBy]=@ModifiedBy
                    ,[ModifiedDate]=@ModifiedDate
                    ,[Tags]=@Tags   
                    ,[TemplateId]=@TemplateId   
                    ,[Caption]=@Caption   
                    ,[AllowSale]=@AllowSale
                    ,[CallPhone]=CASE WHEN COL_LENGTH('MediaUnit','CallPhone')=-1 THEN @CallPhone ELSE LEFT(@CallPhone,COL_LENGTH('MediaUnit','CallPhone')) END
                    ,[Email]=CASE WHEN COL_LENGTH('MediaUnit','Email')=-1 THEN @Email ELSE LEFT(@Email,COL_LENGTH('MediaUnit','Email')) END
                    ,[ApprovedBy]=@ApprovedBy
                    ,[ApprovedDate]=@ApprovedDate
                    ,[CommentMode]=@CommentMode
            WHERE [Id]=@Id                
            ";

        public const string UpdateStatus = @"UPDATE [MediaUnit] 
                SET [Status]=@Status,
                    [ModifiedBy]=@ModifiedBy
                   ,[ModifiedDate]=@ModifiedDate
                    ,[DistributionDate]=@DistributionDate
                    ,[ApprovedBy]=@ApprovedBy
                    ,[ApprovedDate]=@ApprovedDate
            WHERE [Id]=@Id ";

        public const string GetById = @"SELECT * FROM [MediaUnit] WHERE [Id]=@Id";

        public const string CountMediaUnit = @"SELECT count(Id) as Total from MediaUnit ";

        public const string InitAllMediaUnit = @"DECLARE @UpperBand int, @LowerBand int
                                        
                                        SET @LowerBand  = (@PageIndex - 1) * @PageSize
                                        SET @UpperBand  = (@PageIndex * @PageSize)
                                        SELECT * FROM (
                                        SELECT d.* ,
                                        ROW_NUMBER() OVER(ORDER BY d.Id DESC) AS RowNumber 
                                        FROM MediaUnit d 
                                        where (@DateFrom is null and (1=1)) or (@DateFrom is not null and (CONVERT(datetime,d.CreatedDate) >= CONVERT(datetime,@DateFrom) AND CONVERT(datetime,d.CreatedDate)<=CONVERT(datetime,@DateTo)))
                                        ) AS temp
                                        WHERE RowNumber > @LowerBand AND RowNumber <= @UpperBand";
    }
}
