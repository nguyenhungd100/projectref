﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ChannelVN.IMS2.Core.Models.DataStore.Scripts
{
    internal class System
    {
        public const string ConvertData = @"
            BEGIN TRANSACTION;
            IF((SELECT count(Id) FROM [Account] WHERE [Mobile] = @Mobile AND [Id] <> @Id) > 0)
            BEGIN
                DECLARE @UserIdOld BIGINT;
                SET @UserIdOld = (SELECT TOP 1 Id FROM [Account] WHERE [Mobile] = @Mobile AND [Id]<>@Id);
                --Update account info
                
                    
                --Update AccountMember
                UPDATE [AccountMember] SET [OfficerId] = @Id WHERE [OfficerId] = @UserIdOld;
                UPDATE [AccountMember] SET [MemberId] = @Id WHERE [MemberId] = @UserIdOld;
                --Update Article
                UPDATE [Article] SET [CreatedBy] = Convert(Varchar(100),@Id) WHERE [CreatedBy] = Convert(Varchar(100),@UserIdOld);
                --Update Beam
                UPDATE [Beam] SET [CreatedBy] = Convert(Varchar(100),@Id) WHERE [CreatedBy] = Convert(Varchar(100),@UserIdOld);
                --Update Board
                UPDATE [Board] SET [CreatedBy] = Convert(Varchar(100),@Id) WHERE [CreatedBy] = Convert(Varchar(100),@UserIdOld);
                --Update Distributor
                UPDATE [Distributor] SET [CreatedBy] = Convert(Varchar(100),@Id) WHERE [CreatedBy] = Convert(Varchar(100),@UserIdOld);
                --Update MediaUnit
                UPDATE [MediaUnit] SET [CreatedBy] = Convert(Varchar(100),@Id) WHERE [CreatedBy] = Convert(Varchar(100),@UserIdOld);
                --Update NewsInAccount
                UPDATE [NewsInAccount] SET [AccountId] = @Id WHERE [AccountId] = @UserIdOld;
                --Update Photo
                UPDATE [Photo] SET [CreatedBy] = Convert(Varchar(100),@Id) WHERE [CreatedBy] = Convert(Varchar(100),@UserIdOld);
                --Update Playlist
                UPDATE [Playlist] SET [CreatedBy] = Convert(Varchar(100),@Id) WHERE [CreatedBy] = Convert(Varchar(100),@UserIdOld);
                --Update Post
                UPDATE [Post] SET [CreatedBy] = Convert(Varchar(100),@Id) WHERE [CreatedBy] = Convert(Varchar(100),@UserIdOld);
                --Update ShareLink
                UPDATE [ShareLink] SET [CreatedBy] = Convert(Varchar(100),@Id) WHERE [CreatedBy] = Convert(Varchar(100),@UserIdOld);

                --Update Tag
                UPDATE [Tag] SET [CreatedBy] = Convert(Varchar(100),@Id) WHERE [CreatedBy] = Convert(Varchar(100),@UserIdOld);
                --Update Template
                UPDATE [Template] SET [CreatedBy] = Convert(Varchar(100),@Id) WHERE [CreatedBy] = Convert(Varchar(100),@UserIdOld);
                --Update TemplateCategory
                UPDATE [TemplateCategory] SET [CreatedBy] = Convert(Varchar(100),@Id) WHERE [CreatedBy] = Convert(Varchar(100),@UserIdOld);
                --Update Video
                UPDATE [Video] SET [CreatedBy] = Convert(Varchar(100),@Id) WHERE [CreatedBy] = Convert(Varchar(30),@UserIdOld);

                --Delete old account
               --DECLARE @old_id bigint;
                --SET @old_id = (SELECT [Id] FROM [Account]  WHERE [Id] <> @Id AND [Mobile] = @Mobile;
                DELETE [Account] WHERE [Id] <> @Id AND [Mobile] = @Mobile;
                --DELETE [UserProfile] WHERE [Id] <> @Id AND [Mobile] = @Mobile;
            END;
            COMMIT;
        ";
    }
}
