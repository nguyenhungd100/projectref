﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ChannelVN.IMS2.Core.Models.DataStore.Scripts
{
    internal class Video
    {
        public const string Add = @"
            BEGIN TRANSACTION  ;     
         INSERT INTO [Video](
            [Id]
           ,[Type]
           ,[Url]
           ,[OriginalUrl]
           ,[Status]
           ,[Author]
           ,[CategoryId]
           ,[CreatedDate]
           ,[CreatedBy]
           ,[Location]
           ,[PublishMode]
           ,[PublishData]
           ,[DistributionDate]
           ,[DistributorId]
           ,[Tags]
           ,[CardType]
           ,[Name]
           ,[UnsignName]
           ,[Description]
           ,[HtmlCode]
           ,[KeyVideo]
           ,[UploadedDate]
           ,[Source]
           ,[VideoRelation]
           ,[FileName]
           ,[Duration]
           ,[Size]
           ,[Capacity]
           ,[AllowAdv]
           ,[IsRemovedLogo]
           ,[IsConverted]
           ,[PolicyContentId]
           ,[OriginalId]
           ,[Trailer]
           ,[MetaAvatar]
           ,[DisplayStyle]
           ,[MetaData]
           ,[ApprovedBy]
        ) VALUES (
            @Id
           ,@Type
           ,CASE WHEN COL_LENGTH('Video','Url')=-1 THEN @Url ELSE LEFT(@Url,COL_LENGTH('Video','Url')/2) END
           ,CASE WHEN COL_LENGTH('Video','OriginalUrl')=-1 THEN @OriginalUrl ELSE LEFT(@OriginalUrl,COL_LENGTH('Video','OriginalUrl')/2) END
           ,@Status
           ,CASE WHEN COL_LENGTH('Video','Author')=-1 THEN @Author ELSE LEFT(@Author,COL_LENGTH('Video','Author')/2) END
           ,@CategoryId
           ,@CreatedDate
           ,@CreatedBy
           ,CASE WHEN COL_LENGTH('Video','Location')=-1 THEN @Location ELSE LEFT(@Location,COL_LENGTH('Video','Location')/2) END
           ,@PublishMode
           ,@PublishData
           ,@DistributionDate
           ,@DistributorId
           ,@Tags
           ,@CardType
           ,CASE WHEN COL_LENGTH('Video','Name')=-1 THEN @Name ELSE LEFT(@Name,COL_LENGTH('Video','Name')/2) END
           ,CASE WHEN COL_LENGTH('Video','UnsignName')=-1 THEN @UnsignName ELSE LEFT(@UnsignName,COL_LENGTH('Video','UnsignName')) END
           ,@Description
           ,@HtmlCode
           ,CASE WHEN COL_LENGTH('Video','KeyVideo')=-1 THEN @KeyVideo ELSE LEFT(@KeyVideo,COL_LENGTH('Video','KeyVideo')) END
           ,@UploadedDate
           ,CASE WHEN COL_LENGTH('Video','Source')=-1 THEN @Source ELSE LEFT(@Source,COL_LENGTH('Video','Source')/2) END
           ,CASE WHEN COL_LENGTH('Video','VideoRelation')=-1 THEN @VideoRelation ELSE LEFT(@VideoRelation,COL_LENGTH('Video','VideoRelation')) END
           ,CASE WHEN COL_LENGTH('Video','FileName')=-1 THEN @FileName ELSE LEFT(@FileName,COL_LENGTH('Video','FileName')) END
           ,CASE WHEN COL_LENGTH('Video','Duration')=-1 THEN @Duration ELSE LEFT(@Duration,COL_LENGTH('Video','Duration')) END
           ,CASE WHEN COL_LENGTH('Video','Size')=-1 THEN @Size ELSE LEFT(@Size,COL_LENGTH('Video','Size')) END
           ,@Capacity
           ,@AllowAdv
           ,@IsRemovedLogo
           ,@IsConverted
           ,CASE WHEN COL_LENGTH('Video','PolicyContentId')=-1 THEN @PolicyContentId ELSE LEFT(@PolicyContentId,COL_LENGTH('Video','PolicyContentId')) END
           ,@OriginalId
           ,CASE WHEN COL_LENGTH('Video','Trailer')=-1 THEN @Trailer ELSE LEFT(@Trailer,COL_LENGTH('Video','Trailer')/2) END
           ,@MetaAvatar
           ,@DisplayStyle
           ,@MetaData
           ,@ApprovedBy
        );
        
        IF(@@ROWCOUNT<>0) 
                INSERT INTO [NewsInAccount]
                ([AccountId]
                  ,[NewsId]
                  ,[PublishedDate]
                  ,[PublishedType])
                VALUES
                (@AccountId
                  ,@Id
                  ,@PublishedDate
                  ,@PublishedType) ;
        IF(@@ROWCOUNT<>0) 
            BEGIN
                IF(LEN(@Tags)>0)
                   BEGIN
                        INSERT INTO [NewsInTag]
                        ([NewsId]
                        ,[TagId]
                        ,[Priority]
                        ,[TaggedDate])
                        SELECT @Id,tb.value,ROW_NUMBER() OVER (ORDER BY (SELECT null) ),@CreatedDate FROM STRING_SPLIT(@Tags,',') tb;
                        IF(@@ROWCOUNT=0) ROLLBACK;ELSE COMMIT;
                    END;
                ELSE COMMIT;
            END;
        ELSE ROLLBACK;
        ";

        //public const string AddVideo = @"
        //   BEGIN TRANSACTION  ;     
        // INSERT INTO [Video](
        //    [Id]
        //   ,[CategoryId]
        //   ,[Name]
        //   ,[UnsignName]
        //   ,[Description]
        //   ,[HtmlCode]
        //   ,[Avatar]
        //   ,[KeyVideo]
        //   ,[Status]
        //   ,[Mode]
        //   ,[ViewCount]
        //   ,[Tags]
        //   ,[CreatedBy]
        //   ,[CreatedDate]
        //   ,[EditedDate]
        //   ,[EditedBy]
        //   ,[Url]
        //   ,[Source]
        //   ,[VideoRelation]
        //   ,[FileName]
        //   ,[Duration]
        //   ,[Size]
        //   ,[Capacity]
        //   ,[AllowAdv]
        //   ,[IsRemovedLogo]
        //   ,[Type]
        //   ,[IsConverted]
        //   ,[OriginalUrl]
        //   ,[Author]
        //   ,[ParentId]
        //   ,[NewsId]
        //   ,[Location]
        //   ,[OriginalId]
        //   ,[PolicyContentId]
        //   ,[RawId]
        //   ,[TrailerUrl]
        //   ,[MetaAvatar]
        //   ,[DisplayStyle]
        //    ,[MetaData]
        //) VALUES (
        //     @Id
        //   ,@CategoryId
        //   ,@Name
        //   ,@UnsignName
        //   ,@Description
        //   ,@HtmlCode
        //   ,@Avatar
        //   ,@KeyVideo
        //   ,@Status
        //   ,@Mode
        //   ,@ViewCount
        //   ,@Tags
        //   ,@CreatedBy
        //   ,@CreatedDate

        //   ,@EditedDate
        //   ,@EditedBy
        //   ,@Url
        //   ,@Source
        //   ,@VideoRelation
        //   ,@FileName
        //   ,@Duration
        //   ,@Size
        //   ,@Capacity
        //   ,@AllowAdv
        //   ,@IsRemovedLogo
        //    ,@Type
        //   ,@IsConverted
        //   ,@OriginalUrl
        //   ,@Author
        //   ,@ParentId
        //   ,@NewsId
        //   ,@Location
        //   ,@OriginalId
        //   ,@PolicyContentId
        //   ,@RawId
        //   ,@TrailerUrl
        //   ,@MetaAvatar
        //   ,@DisplayStyle
        //    ,@MetaData
        //);
        //if(@@ROWCOUNT<>0)      
        //INSERT INTO [News]
        //        ([Id]
        //          ,[Title]      
        //          ,[Type]           
        //          ,[Status]
        //          ,[TemplateId]
        //          --,[Avatar]
        //          ,[Url]     
        //          ,[CreatedBy]
        //          ,[CreatedDate]    
        //          )
        //    VALUES
        //        (@Id
        //        ,@Name
        //        ,@Type              
        //        ,@Status
        //        ,@TemplateId                
        //        --,@Avatar
        //        ,@Url
        //        ,@CreatedBy
        //        ,@CreatedDate
        //        );

        //       -- INSERT INTO [NewsSeoMeta]
        //        --([NewsId]
        //         -- ,[UrlSlug]
        //         -- ,[MetaTitle]
        //         -- ,[MetaDescription]
        //         -- ,[MetaAuthor])
        //       -- VALUES
        //        --(@Id
        //          --,@UrlSlug
        //          --,@MetaTitle
        //          --,@MetaDescription
        //         -- ,@MetaAuthor);
        //IF(@@ROWCOUNT<>0) 
        //        INSERT INTO [NewsInAccount]
        //        ([AccountId]
        //          ,[NewsId]
        //          ,[PublishedDate]
        //          ,[PublishedType])
        //        VALUES
        //        (@AccountId
        //          ,@Id
        //          ,@PublishedDate
        //          ,@PublishedType) ;
        //IF(@@ROWCOUNT=0) ROLLBACK;
        //ELSE COMMIT;
        //";

        public const string Update = @"
        BEGIN TRANSACTION;
        UPDATE [Video]
           SET 
            [Url]=CASE WHEN COL_LENGTH('Video','Url')=-1 THEN @Url ELSE LEFT(@Url,COL_LENGTH('Video','Url')/2) END
           ,[OriginalUrl]=CASE WHEN COL_LENGTH('Video','OriginalUrl')=-1 THEN @OriginalUrl ELSE LEFT(@OriginalUrl,COL_LENGTH('Video','OriginalUrl')/2) END
           ,[Author]=CASE WHEN COL_LENGTH('Video','Author')=-1 THEN @Author ELSE LEFT(@Author,COL_LENGTH('Video','Author')/2) END
           ,[CategoryId]=@CategoryId
           ,[Location]=CASE WHEN COL_LENGTH('Video','Location')=-1 THEN @Location ELSE LEFT(@Location,COL_LENGTH('Video','Location')/2) END
           ,[PublishMode]=@PublishMode
           ,[PublishData]=@PublishData
           ,[DistributionDate]=@DistributionDate
           ,[ModifiedDate]=@ModifiedDate
           ,[ModifiedBy]=@ModifiedBy
           ,[Tags]=@Tags
           ,[Name]=CASE WHEN COL_LENGTH('Video','Name')=-1 THEN @Name ELSE LEFT(@Name,COL_LENGTH('Video','Name')/2) END
           ,[UnsignName]=CASE WHEN COL_LENGTH('Video','UnsignName')=-1 THEN @UnsignName ELSE LEFT(@UnsignName,COL_LENGTH('Video','UnsignName')) END
           ,[Description]=@Description
           ,[HtmlCode]=@HtmlCode
           ,[KeyVideo]=CASE WHEN COL_LENGTH('Video','KeyVideo')=-1 THEN @KeyVideo ELSE LEFT(@KeyVideo,COL_LENGTH('Video','KeyVideo')) END
           ,[UploadedDate]=@UploadedDate
           ,[Source]=CASE WHEN COL_LENGTH('Video','Source')=-1 THEN @Source ELSE LEFT(@Source,COL_LENGTH('Video','Source')/2) END
           ,[VideoRelation]=CASE WHEN COL_LENGTH('Video','VideoRelation')=-1 THEN @VideoRelation ELSE LEFT(@VideoRelation,COL_LENGTH('Video','VideoRelation')) END
           ,[FileName]=CASE WHEN COL_LENGTH('Video','FileName')=-1 THEN @FileName ELSE LEFT(@FileName,COL_LENGTH('Video','FileName')) END
           ,[Duration]=CASE WHEN COL_LENGTH('Video','Duration')=-1 THEN @Duration ELSE LEFT(@Duration,COL_LENGTH('Video','Duration')) END
           ,[Size]=CASE WHEN COL_LENGTH('Video','Size')=-1 THEN @Size ELSE LEFT(@Size,COL_LENGTH('Video','Size')) END
           ,[Capacity]=@Capacity
           ,[AllowAdv]=@AllowAdv
           ,[IsRemovedLogo]=@IsRemovedLogo
           ,[IsConverted]=@IsConverted
           ,[PolicyContentId]=CASE WHEN COL_LENGTH('Video','PolicyContentId')=-1 THEN @PolicyContentId ELSE LEFT(@PolicyContentId,COL_LENGTH('Video','PolicyContentId')) END
           ,[OriginalId]=@OriginalId
           ,[Trailer]=CASE WHEN COL_LENGTH('Video','Trailer')=-1 THEN @Trailer ELSE LEFT(@Trailer,COL_LENGTH('Video','Trailer')/2) END
           ,[MetaAvatar]=@MetaAvatar
           ,[DisplayStyle]=@DisplayStyle
           ,[MetaData]=@MetaData
           ,[ApprovedBy]=@ApprovedBy
           ,[ApprovedDate]=@ApprovedDate
           ,[CommentMode]=@CommentMode
        WHERE [Id]=@Id;
        IF(@@ROWCOUNT<>0) 
            BEGIN
                DELETE [NewsInTag] WHERE [NewsId]=@Id;
                IF(LEN(@Tags)>0)
                BEGIN
                    INSERT INTO [NewsInTag]
                    ([NewsId]
                    ,[TagId]
                    ,[Priority]
                    ,[TaggedDate])
                    SELECT @Id,tb.value,ROW_NUMBER() OVER (ORDER BY (SELECT null) ),@ModifiedDate FROM STRING_SPLIT(@Tags,',') tb;
                    IF(@@ROWCOUNT<>0) COMMIT; ELSE ROLLBACK;
                END;
                ELSE COMMIT;
            END;
        ELSE ROLLBACK;
        ";

        public const string UpdateStatus = @"UPDATE [Video]
           SET [Status]=@Status
                ,[ModifiedDate]=@ModifiedDate
                ,[ModifiedBy]=@ModifiedBy
                ,[DistributionDate]=@DistributionDate
                ,[ApprovedBy]=@ApprovedBy
                ,[ApprovedDate]=@ApprovedDate
           WHERE [Id]=@Id
        ;";

        public const string DeleteVideoById = @"DELETE Video WHERE [Id]=@VideoId;";
        public const string CountVideo = @"SELECT count(Id) as Total FROM [Video]
        ;";

        public const string InitAllVideo = @"DECLARE @UpperBand int, @LowerBand int                        
        SET @LowerBand  = (@PageIndex - 1) * @PageSize
        SET @UpperBand  = (@PageIndex * @PageSize)
        SELECT * FROM (
        SELECT 
            v.[Id]
            ,v.[CategoryId]
            ,v.[Name]
            ,v.[UnsignName]
            ,v.[Description]
            ,v.[HtmlCode]
            ,v.[Avatar]
            ,v.[KeyVideo]
            ,n.[Status]
            ,v.[Mode]
            ,v.[ViewCount]
            --,v.[Tags]
            ,v.[CreatedBy]
            ,v.[CreatedDate]
            ,v.[ModifiedBy]
            ,n.[ModifiedDate]
            ,v.[PublishedDate]
            ,v.[PublishedBy]
            ,v.[EditedDate]
            ,v.[EditedBy]
            ,n.[Url]
            ,v.[Source]
            ,v.[FileName]
            ,v.[Duration]
            ,v.[Size]
            ,v.[Capacity]
            ,v.[AllowAdv]
            ,v.[IsRemovedLogo]
            ,n.[Type]
            ,v.[IsConverted]
            ,v.[OriginalUrl]
            ,v.[Author]
            ,v.[ParentId]
            ,v.[NewsId]
            ,v.[Location]
            ,v.[OriginalId]
            ,v.[PolicyContentId]
            ,v.[RawId]
            ,v.[TrailerUrl]
            ,v.[MetaAvatar]
            ,v.[DisplayStyle]
            ,v.[MetaData] ,
        ROW_NUMBER() OVER(ORDER BY v.Id DESC) AS RowNumber 
        FROM Video v 
        INNER JOIN News n ON v.Id=n.Id
        where v.Type=2 and (@DateFrom is null and (1=1)) or (@DateFrom is not null and (CONVERT(datetime,n.CreatedDate) >= CONVERT(datetime,@DateFrom) AND CONVERT(datetime,n.CreatedDate)<=CONVERT(datetime,@DateTo)))
        ) AS temp
        WHERE RowNumber > @LowerBand AND RowNumber <= @UpperBand";

        public const string GetVideoRelation = @"SELECT TOP 1 [VideoRelation] as Ids FROM [Video] WHERE [Id]=@Id;
            
        ";

        public const string GetVideoTag = @"SELECT TOP 1 [Tags] as Ids FROM [Video] WHERE [Id]=@Id;";

        public const string Publish = @"UPDATE [News] 
                SET [Status]=@Status               
                ,[ModifiedDate]=@DistributionDate
                ,[DistributionDate]=@DistributionDate
                ,[DistributorId]=@DistributorId
                ,[PublishTempId]=@PublishTempId
                ,[PublishTempData]=@PublishTempData
            WHERE [Id]=@Id ;
            
         
            ";

        public const string AddVideoInTag = @"INSERT INTO [VideoInTag]
                                            ([VideoId],[TagId],[TagMode],[Priority]) 
                                            VALUES
                                            (@VideoId,@TagId,0,@Priority)
                                            ";

        public const string DeleteVideoInTag = @"DELETE [VideoInTag] WHERE VideoId=@VideoId";
    }
}
