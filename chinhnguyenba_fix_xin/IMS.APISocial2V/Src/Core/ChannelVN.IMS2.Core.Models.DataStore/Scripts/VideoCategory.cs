﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ChannelVN.IMS2.Core.Models.DataStore.Scripts
{
    internal class VideoCategory
    {
        public const string Add = @"INSERT INTO [VideoCategory]
        ([Id]
            ,[Name]
            ,[Url]
            ,[Order]
            ,[ParentId]
            ,[Status]
            ,[CreatedDate]
            ,[ModifiedDate]
            ,[Avatar]
            ,[AvatarCover]
            ,[CategoryRelation]
            ,[MetaAvatar]
            ,[Description]
        ) VALUES (@Id
            ,@Name
            ,@Url
            ,@Order
            ,@ParentId
            ,@Status
            ,@CreatedDate
            ,@ModifiedDate
            ,@Avatar
            ,@AvatarCover
            ,@CategoryRelation
            ,@MetaAvatar
            ,@Description
        )";

        public const string Update = @"UPDATE [VideoCategory]
        SET [Name]=@Name
            ,[Url]=@Url
            ,[Order]=@Order
            ,[ParentId]=@ParentId
            ,[Status]=@Status
            ,[ModifiedDate]=@ModifiedDate
            ,[Avatar]=@Avatar
            ,[AvatarCover]=@AvatarCover
            ,[CategoryRelation]=@CategoryRelation
            ,[MetaAvatar]=@MetaAvatar
            ,[Description]=@Description
        ) WHERE [Id]=@Id";

        public const string CountVideoCategory = @"SELECT count(id) FROM VideoCategory";

        public const string InitAllVideoCategory = @"DECLARE @UpperBand int, @LowerBand int
                                        
            SET @LowerBand  = (@PageIndex - 1) * @PageSize
            SET @UpperBand  = (@PageIndex * @PageSize)
            SELECT * FROM (
            SELECT 
                 vc.[Id]
                  ,vc.[Name]
                  ,vc.[Url]
                  ,vc.[Order]
                  ,vc.[ParentId]
                  ,vc.[Status]
                  ,vc.[CreatedDate]
                  ,vc.[ModifiedDate]
                  ,vc.[Avatar]
                  ,vc.[AvatarCover]
                  ,vc.[MetaAvatar]
                  ,vc.[Description],
            ROW_NUMBER() OVER(ORDER BY vc.Id DESC) AS RowNumber 
            FROM [VideoCategory] vc
            where (@DateFrom is null and (1=1)) or (@DateFrom is not null and (CONVERT(datetime,vc.CreatedDate) >= CONVERT(datetime,@DateFrom) AND CONVERT(datetime,vc.CreatedDate)<=CONVERT(datetime,@DateTo)))
            ) AS temp
            WHERE RowNumber > @LowerBand AND RowNumber <= @UpperBand";

        public const string GetRelation = @"SELECT [CategoryRelation] FROM VideoCategory WHERE [Id]=@Id";
    }
}
