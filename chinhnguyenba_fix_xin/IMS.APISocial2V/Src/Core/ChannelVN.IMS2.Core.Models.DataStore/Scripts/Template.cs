﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ChannelVN.IMS2.Core.Models.DataStore.Scripts
{
    internal class Template
    {
        public const string Add = @"INSERT INTO [Template]
                ([Title]
                  ,[Avatar]
                  ,[MetaData]
                  ,[Description]
                  ,[Status]
                  ,[Priority]                                                      
                  ,[CategoryId]
                  ,[CreatedDate]
                  ,[CreatedBy]
                )
            VALUES
                (@Title
                ,@Avatar
                ,@MetaData
                ,@Description
                ,@Status
                ,@Priority                                
                ,@CategoryId
                ,@CreatedDate
                ,@CreatedBy)

            SET @Id=scope_identity()
            ";

        public const string Update = @"UPDATE [Template] 
                  SET [Title]=@Title
                  ,[Avatar]=@Avatar
                  ,[MetaData]=@MetaData
                  ,[Description]=@Description
                  --,[Status]=@Status
                  ,[Priority]=@Priority
                  ,[CategoryId]=@CategoryId
                    ,[ModifiedBy]=@ModifiedBy
                    ,[ModifiedDate]=@ModifiedDate                    
            WHERE [Id]=@Id                
            ";

        public const string UpdateStatus = @"UPDATE [Template] 
                SET [Status]=@Status,
                    [ModifiedBy]=@ModifiedBy
                   ,[ModifiedDate]=@ModifiedDate
            WHERE [Id]=@Id ";

        public const string GetById = @"SELECT * FROM [Template] WHERE [Id]=@Id";

        public const string CountTemplate = @"SELECT count(Id) as Total from Template ";

        public const string InitAllTemplate = @"DECLARE @UpperBand int, @LowerBand int
                                        
                                        SET @LowerBand  = (@PageIndex - 1) * @PageSize
                                        SET @UpperBand  = (@PageIndex * @PageSize)
                                        SELECT * FROM (
                                        SELECT d.* ,
                                        ROW_NUMBER() OVER(ORDER BY d.Id DESC) AS RowNumber 
                                        FROM Template d 
                                        where (@DateFrom is null and (1=1)) or (@DateFrom is not null and (CONVERT(datetime,d.CreatedDate) >= CONVERT(datetime,@DateFrom) AND CONVERT(datetime,d.CreatedDate)<=CONVERT(datetime,@DateTo)))
                                        ) AS temp
                                        WHERE RowNumber > @LowerBand AND RowNumber <= @UpperBand";
    }
}
