﻿using ChannelVN.IMS2.Core.Entities.AccountAssignment;
using ChannelVN.IMS2.Foundation.Common;
using ChannelVN.IMS2.Foundation.Logging;
using StackExchange.Redis;
using System;
using System.Threading.Tasks;

namespace ChannelVN.IMS2.Core.Models.CacheStore
{
    public class AccountAssignmentCacheStore : CmsMainCacheStore
    {
        public Task<bool> AddAsync(AccountAssignment data)
        {
            var returnValue = Task.FromResult(false);
            try
            {                
                using (var _context = GetContext())
                {
                    returnValue = _context.MultiAsync(async (transac) =>
                    {
                        await transac.HashSetAsync(_context.NameOf<AccountAssignment>(), data.Id, Json.Stringify(data));                        
                    });
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
            }
            return returnValue;
        }
        public Task<bool> UpdateAsync(AccountAssignment data)
        {
            var returnValue = Task.FromResult(false);
            try
            {
                using (var _context = GetContext())
                {
                    returnValue = _context.MultiAsync((transac) =>
                    {
                        transac.HashSetAsync(_context.NameOf<AccountAssignment>(), data.Id, Json.Stringify(data));
                    });
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
            }
            return returnValue;
        }
        public Task<bool> RemoveAsync(AccountAssignment data)
        {
            var returnValue = Task.FromResult(false);
            try
            {
                using (var _context = GetContext())
                {
                    returnValue = _context.MultiAsync(async (transac) =>
                    {
                        await transac.HashDeleteAsync(_context.NameOf<AccountAssignment>(), data.Id);
                    });
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
            }
            return returnValue;
        }
        public Task<AccountAssignment> GetByIdAsync(long id)
        {
            var returnValue = Task.FromResult(default(AccountAssignment));
            try
            {
                using (var _context = GetContext())
                {
                    returnValue = _context.HashGetExAsync<AccountAssignment>(id, CommandFlags.PreferSlave);
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
            }
            return returnValue;
        }        
    }
}
