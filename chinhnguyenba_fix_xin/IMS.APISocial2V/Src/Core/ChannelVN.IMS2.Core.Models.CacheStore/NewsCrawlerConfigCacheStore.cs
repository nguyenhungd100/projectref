﻿using ChannelVN.IMS2.Core.Entities;
using ChannelVN.IMS2.Foundation.Common;
using ChannelVN.IMS2.Foundation.Common.Configuration;
using ChannelVN.IMS2.Foundation.Logging;
using StackExchange.Redis;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using static ChannelVN.IMS2.Core.Entities.ErrorCode.ErrorMapping;

namespace ChannelVN.IMS2.Core.Models.CacheStore
{
    public class NewsCrawlerConfigCacheStore : CmsMainCacheStore
    {
        public async Task<ErrorCodes> AddAsync(NewCrawlerConfigurationSearch data)
        {
            var returnValue = ErrorCodes.UpdateError;
            try
            {
                using (var _context = GetContext())
                {
                    var crawlerConfigDb = await _context.HashGetExAsync<NewCrawlerConfigurationSearch>("newscrawlerconfiguration", data.Id);
                    if (crawlerConfigDb != null)
                    {
                        crawlerConfigDb.OfficerId = data.OfficerId;
                        crawlerConfigDb.Source = data.Source;
                        crawlerConfigDb.PublishMode = data.PublishMode;
                        crawlerConfigDb.IntervalTime = data.IntervalTime;
                        crawlerConfigDb.Status = data.Status;
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
                returnValue = ErrorCodes.Exception;
            }
            return returnValue;
        }

        public async Task<ErrorCodes> UpdateConfigAsync(NewCrawlerConfigurationSearch crawlerConfig)
        {
            var returnValue = ErrorCodes.UpdateError;
            try
            {
                using (var _context = GetContext())
                {
                    var configDb = await _context.HashGetExAsync<NewCrawlerConfigurationSearch>("newscrawlerconfiguration", crawlerConfig.Id);
                    if (configDb != null)
                    {
                        var returnValueUpdate = await _context.MultiAsync((transac) =>
                        {
                            transac.HashSetAsync(_context.NameOf("newscrawlerconfiguration"), crawlerConfig.Id, Json.Stringify(crawlerConfig));
                        });
                        if (returnValueUpdate) returnValue = ErrorCodes.Success;
                    }
                    else returnValue = ErrorCodes.DataNotExist;
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
            }
            return returnValue;
        }

        public async Task<ErrorCodes> AddConfigAsync(NewCrawlerConfigurationSearch data)
        {
            var returnValue = ErrorCodes.AddError;
            try
            {
                using (var _context = GetContext())
                {
                    var resultAdd = await _context.MultiAsync(async (transac) =>
                    {
                        await transac.HashSetAsync(_context.NameOf("newscrawlerconfiguration"), data.Id, Json.Stringify(data));
                        await transac.SortedSetAddAsync(_context.NameOf("newscrawlerconfiguration"), data.Id, Utility.ConvertToTimestamp10(data.CreatedDate ?? DateTime.Now));
                    });
                    if (resultAdd) returnValue = ErrorCodes.Success;
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
            }
            return returnValue;
        }

        public NewCrawlerConfigurationSearch PopQueueAutoCrawlerConfig()
        {
            var crawlerConfig = default(NewCrawlerConfigurationSearch);
            try
            {
                using (var _context = GetContext())
                {

                    var returnValue = _context.ListLeftPop(nameof(NewsCrawlerConfigQueue));
                    if (returnValue.HasValue)
                    {
                        crawlerConfig = Json.Parse<NewCrawlerConfigurationSearch>(returnValue.ToString());
                    }
                    return crawlerConfig;
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
                return null;
            }
        }


        public Task<bool> PushQueueAutoCrawlerConfig(List<NewCrawlerConfigurationSearch> data)
        {
            var returnValue = Task.FromResult(false);
            try
            {
                using (var _context = GetContext())
                {
                    foreach (var item in data)
                    {
                        _context.ListRightPush(nameof(NewsCrawlerConfigQueue), Json.Stringify(item));
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
            }
            return returnValue;
        }

        public bool CheckExistQueueAutoCrawlerConfig()
        {
            var returnValue = true;
            try
            {
                using (var _context = GetContext())
                {
                    var redisValues = _context.ListRange(nameof(NewsCrawlerConfigQueue));
                    if (redisValues.Count() == 0)
                        returnValue = false;
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
            }
            return returnValue;
        }

        public async Task<bool> PushListConfigSearchToCacheAsync(List<NewCrawlerConfigurationSearch> data)
        {
            var returnValue = false;
            try
            {
                var hashFields = new List<HashEntry>();
                foreach (var item in data)
                {
                    hashFields.Add(new HashEntry(item.Id.ToString(), Json.Stringify(item)));
                }
                using (var _context = GetContext())
                {
                    var returnValuePushlish = await _context.MultiAsync((transac) =>
                    {
                        transac.HashSetAsync(_context.NameOf<NewsCrawlerSourceScan>(), hashFields.ToArray());
                        transac.KeyExpireAsync(_context.NameOf<NewsCrawlerSourceScan>(),
                            DateTime.Now.AddSeconds(AppSettings.Current.JobAutoNewsCrawlerSetting.TimeSpacingSeachConfig),
                            CommandFlags.FireAndForget);
                    });
                    if (returnValuePushlish)
                    {
                        returnValue = true;
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
            }
            return returnValue;
        }

        public async Task<bool> ExistListSearchConfigAsync()
        {
            var returnValue = false;
            try
            {
                using (var _context = GetContext())
                {
                    var returnCheck = await _context.KeyExistsAsync(nameof(NewsCrawlerSourceScan));
                    if (returnCheck)
                    {
                        returnValue = true;
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
            }
            return returnValue;
        }

        public async Task<bool> UpdateTimeScanCrawlerAsync(NewCrawlerConfigurationSearch data)
        {
            var returnValue = false;
            try
            {
                using (var _context = GetContext())
                {
                    var returnUpdate = await _context.MultiAsync((transac) =>
                     {
                        transac.HashSetAsync(_context.NameOf<NewsCrawlerSourceScan>(), data.Id, Json.Stringify(data), When.Exists);
                     });

                    if (returnUpdate)
                    {
                        returnValue = true;
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
            }
            return returnValue;
        }

        public async Task<List<NewCrawlerConfigurationSearch>> GetListConfigSearchFromCacheAsync()
        {
            var returnValue = default(List<NewCrawlerConfigurationSearch>);
            try
            {
                using (var _context = GetContext())
                {
                    returnValue = await _context.HashGetAllExxAsync<NewCrawlerConfigurationSearch>(nameof(NewsCrawlerSourceScan));
                    //foreach (var item in values)
                    //{
                    //    returnValue.Add(Json.Parse<NewCrawlerConfigurationSearch>(item));
                    //}

                    // Phần này fix lỗi( check list search config cache = -1-- vô thời hạn), lần commit sau sẽ xóa -- hùng
                    var ttl = await _context.KeyTimeToLiveAsync(nameof(NewsCrawlerSourceScan));
                    if (returnValue.Count() > 0 && !ttl.HasValue)
                    {
                        await _context.KeyExpireAsync(nameof(NewsCrawlerSourceScan),
                            DateTime.Now.AddSeconds(AppSettings.Current.JobAutoNewsCrawlerSetting.TimeSpacingSeachConfig),
                            CommandFlags.FireAndForget);

                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
            }
            return returnValue;
        }
    }
}
