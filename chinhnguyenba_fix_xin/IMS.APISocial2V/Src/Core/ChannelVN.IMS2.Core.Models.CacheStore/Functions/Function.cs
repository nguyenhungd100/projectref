﻿using ChannelVN.IMS2.Core.Entities;
using ChannelVN.IMS2.Core.Entities.Board;
using ChannelVN.IMS2.Core.Entities.Security;
using ChannelVN.IMS2.Foundation.Common;
using ChannelVN.IMS2.Foundation.Data.Providers;
using ChannelVN.IMS2.Foundation.Logging;
using StackExchange.Redis;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace ChannelVN.IMS2.Core.Models.CacheStore.Functions
{
    public static class Function
    {
        public static async Task<bool> IsWriter(long? officerId, string userCretedPost, RedisContext _context)
        {
            var isWriter = false;
            try
            {
                var accPage = await _context.HashGetExAsync<Account>(officerId, CommandFlags.PreferSlave);
                if(accPage != null && accPage.Status == (int)AccountStatus.Actived)
                {
                    var listMemberId = await _context.SortedSetRangeByRankAsync(IndexKeys.KeyAccountMemberInOfficer<AccountMember>(officerId), 0, -1, Order.Descending, CommandFlags.PreferSlave);
                    var listMember = await _context.GetsFromHashExAsync<AccountMember>(listMemberId?.Select(p => officerId + IndexKeys.SeparateChar + p.ToString())?.ToList(), CommandFlags.PreferSlave);

                    var userAccount = await _context.HashGetExAsync<Account>(listMember?.Where(p => p.Role == AccountMemberRole.Owner)?.FirstOrDefault().MemberId, CommandFlags.PreferSlave);

                    var member = listMember?.Where(p => p.MemberId.ToString().Equals(userCretedPost))?.FirstOrDefault();

                    if (userAccount != null && userAccount.Status == (int)AccountStatus.Actived && userAccount.Type == (int)AccountType.Personal && member.IsLocked == false && member?.Permissions != null && member.Permissions.Contains(MemberPermission.CreatePost))
                    {
                        isWriter = true;
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
            }
            return isWriter;
        }


        public static async Task<bool> IsModifier(long officerId, long newsId, string modifiedBy, string createdBy, int? status, RedisContext _context)
        {
            var IsModifier = false;
            try
            {
                var listOfficerOfNews = await _context.SetMembersAsync(IndexKeys.KeyAccountInNews(newsId), CommandFlags.PreferSlave);
                if (listOfficerOfNews != null && listOfficerOfNews.Contains((RedisValue)officerId))
                {
                    var listMemberId = await _context.SortedSetRangeByRankAsync(IndexKeys.KeyAccountMemberInOfficer<AccountMember>(officerId), 0, -1, Order.Descending, CommandFlags.PreferSlave);
                    var listMember = await _context.GetsFromHashExAsync<AccountMember>(listMemberId?.Select(p => officerId + IndexKeys.SeparateChar + p.ToString())?.ToList(), CommandFlags.PreferSlave);

                    var permissions = listMember?.Where(p => p.MemberId == long.Parse(modifiedBy) && p.IsLocked == false)?.FirstOrDefault()?.Permissions;
                    if (permissions != null)
                    {

                        if (status != (int)NewsStatus.Published)
                        {
                            if (modifiedBy.Equals(createdBy))
                            {
                                IsModifier = true;
                            }
                        }
                        else
                        {
                            var officer = await _context.HashGetExAsync<Account>(officerId);
                            if (officer.Mode == (int)AccountMode.Check)
                            {
                                if (permissions.Contains(MemberPermission.ModifyPostOnPage))
                                {
                                    IsModifier = true;
                                }
                            }
                            else
                            {
                                if (permissions.Contains(MemberPermission.ModifyPostOnPage) || modifiedBy.Equals(createdBy))
                                {
                                    IsModifier = true;
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
            }
            return IsModifier;
        }

        public static Task<bool> IsPublisher(AccountMember accountMember, string createdBy, string approvedBy, int? mode, int? statusOld)
        {
            var IsPublisher = Task.FromResult(false);
            try
            {
                if(accountMember.IsLocked == false)
                {
                    if (mode == (int)AccountMode.Check)
                    {
                        if (accountMember.Permissions != null && (accountMember.Permissions.Contains(MemberPermission.ApprovePost) && accountMember.MemberId.ToString().Equals(createdBy))
                        || (accountMember.Permissions.Contains(MemberPermission.ApprovePost) && accountMember.MemberId.ToString().Equals(approvedBy) && statusOld == (int)NewsStatus.Approve)
                        || (accountMember.Permissions.Contains(MemberPermission.ApprovePost) && string.IsNullOrEmpty(approvedBy) && statusOld == (int)NewsStatus.Approve)
                        || (accountMember.Permissions.Contains(MemberPermission.PublishPost) && accountMember.MemberId.ToString().Equals(createdBy)))
                        {
                            IsPublisher = Task.FromResult(true);
                        }
                    }
                    else
                    {
                        if (accountMember.MemberId.ToString().Equals(createdBy) || (accountMember.Permissions != null && accountMember.Permissions.Contains(MemberPermission.ApprovePost) && accountMember.MemberId.ToString().Equals(approvedBy) && statusOld == (int)NewsStatus.Approve)
                        || (accountMember.Permissions != null && accountMember.Permissions.Contains(MemberPermission.ApprovePost) && string.IsNullOrEmpty(approvedBy) && statusOld == (int)NewsStatus.Approve))
                        {
                            IsPublisher = Task.FromResult(true);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
            }
            return IsPublisher;
        }

        public static Task<bool> IsRemover(AccountMember accountMember, string createdBy, string approvedBy, int? mode, int? statusOld)
        {
            var IsPublisher = Task.FromResult(false);
            try
            {
                if(accountMember.IsLocked == false)
                {
                    if (mode == (int)AccountMode.Check)
                    {
                        if (accountMember?.Permissions != null && (accountMember.Permissions.Contains(MemberPermission.RemovePostOnPage)))
                        {
                            IsPublisher = Task.FromResult(true);
                        }
                    }
                    else
                    {
                        if (createdBy.Equals(accountMember?.MemberId.ToString()) || (accountMember?.Permissions != null && accountMember.Permissions.Contains(MemberPermission.RemovePostOnPage)))
                        {
                            IsPublisher = Task.FromResult(true);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
            }
            return IsPublisher;
        }

        public static Task<bool> CheckBoardOfUser(long officerId, long userId, long boardId, RedisContext _context)
        {
            var returnValue = Task.FromResult(false);
            try
            {
                returnValue = _context.ExecAsync(async (db) =>
                {
                    var result = false;
                    var listMemberId = await db.SortedSetRangeByRankAsync(_context.NameOf(IndexKeys.KeyAccountMemberInOfficer<AccountMember>(officerId)), 0, -1, Order.Descending, CommandFlags.PreferSlave);

                    var listMember = (await db.HashGetAsync(_context.NameOf<AccountMember>(), listMemberId?.Select(p => (RedisValue)(officerId + IndexKeys.SeparateChar + p.ToString()))?.ToArray(), CommandFlags.PreferSlave))?.Select(p => Json.Parse<AccountMember>(p));

                    if (listMember != null && listMember.Where(p => p.MemberId == userId).FirstOrDefault()?.Permissions != null && listMember.Where(p => p.MemberId == userId).FirstOrDefault().Permissions.Contains(MemberPermission.CreatePost))
                    {
                        var value = await db.SortedSetRangeByScoreAsync(_context.NameOf(IndexKeys.KeyCreatedBy<Board>(officerId.ToString())));
                        if (value != null && value.Contains(boardId)) result = true;
                    }
                    return result;
                });
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
            }
            return returnValue;
        }
    }
}
