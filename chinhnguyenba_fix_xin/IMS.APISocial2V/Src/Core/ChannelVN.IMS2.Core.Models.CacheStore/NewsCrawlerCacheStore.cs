﻿using ChannelVN.IMS2.Core.Entities;
using ChannelVN.IMS2.Foundation.Common;
using ChannelVN.IMS2.Foundation.Logging;
using StackExchange.Redis;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ChannelVN.IMS2.Core.Models.CacheStore
{
    public class NewsCrawlerCacheStore : CmsMainCacheStore
    {
        public Task<bool> AddAsync(List<NewsCrawler> data)
        {
            var returnValue = Task.FromResult(false);
            try
            {
                var hashFields = new List<HashEntry>();
                foreach(var item in data)
                {
                    hashFields.Add(new HashEntry(item.Id.ToString(),Json.Stringify(item)));
                }
                using (var _context = GetContext())
                {
                    returnValue = _context.MultiAsync(async (transac) =>
                    {
                        await transac.HashSetAsync(_context.NameOf<NewsCrawler>(), hashFields.ToArray());                        
                    });
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
            }
            return returnValue;
        }

        public Task<bool> UpdateAsync(NewsCrawler data)
        {
            var returnValue = Task.FromResult(false);
            try
            {
                using (var _context = GetContext())
                {
                    returnValue = _context.MultiAsync((transac) =>
                    {
                        transac.HashSetAsync(_context.NameOf<NewsCrawler>(), data.Id, Json.Stringify(data));
                    });
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
            }
            return returnValue;
        }
        public Task<bool> SetScheduleAsync(NewsCrawler data)
        {
            var returnValue = Task.FromResult(false);
            try
            {
                using (var _context = GetContext())
                {
                    returnValue = _context.MultiAsync((transac) =>
                    {
                        transac.HashSetAsync(_context.NameOf<ScheduleNewsCrawler>(), data.Id, Json.Stringify(data));
                    });
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
            }
            return returnValue;
        }
        public Task<NewsCrawler> GetByIdAsync(long id)
        {
            var returnValue = Task.FromResult(default(NewsCrawler));
            try
            {
                using (var _context = GetContext())
                {
                    returnValue = _context.HashGetExAsync<NewsCrawler>(id, CommandFlags.PreferSlave);
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
            }
            return returnValue;
        }
        public Task<NewsCrawler> GetByLinkAsync(string link)
        {
            var returnValue = Task.FromResult(default(NewsCrawler));
            try
            {
                using (var _context = GetContext())
                {
                    returnValue = _context.HashGetExAsync<NewsCrawler>(link, CommandFlags.PreferSlave);
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
            }
            return returnValue;
        }

        public Task<List<Tag>> GetListByIdsAsync(List<string> ids)
        {
            var returnValue = Task.FromResult(default(List<Tag>));
            try
            {
                using (var _context = GetContext())
                {
                    returnValue = _context.GetsFromHashExAsync<Tag>(ids, CommandFlags.PreferSlave);
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
            }
            return returnValue;
        }        
    }
}
