﻿using ChannelVN.IMS2.Core.Entities;
using ChannelVN.IMS2.Core.Entities.Board;
using ChannelVN.IMS2.Core.Entities.Security;
using ChannelVN.IMS2.Core.Entities.Statistics;
using ChannelVN.IMS2.Foundation.Common;
using ChannelVN.IMS2.Foundation.Logging;
using StackExchange.Redis;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ChannelVN.IMS2.Core.Models.CacheStore.Statistics
{
    public class StatisticsCacheStore: CmsMainCacheStore
    {
        public async Task<Dictionary<DateTime, NewsCount2[]>> PublishNewsCountAsync(GetNewsCount search)
        {
            var returnValue = new Dictionary<DateTime, NewsCount2[]>();
            try
            {
                using (var _context = GetContext())
                {
                    returnValue = await _context.ExecAsync(async (db) =>
                    {
                        var resultValue = new Dictionary<DateTime, NewsCount2[]>();
                        var listType = new List<NewsType2>();

                        var tempKey = _context.NameOf("index:temp:intersect:" + Generator.SorterSetSearchId());
                        var keyNewsInAccount = _context.NameOf(IndexKeys.KeyNewsInAccount(search.AccountId));
                        var keyStatus = _context.NameOf(IndexKeys.KeyStatus<NewsAll>((int)NewsStatus.Published));
                        var keyBoardInOfficer = _context.NameOf(IndexKeys.KeyCreatedBy<Board>(search.AccountId?.ToString()));
                        var keyStatusBoard = _context.NameOf(IndexKeys.KeyStatus<Board>((int)BoardStatus.Actived));

                        if (search.NewsType != null)
                        {
                            listType.Add(search.NewsType.Value);
                        }
                        else
                        {
                            foreach (NewsType2 newsType in Enum.GetValues(typeof(NewsType2)))
                            {
                                listType.Add(newsType);
                            }
                        }

                        var dic = new Dictionary<NewsType2, SortedSetEntry[]>();
                        foreach (var type in listType)
                        {
                            if(type == NewsType2.Board)
                            {
                                dic.Add(type, null);
                                var keyType = _context.NameOf(IndexKeys.KeyType<NewsAll>((int)type));

                                var listKey = new List<RedisKey>() { keyStatusBoard, keyBoardInOfficer };

                                var value = await db.SortedSetCombineAndStoreAsync(SetOperation.Intersect, tempKey, listKey.ToArray(), null, Aggregate.Max, CommandFlags.None);
                                if (value > 0)
                                {
                                    var data = await db.SortedSetRangeByScoreWithScoresAsync(tempKey
                                                    , (search.FromDate ?? DateTime.MinValue).ConvertToTimestamp10()
                                                    , (search.ToDate ?? DateTime.Now).ConvertToTimestamp10()
                                                    , Exclude.None
                                                    , Order.Descending
                                                    , 0
                                                    , -1
                                                    , CommandFlags.None);
                                    dic[type] = data;
                                }
                            }
                            else
                            {
                                dic.Add(type, null);
                                var keyType = _context.NameOf(IndexKeys.KeyType<NewsAll>((int)type));

                                var listKey = new List<RedisKey>() { keyType, keyNewsInAccount, keyStatus };

                                var value = await db.SortedSetCombineAndStoreAsync(SetOperation.Intersect, tempKey, listKey.ToArray(), null, Aggregate.Max, CommandFlags.None);
                                if (value > 0)
                                {
                                    var data = await db.SortedSetRangeByScoreWithScoresAsync(tempKey
                                                    , (search.FromDate ?? DateTime.MinValue).ConvertToTimestamp10()
                                                    , (search.ToDate ?? DateTime.Now).ConvertToTimestamp10()
                                                    , Exclude.None
                                                    , Order.Descending
                                                    , 0
                                                    , -1
                                                    , CommandFlags.None);
                                    dic[type] = data;
                                }
                            }
                        }
                        await db.KeyDeleteAsync(tempKey);

                        foreach (DateTime day in Utility.EachDay(search.FromDate.Value, search.ToDate.Value))
                        {
                            var newsCounts = new List<NewsCount2>();
                            foreach (var type in listType)
                            {
                                var count = dic[type]?.Where(p => day.ToString("dd-MM-yyyy").Equals(Utility.ConvertSecondsToDate(p.Score)?.ToString("dd-MM-yyyy"))).Count() ?? 0;
                                newsCounts.Add(new NewsCount2
                                {
                                    NewsType = type,
                                    Count = count
                                });
                            }
                            resultValue.Add(day, newsCounts.ToArray());
                        }
                        return resultValue;
                    });
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
            }
            return returnValue;
        }

        public async Task<Dictionary<DateTime, NewsCount[]>> PublishNewsCountOfUserOnPageAsync(GetNewsCountOfUser search)
        {
            var returnValue = new Dictionary<DateTime, NewsCount[]>();
            try
            {
                using (var _context = GetContext())
                {
                    returnValue = await _context.ExecAsync(async (db) =>
                    {
                        var resultValue = new Dictionary<DateTime, NewsCount[]>();
                        var listType = new List<NewsType>();

                        var tempKey = _context.NameOf("index:temp:intersect:" + Generator.SorterSetSearchId());
                        var keyNewsInAccount = _context.NameOf(IndexKeys.KeyNewsInAccount(search.OfficerId));
                        var keyStatus = _context.NameOf(IndexKeys.KeyStatus<NewsAll>((int)NewsStatus.Published));

                        var keyCreatedBy = string.Empty;
                        if (search.AccountId != null && search.AccountId > 0)
                        {
                            keyCreatedBy = _context.NameOf(IndexKeys.KeyCreatedBy<NewsAll>(search.AccountId.ToString()));
                        }
                        

                        if (search.NewsType != null)
                        {
                            listType.Add(search.NewsType.Value);
                        }
                        else
                        {
                            foreach (NewsType newsType in Enum.GetValues(typeof(NewsType)))
                            {
                                listType.Add(newsType);
                            }
                        }

                        var dic = new Dictionary<NewsType, SortedSetEntry[]>();
                        foreach (var type in listType)
                        {
                            dic.Add(type, null);
                            var keyType = _context.NameOf(IndexKeys.KeyType<NewsAll>((int)type));

                            var listKey = new List<RedisKey>() { keyType, keyNewsInAccount, keyStatus};
                            if (!string.IsNullOrEmpty(keyCreatedBy))
                            {
                                listKey.Add(keyCreatedBy);
                            }

                            var value = await db.SortedSetCombineAndStoreAsync(SetOperation.Intersect, tempKey, listKey.ToArray(), null, Aggregate.Max, CommandFlags.None);
                            if (value > 0)
                            {
                                var data = await db.SortedSetRangeByScoreWithScoresAsync(tempKey
                                                , (search.FromDate ?? DateTime.MinValue).ConvertToTimestamp10()
                                                , (search.ToDate ?? DateTime.Now).ConvertToTimestamp10()
                                                , Exclude.None
                                                , Order.Descending
                                                , 0
                                                , -1
                                                , CommandFlags.None);
                                dic[type] = data;
                            }
                        }
                        await db.KeyDeleteAsync(tempKey);

                        foreach (DateTime day in Utility.EachDay(search.FromDate.Value, search.ToDate.Value))
                        {
                            var newsCounts = new List<NewsCount>();
                            foreach (var type in listType)
                            {
                                var count = dic[type]?.Where(p => day.ToString("dd-MM-yyyy").Equals(Utility.ConvertSecondsToDate(p.Score)?.ToString("dd-MM-yyyy"))).Count() ?? 0;
                                newsCounts.Add(new NewsCount
                                {
                                    NewsType = type,
                                    Count = count
                                });
                            }
                            resultValue.Add(day, newsCounts.ToArray());
                        }
                        return resultValue;
                    });
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
            }
            return returnValue;
        }
    }
}
