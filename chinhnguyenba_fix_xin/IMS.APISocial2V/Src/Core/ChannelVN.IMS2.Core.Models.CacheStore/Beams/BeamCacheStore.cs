﻿using ChannelVN.IMS2.Core.Entities;
using ChannelVN.IMS2.Core.Entities.Beam;
using ChannelVN.IMS2.Core.Entities.Distribution;
using ChannelVN.IMS2.Core.Entities.Security;
using ChannelVN.IMS2.Foundation.Common;
using ChannelVN.IMS2.Foundation.Logging;
using StackExchange.Redis;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using static ChannelVN.IMS2.Core.Entities.ErrorCode.ErrorMapping;

namespace ChannelVN.IMS2.Core.Models.CacheStore.Beams
{
    public class BeamCacheStore : CmsMainCacheStore
    {
        public async Task<ErrorCodes> AddAsync(BeamSearch dataSearch)
        {
            var returnValue = ErrorCodes.AddError;
            try
            {
                using(var _context = GetContext())
                {
                    //Kiểm tra quyền viết bài
                    var isWriter = await Functions.Function.IsWriter(dataSearch?.NewsInAccount?.FirstOrDefault().AccountId, dataSearch.CreatedBy, _context);
                    
                    ////////////////
                    if (isWriter)
                    {
                        var data = Mapper<Beam>.Map(dataSearch, new Beam());
                        var newsAll = Mapper<NewsAll>.MapNewsAll(data, new NewsAll());
                        var resultAdd = await _context.MultiAsync((transac) =>
                        {
                            transac.HashSetAsync(_context.NameOf(data.GetType().Name), data.Id, Json.Stringify(data));

                            transac.HashSetAsync(_context.NameOf(newsAll.GetType().Name), data.Id, Json.Stringify(newsAll));
                            transac.SortedSetAddAsync(_context.NameOf(IndexKeys.KeyCreatedBy<NewsAll>(newsAll.CreatedBy)), newsAll.Id, (newsAll.CreatedDate ?? DateTime.Now).ConvertToTimestamp10());
                            transac.SortedSetAddAsync(_context.NameOf(IndexKeys.KeyStatus<NewsAll>(newsAll.Status)), newsAll.Id, (newsAll.CreatedDate ?? DateTime.Now).ConvertToTimestamp10());
                            transac.SortedSetAddAsync(_context.NameOf(IndexKeys.KeyDistributorid<NewsAll>(newsAll.DistributorId)), newsAll.Id, (newsAll.CreatedDate ?? DateTime.Now).ConvertToTimestamp10());
                            transac.SortedSetAddAsync(_context.NameOf(IndexKeys.KeyType<NewsAll>(newsAll.Type ?? 3)), newsAll.Id, (newsAll.CreatedDate ?? DateTime.Now).ConvertToTimestamp10());

                            transac.HashSetAsync(_context.NameOf(typeof(NewsInAccount).Name), dataSearch.NewsInAccount?.Select(p => new HashEntry(p.AccountId + ":" + p.NewsId, Json.Stringify(p))).ToArray());
                            transac.SetAddAsync(_context.NameOf(IndexKeys.KeyAccountInNews(dataSearch.Id)), dataSearch.NewsInAccount?.LastOrDefault()?.AccountId ?? 0);
                            transac.SetAddAsync(_context.NameOf(IndexKeys.KeyNewsInAccount(dataSearch.NewsInAccount?.LastOrDefault()?.AccountId)), dataSearch.Id);
                            transac.SortedSetAddAsync(_context.NameOf(IndexKeys.KeyPublishedDate<NewsInAccount>())
                                , (dataSearch.NewsInAccount?.LastOrDefault()?.AccountId ?? 0) + IndexKeys.SeparateChar + dataSearch.Id
                                , (dataSearch.CreatedDate ?? DateTime.Now).ConvertToTimestamp10());

                            if (dataSearch.ArticleInBeam != null && dataSearch.ArticleInBeam.Count() > 0)
                            {
                                transac.HashSetAsync(_context.NameOf(dataSearch.ArticleInBeam.FirstOrDefault().GetType().Name)
                                    , dataSearch.ArticleInBeam.Select(p => new HashEntry(p.BeamId + IndexKeys.SeparateChar + p.ArticleId, Json.Stringify(p))).ToArray());
                                transac.SortedSetAddAsync(_context.NameOf(IndexKeys.KeyArticleInBeam(dataSearch.Id))
                                    , dataSearch.ArticleInBeam.Select(p => new SortedSetEntry(p.ArticleId, (p.PublishedDate ?? DateTime.Now).ConvertToTimestamp10())).ToArray());
                                transac.SortedSetAddAsync(_context.NameOf(IndexKeys.KeyPublishedDate<ArticleInBeam>())
                                    , dataSearch.ArticleInBeam.Select(p => new SortedSetEntry(p.BeamId + IndexKeys.SeparateChar + p.ArticleId, Utility.ConvertToTimestamp10(p.PublishedDate ?? DateTime.Now))).ToArray());
                            }
                        });
                        if (resultAdd) returnValue = ErrorCodes.Success;
                    }
                    else
                    {
                        returnValue = ErrorCodes.PermissionInvalid;
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
                returnValue = ErrorCodes.Exception;
            }
            return returnValue;
        }

        public Task<Beam> GetByIdAsync(long id)
        {
            var returnValue = Task.FromResult(default(Beam));
            try
            {
                using (var _context = GetContext())
                {
                    returnValue = _context.HashGetExAsync<Beam>(id, CommandFlags.PreferSlave);
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
            }
            return returnValue;
        }

        public Task<BeamSearch> GetBeamFullInfoAsync(long beamId)
        {
            var returnValue = Task.FromResult(default(BeamSearch));
            try
            {
                using (var _context = GetContext())
                {
                    returnValue = _context.ExecAsync(async (db) =>
                    {
                        var result = default(BeamSearch);
                        try
                        {
                            var beam = Json.Parse<Beam>(await db.HashGetAsync(_context.NameOf(typeof(Beam).Name), beamId, CommandFlags.PreferSlave));
                            if (beam != null)
                            {
                                result = Mapper<BeamSearch>.Map(beam, new BeamSearch());
                                if (returnValue != null)
                                {
                                    var listAccountId = await db.SetMembersAsync(_context.NameOf(IndexKeys.KeyAccountInNews(beamId)), CommandFlags.PreferSlave);
                                    result.NewsInAccount = (await db.HashGetAsync(_context.NameOf(typeof(NewsInAccount).Name), listAccountId?.Select(p => (RedisValue)(p + IndexKeys.SeparateChar + beamId))?.ToArray(), CommandFlags.PreferSlave))?.Select(p => Json.Parse<NewsInAccount>(p))?.ToArray();

                                    var listDistributorIdAndItemStreamId = await db.SortedSetRangeByScoreWithScoresAsync(_context.NameOf(IndexKeys.KeyNewsDistribution(beamId)), double.MinValue, double.MaxValue, Exclude.None, Order.Ascending, 0, -1, CommandFlags.PreferSlave);

                                    var listKeyNewsDistribution = listDistributorIdAndItemStreamId?.Select(p => (RedisValue)(p.Element.ToString()?.Split(IndexKeys.SeparateChar)?.FirstOrDefault() + IndexKeys.SeparateChar + beamId
                                    + IndexKeys.SeparateChar + p.Element.ToString().Split(IndexKeys.SeparateChar)?.LastOrDefault()))?.ToArray();

                                    result.NewsDistribution = (await db.HashGetAsync(_context.NameOf(typeof(NewsDistribution).Name), listKeyNewsDistribution, CommandFlags.PreferSlave))?.Select(p => Json.Parse<NewsDistribution>(p))?.ToArray();

                                    var listArticleInBeam = await db.SortedSetRangeByScoreWithScoresAsync(_context.NameOf(IndexKeys.KeyArticleInBeam(beamId)), double.MinValue, double.MaxValue, Exclude.None, Order.Ascending, 0, -1, CommandFlags.PreferSlave);

                                    var listKeyArticleInBeam = listArticleInBeam?.Select(p => (RedisValue)(beamId + IndexKeys.SeparateChar + p.Element.ToString()))?.ToArray();
                                    result.ArticleInBeam = (await db.HashGetAsync(_context.NameOf(typeof(ArticleInBeam).Name), listKeyArticleInBeam, CommandFlags.PreferSlave))?.Select(p => Json.Parse<ArticleInBeam>(p))?.ToArray();
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            Logger.Error(ex, ex.Message);
                        }
                        return result;
                    });
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
            }
            return returnValue;
        }
        public async Task<ErrorCodes> UpdateAsync(Beam data, ArticleInBeam[] articleInBeams, long officerId = 0)
        {
            var returnValue = ErrorCodes.UpdateError;
            try
            {
                using (var _context = GetContext())
                {
                    var approverOld = string.Empty;
                    var dataDb = await _context.HashGetExAsync<Beam>(data.Id);

                    if (dataDb != null)
                    {
                        var isModifier = await Functions.Function.IsModifier(officerId, data.Id, data.ModifiedBy, dataDb.CreatedBy, dataDb.Status, _context);

                        if (isModifier)
                        {
                            approverOld = dataDb.ApprovedBy;

                            dataDb.Url = data.Url;
                            dataDb.OriginalUrl = data.OriginalUrl;
                            dataDb.Author = data.Author;
                            dataDb.CategoryId = data.CategoryId;
                            dataDb.Location = data.Location;
                            dataDb.PublishMode = data.PublishMode;
                            dataDb.PublishData = data.PublishData;
                            dataDb.DistributionDate = data.DistributionDate.HasValue && data.DistributionDate >= DateTime.Now ? data.DistributionDate : dataDb.DistributionDate;
                            dataDb.ModifiedDate = data.ModifiedDate;
                            dataDb.ModifiedBy = data.ModifiedBy;
                            dataDb.DistributorId = data.DistributorId;
                            dataDb.Tags = data.Tags;
                            dataDb.CardType = data.CardType;
                            dataDb.Name = data.Name;
                            dataDb.UnsignName = data.UnsignName;
                            dataDb.Description = data.Description;
                            dataDb.LastInsertedDate = data.LastInsertedDate;
                            dataDb.BeamRelation = data.BeamRelation;
                            dataDb.MetaAvatar = data.MetaAvatar;
                            dataDb.MetaData = data.MetaData;
                            dataDb.Cover = data.Cover;

                            if (dataDb.Status != (int)NewsStatus.Published)
                            {
                                dataDb.ApprovedBy = data.ApprovedBy;
                            }
                            dataDb.CommentMode = data.CommentMode;

                            var oldArticleInBeamIds = await _context.SortedSetRangeByScoreAsync(IndexKeys.KeyArticleInBeam(data.Id), double.NegativeInfinity, double.PositiveInfinity, Exclude.None, Order.Ascending, 0, -1, CommandFlags.PreferSlave);

                            var newsAll = Mapper<NewsAll>.MapNewsAll(dataDb, new NewsAll());

                            var resultUpdate = await _context.MultiAsync((transac) =>
                            {

                                transac.HashSetAsync(_context.NameOf(dataDb.GetType().Name), dataDb.Id, Json.Stringify(dataDb));
                                transac.HashSetAsync(_context.NameOf(newsAll.GetType().Name), newsAll.Id, Json.Stringify(newsAll));

                                if (articleInBeams != null && articleInBeams.Count() > 0)
                                {
                                    transac.HashDeleteAsync(_context.NameOf(articleInBeams.FirstOrDefault().GetType().Name), oldArticleInBeamIds?.Except(articleInBeams?.Select(p => (RedisValue)p.ArticleId)).Select(p => (RedisValue)(data.Id + IndexKeys.SeparateChar + p))?.ToArray());

                                    transac.HashSetAsync(_context.NameOf(articleInBeams.FirstOrDefault().GetType().Name)
                                        , articleInBeams.Select(p => new HashEntry(p.BeamId + IndexKeys.SeparateChar + p.ArticleId, Json.Stringify(p))).ToArray());

                                    ////////////////
                                    ///
                                    var listRemove = oldArticleInBeamIds?.Except(articleInBeams?.Select(p => (RedisValue)p.ArticleId))?.ToArray();
                                    if (listRemove != null && listRemove.Count() > 0)
                                    {
                                        transac.SortedSetRemoveAsync(_context.NameOf(IndexKeys.KeyArticleInBeam(data.Id)),
                                                                        oldArticleInBeamIds?.Except(articleInBeams?.Select(p => (RedisValue)p.ArticleId))?.ToArray());
                                    }

                                    transac.SortedSetAddAsync(_context.NameOf(IndexKeys.KeyArticleInBeam(data.Id))
                                        , articleInBeams.Where(p => !oldArticleInBeamIds.Contains(p.ArticleId)).Select(p => new SortedSetEntry(p.ArticleId, (p.PublishedDate ?? DateTime.Now).ConvertToTimestamp10())).ToArray());

                                    //data all ArticleInBeam
                                    var listRemove2 = oldArticleInBeamIds?.Except(articleInBeams?.Select(p => (RedisValue)p.ArticleId)).Select(p => (RedisValue)(data.Id + IndexKeys.SeparateChar + p))?.ToArray();
                                    if (listRemove2 != null && listRemove2.Count() > 0)
                                    {
                                        transac.SortedSetRemoveAsync(_context.NameOf(IndexKeys.KeyPublishedDate<ArticleInBeam>()),
                                        oldArticleInBeamIds?.Except(articleInBeams?.Select(p => (RedisValue)p.ArticleId)).Select(p => (RedisValue)(data.Id + IndexKeys.SeparateChar + p))?.ToArray());
                                    }

                                    transac.SortedSetAddAsync(_context.NameOf(IndexKeys.KeyPublishedDate<ArticleInBeam>())
                                        , articleInBeams.Select(p => new SortedSetEntry(p.BeamId + IndexKeys.SeparateChar + p.ArticleId, Utility.ConvertToTimestamp10(p.PublishedDate ?? DateTime.Now))).ToArray());
                                }

                                if (dataDb.Status == (int)NewsStatus.Approve)
                                {
                                    if (long.TryParse(approverOld, out long _approverOld))
                                        transac.SortedSetRemoveAsync(_context.NameOf(IndexKeys.KeyApproveInAccount<Beam>(officerId, _approverOld)), dataDb.Id);

                                    if (!string.IsNullOrEmpty(dataDb.ApprovedBy))
                                    {
                                        if (long.TryParse(dataDb.ApprovedBy, out long accountId))
                                        {
                                            transac.SortedSetAddAsync(_context.NameOf(IndexKeys.KeyApproveInAccount<Beam>(officerId, accountId)), dataDb.Id, DateTime.Now.ConvertToTimestamp10());
                                        }
                                    }
                                }
                            });
                            if (resultUpdate) returnValue = ErrorCodes.Success;
                        }
                        else
                        {
                            returnValue = ErrorCodes.PermissionUpdateInvalid;
                        }
                    }
                    else
                    {
                        return ErrorCodes.DataNotExist;
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
                returnValue = ErrorCodes.Exception;
            }
            return returnValue;
        }

        public async Task<ErrorCodes> UpdateV2Async(Beam data, ArticleInBeam[] articleInBeams, long officerId, Action<int> actionAutoShare)
        {
            var returnValue = ErrorCodes.UpdateError;
            var statusOld = default(int?);
            try
            {
                using (var _context = GetContext())
                {
                    var approverOld = string.Empty;
                    var dataDb = await _context.HashGetExAsync<Beam>(data.Id);

                    if (dataDb != null)
                    {
                        statusOld = dataDb.Status;

                        var isModifier = await Functions.Function.IsModifier(officerId, data.Id, data.ModifiedBy, dataDb.CreatedBy, dataDb.Status, _context);

                        if (isModifier)
                        {
                            approverOld = dataDb.ApprovedBy;

                            dataDb.Url = data.Url;
                            dataDb.OriginalUrl = data.OriginalUrl;
                            dataDb.Author = data.Author;
                            dataDb.CategoryId = data.CategoryId;
                            dataDb.Location = data.Location;
                            dataDb.PublishMode = data.PublishMode;
                            dataDb.PublishData = data.PublishData;
                            dataDb.DistributionDate = data.DistributionDate.HasValue && data.DistributionDate >= DateTime.Now ? data.DistributionDate : dataDb.DistributionDate;
                            dataDb.ModifiedDate = data.ModifiedDate;
                            dataDb.ModifiedBy = data.ModifiedBy;
                            dataDb.DistributorId = data.DistributorId;
                            dataDb.Tags = data.Tags;
                            dataDb.CardType = data.CardType;
                            dataDb.Name = data.Name;
                            dataDb.UnsignName = data.UnsignName;
                            dataDb.Description = data.Description;
                            dataDb.LastInsertedDate = data.LastInsertedDate;
                            dataDb.BeamRelation = data.BeamRelation;
                            dataDb.MetaAvatar = data.MetaAvatar;
                            dataDb.MetaData = data.MetaData;
                            dataDb.Cover = data.Cover;

                            if (dataDb.Status != (int)NewsStatus.Published)
                            {
                                dataDb.ApprovedBy = data.ApprovedBy;
                            }
                            dataDb.CommentMode = data.CommentMode;

                            var oldArticleInBeamIds = await _context.SortedSetRangeByScoreAsync(IndexKeys.KeyArticleInBeam(data.Id), double.NegativeInfinity, double.PositiveInfinity, Exclude.None, Order.Ascending, 0, -1, CommandFlags.PreferSlave);

                            var newsAll = Mapper<NewsAll>.MapNewsAll(dataDb, new NewsAll());

                            var resultUpdate = await _context.MultiAsync((transac) =>
                            {

                                transac.HashSetAsync(_context.NameOf(dataDb.GetType().Name), dataDb.Id, Json.Stringify(dataDb));
                                transac.HashSetAsync(_context.NameOf(newsAll.GetType().Name), newsAll.Id, Json.Stringify(newsAll));

                                if (articleInBeams != null && articleInBeams.Count() > 0)
                                {
                                    transac.HashDeleteAsync(_context.NameOf(articleInBeams.FirstOrDefault().GetType().Name), oldArticleInBeamIds?.Except(articleInBeams?.Select(p => (RedisValue)p.ArticleId)).Select(p => (RedisValue)(data.Id + IndexKeys.SeparateChar + p))?.ToArray());

                                    transac.HashSetAsync(_context.NameOf(articleInBeams.FirstOrDefault().GetType().Name)
                                        , articleInBeams.Select(p => new HashEntry(p.BeamId + IndexKeys.SeparateChar + p.ArticleId, Json.Stringify(p))).ToArray());

                                    ////////////////
                                    ///
                                    var listRemove = oldArticleInBeamIds?.Except(articleInBeams?.Select(p => (RedisValue)p.ArticleId))?.ToArray();
                                    if (listRemove != null && listRemove.Count() > 0)
                                    {
                                        transac.SortedSetRemoveAsync(_context.NameOf(IndexKeys.KeyArticleInBeam(data.Id)),
                                                                        oldArticleInBeamIds?.Except(articleInBeams?.Select(p => (RedisValue)p.ArticleId))?.ToArray());
                                    }

                                    transac.SortedSetAddAsync(_context.NameOf(IndexKeys.KeyArticleInBeam(data.Id))
                                        , articleInBeams.Where(p => !oldArticleInBeamIds.Contains(p.ArticleId)).Select(p => new SortedSetEntry(p.ArticleId, (p.PublishedDate ?? DateTime.Now).ConvertToTimestamp10())).ToArray());

                                    //data all ArticleInBeam
                                    var listRemove2 = oldArticleInBeamIds?.Except(articleInBeams?.Select(p => (RedisValue)p.ArticleId)).Select(p => (RedisValue)(data.Id + IndexKeys.SeparateChar + p))?.ToArray();
                                    if (listRemove2 != null && listRemove2.Count() > 0)
                                    {
                                        transac.SortedSetRemoveAsync(_context.NameOf(IndexKeys.KeyPublishedDate<ArticleInBeam>()),
                                        oldArticleInBeamIds?.Except(articleInBeams?.Select(p => (RedisValue)p.ArticleId)).Select(p => (RedisValue)(data.Id + IndexKeys.SeparateChar + p))?.ToArray());
                                    }

                                    transac.SortedSetAddAsync(_context.NameOf(IndexKeys.KeyPublishedDate<ArticleInBeam>())
                                        , articleInBeams.Select(p => new SortedSetEntry(p.BeamId + IndexKeys.SeparateChar + p.ArticleId, Utility.ConvertToTimestamp10(p.PublishedDate ?? DateTime.Now))).ToArray());
                                }

                                if (dataDb.Status == (int)NewsStatus.Approve)
                                {
                                    if (long.TryParse(approverOld, out long _approverOld))
                                        transac.SortedSetRemoveAsync(_context.NameOf(IndexKeys.KeyApproveInAccount<Beam>(officerId, _approverOld)), dataDb.Id);

                                    if (!string.IsNullOrEmpty(dataDb.ApprovedBy))
                                    {
                                        if (long.TryParse(dataDb.ApprovedBy, out long accountId))
                                        {
                                            transac.SortedSetAddAsync(_context.NameOf(IndexKeys.KeyApproveInAccount<Beam>(officerId, accountId)), dataDb.Id, DateTime.Now.ConvertToTimestamp10());
                                        }
                                    }
                                }
                            });
                            if (resultUpdate) returnValue = ErrorCodes.Success;
                        }
                        else
                        {
                            returnValue = ErrorCodes.PermissionUpdateInvalid;
                        }
                    }
                    else
                    {
                        return ErrorCodes.DataNotExist;
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
                returnValue = ErrorCodes.Exception;
            }
            if (returnValue == ErrorCodes.Success && statusOld != null)
            {
                actionAutoShare(statusOld ?? 0);
            }

            return returnValue;
        }

        public Task<bool> UpdateStatusAsync(Beam data, int statusOld)
        {
            var returnValue = Task.FromResult(false);
            try
            {
                if (data != null)
                {
                    var newsAll = Mapper<NewsAll>.MapNewsAll(data, new NewsAll());

                    using (var _context = GetContext())
                    {
                        returnValue = _context.MultiAsync((transac) =>
                        {
                            transac.HashSetAsync(_context.NameOf(data.GetType().Name), data.Id, Json.Stringify(data));

                            transac.HashSetAsync(_context.NameOf(newsAll.GetType().Name), newsAll.Id, Json.Stringify(newsAll));
                            transac.SortedSetRemoveAsync(_context.NameOf(IndexKeys.KeyStatus<NewsAll>(statusOld)), newsAll.Id);
                            transac.SortedSetAddAsync(_context.NameOf(IndexKeys.KeyStatus<NewsAll>(newsAll.Status)), newsAll.Id, (newsAll.CreatedDate ?? DateTime.Now).ConvertToTimestamp10());
                        });
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
            }
            return returnValue;
        }


        public async Task<ErrorCodes> UpdateStatusV2Async(long id, NewsStatus status, long userId, Action<BeamSearch, int> actionAutoShare)
        {
            var returnValue = ErrorCodes.UpdateError;
            var statusOld = default(int?);
            var dataDb = default(Beam);

            try
            {
                using (var _context = GetContext())
                {
                    
                    var newsAll = default(NewsAll);
                    var resultUpdate = false;
                    var officerIds = default(RedisValue[]);
                    var officerId = 0L;

                    dataDb = await _context.HashGetExAsync<Beam>(id, CommandFlags.PreferSlave);
                    statusOld = dataDb.Status;

                    if (dataDb != null)
                    {
                        if (dataDb.Status == (int)status)
                        {
                            returnValue = ErrorCodes.Success;
                        }
                        else
                        {
                            switch (status)
                            {
                                #region Xuat ban
                                case NewsStatus.Published:// xuất bản
                                    officerIds = await _context.SetMembersAsync(IndexKeys.KeyAccountInNews(id), CommandFlags.PreferSlave);
                                    if (officerIds != null && officerIds.Count() != 0)
                                    {
                                        if (long.TryParse(officerIds[0], out officerId))
                                        {
                                            var officer = await _context.HashGetExAsync<Account>(officerId, CommandFlags.PreferSlave);

                                            var listMemberId = await _context.SortedSetRangeByScoreAsync(IndexKeys.KeyAccountMemberInOfficer<AccountMember>(officerId), double.NegativeInfinity, double.PositiveInfinity);

                                            var listMember = await _context.GetsFromHashExAsync<AccountMember>(listMemberId?.Select(p => officerId + IndexKeys.SeparateChar + p.ToString())?.ToList(), CommandFlags.PreferSlave);
                                            var accountMember = listMember?.Where(p => p.MemberId == userId).FirstOrDefault();

                                            if (accountMember != null && officer != null)
                                            {
                                                var isPublisher = await Functions.Function.IsPublisher(accountMember, dataDb.CreatedBy, dataDb.ApprovedBy, officer.Mode, dataDb.Status);
                                                if (isPublisher)
                                                {
                                                    dataDb.Status = (int)status;
                                                    dataDb.ModifiedDate = DateTime.Now;
                                                    dataDb.ModifiedBy = userId.ToString();
                                                    dataDb.DistributionDate = dataDb.DistributionDate > DateTime.Now ? dataDb.DistributionDate : DateTime.Now;

                                                    var oldApprovedBy = dataDb.ApprovedBy;
                                                    dataDb.ApprovedBy = accountMember.MemberId.ToString();
                                                    dataDb.ApprovedDate = DateTime.Now;

                                                    newsAll = Mapper<NewsAll>.MapNewsAll(dataDb, new NewsAll());

                                                    resultUpdate = await _context.MultiAsync((transac) =>
                                                    {
                                                        transac.HashSetAsync(_context.NameOf(dataDb.GetType().Name), dataDb.Id, Json.Stringify(dataDb));
                                                        transac.HashSetAsync(_context.NameOf(newsAll.GetType().Name), newsAll.Id, Json.Stringify(newsAll));

                                                        transac.SortedSetRemoveAsync(_context.NameOf(IndexKeys.KeyStatus<NewsAll>(statusOld)), newsAll.Id);
                                                        transac.SortedSetAddAsync(_context.NameOf(IndexKeys.KeyStatus<NewsAll>(newsAll.Status)), newsAll.Id, (newsAll.DistributionDate ?? DateTime.Now).ConvertToTimestamp10());

                                                        //remove value in set approve in account if exist
                                                        if (!string.IsNullOrEmpty(oldApprovedBy) && long.TryParse(oldApprovedBy, out long oldApprovedById))
                                                        {
                                                            transac.SortedSetRemoveAsync(_context.NameOf(IndexKeys.KeyApproveInAccount<Beam>(officerId, oldApprovedById)), dataDb.Id, CommandFlags.PreferSlave);
                                                        }
                                                        else
                                                        {
                                                            //remove value in set approve if exist
                                                            transac.SortedSetRemoveAsync(_context.NameOf(IndexKeys.KeyApprove<Beam>(officerId)), dataDb.Id, CommandFlags.PreferSlave);
                                                        }

                                                        //add value into key approved in account
                                                        transac.SortedSetAddAsync(_context.NameOf(IndexKeys.KeyApprovedInAccount<Beam>(officerId, userId)), dataDb.Id, DateTime.Now.ConvertToTimestamp10());
                                                    });

                                                    if (resultUpdate) returnValue = ErrorCodes.Success;
                                                }
                                                else
                                                {
                                                    returnValue = ErrorCodes.PermissionPublishInvalid;
                                                }
                                            }
                                            else
                                            {
                                                returnValue = ErrorCodes.PermissionPublishInvalid;
                                            }
                                        }
                                        else
                                        {
                                            returnValue = ErrorCodes.DataInvalid;
                                        }
                                    }
                                    else
                                    {
                                        returnValue = ErrorCodes.DataInvalid;
                                    }
                                    break;
                                #endregion
                                #region Xoa bai
                                case NewsStatus.Remove: // xóa bài
                                    statusOld = dataDb.Status;
                                    if (statusOld != (int)NewsStatus.Published)
                                    {
                                        if (userId.ToString().Equals(dataDb.CreatedBy))
                                        {
                                            statusOld = dataDb.Status;
                                            dataDb.Status = (int)status;
                                            dataDb.ModifiedDate = DateTime.Now;
                                            dataDb.ModifiedBy = userId.ToString();

                                            newsAll = Mapper<NewsAll>.MapNewsAll(dataDb, new NewsAll());

                                            resultUpdate = await _context.MultiAsync((transac) =>
                                            {
                                                transac.HashSetAsync(_context.NameOf(dataDb.GetType().Name), dataDb.Id, Json.Stringify(dataDb));

                                                transac.HashSetAsync(_context.NameOf(newsAll.GetType().Name), newsAll.Id, Json.Stringify(newsAll));

                                                transac.SortedSetRemoveAsync(_context.NameOf(IndexKeys.KeyStatus<NewsAll>(statusOld)), newsAll.Id);
                                                transac.SortedSetAddAsync(_context.NameOf(IndexKeys.KeyStatus<NewsAll>(newsAll.Status)), newsAll.Id, (dataDb.ModifiedDate ?? DateTime.Now).ConvertToTimestamp10());
                                            });

                                            if (resultUpdate) returnValue = ErrorCodes.Success;
                                        }
                                        else
                                        {
                                            returnValue = ErrorCodes.PermissionUpdateInvalid;
                                        }
                                    }
                                    else
                                    {
                                        // trạng thái cũ là publish
                                        officerIds = await _context.SetMembersAsync(IndexKeys.KeyAccountInNews(id), CommandFlags.PreferSlave);
                                        if (officerIds != null && officerIds.Count() != 0)
                                        {
                                            if (long.TryParse(officerIds[0], out officerId))
                                            {
                                                var officer = await _context.HashGetExAsync<Account>(officerId, CommandFlags.PreferSlave);

                                                var listMemberId = await _context.SortedSetRangeByScoreAsync(IndexKeys.KeyAccountMemberInOfficer<AccountMember>(officerId), double.NegativeInfinity, double.PositiveInfinity);

                                                var listMember = await _context.GetsFromHashExAsync<AccountMember>(listMemberId?.Select(p => officerId + IndexKeys.SeparateChar + p.ToString())?.ToList(), CommandFlags.PreferSlave);
                                                var accountMember = listMember?.Where(p => p.MemberId == userId).FirstOrDefault();

                                                if(accountMember!=null && officer != null)
                                                {
                                                    var isRemover = await Functions.Function.IsRemover(accountMember, dataDb.CreatedBy, dataDb.ApprovedBy, officer.Mode, statusOld);
                                                    if (isRemover)
                                                    {
                                                        dataDb.Status = (int)status;
                                                        dataDb.ModifiedDate = DateTime.Now;
                                                        dataDb.ModifiedBy = userId.ToString();

                                                        newsAll = Mapper<NewsAll>.MapNewsAll(dataDb, new NewsAll());

                                                        resultUpdate = await _context.MultiAsync((transac) =>
                                                        {
                                                            transac.HashSetAsync(_context.NameOf(dataDb.GetType().Name), dataDb.Id, Json.Stringify(dataDb));

                                                            transac.HashSetAsync(_context.NameOf(newsAll.GetType().Name), newsAll.Id, Json.Stringify(newsAll));

                                                            transac.SortedSetRemoveAsync(_context.NameOf(IndexKeys.KeyStatus<NewsAll>(statusOld)), newsAll.Id);
                                                            transac.SortedSetAddAsync(_context.NameOf(IndexKeys.KeyStatus<NewsAll>(newsAll.Status)), newsAll.Id, (dataDb.ModifiedDate ?? DateTime.Now).ConvertToTimestamp10());

                                                            if (long.TryParse(dataDb.ApprovedBy, out long accountId))
                                                            {
                                                                transac.SortedSetRemoveAsync(_context.NameOf(IndexKeys.KeyApprovedInAccount<Beam>(officerId, accountId)), dataDb.Id);
                                                            }
                                                        });
                                                        if (resultUpdate) returnValue = ErrorCodes.Success;
                                                    }
                                                    else
                                                    {
                                                        returnValue = ErrorCodes.PermissionUpdateInvalid;
                                                    }
                                                }
                                                else
                                                {
                                                    returnValue = ErrorCodes.PermissionUpdateInvalid;
                                                }
                                            }
                                            else
                                            {
                                                returnValue = ErrorCodes.DataInvalid;
                                            }
                                        }
                                        else
                                        {
                                            returnValue = ErrorCodes.DataInvalid;
                                        }
                                    }

                                    break;
                                #endregion
                                #region Gui nho duyet
                                case NewsStatus.Approve:// Gửi nhờ duyệt
                                    if (dataDb.Status == (int)NewsStatus.Published)
                                    {
                                        returnValue = ErrorCodes.DataInvalid;
                                    }
                                    else
                                    {
                                        if (userId.ToString().Equals(dataDb.CreatedBy))
                                        {
                                            officerIds = await _context.SetMembersAsync(IndexKeys.KeyAccountInNews(id), CommandFlags.PreferSlave);
                                            if (officerIds != null && officerIds.Count() != 0)
                                            {
                                                if (long.TryParse(officerIds[0], out officerId))
                                                {
                                                    dataDb.Status = (int)status;
                                                    dataDb.ModifiedDate = DateTime.Now;
                                                    dataDb.ModifiedBy = userId.ToString();

                                                    newsAll = Mapper<NewsAll>.MapNewsAll(dataDb, new NewsAll());

                                                    resultUpdate = await _context.MultiAsync((transac) =>
                                                    {
                                                        transac.HashSetAsync(_context.NameOf(dataDb.GetType().Name), dataDb.Id, Json.Stringify(dataDb));

                                                        transac.HashSetAsync(_context.NameOf(newsAll.GetType().Name), newsAll.Id, Json.Stringify(newsAll));

                                                        transac.SortedSetRemoveAsync(_context.NameOf(IndexKeys.KeyStatus<NewsAll>(statusOld)), newsAll.Id);
                                                        transac.SortedSetAddAsync(_context.NameOf(IndexKeys.KeyStatus<NewsAll>(newsAll.Status)), newsAll.Id, (dataDb.ModifiedDate ?? DateTime.Now).ConvertToTimestamp10());

                                                        if (string.IsNullOrEmpty(dataDb.ApprovedBy))
                                                        {
                                                            transac.SortedSetAddAsync(_context.NameOf(IndexKeys.KeyApprove<Beam>(officerId)), dataDb.Id, DateTime.Now.ConvertToTimestamp10());
                                                        }
                                                        else
                                                        {
                                                            if (long.TryParse(dataDb.ApprovedBy, out long accountId))
                                                            {
                                                                transac.SortedSetAddAsync(_context.NameOf(IndexKeys.KeyApproveInAccount<Beam>(officerId, accountId)), dataDb.Id, DateTime.Now.ConvertToTimestamp10());
                                                            }
                                                        }
                                                    });
                                                }
                                            }

                                            if (resultUpdate) returnValue = ErrorCodes.Success;
                                        }
                                        else
                                        {
                                            returnValue = ErrorCodes.PermissionUpdateInvalid;
                                        }
                                    }
                                    break;
                                #endregion
                                #region default
                                default:
                                    if (userId.ToString().Equals(dataDb.CreatedBy))
                                    {
                                        dataDb.Status = (int)status;
                                        dataDb.ModifiedDate = DateTime.Now;
                                        dataDb.ModifiedBy = userId.ToString();

                                        if (statusOld == (int)NewsStatus.Published)
                                        {
                                            officerIds = await _context.SetMembersAsync(IndexKeys.KeyAccountInNews(id), CommandFlags.PreferSlave);
                                            if (officerIds != null && officerIds.Count() != 0)
                                            {
                                                long.TryParse(officerIds[0], out officerId);
                                            }
                                        }

                                        newsAll = Mapper<NewsAll>.MapNewsAll(dataDb, new NewsAll());

                                        resultUpdate = await _context.MultiAsync((transac) =>
                                        {
                                            transac.HashSetAsync(_context.NameOf(dataDb.GetType().Name), dataDb.Id, Json.Stringify(dataDb));

                                            transac.HashSetAsync(_context.NameOf(newsAll.GetType().Name), newsAll.Id, Json.Stringify(newsAll));

                                            transac.SortedSetRemoveAsync(_context.NameOf(IndexKeys.KeyStatus<NewsAll>(statusOld)), newsAll.Id);
                                            transac.SortedSetAddAsync(_context.NameOf(IndexKeys.KeyStatus<NewsAll>(newsAll.Status)), newsAll.Id, (dataDb.ModifiedDate ?? DateTime.Now).ConvertToTimestamp10());

                                            if (officerId > 0)
                                            {
                                                if (long.TryParse(dataDb.ApprovedBy, out long accountId))
                                                {
                                                    transac.SortedSetRemoveAsync(_context.NameOf(IndexKeys.KeyApprovedInAccount<Beam>(officerId, accountId)), dataDb.Id);
                                                }
                                            }
                                        });

                                        if (resultUpdate) returnValue = ErrorCodes.Success;
                                    }
                                    else
                                    {
                                        if (statusOld == (int?)NewsStatus.Published || statusOld == (int?)NewsStatus.Archive || statusOld == (int?)NewsStatus.Approve)
                                        {
                                            officerIds = await _context.SetMembersAsync(IndexKeys.KeyAccountInNews(id), CommandFlags.PreferSlave);
                                            if (officerIds != null && officerIds.Count() != 0)
                                            {
                                                if (long.TryParse(officerIds[0], out officerId))
                                                {
                                                    var officer = await _context.HashGetExAsync<Account>(officerId, CommandFlags.PreferSlave);
                                                    var listMemberId = await _context.SortedSetRangeByScoreAsync(IndexKeys.KeyAccountMemberInOfficer<AccountMember>(officerId), double.NegativeInfinity, double.PositiveInfinity);

                                                    var listMember = await _context.GetsFromHashExAsync<AccountMember>(listMemberId?.Select(p => officerId + IndexKeys.SeparateChar + p.ToString())?.ToList(), CommandFlags.PreferSlave);
                                                    var accountMember = listMember?.Where(p => p.MemberId == userId).FirstOrDefault();

                                                    if(accountMember != null && officer != null)
                                                    {
                                                        var isRemover = await Functions.Function.IsRemover(accountMember, dataDb.CreatedBy, dataDb.ApprovedBy, officer.Mode, statusOld);
                                                        if (isRemover)
                                                        {
                                                            dataDb.Status = (int)status;
                                                            dataDb.ModifiedDate = DateTime.Now;
                                                            dataDb.ModifiedBy = userId.ToString();

                                                            newsAll = Mapper<NewsAll>.MapNewsAll(dataDb, new NewsAll());

                                                            resultUpdate = await _context.MultiAsync((transac) =>
                                                            {
                                                                transac.HashSetAsync(_context.NameOf(dataDb.GetType().Name), dataDb.Id, Json.Stringify(dataDb));

                                                                transac.HashSetAsync(_context.NameOf(newsAll.GetType().Name), newsAll.Id, Json.Stringify(newsAll));

                                                                transac.SortedSetRemoveAsync(_context.NameOf(IndexKeys.KeyStatus<NewsAll>(statusOld)), newsAll.Id);
                                                                transac.SortedSetAddAsync(_context.NameOf(IndexKeys.KeyStatus<NewsAll>(newsAll.Status)), newsAll.Id, (dataDb.ModifiedDate ?? DateTime.Now).ConvertToTimestamp10());

                                                                if (officerId > 0)
                                                                {
                                                                    if (long.TryParse(dataDb.ApprovedBy, out long accountId))
                                                                    {
                                                                        transac.SortedSetRemoveAsync(_context.NameOf(IndexKeys.KeyApprovedInAccount<Beam>(officerId, accountId)), dataDb.Id);
                                                                    }
                                                                }
                                                            });

                                                            if (resultUpdate) returnValue = ErrorCodes.Success;
                                                        }
                                                        else
                                                        {
                                                            returnValue = ErrorCodes.PermissionUpdateInvalid;
                                                        }
                                                    }
                                                    else
                                                    {
                                                        returnValue = ErrorCodes.PermissionUpdateInvalid;
                                                    }
                                                }
                                                else
                                                {
                                                    returnValue = ErrorCodes.DataInvalid;
                                                }
                                            }
                                            else
                                            {
                                                returnValue = ErrorCodes.DataInvalid;
                                            }
                                        }
                                        else
                                        {
                                            returnValue = ErrorCodes.PermissionUpdateInvalid;
                                        }
                                    }

                                    break;
                                    #endregion
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
            }

            if(returnValue == ErrorCodes.Success && dataDb!=null && statusOld != null)
            {
                actionAutoShare(Mapper<BeamSearch>.Map(dataDb, new BeamSearch()), statusOld ?? 0);
            }

            return returnValue;
        }

        public Task<List<Beam>> GetListByIdsAsync(List<string> ids)
        {
            var returnValue = Task.FromResult(default(List<Beam>));
            try
            {
                using (var _context = GetContext())
                {
                    returnValue = _context.GetsFromHashExAsync<Beam>(ids, CommandFlags.PreferSlave);
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
            }
            return returnValue;
        }

        public Task<List<BeamSearch>> GetListBeamSearchByIdsAsync(List<string> ids)
        {
            var returnValue = Task.FromResult(default(List<BeamSearch>));
            try
            {
                using (var _context = GetContext())
                {
                    returnValue = _context.ExecAsync(async (db) =>
                    {
                        var result = default(List<BeamSearch>);
                        try
                        {
                            var listValue = (await db.HashGetAsync(_context.NameOf<Beam>(), ids?.Select(p => (RedisValue)p)?.ToArray(), CommandFlags.PreferSlave))
                        ?.Select(p => Json.Parse<Beam>(p))?.ToList();
                            if (listValue != null)
                            {
                                result = new List<BeamSearch>();
                                foreach (var playlist in listValue)
                                {
                                    var beamSearchReturn = Mapper<BeamSearch>.Map(playlist, new BeamSearch());
                                    var listTmp = new List<NewsDistribution>();//videoPlaylistSearchReturn.NewsDistribution = new NewsDistribution[] { };
                                    try
                                    {
                                        var newsDistribution = await db.SortedSetRangeByScoreWithScoresAsync(_context.NameOf(IndexKeys.KeyNewsDistribution(playlist.Id)), double.MinValue, double.MaxValue, Exclude.None, Order.Ascending, 0, -1, CommandFlags.PreferSlave);
                                        if (newsDistribution != null)
                                        {
                                            foreach (var item in newsDistribution)
                                            {
                                                if (long.TryParse(item.Element.ToString()?.Split(IndexKeys.SeparateChar)?.FirstOrDefault(), out long distributorId))
                                                {
                                                    listTmp.Add(new NewsDistribution
                                                    {
                                                        DistributorId = distributorId,
                                                        SharedDate = Utility.ConvertSecondsToDate(item.Score),
                                                        DistributorName = Json.Parse<Distributor>(await db.HashGetAsync(_context.NameOf<Distributor>(), distributorId, CommandFlags.PreferSlave))?.Name
                                                    });
                                                }
                                            }
                                        }
                                    }
                                    catch (Exception ex)
                                    {
                                        Logger.Error(ex, ex.Message);
                                    }
                                    beamSearchReturn.NewsDistribution = listTmp.ToArray();

                                    //get ArticleInBeam
                                    try
                                    {
                                        var listKeyArticle = await db.SortedSetRangeByRankAsync(_context.NameOf(IndexKeys.KeyArticleInBeam(beamSearchReturn.Id)), 0, -1, Order.Ascending, CommandFlags.PreferSlave);

                                        beamSearchReturn.ArticleInBeam = (await db.HashGetAsync(_context.NameOf<ArticleInBeam>(), listKeyArticle?.Select(p => (RedisValue)(beamSearchReturn.Id + IndexKeys.SeparateChar + p))?.ToArray(), CommandFlags.PreferSlave))?.Select(p => Json.Parse<ArticleInBeam>(p))?.ToArray();
                                    }
                                    catch (Exception ex)
                                    {
                                        Logger.Error(ex, ex.Message);
                                    }

                                    result.Add(beamSearchReturn);
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            Logger.Error(ex, ex.Message);
                        }

                        return result;
                    });
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
            }
            return returnValue;
        }

        public Task<PagingDataResult<BeamSearchReturn>> SearchAsync(SearchNews data, long userId, Action<string> action)
        {
            var returnValue = Task.FromResult(default(PagingDataResult<BeamSearchReturn>));
            try
            {
                using (var _context = GetContext())
                {
                    returnValue = _context.ExecAsync(async (db) =>
                    {
                        //var listMemberId = await db.SortedSetRangeByRankAsync(_context.NameOf(IndexKeys.KeyAccountMemberInOfficer<AccountMember>(data.OfficerId)), 0, -1, Order.Descending, CommandFlags.PreferSlave);
                        //var memberValid = listMemberId?.Where(p => p.ToString().Equals(userId.ToString()))?.Count() > 0;
                        var member = Json.Parse<AccountMember>(await db.HashGetAsync(_context.NameOf<AccountMember>(), data.OfficerId + IndexKeys.SeparateChar + userId, CommandFlags.PreferSlave));

                        if (member != null)
                        {
                            var result = new PagingDataResult<BeamSearchReturn>()
                            {
                                Data = new List<BeamSearchReturn>()
                            };
                            try
                            {
                                var listKey = new RedisValue[] { };
                                var listBeam = new List<Beam>();
                                var total = default(long);
                                var tempKey = _context.NameOf("index:temp:union:" + Generator.SorterSetSearchId());
                                var createdByKey = _context.NameOf(IndexKeys.KeyCreatedBy<NewsAll>(userId.ToString()));
                                var tempKeyCreatedBy = _context.NameOf("index:temp:createdby:" + Generator.SorterSetSearchId());
                                var listkeyFull = new List<RedisKey>();
                                var listCreatedBy = new List<RedisKey>();
                                var value4 = 0L;

                                if (data.Status.Equals(((int)NewsStatus.Published).ToString()) && member.Permissions != null && (member.Permissions.Contains(MemberPermission.ModifyPostOnPage) || member.Permissions.Contains(MemberPermission.RemovePostOnPage)))
                                {

                                }
                                else
                                {
                                    var listOwner = await _context.SortedSetRangeByRankAsync(IndexKeys.KeyOwnerOfficer(userId), 0, -1, Order.Descending, CommandFlags.PreferSlave);
                                    if (listOwner == null || !listOwner.Contains((RedisValue)data.OfficerId))
                                    {
                                        listCreatedBy.Add(createdByKey);

                                        //Lấy cả những bài chỉ duyệt

                                        if (data.Status?.Split(",")?.Contains(((int)NewsStatus.Published).ToString()) ?? false)
                                        {
                                            listCreatedBy.Add(_context.NameOf(IndexKeys.KeyApprovedInAccount<Beam>(data.OfficerId, userId)));
                                        }

                                        value4 = await db.SortedSetCombineAndStoreAsync(SetOperation.Union
                                        , tempKeyCreatedBy, listCreatedBy.ToArray(), null, Aggregate.Max, CommandFlags.None);

                                        if (value4 > 0)
                                        {
                                            listkeyFull.Add(tempKeyCreatedBy);
                                        }
                                    }
                                    else
                                    {
                                        if ((data.Status?.Split(",")?.Contains(((int)NewsStatus.Approve).ToString()) ?? false) || (data.Status?.Split(",")?.Contains(((int)NewsStatus.Draft).ToString()) ?? false) || (data.Status?.Split(",")?.Contains(((int)NewsStatus.Remove).ToString()) ?? false))
                                        {
                                            listkeyFull.Add(createdByKey);
                                        }
                                    }
                                }

                                try
                                {
                                    listkeyFull.Add(_context.NameOf(IndexKeys.KeyNewsInAccount(data.OfficerId)));
                                    listkeyFull.Add(_context.NameOf(IndexKeys.KeyType<NewsAll>((int)NewsType.Beam)));
                                    switch (string.IsNullOrEmpty(data.Status))
                                    {
                                        case true:
                                            if (data.DistributionId == -1)
                                            {
                                                listkeyFull.Add(_context.NameOf(IndexKeys.KeyDistributorid<NewsDistribution>()));
                                            }
                                            else
                                            {
                                                if (data.DistributionId > 0)
                                                {
                                                    listkeyFull.Add(_context.NameOf(IndexKeys.KeyDistributorid<NewsDistribution>(data.DistributionId)));
                                                }
                                            }
                                            var value1 = await db.SortedSetCombineAndStoreAsync(SetOperation.Intersect
                                                , tempKey, listkeyFull.ToArray(), null, Aggregate.Max, CommandFlags.None);
                                            if (value1 > 0)
                                            {
                                                try
                                                {
                                                    listKey = (await db.SortedSetRangeByScoreAsync(tempKey
                                                    , (data.FromDate ?? DateTime.MinValue).ConvertToTimestamp10()
                                                    , (data.ToDate ?? DateTime.MaxValue).ConvertToTimestamp10()
                                                    , Exclude.None
                                                    , data.OrderBy == SearchNewsOrderBy.AscCreatedDate ? Order.Ascending : Order.Descending
                                                    , ((data.PageIndex ?? 1) - 1) * (data.PageSize ?? 20)
                                                    , data.PageSize ?? 20, CommandFlags.None));

                                                    total = await db.SortedSetLengthAsync(tempKey
                                                        , (data.FromDate ?? DateTime.MinValue).ConvertToTimestamp10()
                                                        , (data.ToDate ?? DateTime.MaxValue).ConvertToTimestamp10(), Exclude.None, CommandFlags.None);

                                                    result.Total = (int)total;
                                                    listBeam = listKey != null && listKey.Length > 0 ? (await db.HashGetAsync(_context.NameOf<Beam>(), listKey, CommandFlags.PreferSlave))?.Select(p => Json.Parse<Beam>(p))?.ToList() : listBeam;
                                                    if (listBeam != null)
                                                    {
                                                        foreach (var playlist in listBeam)
                                                        {
                                                            var beamSearchReturn = Mapper<BeamSearchReturn>.Map(playlist, new BeamSearchReturn());
                                                            var listTmp = new List<NewsDistribution>();
                                                            try
                                                            {
                                                                var newsDistribution = await db.SortedSetRangeByScoreWithScoresAsync(_context.NameOf(IndexKeys.KeyNewsDistribution(playlist.Id)), double.MinValue, double.MaxValue, Exclude.None, Order.Ascending, 0, -1, CommandFlags.PreferSlave);
                                                                if (newsDistribution != null)
                                                                {
                                                                    foreach (var item in newsDistribution)
                                                                    {
                                                                        if (long.TryParse(item.Element.ToString()?.Split(IndexKeys.SeparateChar)?.FirstOrDefault(), out long distributorId))
                                                                        {
                                                                            listTmp.Add(new NewsDistribution
                                                                            {
                                                                                DistributorId = distributorId,
                                                                                SharedDate = Utility.ConvertSecondsToDate(item.Score),
                                                                                DistributorName = Json.Parse<Distributor>(await db.HashGetAsync(_context.NameOf<Distributor>(), distributorId, CommandFlags.PreferSlave))?.Name
                                                                            });
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                            catch (Exception exx)
                                                            {
                                                                Logger.Error(exx, exx.Message);
                                                            }
                                                            beamSearchReturn.NewsDistribution = listTmp.ToArray();

                                                            //get ArticleInBeam
                                                            try
                                                            {
                                                                var listKeyArticle = await db.SortedSetRangeByRankAsync(_context.NameOf(IndexKeys.KeyArticleInBeam(beamSearchReturn.Id)), 0, -1, Order.Ascending, CommandFlags.PreferSlave);

                                                                beamSearchReturn.ArticleInBeam = (await db.HashGetAsync(_context.NameOf<ArticleInBeam>(), listKeyArticle?.Select(p => (RedisValue)(beamSearchReturn.Id + IndexKeys.SeparateChar + p))?.ToArray(), CommandFlags.PreferSlave))?.Select(p => Json.Parse<ArticleInBeam>(p))?.ToArray();
                                                            }
                                                            catch (Exception ex)
                                                            {
                                                                Logger.Error(ex, ex.Message);
                                                            }

                                                            result.Data.Add(beamSearchReturn);
                                                        }
                                                    }
                                                }
                                                catch (Exception ex)
                                                {
                                                    Logger.Error(ex, ex.Message);
                                                }
                                                action(tempKey);
                                            }
                                            break;
                                        case false:
                                            if (data.DistributionId == -1)
                                            {
                                                listkeyFull.Add(_context.NameOf(IndexKeys.KeyDistributorid<NewsDistribution>()));
                                            }
                                            else
                                            {
                                                if (data.DistributionId > 0)
                                                {
                                                    listkeyFull.Add(_context.NameOf(IndexKeys.KeyDistributorid<NewsDistribution>(data.DistributionId)));
                                                }
                                            }

                                            var listRedisKeyStatus = new List<RedisKey>();
                                            var listStrStatus = data.Status.Split(",");
                                            foreach (var s in listStrStatus)
                                            {
                                                if (int.TryParse(s, out int status))
                                                {
                                                    if (Enum.IsDefined(typeof(NewsStatus), status))
                                                    {
                                                        listRedisKeyStatus.Add(_context.NameOf(IndexKeys.KeyStatus<NewsAll>(status)));
                                                    }
                                                }
                                            }
                                            var tempKey2 = _context.NameOf("index:temp:" + Generator.SorterSetSearchId());
                                            var values2 = await db.SortedSetCombineAndStoreAsync(SetOperation.Union
                                                    , tempKey2, listRedisKeyStatus.ToArray(), null, Aggregate.Max, CommandFlags.None);
                                            if (values2 > 0)
                                            {
                                                try
                                                {
                                                    listkeyFull.Add(tempKey2);
                                                    var value3 = await db.SortedSetCombineAndStoreAsync(SetOperation.Intersect
                                                    , tempKey, listkeyFull.ToArray(), null, Aggregate.Max, CommandFlags.None);
                                                    if (value3 > 0)
                                                    {
                                                        try
                                                        {
                                                            total = await db.SortedSetLengthAsync(tempKey
                                                            , (data.FromDate ?? DateTime.MinValue).ConvertToTimestamp10()
                                                            , (data.ToDate ?? DateTime.MaxValue).ConvertToTimestamp10(), Exclude.None, CommandFlags.None);
                                                            result.Total = (int)total;

                                                            listKey = (await db.SortedSetRangeByScoreAsync(tempKey
                                                            , (data.FromDate ?? DateTime.MinValue).ConvertToTimestamp10()
                                                            , (data.ToDate ?? DateTime.MaxValue).ConvertToTimestamp10()
                                                            , Exclude.None
                                                            , data.OrderBy == SearchNewsOrderBy.AscCreatedDate ? Order.Ascending : Order.Descending
                                                            , ((data.PageIndex ?? 1) - 1) * (data.PageSize ?? 20)
                                                            , data.PageSize ?? 20, CommandFlags.None));

                                                            listBeam = listKey != null && listKey.Length > 0 ? (await db.HashGetAsync(_context.NameOf<Beam>(), listKey, CommandFlags.PreferSlave))?.Select(p => Json.Parse<Beam>(p))?.ToList() : listBeam;
                                                            if (listBeam != null)
                                                            {
                                                                foreach (var playlist in listBeam)
                                                                {
                                                                    var beamSearchReturn = Mapper<BeamSearchReturn>.Map(playlist, new BeamSearchReturn());
                                                                    var listTmp = new List<NewsDistribution>(); //videoPlaylistSearchReturn.NewsDistribution = new NewsDistribution[] { };
                                                                    try
                                                                    {
                                                                        var newsDistribution = await db.SortedSetRangeByScoreWithScoresAsync(_context.NameOf(IndexKeys.KeyNewsDistribution(playlist.Id)), double.MinValue, double.MaxValue, Exclude.None, Order.Ascending, 0, -1, CommandFlags.PreferSlave);
                                                                        if (newsDistribution != null)
                                                                        {
                                                                            foreach (var item in newsDistribution)
                                                                            {
                                                                                if (long.TryParse(item.Element.ToString()?.Split(IndexKeys.SeparateChar)?.FirstOrDefault(), out long distributorId))
                                                                                {
                                                                                    listTmp.Add(new NewsDistribution
                                                                                    {
                                                                                        DistributorId = distributorId,
                                                                                        SharedDate = Utility.ConvertSecondsToDate(item.Score),
                                                                                        DistributorName = Json.Parse<Distributor>(await db.HashGetAsync(_context.NameOf<Distributor>(), distributorId, CommandFlags.PreferSlave))?.Name
                                                                                    });
                                                                                }
                                                                            }
                                                                        }
                                                                    }
                                                                    catch (Exception exx)
                                                                    {
                                                                        Logger.Error(exx, exx.Message);
                                                                    }
                                                                    beamSearchReturn.NewsDistribution = listTmp.ToArray();

                                                                    //get ArticleInBeam
                                                                    try
                                                                    {
                                                                        var listKeyArticle = await db.SortedSetRangeByRankAsync(_context.NameOf(IndexKeys.KeyArticleInBeam(beamSearchReturn.Id)), 0, -1, Order.Ascending, CommandFlags.PreferSlave);

                                                                        beamSearchReturn.ArticleInBeam = (await db.HashGetAsync(_context.NameOf<ArticleInBeam>(), listKeyArticle?.Select(p => (RedisValue)(beamSearchReturn.Id + IndexKeys.SeparateChar + p))?.ToArray(), CommandFlags.PreferSlave))?.Select(p => Json.Parse<ArticleInBeam>(p))?.ToArray();
                                                                    }
                                                                    catch (Exception ex)
                                                                    {
                                                                        Logger.Error(ex, ex.Message);
                                                                    }

                                                                    result.Data.Add(beamSearchReturn);
                                                                }
                                                            }
                                                        }
                                                        catch (Exception ex)
                                                        {
                                                            Logger.Error(ex, ex.Message);
                                                        }
                                                        action(tempKey);
                                                    }
                                                }
                                                catch (Exception ex)
                                                {
                                                    Logger.Error(ex, ex.Message);
                                                }
                                                action(tempKey2);
                                            }
                                            break;
                                    }
                                }
                                catch (Exception ex)
                                {
                                    Logger.Error(ex, ex.Message);
                                }

                                if (value4 > 0)
                                {
                                    action(tempKeyCreatedBy);
                                }
                            }
                            catch (Exception ex)
                            {
                                Logger.Error(ex, ex.Message);
                            }
                            return result;
                        }
                        else
                        {
                            return new PagingDataResult<BeamSearchReturn>();
                        }
                    });
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
            }
            return returnValue;
        }
        public Task<PagingDataResult<NewsAll>> ArticleInBeamAsync(long beamId, int pageIndex, int pageSize)
        {
            var returnValue = Task.FromResult(default(PagingDataResult<NewsAll>));
            try
            {
                using (var _context = GetContext())
                {
                    returnValue = _context.ExecAsync(async (db) =>
                    {
                        var result = new PagingDataResult<NewsAll>();
                        try
                        {
                            result.Total = (int)await db.SortedSetLengthAsync(_context.NameOf(IndexKeys.KeyArticleInBeam(beamId)), double.MinValue, double.MaxValue, Exclude.None, CommandFlags.PreferSlave);
                            var listValue = await db.SortedSetRangeByScoreWithScoresAsync(_context.NameOf(IndexKeys.KeyArticleInBeam(beamId)),
                                double.MinValue, double.MaxValue, Exclude.None, Order.Ascending, (pageIndex - 1) * pageSize, pageSize, CommandFlags.PreferSlave);
                            result.Data = (await db.HashGetAsync(_context.NameOf<NewsAll>(), listValue?.Select(p => p.Element)?.ToArray()))?.Select(p => Json.Parse<NewsAll>(p))?.ToList();
                        }
                        catch (Exception ex)
                        {
                            Logger.Error(ex, ex.Message);
                        }

                        return result;
                    });
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
            }
            return returnValue;
        }


        public Task<PagingDataResult<BeamSearch>> GetListApproveAsync(SearchNews data, long accountId, Action<string> action)
        {
            var returnValue = Task.FromResult(default(PagingDataResult<BeamSearch>));
            try
            {
                using (var _context = GetContext())
                {
                    returnValue = _context.ExecAsync(async (db) =>
                    {
                        var listMemberId = await db.SortedSetRangeByRankAsync(_context.NameOf(IndexKeys.KeyAccountMemberInOfficer<AccountMember>(data.OfficerId)), 0, -1, Order.Descending, CommandFlags.PreferSlave);
                        var memberValid = listMemberId?.Where(p => p.ToString().Equals(accountId.ToString()))?.Count() > 0;

                        if(memberValid == true)
                        {
                            var result = new PagingDataResult<BeamSearch>()
                            {
                                Data = new List<BeamSearch>()
                            };
                            try
                            {
                                var listKey = new RedisValue[] { };
                                var listBeam = new List<BeamSearch>();
                                var total = default(long);
                                var tempKey = _context.NameOf("index:temp:union:" + Generator.SorterSetSearchId());

                                var listkeyFull = new List<RedisKey>();
                                listkeyFull.Add(_context.NameOf(IndexKeys.KeyStatus<NewsAll>(int.Parse(data.Status))));

                                var keyApprove = new List<RedisKey>();
                                keyApprove.Add(_context.NameOf(IndexKeys.KeyApprove<Beam>(data.OfficerId)));
                                keyApprove.Add(_context.NameOf(IndexKeys.KeyApproveInAccount<Beam>(data.OfficerId, accountId)));

                                var tempKeyApprove = _context.NameOf("index:temp:approve:" + Generator.SorterSetSearchId());

                                var valueApprove = await db.SortedSetCombineAndStoreAsync(SetOperation.Union
                                            , tempKeyApprove, keyApprove.ToArray(), null, Aggregate.Max, CommandFlags.None);

                                if (valueApprove > 0)
                                {
                                    listkeyFull.Add(tempKeyApprove);
                                    var value = await db.SortedSetCombineAndStoreAsync(SetOperation.Intersect
                                            , tempKey, listkeyFull.ToArray(), null, Aggregate.Max, CommandFlags.None);
                                    if (value > 0)
                                    {
                                        try
                                        {
                                            listKey = (await db.SortedSetRangeByScoreAsync(tempKey
                                            , (data.FromDate ?? DateTime.MinValue).ConvertToTimestamp10()
                                            , (data.ToDate ?? DateTime.Now).ConvertToTimestamp10()
                                            , Exclude.None
                                            , data.OrderBy == SearchNewsOrderBy.AscCreatedDate ? Order.Ascending : Order.Descending
                                            , ((data.PageIndex ?? 1) - 1) * (data.PageSize ?? 20)
                                            , data.PageSize ?? 20
                                            , CommandFlags.None));

                                            total = await db.SortedSetLengthAsync(tempKey
                                                , (data.FromDate ?? DateTime.MinValue).ConvertToTimestamp10()
                                                , (data.ToDate ?? DateTime.Now).ConvertToTimestamp10()
                                                , Exclude.None
                                                , CommandFlags.None);

                                            result.Total = (int)total;
                                            listBeam = listKey != null && listKey.Length > 0 ? (await db.HashGetAsync(_context.NameOf(typeof(Beam).Name), listKey, CommandFlags.PreferSlave))?.Select(p => Json.Parse<BeamSearch>(p))?.ToList() : listBeam;
                                            if (listBeam != null)
                                            {
                                                foreach (var beam in listBeam)
                                                {
                                                    //var articleSearchReturn = Mapper<ArticleSearchReturn>.Map(article, new ArticleSearchReturn());
                                                    //var listTmp = new List<NewsDistribution>();
                                                    //try
                                                    //{
                                                    //    var newsDistribution = await db.SortedSetRangeByScoreWithScoresAsync(_context.NameOf(IndexKeys.KeyNewsDistribution(article.Id)), double.MinValue, double.MaxValue, Exclude.None, Order.Ascending, 0, -1, CommandFlags.PreferSlave);
                                                    //    if (newsDistribution != null)
                                                    //    {
                                                    //        foreach (var item in newsDistribution)
                                                    //        {
                                                    //            if (long.TryParse(item.Element.ToString()?.Split(IndexKeys.SeparateChar)?.FirstOrDefault(), out long distributorId))
                                                    //            {
                                                    //                listTmp.Add(new NewsDistribution
                                                    //                {
                                                    //                    DistributorId = distributorId,
                                                    //                    SharedDate = Utility.ConvertSecondsToDate(item.Score),
                                                    //                    DistributorName = Json.Parse<Distributor>(await db.HashGetAsync(_context.NameOf(typeof(Distributor).Name), distributorId, CommandFlags.PreferSlave))?.Name
                                                    //                });
                                                    //            }
                                                    //        }
                                                    //    }
                                                    //}
                                                    //catch (Exception exx)
                                                    //{
                                                    //    Logger.Error(exx, exx.Message);
                                                    //}
                                                    //articleSearchReturn.NewsDistribution = listTmp.ToArray();
                                                    //result.Data.Add(articleSearchReturn);
                                                    //get ArticleInBeam
                                                    try
                                                    {
                                                        var listKeyArticle = await db.SortedSetRangeByRankAsync(_context.NameOf(IndexKeys.KeyArticleInBeam(beam.Id)), 0, -1, Order.Ascending, CommandFlags.PreferSlave);

                                                        beam.ArticleInBeam = (await db.HashGetAsync(_context.NameOf<ArticleInBeam>(), listKeyArticle?.Select(p => (RedisValue)(beam.Id + IndexKeys.SeparateChar + p))?.ToArray(), CommandFlags.PreferSlave))?.Select(p => Json.Parse<ArticleInBeam>(p))?.ToArray();
                                                    }
                                                    catch (Exception ex)
                                                    {
                                                        Logger.Error(ex, ex.Message);
                                                    }
                                                }

                                                result.Data.AddRange(listBeam);
                                            }
                                        }
                                        catch (Exception ex)
                                        {
                                            Logger.Error(ex, ex.Message);
                                        }
                                        action(tempKey);
                                    }
                                    action(tempKeyApprove);
                                }
                            }
                            catch (Exception ex)
                            {
                                Logger.Error(ex, ex.Message);
                            }

                            return result;
                        }
                        else
                        {
                            return new PagingDataResult<BeamSearch>();
                        }
                    });
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
            }
            return returnValue;
        }

        public async Task<RedisValue[]> GetAllAsync()
        {
            var returnValue = default(RedisValue[]);
            try
            {
                using (var _context = GetContext())
                {
                    returnValue = await _context.ExecAsync(async (db) =>
                    {
                        var tempKey = _context.NameOf("index:temp:union:" + Generator.SorterSetSearchId());
                        var value = await db.SortedSetCombineAndStoreAsync(SetOperation.Intersect
                                        , tempKey, _context.NameOf(IndexKeys.KeyStatus<NewsAll>((int)NewsStatus.Published)), _context.NameOf(IndexKeys.KeyType<NewsAll>((int)NewsType.Beam)));

                        returnValue = await db.SortedSetRangeByRankAsync(tempKey, 0, -1);
                        await db.KeyDeleteAsync(tempKey);
                        return returnValue;
                    });
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
            }
            return returnValue;
        }
    }
}
