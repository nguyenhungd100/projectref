﻿using ChannelVN.IMS2.Core.Entities.Album;
using ChannelVN.IMS2.Core.Entities.Beam;
using ChannelVN.IMS2.Core.Entities.Distribution;
using ChannelVN.IMS2.Core.Entities.Gallery;
using ChannelVN.IMS2.Core.Entities.Media;
using ChannelVN.IMS2.Core.Entities.Newsletters;
using ChannelVN.IMS2.Core.Entities.Photo;
using ChannelVN.IMS2.Core.Entities.Posts;
using ChannelVN.IMS2.Core.Entities.ShareLinks;
using ChannelVN.IMS2.Core.Entities.Videos;
using ChannelVN.IMS2.Foundation.Common;
using ChannelVN.IMS2.Foundation.Data.Providers;
using ChannelVN.IMS2.Foundation.Logging;
using StackExchange.Redis;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ChannelVN.IMS2.Core.Models.CacheStore.Distribution
{
    public class DistributionCacheStore : CmsMainCacheStore
    {
        public Task<bool> AddAsync(Distributor data)
        {
            var returnValue = Task.FromResult(false);
            try
            {
                using(var _context = GetContext())
                {
                    returnValue = _context.MultiAsync((transac) =>
                    {
                        transac.HashSetAsync(_context.NameOf<Distributor>(), data.Id, Json.Stringify(data));
                    });
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
            }
            return returnValue;
        }

        public async Task<bool> UpdateAsync(Distributor data)
        {
            var returnValue = false;
            try
            {
                using (var _context = GetContext())
                {
                    var obj = await _context.HashGetExAsync<Distributor>(data.Id);
                    if (obj != null)
                    {
                        obj.Name = data.Name;
                        obj.Description = data.Description;
                        obj.Status = data.Status;
                        obj.Priority = data.Priority;
                        obj.Config.QuotaNormal = data.Config.QuotaNormal;
                        obj.Config.QuotaProfessional = data.Config.QuotaProfessional;
                        obj.Config.UnitTime = data.Config.UnitTime;
                        obj.Config.UnitAmount = data.Config.UnitAmount;
                        obj.Config.ApiZoneVideo = data.Config.ApiZoneVideo;
                        obj.Config.ApiVideo = data.Config.ApiVideo;
                        obj.Code = data.Code;
                        obj.Config.ApiVideoPlaylist = data.Config.ApiVideoPlaylist;
                        obj.Config.ApiMediaUnit = data.Config.ApiMediaUnit;

                        await Task.WhenAll(_context.HashSetExAsync(obj.Id, obj));
                        returnValue = true;
                    }
                    else
                    {
                        await Task.WhenAll(_context.HashSetExAsync(data.Id, data));
                        returnValue = true;
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
            }
            return returnValue;
        }

        public Task<Distributor> GetByIdAsync(long id)
        {
            var returnValue = Task.FromResult(default(Distributor));
            try
            {
                using (var _context = GetContext())
                {
                    returnValue = _context.HashGetExAsync<Distributor>(id, CommandFlags.PreferSlave);
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
            }
            return returnValue;
        }

        public Task<List<Distributor>> GetAllAsync()
        {
            var returnValue = Task.FromResult(default(List<Distributor>));
            try
            {
                using (var _context = GetContext())
                {
                    returnValue = _context.HashGetAllExxAsync<Distributor>();
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
            }
            return returnValue;
        }

        public Task<bool> ShareMediaUnitAsync(ItemStreamDistribution itemStreamDistribution, List<MediaUnitSearch> listMedia)
        {
            var returnValue = Task.FromResult(false);
            try
            {
                var data = new Dictionary<string, MediaUnit>();

                using (var _context = GetContext())
                {
                    returnValue = _context.MultiAsync((transac) =>
                    {
                        foreach (var entry in listMedia)
                        {
                            data[entry.Id.ToString()] = Mapper<MediaUnit>.Map(entry, new MediaUnit());

                            foreach (var newDistribution in entry.NewsDistribution.Where(p => p.ItemStreamId == itemStreamDistribution.Id))
                            {
                                transac.SetAddAsync(_context.NameOf(IndexKeys.KeyDistributorid<NewsDistribution>(newDistribution.DistributorId)), entry.Id);
                                transac.SetAddAsync(_context.NameOf(IndexKeys.KeyDistributorid<NewsDistribution>()), entry.Id);
                                transac.SortedSetAddAsync(_context.NameOf(IndexKeys.KeyNewsDistribution(entry.Id)), newDistribution.DistributorId + IndexKeys.SeparateChar + itemStreamDistribution.Id, (newDistribution.SharedDate ?? DateTime.SpecifyKind(DateTime.Now, DateTimeKind.Unspecified)).ConvertToTimestamp10());
                                transac.SortedSetAddAsync(_context.NameOf(IndexKeys.KeySharedDate<NewsDistribution>()), newDistribution.DistributorId + IndexKeys.SeparateChar +
                                    entry.Id + IndexKeys.SeparateChar + itemStreamDistribution.Id, (newDistribution.SharedDate ?? DateTime.SpecifyKind(DateTime.Now, DateTimeKind.Unspecified)).ConvertToTimestamp10());
                                transac.HashSetAsync(_context.NameOf(newDistribution.GetType().Name), newDistribution.DistributorId + IndexKeys.SeparateChar + newDistribution.NewsId + IndexKeys.SeparateChar + newDistribution.ItemStreamId, Json.Stringify(newDistribution));
                            }
                        }
                        transac.HashSetAsync(_context.NameOf(data.FirstOrDefault().Value.GetType().Name),
                                data.Select(p => new HashEntry(p.Key, Json.Stringify(p.Value))).ToArray());
                        transac.HashSetAsync(_context.NameOf(itemStreamDistribution.GetType().Name), itemStreamDistribution.Id, Json.Stringify(itemStreamDistribution));
                        transac.SortedSetAddAsync(_context.NameOf(IndexKeys.KeyCreatedDate<ItemStreamDistribution>())
                            , itemStreamDistribution.Id
                            , (itemStreamDistribution.CreatedDate ?? DateTime.Now).ConvertToTimestamp10());
                    });
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
            }
            return returnValue;
        }
        public Task<bool> ShareAlbumAsync(ItemStreamDistribution itemStreamDistribution, List<AlbumSearch> listMedia)
        {
            var returnValue = Task.FromResult(false);
            try
            {
                var data = new Dictionary<string, Entities.Album.Album>();

                using (var _context = GetContext())
                {
                    returnValue = _context.MultiAsync((transac) =>
                    {
                        foreach (var entry in listMedia)
                        {
                            data[entry.Id.ToString()] = Mapper<Entities.Album.Album>.Map(entry, new Entities.Album.Album());

                            foreach (var newDistribution in entry.NewsDistribution.Where(p => p.ItemStreamId == itemStreamDistribution.Id))
                            {
                                transac.SetAddAsync(_context.NameOf(IndexKeys.KeyDistributorid<NewsDistribution>(newDistribution.DistributorId)), entry.Id);
                                transac.SetAddAsync(_context.NameOf(IndexKeys.KeyDistributorid<NewsDistribution>()), entry.Id);
                                transac.SortedSetAddAsync(_context.NameOf(IndexKeys.KeyNewsDistribution(entry.Id)), newDistribution.DistributorId + IndexKeys.SeparateChar + itemStreamDistribution.Id, (newDistribution.SharedDate ?? DateTime.SpecifyKind(DateTime.Now, DateTimeKind.Unspecified)).ConvertToTimestamp10());
                                transac.SortedSetAddAsync(_context.NameOf(IndexKeys.KeySharedDate<NewsDistribution>()), newDistribution.DistributorId + IndexKeys.SeparateChar +
                                    entry.Id + IndexKeys.SeparateChar + itemStreamDistribution.Id, (newDistribution.SharedDate ?? DateTime.SpecifyKind(DateTime.Now, DateTimeKind.Unspecified)).ConvertToTimestamp10());
                                transac.HashSetAsync(_context.NameOf(newDistribution.GetType().Name), newDistribution.DistributorId + IndexKeys.SeparateChar + newDistribution.NewsId + IndexKeys.SeparateChar + newDistribution.ItemStreamId, Json.Stringify(newDistribution));
                            }
                        }
                        transac.HashSetAsync(_context.NameOf(data.FirstOrDefault().Value.GetType().Name),
                            data.Select(p => new HashEntry(p.Key, Json.Stringify(p.Value))).ToArray());
                        transac.HashSetAsync(_context.NameOf(itemStreamDistribution.GetType().Name), itemStreamDistribution.Id, Json.Stringify(itemStreamDistribution));
                        transac.SortedSetAddAsync(_context.NameOf(IndexKeys.KeyCreatedDate<ItemStreamDistribution>())
                            , itemStreamDistribution.Id
                            , (itemStreamDistribution.CreatedDate ?? DateTime.Now).ConvertToTimestamp10());
                    });
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
            }
            return returnValue;
        }

        public Task<bool> ShareGalleryAsync(ItemStreamDistribution itemStreamDistribution, List<GallerySearch> listMedia)
        {
            var returnValue = Task.FromResult(false);
            try
            {
                var data = new Dictionary<string, Entities.Gallery.Gallery>();

                using (var _context = GetContext())
                {
                    returnValue = _context.MultiAsync((transac) =>
                    {
                        foreach (var entry in listMedia)
                        {
                            data[entry.Id.ToString()] = Mapper<Entities.Gallery.Gallery>.Map(entry, new Entities.Gallery.Gallery());

                            foreach (var newDistribution in entry.NewsDistribution.Where(p => p.ItemStreamId == itemStreamDistribution.Id))
                            {
                                transac.SetAddAsync(_context.NameOf(IndexKeys.KeyDistributorid<NewsDistribution>(newDistribution.DistributorId)), entry.Id);
                                transac.SetAddAsync(_context.NameOf(IndexKeys.KeyDistributorid<NewsDistribution>()), entry.Id);
                                transac.SortedSetAddAsync(_context.NameOf(IndexKeys.KeyNewsDistribution(entry.Id)), newDistribution.DistributorId + IndexKeys.SeparateChar + itemStreamDistribution.Id, (newDistribution.SharedDate ?? DateTime.SpecifyKind(DateTime.Now, DateTimeKind.Unspecified)).ConvertToTimestamp10());
                                transac.SortedSetAddAsync(_context.NameOf(IndexKeys.KeySharedDate<NewsDistribution>()), newDistribution.DistributorId + IndexKeys.SeparateChar +
                                    entry.Id + IndexKeys.SeparateChar + itemStreamDistribution.Id, (newDistribution.SharedDate ?? DateTime.SpecifyKind(DateTime.Now, DateTimeKind.Unspecified)).ConvertToTimestamp10());
                                transac.HashSetAsync(_context.NameOf(newDistribution.GetType().Name), newDistribution.DistributorId + IndexKeys.SeparateChar + newDistribution.NewsId + IndexKeys.SeparateChar + newDistribution.ItemStreamId, Json.Stringify(newDistribution));
                            }
                        }
                        transac.HashSetAsync(_context.NameOf(data.FirstOrDefault().Value.GetType().Name),
                            data.Select(p => new HashEntry(p.Key, Json.Stringify(p.Value))).ToArray());
                        transac.HashSetAsync(_context.NameOf(itemStreamDistribution.GetType().Name), itemStreamDistribution.Id, Json.Stringify(itemStreamDistribution));
                        transac.SortedSetAddAsync(_context.NameOf(IndexKeys.KeyCreatedDate<ItemStreamDistribution>())
                            , itemStreamDistribution.Id
                            , (itemStreamDistribution.CreatedDate ?? DateTime.Now).ConvertToTimestamp10());
                    });
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
            }
            return returnValue;
        }

        public Task<bool> SharePhotoAsync(ItemStreamDistribution itemStreamDistribution, List<PhotoSearch> listMedia)
        {
            var returnValue = Task.FromResult(false);
            try
            {
                var data = new Dictionary<string, PhotoUnit>();

                using (var _context = GetContext())
                {
                    returnValue = _context.MultiAsync((transac) =>
                    {
                        foreach (var entry in listMedia)
                        {
                            data[entry.Id.ToString()] = Mapper<PhotoUnit>.Map(entry, new PhotoUnit());

                            foreach (var newDistribution in entry.NewsDistribution.Where(p => p.ItemStreamId == itemStreamDistribution.Id))
                            {
                                transac.SetAddAsync(_context.NameOf(IndexKeys.KeyDistributorid<NewsDistribution>(newDistribution.DistributorId)), entry.Id);
                                transac.SetAddAsync(_context.NameOf(IndexKeys.KeyDistributorid<NewsDistribution>()), entry.Id);
                                transac.SortedSetAddAsync(_context.NameOf(IndexKeys.KeyNewsDistribution(entry.Id)), newDistribution.DistributorId + IndexKeys.SeparateChar + itemStreamDistribution.Id, (newDistribution.SharedDate ?? DateTime.SpecifyKind(DateTime.Now, DateTimeKind.Unspecified)).ConvertToTimestamp10());
                                transac.SortedSetAddAsync(_context.NameOf(IndexKeys.KeySharedDate<NewsDistribution>()), newDistribution.DistributorId + IndexKeys.SeparateChar +
                                    entry.Id + IndexKeys.SeparateChar + itemStreamDistribution.Id, (newDistribution.SharedDate ?? DateTime.SpecifyKind(DateTime.Now, DateTimeKind.Unspecified)).ConvertToTimestamp10());
                                transac.HashSetAsync(_context.NameOf(newDistribution.GetType().Name), newDistribution.DistributorId + IndexKeys.SeparateChar + newDistribution.NewsId + IndexKeys.SeparateChar + newDistribution.ItemStreamId, Json.Stringify(newDistribution));
                            }
                        }
                        transac.HashSetAsync(_context.NameOf(data.FirstOrDefault().Value.GetType().Name),
                            data.Select(p => new HashEntry(p.Key, Json.Stringify(p.Value))).ToArray());
                        transac.HashSetAsync(_context.NameOf(itemStreamDistribution.GetType().Name), itemStreamDistribution.Id, Json.Stringify(itemStreamDistribution));
                        transac.SortedSetAddAsync(_context.NameOf(IndexKeys.KeyCreatedDate<ItemStreamDistribution>())
                            , itemStreamDistribution.Id
                            , (itemStreamDistribution.CreatedDate ?? DateTime.Now).ConvertToTimestamp10());
                    });
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
            }
            return returnValue;
        }

        public Task<bool> ShareArticleAsync(ItemStreamDistribution itemStream, List<ArticleSearch> listNews)
        {
            var returnValue = Task.FromResult(false);
            try
            {
                var data = new Dictionary<string, Article>();

                using (var _context = GetContext())
                {
                    returnValue = _context.MultiAsync((transac) =>
                    {
                        foreach (var entry in listNews)
                        {
                            data[entry.Id.ToString()] = Mapper<Article>.Map(entry, new Article());
                            foreach (var newDistribution in entry.NewsDistribution.Where(p => p.ItemStreamId == itemStream.Id))
                            {
                                transac.SetAddAsync(_context.NameOf(IndexKeys.KeyDistributorid<NewsDistribution>(newDistribution.DistributorId)), entry.Id);
                                transac.SetAddAsync(_context.NameOf(IndexKeys.KeyDistributorid<NewsDistribution>()), entry.Id);
                                transac.SortedSetAddAsync(_context.NameOf(IndexKeys.KeyNewsDistribution(entry.Id)), newDistribution.DistributorId + IndexKeys.SeparateChar + itemStream.Id, (newDistribution.SharedDate ?? DateTime.SpecifyKind(DateTime.Now, DateTimeKind.Unspecified)).ConvertToTimestamp10());
                                transac.SortedSetAddAsync(_context.NameOf(IndexKeys.KeySharedDate<NewsDistribution>()), newDistribution.DistributorId + IndexKeys.SeparateChar +
                                    entry.Id + IndexKeys.SeparateChar + itemStream.Id, (newDistribution.SharedDate ?? DateTime.SpecifyKind(DateTime.Now, DateTimeKind.Unspecified)).ConvertToTimestamp10());
                                transac.HashSetAsync(_context.NameOf(newDistribution.GetType().Name), newDistribution.DistributorId + IndexKeys.SeparateChar + newDistribution.NewsId + IndexKeys.SeparateChar + newDistribution.ItemStreamId, Json.Stringify(newDistribution));
                            }
                        }
                        transac.HashSetAsync(_context.NameOf(data.FirstOrDefault().Value.GetType().Name),
                            data.Select(p => new HashEntry(p.Key, Json.Stringify(p.Value))).ToArray());
                        transac.HashSetAsync(_context.NameOf(itemStream.GetType().Name), itemStream.Id, Json.Stringify(itemStream));
                        transac.SortedSetAddAsync(_context.NameOf(IndexKeys.KeyCreatedDate<ItemStreamDistribution>())
                            , itemStream.Id
                            , (itemStream.CreatedDate ?? DateTime.Now).ConvertToTimestamp10());
                    });
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
            }
            return returnValue;
        }

        public async Task<bool> ShareVideoAsync(ItemStreamDistribution itemStream, List<VideoSearch> listVideo, bool isUpdate = false)
        {
            var returnValue = false;
            try
            {
                var data = new Dictionary<string, Video>();

                using (var _context = GetContext())
                {
                    if (isUpdate)
                    {
                        var itemStreamDb = await _context.HashGetExAsync<ItemStreamDistribution>(itemStream.Id);
                        itemStream.CreatedDate = itemStreamDb?.CreatedDate;
                        itemStream.ModifiedDate = DateTime.SpecifyKind(DateTime.Now, DateTimeKind.Unspecified);
                    }

                    returnValue = await _context.MultiAsync((transac) =>
                    {
                        foreach (var entry in listVideo)
                        {
                            data[entry.Id.ToString()] = Mapper<Video>.Map(entry, new Video());
                            if (isUpdate == false)
                            {
                                foreach (var newDistribution in entry.NewsDistribution.Where(p => p.ItemStreamId == itemStream.Id))
                                {
                                    transac.SetAddAsync(_context.NameOf(IndexKeys.KeyDistributorid<NewsDistribution>(newDistribution.DistributorId)), entry.Id);
                                    transac.SetAddAsync(_context.NameOf(IndexKeys.KeyDistributorid<NewsDistribution>()), entry.Id);
                                    transac.SortedSetAddAsync(_context.NameOf(IndexKeys.KeyNewsDistribution(entry.Id)), newDistribution.DistributorId + IndexKeys.SeparateChar + itemStream.Id, (newDistribution.SharedDate ?? DateTime.SpecifyKind(DateTime.Now, DateTimeKind.Unspecified)).ConvertToTimestamp10());
                                    transac.SortedSetAddAsync(_context.NameOf(IndexKeys.KeySharedDate<NewsDistribution>()), newDistribution.DistributorId + IndexKeys.SeparateChar +
                                    entry.Id + IndexKeys.SeparateChar + itemStream.Id, (newDistribution.SharedDate ?? DateTime.SpecifyKind(DateTime.Now, DateTimeKind.Unspecified)).ConvertToTimestamp10());
                                    transac.HashSetAsync(_context.NameOf(newDistribution.GetType().Name), newDistribution.DistributorId + IndexKeys.SeparateChar + newDistribution.NewsId + IndexKeys.SeparateChar + newDistribution.ItemStreamId, Json.Stringify(newDistribution));
                                }
                            }
                        }
                        transac.HashSetAsync(_context.NameOf(data.FirstOrDefault().Value.GetType().Name),
                            data.Select(p => new HashEntry(p.Key, Json.Stringify(p.Value))).ToArray());
                        transac.HashSetAsync(_context.NameOf(itemStream.GetType().Name), itemStream.Id, Json.Stringify(itemStream));
                        transac.SortedSetAddAsync(_context.NameOf(IndexKeys.KeyCreatedDate<ItemStreamDistribution>())
                            , itemStream.Id
                            , (itemStream.CreatedDate ?? DateTime.Now).ConvertToTimestamp10());
                    });
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
            }
            return returnValue;
        }

        public async Task<bool> ShareVideoPlaylistAsync(ItemStreamDistribution itemStream, List<VideoPlaylistSearch> listVideoPlaylist, bool isUpdate = false)
        {
            var returnValue = false;
            try
            {
                var data = new Dictionary<string, VideoPlaylist>();

                using (var _context = GetContext())
                {
                    if (isUpdate)
                    {
                        var itemStreamDB = await _context.HashGetExAsync<ItemStreamDistribution>(itemStream.Id);
                        itemStream.CreatedDate = itemStreamDB?.CreatedDate;
                        itemStream.ModifiedDate = DateTime.SpecifyKind(DateTime.Now, DateTimeKind.Unspecified);
                    }

                    returnValue = await _context.MultiAsync((transac) =>
                    {
                        foreach (var entry in listVideoPlaylist)
                        {
                            data[entry.Id.ToString()] = Mapper<VideoPlaylist>.Map(entry, new VideoPlaylist());
                            if (isUpdate == false)
                            {
                                foreach (var newDistribution in entry.NewsDistribution.Where(p => p.ItemStreamId == itemStream.Id))
                                {
                                    transac.SetAddAsync(_context.NameOf(IndexKeys.KeyDistributorid<NewsDistribution>(newDistribution.DistributorId)), entry.Id);
                                    transac.SetAddAsync(_context.NameOf(IndexKeys.KeyDistributorid<NewsDistribution>()), entry.Id);
                                    transac.SortedSetAddAsync(_context.NameOf(IndexKeys.KeyNewsDistribution(entry.Id)), newDistribution.DistributorId + IndexKeys.SeparateChar + itemStream.Id, (newDistribution.SharedDate ?? DateTime.SpecifyKind(DateTime.Now, DateTimeKind.Unspecified)).ConvertToTimestamp10());
                                    transac.SortedSetAddAsync(_context.NameOf(IndexKeys.KeySharedDate<NewsDistribution>()), newDistribution.DistributorId + IndexKeys.SeparateChar +
                                   entry.Id + IndexKeys.SeparateChar + itemStream.Id, (newDistribution.SharedDate ?? DateTime.SpecifyKind(DateTime.Now, DateTimeKind.Unspecified)).ConvertToTimestamp10());
                                    transac.HashSetAsync(_context.NameOf(newDistribution.GetType().Name), newDistribution.DistributorId + IndexKeys.SeparateChar + newDistribution.NewsId + IndexKeys.SeparateChar + newDistribution.ItemStreamId, Json.Stringify(newDistribution));
                                }
                            }
                        }
                        transac.HashSetAsync(_context.NameOf(data.FirstOrDefault().Value.GetType().Name),
                            data.Select(p => new HashEntry(p.Key, Json.Stringify(p.Value))).ToArray());
                        transac.HashSetAsync(_context.NameOf(itemStream.GetType().Name), itemStream.Id, Json.Stringify(itemStream));
                        transac.SortedSetAddAsync(_context.NameOf(IndexKeys.KeyCreatedDate<ItemStreamDistribution>())
                            , itemStream.Id
                            , (itemStream.CreatedDate ?? DateTime.Now).ConvertToTimestamp10());
                    });
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
            }
            return returnValue;
        }

        public async Task<bool> SharePostAsync(ItemStreamDistribution itemStreamDistribution, List<PostSearch> listPost, bool isUpdate = false)
        {
            var returnValue = false;
            try
            {
                var data = new Dictionary<string, Post>();

                using (var _context = GetContext())
                {
                    if (isUpdate)
                    {
                        var itemStreamDb = await _context.HashGetExAsync<ItemStreamDistribution>(itemStreamDistribution.Id);
                        itemStreamDistribution.CreatedDate = itemStreamDb?.CreatedDate;
                        itemStreamDistribution.ModifiedDate = DateTime.SpecifyKind(DateTime.Now, DateTimeKind.Unspecified);
                    }

                    returnValue = await _context.MultiAsync((transac) =>
                    {
                        foreach (var entry in listPost)
                        {
                            data[entry.Id.ToString()] = Mapper<Post>.Map(entry, new Post());
                            if (isUpdate == false)
                            {
                                foreach (var newDistribution in entry.NewsDistribution.Where(p => p.ItemStreamId == itemStreamDistribution.Id))
                                {
                                    transac.SetAddAsync(_context.NameOf(IndexKeys.KeyDistributorid<NewsDistribution>(newDistribution.DistributorId)), entry.Id);
                                    transac.SetAddAsync(_context.NameOf(IndexKeys.KeyDistributorid<NewsDistribution>()), entry.Id);
                                    transac.SortedSetAddAsync(_context.NameOf(IndexKeys.KeyNewsDistribution(entry.Id)), newDistribution.DistributorId + IndexKeys.SeparateChar + itemStreamDistribution.Id, (newDistribution.SharedDate ?? DateTime.SpecifyKind(DateTime.Now, DateTimeKind.Unspecified)).ConvertToTimestamp10());
                                    transac.SortedSetAddAsync(_context.NameOf(IndexKeys.KeySharedDate<NewsDistribution>()), newDistribution.DistributorId + IndexKeys.SeparateChar +
                                   entry.Id + IndexKeys.SeparateChar + itemStreamDistribution.Id, (newDistribution.SharedDate ?? DateTime.SpecifyKind(DateTime.Now, DateTimeKind.Unspecified)).ConvertToTimestamp10());
                                    transac.HashSetAsync(_context.NameOf(newDistribution.GetType().Name), newDistribution.DistributorId + IndexKeys.SeparateChar + newDistribution.NewsId + IndexKeys.SeparateChar + newDistribution.ItemStreamId, Json.Stringify(newDistribution));
                                }
                            }
                        }
                        transac.HashSetAsync(_context.NameOf(data.FirstOrDefault().Value.GetType().Name),
                            data.Select(p => new HashEntry(p.Key, Json.Stringify(p.Value))).ToArray());
                        transac.HashSetAsync(_context.NameOf(itemStreamDistribution.GetType().Name), itemStreamDistribution.Id, Json.Stringify(itemStreamDistribution));
                        transac.SortedSetAddAsync(_context.NameOf(IndexKeys.KeyCreatedDate<ItemStreamDistribution>())
                            , itemStreamDistribution.Id
                            , (itemStreamDistribution.CreatedDate ?? DateTime.Now).ConvertToTimestamp10());
                    });
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
            }
            return returnValue;
        }
        public async Task<bool> ShareShareLinkAsync(ItemStreamDistribution itemStreamDistribution, List<ShareLinkSearch> listPost, bool isUpdate = false)
        {
            var returnValue = false;
            try
            {
                var data = new Dictionary<string, ShareLink>();

                using (var _context = GetContext())
                {
                    if (isUpdate)
                    {
                        var itemStreamDb = await _context.HashGetExAsync<ItemStreamDistribution>(itemStreamDistribution.Id);
                        itemStreamDistribution.CreatedDate = itemStreamDb?.CreatedDate;
                        itemStreamDistribution.ModifiedDate = DateTime.SpecifyKind(DateTime.Now, DateTimeKind.Unspecified);
                    }

                    returnValue = await _context.MultiAsync((transac) =>
                    {
                        foreach (var entry in listPost)
                        {
                            data[entry.Id.ToString()] = Mapper<ShareLink>.Map(entry, new ShareLink());
                            if (isUpdate == false)
                            {
                                foreach (var newDistribution in entry.NewsDistribution.Where(p => p.ItemStreamId == itemStreamDistribution.Id))
                                {
                                    transac.SetAddAsync(_context.NameOf(IndexKeys.KeyDistributorid<NewsDistribution>(newDistribution.DistributorId)), entry.Id);
                                    transac.SetAddAsync(_context.NameOf(IndexKeys.KeyDistributorid<NewsDistribution>()), entry.Id);
                                    transac.SortedSetAddAsync(_context.NameOf(IndexKeys.KeyNewsDistribution(entry.Id)), newDistribution.DistributorId + IndexKeys.SeparateChar + itemStreamDistribution.Id, (newDistribution.SharedDate ?? DateTime.SpecifyKind(DateTime.Now, DateTimeKind.Unspecified)).ConvertToTimestamp10());
                                    transac.SortedSetAddAsync(_context.NameOf(IndexKeys.KeySharedDate<NewsDistribution>()), newDistribution.DistributorId + IndexKeys.SeparateChar +
                                   entry.Id + IndexKeys.SeparateChar + itemStreamDistribution.Id, (newDistribution.SharedDate ?? DateTime.SpecifyKind(DateTime.Now, DateTimeKind.Unspecified)).ConvertToTimestamp10());
                                    transac.HashSetAsync(_context.NameOf(newDistribution.GetType().Name), newDistribution.DistributorId + IndexKeys.SeparateChar + newDistribution.NewsId + IndexKeys.SeparateChar + newDistribution.ItemStreamId, Json.Stringify(newDistribution));
                                }
                            }
                        }

                        transac.HashSetAsync(_context.NameOf(data.FirstOrDefault().Value.GetType().Name),
                            data.Select(p => new HashEntry(p.Key, Json.Stringify(p.Value))).ToArray());
                        transac.HashSetAsync(_context.NameOf(itemStreamDistribution.GetType().Name), itemStreamDistribution.Id, Json.Stringify(itemStreamDistribution));
                        transac.SortedSetAddAsync(_context.NameOf(IndexKeys.KeyCreatedDate<ItemStreamDistribution>())
                            , itemStreamDistribution.Id
                            , (itemStreamDistribution.CreatedDate ?? DateTime.Now).ConvertToTimestamp10());
                    });
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
            }
            return returnValue;
        }

        public async Task<bool> ShareBeamAsync(ItemStreamDistribution itemStream, List<BeamSearch> listBeam, bool isUpdate = false)
        {
            var returnValue = false;
            try
            {
                var data = new Dictionary<string, Beam>();

                using (var _context = GetContext())
                {
                    if (isUpdate)
                    {
                        var itemStreamDB = await _context.HashGetExAsync<ItemStreamDistribution>(itemStream.Id);
                        itemStream.CreatedDate = itemStreamDB?.CreatedDate;
                        itemStream.ModifiedDate = DateTime.SpecifyKind(DateTime.Now, DateTimeKind.Unspecified);
                    }

                    returnValue = await _context.MultiAsync((transac) =>
                    {
                        foreach (var entry in listBeam)
                        {
                            data[entry.Id.ToString()] = Mapper<Beam>.Map(entry, new Beam());
                            if (isUpdate == false)
                            {
                                foreach (var newDistribution in entry.NewsDistribution.Where(p => p.ItemStreamId == itemStream.Id))
                                {
                                    transac.SetAddAsync(_context.NameOf(IndexKeys.KeyDistributorid<NewsDistribution>(newDistribution.DistributorId)), entry.Id);
                                    transac.SetAddAsync(_context.NameOf(IndexKeys.KeyDistributorid<NewsDistribution>()), entry.Id);
                                    transac.SortedSetAddAsync(_context.NameOf(IndexKeys.KeyNewsDistribution(entry.Id)), newDistribution.DistributorId + IndexKeys.SeparateChar + itemStream.Id, (newDistribution.SharedDate ?? DateTime.SpecifyKind(DateTime.Now, DateTimeKind.Unspecified)).ConvertToTimestamp10());
                                    transac.SortedSetAddAsync(_context.NameOf(IndexKeys.KeySharedDate<NewsDistribution>()), newDistribution.DistributorId + IndexKeys.SeparateChar +
                                   entry.Id + IndexKeys.SeparateChar + itemStream.Id, (newDistribution.SharedDate ?? DateTime.SpecifyKind(DateTime.Now, DateTimeKind.Unspecified)).ConvertToTimestamp10());
                                    transac.HashSetAsync(_context.NameOf(newDistribution.GetType().Name), newDistribution.DistributorId + IndexKeys.SeparateChar + newDistribution.NewsId + IndexKeys.SeparateChar + newDistribution.ItemStreamId, Json.Stringify(newDistribution));
                                }
                            }
                        }
                        transac.HashSetAsync(_context.NameOf(data.FirstOrDefault().Value.GetType().Name),
                            data.Select(p => new HashEntry(p.Key, Json.Stringify(p.Value))).ToArray());
                        transac.HashSetAsync(_context.NameOf(itemStream.GetType().Name), itemStream.Id, Json.Stringify(itemStream));
                        transac.SortedSetAddAsync(_context.NameOf(IndexKeys.KeyCreatedDate<ItemStreamDistribution>())
                            , itemStream.Id
                            , (itemStream.CreatedDate ?? DateTime.Now).ConvertToTimestamp10());
                    });
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
            }
            return returnValue;
        }
    }
}
