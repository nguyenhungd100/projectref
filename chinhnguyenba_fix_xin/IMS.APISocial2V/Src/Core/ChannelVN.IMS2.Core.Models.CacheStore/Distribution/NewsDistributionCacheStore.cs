﻿using ChannelVN.IMS2.Core.Entities.Distribution;
using ChannelVN.IMS2.Foundation.Common;
using ChannelVN.IMS2.Foundation.Data.Providers;
using ChannelVN.IMS2.Foundation.Logging;
using StackExchange.Redis;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ChannelVN.IMS2.Core.Models.CacheStore.Distribution
{
    public class NewsDistributionCacheStore : CmsMainCacheStore
    {
        public Task<List<NewsDistribution>> GetNewsDistributionByNewsIdAsync(long newsId)
        {
            var returnValue = Task.FromResult(default(List<NewsDistribution>));
            try
            {
                using(var _context = GetContext())
                {
                    returnValue = _context.ExecAsync(async (db) =>
                    {
                        var newDistributionIds = (await db.SetMembersAsync(_context.NameOf(IndexKeys.KeyNewsDistribution(newsId)), CommandFlags.PreferSlave))
                            ?.Select(p => long.Parse(p.ToString()))?.ToList();

                        var videoSearchReturn = new List<NewsDistribution>();
                        if (newDistributionIds != null)
                        {
                            foreach (var id in newDistributionIds)
                            {
                                videoSearchReturn.Add(new NewsDistribution()
                                {
                                    DistributorId = id,
                                    DistributorName = Json.Parse<Distributor>(await db.HashGetAsync(_context.NameOf<Distributor>(), id, CommandFlags.PreferSlave))?.Name
                                });
                            }
                        }
                        return videoSearchReturn;
                    });

                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
            }
            return returnValue;
        }
        public Task<List<NewsDistribution>> GetAllNewsDistributionAsync()
        {
            var returnValue = Task.FromResult(default(List<NewsDistribution>));
            using (var _context = GetContext())
            {
                returnValue = _context.HashGetAllExxAsync<NewsDistribution>();
            }
            return returnValue;
        }
    }
}
