﻿using ChannelVN.IMS2.Core.Entities;
using ChannelVN.IMS2.Core.Entities.Distribution;
using ChannelVN.IMS2.Core.Entities.ErrorCode;
using ChannelVN.IMS2.Core.Entities.Newsletters;
using ChannelVN.IMS2.Core.Entities.Security;
using ChannelVN.IMS2.Foundation.Common;
using ChannelVN.IMS2.Foundation.Data.Providers;
using ChannelVN.IMS2.Foundation.Logging;
using StackExchange.Redis;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using static ChannelVN.IMS2.Core.Entities.ErrorCode.ErrorMapping;

namespace ChannelVN.IMS2.Core.Models.CacheStore.Articles
{
    public class ArticleCacheStore : CmsMainCacheStore
    {
        public async Task<ErrorCodes> AddAsync(ArticleSearch dataSearch)
        {
            var returnValue = ErrorCodes.AddError;
            try
            {
                var data = Mapper<Article>.Map(dataSearch, new Article());
                var newsAll = Mapper<NewsAll>.MapNewsAll(dataSearch, new NewsAll());
                newsAll.Avatar = dataSearch.Avatar1;

                using (var _context = GetContext())
                {
                    //Kiểm tra quyền viết bài
                    var isWriter = await Functions.Function.IsWriter(dataSearch?.NewsInAccount?.FirstOrDefault().AccountId, data.CreatedBy, _context);

                    if (isWriter)
                    {
                        var resultAdd = await _context.MultiAsync((transac) =>
                        {
                            transac.HashSetAsync(_context.NameOf(data.GetType().Name), data.Id, Json.Stringify(data));

                            transac.HashSetAsync(_context.NameOf(newsAll.GetType().Name), newsAll.Id, Json.Stringify(newsAll));
                            transac.SortedSetAddAsync(_context.NameOf(IndexKeys.KeyCreatedBy<NewsAll>(newsAll.CreatedBy)), newsAll.Id, (newsAll.CreatedDate ?? DateTime.Now).ConvertToTimestamp10());
                            transac.SortedSetAddAsync(_context.NameOf(IndexKeys.KeyStatus<NewsAll>(newsAll.Status)), newsAll.Id, (newsAll.CreatedDate ?? DateTime.Now).ConvertToTimestamp10());
                            transac.SortedSetAddAsync(_context.NameOf(IndexKeys.KeyDistributorid<NewsAll>(newsAll.DistributorId)), newsAll.Id, (newsAll.CreatedDate ?? DateTime.Now).ConvertToTimestamp10());
                            transac.SortedSetAddAsync(_context.NameOf(IndexKeys.KeyType<NewsAll>(newsAll.Type ?? 2)), newsAll.Id, (newsAll.CreatedDate ?? DateTime.Now).ConvertToTimestamp10());

                            transac.HashSetAsync(_context.NameOf(typeof(NewsInAccount).Name), dataSearch.NewsInAccount?.Select(p => new HashEntry(p.AccountId + IndexKeys.SeparateChar + p.NewsId, Json.Stringify(p))).ToArray());
                            transac.SetAddAsync(_context.NameOf(IndexKeys.KeyAccountInNews(dataSearch.Id)), dataSearch.NewsInAccount?.LastOrDefault()?.AccountId ?? 0);
                            transac.SetAddAsync(_context.NameOf(IndexKeys.KeyNewsInAccount(dataSearch.NewsInAccount?.LastOrDefault()?.AccountId)), dataSearch.Id);
                            transac.SortedSetAddAsync(_context.NameOf(IndexKeys.KeyPublishedDate<NewsInAccount>())
                                , (dataSearch.NewsInAccount?.LastOrDefault()?.AccountId ?? 0) + IndexKeys.SeparateChar + dataSearch.Id
                                , (dataSearch.CreatedDate ?? DateTime.Now).ConvertToTimestamp10());
                        });
                        if (resultAdd) returnValue = ErrorCodes.Success;
                    }
                    else
                    {
                        returnValue = ErrorCodes.PermissionInvalid;
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
                returnValue = ErrorCodes.Exception;
            }
            return returnValue;
        }

        public async Task<ErrorCodes> UpdateAsync(Article data, long officerId = 0)
        {
            var returnValue = ErrorCodes.BusinessError;
            try
            {
                using (var _context = GetContext())
                {
                    var approverOld = string.Empty;
                    var dataDb = await _context.HashGetExAsync<Article>(data.Id);

                    if(dataDb != null)
                    {
                        var isModifier = await Functions.Function.IsModifier(officerId, data.Id, data.ModifiedBy, dataDb.CreatedBy, dataDb.Status, _context);

                        if (isModifier)
                        {
                            approverOld = dataDb.ApprovedBy;

                            dataDb.Url = data.Url;
                            dataDb.OriginalUrl = data.OriginalUrl;
                            dataDb.Author = data.Author;
                            dataDb.CategoryId = data.CategoryId;
                            dataDb.Location = data.Location;
                            dataDb.PublishMode = data.PublishMode;
                            dataDb.PublishData = data.PublishData;
                            dataDb.DistributionDate = data.DistributionDate.HasValue && data.DistributionDate >= DateTime.Now ? data.DistributionDate : dataDb.DistributionDate;
                            dataDb.ModifiedDate = data.ModifiedDate;
                            dataDb.ModifiedBy = data.ModifiedBy;
                            dataDb.Tags = data.Tags;
                            dataDb.Title = data.Title;
                            dataDb.Sapo = data.Sapo;
                            dataDb.SubTitle = data.SubTitle;
                            dataDb.Avatar1 = data.Avatar1;
                            dataDb.Avatar2 = data.Avatar2;
                            dataDb.Avatar3 = data.Avatar3;
                            dataDb.ParentId = data.ParentId;
                            dataDb.TemplateId = data.TemplateId;
                            dataDb.Body = data.Body;
                            dataDb.BodyMeta = data.BodyMeta;
                            if (dataDb.Status != (int)NewsStatus.Published)
                            {
                                dataDb.ApprovedBy = data.ApprovedBy;
                            }
                            dataDb.InstantViewId = data.InstantViewId;
                            dataDb.IsInstantView = data.IsInstantView;
                            dataDb.InstantViewUrl = data.InstantViewUrl;
                            dataDb.NativeContent = data.NativeContent;
                            dataDb.CommentMode = data.CommentMode;

                            var newsAll = Mapper<NewsAll>.MapNewsAll(dataDb, new NewsAll());
                            newsAll.Avatar = data.Avatar1;

                            var resultUpdate = await _context.MultiAsync((transac) =>
                            {
                                transac.HashSetAsync(_context.NameOf(data.GetType().Name), dataDb.Id, Json.Stringify(dataDb));
                                transac.HashSetAsync(_context.NameOf(newsAll.GetType().Name), newsAll.Id, Json.Stringify(newsAll));

                                if (dataDb.Status == (int)NewsStatus.Approve)
                                {
                                    if (long.TryParse(approverOld, out long _approverOld))
                                        transac.SortedSetRemoveAsync(_context.NameOf(IndexKeys.KeyApproveInAccount<Article>(officerId, _approverOld)), dataDb.Id);

                                    if (!string.IsNullOrEmpty(dataDb.ApprovedBy))
                                    {
                                        if (long.TryParse(dataDb.ApprovedBy, out long accountId))
                                        {
                                            transac.SortedSetAddAsync(_context.NameOf(IndexKeys.KeyApproveInAccount<Article>(officerId, accountId)), dataDb.Id, DateTime.Now.ConvertToTimestamp10());
                                        }
                                    }
                                }
                            });
                            if (resultUpdate) returnValue = ErrorCodes.Success;
                        }
                        else
                        {
                            returnValue = ErrorCodes.PermissionUpdateInvalid;
                        }
                    }
                    else
                    {
                        returnValue = ErrorCodes.DataNotExist;
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
                returnValue = ErrorCodes.Exception;
            }
            return returnValue;
        }

        public async Task<ErrorCodes> UpdateV2Async(Article data, long officerId, Action<int> actionAutoShare)
        {
            var returnValue = ErrorCodes.BusinessError;
            var statusOld = default(int?);
            try
            {
                using (var _context = GetContext())
                {
                    var approverOld = string.Empty;
                    var dataDb = await _context.HashGetExAsync<Article>(data.Id);

                    if (dataDb != null)
                    {
                        statusOld = dataDb.Status;

                        var isModifier = await Functions.Function.IsModifier(officerId, data.Id, data.ModifiedBy, dataDb.CreatedBy, dataDb.Status, _context);

                        if (isModifier)
                        {
                            approverOld = dataDb.ApprovedBy;

                            dataDb.Url = data.Url;
                            dataDb.OriginalUrl = data.OriginalUrl;
                            dataDb.Author = data.Author;
                            dataDb.CategoryId = data.CategoryId;
                            dataDb.Location = data.Location;
                            dataDb.PublishMode = data.PublishMode;
                            dataDb.PublishData = data.PublishData;
                            dataDb.DistributionDate = data.DistributionDate.HasValue && data.DistributionDate >= DateTime.Now ? data.DistributionDate : dataDb.DistributionDate;
                            dataDb.ModifiedDate = data.ModifiedDate;
                            dataDb.ModifiedBy = data.ModifiedBy;
                            dataDb.Tags = data.Tags;
                            dataDb.Title = data.Title;
                            dataDb.Sapo = data.Sapo;
                            dataDb.SubTitle = data.SubTitle;
                            dataDb.Avatar1 = data.Avatar1;
                            dataDb.Avatar2 = data.Avatar2;
                            dataDb.Avatar3 = data.Avatar3;
                            dataDb.ParentId = data.ParentId;
                            dataDb.TemplateId = data.TemplateId;
                            dataDb.Body = data.Body;
                            dataDb.BodyMeta = data.BodyMeta;
                            if (dataDb.Status != (int)NewsStatus.Published)
                            {
                                dataDb.ApprovedBy = data.ApprovedBy;
                            }
                            dataDb.InstantViewId = data.InstantViewId;
                            dataDb.IsInstantView = data.IsInstantView;
                            dataDb.InstantViewUrl = data.InstantViewUrl;
                            dataDb.NativeContent = data.NativeContent;
                            dataDb.CommentMode = data.CommentMode;

                            var newsAll = Mapper<NewsAll>.MapNewsAll(dataDb, new NewsAll());
                            newsAll.Avatar = data.Avatar1;

                            var resultUpdate = await _context.MultiAsync((transac) =>
                            {
                                transac.HashSetAsync(_context.NameOf(data.GetType().Name), dataDb.Id, Json.Stringify(dataDb));
                                transac.HashSetAsync(_context.NameOf(newsAll.GetType().Name), newsAll.Id, Json.Stringify(newsAll));

                                if (dataDb.Status == (int)NewsStatus.Approve)
                                {
                                    if (long.TryParse(approverOld, out long _approverOld))
                                        transac.SortedSetRemoveAsync(_context.NameOf(IndexKeys.KeyApproveInAccount<Article>(officerId, _approverOld)), dataDb.Id);

                                    if (!string.IsNullOrEmpty(dataDb.ApprovedBy))
                                    {
                                        if (long.TryParse(dataDb.ApprovedBy, out long accountId))
                                        {
                                            transac.SortedSetAddAsync(_context.NameOf(IndexKeys.KeyApproveInAccount<Article>(officerId, accountId)), dataDb.Id, DateTime.Now.ConvertToTimestamp10());
                                        }
                                    }
                                }
                            });
                            if (resultUpdate) returnValue = ErrorCodes.Success;
                        }
                        else
                        {
                            returnValue = ErrorCodes.PermissionUpdateInvalid;
                        }
                    }
                    else
                    {
                        returnValue = ErrorCodes.DataNotExist;
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
                returnValue = ErrorCodes.Exception;
            }
            if (returnValue == ErrorCodes.Success && statusOld != null)
            {
                actionAutoShare(statusOld ?? 0);
            }
            return returnValue;
        }

        public Task<bool> UpdateStatusAsync(ArticleSearch dataSearch, int statusOld)
        {
            var returnValue = Task.FromResult(false);
            try
            {
                var data = Mapper<Article>.Map(dataSearch, new Article());
                
                if (data != null)
                {
                    var newsAll = Mapper<NewsAll>.MapNewsAll(data, new NewsAll());
                    newsAll.Avatar = data.Avatar1;

                    using (var _context = GetContext())
                    {
                        returnValue = _context.MultiAsync((transac) =>
                        {
                            transac.HashSetAsync(_context.NameOf(data.GetType().Name), data.Id, Json.Stringify(data));

                            transac.HashSetAsync(_context.NameOf(newsAll.GetType().Name), newsAll.Id, Json.Stringify(newsAll));
                            transac.SortedSetRemoveAsync(_context.NameOf(IndexKeys.KeyStatus<NewsAll>(statusOld)), newsAll.Id);
                            transac.SortedSetAddAsync(_context.NameOf(IndexKeys.KeyStatus<NewsAll>(newsAll.Status)), newsAll.Id, (newsAll.CreatedDate ?? DateTime.Now).ConvertToTimestamp10());

                            // các index approve
                            if (statusOld == (int)NewsStatus.Published && data.Status != (int)NewsStatus.Published)
                            {
                                if (long.TryParse(data.ApprovedBy, out long accountId))
                                {
                                    transac.SortedSetRemoveAsync(_context.NameOf(IndexKeys.KeyApprovedInAccount<Article>(dataSearch?.NewsInAccount?.FirstOrDefault()?.AccountId, accountId)), dataSearch.Id);
                                }
                            }

                            if (data.Status == (int)NewsStatus.Approve)
                            {
                                if (string.IsNullOrEmpty(data.ApprovedBy))
                                {
                                    transac.SortedSetAddAsync(_context.NameOf(IndexKeys.KeyApprove<Article>(dataSearch?.NewsInAccount?.FirstOrDefault()?.AccountId)), dataSearch.Id, DateTime.Now.ConvertToTimestamp10());
                                }
                                else
                                {
                                    if (long.TryParse(data.ApprovedBy, out long accountId))
                                    {
                                        transac.SortedSetAddAsync(_context.NameOf(IndexKeys.KeyApproveInAccount<Article>(dataSearch?.NewsInAccount?.FirstOrDefault()?.AccountId, accountId)), dataSearch.Id, DateTime.Now.ConvertToTimestamp10());
                                    }
                                }
                            }
                            else
                            {
                                transac.SortedSetRemoveAsync(_context.NameOf(IndexKeys.KeyApprove<Article>(dataSearch?.NewsInAccount?.FirstOrDefault()?.AccountId)), dataSearch.Id);

                                if (long.TryParse(data.ApprovedBy, out long accountId))
                                {
                                    transac.SortedSetRemoveAsync(_context.NameOf(IndexKeys.KeyApproveInAccount<Article>(dataSearch?.NewsInAccount?.FirstOrDefault()?.AccountId, accountId)), dataSearch.Id);
                                }
                                if (data.Status == (int)NewsStatus.Published)
                                {
                                    transac.SortedSetAddAsync(_context.NameOf(IndexKeys.KeyApprovedInAccount<Article>(dataSearch?.NewsInAccount?.FirstOrDefault()?.AccountId, accountId)), dataSearch.Id, DateTime.Now.ConvertToTimestamp10());
                                }
                            }
                        });
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
            }
            return returnValue;
        }
       
        public async Task<ErrorCodes> UpdateStatusV2Async(long id, NewsStatus status, long userId, Action<ArticleSearch, int> actionAutoShare)
        {
            var returnValue = ErrorCodes.UpdateError;
            var dataDb = default(Article);
            var statusOld = default(int?);
            try
            {
                using (var _context = GetContext())
                {
                    var newsAll = default(NewsAll);
                    var resultUpdate = false;
                    var officerIds = default(RedisValue[]);
                    var officerId = 0L;
                    dataDb = await _context.HashGetExAsync<Article>(id, CommandFlags.PreferSlave);
                    statusOld = dataDb.Status;

                    if (dataDb != null)
                    {
                        if (dataDb.Status ==  (int)status)
                        {
                            returnValue = ErrorCodes.Success;
                        }
                        else
                        {
                            switch (status)
                            {
                                #region Xuat ban
                                case NewsStatus.Published:// xuất bản
                                    officerIds = await _context.SetMembersAsync(IndexKeys.KeyAccountInNews(id), CommandFlags.PreferSlave);
                                    if (officerIds != null && officerIds.Count() != 0)
                                    {
                                        if (long.TryParse(officerIds[0], out officerId))
                                        {
                                            var officer = await _context.HashGetExAsync<Account>(officerId, CommandFlags.PreferSlave);
                                            
                                            var listMemberId = await _context.SortedSetRangeByScoreAsync(IndexKeys.KeyAccountMemberInOfficer<AccountMember>(officerId), double.NegativeInfinity, double.PositiveInfinity);

                                            var listMember = await _context.GetsFromHashExAsync<AccountMember>(listMemberId?.Select(p => officerId + IndexKeys.SeparateChar + p.ToString())?.ToList(), CommandFlags.PreferSlave);
                                            var accountMember = listMember?.Where(p => p.MemberId == userId).FirstOrDefault();

                                            if (accountMember != null && officer != null)
                                            {
                                                var isPublisher = await Functions.Function.IsPublisher(accountMember, dataDb.CreatedBy, dataDb.ApprovedBy, officer.Mode, dataDb.Status);
                                                if (isPublisher)
                                                {
                                                    dataDb.Status = (int)status;
                                                    dataDb.ModifiedDate = DateTime.Now;
                                                    dataDb.ModifiedBy = userId.ToString();
                                                    dataDb.DistributionDate = dataDb.DistributionDate > DateTime.Now ? dataDb.DistributionDate : DateTime.Now;

                                                    var oldApprovedBy = dataDb.ApprovedBy;
                                                    dataDb.ApprovedBy = userId.ToString();
                                                    dataDb.ApprovedDate = DateTime.Now;

                                                    newsAll = Mapper<NewsAll>.MapNewsAll(dataDb, new NewsAll());

                                                    resultUpdate = await _context.MultiAsync((transac) =>
                                                    {
                                                        transac.HashSetAsync(_context.NameOf(dataDb.GetType().Name), dataDb.Id, Json.Stringify(dataDb));
                                                        transac.HashSetAsync(_context.NameOf(newsAll.GetType().Name), newsAll.Id, Json.Stringify(newsAll));

                                                        transac.SortedSetRemoveAsync(_context.NameOf(IndexKeys.KeyStatus<NewsAll>(statusOld)), newsAll.Id);
                                                        transac.SortedSetAddAsync(_context.NameOf(IndexKeys.KeyStatus<NewsAll>(newsAll.Status)), newsAll.Id, (newsAll.DistributionDate ?? DateTime.Now).ConvertToTimestamp10());

                                                        //remove value in set approve in account if exist
                                                        if (!string.IsNullOrEmpty(oldApprovedBy) && long.TryParse(oldApprovedBy, out long oldApprovedById))
                                                        {
                                                            transac.SortedSetRemoveAsync(_context.NameOf(IndexKeys.KeyApproveInAccount<Article>(officerId, oldApprovedById)), dataDb.Id, CommandFlags.PreferSlave);
                                                        }
                                                        else
                                                        {
                                                            //remove value in set approve if exist
                                                            transac.SortedSetRemoveAsync(_context.NameOf(IndexKeys.KeyApprove<Article>(officerId)), dataDb.Id, CommandFlags.PreferSlave);
                                                        }

                                                        //add value into key approved in account
                                                        transac.SortedSetAddAsync(_context.NameOf(IndexKeys.KeyApprovedInAccount<Article>(officerId, userId)), dataDb.Id, DateTime.Now.ConvertToTimestamp10());
                                                    });

                                                    if (resultUpdate) returnValue = ErrorCodes.Success;
                                                }
                                                else
                                                {
                                                    returnValue = ErrorCodes.PermissionPublishInvalid;
                                                }
                                            }
                                            else
                                            {
                                                returnValue = ErrorCodes.PermissionPublishInvalid;
                                            }
                                        }
                                        else
                                        {
                                            returnValue = ErrorCodes.DataInvalid;
                                        }
                                    }
                                    else
                                    {
                                        returnValue = ErrorCodes.DataInvalid;
                                    }
                                    break;
                                #endregion
                                #region Xoa bai
                                case NewsStatus.Remove: // xóa bài
                                    if(statusOld!= (int)NewsStatus.Published)
                                    {
                                        if (userId.ToString().Equals(dataDb.CreatedBy))
                                        {
                                            dataDb.Status = (int)status;
                                            dataDb.ModifiedDate = DateTime.Now;
                                            dataDb.ModifiedBy = userId.ToString();

                                            newsAll = Mapper<NewsAll>.MapNewsAll(dataDb, new NewsAll());

                                            resultUpdate = await _context.MultiAsync((transac) =>
                                            {
                                                transac.HashSetAsync(_context.NameOf(dataDb.GetType().Name), dataDb.Id, Json.Stringify(dataDb));

                                                transac.HashSetAsync(_context.NameOf(newsAll.GetType().Name), newsAll.Id, Json.Stringify(newsAll));

                                                transac.SortedSetRemoveAsync(_context.NameOf(IndexKeys.KeyStatus<NewsAll>(statusOld)), newsAll.Id);
                                                transac.SortedSetAddAsync(_context.NameOf(IndexKeys.KeyStatus<NewsAll>(newsAll.Status)), newsAll.Id, (dataDb.ModifiedDate ?? DateTime.Now).ConvertToTimestamp10());
                                            });

                                            if (resultUpdate) returnValue = ErrorCodes.Success;
                                        }
                                        else
                                        {
                                            returnValue = ErrorCodes.PermissionUpdateInvalid;
                                        }
                                    }
                                    else
                                    {
                                        // trạng thái cũ là publish
                                        officerIds = await _context.SetMembersAsync(IndexKeys.KeyAccountInNews(id), CommandFlags.PreferSlave);
                                        if (officerIds != null && officerIds.Count() != 0)
                                        {
                                            if (long.TryParse(officerIds[0], out officerId))
                                            {
                                                var officer = await _context.HashGetExAsync<Account>(officerId, CommandFlags.PreferSlave);

                                                var listMemberId = await _context.SortedSetRangeByScoreAsync(IndexKeys.KeyAccountMemberInOfficer<AccountMember>(officerId), double.NegativeInfinity, double.PositiveInfinity);

                                                var listMember = await _context.GetsFromHashExAsync<AccountMember>(listMemberId?.Select(p => officerId + IndexKeys.SeparateChar + p.ToString())?.ToList(), CommandFlags.PreferSlave);
                                                var accountMember = listMember?.Where(p => p.MemberId == userId).FirstOrDefault();

                                                if(accountMember!=null && officer != null)
                                                {
                                                    var isRemover = await Functions.Function.IsRemover(accountMember, dataDb.CreatedBy, dataDb.ApprovedBy, officer.Mode, statusOld);
                                                    if (isRemover)
                                                    {
                                                        dataDb.Status = (int)status;
                                                        dataDb.ModifiedDate = DateTime.Now;
                                                        dataDb.ModifiedBy = userId.ToString();

                                                        newsAll = Mapper<NewsAll>.MapNewsAll(dataDb, new NewsAll());

                                                        resultUpdate = await _context.MultiAsync((transac) =>
                                                        {
                                                            transac.HashSetAsync(_context.NameOf(dataDb.GetType().Name), dataDb.Id, Json.Stringify(dataDb));

                                                            transac.HashSetAsync(_context.NameOf(newsAll.GetType().Name), newsAll.Id, Json.Stringify(newsAll));

                                                            transac.SortedSetRemoveAsync(_context.NameOf(IndexKeys.KeyStatus<NewsAll>(statusOld)), newsAll.Id);
                                                            transac.SortedSetAddAsync(_context.NameOf(IndexKeys.KeyStatus<NewsAll>(newsAll.Status)), newsAll.Id, (dataDb.ModifiedDate ?? DateTime.Now).ConvertToTimestamp10());

                                                            if (long.TryParse(dataDb.ApprovedBy, out long accountId))
                                                            {
                                                                transac.SortedSetRemoveAsync(_context.NameOf(IndexKeys.KeyApprovedInAccount<Article>(officerId, accountId)), dataDb.Id);
                                                            }
                                                        });
                                                        if (resultUpdate) returnValue = ErrorCodes.Success;
                                                    }
                                                    else
                                                    {
                                                        returnValue = ErrorCodes.PermissionUpdateInvalid;
                                                    }
                                                }
                                                else
                                                {
                                                    returnValue = ErrorCodes.PermissionUpdateInvalid;
                                                }
                                            }
                                            else
                                            {
                                                returnValue = ErrorCodes.DataInvalid;
                                            }
                                        }
                                        else
                                        {
                                            returnValue = ErrorCodes.DataInvalid;
                                        }
                                    }

                                    break;
                                #endregion
                                #region Gui cho duyet
                                case NewsStatus.Approve:// Gửi nhờ duyệt
                                    if(dataDb.Status == (int)NewsStatus.Published)
                                    {
                                        returnValue = ErrorCodes.DataInvalid;
                                    }
                                    else
                                    {
                                        if (userId.ToString().Equals(dataDb.CreatedBy))
                                        {
                                            officerIds = await _context.SetMembersAsync(IndexKeys.KeyAccountInNews(id), CommandFlags.PreferSlave);
                                            if (officerIds != null && officerIds.Count() != 0)
                                            {
                                                if (long.TryParse(officerIds[0], out officerId))
                                                {
                                                    dataDb.Status = (int)status;
                                                    dataDb.ModifiedDate = DateTime.Now;
                                                    dataDb.ModifiedBy = userId.ToString();

                                                    newsAll = Mapper<NewsAll>.MapNewsAll(dataDb, new NewsAll());

                                                    resultUpdate = await _context.MultiAsync((transac) =>
                                                    {
                                                        transac.HashSetAsync(_context.NameOf(dataDb.GetType().Name), dataDb.Id, Json.Stringify(dataDb));

                                                        transac.HashSetAsync(_context.NameOf(newsAll.GetType().Name), newsAll.Id, Json.Stringify(newsAll));

                                                        transac.SortedSetRemoveAsync(_context.NameOf(IndexKeys.KeyStatus<NewsAll>(statusOld)), newsAll.Id);
                                                        transac.SortedSetAddAsync(_context.NameOf(IndexKeys.KeyStatus<NewsAll>(newsAll.Status)), newsAll.Id, (dataDb.ModifiedDate ?? DateTime.Now).ConvertToTimestamp10());

                                                        if (string.IsNullOrEmpty(dataDb.ApprovedBy))
                                                        {
                                                            transac.SortedSetAddAsync(_context.NameOf(IndexKeys.KeyApprove<Article>(officerId)), dataDb.Id, DateTime.Now.ConvertToTimestamp10());
                                                        }
                                                        else
                                                        {
                                                            if (long.TryParse(dataDb.ApprovedBy, out long accountId))
                                                            {
                                                                transac.SortedSetAddAsync(_context.NameOf(IndexKeys.KeyApproveInAccount<Article>(officerId, accountId)), dataDb.Id, DateTime.Now.ConvertToTimestamp10());
                                                            }
                                                        }
                                                    });
                                                }
                                            }

                                            if (resultUpdate) returnValue = ErrorCodes.Success;
                                        }
                                        else
                                        {
                                            returnValue = ErrorCodes.PermissionUpdateInvalid;
                                        }
                                    }
                                    break;
                                #endregion
                                #region Default
                                default:
                                    if (userId.ToString().Equals(dataDb.CreatedBy))
                                    {
                                        dataDb.Status = (int)status;
                                        dataDb.ModifiedDate = DateTime.Now;
                                        dataDb.ModifiedBy = userId.ToString();

                                        if (statusOld == (int)NewsStatus.Published)
                                        {
                                            officerIds = await _context.SetMembersAsync(IndexKeys.KeyAccountInNews(id), CommandFlags.PreferSlave);
                                            if (officerIds != null && officerIds.Count() != 0)
                                            {
                                                long.TryParse(officerIds[0], out officerId);
                                            }
                                        }

                                        newsAll = Mapper<NewsAll>.MapNewsAll(dataDb, new NewsAll());

                                        resultUpdate = await _context.MultiAsync((transac) =>
                                        {
                                            transac.HashSetAsync(_context.NameOf(dataDb.GetType().Name), dataDb.Id, Json.Stringify(dataDb));

                                            transac.HashSetAsync(_context.NameOf(newsAll.GetType().Name), newsAll.Id, Json.Stringify(newsAll));

                                            transac.SortedSetRemoveAsync(_context.NameOf(IndexKeys.KeyStatus<NewsAll>(statusOld)), newsAll.Id);
                                            transac.SortedSetAddAsync(_context.NameOf(IndexKeys.KeyStatus<NewsAll>(newsAll.Status)), newsAll.Id, (dataDb.ModifiedDate ?? DateTime.Now).ConvertToTimestamp10());

                                            if (officerId > 0)
                                            {
                                                if (long.TryParse(dataDb.ApprovedBy, out long accountId))
                                                {
                                                    transac.SortedSetRemoveAsync(_context.NameOf(IndexKeys.KeyApprovedInAccount<Article>(officerId, accountId)), dataDb.Id);
                                                }
                                            }
                                        });

                                        if (resultUpdate) returnValue = ErrorCodes.Success;
                                    }
                                    else
                                    {
                                        if(statusOld == (int?)NewsStatus.Published || statusOld == (int?)NewsStatus.Archive || statusOld == (int?)NewsStatus.Approve)
                                        {
                                            officerIds = await _context.SetMembersAsync(IndexKeys.KeyAccountInNews(id), CommandFlags.PreferSlave);
                                            if (officerIds != null && officerIds.Count() != 0)
                                            {
                                                if (long.TryParse(officerIds[0], out officerId))
                                                {
                                                    var officer = await _context.HashGetExAsync<Account>(officerId, CommandFlags.PreferSlave);

                                                    var listMemberId = await _context.SortedSetRangeByScoreAsync(IndexKeys.KeyAccountMemberInOfficer<AccountMember>(officerId), double.NegativeInfinity, double.PositiveInfinity);

                                                    var listMember = await _context.GetsFromHashExAsync<AccountMember>(listMemberId?.Select(p => officerId + IndexKeys.SeparateChar + p.ToString())?.ToList(), CommandFlags.PreferSlave);
                                                    var accountMember = listMember?.Where(p => p.MemberId == userId).FirstOrDefault();

                                                    if(accountMember!=null && officer != null)
                                                    {
                                                        var isRemover = await Functions.Function.IsRemover(accountMember, dataDb.CreatedBy, dataDb.ApprovedBy, officer.Mode, statusOld);
                                                        if (isRemover)
                                                        {
                                                            dataDb.Status = (int)status;
                                                            dataDb.ModifiedDate = DateTime.Now;
                                                            dataDb.ModifiedBy = userId.ToString();

                                                            newsAll = Mapper<NewsAll>.MapNewsAll(dataDb, new NewsAll());

                                                            resultUpdate = await _context.MultiAsync((transac) =>
                                                            {
                                                                transac.HashSetAsync(_context.NameOf(dataDb.GetType().Name), dataDb.Id, Json.Stringify(dataDb));

                                                                transac.HashSetAsync(_context.NameOf(newsAll.GetType().Name), newsAll.Id, Json.Stringify(newsAll));

                                                                transac.SortedSetRemoveAsync(_context.NameOf(IndexKeys.KeyStatus<NewsAll>(statusOld)), newsAll.Id);
                                                                transac.SortedSetAddAsync(_context.NameOf(IndexKeys.KeyStatus<NewsAll>(newsAll.Status)), newsAll.Id, (dataDb.ModifiedDate ?? DateTime.Now).ConvertToTimestamp10());

                                                                if (officerId > 0)
                                                                {
                                                                    if (long.TryParse(dataDb.ApprovedBy, out long accountId))
                                                                    {
                                                                        transac.SortedSetRemoveAsync(_context.NameOf(IndexKeys.KeyApprovedInAccount<Article>(officerId, accountId)), dataDb.Id);
                                                                    }
                                                                }
                                                            });

                                                            if (resultUpdate) returnValue = ErrorCodes.Success;
                                                        }
                                                        else
                                                        {
                                                            returnValue = ErrorCodes.PermissionUpdateInvalid;
                                                        }
                                                    }
                                                    else
                                                    {
                                                        returnValue = ErrorCodes.PermissionUpdateInvalid;
                                                    }
                                                }
                                                else
                                                {
                                                    returnValue = ErrorCodes.DataInvalid;
                                                }
                                            }
                                            else
                                            {
                                                returnValue = ErrorCodes.DataInvalid;
                                            }
                                        }
                                        else
                                        {
                                            returnValue = ErrorCodes.PermissionUpdateInvalid;
                                        }
                                    }

                                    break;
                                    #endregion
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
            }

            if(returnValue == ErrorCodes.Success && dataDb !=null && statusOld != null)
            {
                actionAutoShare(Mapper<ArticleSearch>.Map(dataDb, new ArticleSearch()), statusOld ?? 0);
            }

            return returnValue;
        }

        public Task<Article> GetByIdAsync(long id)
        {
            var returnValue = Task.FromResult(default(Article));
            try
            {
                using (var _context = GetContext())
                {
                    returnValue = _context.HashGetExAsync<Article>(id, CommandFlags.PreferSlave);
                }
            }
            catch(Exception ex)
            {
                Logger.Error(ex, ex.Message);
            }
            return returnValue;
        }

        public Task<ArticleSearch> GetArticleFullInfoAsync(long newsId)
        {
            var returnValue = Task.FromResult(default(ArticleSearch));
            try
            {
                using (var _context = GetContext())
                {
                    returnValue = _context.ExecAsync(async (db) =>
                    {
                        try
                        {
                            var photo = Json.Parse<Article>(await db.HashGetAsync(_context.NameOf(typeof(Article).Name), newsId, CommandFlags.PreferSlave));
                            if (photo != null)
                            {
                                var result = Mapper<ArticleSearch>.Map(photo, new ArticleSearch());
                                if (result != null)
                                {
                                    var listAccountId = await db.SetMembersAsync(_context.NameOf(IndexKeys.KeyAccountInNews(newsId)), CommandFlags.PreferSlave);
                                    result.NewsInAccount = (await db.HashGetAsync(_context.NameOf<NewsInAccount>(), listAccountId?.Select(p => (RedisValue)(p + IndexKeys.SeparateChar + newsId))?.ToArray(), CommandFlags.PreferSlave))
                                    ?.Select(p => Json.Parse<NewsInAccount>(p))?.ToArray();

                                    var listDistributorIdAndItemStreamId = await db.SortedSetRangeByScoreWithScoresAsync(_context.NameOf(IndexKeys.KeyNewsDistribution(newsId)), double.MinValue, double.MaxValue, Exclude.None, Order.Ascending, 0, -1, CommandFlags.PreferSlave);

                                    var listKeyNewsDistribution = listDistributorIdAndItemStreamId?.Select(p => (RedisValue)(p.Element.ToString().Split(IndexKeys.SeparateChar)?.FirstOrDefault() +
                                    IndexKeys.SeparateChar + newsId + IndexKeys.SeparateChar + p.Element.ToString()?.Split(IndexKeys.SeparateChar)?.LastOrDefault()));
                                    result.NewsDistribution = (await db.HashGetAsync(_context.NameOf(typeof(NewsDistribution).Name), listKeyNewsDistribution?.ToArray(), CommandFlags.PreferSlave))?.Select(p => Json.Parse<NewsDistribution>(p))?.ToArray();
                                }
                                return result;
                            }
                        }
                        catch (Exception ex)
                        {
                            Logger.Error(ex, ex.Message);
                        }
                        return null;
                    });
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
            }
            return returnValue;
        }
        public Task<ItemStreamDistribution> GetItemStreamByIdAsync(long id)
        {
            var returnValue = Task.FromResult(default(ItemStreamDistribution));
            try
            {
                using (var _context = GetContext())
                {
                    returnValue = _context.HashGetExAsync<ItemStreamDistribution>(id, CommandFlags.PreferSlave);
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
            }
            return returnValue;
        }

        public Task<bool> AcceptAsync(ItemStreamDistribution data)
        {
            var returnValue = Task.FromResult(false);
            try
            {
                using (var _context = GetContext())
                {
                    returnValue = _context.MultiAsync((transac) =>
                    {
                        transac.HashSetAsync(_context.NameOf<ItemStreamDistribution>(), data.Id, Json.Stringify(data));
                    });
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
            }
            return returnValue;
        }

        public Task<bool> RejectAsync(ItemStreamDistribution data)
        {
            var returnValue = Task.FromResult(false);
            try
            {
                using (var _context = GetContext())
                {
                    returnValue = _context.MultiAsync((transac) =>
                    {
                        transac.HashSetAsync(_context.NameOf<ItemStreamDistribution>(), data.Id, Json.Stringify(data));
                    });
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
            }
            return returnValue;
        }

        public Task<ArticleDetail> GetDetailByIdAsync(long id)
        {
            var returnValue = Task.FromResult(default(ArticleDetail));
            try
            {
                using (var _context = GetContext())
                {
                    returnValue = _context.ExecAsync(async (db) =>
                    {
                        var result = default(ArticleDetail);
                        try
                        {
                            var article = Json.Parse<Article>(await db.HashGetAsync(_context.NameOf(typeof(Article).Name), id, CommandFlags.PreferSlave));
                            if (article != null)
                            {
                                result = new ArticleDetail();
                                result.News = Mapper<ArticleSearch>.Map(article, new ArticleSearch());
                                if (result.News != null && !string.IsNullOrEmpty(result.News.Tags))
                                {
                                    result.NewsInTag = (await db.HashGetAsync(_context.NameOf(typeof(Tag).Name), result.News.Tags.Split(",")?.Select(p => (RedisValue)p).ToArray(), CommandFlags.PreferSlave))?.Select(p => Json.Parse<Tag>(p))?.ToList();
                                }
                                long.TryParse(result.News.CreatedBy, out long createdID);
                                result.AuthorAvatar = Json.Parse<Account>((await db.HashGetAsync(_context.NameOf(typeof(Account).Name), createdID, CommandFlags.PreferSlave)))?.Avatar;
                            }
                        }
                        catch (Exception ex)
                        {
                            Logger.Error(ex, ex.Message);
                        }
                        return result;
                    });
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
            }
            return returnValue;
        }
        public Task<List<Article>> GetListByIdsAsync(List<string> ids)
        {
            var returnValue = Task.FromResult(default(List<Article>));
            try
            {
                using (var _context = GetContext())
                {
                    returnValue = _context.GetsFromHashExAsync<Article>(ids, CommandFlags.PreferSlave);
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
            }
            return returnValue;
        }
        public Task<List<ArticleSearch>> GetListArticleSearchByIdsAsync(List<string> ids, PagingDataResult<ArticleSearch> listData)
        {
            var returnValue = Task.FromResult(default(List<ArticleSearch>));
            try
            {
                using (var _context = GetContext())
                {
                    returnValue = _context.ExecAsync(async (db) =>
                    {
                        var result = default(List<ArticleSearch>);
                        try
                        {
                            var listValue = (await db.HashGetAsync(_context.NameOf(typeof(Article).Name), ids?.Select(p => (RedisValue)p).ToArray(), CommandFlags.PreferSlave))
                        ?.Select(p => Json.Parse<Article>(p))?.ToList();
                            if (listValue != null)
                            {
                                result = new List<ArticleSearch>();
                                foreach (var article in listValue)
                                {
                                    try
                                    {
                                        var articleSearch = Mapper<ArticleSearch>.Map(article, new ArticleSearch());
                                        articleSearch.NewsDistribution = listData.Data?.Where(p => p.Id == article.Id)?.FirstOrDefault()?.NewsDistribution;
                                        if (articleSearch.NewsDistribution != null)
                                        {
                                            foreach (var item in articleSearch.NewsDistribution)
                                            {
                                                item.DistributorName = Json.Parse<Distributor>(await db.HashGetAsync(_context.NameOf(typeof(Distributor).Name), item.DistributorId, CommandFlags.PreferSlave))?.Name;
                                            }
                                        }
                                        result.Add(articleSearch);
                                    }
                                    catch (Exception ex)
                                    {
                                        Logger.Error(ex, ex.Message);
                                    }
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            Logger.Error(ex, ex.Message);
                        }
                        return result;
                    });
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
            }
            return returnValue;
        }
        public Task<List<ItemStreamDistribution>> GetListItemStreamByIdsAsync(List<string> ids)
        {
            var returnValue = Task.FromResult(default(List<ItemStreamDistribution>));
            try
            {
                using (var _context = GetContext())
                {
                    returnValue = GetContext().GetsFromHashExAsync<ItemStreamDistribution>(ids, CommandFlags.PreferSlave);
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
            }
            return returnValue;
        }


        public Task<PagingDataResult<ArticleOnWall>> ListNewsByAuthorAsync(long accountId, long distributionId, int pageIndex, int pageSize, Action<string> action)
        {
            var returnValue = Task.FromResult(default(PagingDataResult<ArticleOnWall>));
            try
            {
                using (var _context = GetContext())
                {
                    returnValue = _context.ExecAsync(async (db) =>
                    {
                        var result = new PagingDataResult<ArticleOnWall>()
                        {
                            Data = new List<ArticleOnWall>()
                        };
                        try
                        {
                            var listKey = new RedisValue[] { };
                            var listArticle = new List<Article>();
                            var total = default(long);

                            var createdByKey = _context.NameOf(IndexKeys.KeyCreatedBy<Article>(accountId.ToString()));
                            var tempDBName = _context.NameOf("index:temp:union:" + Generator.SorterSetSearchId());
                            var values = await db.SortedSetCombineAndStoreAsync(SetOperation.Intersect
                                        , tempDBName, createdByKey, IndexKeys.KeyStatus<Article>((int)NewsStatus.Published), Aggregate.Max, CommandFlags.None);

                            values = await db.SortedSetCombineAndStoreAsync(SetOperation.Intersect
                                        , tempDBName, tempDBName, _context.NameOf(IndexKeys.KeyDistributorid<Article>(distributionId)), Aggregate.Max, CommandFlags.None);
                            if (values > 0)
                            {
                                try
                                {
                                    total = await db.SortedSetLengthAsync(tempDBName
                                    , DateTime.MinValue.ConvertToTimestamp10()
                                    , DateTime.Now.ConvertToTimestamp10(), Exclude.None, CommandFlags.None
                                );
                                    result.Total = (int)total;
                                    listKey = (await db.SortedSetRangeByScoreAsync(tempDBName
                                       , DateTime.MinValue.ConvertToTimestamp10()
                                       , DateTime.Now.ConvertToTimestamp10()
                                       , Exclude.None
                                       , Order.Descending
                                       , (pageIndex - 1) * pageSize
                                       , pageSize, CommandFlags.None));

                                    listArticle = (await db.HashGetAsync(_context.NameOf(typeof(Article).Name), listKey, CommandFlags.PreferSlave))
                                    ?.Select(p => Json.Parse<Article>(p))?.ToList();
                                    if (listArticle != null)
                                    {
                                        result.Data = listArticle.Select(p => Mapper<ArticleOnWall>.Map(p, new ArticleOnWall()))?.ToList();
                                    }
                                }
                                catch (Exception ex)
                                {
                                    Logger.Error(ex, ex.Message);
                                }
                                action(tempDBName);
                            }
                        }
                        catch (Exception ex)
                        {
                            Logger.Error(ex, ex.Message);
                        }
                        return result;
                    });
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
            }
            return returnValue;
        }


        public Task<PagingDataResult<ArticleSearchReturn>> SearchAsync(SearchNews data, long userId, Action<string> action)
        {
            var returnValue = Task.FromResult(default(PagingDataResult<ArticleSearchReturn>));
            try
            {
                using (var _context = GetContext())
                {
                    returnValue = _context.ExecAsync(async (db) =>
                    {
                        //var listMemberId = await db.SortedSetRangeByRankAsync(_context.NameOf(IndexKeys.KeyAccountMemberInOfficer<AccountMember>(data.OfficerId)), 0, -1, Order.Descending, CommandFlags.PreferSlave);
                        //var memberValid = listMemberId?.Where(p => p.ToString().Equals(userId.ToString()))?.Count() > 0;
                        var member = Json.Parse<AccountMember>(await db.HashGetAsync(_context.NameOf<AccountMember>(), data.OfficerId + IndexKeys.SeparateChar + userId, CommandFlags.PreferSlave));
                        if (member != null)
                        {
                            var result = new PagingDataResult<ArticleSearchReturn>()
                            {
                                Data = new List<ArticleSearchReturn>()
                            };
                            try
                            {
                                var listKey = new RedisValue[] { };
                                var listArticle = new List<Article>();
                                var total = 0L;
                                var tempKey = _context.NameOf("index:temp:union:" + Generator.SorterSetSearchId());
                                var tempKeyCreatedBy = _context.NameOf("index:temp:createdby:" + Generator.SorterSetSearchId());
                                var createdByKey = _context.NameOf(IndexKeys.KeyCreatedBy<NewsAll>(userId.ToString()));
                                var listkeyFull = new List<RedisKey>();
                                var listCreatedBy = new List<RedisKey>();
                                var value4 = 0L;

                                if (data.Status.Equals(((int)NewsStatus.Published).ToString()) && member.Permissions != null && (member.Permissions.Contains(MemberPermission.ModifyPostOnPage) || member.Permissions.Contains(MemberPermission.RemovePostOnPage)))
                                {

                                }
                                else
                                {
                                    var listOwner = await db.SortedSetRangeByRankAsync(_context.NameOf(IndexKeys.KeyOwnerOfficer(userId)), 0, -1, Order.Descending, CommandFlags.PreferSlave);
                                    if (listOwner == null || !listOwner.Contains((RedisValue)data.OfficerId))
                                    {
                                        listCreatedBy.Add(createdByKey);

                                        //Lấy cả những bài chỉ duyệt
                                        if (data.Status?.Split(",")?.Contains(((int)NewsStatus.Published).ToString()) ?? false)
                                        {
                                            listCreatedBy.Add(_context.NameOf(IndexKeys.KeyApprovedInAccount<Article>(data.OfficerId, userId)));
                                        }

                                        value4 = await db.SortedSetCombineAndStoreAsync(SetOperation.Union
                                        , tempKeyCreatedBy, listCreatedBy.ToArray(), null, Aggregate.Max, CommandFlags.None);

                                        if (value4 > 0)
                                        {
                                            listkeyFull.Add(tempKeyCreatedBy);
                                        }
                                    }
                                    else
                                    {
                                        if ((data.Status?.Split(",")?.Contains(((int)NewsStatus.Approve).ToString()) ?? false) || (data.Status?.Split(",")?.Contains(((int)NewsStatus.Draft).ToString()) ?? false) || (data.Status?.Split(",")?.Contains(((int)NewsStatus.Remove).ToString()) ?? false))
                                        {
                                            listkeyFull.Add(createdByKey);
                                        }
                                    }
                                }

                                try
                                {
                                    listkeyFull.Add(_context.NameOf(IndexKeys.KeyNewsInAccount(data.OfficerId)));
                                    listkeyFull.Add(_context.NameOf(IndexKeys.KeyType<NewsAll>((int)NewsType.Article)));
                                    switch (string.IsNullOrEmpty(data.Status))
                                    {
                                        case true:
                                            if (data.DistributionId == -1)
                                            {
                                                listkeyFull.Add(_context.NameOf(IndexKeys.KeyDistributorid<NewsDistribution>()));
                                            }
                                            else
                                            {
                                                if (data.DistributionId > 0)
                                                {
                                                    listkeyFull.Add(_context.NameOf(IndexKeys.KeyDistributorid<NewsDistribution>(data.DistributionId)));
                                                }
                                            }
                                            var value1 = await db.SortedSetCombineAndStoreAsync(SetOperation.Intersect
                                                        , tempKey, listkeyFull.ToArray(), null, Aggregate.Max, CommandFlags.None);
                                            if (value1 > 0)
                                            {
                                                try
                                                {
                                                    listKey = (await db.SortedSetRangeByScoreAsync(tempKey
                                                    , (data.FromDate ?? DateTime.MinValue).ConvertToTimestamp10()
                                                    , (data.ToDate ?? DateTime.Now).ConvertToTimestamp10()
                                                    , Exclude.None
                                                    , data.OrderBy == SearchNewsOrderBy.AscCreatedDate ? Order.Ascending : Order.Descending
                                                    , ((data.PageIndex ?? 1) - 1) * (data.PageSize ?? 20)
                                                    , data.PageSize ?? 20
                                                    , CommandFlags.None));

                                                    total = await db.SortedSetLengthAsync(tempKey
                                                        , (data.FromDate ?? DateTime.MinValue).ConvertToTimestamp10()
                                                        , (data.ToDate ?? DateTime.Now).ConvertToTimestamp10()
                                                        , Exclude.None
                                                        , CommandFlags.None);

                                                    result.Total = (int)total;
                                                    listArticle = listKey != null && listKey.Length > 0 ? (await db.HashGetAsync(_context.NameOf(typeof(Article).Name), listKey, CommandFlags.PreferSlave))?.Select(p => Json.Parse<Article>(p))?.ToList() : listArticle;
                                                    if (listArticle != null)
                                                    {
                                                        foreach (var article in listArticle)
                                                        {
                                                            var articleSearchReturn = Mapper<ArticleSearchReturn>.Map(article, new ArticleSearchReturn());
                                                            var listTmp = new List<NewsDistribution>();
                                                            try
                                                            {
                                                                var newsDistribution = await db.SortedSetRangeByScoreWithScoresAsync(_context.NameOf(IndexKeys.KeyNewsDistribution(article.Id)), double.MinValue, double.MaxValue, Exclude.None, Order.Ascending, 0, -1, CommandFlags.PreferSlave);
                                                                if (newsDistribution != null)
                                                                {
                                                                    foreach (var item in newsDistribution)
                                                                    {
                                                                        if (long.TryParse(item.Element.ToString()?.Split(IndexKeys.SeparateChar)?.FirstOrDefault(), out long distributorId))
                                                                        {
                                                                            listTmp.Add(new NewsDistribution
                                                                            {
                                                                                DistributorId = distributorId,
                                                                                SharedDate = Utility.ConvertSecondsToDate(item.Score),
                                                                                DistributorName = Json.Parse<Distributor>(await db.HashGetAsync(_context.NameOf(typeof(Distributor).Name), distributorId, CommandFlags.PreferSlave))?.Name
                                                                            });
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                            catch (Exception exx)
                                                            {
                                                                Logger.Error(exx, exx.Message);
                                                            }
                                                            articleSearchReturn.NewsDistribution = listTmp.ToArray();
                                                            result.Data.Add(articleSearchReturn);
                                                        }
                                                    }
                                                }
                                                catch (Exception ex)
                                                {
                                                    Logger.Error(ex, ex.Message);
                                                }
                                                action(tempKey);
                                            }
                                            break;
                                        case false:
                                            if (data.DistributionId == -1)
                                            {
                                                listkeyFull.Add(_context.NameOf(IndexKeys.KeyDistributorid<NewsDistribution>()));
                                            }
                                            else
                                            {
                                                if (data.DistributionId > 0)
                                                {
                                                    listkeyFull.Add(_context.NameOf(IndexKeys.KeyDistributorid<NewsDistribution>(data.DistributionId)));
                                                }
                                            }

                                            var listRedisKeyStatus = new List<RedisKey>();
                                            var listStrStatus = data.Status.Split(",");
                                            foreach (var s in listStrStatus)
                                            {
                                                if (int.TryParse(s, out int status))
                                                {
                                                    if (Enum.IsDefined(typeof(NewsStatus), status))
                                                    {
                                                        listRedisKeyStatus.Add(_context.NameOf(IndexKeys.KeyStatus<NewsAll>(status)));
                                                    }
                                                }
                                            }
                                            var tempKey2 = _context.NameOf("index:temp:status:" + Generator.SorterSetSearchId());
                                            var value2 = await db.SortedSetCombineAndStoreAsync(SetOperation.Union
                                                    , tempKey2, listRedisKeyStatus.ToArray(), null, Aggregate.Max, CommandFlags.None);
                                            if (value2 > 0)
                                            {
                                                try
                                                {
                                                    listkeyFull.Add(tempKey2);
                                                    var value3 = await db.SortedSetCombineAndStoreAsync(SetOperation.Intersect
                                                        , tempKey, listkeyFull.ToArray(), null, Aggregate.Max, CommandFlags.None);
                                                    if (value3 > 0)
                                                    {
                                                        try
                                                        {
                                                            total = await db.SortedSetLengthAsync(tempKey
                                                            , (data.FromDate ?? DateTime.MinValue).ConvertToTimestamp10()
                                                            , (data.ToDate ?? DateTime.Now).ConvertToTimestamp10()
                                                            , Exclude.None
                                                            , CommandFlags.None);

                                                            result.Total = (int)total;

                                                            listKey = (await db.SortedSetRangeByScoreAsync(tempKey
                                                            , (data.FromDate ?? DateTime.MinValue).ConvertToTimestamp10()
                                                            , (data.ToDate ?? DateTime.Now).ConvertToTimestamp10()
                                                            , Exclude.None
                                                            , data.OrderBy == SearchNewsOrderBy.AscCreatedDate ? Order.Ascending : Order.Descending
                                                            , ((data.PageIndex ?? 1) - 1) * (data.PageSize ?? 20)
                                                            , data.PageSize ?? 20
                                                            , CommandFlags.None));

                                                            listArticle = listKey != null && listKey.Length > 0 ? (await db.HashGetAsync(_context.NameOf(typeof(Article).Name), listKey, CommandFlags.PreferSlave))?.Select(p => Json.Parse<Article>(p))?.ToList() : listArticle;
                                                            if (listArticle != null)
                                                            {
                                                                foreach (var article in listArticle)
                                                                {
                                                                    var articleSearchReturn = Mapper<ArticleSearchReturn>.Map(article, new ArticleSearchReturn());
                                                                    var listTmp = new List<NewsDistribution>();
                                                                    try
                                                                    {
                                                                        var newsDistribution = await db.SortedSetRangeByScoreWithScoresAsync(_context.NameOf(IndexKeys.KeyNewsDistribution(article.Id)), double.MinValue, double.MaxValue, Exclude.None, Order.Ascending, 0, -1, CommandFlags.PreferSlave);
                                                                        if (newsDistribution != null)
                                                                        {
                                                                            foreach (var item in newsDistribution)
                                                                            {
                                                                                if (long.TryParse(item.Element.ToString()?.Split(IndexKeys.SeparateChar)?.FirstOrDefault(), out long distributorId))
                                                                                {
                                                                                    listTmp.Add(new NewsDistribution
                                                                                    {
                                                                                        DistributorId = distributorId,
                                                                                        SharedDate = Utility.ConvertSecondsToDate(item.Score),
                                                                                        DistributorName = Json.Parse<Distributor>(await db.HashGetAsync(_context.NameOf(typeof(Distributor).Name), distributorId, CommandFlags.PreferSlave))?.Name
                                                                                    });
                                                                                }
                                                                            }
                                                                        }
                                                                    }
                                                                    catch (Exception ex)
                                                                    {
                                                                        Logger.Error(ex, ex.Message);
                                                                    }
                                                                    articleSearchReturn.NewsDistribution = listTmp.ToArray();
                                                                    result.Data.Add(articleSearchReturn);
                                                                }
                                                            }
                                                        }
                                                        catch (Exception ex)
                                                        {
                                                            Logger.Error(ex, ex.Message);
                                                        }
                                                        action(tempKey);
                                                    }
                                                }
                                                catch (Exception ex)
                                                {
                                                    Logger.Error(ex, ex.Message);
                                                }
                                                action(tempKey2);
                                            }
                                            break;
                                    }
                                }
                                catch (Exception ex)
                                {
                                    Logger.Error(ex, ex.Message);
                                }
                                if (value4 > 0)
                                {
                                    action(tempKeyCreatedBy);
                                }
                            }
                            catch (Exception ex)
                            {
                                Logger.Error(ex, ex.Message);
                            }

                            return result;
                        }
                        else
                        {
                            return new PagingDataResult<ArticleSearchReturn>();
                        }
                    });
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
            }
            return returnValue;
        }

        public Task<PagingDataResult<ArticleSearchReturn>> GetListApproveAsync(SearchNews data, long accountId, Action<string> action)
        {
            var returnValue = Task.FromResult(default(PagingDataResult<ArticleSearchReturn>));
            try
            {
                using(var _context = GetContext())
                {
                    returnValue = _context.ExecAsync(async (db) =>
                    {
                        var listMemberId = await db.SortedSetRangeByRankAsync(_context.NameOf(IndexKeys.KeyAccountMemberInOfficer<AccountMember>(data.OfficerId)), 0, -1, Order.Descending, CommandFlags.PreferSlave);
                        var memberValid = listMemberId?.Where(p => p.ToString().Equals(accountId.ToString()))?.Count() > 0;

                        if(memberValid == true)
                        {
                            var result = new PagingDataResult<ArticleSearchReturn>()
                            {
                                Data = new List<ArticleSearchReturn>()
                            };
                            try
                            {
                                var listKey = new RedisValue[] { };
                                var listArticle = new List<Article>();
                                var total = default(long);
                                var tempKey = _context.NameOf("index:temp:union:" + Generator.SorterSetSearchId());

                                var listkeyFull = new List<RedisKey>();
                                listkeyFull.Add(_context.NameOf(IndexKeys.KeyStatus<NewsAll>(int.Parse(data.Status))));

                                var keyApprove = new List<RedisKey>();
                                keyApprove.Add(_context.NameOf(IndexKeys.KeyApprove<Article>(data.OfficerId)));
                                keyApprove.Add(_context.NameOf(IndexKeys.KeyApproveInAccount<Article>(data.OfficerId, accountId)));

                                var tempKeyApprove = _context.NameOf("index:temp:approve:" + Generator.SorterSetSearchId());

                                var valueApprove = await db.SortedSetCombineAndStoreAsync(SetOperation.Union
                                            , tempKeyApprove, keyApprove.ToArray(), null, Aggregate.Max, CommandFlags.None);

                                if (valueApprove > 0)
                                {
                                    listkeyFull.Add(tempKeyApprove);
                                    var value = await db.SortedSetCombineAndStoreAsync(SetOperation.Intersect
                                            , tempKey, listkeyFull.ToArray(), null, Aggregate.Max, CommandFlags.None);
                                    if (value > 0)
                                    {
                                        try
                                        {
                                            listKey = (await db.SortedSetRangeByScoreAsync(tempKey
                                            , (data.FromDate ?? DateTime.MinValue).ConvertToTimestamp10()
                                            , (data.ToDate ?? DateTime.Now).ConvertToTimestamp10()
                                            , Exclude.None
                                            , data.OrderBy == SearchNewsOrderBy.AscCreatedDate ? Order.Ascending : Order.Descending
                                            , ((data.PageIndex ?? 1) - 1) * (data.PageSize ?? 20)
                                            , data.PageSize ?? 20
                                            , CommandFlags.None));

                                            total = await db.SortedSetLengthAsync(tempKey
                                                , (data.FromDate ?? DateTime.MinValue).ConvertToTimestamp10()
                                                , (data.ToDate ?? DateTime.Now).ConvertToTimestamp10()
                                                , Exclude.None
                                                , CommandFlags.None);

                                            result.Total = (int)total;
                                            listArticle = listKey != null && listKey.Length > 0 ? (await db.HashGetAsync(_context.NameOf(typeof(Article).Name), listKey, CommandFlags.PreferSlave))?.Select(p => Json.Parse<Article>(p))?.ToList() : listArticle;
                                            if (listArticle != null)
                                            {
                                                foreach (var article in listArticle)
                                                {
                                                    var articleSearchReturn = Mapper<ArticleSearchReturn>.Map(article, new ArticleSearchReturn());
                                                    var listTmp = new List<NewsDistribution>();
                                                    try
                                                    {
                                                        var newsDistribution = await db.SortedSetRangeByScoreWithScoresAsync(_context.NameOf(IndexKeys.KeyNewsDistribution(article.Id)), double.MinValue, double.MaxValue, Exclude.None, Order.Ascending, 0, -1, CommandFlags.PreferSlave);
                                                        if (newsDistribution != null)
                                                        {
                                                            foreach (var item in newsDistribution)
                                                            {
                                                                if (long.TryParse(item.Element.ToString()?.Split(IndexKeys.SeparateChar)?.FirstOrDefault(), out long distributorId))
                                                                {
                                                                    listTmp.Add(new NewsDistribution
                                                                    {
                                                                        DistributorId = distributorId,
                                                                        SharedDate = Utility.ConvertSecondsToDate(item.Score),
                                                                        DistributorName = Json.Parse<Distributor>(await db.HashGetAsync(_context.NameOf(typeof(Distributor).Name), distributorId, CommandFlags.PreferSlave))?.Name
                                                                    });
                                                                }
                                                            }
                                                        }
                                                    }
                                                    catch (Exception exx)
                                                    {
                                                        Logger.Error(exx, exx.Message);
                                                    }
                                                    articleSearchReturn.NewsDistribution = listTmp.ToArray();
                                                    result.Data.Add(articleSearchReturn);
                                                }
                                            }
                                        }
                                        catch (Exception ex)
                                        {
                                            Logger.Error(ex, ex.Message);
                                        }
                                        action(tempKey);
                                    }
                                    action(tempKeyApprove);
                                }
                            }
                            catch (Exception ex)
                            {
                                Logger.Error(ex, ex.Message);
                            }

                            return result;
                        }
                        else
                        {
                            return new PagingDataResult<ArticleSearchReturn>();
                        }
                    });
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
            }
            return returnValue;
        }

        public async Task<RedisValue[]> GetAllAsync()
        {
            var returnValue = default(RedisValue[]);
            try
            {
                using (var _context = GetContext())
                {
                    returnValue = await _context.ExecAsync(async (db) =>
                    {
                        var tempKey = _context.NameOf("index:temp:union:" + Generator.SorterSetSearchId());
                        var value = await db.SortedSetCombineAndStoreAsync(SetOperation.Intersect
                                        , tempKey, _context.NameOf(IndexKeys.KeyStatus<NewsAll>((int)NewsStatus.Published)), _context.NameOf(IndexKeys.KeyType<NewsAll>((int)NewsType.Article)));
                        
                        returnValue =  await db.SortedSetRangeByRankAsync(tempKey, 0, -1);
                        await db.KeyDeleteAsync(tempKey);
                        return returnValue;
                    });
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
            }
            return returnValue;
        }
    }
}
