﻿using ChannelVN.IMS2.Core.Entities;
using ChannelVN.IMS2.Core.Entities.Board;
using ChannelVN.IMS2.Core.Entities.Security;
using ChannelVN.IMS2.Core.Models.CacheStore.Functions;
using ChannelVN.IMS2.Foundation.Common;
using ChannelVN.IMS2.Foundation.Data.Providers;
using ChannelVN.IMS2.Foundation.Logging;
using StackExchange.Redis;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using static ChannelVN.IMS2.Core.Entities.ErrorCode.ErrorMapping;

namespace ChannelVN.IMS2.Core.Models.DataStore.Boards
{
    public class BoardCacheStore : CmsMainCacheStore
    {
        public async Task<ErrorCodes> AddAsync(Board data, long userId, Action<long> action)
        {
            var returnValue = ErrorCodes.AddError;
            try
            {
                using (var _context = GetContext())
                {
                    //Kiểm tra quyền viết bài
                    var officerId = long.Parse(data.CreatedBy);
                    var listMemberId = await _context.SortedSetRangeByRankAsync(IndexKeys.KeyAccountMemberInOfficer<AccountMember>(officerId), 0, -1, Order.Descending, CommandFlags.PreferSlave);
                    var listMember = await _context.GetsFromHashExAsync<AccountMember>(listMemberId?.Select(p => officerId + IndexKeys.SeparateChar + p)?.ToList(), CommandFlags.PreferSlave);

                    var owner = listMember?.Where(p => p.Role == AccountMemberRole.Owner).FirstOrDefault();
                    if (owner != null)
                    {
                        var isWriter = await Function.IsWriter(officerId, userId.ToString(), _context);
                        if (isWriter)
                        {
                            var resultAdd = await _context.MultiAsync((transac) =>
                            {
                                transac.HashSetAsync(_context.NameOf(data.GetType().Name), data.Id, Json.Stringify(data));
                                transac.SortedSetAddAsync(_context.NameOf(IndexKeys.KeyCreatedBy<Board>(data.CreatedBy)), data.Id, (data.CreatedDate ?? DateTime.Now).ConvertToTimestamp10());
                                transac.SortedSetAddAsync(_context.NameOf(IndexKeys.KeyStatus<Board>(data.Status)), data.Id, (data.CreatedDate ?? DateTime.Now).ConvertToTimestamp10());
                            });
                            if (resultAdd)
                            {
                                returnValue = ErrorCodes.Success;
                                action(owner.MemberId);
                            }
                        }
                        else
                        {
                            returnValue = ErrorCodes.PermissionInvalid;
                        }
                    }
                    else
                    {
                        returnValue = ErrorCodes.PermissionInvalid;
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
                returnValue = ErrorCodes.Exception;
            }
            return returnValue;
        }

        public async Task<ErrorCodes> UpdateAsync(Board data, long officerId)
        {
            var returnValue = ErrorCodes.UpdateError;
            try
            {
                using (var _context = GetContext())
                {
                    //var isUpdate = await _context.ExecAsync(async (db) =>
                    //{
                    //    var listMemberId = await db.SortedSetRangeByRankAsync(_context.NameOf(IndexKeys.KeyAccountMemberInOfficer<AccountMember>(officerId)), 0, -1, Order.Descending, CommandFlags.PreferSlave);

                    //    var listMember = (await db.HashGetAsync(_context.NameOf<AccountMember>(), listMemberId?.Select(p => (RedisValue)(officerId + IndexKeys.SeparateChar + p.ToString()))?.ToArray(), CommandFlags.PreferSlave))?.Select(p => Json.Parse<AccountMember>(p));

                    //    if (listMember != null && listMember.Where(p => p.MemberId.ToString().Equals(data.ModifiedBy)).FirstOrDefault()?.Permissions != null && listMember.Where(p => p.MemberId.ToString().Equals(data.ModifiedBy)).FirstOrDefault().Permissions.Contains(MemberPermission.CreatePost))
                    //    {
                    //        var value = await db.SortedSetRangeByScoreAsync(_context.NameOf(IndexKeys.KeyCreatedBy<Board>(officerId.ToString())));
                    //        if (value != null && value.Contains((RedisValue)data.Id))
                    //        {
                    //            return true;
                    //        }
                    //    }
                    //    return false;
                    //});
                    if(long.TryParse(data.ModifiedBy, out long userId))
                    {
                        var isUpdate = await Function.CheckBoardOfUser(officerId, userId, data.Id, _context);
                        if (isUpdate)
                        {
                            var dataDb = await _context.HashGetExAsync<Board>(data.Id, CommandFlags.PreferSlave);

                            dataDb.Name = data.Name;
                            dataDb.UnsignName = data.UnsignName;
                            dataDb.Description = data.Description;
                            dataDb.Url = data.Url;
                            dataDb.Avatar = data.Avatar;
                            dataDb.Cover = data.Cover;
                            dataDb.NewsCoverId = data.NewsCoverId;
                            dataDb.ModifiedDate = data.ModifiedDate;
                            dataDb.ModifiedBy = data.ModifiedBy;
                            //dataDb.Status = data.Status;
                            dataDb.IsHot = data.IsHot;
                            dataDb.FollowCount = data.FollowCount;
                            dataDb.ParentId = data.ParentId;
                            dataDb.RelationBoard = data.RelationBoard;

                            if (dataDb != null)
                            {
                                var resultUpdate = await _context.MultiAsync((transac) =>
                                {
                                    transac.HashSetAsync(_context.NameOf(dataDb.GetType().Name), dataDb.Id, Json.Stringify(dataDb));
                                });

                                if (resultUpdate) returnValue = ErrorCodes.Success;
                            }
                            else
                            {
                                returnValue = ErrorCodes.DataNotExist;
                            }
                        }
                        else
                        {
                            returnValue = ErrorCodes.PermissionPublishInvalid;
                        }
                    }
                    else
                    {
                        returnValue = ErrorCodes.PermissionPublishInvalid;
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
                returnValue = ErrorCodes.Exception;
            }
            return returnValue;
        }
        public Task<bool> UpdateMediaAsync(IEnumerable<long> listAdd, IEnumerable<long> listDelete, Board data)
        {
            var returnValue = Task.FromResult(false);
            try
            {
                using (var _context = GetContext())
                {
                    returnValue = _context.MultiAsync((transac) =>
                    {
                        transac.HashSetAsync(_context.NameOf(data.GetType().Name), data.Id, Json.Stringify(data));
                        transac.SortedSetRemoveAsync(IndexKeys.KeyNewsInBoard(data.Id), listDelete?.Select(p => (RedisValue)p).ToArray());
                        transac.SortedSetAddAsync(IndexKeys.KeyNewsInBoard(data.Id), listAdd?.Select(p => new SortedSetEntry(p, (data.LastInsertedDate ?? DateTime.Now).ConvertToTimestamp10()))?.ToArray());
                    });
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
            }
            return returnValue;
        }

        public Task<Board> GetByIdAsync(long boardId)
        {
            var returnValue = Task.FromResult(default(Board));
            try
            {
                using (var _context = GetContext())
                {
                    returnValue = _context.HashGetExAsync<Board>(boardId, CommandFlags.PreferSlave);
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
            }
            return returnValue;
        }

        public Task<bool> UpdateStatusAsync(Board data, int oldStatus)
        {
            var returnValue = Task.FromResult(false);
            try
            {
                using (var _context = GetContext())
                {
                    returnValue = _context.MultiAsync((transac) =>
                    {
                        transac.HashSetAsync(_context.NameOf(data.GetType().Name), data.Id, Json.Stringify(data));

                        transac.SortedSetRemoveAsync(_context.NameOf(IndexKeys.KeyStatus<Board>(oldStatus)), data.Id);
                        transac.SortedSetAddAsync(_context.NameOf(IndexKeys.KeyStatus<Board>(data.Status)), data.Id, (data.CreatedDate ?? DateTime.Now).ConvertToTimestamp10());
                    });
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
            }
            return returnValue;
        }

        public Task<PagingDataResult<BoardSearch>> ListAsync(long userId, int? status, int? pageIndex, int? pageSize, Action<string> action)
        {
            var returnValue = Task.FromResult(default(PagingDataResult<BoardSearch>));
            try
            {
                using (var _context = GetContext())
                {
                    returnValue = _context.ExecAsync(async (db) =>
                    {
                        var result = new PagingDataResult<BoardSearch>
                        {
                            Data = new List<BoardSearch>()
                        };
                        try
                        {
                            var listKey = new RedisValue[] { };
                            var listBoardSearch = new List<BoardSearch>();
                            var total = default(long);
                            var tempKey = _context.NameOf("index:temp:union:" + Generator.SorterSetSearchId());
                            var createdByKey = _context.NameOf(IndexKeys.KeyCreatedBy<Board>(userId.ToString()));
                            var listkeyFull = new List<RedisKey>();
                            listkeyFull.Add(createdByKey);
                            if (status != null)
                            {
                                listkeyFull.Add(_context.NameOf(IndexKeys.KeyStatus<Board>(status)));
                            }
                            var value1 = await db.SortedSetCombineAndStoreAsync(SetOperation.Intersect
                                        , tempKey, listkeyFull.ToArray(), null, Aggregate.Max, CommandFlags.None);
                            if (value1 > 0)
                            {
                                try
                                {
                                    listKey = (await db.SortedSetRangeByScoreAsync(tempKey
                                        , double.MinValue
                                        , double.MaxValue
                                        , Exclude.None
                                        , Order.Ascending
                                        , ((pageIndex ?? 1) - 1) * (pageSize ?? 20)
                                        , pageSize ?? 20, CommandFlags.None));

                                    total = await db.SortedSetLengthAsync(tempKey
                                        , double.MinValue
                                        , double.MaxValue, Exclude.None, CommandFlags.None);

                                    result.Total = (int)total;

                                    var listBoard = listKey != null && listKey.Length > 0 ? (await db.HashGetAsync(_context.NameOf<Board>(), listKey, CommandFlags.PreferSlave))?.Select(p => Json.Parse<Board>(p))?.ToList() : null;

                                    if (listBoard != null)
                                    {
                                        listBoardSearch = listBoard.Select(p => Mapper<BoardSearch>.Map(p, new BoardSearch()))?.ToList();
                                        foreach (var board in listBoardSearch)
                                        {
                                            var newsInboard = await db.SortedSetRangeByScoreWithScoresAsync(_context.NameOf(IndexKeys.KeyNewsInBoard(board.Id)), double.MinValue, double.MaxValue);
                                            if (newsInboard != null)
                                            {
                                                var listTmp = new List<NewsInBoard>();
                                                foreach (var item in newsInboard)
                                                {
                                                    if (long.TryParse(item.Element, out long newsId))
                                                    {
                                                        listTmp.Add(new NewsInBoard
                                                        {
                                                            BoardId = board.Id,
                                                            NewsId = newsId,
                                                            PublishedDate = Utility.ConvertSecondsToDate(item.Score)
                                                        });
                                                    }
                                                }
                                                board.NewsInBoard = listTmp.ToArray();
                                            }
                                        }
                                        result.Data = listBoardSearch;
                                    }
                                }
                                catch (Exception ex)
                                {
                                    Logger.Error(ex, ex.Message);
                                }
                                action(tempKey);
                            }
                        }
                        catch (Exception ex)
                        {
                            Logger.Error(ex, ex.Message);
                        }

                        return result;
                    });
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
            }
            return returnValue;
        }

        public Task<bool> InsertMediaAsync(long mediaId, List<Board> dataDb)
        {
            var returnValue = Task.FromResult(false);
            try
            {
                using (var _context = GetContext())
                {
                    returnValue = _context.MultiAsync((transac) =>
                    {
                        foreach (var board in dataDb)
                        {
                            transac.HashSetAsync(_context.NameOf<Board>(), board.Id, Json.Stringify(board));
                            transac.SortedSetAddAsync(_context.NameOf(IndexKeys.KeyNewsInBoard(board.Id)), mediaId, (board.LastInsertedDate ?? DateTime.Now).ConvertToTimestamp10());
                        }
                    });
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
            }
            return returnValue;
        }

        public Task<List<Board>> GetListByIdAsync(long[] boardIds)
        {
            var returnValue = Task.FromResult(default(List<Board>));
            try
            {
                using (var _context = GetContext())
                {
                    returnValue = _context.GetsFromHashExAsync<Board>(boardIds?.Select(p => p.ToString())?.ToList(), CommandFlags.PreferSlave);
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
            }
            return returnValue;
        }

        public async Task<bool> AddNewsAsync(long boardId, string ids, long officerId)
        {
            var returnValue = false;
            try
            {
                var count = 0;
                var listId = ids?.Split(",")?.Where(p => long.TryParse(p, out long newsId))?.Select(p => new SortedSetEntry(p, Utility.ConvertToTimestamp10(DateTime.Now.AddSeconds(count++))));
                if (listId != null && listId.Count() > 0)
                {
                    using (var _context = GetContext())
                    {
                        var board = await _context.HashGetExAsync<Board>(boardId);
                        if (board != null)
                        {
                            board.LastInsertedDate = DateTime.SpecifyKind(DateTime.Now, DateTimeKind.Unspecified);
                            returnValue = await _context.MultiAsync((transac) =>
                            {
                                transac.SortedSetAddAsync(_context.NameOf(IndexKeys.KeyNewsInBoard(boardId)), listId?.ToArray());
                                transac.SortedSetAddAsync(_context.NameOf(IndexKeys.KeyPublishedDate<NewsInBoard>()), listId?.Select(p => new SortedSetEntry(boardId + IndexKeys.SeparateChar + p.Element, p.Score))?.ToArray());
                                transac.HashSetAsync(_context.NameOf<Board>(), board.Id, Json.Stringify(board));
                            });
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
            }
            return returnValue;
        }
        public Task<bool> DeleteNewsAsync(long officerId, long boardId, string newsIds)
        {
            var returnValue = Task.FromResult(false);
            try
            {
                using (var _context = GetContext())
                {
                    returnValue = _context.MultiAsync((transac) =>
                    {
                        transac.SortedSetRemoveAsync(_context.NameOf(IndexKeys.KeyNewsInBoard(boardId)), newsIds.Split(",")?.Select(p => (RedisValue)p).ToArray());
                        var tmp = newsIds.Split(",")?.Select(p => (RedisValue)(boardId + IndexKeys.SeparateChar + p))?.ToArray();
                        transac.SortedSetRemoveAsync(_context.NameOf(IndexKeys.KeyPublishedDate<NewsInBoard>()), tmp);
                    });
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
            }
            return returnValue;
        }

        public Task<PagingDataResult<NewsAllSearch>> NewsInBoardAsync(long officerId, long boardId, int? pageIndex, int? pageSize)
        {
            var returnValue = Task.FromResult(default(PagingDataResult<NewsAllSearch>));
            try
            {
                using (var _context = GetContext())
                {
                    returnValue = _context.ExecAsync(async (db) =>
                    {
                        var result = new PagingDataResult<NewsAllSearch>()
                        {
                            Data = new List<NewsAllSearch>()
                        };

                        try
                        {
                            var listKey = new RedisValue[] { };
                            var listNewsAllSearch = new List<NewsAllSearch>();
                            var total = default(long);
                            listKey = (await db.SortedSetRangeByScoreAsync(_context.NameOf(IndexKeys.KeyNewsInBoard(boardId))
                                        , double.MinValue
                                        , double.MaxValue
                                        , Exclude.None
                                        , Order.Descending
                                        , ((pageIndex ?? 1) - 1) * (pageSize ?? 20)
                                        , pageSize ?? 20, CommandFlags.PreferSlave));

                            total = await db.SortedSetLengthAsync(_context.NameOf(IndexKeys.KeyNewsInBoard(boardId))
                                , double.MinValue
                                , double.MaxValue, Exclude.None, CommandFlags.PreferSlave);

                            result.Total = (int)total;

                            var listNewsAll = listKey != null && listKey.Length > 0 ? (await db.HashGetAsync(_context.NameOf<NewsAll>(), listKey, CommandFlags.PreferSlave))?.Select(p => Json.Parse<NewsAll>(p)) : null;

                            if (listNewsAll != null)
                            {
                                listNewsAllSearch = listNewsAll.Select(p => Mapper<NewsAllSearch>.Map(p, new NewsAllSearch()))?.ToList();
                                result.Data = listNewsAllSearch;
                            }
                        }
                        catch (Exception ex)
                        {
                            Logger.Error(ex, ex.Message);
                        }

                        return result;
                    });
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
            }
            return returnValue;
        }
        public async Task<long[]> NewsInBoardAsync(long boardId)
        {
            var returnValue = new long[] { };
            try
            {
                using (var _context = GetContext())
                {
                    var arrValue = await _context.SortedSetRangeByRankAsync(IndexKeys.KeyNewsInBoard(boardId), 0, -1, Order.Descending, CommandFlags.PreferSlave);
                    returnValue = arrValue?.Select(p => (long)p)?.ToArray();
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
            }
            return returnValue;
        }


        public Task<BoardSearch> GetBoardSearchByIdAsync(long id)
        {
            var returnValue = Task.FromResult(default(BoardSearch));
            try
            {
                using (var _context = GetContext())
                {
                    returnValue = _context.ExecAsync(async (db) =>
                    {
                        var result = default(BoardSearch);
                        try
                        {
                            var board = Json.Parse<Board>(await db.HashGetAsync(_context.NameOf<Board>(), id, CommandFlags.PreferSlave));
                            if (board != null)
                            {
                                result = Mapper<BoardSearch>.Map(board, new BoardSearch());
                                if (result != null)
                                {
                                    var newsInboard = await db.SortedSetRangeByScoreWithScoresAsync(_context.NameOf(IndexKeys.KeyNewsInBoard(id)), double.MinValue, double.MaxValue);
                                    if (newsInboard != null)
                                    {
                                        var listTmp = new List<NewsInBoard>();
                                        foreach (var item in newsInboard)
                                        {
                                            if (long.TryParse(item.Element, out long newsId))
                                            {
                                                listTmp.Add(new NewsInBoard
                                                {
                                                    BoardId = id,
                                                    NewsId = newsId,
                                                    PublishedDate = Utility.ConvertSecondsToDate(item.Score)
                                                });
                                            }
                                        }
                                        result.NewsInBoard = listTmp.ToArray();
                                    }
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            Logger.Error(ex, ex.Message);
                        }

                        return result;
                    });
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
            }
            return returnValue;
        }

        public Task<List<BoardSearch>> GetListByIdsAsync(List<string> Ids)
        {
            var returnValue = Task.FromResult(default(List<BoardSearch>));
            try
            {
                using (var _context = GetContext())
                {
                    returnValue = _context.ExecAsync(async (db) =>
                    {
                        var result = default(List<BoardSearch>);
                        try
                        {
                            var listBoard = (await db.HashGetAsync(_context.NameOf<Board>(), Ids?.Select(p => (RedisValue)p)?.ToArray(), CommandFlags.PreferSlave))?.Select(p => Json.Parse<Board>(p));
                            if (listBoard != null)
                            {
                                var listBoardSearch = listBoard.Select(p => Mapper<BoardSearch>.Map(p, new BoardSearch()));
                                if (listBoardSearch != null)
                                {
                                    foreach (var board in listBoardSearch)
                                    {
                                        var newsInboard = await db.SortedSetRangeByScoreWithScoresAsync(_context.NameOf(IndexKeys.KeyNewsInBoard(board.Id)), double.MinValue, double.MaxValue);
                                        if (newsInboard != null)
                                        {
                                            var listTmp = new List<NewsInBoard>();
                                            foreach (var item in newsInboard)
                                            {
                                                if (long.TryParse(item.Element, out long newsId))
                                                {
                                                    listTmp.Add(new NewsInBoard
                                                    {
                                                        BoardId = board.Id,
                                                        NewsId = newsId,
                                                        PublishedDate = Utility.ConvertSecondsToDate(item.Score)
                                                    });
                                                }
                                            }
                                            board.NewsInBoard = listTmp.ToArray();
                                        }
                                    }
                                }
                                result = listBoardSearch?.ToList();
                            }
                        }
                        catch (Exception ex)
                        {
                            Logger.Error(ex, ex.Message);
                        }

                        return result;
                    });
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
            }
            return returnValue;
        }

        public Task<PagingDataResult<BoardSearch>> SearchAsync(SearchBoard search, long userId, Action<string> action)
        {
            var returnValue = Task.FromResult(default(PagingDataResult<BoardSearch>));
            try
            {
                using (var _context = GetContext())
                {
                    returnValue = _context.ExecAsync(async (db) =>
                    {
                        var result = new PagingDataResult<BoardSearch>
                        {
                            Data = new List<BoardSearch>()
                        };
                        try
                        {
                            var listKey = new RedisValue[] { };
                            var listBoardSearch = new List<BoardSearch>();
                            var total = default(long);
                            var tempKey = _context.NameOf("index:temp:union:" + Generator.SorterSetSearchId());
                            var createdByKey = _context.NameOf(IndexKeys.KeyCreatedBy<Board>(search.OfficerId.ToString()));
                            var listkeyFull = new List<RedisKey>();
                            listkeyFull.Add(createdByKey);
                            if (search.Status != null)
                            {
                                listkeyFull.Add(_context.NameOf(IndexKeys.KeyStatus<Board>((int?)search.Status)));
                            }
                            var value1 = await db.SortedSetCombineAndStoreAsync(SetOperation.Intersect
                                        , tempKey, listkeyFull.ToArray(), null, Aggregate.Max, CommandFlags.None);
                            if (value1 > 0)
                            {
                                try
                                {
                                    listKey = (await db.SortedSetRangeByScoreAsync(tempKey
                                        , double.MinValue
                                        , double.MaxValue
                                        , Exclude.None
                                        , search.OrderBy == SearchNewsOrderBy.DescCreatedDate ? Order.Descending : Order.Ascending
                                        , ((search.PageIndex ?? 1) - 1) * (search.PageSize ?? 20)
                                        , search.PageSize ?? 20, CommandFlags.None));

                                    total = await db.SortedSetLengthAsync(tempKey
                                        , double.MinValue
                                        , double.MaxValue, Exclude.None, CommandFlags.None);

                                    result.Total = (int)total;

                                    var listBoard = listKey != null && listKey.Length > 0 ? (await db.HashGetAsync(_context.NameOf<Board>(), listKey, CommandFlags.PreferSlave))?.Select(p => Json.Parse<Board>(p)) : null;

                                    if (listBoard != null)
                                    {
                                        listBoardSearch = listBoard.Select(p => Mapper<BoardSearch>.Map(p, new BoardSearch()))?.ToList();
                                        foreach (var board in listBoardSearch)
                                        {
                                            var newsInboard = await db.SortedSetRangeByScoreWithScoresAsync(_context.NameOf(IndexKeys.KeyNewsDistribution(board.Id)), double.MinValue, double.MaxValue);
                                            if (newsInboard != null)
                                            {
                                                var listTmp = new List<NewsInBoard>();
                                                foreach (var item in newsInboard)
                                                {
                                                    if (long.TryParse(item.Element, out long newsId))
                                                    {
                                                        listTmp.Add(new NewsInBoard
                                                        {
                                                            BoardId = board.Id,
                                                            NewsId = newsId,
                                                            PublishedDate = Utility.ConvertSecondsToDate(item.Score)
                                                        });
                                                    }
                                                }
                                                board.NewsInBoard = listTmp.ToArray();
                                            }
                                        }
                                        result.Data = listBoardSearch;
                                    }
                                }
                                catch (Exception ex)
                                {
                                    Logger.Error(ex, ex.Message);
                                }
                                action(tempKey);
                            }
                        }
                        catch (Exception ex)
                        {
                            Logger.Error(ex, ex.Message);
                        }

                        return result;
                    });
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
            }
            return returnValue;
        }

        public Task<List<Board>> GetAllAsync()
        {
            var returnValue = Task.FromResult(default(List<Board>));
            try
            {
                using (var _context = GetContext())
                {
                    returnValue = _context.ExecAsync(async (db) =>
                    {
                        var listKey = await db.SortedSetRangeByRankAsync(_context.NameOf(IndexKeys.KeyStatus<Board>((int)BoardStatus.Actived)), 0, -1, Order.Ascending, CommandFlags.PreferSlave);
                        return (await db.HashGetAsync(_context.NameOf<Board>(), listKey, CommandFlags.PreferSlave))?.Select(p => Json.Parse<Board>(p))?.ToList();
                    });
                }
            }
            catch(Exception ex)
            {
                Logger.Error(ex, ex.Message);
            }
            return returnValue;
        }

        public async Task<bool> CheckBoardOfUser(long officerId, long userId, long boardId)
        {
            var returnValue = false;
            try
            {
                using (var _context = GetContext())
                {
                    returnValue = await _context.ExecAsync(async (db) =>
                    {
                        var result = false;
                        var listMemberId = await db.SortedSetRangeByRankAsync(_context.NameOf(IndexKeys.KeyAccountMemberInOfficer<AccountMember>(officerId)), 0, -1, Order.Descending, CommandFlags.PreferSlave);

                        var listMember = (await db.HashGetAsync(_context.NameOf<AccountMember>(), listMemberId?.Select(p => (RedisValue) (officerId + IndexKeys.SeparateChar + p.ToString()))?.ToArray(), CommandFlags.PreferSlave))?.Select(p=>Json.Parse<AccountMember>(p));

                        if (listMember != null && listMember.Where(p => p.MemberId == userId).FirstOrDefault()?.Permissions != null && listMember.Where(p => p.MemberId == userId).FirstOrDefault().Permissions.Contains(MemberPermission.CreatePost))
                        {
                            var value = await db.SortedSetRangeByScoreAsync(_context.NameOf(IndexKeys.KeyCreatedBy<Board>(officerId.ToString())));
                            if (value != null && value.Contains(boardId)) result = true;
                        }
                        return result;
                    });
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
            }
            return returnValue;
        }

        public async Task<bool> CheckBoardOfUser(long officerId, long userId, long[] boardIds)
        {
            var returnValue = false;
            try
            {
                using (var _context = GetContext())
                {
                    returnValue = await _context.ExecAsync(async (db) =>
                    {
                        var result = false;
                        var listMemberId = await db.SortedSetRangeByRankAsync(_context.NameOf(IndexKeys.KeyAccountMemberInOfficer<AccountMember>(officerId)), 0, -1, Order.Descending, CommandFlags.PreferSlave);

                        var listMember = (await db.HashGetAsync(_context.NameOf<AccountMember>(), listMemberId?.Select(p => (RedisValue)(officerId + IndexKeys.SeparateChar + p.ToString()))?.ToArray(), CommandFlags.PreferSlave))?.Select(p => Json.Parse<AccountMember>(p));

                        if (listMember != null && listMember.Where(p => p.MemberId == userId).FirstOrDefault()?.Permissions != null && listMember.Where(p => p.MemberId == userId).FirstOrDefault().Permissions.Contains(MemberPermission.CreatePost))
                        {
                            var value = await db.SortedSetRangeByScoreAsync(_context.NameOf(IndexKeys.KeyCreatedBy<Board>(officerId.ToString())));
                            if (value != null && boardIds != null) {
                                result = true;
                                foreach (var v in boardIds)
                                {
                                    if (!value.Contains(v))
                                    {
                                        result = false;
                                        break;
                                    }
                                }
                            }
                            else
                            {
                                result = false;
                            }
                        }
                        return result;
                    });

                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
            }
            return returnValue;
        }
    }
}
