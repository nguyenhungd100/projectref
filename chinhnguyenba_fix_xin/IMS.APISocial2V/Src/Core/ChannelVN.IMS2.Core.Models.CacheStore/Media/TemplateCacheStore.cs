﻿using ChannelVN.IMS2.Core.Entities.Media;
using ChannelVN.IMS2.Foundation.Common;
using ChannelVN.IMS2.Foundation.Data.Providers;
using ChannelVN.IMS2.Foundation.Logging;
using StackExchange.Redis;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ChannelVN.IMS2.Core.Models.CacheStore.Media
{
    public class TemplateCacheStore : CmsMainCacheStore
    {
        public Task<bool> AddAsync(Template data, bool isQueue = false)
        {
            var returnValue = Task.FromResult(false);
            try
            {
                using (var _context = GetContext())
                {
                    returnValue = _context.MultiAsync((transac) =>
                    {
                        transac.HashSetAsync(_context.NameOf<Template>(), data.Id, Json.Stringify(data));
                    });
                } 
            }
            catch (Exception ex)
            {
                if(!isQueue) Logger.Error(ex, ex.Message);
            }
            return returnValue;
        }

        public async Task<bool> UpdateAsync(Template data, bool isQueue = false)
        {
            var returnValue = false;
            try
            {
                using (var _context = GetContext())
                {
                    var obj = await _context.HashGetExAsync<Template>(data.Id, CommandFlags.PreferSlave);
                    if (obj != null)
                    {
                        obj.Title = data.Title;
                        obj.Avatar = data.Avatar;
                        obj.MetaData = data.MetaData;
                        obj.Description = data.Description;
                        obj.Priority = data.Priority;
                        obj.CategoryId = data.CategoryId;
                        obj.ModifiedBy = data.ModifiedBy;
                        obj.ModifiedDate = data.ModifiedDate;

                        await _context.HashSetExAsync(obj.Id, obj);
                        returnValue = true;
                    }
                }
            }
            catch (Exception ex)
            {
                if(!isQueue) Logger.Error(ex, ex.Message);
            }
            return returnValue;
        }

        public Task<bool> UpdateStatusAsync(Template data)
        {
            var returnValue = Task.FromResult(false);
            try
            {
                if (data != null)
                {
                    using (var _context = GetContext())
                    {
                        returnValue = _context.MultiAsync((transac) =>
                        {
                            transac.HashSetAsync(_context.NameOf<Template>(), data.Id, Json.Stringify(data));
                        });
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
            }
            return returnValue;
        }

        public Task<Template> GetByIdAsync(long id)
        {
            var returnValue = Task.FromResult(default(Template));
            try
            {
                using (var _context = GetContext())
                {
                    returnValue = _context.HashGetExAsync<Template>(id, CommandFlags.PreferSlave);
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
            }
            return returnValue;
        }

        public Task<List<Template>> GetListByIdsAsync(List<string> ids)
        {
            var returnValue = Task.FromResult(default(List<Template>));
            try
            {
                using (var _context = GetContext())
                {
                    returnValue = _context.GetsFromHashExAsync<Template>(ids, CommandFlags.PreferSlave);
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
            }
            return returnValue;
        }

        public async Task<PagingDataResult<Template>> SearchAsync(int categoryId, int? status, int pageIndex, int pageSize)
        {
            var returnValue = new PagingDataResult<Template>();
            try
            {
                var listAll = default(List<Template>);
                using (var _context = GetContext())
                {
                    listAll = await _context.HashGetAllExxAsync<Template>(CommandFlags.PreferSlave);
                }
                
                if (status != null) listAll = listAll?.Where(p => p.Status == status)?.ToList();
                if (categoryId != 0) listAll = listAll?.Where(p => p.CategoryId == categoryId)?.ToList();
                returnValue.Total = (int)listAll?.Count();
                returnValue.Data = listAll?.Skip(((pageIndex - 1) * pageSize))?.Take(pageSize)?.ToList();
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
            }
            return returnValue;
        }
    }
}
