﻿using ChannelVN.IMS2.Foundation.Logging;
using StackExchange.Redis;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace ChannelVN.IMS2.Core.Models.CacheStore
{
    public class SystemCacheStore: CmsMainCacheStore
    {
        public Task<RedisValue[]> ListQueueAsync(string index, long start, long stop)
        {
            var returnValue = Task.FromResult(default(RedisValue[]));
            try
            {
                using (var _context = GetContext())
                {
                    returnValue = _context.ExecAsync((db) =>
                    {
                        return db.ListRangeAsync(index, start, stop, CommandFlags.PreferSlave);
                    });
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
            }
            return returnValue;
        }
    }
}
