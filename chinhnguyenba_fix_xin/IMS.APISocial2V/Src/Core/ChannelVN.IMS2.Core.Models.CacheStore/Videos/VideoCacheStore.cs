﻿using ChannelVN.IMS2.Core.Entities;
using ChannelVN.IMS2.Core.Entities.Distribution;
using ChannelVN.IMS2.Core.Entities.ErrorCode;
using ChannelVN.IMS2.Core.Entities.Security;
using ChannelVN.IMS2.Core.Entities.Videos;
using ChannelVN.IMS2.Foundation.Common;
using ChannelVN.IMS2.Foundation.Logging;
using StackExchange.Redis;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using static ChannelVN.IMS2.Core.Entities.ErrorCode.ErrorMapping;

namespace ChannelVN.IMS2.Core.Models.CacheStore.Videos
{
    public class VideoCacheStore : CmsMainCacheStore
    {
        public async Task<ErrorCodes> AddAsync(VideoSearch dataSearch)
        {
            var returnValue = ErrorCodes.AddError;
            try
            {
                var data = Mapper<Video>.Map(dataSearch, new Video());
                var newsAll = Mapper<NewsAll>.MapNewsAll(data, new NewsAll());

                using(var _context = GetContext())
                {
                    //Kiểm tra quyền viết bài
                    var isWriter = await Functions.Function.IsWriter(dataSearch?.NewsInAccount?.FirstOrDefault().AccountId, dataSearch.CreatedBy, _context);

                    ///
                    if (isWriter)
                    {
                        var resultAdd = await _context.MultiAsync((transac) =>
                        {
                            transac.HashSetAsync(_context.NameOf(data.GetType().Name), data.Id, Json.Stringify(data));

                            transac.HashSetAsync(_context.NameOf(newsAll.GetType().Name), newsAll.Id, Json.Stringify(newsAll));
                            transac.SortedSetAddAsync(_context.NameOf(IndexKeys.KeyCreatedBy<NewsAll>(newsAll.CreatedBy)), newsAll.Id, Utility.ConvertToTimestamp10(newsAll.CreatedDate ?? DateTime.Now));
                            transac.SortedSetAddAsync(_context.NameOf(IndexKeys.KeyStatus<NewsAll>(newsAll.Status)), newsAll.Id, Utility.ConvertToTimestamp10(newsAll.CreatedDate ?? DateTime.Now));
                            transac.SortedSetAddAsync(_context.NameOf(IndexKeys.KeyDistributorid<NewsAll>(newsAll.DistributorId)), newsAll.Id, Utility.ConvertToTimestamp10(newsAll.CreatedDate ?? DateTime.Now));
                            transac.SortedSetAddAsync(_context.NameOf(IndexKeys.KeyType<NewsAll>(newsAll.Type ?? 2)), newsAll.Id, Utility.ConvertToTimestamp10(newsAll.CreatedDate ?? DateTime.Now));

                            transac.HashSetAsync(_context.NameOf(typeof(NewsInAccount).Name), dataSearch.NewsInAccount?.Select(p => new HashEntry(p.AccountId + ":" + p.NewsId, Json.Stringify(p)))?.ToArray());
                            transac.SetAddAsync(_context.NameOf(IndexKeys.KeyAccountInNews(dataSearch.Id)), dataSearch.NewsInAccount?.LastOrDefault()?.AccountId ?? 0);
                            transac.SetAddAsync(_context.NameOf(IndexKeys.KeyNewsInAccount(dataSearch.NewsInAccount?.LastOrDefault()?.AccountId)), dataSearch.Id);
                        });
                        if (resultAdd) returnValue = ErrorCodes.Success;
                    }
                    else
                    {
                        returnValue = ErrorCodes.PermissionInvalid;
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
                returnValue = ErrorCodes.Exception;
            }
            return returnValue;
        }

        public Task<Video> GetByIdAsync(long id)
        {
            var returnValue = Task.FromResult(default(Video));
            try
            {
                using (var _context = GetContext())
                {
                    returnValue = _context.HashGetExAsync<Video>(id, CommandFlags.PreferSlave);
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
            }
            return returnValue;
        }

        public Task<VideoSearch> GetById3Async(long id)
        {
            var returnValue = Task.FromResult(default(VideoSearch));
            try
            {
                using (var _context = GetContext())
                {
                    returnValue = _context.HashGetExAsync<VideoSearch>(id, CommandFlags.PreferSlave);
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
            }
            return returnValue;
        }

        public Task<VideoSearch> GetVideoFullInfoAsync(long newsId)
        {
            var returnValue = Task.FromResult(default(VideoSearch));
            try
            {
                using (var _context = GetContext())
                {
                    returnValue = _context.ExecAsync(async (db) =>
                    {
                        var result = default(VideoSearch);
                        try
                        {
                            var video = Json.Parse<Video>(await db.HashGetAsync(_context.NameOf<Video>(), newsId, CommandFlags.PreferSlave));
                            if (video != null)
                            {
                                result = Mapper<VideoSearch>.Map(video, new VideoSearch());
                                if (result != null)
                                {
                                    var listAccountId = await db.SetMembersAsync(_context.NameOf(IndexKeys.KeyAccountInNews(newsId)), CommandFlags.PreferSlave);
                                    result.NewsInAccount = (await db.HashGetAsync(_context.NameOf<NewsInAccount>(), listAccountId?.Select(p => (RedisValue)(p + IndexKeys.SeparateChar + newsId))?.ToArray(), CommandFlags.PreferSlave))?.Select(p => Json.Parse<NewsInAccount>(p))?.ToArray();

                                    var listDistributorIdAndItemStreamId = await db.SortedSetRangeByScoreWithScoresAsync(_context.NameOf(IndexKeys.KeyNewsDistribution(newsId)), double.MinValue, double.MaxValue, Exclude.None, Order.Ascending, 0, -1, CommandFlags.PreferSlave);

                                    var listKeyNewsDistribution = listDistributorIdAndItemStreamId?.Select(p => (RedisValue)(p.Element.ToString()?.Split(IndexKeys.SeparateChar)?.FirstOrDefault() + IndexKeys.SeparateChar + newsId + IndexKeys.SeparateChar + p.Element.ToString()?.Split(IndexKeys.SeparateChar)?.LastOrDefault()));
                                    result.NewsDistribution = (await db.HashGetAsync(_context.NameOf<NewsDistribution>(), listKeyNewsDistribution?.ToArray(), CommandFlags.PreferSlave))?.Select(p => Json.Parse<NewsDistribution>(p))?.ToArray();
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            Logger.Error(ex, ex.Message);
                        }

                        return result;
                    });
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
            }
            return returnValue;
        }
        public Task<bool> UpdateStatusAsync(Video data, int statusOld)
        {
            var returnValue = Task.FromResult(false);
            try
            {
                if (data != null)
                {
                    var newsAll = Mapper<NewsAll>.MapNewsAll(data, new NewsAll());

                    using (var _context = GetContext())
                    {
                        returnValue = _context.MultiAsync((transac) =>
                        {
                            transac.HashSetAsync(_context.NameOf(data.GetType().Name), data.Id, Json.Stringify(data));

                            transac.HashSetAsync(_context.NameOf(newsAll.GetType().Name), newsAll.Id, Json.Stringify(newsAll));
                            transac.SortedSetRemoveAsync(_context.NameOf(IndexKeys.KeyStatus<NewsAll>(statusOld)), newsAll.Id, CommandFlags.DemandMaster);
                            transac.SortedSetAddAsync(_context.NameOf(IndexKeys.KeyStatus<NewsAll>(newsAll.Status)), newsAll.Id, Utility.ConvertToTimestamp10(newsAll.CreatedDate ?? DateTime.Now));
                        });
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
            }
            return returnValue;
        }

        public async Task<ErrorCodes> UpdateStatusV2Async(long id, NewsStatus status, long userId, Action<VideoSearch, int> actionAutoShare)
        {
            var returnValue = ErrorCodes.UpdateError;
            var statusOld = default(int?);
            var dataDb = default(Video);
            try
            {
                using (var _context = GetContext())
                {
                    
                    var newsAll = default(NewsAll);
                    var resultUpdate = false;
                    var officerIds = default(RedisValue[]);
                    var officerId = 0L;
                    dataDb = await _context.HashGetExAsync<Video>(id, CommandFlags.PreferSlave);
                    statusOld = dataDb.Status;

                    if (dataDb != null)
                    {
                        if (dataDb.Status == (int)status)
                        {
                            returnValue = ErrorCodes.Success;
                        }
                        else
                        {
                            switch (status)
                            {
                                #region xuat ban
                                case NewsStatus.Published:// xuất bản
                                    officerIds = await _context.SetMembersAsync(IndexKeys.KeyAccountInNews(id), CommandFlags.PreferSlave);
                                    if (officerIds != null && officerIds.Count() != 0)
                                    {
                                        if (long.TryParse(officerIds[0], out officerId))
                                        {
                                            var officer = await _context.HashGetExAsync<Account>(officerId, CommandFlags.PreferSlave);

                                            var listMemberId = await _context.SortedSetRangeByScoreAsync(IndexKeys.KeyAccountMemberInOfficer<AccountMember>(officerId), double.NegativeInfinity, double.PositiveInfinity);

                                            var listMember = await _context.GetsFromHashExAsync<AccountMember>(listMemberId?.Select(p => officerId + IndexKeys.SeparateChar + p.ToString())?.ToList(), CommandFlags.PreferSlave);
                                            var accountMember = listMember?.Where(p => p.MemberId == userId).FirstOrDefault();

                                            if (accountMember != null && officer!=null)
                                            {
                                                var isPublisher = await Functions.Function.IsPublisher(accountMember, dataDb.CreatedBy, dataDb.ApprovedBy, officer.Mode, dataDb.Status);
                                                if (isPublisher)
                                                {
                                                    dataDb.Status = (int)status;
                                                    dataDb.ModifiedDate = DateTime.Now;
                                                    dataDb.ModifiedBy = userId.ToString();
                                                    dataDb.DistributionDate = dataDb.DistributionDate > DateTime.Now ? dataDb.DistributionDate : DateTime.Now;

                                                    var oldApprovedBy = dataDb.ApprovedBy;
                                                    dataDb.ApprovedBy = accountMember.MemberId.ToString();
                                                    dataDb.ApprovedDate = DateTime.Now;

                                                    newsAll = Mapper<NewsAll>.MapNewsAll(dataDb, new NewsAll());

                                                    resultUpdate = await _context.MultiAsync((transac) =>
                                                    {
                                                        transac.HashSetAsync(_context.NameOf(dataDb.GetType().Name), dataDb.Id, Json.Stringify(dataDb));
                                                        transac.HashSetAsync(_context.NameOf(newsAll.GetType().Name), newsAll.Id, Json.Stringify(newsAll));

                                                        transac.SortedSetRemoveAsync(_context.NameOf(IndexKeys.KeyStatus<NewsAll>(statusOld)), newsAll.Id);
                                                        transac.SortedSetAddAsync(_context.NameOf(IndexKeys.KeyStatus<NewsAll>(newsAll.Status)), newsAll.Id, (newsAll.DistributionDate ?? DateTime.Now).ConvertToTimestamp10());

                                                        //remove value in set approve in account if exist
                                                        if (!string.IsNullOrEmpty(oldApprovedBy) && long.TryParse(oldApprovedBy, out long oldApprovedById))
                                                        {
                                                            transac.SortedSetRemoveAsync(_context.NameOf(IndexKeys.KeyApproveInAccount<Video>(officerId, oldApprovedById)), dataDb.Id, CommandFlags.PreferSlave);
                                                        }
                                                        else
                                                        {
                                                            //remove value in set approve if exist
                                                            transac.SortedSetRemoveAsync(_context.NameOf(IndexKeys.KeyApprove<Video>(officerId)), dataDb.Id, CommandFlags.PreferSlave);
                                                        }

                                                        //add value into key approved in account
                                                        transac.SortedSetAddAsync(_context.NameOf(IndexKeys.KeyApprovedInAccount<Video>(officerId, userId)), dataDb.Id, DateTime.Now.ConvertToTimestamp10());
                                                    });

                                                    if (resultUpdate) returnValue = ErrorCodes.Success;
                                                }
                                                else
                                                {
                                                    returnValue = ErrorCodes.PermissionPublishInvalid;
                                                }
                                            }
                                            else
                                            {
                                                returnValue = ErrorCodes.PermissionInvalid;
                                            }
                                        }
                                        else
                                        {
                                            returnValue = ErrorCodes.DataInvalid;
                                        }
                                    }
                                    else
                                    {
                                        returnValue = ErrorCodes.DataInvalid;
                                    }
                                    break;
                                #endregion
                                #region Xoa bai
                                case NewsStatus.Remove: // xóa bài
                                    if (statusOld != (int)NewsStatus.Published)
                                    {
                                        if (userId.ToString().Equals(dataDb.CreatedBy))
                                        {
                                            statusOld = dataDb.Status;
                                            dataDb.Status = (int)status;
                                            dataDb.ModifiedDate = DateTime.Now;
                                            dataDb.ModifiedBy = userId.ToString();

                                            newsAll = Mapper<NewsAll>.MapNewsAll(dataDb, new NewsAll());

                                            resultUpdate = await _context.MultiAsync((transac) =>
                                            {
                                                transac.HashSetAsync(_context.NameOf(dataDb.GetType().Name), dataDb.Id, Json.Stringify(dataDb));

                                                transac.HashSetAsync(_context.NameOf(newsAll.GetType().Name), newsAll.Id, Json.Stringify(newsAll));

                                                transac.SortedSetRemoveAsync(_context.NameOf(IndexKeys.KeyStatus<NewsAll>(statusOld)), newsAll.Id);
                                                transac.SortedSetAddAsync(_context.NameOf(IndexKeys.KeyStatus<NewsAll>(newsAll.Status)), newsAll.Id, (dataDb.ModifiedDate ?? DateTime.Now).ConvertToTimestamp10());
                                            });

                                            if (resultUpdate) returnValue = ErrorCodes.Success;
                                        }
                                        else
                                        {
                                            returnValue = ErrorCodes.PermissionUpdateInvalid;
                                        }
                                    }
                                    else
                                    {
                                        // trạng thái cũ là publish
                                        officerIds = await _context.SetMembersAsync(IndexKeys.KeyAccountInNews(id), CommandFlags.PreferSlave);
                                        if (officerIds != null && officerIds.Count() != 0)
                                        {
                                            if (long.TryParse(officerIds[0], out officerId))
                                            {
                                                var officer = await _context.HashGetExAsync<Account>(officerId, CommandFlags.PreferSlave);

                                                var listMemberId = await _context.SortedSetRangeByScoreAsync(IndexKeys.KeyAccountMemberInOfficer<AccountMember>(officerId), double.NegativeInfinity, double.PositiveInfinity);

                                                var listMember = await _context.GetsFromHashExAsync<AccountMember>(listMemberId?.Select(p => officerId + IndexKeys.SeparateChar + p.ToString())?.ToList(), CommandFlags.PreferSlave);
                                                var accountMember = listMember?.Where(p => p.MemberId == userId).FirstOrDefault();

                                                if (accountMember != null && officer!=null)
                                                {
                                                    var isRemover = await Functions.Function.IsRemover(accountMember, dataDb.CreatedBy, dataDb.ApprovedBy, officer.Mode, statusOld);
                                                    if (isRemover)
                                                    {
                                                        dataDb.Status = (int)status;
                                                        dataDb.ModifiedDate = DateTime.Now;
                                                        dataDb.ModifiedBy = userId.ToString();

                                                        newsAll = Mapper<NewsAll>.MapNewsAll(dataDb, new NewsAll());

                                                        resultUpdate = await _context.MultiAsync((transac) =>
                                                        {
                                                            transac.HashSetAsync(_context.NameOf(dataDb.GetType().Name), dataDb.Id, Json.Stringify(dataDb));

                                                            transac.HashSetAsync(_context.NameOf(newsAll.GetType().Name), newsAll.Id, Json.Stringify(newsAll));

                                                            transac.SortedSetRemoveAsync(_context.NameOf(IndexKeys.KeyStatus<NewsAll>(statusOld)), newsAll.Id);
                                                            transac.SortedSetAddAsync(_context.NameOf(IndexKeys.KeyStatus<NewsAll>(newsAll.Status)), newsAll.Id, (dataDb.ModifiedDate ?? DateTime.Now).ConvertToTimestamp10());

                                                            if (long.TryParse(dataDb.ApprovedBy, out long accountId))
                                                            {
                                                                transac.SortedSetRemoveAsync(_context.NameOf(IndexKeys.KeyApprovedInAccount<Video>(officerId, accountId)), dataDb.Id);
                                                            }
                                                        });

                                                        if (resultUpdate) returnValue = ErrorCodes.Success;
                                                    }
                                                    else
                                                    {
                                                        returnValue = ErrorCodes.PermissionUpdateInvalid;
                                                    }
                                                }
                                                else
                                                {
                                                    returnValue = ErrorCodes.PermissionUpdateInvalid;
                                                }
                                            }
                                            else
                                            {
                                                returnValue = ErrorCodes.DataInvalid;
                                            }
                                        }
                                        else
                                        {
                                            returnValue = ErrorCodes.DataInvalid;
                                        }
                                    }

                                    break;
                                #endregion
                                #region Gui cho duyet
                                case NewsStatus.Approve:// Gửi nhờ duyệt
                                    if (dataDb.Status == (int)NewsStatus.Published)
                                    {
                                        returnValue = ErrorCodes.DataInvalid;
                                    }
                                    else
                                    {
                                        if (userId.ToString().Equals(dataDb.CreatedBy))
                                        {
                                            officerIds = await _context.SetMembersAsync(IndexKeys.KeyAccountInNews(id), CommandFlags.PreferSlave);
                                            if (officerIds != null && officerIds.Count() != 0)
                                            {
                                                if (long.TryParse(officerIds[0], out officerId))
                                                {
                                                    dataDb.Status = (int)status;
                                                    dataDb.ModifiedDate = DateTime.Now;
                                                    dataDb.ModifiedBy = userId.ToString();

                                                    newsAll = Mapper<NewsAll>.MapNewsAll(dataDb, new NewsAll());

                                                    resultUpdate = await _context.MultiAsync((transac) =>
                                                    {
                                                        transac.HashSetAsync(_context.NameOf(dataDb.GetType().Name), dataDb.Id, Json.Stringify(dataDb));

                                                        transac.HashSetAsync(_context.NameOf(newsAll.GetType().Name), newsAll.Id, Json.Stringify(newsAll));

                                                        transac.SortedSetRemoveAsync(_context.NameOf(IndexKeys.KeyStatus<NewsAll>(statusOld)), newsAll.Id);
                                                        transac.SortedSetAddAsync(_context.NameOf(IndexKeys.KeyStatus<NewsAll>(newsAll.Status)), newsAll.Id, (dataDb.ModifiedDate ?? DateTime.Now).ConvertToTimestamp10());

                                                        if (string.IsNullOrEmpty(dataDb.ApprovedBy))
                                                        {
                                                            transac.SortedSetAddAsync(_context.NameOf(IndexKeys.KeyApprove<Video>(officerId)), dataDb.Id, DateTime.Now.ConvertToTimestamp10());
                                                        }
                                                        else
                                                        {
                                                            if (long.TryParse(dataDb.ApprovedBy, out long accountId))
                                                            {
                                                                transac.SortedSetAddAsync(_context.NameOf(IndexKeys.KeyApproveInAccount<Video>(officerId, accountId)), dataDb.Id, DateTime.Now.ConvertToTimestamp10());
                                                            }
                                                        }
                                                    });
                                                }
                                            }

                                            if (resultUpdate) returnValue = ErrorCodes.Success;
                                        }
                                        else
                                        {
                                            returnValue = ErrorCodes.PermissionUpdateInvalid;
                                        }
                                    }
                                    break;
                                #endregion
                                #region Default
                                default:
                                    if (userId.ToString().Equals(dataDb.CreatedBy))
                                    {
                                        dataDb.Status = (int)status;
                                        dataDb.ModifiedDate = DateTime.Now;
                                        dataDb.ModifiedBy = userId.ToString();

                                        if (statusOld == (int)NewsStatus.Published)
                                        {
                                            officerIds = await _context.SetMembersAsync(IndexKeys.KeyAccountInNews(id), CommandFlags.PreferSlave);
                                            if (officerIds != null && officerIds.Count() != 0)
                                            {
                                                long.TryParse(officerIds[0], out officerId);
                                            }
                                        }

                                        newsAll = Mapper<NewsAll>.MapNewsAll(dataDb, new NewsAll());

                                        resultUpdate = await _context.MultiAsync((transac) =>
                                        {
                                            transac.HashSetAsync(_context.NameOf(dataDb.GetType().Name), dataDb.Id, Json.Stringify(dataDb));

                                            transac.HashSetAsync(_context.NameOf(newsAll.GetType().Name), newsAll.Id, Json.Stringify(newsAll));

                                            transac.SortedSetRemoveAsync(_context.NameOf(IndexKeys.KeyStatus<NewsAll>(statusOld)), newsAll.Id);
                                            transac.SortedSetAddAsync(_context.NameOf(IndexKeys.KeyStatus<NewsAll>(newsAll.Status)), newsAll.Id, (dataDb.ModifiedDate ?? DateTime.Now).ConvertToTimestamp10());

                                            if (officerId > 0)
                                            {
                                                if (long.TryParse(dataDb.ApprovedBy, out long accountId))
                                                {
                                                    transac.SortedSetRemoveAsync(_context.NameOf(IndexKeys.KeyApprovedInAccount<Video>(officerId, accountId)), dataDb.Id);
                                                }
                                            }
                                        });

                                        if (resultUpdate) returnValue = ErrorCodes.Success;
                                    }
                                    else
                                    {
                                        if (statusOld == (int?)NewsStatus.Published || statusOld == (int?)NewsStatus.Archive || statusOld == (int?)NewsStatus.Approve)
                                        {
                                            officerIds = await _context.SetMembersAsync(IndexKeys.KeyAccountInNews(id), CommandFlags.PreferSlave);
                                            if (officerIds != null && officerIds.Count() != 0)
                                            {
                                                if (long.TryParse(officerIds[0], out officerId))
                                                {
                                                    var officer = await _context.HashGetExAsync<Account>(officerId, CommandFlags.PreferSlave);

                                                    var listMemberId = await _context.SortedSetRangeByScoreAsync(IndexKeys.KeyAccountMemberInOfficer<AccountMember>(officerId), double.NegativeInfinity, double.PositiveInfinity);

                                                    var listMember = await _context.GetsFromHashExAsync<AccountMember>(listMemberId?.Select(p => officerId + IndexKeys.SeparateChar + p.ToString())?.ToList(), CommandFlags.PreferSlave);
                                                    var accountMember = listMember?.Where(p => p.MemberId == userId).FirstOrDefault();

                                                    if (accountMember != null && officer!=null)
                                                    {
                                                        var isRemover = await Functions.Function.IsRemover(accountMember, dataDb.CreatedBy, dataDb.ApprovedBy, officer.Mode, statusOld);
                                                        if (isRemover)
                                                        {
                                                            dataDb.Status = (int)status;
                                                            dataDb.ModifiedDate = DateTime.Now;
                                                            dataDb.ModifiedBy = userId.ToString();

                                                            newsAll = Mapper<NewsAll>.MapNewsAll(dataDb, new NewsAll());

                                                            resultUpdate = await _context.MultiAsync((transac) =>
                                                            {
                                                                transac.HashSetAsync(_context.NameOf(dataDb.GetType().Name), dataDb.Id, Json.Stringify(dataDb));

                                                                transac.HashSetAsync(_context.NameOf(newsAll.GetType().Name), newsAll.Id, Json.Stringify(newsAll));

                                                                transac.SortedSetRemoveAsync(_context.NameOf(IndexKeys.KeyStatus<NewsAll>(statusOld)), newsAll.Id);
                                                                transac.SortedSetAddAsync(_context.NameOf(IndexKeys.KeyStatus<NewsAll>(newsAll.Status)), newsAll.Id, (dataDb.ModifiedDate ?? DateTime.Now).ConvertToTimestamp10());

                                                                if (officerId > 0)
                                                                {
                                                                    if (long.TryParse(dataDb.ApprovedBy, out long accountId))
                                                                    {
                                                                        transac.SortedSetRemoveAsync(_context.NameOf(IndexKeys.KeyApprovedInAccount<Video>(officerId, accountId)), dataDb.Id);
                                                                    }
                                                                }
                                                            });

                                                            if (resultUpdate) returnValue = ErrorCodes.Success;
                                                        }
                                                        else
                                                        {
                                                            returnValue = ErrorCodes.PermissionUpdateInvalid;
                                                        }
                                                    }
                                                    else
                                                    {
                                                        returnValue = ErrorCodes.PermissionUpdateInvalid;
                                                    }
                                                }
                                                else
                                                {
                                                    returnValue = ErrorCodes.DataInvalid;
                                                }
                                            }
                                            else
                                            {
                                                returnValue = ErrorCodes.DataInvalid;
                                            }
                                        }
                                        else
                                        {
                                            returnValue = ErrorCodes.PermissionUpdateInvalid;
                                        }
                                    }

                                    break;
                                    #endregion
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
            }

            if(returnValue == ErrorCodes.Success && dataDb!=null && statusOld != null)
            {
                actionAutoShare(Mapper<VideoSearch>.Map(dataDb, new VideoSearch()), statusOld ?? 0);
            }

            return returnValue;
        }

        public Task<PagingDataResult<Video>> GetListByPlaylistIdAsync(long playlistId, int? pageIndex, int? pageSize)
        {
            var returnValue = Task.FromResult(default(PagingDataResult<Video>));
            try
            {
                using (var _context = GetContext())
                {
                    returnValue = _context.ExecAsync(async (db) =>
                    {
                        var result = new PagingDataResult<Video>();
                        try
                        {
                            var listId = await db.SortedSetRangeByRankAsync(_context.NameOf(IndexKeys.KeyVideoInPlaylist<VideoInPlaylist>(playlistId)), ((pageIndex - 1) * pageSize) ?? 0, pageSize ?? 20, Order.Descending, CommandFlags.PreferSlave);
                            result.Total = (int)await db.SortedSetLengthAsync(_context.NameOf(IndexKeys.KeyVideoInPlaylist<VideoInPlaylist>(playlistId)));
                            result.Data = (await db.HashGetAsync(_context.NameOf<Video>(), listId?.ToArray()))?.Select(p => Json.Parse<Video>(p))?.ToList();
                        }
                        catch (Exception ex)
                        {
                            Logger.Error(ex, ex.Message);
                        }

                        return result;
                    });
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
            }
            return returnValue;
        }

        public async Task<ErrorCodes> UpdateAsync(Video data, long officerId)
        {
            var returnValue = ErrorCodes.UpdateError;
            try
            {
                using (var _context = GetContext())
                {
                    var approverOld = string.Empty;
                    var videoDb = await _context.HashGetExAsync<Video>(data.Id);

                    if (videoDb != null)
                    {
                        var isModifier = await Functions.Function.IsModifier(officerId, data.Id, data.ModifiedBy, videoDb.CreatedBy, videoDb.Status, _context);

                        if (isModifier)
                        {
                            approverOld = videoDb.ApprovedBy;

                            videoDb.Url = data.Url;
                            videoDb.OriginalUrl = data.OriginalUrl;
                            videoDb.Author = data.Author;
                            videoDb.CategoryId = data.CategoryId;
                            videoDb.Location = data.Location;
                            videoDb.PublishMode = data.PublishMode;
                            videoDb.PublishData = data.PublishData;
                            videoDb.DistributionDate = data.DistributionDate.HasValue && data.DistributionDate >= DateTime.Now ? data.DistributionDate : videoDb.DistributionDate;
                            videoDb.ModifiedDate = data.ModifiedDate;
                            videoDb.ModifiedBy = data.ModifiedBy;
                            videoDb.Tags = data.Tags;
                            videoDb.Name = data.Name;
                            videoDb.UnsignName = data.UnsignName;
                            videoDb.Description = data.Description;
                            videoDb.HtmlCode = data.HtmlCode;
                            videoDb.KeyVideo = data.KeyVideo;
                            videoDb.UploadedDate = data.UploadedDate;
                            videoDb.Source = data.Source;
                            videoDb.VideoRelation = data.VideoRelation;
                            videoDb.FileName = data.FileName;
                            videoDb.Duration = data.Duration;
                            videoDb.Size = data.Size;
                            videoDb.Capacity = data.Capacity;
                            videoDb.AllowAdv = data.AllowAdv ?? false;
                            videoDb.IsRemovedLogo = data.IsRemovedLogo ?? false;
                            videoDb.IsConverted = data.IsConverted ?? false;
                            videoDb.PolicyContentId = data.PolicyContentId;
                            videoDb.OriginalId = data.OriginalId;
                            videoDb.Trailer = data.Trailer;
                            videoDb.MetaAvatar = data.MetaAvatar;
                            videoDb.DisplayStyle = data.DisplayStyle;
                            videoDb.MetaData = data.MetaData;

                            if (videoDb.Status != (int)NewsStatus.Published)
                            {
                                videoDb.ApprovedBy = data.ApprovedBy;
                            }
                            videoDb.CommentMode = data.CommentMode;

                            var newsAll = Mapper<NewsAll>.MapNewsAll(videoDb, new NewsAll());

                            var returnValueUpdate = await _context.MultiAsync((transac) =>
                            {
                                transac.HashSetAsync(_context.NameOf(videoDb.GetType().Name), videoDb.Id, Json.Stringify(videoDb));
                                transac.HashSetAsync(_context.NameOf(newsAll.GetType().Name), newsAll.Id, Json.Stringify(newsAll));

                                if (videoDb.Status == (int)NewsStatus.Approve)
                                {
                                    if (long.TryParse(approverOld, out long _approverOld))
                                        transac.SortedSetRemoveAsync(_context.NameOf(IndexKeys.KeyApproveInAccount<Video>(officerId, _approverOld)), videoDb.Id);

                                    if (!string.IsNullOrEmpty(videoDb.ApprovedBy))
                                    {
                                        if (long.TryParse(videoDb.ApprovedBy, out long accountId))
                                        {
                                            transac.SortedSetAddAsync(_context.NameOf(IndexKeys.KeyApproveInAccount<Video>(officerId, accountId)), videoDb.Id, DateTime.Now.ConvertToTimestamp10());
                                        }
                                    }
                                }
                            });

                            if (returnValueUpdate) returnValue = ErrorCodes.Success;
                        }
                        else
                        {
                            returnValue = ErrorCodes.PermissionUpdateInvalid;
                        }
                    }
                    else
                    {
                        returnValue = ErrorCodes.DataNotExist;
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
                returnValue = ErrorCodes.Exception;
            }
            return returnValue;
        }

        public async Task<ErrorCodes> UpdateV2Async(Video data, long officerId, Action<int> actionAuto)
        {
            var returnValue = ErrorCodes.UpdateError;
            var statusOld = default(int?);            
            try
            {
                using (var _context = GetContext())
                {
                    var approverOld = string.Empty;
                    var videoDb = await _context.HashGetExAsync<Video>(data.Id, CommandFlags.PreferSlave);                    

                    if (videoDb != null)
                    {
                        statusOld = videoDb.Status;

                        var isModifier = await Functions.Function.IsModifier(officerId, data.Id, data.ModifiedBy, videoDb.CreatedBy, videoDb.Status, _context);

                        if (isModifier)
                        {
                            approverOld = videoDb.ApprovedBy;

                            videoDb.Url = data.Url;
                            videoDb.OriginalUrl = data.OriginalUrl;
                            videoDb.Author = data.Author;
                            videoDb.CategoryId = data.CategoryId;
                            videoDb.Location = data.Location;
                            videoDb.PublishMode = data.PublishMode;
                            videoDb.PublishData = data.PublishData;
                            videoDb.DistributionDate = data.DistributionDate.HasValue && data.DistributionDate >= DateTime.Now ? data.DistributionDate : videoDb.DistributionDate;
                            videoDb.ModifiedDate = data.ModifiedDate;
                            videoDb.ModifiedBy = data.ModifiedBy;
                            videoDb.Tags = data.Tags;
                            videoDb.Name = data.Name;
                            videoDb.UnsignName = data.UnsignName;
                            videoDb.Description = data.Description;
                            videoDb.HtmlCode = data.HtmlCode;
                            videoDb.KeyVideo = data.KeyVideo;
                            videoDb.UploadedDate = data.UploadedDate;
                            videoDb.Source = data.Source;
                            videoDb.VideoRelation = data.VideoRelation;
                            videoDb.FileName = data.FileName;
                            videoDb.Duration = data.Duration;
                            videoDb.Size = data.Size;
                            videoDb.Capacity = data.Capacity;
                            videoDb.AllowAdv = data.AllowAdv ?? false;
                            videoDb.IsRemovedLogo = data.IsRemovedLogo ?? false;
                            videoDb.IsConverted = data.IsConverted ?? false;
                            videoDb.PolicyContentId = data.PolicyContentId;
                            videoDb.OriginalId = data.OriginalId;
                            videoDb.Trailer = data.Trailer;
                            videoDb.MetaAvatar = data.MetaAvatar;
                            videoDb.DisplayStyle = data.DisplayStyle;
                            videoDb.MetaData = data.MetaData;

                            if (videoDb.Status != (int)NewsStatus.Published)
                            {
                                videoDb.ApprovedBy = data.ApprovedBy;
                            }
                            videoDb.CommentMode = data.CommentMode;

                            var newsAll = Mapper<NewsAll>.MapNewsAll(videoDb, new NewsAll());

                            var returnValueUpdate = await _context.MultiAsync((transac) =>
                            {
                                transac.HashSetAsync(_context.NameOf(videoDb.GetType().Name), videoDb.Id, Json.Stringify(videoDb));
                                transac.HashSetAsync(_context.NameOf(newsAll.GetType().Name), newsAll.Id, Json.Stringify(newsAll));

                                if (videoDb.Status == (int)NewsStatus.Approve)
                                {
                                    if (long.TryParse(approverOld, out long _approverOld))
                                        transac.SortedSetRemoveAsync(_context.NameOf(IndexKeys.KeyApproveInAccount<Video>(officerId, _approverOld)), videoDb.Id);

                                    if (!string.IsNullOrEmpty(videoDb.ApprovedBy))
                                    {
                                        if (long.TryParse(videoDb.ApprovedBy, out long accountId))
                                        {
                                            transac.SortedSetAddAsync(_context.NameOf(IndexKeys.KeyApproveInAccount<Video>(officerId, accountId)), videoDb.Id, DateTime.Now.ConvertToTimestamp10());
                                        }
                                    }
                                }
                            });

                            if (returnValueUpdate) returnValue = ErrorCodes.Success;
                        }
                        else
                        {
                            returnValue = ErrorCodes.PermissionUpdateInvalid;
                        }
                    }
                    else
                    {
                        returnValue = ErrorCodes.DataNotExist;
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
                returnValue = ErrorCodes.Exception;
            }
            if (returnValue == ErrorCodes.Success && statusOld != null)
            {
                actionAuto(statusOld ?? 0);
            }
            return returnValue;
        }


        public Task<List<Video>> GetMultiVideoAsync(List<string> listKey)
        {
            var returnValue = Task.FromResult(default(List<Video>));
            try
            {
                using (var _context = GetContext())
                {
                    returnValue = _context.GetsFromHashExAsync<Video>(listKey, CommandFlags.PreferSlave);
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
            }
            return returnValue;
        }

        public Task<List<Video>> GetListByIdsAsync(List<string> ids)
        {
            var returnValue = Task.FromResult(default(List<Video>));
            try
            {
                using (var _context = GetContext())
                {
                    returnValue = _context.GetsFromHashExAsync<Video>(ids, CommandFlags.PreferSlave);
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
            }
            return returnValue;
        }


        public Task<List<VideoSearch>> GetListVideoSearchByIdsAsync(List<string> ids, PagingDataResult<VideoSearch> listData)
        {
            var returnValue = Task.FromResult(default(List<VideoSearch>));
            try
            {
                using (var _context = GetContext())
                {
                    returnValue = _context.ExecAsync(async (db) =>
                    {
                        var result = new List<VideoSearch>();
                        try
                        {
                            var listValue = (await db.HashGetAsync(_context.NameOf<Video>(), ids?.Select(p => (RedisValue)p)?.ToArray(), CommandFlags.PreferSlave))?.Select(p => Json.Parse<Video>(p))?.ToList();
                            if (listValue != null)
                            {
                                var listKey = new List<string>();
                                result = listValue.Select(p => Mapper<VideoSearch>.Map(p, new VideoSearch()))?.ToList();
                                result?.ForEach(p =>
                                {
                                    p.NewsDistribution = listData?.Data?.Where(c => c.Id == p.Id)?.FirstOrDefault()?.NewsDistribution;
                                    listKey.AddRange(p.NewsDistribution?.Select(s => s.DistributorId.ToString()));
                                });
                                var listDistributor = (await db.HashGetAsync(_context.NameOf<Distributor>(), listKey?.Select(p => (RedisValue)p)?.ToArray(), CommandFlags.PreferSlave))?.Select(p => Json.Parse<Distributor>(p));
                                result?.ForEach(p =>
                                {
                                    foreach (var item in p.NewsDistribution)
                                    {
                                        item.DistributorName = listDistributor?.Where(d => d.Id == item.DistributorId)?.FirstOrDefault()?.Name;
                                    }
                                });
                            }
                        }
                        catch (Exception ex)
                        {
                            Logger.Error(ex, ex.Message);
                        }

                        return result;
                    });
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
            }
            return returnValue;
        }

        public List<VideoSearch> GetListVideoSearchByIds(List<string> ids, PagingDataResult<VideoSearch> listData)
        {
            var returnValue = new List<VideoSearch>();
            try
            {
                using (var _context = GetContext())
                {
                    var listValue = _context.GetsFromHash<Video>(ids);
                    if (listValue != null)
                    {
                        var listKey = new List<string>();
                        returnValue = listValue.Select(p => Mapper<VideoSearch>.Map(p, new VideoSearch()))?.ToList();
                        returnValue?.ForEach(p =>
                        {
                            p.NewsDistribution = listData?.Data?.Where(c => c.Id == p.Id)?.FirstOrDefault()?.NewsDistribution;
                            listKey.AddRange(p.NewsDistribution?.Select(s => s.DistributorId.ToString()));
                        });
                        var listDistributor = _context.GetsFromHash<Distributor>(listKey);
                        returnValue?.ForEach(p =>
                        {
                            foreach (var item in p.NewsDistribution)
                            {
                                item.DistributorName = listDistributor?.Where(d => d.Id == item.DistributorId)?.FirstOrDefault()?.Name;
                            }
                        });
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
            }
            return returnValue;
        }


        public Task<PagingDataResult<VideoInPlaylistSearchReturn>> SearchVideoAsync(SearchNews data, long userId, Action<string> action)
        {
            var returnValue = Task.FromResult(default(PagingDataResult<VideoInPlaylistSearchReturn>));
            try
            {
                using (var _context = GetContext())
                {
                    returnValue = _context.ExecAsync(async (db) =>
                    {
                        var result = new PagingDataResult<VideoInPlaylistSearchReturn>
                        {
                            Data = new List<VideoInPlaylistSearchReturn>()
                        };
                        try
                        {
                            var listKey = new RedisValue[] { };
                            var listVideo = new List<Video>();
                            var total = default(long);
                            var tempKey = _context.NameOf("index:temp:union:" + Generator.SorterSetSearchId());
                            var createdByKey = _context.NameOf(IndexKeys.KeyCreatedBy<NewsAll>(userId.ToString()));
                            var listkeyFull = new List<RedisKey>();
                            if (userId != data.OfficerId)
                                listkeyFull.Add(createdByKey);
                            listkeyFull.Add(_context.NameOf(IndexKeys.KeyNewsInAccount(data.OfficerId)));
                            listkeyFull.Add(_context.NameOf(IndexKeys.KeyType<NewsAll>((int)NewsType.Video)));
                            switch (string.IsNullOrEmpty(data.Status))
                            {
                                case true:
                                    if (data.DistributionId == -1)
                                    {
                                        listkeyFull.Add(_context.NameOf(IndexKeys.KeyDistributorid<NewsDistribution>()));
                                    }
                                    else
                                    {
                                        if (data.DistributionId > 0)
                                        {
                                            listkeyFull.Add(_context.NameOf(IndexKeys.KeyDistributorid<NewsDistribution>(data.DistributionId)));
                                        }
                                    }
                                    var value1 = await db.SortedSetCombineAndStoreAsync(SetOperation.Intersect, tempKey, listkeyFull.ToArray(), null, Aggregate.Max, CommandFlags.None);
                                    if (value1 > 0)
                                    {
                                        try
                                        {
                                            listKey = (await db.SortedSetRangeByScoreAsync(tempKey
                                            , Utility.ConvertToTimestamp10(data.FromDate ?? DateTime.MinValue)
                                            , Utility.ConvertToTimestamp10(data.ToDate ?? DateTime.Now)
                                            , Exclude.None
                                            , data.OrderBy == SearchNewsOrderBy.AscCreatedDate ? Order.Ascending : Order.Descending
                                            , ((data.PageIndex ?? 1) - 1) * (data.PageSize ?? 20)
                                            , data.PageSize ?? 20
                                            , CommandFlags.None));

                                            total = await db.SortedSetLengthAsync(tempKey
                                                , Utility.ConvertToTimestamp10(data.FromDate ?? DateTime.MinValue)
                                                , Utility.ConvertToTimestamp10(data.ToDate ?? DateTime.Now)
                                                , Exclude.None
                                                , CommandFlags.None);

                                            result.Total = (int)total;
                                            listVideo = listKey != null && listKey.Length > 0 ? (await db.HashGetAsync(_context.NameOf<Video>(), listKey, CommandFlags.PreferSlave))?.Select(p => Json.Parse<Video>(p))?.ToList() : listVideo;
                                            if (listVideo != null)
                                            {
                                                foreach (var video in listVideo)
                                                {
                                                    var videoSearchReturn = Mapper<VideoInPlaylistSearchReturn>.Map(video, new VideoInPlaylistSearchReturn());
                                                    try
                                                    {
                                                        var newsDistribution = await db.SortedSetRangeByScoreWithScoresAsync(_context.NameOf(IndexKeys.KeyNewsDistribution(video.Id)), double.MinValue, double.MaxValue, Exclude.None, Order.Ascending, 0, -1, CommandFlags.PreferSlave);
                                                        var listTmp = new List<NewsDistribution>();
                                                        if (newsDistribution != null)
                                                        {
                                                            foreach (var item in newsDistribution)
                                                            {
                                                                if (long.TryParse(item.Element.ToString()?.Split(IndexKeys.SeparateChar)?.FirstOrDefault(), out long distributorId))
                                                                {
                                                                    listTmp.Add(new NewsDistribution
                                                                    {
                                                                        DistributorId = distributorId,
                                                                        SharedDate = Utility.ConvertSecondsToDate(item.Score),
                                                                        DistributorName = Json.Parse<Distributor>(await db.HashGetAsync(_context.NameOf<Distributor>(), distributorId, CommandFlags.PreferSlave))?.Name
                                                                    });
                                                                }
                                                            }
                                                        }
                                                        videoSearchReturn.NewsDistribution = listTmp.ToArray();
                                                    }
                                                    catch (Exception exx)
                                                    {
                                                        Logger.Error(exx, exx.Message);
                                                    }
                                                    result.Data.Add(videoSearchReturn);
                                                }
                                            }
                                        }
                                        catch (Exception ex)
                                        {
                                            Logger.Error(ex, ex.Message);
                                        }
                                        action(tempKey);
                                    }
                                    break;
                                case false:
                                    if (data.DistributionId == -1)
                                    {
                                        listkeyFull.Add(_context.NameOf(IndexKeys.KeyDistributorid<NewsDistribution>()));
                                    }
                                    else
                                    {
                                        if (data.DistributionId > 0)
                                        {
                                            listkeyFull.Add(_context.NameOf(IndexKeys.KeyDistributorid<NewsDistribution>(data.DistributionId)));
                                        }
                                    }

                                    var listRedisKeyStatus = new List<RedisKey>();
                                    var listStrStatus = data.Status.Split(",");
                                    foreach (var s in listStrStatus)
                                    {
                                        if (int.TryParse(s, out int status))
                                        {
                                            if (Enum.IsDefined(typeof(NewsStatus), status))
                                            {
                                                listRedisKeyStatus.Add(_context.NameOf(IndexKeys.KeyType<NewsAll>(status)));
                                            }
                                        }
                                    }
                                    var tempKey2 = _context.NameOf("index:temp:" + Generator.SorterSetSearchId());
                                    var values2 = await db.SortedSetCombineAndStoreAsync(SetOperation.Union
                                            , tempKey2, listRedisKeyStatus.ToArray(), null, Aggregate.Max, CommandFlags.None);
                                    if (values2 > 0)
                                    {
                                        try
                                        {
                                            listkeyFull.Add(tempKey2);
                                            var value3 = await db.SortedSetCombineAndStoreAsync(SetOperation.Intersect
                                                , tempKey, listkeyFull.ToArray(), null, Aggregate.Max, CommandFlags.None);
                                            if (value3 > 0)
                                            {
                                                try
                                                {
                                                    total = await db.SortedSetLengthAsync(tempKey
                                                    , Utility.ConvertToTimestamp10(data.FromDate ?? DateTime.MinValue)
                                                    , Utility.ConvertToTimestamp10(data.ToDate ?? DateTime.Now)
                                                    , Exclude.None
                                                    , CommandFlags.None);
                                                    result.Total = (int)total;

                                                    listKey = (await db.SortedSetRangeByScoreAsync(tempKey
                                                    , Utility.ConvertToTimestamp10(data.FromDate ?? DateTime.MinValue)
                                                    , Utility.ConvertToTimestamp10(data.ToDate ?? DateTime.Now)
                                                    , Exclude.None
                                                    , data.OrderBy == SearchNewsOrderBy.AscCreatedDate ? Order.Ascending : Order.Descending
                                                    , ((data.PageIndex ?? 1) - 1) * (data.PageSize ?? 20)
                                                    , data.PageSize ?? 20
                                                    , CommandFlags.None));

                                                    listVideo = listKey != null && listKey.Length > 0 ? (await db.HashGetAsync(_context.NameOf<Video>(), listKey, CommandFlags.PreferSlave))?.Select(p => Json.Parse<Video>(p))?.ToList() : listVideo;
                                                    if (listVideo != null)
                                                    {
                                                        foreach (var video in listVideo)
                                                        {
                                                            var videoSearchReturn = Mapper<VideoInPlaylistSearchReturn>.Map(video, new VideoInPlaylistSearchReturn());
                                                            try
                                                            {
                                                                var newsDistribution = await db.SortedSetRangeByScoreWithScoresAsync(_context.NameOf(IndexKeys.KeyNewsDistribution(video.Id)), double.MinValue, double.MaxValue, Exclude.None, Order.Ascending, 0, -1, CommandFlags.PreferSlave);
                                                                var listTmp = new List<NewsDistribution>();
                                                                if (newsDistribution != null)
                                                                {
                                                                    foreach (var item in newsDistribution)
                                                                    {
                                                                        if (long.TryParse(item.Element.ToString()?.Split(IndexKeys.SeparateChar)?.FirstOrDefault(), out long distributorId))
                                                                        {
                                                                            listTmp.Add(new NewsDistribution
                                                                            {
                                                                                DistributorId = distributorId,
                                                                                SharedDate = Utility.ConvertSecondsToDate(item.Score),
                                                                                DistributorName = Json.Parse<Distributor>(await db.HashGetAsync(_context.NameOf<Distributor>(), distributorId, CommandFlags.PreferSlave))?.Name
                                                                            });
                                                                        }
                                                                    }
                                                                }
                                                                videoSearchReturn.NewsDistribution = listTmp.ToArray();
                                                            }
                                                            catch (Exception exx)
                                                            {
                                                                Logger.Error(exx, exx.Message);
                                                            }

                                                            result.Data.Add(videoSearchReturn);
                                                        }
                                                    }
                                                }
                                                catch (Exception ex)
                                                {
                                                    Logger.Error(ex, ex.Message);
                                                }
                                                action(tempKey);
                                            }
                                        }
                                        catch (Exception ex)
                                        {
                                            Logger.Error(ex, ex.Message);
                                        }
                                        action(tempKey2);
                                    }
                                    break;
                            }
                        }
                        catch (Exception ex)
                        {
                            Logger.Error(ex, ex.Message);
                        }

                        return result;
                    });
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
            }
            return returnValue;
        }


        public Task<PagingDataResult<VideoSearchReturn>> SearchAsync(SearchNews data, long userId, Action<string> action)
        {
            var returnValue = Task.FromResult(default(PagingDataResult<VideoSearchReturn>));
            try
            {
                using (var _context = GetContext())
                {
                    returnValue = _context.ExecAsync(async (db) =>
                    {
                        //var listMemberId = await db.SortedSetRangeByRankAsync(_context.NameOf(IndexKeys.KeyAccountMemberInOfficer<AccountMember>(data.OfficerId)), 0, -1, Order.Descending, CommandFlags.PreferSlave);
                        //var memberValid = listMemberId?.Where(p => p.ToString().Equals(userId.ToString()))?.Count() > 0;
                        var member = Json.Parse<AccountMember>(await db.HashGetAsync(_context.NameOf<AccountMember>(), data.OfficerId + IndexKeys.SeparateChar + userId, CommandFlags.PreferSlave));
                        if (member!= null)
                        {
                            var result = new PagingDataResult<VideoSearchReturn>
                            {
                                Data = new List<VideoSearchReturn>()
                            };

                            try
                            {
                                var listKey = new RedisValue[] { };
                                var listVideo = new List<Video>();
                                var total = default(long);
                                var tempKey = _context.NameOf("index:temp:union:" + Generator.SorterSetSearchId());
                                var createdByKey = _context.NameOf(IndexKeys.KeyCreatedBy<NewsAll>(userId.ToString()));
                                var tempKeyCreatedBy = _context.NameOf("index:temp:createdby:" + Generator.SorterSetSearchId());
                                var listkeyFull = new List<RedisKey>();
                                var listCreatedBy = new List<RedisKey>();
                                var value4 = 0L;

                                if (data.Status.Equals(((int)NewsStatus.Published).ToString()) && member.Permissions != null && (member.Permissions.Contains(MemberPermission.ModifyPostOnPage) || member.Permissions.Contains(MemberPermission.RemovePostOnPage)))
                                {

                                }
                                else
                                {
                                    var listOwner = await _context.SortedSetRangeByRankAsync(IndexKeys.KeyOwnerOfficer(userId), 0, -1, Order.Descending, CommandFlags.PreferSlave);
                                    if (listOwner == null || !listOwner.Contains((RedisValue)data.OfficerId))
                                    {
                                        //listkeyFull.Add(createdByKey);
                                        listCreatedBy.Add(createdByKey);

                                        //Lấy cả những bài chỉ duyệt

                                        if (data.Status?.Split(",")?.Contains(((int)NewsStatus.Published).ToString()) ?? false)
                                        {
                                            listCreatedBy.Add(_context.NameOf(IndexKeys.KeyApprovedInAccount<Video>(data.OfficerId, userId)));
                                        }

                                        value4 = await db.SortedSetCombineAndStoreAsync(SetOperation.Union
                                        , tempKeyCreatedBy, listCreatedBy.ToArray(), null, Aggregate.Max, CommandFlags.None);

                                        if (value4 > 0)
                                        {
                                            listkeyFull.Add(tempKeyCreatedBy);
                                        }
                                    }
                                    else
                                    {
                                        if ((data.Status?.Split(",")?.Contains(((int)NewsStatus.Approve).ToString()) ?? false) || (data.Status?.Split(",")?.Contains(((int)NewsStatus.Draft).ToString()) ?? false) || (data.Status?.Split(",")?.Contains(((int)NewsStatus.Remove).ToString()) ?? false))
                                        {
                                            listkeyFull.Add(createdByKey);
                                        }
                                    }
                                }

                                try
                                {
                                    listkeyFull.Add(_context.NameOf(IndexKeys.KeyNewsInAccount(data.OfficerId)));
                                    listkeyFull.Add(_context.NameOf(IndexKeys.KeyType<NewsAll>((int)NewsType.Video)));
                                    switch (string.IsNullOrEmpty(data.Status))
                                    {
                                        case true:

                                            if (data.DistributionId == -1)
                                            {
                                                listkeyFull.Add(_context.NameOf(IndexKeys.KeyDistributorid<NewsDistribution>()));
                                            }
                                            else
                                            {
                                                if (data.DistributionId > 0)
                                                {
                                                    listkeyFull.Add(_context.NameOf(IndexKeys.KeyDistributorid<NewsDistribution>(data.DistributionId)));
                                                }
                                            }
                                            var value1 = await db.SortedSetCombineAndStoreAsync(SetOperation.Intersect, tempKey, listkeyFull.ToArray(), null, Aggregate.Max, CommandFlags.None);
                                            if (value1 > 0)
                                            {
                                                try
                                                {
                                                    listKey = (await db.SortedSetRangeByScoreAsync(tempKey
                                                    , Utility.ConvertToTimestamp10(data.FromDate ?? DateTime.MinValue)
                                                    , Utility.ConvertToTimestamp10(data.ToDate ?? DateTime.Now)
                                                    , Exclude.None
                                                    , data.OrderBy == SearchNewsOrderBy.AscCreatedDate ? Order.Ascending : Order.Descending
                                                    , ((data.PageIndex ?? 1) - 1) * (data.PageSize ?? 20)
                                                    , data.PageSize ?? 20
                                                    , CommandFlags.None));

                                                    total = await db.SortedSetLengthAsync(tempKey
                                                        , Utility.ConvertToTimestamp10(data.FromDate ?? DateTime.MinValue)
                                                        , Utility.ConvertToTimestamp10(data.ToDate ?? DateTime.Now)
                                                        , Exclude.None
                                                        , CommandFlags.None);

                                                    result.Total = (int)total;
                                                    listVideo = listKey != null && listKey.Length > 0 ? (await db.HashGetAsync(_context.NameOf<Video>(), listKey, CommandFlags.PreferSlave))?.Select(p => Json.Parse<Video>(p))?.ToList() : listVideo;
                                                    if (listVideo != null)
                                                    {
                                                        foreach (var video in listVideo)
                                                        {
                                                            var videoSearchReturn = Mapper<VideoSearchReturn>.Map(video, new VideoSearchReturn());
                                                            var listTmp = new List<NewsDistribution>(); //videoSearchReturn.NewsDistribution = new NewsDistribution[] { };
                                                            try
                                                            {
                                                                var newsDistribution = await db.SortedSetRangeByScoreWithScoresAsync(_context.NameOf(IndexKeys.KeyNewsDistribution(video.Id)), double.MinValue
                                                                    , double.MaxValue, Exclude.None, Order.Ascending, 0, -1, CommandFlags.PreferSlave);
                                                                if (newsDistribution != null)
                                                                {
                                                                    foreach (var item in newsDistribution)
                                                                    {
                                                                        if (long.TryParse(item.Element.ToString()?.Split(IndexKeys.SeparateChar)?.FirstOrDefault(), out long distributorId))
                                                                        {
                                                                            listTmp.Add(new NewsDistribution
                                                                            {
                                                                                DistributorId = distributorId,
                                                                                SharedDate = Utility.ConvertSecondsToDate(item.Score),
                                                                                DistributorName = Json.Parse<Distributor>(await db.HashGetAsync(_context.NameOf<Distributor>(), distributorId, CommandFlags.PreferSlave))?.Name
                                                                            });
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                            catch (Exception exx)
                                                            {
                                                                Logger.Error(exx, exx.Message);
                                                            }
                                                            videoSearchReturn.NewsDistribution = listTmp.ToArray();
                                                            result.Data.Add(videoSearchReturn);
                                                        }
                                                    }
                                                }
                                                catch (Exception ex)
                                                {
                                                    Logger.Error(ex, ex.Message);
                                                }
                                                action(tempKey);
                                            }
                                            break;
                                        case false:
                                            if (data.DistributionId == -1)
                                            {
                                                listkeyFull.Add(_context.NameOf(IndexKeys.KeyDistributorid<NewsDistribution>()));
                                            }
                                            else
                                            {
                                                if (data.DistributionId > 0)
                                                {
                                                    listkeyFull.Add(_context.NameOf(IndexKeys.KeyDistributorid<NewsDistribution>(data.DistributionId)));
                                                }
                                            }

                                            var listRedisKeyStatus = new List<RedisKey>();
                                            var listStrStatus = data.Status.Split(",");
                                            foreach (var s in listStrStatus)
                                            {
                                                if (int.TryParse(s, out int status))
                                                {
                                                    if (Enum.IsDefined(typeof(NewsStatus), status))
                                                    {
                                                        listRedisKeyStatus.Add(_context.NameOf(IndexKeys.KeyStatus<NewsAll>(status)));
                                                    }
                                                }
                                            }

                                            var tempKey2 = _context.NameOf("index:temp:" + Generator.SorterSetSearchId());
                                            var values2 = await db.SortedSetCombineAndStoreAsync(SetOperation.Union
                                                    , tempKey2, listRedisKeyStatus.ToArray(), null, Aggregate.Max, CommandFlags.None);
                                            if (values2 > 0)
                                            {
                                                try
                                                {
                                                    listkeyFull.Add(tempKey2);
                                                    var value3 = await db.SortedSetCombineAndStoreAsync(SetOperation.Intersect
                                                         , tempKey, listkeyFull.ToArray(), null, Aggregate.Max, CommandFlags.None);
                                                    if (value3 > 0)
                                                    {
                                                        try
                                                        {
                                                            total = await db.SortedSetLengthAsync(tempKey
                                                            , Utility.ConvertToTimestamp10(data.FromDate ?? DateTime.MinValue)
                                                            , Utility.ConvertToTimestamp10(data.ToDate ?? DateTime.Now)
                                                            , Exclude.None
                                                            , CommandFlags.None);
                                                            result.Total = (int)total;

                                                            listKey = (await db.SortedSetRangeByScoreAsync(tempKey
                                                            , Utility.ConvertToTimestamp10(data.FromDate ?? DateTime.MinValue)
                                                            , Utility.ConvertToTimestamp10(data.ToDate ?? DateTime.Now)
                                                            , Exclude.None
                                                            , data.OrderBy == SearchNewsOrderBy.AscCreatedDate ? Order.Ascending : Order.Descending
                                                            , ((data.PageIndex ?? 1) - 1) * (data.PageSize ?? 20)
                                                            , data.PageSize ?? 20
                                                            , CommandFlags.None));

                                                            listVideo = listKey != null && listKey.Length > 0 ? (await db.HashGetAsync(_context.NameOf<Video>(), listKey, CommandFlags.PreferSlave))?.Select(p => Json.Parse<Video>(p))?.ToList() : listVideo;
                                                            if (listVideo != null)
                                                            {
                                                                foreach (var video in listVideo)
                                                                {
                                                                    var videoSearchReturn = Mapper<VideoSearchReturn>.Map(video, new VideoSearchReturn());
                                                                    var listTmp = new List<NewsDistribution>(); //videoSearchReturn.NewsDistribution = new NewsDistribution[] { };
                                                                    try
                                                                    {
                                                                        var newsDistribution = await db.SortedSetRangeByScoreWithScoresAsync(_context.NameOf(IndexKeys.KeyNewsDistribution(video.Id)), double.MinValue, double.MaxValue, Exclude.None, Order.Ascending, 0, -1, CommandFlags.PreferSlave);
                                                                        if (newsDistribution != null)
                                                                        {
                                                                            foreach (var item in newsDistribution)
                                                                            {
                                                                                if (long.TryParse(item.Element.ToString()?.Split(IndexKeys.SeparateChar)?.FirstOrDefault(), out long distributorId))
                                                                                {
                                                                                    listTmp.Add(new NewsDistribution
                                                                                    {
                                                                                        DistributorId = distributorId,
                                                                                        SharedDate = Utility.ConvertSecondsToDate(item.Score),
                                                                                        DistributorName = Json.Parse<Distributor>(await db.HashGetAsync(_context.NameOf<Distributor>(), distributorId, CommandFlags.PreferSlave))?.Name
                                                                                    });
                                                                                }
                                                                            }
                                                                        }
                                                                    }
                                                                    catch (Exception exx)
                                                                    {
                                                                        Logger.Error(exx, exx.Message);
                                                                    }
                                                                    videoSearchReturn.NewsDistribution = listTmp.ToArray();
                                                                    result.Data.Add(videoSearchReturn);
                                                                }
                                                            }
                                                        }
                                                        catch (Exception ex)
                                                        {
                                                            Logger.Error(ex, ex.Message);
                                                        }
                                                        action(tempKey);
                                                    }
                                                }
                                                catch (Exception ex)
                                                {
                                                    Logger.Error(ex, ex.Message);
                                                }
                                                action(tempKey2);
                                            }
                                            break;
                                    }
                                }
                                catch (Exception ex)
                                {
                                    Logger.Error(ex, ex.Message);
                                }

                                if (value4 > 0)
                                {
                                    action(tempKeyCreatedBy);
                                }
                            }
                            catch (Exception ex)
                            {
                                Logger.Error(ex, ex.Message);
                            }

                            return result;
                        }
                        else
                        {
                            return new PagingDataResult<VideoSearchReturn>();
                        }
                    });
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
            }
            return returnValue;
        }

        public Task<PagingDataResult<Video>> GetListApproveAsync(SearchNews data, long accountId, Action<string> action)
        {
            var returnValue = Task.FromResult(default(PagingDataResult<Video>));
            try
            {
                using (var _context = GetContext())
                {
                    returnValue = _context.ExecAsync(async (db) =>
                    {
                        var listMemberId = await db.SortedSetRangeByRankAsync(_context.NameOf(IndexKeys.KeyAccountMemberInOfficer<AccountMember>(data.OfficerId)), 0, -1, Order.Descending, CommandFlags.PreferSlave);
                        var memberValid = listMemberId?.Where(p => p.ToString().Equals(accountId.ToString()))?.Count() > 0;

                        if(memberValid == true)
                        {
                            var result = new PagingDataResult<Video>()
                            {
                                Data = new List<Video>()
                            };
                            try
                            {
                                var listKey = new RedisValue[] { };
                                var listArticle = new List<Video>();
                                var total = default(long);
                                var tempKey = _context.NameOf("index:temp:union:" + Generator.SorterSetSearchId());

                                var listkeyFull = new List<RedisKey>();
                                listkeyFull.Add(_context.NameOf(IndexKeys.KeyStatus<NewsAll>(int.Parse(data.Status))));

                                var keyApprove = new List<RedisKey>();
                                keyApprove.Add(_context.NameOf(IndexKeys.KeyApprove<Video>(data.OfficerId)));
                                keyApprove.Add(_context.NameOf(IndexKeys.KeyApproveInAccount<Video>(data.OfficerId, accountId)));

                                var tempKeyApprove = _context.NameOf("index:temp:approve:" + Generator.SorterSetSearchId());

                                var valueApprove = await db.SortedSetCombineAndStoreAsync(SetOperation.Union
                                            , tempKeyApprove, keyApprove.ToArray(), null, Aggregate.Max, CommandFlags.None);

                                if (valueApprove > 0)
                                {
                                    listkeyFull.Add(tempKeyApprove);
                                    var value = await db.SortedSetCombineAndStoreAsync(SetOperation.Intersect
                                            , tempKey, listkeyFull.ToArray(), null, Aggregate.Max, CommandFlags.None);
                                    if (value > 0)
                                    {
                                        try
                                        {
                                            listKey = (await db.SortedSetRangeByScoreAsync(tempKey
                                            , (data.FromDate ?? DateTime.MinValue).ConvertToTimestamp10()
                                            , (data.ToDate ?? DateTime.Now).ConvertToTimestamp10()
                                            , Exclude.None
                                            , data.OrderBy == SearchNewsOrderBy.AscCreatedDate ? Order.Ascending : Order.Descending
                                            , ((data.PageIndex ?? 1) - 1) * (data.PageSize ?? 20)
                                            , data.PageSize ?? 20
                                            , CommandFlags.None));

                                            total = await db.SortedSetLengthAsync(tempKey
                                                , (data.FromDate ?? DateTime.MinValue).ConvertToTimestamp10()
                                                , (data.ToDate ?? DateTime.Now).ConvertToTimestamp10()
                                                , Exclude.None
                                                , CommandFlags.None);

                                            result.Total = (int)total;
                                            listArticle = listKey != null && listKey.Length > 0 ? (await db.HashGetAsync(_context.NameOf(typeof(Video).Name), listKey, CommandFlags.PreferSlave))?.Select(p => Json.Parse<Video>(p))?.ToList() : listArticle;
                                            if (listArticle != null)
                                            {
                                                result.Data.AddRange(listArticle);
                                            }
                                        }
                                        catch (Exception ex)
                                        {
                                            Logger.Error(ex, ex.Message);
                                        }
                                        action(tempKey);
                                    }
                                    action(tempKeyApprove);
                                }
                            }
                            catch (Exception ex)
                            {
                                Logger.Error(ex, ex.Message);
                            }

                            return result;
                        }
                        else
                        {
                            return new PagingDataResult<Video>();
                        }
                    });
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
            }
            return returnValue;
        }
        public async Task<RedisValue[]> GetAllAsync()
        {
            var returnValue = default(RedisValue[]);
            try
            {
                using (var _context = GetContext())
                {
                    returnValue = await _context.ExecAsync(async (db) =>
                    {
                        var tempKey = _context.NameOf("index:temp:union:" + Generator.SorterSetSearchId());
                        var value = await db.SortedSetCombineAndStoreAsync(SetOperation.Intersect
                                        , tempKey, _context.NameOf(IndexKeys.KeyStatus<NewsAll>((int)NewsStatus.Published)), _context.NameOf(IndexKeys.KeyType<NewsAll>((int)NewsType.Video)));

                        returnValue = await db.SortedSetRangeByRankAsync(tempKey, 0, -1);
                        await db.KeyDeleteAsync(tempKey);
                        return returnValue;
                    });
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
            }
            return returnValue;
        }
    }
}
