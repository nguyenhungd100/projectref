﻿using ChannelVN.IMS2.Core.Entities;
using ChannelVN.IMS2.Core.Entities.Distribution;
using ChannelVN.IMS2.Core.Entities.Security;
using ChannelVN.IMS2.Core.Entities.Videos;
using ChannelVN.IMS2.Foundation.Common;
using ChannelVN.IMS2.Foundation.Logging;
using StackExchange.Redis;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using static ChannelVN.IMS2.Core.Entities.ErrorCode.ErrorMapping;

namespace ChannelVN.IMS2.Core.Models.CacheStore.Videos
{
    public class VideoPlaylistCacheStore : CmsMainCacheStore
    {
        public async Task<ErrorCodes> AddAsync(VideoPlaylistSearch dataSearch)
        {
            var returnValue = ErrorCodes.AddError;
            try
            {
                var data = Mapper<VideoPlaylist>.Map(dataSearch, new VideoPlaylist());
                var newsAll = Mapper<NewsAll>.MapNewsAll(data, new NewsAll());

                using(var _context = GetContext())
                {
                    //Kiểm tra quyền viết bài
                    var isWriter = await Functions.Function.IsWriter(dataSearch?.NewsInAccount?.FirstOrDefault().AccountId, dataSearch.CreatedBy, _context);

                    //////////////////
                    if (isWriter)
                    {
                        var resultAdd = await _context.MultiAsync((transac) =>
                        {
                            transac.HashSetAsync(_context.NameOf(data.GetType().Name), data.Id, Json.Stringify(data));

                            if (dataSearch.VideoInPlaylist?.Count() > 0)
                            {
                                transac.HashSetAsync(_context.NameOf(dataSearch.VideoInPlaylist.FirstOrDefault().GetType().Name), dataSearch.VideoInPlaylist.Select(p => new HashEntry(p.PlaylistId + IndexKeys.SeparateChar + p.VideoId, Json.Stringify(p)))?.ToArray());
                                transac.SortedSetAddAsync(_context.NameOf(IndexKeys.KeyVideoInPlaylist<VideoInPlaylist>(dataSearch.Id)), dataSearch.VideoInPlaylist.Select(p => new SortedSetEntry(p.VideoId, (p.PlayOnTime ?? DateTime.Now).ConvertToTimestamp10())).ToArray());
                            }

                            transac.HashSetAsync(_context.NameOf(newsAll.GetType().Name), data.Id, Json.Stringify(newsAll));
                            transac.SortedSetAddAsync(_context.NameOf(IndexKeys.KeyCreatedBy<NewsAll>(newsAll.CreatedBy)), newsAll.Id, Utility.ConvertToTimestamp10(newsAll.CreatedDate ?? DateTime.Now));
                            transac.SortedSetAddAsync(_context.NameOf(IndexKeys.KeyStatus<NewsAll>(newsAll.Status)), newsAll.Id, Utility.ConvertToTimestamp10(newsAll.CreatedDate ?? DateTime.Now));
                            transac.SortedSetAddAsync(_context.NameOf(IndexKeys.KeyDistributorid<NewsAll>(newsAll.DistributorId)), newsAll.Id, Utility.ConvertToTimestamp10(newsAll.CreatedDate ?? DateTime.Now));
                            transac.SortedSetAddAsync(_context.NameOf(IndexKeys.KeyType<NewsAll>(newsAll.Type ?? 3)), newsAll.Id, Utility.ConvertToTimestamp10(newsAll.CreatedDate ?? DateTime.Now));

                            transac.HashSetAsync(_context.NameOf(typeof(NewsInAccount).Name), dataSearch.NewsInAccount?.Select(p => new HashEntry(p.AccountId + IndexKeys.SeparateChar + p.NewsId, Json.Stringify(p))).ToArray());
                            transac.SetAddAsync(_context.NameOf(IndexKeys.KeyAccountInNews(dataSearch.Id)), dataSearch.NewsInAccount?.LastOrDefault()?.AccountId ?? 0);
                            transac.SetAddAsync(_context.NameOf(IndexKeys.KeyNewsInAccount(dataSearch.NewsInAccount?.LastOrDefault()?.AccountId)), dataSearch.Id);
                        });
                        if (resultAdd) returnValue = ErrorCodes.Success;
                    }
                    else
                    {
                        returnValue = ErrorCodes.PermissionInvalid;
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
                returnValue = ErrorCodes.Exception;
            }
            return returnValue;
        }

        public Task<VideoPlaylist> GetByIdAsync(long id)
        {
            var returnValue = Task.FromResult(default(VideoPlaylist));
            try
            {
                using (var _context = GetContext())
                {
                    returnValue = _context.HashGetExAsync<VideoPlaylist>(id, CommandFlags.PreferSlave);
                }
            }
            catch(Exception ex)
            {
                Logger.Error(ex, ex.Message);
            }
            return returnValue;
        }

        public Task<VideoPlaylistSearch> GetVideoPlaylistFullInfoAsync(long newsId)
        {
            var returnValue = Task.FromResult(default(VideoPlaylistSearch));
            try
            {
                using (var _context = GetContext())
                {
                    returnValue = _context.ExecAsync(async (db) =>
                    {
                        var result = default(VideoPlaylistSearch);
                        try
                        {
                            var videoPlaylist = Json.Parse<VideoPlaylist>(await db.HashGetAsync(_context.NameOf<VideoPlaylist>(), newsId, CommandFlags.PreferSlave));
                            if (videoPlaylist != null)
                            {
                                result = Mapper<VideoPlaylistSearch>.Map(videoPlaylist, new VideoPlaylistSearch());
                                if (returnValue != null)
                                {
                                    var listAccountId = await db.SetMembersAsync(_context.NameOf(IndexKeys.KeyAccountInNews(newsId)), CommandFlags.PreferSlave);
                                    result.NewsInAccount = (await db.HashGetAsync(_context.NameOf<NewsInAccount>(), listAccountId?.Select(p => (RedisValue)(p + IndexKeys.SeparateChar + newsId))?.ToArray()
                                        , CommandFlags.PreferSlave))?.Select(p => Json.Parse<NewsInAccount>(p))?.ToArray();

                                    var listDistributorIdAndItemStreamId = await db.SortedSetRangeByScoreWithScoresAsync(_context.NameOf(IndexKeys.KeyNewsDistribution(newsId)), double.MinValue, double.MaxValue, Exclude.None, Order.Ascending, 0, -1, CommandFlags.PreferSlave);

                                    var listKeyNewsDistribution = listDistributorIdAndItemStreamId?.Select(p => (RedisValue)(p.Element.ToString()?.Split(IndexKeys.SeparateChar)?.FirstOrDefault() + IndexKeys.SeparateChar
                                    + newsId + IndexKeys.SeparateChar + p.Element.ToString()?.Split(IndexKeys.SeparateChar)?.LastOrDefault()));
                                    result.NewsDistribution = (await db.HashGetAsync(_context.NameOf<NewsDistribution>(), listKeyNewsDistribution?.ToArray(), CommandFlags.PreferSlave))?.Select(p => Json.Parse<NewsDistribution>(p))?.ToArray();

                                    var listKeyVideo = await db.SortedSetRangeByRankAsync(_context.NameOf(IndexKeys.KeyVideoInPlaylist<VideoInPlaylist>(newsId)), 0, -1, Order.Descending, CommandFlags.PreferSlave);

                                    result.VideoInPlaylist = (await db.HashGetAsync(_context.NameOf<VideoInPlaylist>(), listKeyVideo?.Select(p => (RedisValue)(newsId + IndexKeys.SeparateChar + p.ToString()))?.ToArray(), CommandFlags.PreferSlave))?.Select(p => Json.Parse<VideoInPlaylist>(p))?.ToArray();
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            Logger.Error(ex, ex.Message);
                        }

                        return result;
                    });
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
            }
            return returnValue;
        }

        public async Task<ErrorCodes> UpdateAsync(VideoPlaylistSearch data, long officerId, Action<int> actionAutoShare)
        {
            var returnValue = ErrorCodes.UpdateError;
            var statusOld = default(int?);
            try
            {
                using (var _context = GetContext())
                {
                    var approverOld = string.Empty;
                    var dataDb = await _context.ExecAsync(async (db) =>
                    {
                        var result = default(VideoPlaylistSearch);
                        try
                        {
                            var videoPlaylist = Json.Parse<VideoPlaylist>(await db.HashGetAsync(_context.NameOf<VideoPlaylist>(), data.Id, CommandFlags.PreferSlave));
                            if (videoPlaylist != null)
                            {
                                result = Mapper<VideoPlaylistSearch>.Map(videoPlaylist, new VideoPlaylistSearch());
                                if (result != null)
                                {
                                    var listKeyVideo = await db.SortedSetRangeByRankAsync(_context.NameOf(IndexKeys.KeyVideoInPlaylist<VideoInPlaylist>(data.Id)), 0, -1, Order.Descending, CommandFlags.PreferSlave);

                                    result.VideoInPlaylist = (await db.HashGetAsync(_context.NameOf<VideoInPlaylist>(), listKeyVideo?.Select(p => (RedisValue)(data.Id + IndexKeys.SeparateChar + p.ToString()))?.ToArray(), CommandFlags.PreferSlave))?.Select(p => Json.Parse<VideoInPlaylist>(p))?.ToArray();
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            Logger.Error(ex, ex.Message);
                        }

                        return result;
                    });

                    if (dataDb != null)
                    {
                        statusOld = dataDb.Status;

                        var isModifier = await Functions.Function.IsModifier(officerId, data.Id, data.ModifiedBy, dataDb.CreatedBy, dataDb.Status, _context);

                        if (isModifier)
                        {
                            approverOld = dataDb.ApprovedBy;

                            dataDb.Url = data.Url;
                            dataDb.OriginalUrl = data.OriginalUrl;
                            dataDb.Author = data.Author;
                            dataDb.CategoryId = data.CategoryId;
                            dataDb.Location = data.Location;
                            dataDb.PublishMode = data.PublishMode;
                            dataDb.PublishData = data.PublishData;
                            dataDb.DistributionDate = data.DistributionDate.HasValue && data.DistributionDate >= DateTime.Now ? data.DistributionDate : dataDb.DistributionDate;
                            dataDb.ModifiedDate = data.ModifiedDate;
                            dataDb.ModifiedBy = data.ModifiedBy;
                            dataDb.DistributorId = data.DistributorId;
                            dataDb.Tags = data.Tags;
                            dataDb.CardType = data.CardType;
                            dataDb.Name = data.Name;
                            dataDb.UnsignName = data.UnsignName;
                            dataDb.Description = data.Description;
                            dataDb.LastInsertedDate = data.LastInsertedDate;
                            dataDb.Cover = data.Cover;
                            dataDb.IntroClip = data.IntroClip;
                            dataDb.PlaylistRelation = data.PlaylistRelation;
                            dataDb.MetaAvatar = data.MetaAvatar;
                            dataDb.MetaData = data.MetaData;

                            if (dataDb.Status != (int)NewsStatus.Published)
                            {
                                dataDb.ApprovedBy = data.ApprovedBy;
                            }
                            dataDb.CommentMode = data.CommentMode;

                            var listDel = dataDb.VideoInPlaylist?.Except(data.VideoInPlaylist);

                            var listAdd = data.VideoInPlaylist?.Except(dataDb.VideoInPlaylist);
                            if (listAdd != null)
                            {
                                foreach (var video in listAdd)
                                {
                                    video.PlaylistId = dataDb.Id;
                                    video.PlayOnTime = DateTime.Now;
                                    dataDb.VideoInPlaylist = dataDb.VideoInPlaylist ?? new VideoInPlaylist[] { };
                                    dataDb.VideoInPlaylist = dataDb.VideoInPlaylist.Append(video).ToArray();
                                }
                            }
                            var listVideoOld = dataDb.VideoInPlaylist;

                            if (data.VideoInPlaylist != null)
                            {
                                foreach (var video in data.VideoInPlaylist)
                                {
                                    if (!(dataDb.VideoInPlaylist?.Where(p => p.VideoId == video.VideoId)?.Count() > 0))
                                    {
                                        video.PlaylistId = dataDb.Id;
                                        video.PlayOnTime = DateTime.Now;
                                    }
                                    else
                                    {
                                        video.PlaylistId = dataDb.Id;
                                        video.PlayOnTime = dataDb.VideoInPlaylist?.Where(p => p.VideoId == video.VideoId)?.FirstOrDefault().PlayOnTime;
                                    }
                                }
                            }
                            dataDb.VideoInPlaylist = data.VideoInPlaylist;

                            var dataRedis = Mapper<VideoPlaylist>.Map(dataDb, new VideoPlaylist());
                            var newsAll = Mapper<NewsAll>.MapNewsAll(dataDb, new NewsAll());

                            var updateResult = await _context.MultiAsync((transac) =>
                            {
                                transac.HashSetAsync(_context.NameOf(dataRedis.GetType().Name), dataRedis.Id, Json.Stringify(dataRedis));
                                transac.HashSetAsync(_context.NameOf(newsAll.GetType().Name), newsAll.Id, Json.Stringify(newsAll));

                                //xoa du lieu cu
                                if (listVideoOld != null && listVideoOld.Count() > 0)
                                {
                                    transac.HashDeleteAsync(_context.NameOf(listVideoOld.FirstOrDefault().GetType().Name), listVideoOld.Select(p => (RedisValue)(data.Id + IndexKeys.SeparateChar + p.VideoId))?.ToArray());
                                    transac.SortedSetRemoveAsync(_context.NameOf(IndexKeys.KeyVideoInPlaylist<VideoInPlaylist>(data.Id)), listVideoOld.Select(p => (RedisValue)p.VideoId)?.ToArray());
                                }
                                //add du lieu moi
                                if (data.VideoInPlaylist?.Count() > 0)
                                {
                                    transac.HashSetAsync(_context.NameOf(data.VideoInPlaylist.FirstOrDefault().GetType().Name), data.VideoInPlaylist.Select(p => new HashEntry(data.Id + IndexKeys.SeparateChar + p.VideoId, Json.Stringify(p)))?.ToArray());
                                    transac.SortedSetAddAsync(_context.NameOf(IndexKeys.KeyVideoInPlaylist<VideoInPlaylist>(data.Id)), data.VideoInPlaylist.Select(p => new SortedSetEntry(p.VideoId, (p.PlayOnTime ?? DateTime.Now).ConvertToTimestamp10())).ToArray());
                                }

                                if (dataDb.Status == (int)NewsStatus.Approve)
                                {
                                    if (long.TryParse(approverOld, out long _approverOld))
                                        transac.SortedSetRemoveAsync(_context.NameOf(IndexKeys.KeyApproveInAccount<VideoPlaylist>(officerId, _approverOld)), dataDb.Id);

                                    if (!string.IsNullOrEmpty(dataDb.ApprovedBy))
                                    {
                                        if (long.TryParse(dataDb.ApprovedBy, out long accountId))
                                        {
                                            transac.SortedSetAddAsync(_context.NameOf(IndexKeys.KeyApproveInAccount<VideoPlaylist>(officerId, accountId)), dataDb.Id, DateTime.Now.ConvertToTimestamp10());
                                        }
                                    }
                                }
                            });

                            if (updateResult) returnValue = ErrorCodes.Success;
                        }
                        else
                        {
                            returnValue = ErrorCodes.PermissionUpdateInvalid;
                        }
                    }
                    else
                    {
                        returnValue = ErrorCodes.DataNotExist;
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
                returnValue = ErrorCodes.Exception;
            }
            if (returnValue == ErrorCodes.Success && statusOld != null)
            {
                actionAutoShare(statusOld ?? 0);
            }
            return returnValue;
        }

        public Task<bool> UpdateStatusAsync(VideoPlaylist data, int statusOld)
        {
            var returnValue = Task.FromResult(false);
            try
            {
                if (data != null)
                {
                    var newsAll = Mapper<NewsAll>.MapNewsAll(data, new NewsAll());

                    using (var _context = GetContext())
                    {
                        returnValue = _context.MultiAsync((transac) =>
                        {
                            transac.HashSetAsync(_context.NameOf(data.GetType().Name), data.Id, Json.Stringify(data));

                            transac.HashSetAsync(_context.NameOf(newsAll.GetType().Name), newsAll.Id, Json.Stringify(newsAll));
                            transac.SortedSetRemoveAsync(_context.NameOf(IndexKeys.KeyStatus<NewsAll>(statusOld)), newsAll.Id);
                            transac.SortedSetAddAsync(_context.NameOf(IndexKeys.KeyStatus<NewsAll>(newsAll.Status)), newsAll.Id, Utility.ConvertToTimestamp10(newsAll.CreatedDate ?? DateTime.Now));
                        });
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
            }
            return returnValue;
        }


        public async Task<ErrorCodes> UpdateStatusV2Async(long id, NewsStatus status, long userId, Action<VideoPlaylistSearch, int> actionAutoShare)
        {
            var returnValue = ErrorCodes.UpdateError;
            var statusOld = default(int?);
            var dataDb = default(VideoPlaylist);

            try
            {
                using (var _context = GetContext())
                {
                    
                    var newsAll = default(NewsAll);
                    var resultUpdate = false;
                    var officerIds = default(RedisValue[]);
                    var officerId = 0L;

                    dataDb = await _context.HashGetExAsync<VideoPlaylist>(id, CommandFlags.PreferSlave);
                    statusOld = dataDb.Status;

                    if (dataDb != null)
                    {
                        if (dataDb.Status == (int)status)
                        {
                            returnValue = ErrorCodes.Success;
                        }
                        else
                        {
                            switch (status)
                            {
                                #region Xuat ban
                                case NewsStatus.Published:// xuất bản
                                    officerIds = await _context.SetMembersAsync(IndexKeys.KeyAccountInNews(id), CommandFlags.PreferSlave);
                                    if (officerIds != null && officerIds.Count() != 0)
                                    {
                                        if (long.TryParse(officerIds[0], out officerId))
                                        {
                                            var officer = await _context.HashGetExAsync<Account>(officerId, CommandFlags.PreferSlave);

                                            var listMemberId = await _context.SortedSetRangeByScoreAsync(IndexKeys.KeyAccountMemberInOfficer<AccountMember>(officerId), double.NegativeInfinity, double.PositiveInfinity);

                                            var listMember = await _context.GetsFromHashExAsync<AccountMember>(listMemberId?.Select(p => officerId + IndexKeys.SeparateChar + p.ToString())?.ToList(), CommandFlags.PreferSlave);
                                            var accountMember = listMember?.Where(p => p.MemberId == userId).FirstOrDefault();

                                            if (accountMember != null && officer!=null)
                                            {
                                                var isPublisher = await Functions.Function.IsPublisher(accountMember, dataDb.CreatedBy, dataDb.ApprovedBy, officer.Mode, dataDb.Status);
                                                if (isPublisher)
                                                {
                                                    dataDb.Status = (int)status;
                                                    dataDb.ModifiedDate = DateTime.Now;
                                                    dataDb.ModifiedBy = userId.ToString();
                                                    dataDb.DistributionDate = dataDb.DistributionDate > DateTime.Now ? dataDb.DistributionDate : DateTime.Now;

                                                    var oldApprovedBy = dataDb.ApprovedBy;
                                                    dataDb.ApprovedBy = accountMember.MemberId.ToString();
                                                    dataDb.ApprovedDate = DateTime.Now;

                                                    newsAll = Mapper<NewsAll>.MapNewsAll(dataDb, new NewsAll());

                                                    resultUpdate = await _context.MultiAsync((transac) =>
                                                    {
                                                        transac.HashSetAsync(_context.NameOf(dataDb.GetType().Name), dataDb.Id, Json.Stringify(dataDb));
                                                        transac.HashSetAsync(_context.NameOf(newsAll.GetType().Name), newsAll.Id, Json.Stringify(newsAll));

                                                        transac.SortedSetRemoveAsync(_context.NameOf(IndexKeys.KeyStatus<NewsAll>(statusOld)), newsAll.Id);
                                                        transac.SortedSetAddAsync(_context.NameOf(IndexKeys.KeyStatus<NewsAll>(newsAll.Status)), newsAll.Id, (newsAll.DistributionDate ?? DateTime.Now).ConvertToTimestamp10());

                                                        //remove value in set approve in account if exist
                                                        if (!string.IsNullOrEmpty(oldApprovedBy) && long.TryParse(oldApprovedBy, out long oldApprovedById))
                                                        {
                                                            transac.SortedSetRemoveAsync(_context.NameOf(IndexKeys.KeyApproveInAccount<VideoPlaylist>(officerId, oldApprovedById)), dataDb.Id, CommandFlags.PreferSlave);
                                                        }
                                                        else
                                                        {
                                                            //remove value in set approve if exist
                                                            transac.SortedSetRemoveAsync(_context.NameOf(IndexKeys.KeyApprove<VideoPlaylist>(officerId)), dataDb.Id, CommandFlags.PreferSlave);
                                                        }

                                                        //add value into key approved in account
                                                        transac.SortedSetAddAsync(_context.NameOf(IndexKeys.KeyApprovedInAccount<VideoPlaylist>(officerId, userId)), dataDb.Id, DateTime.Now.ConvertToTimestamp10());
                                                    });

                                                    if (resultUpdate) returnValue = ErrorCodes.Success;
                                                }
                                                else
                                                {
                                                    returnValue = ErrorCodes.PermissionPublishInvalid;
                                                }
                                            }
                                            else
                                            {
                                                returnValue = ErrorCodes.PermissionPublishInvalid;
                                            }
                                        }
                                        else
                                        {
                                            returnValue = ErrorCodes.DataInvalid;
                                        }
                                    }
                                    else
                                    {
                                        returnValue = ErrorCodes.DataInvalid;
                                    }
                                    break;
                                #endregion
                                #region Xoa bai
                                case NewsStatus.Remove: // xóa bài
                                    if (statusOld != (int)NewsStatus.Published)
                                    {
                                        if (userId.ToString().Equals(dataDb.CreatedBy))
                                        {
                                            dataDb.Status = (int)status;
                                            dataDb.ModifiedDate = DateTime.Now;
                                            dataDb.ModifiedBy = userId.ToString();

                                            newsAll = Mapper<NewsAll>.MapNewsAll(dataDb, new NewsAll());

                                            resultUpdate = await _context.MultiAsync((transac) =>
                                            {
                                                transac.HashSetAsync(_context.NameOf(dataDb.GetType().Name), dataDb.Id, Json.Stringify(dataDb));

                                                transac.HashSetAsync(_context.NameOf(newsAll.GetType().Name), newsAll.Id, Json.Stringify(newsAll));

                                                transac.SortedSetRemoveAsync(_context.NameOf(IndexKeys.KeyStatus<NewsAll>(statusOld)), newsAll.Id);
                                                transac.SortedSetAddAsync(_context.NameOf(IndexKeys.KeyStatus<NewsAll>(newsAll.Status)), newsAll.Id, (dataDb.ModifiedDate ?? DateTime.Now).ConvertToTimestamp10());
                                            });

                                            if (resultUpdate) returnValue = ErrorCodes.Success;
                                        }
                                        else
                                        {
                                            returnValue = ErrorCodes.PermissionUpdateInvalid;
                                        }
                                    }
                                    else
                                    {
                                        // trạng thái cũ là publish
                                        officerIds = await _context.SetMembersAsync(IndexKeys.KeyAccountInNews(id), CommandFlags.PreferSlave);
                                        if (officerIds != null && officerIds.Count() != 0)
                                        {
                                            if (long.TryParse(officerIds[0], out officerId))
                                            {
                                                var officer = await _context.HashGetExAsync<Account>(officerId, CommandFlags.PreferSlave);

                                                var listMemberId = await _context.SortedSetRangeByScoreAsync(IndexKeys.KeyAccountMemberInOfficer<AccountMember>(officerId), double.NegativeInfinity, double.PositiveInfinity);

                                                var listMember = await _context.GetsFromHashExAsync<AccountMember>(listMemberId?.Select(p => officerId + IndexKeys.SeparateChar + p.ToString())?.ToList(), CommandFlags.PreferSlave);

                                                var accountMember = listMember?.Where(p => p.MemberId == userId).FirstOrDefault();
                                                if (accountMember != null && officer != null)
                                                {
                                                    var isRemover = await Functions.Function.IsRemover(accountMember, dataDb.CreatedBy, dataDb.ApprovedBy, officer.Mode, statusOld);
                                                    if (isRemover)
                                                    {
                                                        dataDb.Status = (int)status;
                                                        dataDb.ModifiedDate = DateTime.Now;
                                                        dataDb.ModifiedBy = userId.ToString();

                                                        newsAll = Mapper<NewsAll>.MapNewsAll(dataDb, new NewsAll());

                                                        resultUpdate = await _context.MultiAsync((transac) =>
                                                        {
                                                            transac.HashSetAsync(_context.NameOf(dataDb.GetType().Name), dataDb.Id, Json.Stringify(dataDb));

                                                            transac.HashSetAsync(_context.NameOf(newsAll.GetType().Name), newsAll.Id, Json.Stringify(newsAll));

                                                            transac.SortedSetRemoveAsync(_context.NameOf(IndexKeys.KeyStatus<NewsAll>(statusOld)), newsAll.Id);
                                                            transac.SortedSetAddAsync(_context.NameOf(IndexKeys.KeyStatus<NewsAll>(newsAll.Status)), newsAll.Id, (dataDb.ModifiedDate ?? DateTime.Now).ConvertToTimestamp10());

                                                            if (long.TryParse(dataDb.ApprovedBy, out long accountId))
                                                            {
                                                                transac.SortedSetRemoveAsync(_context.NameOf(IndexKeys.KeyApprovedInAccount<VideoPlaylist>(officerId, accountId)), dataDb.Id);
                                                            }
                                                        });

                                                        if (resultUpdate) returnValue = ErrorCodes.Success;
                                                    }
                                                    else
                                                    {
                                                        returnValue = ErrorCodes.PermissionUpdateInvalid;
                                                    }
                                                }
                                                else
                                                {
                                                    returnValue = ErrorCodes.PermissionUpdateInvalid;
                                                }
                                            }
                                            else
                                            {
                                                returnValue = ErrorCodes.DataInvalid;
                                            }
                                        }
                                        else
                                        {
                                            returnValue = ErrorCodes.DataInvalid;
                                        }
                                    }

                                    break;
                                #endregion
                                #region Gui cho duyet
                                case NewsStatus.Approve:// Gửi nhờ duyệt
                                    if (dataDb.Status == (int)NewsStatus.Published)
                                    {
                                        returnValue = ErrorCodes.DataInvalid;
                                    }
                                    else
                                    {
                                        if (userId.ToString().Equals(dataDb.CreatedBy))
                                        {
                                            officerIds = await _context.SetMembersAsync(IndexKeys.KeyAccountInNews(id), CommandFlags.PreferSlave);
                                            if (officerIds != null && officerIds.Count() != 0)
                                            {
                                                if (long.TryParse(officerIds[0], out officerId))
                                                {
                                                    dataDb.Status = (int)status;
                                                    dataDb.ModifiedDate = DateTime.Now;
                                                    dataDb.ModifiedBy = userId.ToString();

                                                    newsAll = Mapper<NewsAll>.MapNewsAll(dataDb, new NewsAll());

                                                    resultUpdate = await _context.MultiAsync((transac) =>
                                                    {
                                                        transac.HashSetAsync(_context.NameOf(dataDb.GetType().Name), dataDb.Id, Json.Stringify(dataDb));

                                                        transac.HashSetAsync(_context.NameOf(newsAll.GetType().Name), newsAll.Id, Json.Stringify(newsAll));

                                                        transac.SortedSetRemoveAsync(_context.NameOf(IndexKeys.KeyStatus<NewsAll>(statusOld)), newsAll.Id);
                                                        transac.SortedSetAddAsync(_context.NameOf(IndexKeys.KeyStatus<NewsAll>(newsAll.Status)), newsAll.Id, (dataDb.ModifiedDate ?? DateTime.Now).ConvertToTimestamp10());

                                                        if (string.IsNullOrEmpty(dataDb.ApprovedBy))
                                                        {
                                                            transac.SortedSetAddAsync(_context.NameOf(IndexKeys.KeyApprove<VideoPlaylist>(officerId)), dataDb.Id, DateTime.Now.ConvertToTimestamp10());
                                                        }
                                                        else
                                                        {
                                                            if (long.TryParse(dataDb.ApprovedBy, out long accountId))
                                                            {
                                                                transac.SortedSetAddAsync(_context.NameOf(IndexKeys.KeyApproveInAccount<VideoPlaylist>(officerId, accountId)), dataDb.Id, DateTime.Now.ConvertToTimestamp10());
                                                            }
                                                        }
                                                    });
                                                }
                                            }

                                            if (resultUpdate) returnValue = ErrorCodes.Success;
                                        }
                                        else
                                        {
                                            returnValue = ErrorCodes.PermissionUpdateInvalid;
                                        }
                                    }
                                    break;
                                #endregion
                                #region Default
                                default:
                                    if (userId.ToString().Equals(dataDb.CreatedBy))
                                    {
                                        dataDb.Status = (int)status;
                                        dataDb.ModifiedDate = DateTime.Now;
                                        dataDb.ModifiedBy = userId.ToString();

                                        if (statusOld == (int)NewsStatus.Published)
                                        {
                                            officerIds = await _context.SetMembersAsync(IndexKeys.KeyAccountInNews(id), CommandFlags.PreferSlave);
                                            if (officerIds != null && officerIds.Count() != 0)
                                            {
                                                long.TryParse(officerIds[0], out officerId);
                                            }
                                        }

                                        newsAll = Mapper<NewsAll>.MapNewsAll(dataDb, new NewsAll());

                                        resultUpdate = await _context.MultiAsync((transac) =>
                                        {
                                            transac.HashSetAsync(_context.NameOf(dataDb.GetType().Name), dataDb.Id, Json.Stringify(dataDb));

                                            transac.HashSetAsync(_context.NameOf(newsAll.GetType().Name), newsAll.Id, Json.Stringify(newsAll));

                                            transac.SortedSetRemoveAsync(_context.NameOf(IndexKeys.KeyStatus<NewsAll>(statusOld)), newsAll.Id);
                                            transac.SortedSetAddAsync(_context.NameOf(IndexKeys.KeyStatus<NewsAll>(newsAll.Status)), newsAll.Id, (dataDb.ModifiedDate ?? DateTime.Now).ConvertToTimestamp10());

                                            if (officerId > 0)
                                            {
                                                if (long.TryParse(dataDb.ApprovedBy, out long accountId))
                                                {
                                                    transac.SortedSetRemoveAsync(_context.NameOf(IndexKeys.KeyApprovedInAccount<VideoPlaylist>(officerId, accountId)), dataDb.Id);
                                                }
                                            }
                                        });

                                        if (resultUpdate) returnValue = ErrorCodes.Success;
                                    }
                                    else
                                    {
                                        if (statusOld == (int?)NewsStatus.Published || statusOld == (int?)NewsStatus.Archive || statusOld == (int?)NewsStatus.Approve)
                                        {
                                            officerIds = await _context.SetMembersAsync(IndexKeys.KeyAccountInNews(id), CommandFlags.PreferSlave);
                                            if (officerIds != null && officerIds.Count() != 0)
                                            {
                                                if (long.TryParse(officerIds[0], out officerId))
                                                {
                                                    var officer = await _context.HashGetExAsync<Account>(officerId, CommandFlags.PreferSlave);

                                                    var listMemberId = await _context.SortedSetRangeByScoreAsync(IndexKeys.KeyAccountMemberInOfficer<AccountMember>(officerId), double.NegativeInfinity, double.PositiveInfinity);

                                                    var listMember = await _context.GetsFromHashExAsync<AccountMember>(listMemberId?.Select(p => officerId + IndexKeys.SeparateChar + p.ToString())?.ToList(), CommandFlags.PreferSlave);

                                                    var accountMember = listMember?.Where(p => p.MemberId == userId).FirstOrDefault();
                                                    if (accountMember != null && officer != null)
                                                    {
                                                        var isRemover = await Functions.Function.IsRemover(accountMember, dataDb.CreatedBy, dataDb.ApprovedBy, officer.Mode, statusOld);
                                                        if (isRemover)
                                                        {
                                                            dataDb.Status = (int)status;
                                                            dataDb.ModifiedDate = DateTime.Now;
                                                            dataDb.ModifiedBy = userId.ToString();

                                                            newsAll = Mapper<NewsAll>.MapNewsAll(dataDb, new NewsAll());

                                                            resultUpdate = await _context.MultiAsync((transac) =>
                                                            {
                                                                transac.HashSetAsync(_context.NameOf(dataDb.GetType().Name), dataDb.Id, Json.Stringify(dataDb));

                                                                transac.HashSetAsync(_context.NameOf(newsAll.GetType().Name), newsAll.Id, Json.Stringify(newsAll));

                                                                transac.SortedSetRemoveAsync(_context.NameOf(IndexKeys.KeyStatus<NewsAll>(statusOld)), newsAll.Id);
                                                                transac.SortedSetAddAsync(_context.NameOf(IndexKeys.KeyStatus<NewsAll>(newsAll.Status)), newsAll.Id, (dataDb.ModifiedDate ?? DateTime.Now).ConvertToTimestamp10());

                                                                if (officerId > 0)
                                                                {
                                                                    if (long.TryParse(dataDb.ApprovedBy, out long accountId))
                                                                    {
                                                                        transac.SortedSetRemoveAsync(_context.NameOf(IndexKeys.KeyApprovedInAccount<VideoPlaylist>(officerId, accountId)), dataDb.Id);
                                                                    }
                                                                }
                                                            });

                                                            if (resultUpdate) returnValue = ErrorCodes.Success;
                                                        }
                                                        else
                                                        {
                                                            returnValue = ErrorCodes.PermissionUpdateInvalid;
                                                        }
                                                    }
                                                    else
                                                    {
                                                        returnValue = ErrorCodes.PermissionUpdateInvalid;
                                                    }
                                                }
                                                else
                                                {
                                                    returnValue = ErrorCodes.DataInvalid;
                                                }
                                            }
                                            else
                                            {
                                                returnValue = ErrorCodes.DataInvalid;
                                            }
                                        }
                                        else
                                        {
                                            returnValue = ErrorCodes.PermissionUpdateInvalid;
                                        }
                                    }

                                    break;
                                    #endregion
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
            }

            if(returnValue == ErrorCodes.Success && dataDb !=null && statusOld != null)
            {
                actionAutoShare(Mapper<VideoPlaylistSearch>.Map(dataDb, new VideoPlaylistSearch()), statusOld ?? 0);
            }

            return returnValue;
        }

        public Task<List<VideoPlaylist>> GetListByIdsAsync(List<string> ids)
        {
            var returnValue = Task.FromResult(default(List<VideoPlaylist>));
            try
            {
                using (var _context = GetContext())
                {
                    returnValue = _context.GetsFromHashExAsync<VideoPlaylist>(ids, CommandFlags.PreferSlave);
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
            }
            return returnValue;
        }


        public Task<List<VideoPlaylistSearch>> GetListPlaylistSearchByIdsAsync(List<string> ids)
        {
            var returnValue = Task.FromResult(default(List<VideoPlaylistSearch>));
            try
            {
                using (var _context = GetContext())
                {
                    returnValue = _context.ExecAsync(async (db) =>
                    {
                        var result = new List<VideoPlaylistSearch>();
                        try
                        {
                            var listValue = (await db.HashGetAsync(_context.NameOf<VideoPlaylist>(), ids?.Select(p => (RedisValue)p)?.ToArray(), CommandFlags.PreferSlave))?.Select(p => Json.Parse<VideoPlaylist>(p));
                            if (listValue != null)
                            {
                                foreach (var playlist in listValue)
                                {
                                    var videoPlaylistSearchReturn = Mapper<VideoPlaylistSearch>.Map(playlist, new VideoPlaylistSearch());
                                    var listTmp = new List<NewsDistribution>();//videoPlaylistSearchReturn.NewsDistribution = new NewsDistribution[] { };
                                    try
                                    {
                                        var newsDistribution = await db.SortedSetRangeByScoreWithScoresAsync(_context.NameOf(IndexKeys.KeyNewsDistribution(playlist.Id)), double.MinValue, double.MaxValue, Exclude.None, Order.Ascending, 0, -1, CommandFlags.PreferSlave);
                                        if (newsDistribution != null)
                                        {
                                            foreach (var item in newsDistribution)
                                            {
                                                if (long.TryParse(item.Element.ToString()?.Split(IndexKeys.SeparateChar)?.FirstOrDefault(), out long distributorId))
                                                {
                                                    listTmp.Add(new NewsDistribution
                                                    {
                                                        DistributorId = distributorId,
                                                        SharedDate = Utility.ConvertSecondsToDate(item.Score),
                                                        DistributorName = Json.Parse<Distributor>(await db.HashGetAsync(_context.NameOf<Distributor>(), distributorId, CommandFlags.PreferSlave))?.Name
                                                    });
                                                }
                                            }
                                        }
                                    }
                                    catch (Exception ex)
                                    {
                                        Logger.Error(ex, ex.Message);
                                    }
                                    videoPlaylistSearchReturn.NewsDistribution = listTmp.ToArray();

                                    //get VideoInPlaylist
                                    try
                                    {
                                        //videoPlaylistSearchReturn.VideoInPlaylist = await
                                        var listKeyVideo = await db.SortedSetRangeByRankAsync(_context.NameOf(IndexKeys.KeyVideoInPlaylist<VideoInPlaylist>(videoPlaylistSearchReturn.Id)), 0, -1, Order.Ascending, CommandFlags.PreferSlave);
                                        videoPlaylistSearchReturn.VideoInPlaylist = (await db.HashGetAsync(_context.NameOf<VideoInPlaylist>(), listKeyVideo?.Select(p => (RedisValue)(videoPlaylistSearchReturn.Id + IndexKeys.SeparateChar + p))?.ToArray(), CommandFlags.PreferSlave))?.Select(p => Json.Parse<VideoInPlaylist>(p))?.ToArray();
                                    }
                                    catch (Exception ex)
                                    {
                                        Logger.Error(ex, ex.Message);
                                    }

                                    result.Add(videoPlaylistSearchReturn);
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            Logger.Error(ex, ex.Message);
                        }

                        return result;
                    });
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
            }
            return returnValue;
        }

        public Task<PagingDataResult<VideoPlaylistSearchReturn>> SearchAsync(SearchNews data, long userId, Action<string> action)
        {
            var returnValue = Task.FromResult(default(PagingDataResult<VideoPlaylistSearchReturn>));
            try
            {
                using (var _context = GetContext())
                {
                    returnValue = _context.ExecAsync(async (db) =>
                    {
                        //var listMemberId = await db.SortedSetRangeByRankAsync(_context.NameOf(IndexKeys.KeyAccountMemberInOfficer<AccountMember>(data.OfficerId)), 0, -1, Order.Descending, CommandFlags.PreferSlave);
                        //var memberValid = listMemberId?.Where(p => p.ToString().Equals(userId.ToString()))?.Count() > 0;
                        var member = Json.Parse<AccountMember>(await db.HashGetAsync(_context.NameOf<AccountMember>(), data.OfficerId + IndexKeys.SeparateChar + userId, CommandFlags.PreferSlave));
                        if (member != null)
                        {
                            var result = new PagingDataResult<VideoPlaylistSearchReturn>()
                            {
                                Data = new List<VideoPlaylistSearchReturn>()
                            };

                            try
                            {
                                var listKey = new RedisValue[] { };
                                var listVideoPlaylist = new List<VideoPlaylist>();
                                var total = default(long);
                                var tempKey = _context.NameOf("index:temp:union:" + Generator.SorterSetSearchId());
                                var createdByKey = _context.NameOf(IndexKeys.KeyCreatedBy<NewsAll>(userId.ToString()));
                                var tempKeyCreatedBy = _context.NameOf("index:temp:createdby:" + Generator.SorterSetSearchId());
                                var listkeyFull = new List<RedisKey>();
                                var listCreatedBy = new List<RedisKey>();
                                var value4 = 0L;

                                if (data.Status.Equals(((int)NewsStatus.Published).ToString()) && member.Permissions != null && (member.Permissions.Contains(MemberPermission.ModifyPostOnPage) || member.Permissions.Contains(MemberPermission.RemovePostOnPage)))
                                {

                                }
                                else
                                {
                                    var listOwner = await _context.SortedSetRangeByRankAsync(IndexKeys.KeyOwnerOfficer(userId), 0, -1, Order.Descending, CommandFlags.PreferSlave);
                                    if (listOwner == null || !listOwner.Contains((RedisValue)data.OfficerId))
                                    {
                                        //listkeyFull.Add(createdByKey);
                                        listCreatedBy.Add(createdByKey);

                                        //Lấy cả những bài chỉ duyệt

                                        if (data.Status?.Split(",")?.Contains(((int)NewsStatus.Published).ToString()) ?? false)
                                        {
                                            listCreatedBy.Add(_context.NameOf(IndexKeys.KeyApprovedInAccount<Video>(data.OfficerId, userId)));
                                        }

                                        value4 = await db.SortedSetCombineAndStoreAsync(SetOperation.Union
                                        , tempKeyCreatedBy, listCreatedBy.ToArray(), null, Aggregate.Max, CommandFlags.None);

                                        if (value4 > 0)
                                        {
                                            listkeyFull.Add(tempKeyCreatedBy);
                                        }
                                    }
                                    else
                                    {
                                        if ((data.Status?.Split(",")?.Contains(((int)NewsStatus.Approve).ToString()) ?? false) || (data.Status?.Split(",")?.Contains(((int)NewsStatus.Draft).ToString()) ?? false) || (data.Status?.Split(",")?.Contains(((int)NewsStatus.Remove).ToString()) ?? false))
                                        {
                                            listkeyFull.Add(createdByKey);
                                        }
                                    }
                                }

                                try
                                {
                                    listkeyFull.Add(_context.NameOf(IndexKeys.KeyNewsInAccount(data.OfficerId)));
                                    listkeyFull.Add(_context.NameOf(IndexKeys.KeyType<NewsAll>((int)NewsType.PlayList)));
                                    switch (string.IsNullOrEmpty(data.Status))
                                    {
                                        case true:
                                            if (data.DistributionId == -1)
                                            {
                                                listkeyFull.Add(_context.NameOf(IndexKeys.KeyDistributorid<NewsDistribution>()));
                                            }
                                            else
                                            {
                                                if (data.DistributionId > 0)
                                                {
                                                    listkeyFull.Add(_context.NameOf(IndexKeys.KeyDistributorid<NewsDistribution>(data.DistributionId)));
                                                }
                                            }
                                            var value1 = await db.SortedSetCombineAndStoreAsync(SetOperation.Intersect
                                                , tempKey, listkeyFull.ToArray(), null, Aggregate.Max, CommandFlags.None);
                                            if (value1 > 0)
                                            {
                                                try
                                                {
                                                    listKey = (await db.SortedSetRangeByScoreAsync(tempKey
                                                    , Utility.ConvertToTimestamp10(data.FromDate ?? DateTime.MinValue)
                                                    , Utility.ConvertToTimestamp10(data.ToDate ?? DateTime.Now)
                                                    , Exclude.None
                                                    , data.OrderBy == SearchNewsOrderBy.AscCreatedDate ? Order.Ascending : Order.Descending
                                                    , ((data.PageIndex ?? 1) - 1) * (data.PageSize ?? 20)
                                                    , data.PageSize ?? 20, CommandFlags.None));

                                                    total = await db.SortedSetLengthAsync(tempKey
                                                        , Utility.ConvertToTimestamp10(data.FromDate ?? DateTime.MinValue)
                                                        , Utility.ConvertToTimestamp10(data.ToDate ?? DateTime.Now), Exclude.None, CommandFlags.None);

                                                    result.Total = (int)total;
                                                    listVideoPlaylist = listKey != null && listKey.Length > 0 ? (await db.HashGetAsync(_context.NameOf<VideoPlaylist>(), listKey, CommandFlags.PreferSlave))?.Select(p => Json.Parse<VideoPlaylist>(p))?.ToList() : listVideoPlaylist;
                                                    if (listVideoPlaylist != null)
                                                    {
                                                        foreach (var playlist in listVideoPlaylist)
                                                        {
                                                            var videoPlaylistSearchReturn = Mapper<VideoPlaylistSearchReturn>.Map(playlist, new VideoPlaylistSearchReturn());
                                                            var listTmp = new List<NewsDistribution>();//videoPlaylistSearchReturn.NewsDistribution = new NewsDistribution[] { };
                                                            try
                                                            {
                                                                var newsDistribution = await db.SortedSetRangeByScoreWithScoresAsync(_context.NameOf(IndexKeys.KeyNewsDistribution(playlist.Id)), double.MinValue, double.MaxValue, Exclude.None, Order.Ascending, 0, -1, CommandFlags.PreferSlave);
                                                                if (newsDistribution != null)
                                                                {
                                                                    foreach (var item in newsDistribution)
                                                                    {
                                                                        if (long.TryParse(item.Element.ToString()?.Split(IndexKeys.SeparateChar)?.FirstOrDefault(), out long distributorId))
                                                                        {
                                                                            listTmp.Add(new NewsDistribution
                                                                            {
                                                                                DistributorId = distributorId,
                                                                                SharedDate = Utility.ConvertSecondsToDate(item.Score),
                                                                                DistributorName = Json.Parse<Distributor>(await db.HashGetAsync(_context.NameOf<Distributor>(), distributorId, CommandFlags.PreferSlave))?.Name
                                                                            });
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                            catch (Exception exx)
                                                            {
                                                                Logger.Error(exx, exx.Message);
                                                            }
                                                            videoPlaylistSearchReturn.NewsDistribution = listTmp.ToArray();

                                                            //get VideoInPlaylist
                                                            try
                                                            {
                                                                //videoPlaylistSearchReturn.VideoInPlaylist = await
                                                                var listKeyVideo = await db.SortedSetRangeByRankAsync(_context.NameOf(IndexKeys.KeyVideoInPlaylist<VideoInPlaylist>(videoPlaylistSearchReturn.Id)), 0, -1, Order.Ascending, CommandFlags.PreferSlave);
                                                                videoPlaylistSearchReturn.VideoInPlaylist = (await db.HashGetAsync(_context.NameOf<VideoInPlaylist>(), listKeyVideo?.Select(p => (RedisValue)(videoPlaylistSearchReturn.Id + IndexKeys.SeparateChar + p))?.ToArray(), CommandFlags.PreferSlave))?.Select(p => Json.Parse<VideoInPlaylist>(p))?.ToArray();
                                                            }
                                                            catch (Exception ex)
                                                            {
                                                                Logger.Error(ex, ex.Message);
                                                            }

                                                            result.Data.Add(videoPlaylistSearchReturn);
                                                        }
                                                    }
                                                }
                                                catch (Exception ex)
                                                {
                                                    Logger.Error(ex, ex.Message);
                                                }
                                                action(tempKey);
                                            }
                                            break;
                                        case false:
                                            if (data.DistributionId == -1)
                                            {
                                                listkeyFull.Add(_context.NameOf(IndexKeys.KeyDistributorid<NewsDistribution>()));
                                            }
                                            else
                                            {
                                                if (data.DistributionId > 0)
                                                {
                                                    listkeyFull.Add(_context.NameOf(IndexKeys.KeyDistributorid<NewsDistribution>(data.DistributionId)));
                                                }
                                            }

                                            var listRedisKeyStatus = new List<RedisKey>();
                                            var listStrStatus = data.Status.Split(",");
                                            foreach (var s in listStrStatus)
                                            {
                                                if (int.TryParse(s, out int status))
                                                {
                                                    if (Enum.IsDefined(typeof(NewsStatus), status))
                                                    {
                                                        listRedisKeyStatus.Add(_context.NameOf(IndexKeys.KeyStatus<NewsAll>(status)));
                                                    }
                                                }
                                            }
                                            var tempKey2 = _context.NameOf("index:temp:" + Generator.SorterSetSearchId());
                                            var values2 = await db.SortedSetCombineAndStoreAsync(SetOperation.Union
                                                    , tempKey2, listRedisKeyStatus.ToArray(), null, Aggregate.Max, CommandFlags.None);
                                            if (values2 > 0)
                                            {
                                                try
                                                {
                                                    listkeyFull.Add(tempKey2);
                                                    var value3 = await db.SortedSetCombineAndStoreAsync(SetOperation.Intersect
                                                    , tempKey, listkeyFull.ToArray(), null, Aggregate.Max, CommandFlags.None);
                                                    if (value3 > 0)
                                                    {
                                                        try
                                                        {
                                                            total = await db.SortedSetLengthAsync(tempKey
                                                            , Utility.ConvertToTimestamp10(data.FromDate ?? DateTime.MinValue)
                                                            , Utility.ConvertToTimestamp10(data.ToDate ?? DateTime.Now), Exclude.None, CommandFlags.None);
                                                            result.Total = (int)total;

                                                            listKey = (await db.SortedSetRangeByScoreAsync(tempKey
                                                            , Utility.ConvertToTimestamp10(data.FromDate ?? DateTime.MinValue)
                                                            , Utility.ConvertToTimestamp10(data.ToDate ?? DateTime.Now)
                                                            , Exclude.None
                                                            , data.OrderBy == SearchNewsOrderBy.AscCreatedDate ? Order.Ascending : Order.Descending
                                                            , ((data.PageIndex ?? 1) - 1) * (data.PageSize ?? 20)
                                                            , data.PageSize ?? 20, CommandFlags.None));

                                                            listVideoPlaylist = listKey != null && listKey.Length > 0 ? (await db.HashGetAsync(_context.NameOf<VideoPlaylist>(), listKey, CommandFlags.PreferSlave))?.Select(p => Json.Parse<VideoPlaylist>(p))?.ToList() : listVideoPlaylist;
                                                            if (listVideoPlaylist != null)
                                                            {
                                                                foreach (var playlist in listVideoPlaylist)
                                                                {
                                                                    var videoPlaylistSearchReturn = Mapper<VideoPlaylistSearchReturn>.Map(playlist, new VideoPlaylistSearchReturn());
                                                                    try
                                                                    {
                                                                        var listTmp = new List<NewsDistribution>();
                                                                        var newsDistribution = await db.SortedSetRangeByScoreWithScoresAsync(_context.NameOf(IndexKeys.KeyNewsDistribution(playlist.Id)), double.MinValue, double.MaxValue, Exclude.None, Order.Ascending, 0, -1, CommandFlags.PreferSlave);
                                                                        if (newsDistribution != null)
                                                                        {
                                                                            foreach (var item in newsDistribution)
                                                                            {
                                                                                if (long.TryParse(item.Element.ToString()?.Split(IndexKeys.SeparateChar)?.FirstOrDefault(), out long distributorId))
                                                                                {
                                                                                    listTmp.Add(new NewsDistribution
                                                                                    {
                                                                                        DistributorId = distributorId,
                                                                                        SharedDate = Utility.ConvertSecondsToDate(item.Score),
                                                                                        DistributorName = Json.Parse<Distributor>(await db.HashGetAsync(_context.NameOf<Distributor>(), distributorId, CommandFlags.PreferSlave))?.Name
                                                                                    });
                                                                                }
                                                                            }
                                                                        }
                                                                        videoPlaylistSearchReturn.NewsDistribution = listTmp.ToArray();
                                                                    }
                                                                    catch (Exception exx)
                                                                    {
                                                                        Logger.Error(exx, exx.Message);
                                                                    }

                                                                    try
                                                                    {
                                                                        //videoPlaylistSearchReturn.VideoInPlaylist = await
                                                                        var listKeyVideo = await db.SortedSetRangeByRankAsync(_context.NameOf(IndexKeys.KeyVideoInPlaylist<VideoInPlaylist>(videoPlaylistSearchReturn.Id)), 0, -1, Order.Ascending, CommandFlags.PreferSlave);
                                                                        videoPlaylistSearchReturn.VideoInPlaylist = (await db.HashGetAsync(_context.NameOf<VideoInPlaylist>(), listKeyVideo?.Select(p => (RedisValue)(videoPlaylistSearchReturn.Id + IndexKeys.SeparateChar + p))?.ToArray(), CommandFlags.PreferSlave))?.Select(p => Json.Parse<VideoInPlaylist>(p))?.ToArray();
                                                                    }
                                                                    catch (Exception ex)
                                                                    {
                                                                        Logger.Error(ex, ex.Message);
                                                                    }
                                                                    result.Data.Add(videoPlaylistSearchReturn);
                                                                }
                                                            }
                                                        }
                                                        catch (Exception ex)
                                                        {
                                                            Logger.Error(ex, ex.Message);
                                                        }
                                                        action(tempKey);
                                                    }
                                                }
                                                catch (Exception ex)
                                                {
                                                    Logger.Error(ex, ex.Message);
                                                }
                                                action(tempKey2);
                                            }
                                            break;
                                    }
                                }
                                catch (Exception ex)
                                {
                                    Logger.Error(ex, ex.Message);
                                }

                                if (value4 > 0)
                                {
                                    action(tempKeyCreatedBy);
                                }
                            }
                            catch (Exception ex)
                            {
                                Logger.Error(ex, ex.Message);
                            }

                            return result;
                        }
                        else
                        {
                            return new PagingDataResult<VideoPlaylistSearchReturn>();
                        }
                    });
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
            }
            return returnValue;
        }

        public Task<bool> AddVideoAsync(long videoId, List<VideoPlaylist> dataDb)
        {
            var returnValue = Task.FromResult(false);
            try
            {
                using(var _context = GetContext())
                {
                    returnValue = _context.MultiAsync((transac) =>
                    {
                        foreach (var playlist in dataDb)
                        {
                            var newsAll = Mapper<NewsAll>.MapNewsAll(playlist, new NewsAll());
                            var photoInAlbum = new VideoInPlaylist
                            {
                                PlaylistId = playlist.Id,
                                VideoId = videoId,
                                Priority = 0,
                                PlayOnTime = DateTime.Now
                            };
                            transac.HashSetAsync(_context.NameOf(playlist.GetType().Name), playlist.Id, Json.Stringify(playlist));
                            transac.HashSetAsync(_context.NameOf<VideoInPlaylist>(), playlist.Id + IndexKeys.SeparateChar + videoId, Json.Stringify(photoInAlbum));
                            transac.SortedSetAddAsync(_context.NameOf(IndexKeys.KeyVideoInPlaylist<VideoInPlaylist>(playlist.Id)), videoId, (photoInAlbum.PlayOnTime ?? DateTime.Now).ConvertToTimestamp10());

                            transac.HashSetAsync(_context.NameOf<NewsAll>(), newsAll.Id, Json.Stringify(newsAll));
                        }
                    });
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
            }
            return returnValue;
        }


        public Task<PagingDataResult<VideoPlaylistSearch>> GetListApproveAsync(SearchNews data, long accountId, Action<string> action)
        {
            var returnValue = Task.FromResult(default(PagingDataResult<VideoPlaylistSearch>));
            try
            {
                using (var _context = GetContext())
                {
                    returnValue = _context.ExecAsync(async (db) =>
                    {
                        var listMemberId = await db.SortedSetRangeByRankAsync(_context.NameOf(IndexKeys.KeyAccountMemberInOfficer<AccountMember>(data.OfficerId)), 0, -1, Order.Descending, CommandFlags.PreferSlave);
                        var memberValid = listMemberId?.Where(p => p.ToString().Equals(accountId.ToString()))?.Count() > 0;

                        if(memberValid == true)
                        {
                            var result = new PagingDataResult<VideoPlaylistSearch>()
                            {
                                Data = new List<VideoPlaylistSearch>()
                            };
                            try
                            {
                                var listKey = new RedisValue[] { };
                                var listPlaylist = new List<VideoPlaylistSearch>();
                                var total = default(long);
                                var tempKey = _context.NameOf("index:temp:union:" + Generator.SorterSetSearchId());

                                var listkeyFull = new List<RedisKey>();
                                listkeyFull.Add(_context.NameOf(IndexKeys.KeyStatus<NewsAll>(int.Parse(data.Status))));

                                var keyApprove = new List<RedisKey>();
                                keyApprove.Add(_context.NameOf(IndexKeys.KeyApprove<VideoPlaylist>(data.OfficerId)));
                                keyApprove.Add(_context.NameOf(IndexKeys.KeyApproveInAccount<VideoPlaylist>(data.OfficerId, accountId)));

                                var tempKeyApprove = _context.NameOf("index:temp:approve:" + Generator.SorterSetSearchId());

                                var valueApprove = await db.SortedSetCombineAndStoreAsync(SetOperation.Union
                                            , tempKeyApprove, keyApprove.ToArray(), null, Aggregate.Max, CommandFlags.None);

                                if (valueApprove > 0)
                                {
                                    listkeyFull.Add(tempKeyApprove);
                                    var value = await db.SortedSetCombineAndStoreAsync(SetOperation.Intersect
                                            , tempKey, listkeyFull.ToArray(), null, Aggregate.Max, CommandFlags.None);
                                    if (value > 0)
                                    {
                                        try
                                        {
                                            listKey = (await db.SortedSetRangeByScoreAsync(tempKey
                                            , (data.FromDate ?? DateTime.MinValue).ConvertToTimestamp10()
                                            , (data.ToDate ?? DateTime.Now).ConvertToTimestamp10()
                                            , Exclude.None
                                            , data.OrderBy == SearchNewsOrderBy.AscCreatedDate ? Order.Ascending : Order.Descending
                                            , ((data.PageIndex ?? 1) - 1) * (data.PageSize ?? 20)
                                            , data.PageSize ?? 20
                                            , CommandFlags.None));

                                            total = await db.SortedSetLengthAsync(tempKey
                                                , (data.FromDate ?? DateTime.MinValue).ConvertToTimestamp10()
                                                , (data.ToDate ?? DateTime.Now).ConvertToTimestamp10()
                                                , Exclude.None
                                                , CommandFlags.None);

                                            result.Total = (int)total;
                                            listPlaylist = listKey != null && listKey.Length > 0 ? (await db.HashGetAsync(_context.NameOf(typeof(VideoPlaylist).Name), listKey, CommandFlags.PreferSlave))?.Select(p => Json.Parse<VideoPlaylistSearch>(p))?.ToList() : listPlaylist;

                                            if (listPlaylist != null)
                                            {
                                                foreach (var article in listPlaylist)
                                                {
                                                    try
                                                    {
                                                        //videoPlaylistSearchReturn.VideoInPlaylist = await
                                                        var listKeyVideo = await db.SortedSetRangeByRankAsync(_context.NameOf(IndexKeys.KeyVideoInPlaylist<VideoInPlaylist>(article.Id)), 0, -1, Order.Ascending, CommandFlags.PreferSlave);
                                                        article.VideoInPlaylist = (await db.HashGetAsync(_context.NameOf<VideoInPlaylist>(), listKeyVideo?.Select(p => (RedisValue)(article.Id + IndexKeys.SeparateChar + p))?.ToArray(), CommandFlags.PreferSlave))?.Select(p => Json.Parse<VideoInPlaylist>(p))?.ToArray();
                                                    }
                                                    catch (Exception ex)
                                                    {
                                                        Logger.Error(ex, ex.Message);
                                                    }
                                                }

                                                result.Data.AddRange(listPlaylist);
                                            }
                                        }
                                        catch (Exception ex)
                                        {
                                            Logger.Error(ex, ex.Message);
                                        }
                                        action(tempKey);
                                    }
                                    action(tempKeyApprove);
                                }
                            }
                            catch (Exception ex)
                            {
                                Logger.Error(ex, ex.Message);
                            }

                            return result;
                        }
                        else
                        {
                            return new PagingDataResult<VideoPlaylistSearch>();
                        }
                    });
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
            }
            return returnValue;
        }

        public async Task<RedisValue[]> GetAllAsync()
        {
            var returnValue = default(RedisValue[]);
            try
            {
                using (var _context = GetContext())
                {
                    returnValue = await _context.ExecAsync(async (db) =>
                    {
                        var tempKey = _context.NameOf("index:temp:union:" + Generator.SorterSetSearchId());
                        var value = await db.SortedSetCombineAndStoreAsync(SetOperation.Intersect
                                        , tempKey, _context.NameOf(IndexKeys.KeyStatus<NewsAll>((int)NewsStatus.Published)), _context.NameOf(IndexKeys.KeyType<NewsAll>((int)NewsType.PlayList)));

                        returnValue = await db.SortedSetRangeByRankAsync(tempKey, 0, -1);
                        await db.KeyDeleteAsync(tempKey);
                        return returnValue;
                    });
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
            }
            return returnValue;
        }
    }
}
