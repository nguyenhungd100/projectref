﻿using ChannelVN.IMS2.Core.Entities.Security;
using ChannelVN.IMS2.Foundation.Common;
using ChannelVN.IMS2.Foundation.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ChannelVN.IMS2.Core.Models.CacheStore.Security
{
    public class SystemToolkitCacheStore : CmsMainCacheStore
    {        
        public Task<bool> InitRedisKeyMobileFromDB(List<Account> data)
        {
            var returnValue = Task.FromResult(false);
            try
            {
                using (var _context = GetContext())
                {
                    returnValue = _context.MultiAsync((transac) =>
                    {         
                        for(var i =0;i< data.Count(); i++)
                        {
                            if (data[i] != null)
                            {
                                if (!string.IsNullOrEmpty(data[i].Mobile))
                                {
                                    transac.SortedSetAddAsync(_context.NameOf(IndexKeys.KeyPhoneNumberInSytem<Account>()), data[i].Mobile?.Trim(), data[i].Id);
                                }
                            }
                        }
                    });
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
            }
            return returnValue;
        }

        public Task<bool> InitRedisKeyEmailFromDB(List<Account> data)
        {
            var returnValue = Task.FromResult(false);
            try
            {
                using (var _context = GetContext())
                {
                    returnValue = _context.MultiAsync((transac) =>
                    {
                        for (var i = 0; i < data.Count(); i++)
                        {
                            if (data[i] != null)
                            {
                                if (!string.IsNullOrEmpty(data[i].Email))
                                {
                                    transac.SortedSetAddAsync(_context.NameOf(IndexKeys.KeyEmailInSytem<Account>()), data[i].Email?.Trim(), data[i].Id);
                                }
                            }
                        }
                    });
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
            }
            return returnValue;
        }

        public Task<bool> DeleteRedisByKey(string key)
        {
            var returnValue = Task.FromResult(false);
            try
            {
                using (var _context = GetContext())
                {
                    returnValue = _context.MultiAsync((transac) =>
                    {                        
                        transac.KeyDeleteAsync(_context.NameOf(key));
                    });
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
            }
            return returnValue;
        }
    }
}
