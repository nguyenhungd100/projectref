﻿using ChannelVN.IMS2.Core.Entities;
using ChannelVN.IMS2.Core.Entities.Beam;
using ChannelVN.IMS2.Core.Entities.Distribution;
using ChannelVN.IMS2.Core.Entities.Media;
using ChannelVN.IMS2.Core.Entities.Newsletters;
using ChannelVN.IMS2.Core.Entities.Photo;
using ChannelVN.IMS2.Core.Entities.Posts;
using ChannelVN.IMS2.Core.Entities.Security;
using ChannelVN.IMS2.Core.Entities.ShareLinks;
using ChannelVN.IMS2.Core.Entities.Videos;
using ChannelVN.IMS2.Foundation.Common;
using ChannelVN.IMS2.Foundation.Data.Providers;
using ChannelVN.IMS2.Foundation.Logging;
using StackExchange.Redis;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ChannelVN.IMS2.Core.Models.CacheStore
{
    public class NewsCacheStore : CmsMainCacheStore
    {
        public Task<List<NewsAllSearch>> GetListByKeysAndIdsAsync(List<NewsAllSearch> listData)
        {
            var returnValue = Task.FromResult(default(List<NewsAllSearch>));
            if (listData == null) return returnValue;
            try
            {
                using(var _context = GetContext())
                {
                    returnValue = _context.ExecAsync(async (db) =>
                    {
                        var listKeyId = new Dictionary<int, List<long>>();

                        listData.ForEach(p =>
                        {
                            var key = p.Type ?? 0;

                            if (listKeyId.ContainsKey(key))
                            {
                                listKeyId[key].Add(p.Id);
                            }
                            else
                            {
                                listKeyId[key] = new List<long>() { p.Id };
                            }
                        });

                        foreach (var item in listKeyId)
                        {
                            switch (item.Key)
                            {
                                case (int)NewsType.Article:
                                    var listArticle = (await db.HashGetAsync(_context.NameOf<Article>(), item.Value?.Select(p => (RedisValue)p).ToArray(), CommandFlags.PreferSlave))?.Select(p => Json.Parse<Article>(p));
                                    listData.ForEach(p =>
                                    {
                                        p.PublishData = listArticle?.Where(c => c.Id == p.Id)?.FirstOrDefault()?.PublishData;
                                    });
                                    break;
                                case (int)NewsType.Video:
                                    var listVideo = (await db.HashGetAsync(_context.NameOf<Video>(), item.Value?.Select(p => (RedisValue)p).ToArray(), CommandFlags.PreferSlave))?.Select(p => Json.Parse<Video>(p));
                                    listData.ForEach(p =>
                                    {
                                        p.PublishData = listVideo?.Where(c => c.Id == p.Id)?.FirstOrDefault()?.PublishData;
                                    });
                                    break;
                                case (int)NewsType.PlayList:
                                    var listPlaylist = (await db.HashGetAsync(_context.NameOf<VideoPlaylist>(), item.Value?.Select(p => (RedisValue)p).ToArray(), CommandFlags.PreferSlave))?.Select(p => Json.Parse<VideoPlaylist>(p));
                                    listData.ForEach(p =>
                                    {
                                        var tmp = listPlaylist?.Where(c => c.Id == p.Id)?.FirstOrDefault();
                                        p.PublishData = tmp?.PublishData;
                                        p.Cover = tmp?.Cover;
                                    });
                                    break;
                                case (int)NewsType.MediaUnit:
                                    var listMediaUnit = (await db.HashGetAsync(_context.NameOf<MediaUnit>(), item.Value?.Select(p => (RedisValue)p).ToArray(), CommandFlags.PreferSlave))?.Select(p => Json.Parse<MediaUnit>(p));
                                    listData.ForEach(p =>
                                    {
                                        p.PublishData = listMediaUnit?.Where(c => c.Id == p.Id)?.FirstOrDefault()?.PublishData;
                                    });
                                    break;
                                case (int)NewsType.Post:
                                    var listPost = (await db.HashGetAsync(_context.NameOf<Post>(), item.Value?.Select(p => (RedisValue)p).ToArray(), CommandFlags.PreferSlave))?.Select(p => Json.Parse<Post>(p));
                                    listData.ForEach(p =>
                                    {
                                        p.PublishData = listPost?.Where(c => c.Id == p.Id)?.FirstOrDefault()?.PublishData;
                                    });
                                    break;
                                case (int)NewsType.Photo:
                                    var listPhoto = (await db.HashGetAsync(_context.NameOf<PhotoUnit>(), item.Value?.Select(p => (RedisValue)p).ToArray(), CommandFlags.PreferSlave))?.Select(p => Json.Parse<PhotoUnit>(p));
                                    listData.ForEach(p =>
                                    {
                                        p.PublishData = listPhoto?.Where(c => c.Id == p.Id)?.FirstOrDefault()?.PublishData;
                                    });
                                    break;
                                case (int)NewsType.ShareLink:
                                    var listShareLink = (await db.HashGetAsync(_context.NameOf<ShareLink>(), item.Value?.Select(p => (RedisValue)p).ToArray(), CommandFlags.PreferSlave))?.Select(p => Json.Parse<ShareLink>(p));
                                    listData.ForEach(p =>
                                    {
                                        p.PublishData = listShareLink?.Where(c => c.Id == p.Id)?.FirstOrDefault()?.PublishData;
                                    });
                                    break;
                                case (int)NewsType.Album:
                                    var listAlbum = (await db.HashGetAsync(_context.NameOf<Entities.Album.Album>(), item.Value?.Select(p => (RedisValue)p).ToArray(), CommandFlags.PreferSlave))?.Select(p => Json.Parse<Entities.Album.Album>(p));
                                    listData.ForEach(p =>
                                    {
                                        p.PublishData = listAlbum?.Where(c => c.Id == p.Id)?.FirstOrDefault()?.PublishData;
                                    });
                                    break;
                                case (int)NewsType.Beam:
                                    var listBeam = (await db.HashGetAsync(_context.NameOf<Beam>(), item.Value?.Select(p => (RedisValue)p).ToArray(), CommandFlags.PreferSlave))?.Select(p => Json.Parse<Beam>(p));
                                    listData.ForEach(p =>
                                    {
                                        p.PublishData = listBeam?.Where(c => c.Id == p.Id)?.FirstOrDefault()?.PublishData;
                                    });
                                    break;

                            }
                        }
                        return listData;
                    });
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
            }
            return returnValue;
        }

        public Task<PagingDataResult<NewsAllSearch>> SearchAsync(SearchNews data, long accountId, Action<string> action, bool isDistribution = false)
        {
            var returnValue = Task.FromResult(default(PagingDataResult<NewsAllSearch>));

            try
            {
                using (var _context = GetContext())
                {
                    returnValue = _context.ExecAsync(async (db) =>
                    {
                        var result = new PagingDataResult<NewsAllSearch>
                        {
                            Data = new List<NewsAllSearch>()
                        };

                        try
                        {
                            var listKey = new RedisValue[] { };
                            var listNews = new List<NewsAllSearch>();
                            var total = default(long);
                            var tempKey = _context.NameOf("index:temp:union:" + Generator.SorterSetSearchId());
                            var createdByKey = _context.NameOf(IndexKeys.KeyCreatedBy<NewsAll>(accountId.ToString()));
                            var listkeyFull = new List<RedisKey>();

                            var listOwner = await _context.SortedSetRangeByRankAsync(IndexKeys.KeyOwnerOfficer(accountId), 0, -1, Order.Descending, CommandFlags.PreferSlave);
                            if (listOwner == null || !listOwner.Contains((RedisValue)data.OfficerId))
                            {
                                listkeyFull.Add(createdByKey);
                            }
                            //listkeyFull.Add(createdByKey);

                            listkeyFull.Add(_context.NameOf(IndexKeys.KeyNewsInAccount(data.OfficerId)));

                            if (isDistribution) data.Status = ((int)NewsStatus.Published).ToString();

                            switch (string.IsNullOrEmpty(data.Status))
                            {
                                case true:
                                    if (data.DistributionId == -1)
                                    {
                                        listkeyFull.Add(_context.NameOf(IndexKeys.KeyDistributorid<NewsDistribution>()));
                                    }
                                    else
                                    {
                                        if (data.DistributionId > 0)
                                        {
                                            listkeyFull.Add(_context.NameOf(IndexKeys.KeyDistributorid<NewsDistribution>()));
                                            listkeyFull.Add(_context.NameOf(IndexKeys.KeyDistributorid<NewsDistribution>(data.DistributionId)));
                                        }
                                    }
                                    if (data.Type != null)
                                    {
                                        listkeyFull.Add(_context.NameOf(IndexKeys.KeyType<NewsAll>((int)data.Type)));
                                    }
                                    var value1 = await db.SortedSetCombineAndStoreAsync(SetOperation.Intersect, tempKey, listkeyFull.ToArray(), null, Aggregate.Max, CommandFlags.None);
                                    if (value1 > 0)
                                    {
                                        try
                                        {
                                            listKey = (await db.SortedSetRangeByScoreAsync(tempKey
                                            , Utility.ConvertToTimestamp10(data.FromDate ?? DateTime.MinValue)
                                            , Utility.ConvertToTimestamp10(data.ToDate ?? DateTime.Now)
                                            , Exclude.None
                                            , data.OrderBy == SearchNewsOrderBy.AscCreatedDate ? Order.Ascending : Order.Descending
                                            , ((data.PageIndex ?? 1) - 1) * (data.PageSize ?? 20)
                                            , data.PageSize ?? 20, CommandFlags.None));

                                            total = await db.SortedSetLengthAsync(tempKey
                                                , Utility.ConvertToTimestamp10(data.FromDate ?? DateTime.MinValue)
                                                , Utility.ConvertToTimestamp10(data.ToDate ?? DateTime.Now), Exclude.None, CommandFlags.None);

                                            result.Total = (int)total;
                                            var listNewsRedis = listKey != null && listKey.Length > 0 ? (await db.HashGetAsync(_context.NameOf<NewsAll>(), listKey, CommandFlags.PreferSlave))?.Select(p => Json.Parse<NewsAll>(p)) : new List<NewsAll>();
                                            listNews = listNewsRedis?.Select(p => Mapper<NewsAllSearch>.Map(p, new NewsAllSearch())).ToList();
                                            if (listNews != null)
                                            {
                                                foreach (var news in listNews)
                                                {
                                                    try
                                                    {
                                                        var listTmp = new List<NewsDistribution>();// news.NewsDistribution = new NewsDistribution[] { };
                                                        var newsDistribution = await db.SortedSetRangeByScoreWithScoresAsync(_context.NameOf(IndexKeys.KeyNewsDistribution(news.Id)), double.MinValue, double.MaxValue, Exclude.None, Order.Ascending, 0, -1, CommandFlags.PreferSlave);
                                                        if (newsDistribution != null)
                                                        {
                                                            foreach (var item in newsDistribution)
                                                            {
                                                                if (long.TryParse(item.Element.ToString().Split(IndexKeys.SeparateChar).FirstOrDefault(), out long distributorId))
                                                                {
                                                                    listTmp.Add(new NewsDistribution
                                                                    {
                                                                        DistributorId = distributorId,
                                                                        SharedDate = Utility.ConvertSecondsToDate(item.Score),
                                                                        DistributorName = Json.Parse<Distributor>(await db.HashGetAsync(_context.NameOf<Distributor>(), distributorId, CommandFlags.PreferSlave))?.Name
                                                                    });
                                                                }
                                                            }
                                                        }
                                                        news.NewsDistribution = listTmp.ToArray();
                                                    }
                                                    catch (Exception ex)
                                                    {
                                                        Logger.Error(ex, ex.Message);
                                                    }
                                                }
                                            }
                                            result.Data = listNews;
                                        }
                                        catch (Exception ex)
                                        {
                                            Logger.Error(ex, ex.Message);
                                        }
                                        action(tempKey);
                                    }
                                    break;
                                case false:
                                    if (data.DistributionId == -1)
                                    {
                                        listkeyFull.Add(_context.NameOf(IndexKeys.KeyDistributorid<NewsDistribution>()));
                                    }
                                    else
                                    {
                                        if (data.DistributionId > 0)
                                        {
                                            listkeyFull.Add(_context.NameOf(IndexKeys.KeyDistributorid<NewsDistribution>(data.DistributionId)));
                                        }
                                    }
                                    if (data.Type != null)
                                    {
                                        listkeyFull.Add(_context.NameOf(IndexKeys.KeyType<NewsAll>((int)data.Type)));
                                    }

                                    var listRedisKeyStatus = new List<RedisKey>();
                                    var listStrStatus = data.Status.Split(",");
                                    foreach (var s in listStrStatus)
                                    {
                                        if (int.TryParse(s, out int status))
                                        {
                                            if (Enum.IsDefined(typeof(NewsStatus), status))
                                            {
                                                listRedisKeyStatus.Add(_context.NameOf(IndexKeys.KeyStatus<NewsAll>(status)));
                                            }
                                        }
                                    }
                                    var tempKey2 = _context.NameOf("index:temp:" + Generator.SorterSetSearchId());
                                    var values2 = await db.SortedSetCombineAndStoreAsync(SetOperation.Union
                                            , tempKey2, listRedisKeyStatus.ToArray(), null, Aggregate.Max, CommandFlags.None);
                                    if (values2 > 0)
                                    {
                                        try
                                        {
                                            listkeyFull.Add(tempKey2);
                                            var value3 = await db.SortedSetCombineAndStoreAsync(SetOperation.Intersect
                                                 , tempKey, listkeyFull.ToArray(), null, Aggregate.Max, CommandFlags.None);
                                            if (value3 > 0)
                                            {
                                                try
                                                {
                                                    total = await db.SortedSetLengthAsync(tempKey
                                                    , Utility.ConvertToTimestamp10(data.FromDate ?? DateTime.MinValue)
                                                    , Utility.ConvertToTimestamp10(data.ToDate ?? DateTime.Now), Exclude.None, CommandFlags.None);
                                                    result.Total = (int)total;
                                                    listKey = (await db.SortedSetRangeByScoreAsync(tempKey
                                                    , Utility.ConvertToTimestamp10(data.FromDate ?? DateTime.MinValue)
                                                    , Utility.ConvertToTimestamp10(data.ToDate ?? DateTime.Now)
                                                    , Exclude.None
                                                    , data.OrderBy == SearchNewsOrderBy.AscCreatedDate ? Order.Ascending : Order.Descending
                                                    , ((data.PageIndex ?? 1) - 1) * (data.PageSize ?? 20)
                                                    , data.PageSize ?? 20, CommandFlags.None));

                                                    var listNewsRedis2 = listKey != null && listKey.Length > 0 ? (await db.HashGetAsync(_context.NameOf<NewsAll>(), listKey, CommandFlags.PreferSlave))?.Select(p => Json.Parse<NewsAll>(p)) : new List<NewsAll>();
                                                    listNews = listNewsRedis2?.Select(p => Mapper<NewsAllSearch>.Map(p, new NewsAllSearch())).ToList();
                                                    if (listNews != null)
                                                    {
                                                        foreach (var news in listNews)
                                                        {
                                                            try
                                                            {
                                                                var listTmp = new List<NewsDistribution>();
                                                                //news.NewsDistribution = new NewsDistribution[] { };
                                                                var newsDistribution = await db.SortedSetRangeByScoreWithScoresAsync(_context.NameOf(IndexKeys.KeyNewsDistribution(news.Id)), double.MinValue, double.MaxValue, Exclude.None, Order.Ascending, 0, -1, CommandFlags.PreferSlave);
                                                                if (newsDistribution != null)
                                                                {
                                                                    foreach (var item in newsDistribution)
                                                                    {
                                                                        if (long.TryParse(item.Element.ToString().Split(IndexKeys.SeparateChar).FirstOrDefault(), out long distributorId))
                                                                        {
                                                                            listTmp.Add(new NewsDistribution
                                                                            {
                                                                                DistributorId = distributorId,
                                                                                SharedDate = Utility.ConvertSecondsToDate(item.Score),
                                                                                DistributorName = Json.Parse<Distributor>(await db.HashGetAsync(_context.NameOf<Distributor>(), distributorId, CommandFlags.PreferSlave))?.Name
                                                                            });
                                                                        }
                                                                    }
                                                                }
                                                                news.NewsDistribution = listTmp.ToArray();
                                                            }
                                                            catch (Exception ex)
                                                            {
                                                                Logger.Error(ex, ex.Message);
                                                            }
                                                        }
                                                    }
                                                    result.Data = listNews;
                                                }
                                                catch (Exception ex)
                                                {
                                                    Logger.Error(ex, ex.Message);
                                                }
                                                action(tempKey);
                                            }
                                        }
                                        catch (Exception ex)
                                        {
                                            Logger.Error(ex, ex.Message);
                                        }
                                        action(tempKey2);
                                    }
                                    break;
                            }
                        }
                        catch (Exception ex)
                        {
                            Logger.Error(ex, ex.Message);
                        }

                        return result;
                    });
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
            }
            return returnValue;
        }
        public Task<PagingDataResult<NewsAllSearch>> NewsOnPageAsync(SearchNews data, long accountId, Action<string> action)
        {
            var returnValue = Task.FromResult(default(PagingDataResult<NewsAllSearch>));

            try
            {
                using (var _context = GetContext())
                {
                    returnValue = _context.ExecAsync(async (db) =>
                    {
                        var result = new PagingDataResult<NewsAllSearch>
                        {
                            Data = new List<NewsAllSearch>()
                        };

                        try
                        {
                            var listKey = new RedisValue[] { };
                            var listNews = new List<NewsAllSearch>();
                            var total = default(long);
                            var tempKey = _context.NameOf("index:temp:union:" + Generator.SorterSetSearchId());
                            //var createdByKey = _context.NameOf(IndexKeys.KeyCreatedBy<NewsAll>(accountId.ToString()));
                            var listkeyFull = new List<RedisKey>();

                            //var listOwner = await _context.SortedSetRangeByRankAsync(IndexKeys.KeyOwnerOfficer(accountId), 0, -1, Order.Descending, CommandFlags.PreferSlave);
                            //if (listOwner == null || !listOwner.Contains((RedisValue)data.OfficerId))
                            //{
                            //    listkeyFull.Add(createdByKey);
                            //}
                            listkeyFull.Add(_context.NameOf(IndexKeys.KeyNewsInAccount(data.OfficerId)));

                            if (data.DistributionId == -1)
                            {
                                listkeyFull.Add(_context.NameOf(IndexKeys.KeyDistributorid<NewsDistribution>()));
                            }
                            else
                            {
                                if (data.DistributionId > 0)
                                {
                                    listkeyFull.Add(_context.NameOf(IndexKeys.KeyDistributorid<NewsDistribution>(data.DistributionId)));
                                }
                            }
                            if (data.Type != null)
                            {
                                listkeyFull.Add(_context.NameOf(IndexKeys.KeyType<NewsAll>((int)data.Type)));
                            }

                            try
                            {
                                listkeyFull.Add(_context.NameOf(IndexKeys.KeyStatus<NewsAll>(int.Parse(data.Status))));
                                var value3 = await db.SortedSetCombineAndStoreAsync(SetOperation.Intersect
                                     , tempKey, listkeyFull.ToArray(), null, Aggregate.Max, CommandFlags.None);
                                if (value3 > 0)
                                {
                                    try
                                    {
                                        total = await db.SortedSetLengthAsync(tempKey
                                        , Utility.ConvertToTimestamp10(data.FromDate ?? DateTime.MinValue)
                                        , Utility.ConvertToTimestamp10(data.ToDate ?? DateTime.MaxValue), Exclude.None, CommandFlags.None);
                                        result.Total = (int)total;
                                        listKey = (await db.SortedSetRangeByScoreAsync(tempKey
                                        , Utility.ConvertToTimestamp10(data.FromDate ?? DateTime.MinValue)
                                        , Utility.ConvertToTimestamp10(data.ToDate ?? DateTime.MaxValue)
                                        , Exclude.None
                                        , data.OrderBy == SearchNewsOrderBy.AscCreatedDate ? Order.Ascending : Order.Descending
                                        , ((data.PageIndex ?? 1) - 1) * (data.PageSize ?? 20)
                                        , data.PageSize ?? 20, CommandFlags.None));

                                        var listNewsRedis2 = listKey != null && listKey.Length > 0 ? (await db.HashGetAsync(_context.NameOf<NewsAll>(), listKey, CommandFlags.PreferSlave))?.Select(p => Json.Parse<NewsAll>(p)) : new List<NewsAll>();
                                        listNews = listNewsRedis2?.Select(p => Mapper<NewsAllSearch>.Map(p, new NewsAllSearch())).ToList();
                                        if (listNews != null)
                                        {
                                            foreach (var news in listNews)
                                            {
                                                try
                                                {
                                                    var listTmp = new List<NewsDistribution>();
                                                    //news.NewsDistribution = new NewsDistribution[] { };
                                                    var newsDistribution = await db.SortedSetRangeByScoreWithScoresAsync(_context.NameOf(IndexKeys.KeyNewsDistribution(news.Id)), double.MinValue, double.MaxValue, Exclude.None, Order.Ascending, 0, -1, CommandFlags.PreferSlave);
                                                    if (newsDistribution != null)
                                                    {
                                                        foreach (var item in newsDistribution)
                                                        {
                                                            if (long.TryParse(item.Element.ToString().Split(IndexKeys.SeparateChar).FirstOrDefault(), out long distributorId))
                                                            {
                                                                listTmp.Add(new NewsDistribution
                                                                {
                                                                    DistributorId = distributorId,
                                                                    SharedDate = Utility.ConvertSecondsToDate(item.Score),
                                                                    DistributorName = Json.Parse<Distributor>(await db.HashGetAsync(_context.NameOf<Distributor>(), distributorId, CommandFlags.PreferSlave))?.Name
                                                                });
                                                            }
                                                        }
                                                    }
                                                    news.NewsDistribution = listTmp.ToArray();
                                                }
                                                catch (Exception ex)
                                                {
                                                    Logger.Error(ex, ex.Message);
                                                }
                                            }
                                        }
                                        result.Data = listNews;
                                    }
                                    catch (Exception ex)
                                    {
                                        Logger.Error(ex, ex.Message);
                                    }
                                    action(tempKey);
                                }
                            }
                            catch (Exception ex)
                            {
                                Logger.Error(ex, ex.Message);
                            }
                        }
                        catch (Exception ex)
                        {
                            Logger.Error(ex, ex.Message);
                        }

                        return result;
                    });
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
            }
            return returnValue;
        }
        
        public Task<PagingDataResult<NewsAll>> SearchNewsAsync(SearchNews data, Action<string> action)
        {
            var returnValue = Task.FromResult(default(PagingDataResult<NewsAll>));

            try
            {
                using (var _context = GetContext())
                {
                    returnValue = _context.ExecAsync(async (db) =>
                    {
                        var result = new PagingDataResult<NewsAll>
                        {
                            Data = new List<NewsAll>()
                        };

                        try
                        {
                            var listKey = new RedisValue[] { };
                            var listArticle = new List<NewsAll>();
                            var total = default(long);
                            var tempKey = _context.NameOf("index:temp:union:" + Generator.SorterSetSearchId());
                            var listkeyFull = new List<RedisKey>();

                            listkeyFull.Add(_context.NameOf(IndexKeys.KeyNewsInAccount(data.OfficerId)));
                            var listKeyType = new List<RedisKey>
                            {
                                _context.NameOf(IndexKeys.KeyType<NewsAll>((int)NewsType.Article)),
                                 _context.NameOf(IndexKeys.KeyType<NewsAll>((int)NewsType.ShareLink))
                            };
                            var tempType = _context.NameOf("index:temp:type:" + Generator.SorterSetSearchId());
                            var valueType = await db.SortedSetCombineAndStoreAsync(SetOperation.Union
                                            , tempType, listKeyType.ToArray(), null, Aggregate.Max, CommandFlags.None);
                            if (valueType > 0)
                            {
                                try
                                {
                                    listkeyFull.Add(tempType);
                                    switch (string.IsNullOrEmpty(data.Status))
                                    {
                                        case true:
                                            if (data.DistributionId == -1)
                                            {
                                                listkeyFull.Add(_context.NameOf(IndexKeys.KeyDistributorid<NewsDistribution>()));
                                            }
                                            else
                                            {
                                                if (data.DistributionId > 0)
                                                {
                                                    listkeyFull.Add(_context.NameOf(IndexKeys.KeyDistributorid<NewsDistribution>(data.DistributionId)));
                                                }
                                            }
                                            var value1 = await db.SortedSetCombineAndStoreAsync(SetOperation.Intersect
                                                        , tempKey, listkeyFull.ToArray(), null, Aggregate.Max, CommandFlags.None);
                                            if (value1 > 0)
                                            {
                                                try
                                                {
                                                    listKey = (await db.SortedSetRangeByScoreAsync(tempKey
                                                    , Utility.ConvertToTimestamp10(data.FromDate ?? DateTime.MinValue)
                                                    , Utility.ConvertToTimestamp10(data.ToDate ?? DateTime.Now)
                                                    , Exclude.None
                                                    , data.OrderBy == SearchNewsOrderBy.AscCreatedDate ? Order.Ascending : Order.Descending
                                                    , ((data.PageIndex ?? 1) - 1) * (data.PageSize ?? 20)
                                                    , data.PageSize ?? 20
                                                    , CommandFlags.None));

                                                    total = await db.SortedSetLengthAsync(tempKey
                                                        , Utility.ConvertToTimestamp10(data.FromDate ?? DateTime.MinValue)
                                                        , Utility.ConvertToTimestamp10(data.ToDate ?? DateTime.Now)
                                                        , Exclude.None
                                                        , CommandFlags.None);

                                                    result.Total = (int)total;
                                                    listArticle = listKey != null && listKey.Length > 0 ? (await db.HashGetAsync(_context.NameOf<NewsAll>(), listKey, CommandFlags.PreferSlave))?.Select(p => Json.Parse<NewsAll>(p))?.ToList() : listArticle;
                                                    result.Data = listArticle;
                                                }
                                                catch (Exception ex)
                                                {
                                                    Logger.Error(ex, ex.Message);
                                                }
                                                action(tempKey);
                                            }
                                            break;
                                        case false:
                                            if (data.DistributionId == -1)
                                            {
                                                listkeyFull.Add(_context.NameOf(IndexKeys.KeyDistributorid<NewsDistribution>()));
                                            }
                                            else
                                            {
                                                if (data.DistributionId > 0)
                                                {
                                                    listkeyFull.Add(_context.NameOf(IndexKeys.KeyDistributorid<NewsDistribution>(data.DistributionId)));
                                                }
                                            }

                                            var listRedisKeyStatus = new List<RedisKey>();
                                            var listStrStatus = data.Status.Split(",");
                                            foreach (var s in listStrStatus)
                                            {
                                                if (int.TryParse(s, out int status))
                                                {
                                                    if (Enum.IsDefined(typeof(NewsStatus), status))
                                                    {
                                                        listRedisKeyStatus.Add(_context.NameOf(IndexKeys.KeyStatus<NewsAll>(status)));
                                                    }
                                                }
                                            }
                                            var tempKey2 = _context.NameOf("index:temp:" + Generator.SorterSetSearchId());
                                            var value2 = await db.SortedSetCombineAndStoreAsync(SetOperation.Union
                                                    , tempKey2, listRedisKeyStatus.ToArray(), null, Aggregate.Max, CommandFlags.None);
                                            if (value2 > 0)
                                            {
                                                try
                                                {
                                                    listkeyFull.Add(tempKey2);
                                                    var value3 = await db.SortedSetCombineAndStoreAsync(SetOperation.Intersect
                                                        , tempKey, listkeyFull.ToArray(), null, Aggregate.Max, CommandFlags.None);
                                                    if (value3 > 0)
                                                    {
                                                        try
                                                        {
                                                            total = await db.SortedSetLengthAsync(tempKey
                                                            , Utility.ConvertToTimestamp10(data.FromDate ?? DateTime.MinValue)
                                                            , Utility.ConvertToTimestamp10(data.ToDate ?? DateTime.Now)
                                                            , Exclude.None
                                                            , CommandFlags.None);

                                                            result.Total = (int)total;

                                                            listKey = (await db.SortedSetRangeByScoreAsync(tempKey
                                                            , Utility.ConvertToTimestamp10(data.FromDate ?? DateTime.MinValue)
                                                            , Utility.ConvertToTimestamp10(data.ToDate ?? DateTime.Now)
                                                            , Exclude.None
                                                            , data.OrderBy == SearchNewsOrderBy.AscCreatedDate ? Order.Ascending : Order.Descending
                                                            , ((data.PageIndex ?? 1) - 1) * (data.PageSize ?? 20)
                                                            , data.PageSize ?? 20
                                                            , CommandFlags.None));

                                                            listArticle = listKey != null && listKey.Length > 0 ? (await db.HashGetAsync(_context.NameOf<NewsAll>(), listKey, CommandFlags.PreferSlave))?.Select(p => Json.Parse<NewsAll>(p))?.ToList() : listArticle;
                                                            result.Data = listArticle;
                                                        }
                                                        catch (Exception ex)
                                                        {
                                                            Logger.Error(ex, ex.Message);
                                                        }
                                                        action(tempKey);
                                                    }
                                                }
                                                catch (Exception ex)
                                                {
                                                    Logger.Error(ex, ex.Message);
                                                }
                                                action(tempKey2);
                                            }
                                            break;
                                    }
                                }
                                catch (Exception ex)
                                {
                                    Logger.Error(ex, ex.Message);
                                }
                                action(tempType);
                            }
                        }
                        catch (Exception ex)
                        {
                            Logger.Error(ex, ex.Message);
                        }

                        return result;
                    });
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
            }
            return returnValue;
        }

        public Task<List<NewsAllSearch>> GetListNewsAllSearchAsync(List<string> ids)
        {
            var returnValue = Task.FromResult(default(List<NewsAllSearch>));
            try
            {
                using(var _context = GetContext())
                {
                    returnValue = _context.ExecAsync(async (db) =>
                    {
                        var result = new List<NewsAllSearch>();
                        try
                        {
                            var listNew = (await db.HashGetAsync(_context.NameOf<NewsAll>(), ids?.Select(p => (RedisValue)p)?.ToArray(), CommandFlags.PreferSlave))?.Select(p => Json.Parse<NewsAll>(p));
                            result = listNew?.Select(p => Mapper<NewsAllSearch>.Map(p, new NewsAllSearch()))?.ToList();
                            if (result != null)
                            {
                                foreach (var news in result)
                                {
                                    try
                                    {
                                        var listTmp = new List<NewsDistribution>();
                                        //news.NewsDistribution = new NewsDistribution[] { };
                                        var newsDistribution = await db.SortedSetRangeByScoreWithScoresAsync(_context.NameOf(IndexKeys.KeyNewsDistribution(news.Id)), double.MinValue, double.MaxValue, Exclude.None, Order.Ascending, 0, -1, CommandFlags.PreferSlave);
                                        if (newsDistribution != null)
                                        {
                                            foreach (var item in newsDistribution)
                                            {
                                                if (long.TryParse(item.Element.ToString().Split(IndexKeys.SeparateChar).FirstOrDefault(), out long distributorId))
                                                {
                                                    listTmp.Add(new NewsDistribution
                                                    {
                                                        DistributorId = distributorId,
                                                        SharedDate = Utility.ConvertSecondsToDate(item.Score),
                                                        DistributorName = Json.Parse<Distributor>(await db.HashGetAsync(_context.NameOf<Distributor>(), distributorId, CommandFlags.PreferSlave))?.Name
                                                    });
                                                }
                                            }
                                        }
                                        news.NewsDistribution = listTmp.ToArray();
                                    }
                                    catch (Exception ex)
                                    {
                                        Logger.Error(ex, ex.Message);
                                    }
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            Logger.Error(ex, ex.Message);
                        }

                        return result;
                    });
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
            }
            return returnValue;
        }
        public Task<List<NewsAll>> GetListNewsAsync(List<string> ids)
        {
            var returnValue = Task.FromResult(default(List<NewsAll>));
            try
            {
                using(var _context = GetContext())
                {
                    returnValue = _context.GetsFromHashExAsync<NewsAll>(ids, CommandFlags.PreferSlave);
                }
            }
            catch(Exception ex)
            {
                Logger.Error(ex, ex.Message);
            }
            return returnValue;
        }

        public async Task<bool> ChangeDistributionDateAsync(long newsId, DateTime distributionDate, long accountId, Action<int?> action)
        {
            var returnValue = false;
            try
            {
                using (var _context = GetContext())
                {
                    var member = await _context.ExecAsync<AccountMember>(async (db) =>
                    {
                        var officerInNews = await db.SetMembersAsync(_context.NameOf(IndexKeys.KeyAccountInNews(newsId)));
                        var memberTmp = default(AccountMember);
                        if (officerInNews != null && officerInNews.Count() > 0)
                        {
                            memberTmp = Json.Parse<AccountMember>(await db.HashGetAsync(_context.NameOf<AccountMember>(), officerInNews[0] + IndexKeys.SeparateChar + accountId));
                        }
                        else
                        {
                            Logger.Error($"Lỗi không tìm thấy account in news(id={newsId})");
                        }
                        
                        return memberTmp;
                    });

                    var news = await _context.HashGetExAsync<NewsAll>(newsId, CommandFlags.PreferSlave);

                    if (news != null && news.DistributionDate >= DateTime.Now && news.Status == (int)NewsStatus.Published && (accountId.ToString().Equals(news.CreatedBy) || (member != null && member.IsLocked == false && member.Permissions != null && (member.Permissions.Contains(MemberPermission.ApprovePost) || member.Permissions.Contains(MemberPermission.ModifyPostOnPage) || member.Permissions.Contains(MemberPermission.RemovePostOnPage))))
                        )
                    {
                        news.DistributionDate = distributionDate;
                        switch (news.CardType)
                        {
                            case (int)CardType.Album:
                                var album = await _context.HashGetExAsync<Entities.Album.Album>(newsId);

                                album.DistributionDate = distributionDate;

                                returnValue = await _context.MultiAsync((trans) =>
                                {
                                    trans.HashSetAsync(_context.NameOf<Entities.Album.Album>(), album.Id, Json.Stringify(album));
                                    trans.HashSetAsync(_context.NameOf(news.GetType().Name), news.Id, Json.Stringify(news));

                                    trans.SortedSetAddAsync(_context.NameOf(IndexKeys.KeyStatus<NewsAll>(album.Status)), album.Id, (album.DistributionDate ?? DateTime.Now).ConvertToTimestamp10());
                                });

                                break;
                            case (int)CardType.Article:
                                var article = await _context.HashGetExAsync<Article>(newsId);

                                article.DistributionDate = distributionDate;

                                returnValue = await _context.MultiAsync((trans) =>
                                {
                                    trans.HashSetAsync(_context.NameOf<Article>(), article.Id, Json.Stringify(article));
                                    trans.HashSetAsync(_context.NameOf(news.GetType().Name), news.Id, Json.Stringify(news));

                                    trans.SortedSetAddAsync(_context.NameOf(IndexKeys.KeyStatus<NewsAll>(article.Status)), article.Id, (article.DistributionDate ?? DateTime.Now).ConvertToTimestamp10());
                                });

                                break;
                            case (int)CardType.ListNews:
                                var beam = await _context.HashGetExAsync<Beam>(newsId);

                                beam.DistributionDate = distributionDate;

                                returnValue = await _context.MultiAsync((trans) =>
                                {
                                    trans.HashSetAsync(_context.NameOf<Beam>(), beam.Id, Json.Stringify(beam));
                                    trans.HashSetAsync(_context.NameOf(news.GetType().Name), news.Id, Json.Stringify(news));

                                    trans.SortedSetAddAsync(_context.NameOf(IndexKeys.KeyStatus<NewsAll>(beam.Status)), beam.Id, (beam.DistributionDate ?? DateTime.Now).ConvertToTimestamp10());
                                });

                                break;
                            case (int)CardType.VideoAndImage:
                                var mediaUnit = await _context.HashGetExAsync<MediaUnit>(newsId);

                                mediaUnit.DistributionDate = distributionDate;

                                returnValue = await _context.MultiAsync((trans) =>
                                {
                                    trans.HashSetAsync(_context.NameOf<MediaUnit>(), mediaUnit.Id, Json.Stringify(mediaUnit));
                                    trans.HashSetAsync(_context.NameOf(news.GetType().Name), news.Id, Json.Stringify(news));

                                    trans.SortedSetAddAsync(_context.NameOf(IndexKeys.KeyStatus<NewsAll>(mediaUnit.Status)), mediaUnit.Id, (mediaUnit.DistributionDate ?? DateTime.Now).ConvertToTimestamp10());
                                });

                                break;
                            case (int)CardType.Photo:
                                var photo = await _context.HashGetExAsync<PhotoUnit>(newsId);

                                photo.DistributionDate = distributionDate;

                                returnValue = await _context.MultiAsync((trans) =>
                                {
                                    trans.HashSetAsync(_context.NameOf<PhotoUnit>(), photo.Id, Json.Stringify(photo));
                                    trans.HashSetAsync(_context.NameOf(news.GetType().Name), news.Id, Json.Stringify(news));

                                    trans.SortedSetAddAsync(_context.NameOf(IndexKeys.KeyStatus<NewsAll>(photo.Status)), photo.Id, (photo.DistributionDate ?? DateTime.Now).ConvertToTimestamp10());
                                });

                                break;
                            case (int)CardType.PlaylistVideo:
                                var playlist = await _context.HashGetExAsync<VideoPlaylist>(newsId);

                                playlist.DistributionDate = distributionDate;

                                returnValue = await _context.MultiAsync((trans) =>
                                {
                                    trans.HashSetAsync(_context.NameOf<VideoPlaylist>(), playlist.Id, Json.Stringify(playlist));
                                    trans.HashSetAsync(_context.NameOf(news.GetType().Name), news.Id, Json.Stringify(news));

                                    trans.SortedSetAddAsync(_context.NameOf(IndexKeys.KeyStatus<NewsAll>(playlist.Status)), playlist.Id, (playlist.DistributionDate ?? DateTime.Now).ConvertToTimestamp10());
                                });

                                break;
                            case (int)CardType.Text:
                                var post = await _context.HashGetExAsync<Post>(newsId);

                                post.DistributionDate = distributionDate;

                                returnValue = await _context.MultiAsync((trans) =>
                                {
                                    trans.HashSetAsync(_context.NameOf<Post>(), post.Id, Json.Stringify(post));
                                    trans.HashSetAsync(_context.NameOf(news.GetType().Name), news.Id, Json.Stringify(news));

                                    trans.SortedSetAddAsync(_context.NameOf(IndexKeys.KeyStatus<NewsAll>(post.Status)), post.Id, (post.DistributionDate ?? DateTime.Now).ConvertToTimestamp10());
                                });

                                break;
                            case (int)CardType.ShareNews:
                                var shareLink = await _context.HashGetExAsync<ShareLink>(newsId);

                                shareLink.DistributionDate = distributionDate;

                                returnValue = await _context.MultiAsync((trans) =>
                                {
                                    trans.HashSetAsync(_context.NameOf<ShareLink>(), shareLink.Id, Json.Stringify(shareLink));
                                    trans.HashSetAsync(_context.NameOf(news.GetType().Name), news.Id, Json.Stringify(news));

                                    trans.SortedSetAddAsync(_context.NameOf(IndexKeys.KeyStatus<NewsAll>(shareLink.Status)), shareLink.Id, (shareLink.DistributionDate ?? DateTime.Now).ConvertToTimestamp10());
                                });

                                break;
                            case (int)CardType.Video:
                                var video = await _context.HashGetExAsync<Video>(newsId);

                                video.DistributionDate = distributionDate;

                                returnValue = await _context.MultiAsync((trans) =>
                                {
                                    trans.HashSetAsync(_context.NameOf<Video>(), video.Id, Json.Stringify(video));
                                    trans.HashSetAsync(_context.NameOf(news.GetType().Name), news.Id, Json.Stringify(news));

                                    trans.SortedSetAddAsync(_context.NameOf(IndexKeys.KeyStatus<NewsAll>(video.Status)), video.Id, (video.DistributionDate ?? DateTime.Now).ConvertToTimestamp10());
                                });

                                break;
                            case (int)CardType.Gallery1:
                                var gallery1 = await _context.HashGetExAsync<Entities.Gallery.Gallery>(newsId);

                                gallery1.DistributionDate = distributionDate;

                                returnValue = await _context.MultiAsync((trans) =>
                                {
                                    trans.HashSetAsync(_context.NameOf<Entities.Gallery.Gallery>(), gallery1.Id, Json.Stringify(gallery1));
                                    trans.HashSetAsync(_context.NameOf(news.GetType().Name), news.Id, Json.Stringify(news));

                                    trans.SortedSetAddAsync(_context.NameOf(IndexKeys.KeyStatus<NewsAll>(gallery1.Status)), gallery1.Id, (gallery1.DistributionDate ?? DateTime.Now).ConvertToTimestamp10());
                                });

                                break;
                            case (int)CardType.Gallery2:
                                var gallery2 = await _context.HashGetExAsync<Entities.Gallery.Gallery>(newsId);

                                gallery2.DistributionDate = distributionDate;

                                returnValue = await _context.MultiAsync((trans) =>
                                {
                                    trans.HashSetAsync(_context.NameOf<Entities.Gallery.Gallery>(), gallery2.Id, Json.Stringify(gallery2));
                                    trans.HashSetAsync(_context.NameOf(news.GetType().Name), news.Id, Json.Stringify(news));

                                    trans.SortedSetAddAsync(_context.NameOf(IndexKeys.KeyStatus<NewsAll>(gallery2.Status)), gallery2.Id, (gallery2.DistributionDate ?? DateTime.Now).ConvertToTimestamp10());
                                });

                                break;
                        }

                        action((int?)news.CardType);
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
            }
            return returnValue;
        }
    }
}
