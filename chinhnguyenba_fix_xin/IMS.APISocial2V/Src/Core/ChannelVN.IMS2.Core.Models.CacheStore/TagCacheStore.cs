﻿using ChannelVN.IMS2.Core.Entities;
using ChannelVN.IMS2.Foundation.Common;
using ChannelVN.IMS2.Foundation.Data.Providers;
using ChannelVN.IMS2.Foundation.Logging;
using StackExchange.Redis;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ChannelVN.IMS2.Core.Models.CacheStore
{
    public class TagCacheStore : CmsMainCacheStore
    {
        public Task<bool> AddAsync(Tag data)
        {
            var returnValue = Task.FromResult(false);
            try
            {
                using (var _context = GetContext())
                {
                    returnValue = _context.MultiAsync(async (transac) =>
                    {
                        await transac.HashSetAsync(_context.NameOf(data.GetType().Name), data.Id, Json.Stringify(data));
                        await transac.SortedSetAddAsync(_context.NameOf(IndexKeys.KeyCreatedDate<Tag>()), data.Id, Utility.ConvertToTimestamp10(data.CreatedDate ?? DateTime.Now));
                    });
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
            }
            return returnValue;
        }

        public Task<bool> UpdateAsync(Tag data)
        {
            var returnValue = Task.FromResult(false);
            try
            {
                using (var _context = GetContext())
                {
                    returnValue = _context.MultiAsync((transac) =>
                    {
                        transac.HashSetAsync(_context.NameOf<Tag>(), data.Id, Json.Stringify(data));
                    });
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
            }
            return returnValue;
        }
        public Task<Tag> GetByIdAsync(long id)
        {
            var returnValue = Task.FromResult(default(Tag));
            try
            {
                using (var _context = GetContext())
                {
                    returnValue = _context.HashGetExAsync<Tag>(id, CommandFlags.PreferSlave);
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
            }
            return returnValue;
        }

        public Task<List<Tag>> GetTagByListIdAsync(List<string> ids)
        {
            var returnValue = Task.FromResult(default(List<Tag>));
            try
            {
                using (var _context = GetContext())
                {
                    returnValue = _context.GetsFromHashExAsync<Tag>(ids, CommandFlags.PreferSlave);
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
            }
            return returnValue;
        }
        

        public Task<List<Tag>> GetListByIdsAsync(List<string> ids)
        {
            var returnValue = Task.FromResult(default(List<Tag>));
            try
            {
                using (var _context = GetContext())
                {
                    returnValue = _context.GetsFromHashExAsync<Tag>(ids, CommandFlags.PreferSlave);
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
            }
            return returnValue;
        }
    }
}
