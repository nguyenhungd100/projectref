﻿using ChannelVN.IMS2.Foundation.Common.Configuration;
using ChannelVN.IMS2.Foundation.Logging;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace ChannelVN.IMS2.Core.Models.CacheStore
{
    public class DeleteTempCacheStore: CmsMainCacheStore
    {
        public Task<bool> SetExpireAsync(string tempKey)
        {
            var returnvalue = Task.FromResult(false);
            try
            {
                using(var _context = GetContext())
                {
                    returnvalue = _context.MultiAsync((db) =>
                    {
                        db.KeyExpireAsync(tempKey, TimeSpan.FromSeconds(AppSettings.Current.ChannelConfiguration.ExpireIndexTemp));
                    });
                }
            }
            catch(Exception ex)
            {
                Logger.Error(ex, ex.Message);
            }
            return returnvalue;
        }
    }
}
