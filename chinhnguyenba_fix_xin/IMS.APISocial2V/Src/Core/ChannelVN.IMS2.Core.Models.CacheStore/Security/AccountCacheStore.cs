﻿using ChannelVN.IMS2.Core.Entities;
using ChannelVN.IMS2.Core.Entities.Security;
using ChannelVN.IMS2.Foundation.Common;
using ChannelVN.IMS2.Foundation.Logging;
using StackExchange.Redis;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using static ChannelVN.IMS2.Core.Entities.ErrorCode.ErrorMapping;

namespace ChannelVN.IMS2.Core.Models.CacheStore.Security
{
    public class AccountCacheStore : CmsMainCacheStore
    {
        public async Task<Dictionary<long, ObjectTmp>> GetAllAccountAsync()
        {
            var returnValue = new Dictionary<long, ObjectTmp>();
            try
            {
                using (var _context = GetContext())
                {
                    returnValue = await _context.ExecAsync(async (db) =>
                    {
                        var result = new Dictionary<long, ObjectTmp>();
                        var lstAcc = (await db.HashGetAllAsync(_context.NameOf<Account>())).Select(p=>Json.Parse<Account>(p.Value));
                        lstAcc = lstAcc?.Where(p => p.Type == (int)AccountType.Personal && p.Status == (int)AccountStatus.Actived);
                        foreach(var acc in lstAcc)
                        {
                            var listId = await db.SortedSetRangeByRankAsync(_context.NameOf(IndexKeys.KeyOfficerOfAccount<AccountMember>(acc.Id)),0,-1,Order.Descending);
                            var listPage = (await db.HashGetAsync(_context.NameOf<Account>(), listId?.Select(p => (RedisValue)p).ToArray())).Select(p=>Json.Parse<Account>(p));
                            if(listPage!=null && listPage.Count()>0) result.Add(acc.Id, new ObjectTmp { Account = acc, Pages = listPage?.ToList() });
                        }
                        return result;
                    });
                }
            }
            catch( Exception ex)
            {
                Logger.Error(ex, ex.Message);
            }
            return returnValue;
        }

        public async Task<Dictionary<long, ObjectTmp>> GetAllPageAsync()
        {
            var returnValue = new Dictionary<long, ObjectTmp>();
            try
            {
                using (var _context = GetContext())
                {
                    returnValue = await _context.ExecAsync(async (db) =>
                    {
                        var result = new Dictionary<long, ObjectTmp>();
                        var lstAcc = (await db.HashGetAllAsync(_context.NameOf<Account>())).Select(p => Json.Parse<Account>(p.Value));
                        lstAcc = lstAcc?.Where(p => p.Type == (int)AccountType.Official && p.Status == (int)AccountStatus.Actived);
                        foreach (var acc in lstAcc)
                        {
                            var profile = Json.Parse<UserProfile>(await db.HashGetAsync(_context.NameOf<UserProfile>(),acc.Id));

                            if (acc != null) result.Add(acc.Id, new ObjectTmp { Account = acc, Profile = profile });
                        }
                        return result;
                    });
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
            }
            return returnValue;
        }

        public Task<AccountSearch> GetUserByNameAsync(string username)
        {
            var returnValue = Task.FromResult(default(AccountSearch));
            try
            {
                if (!string.IsNullOrEmpty(username))
                {
                    using (var _context = GetContext())
                    {
                        string findAccountByUsernameScript = "local userId = redis.call('zscore', '" + _context.NameOf("usermeta") + "', '{0}'); return redis.call('hget', '" + _context.NameOf(typeof(Account).Name) + "', userId);";
                        returnValue = _context.ExecAsync(async (db) =>
                        {
                            var result = default(AccountSearch);
                            try
                            {
                                var script = string.Format(findAccountByUsernameScript, username ?? "");
                                var value = await db.ScriptEvaluateAsync(script, null, null, CommandFlags.PreferSlave);
                                if (value != null && !value.IsNull)
                                    result = Json.Parse<AccountSearch>(value.ToString());
                            }
                            catch (Exception ex)
                            {
                                Logger.Error(ex, ex.Message);
                            }
                            return result;
                        });
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
            }
            return returnValue;
        }

        public async Task<List<AccountMember>> ListMembersAsync(long officerId)
        {
            var returnValue = default(List<AccountMember>);
            try
            {
                using (var _context = GetContext())
                {
                    var listId = (await _context.SortedSetRangeByRankAsync(IndexKeys.KeyAccountMemberInOfficer<AccountMember>(officerId), 0, -1, Order.Descending, CommandFlags.PreferSlave))?.Select(p => (long)p)?.ToList();

                    returnValue = (await _context.GetsFromHashExAsync<AccountMember>(listId?.Select(p => officerId + IndexKeys.SeparateChar + p.ToString()).ToList(), CommandFlags.PreferSlave)).ToList();
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
            }
            return returnValue;
        }

        public Task<bool> AddAsync(Account data)
        {
            var returnValue = Task.FromResult(false);
            try
            {
                using (var _context = GetContext())
                {
                    returnValue = _context.MultiAsync((transac) =>
                    {
                        transac.SortedSetRemoveRangeByScoreAsync(_context.NameOf("usermeta"), data.Id, data.Id);
                        if (!string.IsNullOrEmpty(data.UserName))
                        {
                            transac.SortedSetAddAsync(_context.NameOf("usermeta"), data.UserName.ToLower(), data.Id);
                        }

                        transac.HashSetAsync(_context.NameOf(data.GetType().Name), data.Id, Json.Stringify(data));
                        transac.SortedSetAddAsync(_context.NameOf(IndexKeys.KeyCreatedDate<Account>()), data.Id, (data.CreatedDate ?? DateTime.Now).ConvertToTimestamp10());

                        if (!string.IsNullOrEmpty(data.CreatedBy))
                        {
                            transac.SortedSetAddAsync(_context.NameOf(IndexKeys.KeyCreatedBy<Account>(data.CreatedBy)), data.Id, (data.CreatedDate ?? DateTime.Now).ConvertToTimestamp10());
                        }

                        if(data.Type != (byte?)AccountType.Official)
                        {
                            if (!string.IsNullOrEmpty(data.Mobile))
                            {
                                //transac.SetAddAsync(_context.NameOf(IndexKeys.KeyPhoneNumberInSytem<Account>()), data.Mobile);
                                transac.SortedSetAddAsync(_context.NameOf(IndexKeys.KeyPhoneNumberInSytem<Account>()), data.Mobile, data.Id);
                            }

                            if (!string.IsNullOrEmpty(data.Email))
                            {
                                //transac.SetAddAsync(_context.NameOf(IndexKeys.KeyPhoneNumberInSytem<Account>()), data.Mobile);
                                transac.SortedSetAddAsync(_context.NameOf(IndexKeys.KeyEmailInSytem<Account>()), data.Email, data.Id);
                            }
                        }
                    });
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
            }
            return returnValue;
        }
        public Task<bool> AddOfficialAsync(Account officerAccount, UserProfile officerProfile, AccountMember accountMember)
        {
            var returnValue = Task.FromResult(false);
            try
            {
                using (var _context = GetContext())
                {
                    returnValue = _context.MultiAsync((transac) =>
                    {
                        transac.HashSetAsync(_context.NameOf(officerAccount.GetType().Name), officerAccount.Id, Json.Stringify(officerAccount));
                        transac.HashSetAsync(_context.NameOf(officerProfile.GetType().Name), officerProfile.Id, Json.Stringify(officerProfile));

                        //add member owner

                        transac.HashSetAsync(_context.NameOf(typeof(AccountMember).Name), officerAccount.Id + IndexKeys.SeparateChar + accountMember.MemberId, Json.Stringify(accountMember));

                        transac.SortedSetAddAsync(_context.NameOf(IndexKeys.KeyOfficerOfAccount<AccountMember>(accountMember.MemberId)), accountMember.OfficerId, Utility.ConvertToTimestamp10(accountMember.JoinedDate ?? DateTime.Now));

                        transac.SortedSetAddAsync(_context.NameOf(IndexKeys.KeyAccountMemberInOfficer<AccountMember>(accountMember.OfficerId)), accountMember.MemberId, Utility.ConvertToTimestamp10(accountMember.JoinedDate ?? DateTime.Now));

                        transac.SortedSetAddAsync(_context.NameOf(IndexKeys.KeyOwnerOfficer(accountMember.MemberId)), accountMember.OfficerId, Utility.ConvertToTimestamp10(accountMember.JoinedDate ?? DateTime.Now));
                    });
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
            }
            return returnValue;
        }
        public Task<bool> SaveProfileAsync(UserProfile data, Account userCurrent)
        {
            var returnValue = Task.FromResult(false);
            try
            {
                using (var _context = GetContext())
                {
                    returnValue = _context.MultiAsync((transac) =>
                    {
                        transac.HashSetAsync(_context.NameOf(data.GetType().Name), data.Id, Json.Stringify(data));
                        if (userCurrent != null)
                        {
                            transac.HashSetAsync(_context.NameOf(userCurrent.GetType().Name), userCurrent.Id, Json.Stringify(userCurrent));
                        }
                    });
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
            }
            return returnValue;
        }
        public async Task<bool> UpdateAsync(Account data, UserProfile profile, string mobileOld, string emailOld)
        {
            var returnValue = false;
            try
            {
                using (var _context = GetContext())
                {
                    var profileDb = await _context.HashGetExAsync<UserProfile>(data.Id);
                    if (profileDb == null)
                    {
                        profileDb = new UserProfile()
                        {
                            Id = data.Id,
                            Gender = profile?.Gender,
                            BirthDay = profile?.BirthDay,
                            PenName = profile?.PenName,
                            Title = profile?.Title,
                            WorkPlace = profile?.WorkPlace,
                            School = profile?.School,
                            Address = profile?.Address,
                            UrlNetwork = profile?.UrlNetwork,
                            UrlSocial = profile?.UrlSocial,
                            ProfileUrl = profile?.ProfileUrl,
                            ModifiedDate = DateTime.Now
                        };
                    }
                    else
                    {
                        profileDb.Gender = profile?.Gender;
                        profileDb.BirthDay = profile?.BirthDay;
                        profileDb.PenName = profile?.PenName;
                        profileDb.Title = profile?.Title;
                        profileDb.WorkPlace = profile?.WorkPlace;
                        profileDb.School = profile?.School;
                        profileDb.Address = profile?.Address;
                        profileDb.UrlNetwork = profile?.UrlNetwork;
                        profileDb.UrlSocial = profile?.UrlSocial;
                    }
                    returnValue = await _context.MultiAsync((transac) =>
                    {
                        transac.SortedSetRemoveRangeByScoreAsync(_context.NameOf("usermeta"), data.Id, data.Id);

                        if (!string.IsNullOrEmpty(data.UserName))
                        {
                            transac.SortedSetAddAsync(_context.NameOf("usermeta"), data.UserName.ToLower(), data.Id);
                        }

                        transac.HashSetAsync(_context.NameOf(data.GetType().Name), data.Id, Json.Stringify(data));
                        transac.HashSetAsync(_context.NameOf(profileDb.GetType().Name), profileDb.Id, Json.Stringify(profileDb));

                        if(data.Type != (byte?)AccountType.Official)
                        {
                            if (!string.IsNullOrEmpty(data.Email))
                            {
                                if (!string.IsNullOrEmpty(emailOld))
                                {
                                    transac.SortedSetRemoveAsync(_context.NameOf(IndexKeys.KeyEmailInSytem<Account>()), emailOld);
                                }

                                transac.SortedSetAddAsync(_context.NameOf(IndexKeys.KeyEmailInSytem<Account>()), data.Email, data.Id);
                            }

                            if (!string.IsNullOrEmpty(data.Mobile))
                            {
                                if (!string.IsNullOrEmpty(mobileOld))
                                {
                                    transac.SortedSetRemoveAsync(_context.NameOf(IndexKeys.KeyPhoneNumberInSytem<Account>()), mobileOld);
                                }

                                transac.SortedSetAddAsync(_context.NameOf(IndexKeys.KeyPhoneNumberInSytem<Account>()), data.Mobile, data.Id);
                            }
                        }
                    });
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
            }
            return returnValue;
        }
        public async Task<bool> ConfigPageAsync(long officerId, int mode, long accountId)
        {
            var returnValue = false;
            try
            {
                using (var _context = GetContext())
                {
                    var officer = await _context.HashGetExAsync<Account>(officerId);
                    if (officer != null)
                    {
                        officer.Mode = mode;
                        returnValue = await _context.HashSetExAsync<Account>(officer.Id, officer);
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
            }
            return returnValue;
        }

        public async Task<bool> UpdateAsync(Account data)
        {
            var returnValue = false;
            try
            {
                using (var _context = GetContext())
                {
                    returnValue = await _context.MultiAsync((transac) =>
                    {
                        transac.SortedSetRemoveRangeByScoreAsync(_context.NameOf("usermeta"), data.Id, data.Id);
                        if (!string.IsNullOrEmpty(data.UserName))
                        {
                            transac.SortedSetAddAsync(_context.NameOf("usermeta"), data.UserName.ToLower(), data.Id);
                        }
                        transac.HashSetAsync(_context.NameOf(data.GetType().Name), data.Id, Json.Stringify(data));

                        if (!string.IsNullOrEmpty(data.Email))
                        {
                            transac.SortedSetAddAsync(_context.NameOf(IndexKeys.KeyEmailInSytem<Account>()), data.Email, data.Id);
                        }

                        if (!string.IsNullOrEmpty(data.Mobile))
                        {
                            transac.SortedSetAddAsync(_context.NameOf(IndexKeys.KeyPhoneNumberInSytem<Account>()), data.Mobile, data.Id);
                        }

                    });
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
            }
            return returnValue;
        }
        public Task<List<Account>> GetListByIdsAsync(List<string> ids)
        {
            var returnValue = Task.FromResult(default(List<Account>));
            try
            {
                using (var _context = GetContext())
                {
                    returnValue = _context.GetsFromHashExAsync<Account>(ids, CommandFlags.PreferSlave);
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
            }
            return returnValue;
        }

        public async Task<List<object>> GetListSimpleByIdsAsync(List<string> ids)
        {
            var returnValue = default(List<object>);
            try
            {
                using (var _context = GetContext())
                {
                    //var data = _context.GetsFromHashExAsync<Account>(ids, CommandFlags.PreferSlave);

                    returnValue = await _context.ExecAsync(async(db) =>
                    {
                        var result = new List<object>();
                        var data = (await db.HashGetAsync(_context.NameOf<Account>(), ids?.Select(p=>(RedisValue)p).ToArray()))?.Select(p=>Json.Parse<Account>(p));
                        if (data != null)
                        {
                            foreach(var item in data)
                            {
                                try
                                {
                                    var owner = Json.Parse<Account>(await db.HashGetAsync(_context.NameOf<Account>(), long.Parse(item.CreatedBy)));
                                    if (owner != null)
                                    {
                                        result.Add(new
                                        {
                                            Id = item?.Id.ToString(),
                                            item?.Avatar,
                                            item?.FullName,
                                            item?.CreatedBy,
                                            item.Status,
                                            item.LabelMode,
                                            PhoneOwner = owner.Mobile,
                                            OwnerStatus = owner.Status,
                                            item.Class
                                        });
                                    }
                                }
                                catch(Exception ex)
                                {
                                    Logger.Sensitive(ex, ex.Message);
                                }
                            }
                        }
                        return result;
                    });
                    //GetsFromHashExAsync<Account>(ids, CommandFlags.PreferSlave);
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
            }
            return returnValue;
        }

        public Task<bool> AcceptOrRejectAsync(Account data)
        {
            var returnValue = Task.FromResult(false);
            try
            {
                using (var _context = GetContext())
                {
                    returnValue = _context.MultiAsync((transac) =>
                    {
                        transac.HashSetAsync(_context.NameOf<Account>(), data.Id, Json.Stringify(data));
                    });
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
            }
            return returnValue;
        }

        public Task<Account> GetByIdAsync(long id)
        {
            var returnValue = Task.FromResult(default(Account));
            try
            {
                using (var _context = GetContext())
                {
                    returnValue = _context.HashGetExAsync<Account>(id, CommandFlags.PreferSlave);
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
            }
            return returnValue;
        }

        public Task<AccountSimple> GetSimpleAccountAsync(long id)
        {
            var returnValue = Task.FromResult(default(AccountSimple));
            try
            {
                using (var _context = GetContext())
                {
                    returnValue = _context.ExecAsync(async (db) =>
                    {
                        var account = Json.Parse<Account>(await db.HashGetAsync(_context.NameOf<Account>(), id, CommandFlags.PreferSlave));
                        if (account != null && account.Id > 0)
                        {
                            var profile = Json.Parse<UserProfile>(await db.HashGetAsync(_context.NameOf<UserProfile>(), id, CommandFlags.PreferSlave));

                            var listOfficial = await db.SortedSetRangeByRankAsync(_context.NameOf(IndexKeys.KeyOfficerOfAccount<AccountMember>(id)), 0, -1, Order.Descending, CommandFlags.PreferSlave);
                            var listAccountMember = (await db.HashGetAsync(_context.NameOf<AccountMember>(), listOfficial?.Select(p => (RedisValue)(p + IndexKeys.SeparateChar + id))?.ToArray(), CommandFlags.PreferSlave))
                            ?.Select(p => Json.Parse<AccountMember>(p));

                            var accountSimple = new AccountSimple()
                            {
                                Id = account.EncryptId,
                                Avatar = account.Avatar,
                                Type = account.Type,
                                Banner = account.Banner,
                                Class = account.Class,
                                Email = account.Email,
                                FullName = account.FullName,
                                Mobile = account.Mobile,
                                Status = account.Status,
                                UserName = account.UserName,
                                VerifiedBy = account.VerifiedBy,
                                VerifiedDate = account.VerifiedDate,
                                Description = account.Description,
                                BirthDay = profile?.BirthDay,
                                Gender = profile?.Gender,
                                PenName = profile?.PenName,
                                Address = profile?.Address,
                                Title = profile?.Title,
                                WorkPlace = profile?.WorkPlace,
                                School = profile?.School,
                                UrlNetwork = profile?.UrlNetwork,
                                UrlSocial = profile?.UrlSocial,
                                CreatedBy = account.CreatedBy,
                                Mode = account.Mode,
                                Permissions = listAccountMember?.Where(p => p != null).ToDictionary(key => key.OfficerId.ToString(), value => value.Permissions?.ToList()),
                                CommentMode = account.CommentMode,
                                LabelMode = account.LabelMode,
                                RelatedId = account.RelatedId,
                                RelatedType = account.RelatedType,
                                ProfileUrl = profile?.ProfileUrl,
                                DelegatorId = account.DelegatorId?.ToString(),
                                OtpSecretKey = account.OtpSecretKey
                            };

                            return accountSimple;
                        }
                        return null;
                    });
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
            }
            return returnValue;
        }

        public Task<AccountSearch> GetAccountSearchByIdAsync(long id)
        {
            var returnValue = Task.FromResult(default(AccountSearch));
            try
            {
                using (var _context = GetContext())
                {
                    returnValue = _context.ExecAsync(async (db) =>
                    {
                        var result = default(AccountSearch);
                        try
                        {
                            var acc = Json.Parse<Account>(await db.HashGetAsync(_context.NameOf<Account>(), id, CommandFlags.PreferSlave));
                            if (acc != null)
                            {
                                var listMemberId = await db.SortedSetRangeByRankAsync(_context.NameOf(IndexKeys.KeyAccountMemberInOfficer<AccountMember>(id)), 0, -1, Order.Descending, CommandFlags.PreferSlave);
                                result = Mapper<AccountSearch>.Map(acc, new AccountSearch());
                                if (listMemberId != null && listMemberId.Count() > 0)
                                {
                                    result.AccountMember = (await db.HashGetAsync(_context.NameOf<AccountMember>(), listMemberId?.Select(p => (RedisValue)(id + IndexKeys.SeparateChar + p.ToString())).ToArray()))?.Select(p => Json.Parse<AccountMember>(p)).ToArray();
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            Logger.Error(ex, ex.Message);
                        }

                        return result;
                    });
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
            }
            return returnValue;
        }
        public Task<bool> UpdateStatusAsync(AccountSearch account)
        {
            var returnValue = Task.FromResult(false);
            try
            {
                using (var _context = GetContext())
                {
                    returnValue = _context.MultiAsync((trans) =>
                    {
                        trans.HashSetAsync(_context.NameOf<Account>(), account.Id, Json.Stringify(Mapper<Account>.Map(account, new Account())));
                    });
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
            }
            return returnValue;
        }

        public Task<UserProfile> GetProfileAsync(long id)
        {
            var returnValue = Task.FromResult(default(UserProfile));
            try
            {
                using (var _context = GetContext())
                {
                    returnValue = _context.HashGetExAsync<UserProfile>(id, CommandFlags.PreferSlave);
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
            }
            return returnValue;
        }

        public async Task<List<UserProfile>> GetListProfileAsync(List<string> listId)
        {
            var returnValue = default(List<UserProfile>);
            try
            {
                using (var _context = GetContext())
                {
                    returnValue = await _context.GetsFromHashExAsync<UserProfile>(typeof(UserProfile).Name, listId?.Select(p=>(RedisValue)p)?.ToList(), CommandFlags.PreferSlave);
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
            }
            return returnValue;
        }

        public Task<AccountMember> GetAccountMemberByIdAsync(string id)
        {
            var returnValue = Task.FromResult(default(AccountMember));
            try
            {
                using (var _context = GetContext())
                {
                    returnValue = _context.HashGetExAsync<AccountMember>(id, CommandFlags.PreferSlave);
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
            }
            return returnValue;
        }
        public Task<double?> GetByUsernameAsync(string username)
        {
            var returnValue = Task.FromResult(default(double?));
            try
            {
                using (var _context = GetContext())
                {
                    returnValue = _context.SortedSetScoreAsync("usermeta", username, CommandFlags.PreferSlave);
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
            }
            return returnValue;
        }

        public Task<bool> UpdateAccountAsync(Account data)
        {
            var returnValue = Task.FromResult(false);
            try
            {
                using (var _context = GetContext())
                {
                    returnValue = _context.MultiAsync((transac) =>
                    {
                        transac.HashSetAsync(_context.NameOf<Account>(), data.Id, Json.Stringify(data));

                        if(data.Type != (byte?)AccountType.Official)
                        {
                            if (!string.IsNullOrEmpty(data.Email))
                            {
                                transac.SortedSetAddAsync(_context.NameOf(IndexKeys.KeyEmailInSytem<Account>()), data.Email, data.Id);
                            }

                            if (!string.IsNullOrEmpty(data.Mobile))
                            {
                                transac.SortedSetAddAsync(_context.NameOf(IndexKeys.KeyPhoneNumberInSytem<Account>()), data.Mobile, data.Id);
                            }
                        }
                    });
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
            }
            return returnValue;
        }

        public Task<bool> UpdateAccountOpentIdAsync(Account data)
        {
            var returnValue = Task.FromResult(false);
            try
            {
                using (var _context = GetContext())
                {
                    returnValue = _context.MultiAsync((transac) =>
                    {
                        transac.HashSetAsync(_context.NameOf<Account>(), data.Id, Json.Stringify(data));                        
                    });
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
            }
            return returnValue;
        }

        public Task<bool> AddAccountMemberAsync(AccountMember accountMember)
        {
            var returnValue = Task.FromResult(false);
            try
            {
                using (var _context = GetContext())
                {
                    returnValue = _context.MultiAsync((transac) =>
                    {
                        transac.HashSetAsync(_context.NameOf(accountMember.GetType().Name), accountMember.OfficerId + IndexKeys.SeparateChar + accountMember.MemberId, Json.Stringify(accountMember));
                        transac.SortedSetAddAsync(_context.NameOf(IndexKeys.KeyOfficerOfAccount<AccountMember>(accountMember.MemberId)), accountMember.OfficerId, Utility.ConvertToTimestamp10(accountMember.JoinedDate ?? DateTime.Now));
                        transac.SortedSetAddAsync(_context.NameOf(IndexKeys.KeyAccountMemberInOfficer<AccountMember>(accountMember.OfficerId)), accountMember.MemberId, Utility.ConvertToTimestamp10(accountMember.JoinedDate ?? DateTime.Now));
                    });
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
            }
            return returnValue;
        }

        public Task<bool> Update_AccountMemberAsync(AccountMember accountMember)
        {
            var returnValue = Task.FromResult(false);
            try
            {
                using (var _context = GetContext())
                {
                    returnValue = _context.MultiAsync((transac) =>
                    {
                        transac.HashSetAsync(_context.NameOf(accountMember.GetType().Name), accountMember.OfficerId + IndexKeys.SeparateChar + accountMember.MemberId, Json.Stringify(accountMember));
                    });
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
            }
            return returnValue;
        }

        public Task<bool> RemoveMemberAsync(long officerId, long memberId)
        {
            var returnValue = Task.FromResult(false);
            try
            {
                using (var _context = GetContext())
                {
                    returnValue = _context.MultiAsync((transac) =>
                    {
                        transac.HashDeleteAsync(_context.NameOf(typeof(AccountMember).Name), officerId + IndexKeys.SeparateChar + memberId);
                        transac.SortedSetRemoveAsync(_context.NameOf(IndexKeys.KeyOfficerOfAccount<AccountMember>(memberId)), officerId);
                        transac.SortedSetRemoveAsync(_context.NameOf(IndexKeys.KeyAccountMemberInOfficer<AccountMember>(officerId)), memberId);
                    });
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
            }
            return returnValue;
        }

        public async Task<ErrorCodes> RemoveMemberOnAllPageAsync(long accountId, long memberId, Action<IEnumerable<long>> action)
        {
            var returnValue = ErrorCodes.UnknowError;
            try
            {
                using (var _context = GetContext())
                {
                    var listValid = await _context.ExecAsync(async (db) =>
                    {
                        var listOfficerId = await db.SortedSetRangeByRankAsync(_context.NameOf(IndexKeys.KeyOfficerOfAccount<AccountMember>(accountId)), 0, -1, Order.Descending, CommandFlags.PreferSlave);


                        var tmp = (await db.HashGetAsync(_context.NameOf<AccountMember>(), listOfficerId?.Select(p => (RedisValue)(p + IndexKeys.SeparateChar + accountId))?.ToArray(), CommandFlags.PreferSlave))?.Select(p => Json.Parse<AccountMember>(p))?.Where(p => p.Permissions != null && p.Permissions.Contains(MemberPermission.ManagerMember))?.Select(p => new { p.OfficerId, p.Role, p.Level });

                        var listMemberInValid = (await db.HashGetAsync(_context.NameOf<AccountMember>(), tmp?.Select(p => (RedisValue)(p.OfficerId + IndexKeys.SeparateChar + memberId)).ToArray(), CommandFlags.PreferSlave))?.Select(p => Json.Parse<AccountMember>(p))?.Where(p => p.Role == AccountMemberRole.Owner || tmp.Where(t => t.Level >= p.Level)?.Count() > 0);

                        if (listMemberInValid?.Count() > 0)
                        {
                            return null;
                        }
                        return tmp;
                    });

                    if (listValid != null && listValid.Count() > 0)
                    {
                        var removeResult = await _context.MultiAsync((transac) =>
                        {
                            transac.HashDeleteAsync(_context.NameOf(typeof(AccountMember).Name), listValid?.Select(p => (RedisValue)(p + IndexKeys.SeparateChar + memberId)).ToArray());
                            transac.SortedSetRemoveAsync(_context.NameOf(IndexKeys.KeyOfficerOfAccount<AccountMember>(memberId)), listValid?.Select(p => (RedisValue)p.OfficerId).ToArray());
                            foreach (var member in listValid)
                            {
                                transac.SortedSetRemoveAsync(_context.NameOf(IndexKeys.KeyAccountMemberInOfficer<AccountMember>(member.OfficerId)), memberId);
                            }
                        });

                        if (removeResult)
                        {
                            action(listValid.Select(p => p.OfficerId));
                            returnValue = ErrorCodes.Success;
                        }
                    }
                    else
                    {
                        returnValue = ErrorCodes.RemoveMemberPermissionInvalid;
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
                returnValue = ErrorCodes.Exception;
            }
            return returnValue;
        }

        public async Task<bool> LockOrUnlockMemberAsync(AccountMember data)
        {
            var returnValue = false;
            try
            {
                using (var _context = GetContext())
                {
                    returnValue = await _context.MultiAsync((transac) =>
                    {
                        transac.HashSetAsync(_context.NameOf(typeof(AccountMember).Name), data.OfficerId + IndexKeys.SeparateChar + data.MemberId, Json.Stringify(data));
                    });
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
            }
            return returnValue;
        }

        public async Task<ErrorCodes> LockOrUnlockMemberOnAllPageAsync(long accountId, long memberId, int act, Action<IEnumerable<AccountMember>> action)
        {
            var returnValue = ErrorCodes.UnknowError;
            try
            {
                using (var _context = GetContext())
                {
                    var (listValid, listMemberLock) = await _context.ExecAsync(async (db) =>
                    {
                        var listOfficerId = await db.SortedSetRangeByRankAsync(_context.NameOf(IndexKeys.KeyOfficerOfAccount<AccountMember>(accountId)), 0, -1, Order.Descending, CommandFlags.PreferSlave);


                        var tmp = (await db.HashGetAsync(_context.NameOf<AccountMember>(), listOfficerId?.Select(p => (RedisValue)(p + IndexKeys.SeparateChar + accountId))?.ToArray(), CommandFlags.PreferSlave))?.Select(p => Json.Parse<AccountMember>(p))?.Where(p => p.Permissions != null && p.Permissions.Contains(MemberPermission.ManagerMember))?.Select(p => new { p.OfficerId, p.Role, p.Level });

                        var listMemberInValid = (await db.HashGetAsync(_context.NameOf<AccountMember>(), tmp?.Select(p => (RedisValue)(p.OfficerId + IndexKeys.SeparateChar + memberId)).ToArray(), CommandFlags.PreferSlave))?.Select(p => Json.Parse<AccountMember>(p))?.Where(p => p.Role == AccountMemberRole.Owner || tmp.Where(t=>t.Level >= p.Level)?.Count() > 0);

                        if (listMemberInValid?.Count() > 0)
                        {
                            return (null, null);
                        }

                        var listMemberLockTmp = (await db.HashGetAsync(_context.NameOf<AccountMember>(), tmp?.Select(p => (RedisValue)(p.OfficerId + IndexKeys.SeparateChar + memberId)).ToArray(), CommandFlags.PreferSlave))?.Select(p => Json.Parse<AccountMember>(p));

                        return (tmp, listMemberLockTmp);
                    });

                    if (listValid != null && listValid.Count() > 0)
                    {
                        listMemberLock?.ToList().ForEach(p =>
                        {
                            if (act == 1)
                                p.LockedDate = DateTime.Now;
                            else p.LockedDate = null;
                        });

                        var lockResult = await _context.MultiAsync((transac) =>
                        {
                            transac.HashSetAsync(_context.NameOf(typeof(AccountMember).Name), listMemberLock?.Select(p => new HashEntry(p.OfficerId + IndexKeys.SeparateChar + p.MemberId, Json.Stringify(p)))?.ToArray());
                        });

                        if (lockResult)
                        {
                            action(listMemberLock);
                            returnValue = ErrorCodes.Success;
                        }
                    }
                    else
                    {
                        returnValue = ErrorCodes.RemoveMemberPermissionInvalid;
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
                returnValue = ErrorCodes.Exception;
            }
            return returnValue;
        }

        public async Task<bool> CheckOwnerExist(long userId)
        {
            var returnValue = false;
            try
            {
                using (var _context = GetContext())
                {
                    var listOwner = await _context.SortedSetRangeByRankAsync(IndexKeys.KeyOwnerOfficer(userId), 0, -1, Order.Descending, CommandFlags.PreferSlave);
                    returnValue = listOwner != null && listOwner.Count() > 0 ? true : false;
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
            }
            return returnValue;
        }
        public async Task<long[]> ListOwnerAsync(long userId)
        {
            var returnValue = default(long[]);
            try
            {
                using (var _context = GetContext())
                {
                    var listOwner = await _context.SortedSetRangeByRankAsync(IndexKeys.KeyOwnerOfficer(userId), 0, -1, Order.Descending, CommandFlags.PreferSlave);
                    returnValue = listOwner?.Select(p => long.Parse(p)).ToArray();
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
            }
            return returnValue;
        }

        public async Task<bool> CheckAccountMember(long officerId, long memberId)
        {
            var returnValue = false;
            try
            {
                using (var _context = GetContext())
                {
                    if (officerId == memberId)
                    {
                        returnValue = true;
                    }
                    else
                    {
                        var listMemberId = await _context.SortedSetRangeByRankAsync(IndexKeys.KeyAccountMemberInOfficer<AccountMember>(officerId), 0, -1, Order.Descending, CommandFlags.PreferSlave);
                        returnValue = listMemberId?.Where(p => p.ToString().Equals(memberId.ToString()))?.Count() > 0;
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
            }
            return returnValue;
        }


        public Task<PagingDataResult<AccountMemberInfo>> GetMembersAsync(long? officerId, long accountId, AccountMemberRole? role, string keyWord, int pageIndex, int pageSize)
        {
            var returnValue = Task.FromResult(default(PagingDataResult<AccountMemberInfo>));
            try
            {
                using (var _context = GetContext())
                {
                    returnValue = _context.ExecAsync(async (db) =>
                    {
                        var result = new PagingDataResult<AccountMemberInfo>()
                        {
                            Data = new List<AccountMemberInfo>()
                        };
                        try
                        {
                            var listOfficialId = new RedisValue[] { };
                            if (officerId == null)
                            {
                                listOfficialId = await db.SortedSetRangeByRankAsync(_context.NameOf(IndexKeys.KeyOfficerOfAccount<AccountMember>(accountId)));

                                listOfficialId = (await db.HashGetAsync(_context.NameOf<AccountMember>(), listOfficialId?.Select(p => (RedisValue)(p + IndexKeys.SeparateChar + accountId.ToString()))?.ToArray(), CommandFlags.PreferSlave))?.Select(p => Json.Parse<AccountMember>(p))?.Where(p => p.Permissions != null && p.Permissions.Contains(MemberPermission.ManagerMember))?.Select(p => (RedisValue)p.OfficerId)?.ToArray();
                            }
                            else
                            {
                                listOfficialId.Append(officerId);
                            }

                            var listMemberId = new List<RedisValue>();

                            foreach (var id in listOfficialId)
                            {
                                var listTmp = await db.SortedSetRangeByRankAsync(_context.NameOf(IndexKeys.KeyAccountMemberInOfficer<AccountMember>((long)id)), 0, -1, Order.Descending, CommandFlags.PreferSlave);
                                listMemberId.AddRange(listTmp?.Select(p => (RedisValue)(id + IndexKeys.SeparateChar + p)));
                            }

                            if (listMemberId != null && listMemberId.Count() > 0)
                            {
                                var listMember = default(IEnumerable<AccountMember>);
                                if (role == null)
                                {
                                    listMember = (await db.HashGetAsync(_context.NameOf<AccountMember>(), listMemberId.ToArray(), CommandFlags.PreferSlave))?.Select(p => Json.Parse<AccountMember>(p));
                                }
                                else
                                {
                                    listMember = (await db.HashGetAsync(_context.NameOf<AccountMember>(), listMemberId.ToArray(), CommandFlags.PreferSlave))?.Select(p => Json.Parse<AccountMember>(p))?.Where(p => p.Role == role);
                                }

                                if (listMember != null && listMember.Count() > 0)
                                {
                                    if (string.IsNullOrEmpty(keyWord))
                                    {
                                        result.Total = listMember?.Count() ?? 0;
                                        result.Data = listMember.Skip((pageIndex - 1) * pageSize).Take(pageSize).Select(p => Mapper<AccountMemberInfo>.Map(p, new AccountMemberInfo()))?.ToList();

                                        var listAccount = (await db.HashGetAsync(_context.NameOf<Account>(), result.Data?.Select(p => (RedisValue)p.MemberId)?.ToArray(), CommandFlags.PreferSlave))?.Select(p => Json.Parse<Account>(p));
                                        foreach (var member in result.Data)
                                        {
                                            var account = listAccount?.Where(p => p.Id == member.MemberId)?.FirstOrDefault();
                                            member.FullName = account?.FullName;
                                            member.Email = account?.Email;
                                            member.Mobile = account?.Mobile;
                                        }
                                    }
                                    else
                                    {
                                        var listAccount = (await db.HashGetAsync(_context.NameOf<Account>(), listMember.Select(p => (RedisValue)p.MemberId)?.ToArray(), CommandFlags.PreferSlave))?.Select(p => Json.Parse<Account>(p));

                                        var resultSearch = listAccount?.Where(p => (p.FullName != null && p.FullName.Contains(keyWord))
                                        || (p.Mobile != null && p.Mobile.Contains(keyWord)) || (p.Email != null && p.Email.Contains(keyWord)))?.Select(p => p.Id);

                                        if (resultSearch != null)
                                        {
                                            listMember = listMember.Where(p => resultSearch.Contains(p.MemberId));
                                            result.Total = listMember?.Count() ?? 0;
                                            result.Data = listMember?.Skip((pageIndex - 1) * pageSize).Take(pageSize).Select(p => Mapper<AccountMemberInfo>.Map(p, new AccountMemberInfo()))?.ToList();

                                            foreach (var member in result.Data)
                                            {
                                                var account = listAccount?.Where(p => p.Id == member.MemberId)?.FirstOrDefault();
                                                member.FullName = account?.FullName;
                                                member.Email = account?.Email;
                                                member.Mobile = account?.Mobile;
                                            }
                                        }
                                    }
                                }
                            }

                            //if (role == null)
                            //{
                            //    var listId = await db.SortedSetRangeByRankAsync(_context.NameOf(IndexKeys.KeyAccountMemberInOfficer<AccountMember>(officerId)), (pageIndex - 1) * pageSize,
                            //    pageSize, Order.Descending, CommandFlags.PreferSlave);

                            //    result.Total = (int)await db.SortedSetLengthAsync(_context.NameOf(IndexKeys.KeyAccountMemberInOfficer<AccountMember>(officerId)), double.MinValue, double.MaxValue, Exclude.None, CommandFlags.PreferSlave);

                            //    if (listId != null && listId.Count() > 0)
                            //    {
                            //        var listMember = (await db.HashGetAsync(_context.NameOf<AccountMember>(), listId?.Select(p => (RedisValue)(officerId + IndexKeys.SeparateChar + p.ToString()))?.ToArray(), CommandFlags.PreferSlave))?.Select(p => Json.Parse<AccountMember>(p));
                            //        if (listMember != null && listMember.Count() > 0)
                            //        {
                            //            result.Data = listMember.Select(p => Mapper<AccountMemberInfo>.Map(p, new AccountMemberInfo()))?.ToList();
                            //            var listAccount = (await db.HashGetAsync(_context.NameOf<Account>(), listMember?.Select(p => (RedisValue)p.MemberId)?.ToArray(), CommandFlags.PreferSlave))?.Select(p => Json.Parse<Account>(p));
                            //            foreach (var member in result.Data)
                            //            {
                            //                var account = listAccount?.Where(p => p.Id == member.MemberId)?.FirstOrDefault();
                            //                member.FullName = account?.FullName;
                            //                member.Email = account?.Email;
                            //                member.Mobile = account?.Mobile;
                            //            }
                            //        }
                            //    }
                            //}
                            //else
                            //{
                            //    var listId = await db.SortedSetRangeByRankAsync(_context.NameOf(IndexKeys.KeyAccountMemberInOfficer<AccountMember>(officerId)), 0,
                            //    -1, Order.Descending, CommandFlags.PreferSlave);
                            //    if (listId != null && listId.Count() > 0)
                            //    {
                            //        var listMember = (await db.HashGetAsync(_context.NameOf<AccountMember>(), listId?.Select(p => (RedisValue)(officerId + IndexKeys.SeparateChar + p.ToString()))?.ToArray(), CommandFlags.PreferSlave))?.Select(p => Json.Parse<AccountMember>(p));
                            //        var listAll = listMember?.Where(p => p.Role == role)?.ToList();

                            //        result.Total = listAll?.Count() ?? 0;
                            //        var listReturn = listAll?.Skip((pageIndex - 1) * pageSize).Take(pageSize)?.ToList();

                            //        if (listReturn != null && listReturn.Count() > 0)
                            //        {
                            //            result.Data = listReturn.Select(p => Mapper<AccountMemberInfo>.Map(p, new AccountMemberInfo()))?.ToList();
                            //            var listAccount = (await db.HashGetAsync(_context.NameOf<Account>(), result.Data?.Select(p => (RedisValue)p.MemberId)?.ToArray(), CommandFlags.PreferSlave))?.Select(p => Json.Parse<Account>(p));
                            //            foreach (var member in result.Data)
                            //            {
                            //                var account = listAccount?.Where(p => p.Id == member.MemberId)?.FirstOrDefault();
                            //                member.FullName = account?.FullName;
                            //                member.Email = account?.Email;
                            //                member.Mobile = account?.Mobile;
                            //            }
                            //        }
                            //    }
                            //}
                        }
                        catch (Exception ex)
                        {
                            Logger.Error(ex, ex.Message);
                        }

                        return result;
                    });
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
            }
            return returnValue;
        }

        public Task<List<AccountMemberInfo>> GetMembersAsync(long officerId)
        {
            var returnValue = Task.FromResult(default(List<AccountMemberInfo>));
            try
            {
                using (var _context = GetContext())
                {
                    returnValue = _context.ExecAsync(async (db) =>
                    {
                        var result = default(List<AccountMemberInfo>);
                        try
                        {
                            var listId = await db.SortedSetRangeByRankAsync(_context.NameOf(IndexKeys.KeyAccountMemberInOfficer<AccountMember>(officerId)), 0,
                        -1, Order.Descending, CommandFlags.PreferSlave);

                            if (listId != null && listId.Count() > 0)
                            {
                                var listMem = (await db.HashGetAsync(_context.NameOf<AccountMember>(), listId?.Select(p => (RedisValue)(officerId + IndexKeys.SeparateChar + p.ToString()))?.ToArray(), CommandFlags.PreferSlave))?.Select(p => Json.Parse<AccountMember>(p))?.ToList();

                                if (listMem != null && listMem.Count() > 0)
                                {
                                    result = listMem?.Select(p => Mapper<AccountMemberInfo>.Map(p, new AccountMemberInfo()))?.ToList();

                                    var listAcc = (await db.HashGetAsync(_context.NameOf<Account>(), listMem?.Select(p => (RedisValue)p.MemberId)?.ToArray(), CommandFlags.PreferSlave))?.Select(p => Json.Parse<Account>(p));
                                    result.ForEach(p =>
                                    {
                                        var tmp = listAcc?.Where(w => w.Id == p.MemberId)?.FirstOrDefault();
                                        p.Email = tmp?.Email;
                                        p.FullName = tmp?.FullName;
                                        p.Mobile = tmp?.Mobile;
                                        p.Avatar = tmp?.Avatar;
                                        p.DelegatorId = tmp?.DelegatorId;
                                    });
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            Logger.Error(ex, ex.Message);
                        }

                        return result;
                    });
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
            }
            return returnValue;
        }


        public Task<List<AccountMemberInfo>> ApproversAsync(long officerId)
        {
            var returnValue = Task.FromResult(default(List<AccountMemberInfo>));
            try
            {
                using (var _context = GetContext())
                {
                    returnValue = _context.ExecAsync(async (db) =>
                    {
                        var result = default(List<AccountMemberInfo>);
                        try
                        {
                            var listId = await db.SortedSetRangeByRankAsync(_context.NameOf(IndexKeys.KeyAccountMemberInOfficer<AccountMember>(officerId)), 0,
                                                -1, Order.Descending, CommandFlags.PreferSlave);
                            if (listId != null && listId.Count() > 0)
                            {
                                var listMember = (await db.HashGetAsync(_context.NameOf<AccountMember>(), listId?.Select(p => (RedisValue)(officerId + IndexKeys.SeparateChar + p.ToString()))?.ToArray(), CommandFlags.PreferSlave))?.Select(p => Json.Parse<AccountMember>(p));
                                var listAll = listMember?.Where(p => p.Permissions != null && p.Permissions.Contains(MemberPermission.ApprovePost))?.ToList();

                                if (listAll != null && listAll.Count() > 0)
                                {
                                    result = listAll.Select(p => Mapper<AccountMemberInfo>.Map(p, new AccountMemberInfo()))?.ToList();
                                    if (result != null)
                                    {
                                        var listAccount = (await db.HashGetAsync(_context.NameOf<Account>(), result?.Select(p => (RedisValue)p.MemberId)?.ToArray(), CommandFlags.PreferSlave))?.Select(p => Json.Parse<Account>(p));
                                        foreach (var member in result)
                                        {
                                            var account = listAccount?.Where(p => p.Id == member.MemberId)?.FirstOrDefault();
                                            member.FullName = account?.FullName;
                                            member.Email = account?.Email;
                                            member.Mobile = account?.Mobile;
                                        }
                                    }
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            Logger.Error(ex, ex.Message);
                        }

                        return result;
                    });
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
            }
            return returnValue;
        }

        public async Task<bool> IsMemberAsync(long userId)
        {
            var returnValue = false;
            try
            {
                using (var _context = GetContext())
                {
                    var listOfficer = await _context.SortedSetLengthAsync(IndexKeys.KeyOfficerOfAccount<AccountMember>(userId), double.NegativeInfinity, double.PositiveInfinity, Exclude.None, CommandFlags.PreferSlave);
                    if (listOfficer > 0) returnValue = true;
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
            }
            return returnValue;
        }

        public async Task<ErrorCodes> ChangeOwnerAsync(long officerId, long oldOwnerId, long newOwnerId, long userId)
        {
            var returnValue = ErrorCodes.UpdateError;
            try
            {
                using (var _context = GetContext())
                {
                    var userCurrent = await _context.HashGetExAsync<Account>(userId);
                    if (userCurrent != null && userCurrent.IsFullPermission == true && userCurrent.IsSystem == true)
                    {
                        //var checkOwnerExist = await _context.SortedSetLengthAsync(IndexKeys.KeyOwnerOfficer(newOwnerId), double.NegativeInfinity, double.PositiveInfinity, Exclude.None, CommandFlags.PreferSlave);

                        //if (checkOwnerExist > 0)
                        //{
                        //    returnValue = ErrorCodes.PageOfUserExist;
                        //}
                        //else
                        //{
                        var officer = await _context.HashGetExAsync<Account>(officerId, CommandFlags.PreferSlave);
                        if (officer != null && oldOwnerId.ToString().Equals(officer.CreatedBy))
                        {
                            officer.CreatedBy = newOwnerId.ToString();
                            var resultUpdate = await _context.MultiAsync((transac) =>
                            {
                                transac.HashSetAsync(_context.NameOf<Account>(), officer.Id, Json.Stringify(officer));

                                    //xóa value liên quan oldOwnerId
                                    transac.SortedSetRemoveAsync(_context.NameOf(IndexKeys.KeyOwnerOfficer(oldOwnerId)), officerId);
                                transac.HashDeleteAsync(_context.NameOf<AccountMember>(), officerId + IndexKeys.SeparateChar + oldOwnerId);
                                transac.SortedSetRemoveAsync(_context.NameOf(IndexKeys.KeyAccountMemberInOfficer<AccountMember>(officerId)), oldOwnerId);
                                transac.SortedSetRemoveAsync(_context.NameOf(IndexKeys.KeyOfficerOfAccount<AccountMember>(oldOwnerId)), officerId);

                                    //thêm value liên quan newOwnerId
                                    transac.SortedSetAddAsync(_context.NameOf(IndexKeys.KeyOwnerOfficer(newOwnerId)), officerId, DateTime.Now.ConvertToTimestamp10());
                                transac.HashSetAsync(_context.NameOf<AccountMember>(), officerId + IndexKeys.SeparateChar + newOwnerId, Json.Stringify(new AccountMember
                                {
                                    OfficerId = officerId,
                                    MemberId = newOwnerId,
                                    JoinedDate = DateTime.Now,
                                    Role = (int)AccountMemberRole.Owner,
                                    Permissions = MemberRole.Roles[AccountMemberRole.Owner].ToArray()
                                }));
                                transac.SortedSetAddAsync(_context.NameOf(IndexKeys.KeyAccountMemberInOfficer<AccountMember>(officerId)), newOwnerId, DateTime.Now.ConvertToTimestamp10());
                                transac.SortedSetAddAsync(_context.NameOf(IndexKeys.KeyOfficerOfAccount<AccountMember>(newOwnerId)), officerId, DateTime.Now.ConvertToTimestamp10());
                            });

                            if (resultUpdate) returnValue = ErrorCodes.Success;
                        }
                        else
                        {
                            returnValue = ErrorCodes.DataInvalid;
                        }
                        //}
                    }
                    else
                    {
                        returnValue = ErrorCodes.ChangeOwnerPermissionInvalid;
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
            }
            return returnValue;
        }

        public Task<PagingDataResult<OfficialAccount>> MyOfficialAsync(string keyword, long userId, int pageIndex, int pageSize, AccountStatus status = AccountStatus.Actived)
        {
            var returnValue = Task.FromResult(default(PagingDataResult<OfficialAccount>));
            try
            {
                using (var _context = GetContext())
                {
                    returnValue = _context.ExecAsync(async (db) =>
                    {
                        var result = new PagingDataResult<OfficialAccount>();
                        try
                        {
                            var total = await db.SortedSetLengthAsync(_context.NameOf(IndexKeys.KeyOfficerOfAccount<AccountMember>(userId)), double.NegativeInfinity, double.PositiveInfinity, Exclude.None, CommandFlags.PreferSlave);
                            var listId = await db.SortedSetRangeByRankAsync(_context.NameOf(IndexKeys.KeyOfficerOfAccount<AccountMember>(userId)), (pageIndex - 1) * pageSize, pageSize * pageIndex - 1, Order.Descending, CommandFlags.PreferSlave);

                            var listPage = await db.SortedSetRangeByRankAsync(_context.NameOf(IndexKeys.KeyOwnerOfficer(userId)), 0, -1, Order.Descending, CommandFlags.PreferSlave);

                            if (listId != null && listId.Count() > 0)
                            {
                                var listOfficer = (await db.HashGetAsync(_context.NameOf<Account>(), listId?.ToArray(), CommandFlags.PreferSlave))?.Select(p => Json.Parse<OfficialAccount>(p))?.ToList();
                                var listProfile = (await db.HashGetAsync(_context.NameOf<UserProfile>(), listId?.ToArray(), CommandFlags.PreferSlave))?.Select(p => Json.Parse<UserProfile>(p))?.ToList();

                                if (listOfficer != null && listOfficer.Count() > 0)
                                {
                                    listOfficer = listOfficer.Where(p => p != null && p.Status == (int)AccountStatus.Actived).ToList();
                                    if (listPage != null && listPage.Count() > 0)
                                    {
                                        foreach (var officer in listOfficer)
                                        {
                                            if (listPage.Contains((RedisValue)officer.Id))
                                            {
                                                officer.IsOwner = true;
                                            }
                                            officer.ProfileUrl = listProfile?.Where(p => p!=null && p.Id == officer.Id)?.FirstOrDefault()?.ProfileUrl;
                                        }
                                    }

                                    result.Total = (int)total;
                                    result.Data = listOfficer.ToList();
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            Logger.Error(ex, ex.Message);
                        }

                        return result;
                    });
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
            }
            return returnValue;
        }

        public Task<PagingDataResult<OfficialAccount>> MyPagesAsync(SearchAccountEntity search, AccountStatus status = AccountStatus.Actived)
        {
            var returnValue = Task.FromResult(default(PagingDataResult<OfficialAccount>));
            try
            {
                using (var _context = GetContext())
                {
                    returnValue = _context.ExecAsync(async (db) =>
                    {
                        var result = new PagingDataResult<OfficialAccount>();
                        try
                        {
                            var total = await db.SortedSetLengthAsync(_context.NameOf(IndexKeys.KeyOfficerOfAccount<AccountMember>(search.CreatedBy)), double.NegativeInfinity, double.PositiveInfinity, Exclude.None, CommandFlags.PreferSlave);
                            var listId = await db.SortedSetRangeByRankAsync(_context.NameOf(IndexKeys.KeyOfficerOfAccount<AccountMember>(search.CreatedBy)), (search.PageIndex.Value - 1) * search.PageSize.Value, search.PageSize.Value * search.PageIndex.Value - 1, Order.Descending, CommandFlags.PreferSlave);

                            var listPage = await db.SortedSetRangeByRankAsync(_context.NameOf(IndexKeys.KeyOwnerOfficer(search.CreatedBy.Value)), 0, -1, Order.Descending, CommandFlags.PreferSlave);

                            if (listId != null && listId.Count() > 0)
                            {
                                var listOfficer = (await db.HashGetAsync(_context.NameOf<Account>(), listId?.ToArray(), CommandFlags.PreferSlave))?.Select(p => Json.Parse<OfficialAccount>(p))?.ToList();
                                var listProfile = (await db.HashGetAsync(_context.NameOf<UserProfile>(), listId?.ToArray(), CommandFlags.PreferSlave))?.Select(p => Json.Parse<UserProfile>(p))?.ToList();

                                if (listOfficer != null && listOfficer.Count() > 0)
                                {
                                    listOfficer = listOfficer.Where(p => p != null).ToList();
                                    if (listPage != null && listPage.Count() > 0)
                                    {
                                        foreach (var officer in listOfficer)
                                        {
                                            if (listPage.Contains((RedisValue)officer.Id))
                                            {
                                                officer.IsOwner = true;
                                            }
                                            officer.ProfileUrl = listProfile?.Where(p => p!=null && p.Id == officer.Id)?.FirstOrDefault()?.ProfileUrl;
                                        }
                                    }

                                    result.Total = (int)total;
                                    result.Data = listOfficer.ToList();
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            Logger.Error(ex, ex.Message);
                        }

                        return result;
                    });
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
            }
            return returnValue;
        }

        public Task<List<OfficialAccount>> GetListPageOwnerAsync(long ownerId)
        {
            var returnValue = Task.FromResult(default(List<OfficialAccount>));
            try
            {
                using (var _context = GetContext())
                {
                    returnValue = _context.ExecAsync(async (db) =>
                    {
                        var result = new List<OfficialAccount>();
                        try
                        {
                            var listPage = await db.SortedSetRangeByRankAsync(_context.NameOf(IndexKeys.KeyOwnerOfficer(ownerId)), 0, -1, Order.Descending, CommandFlags.PreferSlave);

                            result = (await db.HashGetAsync(_context.NameOf<Account>(), listPage?.ToArray(), CommandFlags.PreferSlave))?.Select(p => Json.Parse<OfficialAccount>(p))?.ToList();
                        }
                        catch (Exception ex)
                        {
                            Logger.Error(ex, ex.Message);
                        }

                        return result;
                    });
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
            }
            return returnValue;
        }

        public async Task<bool> IsModifier(long officerId, long userId)
        {
            var IsOwner = false;
            try
            {
                using (var _context = GetContext())
                {
                    var listMemberId = await _context.SortedSetRangeByRankAsync(IndexKeys.KeyAccountMemberInOfficer<AccountMember>(officerId), 0, -1, Order.Descending, CommandFlags.PreferSlave);
                    var listMember = await _context.GetsFromHashExAsync<AccountMember>(listMemberId?.Select(p => officerId + IndexKeys.SeparateChar + p.ToString())?.ToList(), CommandFlags.PreferSlave);

                    if ((int?)listMember?.Where(p => p.MemberId == userId)?.FirstOrDefault()?.Role <= (int)AccountMemberRole.Admin)
                    {
                        IsOwner = true;
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
            }
            return IsOwner;
        }

        public async Task<long?> CheckMobileAsync(string mobile)
        {
            var result = default(long?);
            try
            {
                using (var _context = GetContext())
                {
                    result = (long?) await _context.SortedSetScoreAsync(IndexKeys.KeyPhoneNumberInSytem<Account>(), mobile);
                    if(!(result > 0))
                    {
                        result = (long?)await _context.SortedSetScoreAsync(IndexKeys.KeyEmailInSytem<Account>(), mobile);
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
            }
            return result;
        }

        public async Task<ErrorCodes> SetProfileIsPageOnAppAsync(long officerId, long userId, long memberId, bool isEnable, Action<Account> action)
        {
            var returnValue = ErrorCodes.UnknowError;
            try
            {
                using (var _context = GetContext())
                {
                    returnValue = await _context.ExecAsync(async (db) =>
                    {
                        var result = ErrorCodes.UnknowError;

                        var officer = Json.Parse<Account>(await db.HashGetAsync(_context.NameOf<Account>(), officerId, CommandFlags.PreferSlave));
                        if(officer != null)
                        {
                            var ownver = Json.Parse<Account>(await db.HashGetAsync(_context.NameOf<Account>(), officer?.CreatedBy, CommandFlags.PreferSlave));
                            if (ownver != null)
                            {
                                //var pageCount = await db.SortedSetLengthAsync(_context.NameOf(IndexKeys.KeyOwnerOfficer(ownver.Id)), double.NegativeInfinity, double.PositiveInfinity);
                                var pageIds = await db.SortedSetRangeByRankAsync(_context.NameOf(IndexKeys.KeyOwnerOfficer(ownver.Id)), 0, -1);
                                var listPage = (await db.HashGetAsync(_context.NameOf<Account>(), pageIds))?.Select(p => Json.Parse<Account>(p))?.Where(p => p != null && p.Status == (int)AccountStatus.Actived);

                                if (isEnable == true)
                                {
                                    if (listPage?.Select(p=>p.Id == officerId) != null)
                                    {
                                        var currentMember = Json.Parse<AccountMember>(await db.HashGetAsync(_context.NameOf<AccountMember>(), officerId + IndexKeys.SeparateChar + userId, CommandFlags.PreferSlave));
                                        if (currentMember != null && currentMember.IsLocked == false && currentMember.Permissions != null && currentMember.Permissions.Contains(MemberPermission.ManagerMember))
                                        {
                                            if (officer.Class == (byte?)AccountClass.KOL || officer.Class == (byte?)AccountClass.Exbert)
                                            {
                                                var member = Json.Parse<AccountMember>(await db.HashGetAsync(_context.NameOf<AccountMember>(), officerId + IndexKeys.SeparateChar + memberId, CommandFlags.PreferSlave));
                                                if (member != null)
                                                {
                                                    var memberAccount = Json.Parse<Account>(await db.HashGetAsync(_context.NameOf<Account>(), memberId, CommandFlags.PreferSlave));
                                                    if (memberAccount != null)
                                                    {
                                                        if (memberAccount.DelegatorId == null)
                                                        {
                                                            memberAccount.DelegatorId = officerId;
                                                            await db.HashSetAsync(_context.NameOf<Account>(), memberAccount.Id, Json.Stringify(memberAccount));
                                                            result = ErrorCodes.Success;
                                                        }
                                                        else
                                                        {
                                                            result = ErrorCodes.SetProfileError_MemberIsSetted;
                                                        }
                                                        action(memberAccount);
                                                    }
                                                    else
                                                    {
                                                        result = ErrorCodes.AccountNotExits;
                                                    }
                                                }
                                                else
                                                {
                                                    result = ErrorCodes.AccountNotExits;
                                                }
                                            }
                                            else
                                            {
                                                result = ErrorCodes.PermissionUpdateInvalid;
                                            }
                                        }
                                        else
                                        {
                                            result = ErrorCodes.PermissionUpdateInvalid;
                                        }
                                    }
                                    else
                                    {
                                        result = ErrorCodes.AddProfileError_NumberOwnerPageInvalid;
                                    }
                                }
                                else
                                {
                                    var currentMember = Json.Parse<AccountMember>(await db.HashGetAsync(_context.NameOf<AccountMember>(), officerId + IndexKeys.SeparateChar + userId, CommandFlags.PreferSlave));
                                    if (currentMember != null && currentMember.IsLocked == false && currentMember.Permissions != null && currentMember.Permissions.Contains(MemberPermission.ManagerMember))
                                    {
                                        if (officer.Class == (byte?)AccountClass.KOL || officer.Class == (byte?)AccountClass.Exbert)
                                        {
                                            var member = Json.Parse<AccountMember>(await db.HashGetAsync(_context.NameOf<AccountMember>(), officerId + IndexKeys.SeparateChar + memberId, CommandFlags.PreferSlave));
                                            if (member != null)
                                            {
                                                var memberAccount = Json.Parse<Account>(await db.HashGetAsync(_context.NameOf<Account>(), memberId, CommandFlags.PreferSlave));
                                                if (memberAccount != null)
                                                {
                                                    if (memberAccount.DelegatorId == officerId)
                                                    {
                                                        memberAccount.DelegatorId = null;
                                                        await db.HashSetAsync(_context.NameOf<Account>(), memberAccount.Id, Json.Stringify(memberAccount));
                                                        result = ErrorCodes.Success;
                                                    }
                                                    else
                                                    {
                                                        result = ErrorCodes.RemoveProfileError_MemberIsNotSetted;
                                                    }
                                                    action(memberAccount);
                                                }
                                                else
                                                {
                                                    result = ErrorCodes.AccountNotExits;
                                                }
                                            }
                                            else
                                            {
                                                result = ErrorCodes.AccountNotExits;
                                            }
                                        }
                                        else
                                        {
                                            result = ErrorCodes.PermissionUpdateInvalid;
                                        }
                                    }
                                    else
                                    {
                                        result = ErrorCodes.PermissionUpdateInvalid;
                                    }
                                }
                            }
                            else
                            {
                                result = ErrorCodes.AccountNotExits;
                            }
                        }
                        else
                        {
                            result = ErrorCodes.AccountNotExits;
                        }
                        
                        return result;
                    });
                }
            }
            catch(Exception ex)
            {
                Logger.Error(ex, ex.Message);
            }
            return returnValue;
        }

        public async Task<bool> UpdateProfileUrlAsync(UserProfile profile)
        {
            var returnValue = false;
            try
            {
                using (var _context = GetContext())
                {
                    returnValue = await _context.MultiAsync((trans) =>
                    {
                        trans.HashSetAsync(_context.NameOf<UserProfile>(), profile.Id, Json.Stringify(profile));
                    });
                }
            }catch(Exception ex)
            {
                Logger.Error(ex, ex.Message);
            }
            return returnValue;
        }

        public async Task<long> GetAccountInNewsAsync(long newsId)
        {
            var returnValue = 0L;
            try
            {
                using (var _context = GetContext())
                {
                    returnValue = (long)(await _context.SetMembersAsync(typeof(NewsInAccount).Name, CommandFlags.PreferSlave))?.FirstOrDefault();
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
            }
            return returnValue;
        }

        public Task<string> GetAccessTokenAsync(string mobile)
        {
            var returnValue = Task.FromResult(string.Empty);
            try
            {
                using (var _context = GetContext())
                {
                    returnValue = _context.ExecAsync(async (db) =>
                    {
                        var accountId = await db.SortedSetScoreAsync(_context.NameOf(IndexKeys.KeyPhoneNumberInSytem<Account>()), mobile, CommandFlags.PreferSlave);
                        var account = Json.Parse<Account>(await db.HashGetAsync(_context.NameOf<Account>(), accountId, CommandFlags.PreferSlave));

                        return account?.OtpSecretKey;
                    });
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
            }
            return returnValue;
        }
    }
}
