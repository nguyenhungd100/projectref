﻿using ChannelVN.IMS2.Core.Entities;
using ChannelVN.IMS2.Core.Entities.Distribution;
using ChannelVN.IMS2.Core.Entities.Security;
using ChannelVN.IMS2.Core.Entities.ShareLinks;
using ChannelVN.IMS2.Foundation.Common;
using ChannelVN.IMS2.Foundation.Logging;
using StackExchange.Redis;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using static ChannelVN.IMS2.Core.Entities.ErrorCode.ErrorMapping;

namespace ChannelVN.IMS2.Core.Models.CacheStore.ShareLinks
{
    public class ShareLinkCacheStore : CmsMainCacheStore
    {
        public async Task<ErrorCodes> AddAsync(ShareLinkSearch dataSearch)
        {
            var returnValue = ErrorCodes.AddError;
            try
            {
                var data = Mapper<ShareLink>.Map(dataSearch, new ShareLink());
                var newsAll = Mapper<NewsAll>.MapNewsAll(data, new NewsAll());

                using (var _context = GetContext())
                {
                    //Kiểm tra quyền viết bài
                    var isWriter = await Functions.Function.IsWriter(dataSearch?.NewsInAccount?.FirstOrDefault().AccountId, dataSearch.CreatedBy, _context);

                    ///
                    if (isWriter)
                    {
                        var resultAdd = await _context.MultiAsync((transac) =>
                        {
                            transac.HashSetAsync(_context.NameOf(data.GetType().Name), data.Id, Json.Stringify(data));

                            transac.HashSetAsync(_context.NameOf(newsAll.GetType().Name), newsAll.Id, Json.Stringify(newsAll));
                            transac.SortedSetAddAsync(_context.NameOf(IndexKeys.KeyCreatedBy<NewsAll>(newsAll.CreatedBy)), newsAll.Id, (newsAll.CreatedDate ?? DateTime.Now).ConvertToTimestamp10());
                            transac.SortedSetAddAsync(_context.NameOf(IndexKeys.KeyStatus<NewsAll>(newsAll.Status)), newsAll.Id, (newsAll.CreatedDate ?? DateTime.Now).ConvertToTimestamp10());
                            transac.SortedSetAddAsync(_context.NameOf(IndexKeys.KeyDistributorid<NewsAll>(newsAll.DistributorId)), newsAll.Id, (newsAll.CreatedDate ?? DateTime.Now).ConvertToTimestamp10());
                            transac.SortedSetAddAsync(_context.NameOf(IndexKeys.KeyType<NewsAll>(newsAll.Type ?? 5)), newsAll.Id, (newsAll.CreatedDate ?? DateTime.Now).ConvertToTimestamp10());

                            transac.HashSetAsync(_context.NameOf(typeof(NewsInAccount).Name), dataSearch.NewsInAccount?.Select(p => new HashEntry(p.AccountId + IndexKeys.SeparateChar + p.NewsId, Json.Stringify(p))).ToArray());
                            transac.SetAddAsync(_context.NameOf(IndexKeys.KeyAccountInNews(dataSearch.Id)), dataSearch.NewsInAccount?.LastOrDefault()?.AccountId ?? 0);
                            transac.SetAddAsync(_context.NameOf(IndexKeys.KeyNewsInAccount(dataSearch.NewsInAccount?.LastOrDefault()?.AccountId)), dataSearch.Id);
                        });
                        if (resultAdd) returnValue = ErrorCodes.Success;
                    }
                    else
                    {
                        returnValue = ErrorCodes.PermissionInvalid;
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
                returnValue = ErrorCodes.Exception;
            }
            return returnValue;
        }

        public Task<ShareLink> GetByIdAsync(long id)
        {
            var returnValue = Task.FromResult(default(ShareLink));
            try
            {
                using (var _context = GetContext())
                {
                    returnValue = _context.HashGetExAsync<ShareLink>(id, CommandFlags.PreferSlave);
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
            }
            return returnValue;
        }

        public Task<ShareLinkSearch> GetFullInfoAsync(long newsId)
        {
            var returnValue = Task.FromResult(default(ShareLinkSearch));
            try
            {
                using (var _context = GetContext())
                {
                    returnValue = _context.ExecAsync(async (db) =>
                    {
                        var result = default(ShareLinkSearch);
                        try
                        {
                            var post = Json.Parse<ShareLink>(await db.HashGetAsync(_context.NameOf<ShareLink>(), newsId, CommandFlags.PreferSlave));
                            if (post != null)
                            {
                                result = Mapper<ShareLinkSearch>.Map(post, new ShareLinkSearch());
                                if (result != null)
                                {
                                    var listAccountId = await db.SetMembersAsync(_context.NameOf(IndexKeys.KeyAccountInNews(newsId)), CommandFlags.PreferSlave);
                                    result.NewsInAccount = (await db.HashGetAsync(_context.NameOf<NewsInAccount>(), listAccountId?.Select(p => (RedisValue)(p + IndexKeys.SeparateChar + newsId))?.ToArray(), CommandFlags.PreferSlave))?.Select(p => Json.Parse<NewsInAccount>(p))?.ToArray();

                                    var listDistributorIdAndItemStreamId = await db.SortedSetRangeByScoreWithScoresAsync(_context.NameOf(IndexKeys.KeyNewsDistribution(newsId)), double.MinValue, double.MaxValue, Exclude.None, Order.Ascending, 0, -1, CommandFlags.PreferSlave);

                                    var listKeyNewsDistribution = listDistributorIdAndItemStreamId?.Select(p => (RedisValue)(p.Element.ToString()?.Split(IndexKeys.SeparateChar)?.FirstOrDefault() + IndexKeys.SeparateChar
                                    + newsId + IndexKeys.SeparateChar + p.Element.ToString()?.Split(IndexKeys.SeparateChar)?.LastOrDefault()));
                                    result.NewsDistribution = (await db.HashGetAsync(_context.NameOf<NewsDistribution>(), listKeyNewsDistribution?.ToArray(), CommandFlags.PreferSlave))?.Select(p => Json.Parse<NewsDistribution>(p))?.ToArray();
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            Logger.Error(ex, ex.Message);
                        }

                        return result;
                    });
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
            }
            return returnValue;
        }
        public Task<bool> UpdateStatusAsync(ShareLink data, int statusOld)
        {
            var returnValue = Task.FromResult(false);
            try
            {
                var newsAll = Mapper<NewsAll>.MapNewsAll(data, new NewsAll());
                
                if (data != null)
                {
                    using (var _context = GetContext())
                    {
                        returnValue = _context.MultiAsync((transac) =>
                        {
                            transac.HashSetAsync(_context.NameOf(data.GetType().Name), data.Id, Json.Stringify(data));

                            transac.HashSetAsync(_context.NameOf(newsAll.GetType().Name), newsAll.Id, Json.Stringify(newsAll));
                            transac.SortedSetRemoveAsync(_context.NameOf(IndexKeys.KeyStatus<NewsAll>(statusOld)), newsAll.Id);
                            transac.SortedSetAddAsync(_context.NameOf(IndexKeys.KeyStatus<NewsAll>(newsAll.Status)), newsAll.Id, (newsAll.CreatedDate ?? DateTime.Now).ConvertToTimestamp10());
                        });
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
            }
            return returnValue;
        }

        public async Task<ErrorCodes> UpdateStatusV2Async(long id, NewsStatus status, long userId, Action<ShareLinkSearch, int> actionAutoShare)
        {
            var returnValue = ErrorCodes.UpdateError;
            var statusOld = default(int?);
            var dataDb = default(ShareLink);

            try
            {
                using (var _context = GetContext())
                {
                    var newsAll = default(NewsAll);
                    var resultUpdate = false;
                    var officerIds = default(RedisValue[]);
                    var officerId = 0L;

                    dataDb = await _context.HashGetExAsync<ShareLink>(id, CommandFlags.PreferSlave);
                    statusOld = dataDb.Status;

                    if (dataDb != null)
                    {
                        //fix ko cho phuc hoi khi xoa post
                        //if (dataDb.Status == (int)NewsStatus.Remove)
                        //    returnValue = ErrorCodes.PostRemovedNotRecovery;

                        if (dataDb.Status == (int)status)
                        {
                            returnValue = ErrorCodes.Success;
                        }
                        else
                        {
                            switch (status)
                            {
                                #region Xuat ban
                                case NewsStatus.Published:// xuất bản
                                    officerIds = await _context.SetMembersAsync(IndexKeys.KeyAccountInNews(id), CommandFlags.PreferSlave);
                                    if (officerIds != null && officerIds.Count() != 0)
                                    {
                                        if (long.TryParse(officerIds[0], out officerId))
                                        {
                                            var officer = await _context.HashGetExAsync<Account>(officerId, CommandFlags.PreferSlave);

                                            var listMemberId = await _context.SortedSetRangeByScoreAsync(IndexKeys.KeyAccountMemberInOfficer<AccountMember>(officerId), double.NegativeInfinity, double.PositiveInfinity);

                                            var listMember = await _context.GetsFromHashExAsync<AccountMember>(listMemberId?.Select(p => officerId + IndexKeys.SeparateChar + p.ToString())?.ToList(), CommandFlags.PreferSlave);
                                            var accountMember = listMember?.Where(p => p.MemberId == userId).FirstOrDefault();

                                            if (accountMember != null && officer != null)
                                            {
                                                var isPublisher = await Functions.Function.IsPublisher(accountMember, dataDb.CreatedBy, dataDb.ApprovedBy, officer.Mode, dataDb.Status);
                                                if (isPublisher)
                                                {
                                                    dataDb.Status = (int)status;
                                                    dataDb.ModifiedDate = DateTime.Now;
                                                    dataDb.ModifiedBy = userId.ToString();
                                                    dataDb.DistributionDate = dataDb.DistributionDate > DateTime.Now ? dataDb.DistributionDate : DateTime.Now;

                                                    var oldApprovedBy = dataDb.ApprovedBy;
                                                    dataDb.ApprovedBy = accountMember.MemberId.ToString();
                                                    dataDb.ApprovedDate = DateTime.Now;

                                                    newsAll = Mapper<NewsAll>.MapNewsAll(dataDb, new NewsAll());

                                                    resultUpdate = await _context.MultiAsync((transac) =>
                                                    {
                                                        transac.HashSetAsync(_context.NameOf(dataDb.GetType().Name), dataDb.Id, Json.Stringify(dataDb));
                                                        transac.HashSetAsync(_context.NameOf(newsAll.GetType().Name), newsAll.Id, Json.Stringify(newsAll));

                                                        transac.SortedSetRemoveAsync(_context.NameOf(IndexKeys.KeyStatus<NewsAll>(statusOld)), newsAll.Id);
                                                        transac.SortedSetAddAsync(_context.NameOf(IndexKeys.KeyStatus<NewsAll>(newsAll.Status)), newsAll.Id, (newsAll.DistributionDate ?? DateTime.Now).ConvertToTimestamp10());

                                                        //remove value in set approve in account if exist
                                                        if (!string.IsNullOrEmpty(oldApprovedBy) && long.TryParse(oldApprovedBy, out long oldApprovedById))
                                                        {
                                                            transac.SortedSetRemoveAsync(_context.NameOf(IndexKeys.KeyApproveInAccount<ShareLink>(officerId, oldApprovedById)), dataDb.Id, CommandFlags.PreferSlave);
                                                        }
                                                        else
                                                        {
                                                            //remove value in set approve if exist
                                                            transac.SortedSetRemoveAsync(_context.NameOf(IndexKeys.KeyApprove<ShareLink>(officerId)), dataDb.Id, CommandFlags.PreferSlave);
                                                        }

                                                        //add value into key approved in account
                                                        transac.SortedSetAddAsync(_context.NameOf(IndexKeys.KeyApprovedInAccount<ShareLink>(officerId, userId)), dataDb.Id, DateTime.Now.ConvertToTimestamp10());
                                                    });

                                                    if (resultUpdate) returnValue = ErrorCodes.Success;
                                                }
                                                else
                                                {
                                                    returnValue = ErrorCodes.PermissionPublishInvalid;
                                                }
                                            }
                                            else
                                            {
                                                returnValue = ErrorCodes.PermissionPublishInvalid;
                                            }
                                        }
                                        else
                                        {
                                            returnValue = ErrorCodes.DataInvalid;
                                        }
                                    }
                                    else
                                    {
                                        returnValue = ErrorCodes.DataInvalid;
                                    }
                                    break;
                                #endregion
                                #region Xoa bai
                                case NewsStatus.Remove: // xóa bài
                                    if (statusOld != (int)NewsStatus.Published)
                                    {
                                        if (userId.ToString().Equals(dataDb.CreatedBy))
                                        {
                                            dataDb.Status = (int)status;
                                            dataDb.ModifiedDate = DateTime.Now;
                                            dataDb.ModifiedBy = userId.ToString();

                                            newsAll = Mapper<NewsAll>.MapNewsAll(dataDb, new NewsAll());

                                            resultUpdate = await _context.MultiAsync((transac) =>
                                            {
                                                transac.HashSetAsync(_context.NameOf(dataDb.GetType().Name), dataDb.Id, Json.Stringify(dataDb));

                                                transac.HashSetAsync(_context.NameOf(newsAll.GetType().Name), newsAll.Id, Json.Stringify(newsAll));

                                                transac.SortedSetRemoveAsync(_context.NameOf(IndexKeys.KeyStatus<NewsAll>(statusOld)), newsAll.Id);
                                                transac.SortedSetAddAsync(_context.NameOf(IndexKeys.KeyStatus<NewsAll>(newsAll.Status)), newsAll.Id, (dataDb.DistributionDate ?? DateTime.Now).ConvertToTimestamp10());
                                            });

                                            if (resultUpdate) returnValue = ErrorCodes.Success;
                                        }
                                        else
                                        {
                                            returnValue = ErrorCodes.PermissionUpdateInvalid;
                                        }
                                    }
                                    else
                                    {
                                        // trạng thái cũ là publish
                                        officerIds = await _context.SetMembersAsync(IndexKeys.KeyAccountInNews(id), CommandFlags.PreferSlave);
                                        if (officerIds != null && officerIds.Count() != 0)
                                        {
                                            if (long.TryParse(officerIds[0], out officerId))
                                            {
                                                var officer = await _context.HashGetExAsync<Account>(officerId, CommandFlags.PreferSlave);

                                                var listMemberId = await _context.SortedSetRangeByScoreAsync(IndexKeys.KeyAccountMemberInOfficer<AccountMember>(officerId), double.NegativeInfinity, double.PositiveInfinity);

                                                var listMember = await _context.GetsFromHashExAsync<AccountMember>(listMemberId?.Select(p => officerId + IndexKeys.SeparateChar + p.ToString())?.ToList(), CommandFlags.PreferSlave);
                                                var accountMember = listMember?.Where(p => p.MemberId == userId).FirstOrDefault();
                                                if (accountMember != null && officer!=null)
                                                {
                                                    var isRemover = await Functions.Function.IsRemover(accountMember, dataDb.CreatedBy, dataDb.ApprovedBy, officer.Mode, statusOld);
                                                    if (isRemover)
                                                    {
                                                        dataDb.Status = (int)status;
                                                        dataDb.ModifiedDate = DateTime.Now;
                                                        dataDb.ModifiedBy = userId.ToString();

                                                        newsAll = Mapper<NewsAll>.MapNewsAll(dataDb, new NewsAll());

                                                        resultUpdate = await _context.MultiAsync((transac) =>
                                                        {
                                                            transac.HashSetAsync(_context.NameOf(dataDb.GetType().Name), dataDb.Id, Json.Stringify(dataDb));

                                                            transac.HashSetAsync(_context.NameOf(newsAll.GetType().Name), newsAll.Id, Json.Stringify(newsAll));

                                                            transac.SortedSetRemoveAsync(_context.NameOf(IndexKeys.KeyStatus<NewsAll>(statusOld)), newsAll.Id);
                                                            transac.SortedSetAddAsync(_context.NameOf(IndexKeys.KeyStatus<NewsAll>(newsAll.Status)), newsAll.Id, (dataDb.DistributionDate ?? DateTime.Now).ConvertToTimestamp10());

                                                            if (long.TryParse(dataDb.ApprovedBy, out long accountId))
                                                            {
                                                                transac.SortedSetRemoveAsync(_context.NameOf(IndexKeys.KeyApprovedInAccount<ShareLink>(officerId, accountId)), dataDb.Id);
                                                            }
                                                        });

                                                        if (resultUpdate) returnValue = ErrorCodes.Success;
                                                    }
                                                    else
                                                    {
                                                        returnValue = ErrorCodes.PermissionUpdateInvalid;
                                                    }  
                                                }
                                                else
                                                {
                                                    returnValue = ErrorCodes.PermissionUpdateInvalid;
                                                }
                                            }
                                            else
                                            {
                                                returnValue = ErrorCodes.DataInvalid;
                                            }
                                        }
                                        else
                                        {
                                            returnValue = ErrorCodes.DataInvalid;
                                        }
                                    }

                                    break;
                                #endregion
                                #region
                                case NewsStatus.Approve:// Gửi nhờ duyệt
                                    if (dataDb.Status == (int)NewsStatus.Published)
                                    {
                                        returnValue = ErrorCodes.DataInvalid;
                                    }
                                    else
                                    {
                                        if (userId.ToString().Equals(dataDb.CreatedBy))
                                        {
                                            officerIds = await _context.SetMembersAsync(IndexKeys.KeyAccountInNews(id), CommandFlags.PreferSlave);
                                            if (officerIds != null && officerIds.Count() != 0)
                                            {
                                                if (long.TryParse(officerIds[0], out officerId))
                                                {
                                                    dataDb.Status = (int)status;
                                                    dataDb.ModifiedDate = DateTime.Now;
                                                    dataDb.ModifiedBy = userId.ToString();

                                                    newsAll = Mapper<NewsAll>.MapNewsAll(dataDb, new NewsAll());

                                                    resultUpdate = await _context.MultiAsync((transac) =>
                                                    {
                                                        transac.HashSetAsync(_context.NameOf(dataDb.GetType().Name), dataDb.Id, Json.Stringify(dataDb));

                                                        transac.HashSetAsync(_context.NameOf(newsAll.GetType().Name), newsAll.Id, Json.Stringify(newsAll));

                                                        transac.SortedSetRemoveAsync(_context.NameOf(IndexKeys.KeyStatus<NewsAll>(statusOld)), newsAll.Id);
                                                        transac.SortedSetAddAsync(_context.NameOf(IndexKeys.KeyStatus<NewsAll>(newsAll.Status)), newsAll.Id, (dataDb.ModifiedDate ?? DateTime.Now).ConvertToTimestamp10());

                                                        if (string.IsNullOrEmpty(dataDb.ApprovedBy))
                                                        {
                                                            transac.SortedSetAddAsync(_context.NameOf(IndexKeys.KeyApprove<ShareLink>(officerId)), dataDb.Id, DateTime.Now.ConvertToTimestamp10());
                                                        }
                                                        else
                                                        {
                                                            if (long.TryParse(dataDb.ApprovedBy, out long accountId))
                                                            {
                                                                transac.SortedSetAddAsync(_context.NameOf(IndexKeys.KeyApproveInAccount<ShareLink>(officerId, accountId)), dataDb.Id, DateTime.Now.ConvertToTimestamp10());
                                                            }
                                                        }
                                                    });
                                                }
                                            }

                                            if (resultUpdate) returnValue = ErrorCodes.Success;
                                        }
                                        else
                                        {
                                            returnValue = ErrorCodes.PermissionUpdateInvalid;
                                        }
                                    }
                                    break;
                                #endregion
                                #region Default
                                default:
                                    if (userId.ToString().Equals(dataDb.CreatedBy))
                                    {
                                        statusOld = dataDb.Status;
                                        dataDb.Status = (int)status;
                                        dataDb.ModifiedDate = DateTime.Now;
                                        dataDb.ModifiedBy = userId.ToString();

                                        if (statusOld == (int)NewsStatus.Published)
                                        {
                                            officerIds = await _context.SetMembersAsync(IndexKeys.KeyAccountInNews(id), CommandFlags.PreferSlave);
                                            if (officerIds != null && officerIds.Count() != 0)
                                            {
                                                long.TryParse(officerIds[0], out officerId);
                                            }
                                        }

                                        newsAll = Mapper<NewsAll>.MapNewsAll(dataDb, new NewsAll());

                                        resultUpdate = await _context.MultiAsync((transac) =>
                                        {
                                            transac.HashSetAsync(_context.NameOf(dataDb.GetType().Name), dataDb.Id, Json.Stringify(dataDb));

                                            transac.HashSetAsync(_context.NameOf(newsAll.GetType().Name), newsAll.Id, Json.Stringify(newsAll));

                                            transac.SortedSetRemoveAsync(_context.NameOf(IndexKeys.KeyStatus<NewsAll>(statusOld)), newsAll.Id);
                                            transac.SortedSetAddAsync(_context.NameOf(IndexKeys.KeyStatus<NewsAll>(newsAll.Status)), newsAll.Id, (dataDb.ModifiedDate ?? DateTime.Now).ConvertToTimestamp10());

                                            if (officerId > 0)
                                            {
                                                if (long.TryParse(dataDb.ApprovedBy, out long accountId))
                                                {
                                                    transac.SortedSetRemoveAsync(_context.NameOf(IndexKeys.KeyApprovedInAccount<ShareLink>(officerId, accountId)), dataDb.Id);
                                                }
                                            }
                                        });

                                        if (resultUpdate) returnValue = ErrorCodes.Success;
                                    }
                                    else
                                    {
                                        if (statusOld == (int?)NewsStatus.Published || statusOld == (int?)NewsStatus.Archive || statusOld == (int?)NewsStatus.Approve)
                                        {
                                            officerIds = await _context.SetMembersAsync(IndexKeys.KeyAccountInNews(id), CommandFlags.PreferSlave);
                                            if (officerIds != null && officerIds.Count() != 0)
                                            {
                                                if (long.TryParse(officerIds[0], out officerId))
                                                {
                                                    var officer = await _context.HashGetExAsync<Account>(officerId, CommandFlags.PreferSlave);

                                                    var listMemberId = await _context.SortedSetRangeByScoreAsync(IndexKeys.KeyAccountMemberInOfficer<AccountMember>(officerId), double.NegativeInfinity, double.PositiveInfinity);

                                                    var listMember = await _context.GetsFromHashExAsync<AccountMember>(listMemberId?.Select(p => officerId + IndexKeys.SeparateChar + p.ToString())?.ToList(), CommandFlags.PreferSlave);
                                                    var accountMember = listMember?.Where(p => p.MemberId == userId).FirstOrDefault();
                                                    if (accountMember != null && officer!=null)
                                                    {
                                                        var isRemover = await Functions.Function.IsRemover(accountMember, dataDb.CreatedBy, dataDb.ApprovedBy, officer.Mode, statusOld);
                                                        if (isRemover)
                                                        {
                                                            dataDb.Status = (int)status;
                                                            dataDb.ModifiedDate = DateTime.Now;
                                                            dataDb.ModifiedBy = userId.ToString();

                                                            newsAll = Mapper<NewsAll>.MapNewsAll(dataDb, new NewsAll());

                                                            resultUpdate = await _context.MultiAsync((transac) =>
                                                            {
                                                                transac.HashSetAsync(_context.NameOf(dataDb.GetType().Name), dataDb.Id, Json.Stringify(dataDb));

                                                                transac.HashSetAsync(_context.NameOf(newsAll.GetType().Name), newsAll.Id, Json.Stringify(newsAll));

                                                                transac.SortedSetRemoveAsync(_context.NameOf(IndexKeys.KeyStatus<NewsAll>(statusOld)), newsAll.Id);
                                                                transac.SortedSetAddAsync(_context.NameOf(IndexKeys.KeyStatus<NewsAll>(newsAll.Status)), newsAll.Id, (dataDb.ModifiedDate ?? DateTime.Now).ConvertToTimestamp10());

                                                                if (officerId > 0)
                                                                {
                                                                    if (long.TryParse(dataDb.ApprovedBy, out long accountId))
                                                                    {
                                                                        transac.SortedSetRemoveAsync(_context.NameOf(IndexKeys.KeyApprovedInAccount<ShareLink>(officerId, accountId)), dataDb.Id);
                                                                    }
                                                                }
                                                            });

                                                            if (resultUpdate) returnValue = ErrorCodes.Success;
                                                        }
                                                        else
                                                        {
                                                            returnValue = ErrorCodes.PermissionUpdateInvalid;
                                                        }
                                                    }
                                                    else
                                                    {
                                                        returnValue = ErrorCodes.PermissionUpdateInvalid;
                                                    }
                                                }
                                                else
                                                {
                                                    returnValue = ErrorCodes.DataInvalid;
                                                }
                                            }
                                            else
                                            {
                                                returnValue = ErrorCodes.DataInvalid;
                                            }
                                        }
                                        else
                                        {
                                            returnValue = ErrorCodes.PermissionUpdateInvalid;
                                        }
                                    }

                                    break;
                                    #endregion
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
            }

            if(returnValue == ErrorCodes.Success && dataDb !=null && statusOld!= null)
            {
                actionAutoShare(Mapper<ShareLinkSearch>.Map(dataDb, new ShareLinkSearch()), statusOld ?? 0);
            }

            return returnValue;
        }

        public async Task<ErrorCodes> UpdateAsync(ShareLink data, long officerId)
        {
            var returnValue = ErrorCodes.UpdateError;
            try
            {
                using (var _context = GetContext())
                {
                    var approverOld = string.Empty;
                    var postDb = await _context.HashGetExAsync<ShareLink>(data.Id);

                    if (postDb != null)
                    {
                        var isModifier = await Functions.Function.IsModifier(officerId, data.Id, data.ModifiedBy, postDb.CreatedBy, postDb.Status, _context);

                        if (isModifier)
                        {
                            approverOld = postDb.ApprovedBy;

                            postDb.Url = data.Url;
                            postDb.OriginalUrl = data.OriginalUrl;
                            postDb.Author = data.Author;
                            postDb.CategoryId = data.CategoryId;
                            postDb.Location = data.Location;
                            postDb.PublishMode = data.PublishMode;
                            postDb.PublishData = data.PublishData;
                            postDb.DistributionDate = data.DistributionDate.HasValue && data.DistributionDate >= DateTime.Now ? data.DistributionDate : postDb.DistributionDate;
                            postDb.ModifiedDate = data.ModifiedDate;
                            postDb.ModifiedBy = data.ModifiedBy;
                            postDb.Tags = data.Tags;

                            postDb.Title = data.Title;
                            postDb.Source = data.Source;
                            postDb.Avatar = data.Avatar;

                            if (postDb.Status != (int)NewsStatus.Published)
                            {
                                postDb.ApprovedBy = data.ApprovedBy;
                            }
                            postDb.CommentMode = data.CommentMode;

                            var newsAll = Mapper<NewsAll>.MapNewsAll(postDb, new NewsAll());

                            var returnValueUpdate = await _context.MultiAsync((transac) =>
                            {
                                transac.HashSetAsync(_context.NameOf(postDb.GetType().Name), postDb.Id, Json.Stringify(postDb));
                                transac.HashSetAsync(_context.NameOf(newsAll.GetType().Name), newsAll.Id, Json.Stringify(newsAll));

                                if (postDb.Status == (int)NewsStatus.Approve)
                                {
                                    if (long.TryParse(approverOld, out long _approverOld))
                                        transac.SortedSetRemoveAsync(_context.NameOf(IndexKeys.KeyApproveInAccount<ShareLink>(officerId, _approverOld)), postDb.Id);

                                    if (!string.IsNullOrEmpty(postDb.ApprovedBy))
                                    {
                                        if (long.TryParse(postDb.ApprovedBy, out long accountId))
                                        {
                                            transac.SortedSetAddAsync(_context.NameOf(IndexKeys.KeyApproveInAccount<ShareLink>(officerId, accountId)), postDb.Id, DateTime.Now.ConvertToTimestamp10());
                                        }
                                    }
                                }
                            });

                            if (returnValueUpdate) returnValue = ErrorCodes.Success;
                        }
                        else
                        {
                            returnValue = ErrorCodes.PermissionUpdateInvalid;
                        }
                    }
                    else
                    {
                        returnValue = ErrorCodes.DataNotExist;
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
                returnValue = ErrorCodes.Exception;
            }
            return returnValue;
        }

        public async Task<ErrorCodes> UpdateV2Async(ShareLink data, long officerId, Action<int> actionAutoShare)
        {
            var returnValue = ErrorCodes.UpdateError;
            var statusOld = default(int?);            
            try
            {
                using (var _context = GetContext())
                {
                    var approverOld = string.Empty;
                    var postDb = await _context.HashGetExAsync<ShareLink>(data.Id, CommandFlags.PreferSlave);                    

                    if (postDb != null)
                    {
                        statusOld = postDb.Status;

                        var isModifier = await Functions.Function.IsModifier(officerId, data.Id, data.ModifiedBy, postDb.CreatedBy, postDb.Status, _context);

                        if (isModifier)
                        {
                            approverOld = postDb.ApprovedBy;

                            postDb.Url = data.Url;
                            postDb.OriginalUrl = data.OriginalUrl;
                            postDb.Author = data.Author;
                            postDb.CategoryId = data.CategoryId;
                            postDb.Location = data.Location;
                            postDb.PublishMode = data.PublishMode;
                            postDb.PublishData = data.PublishData;
                            postDb.DistributionDate = data.DistributionDate.HasValue && data.DistributionDate >= DateTime.Now ? data.DistributionDate : postDb.DistributionDate;
                            postDb.ModifiedDate = data.ModifiedDate;
                            postDb.ModifiedBy = data.ModifiedBy;
                            postDb.Tags = data.Tags;

                            postDb.Title = data.Title;
                            postDb.Source = data.Source;
                            postDb.Avatar = data.Avatar;

                            if (postDb.Status != (int)NewsStatus.Published)
                            {
                                postDb.ApprovedBy = data.ApprovedBy;
                            }
                            postDb.CommentMode = data.CommentMode;

                            var newsAll = Mapper<NewsAll>.MapNewsAll(postDb, new NewsAll());

                            var returnValueUpdate = await _context.MultiAsync((transac) =>
                            {
                                transac.HashSetAsync(_context.NameOf(postDb.GetType().Name), postDb.Id, Json.Stringify(postDb));
                                transac.HashSetAsync(_context.NameOf(newsAll.GetType().Name), newsAll.Id, Json.Stringify(newsAll));

                                if (postDb.Status == (int)NewsStatus.Approve)
                                {
                                    if (long.TryParse(approverOld, out long _approverOld))
                                        transac.SortedSetRemoveAsync(_context.NameOf(IndexKeys.KeyApproveInAccount<ShareLink>(officerId, _approverOld)), postDb.Id);

                                    if (!string.IsNullOrEmpty(postDb.ApprovedBy))
                                    {
                                        if (long.TryParse(postDb.ApprovedBy, out long accountId))
                                        {
                                            transac.SortedSetAddAsync(_context.NameOf(IndexKeys.KeyApproveInAccount<ShareLink>(officerId, accountId)), postDb.Id, DateTime.Now.ConvertToTimestamp10());
                                        }
                                    }
                                }
                            });

                            if (returnValueUpdate) returnValue = ErrorCodes.Success;
                        }
                        else
                        {
                            returnValue = ErrorCodes.PermissionUpdateInvalid;
                        }
                    }
                    else
                    {
                        returnValue = ErrorCodes.DataNotExist;
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
                returnValue = ErrorCodes.Exception;
            }
            if (returnValue == ErrorCodes.Success && statusOld != null)
            {
                actionAutoShare(statusOld ?? 0);
            }
            return returnValue;
        }

        public Task<List<ShareLink>> GetListByIdsAsync(List<string> ids)
        {
            var returnValue = Task.FromResult(default(List<ShareLink>));
            try
            {
                using (var _context = GetContext())
                {
                    returnValue = _context.GetsFromHashExAsync<ShareLink>(ids, CommandFlags.PreferSlave);
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
            }
            return returnValue;
        }

        public Task<PagingDataResult<ShareLinkSearchReturn>> SearchAsync(SearchNews data, long userId, Action<string> action)
        {
            var returnValue = Task.FromResult(default(PagingDataResult<ShareLinkSearchReturn>));
            try
            {
                using (var _context = GetContext())
                {
                    returnValue = _context.ExecAsync(async (db) =>
                    {
                        //var listMemberId = await db.SortedSetRangeByRankAsync(_context.NameOf(IndexKeys.KeyAccountMemberInOfficer<AccountMember>(data.OfficerId)), 0, -1, Order.Descending, CommandFlags.PreferSlave);
                        //var memberValid = listMemberId?.Where(p => p.ToString().Equals(userId.ToString()))?.Count() > 0;
                        var member = Json.Parse<AccountMember>(await db.HashGetAsync(_context.NameOf<AccountMember>(), data.OfficerId + IndexKeys.SeparateChar + userId, CommandFlags.PreferSlave));
                        if (member != null)
                        {
                            var result = new PagingDataResult<ShareLinkSearchReturn>
                            {
                                Data = new List<ShareLinkSearchReturn>()
                            };
                            try
                            {
                                var listKey = new RedisValue[] { };
                                var listPost = new List<ShareLink>();
                                var createdByKey = _context.NameOf(IndexKeys.KeyCreatedBy<NewsAll>(userId.ToString()));
                                var tempKeyCreatedBy = _context.NameOf("index:temp:createdby:" + Generator.SorterSetSearchId());
                                var tempKey = _context.NameOf("index:temp:union:" + Generator.SorterSetSearchId());
                                var total = default(long);
                                var listkeyFull = new List<RedisKey>();
                                var listCreatedBy = new List<RedisKey>();
                                var value4 = 0L;

                                if (data.Status.Equals(((int)NewsStatus.Published).ToString()) && member.Permissions != null && (member.Permissions.Contains(MemberPermission.ModifyPostOnPage) || member.Permissions.Contains(MemberPermission.RemovePostOnPage)))
                                {

                                }
                                else
                                {
                                    var listOwner = await _context.SortedSetRangeByRankAsync(IndexKeys.KeyOwnerOfficer(userId), 0, -1, Order.Descending, CommandFlags.PreferSlave);
                                    if (listOwner == null || !listOwner.Contains((RedisValue)data.OfficerId))
                                    {
                                        //listkeyFull.Add(createdByKey);
                                        listCreatedBy.Add(createdByKey);

                                        //Lấy cả những bài chỉ duyệt

                                        if (data.Status?.Split(",")?.Contains(((int)NewsStatus.Published).ToString()) ?? false)
                                        {
                                            listCreatedBy.Add(_context.NameOf(IndexKeys.KeyApprovedInAccount<ShareLink>(data.OfficerId, userId)));
                                        }

                                        value4 = await db.SortedSetCombineAndStoreAsync(SetOperation.Union
                                        , tempKeyCreatedBy, listCreatedBy.ToArray(), null, Aggregate.Max, CommandFlags.None);

                                        if (value4 > 0)
                                        {
                                            listkeyFull.Add(tempKeyCreatedBy);
                                        }
                                    }
                                    else
                                    {
                                        if ((data.Status?.Split(",")?.Contains(((int)NewsStatus.Approve).ToString()) ?? false) || (data.Status?.Split(",")?.Contains(((int)NewsStatus.Draft).ToString()) ?? false) || (data.Status?.Split(",")?.Contains(((int)NewsStatus.Remove).ToString()) ?? false))
                                        {
                                            listkeyFull.Add(createdByKey);
                                        }
                                    }
                                }

                                try
                                {
                                    listkeyFull.Add(_context.NameOf(IndexKeys.KeyNewsInAccount(data.OfficerId)));
                                    listkeyFull.Add(_context.NameOf(IndexKeys.KeyType<NewsAll>((int)NewsType.ShareLink)));
                                    switch (string.IsNullOrEmpty(data.Status))
                                    {
                                        case true:
                                            if (data.DistributionId == -1)
                                            {
                                                listkeyFull.Add(_context.NameOf(IndexKeys.KeyDistributorid<NewsDistribution>()));
                                            }
                                            else
                                            {
                                                if (data.DistributionId > 0)
                                                {
                                                    listkeyFull.Add(_context.NameOf(IndexKeys.KeyDistributorid<NewsDistribution>(data.DistributionId)));
                                                }
                                            }
                                            var value1 = await db.SortedSetCombineAndStoreAsync(SetOperation.Intersect
                                                , tempKey, listkeyFull.ToArray(), null, Aggregate.Max, CommandFlags.None);
                                            if (value1 > 0)
                                            {
                                                try
                                                {
                                                    listKey = (await db.SortedSetRangeByScoreAsync(tempKey
                                                    , Utility.ConvertToTimestamp10(data.FromDate ?? DateTime.MinValue)
                                                    , Utility.ConvertToTimestamp10(data.ToDate ?? DateTime.Now)
                                                    , Exclude.None
                                                    , data.OrderBy == SearchNewsOrderBy.AscCreatedDate ? Order.Ascending : Order.Descending
                                                    , ((data.PageIndex ?? 1) - 1) * (data.PageSize ?? 20)
                                                    , data.PageSize ?? 20
                                                    , CommandFlags.None));

                                                    total = await db.SortedSetLengthAsync(tempKey
                                                        , Utility.ConvertToTimestamp10(data.FromDate ?? DateTime.MinValue)
                                                        , Utility.ConvertToTimestamp10(data.ToDate ?? DateTime.Now)
                                                        , Exclude.None
                                                        , CommandFlags.None);

                                                    result.Total = (int)total;

                                                    listPost = (await db.HashGetAsync(_context.NameOf<ShareLink>(), listKey, CommandFlags.PreferSlave))?.Select(p => Json.Parse<ShareLink>(p))?.ToList();
                                                    if (listPost != null)
                                                    {
                                                        foreach (var post in listPost)
                                                        {
                                                            var postSearchReturn = Mapper<ShareLinkSearchReturn>.Map(post, new ShareLinkSearchReturn());
                                                            var listTmp = new List<NewsDistribution>(); //postSearchReturn.NewsDistribution = new NewsDistribution[] { };
                                                            try
                                                            {
                                                                var newsDistribution = await db.SortedSetRangeByScoreWithScoresAsync(_context.NameOf(IndexKeys.KeyNewsDistribution(post.Id)), double.MinValue, double.MaxValue, Exclude.None, Order.Ascending, 0, -1, CommandFlags.PreferSlave);
                                                                if (newsDistribution != null)
                                                                {
                                                                    foreach (var item in newsDistribution)
                                                                    {
                                                                        if (long.TryParse(item.Element.ToString()?.Split(IndexKeys.SeparateChar)?.FirstOrDefault(), out long distributorId))
                                                                        {
                                                                            listTmp.Add(new NewsDistribution
                                                                            {
                                                                                DistributorId = distributorId,
                                                                                SharedDate = Utility.ConvertSecondsToDate(item.Score),
                                                                                DistributorName = Json.Parse<Distributor>(await db.HashGetAsync(_context.NameOf<Distributor>(), distributorId, CommandFlags.PreferSlave))?.Name
                                                                            });
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                            catch (Exception exx)
                                                            {
                                                                Logger.Error(exx, exx.Message);
                                                            }
                                                            postSearchReturn.NewsDistribution = listTmp.ToArray();
                                                            result.Data.Add(postSearchReturn);
                                                        }
                                                    }
                                                }
                                                catch (Exception ex)
                                                {
                                                    Logger.Error(ex, ex.Message);
                                                }
                                                action(tempKey);
                                            }
                                            break;
                                        case false:
                                            if (data.DistributionId == -1)
                                            {
                                                listkeyFull.Add(_context.NameOf(IndexKeys.KeyDistributorid<NewsDistribution>()));
                                            }
                                            else
                                            {
                                                if (data.DistributionId > 0)
                                                {
                                                    listkeyFull.Add(_context.NameOf(IndexKeys.KeyDistributorid<NewsDistribution>(data.DistributionId)));
                                                }
                                            }

                                            var listRedisKeyStatus = new List<RedisKey>();
                                            var listStrStatus = data.Status.Split(",");
                                            foreach (var s in listStrStatus)
                                            {
                                                if (int.TryParse(s, out int status))
                                                {
                                                    if (Enum.IsDefined(typeof(NewsStatus), status))
                                                    {
                                                        listRedisKeyStatus.Add(_context.NameOf(IndexKeys.KeyStatus<NewsAll>(status)));
                                                    }
                                                }
                                            }
                                            var tempKey2 = _context.NameOf("index:temp:" + Generator.SorterSetSearchId());
                                            var value2 = await db.SortedSetCombineAndStoreAsync(SetOperation.Union
                                                    , tempKey2, listRedisKeyStatus.ToArray(), null, Aggregate.Max, CommandFlags.None);
                                            if (value2 > 0)
                                            {
                                                try
                                                {
                                                    listkeyFull.Add(tempKey2);
                                                    var value3 = await db.SortedSetCombineAndStoreAsync(SetOperation.Intersect
                                                        , tempKey, listkeyFull.ToArray(), null, Aggregate.Max, CommandFlags.None);
                                                    if (value3 > 0)
                                                    {
                                                        try
                                                        {
                                                            total = await db.SortedSetLengthAsync(tempKey
                                                            , Utility.ConvertToTimestamp10(data.FromDate ?? DateTime.MinValue)
                                                            , Utility.ConvertToTimestamp10(data.ToDate ?? DateTime.Now)
                                                            , Exclude.None
                                                            , CommandFlags.None);

                                                            result.Total = (int)total;

                                                            listKey = (await db.SortedSetRangeByScoreAsync(tempKey
                                                            , Utility.ConvertToTimestamp10(data.FromDate ?? DateTime.MinValue)
                                                            , Utility.ConvertToTimestamp10(data.ToDate ?? DateTime.Now)
                                                            , Exclude.None
                                                            , data.OrderBy == SearchNewsOrderBy.AscCreatedDate ? Order.Ascending : Order.Descending
                                                            , ((data.PageIndex ?? 1) - 1) * (data.PageSize ?? 20)
                                                            , data.PageSize ?? 20
                                                            , CommandFlags.None));

                                                            listPost = (await db.HashGetAsync(_context.NameOf<ShareLink>(), listKey, CommandFlags.PreferSlave))?.Select(p => Json.Parse<ShareLink>(p))?.ToList();
                                                            if (listPost != null)
                                                            {
                                                                foreach (var post in listPost)
                                                                {
                                                                    var postSearchReturn = Mapper<ShareLinkSearchReturn>.Map(post, new ShareLinkSearchReturn());
                                                                    var listTmp = new List<NewsDistribution>();//postSearchReturn.NewsDistribution = new NewsDistribution[] { };
                                                                    try
                                                                    {
                                                                        var newsDistribution = await db.SortedSetRangeByScoreWithScoresAsync(_context.NameOf(IndexKeys.KeyNewsDistribution(post.Id)), double.MinValue, double.MaxValue, Exclude.None, Order.Ascending, 0, -1, CommandFlags.PreferSlave);
                                                                        if (newsDistribution != null)
                                                                        {
                                                                            foreach (var item in newsDistribution)
                                                                            {
                                                                                if (long.TryParse(item.Element.ToString()?.Split(IndexKeys.SeparateChar)?.FirstOrDefault(), out long distributorId))
                                                                                {
                                                                                    listTmp.Add(new NewsDistribution
                                                                                    {
                                                                                        DistributorId = distributorId,
                                                                                        SharedDate = Utility.ConvertSecondsToDate(item.Score),
                                                                                        DistributorName = Json.Parse<Distributor>(await db.HashGetAsync(_context.NameOf<Distributor>(), distributorId, CommandFlags.PreferSlave))?.Name
                                                                                    });
                                                                                }
                                                                            }
                                                                        }
                                                                    }
                                                                    catch (Exception exx)
                                                                    {
                                                                        Logger.Error(exx, exx.Message);
                                                                    }
                                                                    postSearchReturn.NewsDistribution = listTmp.ToArray();
                                                                    result.Data.Add(postSearchReturn);
                                                                }
                                                            }
                                                        }
                                                        catch (Exception ex)
                                                        {
                                                            Logger.Error(ex, ex.Message);
                                                        }
                                                        action(tempKey);
                                                    }
                                                }
                                                catch (Exception ex)
                                                {
                                                    Logger.Error(ex, ex.Message);
                                                }
                                                action(tempKey2);
                                            }
                                            break;
                                    }
                                }
                                catch (Exception ex)
                                {
                                    Logger.Error(ex, ex.Message);
                                }

                                if (value4 > 0)
                                {
                                    action(tempKeyCreatedBy);
                                }
                            }
                            catch (Exception ex)
                            {
                                Logger.Error(ex, ex.Message);
                            }

                            return result;
                        }
                        else
                        {
                            return new PagingDataResult<ShareLinkSearchReturn>();
                        }
                    });
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
            }
            return returnValue;
        }

        public Task<PagingDataResult<ShareLink>> GetListApproveAsync(SearchNews data, long accountId, Action<string> action)
        {
            var returnValue = Task.FromResult(default(PagingDataResult<ShareLink>));
            try
            {
                using (var _context = GetContext())
                {
                    returnValue = _context.ExecAsync(async (db) =>
                    {
                        var listMemberId = await db.SortedSetRangeByRankAsync(_context.NameOf(IndexKeys.KeyAccountMemberInOfficer<AccountMember>(data.OfficerId)), 0, -1, Order.Descending, CommandFlags.PreferSlave);
                        var memberValid = listMemberId?.Where(p => p.ToString().Equals(accountId.ToString()))?.Count() > 0;

                        if(memberValid == true)
                        {
                            var result = new PagingDataResult<ShareLink>()
                            {
                                Data = new List<ShareLink>()
                            };
                            try
                            {
                                var listKey = new RedisValue[] { };
                                var listArticle = new List<ShareLink>();
                                var total = default(long);
                                var tempKey = _context.NameOf("index:temp:union:" + Generator.SorterSetSearchId());

                                var listkeyFull = new List<RedisKey>();
                                listkeyFull.Add(_context.NameOf(IndexKeys.KeyStatus<NewsAll>(int.Parse(data.Status))));

                                var keyApprove = new List<RedisKey>();
                                keyApprove.Add(_context.NameOf(IndexKeys.KeyApprove<ShareLink>(data.OfficerId)));
                                keyApprove.Add(_context.NameOf(IndexKeys.KeyApproveInAccount<ShareLink>(data.OfficerId, accountId)));

                                var tempKeyApprove = _context.NameOf("index:temp:approve:" + Generator.SorterSetSearchId());

                                var valueApprove = await db.SortedSetCombineAndStoreAsync(SetOperation.Union
                                            , tempKeyApprove, keyApprove.ToArray(), null, Aggregate.Max, CommandFlags.None);

                                if (valueApprove > 0)
                                {
                                    listkeyFull.Add(tempKeyApprove);
                                    var value = await db.SortedSetCombineAndStoreAsync(SetOperation.Intersect
                                            , tempKey, listkeyFull.ToArray(), null, Aggregate.Max, CommandFlags.None);
                                    if (value > 0)
                                    {
                                        try
                                        {
                                            listKey = (await db.SortedSetRangeByScoreAsync(tempKey
                                            , (data.FromDate ?? DateTime.MinValue).ConvertToTimestamp10()
                                            , (data.ToDate ?? DateTime.Now).ConvertToTimestamp10()
                                            , Exclude.None
                                            , data.OrderBy == SearchNewsOrderBy.AscCreatedDate ? Order.Ascending : Order.Descending
                                            , ((data.PageIndex ?? 1) - 1) * (data.PageSize ?? 20)
                                            , data.PageSize ?? 20
                                            , CommandFlags.None));

                                            total = await db.SortedSetLengthAsync(tempKey
                                                , (data.FromDate ?? DateTime.MinValue).ConvertToTimestamp10()
                                                , (data.ToDate ?? DateTime.Now).ConvertToTimestamp10()
                                                , Exclude.None
                                                , CommandFlags.None);

                                            result.Total = (int)total;
                                            listArticle = listKey != null && listKey.Length > 0 ? (await db.HashGetAsync(_context.NameOf(typeof(ShareLink).Name), listKey, CommandFlags.PreferSlave))?.Select(p => Json.Parse<ShareLink>(p))?.ToList() : listArticle;
                                            if (listArticle != null)
                                            {
                                                //foreach (var article in listArticle)
                                                //{
                                                //    var articleSearchReturn = Mapper<ArticleSearchReturn>.Map(article, new ArticleSearchReturn());
                                                //    var listTmp = new List<NewsDistribution>();
                                                //    try
                                                //    {
                                                //        var newsDistribution = await db.SortedSetRangeByScoreWithScoresAsync(_context.NameOf(IndexKeys.KeyNewsDistribution(article.Id)), double.MinValue, double.MaxValue, Exclude.None, Order.Ascending, 0, -1, CommandFlags.PreferSlave);
                                                //        if (newsDistribution != null)
                                                //        {
                                                //            foreach (var item in newsDistribution)
                                                //            {
                                                //                if (long.TryParse(item.Element.ToString()?.Split(IndexKeys.SeparateChar)?.FirstOrDefault(), out long distributorId))
                                                //                {
                                                //                    listTmp.Add(new NewsDistribution
                                                //                    {
                                                //                        DistributorId = distributorId,
                                                //                        SharedDate = Utility.ConvertSecondsToDate(item.Score),
                                                //                        DistributorName = Json.Parse<Distributor>(await db.HashGetAsync(_context.NameOf(typeof(Distributor).Name), distributorId, CommandFlags.PreferSlave))?.Name
                                                //                    });
                                                //                }
                                                //            }
                                                //        }
                                                //    }
                                                //    catch (Exception exx)
                                                //    {
                                                //        Logger.Error(exx, exx.Message);
                                                //    }
                                                //    articleSearchReturn.NewsDistribution = listTmp.ToArray();
                                                //    result.Data.Add(articleSearchReturn);
                                                //}

                                                result.Data.AddRange(listArticle);
                                            }
                                        }
                                        catch (Exception ex)
                                        {
                                            Logger.Error(ex, ex.Message);
                                        }
                                        action(tempKey);
                                    }
                                    action(tempKeyApprove);
                                }
                            }
                            catch (Exception ex)
                            {
                                Logger.Error(ex, ex.Message);
                            }

                            return result;
                        }
                        else
                        {
                            return new PagingDataResult<ShareLink>();
                        }
                    });
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
            }
            return returnValue;
        }

        public async Task<RedisValue[]> GetAllAsync()
        {
            var returnValue = default(RedisValue[]);
            try
            {
                using (var _context = GetContext())
                {
                    returnValue = await _context.ExecAsync(async (db) =>
                    {
                        var tempKey = _context.NameOf("index:temp:union:" + Generator.SorterSetSearchId());
                        var value = await db.SortedSetCombineAndStoreAsync(SetOperation.Intersect
                                        , tempKey, _context.NameOf(IndexKeys.KeyStatus<NewsAll>((int)NewsStatus.Published)), _context.NameOf(IndexKeys.KeyType<NewsAll>((int)NewsType.ShareLink)));
                       
                        returnValue = await db.SortedSetRangeByRankAsync(tempKey, 0, -1);
                        await db.KeyDeleteAsync(tempKey);
                        return returnValue;
                    });
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
            }
            return returnValue;
        }
    }
}
