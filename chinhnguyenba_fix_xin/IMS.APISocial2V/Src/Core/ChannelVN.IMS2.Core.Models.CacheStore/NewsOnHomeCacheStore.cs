﻿using ChannelVN.IMS2.Core.Entities;
using ChannelVN.IMS2.Core.Entities.NewsOnHome;
using ChannelVN.IMS2.Foundation.Common;
using ChannelVN.IMS2.Foundation.Common.Configuration;
using ChannelVN.IMS2.Foundation.Data.Providers;
using ChannelVN.IMS2.Foundation.Logging;
using StackExchange.Redis;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ChannelVN.IMS2.Core.Models.CacheStore
{
    public class NewsOnHomeCacheStore : CmsMainCacheStore
    {
        public Task<bool> AddAsync(NewsOnHome data)
        {
            var returnValue = Task.FromResult(false);
            try
            {                
                using (var _context = GetContext())
                {
                    returnValue = _context.MultiAsync(async (transac) =>
                    {
                        await transac.HashSetAsync(_context.NameOf<NewsOnHome>(), data.NewsId, Json.Stringify(data));                        
                    });
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
            }
            return returnValue;
        }
        public Task<bool> UpdateAsync(NewsOnHome data)
        {
            var returnValue = Task.FromResult(false);
            try
            {
                using (var _context = GetContext())
                {
                    returnValue = _context.MultiAsync((transac) =>
                    {
                        transac.HashSetAsync(_context.NameOf<NewsOnHome>(), data.NewsId, Json.Stringify(data));
                    });
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
            }
            return returnValue;
        }        
        public Task<NewsOnHome> GetByIdAsync(long id)
        {
            var returnValue = Task.FromResult(default(NewsOnHome));
            try
            {
                using (var _context = GetContext())
                {
                    returnValue = _context.HashGetExAsync<NewsOnHome>(id, CommandFlags.PreferSlave);
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
            }
            return returnValue;
        }

        public async Task<bool> ExistListSearchCacheAsync()
        {
            var returnValue = false;
            try
            {
                using (var _context = GetContext())
                {
                    var returnCheck = await _context.KeyExistsAsync(nameof(ListSearchNewsOnHomeCache));
                    if (returnCheck)
                    {
                        returnValue = true;
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
            }
            return returnValue;
        }

        public async Task<List<NewsOnHome>> GetListSearchFromCacheAsync()
        {
            var returnValue = default(List<NewsOnHome>);
            try
            {
                using (var _context = GetContext())
                {
                    returnValue = await _context.HashGetAllExxAsync<NewsOnHome>(nameof(ListSearchNewsOnHomeCache));
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
            }
            return returnValue;
        }

        public async Task<bool> PushListSearchToCacheAsync(List<NewsOnHome> data)
        {
            var returnValue = false;
            try
            {
                var hashFields = new List<HashEntry>();
                foreach (var item in data)
                {
                    hashFields.Add(new HashEntry(item.Id.ToString(), Json.Stringify(item)));
                }
                using (var _context = GetContext())
                {
                    var returnPush = await _context.MultiAsync(transac =>
                    {
                        
                        transac.HashSetAsync(_context.NameOf<ListSearchNewsOnHomeCache>(), hashFields.ToArray());
                        transac.KeyExpireAsync(_context.NameOf<ListSearchNewsOnHomeCache>(),
                           DateTime.Now.AddDays(AppSettings.Current.JobWarningLateApproveSetting.TimeSpacingSearch),
                           CommandFlags.FireAndForget);
                    });
                    if (returnPush)
                    {
                        returnValue = true;
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
            }
            return returnValue;
        }

        public NewsOnHome PopQueueJobNewsOnHome()
        {
            var popValue = default(NewsOnHome);
            try
            {
                using (var _context = GetContext())
                {

                    var returnValue = _context.ListLeftPop(nameof(ListQueueJobNewsOnHome));
                    if (returnValue.HasValue)
                    {
                        popValue = Json.Parse<NewsOnHome>(returnValue.ToString());
                        Console.WriteLine($"Pop Queue: {nameof(ListQueueJobNewsOnHome)}, {popValue.Id}");
                    }
                    return popValue;
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
                return null;
            }
        }

        public bool CheckExistQueueJobNewsOnHome()
        {
            var returnValue = true;
            try
            {
                using (var _context = GetContext())
                {
                    var redisValues = _context.ListRange(nameof(ListQueueJobNewsOnHome));
                    if (redisValues.Count() == 0)
                        returnValue = false;
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
            }
            return returnValue;
        }

        public Task<bool> PushQueueJobNewsOnHome(IEnumerable<NewsOnHome> listToScans)
        {
            var returnValue = Task.FromResult(false);
            try
            {
                using (var _context = GetContext())
                {
                    foreach (var item in listToScans)
                    {
                        _context.ListRightPush(nameof(ListQueueJobNewsOnHome), Json.Stringify(item));
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
            }
            return returnValue;
        }
    }
}
