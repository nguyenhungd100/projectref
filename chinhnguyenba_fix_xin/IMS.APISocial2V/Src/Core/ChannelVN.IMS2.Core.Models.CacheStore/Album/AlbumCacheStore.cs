﻿using ChannelVN.IMS2.Core.Entities.Distribution;
using ChannelVN.IMS2.Core.Entities.Album;
using ChannelVN.IMS2.Foundation.Common;
using ChannelVN.IMS2.Foundation.Logging;
using StackExchange.Redis;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ChannelVN.IMS2.Core.Entities;
using ChannelVN.IMS2.Core.Entities.Security;
using static ChannelVN.IMS2.Core.Entities.ErrorCode.ErrorMapping;

namespace ChannelVN.IMS2.Core.Models.CacheStore.Album
{
    public class AlbumCacheStore : CmsMainCacheStore
    {
        public Task<List<Entities.Album.Album>> GetAllAsync()
        {
            var returnValue = Task.FromResult(default(List<Entities.Album.Album>));
            try
            {
                using (var _context = GetContext())
                {
                    returnValue = _context.HashGetAllExxAsync<Entities.Album.Album>(CommandFlags.PreferSlave);
                }
            }catch(Exception ex)
            {
                Logger.Error(ex, ex.Message);
            }
            return returnValue;
        }
        public async Task<ErrorCodes> AddAsync(AlbumSearch dataSearch)
        {
            var returnValue = ErrorCodes.AddError;
            try
            {
                var data = Mapper<Entities.Album.Album>.Map(dataSearch, new Entities.Album.Album());
                var newsAll = Mapper<NewsAll>.MapNewsAll(data, new NewsAll());
                using (var _context = GetContext())
                {
                    //Kiểm tra quyền viết bài
                    var isWriter = await Functions.Function.IsWriter(dataSearch?.NewsInAccount?.FirstOrDefault().AccountId, data.CreatedBy, _context);

                    //Neu có quyền viết bài
                    if (isWriter)
                    {
                        var result = await _context.MultiAsync((transac) =>
                        {
                            transac.HashSetAsync(_context.NameOf(data.GetType().Name), data.Id, Json.Stringify(data));

                            if (dataSearch.PhotoInAlbum != null && dataSearch.PhotoInAlbum.Count() > 0)
                            {
                                transac.HashSetAsync(_context.NameOf(dataSearch.PhotoInAlbum.FirstOrDefault().GetType().Name), dataSearch.PhotoInAlbum.Select(p => new HashEntry(p.AlbumId + IndexKeys.SeparateChar + p.PhotoId, Json.Stringify(p)))?.ToArray());

                                transac.SortedSetAddAsync(_context.NameOf(IndexKeys.KeyPhotoInAlbum<Entities.Album.PhotoInAlbum>(data.Id)), dataSearch.PhotoInAlbum.Select(p => new SortedSetEntry(p.PhotoId, (p.PublishedDate ?? DateTime.Now).ConvertToTimestamp10())).ToArray());
                            }

                            transac.HashSetAsync(_context.NameOf(newsAll.GetType().Name), newsAll.Id, Json.Stringify(newsAll));

                            transac.SortedSetAddAsync(_context.NameOf(IndexKeys.KeyCreatedBy<NewsAll>(newsAll.CreatedBy)), newsAll.Id, (newsAll.CreatedDate ?? DateTime.Now).ConvertToTimestamp10());

                            transac.SortedSetAddAsync(_context.NameOf(IndexKeys.KeyStatus<NewsAll>(newsAll.Status)), newsAll.Id, (newsAll.CreatedDate ?? DateTime.Now).ConvertToTimestamp10());

                            transac.SortedSetAddAsync(_context.NameOf(IndexKeys.KeyDistributorid<NewsAll>(newsAll.DistributorId)), data.Id, (newsAll.CreatedDate ?? DateTime.Now).ConvertToTimestamp10());

                            transac.SortedSetAddAsync(_context.NameOf(IndexKeys.KeyType<NewsAll>(newsAll.Type ?? 8)), newsAll.Id, (newsAll.CreatedDate ?? DateTime.Now).ConvertToTimestamp10());

                            transac.HashSetAsync(_context.NameOf(typeof(NewsInAccount).Name), dataSearch.NewsInAccount?.Select(p => new HashEntry(p.AccountId + IndexKeys.SeparateChar + p.NewsId, Json.Stringify(p))).ToArray());

                            transac.SetAddAsync(_context.NameOf(IndexKeys.KeyAccountInNews(dataSearch.Id)), dataSearch.NewsInAccount?.LastOrDefault()?.AccountId ?? 0);

                            transac.SetAddAsync(_context.NameOf(IndexKeys.KeyNewsInAccount(dataSearch.NewsInAccount?.LastOrDefault()?.AccountId)), dataSearch.Id);

                            transac.SortedSetAddAsync(_context.NameOf(IndexKeys.KeyPublishedDate<NewsInAccount>())
                                , (dataSearch.NewsInAccount?.LastOrDefault()?.AccountId ?? 0) + IndexKeys.SeparateChar + dataSearch.Id
                                , (dataSearch.CreatedDate ?? DateTime.Now).ConvertToTimestamp10());
                        });
                        if (result)
                        {
                            returnValue = ErrorCodes.Success;
                        }
                    }
                    else
                    {
                        returnValue = ErrorCodes.PermissionInvalid;
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
                returnValue = ErrorCodes.Exception;
            }
            return returnValue;
        }

        public async Task<ErrorCodes> UpdateAsync(AlbumSearch dataSearch, long OfficerId = 0)
        {
            var returnValue = ErrorCodes.UpdateError;
            try
            {
                using (var _context = GetContext())
                {
                    var approverOld = string.Empty;
                    var dataDb = await _context.HashGetExAsync<Entities.Album.Album>(dataSearch.Id, CommandFlags.PreferSlave);
                    if (dataDb != null)
                    {
                        var isModifier = await Functions.Function.IsModifier(OfficerId, dataSearch.Id, dataSearch.ModifiedBy, dataDb.CreatedBy, dataDb.Status, _context);

                        if (isModifier)
                        {
                            var listPhotoOld = new PhotoInAlbum[] { };

                            approverOld = dataDb.ApprovedBy;

                            dataDb.PublishData = dataSearch.PublishData;
                            dataDb.DistributionDate = dataSearch.DistributionDate.HasValue && dataSearch.DistributionDate >= DateTime.Now ? dataSearch.DistributionDate : dataDb.DistributionDate;
                            dataDb.ModifiedDate = dataSearch.ModifiedDate;
                            dataDb.ModifiedBy = dataSearch.ModifiedBy;
                            dataDb.Tags = dataSearch.Tags;
                            dataDb.CategoryId = dataSearch.CategoryId;
                            dataDb.Name = dataSearch.Name;
                            dataDb.Location = dataSearch.Location;
                            dataDb.Author = dataSearch.Author;
                            dataDb.UnsignName = dataSearch.UnsignName;
                            dataDb.Description = dataSearch.Description;
                            dataDb.TemplateId = dataSearch.TemplateId ?? dataDb.TemplateId;
                            dataDb.AlbumRelation = dataSearch.AlbumRelation;
                            dataDb.MetaAvatar = dataSearch.MetaAvatar;
                            dataDb.MetaData = dataSearch.MetaData;

                            if (dataDb.Status != (int)NewsStatus.Published)
                            {
                                dataDb.ApprovedBy = dataSearch.ApprovedBy;
                            }
                            dataDb.CommentMode = dataSearch.CommentMode;

                            var listKeyPhoto = await _context.SortedSetRangeByRankAsync(IndexKeys.KeyPhotoInAlbum<PhotoInAlbum>(dataSearch.Id), 0, -1, Order.Descending, CommandFlags.PreferSlave);

                            listPhotoOld = (await _context.GetsFromHashExAsync<PhotoInAlbum>(typeof
                                (PhotoInAlbum).Name, listKeyPhoto?.Select(p => (RedisValue)(dataSearch.Id + IndexKeys.SeparateChar + p.ToString()))?.ToList())).ToArray();

                            if (dataSearch.PhotoInAlbum != null)
                            {
                                var priority = 0;
                                foreach (var photo in dataSearch.PhotoInAlbum)
                                {
                                    if (listPhotoOld == null || listPhotoOld.Where(p => p.PhotoId == photo.PhotoId) == null || !(listPhotoOld.Where(p => p.PhotoId == photo.PhotoId).Count() > 0))
                                    {
                                        photo.AlbumId = dataDb.Id;
                                        photo.PublishedDate = DateTime.Now;
                                    }
                                    photo.Priority = priority++;
                                }
                            }

                            var newsAll = Mapper<NewsAll>.MapNewsAll(dataDb, new NewsAll());

                            var resultUpdate = await _context.MultiAsync((transac) =>
                            {
                                transac.HashSetAsync(_context.NameOf(dataDb.GetType().Name), dataDb.Id, Json.Stringify(dataDb));
                                transac.HashSetAsync(_context.NameOf(newsAll.GetType().Name), newsAll.Id, Json.Stringify(newsAll));
                                //Xoa du lieu cu
                                if (listPhotoOld != null && listPhotoOld.Count() > 0)
                                {
                                    transac.HashDeleteAsync(_context.NameOf(listPhotoOld.FirstOrDefault().GetType().Name), listPhotoOld.Select(p => (RedisValue)(p.AlbumId + IndexKeys.SeparateChar + p.PhotoId)).ToArray());
                                    transac.SortedSetRemoveAsync(_context.NameOf(IndexKeys.KeyPhotoInAlbum<PhotoInAlbum>(dataSearch.Id)), listPhotoOld.Select(p => (RedisValue)p.PhotoId)?.ToArray());
                                }
                                //add du lieu moi
                                if (dataSearch.PhotoInAlbum?.Count() > 0)
                                {
                                    transac.HashSetAsync(_context.NameOf(dataSearch.PhotoInAlbum.FirstOrDefault().GetType().Name), dataSearch.PhotoInAlbum.Select(p => new HashEntry(p.AlbumId + IndexKeys.SeparateChar + p.PhotoId, Json.Stringify(p)))?.ToArray());
                                    transac.SortedSetAddAsync(_context.NameOf(IndexKeys.KeyPhotoInAlbum<PhotoInAlbum>(dataSearch.Id)), dataSearch.PhotoInAlbum.Select(p => new SortedSetEntry(p.PhotoId, (p.PublishedDate ?? DateTime.Now).ConvertToTimestamp10())).ToArray());
                                }

                                if (dataDb.Status == (int)NewsStatus.Approve)
                                {
                                    if (long.TryParse(approverOld, out long _approverOld))
                                        transac.SortedSetRemoveAsync(_context.NameOf(IndexKeys.KeyApproveInAccount<Entities.Album.Album>(OfficerId, _approverOld)), dataDb.Id);

                                    if (!string.IsNullOrEmpty(dataDb.ApprovedBy))
                                    {
                                        if (long.TryParse(dataDb.ApprovedBy, out long accountId))
                                        {
                                            transac.SortedSetAddAsync(_context.NameOf(IndexKeys.KeyApproveInAccount<Entities.Album.Album>(OfficerId, accountId)), dataDb.Id, DateTime.Now.ConvertToTimestamp10());
                                        }
                                    }
                                }

                            });
                            if (resultUpdate) returnValue = ErrorCodes.Success;
                        }
                        else
                        {
                            returnValue = ErrorCodes.PermissionUpdateInvalid;
                        }
                    }
                    else
                    {
                        returnValue = ErrorCodes.DataNotExist;
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
                returnValue = ErrorCodes.Exception;
            }
            return returnValue;
        }

        public async Task<ErrorCodes> UpdateV2Async(AlbumSearch dataSearch, long OfficerId, Action<int> actionAutoShare)
        {
            var returnValue = ErrorCodes.UpdateError;
            var statusOld = default(int?);
            try
            {
                using (var _context = GetContext())
                {
                    var approverOld = string.Empty;
                    var dataDb = await _context.HashGetExAsync<Entities.Album.Album>(dataSearch.Id, CommandFlags.PreferSlave);
                    statusOld = dataDb.Status;

                    if (dataDb != null)
                    {
                        var isModifier = await Functions.Function.IsModifier(OfficerId, dataSearch.Id, dataSearch.ModifiedBy, dataDb.CreatedBy, dataDb.Status, _context);

                        if (isModifier)
                        {
                            var listPhotoOld = new PhotoInAlbum[] { };

                            approverOld = dataDb.ApprovedBy;

                            dataDb.PublishData = dataSearch.PublishData;
                            dataDb.DistributionDate = dataSearch.DistributionDate.HasValue && dataSearch.DistributionDate >= DateTime.Now ? dataSearch.DistributionDate : dataDb.DistributionDate;
                            dataDb.ModifiedDate = dataSearch.ModifiedDate;
                            dataDb.ModifiedBy = dataSearch.ModifiedBy;
                            dataDb.Tags = dataSearch.Tags;
                            dataDb.CategoryId = dataSearch.CategoryId;
                            dataDb.Name = dataSearch.Name;
                            dataDb.Location = dataSearch.Location;
                            dataDb.Author = dataSearch.Author;
                            dataDb.UnsignName = dataSearch.UnsignName;
                            dataDb.Description = dataSearch.Description;
                            dataDb.TemplateId = dataSearch.TemplateId ?? dataDb.TemplateId;
                            dataDb.AlbumRelation = dataSearch.AlbumRelation;
                            dataDb.MetaAvatar = dataSearch.MetaAvatar;
                            dataDb.MetaData = dataSearch.MetaData;

                            if (dataDb.Status != (int)NewsStatus.Published)
                            {
                                dataDb.ApprovedBy = dataSearch.ApprovedBy;
                            }
                            dataDb.CommentMode = dataSearch.CommentMode;

                            var listKeyPhoto = await _context.SortedSetRangeByRankAsync(IndexKeys.KeyPhotoInAlbum<PhotoInAlbum>(dataSearch.Id), 0, -1, Order.Descending, CommandFlags.PreferSlave);

                            listPhotoOld = (await _context.GetsFromHashExAsync<PhotoInAlbum>(typeof
                                (PhotoInAlbum).Name, listKeyPhoto?.Select(p => (RedisValue)(dataSearch.Id + IndexKeys.SeparateChar + p.ToString()))?.ToList())).ToArray();

                            if (dataSearch.PhotoInAlbum != null)
                            {
                                var priority = 0;
                                foreach (var photo in dataSearch.PhotoInAlbum)
                                {
                                    if (listPhotoOld == null || listPhotoOld.Where(p => p.PhotoId == photo.PhotoId) == null || !(listPhotoOld.Where(p => p.PhotoId == photo.PhotoId).Count() > 0))
                                    {
                                        photo.AlbumId = dataDb.Id;
                                        photo.PublishedDate = DateTime.Now;
                                    }
                                    photo.Priority = priority++;
                                }
                            }

                            var newsAll = Mapper<NewsAll>.MapNewsAll(dataDb, new NewsAll());

                            var resultUpdate = await _context.MultiAsync((transac) =>
                            {
                                transac.HashSetAsync(_context.NameOf(dataDb.GetType().Name), dataDb.Id, Json.Stringify(dataDb));
                                transac.HashSetAsync(_context.NameOf(newsAll.GetType().Name), newsAll.Id, Json.Stringify(newsAll));
                                //Xoa du lieu cu
                                if (listPhotoOld != null && listPhotoOld.Count() > 0)
                                {
                                    transac.HashDeleteAsync(_context.NameOf(listPhotoOld.FirstOrDefault().GetType().Name), listPhotoOld.Select(p => (RedisValue)(p.AlbumId + IndexKeys.SeparateChar + p.PhotoId)).ToArray());
                                    transac.SortedSetRemoveAsync(_context.NameOf(IndexKeys.KeyPhotoInAlbum<PhotoInAlbum>(dataSearch.Id)), listPhotoOld.Select(p => (RedisValue)p.PhotoId)?.ToArray());
                                }
                                //add du lieu moi
                                if (dataSearch.PhotoInAlbum?.Count() > 0)
                                {
                                    transac.HashSetAsync(_context.NameOf(dataSearch.PhotoInAlbum.FirstOrDefault().GetType().Name), dataSearch.PhotoInAlbum.Select(p => new HashEntry(p.AlbumId + IndexKeys.SeparateChar + p.PhotoId, Json.Stringify(p)))?.ToArray());
                                    transac.SortedSetAddAsync(_context.NameOf(IndexKeys.KeyPhotoInAlbum<PhotoInAlbum>(dataSearch.Id)), dataSearch.PhotoInAlbum.Select(p => new SortedSetEntry(p.PhotoId, (p.PublishedDate ?? DateTime.Now).ConvertToTimestamp10())).ToArray());
                                }

                                if (dataDb.Status == (int)NewsStatus.Approve)
                                {
                                    if (long.TryParse(approverOld, out long _approverOld))
                                        transac.SortedSetRemoveAsync(_context.NameOf(IndexKeys.KeyApproveInAccount<Entities.Album.Album>(OfficerId, _approverOld)), dataDb.Id);

                                    if (!string.IsNullOrEmpty(dataDb.ApprovedBy))
                                    {
                                        if (long.TryParse(dataDb.ApprovedBy, out long accountId))
                                        {
                                            transac.SortedSetAddAsync(_context.NameOf(IndexKeys.KeyApproveInAccount<Entities.Album.Album>(OfficerId, accountId)), dataDb.Id, DateTime.Now.ConvertToTimestamp10());
                                        }
                                    }
                                }

                            });
                            if (resultUpdate) returnValue = ErrorCodes.Success;
                        }
                        else
                        {
                            returnValue = ErrorCodes.PermissionUpdateInvalid;
                        }
                    }
                    else
                    {
                        returnValue = ErrorCodes.DataNotExist;
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
                returnValue = ErrorCodes.Exception;
            }

            if (returnValue == ErrorCodes.Success && statusOld != null)
            {
                actionAutoShare(statusOld ?? 0);
            }

            return returnValue;
        }

        public async Task<ErrorCodes> UpdateStatusV2Async(long id, NewsStatus status, long userId, Action<AlbumSearch, int> actionAutoShare)
        {
            var returnValue = ErrorCodes.UpdateError;
            var statusOld = default(int?);
            var dataDb = default(Entities.Album.Album);
            try
            {
                using (var _context = GetContext())
                {
                    var newsAll = default(NewsAll);
                    var resultUpdate = false;
                    var officerIds = default(RedisValue[]);
                    var officerId = 0L;

                    dataDb = await _context.HashGetExAsync<Entities.Album.Album>(id, CommandFlags.PreferSlave);                    
                    if (dataDb != null)
                    {
                        statusOld = dataDb.Status;
                        if (dataDb.Status == (int)status)
                        {
                            returnValue = ErrorCodes.Success;
                        }
                        else
                        {
                            switch (status)
                            {
                                #region Xuat ban
                                case NewsStatus.Published:// xuất bản
                                    officerIds = await _context.SetMembersAsync(IndexKeys.KeyAccountInNews(id), CommandFlags.PreferSlave);
                                    if (officerIds != null && officerIds.Count() != 0)
                                    {
                                        if (long.TryParse(officerIds[0], out officerId))
                                        {
                                            var officer = await _context.HashGetExAsync<Account>(officerId, CommandFlags.PreferSlave);

                                            var listMemberId = await _context.SortedSetRangeByScoreAsync(IndexKeys.KeyAccountMemberInOfficer<AccountMember>(officerId), double.NegativeInfinity, double.PositiveInfinity);

                                            var listMember = await _context.GetsFromHashExAsync<AccountMember>(listMemberId?.Select(p => officerId + IndexKeys.SeparateChar + p.ToString())?.ToList(), CommandFlags.PreferSlave);
                                            var accountMember = listMember?.Where(p => p.MemberId == userId).FirstOrDefault();

                                            if (accountMember != null && officer != null)
                                            {
                                                var isPublisher = await Functions.Function.IsPublisher(accountMember, dataDb.CreatedBy, dataDb.ApprovedBy, officer.Mode, dataDb.Status);
                                                if (isPublisher)
                                                {
                                                    statusOld = dataDb.Status;
                                                    dataDb.Status = (int)status;
                                                    dataDb.ModifiedDate = DateTime.Now;
                                                    dataDb.ModifiedBy = userId.ToString();
                                                    dataDb.DistributionDate = dataDb.DistributionDate > DateTime.Now ? dataDb.DistributionDate : DateTime.Now;

                                                    var oldApprovedBy = dataDb.ApprovedBy;
                                                    dataDb.ApprovedBy = accountMember.MemberId.ToString();
                                                    dataDb.ApprovedDate = DateTime.Now;

                                                    newsAll = Mapper<NewsAll>.MapNewsAll(dataDb, new NewsAll());

                                                    resultUpdate = await _context.MultiAsync((transac) =>
                                                    {
                                                        transac.HashSetAsync(_context.NameOf(dataDb.GetType().Name), dataDb.Id, Json.Stringify(dataDb));
                                                        transac.HashSetAsync(_context.NameOf(newsAll.GetType().Name), newsAll.Id, Json.Stringify(newsAll));

                                                        transac.SortedSetRemoveAsync(_context.NameOf(IndexKeys.KeyStatus<NewsAll>(statusOld)), newsAll.Id);
                                                        transac.SortedSetAddAsync(_context.NameOf(IndexKeys.KeyStatus<NewsAll>(newsAll.Status)), newsAll.Id, (newsAll.DistributionDate ?? DateTime.Now).ConvertToTimestamp10());

                                                        //remove value in set approve in account if exist
                                                        if (!string.IsNullOrEmpty(oldApprovedBy) && long.TryParse(oldApprovedBy, out long oldApprovedById))
                                                        {
                                                            transac.SortedSetRemoveAsync(_context.NameOf(IndexKeys.KeyApproveInAccount<Entities.Album.Album>(officerId, oldApprovedById)), dataDb.Id, CommandFlags.PreferSlave);
                                                        }
                                                        else
                                                        {
                                                            //remove value in set approve if exist
                                                            transac.SortedSetRemoveAsync(_context.NameOf(IndexKeys.KeyApprove<Entities.Album.Album>(officerId)), dataDb.Id, CommandFlags.PreferSlave);
                                                        }

                                                        //add value into key approved in account
                                                        transac.SortedSetAddAsync(_context.NameOf(IndexKeys.KeyApprovedInAccount<Entities.Album.Album>(officerId, userId)), dataDb.Id, DateTime.Now.ConvertToTimestamp10());
                                                    });

                                                    if (resultUpdate) returnValue = ErrorCodes.Success;
                                                }
                                                else
                                                {
                                                    returnValue = ErrorCodes.PermissionPublishInvalid;
                                                }
                                            }
                                            else
                                            {
                                                returnValue = ErrorCodes.PermissionPublishInvalid;
                                            }
                                        }
                                        else
                                        {
                                            returnValue = ErrorCodes.DataInvalid;
                                        }
                                    }
                                    else
                                    {
                                        returnValue = ErrorCodes.DataInvalid;
                                    }
                                    break;
                                #endregion
                                #region Xoa bai
                                case NewsStatus.Remove: // xóa bài
                                    if (statusOld != (int)NewsStatus.Published)
                                    {
                                        if (userId.ToString().Equals(dataDb.CreatedBy))
                                        {
                                            statusOld = dataDb.Status;
                                            dataDb.Status = (int)status;
                                            dataDb.ModifiedDate = DateTime.Now;
                                            dataDb.ModifiedBy = userId.ToString();

                                            newsAll = Mapper<NewsAll>.MapNewsAll(dataDb, new NewsAll());

                                            resultUpdate = await _context.MultiAsync((transac) =>
                                            {
                                                transac.HashSetAsync(_context.NameOf(dataDb.GetType().Name), dataDb.Id, Json.Stringify(dataDb));

                                                transac.HashSetAsync(_context.NameOf(newsAll.GetType().Name), newsAll.Id, Json.Stringify(newsAll));

                                                transac.SortedSetRemoveAsync(_context.NameOf(IndexKeys.KeyStatus<NewsAll>(statusOld)), newsAll.Id);
                                                transac.SortedSetAddAsync(_context.NameOf(IndexKeys.KeyStatus<NewsAll>(newsAll.Status)), newsAll.Id, (dataDb.ModifiedDate ?? DateTime.Now).ConvertToTimestamp10());
                                            });

                                            if (resultUpdate) returnValue = ErrorCodes.Success;
                                        }
                                        else
                                        {
                                            returnValue = ErrorCodes.PermissionUpdateInvalid;
                                        }
                                    }
                                    else
                                    {
                                        // trạng thái cũ là publish
                                        officerIds = await _context.SetMembersAsync(IndexKeys.KeyAccountInNews(id), CommandFlags.PreferSlave);
                                        if (officerIds != null && officerIds.Count() != 0)
                                        {
                                            if (long.TryParse(officerIds[0], out officerId))
                                            {
                                                var officer = await _context.HashGetExAsync<Account>(officerId, CommandFlags.PreferSlave);

                                                var listMemberId = await _context.SortedSetRangeByScoreAsync(IndexKeys.KeyAccountMemberInOfficer<AccountMember>(officerId), double.NegativeInfinity, double.PositiveInfinity);

                                                var listMember = await _context.GetsFromHashExAsync<AccountMember>(listMemberId?.Select(p => officerId + IndexKeys.SeparateChar + p.ToString())?.ToList(), CommandFlags.PreferSlave);
                                                var accountMember = listMember?.Where(p => p.MemberId == userId).FirstOrDefault();

                                                if(accountMember!=null && officer != null)
                                                {
                                                    var isRemover = await Functions.Function.IsRemover(accountMember, dataDb.CreatedBy, dataDb.ApprovedBy, officer.Mode, statusOld);

                                                    if (isRemover)
                                                    {
                                                        dataDb.Status = (int)status;
                                                        dataDb.ModifiedDate = DateTime.Now;
                                                        dataDb.ModifiedBy = userId.ToString();

                                                        newsAll = Mapper<NewsAll>.MapNewsAll(dataDb, new NewsAll());

                                                        resultUpdate = await _context.MultiAsync((transac) =>
                                                        {
                                                            transac.HashSetAsync(_context.NameOf(dataDb.GetType().Name), dataDb.Id, Json.Stringify(dataDb));

                                                            transac.HashSetAsync(_context.NameOf(newsAll.GetType().Name), newsAll.Id, Json.Stringify(newsAll));

                                                            transac.SortedSetRemoveAsync(_context.NameOf(IndexKeys.KeyStatus<NewsAll>(statusOld)), newsAll.Id);
                                                            transac.SortedSetAddAsync(_context.NameOf(IndexKeys.KeyStatus<NewsAll>(newsAll.Status)), newsAll.Id, (dataDb.ModifiedDate ?? DateTime.Now).ConvertToTimestamp10());

                                                            if (long.TryParse(dataDb.ApprovedBy, out long accountId))
                                                            {
                                                                transac.SortedSetRemoveAsync(_context.NameOf(IndexKeys.KeyApprovedInAccount<Entities.Album.Album>(officerId, accountId)), dataDb.Id);
                                                            }
                                                        });

                                                        if (resultUpdate) returnValue = ErrorCodes.Success;
                                                    }
                                                    else
                                                    {
                                                        returnValue = ErrorCodes.PermissionUpdateInvalid;
                                                    }
                                                }
                                                else
                                                {
                                                    returnValue = ErrorCodes.PermissionUpdateInvalid;
                                                }
                                            }
                                            else
                                            {
                                                returnValue = ErrorCodes.DataInvalid;
                                            }
                                        }
                                        else
                                        {
                                            returnValue = ErrorCodes.DataInvalid;
                                        }
                                    }

                                    break;
                                #endregion
                                #region Gui nho duyet
                                case NewsStatus.Approve:// Gửi nhờ duyệt
                                    if (dataDb.Status == (int)NewsStatus.Published)
                                    {
                                        returnValue = ErrorCodes.DataInvalid;
                                    }
                                    else
                                    {
                                        if (userId.ToString().Equals(dataDb.CreatedBy))
                                        {
                                            officerIds = await _context.SetMembersAsync(IndexKeys.KeyAccountInNews(id), CommandFlags.PreferSlave);
                                            if (officerIds != null && officerIds.Count() != 0)
                                            {
                                                if (long.TryParse(officerIds[0], out officerId))
                                                {
                                                    dataDb.Status = (int)status;
                                                    dataDb.ModifiedDate = DateTime.Now;
                                                    dataDb.ModifiedBy = userId.ToString();

                                                    newsAll = Mapper<NewsAll>.MapNewsAll(dataDb, new NewsAll());

                                                    resultUpdate = await _context.MultiAsync((transac) =>
                                                    {
                                                        transac.HashSetAsync(_context.NameOf(dataDb.GetType().Name), dataDb.Id, Json.Stringify(dataDb));

                                                        transac.HashSetAsync(_context.NameOf(newsAll.GetType().Name), newsAll.Id, Json.Stringify(newsAll));

                                                        transac.SortedSetRemoveAsync(_context.NameOf(IndexKeys.KeyStatus<NewsAll>(statusOld)), newsAll.Id);
                                                        transac.SortedSetAddAsync(_context.NameOf(IndexKeys.KeyStatus<NewsAll>(newsAll.Status)), newsAll.Id, (dataDb.ModifiedDate ?? DateTime.Now).ConvertToTimestamp10());

                                                        if (string.IsNullOrEmpty(dataDb.ApprovedBy))
                                                        {
                                                            transac.SortedSetAddAsync(_context.NameOf(IndexKeys.KeyApprove<Entities.Album.Album>(officerId)), dataDb.Id, DateTime.Now.ConvertToTimestamp10());
                                                        }
                                                        else
                                                        {
                                                            if (long.TryParse(dataDb.ApprovedBy, out long accountId))
                                                            {
                                                                transac.SortedSetAddAsync(_context.NameOf(IndexKeys.KeyApproveInAccount<Entities.Album.Album>(officerId, accountId)), dataDb.Id, DateTime.Now.ConvertToTimestamp10());
                                                            }
                                                        }
                                                    });
                                                }
                                            }

                                            if (resultUpdate) returnValue = ErrorCodes.Success;
                                        }
                                        else
                                        {
                                            returnValue = ErrorCodes.PermissionUpdateInvalid;
                                        }
                                    }
                                    break;
                                #endregion
                                #region default
                                default:
                                    if (userId.ToString().Equals(dataDb.CreatedBy))
                                    {
                                        dataDb.Status = (int)status;
                                        dataDb.ModifiedDate = DateTime.Now;
                                        dataDb.ModifiedBy = userId.ToString();

                                        if (statusOld == (int)NewsStatus.Published)
                                        {
                                            officerIds = await _context.SetMembersAsync(IndexKeys.KeyAccountInNews(id), CommandFlags.PreferSlave);
                                            if (officerIds != null && officerIds.Count() != 0)
                                            {
                                                long.TryParse(officerIds[0], out officerId);
                                            }
                                        }

                                        newsAll = Mapper<NewsAll>.MapNewsAll(dataDb, new NewsAll());

                                        resultUpdate = await _context.MultiAsync((transac) =>
                                        {
                                            transac.HashSetAsync(_context.NameOf(dataDb.GetType().Name), dataDb.Id, Json.Stringify(dataDb));

                                            transac.HashSetAsync(_context.NameOf(newsAll.GetType().Name), newsAll.Id, Json.Stringify(newsAll));

                                            transac.SortedSetRemoveAsync(_context.NameOf(IndexKeys.KeyStatus<NewsAll>(statusOld)), newsAll.Id);
                                            transac.SortedSetAddAsync(_context.NameOf(IndexKeys.KeyStatus<NewsAll>(newsAll.Status)), newsAll.Id, (dataDb.ModifiedDate ?? DateTime.Now).ConvertToTimestamp10());

                                            if(officerId > 0)
                                            {
                                                if (long.TryParse(dataDb.ApprovedBy, out long accountId))
                                                {
                                                    transac.SortedSetRemoveAsync(_context.NameOf(IndexKeys.KeyApprovedInAccount<Entities.Album.Album>(officerId, accountId)), dataDb.Id);
                                                }
                                            }
                                        });

                                        if (resultUpdate) returnValue = ErrorCodes.Success;
                                    }
                                    else
                                    {
                                        if (statusOld == (int?)NewsStatus.Published || statusOld == (int?)NewsStatus.Archive || statusOld == (int?)NewsStatus.Approve)
                                        {
                                            officerIds = await _context.SetMembersAsync(IndexKeys.KeyAccountInNews(id), CommandFlags.PreferSlave);
                                            if (officerIds != null && officerIds.Count() != 0)
                                            {
                                                if (long.TryParse(officerIds[0], out officerId))
                                                {
                                                    var officer = await _context.HashGetExAsync<Account>(officerId, CommandFlags.PreferSlave);

                                                    var listMemberId = await _context.SortedSetRangeByScoreAsync(IndexKeys.KeyAccountMemberInOfficer<AccountMember>(officerId), double.NegativeInfinity, double.PositiveInfinity);

                                                    var listMember = await _context.GetsFromHashExAsync<AccountMember>(listMemberId?.Select(p => officerId + IndexKeys.SeparateChar + p.ToString())?.ToList(), CommandFlags.PreferSlave);
                                                    var accountMember = listMember?.Where(p => p.MemberId == userId).FirstOrDefault();

                                                    var isRemover = await Functions.Function.IsRemover(accountMember, dataDb.CreatedBy, dataDb.ApprovedBy, officer.Mode, statusOld);
                                                    if (isRemover)
                                                    {
                                                        dataDb.Status = (int)status;
                                                        dataDb.ModifiedDate = DateTime.Now;
                                                        dataDb.ModifiedBy = userId.ToString();

                                                        newsAll = Mapper<NewsAll>.MapNewsAll(dataDb, new NewsAll());

                                                        resultUpdate = await _context.MultiAsync((transac) =>
                                                        {
                                                            transac.HashSetAsync(_context.NameOf(dataDb.GetType().Name), dataDb.Id, Json.Stringify(dataDb));

                                                            transac.HashSetAsync(_context.NameOf(newsAll.GetType().Name), newsAll.Id, Json.Stringify(newsAll));

                                                            transac.SortedSetRemoveAsync(_context.NameOf(IndexKeys.KeyStatus<NewsAll>(statusOld)), newsAll.Id);
                                                            transac.SortedSetAddAsync(_context.NameOf(IndexKeys.KeyStatus<NewsAll>(newsAll.Status)), newsAll.Id, (dataDb.ModifiedDate ?? DateTime.Now).ConvertToTimestamp10());

                                                            if (officerId > 0)
                                                            {
                                                                if (long.TryParse(dataDb.ApprovedBy, out long accountId))
                                                                {
                                                                    transac.SortedSetRemoveAsync(_context.NameOf(IndexKeys.KeyApprovedInAccount<Entities.Album.Album>(officerId, accountId)), dataDb.Id);
                                                                }
                                                            }
                                                        });

                                                        if (resultUpdate) returnValue = ErrorCodes.Success;
                                                    }
                                                    else
                                                    {
                                                        returnValue = ErrorCodes.PermissionUpdateInvalid;
                                                    }
                                                }
                                            }
                                        }
                                        else
                                        {
                                            returnValue = ErrorCodes.PermissionUpdateInvalid;
                                        }  
                                    }
                                    break;
                                #endregion
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
            }

            if(returnValue == ErrorCodes.Success && statusOld != null && dataDb != null)
            {
                actionAutoShare(Mapper<AlbumSearch>.Map(dataDb, new AlbumSearch()), statusOld??0);
            }

            return returnValue;
        }

        public Task<Entities.Album.Album> GetByIdAsync(long id)
        {
            var returnValue = Task.FromResult(default(Entities.Album.Album));
            try
            {
                using (var _context = GetContext())
                {
                    returnValue = _context.HashGetExAsync<Entities.Album.Album>(id, CommandFlags.PreferSlave);
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
            }
            return returnValue;
        }
        public Task<Entities.Album.Album> GetByIdTest(string accountId, int mode, long id)
        {
            var returnValue = Task.FromResult(default(Entities.Album.Album));
            try
            {
                switch (mode)
                {
                    case 1:
                        using (var _context = GetContext())
                        {
                            returnValue = _context.ExecAsync(async (db) =>
                            {
                                return Json.Parse<Entities.Album.Album>(await db.HashGetAsync(_context.NameOf<Entities.Album.Album>(), id, CommandFlags.PreferSlave));
                            });

                        }
                        break;
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
            }
            return returnValue;
        }

        public Task<Entities.Album.Album> GetByIdTest2(long accountId, int mode, long id)
        {
            var returnValue = Task.FromResult(default(Entities.Album.Album));
            try
            {
                switch (mode)
                {
                    case 1:
                        using (var _context = GetContext())
                        {
                            returnValue = _context.HashGetExAsync<Entities.Album.Album>(id, CommandFlags.PreferSlave);
                        }
                        break;
                }

            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
            }
            return returnValue;
        }


        public Task<AlbumSearch> GetAlbumFullInfoAsync(long newsId)
        {
            var returnValue = Task.FromResult(default(AlbumSearch));
            try
            {
                using (var _context = GetContext())
                {
                    returnValue = _context.ExecAsync(async (db) =>
                    {
                        var result = default(AlbumSearch);
                        //await Task.Delay(10000);
                        var photo = Json.Parse<Entities.Album.Album>((await db.HashGetAsync(_context.NameOf(typeof(Entities.Album.Album).Name), newsId, CommandFlags.PreferSlave)).ToString());

                        if (photo != null)
                        {
                            result = Mapper<AlbumSearch>.Map(photo, new AlbumSearch());
                            if (result != null)
                            {
                                var listAccountId = await db.SetMembersAsync(_context.NameOf(IndexKeys.KeyAccountInNews(newsId)), CommandFlags.PreferSlave);
                                result.NewsInAccount = (await db.HashGetAsync(_context.NameOf(typeof(NewsInAccount).Name), listAccountId?.Select(p => (RedisValue)(p + IndexKeys.SeparateChar + newsId))?.ToArray(), CommandFlags.PreferSlave))?.Select(p => Json.Parse<NewsInAccount>(p.ToString()))?.ToArray();
                                var listDistributorIdAndItemStreamId = await db.SortedSetRangeByScoreWithScoresAsync(_context.NameOf(IndexKeys.KeyNewsDistribution(newsId)), double.MinValue, double.MaxValue, Exclude.None, Order.Ascending, 0, -1, CommandFlags.PreferSlave);

                                var listKeyNewsDistribution = listDistributorIdAndItemStreamId?.Select(p => p.Element.ToString()?.Split(IndexKeys.SeparateChar)?.FirstOrDefault() + IndexKeys.SeparateChar + newsId + IndexKeys.SeparateChar + p.Element.ToString()?.Split(IndexKeys.SeparateChar)?.LastOrDefault());

                                result.NewsDistribution = (await db.HashGetAsync(_context.NameOf(typeof(NewsDistribution).Name), listKeyNewsDistribution?.Select(p => (RedisValue)p)?.ToArray(), CommandFlags.PreferSlave))?.Select(p => Json.Parse<NewsDistribution>(p.ToString())).ToArray();

                                var listKeyPhoto = await db.SortedSetRangeByRankAsync(_context.NameOf(IndexKeys.KeyPhotoInAlbum<PhotoInAlbum>(newsId)), 0, -1, Order.Descending, CommandFlags.PreferSlave);
                                result.PhotoInAlbum = (await db.HashGetAsync(_context.NameOf(typeof
                                    (PhotoInAlbum).Name), listKeyPhoto?.Select(p => (RedisValue)(newsId + IndexKeys.SeparateChar + p.ToString()))?.ToArray()))?
                                    .Select(p => Json.Parse<PhotoInAlbum>(p.ToString())).ToArray();
                            }
                        }
                        return result;
                    });
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
            }
            return returnValue;
        }

        public Task<List<Entities.Album.Album>> GetListByIdAsync(long[] ids)
        {
            var returnValue = Task.FromResult(default(List<Entities.Album.Album>));
            try
            {
                using (var _context = GetContext())
                {
                    returnValue = _context.GetsFromHashExAsync<Entities.Album.Album>(ids?.Select(p => p.ToString())?.ToList(), CommandFlags.PreferSlave);
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
            }
            return returnValue;
        }
        public Task<List<AlbumSearch>> GetListAlbumSearchByIdsAsync(List<string> ids)
        {
            var returnValue = Task.FromResult(default(List<AlbumSearch>));
            try
            {
                using (var _context = GetContext())
                {
                    returnValue = _context.ExecAsync(async (db) =>
                    {
                        var result = new List<AlbumSearch>();
                        var data = (await db.HashGetAsync(_context.NameOf(typeof(Entities.Album.Album).Name), ids?.Select(p => (RedisValue)p)?.ToArray(), CommandFlags.PreferSlave))
                        ?.Select(p => Json.Parse<Entities.Album.Album>(p.ToString()))?.ToList();
                        if (data != null)
                        {
                            foreach (var media in data)
                            {
                                var AlbumSearch = Mapper<AlbumSearch>.Map(media, new AlbumSearch());
                                var listTmp = new List<NewsDistribution>(); //AlbumSearch.NewsDistribution = new NewsDistribution[] { };
                                try
                                {
                                    var newsDistribution = await db.SortedSetRangeByScoreWithScoresAsync(_context.NameOf(IndexKeys.KeyNewsDistribution(media.Id)), double.MinValue, double.MaxValue, Exclude.None, Order.Ascending, 0, -1, CommandFlags.PreferSlave);
                                    if (newsDistribution != null && newsDistribution.Count() > 0)
                                    {
                                        foreach (var item in newsDistribution)
                                        {
                                            if (long.TryParse(item.Element.ToString()?.Split(IndexKeys.SeparateChar)?.FirstOrDefault(), out long distributorId))
                                            {
                                                listTmp.Add(new NewsDistribution
                                                {
                                                    DistributorId = distributorId,
                                                    SharedDate = Utility.ConvertSecondsToDate(item.Score),
                                                    DistributorName = Json.Parse<Distributor>((await db.HashGetAsync(_context.NameOf(typeof(Distributor).Name), distributorId, CommandFlags.PreferSlave)))?.Name
                                                });
                                            }
                                        }
                                    }
                                }
                                catch (Exception ex)
                                {
                                    Logger.Error(ex, ex.Message);
                                }
                                AlbumSearch.NewsDistribution = listTmp.ToArray();

                                //get PhotoInAlbum
                                try
                                {
                                    //videoPlaylistSearchReturn.VideoInPlaylist = await
                                    var listKeyPhoto = await db.SortedSetRangeByRankAsync(_context.NameOf(IndexKeys.KeyPhotoInAlbum<PhotoInAlbum>(AlbumSearch.Id)), 0, -1, Order.Ascending, CommandFlags.PreferSlave);

                                    AlbumSearch.PhotoInAlbum = (await db.HashGetAsync(_context.NameOf<PhotoInAlbum>(), listKeyPhoto?.Select(p => (RedisValue)(AlbumSearch.Id + IndexKeys.SeparateChar + p))?.ToArray(), CommandFlags.PreferSlave))?.Select(p => Json.Parse<PhotoInAlbum>(p))?.ToArray();
                                }
                                catch (Exception ex)
                                {
                                    Logger.Error(ex, ex.Message);
                                }

                                result.Add(AlbumSearch);
                            }
                        }
                        return result;
                    });
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
            }
            return returnValue;
        }

        public Task<PagingDataResult<AlbumSearchReturn>> SearchAsync(SearchNews data, long userId, Action<string> action)
        {
            var returnValue = Task.FromResult(default(PagingDataResult<AlbumSearchReturn>));
            try
            {
                using (var _context = GetContext())
                {
                    returnValue = _context.ExecAsync(async (db) =>
                    {
                        //var listMemberId = await db.SortedSetRangeByRankAsync(_context.NameOf(IndexKeys.KeyAccountMemberInOfficer<AccountMember>(data.OfficerId)), 0, -1, Order.Descending, CommandFlags.PreferSlave);
                        //var memberValid = listMemberId?.Where(p => p.ToString().Equals(userId.ToString()))?.Count() > 0;

                        var member = Json.Parse<AccountMember>(await db.HashGetAsync(_context.NameOf<AccountMember>(), data.OfficerId + IndexKeys.SeparateChar + userId, CommandFlags.PreferSlave));

                        if (member != null)
                        {
                            var result = new PagingDataResult<AlbumSearchReturn>() { Data = new List<AlbumSearchReturn>() };
                            try
                            {
                                var listKey = new RedisValue[] { };
                                var listPost = new List<Entities.Album.Album>();
                                var tempKeyCreatedBy = _context.NameOf("index:temp:createdby:" + Generator.SorterSetSearchId());
                                var createdByKey = _context.NameOf(IndexKeys.KeyCreatedBy<NewsAll>(userId.ToString()));
                                var tempKey = _context.NameOf("index:temp:union:" + Generator.SorterSetSearchId());
                                var total = default(long);
                                var listkeyFull = new List<RedisKey>();
                                var listCreatedBy = new List<RedisKey>();
                                var value4 = 0L;

                                if (data.Status.Equals(((int)NewsStatus.Published).ToString()) && member.Permissions != null && (member.Permissions.Contains(MemberPermission.ModifyPostOnPage) || member.Permissions.Contains(MemberPermission.RemovePostOnPage)))
                                {

                                }
                                else
                                {
                                    var listOwner = await _context.SortedSetRangeByRankAsync(IndexKeys.KeyOwnerOfficer(userId), 0, -1, Order.Descending, CommandFlags.PreferSlave);
                                    if (listOwner == null || !listOwner.Contains((RedisValue)data.OfficerId))
                                    {
                                        listCreatedBy.Add(createdByKey);

                                        //Lấy cả những bài chỉ duyệt
                                        if (data.Status?.Split(",")?.Contains(((int)NewsStatus.Published).ToString()) ?? false)
                                        {
                                            listCreatedBy.Add(_context.NameOf(IndexKeys.KeyApprovedInAccount<Entities.Album.Album>(data.OfficerId, userId)));
                                        }

                                        value4 = await db.SortedSetCombineAndStoreAsync(SetOperation.Union
                                        , tempKeyCreatedBy, listCreatedBy.ToArray(), null, Aggregate.Max, CommandFlags.None);

                                        if (value4 > 0)
                                        {
                                            listkeyFull.Add(tempKeyCreatedBy);
                                        }
                                    }
                                    else
                                    {
                                        if ((data.Status?.Split(",")?.Contains(((int)NewsStatus.Approve).ToString()) ?? false) || (data.Status?.Split(",")?.Contains(((int)NewsStatus.Draft).ToString()) ?? false) || (data.Status?.Split(",")?.Contains(((int)NewsStatus.Remove).ToString()) ?? false))
                                        {
                                            listkeyFull.Add(createdByKey);
                                        }
                                    }
                                }

                                try
                                {
                                    listkeyFull.Add(_context.NameOf(IndexKeys.KeyNewsInAccount(data.OfficerId)));
                                    listkeyFull.Add(_context.NameOf(IndexKeys.KeyType<NewsAll>((int)NewsType.Album)));
                                    switch (string.IsNullOrEmpty(data.Status))
                                    {
                                        case true:
                                            if (data.DistributionId == -1)
                                            {
                                                listkeyFull.Add(_context.NameOf(IndexKeys.KeyDistributorid<NewsDistribution>()));
                                            }
                                            else
                                            {
                                                if (data.DistributionId > 0)
                                                {
                                                    listkeyFull.Add(_context.NameOf(IndexKeys.KeyDistributorid<NewsDistribution>(data.DistributionId)));
                                                }
                                            }
                                            var value1 = await db.SortedSetCombineAndStoreAsync(SetOperation.Intersect
                                                        , tempKey, listkeyFull?.ToArray(), null, Aggregate.Max, CommandFlags.None);
                                            if (value1 > 0)
                                            {
                                                try
                                                {
                                                    listKey = (await db.SortedSetRangeByScoreAsync(tempKey
                                                    , (data.FromDate ?? DateTime.MinValue).ConvertToTimestamp10()
                                                    , (data.ToDate ?? DateTime.Now).ConvertToTimestamp10()
                                                    , Exclude.None
                                                    , data.OrderBy == SearchNewsOrderBy.AscCreatedDate ? Order.Ascending : Order.Descending
                                                    , ((data.PageIndex ?? 1) - 1) * (data.PageSize ?? 20)
                                                    , data.PageSize ?? 20, CommandFlags.None));

                                                    total = await db.SortedSetLengthAsync(tempKey
                                                        , (data.FromDate ?? DateTime.MinValue).ConvertToTimestamp10()
                                                        , (data.ToDate ?? DateTime.Now).ConvertToTimestamp10(), Exclude.None, CommandFlags.None);
                                                    result.Total = (int)total;
                                                    listPost = listKey != null && listKey.Length > 0 ? (await db.HashGetAsync(_context.NameOf(typeof(Entities.Album.Album).Name), listKey, CommandFlags.PreferSlave))?.Select(p => Json.Parse<Entities.Album.Album>(p))?.ToList() : listPost;
                                                    if (listPost != null)
                                                    {
                                                        foreach (var video in listPost)
                                                        {
                                                            var albumSearchReturn = Mapper<AlbumSearchReturn>.Map(video, new AlbumSearchReturn());
                                                            var listTmp = new List<NewsDistribution>(); //videoSearchReturn.NewsDistribution = new NewsDistribution[] { };
                                                            try
                                                            {
                                                                var newsDistribution = await db.SortedSetRangeByScoreWithScoresAsync(_context.NameOf(IndexKeys.KeyNewsDistribution(video.Id)), double.MinValue, double.MaxValue, Exclude.None, Order.Ascending, 0, -1, CommandFlags.PreferSlave);
                                                                if (newsDistribution != null)
                                                                {
                                                                    foreach (var item in newsDistribution)
                                                                    {
                                                                        if (long.TryParse(item.Element.ToString()?.Split(IndexKeys.SeparateChar)?.FirstOrDefault(), out long distributorId))
                                                                        {
                                                                            listTmp.Add(new NewsDistribution
                                                                            {
                                                                                DistributorId = distributorId,
                                                                                SharedDate = Utility.ConvertSecondsToDate(item.Score),
                                                                                DistributorName = Json.Parse<Distributor>((await db.HashGetAsync(_context.NameOf(typeof(Distributor).Name), distributorId, CommandFlags.PreferSlave)).ToString())?.Name
                                                                            });
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                            catch (Exception exx)
                                                            {
                                                                Logger.Error(exx, exx.Message);
                                                            }
                                                            albumSearchReturn.NewsDistribution = listTmp.ToArray();

                                                            //get PhotoInAlbum
                                                            try
                                                            {
                                                                //videoPlaylistSearchReturn.VideoInPlaylist = await
                                                                var listKeyPhoto = await db.SortedSetRangeByRankAsync(_context.NameOf(IndexKeys.KeyPhotoInAlbum<PhotoInAlbum>(albumSearchReturn.Id)), 0, -1, Order.Ascending, CommandFlags.PreferSlave);

                                                                albumSearchReturn.PhotoInAlbum = (await db.HashGetAsync(_context.NameOf<PhotoInAlbum>(), listKeyPhoto?.Select(p => (RedisValue)(albumSearchReturn.Id + IndexKeys.SeparateChar + p))?.ToArray(), CommandFlags.PreferSlave))?.Select(p => Json.Parse<PhotoInAlbum>(p))?.ToArray();
                                                            }
                                                            catch (Exception ex)
                                                            {
                                                                Logger.Error(ex, ex.Message);
                                                            }

                                                            result.Data.Add(albumSearchReturn);
                                                        }
                                                    }
                                                }
                                                catch (Exception ex)
                                                {
                                                    Logger.Error(ex, ex.Message);
                                                }
                                                action(tempKey);
                                            }
                                            break;
                                        case false:
                                            if (data.DistributionId == -1)
                                            {
                                                listkeyFull.Add(_context.NameOf(IndexKeys.KeyDistributorid<NewsDistribution>()));
                                            }
                                            else
                                            {
                                                if (data.DistributionId > 0)
                                                {
                                                    listkeyFull.Add(_context.NameOf(IndexKeys.KeyDistributorid<NewsDistribution>(data.DistributionId)));
                                                }
                                            }

                                            var listRedisKeyStatus = new List<RedisKey>();
                                            var listStrStatus = data.Status.Split(",");
                                            foreach (var s in listStrStatus)
                                            {
                                                if (int.TryParse(s, out int status))
                                                {
                                                    if (Enum.IsDefined(typeof(NewsStatus), status))
                                                    {
                                                        listRedisKeyStatus.Add(_context.NameOf(IndexKeys.KeyStatus<NewsAll>(status)));
                                                    }
                                                }
                                            }
                                            var tempKey2 = _context.NameOf("index:temp:" + Generator.SorterSetSearchId());
                                            var value2 = await db.SortedSetCombineAndStoreAsync(SetOperation.Union
                                                     , tempKey2, listRedisKeyStatus.ToArray(), null, Aggregate.Max, CommandFlags.None);
                                            if (value2 > 0)
                                            {
                                                try
                                                {
                                                    listkeyFull.Add(tempKey2);
                                                    var value3 = await db.SortedSetCombineAndStoreAsync(SetOperation.Intersect, tempKey, listkeyFull.ToArray(), null, Aggregate.Max, CommandFlags.None);
                                                    if (value3 > 0)
                                                    {
                                                        try
                                                        {
                                                            total = await db.SortedSetLengthAsync(tempKey
                                                            , (data.FromDate ?? DateTime.MinValue).ConvertToTimestamp10()
                                                            , (data.ToDate ?? DateTime.Now).ConvertToTimestamp10(), Exclude.None, CommandFlags.None);
                                                            result.Total = (int)total;
                                                            listKey = (await db.SortedSetRangeByScoreAsync(tempKey
                                                            , (data.FromDate ?? DateTime.MinValue).ConvertToTimestamp10()
                                                            , (data.ToDate ?? DateTime.Now).ConvertToTimestamp10()
                                                            , Exclude.None
                                                            , data.OrderBy == SearchNewsOrderBy.AscCreatedDate ? Order.Ascending : Order.Descending
                                                            , ((data.PageIndex ?? 1) - 1) * (data.PageSize ?? 20)
                                                            , data.PageSize ?? 20, CommandFlags.None));

                                                            listPost = listKey != null && listKey.Length > 0 ? (await db.HashGetAsync(_context.NameOf(typeof(Entities.Album.Album).Name
                                                               ), listKey, CommandFlags.PreferSlave))?.Select(p => Json.Parse<Entities.Album.Album>(p))?.ToList() : listPost;
                                                            if (listPost != null)
                                                            {
                                                                foreach (var video in listPost)
                                                                {
                                                                    var albumSearchReturn = Mapper<AlbumSearchReturn>.Map(video, new AlbumSearchReturn());
                                                                    var listTmp = new List<NewsDistribution>(); //videoSearchReturn.NewsDistribution = new NewsDistribution[] { };
                                                                    try
                                                                    {
                                                                        var newsDistribution = await db.SortedSetRangeByScoreWithScoresAsync(_context.NameOf(IndexKeys.KeyNewsDistribution(video.Id)), double.MinValue, double.MaxValue, Exclude.None, Order.Ascending, 0, -1, CommandFlags.PreferSlave);
                                                                        if (newsDistribution != null)
                                                                        {
                                                                            foreach (var item in newsDistribution)
                                                                            {
                                                                                if (long.TryParse(item.Element.ToString()?.Split(IndexKeys.SeparateChar)?.FirstOrDefault(), out long distributorId))
                                                                                {
                                                                                    listTmp.Add(new NewsDistribution
                                                                                    {
                                                                                        DistributorId = distributorId,
                                                                                        SharedDate = Utility.ConvertSecondsToDate(item.Score),
                                                                                        DistributorName = Json.Parse<Distributor>((await db.HashGetAsync(_context.NameOf(typeof(Distributor).Name), distributorId, CommandFlags.PreferSlave)))?.Name
                                                                                    });
                                                                                }
                                                                            }
                                                                        }
                                                                    }
                                                                    catch (Exception exx)
                                                                    {
                                                                        Logger.Error(exx, exx.Message);
                                                                    }
                                                                    albumSearchReturn.NewsDistribution = listTmp.ToArray();

                                                                    //get PhotoInAlbum
                                                                    try
                                                                    {
                                                                        var listKeyPhoto = await db.SortedSetRangeByRankAsync(_context.NameOf(IndexKeys.KeyPhotoInAlbum<PhotoInAlbum>(albumSearchReturn.Id)), 0, -1, Order.Ascending, CommandFlags.PreferSlave);

                                                                        albumSearchReturn.PhotoInAlbum = (await db.HashGetAsync(_context.NameOf<PhotoInAlbum>(), listKeyPhoto?.Select(p => (RedisValue)(albumSearchReturn.Id + IndexKeys.SeparateChar + p))?.ToArray(), CommandFlags.PreferSlave))?.Select(p => Json.Parse<PhotoInAlbum>(p))?.ToArray();
                                                                    }
                                                                    catch (Exception ex)
                                                                    {
                                                                        Logger.Error(ex, ex.Message);
                                                                    }

                                                                    result.Data.Add(albumSearchReturn);
                                                                }
                                                            }
                                                        }
                                                        catch (Exception ex)
                                                        {
                                                            Logger.Error(ex, ex.Message);
                                                        }
                                                        action(tempKey);
                                                    }
                                                }
                                                catch (Exception ex)
                                                {
                                                    Logger.Error(ex, ex.Message);
                                                }
                                                action(tempKey2);
                                            }
                                            break;
                                    }
                                }
                                catch (Exception ex)
                                {
                                    Logger.Error(ex, ex.Message);
                                }

                                if (value4 > 0)
                                {
                                    action(tempKeyCreatedBy);
                                }
                            }
                            catch (Exception ex)
                            {
                                Logger.Error(ex, ex.Message);
                            }
                            return result;
                        }
                        else
                        {
                            return new PagingDataResult<AlbumSearchReturn>();
                        }
                    });
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
            }
            return returnValue;
        }

        public Task<bool> AddPhotoAsync(long photoId, List<Entities.Album.Album> dataDb)
        {
            var returnValue = Task.FromResult(false);
            try
            {
                using (var _context = GetContext())
                {
                    returnValue = _context.MultiAsync((transac) =>
                    {
                        foreach (var album in dataDb)
                        {
                            var newsAll = Mapper<NewsAll>.MapNewsAll(album, new NewsAll());
                            var photoInAlbum = new PhotoInAlbum
                            {
                                AlbumId = album.Id,
                                PhotoId = photoId,
                                Priority = 0,
                                PublishedDate = DateTime.Now
                            };
                            transac.HashSetAsync(_context.NameOf(album.GetType().Name), album.Id, Json.Stringify(album));
                            transac.HashSetAsync(_context.NameOf<PhotoInAlbum>(), album.Id + IndexKeys.SeparateChar + photoId, Json.Stringify(photoInAlbum));
                            transac.SortedSetAddAsync(_context.NameOf(IndexKeys.KeyPhotoInAlbum<PhotoInAlbum>(album.Id)), photoId, (photoInAlbum.PublishedDate ?? DateTime.Now).ConvertToTimestamp10());

                            transac.HashSetAsync(_context.NameOf<NewsAll>(), newsAll?.Id, Json.Stringify(newsAll));
                        }
                    });
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
            }
            return returnValue;
        }


        public Task<PagingDataResult<AlbumSearch>> GetListApproveAsync(SearchNews data, long accountId, Action<string> action)
        {
            var returnValue = Task.FromResult(default(PagingDataResult<AlbumSearch>));
            try
            {
                using (var _context = GetContext())
                {
                    returnValue = _context.ExecAsync(async (db) =>
                    {
                        var listMemberId = await db.SortedSetRangeByRankAsync(_context.NameOf(IndexKeys.KeyAccountMemberInOfficer<AccountMember>(data.OfficerId)), 0, -1, Order.Descending, CommandFlags.PreferSlave);
                        var memberValid = listMemberId?.Where(p => p.ToString().Equals(accountId.ToString()))?.Count() > 0;

                        if(memberValid == true)
                        {
                            var result = new PagingDataResult<AlbumSearch>()
                            {
                                Data = new List<AlbumSearch>()
                            };
                            try
                            {
                                var listKey = new RedisValue[] { };
                                var listAlbum = new List<AlbumSearch>();
                                var total = default(long);
                                var tempKey = _context.NameOf("index:temp:union:" + Generator.SorterSetSearchId());

                                var listkeyFull = new List<RedisKey>();
                                listkeyFull.Add(_context.NameOf(IndexKeys.KeyStatus<NewsAll>(int.Parse(data.Status))));

                                var keyApprove = new List<RedisKey>();
                                keyApprove.Add(_context.NameOf(IndexKeys.KeyApprove<Entities.Album.Album>(data.OfficerId)));
                                keyApprove.Add(_context.NameOf(IndexKeys.KeyApproveInAccount<Entities.Album.Album>(data.OfficerId, accountId)));

                                var tempKeyApprove = _context.NameOf("index:temp:approve:" + Generator.SorterSetSearchId());

                                var valueApprove = await db.SortedSetCombineAndStoreAsync(SetOperation.Union
                                            , tempKeyApprove, keyApprove.ToArray(), null, Aggregate.Max, CommandFlags.None);

                                if (valueApprove > 0)
                                {
                                    listkeyFull.Add(tempKeyApprove);
                                    var value = await db.SortedSetCombineAndStoreAsync(SetOperation.Intersect
                                            , tempKey, listkeyFull.ToArray(), null, Aggregate.Max, CommandFlags.None);
                                    if (value > 0)
                                    {
                                        try
                                        {
                                            listKey = (await db.SortedSetRangeByScoreAsync(tempKey
                                            , (data.FromDate ?? DateTime.MinValue).ConvertToTimestamp10()
                                            , (data.ToDate ?? DateTime.Now).ConvertToTimestamp10()
                                            , Exclude.None
                                            , data.OrderBy == SearchNewsOrderBy.AscCreatedDate ? Order.Ascending : Order.Descending
                                            , ((data.PageIndex ?? 1) - 1) * (data.PageSize ?? 20)
                                            , data.PageSize ?? 20
                                            , CommandFlags.None));

                                            total = await db.SortedSetLengthAsync(tempKey
                                                , (data.FromDate ?? DateTime.MinValue).ConvertToTimestamp10()
                                                , (data.ToDate ?? DateTime.Now).ConvertToTimestamp10()
                                                , Exclude.None
                                                , CommandFlags.None);

                                            result.Total = (int)total;
                                            listAlbum = listKey != null && listKey.Length > 0 ? (await db.HashGetAsync(_context.NameOf(typeof(Entities.Album.Album).Name), listKey, CommandFlags.PreferSlave))?.Select(p => Json.Parse<AlbumSearch>(p))?.ToList() : listAlbum;
                                            if (listAlbum != null)
                                            {
                                                foreach (var album in listAlbum)
                                                {
                                                    //var articleSearchReturn = Mapper<ArticleSearchReturn>.Map(article, new ArticleSearchReturn());
                                                    //var listTmp = new List<NewsDistribution>();
                                                    //try
                                                    //{
                                                    //    var newsDistribution = await db.SortedSetRangeByScoreWithScoresAsync(_context.NameOf(IndexKeys.KeyNewsDistribution(article.Id)), double.MinValue, double.MaxValue, Exclude.None, Order.Ascending, 0, -1, CommandFlags.PreferSlave);
                                                    //    if (newsDistribution != null)
                                                    //    {
                                                    //        foreach (var item in newsDistribution)
                                                    //        {
                                                    //            if (long.TryParse(item.Element.ToString()?.Split(IndexKeys.SeparateChar)?.FirstOrDefault(), out long distributorId))
                                                    //            {
                                                    //                listTmp.Add(new NewsDistribution
                                                    //                {
                                                    //                    DistributorId = distributorId,
                                                    //                    SharedDate = Utility.ConvertSecondsToDate(item.Score),
                                                    //                    DistributorName = Json.Parse<Distributor>(await db.HashGetAsync(_context.NameOf(typeof(Distributor).Name), distributorId, CommandFlags.PreferSlave))?.Name
                                                    //                });
                                                    //            }
                                                    //        }
                                                    //    }
                                                    //}
                                                    //catch (Exception exx)
                                                    //{
                                                    //    Logger.Error(exx, exx.Message);
                                                    //}
                                                    //articleSearchReturn.NewsDistribution = listTmp.ToArray();
                                                    //result.Data.Add(articleSearchReturn);

                                                    //get PhotoInAlbum
                                                    try
                                                    {
                                                        var listKeyPhoto = await db.SortedSetRangeByRankAsync(_context.NameOf(IndexKeys.KeyPhotoInAlbum<PhotoInAlbum>(album.Id)), 0, -1, Order.Ascending, CommandFlags.PreferSlave);

                                                        album.PhotoInAlbum = (await db.HashGetAsync(_context.NameOf<PhotoInAlbum>(), listKeyPhoto?.Select(p => (RedisValue)(album.Id + IndexKeys.SeparateChar + p))?.ToArray(), CommandFlags.PreferSlave))?.Select(p => Json.Parse<PhotoInAlbum>(p))?.ToArray();
                                                    }
                                                    catch (Exception ex)
                                                    {
                                                        Logger.Error(ex, ex.Message);
                                                    }
                                                }

                                                result.Data.AddRange(listAlbum);
                                            }
                                        }
                                        catch (Exception ex)
                                        {
                                            Logger.Error(ex, ex.Message);
                                        }
                                        action(tempKey);
                                    }
                                    action(tempKeyApprove);
                                }
                            }
                            catch (Exception ex)
                            {
                                Logger.Error(ex, ex.Message);
                            }

                            return result;
                        }
                        else
                        {
                            return new PagingDataResult<AlbumSearch>();
                        }
                    });
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
            }
            return returnValue;
        }

        public async Task<RedisValue[]> GetAllAsync2()
        {
            var returnValue = default(RedisValue[]);
            try
            {
                using (var _context = GetContext())
                {
                    returnValue = await _context.ExecAsync(async (db) =>
                    {
                        var tempKey = _context.NameOf("index:temp:union:" + Generator.SorterSetSearchId());
                        var value = await db.SortedSetCombineAndStoreAsync(SetOperation.Intersect
                                        , tempKey, _context.NameOf(IndexKeys.KeyStatus<NewsAll>((int)NewsStatus.Published)), _context.NameOf(IndexKeys.KeyType<NewsAll>((int)NewsType.Album)));

                        returnValue = await db.SortedSetRangeByRankAsync(tempKey, 0, -1);
                        await db.KeyDeleteAsync(tempKey);
                        return returnValue;
                    });
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
            }
            return returnValue;
        }
    }
}
