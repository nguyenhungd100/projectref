﻿using ChannelVN.IMS2.Core.Entities;
using ChannelVN.IMS2.Core.Entities.Posts;
using ChannelVN.IMS2.Core.Repositories;
using ChannelVN.IMS2.Foundation.Common;
using System;
using System.Linq;
using System.Threading.Tasks;
using static ChannelVN.IMS2.Core.Entities.ErrorCode.ErrorMapping;

namespace ChannelVN.IMS2.Core.Services
{
    public class PostService
    {
        public static Task<ErrorCodes> AddAsync(PostSearch data, string clientIP)
        {
            return PostRepository.AddAsync(data, clientIP);
        }

        public static Task<PagingDataResult<PostSearchReturn>> SearchAsync(SearchNews data, long userId, bool isGetDistribution = false)
        {
            return PostRepository.SearchAsync(data, userId, isGetDistribution);
        }

        public static Task<PagingDataResult<Post>> GetListApproveAsync(SearchNews data, long accountId)
        {
            return PostRepository.GetListApproveAsync(data, accountId);
        }

        public static Task<ErrorCodes> UpdateStatusAsync(PostSearch data, int statusOld, string clientIP)
        {
            return PostRepository.UpdateStatusAsync(data, statusOld, clientIP);
        }

        public static Task<ErrorCodes> UpdateStatusV2Async(long id, NewsStatus status, long userId, string clientIP, string sessionId)
        {
            return PostRepository.UpdateStatusV2Async(id, status, userId, clientIP, sessionId);
        }

        public static Task<ErrorCodes> UpdateAsync(Post data, long officerId, string clientIP, string sessionId)
        {
            return PostRepository.UpdateAsync(data, officerId, clientIP, sessionId);
        }
        public static Task<PostSearch> GetPostSearchByIdAsync(long id)
        {
            return PostRepository.GetPostSearchByIdAsync(id);
        }
    }
}
