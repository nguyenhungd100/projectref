﻿using ChannelVN.IMS2.Core.Entities;
using ChannelVN.IMS2.Core.Entities.Statistic;
using ChannelVN.IMS2.Core.Entities.Statistic.Rearch.Request;
using ChannelVN.IMS2.Core.Entities.Statistic.Rearch.Response;
using ChannelVN.IMS2.Core.Entities.Statistic.Video;
using ChannelVN.IMS2.Core.Entities.Statistics;
using ChannelVN.IMS2.Core.Repositories;
using ChannelVN.IMS2.Foundation.Common;
using ChannelVN.IMS2.Foundation.Common.Configuration;
using ChannelVN.IMS2.Foundation.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;

namespace ChannelVN.IMS2.Core.Services
{
    public static class StatisticsService
    {
        public static Task<Dictionary<DateTime, NewsCount2[]>> PublishNewsCountAsync(GetNewsCount search)
        {
            return StatisticsRepository.PublishNewsCountAsync(search);
        }

        public static Task<Dictionary<DateTime, NewsCount[]>> PublishNewsCountOfUserOnPageAsync(GetNewsCountOfUser search)
        {
            return StatisticsRepository.PublishNewsCountOfUserOnPageAsync(search);
        }


        public static Task<PagingDataResult<object>> ListPageAsync(SearchPage search)
        {
            return StatisticsRepository.ListPageAsync(search);
        }

        public static async Task<HttpClientResponse<Dictionary<DateTime, long>>> GetFollowersOnPagePAsync(FollowerRequestDataModel request)
        {
            var returnValue = default(HttpClientResponse<Dictionary<DateTime, long>>);
            try
            {
                var query = await (new FormUrlEncodedContent(new
                {
                    id = request.Id,
                    start_date = request.StartDate.ToString("yyyy-MM-dd"),
                    end_date = request.EndDate.ToString("yyyy-MM-dd")
                }.ConvertToKeyValuePair())).ReadAsStringAsync();
                var url = AppSettings.Current.ChannelConfiguration.ApiInteractionsStatisticsSetting.UrlGetFollowerOnPage;

                var result = await HttpClientService.GetDataAsync<GetFollowerResponseDataModel[]>(url, query);
                if (result != null && result.Status == 1 && result.Result != null && result.Result.Count() > 0)
                {
                    returnValue = new HttpClientResponse<Dictionary<DateTime, long>>
                    {
                        Data = result.Result.ToDictionary(p => p.Date.Value, p => p.Follower),
                        Success = true
                    };
                }
                else
                {
                    returnValue = new HttpClientResponse<Dictionary<DateTime, long>>
                    {
                        Data = new Dictionary<DateTime, long>(),
                        Success = false,
                        Message = result?.Message ?? "Không lấy được dữ liệu."
                    };
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
                returnValue = new HttpClientResponse<Dictionary<DateTime, long>>
                {
                    Success = false,
                    Message = ex.Message
                };
            }
            return returnValue;
        }

        public static async Task<Dictionary<DateTime, GetInteractionsResponseDataModel>> GetInteractionsOnPageOrPostPAsync(InteractionsRequestDataModel request)
        {
            var returnValue = default(Dictionary<DateTime, GetInteractionsResponseDataModel>);
            try
            {
                var query = await (new FormUrlEncodedContent(new
                {
                    id = request.Id,
                    start_date = request.StartDate.ToString("yyyy-MM-dd"),
                    end_date = request.EndDate.ToString("yyyy-MM-dd"),
                    type = (int)request.Type
                }.ConvertToKeyValuePair())).ReadAsStringAsync();
                var url = AppSettings.Current.ChannelConfiguration.ApiInteractionsStatisticsSetting.UrlGetInteractions;

                var result = await HttpClientService.GetDataAsync<GetInteractionsResponseDataModel[]>(url, query);
                if (result != null && result.Status == 1 && result.Result != null)
                {
                    returnValue = result.Result.ToDictionary(p => p.Date.Value, p => p);
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
            }
            return returnValue;
        }

        public static async Task<Dictionary<DateTime, int>> GetPostCountOfPostPAsync(FollowerRequestDataModel request)
        {
            var returnValue = default(Dictionary<DateTime, int>);
            try
            {
                var query = await (new FormUrlEncodedContent(new
                {
                    post_id = request.Id,
                    start_date = request.StartDate.ToString("yyyy-MM-dd"),
                    end_date = request.EndDate.ToString("yyyy-MM-dd")
                }.ConvertToKeyValuePair())).ReadAsStringAsync();
                var url = AppSettings.Current.ChannelConfiguration.ApiInteractionsStatisticsSetting.UrlGetPostCount;

                var result = await HttpClientService.GetDataAsync<GetPostResponseDataModel[]>(url, query);
                if (result != null && result.Status == 1 && result.Result != null)
                {
                    returnValue = result.Result.ToDictionary(p => p.Date, p => p.Post);
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
            }
            return returnValue;
        }

        public static async Task<Dictionary<DateTime, int>> GetPostCountOfAllPostOfPageAsync(FollowerRequestDataModel request)
        {
            var returnValue = default(Dictionary<DateTime, int>);
            try
            {
                var query = await (new FormUrlEncodedContent(new
                {
                    id = request.Id,
                    start_date = request.StartDate.ToString("yyyy-MM-dd"),
                    end_date = request.EndDate.ToString("yyyy-MM-dd")
                }.ConvertToKeyValuePair())).ReadAsStringAsync();
                var url = AppSettings.Current.ChannelConfiguration.ApiInteractionsStatisticsSetting.UrlGetPostCountOfAllPostOfPage;

                var result = await HttpClientService.GetDataAsync<GetPostResponseDataModel[]>(url, query);
                if (result != null && result.Status == 1 && result.Result != null)
                {
                    returnValue = result.Result.ToDictionary(p => p.Date, p => p.Post);
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
            }
            return returnValue;
        }

        public static async Task<Dictionary<DateTime, int>> GetRepostCountOfAllPostOfPageAsync(FollowerRequestDataModel request)
        {
            var returnValue = default(Dictionary<DateTime, int>);
            try
            {
                var query = await (new FormUrlEncodedContent(new
                {
                    id = request.Id,
                    start_date = request.StartDate.ToString("yyyy-MM-dd"),
                    end_date = request.EndDate.ToString("yyyy-MM-dd")
                }.ConvertToKeyValuePair())).ReadAsStringAsync();
                var url = AppSettings.Current.ChannelConfiguration.ApiInteractionsStatisticsSetting.UrlGetRepostCountOfAllPostOfPage;

                var result = await HttpClientService.GetDataAsync<GetRepostResponseDataModel[]>(url, query);
                if (result != null && result.Status == 1 && result.Result != null)
                {
                    returnValue = result.Result.ToDictionary(p => p.Date, p => p.Repost);
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
            }
            return returnValue;
        }

        public static async Task<ViewByDate[]> GetIndexVideoByDateAsync(string ids, DateTime fromDate, DateTime toDate)
        {
            var returnValue = default(ViewByDate[]);
            try
            {
                var dateFormat = AppSettings.Current.ChannelConfiguration.DateFormat;

                var query = await (new FormUrlEncodedContent(new
                {
                    keys = ids,
                    from_date = fromDate.ToString(dateFormat),
                    to_date = toDate.ToString(dateFormat)
                }.ConvertToKeyValuePair())).ReadAsStringAsync();

                var url = AppSettings.Current.ChannelConfiguration.ViewVideoSettings.ByDate;
                returnValue = await  HttpClientService.GetData2Async<ViewByDate[]>(url, query);
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
            }
            return returnValue;
        }

        public static async Task<object> GetIndexVideoAsync(string ids, DateTime fromDate, DateTime toDate)
        {
            var returnValue = default(object);
            try
            {
                var dateFormat = AppSettings.Current.ChannelConfiguration.DateFormat;

                var query = await (new FormUrlEncodedContent(new
                {
                    keys = ids,
                    from_date = fromDate.ToString(dateFormat),
                    to_date = toDate.ToString(dateFormat)
                }.ConvertToKeyValuePair())).ReadAsStringAsync();

                var url = AppSettings.Current.ChannelConfiguration.ViewVideoSettings.AllDate;
                returnValue = await HttpClientService.GetData2Async<object>(url, query);
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
            }
            return returnValue;
        }

        public static async Task<object> TopVideoAsync(long pageId, DateTime fromDate, DateTime toDate, int mode, string order, int limit, int page)
        {
            var returnValue = default(object);
            try
            {
                var dateFormat = AppSettings.Current.ChannelConfiguration.DateFormat;

                var query = await (new FormUrlEncodedContent(new
                {
                    pages = pageId.ToString(),
                    from_date = fromDate.ToString(dateFormat),
                    to_date = toDate.ToString(dateFormat),
                    type = mode,
                    order = order,
                    limit = limit,
                    page = page
                }.ConvertToKeyValuePair())).ReadAsStringAsync();

                var url = AppSettings.Current.ChannelConfiguration.ViewVideoSettings.TopVideo;
                returnValue = await HttpClientService.GetData2Async<object>(url, query);
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
            }
            return returnValue;
        }
        

        public static async Task<object> GetUserOverviewAsync(string pageIds, DateTime fromDate, DateTime toDate)
        {
            var returnValue = default(object);
            try
            {
                var dateFormat = AppSettings.Current.ChannelConfiguration.DateFormat;

                var query = await (new FormUrlEncodedContent(new
                {
                    user_ids = pageIds,
                    from_date = fromDate.ToString(dateFormat),
                    to_date = toDate.ToString(dateFormat)
                }.ConvertToKeyValuePair())).ReadAsStringAsync();

                
                var header = new Dictionary<string, string>();
                header.Add("app-ims-api-key", AppSettings.Current.ChannelConfiguration.ApiInteractionsStatisticsSetting.UserOverview.ApiKey);

                var url = AppSettings.Current.ChannelConfiguration.ApiInteractionsStatisticsSetting.UserOverview.Url;
                returnValue = await HttpClientService.GetData2Async<object>(url, query, header);
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
            }
            return returnValue;
        }

        public static async Task<object> GetActionByContentTypeAsync(long pageId, DateTime fromDate, DateTime toDate)
        {
            var returnValue = default(object);
            try
            {
                var dateFormat = AppSettings.Current.ChannelConfiguration.DateFormat;

                var query = await (new FormUrlEncodedContent(new
                {
                    userid = pageId,
                    from_date = fromDate.ToString(dateFormat),
                    to_date = toDate.ToString(dateFormat)
                }.ConvertToKeyValuePair())).ReadAsStringAsync();

                var header = new Dictionary<string, string>();
                header.Add("app-ims-api-key", AppSettings.Current.ChannelConfiguration.ApiInteractionsStatisticsSetting.OverViewAction.ApiKey);

                var url = AppSettings.Current.ChannelConfiguration.ApiInteractionsStatisticsSetting.OverViewAction.Url;
                returnValue = await HttpClientService.GetData2Async<object>(url, query, header);
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
            }
            return returnValue;
        }

        public static async Task<object> GetViewProfileAsync(long pageId, DateTime fromDate, DateTime toDate)
        {
            var returnValue = default(object);
            try
            {
                var dateFormat = AppSettings.Current.ChannelConfiguration.DateFormat;

                var query = await (new FormUrlEncodedContent(new
                {
                    userid = pageId,
                    from_date = fromDate.ToString(dateFormat),
                    to_date = toDate.ToString(dateFormat)
                }.ConvertToKeyValuePair())).ReadAsStringAsync();

                var header = new Dictionary<string, string>();
                header.Add("app-ims-api-key", AppSettings.Current.ChannelConfiguration.ApiInteractionsStatisticsSetting.ViewProfile.ApiKey);

                var url = AppSettings.Current.ChannelConfiguration.ApiInteractionsStatisticsSetting.ViewProfile.Url;
                returnValue = await HttpClientService.GetData2Async<object>(url, query, header);
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
            }
            return returnValue;
        }

        public static async Task<object> GetViewProfileByAgeAsync(long pageId, DateTime fromDate, DateTime toDate)
        {
            var returnValue = default(object);
            try
            {
                var dateFormat = AppSettings.Current.ChannelConfiguration.DateFormat;

                var query = await (new FormUrlEncodedContent(new
                {
                    userid = pageId,
                    from_date = fromDate.ToString(dateFormat),
                    to_date = toDate.ToString(dateFormat)
                }.ConvertToKeyValuePair())).ReadAsStringAsync();

                var header = new Dictionary<string, string>();
                header.Add("app-ims-api-key", AppSettings.Current.ChannelConfiguration.ApiInteractionsStatisticsSetting.ViewProfileByAge.ApiKey);

                var url = AppSettings.Current.ChannelConfiguration.ApiInteractionsStatisticsSetting.ViewProfileByAge.Url;
                returnValue = await HttpClientService.GetData2Async<object>(url, query, header);
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
            }
            return returnValue;
        }

        public static async Task<object> GetViewProfileByGenderAsync(long pageId, DateTime fromDate, DateTime toDate)
        {
            var returnValue = default(object);
            try
            {
                var dateFormat = AppSettings.Current.ChannelConfiguration.DateFormat;

                var query = await (new FormUrlEncodedContent(new
                {
                    userid = pageId,
                    from_date = fromDate.ToString(dateFormat),
                    to_date = toDate.ToString(dateFormat)
                }.ConvertToKeyValuePair())).ReadAsStringAsync();

                var header = new Dictionary<string, string>();
                header.Add("app-ims-api-key", AppSettings.Current.ChannelConfiguration.ApiInteractionsStatisticsSetting.ViewProfileByGender.ApiKey);

                var url = AppSettings.Current.ChannelConfiguration.ApiInteractionsStatisticsSetting.ViewProfileByGender.Url;
                returnValue = await HttpClientService.GetData2Async<object>(url, query, header);
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
            }
            return returnValue;
        }

        public static async Task<object> GetViewProfileByDeviceAsync(long pageId, DateTime fromDate, DateTime toDate)
        {
            var returnValue = default(object);
            try
            {
                var dateFormat = AppSettings.Current.ChannelConfiguration.DateFormat;

                var query = await (new FormUrlEncodedContent(new
                {
                    userid = pageId,
                    from_date = fromDate.ToString(dateFormat),
                    to_date = toDate.ToString(dateFormat)
                }.ConvertToKeyValuePair())).ReadAsStringAsync();

                var header = new Dictionary<string, string>();
                header.Add("app-ims-api-key", AppSettings.Current.ChannelConfiguration.ApiInteractionsStatisticsSetting.ViewProfileByDevice.ApiKey);

                var url = AppSettings.Current.ChannelConfiguration.ApiInteractionsStatisticsSetting.ViewProfileByDevice.Url;
                returnValue = await HttpClientService.GetData2Async<object>(url, query, header);
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
            }
            return returnValue;
        }

        public static async Task<object> GetViewProfileByLocationAsync(long pageId, DateTime fromDate, DateTime toDate)
        {
            var returnValue = default(object);
            try
            {
                var dateFormat = AppSettings.Current.ChannelConfiguration.DateFormat;

                var query = await (new FormUrlEncodedContent(new
                {
                    userid = pageId,
                    from_date = fromDate.ToString(dateFormat),
                    to_date = toDate.ToString(dateFormat)
                }.ConvertToKeyValuePair())).ReadAsStringAsync();

                var header = new Dictionary<string, string>();
                header.Add("app-ims-api-key", AppSettings.Current.ChannelConfiguration.ApiInteractionsStatisticsSetting.ViewProfileByLocation.ApiKey);

                var url = AppSettings.Current.ChannelConfiguration.ApiInteractionsStatisticsSetting.ViewProfileByLocation.Url;
                returnValue = await HttpClientService.GetData2Async<object>(url, query, header);
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
            }
            return returnValue;
        }


        public static async Task<object> GetFollowPageAsync(string pageIds, DateTime fromDate, DateTime toDate)
        {
            var returnValue = default(object);
            try
            {
                var dateFormat = AppSettings.Current.ChannelConfiguration.DateFormat;

                var query = await (new FormUrlEncodedContent(new
                {
                    user_ids = pageIds,
                    from_date = fromDate.ToString(dateFormat),
                    to_date = toDate.ToString(dateFormat)
                }.ConvertToKeyValuePair())).ReadAsStringAsync();

                var header = new Dictionary<string, string>();
                header.Add("app-ims-api-key", AppSettings.Current.ChannelConfiguration.ApiInteractionsStatisticsSetting.Follow.ApiKey);

                var url = AppSettings.Current.ChannelConfiguration.ApiInteractionsStatisticsSetting.Follow.Url;
                returnValue = await HttpClientService.GetData2Async<object>(url, query, header);
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
            }
            return returnValue;
        }

        #region CHINHNB DashBoard
        public static async Task<object> TopPostAsync(long pageId, DateTime fromDate, DateTime toDate, int mode, string activity, int limit, int page, string sessionId)
        {
            var returnValue1 = default(TopPostResponse);
            var returnValue2 = default(TopReachPostResponse);
            try
            {
                var dateFormat = AppSettings.Current.ChannelConfiguration.DateFormat;

                using (var client = new HttpClient())
                {
                    client.Timeout = TimeSpan.FromSeconds(AppSettings.Current.ChannelConfiguration.HttpClientTimeOut);
                    client.DefaultRequestHeaders.Add("app-ims-api-key", AppSettings.Current.ChannelConfiguration.ApiInteractionsStatisticsSetting.TopPostOfPage.ApiKey);

                    var url = AppSettings.Current.ChannelConfiguration.ApiInteractionsStatisticsSetting.TopPostOfPage.Url;
                    if (OrderEnum.clicks.ToString().Equals(activity) || OrderEnum.views.ToString().Equals(activity)|| OrderEnum.reachs.ToString().Equals(activity))
                    {
                        url = AppSettings.Current.ChannelConfiguration.ApiInteractionsStatisticsSetting.TopReachOfPostOfPage.Url;
                    }

                    var query = await (new FormUrlEncodedContent(new
                    {
                        activity = activity,
                        from_date = fromDate.ToString("yyyy-MM-dd"),
                        to_date = toDate.ToString("yyyy-MM-dd"),
                        limit = limit.ToString(),
                        offset = (page - 1) * limit,
                        userid = pageId.ToString()
                    }.ConvertToKeyValuePairForGetMethod())).ReadAsStringAsync();

                    var path = url + "?" + query;
                    Logger.Debug("Query top post=>" + path);
                    var response = await client.GetAsync(path);
                    if (!response.IsSuccessStatusCode)
                    {
                        response.EnsureSuccessStatusCode();
                    }

                    if(response.StatusCode == HttpStatusCode.OK)
                    {
                        var astr = await response.Content?.ReadAsStringAsync();
                        
                        Logger.Debug("Response top post=>" + astr);
                        if (OrderEnum.clicks.ToString().Equals(activity) || OrderEnum.views.ToString().Equals(activity) || OrderEnum.reachs.ToString().Equals(activity))
                        {
                            returnValue2 = Json.Parse<TopReachPostResponse>(astr);
                            if (returnValue2 != null && returnValue2.list?.Count() > 0)
                            {
                                var listId = new List<string>();
                                foreach (var post in returnValue2.list)
                                {
                                    listId.Add(post?.postId);
                                }
                                //Logger.Debug("list Id Adtech =>" + Json.Stringify(listId));
                                var dict = await GetListIdOnAdtechAsync(listId, sessionId);

                                for (var i = 0; i < returnValue2.list.Count(); i++)
                                {
                                    try
                                    {
                                        var media = dict.GetValueOrDefault(returnValue2.list[i]?.postId);
                                        //var item = listPost?.Where(w => w.Id.ToString().Equals(mediaId))?.FirstOrDefault();
                                        returnValue2.list[i].id = media?.media_id.ToString();
                                        returnValue2.list[i].avatar = "";// item?.Avatar;
                                        returnValue2.list[i].distibutionDate = media?.data?.First?.created_at?.ToString("yyyy-MM-ddThh:mm:ss.fffZ");
                                        returnValue2.list[i].title = media?.title;
                                        returnValue2.list[i].mediaUnitDesc = media?.data;
                                    }
                                    catch (Exception ex)
                                    {
                                        Logger.Sensitive(ex, ex.Message);
                                        Logger.Debug(ex, ex.Message);
                                    }
                                };
                            }

                            return returnValue2;
                        }
                        else
                        {
                            returnValue1 = Json.Parse<TopPostResponse>(astr);
                            if (returnValue1 != null && returnValue1.list?.Count() > 0)
                            {
                                var listId = new List<string>();
                                foreach (var post in returnValue1.list)
                                {
                                    listId.Add(post?.key);
                                }
                                //Logger.Debug("list Id Adtech =>" + Json.Stringify(listId));
                                var dict = await GetListIdOnAdtechAsync(listId, sessionId);

                                for (var i = 0; i < returnValue1.list.Count(); i++)
                                {
                                    try
                                    {
                                        var media = dict.GetValueOrDefault(returnValue1.list[i]?.key);
                                        //var item = listPost?.Where(w => w.Id.ToString().Equals(mediaId))?.FirstOrDefault();
                                        returnValue1.list[i].id = media?.media_id.ToString();
                                        returnValue1.list[i].avatar = "";// item?.Avatar;
                                        returnValue1.list[i].distibutionDate = media?.data?.First?.created_at?.ToString("yyyy-MM-ddThh:mm:ss.fffZ");
                                        returnValue1.list[i].title = media?.title;
                                        returnValue1.list[i].mediaUnitDesc = media?.data;
                                    }
                                    catch (Exception ex)
                                    {
                                        Logger.Sensitive(ex, ex.Message);
                                        Logger.Debug(ex, ex.Message);
                                    }
                                };
                            }

                            return returnValue1;
                        }
                    }
                    else
                    {
                        Logger.Sensitive("DataGetTopPost=>", await response.Content?.ReadAsStringAsync());
                        Logger.Debug("DataGetTopPost=>", await response.Content?.ReadAsStringAsync());
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Sensitive(ex, ex.Message);
                Logger.Debug(ex, ex.Message);
            }            

            return null;
        }

        private static async Task<Dictionary<string, dynamic>> GetListIdOnAdtechAsync(List<string> listId, string sessionId)
        {
            var listIdCms = new Dictionary<string, dynamic>();
            try
            {
                using(var client = new HttpClient())
                {
                    client.Timeout = TimeSpan.FromSeconds(AppSettings.Current.ChannelConfiguration.HttpClientTimeOut);
                    var url = AppSettings.Current.ChannelConfiguration.ApiInteractionsStatisticsSetting.CardInfo.Url;
                    var query = await (new FormUrlEncodedContent(new
                    {
                        postids = string.Join(",", listId)
                    }.ConvertToKeyValuePairForGetMethod())).ReadAsStringAsync();

                    client.DefaultRequestHeaders.Add("session-id", sessionId);
                    var path = url + "?" + query;

                    var response = await client.GetAsync(path);
                    if (!response.IsSuccessStatusCode)
                    {
                        response.EnsureSuccessStatusCode();
                    }

                    if (response.StatusCode == HttpStatusCode.OK)
                    {
                        var astr = await response.Content?.ReadAsStringAsync();

                        var returnValue = Json.Parse<dynamic>(astr);
                        if (returnValue != null && returnValue.result?.data != null)
                        {
                            foreach(var post in returnValue.result?.data)
                            {
                                listIdCms.Add(post?.id?.ToString(), post);
                            }
                        }
                    }
                }
            }catch(Exception ex)
            {
                Logger.Error(ex, ex.Message);
            }
            return listIdCms;
        }

        public static async Task<object> TopAccountAsync(long pageId, DateTime fromDate, DateTime toDate, int mode, string order, int limit, int page, string keyword = "")
        {
            var returnValue = default(object);
            try
            {
                var dateFormat = AppSettings.Current.ChannelConfiguration.DateFormat;

                var query = await (new FormUrlEncodedContent(new
                {
                    keyword,
                    pages = pageId.ToString(),
                    from_date = fromDate.ToString(dateFormat),
                    to_date = toDate.ToString(dateFormat),
                    type= mode,
                    order,
                    limit,
                    page
                }.ConvertToKeyValuePair())).ReadAsStringAsync();

                var url = AppSettings.Current.ChannelConfiguration.ViewVideoSettings.TopVideo;
                returnValue = await HttpClientService.GetData2Async<object>(url, query);
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
            }
            return returnValue;
        }

        #endregion
    }
}
