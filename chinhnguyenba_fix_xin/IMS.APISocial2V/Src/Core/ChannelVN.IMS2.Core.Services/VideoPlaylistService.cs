﻿using ChannelVN.IMS2.Core.Entities;
using ChannelVN.IMS2.Core.Entities.Videos;
using ChannelVN.IMS2.Core.Repositories;
using ChannelVN.IMS2.Foundation.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using static ChannelVN.IMS2.Core.Entities.ErrorCode.ErrorMapping;

namespace ChannelVN.IMS2.Core.Services
{
    public static class VideoPlaylistService
    {
        public static Task<ErrorCodes> AddAsync(VideoPlaylistSearch data, string clientIP)
        {
            return VideoPlaylistRepository.AddAsync(data, clientIP);
        }

        public static Task<ErrorCodes> UpdateAsync(VideoPlaylistSearch data, long officerId, string clientIP, string sessionId)
        {
            return VideoPlaylistRepository.UpdateAsync(data, officerId, clientIP, sessionId);
        }

        public static Task<ErrorCodes> UpdateStatusV2Async(long id, NewsStatus status, long userId, string clientIP, string sessionId)
        {
            return VideoPlaylistRepository.UpdateStatusV2Async(id, status, userId, clientIP, sessionId);
        }

        public static Task<PagingDataResult<VideoPlaylistSearchReturn>> SearchAsync(SearchNews data, long userId, bool isGetDistribution = false)
        {
            return VideoPlaylistRepository.SearchAsync(data, userId, isGetDistribution);
        }
        public static Task<PagingDataResult<VideoPlaylistSearch>> GetListApproveAsync(SearchNews data, long accountId)
        {
            return VideoPlaylistRepository.GetListApproveAsync(data, accountId);
        }

        public static Task<PagingDataResult<VideoInPlaylistSearchReturn>> SearchVideoAsync(SearchNews data, long userId)
        {
            return VideoPlaylistRepository.SearchVideoAsync(data, userId);
        }
        public static Task<VideoPlaylist> GetByIdAsync(long id)
        {
            return VideoPlaylistRepository.GetByIdAsync(id);
        }
        public static Task<VideoPlaylistSearch> GetVideoPlaylistSearchByIdAsync(long id)
        {
            return VideoPlaylistRepository.GetVideoPlaylistSearchByIdAsync(id);
        }

        public static Task<PagingDataResult<VideoPlaylistSearch>> GetListDistributionAsync(SearchNews searchEntity, long userId)
        {
            return VideoPlaylistRepository.GetListDistributionAsync(searchEntity, userId);
        }

        public static Task<long> GetCountByStatusAsync(int status, long officialId, long userId)
        {
            return VideoPlaylistRepository.GetCountByStatusAsync(status, officialId, userId);
        }

        public static Task<PagingDataResult<VideoInPlaylistSearchReturn>> GetListVideoAsync(List<string> videoIds, int? pageIndex, int? pageSize)
        {
            return VideoPlaylistRepository.GetListVideoAsync(videoIds, pageIndex, pageSize);
        }

        public static Task<PagingDataResult<VideoPlaylistSearch>> GetListRelationAsync(string videoPlaylistIds, int? pageIndex, int? pageSize)
        {
            return VideoPlaylistRepository.GetListRelationAsync(videoPlaylistIds, pageIndex, pageSize);
        }

        public static Task<List<VideoPlaylist>> GetListByIdAsync(long[] ids)
        {
            return VideoPlaylistRepository.GetListByIdAsync(ids?.Select(p => p.ToString())?.ToList());
        }

        public static Task<PagingDataResult<Video>> GetVideoByPlaylistIdAsync(long playlistId, int? pageIndex, int? pageSize)
        {
            return VideoPlaylistRepository.GetVideoByPlaylistIdAsync(playlistId, pageIndex, pageSize);
        }
    }
}
