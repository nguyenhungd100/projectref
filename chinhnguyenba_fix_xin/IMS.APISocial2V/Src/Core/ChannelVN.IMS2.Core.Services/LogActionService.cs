﻿using ChannelVN.IMS2.Foundation.Logging;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace ChannelVN.IMS2.Core.Services
{
    public static class LogActionService
    {
        public static Task<List<LogActionEntity>> SearchLog(string keyword, int pageIndex, int pageSize, long objectId, string sourceId, int[] actionTypeDetail, int[] type, DateTime dateFrom, DateTime dateTo)
        {
            return LogActionNodeJs.SearchLog(keyword, pageIndex, pageSize, objectId, sourceId, actionTypeDetail, type, dateFrom, dateTo);
        }
    }
}
