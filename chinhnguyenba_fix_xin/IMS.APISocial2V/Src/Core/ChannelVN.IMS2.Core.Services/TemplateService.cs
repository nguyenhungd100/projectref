﻿using ChannelVN.IMS2.Core.Entities.Media;
using ChannelVN.IMS2.Core.Repositories;
using ChannelVN.IMS2.Foundation.Common;
using ChannelVN.IMS2.Foundation.Logging;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace ChannelVN.IMS2.Core.Services
{
    public static class TemplateService
    {
        public static Task<bool> AddAsync(Template data)
        {
            return TemplateRepository.AddAsync(data);
        }

        public static Task<bool> UpdateAsync(Template data)
        {
            return TemplateRepository.UpdateAsync(data);
        }

        public static async Task<bool> UpdateStatusAsync(long id, int status, long userId)
        {
            var data = await TemplateRepository.GetByIdAsync(id);
            if (data == null)
            {
                return false;
            }
            data.Status = status;
            data.ModifiedBy = userId.ToString();
            data.ModifiedDate = DateTime.Now;

            return await TemplateRepository.UpdateStatusAsync(data);
        }

        public static Task<Template> GetByIdAsync(long id)
        {
            return TemplateRepository.GetByIdAsync(id);
        }

        public static async Task<PagingDataResult<Template>> SearchAsync(string keyword,int CategoryId, int? status, int pageIndex, int pageSize)
        {
            var data = await TemplateRepository.SearchAsync(keyword, CategoryId, status, pageIndex, pageSize);
            if(!string.IsNullOrEmpty(keyword))
            {
                if (data?.Data?.Count > 0)
                {
                    data.Data = await TemplateRepository.GetListByIdsAsync(data.Data.Select(s => s.Id.ToString()).ToList());
                }
            }
            return data;
        }

        public static async Task<PagingDataResult<Template>> SearchTestAsync(string keyword, int? status, int pageIndex, int pageSize)
        {
            var data = new PagingDataResult<Template>
            {
                Data = new System.Collections.Generic.List<Template> {
                    new Template
                    {
                        Id=1
                    },
                    new Template
                    {
                        Id=2
                    }
                }
            };
            if (data?.Data?.Count > 0)
            {
                data.Data = await TemplateRepository.GetListByIdsAsync(data.Data.Select(s => s.Id.ToString()).ToList());
            }
            return data;
        }
    }
}
