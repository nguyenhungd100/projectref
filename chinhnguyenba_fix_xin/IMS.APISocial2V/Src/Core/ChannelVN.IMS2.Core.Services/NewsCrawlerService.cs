﻿using ChannelVN.IMS2.Core.Entities;
using ChannelVN.IMS2.Core.Entities.Media;
using ChannelVN.IMS2.Core.Entities.ShareLinks;
using ChannelVN.IMS2.Core.Repositories;
using ChannelVN.IMS2.Foundation.Common;
using ChannelVN.IMS2.Foundation.Common.Configuration;
using ChannelVN.IMS2.Foundation.Logging;
using HtmlAgilityPack;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using static ChannelVN.IMS2.Core.Entities.ErrorCode.ErrorMapping;
using System.Drawing;
using ChannelVN.IMS2.Core.Services.VietId;
using TelegramNotification = ChannelVN.IMS2.Foundation.Logging.TelegramNotification;

namespace ChannelVN.IMS2.Core.Services
{
    public class NewsCrawlerService
    {
        private static readonly object _object = new object();
        public static async Task<ErrorCodes> Crawler(string link, string userName, long officerId)
        {
            try
            {

                var url = AppSettings.Current.ChannelConfiguration.ApiNewsCrawlerUrl;
                var data = await HttpClientGetDataAsync(url, string.Format("url={0}", link));

                if (data != null && data.lstLink.Count > 0)
                {
                    var muiltTask = new List<Task>();
                    var listLinkInfo = new List<NewsCrawler>();
                    foreach (var itemlink in data.lstLink.Distinct().ToList())
                    {
                        muiltTask.Add(Task.Run(async () =>
                        {
                            var linkInfo = await GetDetailInfo(link, itemlink, userName, officerId);
                            lock (_object)
                            {
                                if (linkInfo != null && !listLinkInfo.Contains(linkInfo))
                                {
                                    listLinkInfo.Add(linkInfo);
                                }
                            }
                        }));
                    }
                    Task.WaitAll(muiltTask.ToArray());

                    if (listLinkInfo.Count > 0)
                    {
                        if (await AddAsync(listLinkInfo))
                            return ErrorCodes.Success;
                        return ErrorCodes.BusinessError;
                    }
                    return ErrorCodes.DataExist;
                }
                return ErrorCodes.DataNotExist;
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
                return ErrorCodes.BusinessError;
            }
        }
        private static async Task<ResponseLinkDataModel> HttpClientGetDataAsync(string url, string query, Dictionary<string, string> headerParams = null)
        {
            var result = default(ResponseLinkDataModel);
            string path = url + "?" + query;
            try
            {
                using (var client = new HttpClient())
                {
                    if (headerParams != null && headerParams.Count > 0)
                    {
                        foreach (var param in headerParams)
                        {
                            client.DefaultRequestHeaders.Add(param.Key, param.Value);
                        }
                    }

                    client.Timeout = TimeSpan.FromSeconds(AppSettings.Current.ChannelConfiguration.HttpClientTimeOut);

                    //Logger.Debug("Start call Adtech:" + DateTime.Now.ToString("HH:mm:ss.fff"));

                    var response = await client.GetAsync(path);
                    if (!response.IsSuccessStatusCode)
                    {
                        TelegramNotification.SendMessageAsync(Json.Stringify(new { api = path, response = response.ToString()}));
                        response.EnsureSuccessStatusCode();
                    }

                    var astr = await response.Content?.ReadAsStringAsync();
                    //result.lstLink = Json.Parse<List<string>>(astr);
                    result = Json.Parse<ResponseLinkDataModel>(astr);
                    //Logger.Debug("End parse:" + DateTime.Now.ToString("HH:mm:ss.fff"));
                }
            }
            catch (Exception ex)
            {
                TelegramNotification.SendMessageAsync(Json.Stringify(new { api = path, response = ex.Message }));
                Logger.Error(ex, ex.Message);
                result = new ResponseLinkDataModel();
            }
            return result;
        }
        private static async Task<NewsCrawler> GetDetailInfo(string source, string link, string accountName, long officerId)
        {
            try
            {
                //check link tồn tại
                var infoLinkDb = await NewsCrawlerRepository.GetByLinkAsync(link, officerId);
                if (infoLinkDb != null)
                {
                    //đã tồn tại ko insert nữa
                    return null;
                }

                var linkInfo = new NewsCrawler
                {
                    Id = Generator.NewsId(),
                    Source = source,
                    Link = link,
                    CrawledBy = accountName,
                    CrawledDate = DateTime.Now,
                    Status = (int)EnumStatusNewsCrawler.Default,
                    OfficerId = officerId
                };
                string strHtml = RetrieveData(link, false);
                var doc = new HtmlDocument();
                doc.LoadHtml(strHtml);

                //title
                HtmlNode meta = doc.DocumentNode.Descendants().FirstOrDefault(t => t.Name == "meta" && t.Attributes["property"] != null
                && t.Attributes["property"].Value.Equals("og:title"));
                if (meta != null)
                {
                    linkInfo.Title = HttpUtility.HtmlDecode(meta.Attributes["content"].Value.ToString());
                }
                else
                {
                    meta = doc.DocumentNode.Descendants().FirstOrDefault(t => t.Name.Equals("title"));
                    linkInfo.Title = meta == null ? "" : HttpUtility.HtmlDecode(meta.FirstChild.OuterHtml);
                }
                //check title trống thì bỏ
                if(string.IsNullOrEmpty(linkInfo.Title)) return null;

                //avatar
                meta = doc.DocumentNode.Descendants().FirstOrDefault(t => t.Name == "meta" && t.Attributes["property"] != null
                && t.Attributes["property"].Value.Equals("og:image"));
                if (meta != null)
                {
                    linkInfo.Avatar = meta.Attributes["content"].Value.ToString();
                }
                //check avatar trống thì bỏ
                if (string.IsNullOrEmpty(linkInfo.Title)) return null;

                //Des
                meta = doc.DocumentNode.Descendants().FirstOrDefault(t => t.Name == "meta" && t.Attributes["property"] != null
                && t.Attributes["property"].Value.Equals("og:description"));
                if (meta != null)
                {
                    linkInfo.Description = HttpUtility.HtmlDecode(meta.Attributes["content"].Value.ToString());
                }
                else
                {
                    meta = doc.DocumentNode.Descendants().FirstOrDefault(t => t.Name == "meta" && t.Attributes["name"] != null
                    && t.Attributes["name"].Value.Equals("description"));
                    linkInfo.Description = meta == null ? "" : HttpUtility.HtmlDecode(meta.Attributes["content"].Value.ToString());
                }
                return linkInfo;
            }
            catch
            {
                return null;
            }
        }
        private static string RetrieveData(string url, bool isMobile)
        {
            var sb = new StringBuilder();

            var buf = new byte[8192];
            try
            {
                var request = (HttpWebRequest)
                                         WebRequest.Create(url);
                if (isMobile)
                {
                    request.UserAgent = "Mozilla/5.0 (iPhone; CPU iPhone OS 11_0 like Mac OS X) AppleWebKit/604.1.38 (KHTML, like Gecko) Version/11.0 Mobile/15A372 Safari/604.1";
                }

                var response = (HttpWebResponse)request.GetResponse();

                var resStream = response.GetResponseStream();

                string tempString = null;
                int count = 0;
                do
                {
                    count = resStream.Read(buf, 0, buf.Length);

                    if (count != 0)
                    {
                        tempString = Encoding.UTF8.GetString(buf, 0, count);

                        sb.Append(tempString);
                    }
                } while (count > 0);

            }
            catch
            {
                return "";
            }
            return HttpUtility.HtmlDecode(sb.ToString());
        }
        public static Task<bool> AddAsync(List<NewsCrawler> data)
        {
            return NewsCrawlerRepository.AddAsync(data);
        }

        public static Task<PagingDataResult<NewsCrawler>> SearchAsync(string keyword, long officerId, int? status, int pageIndex, int pageSize, OrderBy? orderBy, DateTime fromDate, DateTime toDate)
        {
            return NewsCrawlerRepository.SearchAsync(keyword, officerId, status, pageIndex, pageSize, orderBy, fromDate, toDate);
        }

        public static async Task<ErrorCodes> SetScheduleByIdAsync(NewsCrawler data, long userId, string clientIP, string sessionId)
        {
            var objDb = await GetByIdAsync(data.Id);
            if (objDb != null)
            {
                objDb.Status = data.Status;
                objDb.ScheduleDate = data.ScheduleDate;
                objDb.ScheduleBy = data.ScheduleBy;

                var imgWidth = 0;
                var imgHeight = 0;
                try
                {
                    if (!string.IsNullOrEmpty(objDb.Avatar))
                    {
                        byte[] imageData = new WebClient().DownloadData(objDb.Avatar);
                        var imgStream = new MemoryStream(imageData);
                        using (var image = Image.FromStream(imgStream))
                        {
                            imgWidth = image.Width;
                            imgHeight = image.Height;
                        }
                    }
                }
                catch (Exception ex)
                {
                    Logger.Error("FreeImageBitmap => url: " + objDb.Avatar + ", id: " + data.Id + ", ex: " + ex.Message);
                }

                var shareLink = new ShareLinkSearch
                {
                    Type = (int)NewsType.ShareLink,
                    CardType = (int)CardType.ShareNews,
                    Id = Generator.NewsId(),
                    Status = (int)NewsStatus.Draft,
                    CreatedDate = DateTime.Now,
                    CategoryId = 0,
                    PublishMode = (int)NewsPublishMode.Public
                };

                //build client
                shareLink.Avatar = objDb.Avatar;
                shareLink.Source = objDb.Source;
                shareLink.Title = objDb.Title;
                shareLink.PublishData = Json.Stringify(new
                {
                    isInstantView = false,
                    caption = objDb.Title,
                    captionExt = new dynamic[] {
                        new {
                            type = "text",
                            text=objDb.Description
                        }
                        //new {
                        //    type = "link",
                        //    link=objDb.Link,
                        //    text=objDb.Link
                        //}
                    },
                    captionMetaData = "{}",
                    items = new dynamic[] {
                        new
                        {
                            title=objDb.Title,
                            original_url=objDb.Link,
                            link=objDb.Link,
                            source=objDb.Source,
                            image = new
                            {
                                thumb=objDb.Avatar,
                                height=imgHeight,
                                width=imgWidth,
                                content_type=2
                            },
                            dataType=0
                        }
                    }
                });

                shareLink.DistributionDate = data.ScheduleDate;
                shareLink.DistributorId = shareLink.DistributorId.HasValue ? shareLink.DistributorId : AppSettings.Current.Distribution.Automation.DistributorId;
                shareLink.CreatedBy = userId.ToString();
                shareLink.NewsInAccount = new[] {
                            new NewsInAccount
                            {
                                AccountId = objDb.OfficerId,
                                NewsId=shareLink.Id,
                                PublishedDate=DateTime.Now,
                                PublishedType=(int)NewsPublishedType.Post
                            }
                        };
                shareLink.Url = string.Format(AppSettings.Current.ChannelConfiguration.CommonSettings.UrlFormatVideo, Utility.UnicodeToUnsignedAndDash(shareLink.GetType().Name), shareLink.Id);
                if (string.IsNullOrEmpty(shareLink.OriginalUrl))
                    shareLink.OriginalUrl = shareLink.Url;


                var status = NewsStatus.Published;

                var result = await ShareLinkService.AddAndUpdateStatusAsync(shareLink, status, userId, clientIP, sessionId);
                if (result == ErrorCodes.Success)
                {
                    objDb.MediaId = shareLink.Id;
                    await NewsCrawlerRepository.UpdateAsync(objDb);
                    //await NewsCrawlerRepository.SetScheduleAsync(objDb);
                }
                return result;
            }
            return ErrorCodes.DataNotExist;
        }
        public static async Task<bool> RemoveAsync(NewsCrawler data)
        {
            var objDb = await GetByIdAsync(data.Id);
            if (objDb != null)
            {
                objDb.Status = data.Status;

                var result = await NewsCrawlerRepository.UpdateAsync(objDb);

                return result;
            }
            return false;
        }
        public static async Task<bool> UpdateAsync(NewsCrawler data)
        {
            var objDb = await GetByIdAsync(data.Id);
            objDb.Status = data.Status;
            objDb.ScheduleDate = data.ScheduleDate;
            objDb.ScheduleBy = data.ScheduleBy;

            return await NewsCrawlerRepository.UpdateAsync(objDb);
        }
        public static Task<NewsCrawler> GetByIdAsync(long id)
        {
            return NewsCrawlerRepository.GetByIdAsync(id);
        }

        public static Task<NewCrawlerConfigurationSearch> GetConfigByIdAsync(long id)
        {
            return NewsCrawlerRepository.GetConfigByIdAsync(id);
        }

        public static Task<ErrorCodes> AddConfigAsync(NewCrawlerConfigurationSearch crawlerConfig, string clientIp)
        {
            return NewsCrawlerRepository.AddConfigAsync(crawlerConfig, clientIp);
        }

        public static async Task<ErrorCodes> UpdateConfigAsync(NewCrawlerConfigurationSearch crawlerConfig, string clientIp)
        {
            var config = await GetConfigByIdAsync(crawlerConfig.Id);
            config.IntervalTime = crawlerConfig.IntervalTime ?? config.IntervalTime;
            config.PublishMode = crawlerConfig.PublishMode ?? config.PublishMode;
            return await NewsCrawlerRepository.UpdateConfigAsync(config);
        }


        public static Task<PagingDataResult<NewCrawlerConfigurationSearch>> SearchConfigAsync(string keyword, long officerId, int? status, int pageIndex, int pageSize, ConfigOrderBy? orderBy, DateTime fromDate, DateTime toDate)
        {
            return NewsCrawlerRepository.SearchConfigAsync(keyword, officerId, status, pageIndex, pageSize, orderBy, fromDate, toDate);
        }

        public static async Task<bool> RemoveConfigAsync(NewCrawlerConfigurationSearch data)
        {
            var objDb = await GetConfigByIdAsync(data.Id);
            if (objDb != null)
            {
                objDb.Status = data.Status;

                var result = await NewsCrawlerRepository.UpdateConfigAsync(objDb);
                if (result == ErrorCodes.Success)
                {
                    return true;
                }
            }
            return false;
        }

        public static async Task AddLinksAutoJobAsync(List<NewCrawlerConfigurationSearch> data)
        {
            foreach (var item in data)
            {
                var url = AppSettings.Current.ChannelConfiguration.ApiNewsCrawlerUrl;
                var links = await HttpClientGetDataAsync(url, string.Format("url={0}", item.Source));

                if (links != null && links.lstLink.Count > 0)
                {

                }
            }
        }
        public static async Task<List<NewCrawlerConfigurationSearch>> CheckListSearchConfigFromCache()
        {
            if (!await NewsCrawlerRepository.ExistListSearchConfigAsync())
            {
                var searchConfig = await SearchConfigAsync("", 0, (int)EnumStatusCrawlerConfig.EnableJob, 1, 10000, ConfigOrderBy.CreatedDate_DESC, DateTime.MinValue, DateTime.MaxValue);
                if (searchConfig != null && searchConfig.Data != null && searchConfig.Data.Count > 0)
                    await NewsCrawlerRepository.PushListConfigSearchToCacheAsync(searchConfig.Data);
                return searchConfig.Data;
            }
            else
            {
                return await NewsCrawlerRepository.GetListConfigSearchFromCacheAsync();
            }
        }

        public static async Task<ErrorCodes> HandlingCrawlerPublish(string link, string userName, long officerId, PublishModeStatus? mode)
        {
            try
            {
                var url = AppSettings.Current.ChannelConfiguration.ApiNewsCrawlerUrl;
                var data = await HttpClientGetDataAsync(url, string.Format("url={0}", link));

                if (data != null && data.lstLink.Count > 0)
                {
                    var muiltTask = new List<Task>();
                    var listLinkInfo = new List<NewsCrawler>();
                    foreach (var itemlink in data.lstLink.Distinct().ToList())
                    {
                        muiltTask.Add(Task.Run(async () =>
                        {
                            var linkInfo = await GetDetailInfo(link, itemlink, userName, officerId);
                            lock (_object)
                            {
                                if (linkInfo != null && !listLinkInfo.Contains(linkInfo))
                                {
                                    if (!string.IsNullOrEmpty(linkInfo.Title) && !string.IsNullOrEmpty(linkInfo.Avatar))
                                    {
                                        if (mode == PublishModeStatus.AutoPublish)
                                        {
                                            linkInfo.Status = (int)EnumStatusNewsCrawler.Publish;
                                        }
                                        listLinkInfo.Add(linkInfo);
                                    }
                                }
                            }
                        }));
                    }
                    Task.WaitAll(muiltTask.ToArray());

                    if (listLinkInfo.Count > 0)
                    {
                        if (await AddAsync(listLinkInfo))
                        {
                            var metaData = await DataVietIdService.GetAccessToken(userName);
                            string sessionId = (await DataVietIdService.GetSessionId(metaData?.Datas?.AccessToken))?.data?.sessionId;
                            if (string.IsNullOrEmpty(sessionId))
                                return ErrorCodes.BusinessError;

                            if (mode == PublishModeStatus.AutoPublish)
                            {
                                var dateCurrent = DateTime.Now;
                                var muiltTaskPublish = new List<Task>();
                                foreach (var dataLink in listLinkInfo)
                                {
                                    dataLink.ScheduleDate = dateCurrent.AddMinutes(1);
                                    muiltTask.Add(Task.Run(async () =>
                                    {
                                        await SetScheduleByIdAsync(dataLink, Utility.ConvertToLong(userName), "", "");
                                    }));
                                }
                                Task.WaitAll(muiltTask.ToArray());
                            }
                            return ErrorCodes.Success;
                        }
                        return ErrorCodes.BusinessError;
                    }
                    return ErrorCodes.DataExist;
                }
                return ErrorCodes.DataNotExist;
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
                return ErrorCodes.BusinessError;
            }
        }

        public static List<NewCrawlerConfigurationSearch> GetListScanCrawlerConfig(List<NewCrawlerConfigurationSearch> listData)
        {
            var timeNow = DateTime.Now;
            var listToScans = new List<NewCrawlerConfigurationSearch>();
            foreach (var item in listData)
            {
                var intervalTime = (timeNow - (item.LastScanTime ?? DateTime.MinValue)).TotalSeconds;
                if (intervalTime > item.IntervalTime)
                {
                    listToScans.Add(item);
                }
            }
            return listToScans;
        }

        public static async void JobAutoNewsCrawlerSetting()
        {
            var listData = new List<NewCrawlerConfigurationSearch>();

            //B1: Chech có list config trong cache
            // Nếu có thì lấy list config từ cache
            // Nếu không thì search lại và đẩy vào cache list config và set expire
            listData = await CheckListSearchConfigFromCache();

            if (listData != null && listData.Count > 0)
            {
                if (!NewsCrawlerRepository.CheckExistQueueAutoCrawlerConfig())
                {
                    //B2 Check link source đã đến thời điểm cần quét chưa?
                    var listToScans = GetListScanCrawlerConfig(listData);

                    //B3 Push các linh source đến thời điểm cần quét vào hàng đợi
                    NewsCrawlerRepository.PushQueueAutoCrawlerConfig(listToScans);
                }
                else
                {
                    var flagJob = true;
                    if (flagJob)
                    {
                        flagJob = false;
                        while (true)
                        {
                            var popCrawler = NewsCrawlerRepository.PopQueueAutoCrawlerConfig();

                            if (popCrawler != null)
                            {
                                //if (popCrawler.LastScanTime == null)
                                //    popCrawler.LastScanTime = DateTime.MinValue;
                                //var intervalTime = (DateTime.Now - popCrawler.LastScanTime).Value.TotalSeconds;

                                var flagError = ErrorCodes.BusinessError;
                                //B4: Xử lý quét, crawler và xuất bản cho từng link source
                                //B5: Crawler list tin cho link source
                                //B6: Check mode
                                flagError = await HandlingCrawlerPublish(popCrawler.Source, popCrawler.CreatedBy, popCrawler.OfficerId, popCrawler.PublishMode);
                                Console.WriteLine(string.Format("{0},{1},{2},{3}", popCrawler.Source, 0, popCrawler.PublishMode, flagError));

                                if (flagError == ErrorCodes.Success)
                                {
                                    //update jobDate
                                   await NewsCrawlerRepository.UpdateJobDateConfigAsync(popCrawler.Id, DateTime.Now);
                                }
                            }
                            else
                            {
                                flagJob = false;
                                break;
                            }
                        }
                        //het queue
                        flagJob = true;
                    }
                }
            }
        }

        public static async Task<ErrorCodes> CrawlerPublish(string link, string userName, long officerId)
        {
            try
            {
                var url = AppSettings.Current.ChannelConfiguration.ApiNewsCrawlerUrl;
                var data = await HttpClientGetDataAsync(url, string.Format("url={0}", link));

                if (data != null && data.lstLink.Count > 0)
                {
                    var muiltTask = new List<Task>();
                    var listLinkInfo = new List<NewsCrawler>();
                    foreach (var itemlink in data.lstLink.Distinct().ToList())
                    {
                        muiltTask.Add(Task.Run(async () =>
                        {
                            var linkInfo = await GetDetailInfo(link, itemlink, userName, officerId);
                            lock (_object)
                            {
                                if (linkInfo != null && !listLinkInfo.Contains(linkInfo))
                                {
                                    linkInfo.Status = (int)EnumStatusNewsCrawler.Publish;
                                    listLinkInfo.Add(linkInfo);
                                }
                            }
                        }));
                    }
                    Task.WaitAll(muiltTask.ToArray());

                    if (listLinkInfo.Count > 0)
                    {
                        if (await AddAsync(listLinkInfo))
                        {
                            //lay sesionId tho account config
                            //var metaData = await DataVietIdService.GetAccessToken(userName);
                            //string sessionId = (await DataVietIdService.GetSessionId(metaData?.Datas?.AccessToken))?.data?.sessionId;
                            //if(string.IsNullOrEmpty(sessionId))
                            //    return ErrorCodes.BusinessError;

                            var dateCurrent = DateTime.Now;
                            var muiltTaskPublish = new List<Task>();
                            foreach (var dataLink in listLinkInfo)
                            {
                                dataLink.ScheduleDate = dateCurrent.AddMinutes(1);
                                muiltTask.Add(Task.Run(async () =>
                                {
                                    await SetScheduleByIdAsync(dataLink, Utility.ConvertToLong(userName), "", "");
                                }));
                            }
                            Task.WaitAll(muiltTask.ToArray());

                            return ErrorCodes.Success;
                        }
                        return ErrorCodes.BusinessError;
                    }
                    return ErrorCodes.DataExist;
                }
                return ErrorCodes.DataNotExist;
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
                return ErrorCodes.BusinessError;
            }
        }
    }
}
