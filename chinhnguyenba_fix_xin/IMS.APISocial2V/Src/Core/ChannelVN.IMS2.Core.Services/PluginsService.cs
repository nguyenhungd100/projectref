﻿using ChannelVN.IMS2.Core.Entities.DataNewsJson;
using ChannelVN.IMS2.Core.Entities.ErrorCode;
using ChannelVN.IMS2.Foundation.Common;
using ChannelVN.IMS2.Foundation.Common.Configuration;
using ChannelVN.IMS2.Foundation.Logging;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;

namespace ChannelVN.IMS2.Core.Services
{
    public static class PluginsService
    {
       
        public static Task<ActionResponse<DataNewsJson>> CrawlerLinkAsync(string link)
        {
            try
            {
                var content = new FormUrlEncodedContent(new[]
                {
                    new KeyValuePair<string, string>("url_down",link)
                });
                var result = EditorApiGetRequest<DataNewsJson>("?url_down=", link);
                return result;
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
                return Task.FromResult(new ActionResponse<DataNewsJson>() { ErrorCode = (int)ErrorMapping.ErrorCodeBase.Exception });
            }
        }
        
        private static async Task<ActionResponse<T>> EditorApiGetRequest<T>(string apiName, string query)
        {
            var returnValue = new ActionResponse<T>();
            try
            {
                using (var client = new HttpClient())
                {
                    client.Timeout = TimeSpan.FromSeconds(AppSettings.Current.ChannelConfiguration.HttpClientTimeOut);
                    string path = AppSettings.Current.ChannelConfiguration.EditorApiSetting.ApiUrl + apiName;
                    var response = await client.GetAsync(path + query);
                    if (!response.IsSuccessStatusCode)
                    {
                        response.EnsureSuccessStatusCode();
                    }
                    returnValue.ErrorCode = (int)response.StatusCode;
                    returnValue.Data = Json.Parse<T>(await response.Content.ReadAsStringAsync());
                }
            }
            catch (TaskCanceledException tcex)
            {
                Logger.Error(tcex, tcex.Message);
                returnValue.ErrorCode = 408;
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
                returnValue.ErrorCode = (int)ErrorMapping.ErrorCodeBase.Exception;
            }
            return returnValue;
        }
    }
}
