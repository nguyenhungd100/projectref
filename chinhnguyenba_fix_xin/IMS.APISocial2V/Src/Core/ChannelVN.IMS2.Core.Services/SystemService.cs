﻿using ChannelVN.IMS2.Core.Entities.Media;
using ChannelVN.IMS2.Core.Entities.Security;
using ChannelVN.IMS2.Core.Entities.Statistics;
using ChannelVN.IMS2.Core.Models.ExternalAPI;
using ChannelVN.IMS2.Core.Repositories;
using ChannelVN.IMS2.Core.Repositories.Constants;
using ChannelVN.IMS2.Core.Repositories.Functions;
using ChannelVN.IMS2.Foundation.Common;
using ChannelVN.IMS2.Foundation.Logging;
using ChannelVN.IMS2.Foundation.Toolkit.Messaging;
using StackExchange.Redis;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using static ChannelVN.IMS2.Core.Entities.ErrorCode.ErrorMapping;
using static ChannelVN.IMS2.Core.Models.ExternalAPI.Bizfly;

namespace ChannelVN.IMS2.Core.Services
{
    public class SystemService
    {
        public static async Task<bool> RestoreJobQueue(string KeyQueue)
        {
            try
            {
                if (string.IsNullOrEmpty(KeyQueue))
                {
                    //ExchangeFactory.GetStream(TopicName.SHARE).Restore();
                    //ExchangeFactory.GetStream(TopicName.AUTOSHAREVIDEO).Restore();
                    //ExchangeFactory.GetStream(TopicName.AUTOSHAREVIDEOPLAYLIST).Restore();
                    //ExchangeFactory.GetStream(TopicName.AUTOSHAREPOST).Restore();
                    //ExchangeFactory.GetStream(TopicName.AUTOSHARESHARELINK).Restore();
                    //#region Video
                    //ExchangeFactory.GetStream(TopicName.ES_VIDEO).Restore();
                    //ExchangeFactory.GetStream(TopicName.SQL_VIDEO).Restore();
                    //#endregion
                    //#region Playlist
                    //ExchangeFactory.GetStream(TopicName.ES_PLAYLIST).Restore();
                    //ExchangeFactory.GetStream(TopicName.SQL_PLAYLIST).Restore();
                    //#endregion
                    //#region Post
                    //ExchangeFactory.GetStream(TopicName.ES_POST).Restore();
                    //ExchangeFactory.GetStream(TopicName.SQL_POST).Restore();
                    //#endregion
                    //#region ShareLink
                    //ExchangeFactory.GetStream(TopicName.ES_SHARE_LINK).Restore();
                    //ExchangeFactory.GetStream(TopicName.SQL_SHARE_LINK).Restore();
                    //#endregion
                    //#region Article
                    //ExchangeFactory.GetStream(TopicName.ES_ARTICLE).Restore();
                    //ExchangeFactory.GetStream(TopicName.SQL_ARTICLE).Restore();
                    //#endregion
                    //#region Photo
                    //ExchangeFactory.GetStream(TopicName.ES_PHOTO).Restore();
                    //ExchangeFactory.GetStream(TopicName.SQL_PHOTO).Restore();
                    //#endregion
                    //#region MediaUnit
                    //ExchangeFactory.GetStream(TopicName.ES_MEDIA_UNIT).Restore();
                    //ExchangeFactory.GetStream(TopicName.SQL_MEDIA_UNIT).Restore();
                    //#endregion
                    //#region Board
                    //ExchangeFactory.GetStream(TopicName.ES_BOARD).Restore();
                    //ExchangeFactory.GetStream(TopicName.SQL_BOARD).Restore();
                    //#endregion
                    //#region Album
                    //ExchangeFactory.GetStream(TopicName.ES_ALBUM).Restore();
                    //ExchangeFactory.GetStream(TopicName.SQL_ALBUM).Restore();
                    //#endregion
                    return true;
                }
                switch (KeyQueue.ToLower())
                {
                    //#region AutoShare
                    //case TopicName.SHARE:
                    //    ExchangeFactory.GetStream(TopicName.SHARE).Restore();
                    //    break;
                    //case TopicName.AUTOSHAREVIDEO:
                    //    ExchangeFactory.GetStream(TopicName.AUTOSHAREVIDEO).Restore();
                    //    break;
                    //case TopicName.AUTOSHAREVIDEOPLAYLIST:
                    //    ExchangeFactory.GetStream(TopicName.AUTOSHAREVIDEOPLAYLIST).Restore();
                    //    break;
                    //case TopicName.AUTOSHAREPOST:
                    //    ExchangeFactory.GetStream(TopicName.AUTOSHAREPOST).Restore();
                    //    break;
                    //#endregion

                    //#region Video
                    //case TopicName.ES_VIDEO:
                    //    ExchangeFactory.GetStream(TopicName.ES_VIDEO).Restore();
                    //    break;
                    //case TopicName.SQL_VIDEO:
                    //    ExchangeFactory.GetStream(TopicName.SQL_VIDEO).Restore();
                    //    break;
                    //#endregion
                    //#region Playlist
                    //case TopicName.ES_PLAYLIST:
                    //    ExchangeFactory.GetStream(TopicName.ES_PLAYLIST).Restore();
                    //    break;
                    //case TopicName.SQL_PLAYLIST:
                    //    ExchangeFactory.GetStream(TopicName.SQL_PLAYLIST).Restore();
                    //    break;
                    //#endregion
                    //#region Post
                    //case TopicName.ES_POST:
                    //    ExchangeFactory.GetStream(TopicName.ES_POST).Restore();
                    //    break;
                    //case TopicName.SQL_POST:
                    //    ExchangeFactory.GetStream(TopicName.SQL_POST).Restore();
                    //    break;
                    //#endregion
                    //#region Article
                    //case TopicName.ES_ARTICLE:
                    //    ExchangeFactory.GetStream(TopicName.ES_ARTICLE).Restore();
                    //    break;
                    //case TopicName.SQL_ARTICLE:
                    //    ExchangeFactory.GetStream(TopicName.SQL_ARTICLE).Restore();
                    //    break;
                    //#endregion
                    //#region Photo
                    //case TopicName.ES_PHOTO:
                    //    ExchangeFactory.GetStream(TopicName.ES_PHOTO).Restore();
                    //    break;
                    //case TopicName.SQL_PHOTO:
                    //    ExchangeFactory.GetStream(TopicName.SQL_PHOTO).Restore();
                    //    break;
                    //#endregion
                    //#region MediaUnit
                    //case TopicName.ES_MEDIA_UNIT:
                    //    ExchangeFactory.GetStream(TopicName.ES_MEDIA_UNIT).Restore();
                    //    break;
                    //case TopicName.SQL_MEDIA_UNIT:
                    //    ExchangeFactory.GetStream(TopicName.SQL_MEDIA_UNIT).Restore();
                    //    break;
                    //#endregion
                    //default:
                    //    return false;
                }
                return true;
            }
            catch
            {
                await Task.Delay(1);
                return false;
            }
        }

        public static async Task<bool> InitSQLDataFromRedis(string tableName)
        {
            return await SystemRepository.InitSQLDataFromRedis(tableName);
        }

        public static async Task<bool> PushToAppAsync(string tableName, string ids, string clientIP)
        {
            return await SystemRepository.PushToAppAsync(tableName, ids, clientIP);
        }

        public static async Task<bool> AddToAppAsync(string tableName, string ids, string clientIP)
        {
            return await SystemRepository.AddToAppAsync(tableName, ids, clientIP);
        }

        public static async Task<bool> PushDropHeartAsync(string tableName, string ids, string clientIP)
        {
            return await SystemRepository.PushDropHeartAsync(tableName, ids, clientIP);
        }

        public static async Task<bool> ExecuteQueryAsync(string query)
        {
            return await SystemRepository.ExecuteQueryAsync(query);
        }
        public static async Task<RedisValue[]> ListQueueAsync(string index, long start, long stop)
        {
            return await SystemRepository.ListQueueAsync(index, start, stop);
        }

        public static async Task<object> GetAsync(string query)
        {
            return await SystemRepository.GetAsync(query);
        }

        public static async Task<bool> UpdateOwnerPageAsync(string query)
        {
            return await SystemRepository.UpdateOwnerPageAsync(query);
        }

        public static async Task<bool> CheckUserInfoOnAppAsync(string ids, string sessionId)
        {
            var returnValue = false;
            try
            {
                if (string.IsNullOrEmpty(ids))
                {
                    var data = await SystemRepository.GetAsync("select a.id, b.mobile,a.avatar from account a inner join account b on a.createdBy in (b.Id) where a.type=2 and a.status = 1");
                    if (data != null)
                    {
                        var listAccount = ChannelVN.IMS2.Foundation.Common.Json.Parse<List<Account>>(ChannelVN.IMS2.Foundation.Common.Json.Stringify(data));
                        if (listAccount != null)
                        {
                            for (var i = 0; i < listAccount.Count(); i++)
                            {
                                await Function.AddToQueue(ActionName.CheckUserInfoOnApp, TopicName.SYNC_USERINFO, new QueueData<long, string, string, object>
                                {
                                    Data1 = listAccount[i]?.Id ?? 0,
                                    Data2 = listAccount[i]?.Avatar,
                                    Data3 = listAccount[i].Mobile
                                });
                            }
                            returnValue = true;
                        }
                    }
                }
                else
                {
                    var data = await SystemRepository.GetAsync("select a.id, b.mobile,a.avatar from account a inner join account b on a.createdBy in (b.Id) where a.type=2 and a.status = 1 and a.id in ("+ids+")");
                    if (data != null)
                    {
                        var listAccount = ChannelVN.IMS2.Foundation.Common.Json.Parse<List<Account>>(ChannelVN.IMS2.Foundation.Common.Json.Stringify(data));
                        if (listAccount != null)
                        {
                            for (var i = 0; i < listAccount.Count(); i++)
                            {
                                await Function.AddToQueue(ActionName.CheckUserInfoOnApp, TopicName.SYNC_USERINFO, new QueueData<long, string, string, object>
                                {
                                    Data1 = listAccount[i]?.Id ?? 0,
                                    Data2 = listAccount[i]?.Avatar,
                                    Data3 = listAccount[i].Mobile
                                });
                            }
                            returnValue = true;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
            }
            return returnValue;
        }

        public static Task<Dictionary<string, Dictionary<int, int>>> CountNewsOrderAsync(string ids)
        {
            return SystemRepository.CountNewsOrderAsync(ids);
        }

        public static Task<PagingDataResult<object>> ListPageManagerAsync(SearchPage search)
        {
            return SystemRepository.ListPageManagerAsync(search);
        }

        public static Task<BizflyResponse> ActiveChatAsync(long pageId)
        {
            return Bizfly.ActiveChatAsync(pageId);
        }

        public static Task<BizflyResponse> DeactiveChatAsync(long pageId)
        {
            return Bizfly.DeactiveChatAsync(pageId);
        }

        public static Task<BizflyResponse> AddUserToPageAsync(long pageId, string email, string mobile)
        {
            return Bizfly.AddUserToPageAsync(pageId, email, mobile);
        }

        public static Task<BizflyResponse> CheckActiveChatAsync(string pageIds)
        {
            return Bizfly.CheckActiveChatAsync(pageIds);
        }

        public static Task<BizflyResponse> GetPageOfUserAsync(string email, string mobile)
        {
            return Bizfly.GetPageOfUserAsync(email, mobile);
        }
    }
}
