﻿using ChannelVN.IMS2.Core.Entities;
using ChannelVN.IMS2.Core.Repositories;
using ChannelVN.IMS2.Foundation.Common;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ChannelVN.IMS2.Core.Services
{
    public static class NewsService
    {
        public static Task<PagingDataResult<NewsAllSearch>> SearchAsync(SearchNews data, long accountId, bool isDistribution = false)
        {
            return NewsRepository.SearchAsync(data, accountId, isDistribution);
        }

        public static Task<PagingDataResult<NewsAllSearch>> NewsOnPageAsync(SearchNews data, long accountId)
        {
            return NewsRepository.NewsOnPageAsync(data, accountId);
        }
        
        public static Task<List<NewsAllSearch>> GetListNewsAsync(List<string> ids)
        {
            return NewsRepository.GetListNewsAsync(ids);
        }

        public static Task<bool> ChangeDistributionDateAsync(long newsId, DateTime distributionDate, long accountId, string clientIP)
        {
            return NewsRepository.ChangeDistributionDateAsync(newsId, distributionDate, accountId, clientIP);
        }

        public static Task<Dictionary<string,int?>> GetNewsOnHomeStatusAsync(IEnumerable<long> listId)
        {
            return NewsRepository.GetNewsOnHomeStatusAsync(listId);
        } 
    }
}
