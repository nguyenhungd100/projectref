﻿using ChannelVN.IMS2.Core.Entities;
using ChannelVN.IMS2.Core.Repositories;
using ChannelVN.IMS2.Foundation.Common;
using System.Threading.Tasks;

namespace ChannelVN.IMS2.Core.Services
{
    public static class TagService
    {
        public static Task<bool> AddAsync(Tag data)
        {
            return TagRepository.AddAsync(data);
        }

        public static async Task<bool> UpdateAsync(Tag data)
        {
            var tagDb = await GetByIdAsync(data.Id);
            tagDb.ParentId = data.ParentId;
            tagDb.Name = data.Name;
            tagDb.UnsignName = data.UnsignName;
            tagDb.Description = data.Description;
            tagDb.Url = data.Url;
            tagDb.IsHotTag = data.IsHotTag ?? tagDb.IsHotTag ?? false;
            tagDb.Status = data.Status;
            return await TagRepository.UpdateAsync(tagDb);
        }
        public static Task<Tag> GetByIdAsync(long id)
        {
            return TagRepository.GetByIdAsync(id);
        }

        public static Task<PagingDataResult<Tag>> SearchAsync(string keyword, int? status, int pageIndex, int pageSize)
        {
            return TagRepository.SearchAsync(keyword, status, pageIndex, pageSize);
        }

        public static Task<Tag> GetByNameAsync(string name)
        {
            return TagRepository.GetByNameAsync(name);
        }
    }
}
