﻿using ChannelVN.IMS2.Core.Entities.Security;
using ChannelVN.IMS2.Core.Repositories;
using ChannelVN.IMS2.Core.Services.VietId;
using ChannelVN.IMS2.Foundation.Common;
using ChannelVN.IMS2.Foundation.Common.Configuration;
using ChannelVN.IMS2.Foundation.Logging;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using static ChannelVN.IMS2.Core.Entities.ErrorCode.ErrorMapping;

namespace ChannelVN.IMS2.Core.Services
{
    public static class SecurityService
    {
        public static Task<Dictionary<long, ObjectTmp>> GetAllAccountAsync()
        {
            return SecurityRepository.GetAllAccountAsync();
        }

        public static Task<Dictionary<long, ObjectTmp>> GetAllPageAsync()
        {
            return SecurityRepository.GetAllPageAsync();
        }

        public static Task<ErrorCodes> AddAsync(Account data)
        {
            return SecurityRepository.AddAsync(data);
        }

        public static Task<ErrorCodes> AddOfficialAsync(Account officerAccount, UserProfile officerProfile, long currentAccoutId)
        {
            return SecurityRepository.AddOfficialAsync(officerAccount, officerProfile, currentAccoutId);
        }
        public static Task<ErrorCodes> SaveProfileAsync(UserProfile data, Account userCurrent)
        {
            return SecurityRepository.SaveProfileAsync(data, userCurrent);
        }

        public static async Task<(ErrorCodes,Account)> CreateByMobileAsync(Account account)
        {
            var returnCode = ErrorCodes.BusinessError;
            var kinghubId = 0L;
            try
            {
                var vietIDResponse = await DataVietIdService.GetAccessToken(account.Mobile);
                if(vietIDResponse?.Datas != null)
                {
                    var kingHubResponse = await DataVietIdService.AddUserKingHub(vietIDResponse.Datas.AccessToken);

                    if (null == kingHubResponse || kingHubResponse.status == 0 || null == kingHubResponse.data || string.IsNullOrEmpty(kingHubResponse.data.userID) || string.IsNullOrEmpty(kingHubResponse.data.sessionId))
                    {
                        returnCode = ErrorCodes.AddError;
                    }
                    else
                    {
                        var userInfo = await DataVietIdService.GetUserInfo(vietIDResponse.Datas.AccessToken);
                        if (!string.IsNullOrEmpty(userInfo?.Datas?.Id))
                        {
                            account.Id = long.Parse(kingHubResponse.data.userID);
                            account.Mobile = string.IsNullOrEmpty(userInfo.Datas.Mobile) ? account.Mobile : userInfo.Datas.Mobile;
                            account.Email = userInfo.Datas.Email;

                            var resultAddCms = await AddAsync(account);

                            if(resultAddCms == ErrorCodes.Success)
                            {
                                kinghubId = long.Parse(kingHubResponse.data.userID);
                                returnCode = ErrorCodes.Success;
                            }
                            else
                            {
                                returnCode = ErrorCodes.AddError;
                            }
                        }
                        else
                        {
                            returnCode = ErrorCodes.AccountNotExits;
                        }
                    }
                }
            }
            catch(Exception ex)
            {
                Logger.Sensitive(ex, ex.Message);
                returnCode = ErrorCodes.Exception;
            }
            return (returnCode, account);
        }
        
        public static Task<List<AccountMember>> ListMembersAsync(long officerId)
        {
            return SecurityRepository.ListMembersAsync(officerId);
        }
        
        public static Task<ErrorCodes> UpdateAsync(AccountSearch data, UserProfile profile, bool isUpdatePage, string mobileOld, string emailOld, string sessionId = null)
        {
            return SecurityRepository.UpdateAsync(data, profile, isUpdatePage, mobileOld, emailOld, sessionId);
        }
        public static Task<ErrorCodes> ConfigPageAsync(long officerId, int mode, long accountId)
        {
            return SecurityRepository.ConfigPageAsync(officerId, mode, accountId);
        }
        
        public static Task<ErrorCodes> AcceptOrRejectAsync(AccountSearch data)
        {
            return SecurityRepository.AcceptOrRejectAsync(data);
        }

        public static Task<bool> ConvertDataAsync(Account data)
        {
            return SecurityRepository.ConvertDataAsync(data);
        }

        public static Task<Account> GetByIdAsync(long id)
        {
            return SecurityRepository.GetByIdAsync(id);
        }

        public static Task<List<Account>> GetListByIdsAsync(List<string> ids)
        {
            return SecurityRepository.GetListByIdsAsync(ids);
        }
        
        public static Task<AccountSimple> GetSimpleAccountAsync(long id)
        {
            return SecurityRepository.GetSimpleAccountAsync(id);
        }

        public static Task<AccountSearch> GetAccountSearchByIdAsync(long id)
        {
            return SecurityRepository.GetAccountSearchByIdAsync(id);
        }
        public static Task<AccountSearch> GetByUsernameAsync(string username)
        {
            return SecurityRepository.GetAccountByUserNameAsync(username);
        }
        public static Task<PagingDataResult<AccountSearch>> SearchAsync(SearchAccountEntity search)
        {
            return SecurityRepository.SearchAsync(search);
        }
        public static async Task<SearchAccountData[]> SearchInviteAsync(string keyword, int pageIndex, int pageSize)
        {
            var returnValue = default(SearchAccountData[]);
            string path = string.Empty;
            using (var client = new HttpClient())
            {
                try
                {
                    var content = new FormUrlEncodedContent(new
                    {
                        query = keyword,
                        page = pageIndex,
                        limit = pageSize
                    }.ConvertToKeyValuePair());
                    string query = await content?.ReadAsStringAsync();

                    client.Timeout = TimeSpan.FromSeconds(AppSettings.Current.ChannelConfiguration.HttpClientTimeOut);
                    path = AppSettings.Current.ChannelConfiguration.ApiUserKingHub.UrlSearchUser + "?" + query;

                    var response = await client.GetAsync(path);
                    if (!response.IsSuccessStatusCode)
                    {
                        response.EnsureSuccessStatusCode();
                    }

                    var responseContent = await response.Content?.ReadAsStringAsync();
                    if (response?.StatusCode == System.Net.HttpStatusCode.OK)
                    {
                        var result = Json.Parse<SearchAccountResponse>(responseContent);
                        if (result?.Status == 1)
                            returnValue = result.Data;
                        else
                            Logger.Error($"SearchUserKingHub ({keyword},{pageIndex},{pageSize}) => " + responseContent);
                    }
                    else
                    {
                        Logger.Debug($"SearchUserKingHub ({keyword},{pageIndex},{pageSize}) => " + responseContent);
                    }
                }
                catch(Exception ex)
                {
                    Logger.Error(ex, ex.Message);
                }
                
            }
            return returnValue;
        }
        public static Task<PagingDataResult<AccountSearch>> SearchByRegisterAccountStatusAsync(SearchAccountEntity search)
        {
            return SecurityRepository.SearchByRegisterAccountStatusAsync(search);
        }
        public static Task<long> GetCountByUserNameAsync(string userName)
        {
            return SecurityRepository.GetCountByUserNameAsync(userName);
        }

        public static async Task<ErrorCodes> UpdateStatusAsync(long officerId, Account account, AccountStatus status, AccountClass? accountClass = 0)
        {
            var returnValue = ErrorCodes.UpdateError;
            try
            {
                var dataDb = await SecurityRepository.GetAccountSearchByIdAsync(officerId);
                if (dataDb != null)
                {
                    if(status == AccountStatus.UnActived)
                    {
                        if ((account.Id.ToString().Equals(dataDb.CreatedBy) && !dataDb.Id.Equals(dataDb.CreatedBy) && dataDb.Type == (int)AccountType.Official)
                       || (account.IsSystem == true && account.IsFullPermission == true))
                        {
                            dataDb.Status = (int)AccountStatus.UnActived;
                            //dataDb.Class = (Byte?) accountClass;
                            returnValue = await SecurityRepository.UpdateStatusAsync(dataDb);
                        }
                        else
                        {
                            returnValue = ErrorCodes.PermissionUpdateInvalid;
                        }
                    }
                    else
                    {
                        if (status == AccountStatus.Actived)
                        {
                            if (account.IsSystem == true && account.IsFullPermission == true && dataDb.Type == (int)AccountType.Official)
                            {
                                dataDb.Status = (int)AccountStatus.Actived;
                                dataDb.Class = (Byte?)accountClass;
                                returnValue = await SecurityRepository.UpdateStatusAsync(dataDb);
                            }
                            else
                            {
                                returnValue = ErrorCodes.PermissionUpdateInvalid;
                            }
                        }
                        else
                        {
                            returnValue = ErrorCodes.DataInvalid;
                        }
                    }
                }
                else
                {
                    returnValue = ErrorCodes.AccountNotExits;
                }
            }catch(Exception ex)
            {
                Logger.Error(ex, ex.Message);
                returnValue = ErrorCodes.Exception;
            }
            return returnValue;
        }

        public static async Task<ErrorCodes> ConfigLabelTypeAsync(long officerId, Account account, int? labelType)
        {
            var returnValue = ErrorCodes.UpdateError;
            try
            {
                var dataDb = await SecurityRepository.GetAccountSearchByIdAsync(officerId);
                if (dataDb != null)
                {
                    if (account.IsSystem == true && account.IsFullPermission == true && dataDb.Type == (int)AccountType.Official)
                    {
                        dataDb.LabelMode = labelType;
                        returnValue = await SecurityRepository.UpdateStatusAsync(dataDb);
                    }
                    else
                    {
                        returnValue = ErrorCodes.PermissionUpdateInvalid;
                    }
                }
                else
                {
                    returnValue = ErrorCodes.AccountNotExits;
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
                returnValue = ErrorCodes.Exception;
            }
            return returnValue;
        }
        
        public static Task<ErrorCodes> AddAccountMemberAsync(AccountMember accountMember, AccountSearch officerAcc)
        {
            return SecurityRepository.AddAccountMemberAsync(accountMember, officerAcc);
        }

        public static Task<ErrorCodes> UpdateAccountMemberAsync(AccountMember accountMember)
        {
            return SecurityRepository.Update_AccountMemberAsync(accountMember);
        }

        public static Task<AccountMember> GetAccountMemberByIdAsync(string accountMemberId)
        {
            return SecurityRepository.GetAccountMemberByIdAsync(accountMemberId);
        }
        public static Task<AccountSearch> GetByIdESAsync(long officerId)
        {
            return SecurityRepository.GetByIdESAsync(officerId);
        }
        public static Task<ErrorCodes> UpdateAccountTypeAsync(Account accDb)
        {
            return SecurityRepository.UpdateAccountTypeAsync(accDb);
        }

        public static Task<ErrorCodes> UpdateAccountClassAsync(Account accDb)
        {
            return SecurityRepository.UpdateAccountClassAsync(accDb);
        }

        public static async Task<ErrorCodes> UpdateAccountOpentIdAsync(string mobile, long openId)
        {
            var accountId = await SecurityRepository.CheckMobileAsync(mobile);
            if (accountId > 0)
            {
                var accDb = await GetByIdAsync(accountId.Value);
                if (accDb == null)
                {
                    return ErrorCodes.AccountNotExits;
                }
                accDb.OpenId = openId;

                return await SecurityRepository.UpdateAccountOpenIdAsync(accDb);
            }
            return ErrorCodes.AccountNotExits;
        }

        public static Task<ErrorCodes> UpdateRoleAsync(Account accDb)
        {
            return SecurityRepository.UpdateRoleAsync(accDb);
        }

        public static Task<UserProfile> GetProfileAsync(long id)
        {
            return SecurityRepository.GetProfileAsync(id);
        }

        public static Task<bool> RemoveMemberAsync(long officerId, long memberId)
        {
            return SecurityRepository.RemoveMemberAsync(officerId, memberId);
        }

        public static Task<ErrorCodes> RemoveMemberOnAllPageAsync(long accountId, long memberId)
        {
            return SecurityRepository.RemoveMemberOnAllPageAsync(accountId, memberId);
        }

        public static async Task<bool> LockMemberAsync(long officerId, long memberId)
        {
            var returnValue = false;
            try
            {
                var dataDb = await SecurityRepository.GetAccountMemberByIdAsync(officerId + IndexKeys.SeparateChar + memberId);
                if (dataDb != null)
                {
                    dataDb.LockedDate = DateTime.Now;
                }
                returnValue = await SecurityRepository.LockOrUnlockMemberAsync(dataDb);
            }
            catch(Exception ex)
            {
                Logger.Error(ex, ex.Message);
            }
            return returnValue;
        }

        public static async Task<bool> UnLockMemberAsync(long officerId, long memberId)
        {
            var returnValue = false;
            try
            {
                var dataDb = await SecurityRepository.GetAccountMemberByIdAsync(officerId + IndexKeys.SeparateChar + memberId);
                if (dataDb != null)
                {
                    dataDb.LockedDate = null;
                }
                returnValue = await SecurityRepository.LockOrUnlockMemberAsync(dataDb);
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
            }
            return returnValue;
        }

        public static Task<ErrorCodes> LockOrUnlockMemberOnAllPageAsync(long accountId, long memberId, int act)
        {
            return SecurityRepository.LockOrUnlockMemberOnAllPageAsync(accountId, memberId, act);
        }
        
        public static Task<AccountSearch> GetAccountESAsync(long officerId)
        {
            return SecurityRepository.GetAccountESAsync(officerId);
        }

        public static Task<PagingDataResult<OfficialAccount>> MyOfficialAsync(string keyword, long userId, int pageIndex, int pageSize)
        {
            return SecurityRepository.MyOfficialAsync(keyword, userId, pageIndex, pageSize);
        }

        public static Task<PagingDataResult<OfficialAccount>> MyPagesAsync(SearchAccountEntity search)
        {
            return SecurityRepository.MyPagesAsync(search);
        }

        public static Task<List<OfficialAccount>> GetListPageOwnerAsync(long ownerId)
        {
            return SecurityRepository.GetListPageOwnerAsync(ownerId);
        }

        public static Task<bool> CheckAccountMember(long officialId, long memberId)
        {
            return SecurityRepository.CheckAccountMember(officialId, memberId);
        }
        
        public static Task<PagingDataResult<AccountMemberInfo>> GetMembersAsync(long? officerId, long accountId, AccountMemberRole? role, string keyWord, int pageIndex, int pageSize)
        {
            return SecurityRepository.GetMembersAsync(officerId, accountId, role, keyWord, pageIndex, pageSize);
        }
        public static Task<List<AccountMemberInfo>> GetMembersAsync(long officerId)
        {
            return SecurityRepository.GetMembersAsync(officerId);
        }
        public static Task<List<AccountMemberInfo>> ApproversAsync(long officerId)
        {
            return SecurityRepository.ApproversAsync(officerId);
        }

        public static Task<ErrorCodes> ChangeOwnerAsync(long officerId, long oldOwnerId, long newOwnerId, long userId)
        {
            return SecurityRepository.ChangeOwnerAsync(officerId, oldOwnerId, newOwnerId, userId);
        }

        public static Task<bool> CheckOwnerExist(long userId)
        {
            return SecurityRepository.CheckOwnerExist(userId);
        }

        public static Task<long[]> ListOwnerAsync(long userId)
        {
            return SecurityRepository.ListOwnerAsync(userId);
        }

        public static Task<bool> ApprovedOnAppAsync(long userId, int role)
        {
            return SecurityRepository.ApprovedOnAppAsync(userId, role);
        }

        public static Task<DataResponse> GetUserInfo(long userId)
        {
            return SecurityRepository.GetUserInfo(userId);
        }

        public static Task<object> GetUserInfoV2(long userId)
        {
            return SecurityRepository.GetUserInfoV2(userId);
        }

        public static async Task<DataResponse> AuthenKinghub(string accesstoken)
        {
            var returnValue = default(DataResponse);
            try
            {
                using (var client = new HttpClient())
                {
                    client.Timeout = TimeSpan.FromSeconds(AppSettings.Current.ChannelConfiguration.HttpClientTimeOut);
                    string path = AppSettings.Current.ChannelConfiguration.AuthenKingHubSetting.ApiUrl;

                    var content = new FormUrlEncodedContent(new
                    {
                        access_token = accesstoken

                    }.ConvertToKeyValuePair());                    

                    var response = await client.PostAsync(path, content);
                    if (!response.IsSuccessStatusCode)
                    {
                        response.EnsureSuccessStatusCode();
                    }
                    if (response?.StatusCode == System.Net.HttpStatusCode.OK)
                    {
                        var userData = Json.Parse<LoginResponse>(await response.Content?.ReadAsStringAsync());
                        if (userData != null && userData.Data != null && !string.IsNullOrEmpty(userData.Data.User_id))
                        {
                            returnValue = userData.Data;
                        }
                        else
                        {
                            Logger.Debug("AuthenKinghub => " + await response?.Content?.ReadAsStringAsync());
                        }
                    }
                    else
                    {
                        Logger.Debug("AuthenKinghub => " + await response?.Content?.ReadAsStringAsync());
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, "AuthenKinghub => " + ex.Message);
            }
            return returnValue;
        }

        public static async Task<UserResponse> CreatedKingHubId(long ownerId, string fullName, string avatar, string cover, string sesionId, string mode = null)
        {
            var returnValue = default(UserResponse);
            try
            {
                using (var client = new HttpClient())
                {
                    if(mode == null) mode = AppSettings.Current.ChannelConfiguration.ApiUserKingHub.Mode.ToString();
                    var multipartFormDataContent = new MultipartFormDataContent();
                    var values = new[]
                       {
                            new KeyValuePair<string, string>("owner_id", ownerId.ToString()),
                            new KeyValuePair<string, string>("dev", mode),
                            new KeyValuePair<string, string>("full_name", fullName??string.Empty),
                            new KeyValuePair<string, string>("avatar", avatar??string.Empty),
                            new KeyValuePair<string, string>("cover", cover??string.Empty)
                        };

                    foreach (var keyValuePair in values)
                    {
                        multipartFormDataContent.Add(new StringContent(keyValuePair.Value?? string.Empty),
                            String.Format("\"{0}\"", keyValuePair.Key));
                    }

                    var hashData = string.Format("POST/generate-useravatar={0}&cover={1}&dev={2}&full_name={3}&owner_id={4}", avatar ?? string.Empty, cover ?? string.Empty, mode, fullName ?? string.Empty, ownerId);
                    var hashKey = AppSettings.Current.ChannelConfiguration.ApiUserKingHub.TokenGenUser;
                    var hmac = Encryption.HMACSHA256Encode(hashData, hashKey);

                    client.DefaultRequestHeaders.Add("session-id", sesionId);
                    client.DefaultRequestHeaders.Add("hmac", hmac);

                    var requestUri = AppSettings.Current.ChannelConfiguration.ApiUserKingHub.UrlCreate;
                    var response = await client.PostAsync(requestUri, multipartFormDataContent);
                    
                    if (!response.IsSuccessStatusCode)
                    {
                        response.EnsureSuccessStatusCode();
                    }

                    if (response?.StatusCode == System.Net.HttpStatusCode.OK)
                    {
                        var stringContent = await response.Content?.ReadAsStringAsync();
                        var userData = Json.Parse<AccountRespone>(stringContent);
                        if (userData != null && userData.Data != null && !string.IsNullOrEmpty(userData.Data.UserID))
                        {
                            returnValue = userData.Data;
                        }
                        else
                        {
                            Logger.Error("Create page => " + await response?.Content?.ReadAsStringAsync());
                        }
                    }
                    else
                    {
                        Logger.Error("Create page => " + await response?.Content?.ReadAsStringAsync());
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, "Create page => " + ex.Message);
            }
            return returnValue;
        }

        public static Task<bool> IsModifier(long officerId, long userId)
        {
            return SecurityRepository.IsModifier(officerId, userId);
        }

        public static Task<long?> CheckMobileAsync(string mobile)
        {
            return SecurityRepository.CheckMobileAsync(mobile);
        }

        public static Task<bool> SendCRMAsync(long userId, string userCurrentName, string userFullName, string phoneNumber, string email, AccountType? accountType, List<Account> pages)
        {
            return SecurityRepository.SendCRMAsync(userId, userCurrentName, userFullName, phoneNumber, email, accountType, pages);
        }

        public static Task<ErrorCodes> SetProfileIsPageOnAppAsync(long officerId, long userId, long memberId, bool isEnable)
        {
            return SecurityRepository.SetProfileIsPageOnAppAsync(officerId, userId, memberId, isEnable);
        }
        #region API phục vụ đăng ký, đăng nhập
        //public async Task<LoginMobileResponse> LoginMobile(string mobile)
        //{
        //    var timeStamp = new DateTimeOffset(DateTime.UtcNow).ToUnixTimeSeconds();
        //    var checkSum = MD5Hash(_vietIDConfig.ClientSecret + timeStamp);
        //    Dictionary<string, string> dict = new Dictionary<string, string>()
        //    {
        //        {"app_id", _vietIDConfig.ClientID},
        //        {"mobile",mobile },
        //        {"timestamp",timeStamp.ToString() },
        //        {"checksum",checkSum}
        //    };
        //    var result = await RequestApiAsync<LoginMobileResponse>("/loginMobile", dict);
        //    return result;
        //}

        //private static string MD5Hash(string input)
        //{
        //    StringBuilder hash = new StringBuilder();
        //    MD5CryptoServiceProvider md5provider = new MD5CryptoServiceProvider();
        //    byte[] bytes = md5provider.ComputeHash(new UTF8Encoding().GetBytes(input));

        //    for (int i = 0; i < bytes.Length; i++)
        //    {
        //        hash.Append(bytes[i].ToString("x2"));
        //    }
        //    return hash.ToString();
        //}
        #endregion

        public static Task<long> GetAccountInNewsAsync(long newsId)
        {
            return SecurityRepository.GetAccountInNewsAsync(newsId);
        }

        public static Task<bool> UpdateAccessTokenAsync(AccountSearch account)
        {
            return SecurityRepository.UpdateAccessTokenAsync(account);
        }

        public static async Task<bool> UpdateUserAvatarOnAppAsync(AccountSearch account, UserProfile profile, string sessionId)
        {
            var returnValue = false;
            try
            {
                var avatar = account.Avatar;
                account = await SecurityRepository.GetAccountSearchByIdAsync(account.Id);
                account.Avatar = avatar;
                profile = await SecurityRepository.GetProfileAsync(account.Id);
                using (var client = new HttpClient())
                {
                    var multipartFormDataContent = new MultipartFormDataContent();

                    var data = new
                    {
                        avatar = account.Avatar ?? string.Empty,
                        birthday = profile?.BirthDay?.ToString(AppSettings.Current.ChannelConfiguration.BirthdayFormat) ?? string.Empty,
                        cover = account.Banner ?? string.Empty,
                        dev = AppSettings.Current.ChannelConfiguration.ApiUserKingHub.Mode.ToString(),
                        full_name = account.FullName ?? string.Empty,
                        job_position = profile?.Title ?? string.Empty,
                        living_place = profile?.Address ?? string.Empty,
                        school = profile?.School ?? string.Empty,
                        sex = profile?.Gender == 0 ? "FEMALE" : "MALE",
                        user_id = account.EncryptId ?? string.Empty,
                        user_status = string.Empty,
                        username = account.UserName ?? string.Empty,
                        workplace = profile?.WorkPlace ?? string.Empty
                    };

                    //var values = new[]
                    //   {
                    //        new KeyValuePair<string, string>("user_id", account.EncryptId??string.Empty),
                    //        new KeyValuePair<string, string>("username", account.UserName??string.Empty),
                    //        new KeyValuePair<string, string>("sex", "MALE"),
                    //        new KeyValuePair<string, string>("full_name", account.FullName??string.Empty),
                    //        new KeyValuePair<string, string>("avatar", account.Avatar??string.Empty),
                    //        new KeyValuePair<string, string>("cover", account.Banner??string.Empty),
                    //        new KeyValuePair<string, string>("dev", AppSettings.Current.ChannelConfiguration.ApiUserKingHub.Mode.ToString())
                    //    };

                    var values = data.ConvertToKeyValuePair();
                    foreach (var keyValuePair in values)
                    {
                        multipartFormDataContent.Add(new StringContent(keyValuePair.Value),
                            String.Format("\"{0}\"", keyValuePair.Key));
                    }

                    Logger.Debug("Account Data=>" + Json.Stringify(account));
                    Logger.Debug("Profile Data=>" + Json.Stringify(profile));
                    //var hashData = string.Format("POST/update-userinfoavatar={0}&cover={1}&dev={2}&full_name={3}&sex={4}&user_id={5}&username={6}", data.avatar ?? string.Empty, data.cover ?? string.Empty, data.dev, data.full_name ?? string.Empty, data.sex, data.user_id, data.username);
                    var hashData = data.BuildHMAQueyString("POST/update-userinfo");
                    var hashKey = AppSettings.Current.ChannelConfiguration.ApiUserKingHub.TokenGenUser;
                    var hmac = Encryption.HMACSHA256Encode(hashData, hashKey);

                    client.DefaultRequestHeaders.Add("hmac", hmac);
                    client.DefaultRequestHeaders.Add("session-id", sessionId);

                    // Logger.Debug("Sync user info to app =>" + Json.Stringify(data));
                    //Logger.Debug("hashData=>" + hashData);
                    //Logger.Debug("hmac=>" + hmac);

                    var requestUri = AppSettings.Current.ChannelConfiguration.ApiUserKingHub.UrlUpdate;
                    var response = await client.PostAsync(requestUri, multipartFormDataContent);

                    if (!response.IsSuccessStatusCode)
                    {
                        response.EnsureSuccessStatusCode();
                    }

                    if (response?.StatusCode == System.Net.HttpStatusCode.OK)
                    {
                        var stringContent = await response.Content?.ReadAsStringAsync();
                        var userData = Json.Parse<AccountRespone>(stringContent);
                        if (userData != null && userData.Status == 1)
                        {
                            returnValue = true;
                        }

                        Logger.Debug("Responce sync user info to app 1 =>" + stringContent);
                    }
                    else
                    {
                        Logger.Debug("Responce sync user info to app 2 =>" + await response?.Content?.ReadAsStringAsync());
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Debug(ex, "Responce sync user info to app 3 =>" + ex.Message);
                Logger.Error(ex, ex.Message);
            }
            return returnValue;
        }
    }
}
