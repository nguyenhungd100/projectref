﻿using ChannelVN.IMS2.Core.Entities.Security;
using ChannelVN.IMS2.Core.Repositories;
using System.Threading.Tasks;

namespace ChannelVN.IMS2.Core.Services
{
    public class KinghubService
    {
        public static async Task<bool> SetUserRolesAsync(Account account)
        {
            var result = await KingHubRepository.SetUserRolesAsync(account);
            if (!result)
            {
                await KingHubRepository.AddToQueue(account);
            }
            return result;
        }
    }
}
