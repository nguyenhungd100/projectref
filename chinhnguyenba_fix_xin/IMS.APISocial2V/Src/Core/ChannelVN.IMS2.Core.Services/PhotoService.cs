﻿using ChannelVN.IMS2.Core.Entities;
using ChannelVN.IMS2.Core.Entities.Album;
using ChannelVN.IMS2.Core.Entities.Photo;
using ChannelVN.IMS2.Core.Repositories;
using ChannelVN.IMS2.Foundation.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using static ChannelVN.IMS2.Core.Entities.ErrorCode.ErrorMapping;

namespace ChannelVN.IMS2.Core.Services
{
    public static class PhotoService
    {
        public static Task<ErrorCodes> AddAsync(PhotoSearch data, string clientIP)
        {
            return PhotoRepository.AddAsync(data, clientIP);
        }

        public static Task<ErrorCodes> UpdateAsync(PhotoUnit data, long officerId, string clientIP, string sessionId)
        {
            return PhotoRepository.UpdateAsync(data, officerId, clientIP, sessionId);
        }

        public static Task<ErrorCodes> UpdateStatusV2Async(long id, NewsStatus status, long userId, string clientIP, string sessionId)
        {
            return PhotoRepository.UpdateStatusV2Async(id, status, userId, clientIP, sessionId);
        }

        public static Task<PhotoSearch> GetByIdAsync(long id)
        {
            return PhotoRepository.GetDetailByIdAsync(id);
        }

        public static Task<PagingDataResult<PhotoSearchReturn>> SearchAsync(SearchNews search, long accountId)
        {
            return PhotoRepository.SearchAsync(search, accountId);
        }

        public static Task<PagingDataResult<PhotoUnit>> GetListApproveAsync(SearchNews data, long accountId)
        {
            return PhotoRepository.GetListApproveAsync(data, accountId);
        }

        public static async Task<PagingDataResult<PhotoSearch>> ListDistributionAsync(SearchNews search, long accountId)
        {
            var data = await PhotoRepository.ListDistributionAsync(search, accountId);
            if (data?.Data?.Count > 0)
            {
                data.Data = await PhotoRepository.GetListPhotoSearchByIdsAsync(data.Data.Select(s => s.Id.ToString()).ToList());
            }

            return data;
        }
        public static Task<bool> AddToAlbumAsync(long photoId, List<Album> dataDb)
        {
            return PhotoRepository.AddToAlbumAsync(photoId, dataDb);
        }
    }
}
