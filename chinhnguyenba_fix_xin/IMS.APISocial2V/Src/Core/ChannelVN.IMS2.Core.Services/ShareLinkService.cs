﻿using ChannelVN.IMS2.Core.Entities;
using ChannelVN.IMS2.Core.Entities.ShareLinks;
using ChannelVN.IMS2.Core.Repositories;
using ChannelVN.IMS2.Foundation.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static ChannelVN.IMS2.Core.Entities.ErrorCode.ErrorMapping;

namespace ChannelVN.IMS2.Core.Services
{
    public class ShareLinkService
    {
        public static Task<ErrorCodes> AddAsync(ShareLinkSearch data, string clientIP)
        {
            return ShareLinkRepository.AddAsync(data, clientIP);
        }

        public static Task<PagingDataResult<ShareLinkSearchReturn>> SearchAsync(SearchNews data, long userId, bool isGetDistribution = false)
        {
            return ShareLinkRepository.SearchAsync(data, userId, isGetDistribution);
        }

        public static Task<PagingDataResult<ShareLink>> GetListApproveAsync(SearchNews data, long accountId)
        {
            return ShareLinkRepository.GetListApproveAsync(data, accountId);
        }

        public static Task<ErrorCodes> UpdateStatusAsync(ShareLinkSearch data, int statusOld, string clientIP)
        {
            return ShareLinkRepository.UpdateStatusAsync(data, statusOld, clientIP);
        }

        public static Task<ErrorCodes> UpdateStatusV2Async(long id, NewsStatus status, long userId, string clientIP, string sessionId)
        {
            return ShareLinkRepository.UpdateStatusV2Async(id, status, userId, clientIP, sessionId);
        }

        public static Task<ErrorCodes> UpdateAsync(ShareLink data, long officerId, string clientIP, string sessionId)
        {
            return ShareLinkRepository.UpdateAsync(data, officerId, clientIP, sessionId);
        }
        public static Task<ShareLinkSearch> GetShareLinkSearchByIdAsync(long id)
        {
            return ShareLinkRepository.GetShareLinkSearchByIdAsync(id);
        }

        public static async Task<ErrorCodes> AddAndUpdateStatusAsync(ShareLinkSearch data, NewsStatus status, long userId, string clientIP, string sessionId)
        {
            var result = await ShareLinkRepository.AddAsync(data, clientIP);
            if (result == ErrorCodes.Success)
            {
                return await ShareLinkRepository.UpdateStatusV2Async(data.Id, status, userId, clientIP, sessionId);
            }
            return result;
        }

    }
}
