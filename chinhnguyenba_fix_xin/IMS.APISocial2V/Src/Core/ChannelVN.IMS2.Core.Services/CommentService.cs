﻿using ChannelVN.IMS2.Core.Entities.Comment;
using ChannelVN.IMS2.Core.Repositories;
using ChannelVN.IMS2.Foundation.Common;
using ChannelVN.IMS2.Foundation.Common.Configuration;
using ChannelVN.IMS2.Foundation.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace ChannelVN.IMS2.Core.Services
{
    public class CommentService
    {
        public static async Task<ResponseGetDataModel<ResultGetDataModel<CommentDataModel>>> GetByPostIdAsync(RequestDataModel requestData)
        {
            var result = default(ResponseGetDataModel<ResultGetDataModel<CommentDataModel>>);
            try
            {
                var url = AppSettings.Current.ChannelConfiguration.ApiCommentSetting.UrlSearchComment;
                var query = await (new FormUrlEncodedContent(requestData.ConvertToKeyValuePair())).ReadAsStringAsync();
                Logger.Debug("Get_By_Post_Id => Url:" + url + ", Query:" + query);
                result = await HttpClientGetDataAsync<ResultGetDataModel<CommentDataModel>>(url, query);
                Logger.Debug("Get_By_Post_Id => Response:" + Json.Stringify(result));
            }
            catch(Exception ex)
            {
                Logger.Error(ex, ex.Message);
                result = new ResponseGetDataModel<ResultGetDataModel<CommentDataModel>>
                {
                    Status = 0,
                    Message = ex.Message,
                    Code = (int)HttpStatusCode.OK
                };
            }
            return result;
        }

        public static async Task<ResponseGetDataModel<ResultGetDataModel<CommentDataModel>>> GetDetailAsync(RequestDataModel requestData)
        {
            var result = default(ResponseGetDataModel<ResultGetDataModel<CommentDataModel>>);
            try
            {
                var url = AppSettings.Current.ChannelConfiguration.ApiCommentSetting.UrlSearchComment;
                var query = await (new FormUrlEncodedContent(requestData.ConvertToKeyValuePair())).ReadAsStringAsync();
                result = await HttpClientGetDataAsync<ResultGetDataModel<CommentDataModel>>(url, query);
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
                result = new ResponseGetDataModel<ResultGetDataModel<CommentDataModel>>
                {
                    Status = 0,
                    Message = ex.Message,
                    Code = (int)HttpStatusCode.OK
                };
            }
            return result;
        }

        public static async Task<ResponseDeleteDataModel<ResultDeleteDataModel<DataModel>>> DeleteAsync(string ids, long officerId)
        {
            var result = default(ResponseDeleteDataModel<ResultDeleteDataModel<DataModel>>);
            try
            {
                var url = AppSettings.Current.ChannelConfiguration.ApiCommentSetting.UrlDeleteComment;
                var query = await (new FormUrlEncodedContent(new {
                    id = ids
                }.ConvertToKeyValuePair())).ReadAsStringAsync();

                var headerParam = new Dictionary<string, string>();
                headerParam.Add("user-id", officerId.ToString());

                result = await HttpClienDeleteDataAsync<ResultDeleteDataModel<DataModel>>(url, query, headerParam);
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
                result = new ResponseDeleteDataModel<ResultDeleteDataModel<DataModel>>
                {
                    Status = 0,
                    Code = (int)HttpStatusCode.OK,
                    Message = ex.Message
                };
            }
            return result;
        }

        public static async Task<ResponseGetDataModel<ResponseData<CommentDataModel>[]>> RestoreAsync(string ids, bool recursive, long officerId)
        {
            var result = default(ResponseGetDataModel<ResponseData<CommentDataModel>[]>);
            try
            {
                var url = AppSettings.Current.ChannelConfiguration.ApiCommentSetting.UrlRestoreComment;
                var query = await (new FormUrlEncodedContent(new
                {
                    id = ids,
                    recursive = recursive
                }.ConvertToKeyValuePair())).ReadAsStringAsync();

                var headerParam = new Dictionary<string, string>();
                headerParam.Add("user-id", officerId.ToString());

                result = await HttpClientGetDataAsync<ResponseData<CommentDataModel>[]>(url, query, headerParam);
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
                result = new ResponseGetDataModel<ResponseData<CommentDataModel>[]>
                {
                    Status = 0,
                    Code = (int)HttpStatusCode.OK,
                    Message = ex.Message
                };
            }
            return result;
        }

        public static async Task<ResponsePutDataModel<ResultPutDataModel<CommentDataModel>>> UpdateAsync(string id, string jsonContent)
        {
            var result = default(ResponsePutDataModel<ResultPutDataModel<CommentDataModel>>);
            try
            {
                var urlGetDetail = AppSettings.Current.ChannelConfiguration.ApiCommentSetting.UrlGetDetailComment;
                var query = await (new FormUrlEncodedContent(new
                {
                    id = id
                }.ConvertToKeyValuePair())).ReadAsStringAsync();

                var oldComment = await HttpClientGetDataAsync<ResultGetDataModel<CommentDataModel>>(urlGetDetail, query);
                if(oldComment!=null && oldComment.Result!=null && oldComment.Result.data!=null && oldComment.Result.data.Count() > 0)
                {

                    var urlUpdate = AppSettings.Current.ChannelConfiguration.ApiCommentSetting.UrlUpdateComment;
                    oldComment.Result.data.FirstOrDefault().Content = Json.Parse<ContentDataModel>(jsonContent);

                    var headerParam = new Dictionary<string, string>();
                    headerParam.Add("user-id", oldComment.Result.data.FirstOrDefault()?.User?.Id);

                    result = await HttpClientPutDataAsync<ResultPutDataModel<CommentDataModel>>(urlUpdate, Json.StringifyFirstLowercase(new
                    {
                        data = oldComment.Result.data
                    }), headerParam);
                }
                else
                {
                    result = new ResponsePutDataModel<ResultPutDataModel<CommentDataModel>>
                    {
                        Status = 0,
                        Code = (int)HttpStatusCode.OK,
                        Message = "Comment not exist.",
                    };
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
                result = new ResponsePutDataModel<ResultPutDataModel<CommentDataModel>>
                {
                    Status = 0,
                    Code = (int)HttpStatusCode.OK,
                    Message = ex.Message,
                };
            }
            return result;
        }

        public static async Task<ResponseGetDataModel<ResponseData<CommentDataModel>[]>> CreateAsync(long mediaId, string parentCommentID, string jsonContent, long accountId)
        {
            var result = default(ResponseGetDataModel<ResponseData<CommentDataModel>[]>);
            try
            {
                var obj = new
                {
                    data = new[]
                    {
                        new
                        {
                            mediaID = mediaId.ToString(),
                            parentCommentID = parentCommentID,
                            createdBy = accountId.ToString(),
                            content = Json.Parse<dynamic>(jsonContent),
                            mode = 0
                        }
                    }
                };

                var objJson = Json.Parse<object>(jsonContent);
                var headerParam = new Dictionary<string, string>();
                headerParam.Add("user-id", accountId.ToString());

                var url = AppSettings.Current.ChannelConfiguration.ApiCommentSetting.UrlReplyComment;

                var content = Json.Stringify(Json.Parse<dynamic>(jsonContent));
                Logger.Debug("Create comment content=>" + content);
                Logger.Debug("Create comment url=>" + url);
                Logger.Debug("Create comment body=>" + Json.Stringify(obj));
                Logger.Debug("Create comment header=>" + Json.Stringify(headerParam));

                var comment = await HttpClientPostDataAsync<ResponseData<CommentDataModel>[]>(url, Json.Stringify(obj), headerParam);
                if (comment != null && comment.Result != null && comment.Result != null)
                {
                    result = comment;
                }
                else
                {
                    result = new ResponseGetDataModel<ResponseData<CommentDataModel>[]>
                    {
                        Status = 0,
                        Code = (int)HttpStatusCode.OK,
                        Message = "Create not success.",
                    };
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
                result = new ResponseGetDataModel<ResponseData<CommentDataModel>[]>
                {
                    Status = 0,
                    Code = (int)HttpStatusCode.OK,
                    Message = ex.Message,
                };
            }
            return result;
        }

        public static async Task<string> CreateAsync2(long mediaId, string parentCommentID, string jsonContent, long accountId)
        {
            var result = string.Empty;
            try
            {
                var obj = new
                {
                    data = new[]
                    {
                        new
                        {
                            mediaID = mediaId.ToString(),
                            parentCommentID = parentCommentID,
                            createdBy = accountId.ToString(),
                            content = Json.Parse<dynamic>(jsonContent),
                            mode = 0
                        }
                    }
                };

                var objJson = Json.Parse<object>(jsonContent);
                var headerParam = new Dictionary<string, string>();
                headerParam.Add("user-id", accountId.ToString());

                var url = AppSettings.Current.ChannelConfiguration.ApiCommentSetting.UrlReplyComment;

                var content = Json.Stringify(Json.Parse<dynamic>(jsonContent));
                Logger.Debug("Create comment content=>" + content);
                Logger.Debug("Create comment url=>" + url);
                Logger.Debug("Create comment body=>" + Json.Stringify(obj));
                Logger.Debug("Create comment header=>" + Json.Stringify(headerParam));

                result = await HttpClientPostDataAsync2<CommentDataModel>(url, Json.Stringify(obj), headerParam);
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
                result = ex.Message;
            }
            return result;
        }

        
        public static async Task<ResponseGetDataModel<long>> GetCountAsync(RequestDataModel search)
        {
            var result = default(ResponseGetDataModel<long>);
            try
            {
                var url = AppSettings.Current.ChannelConfiguration.ApiCommentSetting.UrlGetCount;
                var query = await (new FormUrlEncodedContent(new
                {
                    id = search.Id,
                    check = search.IsCheck,
                    delete = search.IsDelete,
                    text = search.Text,
                    media = search.PostId,
                    page = search.PageId,
                    fromDate = search.FromDate != null ? Utility.ConvertToTimestamp10(search.FromDate.Value).ToString() : "",
                    toDate = search.ToDate != null ? Utility.ConvertToTimestamp10(search.ToDate.Value).ToString() : ""
                }.ConvertToKeyValuePairForGetMethod())).ReadAsStringAsync();

                result = await HttpClientGetDataAsync<long>(url, query);
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
                result = new ResponseGetDataModel<long>
                {
                    Result = 0,
                    Status = 0,
                    Message = ex.Message,
                    Code = (int)HttpStatusCode.OK
                };
            }
            return result;
        }

        public static async Task<ResponseGetDataModel<ResultGetDataModel<CommentDataModel>>> SearchAsync(RequestDataModel search)
        {
            var result = default(ResponseGetDataModel<ResultGetDataModel<CommentDataModel>>);
            try
            {
                var url = AppSettings.Current.ChannelConfiguration.ApiCommentSetting.UrlSearchComment;
                var query = await (new FormUrlEncodedContent(new {
                    id = search.Id,
                    check = search.IsCheck,
                    delete = search.IsDelete,
                    text = search.Text,
                    media = search.PostId,
                    page = search.PageId,
                    limit = search.Limit,
                    beforeCursor = search.BeforeCursor,
                    afterCursor = search.AfterCursor,
                    fromDate = search.FromDate != null ? Utility.ConvertToTimestamp10(search.FromDate.Value).ToString() : "",
                    toDate = search.ToDate != null ? Utility.ConvertToTimestamp10(search.ToDate.Value).ToString() : "",
                    createdBy = search.CreatedBy,
                    isParent = search.IsParent
                }.ConvertToKeyValuePairForGetMethod())).ReadAsStringAsync();

                result = await HttpClientGetDataAsync<ResultGetDataModel<CommentDataModel>>(url, query);

                if (result != null && result.Result != null && result.Result.data != null && result.Result.data.Count() > 0)
                {
                    if(result.Result.data.Select(p => p?.Post?.Media_id)?.ToList() != null)
                    {
                        var listPost = await NewsRepository.GetListNewsAsync(result.Result.data.Select(p => p?.Post?.Media_id)?.ToList());
                        foreach (var item in result.Result.data)
                        {
                            var tmp = listPost?.Where(p => p != null && p.EncryptId != null && p.EncryptId.Equals(item.Post.Media_id))?.FirstOrDefault();
                            item.Post.PostName = tmp?.Keyword;
                            item.Post.PublishData = tmp?.PublishData;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
                result = new ResponseGetDataModel<ResultGetDataModel<CommentDataModel>>
                {
                    Status = 0,
                    Message = ex.Message,
                    Code = (int)HttpStatusCode.OK
                };
            }
            return result;
        }

        public static async Task<ResponseGetDataModel<object>> ApproveAsync(string id, CommentStatus? status, string logID, long accountId)
        {
            var result = default(ResponseGetDataModel<object>);
            try
            {
                var urlApprove = AppSettings.Current.ChannelConfiguration.ApiCommentSetting.UrlApproveComment;

                var headerParam = new Dictionary<string, string>();
                headerParam.Add("user-id", accountId.ToString());

                var content = new {
                    data = new[]
                    {
                        new {
                            commentID = id,
                            logID = logID,
                            status = (int)status
                        }
                    }
                };

                result = await HttpClientPostDataAsync<object>(urlApprove, Json.Stringify(content), headerParam);
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
                result = new ResponseGetDataModel<object>
                {
                    Status = 0,
                    Code = (int)HttpStatusCode.OK,
                    Message = "Có lỗi xảy ra.",
                };
            }
            return result;
        }

        public static async Task<ResponseGetDataModel<ResponseData<UserCommentResponse[]>>> BlockSearchAsync(RequestMemberModel search)
        {
            var result = default(ResponseGetDataModel<ResponseData<UserCommentResponse[]>>);
            try
            {
                var urlApprove = AppSettings.Current.ChannelConfiguration.ApiCommentSetting.UrlBlockSearch;

                var obj = new
                {
                    recordID = search.RecordID,
                    pageID = search.OfficerId?.ToString(),
                    userID = search.UserID?.ToString(),
                    lastUpdateTime = search.LastUpdateTime?.ConvertToTimestamp10(),
                    lastUpdateBy = search.LastUpdateBy,
                    status = search.Status,
                    beforeCursor = search.BeforeCursor,
                    afterCursor = search.AfterCursor,
                    fromDate = search.FromDate != null ? Utility.ConvertToTimestamp10(search.FromDate.Value).ToString() : "",
                    toDate = search.ToDate != null ? Utility.ConvertToTimestamp10(search.ToDate.Value).ToString() : "",
                    limit = search.Limit
                };

                var query = await new FormUrlEncodedContent(obj.ConvertToKeyValuePairForGetMethod()).ReadAsStringAsync();
               // Logger.Debug("Request data obj => " + Json.Stringify(obj));
                result = await HttpClientGetDataAsync<ResponseData<UserCommentResponse[]>>(urlApprove, query, null);
                if (result != null && result?.Result?.Data?.Count() > 0)
                {
                    var listId = string.Join(",", result.Result.Data.Select(p => p.UserID));
                    var body = Json.Stringify(new
                    {
                        user_id = listId
                    });
                    var urlGetInfo = AppSettings.Current.ChannelConfiguration.ApiUserKingHub.UrlGetUserInfo;
                    var infos = await HttpClientPostDataAsync3<UserInfoResponse[]>(urlGetInfo, body, null);
                    if (infos?.Data?.Count() > 0)
                    {
                        result.Result.Data.ToList().ForEach(p =>
                        {
                            var userCurrent = infos.Data.FirstOrDefault(f => f.user_id != null && f.user_id.Equals(p.UserID));
                            p.Avatar = userCurrent?.avatar;
                            p.FullName = userCurrent?.full_name;
                        });
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
                result = new ResponseGetDataModel<ResponseData<UserCommentResponse[]>>
                {
                    Status = 0,
                    Code = (int)HttpStatusCode.OK,
                    Message = "Có lỗi xảy ra.",
                };
            }
            return result;
        }

        public static async Task<ResponseGetDataModel<ResponseData<UserCommentResponse[]>>> ClassifySearchAsync(RequestMemberModel search)
        {
            var result = default(ResponseGetDataModel<ResponseData<UserCommentResponse[]>>);
            try
            {
                var urlApprove = AppSettings.Current.ChannelConfiguration.ApiCommentSetting.UrlClassifySearch;

                var obj = new
                {
                    recordID = search.RecordID,
                    pageID = search.OfficerId?.ToString(),
                    userID = search.UserID?.ToString(),
                    lastUpdateTime = search.LastUpdateTime?.ConvertToTimestamp10(),
                    lastUpdateBy = search.LastUpdateBy,
                    status = search.Status,
                    beforeCursor = search.BeforeCursor,
                    afterCursor = search.AfterCursor,
                    fromDate = search.FromDate != null ? Utility.ConvertToTimestamp10(search.FromDate.Value).ToString() : "",
                    toDate = search.ToDate != null ? Utility.ConvertToTimestamp10(search.ToDate.Value).ToString() : "",
                    limit = search.Limit
                };

                var query = await new FormUrlEncodedContent(obj.ConvertToKeyValuePairForGetMethod()).ReadAsStringAsync();

                result = await HttpClientGetDataAsync<ResponseData<UserCommentResponse[]>>(urlApprove, query, null);
                if (result != null && result?.Result?.Data?.Count() > 0)
                {
                    var listId = string.Join(",", result.Result.Data.Select(p => p.UserID));
                    var body = Json.Stringify(new
                    {
                        user_id = listId
                    });
                    var urlGetInfo = AppSettings.Current.ChannelConfiguration.ApiUserKingHub.UrlGetUserInfo;
                    var infos = await HttpClientPostDataAsync3<UserInfoResponse[]>(urlGetInfo, body, null);
                    if (infos?.Data?.Count() > 0)
                    {
                        result.Result.Data.ToList().ForEach(p =>
                        {
                            var userCurrent = infos.Data.FirstOrDefault(f => f.user_id != null && f.user_id.Equals(p.UserID));
                            p.Avatar = userCurrent?.avatar;
                            p.FullName = userCurrent?.full_name;
                        });
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
                result = new ResponseGetDataModel<ResponseData<UserCommentResponse[]>>
                {
                    Status = 0,
                    Code = (int)HttpStatusCode.OK,
                    Message = "Có lỗi xảy ra.",
                };
            }
            return result;
        }

        public static async Task<ResponseGetDataModel<ResponseData<UserCommentResponse[]>>> TopMemberSearchAsync(RequestMemberModel search)
        {
            var result = default(ResponseGetDataModel<ResponseData<UserCommentResponse[]>>);
            try
            {
                var urlApprove = AppSettings.Current.ChannelConfiguration.ApiCommentSetting.UrlTopMemberSearch;

                var obj = new
                {
                    recordID = search.RecordID,
                    pageID = search.OfficerId?.ToString(),
                    userID = search.UserID?.ToString(),
                    lastUpdateTime = search.LastUpdateTime?.ConvertToTimestamp10(),
                    lastUpdateBy = search.LastUpdateBy,
                    status = search.Status,
                    beforeCursor = search.BeforeCursor,
                    afterCursor = search.AfterCursor,
                    fromDate = search.FromDate != null ? Utility.ConvertToTimestamp10(search.FromDate.Value).ToString() : "",
                    toDate = search.ToDate != null ? Utility.ConvertToTimestamp10(search.ToDate.Value).ToString() : "",
                    limit = search.Limit
                };

                var query = await new FormUrlEncodedContent(obj.ConvertToKeyValuePairForGetMethod()).ReadAsStringAsync();
                result = await HttpClientGetDataAsync<ResponseData<UserCommentResponse[]>>(urlApprove, query, null);
                if (result != null && result?.Result?.Data?.Count() > 0)
                {
                    var listId = string.Join(",", result.Result.Data.Select(p => p.UserID));
                    var body = Json.Stringify(new
                    {
                        user_id = listId
                    });
                    var urlGetInfo = AppSettings.Current.ChannelConfiguration.ApiUserKingHub.UrlGetUserInfo;
                    var infos = await HttpClientPostDataAsync3<UserInfoResponse[]>(urlGetInfo, body, null);
                    if (infos?.Data?.Count() > 0)
                    {
                        result.Result.Data.ToList().ForEach(p =>
                        {
                            var userCurrent = infos.Data.FirstOrDefault(f => f.user_id != null && f.user_id.Equals(p.UserID));
                            p.Avatar = userCurrent?.avatar;
                            p.FullName = userCurrent?.full_name;
                        });
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
                result = new ResponseGetDataModel<ResponseData<UserCommentResponse[]>>
                {
                    Status = 0,
                    Code = (int)HttpStatusCode.OK,
                    Message = "Có lỗi xảy ra.",
                };
            }
            return result;
        }

        public static async Task<ResponseGetDataModel<ResponseData<UserCommentResponse[]>>> MemberSearchAsync(RequestMemberModel search)
        {
            var result = default(ResponseGetDataModel<ResponseData<UserCommentResponse[]>>);
            try
            {
                var urlApprove = AppSettings.Current.ChannelConfiguration.ApiCommentSetting.UrlMemberSearch;

                var obj = new
                {
                    recordID = search.RecordID,
                    pageID = search.OfficerId?.ToString(),
                    userID = search.UserID?.ToString(),
                    lastUpdateTime = search.LastUpdateTime?.ConvertToTimestamp10(),
                    lastUpdateBy = search.LastUpdateBy,
                    status = search.Status,
                    beforeCursor = search.BeforeCursor,
                    afterCursor = search.AfterCursor,
                    fromDate = search.FromDate != null ? Utility.ConvertToTimestamp10(search.FromDate.Value).ToString() : "",
                    toDate = search.ToDate != null ? Utility.ConvertToTimestamp10(search.ToDate.Value).ToString() : "",
                    limit = search.Limit
                };

                var query = await new FormUrlEncodedContent(obj.ConvertToKeyValuePairForGetMethod()).ReadAsStringAsync();
                // Logger.Debug("Request data obj => " + Json.Stringify(obj));

                result = await HttpClientGetDataAsync<ResponseData<UserCommentResponse[]>>(urlApprove, query, null);
                if(result != null && result?.Result?.Data?.Count() > 0)
                {
                    var listId = string.Join(",", result.Result.Data.Select(p => p.UserID));
                    var body = Json.Stringify(new
                    {
                        user_id = listId
                    });
                    var urlGetInfo = AppSettings.Current.ChannelConfiguration.ApiUserKingHub.UrlGetUserInfo;
                    var infos = await HttpClientPostDataAsync3<UserInfoResponse[]>(urlGetInfo, body, null);

                    //var apiPlatformConfig = AppSettings.Current.ChannelConfiguration.ApiPlatformConfig;
                    //var query2 = $"page-id={search.OfficerId}&user-ids={listId}";
                    //using(var client = new HttpClient()) { 

                    //}
                    //var listFollower = await HttpClientGetDataAsyncV2<dynamic>(apiPlatformConfig.GetFollower, query2);

                    if(infos?.Data?.Count() > 0)
                    {
                        result.Result.Data.ToList().ForEach(p =>
                        {
                            var userCurrent = infos.Data.FirstOrDefault(f => f.user_id != null && f.user_id.Equals(p.UserID));
                            p.Avatar = userCurrent?.avatar;
                            p.FullName = userCurrent?.full_name;
                        });
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
                result = new ResponseGetDataModel<ResponseData<UserCommentResponse[]>>
                {
                    Status = 0,
                    Code = (int)HttpStatusCode.OK,
                    Message = "Có lỗi xảy ra.",
                };
            }
            return result;
        }


        public static async Task<ResponseGetDataModel<UserCommentResponse>> BlockUpdateAsync(RequestMemberModel search, long accountId)
        {
            var result = default(ResponseGetDataModel<UserCommentResponse>);
            try
            {
                var urlApprove = AppSettings.Current.ChannelConfiguration.ApiCommentSetting.UrlBlockUpdate;

                var body = new UserCommentResponse
                {
                    RecordID = search.RecordID,
                    PageID = search.OfficerId?.ToString(),
                    UserID = search.UserID?.ToString(),
                    LastUpdateTime = search.LastUpdateTime?.ConvertToTimestamp10(),
                    LastUpdateBy = search.LastUpdateBy,
                    Status = search.Status
                };

                var headers = new Dictionary<string, string>();
                headers.Add("user-id", accountId.ToString());

                result = await HttpClientPostDataAsync<UserCommentResponse>(urlApprove, Json.StringifyFirstLowercase(body), headers);
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
                result = new ResponseGetDataModel<UserCommentResponse>
                {
                    Status = 0,
                    Code = (int)HttpStatusCode.OK,
                    Message = "Có lỗi xảy ra.",
                };
            }
            return result;
        }

        public static async Task<ResponseGetDataModel<UserCommentResponse>> ClassifyUpdateAsync(RequestMemberModel search, long accountId)
        {
            var result = default(ResponseGetDataModel<UserCommentResponse>);
            try
            {
                var urlApprove = AppSettings.Current.ChannelConfiguration.ApiCommentSetting.UrlClassifyUpdate;

                var body = new UserCommentResponse
                {
                    RecordID = search.RecordID,
                    PageID = search.OfficerId?.ToString(),
                    UserID = search.UserID?.ToString(),
                    LastUpdateTime = search.LastUpdateTime?.ConvertToTimestamp10(),
                    LastUpdateBy = search.LastUpdateBy,
                    Status = search.Status
                };

                var headers = new Dictionary<string, string>();
                headers.Add("user-id", accountId.ToString());

                result = await HttpClientPostDataAsync<UserCommentResponse>(urlApprove, Json.StringifyFirstLowercase(body), headers);
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
                result = new ResponseGetDataModel<UserCommentResponse>
                {
                    Status = 0,
                    Code = (int)HttpStatusCode.OK,
                    Message = "Có lỗi xảy ra.",
                };
            }
            return result;
        }

        public static async Task<ResponseGetDataModel<UserCommentResponse>> TopMemberUpdateAsync(RequestMemberModel search, long accountId)
        {
            var result = default(ResponseGetDataModel<UserCommentResponse>);
            try
            {
                var urlApprove = AppSettings.Current.ChannelConfiguration.ApiCommentSetting.UrlTopMemberUpdate;

                var body = new UserCommentResponse
                {
                    RecordID = search.RecordID,
                    PageID = search.OfficerId?.ToString(),
                    UserID = search.UserID?.ToString(),
                    LastUpdateTime = search.LastUpdateTime?.ConvertToTimestamp10(),
                    LastUpdateBy = search.LastUpdateBy,
                    Status = search.Status
                };

                var headers = new Dictionary<string, string>();
                headers.Add("user-id", accountId.ToString());

                result = await HttpClientPostDataAsync<UserCommentResponse>(urlApprove, Json.StringifyFirstLowercase(body), headers);
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
                result = new ResponseGetDataModel<UserCommentResponse>
                {
                    Status = 0,
                    Code = (int)HttpStatusCode.OK,
                    Message = "Có lỗi xảy ra.",
                };
            }
            return result;
        }


        #region function
        private static async Task<ResponseGetDataModel<T>> HttpClientPostDataAsync<T>(string url, FormUrlEncodedContent content, Dictionary<string, string> headerParams = null)
        {
            var result = default(ResponseGetDataModel<T>);
            try
            {
                using (var client = new HttpClient())
                {
                    if (headerParams != null && headerParams.Count > 0)
                    {
                        foreach (var param in headerParams)
                        {
                            client.DefaultRequestHeaders.Add(param.Key, param.Value);
                        }
                    }

                    client.Timeout = TimeSpan.FromSeconds(AppSettings.Current.ChannelConfiguration.HttpClientTimeOut);
                    Logger.Debug("Request data Url => " + url);
                    Logger.Debug("Request data Body => " + content);
                    Logger.Debug("Request data Header => " + Json.Stringify(headerParams));
                    var response = await client.PostAsync(url, content);
                    if (!response.IsSuccessStatusCode)
                    {
                        response.EnsureSuccessStatusCode();
                    }

                    var astr = await response.Content?.ReadAsStringAsync();
                    result = Json.Parse<ResponseGetDataModel<T>>(astr);
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
                result = new ResponseGetDataModel<T>
                {
                    Status = 0,
                    Message = ex.Message,
                    Code = (int)HttpStatusCode.OK
                };
            }
            return result;
        }

        private static async Task<ResponseGetDataModel<T>> HttpClientPostDataAsync<T>(string url, string jsonBody, Dictionary<string, string> headerParams = null)
        {
            var result = default(ResponseGetDataModel<T>);
            try
            {
                using (var client = new HttpClient())
                {
                    if (headerParams != null && headerParams.Count > 0)
                    {
                        foreach (var param in headerParams)
                        {
                            client.DefaultRequestHeaders.Add(param.Key, param.Value);
                        }
                    }

                    client.Timeout = TimeSpan.FromSeconds(AppSettings.Current.ChannelConfiguration.HttpClientTimeOut);

                    //Logger.Debug("Request data Url => " + url);
                    //Logger.Debug("Request data Body => " + jsonBody);
                    //Logger.Debug("Request data Header => " + Json.Stringify(headerParams));
                    var response = await client.PostAsync(url, new StringContent(jsonBody, Encoding.UTF8, "application/json"));

                    if (!response.IsSuccessStatusCode)
                    {
                        response.EnsureSuccessStatusCode();
                    }

                    var astr = await response.Content?.ReadAsStringAsync();
                    
                    result = Json.Parse<ResponseGetDataModel<T>>(astr);
                }
            }
            catch (Exception ex)
            {
                //Logger.Fatal("Url=>"+url+ ", Header=>user_id="+ headerParams["user_id"] + ", JsonBody=>"+ jsonBody + ", StringContent=>" + Json.Stringify(new StringContent(jsonBody, Encoding.UTF8, "application/json")));
                Logger.Error(ex, ex.Message);
                result = new ResponseGetDataModel<T>
                {
                    Status = 0,
                    Message = ex.Message,
                    Code = (int)HttpStatusCode.OK
                };
            }
            return result;
        }

        private static async Task<dynamic> HttpClientPostDataAsync2<T>(string url, string jsonBody, Dictionary<string, string> headerParams = null)
        {
            var result = string.Empty;
            try
            {
                using (var client = new HttpClient())
                {
                    if (headerParams != null && headerParams.Count > 0)
                    {
                        foreach (var param in headerParams)
                        {
                            client.DefaultRequestHeaders.Add(param.Key, param.Value);
                        }
                    }

                    client.Timeout = TimeSpan.FromSeconds(AppSettings.Current.ChannelConfiguration.HttpClientTimeOut);

                    var response = await client.PostAsync(url, new StringContent(jsonBody, Encoding.UTF8, "application/json"));

                    if (!response.IsSuccessStatusCode)
                    {
                        response.EnsureSuccessStatusCode();
                    }

                    result = await response.Content?.ReadAsStringAsync();
                    Logger.Debug("Response create comment result=>" + result);
                }
            }
            catch (Exception ex)
            {
                //Logger.Fatal("Url=>"+url+ ", Header=>user_id="+ headerParams["user_id"] + ", JsonBody=>"+ jsonBody + ", StringContent=>" + Json.Stringify(new StringContent(jsonBody, Encoding.UTF8, "application/json")));
                Logger.Error(ex, ex.Message);
                result = ex.Message;
            }
            return result;
        }

        public static async Task<ResponseUserInfoData<T>> HttpClientPostDataAsync3<T>(string url, string jsonBody, Dictionary<string, string> headerParams = null)
        {
            var result = default(ResponseUserInfoData<T>);
            try
            {
                using (var client = new HttpClient())
                {
                    if (headerParams != null && headerParams.Count > 0)
                    {
                        foreach (var param in headerParams)
                        {
                            client.DefaultRequestHeaders.Add(param.Key, param.Value);
                        }
                    }

                    client.Timeout = TimeSpan.FromSeconds(AppSettings.Current.ChannelConfiguration.HttpClientTimeOut);

                    //Logger.Debug("Request data Url => " + url);
                    //Logger.Debug("Request data Body => " + jsonBody);
                    //Logger.Debug("Request data Header => " + Json.Stringify(headerParams));
                    var response = await client.PostAsync(url, new StringContent(jsonBody, Encoding.UTF8, "application/json"));

                    if (!response.IsSuccessStatusCode)
                    {
                        response.EnsureSuccessStatusCode();
                    }

                    var astr = await response.Content?.ReadAsStringAsync();

                    result = Json.Parse<ResponseUserInfoData<T>>(astr);
                }
            }
            catch (Exception ex)
            {
                //Logger.Fatal("Url=>"+url+ ", Header=>user_id="+ headerParams["user_id"] + ", JsonBody=>"+ jsonBody + ", StringContent=>" + Json.Stringify(new StringContent(jsonBody, Encoding.UTF8, "application/json")));
                Logger.Error(ex, ex.Message);
                result = new ResponseUserInfoData<T>
                {
                    Status = 0,
                    Message = ex.Message,
                    Code = (int)HttpStatusCode.OK
                };
            }
            return result;
        }

        private static async Task<ResponseGetDataModel<T>> HttpClientGetDataAsync<T>(string url, string query, Dictionary<string, string> headerParams = null)
        {
            var result = default(ResponseGetDataModel<T>);
            string path = url + "?" + query;
            try
            {
                using (var client = new HttpClient())
                {
                    if (headerParams != null && headerParams.Count > 0)
                    {
                        foreach (var param in headerParams)
                        {
                            client.DefaultRequestHeaders.Add(param.Key, param.Value);
                        }
                    }

                    client.Timeout = TimeSpan.FromSeconds(AppSettings.Current.ChannelConfiguration.HttpClientTimeOut);


                    Logger.Debug("Url=>" + url + ", Quey=>" + query);

                    var response = await client.GetAsync(path);
                    if (!response.IsSuccessStatusCode)
                    {
                        response.EnsureSuccessStatusCode();
                    }

                    var astr = await response.Content?.ReadAsStringAsync();
                    result = Json.Parse<ResponseGetDataModel<T>>(astr);
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
                //Logger.Fatal(ex, "Path:"+ path + ", message" + ex.Message);
                result = new ResponseGetDataModel<T>
                {
                    Status = 0,
                    Message = ex.Message,
                    Code = (int)HttpStatusCode.OK
                };
            }
            return result;
        }

        private static async Task<ResponseDeleteDataModel<T>> HttpClienDeleteDataAsync<T>(string url, string query, Dictionary<string, string> headerParams = null)
        {
            var result = default(ResponseDeleteDataModel<T>);
            try
            {
                using (var client = new HttpClient())
                {
                    if (headerParams != null && headerParams.Count > 0)
                    {
                        foreach (var param in headerParams)
                        {
                            client.DefaultRequestHeaders.Add(param.Key, param.Value);
                        }
                    }

                    client.Timeout = TimeSpan.FromSeconds(AppSettings.Current.ChannelConfiguration.HttpClientTimeOut);
                    string path = url + "?" + query;
                    Logger.Debug("Path delete =>" + path);
                    var response = await client.DeleteAsync(path);
                    if (!response.IsSuccessStatusCode)
                    {
                        response.EnsureSuccessStatusCode();
                    }

                    var astr = await response.Content?.ReadAsStringAsync();
                    Logger.Debug("Response delete =>" + astr);
                    result = Json.Parse<ResponseDeleteDataModel<T>>(astr);
                    Logger.Debug("Response delete obj=>" + Json.Stringify(result));
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
                result = new ResponseDeleteDataModel<T>
                {
                    Status = 0,
                    Code = (int)HttpStatusCode.OK,
                    Message = ex.Message
                };
            }
            return result;
        }

        private static async Task<ResponsePutDataModel<T>> HttpClientPutDataAsync<T>(string url, string jsonBody, Dictionary<string, string> headerParams = null)
        {
            var result = default(ResponsePutDataModel<T>);
            try
            {
                using (var client = new HttpClient())
                {
                    client.Timeout = TimeSpan.FromSeconds(AppSettings.Current.ChannelConfiguration.HttpClientTimeOut);

                    if (headerParams != null && headerParams.Count > 0)
                    {
                        foreach (var param in headerParams)
                        {
                            client.DefaultRequestHeaders.Add(param.Key, param.Value);
                        }
                    }

                    var response = await client.PutAsync(url, new StringContent(jsonBody, Encoding.UTF8, "application/json"));

                    if (!response.IsSuccessStatusCode)
                    {
                        response.EnsureSuccessStatusCode();
                    }

                    var astr = await response.Content.ReadAsStringAsync();
                    result = Json.Parse<ResponsePutDataModel<T>>(astr);
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
                result = new ResponsePutDataModel<T>
                {
                    Status = 0,
                    Message = ex.Message,
                    Code = (int)HttpStatusCode.OK
                };
            }
            return result;
        }

        private static async Task<T> HttpClientGetDataAsyncV2<T>(string url, string query, Dictionary<string, string> headerParams = null)
        {
            var result = default(T);
            try
            {
                using (var client = new HttpClient())
                {
                    client.Timeout = TimeSpan.FromSeconds(AppSettings.Current.ChannelConfiguration.HttpClientTimeOut);

                    if (headerParams != null && headerParams.Count > 0)
                    {
                        foreach (var param in headerParams)
                        {
                            client.DefaultRequestHeaders.Add(param.Key, param.Value);
                        }
                    }
                    var path = $"{url}?{query}";
                    var response = await client.GetAsync(path);

                    if (!response.IsSuccessStatusCode)
                    {
                        response.EnsureSuccessStatusCode();
                    }

                    var astr = await response.Content.ReadAsStringAsync();
                    result = Json.Parse<T>(astr);
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
            }
            return result;
        }
        #endregion
    }
}
