﻿using ChannelVN.IMS2.Core.Entities;
using ChannelVN.IMS2.Core.Entities.Board;
using ChannelVN.IMS2.Core.Entities.Security;
using ChannelVN.IMS2.Core.Repositories;
using ChannelVN.IMS2.Core.Repositories.Constants;
using ChannelVN.IMS2.Core.Repositories.Functions;
using ChannelVN.IMS2.Foundation.Common;
using ChannelVN.IMS2.Foundation.Common.Configuration;
using ChannelVN.IMS2.Foundation.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;

namespace ChannelVN.IMS2.Core.Services
{
    public class SystemToolkitService
    {
        public async static Task<bool> InitRedisKeyMobileFromDB(string key)
        {
            var returnValue = false;
            var data = await SystemToolkitRepository.GetAllAccountAsync();
            if (data?.Count() > 0)
            {
                switch (key?.ToLower())
                {
                    case "mobile":
                        returnValue = await SystemToolkitRepository.InitRedisKeyMobileFromDB(data);
                        break;
                    case "email":
                        returnValue = await SystemToolkitRepository.InitRedisKeyEmailFromDB(data);
                        break;
                    default: break;
                }
            }

            return returnValue;
        }

        public async static Task<bool> DeleteRedisByKey(string key)
        {
            return await SystemToolkitRepository.DeleteRedisByKey(key);
        }

        public static Task<PagingDataResult<NewsAllSearch>> SearchCheckPostAsync(SearchNews search)
        {
            return SystemToolkitRepository.SearchCheckPostAsync(search);
        }

        public static async Task<int> PushPageOwnerAndTypeToAdtechAsync(string pageIds)
        {
            var returnValue = 0;
            try
            {
                var listPageId = new List<string>();
                if (!string.IsNullOrEmpty(pageIds))
                {
                    listPageId = pageIds.Split(",")?.ToList();
                }
                var listPage = await SecurityService.GetListByIdsAsync(listPageId);
                if (listPage != null && listPage.Count() > 0)
                {
                    foreach (var page in listPage)
                    {
                        if (page != null && page.Id > 0 && !string.IsNullOrEmpty(page.CreatedBy) && !string.IsNullOrEmpty(page.FullName))
                        {
                            await Function.AddToQueue(ActionName.PushPageOwnerAndTypeToAdtech, TopicName.PUSTPAGEINFO, new QueueData<long, string, byte?, string>
                            {
                                Data1 = page.Id,
                                Data2 = page.CreatedBy,
                                Data3 = page.Class,
                                Data4 = page.FullName
                            });
                            returnValue++;
                        }
                        else
                        {
                            Logger.Debug("PageInfoInvalid=>" + Json.Stringify(page));
                        }
                    }
                }
                //returnValue = SystemToolkitRepository.PushPageOwnerAndTypeToAdtechAsync(pageIds);
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
            }
            return returnValue;
        }

        public static async Task<int> PushBoardToAdtechAsync(string boardIds, string clientIP)
        {
            var returnValue = 0;
            try
            {
                var listBoardId = new List<string>();
                if (!string.IsNullOrEmpty(boardIds))
                {
                    listBoardId = boardIds.Split(",")?.ToList();
                }

                var listboard = await BoardService.GetListByIdAsync(listBoardId.Select(p => long.Parse(p)).ToArray());
                if (listboard != null && listboard.Count() > 0)
                {
                    foreach (var board in listboard)
                    {
                        if (board != null && board.Id > 0 && !string.IsNullOrEmpty(board.CreatedBy) && !string.IsNullOrEmpty(board.Name))
                        {
                            var officer = await SecurityRepository.GetByIdAsync(long.Parse(board.CreatedBy));
                            if (officer != null && officer.Type == (byte?)AccountType.Official && !string.IsNullOrEmpty(officer.CreatedBy))
                            {
                                var content = new FormUrlEncodedContent(new
                                {
                                    userid = board.CreatedBy,
                                    ownerid = officer.CreatedBy,
                                    boardname = board.Name,
                                    boardid = board.Id,
                                    avatar = Json.Stringify(new { url = board.Avatar, widh = BoardConstant.AvatarWidth, heigh = BoardConstant.AvatarHeight, content_type = (int)ContentType.Image }),
                                    cover = Json.Stringify(new { url = board.Cover, widh = BoardConstant.CoverWidth, heigh = BoardConstant.CoverHeight, content_type = (int)ContentType.Image }),
                                }.ConvertToKeyValuePair());

                                string query = await content?.ReadAsStringAsync();
                                await Function.AddToQueue(ActionName.PushBoardToChannel, TopicName.PUSHBOARDTOCHANNEL, new QueueData<string, string, string, PushInfo>
                                {
                                    Data1 = query,
                                    Data2 = BoardAction.Add,
                                    Data3 = clientIP,
                                    Data4 = new PushInfo
                                    {
                                        ObjectId = board.Id,
                                        ModifiedBy = board.CreatedBy
                                    }
                                });

                                returnValue++;
                            }
                            else
                            {
                                Logger.Debug("BoardInfoInvalid=>" + Json.Stringify(board));
                            }
                        }
                        else
                        {
                            Logger.Debug("BoardInfoInvalid=>" + Json.Stringify(board));
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
            }
            return returnValue;
        }
    }
}
