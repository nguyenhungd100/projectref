﻿using ChannelVN.IMS2.Core.Repositories;
using System.Threading.Tasks;

namespace ChannelVN.IMS2.Core.Services
{
    public static class CategoryService
    {
        public static Task<bool> Save()
        {
            return CategoryRepository.Save();
        }
    }
}
