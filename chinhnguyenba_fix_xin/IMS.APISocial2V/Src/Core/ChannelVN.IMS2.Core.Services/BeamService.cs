﻿using ChannelVN.IMS2.Core.Entities;
using ChannelVN.IMS2.Core.Entities.Beam;
using ChannelVN.IMS2.Core.Repositories;
using ChannelVN.IMS2.Foundation.Common;
using System;
using System.Threading.Tasks;
using static ChannelVN.IMS2.Core.Entities.ErrorCode.ErrorMapping;

namespace ChannelVN.IMS2.Core.Services
{
    public static class BeamService
    {
        public static Task<ErrorCodes> AddAsync(BeamSearch data, string clientIP)
        {
            return BeamRepository.AddAsync(data, clientIP);
        }

        public static Task<ErrorCodes> UpdateAsync(Beam data, ArticleInBeam[] articleInBeams, string clientIP, long officerId, string sessionId)
        {
            return BeamRepository.UpdateAsync(data, articleInBeams, clientIP, officerId, sessionId);
        }


        public static Task<ErrorCodes> UpdateStatusV2Async(long id, NewsStatus status, long userId, string clientIP, string sessionId)
        {
            return BeamRepository.UpdateStatusV2Async(id, status, userId, clientIP, sessionId);
        }

        public static Task<PagingDataResult<BeamSearchReturn>> SearchAsync(SearchNews data, long userId, bool isGetDistribution = false)
        {
            return BeamRepository.SearchAsync(data, userId, isGetDistribution);
        }

        public static Task<PagingDataResult<BeamSearch>> GetListApproveAsync(SearchNews data, long accountId)
        {
            return BeamRepository.GetListApproveAsync(data, accountId);
        }

        public static Task<PagingDataResult<NewsAll>> SearchNewsAsync(SearchNews data, long userId)
        {
            return BeamRepository.SearchNewsAsync(data, userId);
        }
        public static Task<Beam> GetByIdAsync(long id)
        {
            return BeamRepository.GetByIdAsync(id);
        }
        public static Task<BeamSearch> GetBeamSearchByIdAsync(long id)
        {
            return BeamRepository.GetBeamSearchByIdAsync(id);
        }

        public static Task<PagingDataResult<BeamSearch>> GetListDistributionAsync(SearchNews searchEntity, long userId)
        {
            return BeamRepository.GetListDistributionAsync(searchEntity, userId);
        }

        public static Task<long> GetCountByStatusAsync(int status, long officialId, long userId)
        {
            return BeamRepository.GetCountByStatusAsync(status, officialId, userId);
        }

        public static Task<PagingDataResult<NewsAll>> ArticleInBeamAsync(long beamId, int? pageIndex, int? pageSize)
        {
            return BeamRepository.ArticleInBeamAsync(beamId, pageIndex, pageSize);
        }

        public static Task<PagingDataResult<BeamSearch>> GetListRelationAsync(string beamIds, int? pageIndex, int? pageSize)
        {
            return BeamRepository.GetListRelationAsync(beamIds, pageIndex, pageSize);
        }
    }
}
