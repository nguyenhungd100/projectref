﻿using ChannelVN.IMS2.Core.Entities;
using ChannelVN.IMS2.Core.Entities.Distribution;
using ChannelVN.IMS2.Core.Entities.ErrorCode;
using ChannelVN.IMS2.Core.Entities.Newsletters;
using ChannelVN.IMS2.Core.Entities.Security;
using ChannelVN.IMS2.Core.Repositories;
using ChannelVN.IMS2.Foundation.Common;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ChannelVN.IMS2.Core.Services
{
    public static class ArticleService
    {
        public static Task<ErrorMapping.ErrorCodes> AddAsync(ArticleSearch data, string clientIP)
        {
            return ArticleRepository.AddAsync(data, clientIP);
        }

        public static Task<ErrorMapping.ErrorCodes> UpdateAsync(Article data, string clientIP, long officerId, string sessionId)
        {
            return ArticleRepository.UpdateAsync(data, clientIP, officerId, sessionId);
        }
        public static Task<ErrorMapping.ErrorCodes> UpdateStatusAsync(ArticleSearch dataDb, int statusOld, string clientIP)
        {
            return ArticleRepository.UpdateStatusAsync(dataDb, statusOld, clientIP);
        }

        public static Task<ErrorMapping.ErrorCodes> UpdateStatusV2Async(long id, NewsStatus status, long userId, string clientIP, string sessionId)
        {
            return ArticleRepository.UpdateStatusV2Async(id, status, userId, clientIP, sessionId);
        }

        public static Task<Article> GetByIdAsync(long id)
        {
            return ArticleRepository.GetByIdAsync(id);
        }
        public static Task<Article> GetByIdAsync_Test(long id, int mode)
        {
            return ArticleRepository.GetByIdAsync_Test(id, mode);
        }

        public static Task<ItemStreamDistribution> GetItemStreamByIdAsync(long id)
        {
            return ArticleRepository.GetItemStreamByIdAsync(id);
        }

        public static Task<PagingDataResult<string>> SearchItemStreamByIdAsync(long distributionId, AccountSearch account, DateTime? fromDate, DateTime? toDate)
        {
            return ArticleRepository.SearchItemStreamByIdAsync(distributionId, account, fromDate, toDate);
        }
        public static Task<PagingDataResult<ArticleSearch>> GetByStatusAsync(SearchNews searchNews, long userId)
        {
            return ArticleRepository.GetByStatusAsync(searchNews, userId);
        }
        public static Task<ArticleDetail> GetDetailByIdAsync(long id)
        {
            return ArticleRepository.GetDetailByIdAsync(id);
        }

        public static Task<PagingDataResult<ArticleSearch>> ListMyNewsAsync(long accountId, int pageIndex, int pageSize)
        {
            return ArticleRepository.ListMyNewsAsync(accountId, pageIndex, pageSize);
        }
        public static Task<PagingDataResult<ArticleSearch>> ListNewsPublishAsync(long accountId, int pageIndex, int pageSize)
        {
            return ArticleRepository.ListNewsPublishAsync(accountId, pageIndex, pageSize);
        }
        public static Task<PagingDataResult<ArticleSearch>> ListNewsByPublishStatusAsync(int publishStatus, long accountId, int pageIndex, int pageSize)
        {
            return ArticleRepository.ListNewsByPublishStatusAsync(publishStatus, accountId, pageIndex, pageSize);
        }

        public static Task<PagingDataResult<ArticleOnWall>> ListNewsByAuthorAsync(long accountId, long distributionId, int pageIndex, int pageSize)
        {
            return ArticleRepository.ListNewsByAuthorAsync(accountId, distributionId, pageIndex, pageSize);
        }
        public static Task<PagingDataResult<ArticleSearch>> ListNewsByDistributionAsync(long distributionId, int publishStatus, long accountId, DateTime? fromDate, DateTime? toDate, int pageIndex, int pageSize)
        {
            return ArticleRepository.ListNewsByDistributionAsync(distributionId, publishStatus, accountId, fromDate, toDate, pageIndex, pageSize);
        }
        public static Task<PagingDataResult<ItemStreamDistribution>> ListItemStreamByDistributionAsync(GetNewsByDistribution getNewsByDistributionEntity)
        {
            return ArticleRepository.ListItemStreamByDistributionAsync(getNewsByDistributionEntity);
        }
        public static Task<long> GetCountItemStreamByDistributionAsync(long distributionId, int? publishStatus)
        {
            return ArticleRepository.GetCountItemStreamByDistributionAsync(distributionId, publishStatus);
        }
        public static Task<PagingDataResult<ArticleSearchReturn>> SearchAsync(SearchNews data, long accountId, bool isGetDistribution = false)
        {
            return ArticleRepository.SearchAsync(data, accountId, isGetDistribution);
        }
        public static Task<PagingDataResult<ArticleSearchReturn>> GetListApproveAsync(SearchNews data, long accountId)
        {
            return ArticleRepository.GetListApproveAsync(data, accountId);
        }
        public static Task<List<Article>> GetListByIdsAsync(List<string> ids)
        {
            return ArticleRepository.GetListByIdsAsync(ids);
        }
        public static Task<bool> AcceptAsync(ItemStreamDistribution itemStream)
        {
            return ArticleRepository.AcceptAsync(itemStream);
        }

        public static Task<bool> RejectAsync(ItemStreamDistribution itemStream)
        {
            return ArticleRepository.RejectAsync(itemStream);
        }

        public static Task<int> GetPublishCountAsync(long authorId, int status, long? distributorId)
        {
            return ArticleRepository.GetPublishCountAsync(authorId, status, distributorId);
        }


        public static Task<ArticleSearch> GetArticleSearchByIdAsync(long id)
        {
            return ArticleRepository.GetArticleSearchByIdAsync(id);
        }
    }
}
