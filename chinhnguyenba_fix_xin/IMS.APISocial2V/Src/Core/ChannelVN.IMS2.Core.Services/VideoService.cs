﻿using ChannelVN.IMS2.Core.Entities;
using ChannelVN.IMS2.Core.Entities.Security;
using ChannelVN.IMS2.Core.Entities.Statistic;
using ChannelVN.IMS2.Core.Entities.Videos;
using ChannelVN.IMS2.Core.Repositories;
using ChannelVN.IMS2.Foundation.Common;
using System.Collections.Generic;
using System.Threading.Tasks;
using static ChannelVN.IMS2.Core.Entities.ErrorCode.ErrorMapping;

namespace ChannelVN.IMS2.Core.Services
{
    public static class VideoService
    {
        public static Task<ErrorCodes> AddAsync(VideoSearch video, string clientIP)
        {
            return VideoRepository.AddAsync(video, clientIP);
        }
        public static Task<PagingDataResult<VideoSearchReturn>> SearchAsync(SearchNews data, long userId, bool isGetDistribution = false)
        {
            return VideoRepository.SearchAsync(data, userId, isGetDistribution);
        }

        public static Task<PagingDataResult<Video>> GetListApproveAsync(SearchNews data, long accountId)
        {
            return VideoRepository.GetListApproveAsync(data, accountId);
        }

        public static Task<PagingDataResult<VideoSearch>> MyVideoAsync(SearchNews data, Account acc)
        {
            return VideoRepository.MyVideoAsync(data, acc);
        }

        public static Task<Video> GetByIdAsync(long id)
        {
            return VideoRepository.GetByIdAsync(id);
        }

        public static Task<VideoSearch> GetById3Async(long id)
        {
            return VideoRepository.GetById3Async(id);
        }

        public static Task<ErrorCodes> UpdateStatusAsync(VideoSearch data, int statusOld, string clientIP)
        {
            return VideoRepository.UpdateStatusAsync(data, statusOld, clientIP);
        }

        public static Task<ErrorCodes> UpdateStatusV2Async(long id, NewsStatus status, long userId, string clientIP, string sessionId)
        {
            return VideoRepository.UpdateStatusV2Async(id, status, userId, clientIP, sessionId);
        }

        public static Task<ErrorCodes> UpdateAsync(Video data, long officerId, string clientIP, string sessionId)
        {
            return VideoRepository.UpdateAsync(data, officerId, clientIP, sessionId);
        }
        public static Task<long> GetCountByStatus(string status, long officialId, Account acc)
        {
            return VideoRepository.GetCountByStatus(status, officialId, acc);
        }

        public static Task<PagingDataResult<VideoSearch>> GetListDistribution(SearchNews searchEntity, Account acc)
        {
            return VideoRepository.GetListDistribution(searchEntity, acc);
        }
        public static Task<PagingDataResult<Video>> GetListRelationAsync(string videoIds, int? pageIndex, int? pageSize)
        {
            return VideoRepository.GetListRelationAsync(videoIds, pageIndex, pageSize);
        }
        //public static Task<Dictionary<string, StatisticByAccount>> GetStatisticVideoByAccount(GetStatisticByAccount getStatisticByAccount)
        //{
        //    return VideoRepository.GetStatisticVideoByAccount(getStatisticByAccount);
        //}
        //public static Task<Dictionary<string, int>> GetStatisticVideoAllPage(GetStatisticAllPage getStatisticAllPage)
        //{
        //    return VideoRepository.GetStatisticVideoAllPage(getStatisticAllPage);
        //}
        //public static Task<PagingDataResult<string>> GetListFileNameSharedAsync(GetViewsByDistribution getViewsByDistribution, long? accountId)
        //{
        //    return VideoRepository.GetListFileNameSharedAsync(getViewsByDistribution, accountId);
        //}
        public static Task<VideoSearch> GetVideoSearchByIdAsync(long id)
        {
            return VideoRepository.GetVideoSearchByIdAsync(id);
        }
        public static Task<bool> AddToPlaylistAsync(long videoId, List<VideoPlaylist> dataDb)
        {
            return VideoRepository.AddToPlaylistAsync(videoId, dataDb);
        }
    }
}
