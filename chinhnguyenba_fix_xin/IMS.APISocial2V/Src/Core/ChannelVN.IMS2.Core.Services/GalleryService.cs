﻿using ChannelVN.IMS2.Core.Entities;
using ChannelVN.IMS2.Core.Entities.Album;
using ChannelVN.IMS2.Core.Entities.Distribution;
using ChannelVN.IMS2.Core.Entities.Gallery;
using ChannelVN.IMS2.Core.Entities.Media;
using ChannelVN.IMS2.Core.Entities.Security;
using ChannelVN.IMS2.Core.Repositories;
using ChannelVN.IMS2.Core.Repositories.Constants;
using ChannelVN.IMS2.Core.Repositories.Functions;
using ChannelVN.IMS2.Foundation.Common;
using ChannelVN.IMS2.Foundation.Common.Configuration;
using ChannelVN.IMS2.Foundation.Logging;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using static ChannelVN.IMS2.Core.Entities.ErrorCode.ErrorMapping;

namespace ChannelVN.IMS2.Core.Services
{
    public static class GalleryService
    {
        //public static async Task<long> SaveAsynce(AlbumSearch data, long officerId, CardType cardType)
        //{
        //    try
        //    {
        //        var pageAccount = await SecurityRepository.GetByIdAsync(officerId);

        //        data.Id = Generator.NewsId();

        //        var listItem = Json.Parse<PublishData>(data.PublishData);

        //        var itemAlbum = new List<ItemMediaUnit>();
        //        foreach (var item in listItem.Items)
        //        {
        //            itemAlbum.Add(new ItemMediaUnit
        //            {
        //                Id = Generator.NewsId().ToString(),
        //                Title = item.Title,
        //                ContentType = item.ContentType ?? item.Content_Type,
        //                Duration = item.Duration,
        //                Link = item.Link,
        //                Thumb = item.Thumb,
        //                Width = item.Width,
        //                Height = item.Height,
        //                TypeMedia = item.Type,
        //                Position = item.Position,
        //                IsPlay = item.IsPlay,
        //                CreatedDate = DateTime.Now
        //            });
        //        }

        //        var payloadDataShare = new PayloadDataShare
        //        {
        //            DistributionId = AppSettings.Current.Distribution.Automation.DistributorId,
        //            Title = listItem.Caption,
        //            Type = NewsType.Gallery1,
        //            MetaDataShared = new List<MetaDataShared>
        //                {
        //                    new MetaDataShared
        //                    {
        //                        ObjectId = data.Id,
        //                        Title = listItem.Caption,
        //                        Name = data.Name,
        //                        ZoneId = 0,
        //                        DistributionDate = data.DistributionDate,
        //                        CardType = cardType == CardType.Gallery1 ?  (int)CardType.Gallery1 :  (int)CardType.Gallery2,
        //                        Frameid = data.TemplateId ?? 0 ,
        //                        Tags = data.Tags,
        //                        UserName = pageAccount?.UserName,
        //                        UserId = pageAccount?.Id??0,
        //                        FullName = pageAccount?.FullName,
        //                        AuthorAvatar = pageAccount?.Avatar,
        //                        IsDeleted = data.Status == (int)NewsStatus.Remove||data.Status == (int)NewsStatus.Draft ? 1: (data.Status ==(int)NewsStatus.Archive ? 2 : 0),
        //                        //BoardIds = boardIds,
        //                        ItemMediaUnit = itemAlbum,
        //                        CaptionExt = listItem.CaptionExt,
        //                        CommentMode = data.CommentMode
        //                    }
        //                }
        //        };

        //        var distribution = await DistributionRepository.GetByIdAsync(payloadDataShare.DistributionId);

        //        //push queue trước khi đẩy sang kênh
        //        await Function.AddToQueue(ActionName.ShareGallery, TopicName.SHARE, new QueueData<string, PayloadDataShare, Distributor, string>
        //        {
        //            Data1 = Guid.NewGuid().ToString(),
        //            Data2 = payloadDataShare,
        //            Data3 = distribution,
        //            Data4 = "IP"
        //        });

        //        return data.Id;
        //    }
        //    catch (Exception ex)
        //    {
        //        Logger.Error(ex, ex.Message);
        //        return 0;
        //    }
        //}

        public static Task<ErrorCodes> AddAsync(GallerySearch data, string clientIP)
        {
            return GalleryRepository.AddAsync(data, clientIP);
        }

        public static Task<ErrorCodes> UpdateAsync(GallerySearch data, string clientIP, long OfficerId, string sessionId)
        {
            return GalleryRepository.UpdateAsync(data, clientIP, OfficerId, sessionId);
        }

        public static Task<ErrorCodes> UpdateStatusV2Async(long id, NewsStatus status, long userId, string clientIP, string sessionId)
        {
            return GalleryRepository.UpdateStatusV2Async(id, status, userId, clientIP, sessionId);
        }

        public static Task<GallerySearch> GetByIdAsync(long id)
        {
            return GalleryRepository.GetDetailByIdAsync(id);
        }

        public static Task<PagingDataResult<GallerySearch>> SearchAsync(SearchNews search, long accountId)
        {
            return GalleryRepository.SearchAsync(search, accountId);
        }
    }
}
