﻿using ChannelVN.IMS2.Core.Entities;
using ChannelVN.IMS2.Core.Entities.Media;
using ChannelVN.IMS2.Core.Repositories;
using ChannelVN.IMS2.Foundation.Common;
using System;
using System.Linq;
using System.Threading.Tasks;
using static ChannelVN.IMS2.Core.Entities.ErrorCode.ErrorMapping;

namespace ChannelVN.IMS2.Core.Services
{
    public static class MediaUnitService
    {
        public static Task<ErrorCodes> AddAsync(MediaUnitSearch data, string clientIP)
        {
            return MediaUnitRepository.AddAsync(data, clientIP);
        }

        public static Task<ErrorCodes> UpdateAsync(MediaUnit data, long officerId, string clientIP, string sessionId)
        {
            return MediaUnitRepository.UpdateAsync(data, officerId, clientIP, sessionId);
        }

        public static Task<ErrorCodes> UpdateStatusV2Async(long id, NewsStatus status, long userId, string clientIP, string sessionId)
        {
            return MediaUnitRepository.UpdateStatusV2Async(id, status, userId, clientIP, sessionId);
        }

        public static Task<MediaUnitSearch> GetByIdAsync(long id)
        {
            return MediaUnitRepository.GetDetailByIdAsync(id);
        }

        public static Task<PagingDataResult<MediaUnitReturn>> SearchAsync(SearchNews search, long accountId)
        {
            return MediaUnitRepository.SearchAsync(search, accountId);
        }

        public static Task<PagingDataResult<MediaUnit>> GetListApproveAsync(SearchNews data, long accountId)
        {
            return MediaUnitRepository.GetListApproveAsync(data, accountId);
        }

        public static async Task<PagingDataResult<MediaUnitSearch>> ListDistributionAsync(SearchNews search, long accountId)
        {
            var data = await MediaUnitRepository.ListDistributionAsync(search, accountId);
            if (data?.Data?.Count > 0)
            {
                data.Data = await MediaUnitRepository.GetListMediaUnitSearchByIdsAsync(data.Data.Select(s => s.Id.ToString()).ToList());
            }

            return data;
        }
    }
}
