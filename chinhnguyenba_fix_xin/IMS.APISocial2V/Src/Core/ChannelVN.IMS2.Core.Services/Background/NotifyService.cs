﻿using ChannelVN.IMS2.Foundation.Logging;
using Microsoft.Extensions.Hosting;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace ChannelVN.IMS2.Core.Services.Background
{
    public class NotifyService : IHostedService, IDisposable
    {
        public void Dispose()
        {
            Logger.Information("NotifyService dispose");
            this.Dispose();
        }

        public Task StartAsync(CancellationToken cancellationToken)
        {
            //Logger.Information("NotifyService starting...");

            //new Task(() =>
            //{
            //    try
            //    {
            //        var conf = new ConsumerConfig
            //        {
            //            GroupId = "test-consumer-group",
            //            BootstrapServers = "172.26.33.29:9092,172.26.33.31:9092,172.26.33.32:9092,172.26.33.33:9092",
            //            // Note: The AutoOffsetReset property determines the start offset in the event
            //            // there are not yet any committed offsets for the consumer group for the
            //            // topic/partitions of interest. By default, offsets are committed
            //            // automatically, so in this example, consumption will only start from the
            //            // earliest message in the topic 'my-topic' the first time you run the program.
            //            AutoOffsetReset = AutoOffsetReset.Earliest
            //        };
            //        using (var c = new ConsumerBuilder<Ignore, string>(conf).Build())
            //        {
            //            c.Subscribe("realtime-notify-dev");

            //            CancellationTokenSource cts = new CancellationTokenSource();
            //            Console.CancelKeyPress += (_, e) =>
            //            {
            //                e.Cancel = true; // prevent the process from terminating.
            //                cts.Cancel();
            //            };

            //            try
            //            {
            //                while (true)
            //                {
            //                    try
            //                    {
            //                        var cr = c.Consume(cts.Token);
            //                        Logger.Information($"Consumed message '{cr.Value}' at: '{cr.TopicPartitionOffset}'.");
            //                        //Console.WriteLine($"Consumed message '{cr.Value}' at: '{cr.TopicPartitionOffset}'.");
            //                    }
            //                    catch (ConsumeException e)
            //                    {
            //                        Logger.Error($"Error occured: {e.Error.Reason}");
            //                        break;
            //                        // Console.WriteLine();
            //                    }
            //                    catch (Exception ex)
            //                    {
            //                        Logger.Error(ex, ex.Message);
            //                        break;
            //                    }
            //                }
            //            }
            //            catch (OperationCanceledException)
            //            {
            //                // Ensure the consumer leaves the group cleanly and final offsets are committed.
            //                c.Close();
            //            }
            //        }
            //    }
            //    catch (Exception ex)
            //    {

            //    }
            //}).Start();
            return Task.CompletedTask;
        }

        public Task StopAsync(CancellationToken cancellationToken)
        {
            Logger.Information("NotifyService stop");
            return Task.CompletedTask;
        }
    }
}
