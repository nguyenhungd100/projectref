﻿using ChannelVN.IMS2.Core.Repositories.Streams;
using ChannelVN.IMS2.Foundation.Logging;
using Microsoft.Extensions.Hosting;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace ChannelVN.IMS2.Core.Services.Background
{
    public class StreamService : IHostedService, IDisposable
    {
        public IServiceProvider Services { get; }

        public StreamService(IServiceProvider services)
        {
            Services = services;
        }
        public void Dispose()
        {
            Logger.Dispose();
        }

        public Task StartAsync(CancellationToken cancellationToken)
        {
            //Logger.Information(
            //    "StreamService is starting.");
            DoWork();
            return Task.CompletedTask;
        }

        public Task StopAsync(CancellationToken cancellationToken)
        {
            //Logger.Information(
            //    "StreamService is stop.");
            return Task.CompletedTask;
        }

        private void DoWork()
        {
            new StreamManager();
            new StreamManager2();
        }
    }
}
