﻿using ChannelVN.IMS2.Core.Repositories.Constants;
using ChannelVN.IMS2.Foundation.Common.Configuration;
using ChannelVN.IMS2.Foundation.Logging;
using ChannelVN.IMS2.Foundation.Toolkit.Messaging;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace ChannelVN.IMS2.Core.Services.Background
{
    public class ScopedProcessingService : IScopedProcessingService
    {
        private int time = AppSettings.Current.JobAutoPushQueue.TimeInterval <= 60 ? 120 : AppSettings.Current.JobAutoPushQueue.TimeInterval;
        public async Task DoWork(CancellationToken cancellationToken)
        {
            //Logger.Information("Scoped Processing Service is working.");
            try
            {
                if (AppSettings.Current.JobAutoPushQueue.Enabled)
                {
                    await Task.WhenAny(Task.Run(() =>
                    {
                        ExchangeFactory.GetStream(TopicName.SHARE).Restore();
                        ExchangeFactory.GetStream(TopicName.AUTOSHAREVIDEO).Restore();
                        ExchangeFactory.GetStream(TopicName.AUTOSHAREVIDEOPLAYLIST).Restore();
                        ExchangeFactory.GetStream(TopicName.AUTOSHAREPOST).Restore();
                        ExchangeFactory.GetStream(TopicName.AUTOSHARESHARELINK).Restore();
                        ExchangeFactory.GetStream(TopicName.AUTOSHAREALBUM).Restore();
                        ExchangeFactory.GetStream(TopicName.AUTOSHAREMEDIAUNIT).Restore();
                        ExchangeFactory.GetStream(TopicName.AUTOSHAREPHOTO).Restore();
                        ExchangeFactory.GetStream(TopicName.AUTOSHAREARTICLE).Restore();
                        ExchangeFactory.GetStream(TopicName.AUTOSHAREBEAM).Restore();
                        ExchangeFactory.GetStream(TopicName.AUTOSHAREGALLERY).Restore();

                        ExchangeFactory.GetStream(TopicName.SQL_ACCOUNT).Restore();
                        ExchangeFactory.GetStream(TopicName.ES_ACCOUNT).Restore();
                        ExchangeFactory.GetStream(TopicName.SYNC_USERINFO).Restore();

                        ExchangeFactory.GetStream(TopicName.ES_VIDEO).Restore();
                        ExchangeFactory.GetStream(TopicName.SQL_VIDEO).Restore();

                        ExchangeFactory.GetStream(TopicName.ES_PLAYLIST).Restore();
                        ExchangeFactory.GetStream(TopicName.SQL_PLAYLIST).Restore();

                        ExchangeFactory.GetStream(TopicName.ES_POST).Restore();
                        ExchangeFactory.GetStream(TopicName.SQL_POST).Restore();

                        ExchangeFactory.GetStream(TopicName.ES_SHARE_LINK).Restore();
                        ExchangeFactory.GetStream(TopicName.SQL_SHARE_LINK).Restore();

                        ExchangeFactory.GetStream(TopicName.ES_ARTICLE).Restore();
                        ExchangeFactory.GetStream(TopicName.SQL_ARTICLE).Restore();

                        ExchangeFactory.GetStream(TopicName.ES_PHOTO).Restore();
                        ExchangeFactory.GetStream(TopicName.SQL_PHOTO).Restore();

                        ExchangeFactory.GetStream(TopicName.ES_MEDIA_UNIT).Restore();
                        ExchangeFactory.GetStream(TopicName.SQL_MEDIA_UNIT).Restore();

                        ExchangeFactory.GetStream(TopicName.ES_BOARD).Restore();
                        ExchangeFactory.GetStream(TopicName.SQL_BOARD).Restore();
                        ExchangeFactory.GetStream(TopicName.PUSHBOARDTOCHANNEL).Restore();

                        ExchangeFactory.GetStream(TopicName.ES_ALBUM).Restore();
                        ExchangeFactory.GetStream(TopicName.SQL_ALBUM).Restore();

                        ExchangeFactory.GetStream(TopicName.ES_GALLERY).Restore();
                        ExchangeFactory.GetStream(TopicName.SQL_GALLERY).Restore();

                        ExchangeFactory.GetStream(TopicName.ES_BEAM).Restore();
                        ExchangeFactory.GetStream(TopicName.SQL_BEAM).Restore();

                        ExchangeFactory.GetStream(TopicName.DELETE_INDEX_TEMP).Restore();
                    }),Task.Delay(TimeSpan.FromSeconds(time-10), cancellationToken));
                }
                else
                {
                    Logger.Information(DateTime.Now.ToString("HH:mm") + "Job auto Enabled: false.");
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
            }
        }
    }
}
