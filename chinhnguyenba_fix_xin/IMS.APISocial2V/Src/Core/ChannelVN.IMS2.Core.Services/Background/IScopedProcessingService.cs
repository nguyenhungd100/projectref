﻿using System.Threading;
using System.Threading.Tasks;

namespace ChannelVN.IMS2.Core.Services.Background
{
    public interface IScopedProcessingService
    {
        Task DoWork(CancellationToken cancellationToken);
    }
}
