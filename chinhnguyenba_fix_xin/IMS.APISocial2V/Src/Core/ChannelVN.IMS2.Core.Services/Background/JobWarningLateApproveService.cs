﻿using ChannelVN.IMS2.Foundation.Common.Configuration;
using ChannelVN.IMS2.Foundation.Logging;
using Microsoft.Extensions.Hosting;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ChannelVN.IMS2.Core.Services.Background
{
    public class JobWarningLateApproveService : IHostedService, IDisposable
    {
        private readonly IServiceProvider _services;
        private Timer _timer;
        private CancellationToken _cancellationToken;
        public JobWarningLateApproveService(IServiceProvider services)
        {
            _services = services;
        }
        
        public Task StartAsync(CancellationToken cancellationToken)
        {
            _cancellationToken = cancellationToken;
            var jobSetting = AppSettings.Current.JobWarningLateApproveSetting;
            var time = jobSetting.TimeInterval < 1 ? 5 : jobSetting.TimeInterval;
            if (jobSetting.Enabled)
            {
                var reset = new AutoResetEvent(false);
                _timer = new Timer(DoWork, reset, TimeSpan.Zero, TimeSpan.FromMinutes(time));
            }
            return Task.CompletedTask;
        }       

        private void DoWork(object state)
        {
            try
            {
                Console.WriteLine("JobWarningLateApprove running...");
                NewsOnHomeService.JobWarningLateApproveSetting();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        public Task StopAsync(CancellationToken cancellationToken)
        {
            _timer?.Change(Timeout.Infinite, 0);
            return Task.CompletedTask;
        }

        public void Dispose()
        {
            _timer?.Dispose();
            Logger.Dispose();
        }

    }
}
