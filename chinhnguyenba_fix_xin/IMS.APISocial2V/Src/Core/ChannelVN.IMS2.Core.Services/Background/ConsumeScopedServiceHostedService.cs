﻿using ChannelVN.IMS2.Foundation.Common.Configuration;
using ChannelVN.IMS2.Foundation.Logging;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace ChannelVN.IMS2.Core.Services.Background
{
    public class ConsumeScopedServiceHostedService : IHostedService, IDisposable
    {
        private Timer _timer;
        CancellationToken _cancellationToken;
        public ConsumeScopedServiceHostedService(IServiceProvider services)
        {
            Services = services;
        }

        public IServiceProvider Services { get; }

        public Task StartAsync(CancellationToken cancellationToken)
        {
            _cancellationToken = cancellationToken;
            //Logger.Information("Consume Scoped Service Hosted Service is starting.");
            AutoResetEvent reset = new AutoResetEvent(false);
            var time =  AppSettings.Current.JobAutoPushQueue.TimeInterval <= 60 ? 120 : AppSettings.Current.JobAutoPushQueue.TimeInterval;
            _timer = new Timer(DoWork, reset, TimeSpan.Zero, TimeSpan.FromSeconds(time));
            return Task.CompletedTask;
        }

        private void DoWork(object state)
        {
            //Logger.Information(
            //    "Consume Scoped Service Hosted Service is working.");
            GC.Collect();
            using (var scope = Services.CreateScope())
            {
                var scopedProcessingService =
                    scope.ServiceProvider
                        .GetRequiredService<IScopedProcessingService>();
                scopedProcessingService.DoWork(_cancellationToken);
            }
        }

        public Task StopAsync(CancellationToken cancellationToken)
        {
            //Logger.Information(
            //    "Consume Scoped Service Hosted Service is stopping.");
            _timer?.Change(Timeout.Infinite, 0);
            return Task.CompletedTask;
        }

        public void Dispose()
        {
            _timer?.Dispose();
            Logger.Dispose();
        }
    }
}
