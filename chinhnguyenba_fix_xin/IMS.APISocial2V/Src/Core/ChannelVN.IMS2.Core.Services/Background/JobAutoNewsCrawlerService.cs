﻿using ChannelVN.IMS2.Foundation.Common.Configuration;
using ChannelVN.IMS2.Foundation.Logging;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace ChannelVN.IMS2.Core.Services.Background
{
    public class JobAutoNewsCrawlerService : IHostedService, IDisposable
    {
        private Timer _timer;
        CancellationToken _cancellationToken;
        public JobAutoNewsCrawlerService(IServiceProvider services)
        {
            Services = services;
        }

        public IServiceProvider Services { get; }

        public Task StartAsync(CancellationToken cancellationToken)
        {
            _cancellationToken = cancellationToken;
            var time = AppSettings.Current.JobAutoNewsCrawlerSetting.TimeInterval < 1 ? 5 : AppSettings.Current.JobAutoNewsCrawlerSetting.TimeInterval;
            //Logger.Information("Consume Scoped Service Hosted Service is starting.");
            if (AppSettings.Current.JobAutoNewsCrawlerSetting.Enabled)
            {
                AutoResetEvent reset = new AutoResetEvent(false);
                _timer = new Timer(DoWork, reset, TimeSpan.Zero, TimeSpan.FromMinutes(time));
            }
            return Task.CompletedTask;
        }

        private void DoWork(object state)
        {
            try
            {
                Console.WriteLine("JobAutoNewsCrawlerSetting running...");
                NewsCrawlerService.JobAutoNewsCrawlerSetting();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        public Task StopAsync(CancellationToken cancellationToken)
        {            
            _timer?.Change(Timeout.Infinite, 0);
            return Task.CompletedTask;
        }

        public void Dispose()
        {
            _timer?.Dispose();
            Logger.Dispose();
        }
    }
}
