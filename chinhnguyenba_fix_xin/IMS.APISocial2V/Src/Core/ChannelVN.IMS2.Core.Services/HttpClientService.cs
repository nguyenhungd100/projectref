﻿using ChannelVN.IMS2.Core.Entities.Comment;
using ChannelVN.IMS2.Core.Entities.Statistic.Rearch.Response;
using ChannelVN.IMS2.Foundation.Common;
using ChannelVN.IMS2.Foundation.Common.Configuration;
using ChannelVN.IMS2.Foundation.Logging;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;

namespace ChannelVN.IMS2.Core.Services
{
    public class HttpClientService
    {
        public static async Task<ResponseDataModel<T>> GetDataAsync<T>(string url, string query, Dictionary<string, string> headerParams = null)
        {
            var result = default(ResponseDataModel<T>);
            try
            {
                using (var client = new HttpClient())
                {
                    if (headerParams != null && headerParams.Count > 0)
                    {
                        foreach (var param in headerParams)
                        {
                            client.DefaultRequestHeaders.Add(param.Key, param.Value);
                        }
                    }

                    client.Timeout = TimeSpan.FromSeconds(AppSettings.Current.ChannelConfiguration.HttpClientTimeOut);
                    string path = url + "?" + query;

                    var response = await client.GetAsync(path);
                    if (!response.IsSuccessStatusCode)
                    {
                        response.EnsureSuccessStatusCode();
                    }

                    var astr = await response.Content?.ReadAsStringAsync();

                    result = Json.Parse<ResponseDataModel<T>>(astr);
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
                result = new ResponseDataModel<T>
                {
                    Status = 0,
                    Message = ex.Message,
                    Code = (int)HttpStatusCode.OK
                };
            }
            return result;
        }

        public static async Task<T> GetData2Async<T>(string url, string query, Dictionary<string, string> headerParams = null)
        {
            var result = default(T);
            string path = url + "?" + query;
            try
            {
                using (var client = new HttpClient())
                {
                    if (headerParams != null && headerParams.Count > 0)
                    {
                        foreach (var param in headerParams)
                        {
                            client.DefaultRequestHeaders.Add(param.Key, param.Value);
                        }
                    }

                    client.Timeout = TimeSpan.FromSeconds(AppSettings.Current.ChannelConfiguration.HttpClientTimeOut);
                    
                    Logger.Debug("GetData2Async=> url:" + path);
                    var response = await client.GetAsync(path);
                    if (!response.IsSuccessStatusCode)
                    {
                        response.EnsureSuccessStatusCode();
                    }

                    var astr = await response.Content?.ReadAsStringAsync();
                    Logger.Debug("GetData2Async=> result:" + astr);
                    result = Json.Parse<T>(astr);
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
                Logger.Debug(ex, "GetData2Async=> url:" + path);
            }
            return result;
        }
    }
}
