﻿using ChannelVN.IMS2.Core.Entities;
using ChannelVN.IMS2.Core.Entities.Album;
using ChannelVN.IMS2.Core.Entities.Photo;
using ChannelVN.IMS2.Core.Repositories;
using ChannelVN.IMS2.Foundation.Common;
using ChannelVN.IMS2.Foundation.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using static ChannelVN.IMS2.Core.Entities.ErrorCode.ErrorMapping;

namespace ChannelVN.IMS2.Core.Services
{
    public static class AlbumService
    {
        public static Task<ErrorCodes> AddAsync(AlbumSearch data, string clientIP)
        {
            return AlbumRepository.AddAsync(data, clientIP);
        }

        public static Task<ErrorCodes> UpdateAsync(AlbumSearch data, string clientIP, long OfficerId, string sessionId)
        {
            return AlbumRepository.UpdateAsync(data, clientIP, OfficerId, sessionId);
        }

        public static Task<ErrorCodes> UpdateStatusV2Async(long id, NewsStatus status, long userId, string clientIP, string sessionId)
        {
            return AlbumRepository.UpdateStatusV2Async(id, status, userId, clientIP, sessionId);
        }

        public static Task<AlbumSearch> GetByIdAsync(long id)
        {
            return AlbumRepository.GetDetailByIdAsync(id);
        }
        

        public static Task<List<Album>> GetListByIdAsync(long[] ids)
        {
            return AlbumRepository.GetListByIdAsync(ids);
        }
        public static Task<PagingDataResult<AlbumSearchReturn>> SearchAsync(SearchNews search, long accountId)
        {
            return AlbumRepository.SearchAsync(search, accountId);
        }

        public static Task<PagingDataResult<AlbumSearch>> GetListApproveAsync(SearchNews data, long accountId)
        {
            return AlbumRepository.GetListApproveAsync(data, accountId);
        }

        public static Task<PagingDataResult<AlbumSearchReturn>> SearchV2Async(SearchNews search, long accountId)
        {
            return AlbumRepository.SearchV2Async(search, accountId);
        }
        public static async Task<PagingDataResult<AlbumSearch>> ListDistributionAsync(SearchNews search, long accountId)
        {
            try
            {
                var data = await AlbumRepository.ListDistributionAsync(search, accountId);
                if (data?.Data?.Count > 0)
                {
                    data.Data = await AlbumRepository.GetListAlbumSearchByIdsAsync(data.Data.Select(s => s.Id.ToString()).ToList());
                }
                return data;
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
                return new PagingDataResult<AlbumSearch>();
            }
        }
        public static Task<List<PhotoSearch>> GetListPhotoAsync(List<string> photoIds, int pageIndex, int pageSize)
        {
            return AlbumRepository.GetListPhotoAsync(photoIds, pageIndex, pageSize);
        }

        public static Task<PagingDataResult<PhotoUnit>> PhotoInAlbumIdAsync(long albumId, int? pageIndex, int? pageSize)
        {
            return AlbumRepository.PhotoInAlbumIdAsync(albumId, pageIndex, pageSize);
        }
    }
}
