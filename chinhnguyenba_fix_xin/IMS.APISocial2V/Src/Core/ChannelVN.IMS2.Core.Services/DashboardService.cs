﻿using ChannelVN.IMS2.Core.Entities.DashBoard;
using ChannelVN.IMS2.Core.Repositories;
using System.Threading.Tasks;

namespace ChannelVN.IMS2.Core.Services
{
    public static class DashboardService
    {
        public static Task<long> GetPostCountAsync(StatisticQuantity statisticEntity)
        {
            return DashboardRepository.GetPostCountAsync(statisticEntity);
        }
    }
}
