﻿using ChannelVN.IMS2.Core.Entities.Distribution;
using ChannelVN.IMS2.Core.Entities.Media;
using ChannelVN.IMS2.Foundation.Common;
using ChannelVN.IMS2.Foundation.Logging;
using System;
using System.Threading.Tasks;
using System.Web;

namespace ChannelVN.IMS2.Core.Services.Functions
{
    public static class Function
    {
        public static Task<bool> ValidatePublishData(string jsonPublishData, int cardType)
        {
            var returnValue = Task.FromResult(true);
            try
            {
                var listItem = default(PublishData);
                listItem = Json.Parse<PublishData>(HttpUtility.HtmlDecode(jsonPublishData));
                if (listItem != null)
                {
                    switch (cardType)
                    {
                        case (int)CardType.Photo:
                            if(listItem.Items != null)
                            {
                                foreach (var item in listItem.Items)
                                {
                                    if (string.IsNullOrEmpty(item.Link) || string.IsNullOrEmpty(item.Thumb) || item.Width == 0 || item.Height == 0)
                                    {
                                        returnValue = Task.FromResult(false);
                                        break;
                                    }
                                }
                            }
                            else
                            {
                                returnValue = Task.FromResult(false);
                            }
                            break;
                        case (int)CardType.Video:
                            if (listItem.Items != null)
                            {
                                foreach (var item in listItem.Items)
                                {
                                    if (string.IsNullOrEmpty(item.Link) || string.IsNullOrEmpty(item.Thumb) || item.Width == 0 || item.Height == 0)
                                    {
                                        returnValue = Task.FromResult(false);
                                        break;
                                    }
                                }
                            }
                            else
                            {
                                returnValue = Task.FromResult(false);
                            }
                            break;
                        case (int)CardType.PlaylistVideo:
                            if (listItem.Items != null)
                            {
                                foreach (var item in listItem.Items)
                                {
                                    if (string.IsNullOrEmpty(item.Link) || string.IsNullOrEmpty(item.Thumb) || item.Width == 0 || item.Height == 0)
                                    {
                                        returnValue = Task.FromResult(false);
                                        break;
                                    }
                                }
                            }
                            else
                            {
                                returnValue = Task.FromResult(false);
                            }
                            break;
                        case (int)CardType.VideoAndImage:
                            if (listItem.Items != null)
                            {
                                foreach (var item in listItem.Items)
                                {
                                    if (string.IsNullOrEmpty(item.Id) || string.IsNullOrEmpty(item.Link) || string.IsNullOrEmpty(item.Thumb) || item.Width == 0 || item.Height == 0)
                                    {
                                        returnValue = Task.FromResult(false);
                                        break;
                                    }
                                }
                            }
                            else
                            {
                                returnValue = Task.FromResult(false);
                            }
                            break;
                        case (int)CardType.ListNews:
                            if (listItem.Items != null)
                            {
                                foreach (var item in listItem.Items)
                                {
                                    if (string.IsNullOrEmpty(item.Link) || string.IsNullOrEmpty(item.Title))
                                    {
                                        returnValue = Task.FromResult(false);
                                        break;
                                    }
                                }
                            }
                            else
                            {
                                returnValue = Task.FromResult(false);
                            }
                            break;
                        case (int)CardType.Text:
                            break;
                        case (int)CardType.ShareNews:
                            if (listItem.Items != null)
                            {
                                foreach (var item in listItem.Items)
                                {
                                    if (string.IsNullOrEmpty(item.Link) || string.IsNullOrEmpty(item.Title) || string.IsNullOrEmpty(item.Source))
                                    {
                                        returnValue = Task.FromResult(false);
                                        break;
                                    }
                                }
                            }
                            else
                            {
                                returnValue = Task.FromResult(false);
                            }
                            break;
                        case (int)CardType.Album:
                            if (listItem.Items != null)
                            {
                                foreach (var item in listItem.Items)
                                {
                                    if (string.IsNullOrEmpty(item.Id) || string.IsNullOrEmpty(item.Link) || item.Width == 0 || item.Height == 0)
                                    {
                                        returnValue = Task.FromResult(false);
                                        break;
                                    }
                                }
                            }
                            else
                            {
                                returnValue = Task.FromResult(false);
                            }
                            break;
                        case (int)CardType.Article:
                            if (listItem.Items != null)
                            {
                                foreach (var item in listItem.Items)
                                {
                                    if (string.IsNullOrEmpty(item.Title) || string.IsNullOrEmpty(item.Link) || item.Image == null || string.IsNullOrEmpty(item.Image.Thumb) || item.Image.Width == 0 || item.Image.Height == 0)
                                    {
                                        returnValue = Task.FromResult(false);
                                        break;
                                    }
                                }
                            }
                            else
                            {
                                returnValue = Task.FromResult(false);
                            }
                            break;
                        case (int)CardType.Gallery1:
                            if (listItem.Items != null)
                            {
                                foreach (var item in listItem.Items)
                                {
                                    if (string.IsNullOrEmpty(item.Link) || item.Width == 0 || item.Height == 0)
                                    {
                                        returnValue = Task.FromResult(false);
                                        break;
                                    }
                                }
                            }
                            else
                            {
                                returnValue = Task.FromResult(false);
                            }
                            break;
                        case (int)CardType.Gallery2:
                            if (listItem.Items != null)
                            {
                                foreach (var item in listItem.Items)
                                {
                                    if (string.IsNullOrEmpty(item.Link) || item.Width == 0 || item.Height == 0)
                                    {
                                        returnValue = Task.FromResult(false);
                                        break;
                                    }
                                }
                            }
                            else
                            {
                                returnValue = Task.FromResult(false);
                            }
                            break;
                    }
                }
                else
                {
                    returnValue = Task.FromResult(false);
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
            }
            return returnValue;
        }
    }
}
