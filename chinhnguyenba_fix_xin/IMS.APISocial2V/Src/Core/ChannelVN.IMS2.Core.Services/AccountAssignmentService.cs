﻿using ChannelVN.IMS2.Core.Repositories;
using ChannelVN.IMS2.Foundation.Common;
using System.Threading.Tasks;
using ChannelVN.IMS2.Core.Entities.AccountAssignment;

namespace ChannelVN.IMS2.Core.Services
{
    public class AccountAssignmentService
    {
        public async static Task<bool> AddAsync(AccountAssignment data)
        {
            data.Id = string.Format("{0}:{1}",data.AccountId,data.OfficerId);
            return await AccountAssignmentRepository.AddAsync(data);
        }
        public static async Task<bool> UpdateAsync(AccountAssignment data)
        {
            try
            {
                data.Id = string.Format("{0}:{1}", data.AccountId, data.OfficerId);
                var objDb = await GetByIdAsync(data.Id);
                if (objDb != null)
                {
                    objDb.AssignedBy = data.AssignedBy;
                    objDb.AssignedDate = data.AssignedDate;
                    var result = await AccountAssignmentRepository.UpdateAsync(objDb);
                    if (result)
                    {
                        //đẩy sang Vĩ
                    }

                    return result;
                }
                return false;
            }
            catch
            {
                return false;
            }
        }
        public async static Task<bool> RemoveAsync(AccountAssignment data)
        {
            data.Id = string.Format("{0}:{1}", data.AccountId, data.OfficerId);
            return await AccountAssignmentRepository.RemoveAsync(data);
        }
        public async static Task<PagingDataResult<AccountAssignment>> SearchAsync(long accountId, long officerId, string assignedBy, int pageIndex, int pageSize)
        {
            return await AccountAssignmentRepository.SearchAsync(accountId, officerId, assignedBy, pageIndex, pageSize);            
        }
        public async static Task<PagingDataResult<object>> ListPageAsync(long accountId, long officerId, string assignedBy, int pageIndex, int pageSize)
        {
            return await AccountAssignmentRepository.ListPageAsync(accountId, officerId, assignedBy, pageIndex, pageSize);            
        }                        
        public static Task<AccountAssignment> GetByIdAsync(string id)
        {
            return AccountAssignmentRepository.GetByIdAsync(id);
        }        

        public static Task<bool> IsPageExistAsync(long accountId)
        {
            return AccountAssignmentRepository.IsPageExistAsync(accountId);
        }
    }
}
