﻿using ChannelVN.IMS2.Core.Entities.Distribution;
using ChannelVN.IMS2.Core.Entities.Security;
using ChannelVN.IMS2.Core.Models.ExternalAPI;
using ChannelVN.IMS2.Core.Repositories;
using ChannelVN.IMS2.Foundation.Common;
using ChannelVN.IMS2.Foundation.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace ChannelVN.IMS2.Core.Services
{
    public static class DistributionService
    {
        public static Task<bool> AddAsync(Distributor data)
        {
            return DistributionRepository.AddAsync(data);
        }

        public static Task<bool> UpdateAsync(Distributor data)
        {
            return DistributionRepository.UpdateAsync(data);
        }

        public static Task<Distributor> GetByIdAsync(long id)
        {
            return DistributionRepository.GetByIdAsync(id);
        }

        public static async Task<PagingDataResult<Distributor>> SearchAsync(string keyword, int? status, int pageIndex, int pageSize)
        {
            var res = new PagingDataResult<Distributor>();
            var data = await DistributionRepository.SearchAsync();

            res.Total = data.Count();
            if (status.HasValue)
            {
                res.Data = data.Where(s => s.Status == status.Value).Skip(pageSize * (pageIndex - 1)).Take(pageSize).ToList();
            }
            else
            {
                res.Data = data.Skip(pageSize * (pageIndex - 1)).Take(pageSize).ToList();
            }

            return res;
        }

        public static Task<bool> CheckItemStreamDistributionAsync(long distributionId, long itemStreamDistributionId)
        {
            return DistributionRepository.CheckItemStreamDistributionAsync(distributionId, itemStreamDistributionId);
        }

        public static async Task<List<ZoneVideo>> GetZoneVideoAsync(string config)
        {
            try
            {
                var configZoneVideo = Json.Parse<ConfigZoneVideo>(config);
                if (configZoneVideo != null)
                {
                    using (var client = new HttpClient())
                    {
                        //client.DefaultRequestHeaders.CacheControl.NoCache = true;
                        client.DefaultRequestHeaders.Add("Authorization", configZoneVideo.ApiKey);
                        var apiUrl =
                            $"{configZoneVideo.ApiUrl}?apiclient={configZoneVideo.ApiClient}&apikey={configZoneVideo.ApiKey}";

                        var result = Json.Parse<ResultData>(await (await client.GetAsync(apiUrl)).Content.ReadAsStringAsync());
                        if (result != null)
                            return result.Data;
                    }
                }

                return null;
            }
            catch (Exception ex)
            {
                Logger.Debug(ex, ex.Message);
                return null;
            }
        }

        public static async Task<ResultVideoPlayListPagingData> GetVideoPlaylistAsync(string config, string keyword, int pageIndex, int pageSize)
        {
            try
            {
                var configVideoPlaylist = Json.Parse<ConfigVideoPlaylist>(config);
                if (configVideoPlaylist != null)
                {
                    using (var client = new HttpClient())
                    {
                        //client.DefaultRequestHeaders.CacheControl.NoCache = true;
                        client.DefaultRequestHeaders.Add("Authorization", configVideoPlaylist.ApiKey);

                        var distribution = new List<KeyValuePair<string, string>>
                        {
                            new KeyValuePair<string, string>("Keyword", keyword),
                            new KeyValuePair<string, string>("PageIndex", pageIndex.ToString()),
                            new KeyValuePair<string, string>("PageSize", pageSize.ToString()),
                            new KeyValuePair<string, string>("apiclient", configVideoPlaylist.ApiClient),
                            new KeyValuePair<string, string>("apikey", configVideoPlaylist.ApiKey)
                        };

                        var content = new FormUrlEncodedContent(distribution);

                        var response = await client.PostAsync(configVideoPlaylist.ApiUrl, content);
                        response.EnsureSuccessStatusCode();

                        var responseString = await response.Content.ReadAsStringAsync();

                        var result = Json.Parse<ResultVideoPlayListData>(responseString);
                        if (result != null)
                            return result.Data;
                    }
                }

                return null;
            }
            catch (Exception ex)
            {
                Logger.Debug(ex, ex.Message);
                return null;
            }
        }

        public static Task<bool> ShareAsync(AccountSearch account, ItemStreamDistribution itemStreamDistribution, Distributor distribution, PayloadDataShare payloadDataShare, string clientIP)
        {
            var result = Task.FromResult(false);
            switch (itemStreamDistribution.Type)
            {
                case Entities.NewsType.Article:
                    result = DistributionRepository.ShareArticleAsync(itemStreamDistribution, distribution, payloadDataShare, clientIP, "", Media.Action.add);
                    break;

                case Entities.NewsType.Video:
                    result = DistributionRepository.ShareVideoAsync(itemStreamDistribution, distribution, payloadDataShare, clientIP,"",Media.Action.add, false);
                    break;

                case Entities.NewsType.PlayList:
                    result = DistributionRepository.ShareVideoPlaylistAsync(itemStreamDistribution, distribution, payloadDataShare, clientIP, "", Media.Action.add);
                    break;

                case Entities.NewsType.MediaUnit:
                    result = DistributionRepository.ShareMediaUnitAsync(itemStreamDistribution, distribution, payloadDataShare, clientIP, "", Media.Action.add);
                    break;
                case Entities.NewsType.Post:
                    result = DistributionRepository.SharePostAsync(itemStreamDistribution, distribution, payloadDataShare, clientIP);
                    break;
                case Entities.NewsType.Photo:
                    result = DistributionRepository.SharePhotoAsync(itemStreamDistribution, distribution, payloadDataShare, clientIP, "", Media.Action.add);
                    break;
                default: break;
            }

            return result;
        }
    }
}
