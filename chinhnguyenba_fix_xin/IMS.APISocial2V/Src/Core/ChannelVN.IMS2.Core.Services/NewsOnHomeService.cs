﻿using ChannelVN.IMS2.Core.Entities;
using ChannelVN.IMS2.Core.Repositories;
using ChannelVN.IMS2.Foundation.Common;
using System;
using System.Threading.Tasks;
using ChannelVN.IMS2.Core.Entities.NewsOnHome;
using ChannelVN.IMS2.Core.Models.ExternalAPI;
using System.Linq;
using ChannelVN.IMS2.Core.Entities.Security;
using ChannelVN.IMS2.Core.Services.VietId;
using System.Collections.Generic;
using ChannelVN.IMS2.Core.Repositories.Constants;
using ChannelVN.IMS2.Core.Repositories.Functions;
using static ChannelVN.IMS2.Core.Entities.ErrorCode.ErrorMapping;
using ChannelVN.IMS2.Foundation.Common.Configuration;

namespace ChannelVN.IMS2.Core.Services
{
    public class NewsOnHomeService
    {
        public async static Task<bool> AddAsync(NewsOnHome data, string sessionId, long accountId)
        {            
            //check page
            if (!data.OfficerClass.HasValue)
            {
                var page = await SecurityRepository.GetByIdAsync(data.OfficerId);
                if (page != null)
                {
                    data.OfficerClass = page.Class;
                }
            }
            var result = await NewsOnHomeRepository.AddAsync(data);
            if (result)
            {
                //check nếu page là hậu kiểm => đẩy luôn sang Vĩ
                if (data.OfficerClass == (byte)AccountClass.WaitingPrevCheck)
                {
                    //đẩy sang Vĩ
                    Media.AddOnHome(data.NewsId.ToString(), sessionId, (int)EnumStatusKingHub.NotOnHome, accountId);
                }
                //check nếu page là hậu kiểm => đẩy luôn sang Vĩ
                if (data.OfficerClass == (byte)AccountClass.WaitingPostCheck)
                {
                    //đẩy sang Vĩ
                    Media.AddOnHome(data.NewsId.ToString(), sessionId, (int)EnumStatusKingHub.OnHome, accountId);
                }

                //đẩy tele duyet
                Function.AddToQueue(ActionName.PushTelegramNotify, TopicName.PUSHTELEGRAMNOTIFY, new NotificationModel { Id=data.NewsId,Note="" });
            }
            return result;
        }
        public async static Task<PagingDataResult<NewsOnHomeSearch>> SearchAsync(string keyword, long officerId, long accountId, int? status, byte? officerClass, int pageIndex, int pageSize, OrderBy? orderBy, DateTime fromDate, DateTime toDate, string sessionId)
        {
            var data = new PagingDataResult<NewsOnHomeSearch>();
            var officerIds = new List<string>();
            if (officerId <= 0)
            {
                //list page folow
                var listPageFollow = await AccountAssignmentRepository.SearchAsync(accountId, 0, "", 1, 10000);
                if (listPageFollow != null && listPageFollow.Data != null && listPageFollow.Data.Count() > 0)
                {
                    officerIds = listPageFollow.Data.Select(s => s.OfficerId.ToString()).ToList();
                }
                if (officerIds == null || (officerIds != null && officerIds.Count <= 0))
                    return data;
            }
            else
            {
                officerIds.Add(officerId.ToString());
            }

            var result = await NewsOnHomeRepository.SearchAsync(keyword, officerIds, status, officerClass, pageIndex, pageSize, orderBy, fromDate, toDate);
            if (result != null && result.Data != null && result.Data.Count > 0)
            {
                if (string.IsNullOrEmpty(sessionId))
                {
                    var account = await SecurityRepository.GetByIdAsync(accountId);
                    if (account != null)
                    {
                        var acc = await SecurityRepository.GetByIdAsync(Utility.ConvertToLong(account.CreatedBy));
                        var accessToken = await DataVietIdService.GetAccessToken(acc.Mobile);
                        sessionId = (await DataVietIdService.GetSessionId(accessToken.Datas.AccessToken)).data.sessionId ?? "d4819674e1b50323d07a91216f88a6230000016daa998ad5";
                    }
                }

                var dataApi = await Media.GetByIds<NewsOnHomeSearch>(string.Join(',', result.Data.Select(s => s.NewsId)), sessionId);
                if (dataApi != null && dataApi.result != null && dataApi.result.data != null && dataApi.result.data.Count > 0)
                {
                    data.Total = result.Total;
                    foreach (var item in dataApi.result.data)
                    {
                        foreach (var item2 in result.Data)
                        {
                            if (item.media_id == item2.NewsId.ToString())
                            {
                                item.status = item2.Status;
                                item.officerClass = item2.OfficerClass;
                                item.OfficerId = item2.OfficerId.ToString();
                            }
                        }
                    }
                    data.Data = dataApi.result.data;
                }
            }
            return data;
        }

        public static async Task<bool> UpdateAsync(NewsOnHome data, string sessionId, long accountId)
        {
            try
            {
                var objDb = await GetByIdAsync(data.NewsId);
                if (objDb != null)
                {
                    var status = (int)EnumStatusKingHub.NotOnHome;
                    objDb.Status = data.Status;
                    if (data.Status == (int)EnumStatusNewsOnHome.AcceptedOnHome)
                    {
                        objDb.AcceptedBy = data.AcceptedBy;
                        objDb.AcceptedDate = data.AcceptedDate;
                        status = (int)EnumStatusKingHub.OnHome;
                    }
                    if (data.Status == (int)EnumStatusNewsOnHome.CanceledOnHome)
                    {
                        objDb.CanceledBy = data.CanceledBy;
                        objDb.CanceledDate = data.CanceledDate;
                        status = (int)EnumStatusKingHub.NotOnHome;
                    }
                    var result = await NewsOnHomeRepository.UpdateAsync(objDb);
                    if (result)
                    {
                        //đẩy sang Vĩ
                        var res = await Media.UpdateOnHome(objDb.NewsId.ToString(), sessionId, status, accountId);
                        if (res==ErrorCodes.Success)
                        {
                            //comment tạm chưa dùng tới notify sang app
                            //đẩy notify
                            //Function.AddToQueue(ActionName.PushNotificationPostUsers, TopicName.PUSHNOTIFICATIONAPP, objDb);
                        }
                    }

                    return result;
                }
                return false;
            }
            catch
            {
                return false;
            }
        }        

        public static Task<NewsOnHome> GetByIdAsync(long id)
        {
            return NewsOnHomeRepository.GetByIdAsync(id);
        }

        public static async Task<PagingDataResult<NewsOnHome>> SearchNewsOnHome(string keyword, List<string> officerIds, int? status, byte? officerClass, int pageIndex, int pageSize, OrderBy? orderBy, DateTime fromDate, DateTime toDate)
        {
            return await NewsOnHomeRepository.SearchNewsOnHome(keyword, officerIds, status, officerClass, pageIndex, pageSize, orderBy, fromDate, toDate);
        }

        public static async void JobWarningLateApproveSetting()
        {
            var searchCache = await CheckExistListSearchCacheAsync();
            if (searchCache != null && searchCache.Count > 0)
            {
                if (!NewsOnHomeRepository.CheckExistQueueJobNewsOnHome())
                {
                    var listToScans = GetListScanNewOnHome(searchCache);
                    NewsOnHomeRepository.PushQueueJobNewsOnHome(listToScans);
                }
                else
                {
                    var flagJob = true;
                    if (flagJob)
                    {
                        Task.Run(async () =>
                        {
                            flagJob = false;
                            while (true)
                            {
                                var popValue = NewsOnHomeRepository.PopQueueJobNewsOnHome();
                                if (popValue != null)
                                {
                                    await Function.AddToQueue(ActionName.PushTelegramNotify, TopicName.PUSHTELEGRAMNOTIFY, new { id = popValue.NewsId, note = "delay" });
                                }
                                else
                                {
                                    flagJob = false;
                                    break;
                                }
                            }
                            flagJob = true;
                        }).Wait();
                    }
                }              
            }
        }

        private static IEnumerable<NewsOnHome> GetListScanNewOnHome(List<NewsOnHome> searchCache)
        {
            var timeLates = AppSettings.Current.JobWarningLateApproveSetting.TimeLateDetermine?.Distinct();
            var lateMin = timeLates.Min(c => c.TimeDelay);
            var timeNow = DateTime.Now;
            foreach (var item in searchCache)
            {
                if (item.OrderedDate.HasValue && item.Status == (int)EnumStatusNewsOnHome.OrderOnHome)
                {
                    if (item.OrderedDate.Value.AddMinutes(lateMin) < timeNow)
                    {
                       yield return item;
                    }
                }
            }
        }

        private static async Task<List<NewsOnHome>> CheckExistListSearchCacheAsync()
        {
            if (!await NewsOnHomeRepository.ExistListSearchCacheAsync())
            {
                var searchCache = new List<NewsOnHome>();

                var searchResult = await NewsOnHomeRepository.SearchAsync("", null, (int)EnumStatusNewsOnHome.OrderOnHome, null,
                    1, 10000, OrderBy.CreatedDate_DESC, DateTime.MinValue, DateTime.MaxValue);

                if (searchResult != null && searchResult.Data != null && searchResult.Data.Count > 0)
                {
                    await NewsOnHomeRepository.PushListSearchToCacheAsync(searchResult.Data);
                }
                return searchResult.Data;
            }
            else
            {
                return await NewsOnHomeRepository.GetListSearchFromCacheAsync();
            }
        }
    }
}
