﻿using ChannelVN.IMS2.Core.Entities.VietId;
using ChannelVN.IMS2.Foundation.Common.Configuration;
using ChannelVN.IMS2.Foundation.Logging;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace ChannelVN.IMS2.Core.Services.VietId
{
    public class DataVietIdService
    {
        public static async Task<UserInfoResponse> GetUserInfo(string accessToken)
        {
            var url = string.Format("{0}/{1}", AppSettings.Current.ChannelConfiguration.AuthenVietIDSettings.ApiUrl, "getUserInfo");         
            Dictionary<string, string> dict = new Dictionary<string, string>()
            {
                {"access_token", accessToken}
            };
            var result = await RequestApiAsync<UserInfoResponse>(url, dict);
            return result;
        }

        public static async Task<EnterPinResponse> GetAccessToken(string mobile)
        {
            var url = string.Format("{0}/{1}", AppSettings.Current.ChannelConfiguration.AuthenVietIDSettings.ApiUrl, "enterPin");
            var timeStamp = new DateTimeOffset(DateTime.UtcNow).ToUnixTimeSeconds();
            var checkSum = MD5Hash(AppSettings.Current.ChannelConfiguration.AuthenVietIDSettings.ClientSecret + timeStamp);
            Dictionary<string, string> dict = new Dictionary<string, string>()
            {
                {"app_id", AppSettings.Current.ChannelConfiguration.AuthenVietIDSettings.ClientID},
                {"mobile",mobile },
                {"pin",AppSettings.Current.ChannelConfiguration.AuthenVietIDSettings.Pin },
                {"timestamp",timeStamp.ToString() },
                {"checksum",checkSum}
            };

            var result = await RequestApiAsync<EnterPinResponse>(url, dict);
            return result;
        }

        public static async Task<KinghubApiResponse> GetSessionId(string accessToken)
        {
            var url = AppSettings.Current.ChannelConfiguration.AuthenKingHubSetting.ApiUrl;
            var data = new Dictionary<string, string> { { "access_token", accessToken } };

            return await RequestApiAsync<KinghubApiResponse>(url, data);            
        }

        public static async Task<KinghubApiResponse> AddUserKingHub(string accessToken)
        {
            var url = AppSettings.Current.ChannelConfiguration.AddUserKingHubSetting.ApiUrl;
            var data = new Dictionary<string, string> { { "access_token", accessToken } };

            return await RequestApiAsync<KinghubApiResponse>(url, data);
        }

        private static async Task<T> RequestApiAsync<T>(string url, Dictionary<string, string> keyValuePairs)
        {
            string result = string.Empty;
            try
            {
                using (var client = new HttpClient())
                {
                    client.DefaultRequestHeaders.Accept.Clear();
                    var keyValues = new List<KeyValuePair<string, string>>();
                    if (keyValuePairs != null && keyValuePairs.Count > 0)
                    {
                        foreach (KeyValuePair<string, string> pair in keyValuePairs)
                        {
                            keyValues.Add(new KeyValuePair<string, string>(pair.Key, pair.Value));
                        }
                    }
                    var content = new FormUrlEncodedContent(keyValues);
                    var response = await client.PostAsync(url, content);
                    if (response.IsSuccessStatusCode || response.StatusCode == HttpStatusCode.OK)
                    {
                        result = await response.Content.ReadAsStringAsync();
                    }
                    T obj = JsonConvert.DeserializeObject<T>(result);
                    return obj;
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, "Request to VietID API: " + result, null);
                return default(T);
            }
        }

        private static string MD5Hash(string input)
        {
            StringBuilder hash = new StringBuilder();
            MD5CryptoServiceProvider md5provider = new MD5CryptoServiceProvider();
            byte[] bytes = md5provider.ComputeHash(new UTF8Encoding().GetBytes(input));

            for (int i = 0; i < bytes.Length; i++)
            {
                hash.Append(bytes[i].ToString("x2"));
            }
            return hash.ToString();
        }
    }  
}
