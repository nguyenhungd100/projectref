﻿using ChannelVN.IMS2.Core.Entities;
using ChannelVN.IMS2.Core.Entities.Board;
using ChannelVN.IMS2.Core.Repositories;
using ChannelVN.IMS2.Foundation.Common;
using ChannelVN.IMS2.Foundation.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using static ChannelVN.IMS2.Core.Entities.ErrorCode.ErrorMapping;

namespace ChannelVN.IMS2.Core.Services
{
    public static class BoardService
    {
        public static Task<ErrorCodes> AddAsync(BoardSearch board, long userId, string clientIP)
        {
            return BoardRepository.AddAsync(board, userId, clientIP);
        }
        public static async Task<ErrorCodes> UpdateAsync(Board data, long userId, long officerId, string clientIP)
        {
            try
            {
                var errorCode = await BoardRepository.UpdateAsync(data, officerId, clientIP);
                return errorCode;
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
                return ErrorCodes.Exception;
            }
        }

        public static async Task<ErrorCodes> UpdateMediaAsync(long boardId, long[] newsIds, long userId)
        {
            var returnCode = ErrorCodes.BusinessError;
            try
            {
                var board = await BoardRepository.GetByIdAsync(boardId);
                if (board == null || board.CreatedBy == null || board.CreatedBy.Equals(userId.ToString()))
                {
                    return ErrorCodes.PermissionInvalid;
                }
                var newsInBoard = await BoardRepository.NewsInBoardAsync(boardId);
                var arrAll = newsInBoard?.Union(newsIds);
                var listDelete = arrAll?.Except(newsIds);
                var listAdd = arrAll?.Except(newsInBoard);
                if ((listDelete != null && listDelete.Count() > 0) || (listAdd != null && listAdd.Count() > 0))
                {
                    board.LastInsertedDate = DateTime.Now;
                    returnCode = await BoardRepository.UpdateMediaAsync(listAdd, listDelete, board);
                }
                else
                {
                    returnCode = ErrorCodes.Success;
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
                returnCode = ErrorCodes.Exception;
            }
            return returnCode;
        }


        public static async Task<ErrorCodes> UpdateStatusAsync(long boardId, BoardStatus status, long userId, string clientIP)
        {
            try
            {
                var dataDb = await BoardRepository.GetBoardSearchByIdAsync(boardId);
                if (dataDb == null)
                {
                    return ErrorCodes.DataNotExist;
                }
                int statusOld = dataDb.Status ?? 1;
                dataDb.ModifiedDate = DateTime.SpecifyKind(DateTime.Now, DateTimeKind.Unspecified);
                dataDb.ModifiedBy = userId.ToString();
                dataDb.Status = (int)status;
                var errorCode = await BoardRepository.UpdateStatusAsync(dataDb, statusOld, clientIP);
                return errorCode;
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
                return ErrorCodes.Exception;
            }
        }


        public static Task<Board> GetByIdAsync(long boardId)
        {
            return BoardRepository.GetByIdAsync(boardId);
        }

        public static Task<PagingDataResult<BoardSearch>> ListAsync(long userId, int? status, int? pageIndex, int? pageSize)
        {
            return BoardRepository.ListAsync(userId, status, pageIndex, pageSize);
        }

        public static Task<bool> AddNewsAsync(long boardId, string ids, long officerId, long userId, string clientIP)
        {
            return BoardRepository.AddNewsAsync(boardId, ids, officerId, userId, clientIP);
        }

        public static Task<bool> DeleteNewsAsync(long officerId, long userId, long boardId, string newsIds, string clientIP)
        {
            return BoardRepository.DeleteNewsAsync(officerId, userId, boardId, newsIds, clientIP);
        }
        public static Task<PagingDataResult<NewsAllSearch>> NewsInBoardAsync(long officerId, long boardId, int? pageIndex, int? pageSize)
        {
            return BoardRepository.NewsInBoardAsync(officerId, boardId, pageIndex, pageSize);
        }
        public static Task<PagingDataResult<BoardSearch>> SearchAsync(SearchBoard data, long userId)
        {
            return BoardRepository.SearchAsync(data, userId);
        }

        public static Task<bool> CheckBoardOfUser(long officerId, long userId, long boardId)
        {
            return BoardRepository.CheckBoardOfUser(officerId, userId, boardId);
        }

        public static Task<bool> CheckBoardOfUser(long officerId, long userId, long[] boardIds)
        {
            return BoardRepository.CheckBoardOfUser(officerId, userId, boardIds);
        }

        public static Task<List<Board>> GetListByIdAsync(long[] boardIds)
        {
            return BoardRepository.GetListByIdAsync(boardIds);
        }

        public static Task<bool> InsertMediaAsync(long mediaId, List<Board> dataDb)
        {
            return BoardRepository.InsertMediaAsync(mediaId, dataDb);
        }
    }
}
