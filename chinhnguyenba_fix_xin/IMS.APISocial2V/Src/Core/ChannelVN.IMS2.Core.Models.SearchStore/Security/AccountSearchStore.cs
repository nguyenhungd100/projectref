﻿using ChannelVN.IMS2.Core.Entities.Security;
using ChannelVN.IMS2.Core.Entities.Statistics;
using ChannelVN.IMS2.Foundation.Common;
using ChannelVN.IMS2.Foundation.Logging;
using Nest;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ChannelVN.IMS2.Core.Models.SearchStore.Security
{
    public class AccountSearchStore : CmsMainSearchStore
    {
        public async Task<bool> AddAsync(AccountSearch data, bool isQueue = false)
        {
            var returnValue = false;
            try
            {
                var context = GetContext();
                {
                    var fieldNameAnalyzer = "fullName";
                    returnValue = await context.CreateIndexSettingsAsync<AccountSearch>(fieldNameAnalyzer);
                    returnValue = await context.CreateAsync(data);
                }
            }
            catch (Exception ex)
            {
                if(!isQueue) Logger.Error(ex, ex.Message);

            }
            return returnValue;
        }

        public async Task<bool> UpdateAsync(AccountSearch data, bool isQueue = false)
        {
            var returnValue = false;
            try
            {
                var context = GetContext();
                {
                    if ((await context.GetAsync<AccountSearch>(data.Id)) != null)
                    {
                        returnValue = await context.UpdateAsync<AccountSearch>(data.Id, data);
                    }
                    else
                    {
                        var fieldNameAnalyzer = "fullName";
                        returnValue = await context.CreateIndexSettingsAsync<AccountSearch>(fieldNameAnalyzer);
                        returnValue = await context.CreateAsync(data);
                    }
                }
            }
            catch (Exception ex)
            {
                if(!isQueue) Logger.Error(ex, ex.Message);
            }
            return returnValue;
        }

        public async Task<bool> LockOrUnlockMemberOnAllPageAsync(AccountMember[] data, bool isQueue = false)
        {
            var returnValue = false;
            try
            {
                var context = GetContext();
                {
                    for (var i = 0; i < data.Count(); i++)
                    {
                        returnValue = await context.UpdateAsync<AccountSearch>(data[i].OfficerId + IndexKeys.SeparateChar + data[i].MemberId, data[i]);
                    }
                }
            }
            catch (Exception ex)
            {
                if (!isQueue) Logger.Error(ex, ex.Message);
            }
            return returnValue;
        }

        public async Task<bool> AcceptOrRejectAsync(AccountSearch data, bool isQueue = false)
        {
            var returnValue = false;
            try
            {
                var context = GetContext();
                {
                    returnValue = await context.UpdateAsync<AccountSearch>(data.Id, data);
                }
            }
            catch (Exception ex)
            {
                if(!isQueue) Logger.Error(ex, ex.Message);
            }
            return returnValue;
        }
        public async Task<AccountSearch> GetByIdAsync(long id)
        {
            var returnValue = default(AccountSearch);
            try
            {
                var context = GetContext();
                {
                    returnValue = await context.GetAsync<AccountSearch>(id);
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
            }
            return returnValue;
        }

        public AccountSearch GetById(long id)
        {
            var returnValue = default(AccountSearch);
            try
            {
                var context = GetContext();
                {
                    returnValue = context.Get<AccountSearch>(id);
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
            }
            return returnValue;
        }

        public async Task<PagingDataResult<AccountSearch>> SearchByRegisterAccountStatusAsync(SearchAccountEntity search)
        {
            var data = new PagingDataResult<AccountSearch>();
            try
            {
                var context = GetContext();
                {
                    var searchDescriptor = new SearchDescriptor<AccountSearch>();
                    switch (search.OrderBy)
                    {
                        case 0:
                            searchDescriptor.Sort(s => s.Descending(p => p.CreatedDate));
                            break;
                        case 1:
                            searchDescriptor.Sort(s => s.Ascending(p => p.CreatedDate));
                            break;
                        default:
                            searchDescriptor.Sort(s => s.Descending(p => p.CreatedDate));
                            break;
                    }
                    var fields = new[] { "fullName", "fullName.folded", "email", "mobile" };
                    var termUserId = new TermQuery();
                    if (search.Id > 0)
                    {
                        termUserId.Field = "userId";
                        termUserId.Value = search.Id;
                    }
                    var termStatus = new TermQuery();
                    if (search.Status > 0)
                    {
                        termStatus.Field = "status";
                        termStatus.Value = search.Status;
                    }
                    var termAccountType = new TermQuery();
                    if (search.Type > 0)
                    {
                        termAccountType.Field = "accountType";
                        termAccountType.Value = search.Type;
                    }
                    var termAccountClass = new TermQuery();
                    if (search.Class > 0)
                    {
                        termAccountClass.Field = "accountClass";
                        termAccountClass.Value = search.Class;
                    }
                    var keyword = EscapeQueryBase.EscapeSearchQuery(search.Keyword);
                    if (search.Status != AccountStatus.Locked)
                    {
                        searchDescriptor.Query(q => q
                        .QueryString(mp => mp
                            .Query(keyword)
                            .Fields(f => f.Fields(fields))
                            .MinimumShouldMatch(MinimumShouldMatch.Percentage(100))
                            .DefaultOperator(Operator.And)
                        ) && termUserId && termStatus && termAccountType && termAccountClass //&& termRegisterAccountStatus
                        ).From((search.PageIndex - 1) * search.PageSize).Size(search.PageSize);
                    }
                    else
                    {
                        searchDescriptor.Query(q => q
                        .QueryString(mp => mp
                            .Query(keyword)
                            .Fields(f => f.Fields(fields))
                            .MinimumShouldMatch(MinimumShouldMatch.Percentage(100))
                            .DefaultOperator(Operator.And)
                        ) && termUserId && termStatus && termAccountType && termAccountClass
                        ).From((search.PageIndex - 1) * search.PageSize).Size(search.PageSize);
                    }
                    var result = await context.SearchExAsync(searchDescriptor);
                    if (result != null)
                    {
                        data.Total = (int)result.Total;
                        data.Data = result.Data.ToList();
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
            }
            return data;
        }

        public async Task<PagingDataResult<AccountSearch>> SearchAsync(SearchAccountEntity search)
        {
            var data = new PagingDataResult<AccountSearch>();
            try
            {
                var context = GetContext();
                {
                    var searchDescriptor = new SearchDescriptor<AccountSearch>();

                    switch (search.OrderBy)
                    {
                        case 0:
                            searchDescriptor.Sort(s => s.Descending(p => p.CreatedDate));
                            break;
                        case 1:
                            searchDescriptor.Sort(s => s.Ascending(p => p.CreatedDate));
                            break;
                        default:
                            searchDescriptor.Sort(s => s.Descending(p => p.CreatedDate));
                            break;
                    }
                    var fields = new[] { "fullName", "fullName.folded", "email", "mobile" };

                    var termUserId = new TermQuery();
                    if (search.Id != null && search.Id > 0)
                    {
                        termUserId.Field = "id";
                        termUserId.Value = search.Id;
                    }

                    var termStatus = new TermQuery();
                    if (search.Status != null)
                    {
                        termStatus.Field = "status";
                        termStatus.Value = (int)search.Status;
                    }
                    var termsLevels= new TermsQuery();
                    if (search.Role != null)
                    {
                        termsLevels.Field = "levels";
                        termsLevels.Terms = new string[] { ((int)search.Role).ToString() }; 
                    }

                    var termsAccountType = new TermsQuery();
                    if (search.Type != null)
                    {
                        termsAccountType.Field = "type";
                        termsAccountType.Terms = new string[] { ((int)search.Type).ToString() };
                    }
                    else
                    {
                        termsAccountType.Field = "type";
                        termsAccountType.Terms = new string[] { ((int)AccountType.ReadOnly).ToString(), ((int)AccountType.Personal).ToString() };
                    }

                    var termCreatedBy = new TermQuery();
                    if (search.CreatedBy != null)
                    {
                        termCreatedBy.Field = "createdBy";
                        termCreatedBy.Value = search.CreatedBy.ToString();
                    }

                    var termAccountClass = new TermQuery();
                    if (search.Class != null)
                    {
                        termAccountClass.Field = "class";
                        termAccountClass.Value = (int)search.Class;
                    }

                    var keyword = EscapeQueryBase.EscapeSearchQuery2(search.Keyword);

                    searchDescriptor.Query(q => q
                    .QueryString(mp => mp
                        .Query(keyword)
                        .Fields(f => f.Fields(fields))
                        .MinimumShouldMatch(MinimumShouldMatch.Percentage(100))
                        .DefaultOperator(Operator.And)
                    ) && termStatus && termsAccountType && termAccountClass && termCreatedBy && termsLevels && q.Bool(b => b.MustNot(termUserId)))//&& q.Bool(b => b.MustNot(m => m.Exists(e => e.Field("createdBy"))))
                        .From((search.PageIndex - 1) * search.PageSize).Size(search.PageSize);
                    var result = await context.SearchExAsync(searchDescriptor);
                    if (result != null)
                    {
                        data.Total = (int)result.Total;
                        data.Data = result.Data.ToList();
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
            }
            return data;
        }

        public async Task<bool> UpdateAccountAsync(AccountSearch accDb)
        {
            var returnValue = false;
            try
            {
                var context = GetContext();
                {
                    returnValue = await context.UpdateAsync<AccountSearch>(accDb.Id, accDb);
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
            }
            return returnValue;
        }

        public async Task<bool> UpdateAccountOpentIdAsync(AccountSearch accDb)
        {
            var returnValue = false;
            try
            {
                var context = GetContext();
                {
                    returnValue = await context.UpdateAsync<AccountSearch>(accDb.Id, accDb);
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
            }
            return returnValue;
        }

        public async Task<bool> AcceptOrRejectOrLockWriterAsync(AccountSearch data)
        {
            var returnValue = false;
            try
            {
                var context = GetContext();
                {
                    var obj = await context.GetAsync<AccountSearch>(data.Id);
                    if (obj != null)
                    {
                        obj.Status = data.Status;
                        returnValue = await context.UpdateAsync<AccountSearch>(obj.Id, obj);
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
            }
            return returnValue;
        }

        public async Task<long> GetCountByUserNameAsync(string userName)
        {
            try
            {
                var context = GetContext();
                {
                    var termUserName = new TermQuery
                    {
                        Field = "userName",
                        Value = userName
                    };
                    var result = await context.CountAsync<AccountSearch>(c => c
                         .Index("vccorp_account")
                         .Type("account")
                         .Query(q => termUserName)
                    );
                    if (result != null)
                    {
                        return result.Count;
                    }
                    else
                    {
                        return 0;
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
                return 0;
            }
        }

        public async Task<bool> UpdateStatusAsync(AccountSearch account, bool isQueue = false)
        {
            var returnValue = false;
            try
            {
                var context = GetContext();
                {
                    returnValue = await context.UpdateAsync<AccountSearch>(account.Id, account);
                }
            }
            catch (Exception ex)
            {
                if(!isQueue) Logger.Error(ex, ex.Message);
            }
            return returnValue;
        }

        public async Task<bool> DeActiveAsync(long id)
        {
            var returnValue = false;
            try
            {
                var context = GetContext();
                {
                    var obj = await context.GetAsync<AccountSearch>(id);
                    if (obj != null)
                    {
                        returnValue = await context.UpdateAsync<AccountSearch>(obj.Id, obj);
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
            }
            return returnValue;
        }

        public async Task<bool> InitAllAccountAsync(List<AccountSearch> listAccount)
        {
            var returnValue = false;
            try
            {
                var context = GetContext();
                {
                    returnValue = await context.CreateIndexSettingsAsync<AccountSearch>("FullName");
                    returnValue = await context.CreateManyAsync(listAccount);
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
            }
            return returnValue;
        }

        public async Task<PagingDataResult<AccountSearch>> ListPageManagerAsync(SearchPage search)
        {
            var data = new PagingDataResult<AccountSearch>();
            try
            {
                Logger.Debug("vao search");
                var context = GetContext();
                {
                    var searchDescriptor = new SearchDescriptor<AccountSearch>();

                    var termType = new TermQuery();
                    termType.Field = "type";
                    termType.Value = (byte?)AccountType.Official;

                    var termsClass = new TermsQuery();
                    if(search.Levels != 1)
                    {
                        if (search.ListClass != null && search.ListClass.Count() > 0)
                        {
                            var listClass = search.ListClass.Select(p => ((int)p)).ToList();
                            termsClass.Field = "class";
                            termsClass.Terms = listClass?.Select(p => ((int)p).ToString());
                        }
                    }

                    var termClass = new TermQuery();
                    var termNotClass = new TermsQuery();
                    if (search.Class == AccountClass.Normal)
                    {
                        var listClass = new List<AccountClass>();
                        listClass.AddRange(Enum.GetValues(typeof(AccountClass)).Cast<AccountClass>().Where(p=>p != AccountClass.Normal).ToList());
                        termNotClass.Field = "class";
                        termNotClass.Terms = listClass?.Select(p => ((int)p).ToString());
                    }
                    else
                    {
                        if (search.Class != null)
                        {
                            termClass.Field = "class";
                            termClass.Value = (int)search.Class;
                        }
                    }

                    var termStatus = new TermQuery();
                    if (search.Status != null)
                    {
                        termStatus.Field = "status";
                        termStatus.Value = (byte?)search.Status;
                    }

                    var fields = new[] { "fullName", "fullName.folded", "email", "mobile" };

                    //Logger.Debug("termsClass=>" + Json.Stringify(new
                    //{
                    //    termsClass,
                    //    termClass,
                    //    termNotClass
                    //}));

                    var keyword = EscapeQueryBase.EscapeSearchQuery2(search.Keyword);
                    searchDescriptor.Query(q => q
                    .QueryString(mp => mp
                        .Query(keyword)
                        .Fields(f => f.Fields(fields))
                        .MinimumShouldMatch(MinimumShouldMatch.Percentage(100))
                        .DefaultOperator(Operator.And)
                    ) && q.Bool(b => b.Must(m => m.Exists(e => e.Field("createdBy")))) && termsClass && termType && termStatus
                    && termClass && q.Bool(b=>b.MustNot(termNotClass))

                    ).From((search.PageIndex - 1) * search.PageSize).Size(search.PageSize);
                    var result = await context.SearchExAsync(searchDescriptor);
                    if (result != null)
                    {
                        data.Total = (int)result.Total;
                        data.Data = result.Data.ToList();
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
            }
            return data;
        }
        #region AccountMember

        public async Task<bool> AddAccountMemberAsync(AccountMember accountMember, AccountSearch officerAcc)
        {
            var returnValue = false;
            try
            {
                var context = GetContext();
                {
                    returnValue = await context.CreateAsync(officerAcc);
                    returnValue = await context.CreateAsync(accountMember);
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
            }
            return returnValue;
        }

        public async Task<bool> InitAllAccountMemberAsync(List<AccountMember> listAccountMember)
        {
            var returnValue = false;
            try
            {
                var context = GetContext();
                {
                    returnValue = await context.CreateManyAsync(listAccountMember);
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
            }
            return returnValue;
        }

        public async Task<bool> Update_AccountMemberAsync(AccountMember accountMember, bool isQueue = false)
        {
            var returnValue = false;
            try
            {
                var context = GetContext();
                {
                    var accDB = await context.GetAsync<AccountSearch>(accountMember.OfficerId);
                    if (accDB != null && accDB.AccountMember != null)
                    {
                        foreach (var member in accDB.AccountMember)
                        {
                            if (member.MemberId == accountMember.MemberId)
                            {
                                member.Role = accountMember.Role;
                            }
                        }
                    }
                    returnValue = await context.UpdateAsync<AccountSearch>(accDB.Id, accDB);
                }
            }
            catch (Exception ex)
            {
                if(!isQueue) Logger.Error(ex, ex.Message);
            }
            return returnValue;
        }

        public async Task<bool> RemoveMemberAsync(long accountId, long memberId, bool isQueue = false)
        {
            var returnValue = false;
            try
            {
                var context = GetContext();
                {
                    var accDB = await context.GetAsync<AccountSearch>(accountId);
                    if (accDB != null && accDB.AccountMember != null)
                    {
                        accDB.AccountMember = accDB.AccountMember.Where(p => p.MemberId != memberId).ToArray();
                    }
                    returnValue = await context.UpdateAsync<AccountSearch>(accDB.Id, accDB);
                }
            }
            catch (Exception ex)
            {
                if(!isQueue) Logger.Error(ex, ex.Message);
            }
            return returnValue;
        }

        public async Task<bool> RemoveMemberOnAllPageAsync(IEnumerable<long> listOfficerId, long memberId, bool isQueue = false)
        {
            var returnValue = false;
            try
            {
                var context = GetContext();
                {
                    var listDb = await context.GetManyAsync<AccountSearch>(new List<string>() { listOfficerId.ToString() },context.NameOf(typeof(AccountSearch).Name), typeof(AccountSearch).Name);
                    if (listDb != null && listDb.Count() > 0)
                    {
                        var listAccount = listDb.Select(p => p.Source)?.ToList();
                        for(var i = 0; i < listAccount.Count(); i++)
                        {
                            listAccount[i].AccountMember = listAccount[i].AccountMember?.Where(a => a.MemberId != memberId)?.ToArray();
                            returnValue = await context.UpdateAsync<AccountSearch>(listAccount[i].Id, listAccount[i]);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                if (!isQueue) Logger.Error(ex, ex.Message);
            }
            return returnValue;
        }

        public async Task<bool> LockOrUnlockMemberAsync(AccountMember data, bool isQueue = false)
        {
            var returnValue = false;
            try
            {
                var context = GetContext();
                {
                    var accDB = await context.GetAsync<AccountSearch>(data.OfficerId);
                    if (accDB != null && accDB.AccountMember != null)
                    {
                        accDB.AccountMember?.ToList().ForEach(p=>
                        {
                            if (p.MemberId == data.MemberId) p = data;
                        });
                    }
                    returnValue = await context.UpdateAsync<AccountSearch>(accDB.Id, accDB);
                }
            }
            catch (Exception ex)
            {
                if (!isQueue) Logger.Error(ex, ex.Message);
            }
            return returnValue;
        }

        public async Task<PagingDataResult<AccountSearch>> MyOfficialAsync(string keyword, long userId, int? pageIndex, int? pageSize)
        {
            var data = new PagingDataResult<AccountSearch>();
            try
            {
                var context = GetContext();
                {
                    var searchDescriptor = new SearchDescriptor<AccountSearch>();

                    var fields = new[] { "fullName", "fullName.folded", "email", "mobile" };
                    var termType = new TermQuery()
                    {
                        Field = "type",
                        Value = 2
                    };
                    var termMemberId = new TermQuery()
                    {
                        Field = "accountMember.memberId",
                        Value = userId.ToString()
                    };
                    var nestTermMember = new NestedQuery()
                    {
                        Path = "accountMember",
                        Query = termMemberId
                    };

                    var keywordSearch = EscapeQueryBase.EscapeSearchQuery(keyword);
                    searchDescriptor.Query(q => q
                    .QueryString(mp => mp
                        .Query(keywordSearch)
                        .Fields(f => f.Fields(fields))
                        .MinimumShouldMatch(MinimumShouldMatch.Percentage(100))
                        .DefaultOperator(Operator.And)
                    ) && termType && nestTermMember//termId ||
                    ).From((pageIndex - 1) * pageSize).Size(pageSize);
                    var result = await context.SearchExAsync(searchDescriptor);
                    if (result != null)
                    {
                        data.Total = (int)result.Total;
                        data.Data = result.Data.ToList();
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
            }
            return data;
        }

        public async Task<bool> CheckAccountMember(long officialId, long memberId)
        {
            var data = false;
            try
            {
                var context = GetContext();
                {
                    var searchDescriptor = new SearchDescriptor<AccountSearch>();


                    var termType = new TermQuery()
                    {
                        Field = "type",
                        Value = 2
                    };
                    var termId = new TermQuery()
                    {
                        Field = "id",
                        Value = officialId.ToString()
                    };
                    var termMemberId = new TermQuery()
                    {
                        Field = "accountMember.memberId",
                        Value = memberId.ToString()
                    };
                    var nestTermMember = new NestedQuery()
                    {
                        Path = "accountMember",
                        Query = termMemberId
                    };
                    searchDescriptor.Query(q => termType && termId && nestTermMember
                       ).From(0).Size(1);

                    var result = await context.SearchExAsync(searchDescriptor);
                    if (result != null && result.Total > 0)
                    {
                        data = true;
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
            }
            return data;
        }

        #endregion
    }
}
