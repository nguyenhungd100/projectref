﻿using ChannelVN.IMS2.Core.Entities;
using ChannelVN.IMS2.Core.Entities.Distribution;
using ChannelVN.IMS2.Core.Entities.Newsletters;
using ChannelVN.IMS2.Core.Entities.Security;
using ChannelVN.IMS2.Foundation.Common;
using ChannelVN.IMS2.Foundation.Common.Configuration;
using ChannelVN.IMS2.Foundation.Logging;
using Nest;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ChannelVN.IMS2.Core.Models.SearchStore.Articles
{
    public class ArticleSearchStore : CmsMainSearchStore
    {
        public async Task<bool> AddAsync(ArticleSearch data,bool isQueue = false)
        {
            var returnValue = false;
            try
            {
                var context = GetContext();
                {
                    returnValue = context.CreateIndexSettings<ArticleSearch>("title", "author");
                    returnValue = context.CreateIndexSettings<NewsAllSearch>("keyword");
                    returnValue = context.CreateIndexSettings<NewsInTag>("tagId");
                    returnValue = await context.CreateAsync(data);
                    await context.CreateAsync(new NewsAllSearch()
                    {
                        Id = data.Id,
                        Status = data.Status,
                        Type = data.Type,
                        Keyword = data.Title,
                        CardType = data.CardType,
                        CreatedBy = data.CreatedBy,
                        CreatedDate = data.CreatedDate,
                        DistributorId = data.DistributorId,
                        Avatar = data.Avatar1,
                        NewsInAccount = data.NewsInAccount
                    });
                    if (!string.IsNullOrEmpty(data.Tags))
                    {
                        var index = 1;
                        var listNewsInTag = new List<NewsInTag>();
                        foreach (var tagId in data.Tags.Split(","))
                        {
                            if (long.TryParse(tagId, out long TagId))
                            {
                                listNewsInTag.Add(new NewsInTag()
                                {
                                    NewsId = data.Id,
                                    TagId = TagId,
                                    Priority = index,
                                    TaggedDate = DateTime.Now
                                });
                                index++;
                            }
                        }
                        if (listNewsInTag.Count > 0) await context.CreateManyAsync(listNewsInTag);
                    }
                }
            }
            catch (Exception ex)
            {
                if(!isQueue) Logger.Error(ex, ex.Message);
            }
            return returnValue;
        }

        public async Task<bool> UpdateAsync(ArticleSearch data, bool isQueue = false)
        {
            var returnValue = false;
            try
            {
                var context = GetContext();
                {
                    returnValue = await context.UpdateAsync<ArticleSearch>(data.Id, data);
                    await context.UpdateAsync<NewsAllSearch>(data.Id, new NewsAllSearch()
                    {
                        Id = data.Id,
                        Status = data.Status,
                        Type = data.Type,
                        Keyword = data.Title,
                        CardType = data.CardType,
                        CreatedBy = data.CreatedBy,
                        CreatedDate = data.CreatedDate,
                        DistributorId = data.DistributorId,
                        Avatar = data.Avatar1,
                        NewsInAccount = data.NewsInAccount,
                        NewsDistribution = data.NewsDistribution
                    });
                    //xóa news in tag
                    try
                    {
                        await context.DeleteByQueryAsync<NewsInTag>(q => q.Index(new[] { context.NameOf("NewsInTag") }).Query(rq => rq.Term(t => t.NewsId, data.Id)));
                    }
                    catch (Exception)
                    {
                        //Logger.Error(ex, ex.Message);
                    }
                    //insert new in tag
                    if (!string.IsNullOrEmpty(data.Tags))
                    {
                        var index = 1;
                        var listNewsInTag = new List<NewsInTag>();
                        foreach (var tagId in data.Tags.Split(","))
                        {
                            if (long.TryParse(tagId, out long TagId))
                            {
                                listNewsInTag.Add(new NewsInTag()
                                {
                                    NewsId = data.Id,
                                    TagId = TagId,
                                    Priority = index,
                                    TaggedDate = DateTime.Now
                                });
                                index++;
                            }
                        }
                        if (listNewsInTag.Count > 0)
                        {
                            await context.CreateManyAsync(listNewsInTag);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                if(!isQueue) Logger.Error(ex, ex.Message);
            }
            return returnValue;
        }
        public async Task<bool> UpdateStatusAsync(ArticleSearch data, bool isQueue = false)
        {
            var returnValue = false;
            try
            {
                var context = GetContext();
                {
                    if (data != null)
                    {
                        returnValue = await context.UpdateAsync<ArticleSearch>(data.Id, data);
                        await context.UpdateAsync<NewsAllSearch>(data.Id, new NewsAllSearch()
                        {
                            Id = data.Id,
                            Status = data.Status,
                            Type = data.Type,
                            Keyword = data.Title,
                            CardType = data.CardType,
                            CreatedBy = data.CreatedBy,
                            CreatedDate = data.CreatedDate,
                            DistributorId = data.DistributorId,
                            Avatar = data.Avatar1,
                            NewsInAccount = data.NewsInAccount,
                            NewsDistribution = data.NewsDistribution
                        });
                    }
                }
            }
            catch (Exception ex)
            {
                if(!isQueue) Logger.Error(ex, ex.Message);
            }
            return returnValue;
        }
        public async Task<ArticleSearch> GetByIdAsync(long id)
        {
            var returnValue = default(ArticleSearch);
            try
            {
                var context = GetContext();
                {
                    returnValue = await context.GetAsync<ArticleSearch>(id);

                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
            }
            return returnValue;
        }
        public async Task<ItemStreamDistribution> GetItemStreamByIdAsync(long id)
        {
            var returnValue = default(ItemStreamDistribution);
            try
            {
                var context = GetContext();
                {
                    returnValue = await context.GetAsync<ItemStreamDistribution>(id);
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
            }
            return returnValue;
        }
        public async Task<PagingDataResult<ArticleSearch>> ListMyNewsAsync(long accountId, int pageIndex, int pageSize)
        {
            var data = new PagingDataResult<ArticleSearch>();
            try
            {
                var context = GetContext();
                {
                    var searchDescriptor = new SearchDescriptor<ArticleSearch>();

                    var termAccountId = new TermQuery
                    {
                        Field = "newsInAccount.accountId",
                        Value = accountId.ToString()
                    };

                    var nestNewsInAccount = new NestedQuery
                    {
                        Path = "newsInAccount",
                        Query = termAccountId
                    };

                    searchDescriptor.Query(q => nestNewsInAccount
                    ).From((pageIndex - 1) * pageSize).Size(pageSize);

                    var result = await context.SearchExAsync(searchDescriptor);

                    if (result != null)
                    {
                        data.Total = (int)result.Total;
                        data.Data = result.Data.ToList();
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
            }
            return data;
        }
        public async Task<PagingDataResult<ArticleSearch>> ListNewsPublishAsync(long accountId, int pageIndex, int pageSize)
        {
            var data = new PagingDataResult<ArticleSearch>();
            try
            {
                var context = GetContext();
                {
                    var searchDescriptor = new SearchDescriptor<ArticleSearch>();
                    searchDescriptor.Sort(s => s.Descending(p => p.CreatedDate));

                    var termAccountId = new TermQuery
                    {
                        Field = "newsInAccount.accountId",
                        Value = accountId.ToString()
                    };

                    var nestNewsInAccount = new NestedQuery
                    {
                        Path = "newsInAccount",
                        Query = termAccountId
                    };

                    var termPublishStatus = new TermQuery
                    {
                        Field = "newsDistribution.publishStatus",
                        Value = (int)NewsDistributionPublishStatus.Accept
                    };

                    var newsNewsDistribution = new NestedQuery
                    {
                        Path = "newsDistribution",
                        Query = termPublishStatus //&& termDistributionId &&
                    };

                    searchDescriptor.Query(q => nestNewsInAccount && newsNewsDistribution
                    ).From((pageIndex - 1) * pageSize).Size(pageSize);

                    var result = await context.SearchExAsync(searchDescriptor);

                    if (result != null)
                    {
                        data.Total = (int)result.Total;
                        data.Data = result.Data.ToList();
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
            }
            return data;
        }
        public async Task<PagingDataResult<ArticleSearch>> ListNewsByPublishStatus(int publishStatus, long accountId, int pageIndex, int pageSize)
        {
            var data = new PagingDataResult<ArticleSearch>();
            try
            {
                var context = GetContext();
                {
                    var searchDescriptor = new SearchDescriptor<ArticleSearch>();
                    searchDescriptor.Sort(s => s.Descending(p => p.CreatedDate));

                    var termAccountId = new TermQuery
                    {
                        Field = "newsInAccount.accountId",
                        Value = accountId.ToString()
                    };

                    var nestNewsInAccount = new NestedQuery
                    {
                        Path = "newsInAccount",
                        Query = termAccountId
                    };


                    var termPublishStatus = new TermQuery
                    {
                        Field = "newsDistribution.publishStatus",
                        Value = publishStatus
                    };

                    var newsNewsDistribution = new NestedQuery
                    {
                        Path = "newsDistribution",
                        Query = termPublishStatus
                    };

                    searchDescriptor.Query(q => nestNewsInAccount && newsNewsDistribution
                    ).From((pageIndex - 1) * pageSize).Size(pageSize);

                    var result = await context.SearchExAsync(searchDescriptor);

                    if (result != null)
                    {
                        data.Total = (int)result.Total;
                        data.Data = result.Data.ToList();
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
            }
            return data;
        }
        public async Task<PagingDataResult<ArticleSearch>> ListNewsByAuthorAsync(long accountId, long distributionId, int pageIndex, int pageSize)
        {
            var data = new PagingDataResult<ArticleSearch>();
            try
            {
                var context = GetContext();
                {
                    var searchDescriptor = new SearchDescriptor<ArticleSearch>();

                    var termDistributorId = new TermQuery();
                    if (distributionId > 0)
                    {
                        termDistributorId.Field = "distributorId";
                        termDistributorId.Value = distributionId;
                    }
                    //chua check distributionId
                    ///////////////////////////////////
                    searchDescriptor.Sort(s => s.Descending(p => p.CreatedDate));
                    var termAccountId = new TermQuery
                    {
                        Field = "newsInAccount.accountId",
                        Value = accountId.ToString()
                    };

                    var termType = new TermQuery
                    {
                        Field = "type",
                        Value = 1

                    };
                    var nestNewsInAccount = new NestedQuery
                    {
                        Path = "newsInAccount",
                        Query = termAccountId
                    };

                    var termStatus = new TermQuery
                    {
                        Field = "status",
                        Value = (int)NewsStatus.Published
                    };

                    searchDescriptor.Query(q => nestNewsInAccount && termStatus && termDistributorId
                    ).From((pageIndex - 1) * pageSize).Size(pageSize);

                    var result = await context.SearchExAsync(searchDescriptor);

                    if (result != null)
                    {
                        data.Total = (int)result.Total;
                        data.Data = result.Data.ToList();
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
            }
            return data;
        }
        public async Task<PagingDataResult<ArticleSearch>> ListNewsByDistributionAsync(long distributionId, int publishStatus, long accountId, DateTime? fromDate, DateTime? toDate, int pageIndex, int pageSize)
        {
            var data = new PagingDataResult<ArticleSearch>();
            try
            {
                var context = GetContext();
                {
                    var searchDescriptor = new SearchDescriptor<ArticleSearch>();

                    var termDistributionId = new TermQuery
                    {
                        Field = "newsDistribution.distributorId",
                        Value = distributionId.ToString()
                    };

                    var termPublishStatus = new TermQuery
                    {
                        Field = "newsDistribution.publishStatus",
                        Value = publishStatus.ToString()
                    };

                    var termType = new TermQuery
                    {
                        Field = "type",
                        Value = 1
                    };

                    string fieldDate = "newsDistribution.sharedDate";
                    var range = new DateRangeQuery
                    {
                        Field = fieldDate,
                        //TimeZone = "+07:00",
                        Format = AppSettings.Current.ChannelConfiguration.DateFormat
                    };
                    if (fromDate != null && fromDate != DateTime.MinValue && toDate != null && toDate != DateTime.MinValue)
                    {
                        range.GreaterThanOrEqualTo = fromDate.Value.ToString(AppSettings.Current.ChannelConfiguration.DateFormat);
                        range.LessThanOrEqualTo = toDate.Value.ToString(AppSettings.Current.ChannelConfiguration.DateFormat);
                    }
                    else
                    {
                        if (fromDate != null && fromDate != DateTime.MinValue)
                        {
                            range.GreaterThanOrEqualTo = fromDate.Value.ToString(AppSettings.Current.ChannelConfiguration.DateFormat);
                        }
                        else if (toDate != null && toDate != DateTime.MinValue)
                        {
                            range.LessThanOrEqualTo = toDate.Value.ToString(AppSettings.Current.ChannelConfiguration.DateFormat);
                        }
                    }

                    var nestNewsDistribution = new NestedQuery
                    {
                        Path = "newsDistribution",
                        Query = termDistributionId & termPublishStatus && range
                    };

                    var nestNewsInAccount = new NestedQuery();
                    if (accountId > 0)
                    {
                        var termAccountId = new TermQuery
                        {
                            Field = "newsInAccount.accountId",
                            Value = accountId.ToString()
                        };
                        nestNewsInAccount.Path = "newsInAccount";
                        nestNewsInAccount.Query = termAccountId;
                    }

                    searchDescriptor.Query(q => nestNewsDistribution && nestNewsInAccount && termType
                    ).From((pageIndex - 1) * pageSize).Size(pageSize);

                    var result = await context.SearchExAsync(searchDescriptor);

                    if (result != null)
                    {
                        data.Total = (int)result.Total;
                        data.Data = result.Data.ToList();
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
            }
            return data;
        }

        public async Task<PagingDataResult<ItemStreamDistribution>> ListItemStreamByDistributionAsync(GetNewsByDistribution getNewsByDistributionEntity)
        {
            var data = new PagingDataResult<ItemStreamDistribution>();
            try
            {
                var context = GetContext();
                {
                    var searchDescriptor = new SearchDescriptor<ItemStreamDistribution>();
                    searchDescriptor.Sort(s => s.Descending(p => p.CreatedDate));
                    var termPublishStatus = new TermQuery
                    {
                        Field = "publishStatus",
                        Value = ((int)getNewsByDistributionEntity.PublishStatus).ToString()
                    };

                    var termDistributionId = new TermQuery
                    {
                        Field = "distributorId",
                        Value = getNewsByDistributionEntity.DistributionId.ToString()
                    };

                    string fieldDate = "createdDate";
                    var range = new DateRangeQuery
                    {
                        Field = fieldDate,
                        //TimeZone = "+07:00",
                        Format = AppSettings.Current.ChannelConfiguration.DateFormat
                    };
                    if (getNewsByDistributionEntity.FromDate != null && getNewsByDistributionEntity.FromDate != DateTime.MinValue
                        && getNewsByDistributionEntity.ToDate != null && getNewsByDistributionEntity.ToDate != DateTime.MinValue)
                    {
                        range.GreaterThanOrEqualTo = getNewsByDistributionEntity.FromDate.Value.ToString(AppSettings.Current.ChannelConfiguration.DateFormat);
                        range.LessThanOrEqualTo = getNewsByDistributionEntity.ToDate.Value.ToString(AppSettings.Current.ChannelConfiguration.DateFormat);
                    }
                    else
                    {
                        if (getNewsByDistributionEntity.FromDate != null && getNewsByDistributionEntity.FromDate != DateTime.MinValue)
                        {
                            range.GreaterThanOrEqualTo = getNewsByDistributionEntity.FromDate.Value.ToString(AppSettings.Current.ChannelConfiguration.DateFormat);
                        }
                        else if (getNewsByDistributionEntity.ToDate != null && getNewsByDistributionEntity.ToDate != DateTime.MinValue)
                        {
                            range.LessThanOrEqualTo = getNewsByDistributionEntity.ToDate.Value.ToString(AppSettings.Current.ChannelConfiguration.DateFormat);
                        }
                    }
                    var pageIndex = getNewsByDistributionEntity.PageIndex < 1 ? 1 : getNewsByDistributionEntity.PageIndex ?? 1;
                    var pageSize = getNewsByDistributionEntity.PageSize < 1 ? 10 : getNewsByDistributionEntity.PageSize ?? 10;


                    searchDescriptor.Query(q => termPublishStatus && range && termDistributionId
                    ).From((pageIndex - 1) * pageSize).Size(pageSize);

                    var result = await context.SearchExAsync(searchDescriptor);

                    if (result != null)
                    {
                        data.Total = (int)result.Total;
                        data.Data = result.Data.ToList();
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
            }
            return data;
        }
        public async Task<long> GetCountItemStreamByDistributionAsync(long distributionId, int? publishStatus)
        {
            try
            {
                var context = GetContext();
                {
                    var termPublishStatus = new TermQuery
                    {
                        Field = "publishStatus",
                        Value = publishStatus.ToString()
                    };
                    var termDistributionId = new TermQuery
                    {
                        Field = "distributorId",
                        Value = distributionId.ToString()
                    };


                    var result = await context.CountAsync<ItemStreamDistribution>(c => c
                        // ReSharper disable once PossiblyMistakenUseOfParamsMethod
                        .Index(context.NameOf(typeof(ItemStreamDistribution).Name))
                         .Type(typeof(ItemStreamDistribution).Name.ToLower())
                         .Query(q => termPublishStatus && termDistributionId)
                    );
                    if (result != null)
                    {
                        return result.Count;
                    }
                    else
                    {
                        return 0;
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
                return 0;
            }
        }
        public async Task<PagingDataResult<ArticleSearch>> ListNewsByStatusAsync(SearchNews searchNews, long userId)
        {
            var data = new PagingDataResult<ArticleSearch>();
            try
            {
                var context = GetContext();
                {
                    var searchDescriptor = new SearchDescriptor<ArticleSearch>();

                    switch (searchNews.OrderBy)
                    {
                        case SearchNewsOrderBy.DescDistributionDate:
                            searchDescriptor.Sort(s => s.Descending(p => p.DistributionDate));
                            break;
                        case SearchNewsOrderBy.AscDistributionDate:
                            searchDescriptor.Sort(s => s.Ascending(p => p.DistributionDate));
                            break;
                        case SearchNewsOrderBy.DescCreatedDate:
                            searchDescriptor.Sort(s => s.Descending(p => p.CreatedDate));
                            break;
                        case SearchNewsOrderBy.AscCreatedDate:
                            searchDescriptor.Sort(s => s.Ascending(p => p.CreatedDate));
                            break;
                        case SearchNewsOrderBy.DescModifiedDate:
                            searchDescriptor.Sort(s => s.Descending(p => p.ModifiedDate));
                            break;
                        case SearchNewsOrderBy.AscModifiedDate:
                            searchDescriptor.Sort(s => s.Ascending(p => p.ModifiedDate));
                            break;
                        default:
                            searchDescriptor.Sort(s => s.Descending(p => p.DistributionDate));
                            break;
                    }
                    
                    var listStatus = new List<string>();
                    if (!string.IsNullOrEmpty(searchNews.Status))
                    {
                        var arrStatus = searchNews.Status.Split(",");
                        for (var i = 0; i < arrStatus.Length; i++)
                        {
                            if (!string.IsNullOrEmpty(arrStatus[i]) && Enum.IsDefined(typeof(NewsStatus), arrStatus[i]?.Trim()))
                            {
                                listStatus.Add(((int)Enum.Parse(typeof(NewsStatus), arrStatus[i].Trim())).ToString());
                            }
                        }
                    }
                    var termsStatus = new TermsQuery()
                    {
                        Field = "status",
                        Terms = listStatus.ToArray()
                    };

                    string fieldDate = "createdDate";

                    switch (searchNews.SearchDateBy)
                    {
                        case 1:
                            fieldDate = "createdDate";
                            break;
                        case 2:
                            fieldDate = "distributionDate";
                            break;
                        case 3:
                            fieldDate = "modifiedDate";
                            break;
                    }

                    var range = new DateRangeQuery()
                    {
                        Field = fieldDate, //"newsInAccount.publishedDate",
                        //TimeZone = "+07:00",
                        Format = AppSettings.Current.ChannelConfiguration.DateFormat
                    };
                    if (searchNews.FromDate != null && searchNews.FromDate != DateTime.MinValue && searchNews.ToDate != null && searchNews.ToDate != DateTime.MinValue)
                    {
                        range.GreaterThanOrEqualTo = searchNews.FromDate.Value.ToString(AppSettings.Current.ChannelConfiguration.DateFormat);
                        range.LessThanOrEqualTo = searchNews.ToDate.Value.ToString(AppSettings.Current.ChannelConfiguration.DateFormat);
                    }
                    else
                    {
                        if (searchNews.FromDate != null && searchNews.FromDate != DateTime.MinValue)
                        {
                            range.GreaterThanOrEqualTo = searchNews.FromDate.Value.ToString(AppSettings.Current.ChannelConfiguration.DateFormat);
                        }
                        else if (searchNews.ToDate != null && searchNews.ToDate != DateTime.MinValue)
                        {
                            range.LessThanOrEqualTo = searchNews.ToDate.Value.ToString(AppSettings.Current.ChannelConfiguration.DateFormat);
                        }
                    }

                    var nestNewsInAccount = new NestedQuery();
                    if (userId > 0)
                    {
                        var termAccountId = new TermQuery
                        {
                            Field = "newsInAccount.accountId",
                            Value = userId.ToString()
                        };
                        nestNewsInAccount.Path = "newsInAccount";
                        nestNewsInAccount.Query = termAccountId;
                    }

                    searchDescriptor.Query(q => termsStatus && nestNewsInAccount && range
                    ).From((searchNews.PageIndex - 1) * searchNews.PageSize).Size(searchNews.PageSize);

                    var result = await context.SearchExAsync(searchDescriptor);

                    if (result != null)
                    {
                        data.Total = (int)result.Total;
                        data.Data = result.Data.ToList();
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
            }
            return data;
        }


        public async Task<PagingDataResult<ArticleSearch>> ListNewsByItemStreamAsync(ItemStreamDistribution itemStream)
        {
            var data = new PagingDataResult<ArticleSearch>();
            try
            {
                var context = GetContext();
                {
                    var searchDescriptor = new SearchDescriptor<ArticleSearch>();
                    searchDescriptor.Sort(s => s.Ascending(p => p.CreatedDate));

                    var newsDistribution = new NestedQuery();
                    var termItemStreamId = new TermQuery
                    {
                        Field = "newsDistribution.itemStreamId",
                        Value = itemStream.Id.ToString()
                    };
                    newsDistribution.Path = "newsDistribution";
                    newsDistribution.Query = termItemStreamId;

                    searchDescriptor.Query(q => newsDistribution
                    );

                    var result = await context.SearchExAsync(searchDescriptor);

                    if (result != null)
                    {
                        data.Total = (int)result.Total;
                        data.Data = result.Data.ToList();
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
            }
            return data;
        }
        public async Task<int> GetPublishCount(long authorId, int status, long? distributorId)
        {
            int publishCount = 0;
            try
            {
                var context = GetContext();
                {
                    var termAccountId = new TermQuery
                    {
                        Field = "newsInAccount.accountId",
                        Value = authorId.ToString()
                    };

                    var termStatus = new TermQuery
                    {
                        Field = "status",
                        Value = status
                    };

                    var termType = new TermQuery
                    {
                        Field = "type",
                        Value = 1
                    };
                    var termDistributorId = new TermQuery();
                    if (distributorId != null && distributorId > 0)
                    {
                        termDistributorId.Field = "distributorId";
                        termDistributorId.Value = termDistributorId;
                    }

                    var nestNewsInAccount = new NestedQuery
                    {
                        Path = "newsInAccount",
                        Query = termAccountId
                    };
                    if (status == -1)
                    {
                        var result = await context.CountAsync<ArticleSearch>(c => c
                            .Index("vccorp_news")
                            .Type("news")
                            .Query(q => nestNewsInAccount && termDistributorId)
                        );
                        publishCount = (int)result.Count;
                    }
                    else
                    {
                        var result = await context.CountAsync<ArticleSearch>(c => c
                            .Index("vccorp_news")
                            .Type("news")
                            .Query(q => nestNewsInAccount && termStatus && termType && termDistributorId)
                        );
                        publishCount = (int)result.Count;
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
            }
            return publishCount;
        }

        public async Task<PagingDataResult<string>> SearchItemStreamByIdAsync(long distributionId, AccountSearch account, DateTime? fromDate, DateTime? toDate)
        {
            var data = new PagingDataResult<string>();
            try
            {
                var context = GetContext();
                {

                    var searchDescriptor = new SearchDescriptor<ItemStreamDistribution>();
                    var termDistributionId = new TermQuery()
                    {
                        Field = "newsDistribution.distributionId",
                        Value = distributionId
                    };

                    string fieldDate = "newsDistribution.sharedDate";

                    var range = new DateRangeQuery
                    {
                        Field = fieldDate,
                        //TimeZone = "+07:00",
                        Format = AppSettings.Current.ChannelConfiguration.DateFormat
                    };
                    if (fromDate != null && fromDate != DateTime.MinValue && toDate != null && toDate != DateTime.MinValue)
                    {

                        range.GreaterThanOrEqualTo = fromDate.Value.ToString(AppSettings.Current.ChannelConfiguration.DateFormat);
                        range.LessThanOrEqualTo = toDate.Value.ToString(AppSettings.Current.ChannelConfiguration.DateFormat);
                    }
                    else
                    {
                        if (fromDate != null && fromDate != DateTime.MinValue)
                        {
                            range.GreaterThanOrEqualTo = fromDate.Value.ToString(AppSettings.Current.ChannelConfiguration.DateFormat);
                        }
                        else if (toDate != null && toDate != DateTime.MinValue)
                        {
                            range.LessThanOrEqualTo = toDate.Value.ToString(AppSettings.Current.ChannelConfiguration.DateFormat);
                        }
                    }

                    var nestDistributionId = new NestedQuery
                    {
                        Path = "newsDistribution",
                        Query = range && termDistributionId
                    };

                    if (account != null)
                    {

                    }

                    searchDescriptor.Query(q => nestDistributionId
                    ).Size(1000000);

                    var result = await context.SearchExAsync(searchDescriptor);

                    if (result != null)
                    {
                        data.Total = (int)result.Total;
                        data.Data = data.Data.Distinct().ToList();
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
            }
            return data;
        }

        public async Task<PagingDataResult<ArticleSearch>> SearchAsync(SearchNews search, long accountId, bool isGetDistribution=false)
        {
            var data = new PagingDataResult<ArticleSearch>();
            try
            {
                var context = GetContext();
                {
                    var isOfficialAccount = false;
                    //check account in official
                    if (search.OfficerId > 0&& search.OfficerId!=accountId)
                    {
                        var searchAccount = new SearchDescriptor<AccountSearch>();
                        var termTypeAcc = new TermQuery()
                        {
                            Field = "type",
                            Value = 2
                        };
                        var termId = new TermQuery()
                        {
                            Field = "id",
                            Value = search.OfficerId
                        };
                        var termMemberId = new TermQuery()
                        {
                            Field = "accountMember.memberId",
                            Value = accountId
                        };
                        var nestTermMember = new NestedQuery()
                        {
                            Path = "accountMember",
                            Query = termMemberId
                        };
                        searchAccount.Query(q => termTypeAcc && termId && nestTermMember
                           ).From(0).Size(1);

                        var resultCheck = await context.SearchExAsync(searchAccount);
                        if (resultCheck != null && resultCheck.Total > 0)
                        {
                            isOfficialAccount = true;
                        }
                        if (isOfficialAccount == false) return data;
                    }
                    //end check account
                    var searchDescriptor = new SearchDescriptor<ArticleSearch>();
                    if (null != search.OrderBy)
                    {
                        switch (search.OrderBy)
                        {
                            case SearchNewsOrderBy.DescDistributionDate:
                                searchDescriptor.Sort(s => s.Descending(p => p.DistributionDate));
                                break;
                            case SearchNewsOrderBy.AscDistributionDate:
                                searchDescriptor.Sort(s => s.Ascending(p => p.DistributionDate));
                                break;
                            case SearchNewsOrderBy.DescCreatedDate:
                                searchDescriptor.Sort(s => s.Descending(p => p.CreatedDate));
                                break;
                            case SearchNewsOrderBy.AscCreatedDate:
                                searchDescriptor.Sort(s => s.Ascending(p => p.CreatedDate));
                                break;
                            case SearchNewsOrderBy.DescModifiedDate:
                                searchDescriptor.Sort(s => s.Descending(p => p.ModifiedDate));
                                break;
                            case SearchNewsOrderBy.AscModifiedDate:
                                searchDescriptor.Sort(s => s.Ascending(p => p.ModifiedDate));
                                break;
                            default:
                                searchDescriptor.Sort(s => s.Descending(p => p.CreatedDate));
                                break;
                        }
                    }
                    var termCategoryId = new TermQuery();
                    if (search.CategoryId > 0)
                    {
                        termCategoryId.Field = "categoryId";
                        termCategoryId.Value = search.CategoryId;
                    }

                    var nestTerm = new NestedQuery();
                    var termDistributorId = new TermQuery();
                    if (search.DistributionId > 0)
                    {
                        termDistributorId.Field = "newsDistribution.distributorId";
                        termDistributorId.Value = search.DistributionId;
                        nestTerm.Path = "newsDistribution";
                        nestTerm.Query = termDistributorId;
                    }
                    else
                    {
                        if (isGetDistribution == true)
                        {
                            var exist = new ExistsQuery()
                            {
                                Field = "newsDistribution.distributorId"
                            };
                            nestTerm.Path = "newsDistribution";
                            nestTerm.Query = exist;
                        }
                    }

                    var listStatus = new List<string>();
                    if (!string.IsNullOrEmpty(search.Status))
                    {
                        var arrStatus = search.Status.Split(",");
                        for (var i = 0; i < arrStatus.Length; i++)
                        {
                            if (int.TryParse(arrStatus[i], out int st))
                            {
                                if (Enum.IsDefined(typeof(NewsStatus), st))
                                {
                                    listStatus.Add(st.ToString());
                                }
                            }
                        }
                    }
                    var termsStatus = new TermsQuery()
                    {
                        Field = "status",
                        Terms = listStatus.ToArray()
                    };
                    
                    var termCreatedBy = new TermQuery();
                    if(search.OfficerId!=accountId)
                    {
                        termCreatedBy.Field = "createdBy";
                        termCreatedBy.Value = accountId;
                    };
                    var termNewsInAccount = new TermQuery();
                    if (isOfficialAccount)
                    {
                        termNewsInAccount.Field = "newsInAccount.accountId";
                        termNewsInAccount.Value = search.OfficerId;
                    }
                    else
                    {
                        termNewsInAccount.Field = "newsInAccount.accountId";
                        termNewsInAccount.Value = accountId;
                    }
                    var nestTermNewsInAccount = new NestedQuery()
                    {
                        Path = "newsInAccount",
                        Query = termNewsInAccount
                    };

                    var termPublishMode = new TermQuery();
                    if (search.PublishMode > 0)
                    {
                        termPublishMode.Field = "publishMode";
                        termPublishMode.Value = search.PublishMode;
                    }
                    string fieldDate = "createdDate";
                    switch (search.SearchDateBy)
                    {
                        case 1:
                            fieldDate = "createdDate";
                            break;
                        case 2:
                            fieldDate = "distributionDate";
                            break;
                        case 3:
                            fieldDate = "modifiedDate";
                            break;
                    }

                    var range = new DateRangeQuery
                    {
                        Field = fieldDate,
                        //TimeZone = "+07:00",
                        Format = AppSettings.Current.ChannelConfiguration.DateFormat
                    };
                    if (search.FromDate != null && search.FromDate != DateTime.MinValue && search.ToDate != null && search.ToDate != DateTime.MinValue)
                    {

                        range.GreaterThanOrEqualTo = search.FromDate.Value.ToString(AppSettings.Current.ChannelConfiguration.DateFormat);
                        range.LessThanOrEqualTo = search.ToDate.Value.ToString(AppSettings.Current.ChannelConfiguration.DateFormat);
                    }
                    else
                    {
                        if (search.FromDate != null && search.FromDate != DateTime.MinValue)
                        {
                            range.GreaterThanOrEqualTo = search.FromDate.Value.ToString(AppSettings.Current.ChannelConfiguration.DateFormat);
                        }
                        else if (search.ToDate != null && search.ToDate != DateTime.MinValue)
                        {
                            range.LessThanOrEqualTo = search.ToDate.Value.ToString(AppSettings.Current.ChannelConfiguration.DateFormat);
                        }
                    }

                    var keyword = EscapeQueryBase.EscapeSearchQuery(search.Keyword);
                    searchDescriptor.Query(q => q
                    .QueryString(mp => mp
                        .Query(keyword)
                        .Fields(f => f.Fields(new string[] { "title", "title.folded" }))
                        .MinimumShouldMatch(MinimumShouldMatch.Percentage(100))
                        .DefaultOperator(Operator.And)
                    )
                    && q.QueryString(mp => mp
                        .Query(search.Author)
                        .Fields(f => f.Fields(new string[] { "author", "author.folded" }))
                        .MinimumShouldMatch(MinimumShouldMatch.Percentage(100))
                        .DefaultOperator(Operator.And)
                    ) && termCategoryId && termPublishMode && termsStatus && q.DateRange(c => range) && nestTermNewsInAccount  && nestTerm && termCreatedBy
                    ).From((search.PageIndex - 1) * search.PageSize).Size(search.PageSize);

                    var result = await context.SearchExAsync(searchDescriptor);

                    if (result != null)
                    {
                        data.Total = (int)result.Total;
                        data.Data = result.Data.ToList();
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
            }
            return data;
        }

        
        public async Task<bool> AddItemStreamAsync(ItemStreamDistribution itemStreamDistribution)
        {
            var returnValue = false;
            try
            {
                var context = GetContext();
                {
                    returnValue = await context.CreateAsync(itemStreamDistribution);
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
            }
            return returnValue;
        }

        public async Task<int> CountNewsByUserIdAsync(long userId, DateTime dateFrom, DateTime dateTo)
        {
            var count = 0;
            try
            {
                var context = GetContext();
                {
                    var searchDescriptor = new SearchDescriptor<ArticleSearch>();

                    var termAccountId = new TermQuery
                    {
                        Field = "newsInAccount.accountId",
                        Value = userId.ToString()
                    };

                    string fieldDate = "distributionDate";
                    var range = new DateRangeQuery
                    {
                        Field = fieldDate,
                        // TimeZone = "+07:00",
                        Format = AppSettings.Current.ChannelConfiguration.DateFormat
                    };
                    range.GreaterThanOrEqualTo = dateFrom.ToString(AppSettings.Current.ChannelConfiguration.DateFormat);
                    range.LessThanOrEqualTo = dateTo.ToString(AppSettings.Current.ChannelConfiguration.DateFormat);

                    searchDescriptor.Query(q => termAccountId && range);

                    var result = await context.SearchExAsync(searchDescriptor);

                    if (result != null)
                    {
                        count = (int)result.Total;
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
            }
            return count;
        }

        public async Task<bool> PublishAsync(ArticleSearch obj)
        {
            var returnValue = false;
            try
            {
                var context = GetContext();
                {
                    if (obj != null)
                    {
                        returnValue = await context.UpdateAsync<ArticleSearch>(obj.Id, obj);
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
            }
            return returnValue;
        }

        public async Task<bool> AcceptAsync(ItemStreamDistribution data, bool isQueue= false)//, List<ArticleSearch> listNews
        {
            bool returnValue;
            try
            {
                var context = GetContext();
                {
                    returnValue = await context.UpdateAsync<ItemStreamDistribution>(data.Id, data);
                }
            }
            catch (Exception ex)
            {
                if(!isQueue) Logger.Error(ex, ex.Message);
                returnValue = false;
            }
            return returnValue;
        }

        public async Task<PagingDataResult<ArticleSearch>> SearchAllAsync()
        {
            var data = new PagingDataResult<ArticleSearch>();
            try
            {
                var context = GetContext();
                {

                    var searchDescriptor = new SearchDescriptor<ArticleSearch>();

                    searchDescriptor.From(0).Size(1000000);

                    var result = await context.SearchExAsync(searchDescriptor);

                    if (result != null)
                    {
                        data.Total = (int)result.Total;
                        data.Data = result.Data.ToList();
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
            }
            return data;
        }

        public async Task<bool> RejectAsync(ItemStreamDistribution data, bool isQueue = false)//, List<ArticleSearch> listNews
        {
            var returnValue = false;
            try
            {
                var context = GetContext();
                {
                    returnValue = await context.UpdateAsync<ItemStreamDistribution>(data.Id, data);
                }
            }
            catch (Exception ex)
            {
                if(!isQueue) Logger.Error(ex, ex.Message);
            }
            return returnValue;
        }

        public async Task<bool> InitAllNewsAsync(List<ArticleSearch> listNews)
        {
            var returnValue = false;
            try
            {
                var context = GetContext();
                {
                    returnValue = context.CreateIndexSettings<ArticleSearch>("title", "author");
                    returnValue = await context.CreateManyAsync(listNews);
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
            }
            return returnValue;
        }




        public async Task<bool> ShareNewsAsync(AccountSearch account, ItemStreamDistribution itemStream, List<ArticleSearch> listNews)
        {
            var returnValue = false;
            try
            {
                var context = GetContext();
                {

                    //returnValue = context.CreateIndexSettings<Article>("title", "author");
                    returnValue = await context.CreateManyAsync(listNews);
                    if (returnValue)
                    {
                        //returnValue = context.CreateIndexSettings<ItemStreamDistribution>("title", "author");
                        returnValue = await context.CreateAsync(itemStream);
                        returnValue = await context.UpdateAsync<AccountSearch>(account.Id, account);
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
            }
            return returnValue;
        }

        public async Task<bool> InitAllItemStreamAsync(List<ItemStreamDistribution> listItemStream)
        {
            var returnValue = false;
            try
            {
                var context = GetContext();
                {
                    returnValue = context.CreateIndexSettings<ItemStreamDistribution>("title");
                    returnValue = await context.CreateManyAsync(listItemStream);
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
            }
            return returnValue;
        }
    }
}
