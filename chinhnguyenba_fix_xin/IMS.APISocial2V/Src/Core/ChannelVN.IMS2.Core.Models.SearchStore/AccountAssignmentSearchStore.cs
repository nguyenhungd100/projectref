﻿using ChannelVN.IMS2.Core.Entities;
using ChannelVN.IMS2.Core.Entities.AccountAssignment;
using ChannelVN.IMS2.Foundation.Common;
using ChannelVN.IMS2.Foundation.Common.Configuration;
using ChannelVN.IMS2.Foundation.Logging;
using Nest;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ChannelVN.IMS2.Core.Models.SearchStore
{
    public class AccountAssignmentSearchStore : CmsMainSearchStore
    {
        public async Task<bool> AddAsync(AccountAssignment data, bool isQueue = false)
        {
            var returnValue = false;
            try
            {
                var context = GetContext();
                {
                    returnValue = await context.CreateIndexSettingsAsync<AccountAssignment>("acceptedBy");
                    returnValue = await context.CreateAsync(data);
                }
            }
            catch (Exception ex)
            {
                if (!isQueue) Logger.Error(ex, ex.Message);
            }
            return returnValue;

        }        
        public async Task<bool> UpdateAsync(AccountAssignment data, bool isQueue = false)
        {
            var returnValue = false;
            try
            {
                var context = GetContext();
                {
                    returnValue = await context.UpdateAsync<AccountAssignment>(data.Id, data);
                }
            }
            catch (Exception ex)
            {
                if (!isQueue) Logger.Error(ex, ex.Message);
            }
            return returnValue;
        }
        public async Task<bool> RemoveAsync(AccountAssignment data, bool isQueue = false)
        {
            var returnValue = false;
            try
            {
                var context = GetContext();
                {
                    returnValue = await context.DeleteMappingAsync<AccountAssignment>(data.Id);
                }
            }
            catch (Exception ex)
            {
                if (!isQueue) Logger.Error(ex, ex.Message);
            }
            return returnValue;

        }
        public async Task<AccountAssignment> GetByIdAsync(string id)
        {
            var returnValue = default(AccountAssignment);
            try
            {
                var context = GetContext();
                {
                    returnValue = await context.GetAsync<AccountAssignment>(id);
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
            }
            return returnValue;
        }        
        public async Task<PagingDataResult<AccountAssignment>> SearchAsync(long accountId, long officerId, string assignedBy, int pageIndex, int pageSize)
        {
            var data = new PagingDataResult<AccountAssignment>();
            try
            {
                var context = GetContext();
                {
                    var searchDescriptor = new SearchDescriptor<AccountAssignment>();

                    var termAccountId = new TermQuery();
                    if (accountId > 0)
                    {
                        termAccountId.Field = "accountId";
                        termAccountId.Value = accountId;
                    }

                    var termOfficerId = new TermQuery();
                    if (officerId > 0)
                    {
                        termOfficerId.Field = "officerId";
                        termOfficerId.Value = officerId;
                    }

                    var termAssignedBy = new TermQuery();
                    if (!string.IsNullOrEmpty(assignedBy))
                    {
                        termAssignedBy.Field = "assignedBy";
                        termAssignedBy.Value = assignedBy;
                    }

                    searchDescriptor.Query(q => termOfficerId && termAccountId && termAssignedBy
                    ).From((pageIndex - 1) * pageSize).Size(pageSize);
                    
                    var result = await context.SearchExAsync(searchDescriptor);

                    if (result != null)
                    {
                        data.Total = (int)result.Total;
                        data.Data = result.Data.ToList();
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
            }
            return data;
        }

        public async Task<bool> IsPageExistAsync(long accountId)
        {
            var returnValue = false;
            try
            {
                var context = GetContext();
                {
                    var termAccountId = new TermQuery
                    {
                        Field = "accountId",
                        Value = accountId
                    };

                    var result = await context.CountAsync<AccountAssignment>(c => c
                            .Index(context.NameOf(typeof(AccountAssignment).Name))
                            .Type(typeof(AccountAssignment).Name.ToLower())
                            .Query(q => termAccountId)
                        );
                    returnValue = result?.Count > 0;
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
            }
            return returnValue;
        }

        public async Task<Dictionary<string, int?>> CountNewsOrderAsync(string ids)
        {
            var data = new Dictionary<string, int?>();
            try
            {
                foreach (var id in ids.Split(","))
                {
                    var context = GetContext();
                    {
                        var termAccountId = new TermQuery
                        {
                            Field = "officerId",
                            Value = id
                        };

                        var termStatus = new TermQuery
                        {
                            Field = "status",
                            Value = 1
                        };

                        var result = await context.CountAsync<AccountAssignment>(c => c
                                .Index("vccorp_news")
                                .Type("news")
                                .Query(q => termAccountId && termStatus)
                            );
                        data.Add(id.ToString(), (int)result.Count);
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
            }
            return data;
        }
    }
}
