﻿using ChannelVN.IMS2.Core.Entities;
using ChannelVN.IMS2.Core.Entities.AccountAssignment;
using ChannelVN.IMS2.Core.Entities.NewsOnHome;
using ChannelVN.IMS2.Foundation.Common;
using ChannelVN.IMS2.Foundation.Common.Configuration;
using ChannelVN.IMS2.Foundation.Logging;
using Nest;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ChannelVN.IMS2.Core.Models.SearchStore
{
    public class NewsOnHomeSearchStore : CmsMainSearchStore
    {
        public async Task<bool> AddAsync(NewsOnHome data, bool isQueue = false)
        {
            var returnValue = false;
            try
            {
                var context = GetContext();
                {
                    returnValue = await context.CreateIndexSettingsAsync<NewsOnHome>("acceptedBy");
                    returnValue = await context.CreateAsync(data);
                }
            }
            catch (Exception ex)
            {
                if (!isQueue) Logger.Error(ex, ex.Message);
            }
            return returnValue;

        }        
        public async Task<bool> UpdateAsync(NewsOnHome data, bool isQueue = false)
        {
            var returnValue = false;
            try
            {
                var context = GetContext();
                {
                    returnValue = await context.UpdateAsync<NewsOnHome>(data.NewsId, data);
                }
            }
            catch (Exception ex)
            {
                if (!isQueue) Logger.Error(ex, ex.Message);
            }
            return returnValue;
        }
        public async Task<NewsOnHome> GetByIdAsync(long id)
        {
            var returnValue = default(NewsOnHome);
            try
            {
                var context = GetContext();
                {
                    returnValue = await context.GetAsync<NewsOnHome>(id);
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
            }
            return returnValue;
        }        

        public async Task<PagingDataResult<NewsOnHome>> SearchAsync(string keyword, List<string> officerIds, int? status, byte? officerClass, int pageIndex, int pageSize, OrderBy? orderBy, DateTime fromDate, DateTime toDate)
        {
            var data = new PagingDataResult<NewsOnHome>();
            var listPageId = new List<string>();
            try
            {               
                var context = GetContext();
                {
                    var searchDescriptor = new SearchDescriptor<NewsOnHome>();
                    var fieldDate = "orderedDate";
                    switch (orderBy)
                    {
                        case OrderBy.CreatedDate_ASC:
                            searchDescriptor.Sort(s => s.Ascending(p => p.OrderedDate));
                            break;                        
                        default:
                            searchDescriptor.Sort(s => s.Descending(p => p.OrderedDate));
                            break;
                    }

                    var fields = new[] { "acceptedBy", "acceptedBy.folded"};

                    var termStatus = new TermQuery();
                    if (Enum.IsDefined(typeof(EnumStatusNewsOnHome), status))
                    {
                        termStatus.Field = "status";
                        termStatus.Value = (int)status;
                    }

                    var termOfficerClass = new TermQuery();
                    if (officerClass>0)
                    {
                        termOfficerClass.Field = "officerClass";
                        termOfficerClass.Value = (byte)officerClass;
                    }

                    var termsOfficerId = new TermsQuery();
                    termsOfficerId.Field = "officerId";
                    termsOfficerId.Terms = officerIds;

                    var range = new DateRangeQuery
                    {
                        Field = fieldDate,
                        //TimeZone = "+07:00",
                        Format = AppSettings.Current.ChannelConfiguration.DateFormat
                    };
                    if (fromDate != null && fromDate != DateTime.MinValue && toDate != null && toDate != DateTime.MinValue)
                    {
                        range.GreaterThanOrEqualTo = fromDate.ToString(AppSettings.Current.ChannelConfiguration.DateFormat);
                        range.LessThanOrEqualTo = toDate.ToString(AppSettings.Current.ChannelConfiguration.DateFormat);
                    }
                    else
                    {
                        if (fromDate != null && fromDate != DateTime.MinValue)
                        {
                            range.GreaterThanOrEqualTo = fromDate.ToString(AppSettings.Current.ChannelConfiguration.DateFormat);
                        }
                        else if (toDate != null && toDate != DateTime.MinValue)
                        {
                            range.LessThanOrEqualTo = toDate.ToString(AppSettings.Current.ChannelConfiguration.DateFormat);
                        }
                    }


                    keyword = EscapeQueryBase.EscapeSearchQuery(keyword);

                    searchDescriptor.Query(q => q
                    .QueryString(mp => mp
                        .Query(keyword)
                        .Fields(f => f.Fields(fields))
                        .MinimumShouldMatch(MinimumShouldMatch.Percentage(100))
                        .DefaultOperator(Operator.And)
                    ) && termStatus && termOfficerClass && termsOfficerId && q.DateRange(c => range)
                    ).From((pageIndex - 1) * pageSize).Size(pageSize);
                    
                    var result = await context.SearchExAsync(searchDescriptor);

                    if (result != null)
                    {
                        data.Total = (int)result.Total;
                        data.Data = result.Data.ToList();
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
            }
            return data;
        }
       
        public async Task<Dictionary<string, int?>> GetNewsOnHomeStatusAsync(IEnumerable<long> listId)
        {
            var data = new Dictionary<string, int?>();
            try
            {
                var context = GetContext();
                {
                    var searchDescriptor = new SearchDescriptor<NewsOnHome>();
                    var termsId = new TermsQuery()
                    {
                        Field = "newsId",
                        Terms = listId.Select(p => p.ToString())
                    };
                    searchDescriptor.Query(q => termsId
                    ).From(0).Size(10000);

                    var result = await context.SearchExAsync(searchDescriptor);
                    if (result != null)
                    {
                        foreach (var id in listId)
                        {
                            data.Add(id.ToString(), result.Data?.FirstOrDefault(p => p.NewsId == id)?.Status);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
            }
            return data;
        }

        public async Task<Dictionary<string, Dictionary<int,int>>> CountNewsOrderAsync(string ids)
        {
            var data = new Dictionary<string, Dictionary<int, int>>();
            try
            {
                foreach (var id in ids.Split(","))
                {
                    var count1 = 0;
                    var count2 = 0;
                    var count3 = 0;
                    var context = GetContext();
                    {
                        var termAccountId = new TermQuery
                        {
                            Field = "officerId",
                            Value = id
                        };

                        var termStatus1 = new TermQuery
                        {
                            Field = "status",
                            Value = (int)EnumStatusNewsOnHome.OrderOnHome
                        };

                        var termStatus2 = new TermQuery
                        {
                            Field = "status",
                            Value = (int)EnumStatusNewsOnHome.AcceptedOnHome
                        };

                        var termStatus3 = new TermQuery
                        {
                            Field = "status",
                            Value = (int)EnumStatusNewsOnHome.CanceledOnHome
                        };

                        var result1 = await context.CountAsync<NewsOnHome>(c => c
                                .Index(context.NameOf(typeof(NewsOnHome).Name))
                                .Type("newsonhome")
                                .Query(q => termAccountId && termStatus1)
                            );
                        var result2 = await context.CountAsync<NewsOnHome>(c => c
                                .Index(context.NameOf(typeof(NewsOnHome).Name))
                                .Type("newsonhome")
                                .Query(q => termAccountId && termStatus2)
                            );
                        var result3 = await context.CountAsync<NewsOnHome>(c => c
                                .Index(context.NameOf(typeof(NewsOnHome).Name))
                                .Type("newsonhome")
                                .Query(q => termAccountId && termStatus3)
                            );
                        if (result1 != null)
                        {
                            count1 = (int)result1.Count;
                        }
                        if (result2 != null)
                        {
                            count2 = (int)result2.Count;
                        }
                        if (result3 != null)
                        {
                            count3 = (int)result3.Count;
                        }
                    }
                    var count = new Dictionary<int, int>();
                    count.Add((int)EnumStatusNewsOnHome.OrderOnHome, count1);
                    count.Add((int)EnumStatusNewsOnHome.AcceptedOnHome, count2);
                    count.Add((int)EnumStatusNewsOnHome.CanceledOnHome, count3);
                    data.Add(id.ToString(), count);
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
            }
            return data;
        }
    }
}
