﻿using ChannelVN.IMS2.Core.Entities.Security;
using ChannelVN.IMS2.Core.Entities.Statistics;
using ChannelVN.IMS2.Foundation.Common;
using ChannelVN.IMS2.Foundation.Logging;
using Nest;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace ChannelVN.IMS2.Core.Models.SearchStore.Statistics
{
    public class StatisticsSearchStore : CmsMainSearchStore
    {
        public async Task<PagingDataResult<AccountSearch>> ListPageAsync(SearchPage search)
        {
            var data = new PagingDataResult<AccountSearch>();
            try
            {
                var context = GetContext();
                {
                    var searchDescriptor = new SearchDescriptor<AccountSearch>();

                    var termType = new TermQuery();
                    termType.Field = "type";
                    termType.Value = (byte?)AccountType.Official;

                    var termClass = new TermQuery();
                    if (search.Class != null)
                    {
                        termClass.Field = "class";
                        termClass.Value = (byte?)search.Class;
                    }

                    var termStatus = new TermQuery();
                    if (search.Status != null)
                    {
                        termStatus.Field = "status";
                        termStatus.Value = (byte?)search.Status;
                    }

                    var fields = new[] { "fullName", "fullName.folded", "email", "mobile"};
                    
                    var keyword = EscapeQueryBase.EscapeSearchQuery2(search.Keyword);
                    searchDescriptor.Query(q => q
                    .QueryString(mp => mp
                        .Query(keyword)
                        .Fields(f => f.Fields(fields))
                        .MinimumShouldMatch(MinimumShouldMatch.Percentage(100))
                        .DefaultOperator(Operator.And)
                    ) && q.Bool(b => b.Must(m => m.Exists(e => e.Field("createdBy")))) && termClass && termType && termStatus
                    ).From((search.PageIndex - 1) * search.PageSize).Size(search.PageSize);
                    var result = await context.SearchExAsync(searchDescriptor);
                    if (result != null)
                    {
                        data.Total = (int)result.Total;
                        data.Data = result.Data.ToList();
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
            }
            return data;
        }

    }
}
