﻿using ChannelVN.IMS2.Core.Entities;
using ChannelVN.IMS2.Foundation.Common;
using ChannelVN.IMS2.Foundation.Common.Configuration;
using ChannelVN.IMS2.Foundation.Logging;
using Nest;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChannelVN.IMS2.Core.Models.SearchStore
{
    public class NewsCrawlerConfigSearchStore : CmsMainSearchStore
    {
        public async Task<PagingDataResult<NewCrawlerConfigurationSearch>> SearchConfigAsync(string keyword, long officerId, int? status, int pageIndex, int pageSize, ConfigOrderBy? orderBy, DateTime fromDate, DateTime toDate)
        {
            var data = new PagingDataResult<NewCrawlerConfigurationSearch>();
            try
            {
                var context = GetContext();
                {
                    var searchDescriptor = new SearchDescriptor<NewCrawlerConfigurationSearch>();
                    var fieldDate = "createdDate";
                    if (orderBy == null)
                    {
                        searchDescriptor.Sort(s => s.Descending(p => p.CreatedDate));
                    }
                    switch (orderBy)
                    {
                        case ConfigOrderBy.CreatedDate_ASC:
                            searchDescriptor.Sort(s => s.Ascending(p => p.CreatedDate));
                            break;
                        case ConfigOrderBy.CreatedDate_DESC:
                            searchDescriptor.Sort(s => s.Descending(p => p.CreatedDate));
                            break;
                    }

                    var termStatus = new TermQuery();


                    if (status != null)
                    {
                        if (Enum.IsDefined(typeof(EnumStatusNewsCrawler), status))
                        {
                            termStatus.Field = "status";
                            termStatus.Value = (int)status;
                        }
                    }

                    if (status == null)
                    {
                        termStatus.Field = "status";
                        termStatus.Value = (int)EnumStatusNewsCrawler.Delete;
                    }

                    var termOfficerId = new TermQuery
                    {
                        Field = "officerId",                        
                    };
                    if (officerId > 0)
                    {
                        termOfficerId.Value = officerId;
                    }
                    

                    var range = new DateRangeQuery
                    {
                        Field = fieldDate,
                        //TimeZone = "+07:00",
                        Format = AppSettings.Current.ChannelConfiguration.DateFormat
                    };
                    if (fromDate != null && fromDate != DateTime.MinValue && toDate != null && toDate != DateTime.MinValue)
                    {

                        range.GreaterThanOrEqualTo = fromDate.ToString(AppSettings.Current.ChannelConfiguration.DateFormat);
                        range.LessThanOrEqualTo = toDate.ToString(AppSettings.Current.ChannelConfiguration.DateFormat);
                    }
                    else
                    {
                        if (fromDate != null && fromDate != DateTime.MinValue)
                        {
                            range.GreaterThanOrEqualTo = fromDate.ToString(AppSettings.Current.ChannelConfiguration.DateFormat);
                        }
                        else if (toDate != null && toDate != DateTime.MinValue)
                        {
                            range.LessThanOrEqualTo = toDate.ToString(AppSettings.Current.ChannelConfiguration.DateFormat);
                        }
                    }


                    keyword = EscapeQueryBase.EscapeSearchQuery(keyword);

                    searchDescriptor.Query(q => q
                    .QueryString(mp => mp
                        .Query(keyword)
                        .MinimumShouldMatch(MinimumShouldMatch.Percentage(100))
                        .DefaultOperator(Operator.And)
                        
                    ) &&(status != null? termStatus : !termStatus) && termOfficerId && q.DateRange(c => range)
                    ).From((pageIndex - 1) * pageSize).Size(pageSize);

                    var result = await context.SearchExAsync(searchDescriptor);

                    if (result != null)
                    {
                        data.Total = (int)result.Total;
                        data.Data = result.Data.ToList();
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
            }
            return data;
        }

        public async Task<NewCrawlerConfigurationSearch> GetConfigByIdAsync(long id)
        {
            var returnValue = default(NewCrawlerConfigurationSearch);
            try
            {
                var context = GetContext();
                {
                    returnValue = await context.GetAsync<NewCrawlerConfigurationSearch>(id);
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
            }
            return returnValue;
        }

        public async Task<bool> AddConfigAsync(NewCrawlerConfigurationSearch data, bool isQuere = false)
        {
            var returnValue = false;
            try
            {
                using (var context = GetContext())
                {
                    returnValue = await context.CreateAsync(data);
                }
            }
            catch (Exception ex)
            {
                if (!isQuere) Logger.Error(ex, ex.Message);
            }
            return returnValue;
        }

        public async Task<bool> UpdateConfigAsync(NewCrawlerConfigurationSearch data, bool isQueue = false)
        {
            var returnValue = false;
            try
            {
                var context = GetContext();
                {
                    returnValue = await context.UpdateAsync<NewCrawlerConfigurationSearch>(data.Id, data);                   
                }
            }
            catch (Exception ex)
            {
                if (!isQueue) Logger.Error(ex, ex.Message);
            }
            return returnValue;
        }       
    }
}
