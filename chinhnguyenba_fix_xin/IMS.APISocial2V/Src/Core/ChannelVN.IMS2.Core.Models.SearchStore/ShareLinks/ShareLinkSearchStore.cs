﻿using ChannelVN.IMS2.Core.Entities;
using ChannelVN.IMS2.Core.Entities.Security;
using ChannelVN.IMS2.Core.Entities.ShareLinks;
using ChannelVN.IMS2.Foundation.Common;
using ChannelVN.IMS2.Foundation.Common.Configuration;
using ChannelVN.IMS2.Foundation.Logging;
using Nest;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChannelVN.IMS2.Core.Models.SearchStore.ShareLinks
{
    public class ShareLinkSearchStore : CmsMainSearchStore
    {
        public async Task<bool> AddAsync(ShareLinkSearch data, bool isQueue = false)
        {
            var returnValue = false;
            try
            {
                var context = GetContext();
                {
                    returnValue = await context.CreateIndexSettingsAsync<ShareLinkSearch>("title");
                    returnValue = await context.CreateIndexSettingsAsync<NewsAllSearch>("keyword");
                    returnValue = await context.CreateAsync(data);
                    await context.CreateAsync(new NewsAllSearch()
                    {
                        Id = data.Id,
                        Status = data.Status,
                        Type = data.Type,
                        Keyword = data.Title,
                        CardType = data.CardType,
                        CreatedBy = data.CreatedBy,
                        CreatedDate = data.CreatedDate,
                        DistributorId = data.DistributorId,
                        NewsInAccount = data.NewsInAccount,
                        NewsDistribution = data.NewsDistribution
                    });
                    if (!string.IsNullOrEmpty(data.Tags))
                    {
                        var index = 1;
                        var listNewsInTag = new List<NewsInTag>();
                        foreach (var tagId in data.Tags.Split(","))
                        {
                            if (long.TryParse(tagId, out long TagId))
                            {
                                listNewsInTag.Add(new NewsInTag()
                                {
                                    NewsId = data.Id,
                                    TagId = TagId,
                                    Priority = index,
                                    TaggedDate = DateTime.Now
                                });
                                index++;
                            }
                        }
                        if (listNewsInTag.Count > 0) await context.CreateManyAsync(listNewsInTag);
                    }
                }
            }
            catch (Exception ex)
            {
                if (!isQueue) Logger.Error(ex, ex.Message);
            }
            return returnValue;
        }

        public async Task<PagingDataResult<ShareLinkSearch>> SearchAsync(SearchNews search, long userId, bool isGetDistribution)
        {
            var data = new PagingDataResult<ShareLinkSearch>();
            try
            {
                var context = GetContext();
                {
                    var isOfficialAccount = false;
                    //check account in official
                    if (search.OfficerId > 0 && search.OfficerId != userId)
                    {
                        var searchAccount = new SearchDescriptor<AccountSearch>();
                        var termType = new TermQuery()
                        {
                            Field = "type",
                            Value = 2
                        };
                        var termId = new TermQuery()
                        {
                            Field = "id",
                            Value = search.OfficerId
                        };
                        var termMemberId = new TermQuery()
                        {
                            Field = "accountMember.memberId",
                            Value = userId
                        };
                        var nestTermMember = new NestedQuery()
                        {
                            Path = "accountMember",
                            Query = termMemberId
                        };
                        searchAccount.Query(q => termType && termId && nestTermMember
                           ).From(0).Size(1);

                        var resultCheck = await context.SearchExAsync(searchAccount);
                        if (resultCheck != null && resultCheck.Total > 0)
                        {
                            isOfficialAccount = true;
                        }
                        if (isOfficialAccount == false) return data;
                    }
                    //end check account
                    var searchDescriptor = new SearchDescriptor<ShareLinkSearch>();
                    if (null != search.OrderBy)
                    {
                        switch (search.OrderBy)
                        {
                            case SearchNewsOrderBy.DescDistributionDate:
                                searchDescriptor.Sort(s => s.Descending(p => p.DistributionDate));
                                break;
                            case SearchNewsOrderBy.AscDistributionDate:
                                searchDescriptor.Sort(s => s.Ascending(p => p.DistributionDate));
                                break;
                            case SearchNewsOrderBy.DescCreatedDate:
                                searchDescriptor.Sort(s => s.Descending(p => p.CreatedDate));
                                break;
                            case SearchNewsOrderBy.AscCreatedDate:
                                searchDescriptor.Sort(s => s.Ascending(p => p.CreatedDate));
                                break;
                            case SearchNewsOrderBy.DescModifiedDate:
                                searchDescriptor.Sort(s => s.Descending(p => p.ModifiedDate));
                                break;
                            case SearchNewsOrderBy.AscModifiedDate:
                                searchDescriptor.Sort(s => s.Ascending(p => p.ModifiedDate));
                                break;
                            default:
                                searchDescriptor.Sort(s => s.Descending(p => p.CreatedDate));
                                break;
                        }
                    }
                    else
                    {
                        searchDescriptor.Sort(s => s.Descending(p => p.CreatedDate));
                    }
                    var termCategoryId = new TermQuery();
                    if (search.CategoryId > 0)
                    {
                        termCategoryId.Field = "categoryId";
                        termCategoryId.Value = search.CategoryId;
                    }
                    var listStatus = new List<string>();
                    if (!string.IsNullOrEmpty(search.Status))
                    {
                        var arrStatus = search.Status.Split(",");
                        for (var i = 0; i < arrStatus.Length; i++)
                        {
                            if (int.TryParse(arrStatus[i], out int st))
                            {
                                if (Enum.IsDefined(typeof(NewsStatus), st))
                                {
                                    listStatus.Add(st.ToString());
                                }
                            }
                        }
                    }
                    var termsStatus = new TermsQuery()
                    {
                        Field = "status",
                        Terms = listStatus.ToArray()
                    };

                    var termNewsInAccount = new TermQuery();
                    if (isOfficialAccount)
                    {
                        termNewsInAccount.Field = "newsInAccount.accountId";
                        termNewsInAccount.Value = search.OfficerId;
                    }
                    else
                    {
                        termNewsInAccount.Field = "newsInAccount.accountId";
                        termNewsInAccount.Value = userId;
                    }
                    var nestTermNewsInAccount = new NestedQuery()
                    {
                        Path = "newsInAccount",
                        Query = termNewsInAccount
                    };

                    var termCreatedBy = new TermQuery();
                    if (search.OfficerId != userId)
                    {
                        termCreatedBy.Field = "createdBy";
                        termCreatedBy.Value = userId;
                    };

                    var nestTerm = new NestedQuery();
                    var termDistributorId = new TermQuery();
                    if (search.DistributionId > 0)
                    {
                        termDistributorId.Field = "newsDistribution.distributorId";
                        termDistributorId.Value = search.DistributionId;
                        nestTerm.Path = "newsDistribution";
                        nestTerm.Query = termDistributorId;
                    }
                    else
                    {
                        if (isGetDistribution == true)
                        {
                            var exist = new ExistsQuery()
                            {
                                Field = "newsDistribution.distributorId"
                            };
                            nestTerm.Path = "newsDistribution";
                            nestTerm.Query = exist;
                        }
                    }

                    string fieldDate = "createdDate";
                    switch (search.SearchDateBy)
                    {
                        case 1:
                            fieldDate = "createdDate";
                            break;
                        case 2:
                            fieldDate = "modifiedDate";
                            break;
                        case 3:
                            fieldDate = "publishedDate";
                            break;
                    }
                    var range = new DateRangeQuery
                    {
                        Field = fieldDate,
                        //TimeZone = "+07:00",
                        Format = AppSettings.Current.ChannelConfiguration.DateFormat
                    };
                    if (search.FromDate != null && search.FromDate != DateTime.MinValue && search.ToDate != null && search.ToDate != DateTime.MinValue)
                    {
                        range.GreaterThanOrEqualTo = search.FromDate.Value.ToString(AppSettings.Current.ChannelConfiguration.DateFormat);
                        range.LessThanOrEqualTo = search.ToDate.Value.ToString(AppSettings.Current.ChannelConfiguration.DateFormat);
                    }
                    else
                    {
                        if (search.FromDate != null && search.FromDate != DateTime.MinValue)
                        {
                            range.GreaterThanOrEqualTo = search.FromDate.Value.ToString(AppSettings.Current.ChannelConfiguration.DateFormat);
                        }
                        else if (search.ToDate != null && search.ToDate != DateTime.MinValue)
                        {
                            range.LessThanOrEqualTo = search.ToDate.Value.ToString(AppSettings.Current.ChannelConfiguration.DateFormat);
                        }
                    }
                    var source = new SourceFilterDescriptor<ShareLinkSearch>();
                    var keyword = EscapeQueryBase.EscapeSearchQuery(search.Keyword);
                    var author = EscapeQueryBase.EscapeSearchQuery(search.Author);
                    searchDescriptor.Query(q => q
                    .QueryString(mp => mp
                        .Query(keyword)
                        .Fields(f => f.Fields(new string[] { "name", "name.folded" }))
                        .MinimumShouldMatch(MinimumShouldMatch.Percentage(100))
                        .DefaultOperator(Operator.And)
                    ) && q.QueryString(mp => mp
                        .Query(author)
                        .Fields(f => f.Fields(new string[] { "author", "author.folded" }))
                        .MinimumShouldMatch(MinimumShouldMatch.Percentage(100))
                        .DefaultOperator(Operator.And)
                    ) && termCategoryId && q.DateRange(c => range) && termsStatus && nestTermNewsInAccount && nestTerm && termCreatedBy
                    ).Source(s => source).From((search.PageIndex - 1) * search.PageSize).Size(search.PageSize);
                    //s.IncludeAll().Excludes(e => e.Fields(p => p.MetaData).Fields(p => p.MetaAvatar).Fields(p => p.HtmlCode)
                    var result = await context.SearchExAsync(searchDescriptor);
                    if (result != null)
                    {
                        data.Total = (int)result.Total;
                        data.Data = result.Data.ToList();
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
            }
            return data;
        }

        public async Task<ShareLinkSearch> GetByIdAsync(long id)
        {
            var returnValue = default(ShareLinkSearch);
            try
            {
                var context = GetContext();
                {
                    returnValue = await context.GetAsync<ShareLinkSearch>(id);
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
            }
            return returnValue;
        }
        public async Task<bool> UpdateAsync(ShareLinkSearch data, bool isQueue = false)
        {
            var returnValue = false;
            try
            {
                var context = GetContext();
                {
                    returnValue = await context.UpdateAsync<ShareLinkSearch>(data.Id, data);
                    await context.UpdateAsync<NewsAllSearch>(data.Id, new NewsAllSearch()
                    {
                        Id = data.Id,
                        Status = data.Status,
                        Type = data.Type,
                        Keyword = data.Title,
                        CardType = data.CardType,
                        CreatedBy = data.CreatedBy,
                        CreatedDate = data.CreatedDate,
                        DistributorId = data.DistributorId,
                        NewsInAccount = data.NewsInAccount,
                        NewsDistribution = data.NewsDistribution
                    });
                    try
                    {
                        await Task.WhenAll(context.DeleteByQueryAsync<NewsInTag>(q => q.Query(rq => rq.Term(t => t.NewsId, data.Id))));
                    }
                    catch (Exception)
                    {
                        //Logger.Error(ex, ex.Message);
                    }

                    if (!string.IsNullOrEmpty(data.Tags))
                    {
                        var index = 1;
                        var listNewsInTag = new List<NewsInTag>();
                        foreach (var tagId in data.Tags.Split(","))
                        {
                            if (long.TryParse(tagId, out long TagId))
                            {
                                listNewsInTag.Add(new NewsInTag()
                                {
                                    NewsId = data.Id,
                                    TagId = TagId,
                                    Priority = index,
                                    TaggedDate = DateTime.Now
                                });
                                index++;
                            }
                        }
                        if (listNewsInTag.Count > 0) await context.CreateManyAsync(listNewsInTag);
                    }
                }
            }
            catch (Exception ex)
            {
                if (!isQueue) Logger.Error(ex, ex.Message);
            }
            return returnValue;
        }

        public async Task<bool> UpdateStatusAsync(ShareLinkSearch data, bool isQueue = false)
        {
            var returnValue = false;
            try
            {
                var context = GetContext();
                {
                    if (data != null)
                    {
                        returnValue = await context.UpdateAsync<ShareLinkSearch>(data.Id, data);
                        await context.UpdateAsync<NewsAllSearch>(data.Id, new NewsAllSearch()
                        {
                            Id = data.Id,
                            Status = data.Status,
                            Type = data.Type,
                            Keyword = data.Title,
                            CardType = data.CardType,
                            CreatedBy = data.CreatedBy,
                            CreatedDate = data.CreatedDate,
                            DistributorId = data.DistributorId,
                            NewsInAccount = data.NewsInAccount,
                            NewsDistribution = data.NewsDistribution
                        });
                    }
                }
            }
            catch (Exception ex)
            {
                if (!isQueue) Logger.Error(ex, ex.Message);
            }
            return returnValue;
        }
    }
}
