﻿using ChannelVN.IMS2.Core.Entities;
using ChannelVN.IMS2.Core.Entities.Board;
using ChannelVN.IMS2.Foundation.Common;
using ChannelVN.IMS2.Foundation.Common.Configuration;
using ChannelVN.IMS2.Foundation.Logging;
using Nest;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace ChannelVN.IMS2.Core.Models.SearchStore.Boards
{
    public class BoardSearchStore:CmsMainSearchStore
    {
        public async Task<bool> AddAsync(BoardSearch data, bool isQueue = false)
        {
            var returnValue = false;
            try
            {
                var context = GetContext();
                {
                    returnValue = await context.CreateIndexSettingsAsync<BoardSearch>("name");
                    returnValue = await context.CreateAsync(data);
                }
            }
            catch (Exception ex)
            {
                if(!isQueue) Logger.Error(ex, ex.Message);
            }
            return returnValue;
        }
        public async Task<bool> UpdateAsync(BoardSearch data, bool isQueue = false)
        {
            var returnValue = false;
            try
            {
                var context = GetContext();
                {
                    returnValue = await context.UpdateAsync<BoardSearch>(data.Id, data);
                }
            }
            catch (Exception ex)
            {
                if(!isQueue) Logger.Error(ex, ex.Message);
            }
            return returnValue;
        }

        public async Task<PagingDataResult<BoardSearch>> SearchAsync(SearchBoard search,long userId)
        {
            var data = new PagingDataResult<BoardSearch>();
            try
            {
                var context = GetContext();
                {
                    var searchDescriptor = new SearchDescriptor<BoardSearch>();

                    //order by
                    if (null != search.OrderBy)
                    {
                        switch (search.OrderBy)
                        {
                            case SearchNewsOrderBy.DescCreatedDate:
                                searchDescriptor.Sort(s => s.Descending(p => p.CreatedDate));
                                break;
                            case SearchNewsOrderBy.AscCreatedDate:
                                searchDescriptor.Sort(s => s.Ascending(p => p.CreatedDate));
                                break;
                            case SearchNewsOrderBy.DescModifiedDate:
                                searchDescriptor.Sort(s => s.Descending(p => p.ModifiedDate));
                                break;
                            case SearchNewsOrderBy.AscModifiedDate:
                                searchDescriptor.Sort(s => s.Ascending(p => p.ModifiedDate));
                                break;
                            default:
                                searchDescriptor.Sort(s => s.Descending(p => p.CreatedDate));
                                break;
                        }
                    }
                    else
                    {
                        searchDescriptor.Sort(s => s.Descending(p => p.CreatedDate));
                    }
                    //Search by status
                    var termStatus = new TermQuery()
                    {
                        Field = "status"
                    };
                    if (search.Status!=null)
                    {
                        termStatus.Value = (int)search.Status;
                    }
                    
                    //Search by CreatedBy
                    var termCreatedBy = new TermQuery();
                    termCreatedBy.Field = "createdBy";
                    termCreatedBy.Value = userId.ToString();

                    //search by date
                    var range = new DateRangeQuery
                    {
                        Field = "createdDate",
                        //TimeZone = "+07:00",
                        Format = AppSettings.Current.ChannelConfiguration.DateFormat
                    };
                    if (search.FromDate != null && search.FromDate != DateTime.MinValue && search.ToDate != null && search.ToDate != DateTime.MinValue)
                    {
                        range.GreaterThanOrEqualTo = search.FromDate.Value.ToString(AppSettings.Current.ChannelConfiguration.DateFormat);
                        range.LessThanOrEqualTo = search.ToDate.Value.ToString(AppSettings.Current.ChannelConfiguration.DateFormat);
                    }
                    else
                    {
                        if (search.FromDate != null && search.FromDate != DateTime.MinValue)
                        {
                            range.GreaterThanOrEqualTo = search.FromDate.Value.ToString(AppSettings.Current.ChannelConfiguration.DateFormat);
                        }
                        else if (search.ToDate != null && search.ToDate != DateTime.MinValue)
                        {
                            range.LessThanOrEqualTo = search.ToDate.Value.ToString(AppSettings.Current.ChannelConfiguration.DateFormat);
                        }
                    }
                    var source = new SourceFilterDescriptor<BoardSearch>();
                    source.Includes(s => s.Fields(f => f.Id));
                    var keyword = EscapeQueryBase.EscapeSearchQuery(search.Keyword);
                    searchDescriptor.Query(q => q
                    .QueryString(mp => mp
                        .Query(keyword)
                        .Fields(f => f.Fields(new string[] { "name", "name.folded" }))
                        .MinimumShouldMatch(MinimumShouldMatch.Percentage(100))
                        .DefaultOperator(Operator.And)
                    ) && q.DateRange(c => range) && termStatus && termCreatedBy
                    ).Source(s => source).From((search.PageIndex - 1) * search.PageSize).Size(search.PageSize); //


                    var result = await context.SearchExAsync(searchDescriptor);
                    if (result != null)
                    {
                        data.Total = (int)result.Total;
                        data.Data = result.Data.ToList();
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
            }
            return data;
        }
    }
}
