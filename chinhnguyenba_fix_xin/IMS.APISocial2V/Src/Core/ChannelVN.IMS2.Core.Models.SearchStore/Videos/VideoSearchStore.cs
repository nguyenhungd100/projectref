﻿using ChannelVN.IMS2.Core.Entities;
using ChannelVN.IMS2.Core.Entities.Security;
using ChannelVN.IMS2.Core.Entities.Statistic;
using ChannelVN.IMS2.Core.Entities.Videos;
using ChannelVN.IMS2.Foundation.Common;
using ChannelVN.IMS2.Foundation.Common.Configuration;
using ChannelVN.IMS2.Foundation.Logging;
using Nest;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ChannelVN.IMS2.Core.Models.SearchStore.Videos
{
    public class VideoSearchStore : CmsMainSearchStore
    {
        public async Task<bool> AddAsync(VideoSearch data, bool isQueue = false)
        {
            var returnValue = false;
            try
            {
                var context = GetContext();
                {
                    returnValue = await context.CreateIndexSettingsAsync<VideoSearch>("name");
                    returnValue = await context.CreateIndexSettingsAsync<NewsAllSearch>("keyword");
                    returnValue = await context.CreateAsync(data);
                    await context.CreateAsync(new NewsAllSearch()
                    {
                        Id = data.Id,
                        Status = data.Status,
                        Keyword = data.Name,
                        CardType = data.CardType,
                        CreatedBy = data.CreatedBy,
                        CreatedDate = data.CreatedDate,
                        DistributorId = data.DistributorId,
                        Avatar = data.MetaAvatar,
                        Type = data.Type,
                        NewsInAccount = data.NewsInAccount,
                        NewsDistribution = data.NewsDistribution
                    });
                    if (!string.IsNullOrEmpty(data.Tags))
                    {
                        var index = 1;
                        var listNewsInTag = new List<NewsInTag>();
                        foreach (var tagId in data.Tags.Split(","))
                        {
                            if (long.TryParse(tagId, out long TagId))
                            {
                                listNewsInTag.Add(new NewsInTag()
                                {
                                    NewsId = data.Id,
                                    TagId = TagId,
                                    Priority = index,
                                    TaggedDate = DateTime.Now
                                });
                                index++;
                            }
                        }
                        if (listNewsInTag.Count > 0) await context.CreateManyAsync(listNewsInTag);
                    }
                }
            }
            catch (Exception ex)
            {
                if (!isQueue) Logger.Error(ex, ex.Message);
            }
            return returnValue;
        }

        public async Task<PagingDataResult<VideoSearch>> SearchAsync(SearchNews search, long userId, bool isGetDistribution = false)
        {
            var data = new PagingDataResult<VideoSearch>();
            try
            {
                var context = GetContext();
                {
                    var isOfficialAccount = false;
                    //check account in official
                    if (search.OfficerId > 0 && search.OfficerId != userId)
                    {
                        var searchAccount = new SearchDescriptor<AccountSearch>();
                        var termType = new TermQuery()
                        {
                            Field = "type",
                            Value = 2
                        };
                        var termId = new TermQuery()
                        {
                            Field = "id",
                            Value = search.OfficerId
                        };
                        var termMemberId = new TermQuery()
                        {
                            Field = "accountMember.memberId",
                            Value = userId
                        };
                        var nestTermMember = new NestedQuery()
                        {
                            Path = "accountMember",
                            Query = termMemberId
                        };
                        searchAccount.Query(q => termType && termId && nestTermMember
                           ).From(0).Size(1);

                        var resultCheck = await context.SearchExAsync(searchAccount);
                        if (resultCheck != null && resultCheck.Total > 0)
                        {
                            isOfficialAccount = true;
                        }
                        if (isOfficialAccount == false) return data;
                    }
                    //end check account

                    var searchDescriptor = new SearchDescriptor<VideoSearch>();
                    if (null != search.OrderBy)
                    {
                        switch (search.OrderBy)
                        {
                            case SearchNewsOrderBy.DescDistributionDate:
                                searchDescriptor.Sort(s => s.Descending(p => p.DistributionDate));
                                break;
                            case SearchNewsOrderBy.AscDistributionDate:
                                searchDescriptor.Sort(s => s.Ascending(p => p.DistributionDate));
                                break;
                            case SearchNewsOrderBy.DescCreatedDate:
                                searchDescriptor.Sort(s => s.Descending(p => p.CreatedDate));
                                break;
                            case SearchNewsOrderBy.AscCreatedDate:
                                searchDescriptor.Sort(s => s.Ascending(p => p.CreatedDate));
                                break;
                            case SearchNewsOrderBy.DescModifiedDate:
                                searchDescriptor.Sort(s => s.Descending(p => p.ModifiedDate));
                                break;
                            case SearchNewsOrderBy.AscModifiedDate:
                                searchDescriptor.Sort(s => s.Ascending(p => p.ModifiedDate));
                                break;
                            default:
                                searchDescriptor.Sort(s => s.Descending(p => p.CreatedDate));
                                break;
                        }
                    }
                    else
                    {
                        searchDescriptor.Sort(s => s.Descending(p => p.CreatedDate));
                    }
                    var termCategoryId = new TermQuery();
                    if (search.CategoryId > 0)
                    {
                        termCategoryId.Field = "categoryId";
                        termCategoryId.Value = search.CategoryId;
                    }

                    var listStatus = new List<string>();
                    if (!string.IsNullOrEmpty(search.Status))
                    {
                        var arrStatus = search.Status.Split(",");
                        for (var i = 0; i < arrStatus.Length; i++)
                        {
                            if (int.TryParse(arrStatus[i], out int st))
                            {
                                if (Enum.IsDefined(typeof(NewsStatus), st))
                                {
                                    listStatus.Add(st.ToString());
                                }
                            }
                        }
                    }
                    var termsStatus = new TermsQuery()
                    {
                        Field = "status",
                        Terms = listStatus.ToArray()
                    };

                    var nestTerm = new NestedQuery();
                    var termDistributorId = new TermQuery();
                    if (search.DistributionId > 0)
                    {
                        termDistributorId.Field = "newsDistribution.distributorId";
                        termDistributorId.Value = search.DistributionId;
                        nestTerm.Path = "newsDistribution";
                        nestTerm.Query = termDistributorId;
                    }
                    else
                    {
                        if (isGetDistribution == true)
                        {
                            var exist = new ExistsQuery()
                            {
                                Field = "newsDistribution.distributorId"
                            };
                            nestTerm.Path = "newsDistribution";
                            nestTerm.Query = exist;
                        }
                    }
                    var termCreatedBy = new TermQuery();
                    if (search.OfficerId != userId)
                    {
                        termCreatedBy.Field = "createdBy";
                        termCreatedBy.Value = userId;
                    };
                    var termNewsInAccount = new TermQuery();
                    if (isOfficialAccount)
                    {
                        termNewsInAccount.Field = "newsInAccount.accountId";
                        termNewsInAccount.Value = search.OfficerId;
                    }
                    else
                    {
                        termNewsInAccount.Field = "newsInAccount.accountId";
                        termNewsInAccount.Value = userId;
                    }
                    var nestTermNewsInAccount = new NestedQuery()
                    {
                        Path = "newsInAccount",
                        Query = termNewsInAccount
                    };

                    string fieldDate = "createdDate";
                    switch (search.SearchDateBy)
                    {
                        case 1:
                            fieldDate = "createdDate";
                            break;
                        case 2:
                            fieldDate = "modifiedDate";
                            break;
                        case 3:
                            fieldDate = "publishedDate";
                            break;
                    }
                    var range = new DateRangeQuery
                    {
                        Field = fieldDate,
                        //TimeZone = "+07:00",
                        Format = AppSettings.Current.ChannelConfiguration.DateFormat
                    };
                    if (search.FromDate != null && search.FromDate != DateTime.MinValue && search.ToDate != null && search.ToDate != DateTime.MinValue)
                    {
                        range.GreaterThanOrEqualTo = search.FromDate.Value.ToString(AppSettings.Current.ChannelConfiguration.DateFormat);
                        range.LessThanOrEqualTo = search.ToDate.Value.ToString(AppSettings.Current.ChannelConfiguration.DateFormat);
                    }
                    else
                    {
                        if (search.FromDate != null && search.FromDate != DateTime.MinValue)
                        {
                            range.GreaterThanOrEqualTo = search.FromDate.Value.ToString(AppSettings.Current.ChannelConfiguration.DateFormat);
                        }
                        else if (search.ToDate != null && search.ToDate != DateTime.MinValue)
                        {
                            range.LessThanOrEqualTo = search.ToDate.Value.ToString(AppSettings.Current.ChannelConfiguration.DateFormat);
                        }
                    }
                    var source = new SourceFilterDescriptor<VideoSearch>();
                    var keyword = EscapeQueryBase.EscapeSearchQuery(search.Keyword);
                    var author = EscapeQueryBase.EscapeSearchQuery(search.Author);
                    searchDescriptor.Query(q => q
                    .QueryString(mp => mp
                        .Query(keyword)
                        .Fields(f => f.Fields(new string[] { "name", "name.folded" }))
                        .MinimumShouldMatch(MinimumShouldMatch.Percentage(100))
                        .DefaultOperator(Operator.And)
                    ) && q.QueryString(mp => mp
                        .Query(author)
                        .Fields(f => f.Fields(new string[] { "author", "author.folded" }))
                        .MinimumShouldMatch(MinimumShouldMatch.Percentage(100))
                        .DefaultOperator(Operator.And)
                    ) && termCategoryId && q.DateRange(c => range) && termsStatus && nestTerm && nestTermNewsInAccount && termCreatedBy
                    ).Source(s => source).From((search.PageIndex - 1) * search.PageSize).Size(search.PageSize);
                    var result = await context.SearchExAsync(searchDescriptor);
                    if (result != null)
                    {
                        data.Total = (int)result.Total;
                        data.Data = result.Data.ToList();
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
            }
            return data;
        }

        public PagingDataResult<VideoSearch> Search(SearchNews search, long userId, bool isGetDistribution = false)
        {
            var data = new PagingDataResult<VideoSearch>();
            try
            {
                var context = GetContext();
                {
                    var isOfficialAccount = false;
                    //check account in official
                    if (search.OfficerId > 0 && search.OfficerId != userId)
                    {
                        var searchAccount = new SearchDescriptor<AccountSearch>();
                        var termType = new TermQuery()
                        {
                            Field = "type",
                            Value = 2
                        };
                        var termId = new TermQuery()
                        {
                            Field = "id",
                            Value = search.OfficerId
                        };
                        var termMemberId = new TermQuery()
                        {
                            Field = "accountMember.memberId",
                            Value = userId
                        };
                        var nestTermMember = new NestedQuery()
                        {
                            Path = "accountMember",
                            Query = termMemberId
                        };
                        searchAccount.Query(q => termType && termId && nestTermMember
                           ).From(0).Size(1);

                        var resultCheck = context.SearchEx(searchAccount);
                        if (resultCheck != null && resultCheck.Total > 0)
                        {
                            isOfficialAccount = true;
                        }
                        if (isOfficialAccount == false) return data;
                    }
                    //end check account

                    var searchDescriptor = new SearchDescriptor<VideoSearch>();
                    if (null != search.OrderBy)
                    {
                        switch (search.OrderBy)
                        {
                            case SearchNewsOrderBy.DescDistributionDate:
                                searchDescriptor.Sort(s => s.Descending(p => p.DistributionDate));
                                break;
                            case SearchNewsOrderBy.AscDistributionDate:
                                searchDescriptor.Sort(s => s.Ascending(p => p.DistributionDate));
                                break;
                            case SearchNewsOrderBy.DescCreatedDate:
                                searchDescriptor.Sort(s => s.Descending(p => p.CreatedDate));
                                break;
                            case SearchNewsOrderBy.AscCreatedDate:
                                searchDescriptor.Sort(s => s.Ascending(p => p.CreatedDate));
                                break;
                            case SearchNewsOrderBy.DescModifiedDate:
                                searchDescriptor.Sort(s => s.Descending(p => p.ModifiedDate));
                                break;
                            case SearchNewsOrderBy.AscModifiedDate:
                                searchDescriptor.Sort(s => s.Ascending(p => p.ModifiedDate));
                                break;
                            default:
                                searchDescriptor.Sort(s => s.Descending(p => p.CreatedDate));
                                break;
                        }
                    }
                    else
                    {
                        searchDescriptor.Sort(s => s.Descending(p => p.CreatedDate));
                    }
                    var termCategoryId = new TermQuery();
                    if (search.CategoryId > 0)
                    {
                        termCategoryId.Field = "categoryId";
                        termCategoryId.Value = search.CategoryId;
                    }

                    var listStatus = new List<string>();
                    if (!string.IsNullOrEmpty(search.Status))
                    {
                        var arrStatus = search.Status.Split(",");
                        for (var i = 0; i < arrStatus.Length; i++)
                        {
                            if (int.TryParse(arrStatus[i], out int st))
                            {
                                if (Enum.IsDefined(typeof(NewsStatus), st))
                                {
                                    listStatus.Add(st.ToString());
                                }
                            }
                        }
                    }
                    var termsStatus = new TermsQuery()
                    {
                        Field = "status",
                        Terms = listStatus.ToArray()
                    };

                    var nestTerm = new NestedQuery();
                    var termDistributorId = new TermQuery();
                    if (search.DistributionId > 0)
                    {
                        termDistributorId.Field = "newsDistribution.distributorId";
                        termDistributorId.Value = search.DistributionId;
                        nestTerm.Path = "newsDistribution";
                        nestTerm.Query = termDistributorId;
                    }
                    else
                    {
                        if (isGetDistribution == true)
                        {
                            var exist = new ExistsQuery()
                            {
                                Field = "newsDistribution.distributorId"
                            };
                            nestTerm.Path = "newsDistribution";
                            nestTerm.Query = exist;
                        }
                    }
                    var termCreatedBy = new TermQuery();
                    if (search.OfficerId != userId)
                    {
                        termCreatedBy.Field = "createdBy";
                        termCreatedBy.Value = userId;
                    };
                    var termNewsInAccount = new TermQuery();
                    if (isOfficialAccount)
                    {
                        termNewsInAccount.Field = "newsInAccount.accountId";
                        termNewsInAccount.Value = search.OfficerId;
                    }
                    else
                    {
                        termNewsInAccount.Field = "newsInAccount.accountId";
                        termNewsInAccount.Value = userId;
                    }
                    var nestTermNewsInAccount = new NestedQuery()
                    {
                        Path = "newsInAccount",
                        Query = termNewsInAccount
                    };

                    string fieldDate = "createdDate";
                    switch (search.SearchDateBy)
                    {
                        case 1:
                            fieldDate = "createdDate";
                            break;
                        case 2:
                            fieldDate = "modifiedDate";
                            break;
                        case 3:
                            fieldDate = "publishedDate";
                            break;
                    }
                    var range = new DateRangeQuery
                    {
                        Field = fieldDate,
                        //TimeZone = "+07:00",
                        Format = AppSettings.Current.ChannelConfiguration.DateFormat
                    };
                    if (search.FromDate != null && search.FromDate != DateTime.MinValue && search.ToDate != null && search.ToDate != DateTime.MinValue)
                    {
                        range.GreaterThanOrEqualTo = search.FromDate.Value.ToString(AppSettings.Current.ChannelConfiguration.DateFormat);
                        range.LessThanOrEqualTo = search.ToDate.Value.ToString(AppSettings.Current.ChannelConfiguration.DateFormat);
                    }
                    else
                    {
                        if (search.FromDate != null && search.FromDate != DateTime.MinValue)
                        {
                            range.GreaterThanOrEqualTo = search.FromDate.Value.ToString(AppSettings.Current.ChannelConfiguration.DateFormat);
                        }
                        else if (search.ToDate != null && search.ToDate != DateTime.MinValue)
                        {
                            range.LessThanOrEqualTo = search.ToDate.Value.ToString(AppSettings.Current.ChannelConfiguration.DateFormat);
                        }
                    }
                    var source = new SourceFilterDescriptor<VideoSearch>();
                    var keyword = EscapeQueryBase.EscapeSearchQuery(search.Keyword);
                    var author = EscapeQueryBase.EscapeSearchQuery(search.Author);
                    searchDescriptor.Query(q => q
                    .QueryString(mp => mp
                        .Query(keyword)
                        .Fields(f => f.Fields(new string[] { "name", "name.folded" }))
                        .MinimumShouldMatch(MinimumShouldMatch.Percentage(100))
                        .DefaultOperator(Operator.And)
                    ) && q.QueryString(mp => mp
                        .Query(author)
                        .Fields(f => f.Fields(new string[] { "author", "author.folded" }))
                        .MinimumShouldMatch(MinimumShouldMatch.Percentage(100))
                        .DefaultOperator(Operator.And)
                    ) && termCategoryId && q.DateRange(c => range) && termsStatus && nestTerm && nestTermNewsInAccount && termCreatedBy
                    ).Source(s => source).From((search.PageIndex - 1) * search.PageSize).Size(search.PageSize);
                    var result = context.SearchEx(searchDescriptor);
                    if (result != null)
                    {
                        data.Total = (int)result.Total;
                        data.Data = result.Data.ToList();
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
            }
            return data;
        }

        public async Task<VideoSearch> GetByIdAsync(long id)
        {
            var returnValue = default(VideoSearch);
            try
            {
                var context = GetContext();
                {
                    returnValue = await context.GetAsync<VideoSearch>(id);
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
            }
            return returnValue;
        }

        public async Task<PagingDataResult<VideoSearch>> SearchAllAsync()
        {
            var data = new PagingDataResult<VideoSearch>();
            try
            {
                var context = GetContext();
                {
                    var searchDescriptor = new SearchDescriptor<VideoSearch>();
                    searchDescriptor.From(0).Size(10000);
                    var result = await context.SearchExAsync(searchDescriptor);
                    if (result != null)
                    {
                        data.Total = (int)result.Total;
                        data.Data = result.Data.ToList();
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
            }
            return data;
        }

        public async Task<VideoSearch> SearchByIdAsync(long id)
        {
            var data = default(VideoSearch);
            try
            {
                var context = GetContext();
                {
                    var searchDescriptor = new SearchDescriptor<VideoSearch>();
                    var termId = new TermQuery()
                    {
                        Field = "id",
                        Value = id
                    };
                    searchDescriptor.Query(q => termId).From(0).Size(10000);
                    var result = await context.SearchExAsync(searchDescriptor);
                    if (result != null)
                    {
                        data = result.Data?.ToList()?.FirstOrDefault();
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
            }
            return data;
        }
        //public async Task<PagingDataResult<string>> GetListFileNameSharedAsync(GetViewsByDistribution getViewsByDistribution, long? accountId)
        //{
        //    var data = new PagingDataResult<string>()
        //    {
        //        Data = new List<string>()
        //    };
        //    try
        //    {
        //        var context = GetContext();
        //        {
        //            var searchDescriptor = new SearchDescriptor<VideoSearch>();
        //            var termDistributionId = new TermQuery();
        //            if (getViewsByDistribution.DistributionId > 0)
        //            {
        //                termDistributionId.Field = "newsDistribution.distributorId";
        //                termDistributionId.Value = getViewsByDistribution.DistributionId.ToString();
        //            }
        //            var termNewsInAccount = new TermQuery();
        //            if (accountId > 0)
        //            {
        //                termNewsInAccount.Field = "newsInAccount.accountId";
        //                termNewsInAccount.Value = accountId;
        //            }
        //            var nestNewsInAccount = new NestedQuery
        //            {
        //                Path = "newsInAccount",
        //                Query = termNewsInAccount
        //            };
        //            string fieldDate = "newsDistribution.sharedDate";
        //            var range = new DateRangeQuery
        //            {
        //                Field = fieldDate,
        //                //TimeZone = "+07:00",
        //                Format = AppSettings.Current.ChannelConfiguration.DateFormat
        //            };
        //            if (getViewsByDistribution.FromDate != null && getViewsByDistribution.FromDate != DateTime.MinValue && getViewsByDistribution.ToDate != null && getViewsByDistribution.ToDate != DateTime.MinValue)
        //            {
        //                range.GreaterThanOrEqualTo = getViewsByDistribution.FromDate.Value.ToString(AppSettings.Current.ChannelConfiguration.DateFormat);
        //                range.LessThanOrEqualTo = getViewsByDistribution.ToDate.Value.ToString(AppSettings.Current.ChannelConfiguration.DateFormat);
        //            }
        //            else
        //            {
        //                if (getViewsByDistribution.FromDate != null && getViewsByDistribution.FromDate != DateTime.MinValue)
        //                {
        //                    range.GreaterThanOrEqualTo = getViewsByDistribution.FromDate.Value.ToString(AppSettings.Current.ChannelConfiguration.DateFormat);
        //                }
        //                else if (getViewsByDistribution.ToDate != null && getViewsByDistribution.ToDate != DateTime.MinValue)
        //                {
        //                    range.LessThanOrEqualTo = getViewsByDistribution.ToDate.Value.ToString(AppSettings.Current.ChannelConfiguration.DateFormat);
        //                }
        //            }
        //            var nestDistributionId = new NestedQuery
        //            {
        //                Path = "newsDistribution",
        //                Query = termDistributionId & range
        //            };
        //            searchDescriptor.Query(q => nestDistributionId && nestNewsInAccount
        //            ).Size(1000000);
        //            var result = await context.SearchExAsync(searchDescriptor);
        //            if (result != null)
        //            {
        //                data.Total = (int)result.Total;
        //                foreach (var item in result.Data)
        //                {
        //                    data.Data.Add(item.FileName);
        //                }
        //                data.Data = data.Data.Distinct().ToList();
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        Logger.Error(ex, ex.Message);
        //    }
        //    return data;
        //}

        public async Task<bool> DeleteDataAsync<T>(long id)
        {
            var returnValue = false;
            try
            {
                var context = GetContext();
                {
                    var a = await context.DeleteAsync<VideoSearch>(id, d => d.Index(context.NameOf(typeof(T).Name)).Type(typeof(T).Name.ToLower()));
                    returnValue = a.IsValid;
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
            }
            return returnValue;
        }

        public async Task<PagingDataResult<VideoSearch>> MyVideoAsync(SearchNews search, Account acc)
        {
            var data = new PagingDataResult<VideoSearch>();
            try
            {
                var context = GetContext();
                {

                    var searchDescriptor = new SearchDescriptor<VideoSearch>();
                    if (null != search.OrderBy)
                    {
                        switch (search.OrderBy)
                        {
                            case SearchNewsOrderBy.DescDistributionDate:
                                searchDescriptor.Sort(s => s.Descending(p => p.DistributionDate));
                                break;
                            case SearchNewsOrderBy.AscDistributionDate:
                                searchDescriptor.Sort(s => s.Ascending(p => p.DistributionDate));
                                break;
                            case SearchNewsOrderBy.DescCreatedDate:
                                searchDescriptor.Sort(s => s.Descending(p => p.CreatedDate));
                                break;
                            case SearchNewsOrderBy.AscCreatedDate:
                                searchDescriptor.Sort(s => s.Ascending(p => p.CreatedDate));
                                break;
                            case SearchNewsOrderBy.DescModifiedDate:
                                searchDescriptor.Sort(s => s.Descending(p => p.ModifiedDate));
                                break;
                            case SearchNewsOrderBy.AscModifiedDate:
                                searchDescriptor.Sort(s => s.Ascending(p => p.ModifiedDate));
                                break;
                            default:
                                searchDescriptor.Sort(s => s.Descending(p => p.CreatedDate));
                                break;
                        }
                    }
                    else
                    {
                        searchDescriptor.Sort(s => s.Descending(p => p.CreatedDate));
                    }
                    var termCategoryId = new TermQuery();
                    if (search.CategoryId > 0)
                    {
                        termCategoryId.Field = "categoryId";
                        termCategoryId.Value = search.CategoryId;
                    }
                    var listStatus = new List<string>();
                    if (!string.IsNullOrEmpty(search.Status))
                    {
                        var arrStatus = search.Status.Split(",");
                        for (var i = 0; i < arrStatus.Length; i++)
                        {
                            if (!string.IsNullOrEmpty(arrStatus[i]) && Enum.IsDefined(typeof(NewsStatus), arrStatus[i]?.Trim()?.ToLower()))
                            {
                                listStatus.Add(((int)Enum.Parse(typeof(NewsStatus), arrStatus[i].Trim().ToLower())).ToString());
                            }
                        }
                    }
                    var termsStatus = new TermsQuery()
                    {
                        Field = "status",
                        Terms = listStatus.ToArray()
                    };
                    var termNewsInAccount = new TermQuery
                    {
                        Field = "newsInAccount.accountId",
                        Value = acc.Id
                    };
                    var nestNewsInAccount = new NestedQuery
                    {
                        Path = "newsInAccount",
                        Query = termNewsInAccount
                    };
                    string fieldDate = "createdDate";
                    switch (search.SearchDateBy)
                    {
                        case 1:
                            fieldDate = "createdDate";
                            break;
                        case 2:
                            fieldDate = "modifiedDate";
                            break;
                        case 3:
                            fieldDate = "publishedDate";
                            break;
                    }
                    var range = new DateRangeQuery
                    {
                        Field = fieldDate,
                        //TimeZone = "+07:00",
                        Format = AppSettings.Current.ChannelConfiguration.DateFormat
                    };
                    if (search.FromDate != null && search.FromDate != DateTime.MinValue && search.ToDate != null && search.ToDate != DateTime.MinValue)
                    {
                        range.GreaterThanOrEqualTo = search.FromDate.Value.ToString(AppSettings.Current.ChannelConfiguration.DateFormat);
                        range.LessThanOrEqualTo = search.ToDate.Value.ToString(AppSettings.Current.ChannelConfiguration.DateFormat);
                    }
                    else
                    {
                        if (search.FromDate != null && search.FromDate != DateTime.MinValue)
                        {
                            range.GreaterThanOrEqualTo = search.FromDate.Value.ToString(AppSettings.Current.ChannelConfiguration.DateFormat);
                        }
                        else if (search.ToDate != null && search.ToDate != DateTime.MinValue)
                        {
                            range.LessThanOrEqualTo = search.ToDate.Value.ToString(AppSettings.Current.ChannelConfiguration.DateFormat);
                        }
                    }
                    var keyword = EscapeQueryBase.EscapeSearchQuery(search.Keyword);
                    searchDescriptor.Query(q => q
                    .QueryString(mp => mp
                        .Query(keyword)
                        .Fields(f => f.Fields(new string[] { "name", "name.folded" }))
                        .MinimumShouldMatch(MinimumShouldMatch.Percentage(100))
                        .DefaultOperator(Operator.And)
                    ) && q.QueryString(mp => mp
                        .Query(search.Author)
                        .Fields(f => f.Fields(new string[] { "author", "author.folded" }))
                        .MinimumShouldMatch(MinimumShouldMatch.Percentage(100))
                        .DefaultOperator(Operator.And)
                    ) && nestNewsInAccount && termCategoryId && q.DateRange(c => range) && termsStatus
                    ).From((search.PageIndex - 1) * search.PageSize).Size(search.PageSize);
                    var result = await context.SearchExAsync(searchDescriptor);
                    if (result != null)
                    {
                        data.Total = (int)result.Total;
                        data.Data = result.Data.ToList();
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
            }
            return data;
        }

        public async Task<PagingDataResult<VideoSearch>> GetListRelationAsync(string videoIds, int? pageIndex, int? pageSize)
        {
            var data = new PagingDataResult<VideoSearch>();
            try
            {
                var context = GetContext();
                {
                    var searchDescriptor = new SearchDescriptor<VideoSearch>();
                    var termsId = new TermsQuery()
                    {
                        Field = "id",
                        Terms = videoIds.Split(",")
                    };
                    searchDescriptor.Query(q => termsId
                    ).From((pageIndex - 1) * pageSize).Size(pageSize);
                    var result = await context.SearchExAsync(searchDescriptor);
                    if (result != null)
                    {
                        data.Total = (int)result.Total;
                        data.Data = result.Data.ToList();
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
            }
            return data;
        }

        public async Task<PagingDataResult<VideoSearch>> GetListDistribution(SearchNews searchEntity, Account acc)
        {
            var data = new PagingDataResult<VideoSearch>();
            try
            {
                var context = GetContext();
                {
                    var isOfficialAccount = false;
                    //check account in official
                    if (searchEntity.OfficerId > 0 && searchEntity.OfficerId != acc.Id)
                    {
                        var searchAccount = new SearchDescriptor<AccountSearch>();
                        var termType = new TermQuery()
                        {
                            Field = "type",
                            Value = 2
                        };
                        var termId = new TermQuery()
                        {
                            Field = "id",
                            Value = searchEntity.OfficerId
                        };
                        var termMemberId = new TermQuery()
                        {
                            Field = "accountMember.memberId",
                            Value = acc.Id
                        };
                        var nestTermMember = new NestedQuery()
                        {
                            Path = "accountMember",
                            Query = termMemberId
                        };
                        searchAccount.Query(q => termType && termId && nestTermMember
                           ).From(0).Size(1);

                        var resultCheck = await context.SearchExAsync(searchAccount);
                        if (resultCheck != null && resultCheck.Total > 0)
                        {
                            isOfficialAccount = true;
                        }
                        if (isOfficialAccount == false) return data;
                    }
                    //end check account

                    var searchDescriptor = new SearchDescriptor<VideoSearch>();
                    switch (searchEntity.OrderBy)
                    {
                        case SearchNewsOrderBy.DescDistributionDate:
                            searchDescriptor.Sort(s => s.Descending(p => p.DistributionDate));
                            break;
                        case SearchNewsOrderBy.AscDistributionDate:
                            searchDescriptor.Sort(s => s.Ascending(p => p.DistributionDate));
                            break;
                        case SearchNewsOrderBy.DescCreatedDate:
                            searchDescriptor.Sort(s => s.Descending(p => p.CreatedDate));
                            break;
                        case SearchNewsOrderBy.AscCreatedDate:
                            searchDescriptor.Sort(s => s.Ascending(p => p.CreatedDate));
                            break;
                        case SearchNewsOrderBy.DescModifiedDate:
                            searchDescriptor.Sort(s => s.Descending(p => p.ModifiedDate));
                            break;
                        case SearchNewsOrderBy.AscModifiedDate:
                            searchDescriptor.Sort(s => s.Ascending(p => p.ModifiedDate));
                            break;
                        default:
                            searchDescriptor.Sort(s => s.Descending(p => p.CreatedDate));
                            break;
                    }
                    var termsCategory = new TermsQuery();
                    var nestCategoryId = new NestedQuery();
                    if (searchEntity.CategoryId != null)
                    {
                        termsCategory.Field = "newsDistribution.sharedZone";
                        termsCategory.Terms = new[] { searchEntity.CategoryId?.ToString() ?? "" };
                        nestCategoryId.Path = "newsDistribution";
                        nestCategoryId.Query = termsCategory;
                    }
                    string fieldDate = "createdDate";
                    var range = new DateRangeQuery
                    {
                        Field = fieldDate,
                        //TimeZone = "+07:00",
                        Format = AppSettings.Current.ChannelConfiguration.DateFormat
                    };
                    if (searchEntity.FromDate != null && searchEntity.FromDate != DateTime.MinValue && searchEntity.ToDate != null && searchEntity.ToDate != DateTime.MinValue)
                    {
                        range.GreaterThanOrEqualTo = searchEntity.FromDate.Value.ToString(AppSettings.Current.ChannelConfiguration.DateFormat);
                        range.LessThanOrEqualTo = searchEntity.ToDate.Value.ToString(AppSettings.Current.ChannelConfiguration.DateFormat);
                    }
                    else
                    {
                        if (searchEntity.FromDate != null && searchEntity.FromDate != DateTime.MinValue)
                        {
                            range.GreaterThanOrEqualTo = searchEntity.FromDate.Value.ToString(AppSettings.Current.ChannelConfiguration.DateFormat);
                        }
                        else if (searchEntity.ToDate != null && searchEntity.ToDate != DateTime.MinValue)
                        {
                            range.LessThanOrEqualTo = searchEntity.ToDate.Value.ToString(AppSettings.Current.ChannelConfiguration.DateFormat);
                        }
                    }

                    var termNewsInAccount = new TermQuery();
                    if (isOfficialAccount)
                    {
                        termNewsInAccount.Field = "newsInAccount.accountId";
                        termNewsInAccount.Value = searchEntity.OfficerId;
                    }
                    else
                    {
                        termNewsInAccount.Field = "newsInAccount.accountId";
                        termNewsInAccount.Value = acc.Id;
                    }
                    var nestTermNewsInAccount = new NestedQuery()
                    {
                        Path = "newsInAccount",
                        Query = termNewsInAccount
                    };

                    var termCreatedBy = new TermQuery();
                    if (searchEntity.OfficerId != acc.Id)
                    {
                        termCreatedBy.Field = "createdBy";
                        termCreatedBy.Value = acc.Id;
                    };

                    var termDistributionId = new TermQuery();
                    if (searchEntity.DistributionId > 0)
                    {
                        termDistributionId.Field = "newsDistribution.distributorId";
                        termDistributionId.Value = searchEntity.DistributionId;
                    };
                    var nestDistributionId = new NestedQuery
                    {
                        Path = "newsDistribution",
                        Query = termDistributionId
                    };

                    var keyword = EscapeQueryBase.EscapeSearchQuery(searchEntity.Keyword);
                    searchDescriptor.Query(q => q
                    .QueryString(mp => mp
                        .Query(keyword)
                        .Fields(f => f.Fields(new string[] { "name", "name.folded" }))
                        .MinimumShouldMatch(MinimumShouldMatch.Percentage(100))
                        .DefaultOperator(Operator.And)
                    ) && q.QueryString(mp => mp
                        .Query(searchEntity.Author)
                        .Fields(f => f.Fields(new string[] { "author", "author.folded" }))
                        .MinimumShouldMatch(MinimumShouldMatch.Percentage(100))
                        .DefaultOperator(Operator.And)
                    ) && nestDistributionId && nestCategoryId && range && nestTermNewsInAccount && termCreatedBy //&& nestNewsDistribution
                    ).From((searchEntity.PageIndex - 1) * searchEntity.PageSize).Size(searchEntity.PageSize);
                    var result = await context.SearchExAsync(searchDescriptor);
                    if (result != null)
                    {
                        data.Total = (int)result.Total;
                        data.Data = result.Data?.ToList();
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
            }
            return data;
        }

        public async Task<long> GetCountByStatus(string status, long? officialId, Account acc)
        {
            try
            {
                var context = GetContext();
                {
                    bool isOfficialAccount = false;
                    //check account in official
                    if (officialId > 0 && officialId != acc.Id)
                    {
                        var searchAccount = new SearchDescriptor<AccountSearch>();
                        var termType = new TermQuery()
                        {
                            Field = "type",
                            Value = 2
                        };
                        var termId = new TermQuery()
                        {
                            Field = "id",
                            Value = officialId
                        };
                        var termMemberId = new TermQuery()
                        {
                            Field = "accountMember.memberId",
                            Value = acc.Id
                        };
                        var nestTermMember = new NestedQuery()
                        {
                            Path = "accountMember",
                            Query = termMemberId
                        };
                        searchAccount.Query(q => termType && termId && nestTermMember
                           ).From(0).Size(1);

                        var resultCheck = await context.SearchExAsync(searchAccount);
                        if (resultCheck != null && resultCheck.Total > 0)
                        {
                            isOfficialAccount = true;
                        }
                        if (isOfficialAccount == false) return 0;
                    }
                    //end check account

                    var termStatus = new TermQuery
                    {
                        Field = "status"
                    };
                    if (!string.IsNullOrEmpty(status) && Enum.IsDefined(typeof(NewsStatus), status.ToLower()))
                    {
                        termStatus.Value = (int)Enum.Parse(typeof(NewsStatus), status.ToLower());
                    }
                    else
                    {
                        termStatus.Value = -1;
                    }
                    var termCreatedBy = new TermQuery();
                    if (officialId != acc.Id)
                    {
                        termCreatedBy.Field = "createdBy";
                        termCreatedBy.Value = acc.Id;
                    }

                    var termNewsInAccount = new TermQuery();
                    if (isOfficialAccount)
                    {
                        termNewsInAccount.Field = "newsInAccount.accountId";
                        termNewsInAccount.Value = officialId;
                    }
                    else
                    {
                        termCreatedBy.Field = "createdBy";
                        termCreatedBy.Value = acc.Id;
                    }
                    var nestTermNewsInAccount = new NestedQuery()
                    {
                        Path = "newsInAccount",
                        Query = termNewsInAccount
                    };

                    var result = await context.CountAsync<VideoSearch>(c => c
                        // ReSharper disable once PossiblyMistakenUseOfParamsMethod
                        .Index(context.NameOf(typeof(VideoSearch).Name))
                         .Type(typeof(VideoSearch).Name)
                         .Query(q => termStatus && termNewsInAccount && termCreatedBy && nestTermNewsInAccount)
                    );
                    if (result != null)
                    {
                        return result.Count;
                    }
                    else
                    {
                        return 0;
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
                return 0;
            }
        }


        public async Task<bool> PublishAsync(VideoSearch video)
        {
            var returnValue = false;
            try
            {
                var context = GetContext();
                {
                    if (video != null)
                    {
                        returnValue = await context.UpdateAsync<VideoSearch>(video.Id, video);
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
            }
            return returnValue;
        }

        public async Task<bool> UpdateAsync(VideoSearch data, bool isQueue = false)
        {
            var returnValue = false;
            try
            {
                var context = GetContext();
                {

                    returnValue = await context.UpdateAsync<VideoSearch>(data.Id, data);
                    await context.UpdateAsync<NewsAllSearch>(data.Id, new NewsAllSearch()
                    {
                        Id = data.Id,
                        Status = data.Status,
                        Keyword = data.Name,
                        CardType = data.CardType,
                        CreatedBy = data.CreatedBy,
                        CreatedDate = data.CreatedDate,
                        DistributorId = data.DistributorId,
                        Avatar = data.MetaAvatar,
                        NewsInAccount = data.NewsInAccount,
                        Type = data.Type,
                        NewsDistribution = data.NewsDistribution
                    });
                    try
                    {
                        await Task.WhenAll(context.DeleteByQueryAsync<NewsInTag>(q => q.Query(rq => rq.Term(t => t.NewsId, data.Id))));
                    }
                    catch (Exception)
                    {
                        //Logger.Error(ex, ex.Message);
                    }

                    if (!string.IsNullOrEmpty(data.Tags))
                    {
                        var index = 1;
                        var listNewsInTag = new List<NewsInTag>();
                        foreach (var tagId in data.Tags.Split(","))
                        {
                            if (long.TryParse(tagId, out long TagId))
                            {
                                listNewsInTag.Add(new NewsInTag()
                                {
                                    NewsId = data.Id,
                                    TagId = TagId,
                                    Priority = index,
                                    TaggedDate = DateTime.Now
                                });
                                index++;
                            }
                        }
                        if (listNewsInTag.Count > 0) await context.CreateManyAsync(listNewsInTag);
                    }
                }
            }
            catch (Exception ex)
            {
                if (!isQueue) Logger.Error(ex, ex.Message);
            }
            return returnValue;
        }



        public async Task<bool> UpdateStatusAsync(VideoSearch data, bool isQueue = false)
        {
            var returnValue = false;
            try
            {
                var context = GetContext();
                {
                    if (data != null)
                    {
                        returnValue = await context.UpdateAsync<VideoSearch>(data.Id, data);
                        await context.UpdateAsync<NewsAllSearch>(data.Id, new NewsAllSearch()
                        {
                            Id = data.Id,
                            Status = data.Status,
                            Keyword = data.Name,
                            CardType = data.CardType,
                            CreatedBy = data.CreatedBy,
                            CreatedDate = data.CreatedDate,
                            DistributorId = data.DistributorId,
                            Avatar = data.MetaAvatar,
                            Type = data.Type,
                            NewsInAccount = data.NewsInAccount,
                            NewsDistribution = data.NewsDistribution
                        });
                    }
                }
            }
            catch (Exception ex)
            {
                if (!isQueue) Logger.Error(ex, ex.Message);
            }
            return returnValue;
        }

        public async Task<bool> InitAllVideoAsync(List<VideoSearch> data)
        {
            var returnValue = false;
            try
            {

                var context = GetContext();
                {
                    returnValue = await context.CreateIndexSettingsAsync<VideoSearch>("name");
                    returnValue = await context.CreateManyAsync(data);
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
            }
            return returnValue;
        }

        //public async Task<Dictionary<string, StatisticByAccount>> GetStatisticVideoByAccount(GetStatisticByAccount getStatisticByAccount)
        //{
        //    var dataResult = default(Dictionary<string, StatisticByAccount>);
        //    try
        //    {
        //        var formatDate = AppSettings.Current.ChannelConfiguration.DateFormat;
        //        dataResult = Utility.EachDay(getStatisticByAccount.FromDate.Value, getStatisticByAccount.ToDate.Value)
        //           .ToDictionary(p => p.ToString("dd-MM-yyyy"),
        //           p => new StatisticByAccount() { AccountId = getStatisticByAccount.AccountId, Count = 0 });
        //        var context = GetContext();
        //        {
        //            var searchDescriptor = new SearchDescriptor<VideoSearch>();

        //            var termNewsInAccount = new TermQuery();
        //            termNewsInAccount.Field = "newsInAccount.accountId";
        //            termNewsInAccount.Value = getStatisticByAccount.AccountId;
        //            var nestNewsInAccount = new NestedQuery
        //            {
        //                Path = "newsInAccount",
        //                Query = termNewsInAccount
        //            };
        //            var termDistributionId = new TermQuery();
        //            if (getStatisticByAccount.DistributionId > 0)
        //            {
        //                termDistributionId.Field = "newsDistribution.distributorId";
        //                termDistributionId.Value = getStatisticByAccount.DistributionId.ToString();
        //            }
        //            string fieldDate = "newsDistribution.sharedDate";
        //            var range = new DateRangeQuery
        //            {
        //                Field = fieldDate,
        //                //TimeZone = "+07:00",
        //                Format = formatDate
        //            };
        //            if (getStatisticByAccount.FromDate != null && getStatisticByAccount.FromDate != DateTime.MinValue
        //                && getStatisticByAccount.ToDate != null && getStatisticByAccount.ToDate != DateTime.MinValue)
        //            {
        //                range.GreaterThanOrEqualTo = getStatisticByAccount.FromDate.Value.ToString(formatDate);
        //                range.LessThanOrEqualTo = getStatisticByAccount.ToDate.Value.ToString(formatDate);
        //            }
        //            else
        //            {
        //                if (getStatisticByAccount.FromDate != null && getStatisticByAccount.FromDate != DateTime.MinValue)
        //                {
        //                    range.GreaterThanOrEqualTo = getStatisticByAccount.FromDate.Value.ToString(formatDate);
        //                }
        //                else if (getStatisticByAccount.ToDate != null && getStatisticByAccount.ToDate != DateTime.MinValue)
        //                {
        //                    range.LessThanOrEqualTo = getStatisticByAccount.ToDate.Value.ToString(formatDate);
        //                }
        //            }
        //            var nestDistribution = new NestedQuery
        //            {
        //                Path = "newsDistribution",
        //                Query = termDistributionId && range
        //            };
        //            searchDescriptor.Query(q => nestNewsInAccount && nestDistribution).Size(10000);
        //            var result = await context.SearchExAsync(searchDescriptor);
        //            if (result != null)
        //            {
        //                var data = result.Data?.ToList();
        //                dataResult = dataResult.ToDictionary(p => p.Key, p => new StatisticByAccount()
        //                {
        //                    AccountId = p.Value.AccountId,
        //                    Count = data.Where(v => v.NewsDistribution.Where(d => d.SharedDate?.ToString("dd-MM-yyyy").Equals(p.Key) ?? false).Count() > 0)?.Count() ?? 0
        //                });
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        Logger.Error(ex, ex.Message);
        //    }
        //    return dataResult;
        //}

        //public async Task<Dictionary<string, int>> GetStatisticVideoAllPage(GetStatisticAllPage getStatisticAllPage)
        //{
        //    var dataResult = default(Dictionary<string, int>);
        //    try
        //    {
        //        var formatDate = AppSettings.Current.ChannelConfiguration.DateFormat;
        //        dataResult = Utility.EachDay(getStatisticAllPage.FromDate.Value, getStatisticAllPage.ToDate.Value)
        //           .ToDictionary(p => p.ToString("dd-MM-yyyy"),
        //           p => 0);
        //        var context = GetContext();
        //        {
        //            var searchDescriptor = new SearchDescriptor<VideoSearch>();

        //            var termDistributionId = new TermQuery();

        //            if (getStatisticAllPage.DistributionId > 0)
        //            {
        //                termDistributionId.Field = "newsDistribution.distributorId";
        //                termDistributionId.Value = getStatisticAllPage.DistributionId.ToString();
        //            }
        //            string fieldDate = "newsDistribution.sharedDate";
        //            var range = new DateRangeQuery
        //            {
        //                Field = fieldDate,
        //                //TimeZone = "+07:00",
        //                Format = formatDate
        //            };
        //            if (getStatisticAllPage.FromDate != null && getStatisticAllPage.FromDate != DateTime.MinValue
        //                && getStatisticAllPage.ToDate != null && getStatisticAllPage.ToDate != DateTime.MinValue)
        //            {
        //                range.GreaterThanOrEqualTo = getStatisticAllPage.FromDate.Value.ToString(formatDate);
        //                range.LessThanOrEqualTo = getStatisticAllPage.ToDate.Value.ToString(formatDate);
        //            }
        //            else
        //            {
        //                if (getStatisticAllPage.FromDate != null && getStatisticAllPage.FromDate != DateTime.MinValue)
        //                {
        //                    range.GreaterThanOrEqualTo = getStatisticAllPage.FromDate.Value.ToString(formatDate);
        //                }
        //                else if (getStatisticAllPage.ToDate != null && getStatisticAllPage.ToDate != DateTime.MinValue)
        //                {
        //                    range.LessThanOrEqualTo = getStatisticAllPage.ToDate.Value.ToString(formatDate);
        //                }
        //            }
        //            var nestDistribution = new NestedQuery
        //            {
        //                Path = "newsDistribution",
        //                Query = termDistributionId & range
        //            };
        //            searchDescriptor.Query(q => nestDistribution).Size(10000);
        //            var result = await context.SearchExAsync(searchDescriptor);
        //            if (result != null)
        //            {
        //                var data = result.Data?.ToList();
        //                dataResult = dataResult.ToDictionary(p => p.Key,
        //                    p => data.Where(v => v.NewsDistribution.Where(d => d.SharedDate?.ToString("dd-MM-yyyy").Equals(p.Key) ?? false).Count() > 0)?.Count() ?? 0);
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        Logger.Error(ex, ex.Message);
        //    }
        //    return dataResult;
        //}
    }
}
