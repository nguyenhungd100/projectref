﻿using ChannelVN.IMS2.Core.Entities;
using ChannelVN.IMS2.Foundation.Common;
using ChannelVN.IMS2.Foundation.Logging;
using Nest;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ChannelVN.IMS2.Core.Models.SearchStore
{
    public class TagSearchStore : CmsMainSearchStore
    {
        public async Task<bool> AddAsync(Tag data, bool isQueue = false)
        {
            var returnValue = false;
            try
            {
                var context = GetContext();
                {
                    returnValue = await context.CreateIndexSettingsAsync<Tag>("name");
                    returnValue = await context.CreateAsync(data);
                }
            }
            catch (Exception ex)
            {
                if (!isQueue) Logger.Error(ex, ex.Message);
            }
            return returnValue;

        }
        public bool Add(Tag data)
        {
            var returnValue = false;
            try
            {
                var context = GetContext();
                {
                    returnValue = context.CreateIndexSettings<Tag>("name");
                    returnValue = context.Create(data);
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
            }
            return returnValue;
        }
        public async Task<bool> UpdateAsync(Tag data, bool isQueue = false)
        {
            var returnValue = false;
            try
            {
                var context = GetContext();
                {
                    returnValue = await context.UpdateAsync<Tag>(data.Id, data);
                }
            }
            catch (Exception ex)
            {
                if (!isQueue) Logger.Error(ex, ex.Message);
            }
            return returnValue;
        }
        public async Task<Tag> GetByIdAsync(int zoneId)
        {
            var returnValue = default(Tag);
            try
            {
                var context = GetContext();
                {
                    returnValue = await context.GetAsync<Tag>(zoneId);
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
            }
            return returnValue;
        }

        public async Task<PagingDataResult<Tag>> SearchAsync(string keyword, int? status, int pageIndex, int pageSize)
        {
            var data = new PagingDataResult<Tag>();
            try
            {
                var context = GetContext();
                {
                    var searchDescriptor = new SearchDescriptor<Tag>();
                    var fields = new[] { "name", "name.folded" };

                    var termStatus = new TermQuery();
                    if (Enum.IsDefined(typeof(TagStatus), status))
                    {
                        termStatus.Field = "status";
                        termStatus.Value = (int)status;
                    }
                    keyword = EscapeQueryBase.EscapeSearchQuery(keyword);

                    searchDescriptor.Query(q => q
                    .QueryString(mp => mp
                        .Query(keyword)
                        .Fields(f => f.Fields(fields))
                        .MinimumShouldMatch(MinimumShouldMatch.Percentage(100))
                        .DefaultOperator(Operator.And)
                    ) && termStatus
                    ).From((pageIndex - 1) * pageSize).Size(pageSize);

                    //searchDescriptor.Query(q=>q.QueryString(mp=>mp.Fields))
                    var result = await context.SearchExAsync(searchDescriptor);

                    if (result != null)
                    {
                        data.Total = (int)result.Total;
                        data.Data = result.Data.ToList();
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
            }
            return data;
        }

        public async Task<Tag> GetByNameAsync(string name)
        {
            var data = default(Tag);
            try
            {
                var context = GetContext();
                {
                    var searchDescriptor = new SearchDescriptor<Tag>();
                    var termName = new TermQuery()
                    {
                        Field = "md5",
                        Value = Encryption.Md5(name ?? "")
                    };
                    searchDescriptor.Query(q => termName
                    ).From(0).Size(100);

                    var result = await context.SearchExAsync(searchDescriptor);

                    if (result != null)
                    {

                        data = result.Data.ToList().FirstOrDefault();
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
            }
            return data;
        }

        public async Task<bool> InitAllVideoTagAsync(List<Tag> data)
        {
            var returnValue = false;
            try
            {
                var context = GetContext();
                {
                    returnValue = context.CreateIndexSettings<Tag>("name");
                    returnValue = await context.CreateManyAsync(data);
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
            }
            return returnValue;
        }
    }
}
