﻿using ChannelVN.IMS2.Core.Entities;
using ChannelVN.IMS2.Core.Entities.Album;
using ChannelVN.IMS2.Core.Entities.Beam;
using ChannelVN.IMS2.Core.Entities.Distribution;
using ChannelVN.IMS2.Core.Entities.Gallery;
using ChannelVN.IMS2.Core.Entities.Media;
using ChannelVN.IMS2.Core.Entities.Newsletters;
using ChannelVN.IMS2.Core.Entities.Photo;
using ChannelVN.IMS2.Core.Entities.Posts;
using ChannelVN.IMS2.Core.Entities.ShareLinks;
using ChannelVN.IMS2.Core.Entities.Videos;
using ChannelVN.IMS2.Foundation.Common;
using ChannelVN.IMS2.Foundation.Logging;
using Nest;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ChannelVN.IMS2.Core.Models.SearchStore.Distribution
{
    public class DistributionSearchStore : CmsMainSearchStore
    {
        public async Task<bool> AddAsync(Distributor data)
        {
            var returnValue = false;
            try
            {
                var context = GetContext();
                {
                    var fieldNameAnalyzer = "name";
                    returnValue = await context.CreateIndexSettingsAsync<Distributor>(fieldNameAnalyzer);
                    returnValue = await context.CreateAsync(data);
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
            }
            return returnValue;
        }

        public async Task<bool> UpdateAsync(Distributor data)
        {
            var returnValue = false;
            try
            {
                var context = GetContext();
                {
                    var obj = await context.GetAsync<Distributor>(data.Id);
                    if (obj != null)
                    {
                        obj.Name = data.Name;
                        obj.Description = data.Description;
                        obj.Status = data.Status;
                        obj.Priority = data.Priority;
                        //obj.ConfigQuotaNormal = data.ConfigQuotaNormal;
                        //obj.ConfigQuotaProfessional = data.ConfigQuotaProfessional;
                        //obj.ConfigUnitTime = data.ConfigUnitTime;
                        //obj.ConfigUnitAmount = data.ConfigUnitAmount;
                        //obj.ConfigZoneVideo = data.ConfigZoneVideo;
                        //obj.ConfigVideo = data.ConfigVideo;
                        //obj.ConfigCode = data.ConfigCode;
                        //obj.ConfigVideoPlaylist = data.ConfigVideoPlaylist;
                        returnValue = await context.UpdateAsync<Distributor>(obj.Id, obj);
                    }

                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
            }
            return returnValue;
        }

        public async Task<Distributor> GetByIdAsync(long id)
        {
            var returnValue = default(Distributor);
            try
            {
                var context = GetContext();
                {
                    returnValue = await context.GetAsync<Distributor>(id);
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);

            }
            return returnValue;
        }

        public async Task<PagingDataResult<Distributor>> SearchAsync(string keyword, int? status, int pageIndex, int pageSize)
        {
            var data = new PagingDataResult<Distributor>();
            try
            {
                var context = GetContext();
                {
                    var searchDescriptor = new SearchDescriptor<Distributor>();
                    var fields = new[] { "name", "name.folded" };

                    var termStatus = new TermQuery();
                    if (status > -1)
                    {
                        termStatus.Field = "status";
                        termStatus.Value = status;
                    }

                    keyword = EscapeQueryBase.EscapeSearchQuery(keyword);
                    if (string.IsNullOrEmpty(keyword))
                    {
                        searchDescriptor.Query(q => termStatus
                        ).From((pageIndex - 1) * pageSize).Size(pageSize);
                    }
                    else
                    {
                        searchDescriptor.Query(q => q
                        .QueryString(mp => mp
                            .Query(keyword)
                            .Fields(f => f.Fields(fields))
                            .MinimumShouldMatch(MinimumShouldMatch.Percentage(100))
                            .DefaultOperator(Operator.And)
                        ) && termStatus
                        ).From((pageIndex - 1) * pageSize).Size(pageSize);
                    }


                    var result = await context.SearchExAsync(searchDescriptor);

                    if (result != null)
                    {
                        data.Total = (int)result.Total;
                        data.Data = result.Data.ToList();
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
            }
            return data;
        }

        public async Task<bool> ShareMediaUnitAsync(ItemStreamDistribution itemStreamDistribution, List<MediaUnitSearch> listMedia, bool isQueue = false)
        {
            var returnValue = false;
            try
            {
                var context = GetContext();
                {

                    returnValue = context.CreateIndexSettings<MediaUnitSearch>("caption");
                    returnValue = await context.CreateManyAsync(listMedia);
                    if (returnValue)
                    {
                        returnValue = context.CreateIndexSettings<ItemStreamDistribution>("title");
                        returnValue = await context.CreateManyAsync(new List<ItemStreamDistribution> { itemStreamDistribution });
                    }
                    foreach (var item in listMedia)
                    {
                        await context.UpdateAsync<NewsAllSearch>(item.Id, new NewsAllSearch()
                        {
                            Id = item.Id,
                            Status = item.Status,
                            Type = item.Type,
                            Keyword = item.Caption,
                            CardType = item.CardType,
                            CreatedBy = item.CreatedBy,
                            CreatedDate = item.CreatedDate,
                            DistributorId = item.DistributorId,
                            NewsInAccount = item.NewsInAccount,
                            NewsDistribution = item.NewsDistribution
                        });
                    }
                }
            }
            catch (Exception ex)
            {
                if(!isQueue) Logger.Error(ex, ex.Message);
            }
            return returnValue;
        }

        public async Task<bool> ShareAlbumAsync(ItemStreamDistribution itemStreamDistribution, List<AlbumSearch> listMedia, bool isQueue = false)
        {
            var returnValue = false;
            try
            {
                var context = GetContext();
                {

                    returnValue = context.CreateIndexSettings<AlbumSearch>("name");
                    returnValue = await context.CreateManyAsync(listMedia);
                    if (returnValue)
                    {
                        returnValue = context.CreateIndexSettings<ItemStreamDistribution>("title");
                        returnValue = await context.CreateManyAsync(new List<ItemStreamDistribution> { itemStreamDistribution });
                    }
                    foreach (var item in listMedia)
                    {
                        await context.UpdateAsync<NewsAllSearch>(item.Id, new NewsAllSearch()
                        {
                            Id = item.Id,
                            Status = item.Status,
                            Type = item.Type,
                            Keyword = item.Name,
                            CardType = item.CardType,
                            CreatedBy = item.CreatedBy,
                            CreatedDate = item.CreatedDate,
                            DistributorId = item.DistributorId,
                            NewsInAccount = item.NewsInAccount,
                            NewsDistribution = item.NewsDistribution
                        });
                    }
                }
            }
            catch (Exception ex)
            {
                if(!isQueue) Logger.Error(ex, ex.Message);
            }
            return returnValue;
        }

        public async Task<bool> ShareGalleryAsync(ItemStreamDistribution itemStreamDistribution, List<GallerySearch> listMedia, bool isQueue = false)
        {
            var returnValue = false;
            try
            {
                var context = GetContext();
                {

                    returnValue = context.CreateIndexSettings<GallerySearch>("name");
                    returnValue = await context.CreateManyAsync(listMedia);
                    if (returnValue)
                    {
                        returnValue = context.CreateIndexSettings<ItemStreamDistribution>("title");
                        returnValue = await context.CreateManyAsync(new List<ItemStreamDistribution> { itemStreamDistribution });
                    }
                    foreach (var item in listMedia)
                    {
                        await context.UpdateAsync<NewsAllSearch>(item.Id, new NewsAllSearch()
                        {
                            Id = item.Id,
                            Status = item.Status,
                            Type = item.Type,
                            Keyword = item.Name,
                            CardType = item.CardType,
                            CreatedBy = item.CreatedBy,
                            CreatedDate = item.CreatedDate,
                            DistributorId = item.DistributorId,
                            NewsInAccount = item.NewsInAccount,
                            NewsDistribution = item.NewsDistribution
                        });
                    }
                }
            }
            catch (Exception ex)
            {
                if (!isQueue) Logger.Error(ex, ex.Message);
            }
            return returnValue;
        }
        public async Task<bool> SharePhotoAsync(ItemStreamDistribution itemStreamDistribution, List<PhotoSearch> listMedia, bool isQueue = false)
        {
            var returnValue = false;
            try
            {
                var context = GetContext();
                {

                    returnValue = context.CreateIndexSettings<PhotoSearch>("caption");
                    returnValue = await context.CreateManyAsync(listMedia);
                    if (returnValue)
                    {
                        returnValue = context.CreateIndexSettings<ItemStreamDistribution>("title");
                        returnValue = await context.CreateManyAsync(new List<ItemStreamDistribution> { itemStreamDistribution });
                    }
                    foreach (var item in listMedia)
                    {
                        await context.UpdateAsync<NewsAllSearch>(item.Id, new NewsAllSearch()
                        {
                            Id = item.Id,
                            Status = item.Status,
                            Type = item.Type,
                            Keyword = item.Caption,
                            CardType = item.CardType,
                            CreatedBy = item.CreatedBy,
                            CreatedDate = item.CreatedDate,
                            DistributorId = item.DistributorId,
                            NewsInAccount = item.NewsInAccount,
                            NewsDistribution = item.NewsDistribution
                        });
                    }
                }
            }
            catch (Exception ex)
            {
                if(!isQueue) Logger.Error(ex, ex.Message);
            }
            return returnValue;
        }

        public async Task<bool> ShareArticleAsync(ItemStreamDistribution itemStream, List<ArticleSearch> listNews, bool isQueue = false)
        {
            var returnValue = false;
            try
            {
                var context = GetContext();
                {
                    returnValue = context.CreateIndexSettings<ArticleSearch>("title", "author");
                    returnValue = await context.CreateManyAsync(listNews);
                    if (returnValue)
                    {
                        returnValue = context.CreateIndexSettings<ItemStreamDistribution>("title");
                        returnValue = await context.CreateAsync(itemStream);
                    }
                    foreach (var item in listNews)
                    {
                        await context.UpdateAsync<NewsAllSearch>(item.Id, new NewsAllSearch()
                        {
                            Id = item.Id,
                            Status = item.Status,
                            Type = item.Type,
                            Keyword = item.Title,
                            CardType = item.CardType,
                            CreatedBy = item.CreatedBy,
                            CreatedDate = item.CreatedDate,
                            DistributorId = item.DistributorId,
                            Avatar = item.Avatar1,
                            NewsDistribution = item.NewsDistribution
                        });
                    }
                }
            }
            catch (Exception ex)
            {
                if(!isQueue) Logger.Error(ex, ex.Message);
            }
            return returnValue;
        }

        public async Task<bool> ShareVideoAsync(ItemStreamDistribution itemStream, List<VideoSearch> listVideo, bool isUpdate = false, bool isQueue = false)
        {
            var returnValue = false;
            try
            {
                var context = GetContext();
                {
                    if (isUpdate)
                    {
                        var itemStreamDb = await context.GetAsync<ItemStreamDistribution>(itemStream.Id);
                        itemStream.CreatedDate = itemStreamDb?.CreatedDate;
                    }
                    returnValue = context.CreateIndexSettings<VideoSearch>("name", "author");
                    returnValue = await context.CreateManyAsync(listVideo);
                    if (returnValue == true)
                    {
                        returnValue = context.CreateIndexSettings<ItemStreamDistribution>("title", "author");
                        returnValue = await context.CreateManyAsync(new List<ItemStreamDistribution>() { itemStream });
                        //returnValue = await context.UpdateAsync<Account>(account.Id, account);
                    }
                    foreach (var item in listVideo)
                    {
                        await context.UpdateAsync<NewsAllSearch>(item.Id, new NewsAllSearch()
                        {
                            Id = item.Id,
                            Status = item.Status,
                            Type = item.Type,
                            Keyword = item.Name,
                            CardType = item.CardType,
                            CreatedBy = item.CreatedBy,
                            CreatedDate = item.CreatedDate,
                            DistributorId = item.DistributorId,
                            Avatar = item.MetaAvatar,
                            NewsDistribution = item.NewsDistribution
                        });
                    }
                }
            }
            catch (Exception ex)
            {
                if(!isQueue) Logger.Error(ex, ex.Message);
            }
            return returnValue;
        }

        public async Task<bool> ShareVideoPlaylistAsync(ItemStreamDistribution itemStreamDistribution, List<VideoPlaylistSearch> listVideoPlaylist, bool isUpdate = false, bool isQueue = false)
        {
            var returnValue = false;
            try
            {
                var context = GetContext();
                {
                    if (isUpdate)
                    {
                        var itemStreamDb = await context.GetAsync<ItemStreamDistribution>(itemStreamDistribution.Id);
                        itemStreamDistribution.CreatedDate = itemStreamDb?.CreatedDate;
                    }
                    returnValue = context.CreateIndexSettings<VideoPlaylistSearch>("name", "author");
                    returnValue = await context.CreateManyAsync(listVideoPlaylist);
                    if (returnValue)
                    {
                        returnValue = context.CreateIndexSettings<ItemStreamDistribution>("title", "author");
                        returnValue = await context.CreateManyAsync(new List<ItemStreamDistribution>() { itemStreamDistribution });
                        //returnValue = await context.UpdateAsync<Account>(account.Id, account);
                    }
                    foreach (var item in listVideoPlaylist)
                    {
                        await context.UpdateAsync<NewsAllSearch>(item.Id, new NewsAllSearch()
                        {
                            Id = item.Id,
                            Status = item.Status,
                            Type = item.Type,
                            Keyword = item.Name,
                            CardType = item.CardType,
                            CreatedBy = item.CreatedBy,
                            CreatedDate = item.CreatedDate,
                            DistributorId = item.DistributorId,
                            Avatar = item.MetaAvatar,
                            NewsDistribution = item.NewsDistribution
                        });
                    }
                }
            }
            catch (Exception ex)
            {
                if(!isQueue) Logger.Error(ex, ex.Message);
            }
            return returnValue;
        }

        public async Task<bool> SharePostAsync(ItemStreamDistribution itemStreamDistribution, List<PostSearch> listPost, bool isUpdate = false, bool isQueue = false)
        {
            var returnValue = false;
            try
            {
                var context = GetContext();
                {
                    if (isUpdate)
                    {
                        var itemStreamDb = await context.GetAsync<ItemStreamDistribution>(itemStreamDistribution.Id);
                        itemStreamDistribution.CreatedDate = itemStreamDb?.CreatedDate;
                        itemStreamDistribution.ModifiedDate = DateTime.SpecifyKind(DateTime.Now, DateTimeKind.Unspecified);
                    }
                    returnValue = context.CreateIndexSettings<PostSearch>("name", "author");
                    returnValue = await context.CreateManyAsync(listPost);
                    if (returnValue)
                    {
                        returnValue = context.CreateIndexSettings<ItemStreamDistribution>("title", "author");
                        returnValue = await context.CreateManyAsync(new List<ItemStreamDistribution>() { itemStreamDistribution });
                        //returnValue = await context.UpdateAsync<Account>(account.Id, account);
                    }
                    foreach (var item in listPost)
                    {
                        await context.UpdateAsync<NewsAllSearch>(item.Id, new NewsAllSearch()
                        {
                            Id = item.Id,
                            Status = item.Status,
                            Type = item.Type,
                            Keyword = item.Caption,
                            CardType = item.CardType,
                            CreatedBy = item.CreatedBy,
                            CreatedDate = item.CreatedDate,
                            DistributorId = item.DistributorId,
                            NewsDistribution = item.NewsDistribution
                        });
                    }
                }
            }
            catch (Exception ex)
            {
                if(!isQueue) Logger.Error(ex, ex.Message);
            }
            return returnValue;
        }
        public async Task<bool> ShareShareLinkAsync(ItemStreamDistribution itemStreamDistribution, List<ShareLinkSearch> listPost, bool isUpdate = false, bool isQueue = false)
        {
            var returnValue = false;
            try
            {
                var context = GetContext();
                {
                    if (isUpdate)
                    {
                        var itemStreamDb = await context.GetAsync<ItemStreamDistribution>(itemStreamDistribution.Id);
                        itemStreamDistribution.CreatedDate = itemStreamDb?.CreatedDate;
                        itemStreamDistribution.ModifiedDate = DateTime.SpecifyKind(DateTime.Now, DateTimeKind.Unspecified);
                    }
                    returnValue = context.CreateIndexSettings<ShareLinkSearch>("title", "author");
                    returnValue = await context.CreateManyAsync(listPost);
                    if (returnValue)
                    {
                        returnValue = context.CreateIndexSettings<ItemStreamDistribution>("title", "author");
                        returnValue = await context.CreateManyAsync(new List<ItemStreamDistribution>() { itemStreamDistribution });
                        //returnValue = await context.UpdateAsync<Account>(account.Id, account);
                    }
                    foreach (var item in listPost)
                    {
                        await context.UpdateAsync<NewsAllSearch>(item.Id, new NewsAllSearch()
                        {
                            Id = item.Id,
                            Status = item.Status,
                            Type = item.Type,
                            Keyword = item.Title,
                            CardType = item.CardType,
                            CreatedBy = item.CreatedBy,
                            CreatedDate = item.CreatedDate,
                            DistributorId = item.DistributorId,
                            NewsDistribution = item.NewsDistribution
                        });
                    }
                }
            }
            catch (Exception ex)
            {
                if(!isQueue) Logger.Error(ex, ex.Message);
            }
            return returnValue;
        }

        public async Task<bool> ShareBeamAsync(ItemStreamDistribution itemStreamDistribution, List<BeamSearch> listBeam, bool isUpdate = false, bool isQueue = false)
        {
            var returnValue = false;
            try
            {
                var context = GetContext();
                {
                    if (isUpdate)
                    {
                        var itemStreamDb = await context.GetAsync<ItemStreamDistribution>(itemStreamDistribution.Id);
                        itemStreamDistribution.CreatedDate = itemStreamDb?.CreatedDate;
                    }
                    returnValue = context.CreateIndexSettings<BeamSearch>("name", "author");
                    returnValue = await context.CreateManyAsync(listBeam);
                    if (returnValue)
                    {
                        returnValue = context.CreateIndexSettings<ItemStreamDistribution>("title", "author");
                        returnValue = await context.CreateManyAsync(new List<ItemStreamDistribution>() { itemStreamDistribution });
                        //returnValue = await context.UpdateAsync<Account>(account.Id, account);
                    }
                    foreach (var item in listBeam)
                    {
                        await context.UpdateAsync<NewsAllSearch>(item.Id, new NewsAllSearch()
                        {
                            Id = item.Id,
                            Status = item.Status,
                            Type = item.Type,
                            Keyword = item.Name,
                            CardType = item.CardType,
                            CreatedBy = item.CreatedBy,
                            CreatedDate = item.CreatedDate,
                            DistributorId = item.DistributorId,
                            Avatar = item.MetaAvatar,
                            NewsDistribution = item.NewsDistribution
                        });
                    }
                }
            }
            catch (Exception ex)
            {
                if(!isQueue) Logger.Error(ex, ex.Message);
            }
            return returnValue;
        }
    }
}
