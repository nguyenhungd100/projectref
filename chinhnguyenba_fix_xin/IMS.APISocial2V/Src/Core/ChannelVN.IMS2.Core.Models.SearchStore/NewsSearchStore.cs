﻿using ChannelVN.IMS2.Core.Entities;
using ChannelVN.IMS2.Core.Entities.NewsOnHome;
using ChannelVN.IMS2.Core.Entities.Security;
using ChannelVN.IMS2.Foundation.Common;
using ChannelVN.IMS2.Foundation.Common.Configuration;
using ChannelVN.IMS2.Foundation.Logging;
using Nest;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ChannelVN.IMS2.Core.Models.SearchStore
{
    public class NewsSearchStore : CmsMainSearchStore
    {
        public async Task<PagingDataResult<NewsAllSearch>> SearchAsync(SearchNews search, long accountId, bool isDistribution)
        {
            var data = new PagingDataResult<NewsAllSearch>();
            try
            {
                var context = GetContext();
                {
                    var isOfficialAccount = false;
                    //check account in official
                    if (search.OfficerId > 0 && search.OfficerId != accountId)
                    {
                        var searchAccount = new SearchDescriptor<AccountSearch>();
                        var termTypeAcc = new TermQuery()
                        {
                            Field = "type",
                            Value = 2
                        };
                        var termId = new TermQuery()
                        {
                            Field = "id",
                            Value = search.OfficerId
                        };
                        var termMemberId = new TermQuery()
                        {
                            Field = "accountMember.memberId",
                            Value = accountId
                        };
                        var nestTermMember = new NestedQuery()
                        {
                            Path = "accountMember",
                            Query = termMemberId
                        };
                        searchAccount.Query(q => termTypeAcc && termId && nestTermMember
                           ).From(0).Size(1);

                        var resultCheck = await context.SearchExAsync(searchAccount);
                        if (resultCheck != null && resultCheck.Total > 0)
                        {
                            isOfficialAccount = true;
                        }
                        if (isOfficialAccount == false) return data;
                    }
                    //end check account

                    var searchDescriptor = new SearchDescriptor<NewsAllSearch>();
                    if (null != search.OrderBy)
                    {
                        switch (search.OrderBy)
                        {
                            //case 0:
                            //    searchDescriptor.Sort(s => s.Descending(p => p.DistributionDate));
                            //    break;
                            //case 1:
                            //    searchDescriptor.Sort(s => s.Ascending(p => p.DistributionDate));
                            //    break;
                            case SearchNewsOrderBy.DescCreatedDate:
                                searchDescriptor.Sort(s => s.Descending(p => p.CreatedDate));
                                break;
                            case SearchNewsOrderBy.AscCreatedDate:
                                searchDescriptor.Sort(s => s.Ascending(p => p.CreatedDate));
                                break;
                            //case 4:
                            //    searchDescriptor.Sort(s => s.Descending(p => p.ModifiedDate));
                            //    break;
                            //case 5:
                            //    searchDescriptor.Sort(s => s.Ascending(p => p.ModifiedDate));
                            //    break;
                            default:
                                searchDescriptor.Sort(s => s.Descending(p => p.CreatedDate));
                                break;
                        }
                    }
                    var termCategoryId = new TermQuery();
                    if (search.CategoryId > 0)
                    {
                        termCategoryId.Field = "categoryId";
                        termCategoryId.Value = search.CategoryId;
                    }

                    var nestTerm = new NestedQuery();
                    var termDistributorId = new TermQuery();
                    if (search.DistributionId > 0)
                    {
                        termDistributorId.Field = "newsDistribution.distributorId";
                        termDistributorId.Value = search.DistributionId;
                        nestTerm.Path = "newsDistribution";
                        nestTerm.Query = termDistributorId;
                    }
                    else
                    {
                        if (search.DistributionId == -1)
                        {
                            var exist = new ExistsQuery()
                            {
                                Field = "newsDistribution.distributorId"
                            };
                            nestTerm.Path = "newsDistribution";
                            nestTerm.Query = exist;
                        }
                    }

                    if (isDistribution) search.Status = "2";
                    var listStatus = new List<string>();
                    if (!string.IsNullOrEmpty(search.Status))
                    {
                        var arrStatus = search.Status.Split(",");
                        for (var i = 0; i < arrStatus.Length; i++)
                        {
                            if (int.TryParse(arrStatus[i], out int st))
                            {
                                if (Enum.IsDefined(typeof(NewsStatus), st))
                                {
                                    listStatus.Add(st.ToString());
                                }
                            }
                        }
                    }
                    var termsStatus = new TermsQuery()
                    {
                        Field = "status",
                        Terms = listStatus.ToArray()
                    };
                    var termType = new TermsQuery()
                    {
                        Field = "type"
                    };
                    if (search.Type != null)
                    {
                        termType.Terms = Enum.GetValues(typeof(NewsType)).Cast<NewsType>().Where(p => p == search.Type).Select(p => ((int)p).ToString()).ToArray();
                    }
                    else
                    {
                        termType.Terms = Enum.GetValues(typeof(NewsType)).Cast<NewsType>()
                            .Where(p => p != NewsType.Article).Select(p => ((int)p).ToString()).ToArray();
                    }

                    var termAccountId = new TermQuery();
                    var nestNewsInAccount = new NestedQuery();
                    if (isOfficialAccount)
                    {
                        termAccountId.Field = "newsInAccount.accountId";
                        termAccountId.Value = search.OfficerId;
                    }
                    else
                    {
                        termAccountId.Field = "newsInAccount.accountId";
                        termAccountId.Value = accountId;
                    }
                    nestNewsInAccount.Path = "newsInAccount";
                    nestNewsInAccount.Query = termAccountId;
                    var termPublishMode = new TermQuery();
                    if (search.PublishMode > 0)
                    {
                        termPublishMode.Field = "publishMode";
                        termPublishMode.Value = search.PublishMode;
                    }
                    string fieldDate = "createdDate";
                    var termCreatedBy = new TermQuery();
                    if (search.OfficerId != accountId)
                    {
                        termCreatedBy.Field = "createdBy";
                        termCreatedBy.Value = accountId.ToString();
                    };

                    var range = new DateRangeQuery
                    {
                        Field = fieldDate,
                        //TimeZone = "+07:00",
                        Format = AppSettings.Current.ChannelConfiguration.DateFormat
                    };
                    if (search.FromDate != null && search.FromDate != DateTime.MinValue && search.ToDate != null && search.ToDate != DateTime.MinValue)
                    {

                        range.GreaterThanOrEqualTo = search.FromDate.Value.ToString(AppSettings.Current.ChannelConfiguration.DateFormat);
                        range.LessThanOrEqualTo = search.ToDate.Value.ToString(AppSettings.Current.ChannelConfiguration.DateFormat);
                    }
                    else
                    {
                        if (search.FromDate != null && search.FromDate != DateTime.MinValue)
                        {
                            range.GreaterThanOrEqualTo = search.FromDate.Value.ToString(AppSettings.Current.ChannelConfiguration.DateFormat);
                        }
                        else if (search.ToDate != null && search.ToDate != DateTime.MinValue)
                        {
                            range.LessThanOrEqualTo = search.ToDate.Value.ToString(AppSettings.Current.ChannelConfiguration.DateFormat);
                        }
                    }

                    var keyword = EscapeQueryBase.EscapeSearchQuery(search.Keyword);
                    searchDescriptor.Query(q => q
                    .QueryString(mp => mp
                        .Query(keyword)
                        .Fields(f => f.Fields(new string[] { "keyword", "keyword.folded" }))
                        .MinimumShouldMatch(MinimumShouldMatch.Percentage(100))
                        .DefaultOperator(Operator.And)
                    ) && termCategoryId && termPublishMode && termsStatus && q.DateRange(c => range) && nestTerm && termCreatedBy && nestNewsInAccount && termType
                    ).From((search.PageIndex - 1) * search.PageSize).Size(search.PageSize);

                    var result = await context.SearchExAsync(searchDescriptor);
                    if (result != null)
                    {
                        data.Total = (int)result.Total;
                        data.Data = result.Data.ToList();
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
            }
            return data;
        }

        public async Task<PagingDataResult<NewsAllSearch>> NewsOnPageAsync(SearchNews search, long accountId)
        {
            var data = new PagingDataResult<NewsAllSearch>();
            try
            {
                var context = GetContext();
                {
                    //var isOfficialAccount = false;
                    ////check account in official
                    //if (search.OfficerId > 0 && search.OfficerId != accountId)
                    //{
                    //    var searchAccount = new SearchDescriptor<AccountSearch>();
                    //    var termTypeAcc = new TermQuery()
                    //    {
                    //        Field = "type",
                    //        Value = 2
                    //    };
                    //    var termId = new TermQuery()
                    //    {
                    //        Field = "id",
                    //        Value = search.OfficerId
                    //    };
                    //    var termMemberId = new TermQuery()
                    //    {
                    //        Field = "accountMember.memberId",
                    //        Value = accountId
                    //    };
                    //    var nestTermMember = new NestedQuery()
                    //    {
                    //        Path = "accountMember",
                    //        Query = termMemberId
                    //    };
                    //    searchAccount.Query(q => termTypeAcc && termId && nestTermMember
                    //       ).From(0).Size(1);

                    //    var resultCheck = await context.SearchExAsync(searchAccount);
                    //    if (resultCheck != null && resultCheck.Total > 0)
                    //    {
                    //        isOfficialAccount = true;
                    //    }
                    //    if (isOfficialAccount == false) return data;
                    //}
                    ////end check account

                    var searchDescriptor = new SearchDescriptor<NewsAllSearch>();
                    if (null != search.OrderBy)
                    {
                        switch (search.OrderBy)
                        {
                            //case 0:
                            //    searchDescriptor.Sort(s => s.Descending(p => p.DistributionDate));
                            //    break;
                            //case 1:
                            //    searchDescriptor.Sort(s => s.Ascending(p => p.DistributionDate));
                            //    break;
                            case SearchNewsOrderBy.DescCreatedDate:
                                searchDescriptor.Sort(s => s.Descending(p => p.CreatedDate));
                                break;
                            case SearchNewsOrderBy.AscCreatedDate:
                                searchDescriptor.Sort(s => s.Ascending(p => p.CreatedDate));
                                break;
                            //case 4:
                            //    searchDescriptor.Sort(s => s.Descending(p => p.ModifiedDate));
                            //    break;
                            //case 5:
                            //    searchDescriptor.Sort(s => s.Ascending(p => p.ModifiedDate));
                            //    break;
                            default:
                                searchDescriptor.Sort(s => s.Descending(p => p.CreatedDate));
                                break;
                        }
                    }
                    var termCategoryId = new TermQuery();
                    if (search.CategoryId > 0)
                    {
                        termCategoryId.Field = "categoryId";
                        termCategoryId.Value = search.CategoryId;
                    }

                    var nestTerm = new NestedQuery();
                    var termDistributorId = new TermQuery();
                    if (search.DistributionId > 0)
                    {
                        termDistributorId.Field = "newsDistribution.distributorId";
                        termDistributorId.Value = search.DistributionId;
                        nestTerm.Path = "newsDistribution";
                        nestTerm.Query = termDistributorId;
                    }
                    else
                    {
                        if (search.DistributionId == -1)
                        {
                            var exist = new ExistsQuery()
                            {
                                Field = "newsDistribution.distributorId"
                            };
                            nestTerm.Path = "newsDistribution";
                            nestTerm.Query = exist;
                        }
                    }
                    
                    var termStatus = new TermQuery()
                    {
                        Field = "status",
                        Value = search.Status
                    };
                    var termType = new TermsQuery()
                    {
                        Field = "type"
                    };
                    if (search.Type != null)
                    {
                        termType.Terms = Enum.GetValues(typeof(NewsType)).Cast<NewsType>().Where(p => p == search.Type).Select(p => ((int)p).ToString()).ToArray();
                    }
                    else
                    {
                        termType.Terms = Enum.GetValues(typeof(NewsType)).Cast<NewsType>()
                            .Where(p => p != NewsType.Article).Select(p => ((int)p).ToString()).ToArray();
                    }

                    var nestNewsInAccount = new NestedQuery();

                    var termAccountId = new TermQuery();
                    termAccountId.Field = "newsInAccount.accountId";
                    termAccountId.Value = search.OfficerId;

                    nestNewsInAccount.Path = "newsInAccount";
                    nestNewsInAccount.Query = termAccountId;
                    var termPublishMode = new TermQuery();
                    if (search.PublishMode > 0)
                    {
                        termPublishMode.Field = "publishMode";
                        termPublishMode.Value = search.PublishMode;
                    }
                    string fieldDate = "createdDate";
                    
                    var range = new DateRangeQuery
                    {
                        Field = fieldDate,
                        //TimeZone = "+07:00",
                        Format = AppSettings.Current.ChannelConfiguration.DateFormat
                    };
                    if (search.FromDate != null && search.FromDate != DateTime.MinValue && search.ToDate != null && search.ToDate != DateTime.MinValue)
                    {

                        range.GreaterThanOrEqualTo = search.FromDate.Value.ToString(AppSettings.Current.ChannelConfiguration.DateFormat);
                        range.LessThanOrEqualTo = search.ToDate.Value.ToString(AppSettings.Current.ChannelConfiguration.DateFormat);
                    }
                    else
                    {
                        if (search.FromDate != null && search.FromDate != DateTime.MinValue)
                        {
                            range.GreaterThanOrEqualTo = search.FromDate.Value.ToString(AppSettings.Current.ChannelConfiguration.DateFormat);
                        }
                        else if (search.ToDate != null && search.ToDate != DateTime.MinValue)
                        {
                            range.LessThanOrEqualTo = search.ToDate.Value.ToString(AppSettings.Current.ChannelConfiguration.DateFormat);
                        }
                    }

                    var keyword = EscapeQueryBase.EscapeSearchQuery(search.Keyword);
                    searchDescriptor.Query(q => q
                    .QueryString(mp => mp
                        .Query(keyword)
                        .Fields(f => f.Fields(new string[] { "keyword", "keyword.folded" }))
                        .MinimumShouldMatch(MinimumShouldMatch.Percentage(100))
                        .DefaultOperator(Operator.And)
                    ) && termCategoryId && termPublishMode && termStatus && q.DateRange(c => range) && nestTerm && nestNewsInAccount && termType
                    ).From((search.PageIndex - 1) * search.PageSize).Size(search.PageSize);

                    var result = await context.SearchExAsync(searchDescriptor);
                    if (result != null)
                    {
                        data.Total = (int)result.Total;
                        data.Data = result.Data.ToList();
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
            }
            return data;
        }
        
        public async Task<PagingDataResult<NewsAllSearch>> SearchNewsAsync(SearchNews search, long userId)
        {
            var data = new PagingDataResult<NewsAllSearch>();
            try
            {
                var context = GetContext();
                {
                    var isOfficialAccount = false;
                    //check account in official
                    if (search.OfficerId > 0 && search.OfficerId != userId)
                    {
                        var searchAccount = new SearchDescriptor<AccountSearch>();
                        var termType = new TermQuery()
                        {
                            Field = "type",
                            Value = 2
                        };
                        var termId = new TermQuery()
                        {
                            Field = "id",
                            Value = search.OfficerId
                        };
                        var termMemberId = new TermQuery()
                        {
                            Field = "accountMember.memberId",
                            Value = userId
                        };
                        var nestTermMember = new NestedQuery()
                        {
                            Path = "accountMember",
                            Query = termMemberId
                        };
                        searchAccount.Query(q => termType && termId && nestTermMember
                           ).From(0).Size(1);

                        var resultCheck = await context.SearchExAsync(searchAccount);
                        if (resultCheck != null && resultCheck.Total > 0)
                        {
                            isOfficialAccount = true;
                        }
                        if (isOfficialAccount == false) return data;
                    }
                    //end check account

                    var searchDescriptor = new SearchDescriptor<NewsAllSearch>();
                    if (null != search.OrderBy)
                    {
                        switch (search.OrderBy)
                        {
                            case SearchNewsOrderBy.DescCreatedDate:
                                searchDescriptor.Sort(s => s.Descending(p => p.CreatedDate));
                                break;
                            case SearchNewsOrderBy.AscCreatedDate:
                                searchDescriptor.Sort(s => s.Ascending(p => p.CreatedDate));
                                break;
                            default:
                                searchDescriptor.Sort(s => s.Descending(p => p.CreatedDate));
                                break;
                        }
                    }
                    var termCategoryId = new TermQuery();
                    if (search.CategoryId > 0)
                    {
                        termCategoryId.Field = "categoryId";
                        termCategoryId.Value = search.CategoryId;
                    }

                    var nestTerm = new NestedQuery();
                    var termDistributorId = new TermQuery();
                    if (search.DistributionId > 0)
                    {
                        termDistributorId.Field = "newsDistribution.distributorId";
                        termDistributorId.Value = search.DistributionId;
                        nestTerm.Path = "newsDistribution";
                        nestTerm.Query = termDistributorId;
                    }



                    var listStatus = new List<string>();
                    if (!string.IsNullOrEmpty(search.Status))
                    {
                        var arrStatus = search.Status.Split(",");
                        for (var i = 0; i < arrStatus.Length; i++)
                        {
                            if (int.TryParse(arrStatus[i], out int st))
                            {
                                if (Enum.IsDefined(typeof(NewsStatus), st))
                                {
                                    listStatus.Add(st.ToString());
                                }
                            }
                        }
                    }
                    var termsStatus = new TermsQuery()
                    {
                        Field = "status",
                        Terms = listStatus.ToArray()
                    };

                    var listType = new List<int> { (int)NewsType.Article, (int)NewsType.ShareLink };
                    var termsType = new TermsQuery()
                    {
                        Field = "type",
                        Terms = listType.Select(p => p.ToString()).ToArray()
                    };

                    var termPublishMode = new TermQuery();
                    if (search.PublishMode > 0)
                    {
                        termPublishMode.Field = "publishMode";
                        termPublishMode.Value = search.PublishMode;
                    }
                    string fieldDate = "createdDate";
                    switch (search.SearchDateBy)
                    {
                        case 1:
                            fieldDate = "createdDate";
                            break;
                    }

                    var termNewsInAccount = new TermQuery();
                    if (isOfficialAccount)
                    {
                        termNewsInAccount.Field = "newsInAccount.accountId";
                        termNewsInAccount.Value = search.OfficerId;
                    }
                    else
                    {
                        termNewsInAccount.Field = "newsInAccount.accountId";
                        termNewsInAccount.Value = userId;
                    }
                    var nestTermNewsInAccount = new NestedQuery()
                    {
                        Path = "newsInAccount",
                        Query = termNewsInAccount
                    };

                    var range = new DateRangeQuery
                    {
                        Field = fieldDate,
                        //TimeZone = "+07:00",
                        Format = AppSettings.Current.ChannelConfiguration.DateFormat
                    };
                    if (search.FromDate != null && search.FromDate != DateTime.MinValue && search.ToDate != null && search.ToDate != DateTime.MinValue)
                    {

                        range.GreaterThanOrEqualTo = search.FromDate.Value.ToString(AppSettings.Current.ChannelConfiguration.DateFormat);
                        range.LessThanOrEqualTo = search.ToDate.Value.ToString(AppSettings.Current.ChannelConfiguration.DateFormat);
                    }
                    else
                    {
                        if (search.FromDate != null && search.FromDate != DateTime.MinValue)
                        {
                            range.GreaterThanOrEqualTo = search.FromDate.Value.ToString(AppSettings.Current.ChannelConfiguration.DateFormat);
                        }
                        else if (search.ToDate != null && search.ToDate != DateTime.MinValue)
                        {
                            range.LessThanOrEqualTo = search.ToDate.Value.ToString(AppSettings.Current.ChannelConfiguration.DateFormat);
                        }
                    }

                    var keyword = EscapeQueryBase.EscapeSearchQuery(search.Keyword);
                    searchDescriptor.Query(q => q
                    .QueryString(mp => mp
                        .Query(keyword)
                        .Fields(f => f.Fields(new string[] { "keyword", "keyword.folded" }))
                        .MinimumShouldMatch(MinimumShouldMatch.Percentage(100))
                        .DefaultOperator(Operator.And)
                    ) && q.QueryString(mp => mp
                        .Query(search.Author)
                        .Fields(f => f.Fields(new string[] { "author", "author.folded" }))
                        .MinimumShouldMatch(MinimumShouldMatch.Percentage(100))
                        .DefaultOperator(Operator.And)
                    ) && termCategoryId && termPublishMode && termsStatus && q.DateRange(c => range) && nestTerm && termsType && nestTermNewsInAccount
                    ).Source(s => s.Includes(i => i.Field("id"))).From((search.PageIndex - 1) * search.PageSize).Size(search.PageSize);

                    var result = await context.SearchExAsync(searchDescriptor);

                    if (result != null)
                    {
                        data.Total = (int)result.Total;
                        data.Data = result.Data.ToList();
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
            }
            return data;
        }

        public async Task<PagingDataResult<NewsAllSearch>> SearchCheckPostAsync(SearchNews search)
        {
            var data = new PagingDataResult<NewsAllSearch>();
            try
            {
                var context = GetContext();
                {
                    var searchDescriptor = new SearchDescriptor<NewsAllSearch>();
                    //if (null != search.OrderBy)
                    //{
                    //    switch (search.OrderBy)
                    //    {
                    //        //case 0:
                    //        //    searchDescriptor.Sort(s => s.Descending(p => p.DistributionDate));
                    //        //    break;
                    //        //case 1:
                    //        //    searchDescriptor.Sort(s => s.Ascending(p => p.DistributionDate));
                    //        //    break;
                    //        case SearchNewsOrderBy.DescCreatedDate:
                    //            searchDescriptor.Sort(s => s.Descending(p => p.CreatedDate));
                    //            break;
                    //        case SearchNewsOrderBy.AscCreatedDate:
                    //            searchDescriptor.Sort(s => s.Ascending(p => p.CreatedDate));
                    //            break;
                    //        //case 4:
                    //        //    searchDescriptor.Sort(s => s.Descending(p => p.ModifiedDate));
                    //        //    break;
                    //        //case 5:
                    //        //    searchDescriptor.Sort(s => s.Ascending(p => p.ModifiedDate));
                    //        //    break;
                    //        default:
                    //            searchDescriptor.Sort(s => s.Descending(p => p.CreatedDate));
                    //            break;
                    //    }
                    //}
                    var termId = new TermQuery();
                    if (search.Id > 0)
                    {
                        termId.Field = "id";
                        termId.Value = search.Id;
                    }

                    //var termCategoryId = new TermQuery();
                    //if (search.CategoryId > 0)
                    //{
                    //    termCategoryId.Field = "categoryId";
                    //    termCategoryId.Value = search.CategoryId;
                    //}

                    //var nestTerm = new NestedQuery();
                    //var termDistributorId = new TermQuery();
                    //if (search.DistributionId > 0)
                    //{
                    //    termDistributorId.Field = "newsDistribution.distributorId";
                    //    termDistributorId.Value = search.DistributionId;
                    //    nestTerm.Path = "newsDistribution";
                    //    nestTerm.Query = termDistributorId;
                    //}
                    //else
                    //{
                    //    if (search.DistributionId == -1)
                    //    {
                    //        var exist = new ExistsQuery()
                    //        {
                    //            Field = "newsDistribution.distributorId"
                    //        };
                    //        nestTerm.Path = "newsDistribution";
                    //        nestTerm.Query = exist;
                    //    }
                    //}

                    
                    //var listStatus = new List<string>();
                    //if (!string.IsNullOrEmpty(search.Status))
                    //{
                    //    var arrStatus = search.Status.Split(",");
                    //    for (var i = 0; i < arrStatus.Length; i++)
                    //    {
                    //        if (int.TryParse(arrStatus[i], out int st))
                    //        {
                    //            if (Enum.IsDefined(typeof(NewsStatus), st))
                    //            {
                    //                listStatus.Add(st.ToString());
                    //            }
                    //        }
                    //    }
                    //}
                    //var termsStatus = new TermsQuery()
                    //{
                    //    Field = "status",
                    //    Terms = listStatus.ToArray()
                    //};
                    //var termType = new TermsQuery()
                    //{
                    //    Field = "type"
                    //};
                    //if (search.Type != null)
                    //{
                    //    termType.Terms = Enum.GetValues(typeof(NewsType)).Cast<NewsType>().Where(p => p == search.Type).Select(p => ((int)p).ToString()).ToArray();
                    //}
                    //else
                    //{
                    //    termType.Terms = Enum.GetValues(typeof(NewsType)).Cast<NewsType>()
                    //        .Where(p => p != NewsType.Article).Select(p => ((int)p).ToString()).ToArray();
                    //}

                    //var termAccountId = new TermQuery();
                    //var nestNewsInAccount = new NestedQuery();
                    
                    //nestNewsInAccount.Path = "newsInAccount";
                    //nestNewsInAccount.Query = termAccountId;
                    //var termPublishMode = new TermQuery();
                    //if (search.PublishMode > 0)
                    //{
                    //    termPublishMode.Field = "publishMode";
                    //    termPublishMode.Value = search.PublishMode;
                    //}
                    //string fieldDate = "createdDate";
                    //var termCreatedBy = new TermQuery();
                   

                    //var range = new DateRangeQuery
                    //{
                    //    Field = fieldDate,
                    //    //TimeZone = "+07:00",
                    //    Format = AppSettings.Current.ChannelConfiguration.DateFormat
                    //};
                    //if (search.FromDate != null && search.FromDate != DateTime.MinValue && search.ToDate != null && search.ToDate != DateTime.MinValue)
                    //{

                    //    range.GreaterThanOrEqualTo = search.FromDate.Value.ToString(AppSettings.Current.ChannelConfiguration.DateFormat);
                    //    range.LessThanOrEqualTo = search.ToDate.Value.ToString(AppSettings.Current.ChannelConfiguration.DateFormat);
                    //}
                    //else
                    //{
                    //    if (search.FromDate != null && search.FromDate != DateTime.MinValue)
                    //    {
                    //        range.GreaterThanOrEqualTo = search.FromDate.Value.ToString(AppSettings.Current.ChannelConfiguration.DateFormat);
                    //    }
                    //    else if (search.ToDate != null && search.ToDate != DateTime.MinValue)
                    //    {
                    //        range.LessThanOrEqualTo = search.ToDate.Value.ToString(AppSettings.Current.ChannelConfiguration.DateFormat);
                    //    }
                    //}

                    var keyword = EscapeQueryBase.EscapeSearchQuery(search.Keyword);
                    searchDescriptor.Query(q => q
                    .QueryString(mp => mp
                        .Query(keyword)
                        .Fields(f => f.Fields(new string[] { "keyword", "keyword.folded" }))
                        .MinimumShouldMatch(MinimumShouldMatch.Percentage(100))
                        .DefaultOperator(Operator.And)
                    ) && termId
                    ).From((search.PageIndex - 1) * search.PageSize).Size(search.PageSize);

                    var result = await context.SearchExAsync(searchDescriptor);
                    if (result != null)
                    {
                        data.Total = (int)result.Total;
                        data.Data = result.Data.ToList();
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
            }
            return data;
        }
    }
}
