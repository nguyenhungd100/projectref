﻿using ChannelVN.IMS2.Core.Entities;
using ChannelVN.IMS2.Core.Entities.Media;
using ChannelVN.IMS2.Core.Entities.Security;
using ChannelVN.IMS2.Foundation.Common;
using ChannelVN.IMS2.Foundation.Common.Configuration;
using ChannelVN.IMS2.Foundation.Logging;
using Nest;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace ChannelVN.IMS2.Core.Models.SearchStore.Media
{
    public class MediaUnitSearchStore : CmsMainSearchStore
    {
        public async Task<bool> AddAsync(MediaUnitSearch data, bool isQueue = false)
        {
            var returnValue = false;
            try
            {
                var context = GetContext();
                {
                    var fieldNameAnalyzer = "caption";
                    returnValue = await context.CreateIndexSettingsAsync<MediaUnitSearch>(fieldNameAnalyzer);
                    returnValue = await context.CreateAsync(data);

                    returnValue = await context.CreateIndexSettingsAsync<NewsAllSearch>("keyword");
                    returnValue = await context.CreateAsync(new NewsAllSearch()
                    {
                        Id = data.Id,
                        Status = data.Status,
                        Type = data.Type,
                        Keyword = data.Caption,
                        CardType = data.CardType,
                        CreatedBy = data.CreatedBy,
                        CreatedDate = data.CreatedDate,
                        DistributorId = data.DistributorId,
                        NewsInAccount = data.NewsInAccount,
                        NewsDistribution = data.NewsDistribution
                    });
                }
            }
            catch (Exception ex)
            {
                if (!isQueue) Logger.Error(ex, ex.Message);
            }
            return returnValue;
        }

        public async Task<bool> UpdateAsync(MediaUnitSearch data, bool isQueue = false)
        {
            var returnValue = false;
            try
            {
                var context = GetContext();
                {
                    returnValue = await context.UpdateAsync<MediaUnitSearch>(data.Id, data);
                    var objAll = await context.GetAsync<NewsAllSearch>(data.Id);
                    if (objAll != null)
                    {
                        objAll.Keyword = data.Caption;

                        returnValue = await context.UpdateAsync<NewsAllSearch>(data.Id, objAll);
                    }
                }
            }
            catch (Exception ex)
            {
                if (!isQueue) Logger.Error(ex, ex.Message);
            }
            return returnValue;
        }

        public async Task<bool> UpdateStatusAsync(MediaUnitSearch data, bool isQueue = false)
        {
            var returnValue = false;
            try
            {
                var context = GetContext();
                {
                    var obj = await context.GetAsync<MediaUnitSearch>(data.Id);
                    if (obj != null)
                    {
                        obj.Status = data.Status;
                        obj.ModifiedBy = data.ModifiedBy;
                        obj.ModifiedDate = data.ModifiedDate;
                        obj.DistributionDate = data.DistributionDate;

                        returnValue = await context.UpdateAsync<MediaUnitSearch>(obj.Id, obj);
                    }

                    var objAll = await context.GetAsync<NewsAllSearch>(data.Id);
                    if (objAll != null)
                    {
                        objAll.Status = data.Status;

                        returnValue = await context.UpdateAsync<NewsAllSearch>(data.Id, objAll);
                    }
                }
            }
            catch (Exception ex)
            {
                if (!isQueue) Logger.Error(ex, ex.Message);
            }
            return returnValue;
        }

        public async Task<PagingDataResult<MediaUnitSearch>> SearchAsync(SearchNews search, long accountId)
        {
            var data = new PagingDataResult<MediaUnitSearch>();
            try
            {
                var context = GetContext();
                {
                    var isOfficialAccount = false;
                    //check account in official
                    if (search.OfficerId > 0 && search.OfficerId != accountId)
                    {
                        var searchAccount = new SearchDescriptor<AccountSearch>();
                        var termType = new TermQuery()
                        {
                            Field = "type",
                            Value = 2
                        };
                        var termId = new TermQuery()
                        {
                            Field = "id",
                            Value = search.OfficerId
                        };
                        var termMemberId = new TermQuery()
                        {
                            Field = "accountMember.memberId",
                            Value = accountId
                        };
                        var nestTermMember = new NestedQuery()
                        {
                            Path = "accountMember",
                            Query = termMemberId
                        };
                        searchAccount.Query(q => termType && termId && nestTermMember
                           ).From(0).Size(1);

                        var resultCheck = await context.SearchExAsync(searchAccount);
                        if (resultCheck != null && resultCheck.Total > 0)
                        {
                            isOfficialAccount = true;
                        }
                        if (isOfficialAccount == false) return data;
                    }
                    //end check account

                    var searchDescriptor = new SearchDescriptor<MediaUnitSearch>();

                    switch (search.OrderBy)
                    {
                        case SearchNewsOrderBy.DescDistributionDate:
                            searchDescriptor.Sort(s => s.Descending(p => p.DistributionDate));
                            break;
                        case SearchNewsOrderBy.AscDistributionDate:
                            searchDescriptor.Sort(s => s.Ascending(p => p.DistributionDate));
                            break;
                        case SearchNewsOrderBy.DescCreatedDate:
                            searchDescriptor.Sort(s => s.Descending(p => p.CreatedDate));
                            break;
                        case SearchNewsOrderBy.AscCreatedDate:
                            searchDescriptor.Sort(s => s.Ascending(p => p.CreatedDate));
                            break;
                        case SearchNewsOrderBy.DescModifiedDate:
                            searchDescriptor.Sort(s => s.Descending(p => p.ModifiedDate));
                            break;
                        case SearchNewsOrderBy.AscModifiedDate:
                            searchDescriptor.Sort(s => s.Ascending(p => p.ModifiedDate));
                            break;
                        default:
                            searchDescriptor.Sort(s => s.Descending(p => p.CreatedDate));
                            break;
                    }

                    var fields = new[] { "caption", "caption.folded" };

                    var termsStatus = new TermsQuery();
                    if (!string.IsNullOrEmpty(search.Status))
                    {
                        termsStatus.Field = "status";
                        termsStatus.Terms = search.Status.Split(",");
                    }

                    var termCreatedBy = new TermQuery();
                    if (search.OfficerId != accountId)
                    {
                        termCreatedBy.Field = "createdBy";
                        termCreatedBy.Value = accountId;
                    };
                    var termAccountId = new TermQuery();
                    var nestNewsInAccount = new NestedQuery();
                    if (isOfficialAccount)
                    {
                        termAccountId.Field = "newsInAccount.accountId";
                        termAccountId.Value = search.OfficerId;

                    }
                    else
                    {
                        termAccountId.Field = "newsInAccount.accountId";
                        termAccountId.Value = accountId;
                    }
                    nestNewsInAccount.Path = "newsInAccount";
                    nestNewsInAccount.Query = termAccountId;
                    string fieldDate = "createdDate";
                    switch (search.SearchDateBy)
                    {
                        case 1:
                            fieldDate = "createdDate";
                            break;
                        case 2:
                            fieldDate = "distributionDate";
                            break;
                        case 3:
                            fieldDate = "modifiedDate";
                            break;
                        default:
                            fieldDate = "createdDate";
                            break;
                    }

                    var range = new DateRangeQuery
                    {
                        Field = fieldDate,
                        //TimeZone = "+07:00",
                        Format = AppSettings.Current.ChannelConfiguration.DateFormat
                    };
                    if (search.FromDate != null && search.FromDate != DateTime.MinValue && search.ToDate != null && search.ToDate != DateTime.MinValue)
                    {

                        range.GreaterThanOrEqualTo = search.FromDate.Value.ToString(AppSettings.Current.ChannelConfiguration.DateFormat);
                        range.LessThanOrEqualTo = search.ToDate.Value.ToString(AppSettings.Current.ChannelConfiguration.DateFormat);
                    }
                    else
                    {
                        if (search.FromDate != null && search.FromDate != DateTime.MinValue)
                        {
                            range.GreaterThanOrEqualTo = search.FromDate.Value.ToString(AppSettings.Current.ChannelConfiguration.DateFormat);
                        }
                        else if (search.ToDate != null && search.ToDate != DateTime.MinValue)
                        {
                            range.LessThanOrEqualTo = search.ToDate.Value.ToString(AppSettings.Current.ChannelConfiguration.DateFormat);
                        }
                    }

                    var keyword = EscapeQueryBase.EscapeSearchQuery(search.Keyword);
                    searchDescriptor.Query(q => q
                    .QueryString(mp => mp
                        .Query(keyword)
                        .Fields(f => f.Fields(new string[] { "caption", "caption.folded" }))
                        .MinimumShouldMatch(MinimumShouldMatch.Percentage(100))
                        .DefaultOperator(Operator.And)
                    ) && termsStatus && nestNewsInAccount && q.DateRange(c => range) && termCreatedBy
                    ).From((search.PageIndex - 1) * search.PageSize).Size(search.PageSize);

                    var result = await context.SearchExAsync(searchDescriptor);

                    if (result != null)
                    {
                        data.Total = (int)result.Total;
                        data.Data = result.Data.ToList();
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
            }
            return data;
        }

        public async Task<PagingDataResult<MediaUnitSearch>> ListDistributionAsync(SearchNews search, long accountId)
        {
            var data = new PagingDataResult<MediaUnitSearch>();
            try
            {
                var context = GetContext();
                {
                    var isOfficialAccount = false;
                    //check account in official
                    if (search.OfficerId > 0 && search.OfficerId != accountId)
                    {
                        var searchAccount = new SearchDescriptor<AccountSearch>();
                        var termType = new TermQuery()
                        {
                            Field = "type",
                            Value = 2
                        };
                        var termId = new TermQuery()
                        {
                            Field = "id",
                            Value = search.OfficerId
                        };
                        var termMemberId = new TermQuery()
                        {
                            Field = "accountMember.memberId",
                            Value = accountId
                        };
                        var nestTermMember = new NestedQuery()
                        {
                            Path = "accountMember",
                            Query = termMemberId
                        };
                        searchAccount.Query(q => termType && termId && nestTermMember
                           ).From(0).Size(1);

                        var resultCheck = await context.SearchExAsync(searchAccount);
                        if (resultCheck != null && resultCheck.Total > 0)
                        {
                            isOfficialAccount = true;
                        }
                        if (isOfficialAccount == false) return data;
                    }
                    //end check account

                    var searchDescriptor = new SearchDescriptor<MediaUnitSearch>();

                    switch (search.OrderBy)
                    {
                        case SearchNewsOrderBy.DescDistributionDate:
                            searchDescriptor.Sort(s => s.Descending(p => p.DistributionDate));
                            break;
                        case SearchNewsOrderBy.AscDistributionDate:
                            searchDescriptor.Sort(s => s.Ascending(p => p.DistributionDate));
                            break;
                        case SearchNewsOrderBy.DescCreatedDate:
                            searchDescriptor.Sort(s => s.Descending(p => p.CreatedDate));
                            break;
                        case SearchNewsOrderBy.AscCreatedDate:
                            searchDescriptor.Sort(s => s.Ascending(p => p.CreatedDate));
                            break;
                        case SearchNewsOrderBy.DescModifiedDate:
                            searchDescriptor.Sort(s => s.Descending(p => p.ModifiedDate));
                            break;
                        case SearchNewsOrderBy.AscModifiedDate:
                            searchDescriptor.Sort(s => s.Ascending(p => p.ModifiedDate));
                            break;
                        default:
                            searchDescriptor.Sort(s => s.Descending(p => p.CreatedDate));
                            break;
                    }

                    var termStatus = new TermQuery()
                    {
                        Field = "status",
                        Value = (int)NewsStatus.Published
                    };


                    var termCreatedBy = new TermQuery();
                    if (search.OfficerId != accountId)
                    {
                        termCreatedBy.Field = "createdBy";
                        termCreatedBy.Value = accountId;
                    };
                    var termAccountId = new TermQuery();
                    var nestNewsInAccount = new NestedQuery();
                    if (isOfficialAccount)
                    {
                        termAccountId.Field = "newsInAccount.accountId";
                        termAccountId.Value = search.OfficerId;
                    }
                    else
                    {
                        termAccountId.Field = "newsInAccount.accountId";
                        termAccountId.Value = accountId;
                    }
                    nestNewsInAccount.Path = "newsInAccount";
                    nestNewsInAccount.Query = termAccountId;
                    var termDistributionId = new TermQuery();
                    if (search.DistributionId > 0)
                    {
                        termDistributionId.Field = "newsDistribution.distributorId";
                        termDistributionId.Value = search.DistributionId.ToString();
                    };

                    var nestDistributionId = new NestedQuery
                    {
                        Path = "newsDistribution",
                        Query = termDistributionId
                    };
                    //them
                    var nestNewsDistribution = new NestedQuery
                    {
                        Path = "newsDistribution"
                    };

                    var exist = new ExistsQuery()
                    {
                        Field = "newsDistribution.distributorId"
                    };
                    nestNewsDistribution.Query = exist;

                    string fieldDate = "createdDate";
                    switch (search.SearchDateBy)
                    {
                        case 1:
                            fieldDate = "createdDate";
                            break;
                        case 2:
                            fieldDate = "distributionDate";
                            break;
                        case 3:
                            fieldDate = "modifiedDate";
                            break;
                        default:
                            fieldDate = "createdDate";
                            break;
                    }

                    var range = new DateRangeQuery
                    {
                        Field = fieldDate,
                        //TimeZone = "+07:00",
                        Format = AppSettings.Current.ChannelConfiguration.DateFormat
                    };
                    if (search.FromDate != null && search.FromDate != DateTime.MinValue && search.ToDate != null && search.ToDate != DateTime.MinValue)
                    {

                        range.GreaterThanOrEqualTo = search.FromDate.Value.ToString(AppSettings.Current.ChannelConfiguration.DateFormat);
                        range.LessThanOrEqualTo = search.ToDate.Value.ToString(AppSettings.Current.ChannelConfiguration.DateFormat);
                    }
                    else
                    {
                        if (search.FromDate != null && search.FromDate != DateTime.MinValue)
                        {
                            range.GreaterThanOrEqualTo = search.FromDate.Value.ToString(AppSettings.Current.ChannelConfiguration.DateFormat);
                        }
                        else if (search.ToDate != null && search.ToDate != DateTime.MinValue)
                        {
                            range.LessThanOrEqualTo = search.ToDate.Value.ToString(AppSettings.Current.ChannelConfiguration.DateFormat);
                        }
                    }

                    var keyword = EscapeQueryBase.EscapeSearchQuery(search.Keyword);
                    searchDescriptor.Query(q => q
                    .QueryString(mp => mp
                        .Query(keyword)
                        .Fields(f => f.Fields(new string[] { "caption", "caption.folded" }))
                        .MinimumShouldMatch(MinimumShouldMatch.Percentage(100))
                        .DefaultOperator(Operator.And)
                    ) && q.DateRange(c => range) && nestNewsInAccount && nestDistributionId && nestNewsDistribution && termStatus && termCreatedBy
                    ).From((search.PageIndex - 1) * search.PageSize).Size(search.PageSize);

                    var result = await context.SearchExAsync(searchDescriptor);

                    if (result != null)
                    {
                        data.Total = (int)result.Total;
                        data.Data = result.Data.ToList();
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
            }
            return data;
        }

        public async Task<MediaUnitSearch> GetByIdAsync(long id)
        {
            var returnValue = default(MediaUnitSearch);
            try
            {
                var context = GetContext();
                {
                    returnValue = await context.GetAsync<MediaUnitSearch>(id);
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
            }
            return returnValue;
        }
    }
}
