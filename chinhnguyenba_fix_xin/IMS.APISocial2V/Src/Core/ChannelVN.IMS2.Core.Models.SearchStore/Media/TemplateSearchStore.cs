﻿using ChannelVN.IMS2.Core.Entities.Media;
using ChannelVN.IMS2.Foundation.Common;
using ChannelVN.IMS2.Foundation.Logging;
using Nest;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace ChannelVN.IMS2.Core.Models.SearchStore.Media
{
    public class TemplateSearchStore : CmsMainSearchStore
    {
        public async Task<bool> AddAsync(Template data, bool isQueue = false)
        {
            var returnValue = false;
            try
            {
                var context = GetContext();
                {
                    var fieldNameAnalyzer = "title";
                    returnValue = await context.CreateIndexSettingsAsync<Template>(fieldNameAnalyzer);
                    returnValue = await context.CreateAsync(data);
                }
            }
            catch (Exception ex)
            {
                if (!isQueue) Logger.Error(ex, ex.Message);
            }
            return returnValue;
        }

        public async Task<bool> UpdateAsync(Template data, bool isQueue = false)
        {
            var returnValue = false;
            try
            {
                var context = GetContext();
                {
                    var obj = await context.GetAsync<Template>(data.Id);
                    if (obj != null)
                    {
                        obj.Title = data.Title;
                        obj.Avatar = data.Avatar;
                        obj.MetaData = data.MetaData;
                        obj.Description = data.Description;
                        obj.Priority = data.Priority;
                        obj.CategoryId = data.CategoryId;
                        obj.ModifiedBy = data.ModifiedBy;
                        obj.ModifiedDate = data.ModifiedDate;

                        returnValue = await context.UpdateAsync<Template>(obj.Id, obj);
                    }
                }
            }
            catch (Exception ex)
            {
                if (!isQueue) Logger.Error(ex, ex.Message);
            }
            return returnValue;
        }

        public async Task<bool> UpdateStatusAsync(Template data, bool isQueue = false)
        {
            var returnValue = false;
            try
            {
                var context = GetContext();
                {
                    if (data != null)
                    {
                        returnValue = await context.UpdateAsync<Template>(data.Id, data);
                    }
                }
            }
            catch (Exception ex)
            {
                if (!isQueue) Logger.Error(ex, ex.Message);
            }
            return returnValue;
        }

        public async Task<PagingDataResult<Template>> SearchAsync(string keyword, int CategoryId, int? status, int pageIndex, int pageSize)
        {
            var data = new PagingDataResult<Template>();
            try
            {
                var context = GetContext();
                {
                    var searchDescriptor = new SearchDescriptor<Template>();
                    var fields = new[] { "title", "title.folded" };

                    var termStatus = new TermQuery();
                    if (status > -1)
                    {
                        termStatus.Field = "status";
                        termStatus.Value = status;
                    }
                    var termCate = new TermQuery()
                    {
                        Field = "categoryId",
                        Value = CategoryId
                    };
                    keyword = EscapeQueryBase.EscapeSearchQuery(keyword);
                    if (string.IsNullOrEmpty(keyword))
                    {
                        searchDescriptor.Query(q => termStatus && termCate
                        ).From((pageIndex - 1) * pageSize).Size(pageSize);
                    }
                    else
                    {
                        searchDescriptor.Query(q => q
                        .QueryString(mp => mp
                            .Query(keyword)
                            .Fields(f => f.Fields(fields))
                            .MinimumShouldMatch(MinimumShouldMatch.Percentage(100))
                            .DefaultOperator(Operator.And)
                        ) && termStatus && termCate
                        ).From((pageIndex - 1) * pageSize).Size(pageSize);
                    }

                    var result = await context.SearchExAsync(searchDescriptor);
                    if (result != null)
                    {
                        data.Total = (int)result.Total;
                        data.Data = result.Data.ToList();
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
            }
            return data;
        }
    }
}
