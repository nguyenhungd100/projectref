﻿using ChannelVN.IMS2.Core.Entities;
using ChannelVN.IMS2.Core.Entities.DashBoard;
using ChannelVN.IMS2.Foundation.Common.Configuration;
using ChannelVN.IMS2.Foundation.Logging;
using Nest;
using System;
using System.Threading.Tasks;

namespace ChannelVN.IMS2.Core.Models.SearchStore.DashBoard
{
    public class DashboardSearch : CmsMainSearchStore
    {
        public async Task<long> GetPostCountAsync(StatisticQuantity statisticEntity)
        {
            try
            {
                var context = GetContext();
                {
                    //thống kê theo account
                    var termNewsInAccount = new TermQuery();
                    if (statisticEntity.AccountId > 0)
                    {
                        termNewsInAccount.Field = "newsInAccount.accountId";
                        termNewsInAccount.Value = statisticEntity.AccountId;
                    }
                    var nestTermNewsInAccount = new NestedQuery()
                    {
                        Path = "newsInAccount",
                        Query = termNewsInAccount
                    };

                    //thống kê theo type
                    var termType = new TermQuery();
                    if (statisticEntity.Type > 0)
                    {
                        termType.Field = "type";
                        termType.Value = statisticEntity.Type;
                    }

                    //thống kê theo trạng thái
                    var termStatus = new TermQuery();
                    if (statisticEntity.Status > 0)
                    {
                        termStatus.Field = "status";
                        termStatus.Value = statisticEntity.Status;
                    }

                    //thống kê theo kênh phân phối
                    var termNewsDistribution = new TermQuery();
                    if (statisticEntity.DistributorId > 0)
                    {
                        termNewsDistribution.Field = "newsDistribution.distributorId";
                        termNewsDistribution.Value = statisticEntity.DistributorId;
                    }
                    var nestTermNewsDistribution = new NestedQuery()
                    {
                        Path = "newsDistribution",
                        Query = termNewsDistribution
                    };

                    //thống kê theo ngày
                    statisticEntity.FromDate = statisticEntity.FromDate.HasValue ? DateTime.SpecifyKind(statisticEntity.FromDate.Value, DateTimeKind.Unspecified) :
                        DateTime.SpecifyKind(DateTime.MinValue, DateTimeKind.Unspecified);
                    statisticEntity.ToDate = statisticEntity.ToDate.HasValue ? DateTime.SpecifyKind(statisticEntity.ToDate.Value, DateTimeKind.Unspecified) :
                        DateTime.SpecifyKind(DateTime.Now, DateTimeKind.Unspecified);
                    statisticEntity.FromDate = statisticEntity.FromDate.Value.AddHours(0).AddMinutes(0).AddSeconds(0);
                    statisticEntity.ToDate = statisticEntity.ToDate.Value.AddHours(23).AddMinutes(59).AddSeconds(59);
                    var fieldDate = "createdDate";
                    var range = new DateRangeQuery
                    {
                        Field = fieldDate,
                        //TimeZone = "+07:00",
                        Format = AppSettings.Current.ChannelConfiguration.DateFormat,
                        GreaterThanOrEqualTo = statisticEntity.FromDate.Value.ToString(AppSettings.Current.ChannelConfiguration.DateFormat),
                        LessThanOrEqualTo = statisticEntity.ToDate.Value.ToString(AppSettings.Current.ChannelConfiguration.DateFormat)
                    };

                    var result = await context.CountAsync<NewsAllSearch>(c => c
                        .Index(context.NameOf(typeof(NewsAllSearch).Name))
                         .Type(typeof(NewsAllSearch).Name.ToLower())
                         .Query(q => range && nestTermNewsInAccount && termType && nestTermNewsDistribution && termStatus)
                    );
                    if (result != null)
                    {
                        return result.Count;
                    }
                    else
                    {
                        return 0;
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
                return 0;
            }
        }
    }
}
