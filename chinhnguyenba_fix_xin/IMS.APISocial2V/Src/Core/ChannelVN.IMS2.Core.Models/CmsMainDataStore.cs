﻿using ChannelVN.IMS2.Foundation.Common.Configuration;
using ChannelVN.IMS2.Foundation.Data;
using ChannelVN.IMS2.Foundation.Data.Providers;

namespace ChannelVN.IMS2.Core.Models
{
    public class CmsMainDataStore : Store<MssqlContext, CmsMainDataStore>
    {
        public CmsMainDataStore()
        {
            Configuration.Add(STO_CONNECTIONSTRING, AppSettings.Current.ChannelConfiguration.ConnectionStrings.CmsMainDb);
            Configuration.Add(STO_NAMESPACE, AppSettings.Current.ServiceConfiguration.Namespace);
        }
    }
}
