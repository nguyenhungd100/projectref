﻿using ChannelVN.IMS2.Core.Entities.DashBoard;
using ChannelVN.IMS2.Core.Models.SearchStore.DashBoard;
using ChannelVN.IMS2.Foundation.Data;
using ChannelVN.IMS2.Foundation.Data.Factories;
using System.Threading.Tasks;

namespace ChannelVN.IMS2.Core.Repositories
{
    public class DashboardRepository : Repository
    {
        public static Task<long> GetPostCountAsync(StatisticQuantity statisticEntity)
        {
            return StoreFactory.Create<DashboardSearch>().GetPostCountAsync(statisticEntity);
        }
    }
}
