﻿using ChannelVN.IMS2.Core.Entities.NewsOnHome;
using ChannelVN.IMS2.Core.Repositories.Constants;
using ChannelVN.IMS2.Foundation.Common;
using ChannelVN.IMS2.Foundation.Logging;
using ChannelVN.IMS2.Foundation.Toolkit.Messaging;
using ChannelVN.IMS2.Foundation.Toolkit.Messaging.ExchangeQueue;
using System;

namespace ChannelVN.IMS2.Core.Repositories.Streams
{
    public sealed class StreamManager2
    {
        public StreamManager2()
        {
            Load();
        }
        private void Load()
        {          
            //notify telegram
            ExchangeFactory.CreateStream(new EQConfiguration { Topic = TopicName.PUSHTELEGRAMNOTIFY, Recovery = true, Retry=3 }, async (msg, ack) =>
            {
                try
                {
                    //TODO somethings
                    var queue = Json.Parse<Entities.Queue<object>>(msg.Payload.ToString());
                    var res = false;
                    if (queue != null && queue.Data != null)
                    {
                        switch (queue.Action)
                        {

                            case ActionName.PushTelegramNotify:
                                var obj = Json.Parse<NotificationModel>(queue.Data.ToString());
                                if (obj !=null && obj.Id > 0)
                                {                                    
                                    res = await NewsOnHomeRepository.PushTelegramNotify(obj.Id,obj.Note);
                                    if (res)
                                    {
                                        ack(null);
                                    }
                                    else
                                    {
                                        ack(new Exception());
                                    }
                                }
                                else
                                {
                                    ack(new Exception());
                                }
                                break;
                            default:
                                ack(new Exception());
                                break;
                        }
                    }
                    else
                    {
                        ack(new Exception());
                    }
                }
                catch (Exception ex)
                {
                    ack(ex);
                    Logger.Sensitive(ex, ex.Message);
                }
            });

            //notify app
            ExchangeFactory.CreateStream(new EQConfiguration { Topic = TopicName.PUSHNOTIFICATIONAPP, Recovery = true, Retry = 3 }, async (msg, ack) =>
            {
                try
                {
                    //TODO somethings
                    var queue = Json.Parse<Entities.Queue<object>>(msg.Payload.ToString());
                    var res = false;
                    if (queue != null && queue.Data != null)
                    {
                        switch (queue.Action)
                        {

                            case ActionName.PushNotificationPostUsers:
                                var obj = Json.Parse<NewsOnHome>(queue.Data.ToString());
                                if (obj != null)
                                {
                                    res = await NewsOnHomeRepository.NotificationPostUsers(obj);
                                    if (res)
                                    {
                                        ack(null);
                                    }
                                    else
                                    {
                                        ack(new Exception());
                                    }
                                }
                                else
                                {
                                    ack(new Exception());
                                }
                                break;
                            default:
                                ack(new Exception());
                                break;
                        }
                    }
                    else
                    {
                        ack(new Exception());
                    }
                }
                catch (Exception ex)
                {
                    ack(ex);
                    Logger.Sensitive(ex, ex.Message);
                }
            });
        }
    }
}
