﻿using ChannelVN.IMS2.Core.Entities;
using ChannelVN.IMS2.Core.Entities.AccountAssignment;
using ChannelVN.IMS2.Core.Entities.Album;
using ChannelVN.IMS2.Core.Entities.Beam;
using ChannelVN.IMS2.Core.Entities.Board;
using ChannelVN.IMS2.Core.Entities.Distribution;
using ChannelVN.IMS2.Core.Entities.Gallery;
using ChannelVN.IMS2.Core.Entities.Media;
using ChannelVN.IMS2.Core.Entities.Newsletters;
using ChannelVN.IMS2.Core.Entities.NewsOnHome;
using ChannelVN.IMS2.Core.Entities.Photo;
using ChannelVN.IMS2.Core.Entities.Posts;
using ChannelVN.IMS2.Core.Entities.Security;
using ChannelVN.IMS2.Core.Entities.ShareLinks;
using ChannelVN.IMS2.Core.Entities.Videos;
using ChannelVN.IMS2.Core.Models.CacheStore;
using ChannelVN.IMS2.Core.Models.CacheStore.Album;
using ChannelVN.IMS2.Core.Models.CacheStore.Articles;
using ChannelVN.IMS2.Core.Models.CacheStore.Beams;
using ChannelVN.IMS2.Core.Models.CacheStore.Gallery;
using ChannelVN.IMS2.Core.Models.CacheStore.Media;
using ChannelVN.IMS2.Core.Models.CacheStore.Photo;
using ChannelVN.IMS2.Core.Models.CacheStore.Posts;
using ChannelVN.IMS2.Core.Models.CacheStore.Security;
using ChannelVN.IMS2.Core.Models.CacheStore.ShareLinks;
using ChannelVN.IMS2.Core.Models.CacheStore.Videos;
using ChannelVN.IMS2.Core.Models.DataStore;
using ChannelVN.IMS2.Core.Models.DataStore.Album;
using ChannelVN.IMS2.Core.Models.DataStore.Articles;
using ChannelVN.IMS2.Core.Models.DataStore.Beams;
using ChannelVN.IMS2.Core.Models.DataStore.Boards;
using ChannelVN.IMS2.Core.Models.DataStore.Distribution;
using ChannelVN.IMS2.Core.Models.DataStore.Gallery;
using ChannelVN.IMS2.Core.Models.DataStore.Media;
using ChannelVN.IMS2.Core.Models.DataStore.Photo;
using ChannelVN.IMS2.Core.Models.DataStore.Posts;
using ChannelVN.IMS2.Core.Models.DataStore.Security;
using ChannelVN.IMS2.Core.Models.DataStore.ShareLinks;
using ChannelVN.IMS2.Core.Models.DataStore.Videos;
using ChannelVN.IMS2.Core.Models.ExternalAPI;
using ChannelVN.IMS2.Core.Models.SearchStore;
using ChannelVN.IMS2.Core.Models.SearchStore.Album;
using ChannelVN.IMS2.Core.Models.SearchStore.Articles;
using ChannelVN.IMS2.Core.Models.SearchStore.Beams;
using ChannelVN.IMS2.Core.Models.SearchStore.Boards;
using ChannelVN.IMS2.Core.Models.SearchStore.Distribution;
using ChannelVN.IMS2.Core.Models.SearchStore.Gallery;
using ChannelVN.IMS2.Core.Models.SearchStore.Media;
using ChannelVN.IMS2.Core.Models.SearchStore.Photo;
using ChannelVN.IMS2.Core.Models.SearchStore.Posts;
using ChannelVN.IMS2.Core.Models.SearchStore.Security;
using ChannelVN.IMS2.Core.Models.SearchStore.ShareLinks;
using ChannelVN.IMS2.Core.Models.SearchStore.Videos;
using ChannelVN.IMS2.Core.Repositories.Constants;
using ChannelVN.IMS2.Foundation.Common;
using ChannelVN.IMS2.Foundation.Common.Configuration;
using ChannelVN.IMS2.Foundation.Data.Factories;
using ChannelVN.IMS2.Foundation.Logging;
using ChannelVN.IMS2.Foundation.Toolkit.Messaging;
using ChannelVN.IMS2.Foundation.Toolkit.Messaging.ExchangeQueue;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using static ChannelVN.IMS2.Core.Entities.ErrorCode.ErrorMapping;

namespace ChannelVN.IMS2.Core.Repositories.Streams
{
    public sealed class StreamManager
    {
        public StreamManager()
        {
            Load();
        }
        private void Load()
        {
            #region push data
            ExchangeFactory.CreateStream(new EQConfiguration { Topic = TopicName.SHARE, Recovery = true, Retry = 3 }, async (msg, ack) =>
            {
                try
                {
                    //TODO somethings
                    var queue = Json.Parse<Entities.Queue<object>>(msg.Payload.ToString());
                    var res = false;
                    if (queue != null && queue.Data != null)
                    {
                        switch (queue.Action)
                        {
                            case ActionName.ShareMediaUnit:
                                var obj1 = Json.Parse<QueueData<string, PayloadDataShare, string, Media.Action>>(queue.Data.ToString());
                                if (obj1 != null && obj1.Data1 != null && obj1.Data2 != null && obj1.Data3 != null)
                                    res = await MediaUnitRepository.PushToKingHub(obj1.Data3, obj1.Data2, obj1.Data4);
                                if (res)
                                {
                                    ack(null);
                                }
                                else
                                {
                                    ack(new Exception());
                                }
                                break;
                            case ActionName.SharePhoto:
                                var obj2 = Json.Parse<QueueData<string, PayloadDataShare, string, Media.Action>>(queue.Data.ToString());
                                if (obj2 != null && obj2.Data1 != null && obj2.Data2 != null && obj2.Data3 != null)
                                    res = await PhotoRepository.PushPhotoToKingHub(obj2.Data3, obj2.Data2, obj2.Data4);
                                if (res)
                                {
                                    ack(null);
                                }
                                else
                                {
                                    ack(new Exception());
                                }
                                break;
                            case ActionName.ShareVideo:
                                var obj3 = Json.Parse<QueueData<string, PayloadDataShare, string, Media.Action>>(queue.Data.ToString());
                                if (obj3 != null && obj3.Data1 != null && obj3.Data2 != null && obj3.Data3 != null)
                                    res = await VideoRepository.PushVideoToKingHub(obj3.Data3, obj3.Data2, obj3.Data4);
                                if (res)
                                {
                                    ack(null);
                                }
                                else
                                {
                                    ack(new Exception());
                                }
                                break;
                            case ActionName.SharePlayList:
                                var obj4 = Json.Parse<QueueData<string, PayloadDataShare, string, Media.Action>>(queue.Data.ToString());
                                if (obj4 != null && obj4.Data1 != null && obj4.Data2 != null && obj4.Data3 != null)                                                                        
                                    res = await VideoPlaylistRepository.PushVideoPlaylistToKingHub(obj4.Data3, obj4.Data2, obj4.Data4);
                                if (res)
                                {
                                    ack(null);
                                }
                                else
                                {
                                    ack(new Exception());
                                }                                                                   
                                break;
                            case ActionName.SharePost:
                                var obj5 = Json.Parse<QueueData<string, PayloadDataShare, string, Media.Action>>(queue.Data.ToString());
                                if (obj5 != null && obj5.Data1 != null && obj5.Data2 != null && obj5.Data3 != null)
                                    res = await PostRepository.PushPostToKingHub(obj5.Data3, obj5.Data2, obj5.Data4);
                                if (res)
                                {
                                    ack(null);
                                }
                                else
                                {
                                    ack(new Exception());
                                }
                                break;
                            case ActionName.ShareShareLink:
                                var obj6 = Json.Parse<QueueData<string, PayloadDataShare, string, Media.Action>>(queue.Data.ToString());
                                if (obj6 != null && obj6.Data1 != null && obj6.Data2 != null && obj6.Data3 != null)
                                    res = await ShareLinkRepository.PushShareLinkToKingHub(obj6.Data3, obj6.Data2, obj6.Data4);
                                if (res)
                                {
                                    ack(null);
                                }
                                else
                                {
                                    ack(new Exception());
                                }
                                break;
                            case ActionName.ShareArticle:
                                var obj7 = Json.Parse<QueueData<string, PayloadDataShare, string, Media.Action>>(queue.Data.ToString());
                                if (obj7 != null && obj7.Data1 != null && obj7.Data2 != null && obj7.Data3 != null)
                                    res = await ArticleRepository.PushToKingHub(obj7.Data3, obj7.Data2, obj7.Data4);
                                if (res)
                                {
                                    ack(null);
                                }
                                else
                                {
                                    ack(new Exception());
                                }
                                break;
                            case ActionName.ShareAlbum:
                                var obj8 = Json.Parse<QueueData<string, PayloadDataShare, string, Media.Action>>(queue.Data.ToString());
                                if (obj8 != null && obj8.Data1 != null && obj8.Data2 != null && obj8.Data3 != null)
                                    res = await AlbumRepository.PushAlbumToKingHub(obj8.Data3, obj8.Data2, obj8.Data4);
                                if (res)
                                {
                                    ack(null);
                                }
                                else
                                {
                                    ack(new Exception());
                                }
                                break;
                            case ActionName.ShareBeam:
                                var obj9 = Json.Parse<QueueData<string, PayloadDataShare, string, Media.Action>>(queue.Data.ToString());
                                if (obj9 == null)
                                {
                                    ack(null);
                                }
                                else
                                {
                                    if (obj9 != null && obj9.Data1 != null && obj9.Data2 != null && !string.IsNullOrEmpty(obj9.Data3))
                                        res = await BeamRepository.PushToKingHub(obj9.Data3, obj9.Data2, obj9.Data4);
                                    if (res)
                                    {
                                        ack(null);
                                    }
                                    else
                                    {
                                        ack(new Exception());
                                    }
                                }
                                break;
                            case ActionName.ShareGallery:
                                var obj10 = Json.Parse<QueueData<string, PayloadDataShare, string, Media.Action>>(queue.Data.ToString());
                                if (obj10 != null && obj10.Data1 != null && obj10.Data2 != null && obj10.Data3 != null)
                                    res = await GalleryRepository.PushGalleryToKingHub(obj10.Data3, obj10.Data2, obj10.Data4);
                                if (res)
                                {
                                    ack(null);
                                }
                                else
                                {
                                    ack(new Exception());
                                }
                                break;
                            default:
                                ack(new Exception());
                                break;
                        }

                    }
                    else
                    {
                        ack(null);
                    }
                }
                catch (Exception ex)
                {
                    ack(ex);
                    Logger.Sensitive(ex, ex.Message);
                }
            });
            #endregion

            #region stream ES
            ExchangeFactory.CreateStream(new EQConfiguration { Topic = TopicName.ES_ARTICLE, Recovery = true }, async (msg, ack) =>
            {
                try
                {
                    //TODO somethings
                    var queue = Json.Parse<Entities.Queue<object>>(msg.Payload.ToString());
                    var res = false;
                    if (queue != null && queue.Data != null)
                    {
                        switch (queue.Action)
                        {
                            case ActionName.AddArticleES:
                                var id1 = Json.Parse<long>(queue.Data.ToString());
                                if (id1 > 0)
                                {
                                    var obj1 = await StoreFactory.Create<ArticleCacheStore>().GetArticleFullInfoAsync(id1);
                                    res = await StoreFactory.Create<ArticleSearchStore>().AddAsync(obj1, true);
                                    if (res || obj1 == null)
                                    {
                                        ack(null);
                                    }
                                    else
                                    {
                                        ack(new Exception());
                                    }
                                }
                                else
                                {
                                    ack(new Exception());
                                }
                                break;
                            case ActionName.UpdateArticleES:
                                var id2 = Json.Parse<long>(queue.Data.ToString());
                                if (id2 > 0)
                                {
                                    var obj2 = await StoreFactory.Create<ArticleCacheStore>().GetArticleFullInfoAsync(id2);
                                    res = await StoreFactory.Create<ArticleSearchStore>().UpdateAsync(obj2, true);
                                    if (res || obj2 == null)
                                    {
                                        ack(null);
                                    }
                                    else
                                    {
                                        ack(new Exception());
                                    }
                                }
                                else
                                {
                                    ack(new Exception());
                                }
                                break;
                            case ActionName.UpdateStatusArticleES:
                                var id3 = Json.Parse<long>(queue.Data.ToString());
                                if (id3 > 0)
                                {
                                    var obj3 = await StoreFactory.Create<ArticleCacheStore>().GetArticleFullInfoAsync(id3);
                                    res = await StoreFactory.Create<ArticleSearchStore>().UpdateStatusAsync(obj3, true);
                                    if (res || obj3 == null)
                                    {
                                        ack(null);
                                    }
                                    else
                                    {
                                        ack(new Exception());
                                    }
                                }
                                else
                                {
                                    ack(new Exception());
                                }
                                break;
                            //case ActionName.PublishNewsES:
                            //    res = await StoreFactory.Create<NewsSearchStore>().PublishAsync(Json.Parse<Article>(queue.Data.ToString()));
                            //    if (res)
                            //    {
                            //        ack(null);
                            //    }
                            //    else
                            //    {
                            //        ack(new Exception());
                            //    }
                            //    break;
                            case ActionName.ShareArticleES:
                                var obj4 = Json.Parse<QueueData<ItemStreamDistribution, List<ArticleSearch>, bool, object>>(queue.Data.ToString());
                                if (obj4 != null && obj4.Data1 != null && obj4.Data2 != null)
                                    res = await StoreFactory.Create<DistributionSearchStore>().ShareArticleAsync(obj4.Data1, obj4.Data2, true);
                                if (res || obj4 == null)
                                {
                                    ack(null);
                                }
                                else
                                {
                                    ack(new Exception());
                                }
                                break;
                            default:
                                ack(new Exception());
                                break;
                        }
                    }
                    else
                    {
                        ack(new Exception());
                    }
                }
                catch (Exception ex)
                {
                    ack(ex);
                    Logger.Sensitive(ex, ex.Message);
                }
            });
            ExchangeFactory.CreateStream(new EQConfiguration { Topic = TopicName.ES_POST, Recovery = true }, async (msg, ack) =>
            {
                try
                {
                    //TODO somethings
                    var queue = Json.Parse<Entities.Queue<object>>(msg.Payload.ToString());
                    var res = false;
                    if (queue != null && queue.Data != null)
                    {
                        switch (queue.Action)
                        {
                            case ActionName.AddPostES:
                                var id1 = Json.Parse<long>(queue.Data.ToString());
                                if (id1 > 0)
                                {
                                    var obj1 = await StoreFactory.Create<PostCacheStore>().GetPostFullInfoAsync(id1);
                                    res = await StoreFactory.Create<PostSearchStore>().AddAsync(obj1, true);
                                    if (res || obj1 == null)
                                    {
                                        ack(null);
                                    }
                                    else
                                    {
                                        ack(new Exception());
                                    }
                                }
                                else
                                {
                                    ack(new Exception());
                                }
                                break;
                            case ActionName.UpdatePostES:
                                var id2 = Json.Parse<long>(queue.Data.ToString());
                                if (id2 > 0)
                                {
                                    var obj2 = await StoreFactory.Create<PostCacheStore>().GetPostFullInfoAsync(id2);
                                    res = await StoreFactory.Create<PostSearchStore>().UpdateAsync(obj2, true);
                                    if (res || obj2 == null)
                                    {
                                        ack(null);
                                    }
                                    else
                                    {
                                        ack(new Exception());
                                    }
                                }
                                else
                                {
                                    ack(new Exception());
                                }
                                break;
                            case ActionName.UpdatePostStatusES:
                                var id3 = Json.Parse<long>(queue.Data.ToString());
                                if (id3 > 0)
                                {
                                    var obj3 = await StoreFactory.Create<PostCacheStore>().GetPostFullInfoAsync(id3);
                                    res = await StoreFactory.Create<PostSearchStore>().UpdateStatusAsync(obj3, true);
                                    if (res || obj3 == null)
                                    {
                                        ack(null);
                                    }
                                    else
                                    {
                                        ack(new Exception());
                                    }
                                }
                                else
                                {
                                    ack(new Exception());
                                }
                                break;
                            case ActionName.SharePostES:
                                var obj4 = Json.Parse<QueueData<ItemStreamDistribution, List<PostSearch>, bool, object>>(queue.Data.ToString());
                                if (obj4 != null && obj4.Data1 != null && obj4.Data2 != null)
                                    res = await StoreFactory.Create<DistributionSearchStore>().SharePostAsync(obj4.Data1, obj4.Data2, obj4.Data3, true);
                                if (res || obj4 == null)
                                {
                                    ack(null);
                                }
                                else
                                {
                                    ack(new Exception());
                                }
                                break;
                            default:
                                ack(new Exception());
                                break;
                        }
                    }
                    else
                    {
                        ack(new Exception());
                    }
                }
                catch (Exception ex)
                {
                    ack(ex);
                    Logger.Sensitive(ex, ex.Message);
                }
            });
            ExchangeFactory.CreateStream(new EQConfiguration { Topic = TopicName.ES_SHARE_LINK, Recovery = true }, async (msg, ack) =>
            {
                try
                {
                    //TODO somethings
                    var queue = Json.Parse<Entities.Queue<object>>(msg.Payload.ToString());
                    var res = false;
                    if (queue != null && queue.Data != null)
                    {
                        switch (queue.Action)
                        {
                            case ActionName.AddShareLinkES:
                                var id1 = Json.Parse<long>(queue.Data.ToString());
                                if (id1 > 0)
                                {
                                    var obj1 = await StoreFactory.Create<ShareLinkCacheStore>().GetFullInfoAsync(id1);
                                    res = await StoreFactory.Create<ShareLinkSearchStore>().AddAsync(obj1, true);
                                    if (res || obj1 == null)
                                    {
                                        ack(null);
                                    }
                                    else
                                    {
                                        ack(new Exception());
                                    }
                                }
                                else
                                {
                                    ack(new Exception());
                                }
                                break;
                            case ActionName.UpdateShareLinkES:
                                var id2 = Json.Parse<long>(queue.Data.ToString());
                                if (id2 > 0)
                                {
                                    var obj2 = await StoreFactory.Create<ShareLinkCacheStore>().GetFullInfoAsync(id2);
                                    res = await StoreFactory.Create<ShareLinkSearchStore>().UpdateAsync(obj2, true);
                                    if (res || obj2 == null)
                                    {
                                        ack(null);
                                    }
                                    else
                                    {
                                        ack(new Exception());
                                    }
                                }
                                else
                                {
                                    ack(new Exception());
                                }
                                break;
                            case ActionName.UpdateShareLinkStatusES:
                                var id3 = Json.Parse<long>(queue.Data.ToString());
                                if (id3 > 0)
                                {
                                    var obj3 = await StoreFactory.Create<ShareLinkCacheStore>().GetFullInfoAsync(id3);
                                    res = await StoreFactory.Create<ShareLinkSearchStore>().UpdateStatusAsync(obj3, true);
                                    if (res || obj3 == null)
                                    {
                                        ack(null);
                                    }
                                    else
                                    {
                                        ack(new Exception());
                                    }
                                }
                                else
                                {
                                    ack(new Exception());
                                }
                                break;
                            case ActionName.ShareShareLinkES:
                                var obj4 = Json.Parse<QueueData<ItemStreamDistribution, List<ShareLinkSearch>, bool, object>>(queue.Data.ToString());
                                if (obj4 != null && obj4.Data1 != null && obj4.Data2 != null)
                                    res = await StoreFactory.Create<DistributionSearchStore>().ShareShareLinkAsync(obj4.Data1, obj4.Data2, obj4.Data3, true);
                                if (res || obj4 == null)
                                {
                                    ack(null);
                                }
                                else
                                {
                                    ack(new Exception());
                                }
                                break;
                            default:
                                ack(new Exception());
                                break;
                        }
                    }
                    else
                    {
                        ack(new Exception());
                    }
                }
                catch (Exception ex)
                {
                    ack(ex);
                    Logger.Sensitive(ex, ex.Message);
                }
            });
            ExchangeFactory.CreateStream(new EQConfiguration { Topic = TopicName.ES_VIDEO, Recovery = true }, async (msg, ack) =>
            {
                try
                {
                    //TODO somethings
                    var queue = Json.Parse<Entities.Queue<object>>(msg.Payload.ToString());
                    var res = false;
                    if (queue != null && queue.Data != null)
                    {
                        switch (queue.Action)
                        {
                            case ActionName.AddVideoES:
                                var id1 = Json.Parse<long>(queue.Data.ToString());
                                if (id1 > 0)
                                {
                                    var obj1 = await StoreFactory.Create<VideoCacheStore>().GetVideoFullInfoAsync(id1);
                                    res = await StoreFactory.Create<VideoSearchStore>().AddAsync(obj1, true);
                                    if (res || obj1 == null)
                                    {
                                        ack(null);
                                    }
                                    else
                                    {
                                        ack(new Exception());
                                    }
                                }
                                else
                                {
                                    ack(new Exception());
                                }
                                break;
                            case ActionName.UpdateVideoES:
                                var id2 = Json.Parse<long>(queue.Data.ToString());
                                if (id2 > 0)
                                {
                                    var obj2 = await StoreFactory.Create<VideoCacheStore>().GetVideoFullInfoAsync(id2);
                                    //var obj2Db = await StoreFactory.Create<VideoCacheStore>().GetByIdAsync(obj2.Id);
                                    //var newsInAccount= await StoreFactory.Create<VideoCacheStore>().GetByIdAsync(obj2.Id);
                                    res = await StoreFactory.Create<VideoSearchStore>().UpdateAsync(obj2, true);
                                    if (res || obj2 == null)
                                    {
                                        ack(null);
                                    }
                                    else
                                    {
                                        ack(new Exception());
                                    }
                                }
                                else
                                {
                                    ack(new Exception());
                                }
                                break;

                            case ActionName.UpdateVideoStatusES:
                                var id3 = Json.Parse<long>(queue.Data.ToString());
                                if (id3 > 0)
                                {
                                    var obj3 = await StoreFactory.Create<VideoCacheStore>().GetVideoFullInfoAsync(id3);
                                    res = await StoreFactory.Create<VideoSearchStore>().UpdateStatusAsync(obj3, true);
                                    if (res || obj3 == null)
                                    {
                                        ack(null);
                                    }
                                    else
                                    {
                                        ack(new Exception());
                                    }
                                }
                                else
                                {
                                    ack(new Exception());
                                }
                                break;
                            case ActionName.ShareVideoES:
                                var obj4 = Json.Parse<QueueData<ItemStreamDistribution, List<VideoSearch>, bool, object>>(queue.Data.ToString());
                                if (obj4 != null && obj4.Data1 != null && obj4.Data2 != null)
                                    res = await StoreFactory.Create<DistributionSearchStore>().ShareVideoAsync(obj4.Data1, obj4.Data2, obj4.Data3, true);
                                if (res || obj4 == null)
                                {
                                    ack(null);
                                }
                                else
                                {
                                    ack(new Exception());
                                }
                                break;
                            default:
                                ack(new Exception());
                                break;
                        }
                    }
                    else
                    {
                        ack(new Exception());
                    }
                }
                catch (Exception ex)
                {
                    ack(ex);
                    Logger.Sensitive(ex, ex.Message);
                }
            });

            ExchangeFactory.CreateStream(new EQConfiguration { Topic = TopicName.ES_PLAYLIST, Recovery = true }, async (msg, ack) =>
            {
                try
                {
                    //TODO somethings
                    var queue = Json.Parse<Entities.Queue<object>>(msg.Payload.ToString());
                    var res = false;
                    if (queue != null && queue.Data != null)
                    {
                        switch (queue.Action)
                        {
                            case ActionName.AddVideoPlaylistES:
                                var id1 = Json.Parse<long>(queue.Data.ToString());
                                if (id1 > 0)
                                {
                                    var obj1 = await StoreFactory.Create<VideoPlaylistCacheStore>().GetVideoPlaylistFullInfoAsync(id1);
                                    res = await StoreFactory.Create<VideoPlaylistSearchStore>().AddAsync(obj1, true);
                                    if (res || obj1 == null)
                                    {
                                        ack(null);
                                    }
                                    else
                                    {
                                        ack(new Exception());
                                    }
                                }
                                else
                                {
                                    ack(new Exception());
                                }
                                break;
                            case ActionName.UpdateVideoPlaylistES:
                                var id2 = Json.Parse<long>(queue.Data.ToString());
                                if (id2 > 0)
                                {
                                    var obj2 = await StoreFactory.Create<VideoPlaylistCacheStore>().GetVideoPlaylistFullInfoAsync(id2);
                                    res = await StoreFactory.Create<VideoPlaylistSearchStore>().UpdateAsync(obj2, true);
                                    if (res || obj2 == null)
                                    {
                                        ack(null);
                                    }
                                    else
                                    {
                                        ack(new Exception());
                                    }
                                }
                                else
                                {
                                    ack(new Exception());
                                }
                                break;
                            case ActionName.ShareVideoPlaylistES:
                                var obj3 = Json.Parse<QueueData<ItemStreamDistribution, List<VideoPlaylistSearch>, bool, object>>(queue.Data.ToString());
                                if (obj3 != null)
                                    res = await StoreFactory.Create<DistributionSearchStore>().ShareVideoPlaylistAsync(obj3.Data1, obj3.Data2, obj3.Data3, true);
                                if (res || obj3 == null || obj3.Data1 == null || obj3.Data2 == null)
                                {
                                    ack(null);
                                }
                                else
                                {
                                    ack(new Exception());
                                }
                                break;
                            case ActionName.UpdateVideoPlaylistStatusES:
                                var id4 = Json.Parse<long>(queue.Data.ToString());
                                if (id4 > 0)
                                {
                                    var obj4 = await StoreFactory.Create<VideoPlaylistCacheStore>().GetVideoPlaylistFullInfoAsync(id4);
                                    res = await StoreFactory.Create<VideoPlaylistSearchStore>().UpdateStatusAsync(obj4, true);
                                    if (res || obj4 == null)
                                    {
                                        ack(null);
                                    }
                                    else
                                    {
                                        ack(new Exception());
                                    }
                                }
                                else
                                {
                                    ack(new Exception());
                                }
                                break;
                            default:
                                ack(new Exception());
                                break;
                        }
                    }
                    else
                    {
                        ack(new Exception());
                    }
                }
                catch (Exception ex)
                {
                    ack(ex);
                    Logger.Sensitive(ex, ex.Message);
                }
            });

            ExchangeFactory.CreateStream(new EQConfiguration { Topic = TopicName.ES_TAG, Recovery = true }, async (msg, ack) =>
            {
                try
                {
                    //TODO somethings
                    var queue = Json.Parse<Entities.Queue<object>>(msg.Payload.ToString());
                    var res = false;
                    if (queue != null && queue.Data != null)
                    {
                        switch (queue.Action)
                        {
                            case ActionName.AddTagES:
                                var obj1 = Json.Parse<Tag>(queue.Data.ToString());
                                if (obj1 != null)
                                    res = await StoreFactory.Create<TagSearchStore>().AddAsync(obj1, true);
                                if (res || obj1 == null)
                                {
                                    ack(null);
                                }
                                else
                                {
                                    ack(new Exception());
                                }
                                break;
                            case ActionName.UpdateTagES:
                                var obj2 = Json.Parse<Tag>(queue.Data.ToString());
                                if (obj2 != null)
                                    res = await StoreFactory.Create<TagSearchStore>().UpdateAsync(obj2, true);
                                if (res || obj2 == null)
                                {
                                    ack(null);
                                }
                                else
                                {
                                    ack(new Exception());
                                }
                                break;
                            default:
                                ack(new Exception());
                                break;
                        }
                    }
                    else
                    {
                        ack(new Exception());
                    }
                }
                catch (Exception ex)
                {
                    ack(ex);
                    Logger.Sensitive(ex, ex.Message);
                }
            });

            ExchangeFactory.CreateStream(new EQConfiguration { Topic = TopicName.ES_MEDIA_UNIT, Recovery = true }, async (msg, ack) =>
            {
                try
                {
                    //TODO somethings
                    var queue = Json.Parse<Entities.Queue<object>>(msg.Payload.ToString());
                    var res = false;
                    if (queue != null && queue.Data != null)
                    {
                        switch (queue.Action)
                        {
                            case ActionName.AddMediaUnitES:
                                var id1 = Json.Parse<long>(queue.Data.ToString());
                                if (id1 > 0)
                                {
                                    var obj1 = await StoreFactory.Create<MediaUnitCacheStore>().GetMediaUnitFullInfoAsync(id1);
                                    res = await StoreFactory.Create<MediaUnitSearchStore>().AddAsync(obj1, true);
                                    if (res || obj1 == null)
                                    {
                                        ack(null);
                                    }
                                    else
                                    {
                                        ack(new Exception());
                                    }
                                }
                                else
                                {
                                    ack(new Exception());
                                }
                                break;
                            case ActionName.UpdateMediaUnitES:
                                var id2 = Json.Parse<long>(queue.Data.ToString());
                                if (id2 > 0)
                                {
                                    var obj2 = await StoreFactory.Create<MediaUnitCacheStore>().GetMediaUnitFullInfoAsync(id2);
                                    res = await StoreFactory.Create<MediaUnitSearchStore>().UpdateAsync(obj2, true);
                                    if (res || obj2 == null)
                                    {
                                        ack(null);
                                    }
                                    else
                                    {
                                        ack(new Exception());
                                    }
                                }
                                else
                                {
                                    ack(new Exception());
                                }
                                break;
                            case ActionName.UpdateStatusMediaUnitES:
                                var id3 = Json.Parse<long>(queue.Data.ToString());
                                if (id3 > 0)
                                {
                                    var obj3 = await StoreFactory.Create<MediaUnitCacheStore>().GetMediaUnitFullInfoAsync(id3);
                                    res = await StoreFactory.Create<MediaUnitSearchStore>().UpdateStatusAsync(obj3, true);
                                    if (res || obj3 == null)
                                    {
                                        ack(null);
                                    }
                                    else
                                    {
                                        ack(new Exception());
                                    }
                                }
                                else
                                {
                                    ack(new Exception());
                                }
                                break;
                            case ActionName.ShareMediaUnitES:
                                var obj4 = Json.Parse<QueueShareDataUnit<MediaUnitSearch>>(queue.Data.ToString());
                                if (obj4 != null && obj4.ItemStreamDistribution != null && obj4.ListMedia != null)
                                    res = await StoreFactory.Create<DistributionSearchStore>().ShareMediaUnitAsync(obj4.ItemStreamDistribution, obj4.ListMedia, true);
                                if (res || obj4 == null || obj4.ItemStreamDistribution == null || obj4.ListMedia == null)
                                {
                                    ack(null);
                                }
                                else
                                {
                                    ack(new Exception());
                                }
                                break;
                            default:
                                ack(new Exception());
                                break;
                        }
                    }
                    else
                    {
                        ack(new Exception());
                    }
                }
                catch (Exception ex)
                {
                    ack(ex);
                    Logger.Sensitive(ex, ex.Message);
                }
            });

            ExchangeFactory.CreateStream(new EQConfiguration { Topic = TopicName.ES_PHOTO, Recovery = true }, async (msg, ack) =>
            {
                try
                {
                    //TODO somethings
                    var queue = Json.Parse<Entities.Queue<object>>(msg.Payload.ToString());
                    var res = false;
                    if (queue != null && queue.Data != null)
                    {
                        switch (queue.Action)
                        {
                            case ActionName.AddPhotoES:
                                var id1 = Json.Parse<long>(queue.Data.ToString());
                                if (id1 > 0)
                                {
                                    var obj1 = await StoreFactory.Create<PhotoCacheStore>().GetPhotoFullInfoAsync(id1);
                                    res = await StoreFactory.Create<PhotoSearchStore>().AddAsync(obj1, true);
                                    if (res || obj1 == null)
                                    {
                                        ack(null);
                                    }
                                    else
                                    {
                                        ack(new Exception());
                                    }
                                }
                                else
                                {
                                    ack(new Exception());
                                }
                                break;
                            case ActionName.UpdatePhotoES:
                                var id2 = Json.Parse<long>(queue.Data.ToString());
                                if (id2 > 0)
                                {
                                    var obj2 = await StoreFactory.Create<PhotoCacheStore>().GetPhotoFullInfoAsync(id2);
                                    res = await StoreFactory.Create<PhotoSearchStore>().UpdateAsync(obj2, true);
                                    if (res || obj2 == null)
                                    {
                                        ack(null);
                                    }
                                    else
                                    {
                                        ack(new Exception());
                                    }
                                }
                                else
                                {
                                    ack(new Exception());
                                }
                                break;
                            case ActionName.UpdateStatusPhotoES:
                                var id3 = Json.Parse<long>(queue.Data.ToString());
                                if (id3 > 0)
                                {
                                    var obj3 = await StoreFactory.Create<PhotoCacheStore>().GetPhotoFullInfoAsync(id3);
                                    res = await StoreFactory.Create<PhotoSearchStore>().UpdateStatusAsync(obj3, true);
                                    if (res || obj3 == null)
                                    {
                                        ack(null);
                                    }
                                    else
                                    {
                                        ack(new Exception());
                                    }
                                }
                                else
                                {
                                    ack(new Exception());
                                }
                                break;
                            case ActionName.SharePhotoES:
                                var obj4 = Json.Parse<QueueShareDataUnit<PhotoSearch>>(queue.Data.ToString());
                                if (obj4 != null && obj4.ItemStreamDistribution != null && obj4.ListMedia != null)
                                    res = await StoreFactory.Create<DistributionSearchStore>().SharePhotoAsync(obj4.ItemStreamDistribution, obj4.ListMedia, true);
                                if (res || obj4 == null)
                                {
                                    ack(null);
                                }
                                else
                                {
                                    ack(new Exception());
                                }
                                break;
                            default:
                                ack(new Exception());
                                break;
                        }
                    }
                    else
                    {
                        ack(new Exception());
                    }
                }
                catch (Exception ex)
                {
                    ack(ex);
                    Logger.Sensitive(ex, ex.Message);
                }
            });

            ExchangeFactory.CreateStream(new EQConfiguration { Topic = TopicName.ES_TEMPLATE, Recovery = true }, async (msg, ack) =>
            {
                try
                {
                    //TODO somethings
                    var queue = Json.Parse<Entities.Queue<object>>(msg.Payload.ToString());
                    var res = false;
                    if (queue != null && queue.Data != null)
                    {
                        switch (queue.Action)
                        {
                            case ActionName.AddTemplateES:
                                var obj1 = Json.Parse<Template>(queue.Data.ToString());
                                if (obj1 != null)
                                    res = await StoreFactory.Create<TemplateSearchStore>().AddAsync(obj1, true);
                                if (res || obj1 == null)
                                {
                                    ack(null);
                                }
                                else
                                {
                                    ack(new Exception());
                                }
                                break;
                            case ActionName.UpdateTemplateES:
                                var obj2 = Json.Parse<Template>(queue.Data.ToString());
                                if (obj2 != null)
                                    res = await StoreFactory.Create<TemplateSearchStore>().UpdateAsync(obj2, true);
                                if (res || obj2 == null)
                                {
                                    ack(null);
                                }
                                else
                                {
                                    ack(new Exception());
                                }
                                break;
                            case ActionName.UpdateStatusTemplateES:
                                var obj3 = Json.Parse<Template>(queue.Data.ToString());
                                if (obj3 != null)
                                    res = await StoreFactory.Create<TemplateSearchStore>().UpdateStatusAsync(obj3, true);
                                if (res)
                                {
                                    ack(null);
                                }
                                else
                                {
                                    ack(new Exception());
                                }
                                break;
                            default:
                                ack(new Exception());
                                break;
                        }
                    }
                    else
                    {
                        ack(new Exception());
                    }
                }
                catch (Exception ex)
                {
                    ack(ex);
                    Logger.Sensitive(ex, ex.Message);
                }
            });

            ExchangeFactory.CreateStream(new EQConfiguration { Topic = TopicName.ES_ACCOUNT, Recovery = true }, async (msg, ack) =>
            {
                try
                {
                    //TODO somethings
                    var queue = Json.Parse<Entities.Queue<object>>(msg.Payload.ToString());
                    var res = false;
                    if (queue != null && queue.Data != null)
                    {
                        switch (queue.Action)
                        {
                            case ActionName.AddAccountES:
                                var obj1 = Json.Parse<AccountSearch>(queue.Data.ToString());
                                if (obj1 != null)
                                    res = await StoreFactory.Create<AccountSearchStore>().AddAsync(obj1, true);
                                if (res || obj1 == null)
                                {
                                    ack(null);
                                }
                                else
                                {
                                    ack(new Exception());
                                }
                                break;
                            case ActionName.UpdateAccountES:
                                var obj2 = Json.Parse<AccountSearch>(queue.Data.ToString());
                                if (obj2 != null)
                                    res = await StoreFactory.Create<AccountSearchStore>().UpdateAsync(obj2, true);
                                if (res || obj2 == null)
                                {
                                    ack(null);
                                }
                                else
                                {
                                    ack(new Exception());
                                }
                                break;
                            case ActionName.AddOfficialAccountES:
                                var obj3 = Json.Parse<AccountSearch>(queue.Data.ToString());
                                if (obj3 != null)
                                    res = await StoreFactory.Create<AccountSearchStore>().AddAsync(obj3, true);
                                if (res || obj3 == null)
                                {
                                    ack(null);
                                }
                                else
                                {
                                    ack(new Exception());
                                }
                                break;

                            case ActionName.AddProfileES:
                                var acc = Json.Parse<Account>(queue.Data.ToString());
                                if (acc != null)
                                {
                                    var accountSearch = await StoreFactory.Create<AccountSearchStore>().GetByIdAsync(acc?.Id ?? 0);
                                    if (accountSearch != null)
                                        res = await StoreFactory.Create<AccountSearchStore>().UpdateAsync(Mapper<AccountSearch>.Map(acc, accountSearch ?? new AccountSearch()), true);
                                    if (res || acc == null || accountSearch == null)
                                    {
                                        ack(null);
                                    }
                                    else
                                    {
                                        ack(new Exception());
                                    }
                                }
                                else
                                {
                                    ack(new Exception());
                                }
                                break;
                            case ActionName.AcceptOrRejectES:
                                var obj4 = Json.Parse<AccountSearch>(queue.Data.ToString());
                                if (obj4 != null)
                                    res = await StoreFactory.Create<AccountSearchStore>().AcceptOrRejectAsync(obj4, true);
                                if (res || obj4 == null)
                                {
                                    ack(null);
                                }
                                else
                                {
                                    ack(new Exception());
                                }
                                break;
                            case ActionName.UpdateAccountStatusES:
                                var obj5 = queue.Data.ToString();
                                if (obj5 != null)
                                {
                                    var accObjId = Json.Parse<long>(obj5);
                                    if (accObjId > 0)
                                    {
                                        var accObj = await StoreFactory.Create<AccountCacheStore>().GetAccountSearchByIdAsync(accObjId);
                                        if (accObj != null)
                                        {
                                            res = await StoreFactory.Create<AccountSearchStore>().UpdateStatusAsync(accObj, true);
                                            if (res)
                                            {
                                                ack(null);
                                            }
                                            else
                                            {
                                                ack(new Exception());
                                            }
                                        }
                                        else
                                        {
                                            ack(new Exception());
                                        }
                                    }
                                    else
                                    {
                                        ack(null);
                                    }
                                }
                                else
                                {
                                    ack(new Exception());
                                }
                                break;
                            case ActionName.UpdateAccountMemberES:
                                var obj6 = queue.Data.ToString();
                                if (obj6 != null)
                                {
                                    var accObj = Json.Parse<AccountMember>(obj6);
                                    if (accObj != null)
                                    {
                                        res = await StoreFactory.Create<AccountSearchStore>().Update_AccountMemberAsync(accObj, true);
                                        if (res)
                                        {
                                            ack(null);
                                        }
                                        else
                                        {
                                            ack(new Exception());
                                        }
                                    }
                                    else
                                    {
                                        ack(null);
                                    }
                                }
                                else
                                {
                                    ack(new Exception());
                                }
                                break;
                            case ActionName.RemoveMemberES:
                                var obj7 = Json.Parse<QueueData<long, long, object, object>>(queue.Data.ToString());
                                if (obj7 != null)
                                {
                                    var accountId5 = obj7.Data1;
                                    var memberId5 = obj7.Data2;
                                    res = await StoreFactory.Create<AccountSearchStore>().RemoveMemberAsync(accountId5, memberId5, true);
                                    if (res)
                                    {
                                        ack(null);
                                    }
                                    else
                                    {
                                        ack(new Exception());
                                    }
                                }
                                else
                                {
                                    ack(null);
                                }
                                break;
                            case ActionName.RemoveMemberOnAllPageES:
                                var data = Json.Parse<QueueData<IEnumerable<long>, long, object, object>>(queue.Data.ToString());
                                if (data != null)
                                {
                                    res = await StoreFactory.Create<AccountSearchStore>().RemoveMemberOnAllPageAsync(data.Data1, data.Data2, true);
                                    if (res)
                                    {
                                        ack(null);
                                    }
                                    else
                                    {
                                        ack(new Exception());
                                    }
                                }
                                else
                                {
                                    ack(null);
                                }
                                break;
                            case ActionName.LockOrUnlockMemberES:
                                var data2 = Json.Parse<AccountMember>(queue.Data.ToString());
                                if (data2 != null)
                                {
                                    res = await StoreFactory.Create<AccountSearchStore>().LockOrUnlockMemberAsync(data2, true);
                                    if (res)
                                    {
                                        ack(null);
                                    }
                                    else
                                    {
                                        ack(new Exception());
                                    }
                                }
                                else
                                {
                                    ack(null);
                                }
                                break;
                            case ActionName.LockOrUnlockMemberOnAllPageES:
                                var data3 = Json.Parse<IEnumerable<AccountMember>>(queue.Data.ToString());
                                if (data3 != null)
                                {
                                    res = await StoreFactory.Create<AccountSearchStore>().LockOrUnlockMemberOnAllPageAsync(data3.ToArray(), true);
                                    if (res)
                                    {
                                        ack(null);
                                    }
                                    else
                                    {
                                        ack(new Exception());
                                    }
                                }
                                else
                                {
                                    ack(null);
                                }
                                break;
                            case ActionName.ConfigPageES:
                                var obj8 = Json.Parse<QueueData<long, int, long, object>>(queue.Data.ToString());
                                if (obj8 != null && obj8.Data1 > 0)
                                {
                                    var officer = await SecurityRepository.GetAccountSearchByIdAsync(obj8.Data1);
                                    res = await StoreFactory.Create<AccountSearchStore>().UpdateAsync(officer, true);
                                    if (res)
                                    {
                                        ack(null);
                                    }
                                    else
                                    {
                                        ack(new Exception());
                                    }
                                }
                                else
                                {
                                    ack(null);
                                }
                                break;
                            case ActionName.ChangePageOwnerES:
                                var obj9 = Json.Parse<QueueData<long, long, long, object>>(queue.Data.ToString());
                                if (obj9 != null && obj9.Data1 > 0 && obj9.Data2 > 0 && obj9.Data3 > 0)
                                {
                                    var officer = await SecurityRepository.GetAccountSearchByIdAsync(obj9.Data1);
                                    res = await StoreFactory.Create<AccountSearchStore>().UpdateAsync(officer, true);
                                    if (res)
                                    {
                                        ack(null);
                                    }
                                    else
                                    {
                                        ack(new Exception());
                                    }
                                }
                                else
                                {
                                    ack(null);
                                }
                                break;
                            default:
                                ack(new Exception());
                                break;
                        }
                    }
                    else
                    {
                        ack(new Exception());
                    }
                }
                catch (Exception ex)
                {
                    ack(ex);
                    Logger.Sensitive(ex, ex.Message);
                }
            });

            ExchangeFactory.CreateStream(new EQConfiguration { Topic = TopicName.ES_ITEMSTREAM, Recovery = true }, async (msg, ack) =>
            {
                try
                {
                    //TODO somethings
                    var queue = Json.Parse<Entities.Queue<object>>(msg.Payload.ToString());
                    var res = false;
                    if (queue != null && queue.Data != null)
                    {
                        switch (queue.Action)
                        {
                            case ActionName.AcceptItemStreamES:
                                var obj1 = Json.Parse<ItemStreamDistribution>(queue.Data.ToString());
                                if (obj1 != null)
                                    res = await StoreFactory.Create<ArticleSearchStore>().AcceptAsync(obj1, true);
                                if (res || obj1 == null)
                                {
                                    ack(null);
                                }
                                else
                                {
                                    ack(new Exception());
                                }
                                break;
                            case ActionName.RejectItemStreamES:
                                var obj2 = Json.Parse<ItemStreamDistribution>(queue.Data.ToString());
                                if (obj2 != null)
                                    res = await StoreFactory.Create<ArticleSearchStore>().RejectAsync(obj2, true);
                                if (res || obj2 == null)
                                {
                                    ack(null);
                                }
                                else
                                {
                                    ack(new Exception());
                                }
                                break;
                            default:
                                ack(new Exception());
                                break;
                        }
                    }
                    else
                    {
                        ack(new Exception());
                    }
                }
                catch (Exception ex)
                {
                    ack(ex);
                    Logger.Sensitive(ex, ex.Message);
                }
            });

            ExchangeFactory.CreateStream(new EQConfiguration { Topic = TopicName.ES_DISTRIBUTOR, Recovery = true }, async (msg, ack) =>
            {
                try
                {
                    //TODO somethings
                    var queue = Json.Parse<Entities.Queue<object>>(msg.Payload.ToString());
                    if (queue != null && queue.Data != null)
                    {
                        switch (queue.Action)
                        {

                            default:
                                await Task.Delay(1);
                                ack(new Exception());
                                break;
                        }
                    }
                    else
                    {
                        ack(new Exception());
                    }
                }
                catch (Exception ex)
                {
                    ack(ex);
                    Logger.Sensitive(ex, ex.Message);
                }
            });

            ExchangeFactory.CreateStream(new EQConfiguration { Topic = TopicName.ES_BOARD, Recovery = true }, async (msg, ack) =>
            {
                try
                {
                    //TODO somethings
                    var queue = Json.Parse<Entities.Queue<object>>(msg.Payload.ToString());
                    var res = false;
                    if (queue != null && queue.Data != null)
                    {
                        switch (queue.Action)
                        {
                            case ActionName.AddBoardES:
                                var id1 = Json.Parse<long>(queue.Data.ToString());
                                if (id1 > 0)
                                {
                                    var obj1 = await StoreFactory.Create<BoardCacheStore>().GetBoardSearchByIdAsync(id1);
                                    res = await StoreFactory.Create<BoardSearchStore>().AddAsync(obj1, true);
                                    if (res || obj1 == null)
                                    {
                                        ack(null);
                                    }
                                    else
                                    {
                                        ack(new Exception());
                                    }
                                }
                                else
                                {
                                    ack(new Exception());
                                }
                                break;
                            case ActionName.UpdateBoardES:
                                var id2 = Json.Parse<long>(queue.Data.ToString());
                                if (id2 > 0)
                                {
                                    var obj2 = await StoreFactory.Create<BoardCacheStore>().GetBoardSearchByIdAsync(id2);
                                    res = await StoreFactory.Create<BoardSearchStore>().UpdateAsync(obj2, true);
                                    if (res || obj2 == null)
                                    {
                                        ack(null);
                                    }
                                    else
                                    {
                                        ack(new Exception());
                                    }
                                }
                                else
                                {
                                    ack(new Exception());
                                }
                                break;
                            default:
                                ack(new Exception());
                                break;
                        }
                    }
                    else
                    {
                        ack(new Exception());
                    }
                }
                catch (Exception ex)
                {
                    ack(ex);
                    Logger.Sensitive(ex, ex.Message);
                }
            });

            ExchangeFactory.CreateStream(new EQConfiguration { Topic = TopicName.ES_ALBUM, Recovery = true }, async (msg, ack) =>
            {
                try
                {
                    //TODO somethings
                    var queue = Json.Parse<Entities.Queue<object>>(msg.Payload.ToString());
                    var res = false;
                    if (queue != null && queue.Data != null)
                    {
                        switch (queue.Action)
                        {
                            case ActionName.AddAlbumES:
                                var id1 = Json.Parse<long>(queue.Data.ToString());
                                if (id1 > 0)
                                {
                                    var obj1 = await StoreFactory.Create<AlbumCacheStore>().GetAlbumFullInfoAsync(id1);
                                    res = await StoreFactory.Create<AlbumSearchStore>().AddAsync(obj1, true);
                                    if (res || obj1 == null)
                                    {
                                        ack(null);
                                    }
                                    else
                                    {
                                        ack(new Exception());
                                    }
                                }
                                else
                                {
                                    ack(new Exception());
                                }
                                break;
                            case ActionName.UpdateAlbumES:
                                var id2 = Json.Parse<long>(queue.Data.ToString());
                                if (id2 > 0)
                                {
                                    var obj2 = await StoreFactory.Create<AlbumCacheStore>().GetAlbumFullInfoAsync(id2);
                                    res = await StoreFactory.Create<AlbumSearchStore>().UpdateAsync(obj2, true);
                                    if (res || obj2 == null)
                                    {
                                        ack(null);
                                    }
                                    else
                                    {
                                        ack(new Exception());
                                    }
                                }
                                else
                                {
                                    ack(new Exception());
                                }
                                break;
                            case ActionName.UpdateStatusAlbumES:
                                var id3 = Json.Parse<long>(queue.Data.ToString());
                                if (id3 > 0)
                                {
                                    var obj3 = await StoreFactory.Create<AlbumCacheStore>().GetAlbumFullInfoAsync(id3);
                                    res = await StoreFactory.Create<AlbumSearchStore>().UpdateStatusAsync(obj3, true);
                                    if (res || obj3 == null)
                                    {
                                        ack(null);
                                    }
                                    else
                                    {
                                        ack(new Exception());
                                    }
                                }
                                else
                                {
                                    ack(new Exception());
                                }
                                break;
                            case ActionName.ShareAlbumES:
                                var obj4 = Json.Parse<QueueShareDataUnit<AlbumSearch>>(queue.Data.ToString());
                                if (obj4 != null && obj4.ItemStreamDistribution != null && obj4.ListMedia != null)
                                    res = await StoreFactory.Create<DistributionSearchStore>().ShareAlbumAsync(obj4.ItemStreamDistribution, obj4.ListMedia, true);
                                if (res || obj4 == null || obj4.ItemStreamDistribution == null || obj4.ListMedia == null)
                                {
                                    ack(null);
                                }
                                else
                                {
                                    ack(new Exception());
                                }
                                break;
                            default:
                                ack(new Exception());
                                break;
                        }
                    }
                    else
                    {
                        ack(new Exception());
                    }
                }
                catch (Exception ex)
                {
                    ack(ex);
                    Logger.Sensitive(ex, ex.Message);
                }
            });
            ExchangeFactory.CreateStream(new EQConfiguration { Topic = TopicName.ES_GALLERY, Recovery = true }, async (msg, ack) =>
            {
                try
                {
                    //TODO somethings
                    var queue = Json.Parse<Entities.Queue<object>>(msg.Payload.ToString());
                    var res = false;
                    if (queue != null && queue.Data != null)
                    {
                        switch (queue.Action)
                        {
                            case ActionName.AddGalleryES:
                                var id1 = Json.Parse<long>(queue.Data.ToString());
                                if (id1 > 0)
                                {
                                    var obj1 = await StoreFactory.Create<GalleryCacheStore>().GetAlbumFullInfoAsync(id1);
                                    res = await StoreFactory.Create<GallerySearchStore>().AddAsync(obj1, true);
                                    if (res || obj1 == null)
                                    {
                                        ack(null);
                                    }
                                    else
                                    {
                                        ack(new Exception());
                                    }
                                }
                                else
                                {
                                    ack(new Exception());
                                }
                                break;
                            case ActionName.UpdateGalleryES:
                                var id2 = Json.Parse<long>(queue.Data.ToString());
                                if (id2 > 0)
                                {
                                    var obj2 = await StoreFactory.Create<GalleryCacheStore>().GetAlbumFullInfoAsync(id2);
                                    res = await StoreFactory.Create<GallerySearchStore>().UpdateAsync(obj2, true);
                                    if (res || obj2 == null)
                                    {
                                        ack(null);
                                    }
                                    else
                                    {
                                        ack(new Exception());
                                    }
                                }
                                else
                                {
                                    ack(new Exception());
                                }
                                break;
                            case ActionName.UpdateStatusGalleryES:
                                var id3 = Json.Parse<long>(queue.Data.ToString());
                                if (id3 > 0)
                                {
                                    var obj3 = await StoreFactory.Create<GalleryCacheStore>().GetAlbumFullInfoAsync(id3);
                                    res = await StoreFactory.Create<GallerySearchStore>().UpdateStatusAsync(obj3, true);
                                    if (res || obj3 == null)
                                    {
                                        ack(null);
                                    }
                                    else
                                    {
                                        ack(new Exception());
                                    }
                                }
                                else
                                {
                                    ack(new Exception());
                                }
                                break;
                            case ActionName.ShareGalleryES:
                                var obj4 = Json.Parse<QueueShareDataUnit<GallerySearch>>(queue.Data.ToString());
                                if (obj4 != null && obj4.ItemStreamDistribution != null && obj4.ListMedia != null)
                                    res = await StoreFactory.Create<DistributionSearchStore>().ShareGalleryAsync(obj4.ItemStreamDistribution, obj4.ListMedia, true);
                                if (res || obj4 == null || obj4.ItemStreamDistribution == null || obj4.ListMedia == null)
                                {
                                    ack(null);
                                }
                                else
                                {
                                    ack(new Exception());
                                }
                                break;
                            default:
                                ack(new Exception());
                                break;
                        }
                    }
                    else
                    {
                        ack(new Exception());
                    }
                }
                catch (Exception ex)
                {
                    ack(ex);
                    Logger.Sensitive(ex, ex.Message);
                }
            });
            ExchangeFactory.CreateStream(new EQConfiguration { Topic = TopicName.ES_BEAM, Recovery = true }, async (msg, ack) =>
            {
                try
                {
                    //TODO somethings
                    var queue = Json.Parse<Entities.Queue<object>>(msg.Payload.ToString());
                    var res = false;
                    if (queue != null && queue.Data != null)
                    {
                        switch (queue.Action)
                        {
                            case ActionName.AddBeamES:
                                var id1 = Json.Parse<long>(queue.Data.ToString());
                                if (id1 > 0)
                                {
                                    var obj1 = await StoreFactory.Create<BeamCacheStore>().GetBeamFullInfoAsync(id1);
                                    res = await StoreFactory.Create<BeamSearchStore>().AddAsync(obj1, true);
                                    if (res || obj1 == null)
                                    {
                                        ack(null);
                                    }
                                    else
                                    {
                                        ack(new Exception());
                                    }
                                }
                                else
                                {
                                    ack(new Exception());
                                }
                                break;
                            case ActionName.UpdateBeamES:
                                var id2 = Json.Parse<long>(queue.Data.ToString());
                                if (id2 > 0)
                                {
                                    var obj2 = await StoreFactory.Create<BeamCacheStore>().GetBeamFullInfoAsync(id2);
                                    res = await StoreFactory.Create<BeamSearchStore>().UpdateAsync(obj2, true);
                                    if (res || obj2 == null)
                                    {
                                        ack(null);
                                    }
                                    else
                                    {
                                        ack(new Exception());
                                    }
                                }
                                else
                                {
                                    ack(new Exception());
                                }
                                break;
                            case ActionName.UpdateStatusBeamES:
                                var id3 = Json.Parse<long>(queue.Data.ToString());
                                if (id3 > 0)
                                {
                                    var obj3 = await StoreFactory.Create<BeamCacheStore>().GetBeamFullInfoAsync(id3);
                                    res = await StoreFactory.Create<BeamSearchStore>().UpdateStatusAsync(obj3, true);
                                    if (res || obj3 == null)
                                    {
                                        ack(null);
                                    }
                                    else
                                    {
                                        ack(new Exception());
                                    }
                                }
                                else
                                {
                                    ack(new Exception());
                                }
                                break;
                            case ActionName.ShareBeamES:
                                var obj4 = Json.Parse<QueueShareDataUnit<BeamSearch>>(queue.Data.ToString());
                                if (obj4 != null && obj4.ItemStreamDistribution != null && obj4.ListMedia != null)
                                    res = await StoreFactory.Create<DistributionSearchStore>().ShareBeamAsync(obj4.ItemStreamDistribution, obj4.ListMedia, true);
                                if (res || obj4 == null || obj4.ItemStreamDistribution == null || obj4.ListMedia == null)
                                {
                                    ack(null);
                                }
                                else
                                {
                                    ack(new Exception());
                                }
                                break;
                            default:
                                ack(new Exception());
                                break;
                        }
                    }
                    else
                    {
                        ack(new Exception());
                    }
                }
                catch (Exception ex)
                {
                    ack(ex);
                    Logger.Sensitive(ex, ex.Message);
                }
            });

            ExchangeFactory.CreateStream(new EQConfiguration { Topic = TopicName.ES_NEWSONHOME, Recovery = true }, async (msg, ack) =>
            {
                try
                {
                    //TODO somethings
                    var queue = Json.Parse<Entities.Queue<object>>(msg.Payload.ToString());
                    var res = false;
                    if (queue != null && queue.Data != null)
                    {
                        switch (queue.Action)
                        {
                            case ActionName.AddNewsOnHomeES:
                                var obj1 = Json.Parse<NewsOnHome>(queue.Data.ToString());
                                if (obj1 != null)
                                    res = await StoreFactory.Create<NewsOnHomeSearchStore>().AddAsync(obj1, true);
                                if (res)
                                {
                                    ack(null);
                                }
                                else
                                {
                                    ack(new Exception());
                                }
                                break;
                            case ActionName.UpdateNewsOnHomeES:
                                var obj2 = Json.Parse<NewsOnHome>(queue.Data.ToString());
                                if (obj2 != null)
                                    res = await StoreFactory.Create<NewsOnHomeSearchStore>().UpdateAsync(obj2, true);
                                if (res || obj2 == null)
                                {
                                    ack(null);
                                }
                                else
                                {
                                    ack(new Exception());
                                }
                                break;
                            default:
                                ack(new Exception());
                                break;
                        }
                    }
                    else
                    {
                        ack(new Exception());
                    }
                }
                catch (Exception ex)
                {
                    ack(ex);
                    Logger.Sensitive(ex, ex.Message);
                }
            });

            ExchangeFactory.CreateStream(new EQConfiguration { Topic = TopicName.ES_NEWSCRAWLER, Recovery = true }, async (msg, ack) =>
            {
                try
                {
                    //TODO somethings
                    var queue = Json.Parse<Entities.Queue<object>>(msg.Payload.ToString());
                    var res = false;
                    if (queue != null && queue.Data != null)
                    {
                        switch (queue.Action)
                        {
                            case ActionName.AddNewsCrawlerES:
                                var obj1 = Json.Parse<List<NewsCrawler>>(queue.Data.ToString());
                                try
                                {
                                    if (obj1 != null)
                                        res = await StoreFactory.Create<NewsCrawlerSearchStore>().AddAsync(obj1, true);
                                    if (res)
                                    {
                                        ack(null);
                                    }
                                    else
                                    {
                                        ack(new Exception());
                                    }
                                }
                                catch (Exception ex)
                                {

                                    throw ex;
                                }
                                
                                break;
                            case ActionName.UpdateNewsCrawlerES:
                                var obj2 = Json.Parse<NewsCrawler>(queue.Data.ToString());
                                if (obj2 != null)
                                    res = await StoreFactory.Create<NewsCrawlerSearchStore>().UpdateAsync(obj2, true);
                                if (res || obj2 == null)
                                {
                                    ack(null);
                                }
                                else
                                {
                                    ack(new Exception());
                                }
                                break;
                            default:
                                ack(new Exception());
                                break;
                        }
                    }
                    else
                    {
                        ack(new Exception());
                    }
                }
                catch (Exception ex)
                {
                    ack(ex);
                    Logger.Sensitive(ex, ex.Message);
                }
            });

            ExchangeFactory.CreateStream(new EQConfiguration { Topic = TopicName.ES_NEWSCRAWLERCONFIG, Recovery = true }, async (msg, ack) =>
            {
                try
                {
                    //TODO somethings
                    var queue = Json.Parse<Entities.Queue<object>>(msg.Payload.ToString());
                    var res = false;
                    if (queue != null && queue.Data != null)
                    {
                        switch (queue.Action)
                        {
                            case ActionName.AddNewsCrawlerConfigES:
                                var obj1 = Json.Parse<NewCrawlerConfigurationSearch>(queue.Data.ToString());
                                if (obj1 != null)
                                    res = await StoreFactory.Create<NewsCrawlerConfigSearchStore>().AddConfigAsync(obj1, true);
                                if (res)
                                {
                                    ack(null);
                                }
                                else
                                {
                                    ack(new Exception());
                                }
                                break;
                            case ActionName.UpdateNewsCrawlerConfigES:
                                var obj2 = Json.Parse<NewCrawlerConfigurationSearch>(queue.Data.ToString());
                                if (obj2 != null)
                                    res = await StoreFactory.Create<NewsCrawlerConfigSearchStore>().UpdateConfigAsync(obj2, true);
                                if (res || obj2 == null)
                                {
                                    ack(null);
                                }
                                else
                                {
                                    ack(new Exception());
                                }
                                break;
                            default:
                                ack(new Exception());
                                break;
                        }
                    }
                    else
                    {
                        ack(new Exception());
                    }
                }
                catch (Exception ex)
                {
                    ack(ex);
                    Logger.Sensitive(ex, ex.Message);
                }
            });

            ExchangeFactory.CreateStream(new EQConfiguration { Topic = TopicName.ES_ACCOUNTASSIGNMENT, Recovery = true }, async (msg, ack) =>
            {
                try
                {
                    //TODO somethings
                    var queue = Json.Parse<Entities.Queue<object>>(msg.Payload.ToString());
                    var res = false;
                    if (queue != null && queue.Data != null)
                    {
                        switch (queue.Action)
                        {
                            case ActionName.AddAccountAssignmentES:
                                var obj1 = Json.Parse<AccountAssignment>(queue.Data.ToString());
                                if (obj1 != null)
                                    res = await StoreFactory.Create<AccountAssignmentSearchStore>().AddAsync(obj1, true);
                                if (res)
                                {
                                    ack(null);
                                }
                                else
                                {
                                    ack(new Exception());
                                }
                                break;
                            case ActionName.UpdateAccountAssignmentES:
                                var obj2 = Json.Parse<AccountAssignment>(queue.Data.ToString());
                                if (obj2 != null)
                                    res = await StoreFactory.Create<AccountAssignmentSearchStore>().UpdateAsync(obj2, true);
                                if (res || obj2 == null)
                                {
                                    ack(null);
                                }
                                else
                                {
                                    ack(new Exception());
                                }
                                break;
                            case ActionName.RemoveAccountAssignmentES:
                                var obj3 = Json.Parse<AccountAssignment>(queue.Data.ToString());
                                if (obj3 != null)
                                    res = await StoreFactory.Create<AccountAssignmentSearchStore>().RemoveAsync(obj3, true);
                                if (res || obj3 == null)
                                {
                                    ack(null);
                                }
                                else
                                {
                                    ack(new Exception());
                                }
                                break;
                            default:
                                ack(new Exception());
                                break;
                        }
                    }
                    else
                    {
                        ack(new Exception());
                    }
                }
                catch (Exception ex)
                {
                    ack(ex);
                    Logger.Sensitive(ex, ex.Message);
                }
            });
            #endregion

            #region stream Redis
            ExchangeFactory.CreateStream(new EQConfiguration { Topic = TopicName.REDIS_TEMPLATE, Recovery = true }, async (msg, ack) =>
            {
                try
                {
                    //TODO somethings
                    var queue = Json.Parse<Entities.Queue<object>>(msg.Payload.ToString());
                    var res = false;
                    if (queue != null && queue.Data != null)
                    {
                        switch (queue.Action)
                        {
                            case ActionName.AddTemplateRedis:
                                var obj1 = Json.Parse<Template>(queue.Data.ToString());
                                if (obj1 != null)
                                    res = await StoreFactory.Create<TemplateCacheStore>().AddAsync(obj1, true);
                                if (res || obj1 == null)
                                {
                                    ack(null);
                                }
                                else
                                {
                                    ack(new Exception());
                                }
                                break;
                            case ActionName.UpdateTemplateRedis:
                                var obj2 = Json.Parse<Template>(queue.Data.ToString());
                                if (obj2 != null)
                                    res = await StoreFactory.Create<TemplateCacheStore>().UpdateAsync(obj2, true);
                                if (res || obj2 == null)
                                {
                                    ack(null);
                                }
                                else
                                {
                                    ack(new Exception());
                                }
                                break;
                            default:
                                ack(new Exception());
                                break;
                        }
                    }
                }
                catch (Exception ex)
                {
                    ack(ex);
                    Logger.Sensitive(ex, ex.Message);
                }
            });
            #endregion

            #region stream SQL
            ExchangeFactory.CreateStream(new EQConfiguration { Topic = TopicName.SQL_ARTICLE, Recovery = true }, async (msg, ack) =>
            {
                try
                {
                    //TODO somethings
                    var queue = Json.Parse<Entities.Queue<object>>(msg.Payload.ToString());
                    var res = false;
                    if (queue != null && queue.Data != null)
                    {
                        switch (queue.Action)
                        {
                            case ActionName.AddArticleSQL:
                                var id1 = Json.Parse<long>(queue.Data.ToString());
                                if (id1 > 0)
                                {
                                    var obj1 = await StoreFactory.Create<ArticleCacheStore>().GetArticleFullInfoAsync(id1);
                                    res = await StoreFactory.Create<ArticleDataStore>().AddAsync(obj1, true);
                                    if (res)
                                    {
                                        ack(null);
                                    }
                                    else
                                    {
                                        ack(new Exception());
                                    }
                                }
                                else
                                {
                                    ack(new Exception());
                                }
                                break;
                            case ActionName.UpdateArticleSQL:
                                var id2 = Json.Parse<long>(queue.Data.ToString());
                                if (id2 > 0)
                                {
                                    var obj2 = await StoreFactory.Create<ArticleCacheStore>().GetArticleFullInfoAsync(id2);
                                    res = await StoreFactory.Create<ArticleDataStore>().UpdateAsync(obj2, true);
                                    if (res || obj2 == null)
                                    {
                                        ack(null);
                                    }
                                    else
                                    {
                                        ack(new Exception());
                                    }
                                }
                                else
                                {
                                    ack(new Exception());
                                }
                                break;
                            case ActionName.UpdateStatusArticleSQL:
                                var id3 = Json.Parse<long>(queue.Data.ToString());
                                if (id3 > 0)
                                {
                                    var obj3 = await StoreFactory.Create<ArticleCacheStore>().GetArticleFullInfoAsync(id3);
                                    res = await StoreFactory.Create<ArticleDataStore>().UpdateStatusAsync(obj3, true);
                                    if (res || obj3 == null)
                                    {
                                        ack(null);
                                    }
                                    else
                                    {
                                        ack(new Exception());
                                    }
                                }
                                else
                                {
                                    ack(new Exception());
                                }
                                break;
                            case ActionName.ShareArticleSQL:
                                var obj4 = Json.Parse<QueueData<ItemStreamDistribution, List<NewsDistribution>, bool, object>>(queue.Data.ToString());
                                if (obj4 != null && obj4.Data1 != null && obj4.Data2 != null)
                                    res = await StoreFactory.Create<DistributionDataStore>().ShareMediaAsync(obj4.Data1, obj4.Data2, obj4.Data3);
                                if (res || obj4 == null || obj4.Data1 == null || obj4.Data2 == null)
                                {
                                    ack(null);
                                }
                                else
                                {
                                    ack(new Exception());
                                }
                                break;
                            default:
                                ack(new Exception());
                                break;
                        }
                    }
                }
                catch (Exception ex)
                {
                    ack(ex);
                    Logger.Sensitive(ex, ex.Message);
                }
            });

            ExchangeFactory.CreateStream(new EQConfiguration { Topic = TopicName.SQL_POST, Recovery = true }, async (msg, ack) =>
            {
                try
                {
                    //TODO somethings
                    var queue = Json.Parse<Entities.Queue<object>>(msg.Payload.ToString());
                    var res = false;
                    if (queue != null && queue.Data != null)
                    {
                        switch (queue.Action)
                        {
                            case ActionName.AddPostSQL:
                                var id1 = Json.Parse<long>(queue.Data.ToString());
                                if (id1 > 0)
                                {
                                    var obj1 = await StoreFactory.Create<PostCacheStore>().GetPostFullInfoAsync(id1);
                                    res = await StoreFactory.Create<PostDataStore>().AddAsync(obj1, true);
                                    if (res || obj1 == null)
                                    {
                                        ack(null);
                                    }
                                    else
                                    {
                                        ack(new Exception());
                                    }
                                }
                                else
                                {
                                    ack(new Exception());
                                }
                                break;
                            case ActionName.UpdatePostSQL:
                                var id2 = Json.Parse<long>(queue.Data.ToString());
                                if (id2 > 0)
                                {
                                    var obj2 = await StoreFactory.Create<PostCacheStore>().GetPostFullInfoAsync(id2);
                                    res = await StoreFactory.Create<PostDataStore>().UpdateAsync(obj2, true);
                                    if (res || obj2 == null)
                                    {
                                        ack(null);
                                    }
                                    else
                                    {
                                        ack(new Exception());
                                    }
                                }
                                else
                                {
                                    ack(new Exception());
                                }
                                break;
                            case ActionName.UpdatePostStatusSQL:
                                var id3 = Json.Parse<long>(queue.Data.ToString());
                                if (id3 > 0)
                                {
                                    var obj3 = await StoreFactory.Create<PostCacheStore>().GetPostFullInfoAsync(id3);
                                    res = await StoreFactory.Create<PostDataStore>().UpdateStatusAsync(obj3, true);
                                    if (res || obj3 == null)
                                    {
                                        ack(null);
                                    }
                                    else
                                    {
                                        ack(new Exception());
                                    }
                                }
                                else
                                {
                                    ack(new Exception());
                                }
                                break;
                            case ActionName.SharePostSQL:
                                var obj4 = Json.Parse<QueueData<ItemStreamDistribution, List<NewsDistribution>, bool, object>>(queue.Data.ToString());
                                if (obj4 != null && obj4.Data1 != null && obj4.Data2 != null)
                                    res = await StoreFactory.Create<DistributionDataStore>().ShareMediaAsync(obj4.Data1, obj4.Data2, obj4.Data3);
                                if (res || obj4 == null || obj4.Data1 == null || obj4.Data2 == null)
                                {
                                    ack(null);
                                }
                                else
                                {
                                    ack(new Exception());
                                }
                                break;
                            default:
                                ack(new Exception());
                                break;
                        }
                    }
                }
                catch (Exception ex)
                {
                    ack(ex);
                    Logger.Sensitive(ex, ex.Message);
                }
            });

            ExchangeFactory.CreateStream(new EQConfiguration { Topic = TopicName.SQL_SHARE_LINK, Recovery = true }, async (msg, ack) =>
            {
                try
                {
                    //TODO somethings
                    var queue = Json.Parse<Entities.Queue<object>>(msg.Payload.ToString());
                    var res = false;
                    if (queue != null && queue.Data != null)
                    {
                        switch (queue.Action)
                        {
                            case ActionName.AddShareLinkSQL:
                                var id1 = Json.Parse<long>(queue.Data.ToString());
                                if (id1 > 0)
                                {
                                    var obj1 = await StoreFactory.Create<ShareLinkCacheStore>().GetFullInfoAsync(id1);
                                    res = await StoreFactory.Create<ShareLinkDataStore>().AddAsync(obj1, true);
                                    if (res || obj1 == null)
                                    {
                                        ack(null);
                                    }
                                    else
                                    {
                                        ack(new Exception());
                                    }
                                }
                                else
                                {
                                    ack(new Exception());
                                }
                                break;
                            case ActionName.UpdateShareLinkSQL:
                                var id2 = Json.Parse<long>(queue.Data.ToString());
                                if (id2 > 0)
                                {
                                    var obj2 = await StoreFactory.Create<ShareLinkCacheStore>().GetFullInfoAsync(id2);
                                    res = await StoreFactory.Create<ShareLinkDataStore>().UpdateAsync(obj2, true);
                                    if (res || obj2 == null)
                                    {
                                        ack(null);
                                    }
                                    else
                                    {
                                        ack(new Exception());
                                    }
                                }
                                else
                                {
                                    ack(new Exception());
                                }
                                break;
                            case ActionName.UpdateShareLinkStatusSQL:
                                var id3 = Json.Parse<long>(queue.Data.ToString());
                                if (id3 > 0)
                                {
                                    var obj3 = await StoreFactory.Create<ShareLinkCacheStore>().GetFullInfoAsync(id3);
                                    res = await StoreFactory.Create<ShareLinkDataStore>().UpdateStatusAsync(obj3, true);
                                    if (res || obj3 == null)
                                    {
                                        ack(null);
                                    }
                                    else
                                    {
                                        ack(new Exception());
                                    }
                                }
                                else
                                {
                                    ack(new Exception());
                                }
                                break;
                            case ActionName.ShareShareLinkSQL:
                                var obj4 = Json.Parse<QueueData<ItemStreamDistribution, List<NewsDistribution>, bool, object>>(queue.Data.ToString());
                                if (obj4 != null && obj4.Data1 != null && obj4.Data2 != null)
                                    res = await StoreFactory.Create<DistributionDataStore>().ShareMediaAsync(obj4.Data1, obj4.Data2, obj4.Data3);
                                if (res || obj4 == null || obj4.Data1 == null || obj4.Data2 == null)
                                {
                                    ack(null);
                                }
                                else
                                {
                                    ack(new Exception());
                                }
                                break;
                            default:
                                ack(new Exception());
                                break;
                        }
                    }
                }
                catch (Exception ex)
                {
                    ack(ex);
                    Logger.Sensitive(ex, ex.Message);
                }
            });

            ExchangeFactory.CreateStream(new EQConfiguration { Topic = TopicName.SQL_VIDEO, Recovery = true }, async (msg, ack) =>
            {
                try
                {
                    //TODO somethings
                    var queue = Json.Parse<Entities.Queue<object>>(msg.Payload.ToString());
                    var res = false;
                    if (queue != null && queue.Data != null)
                    {
                        switch (queue.Action)
                        {
                            case ActionName.AddVideoSQL:
                                var id1 = Json.Parse<long>(queue.Data.ToString());
                                if (id1 > 0)
                                {
                                    var obj1 = await StoreFactory.Create<VideoCacheStore>().GetVideoFullInfoAsync(id1);
                                    res = await StoreFactory.Create<VideoDataStore>().AddAsync(obj1, true);
                                    if (res || obj1 == null)
                                    {
                                        ack(null);
                                    }
                                    else
                                    {
                                        ack(new Exception());
                                    }
                                }
                                else
                                {
                                    ack(new Exception());
                                }
                                break;
                            case ActionName.UpdateVideoSQL:
                                var id2 = Json.Parse<long>(queue.Data.ToString());
                                if (id2 > 0)
                                {
                                    var obj2 = await StoreFactory.Create<VideoCacheStore>().GetVideoFullInfoAsync(id2);
                                    res = await StoreFactory.Create<VideoDataStore>().UpdateAsync(obj2, true);
                                    if (res || obj2 == null)
                                    {
                                        ack(null);
                                    }
                                    else
                                    {
                                        ack(new Exception());
                                    }
                                }
                                else
                                {
                                    ack(new Exception());
                                }
                                break;
                            case ActionName.UpdateVideoStatusSQL:
                                var id3 = Json.Parse<long>(queue.Data.ToString());
                                if (id3 > 0)
                                {
                                    var obj3 = await StoreFactory.Create<VideoCacheStore>().GetVideoFullInfoAsync(id3);
                                    res = await StoreFactory.Create<VideoDataStore>().UpdateStatusAsync(obj3, true);
                                    if (res || obj3 == null)
                                    {
                                        ack(null);
                                    }
                                    else
                                    {
                                        ack(new Exception());
                                    }
                                }
                                else
                                {
                                    ack(new Exception());
                                }
                                break;
                            case ActionName.ShareVideoSQL:
                                var obj4 = Json.Parse<QueueData<ItemStreamDistribution, List<NewsDistribution>, bool, object>>(queue.Data.ToString());
                                if (obj4 != null && obj4.Data1 != null && obj4.Data2 != null)
                                    res = await StoreFactory.Create<DistributionDataStore>().ShareMediaAsync(obj4.Data1, obj4.Data2, obj4.Data3);
                                if (res || obj4 == null || obj4.Data1 == null || obj4.Data2 == null)
                                {
                                    ack(null);
                                }
                                else
                                {
                                    ack(new Exception());
                                }
                                break;
                            case ActionName.AddVideoToPlaylistSQL:
                                var obj5 = Json.Parse<QueueData<long, List<VideoPlaylist>, object, object>>(queue.Data.ToString());
                                if (obj5 != null && obj5.Data1 > 0 && obj5.Data2 != null)
                                    res = await StoreFactory.Create<VideoPlaylistDataStore>().AddVideoAsync(obj5.Data1, obj5.Data2, true);
                                if (res || obj5 == null || obj5.Data1 == 0 || obj5.Data2 == null)
                                {
                                    ack(null);
                                }
                                else
                                {
                                    ack(new Exception());
                                }
                                break;
                            default:
                                ack(new Exception());
                                break;
                        }
                    }
                }
                catch (Exception ex)
                {
                    ack(ex);
                    Logger.Sensitive(ex, ex.Message);
                }
            });

            ExchangeFactory.CreateStream(new EQConfiguration { Topic = TopicName.SQL_PLAYLIST, Recovery = true }, async (msg, ack) =>
            {
                try
                {
                    //TODO somethings
                    var queue = Json.Parse<Entities.Queue<object>>(msg.Payload.ToString());
                    var res = false;
                    if (queue != null && queue.Data != null)
                    {
                        switch (queue.Action)
                        {
                            case ActionName.AddVideoPlaylistSQL:
                                var id1 = Json.Parse<long>(queue.Data.ToString());
                                if (id1 > 0)
                                {
                                    var obj1 = await StoreFactory.Create<VideoPlaylistCacheStore>().GetVideoPlaylistFullInfoAsync(id1);
                                    res = await StoreFactory.Create<VideoPlaylistDataStore>().AddAsync(obj1, true);
                                    if (res || obj1 == null)
                                    {
                                        ack(null);
                                    }
                                    else
                                    {
                                        ack(new Exception());
                                    }
                                }
                                else
                                {
                                    ack(new Exception());
                                }
                                break;
                            case ActionName.UpdateVideoPlaylistSQL:
                                var id2 = Json.Parse<long>(queue.Data.ToString());
                                if (id2 > 0)
                                {
                                    var obj2 = await StoreFactory.Create<VideoPlaylistCacheStore>().GetVideoPlaylistFullInfoAsync(id2);
                                    res = await StoreFactory.Create<VideoPlaylistDataStore>().UpdateAsync(obj2, true);
                                    if (res || obj2 == null)
                                    {
                                        ack(null);
                                    }
                                    else
                                    {
                                        ack(new Exception());
                                    }
                                }
                                else
                                {
                                    ack(new Exception());
                                }
                                break;
                            case ActionName.UpdateVideoPlaylistStatusSQL:
                                var id3 = Json.Parse<long>(queue.Data.ToString());
                                if (id3 > 0)
                                {
                                    var obj3 = await StoreFactory.Create<VideoPlaylistCacheStore>().GetVideoPlaylistFullInfoAsync(id3);
                                    res = await StoreFactory.Create<VideoPlaylistDataStore>().UpdateStatusAsync(obj3, true);
                                    if (res || obj3 == null)
                                    {
                                        ack(null);
                                    }
                                    else
                                    {
                                        ack(new Exception());
                                    }
                                }
                                else
                                {
                                    ack(new Exception());
                                }
                                break;
                            case ActionName.ShareVideoPlaylistSQL:
                                var obj4 = Json.Parse<QueueData<ItemStreamDistribution, List<NewsDistribution>, bool, object>>(queue.Data.ToString());
                                if (obj4 != null && obj4.Data1 != null && obj4.Data2 != null)
                                    res = await StoreFactory.Create<DistributionDataStore>().ShareMediaAsync(obj4.Data1, obj4.Data2, obj4.Data3);
                                if (res || obj4 == null || obj4.Data1 == null || obj4.Data2 == null)
                                {
                                    ack(null);
                                }
                                else
                                {
                                    ack(new Exception());
                                }
                                break;
                            default:
                                ack(new Exception());
                                break;
                        }
                    }
                }
                catch (Exception ex)
                {
                    ack(ex);
                    Logger.Sensitive(ex, ex.Message);
                }
            });

            ExchangeFactory.CreateStream(new EQConfiguration { Topic = TopicName.SQL_TAG, Recovery = true }, async (msg, ack) =>
            {
                try
                {
                    //TODO somethings
                    var queue = Json.Parse<Entities.Queue<object>>(msg.Payload.ToString());
                    var res = false;
                    if (queue != null && queue.Data != null)
                    {
                        switch (queue.Action)
                        {
                            case ActionName.AddTagSQL:
                                var obj1 = Json.Parse<Tag>(queue.Data.ToString());
                                if (obj1 != null)
                                    res = await StoreFactory.Create<TagDataStore>().AddAsync(obj1, true);
                                if (res || obj1 == null)
                                {
                                    ack(null);
                                }
                                else
                                {
                                    ack(new Exception());
                                }
                                break;
                            case ActionName.UpdateTagSQL:
                                var obj2 = Json.Parse<Tag>(queue.Data.ToString());
                                if (obj2 != null)
                                    res = await StoreFactory.Create<TagDataStore>().UpdateAsync(obj2, true);
                                if (res || obj2 == null)
                                {
                                    ack(null);
                                }
                                else
                                {
                                    ack(new Exception());
                                }
                                break;
                            default:
                                ack(new Exception());
                                break;
                        }
                    }
                }
                catch (Exception ex)
                {
                    ack(ex);
                    Logger.Sensitive(ex, ex.Message);
                }
            });

            ExchangeFactory.CreateStream(new EQConfiguration { Topic = TopicName.SQL_MEDIA_UNIT, Recovery = true }, async (msg, ack) =>
            {
                try
                {
                    //TODO somethings
                    var queue = Json.Parse<Entities.Queue<object>>(msg.Payload.ToString());
                    var res = false;
                    if (queue != null && queue.Data != null)
                    {
                        switch (queue.Action)
                        {
                            case ActionName.AddMediaUnitSQL:
                                var id1 = Json.Parse<long>(queue.Data.ToString());
                                if (id1 > 0)
                                {
                                    var obj1 = await StoreFactory.Create<MediaUnitCacheStore>().GetMediaUnitFullInfoAsync(id1);
                                    res = await StoreFactory.Create<MediaUnitDataStore>().AddAsync(obj1, true);
                                    if (res || obj1 == null)
                                    {
                                        ack(null);
                                    }
                                    else
                                    {
                                        ack(new Exception());
                                    }
                                }
                                else
                                {
                                    ack(new Exception());
                                }
                                break;
                            case ActionName.UpdateMediaUnitSQL:
                                var id2 = Json.Parse<long>(queue.Data.ToString());
                                if (id2 > 0)
                                {
                                    var obj2 = await StoreFactory.Create<MediaUnitCacheStore>().GetMediaUnitFullInfoAsync(id2);
                                    res = await StoreFactory.Create<MediaUnitDataStore>().UpdateAsync(obj2, true);
                                    if (res || obj2 == null)
                                    {
                                        ack(null);
                                    }
                                    else
                                    {
                                        ack(new Exception());
                                    }
                                }
                                else
                                {
                                    ack(new Exception());
                                }
                                break;
                            case ActionName.UpdateStatusMediaUnitSQL:
                                var id3 = Json.Parse<long>(queue.Data.ToString());
                                if (id3 > 0)
                                {
                                    var obj3 = await StoreFactory.Create<MediaUnitCacheStore>().GetMediaUnitFullInfoAsync(id3);
                                    res = await StoreFactory.Create<MediaUnitDataStore>().UpdateStatusAsync(obj3, true);
                                    if (res || obj3 == null)
                                    {
                                        ack(null);
                                    }
                                    else
                                    {
                                        ack(new Exception());
                                    }
                                }
                                else
                                {
                                    ack(new Exception());
                                }
                                break;
                            case ActionName.ShareMediaUnitSQL:
                                var obj4 = Json.Parse<QueueShareDataUnit<MediaUnitSearch>>(queue.Data.ToString());
                                if (obj4 != null && obj4.ItemStreamDistribution != null && obj4.ListMedia != null)
                                    res = await StoreFactory.Create<DistributionDataStore>().ShareMediaUnitAsync(obj4.ItemStreamDistribution, obj4.ListMedia);
                                if (res || obj4 == null || obj4.ItemStreamDistribution == null || obj4.ListMedia == null)
                                {
                                    ack(null);
                                }
                                else
                                {
                                    ack(new Exception());
                                }
                                break;
                            default:
                                ack(new Exception());
                                break;
                        }
                    }
                }
                catch (Exception ex)
                {
                    ack(ex);
                    Logger.Sensitive(ex, ex.Message);
                }
            });

            ExchangeFactory.CreateStream(new EQConfiguration { Topic = TopicName.SQL_PHOTO, Recovery = true }, async (msg, ack) =>
            {
                try
                {
                    //TODO somethings
                    var queue = Json.Parse<Entities.Queue<object>>(msg.Payload.ToString());
                    var res = false;
                    if (queue != null && queue.Data != null)
                    {
                        switch (queue.Action)
                        {
                            case ActionName.AddPhotoSQL:
                                var id1 = Json.Parse<long>(queue.Data.ToString());
                                if (id1 > 0)
                                {
                                    var obj1 = await StoreFactory.Create<PhotoCacheStore>().GetPhotoFullInfoAsync(id1);
                                    res = await StoreFactory.Create<PhotoDataStore>().AddAsync(obj1, true);
                                    if (res || obj1 == null)
                                    {
                                        ack(null);
                                    }
                                    else
                                    {
                                        ack(new Exception());
                                    }
                                }
                                else
                                {
                                    ack(new Exception());
                                }
                                break;
                            case ActionName.UpdatePhotoSQL:
                                var id2 = Json.Parse<long>(queue.Data.ToString());
                                if (id2 > 0)
                                {
                                    var obj2 = await StoreFactory.Create<PhotoCacheStore>().GetPhotoFullInfoAsync(id2);
                                    res = await StoreFactory.Create<PhotoDataStore>().UpdateAsync(obj2, true);
                                    if (res || obj2 == null)
                                    {
                                        ack(null);
                                    }
                                    else
                                    {
                                        ack(new Exception());
                                    }
                                }
                                else
                                {
                                    ack(new Exception());
                                }
                                break;
                            case ActionName.UpdateStatusPhotoSQL:
                                var id3 = Json.Parse<long>(queue.Data.ToString());
                                if (id3 > 0)
                                {
                                    var obj3 = await StoreFactory.Create<PhotoCacheStore>().GetPhotoFullInfoAsync(id3);
                                    res = await StoreFactory.Create<PhotoDataStore>().UpdateStatusAsync(obj3, true);
                                    if (res || obj3 == null)
                                    {
                                        ack(null);
                                    }
                                    else
                                    {
                                        ack(new Exception());
                                    }
                                }
                                else
                                {
                                    ack(new Exception());
                                }
                                break;
                            case ActionName.SharePhotoSQL:
                                var obj4 = Json.Parse<QueueShareDataUnit<PhotoSearch>>(queue.Data.ToString());
                                if (obj4 != null && obj4.ItemStreamDistribution != null && obj4.ListMedia != null)
                                    res = await StoreFactory.Create<DistributionDataStore>().SharePhotoAsync(obj4.ItemStreamDistribution, obj4.ListMedia);
                                if (res || obj4 == null || obj4.ItemStreamDistribution == null || obj4.ListMedia == null)
                                {
                                    ack(null);
                                }
                                else
                                {
                                    ack(new Exception());
                                }
                                break;
                            case ActionName.AddPhotoToAlbumSQL:
                                var obj5 = Json.Parse<QueueData<long, List<Album>, object, object>>(queue.Data.ToString());
                                if (obj5 != null && obj5.Data1 > 0 && obj5.Data2 != null)
                                    res = await StoreFactory.Create<AlbumDataStore>().AddPhotoAsync(obj5.Data1, obj5.Data2, true);
                                if (res || obj5 == null || obj5.Data1 == 0 || obj5.Data2 == null)
                                {
                                    ack(null);
                                }
                                else
                                {
                                    ack(new Exception());
                                }
                                break;
                            default:
                                ack(new Exception());
                                break;
                        }
                    }
                }
                catch (Exception ex)
                {
                    ack(ex);
                    Logger.Sensitive(ex, ex.Message);
                }
            });

            ExchangeFactory.CreateStream(new EQConfiguration { Topic = TopicName.SQL_ACCOUNT, Recovery = true }, async (msg, ack) =>
            {
                try
                {
                    //TODO somethings
                    var queue = Json.Parse<Entities.Queue<object>>(msg.Payload.ToString());
                    var res = false;
                    if (queue != null && queue.Data != null)
                    {
                        switch (queue.Action)
                        {
                            case ActionName.AddAccountSQL:
                                var obj1 = Json.Parse<Account>(queue.Data.ToString());
                                if (obj1 != null)
                                    res = await StoreFactory.Create<AccountDataStore>().AddAsync(obj1, true);
                                if (res || obj1 == null)
                                {
                                    ack(null);
                                }
                                else
                                {
                                    ack(new Exception());
                                }
                                break;
                            case ActionName.UpdateAccountSQL:
                                var dataOld = Json.Parse<QueueData<AccountSearch, UserProfile, object, object>>(queue.Data.ToString());
                                if (dataOld != null)
                                {
                                    var accountOld = dataOld?.Data1;//.GetValue(dataOld, null);
                                                                    //var accountOld = dataOld.GetType().GetProperty("account").GetValue(dataOld, null) as AccountSearch;
                                    var profileOld = dataOld?.Data2;
                                    res = await StoreFactory.Create<AccountDataStore>().UpdateAsync(accountOld, profileOld, true);
                                    if (res || dataOld == null || accountOld == null || profileOld == null)
                                    {
                                        ack(null);
                                    }
                                    else
                                    {
                                        ack(new Exception());
                                    }
                                }
                                else
                                {
                                    ack(null);
                                }
                                break;
                            case ActionName.AddOfficialAccountSQL:
                                var dataAll = Json.Parse<QueueData<Account, UserProfile, AccountMember, object>>(queue.Data.ToString());
                                if (dataAll != null)
                                {
                                    var accountOld2 = dataAll.Data1;
                                    accountOld2.Type = (int)AccountType.Official;
                                    var profileOld2 = dataAll.Data2;
                                    var accountMember = dataAll.Data3;
                                    res = await StoreFactory.Create<AccountDataStore>().AddOfficialAsync(accountOld2, profileOld2, accountMember, true);
                                    if (res || dataAll == null || accountOld2 == null || profileOld2 == null || accountMember == null)
                                    {
                                        ack(null);
                                    }
                                    else
                                    {
                                        ack(new Exception());
                                    }
                                }
                                else
                                {
                                    ack(null);
                                }
                                break;
                            case ActionName.AddProfileSQL:
                                var dataAll2 = Json.Parse<QueueData<Account, UserProfile, object, object>>(queue.Data.ToString());
                                if (dataAll2 != null)
                                {
                                    var profileOld3 = dataAll2.Data2;
                                    var accountOld3 = dataAll2.Data1;
                                    res = await StoreFactory.Create<AccountDataStore>().SaveProfileAsync(profileOld3, accountOld3, true);
                                    if (res || profileOld3 == null || accountOld3 == null)
                                    {
                                        ack(null);
                                    }
                                    else
                                    {
                                        ack(new Exception());
                                    }
                                }
                                else
                                {
                                    ack(null);
                                }
                                break;
                            case ActionName.AcceptOrRejectSQL:
                                var obj2 = Json.Parse<AccountSearch>(queue.Data.ToString());
                                if (obj2 != null)
                                    res = await StoreFactory.Create<AccountDataStore>().AcceptOrRejectAsync(obj2, true);
                                if (res || obj2 == null)
                                {
                                    ack(null);
                                }
                                else
                                {
                                    ack(new Exception());
                                }
                                break;
                            case ActionName.UpdateAccountStatusSQL:
                                var dataAll3Id = Json.Parse<Account>(queue.Data.ToString());
                                if (dataAll3Id != null && dataAll3Id.Id > 0)
                                {
                                    var accDb = await StoreFactory.Create<AccountCacheStore>().GetAccountSearchByIdAsync(dataAll3Id.Id);
                                    if (accDb != null)
                                    {
                                        res = await StoreFactory.Create<AccountDataStore>().UpdateStatusAsync(accDb, true);
                                        if (res)
                                        {
                                            ack(null);
                                        }
                                        else
                                        {
                                            ack(new Exception());
                                        }
                                    }
                                    else
                                    {
                                        ack(new Exception());
                                    }
                                }
                                else
                                {
                                    ack(new Exception());
                                }
                                break;
                            case ActionName.AddAccountMemberSQL:
                                var obj3 = Json.Parse<AccountMember>(queue.Data.ToString());
                                if (obj3 != null)
                                    res = await StoreFactory.Create<AccountDataStore>().AddAccountMemberAsync(obj3, true);
                                if (res || obj3 == null)
                                {
                                    ack(null);
                                }
                                else
                                {
                                    ack(new Exception());
                                }
                                break;
                            case ActionName.UpdateAccountMemberSQL:
                                var obj4 = Json.Parse<AccountMember>(queue.Data.ToString());
                                if (obj4 != null)
                                    res = await StoreFactory.Create<AccountDataStore>().UpdateAccountMemberAsync(obj4, true);
                                if (res || obj4 == null)
                                {
                                    ack(null);
                                }
                                else
                                {
                                    ack(new Exception());
                                }
                                break;
                            case ActionName.RemoveMemberSQL:
                                var dataAll5 = Json.Parse<QueueData<long, long, object, object>>(queue.Data.ToString());
                                if (dataAll5 != null)
                                {
                                    var accountId5 = dataAll5.Data1;
                                    var memberId5 = dataAll5.Data2;
                                    res = await StoreFactory.Create<AccountDataStore>().RemoveMemberAsync(accountId5, memberId5, true);
                                    if (res)
                                    {
                                        ack(null);
                                    }
                                    else
                                    {
                                        ack(new Exception());
                                    }
                                }
                                else
                                {
                                    ack(null);
                                }
                                break;
                            case ActionName.RemoveMemberOnAllPageSQL:
                                var data = Json.Parse<QueueData<IEnumerable<long>, long, object, object>>(queue.Data.ToString());
                                if (data != null)
                                {
                                    res = await StoreFactory.Create<AccountDataStore>().RemoveMemberOnAllPageAsync(data.Data1, data.Data2, true);
                                    if (res)
                                    {
                                        ack(null);
                                    }
                                    else
                                    {
                                        ack(new Exception());
                                    }
                                }
                                else
                                {
                                    ack(null);
                                }
                                break;
                            case ActionName.LockOrUnlockMemberSQL:
                                var data2 = Json.Parse<AccountMember>(queue.Data.ToString());
                                if (data2 != null)
                                {
                                    res = await StoreFactory.Create<AccountDataStore>().LockOrUnlockMemberAsync(data2, true);
                                    if (res)
                                    {
                                        ack(null);
                                    }
                                    else
                                    {
                                        ack(new Exception());
                                    }
                                }
                                else
                                {
                                    ack(null);
                                }
                                break;
                            case ActionName.LockOrUnlockMemberOnAllPageSQL:
                                var data3 = Json.Parse<IEnumerable<AccountMember>>(queue.Data.ToString());
                                if (data3 != null)
                                {
                                    res = await StoreFactory.Create<AccountDataStore>().LockOrUnlockMemberOnAllPageAsync(data3.ToArray(), true);
                                    if (res)
                                    {
                                        ack(null);
                                    }
                                    else
                                    {
                                        ack(new Exception());
                                    }
                                }
                                else
                                {
                                    ack(null);
                                }
                                break;
                            case ActionName.UpdateAccountTypeSQL:
                                var accData = Json.Parse<Account>(queue.Data.ToString());
                                if (accData != null)
                                {
                                    res = await StoreFactory.Create<AccountDataStore>().UpdateAccountTypeAsync(accData, true);
                                    if (res || accData == null)
                                    {
                                        ack(null);
                                    }
                                    else
                                    {
                                        ack(new Exception());
                                    }
                                }
                                else
                                {
                                    ack(null);
                                }
                                break;
                            case ActionName.UpdateAccountClassSQL:
                                var accData2 = Json.Parse<Account>(queue.Data.ToString());
                                if (accData2 != null)
                                {
                                    res = await StoreFactory.Create<AccountDataStore>().UpdateAccountTypeAsync(accData2, true);
                                    if (res)
                                    {
                                        ack(null);
                                    }
                                    else
                                    {
                                        ack(new Exception());
                                    }
                                }
                                else
                                {
                                    ack(null);
                                }
                                break;
                            case ActionName.ConfigPageSQL:
                                var obj8 = Json.Parse<QueueData<long, int, long, object>>(queue.Data.ToString());
                                if (obj8 != null && obj8.Data1 > 0)
                                {
                                    var officer = await SecurityRepository.GetByIdAsync(obj8.Data1);
                                    if (officer.Mode != obj8.Data2)
                                    {
                                        res = await StoreFactory.Create<AccountDataStore>().ConfigPageAsync(officer, true);
                                        if (res)
                                        {
                                            ack(null);
                                        }
                                        else
                                        {
                                            ack(new Exception());
                                        }
                                    }
                                    else
                                    {
                                        ack(null);
                                    }
                                }
                                else
                                {
                                    ack(null);
                                }
                                break;
                            case ActionName.ChangePageOwnerSQL:
                                var obj9 = Json.Parse<QueueData<long, long, long, object>>(queue.Data.ToString());
                                if (obj9 != null && obj9.Data1 > 0 && obj9.Data2 > 0 && obj9.Data3 > 0)
                                {
                                    res = await StoreFactory.Create<AccountDataStore>().ChangeOwnerAsync(obj9.Data1, obj9.Data2, obj9.Data3);
                                    if (res)
                                    {
                                        ack(null);
                                    }
                                    else
                                    {
                                        ack(new Exception());
                                    }
                                }
                                else
                                {
                                    ack(null);
                                }
                                break;
                            case ActionName.SetProfileIsPageOnAppSQL:
                                var data10 = Json.Parse<long>(queue.Data.ToString());
                                if (data10 > 0)
                                {
                                    var account = await StoreFactory.Create<AccountCacheStore>().GetByIdAsync(data10);
                                    if (account != null)
                                    {
                                        res = await StoreFactory.Create<AccountDataStore>().SetProfileIsPageOnAppAsync(account);
                                        if (res)
                                        {
                                            ack(null);
                                        }
                                        else
                                        {
                                            ack(new Exception());
                                        }
                                    }
                                    else
                                    {
                                        ack(new Exception());
                                    }
                                }
                                else
                                {
                                    ack(null);
                                }
                                break;
                            case ActionName.UpdateProfileUrlSql:
                                var data11 = Json.Parse<long>(queue.Data.ToString());
                                if (data11 > 0)
                                {
                                    var profile = await StoreFactory.Create<AccountCacheStore>().GetProfileAsync(data11);
                                    if (profile != null)
                                    {
                                        res = await StoreFactory.Create<AccountDataStore>().UpdateProfileUrlAsync(profile);
                                        if (res)
                                        {
                                            ack(null);
                                        }
                                        else
                                        {
                                            ack(new Exception());
                                        }
                                    }
                                    else
                                    {
                                        ack(new Exception());
                                    }
                                }
                                else
                                {
                                    ack(null);
                                }
                                break;
                            default:
                                ack(new Exception());
                                break;
                        }
                    }
                }
                catch (Exception ex)
                {
                    ack(ex);
                    Logger.Sensitive(ex, ex.Message);
                }
            });

            ExchangeFactory.CreateStream(new EQConfiguration { Topic = TopicName.SQL_ITEMSTREAM, Recovery = true }, async (msg, ack) =>
            {
                try
                {
                    //TODO somethings
                    var queue = Json.Parse<Entities.Queue<object>>(msg.Payload.ToString());
                    var res = false;
                    if (queue != null && queue.Data != null)
                    {
                        switch (queue.Action)
                        {
                            case ActionName.AcceptItemStreamSQL:
                                var obj1 = Json.Parse<ItemStreamDistribution>(queue.Data.ToString());
                                if (obj1 != null)
                                    res = await StoreFactory.Create<ArticleDataStore>().AcceptAsync(obj1, true);
                                if (res || obj1 == null)
                                {
                                    ack(null);
                                }
                                else
                                {
                                    ack(new Exception());
                                }
                                break;
                            case ActionName.RejectItemStreamSQL:
                                var obj2 = Json.Parse<NewsDistribution>(queue.Data.ToString());
                                if (obj2 != null)
                                    res = await StoreFactory.Create<ArticleDataStore>().RejectAsync(obj2.ItemStreamId, true);
                                if (res || obj2 == null)
                                {
                                    ack(null);
                                }
                                else
                                {
                                    ack(new Exception());
                                }
                                break;
                            default:
                                ack(new Exception());
                                break;
                        }
                    }
                }
                catch (Exception ex)
                {
                    ack(ex);
                    Logger.Sensitive(ex, ex.Message);
                }
            });

            ExchangeFactory.CreateStream(new EQConfiguration { Topic = TopicName.SQL_TEMPLATE, Recovery = true }, async (msg, ack) =>
            {
                try
                {
                    //TODO somethings
                    var queue = Json.Parse<Entities.Queue<object>>(msg.Payload.ToString());
                    var res = false;
                    if (queue != null && queue.Data != null)
                    {
                        switch (queue.Action)
                        {
                            case ActionName.UpdateStatusTemplateSQL:
                                var obj1 = Json.Parse<Template>(queue.Data.ToString());
                                if (obj1 != null)
                                    res = await StoreFactory.Create<TemplateDataStore>().UpdateStatusAsync(obj1, true);
                                if (res || obj1 == null)
                                {
                                    ack(null);
                                }
                                else
                                {
                                    ack(new Exception());
                                }
                                break;

                            default:
                                ack(new Exception());
                                break;
                        }
                    }
                }
                catch (Exception ex)
                {
                    ack(ex);
                    Logger.Sensitive(ex, ex.Message);
                }
            });

            ExchangeFactory.CreateStream(new EQConfiguration { Topic = TopicName.SQL_DISTRIBUTOR, Recovery = true }, async (msg, ack) =>
            {
                try
                {
                    //TODO somethings
                    var queue = Json.Parse<Entities.Queue<object>>(msg.Payload.ToString());
                    var res = false;
                    if (queue != null && queue.Data != null)
                    {
                        switch (queue.Action)
                        {
                            case ActionName.AddDistributionSQL:
                                var obj1 = Json.Parse<Distributor>(queue.Data.ToString());
                                if (obj1 != null)
                                    res = await StoreFactory.Create<DistributionDataStore>().AddAsync(obj1, true);
                                if (res || obj1 == null)
                                {
                                    ack(null);
                                }
                                else
                                {
                                    ack(new Exception());
                                }
                                break;
                            case ActionName.UpdateDistributionSQL:
                                var obj2 = Json.Parse<Distributor>(queue.Data.ToString());
                                if (obj2 != null)
                                    res = await StoreFactory.Create<DistributionDataStore>().UpdateAsync(obj2, true);
                                if (res || obj2 == null)
                                {
                                    ack(null);
                                }
                                else
                                {
                                    ack(new Exception());
                                }
                                break;
                            default:
                                ack(new Exception());
                                break;
                        }
                    }
                }
                catch (Exception ex)
                {
                    ack(ex);
                    Logger.Sensitive(ex, ex.Message);
                }
            });

            ExchangeFactory.CreateStream(new EQConfiguration { Topic = TopicName.SQL_BOARD, Recovery = true }, async (msg, ack) =>
            {
                try
                {
                    //TODO somethings
                    var queue = Json.Parse<Entities.Queue<object>>(msg.Payload.ToString());
                    var res = false;
                    if (queue != null && queue.Data != null)
                    {
                        switch (queue.Action)
                        {
                            case ActionName.AddBoardSQL:
                                var id1 = Json.Parse<long>(queue.Data.ToString());
                                if (id1 > 0)
                                {
                                    var obj1 = await StoreFactory.Create<BoardCacheStore>().GetBoardSearchByIdAsync(id1);
                                    res = await StoreFactory.Create<BoardDataStore>().AddAsync(obj1, true);
                                    if (res || obj1 == null)
                                    {
                                        ack(null);
                                    }
                                    else
                                    {
                                        ack(new Exception());
                                    }
                                }
                                else
                                {
                                    ack(new Exception());
                                }
                                break;
                            case ActionName.UpdateBoardSQL:
                                var id2 = Json.Parse<long>(queue.Data.ToString());
                                if (id2 > 0)
                                {
                                    var obj2 = await StoreFactory.Create<BoardCacheStore>().GetBoardSearchByIdAsync(id2);
                                    res = await StoreFactory.Create<BoardDataStore>().UpdateAsync(obj2, true);
                                    if (res || obj2 == null)
                                    {
                                        ack(null);
                                    }
                                    else
                                    {
                                        ack(new Exception());
                                    }
                                }
                                else
                                {
                                    ack(new Exception());
                                }
                                break;
                            case ActionName.UpdateMediaSQL:
                                var obj = Json.Parse<QueueData<IEnumerable<long>, IEnumerable<long>, Board, object>>(queue.Data.ToString());
                                if (obj != null && obj.Data3 != null && (obj.Data1 != null || obj.Data2 != null))
                                {
                                    res = await StoreFactory.Create<BoardDataStore>().UpdateMediaAsync(obj.Data1, obj.Data2, obj.Data3, true);
                                    if (res)
                                    {
                                        ack(null);
                                    }
                                    else
                                    {
                                        ack(new Exception());
                                    }
                                }
                                else
                                {
                                    ack(new Exception());
                                }
                                break;
                            case ActionName.AddNewsToBoardSQL:
                                var queueData1 = Json.Parse<QueueData<long, string, long, object>>(queue.Data.ToString());
                                if (queueData1 != null)
                                {
                                    res = await StoreFactory.Create<BoardDataStore>().AddNewsAsync(queueData1.Data1, queueData1.Data2, queueData1.Data3, true);
                                    if (res)
                                    {
                                        ack(null);
                                    }
                                    else
                                    {
                                        ack(new Exception());
                                    }
                                }
                                else
                                {
                                    ack(new Exception());
                                }
                                break;
                            case ActionName.DeleteNewsInBoardSQL:
                                var queueData2 = Json.Parse<QueueData<long, long, string, object>>(queue.Data.ToString());
                                if (queueData2 != null)
                                {
                                    res = await StoreFactory.Create<BoardDataStore>().DeleteNewsAsync(queueData2.Data1, queueData2.Data2, queueData2.Data3, true);
                                    if (res)
                                    {
                                        ack(null);
                                    }
                                    else
                                    {
                                        ack(new Exception());
                                    }
                                }
                                else
                                {
                                    ack(new Exception());
                                }
                                break;
                            case ActionName.InsertMediaSQL:
                                var queueData3 = Json.Parse<QueueData<long, List<Board>, object, object>>(queue.Data.ToString());
                                if (queueData3 != null && queueData3.Data1 > 0 && queueData3.Data2 != null)
                                {
                                    res = await StoreFactory.Create<BoardDataStore>().InsertMediaAsync(queueData3.Data1, queueData3.Data2, true);
                                    if (res)
                                    {
                                        ack(null);
                                    }
                                    else
                                    {
                                        ack(new Exception());
                                    }
                                }
                                else
                                {
                                    ack(new Exception());
                                }
                                break;
                            default:
                                ack(new Exception());
                                break;
                        }
                    }
                    else
                    {
                        ack(new Exception());
                    }
                }
                catch (Exception ex)
                {
                    ack(ex);
                    Logger.Sensitive(ex, ex.Message);
                }
            });

            ExchangeFactory.CreateStream(new EQConfiguration { Topic = TopicName.SQL_ALBUM, Recovery = true }, async (msg, ack) =>
            {
                try
                {
                    //TODO somethings
                    var queue = Json.Parse<Entities.Queue<object>>(msg.Payload.ToString());
                    var res = false;
                    if (queue != null && queue.Data != null)
                    {
                        switch (queue.Action)
                        {
                            case ActionName.AddAlbumSQL:
                                var id1 = Json.Parse<long>(queue.Data.ToString());
                                if (id1 > 0)
                                {
                                    var obj1 = await StoreFactory.Create<AlbumCacheStore>().GetAlbumFullInfoAsync(id1);
                                    res = await StoreFactory.Create<AlbumDataStore>().AddAsync(obj1, true);
                                    if (res || obj1 == null)
                                    {
                                        ack(null);
                                    }
                                    else
                                    {
                                        ack(new Exception());
                                    }
                                }
                                else
                                {
                                    ack(new Exception());
                                }
                                break;
                            case ActionName.UpdateAlbumSQL:
                                var id2 = Json.Parse<long>(queue.Data.ToString());
                                if (id2 > 0)
                                {
                                    var obj2 = await StoreFactory.Create<AlbumCacheStore>().GetAlbumFullInfoAsync(id2);
                                    res = await StoreFactory.Create<AlbumDataStore>().UpdateAsync(obj2, true);
                                    if (res || obj2 == null)
                                    {
                                        ack(null);
                                    }
                                    else
                                    {
                                        ack(new Exception());
                                    }
                                }
                                else
                                {
                                    ack(new Exception());
                                }
                                break;
                            case ActionName.UpdateStatusAlbumSQL:
                                var id3 = Json.Parse<long>(queue.Data.ToString());
                                if (id3 > 0)
                                {
                                    var obj3 = await StoreFactory.Create<AlbumCacheStore>().GetAlbumFullInfoAsync(id3);
                                    res = await StoreFactory.Create<AlbumDataStore>().UpdateStatusAsync(obj3, true);
                                    if (res || obj3 == null)
                                    {
                                        ack(null);
                                    }
                                    else
                                    {
                                        ack(new Exception());
                                    }
                                }
                                else
                                {
                                    ack(new Exception());
                                }
                                break;
                            case ActionName.ShareAlbumSQL:
                                var obj4 = Json.Parse<QueueShareDataUnit<AlbumSearch>>(queue.Data.ToString());
                                if (obj4 != null && obj4.ItemStreamDistribution != null && obj4.ListMedia != null)
                                    res = await StoreFactory.Create<DistributionDataStore>().ShareAlbumAsync(obj4.ItemStreamDistribution, obj4.ListMedia);
                                if (res || obj4 == null || obj4.ItemStreamDistribution == null || obj4.ListMedia == null)
                                {
                                    ack(null);
                                }
                                else
                                {
                                    ack(new Exception());
                                }
                                break;

                            default:
                                ack(new Exception());
                                break;
                        }
                    }
                }
                catch (Exception ex)
                {
                    ack(ex);
                    Logger.Sensitive(ex, ex.Message);
                }
            });
            ExchangeFactory.CreateStream(new EQConfiguration { Topic = TopicName.SQL_GALLERY, Recovery = true }, async (msg, ack) =>
            {
                try
                {
                    //TODO somethings
                    var queue = Json.Parse<Entities.Queue<object>>(msg.Payload.ToString());
                    var res = false;
                    if (queue != null && queue.Data != null)
                    {
                        switch (queue.Action)
                        {
                            case ActionName.AddGallerySQL:
                                var id1 = Json.Parse<long>(queue.Data.ToString());
                                if (id1 > 0)
                                {
                                    var obj1 = await StoreFactory.Create<GalleryCacheStore>().GetAlbumFullInfoAsync(id1);
                                    res = await StoreFactory.Create<GalleryDataStore>().AddAsync(obj1, true);
                                    if (res || obj1 == null)
                                    {
                                        ack(null);
                                    }
                                    else
                                    {
                                        ack(new Exception());
                                    }
                                }
                                else
                                {
                                    ack(new Exception());
                                }
                                break;
                            case ActionName.UpdateGallerySQL:
                                var id2 = Json.Parse<long>(queue.Data.ToString());
                                if (id2 > 0)
                                {
                                    var obj2 = await StoreFactory.Create<GalleryCacheStore>().GetAlbumFullInfoAsync(id2);
                                    res = await StoreFactory.Create<GalleryDataStore>().UpdateAsync(obj2, true);
                                    if (res || obj2 == null)
                                    {
                                        ack(null);
                                    }
                                    else
                                    {
                                        ack(new Exception());
                                    }
                                }
                                else
                                {
                                    ack(new Exception());
                                }
                                break;
                            case ActionName.UpdateStatusGallerySQL:
                                var id3 = Json.Parse<long>(queue.Data.ToString());
                                if (id3 > 0)
                                {
                                    var obj3 = await StoreFactory.Create<GalleryCacheStore>().GetAlbumFullInfoAsync(id3);
                                    res = await StoreFactory.Create<GalleryDataStore>().UpdateStatusAsync(obj3, true);
                                    if (res || obj3 == null)
                                    {
                                        ack(null);
                                    }
                                    else
                                    {
                                        ack(new Exception());
                                    }
                                }
                                else
                                {
                                    ack(new Exception());
                                }
                                break;
                            case ActionName.ShareGallerySQL:
                                var obj4 = Json.Parse<QueueShareDataUnit<GallerySearch>>(queue.Data.ToString());
                                if (obj4 != null && obj4.ItemStreamDistribution != null && obj4.ListMedia != null)
                                    res = await StoreFactory.Create<DistributionDataStore>().ShareGalleryAsync(obj4.ItemStreamDistribution, obj4.ListMedia);
                                if (res || obj4 == null || obj4.ItemStreamDistribution == null || obj4.ListMedia == null)
                                {
                                    ack(null);
                                }
                                else
                                {
                                    ack(new Exception());
                                }
                                break;

                            default:
                                ack(new Exception());
                                break;
                        }
                    }
                }
                catch (Exception ex)
                {
                    ack(ex);
                    Logger.Sensitive(ex, ex.Message);
                }
            });

            ExchangeFactory.CreateStream(new EQConfiguration { Topic = TopicName.SQL_BEAM, Recovery = true }, async (msg, ack) =>
            {
                try
                {
                    //TODO somethings
                    var queue = Json.Parse<Entities.Queue<object>>(msg.Payload.ToString());
                    var res = false;
                    if (queue != null && queue.Data != null)
                    {
                        switch (queue.Action)
                        {
                            case ActionName.AddBeamSQL:
                                var id1 = Json.Parse<long>(queue.Data.ToString());
                                if (id1 > 0)
                                {
                                    var obj1 = await StoreFactory.Create<BeamCacheStore>().GetBeamFullInfoAsync(id1);
                                    res = await StoreFactory.Create<BeamDataStore>().AddAsync(obj1, true);
                                    if (res || obj1 == null)
                                    {
                                        ack(null);
                                    }
                                    else
                                    {
                                        ack(new Exception());
                                    }
                                }
                                else
                                {
                                    ack(new Exception());
                                }
                                break;
                            case ActionName.UpdateBeamSQL:
                                var id2 = Json.Parse<long>(queue.Data.ToString());
                                if (id2 > 0)
                                {
                                    var obj2 = await StoreFactory.Create<BeamCacheStore>().GetBeamFullInfoAsync(id2);
                                    res = await StoreFactory.Create<BeamDataStore>().UpdateAsync(obj2, true);
                                    if (res || obj2 == null)
                                    {
                                        ack(null);
                                    }
                                    else
                                    {
                                        ack(new Exception());
                                    }
                                }
                                else
                                {
                                    ack(new Exception());
                                }
                                break;
                            case ActionName.UpdateStatusBeamSQL:
                                var id3 = Json.Parse<long>(queue.Data.ToString());
                                if (id3 > 0)
                                {
                                    var obj3 = await StoreFactory.Create<BeamCacheStore>().GetBeamFullInfoAsync(id3);
                                    res = await StoreFactory.Create<BeamDataStore>().UpdateStatusAsync(obj3, true);
                                    if (res || obj3 == null)
                                    {
                                        ack(null);
                                    }
                                    else
                                    {
                                        ack(new Exception());
                                    }
                                }
                                else
                                {
                                    ack(new Exception());
                                }
                                break;
                            case ActionName.ShareBeamSQL:
                                var obj4 = Json.Parse<QueueData<ItemStreamDistribution, List<NewsDistribution>, bool, object>>(queue.Data.ToString());
                                if (obj4 != null && obj4.Data1 != null && obj4.Data2 != null)
                                    res = await StoreFactory.Create<DistributionDataStore>().ShareMediaAsync(obj4.Data1, obj4.Data2, obj4.Data3);
                                if (res || obj4 == null || obj4.Data1 == null || obj4.Data2 == null)
                                {
                                    ack(null);
                                }
                                else
                                {
                                    ack(new Exception());
                                }
                                break;
                            default:
                                ack(new Exception());
                                break;
                        }
                    }
                }
                catch (Exception ex)
                {
                    ack(ex);
                    Logger.Sensitive(ex, ex.Message);
                }
            });

            ExchangeFactory.CreateStream(new EQConfiguration { Topic = TopicName.SQL_NEWSCRAWLER, Recovery = true }, async (msg, ack) =>
            {
                try
                {
                    //TODO somethings
                    var queue = Json.Parse<Entities.Queue<object>>(msg.Payload.ToString());
                    var res = false;
                    if (queue != null && queue.Data != null)
                    {
                        switch (queue.Action)
                        {
                            case ActionName.AddNewsCrawlerSQL:
                                var obj1 = Json.Parse<List<NewsCrawler>>(queue.Data.ToString());
                                if (obj1 != null)
                                    res = await StoreFactory.Create<NewsCrawlerDataStore>().AddAsync(obj1, true);
                                if (res)
                                {
                                    ack(null);
                                }
                                else
                                {
                                    ack(new Exception());
                                }
                                break;
                            case ActionName.UpdateNewsCrawlerSQL:
                                var obj2 = Json.Parse<NewsCrawler>(queue.Data.ToString());
                                if (obj2 != null)
                                    res = await StoreFactory.Create<NewsCrawlerDataStore>().UpdateAsync(obj2, true);
                                if (res || obj2 == null)
                                {
                                    ack(null);
                                }
                                else
                                {
                                    ack(new Exception());
                                }
                                break;
                            default:
                                ack(new Exception());
                                break;
                        }
                    }
                }
                catch (Exception ex)
                {
                    ack(ex);
                    Logger.Sensitive(ex, ex.Message);
                }
            });
            ExchangeFactory.CreateStream(new EQConfiguration { Topic = TopicName.SQL_NEWSONHOME, Recovery = true }, async (msg, ack) =>
            {
                try
                {
                    //TODO somethings
                    var queue = Json.Parse<Entities.Queue<object>>(msg.Payload.ToString());
                    var res = false;
                    if (queue != null && queue.Data != null)
                    {
                        switch (queue.Action)
                        {
                            case ActionName.AddNewsOnHomeSQL:
                                var obj1 = Json.Parse<NewsOnHome>(queue.Data.ToString());
                                if (obj1 != null)
                                    res = await StoreFactory.Create<NewsOnHomeDataStore>().AddAsync(obj1, true);
                                if (res)
                                {
                                    ack(null);
                                }
                                else
                                {
                                    ack(new Exception());
                                }
                                break;
                            case ActionName.UpdateNewsOnHomeSQL:
                                var obj2 = Json.Parse<NewsOnHome>(queue.Data.ToString());
                                if (obj2 != null)
                                    res = await StoreFactory.Create<NewsOnHomeDataStore>().UpdateAsync(obj2, true);
                                if (res || obj2 == null)
                                {
                                    ack(null);
                                }
                                else
                                {
                                    ack(new Exception());
                                }
                                break;
                            default:
                                ack(new Exception());
                                break;
                        }
                    }
                }
                catch (Exception ex)
                {
                    ack(ex);
                    Logger.Sensitive(ex, ex.Message);
                }
            });

            ExchangeFactory.CreateStream(new EQConfiguration { Topic = TopicName.SQL_ACCOUNTASSIGNMENT, Recovery = true }, async (msg, ack) =>
            {
                try
                {
                    //TODO somethings
                    var queue = Json.Parse<Entities.Queue<object>>(msg.Payload.ToString());
                    var res = false;
                    if (queue != null && queue.Data != null)
                    {
                        switch (queue.Action)
                        {
                            case ActionName.AddAccountAssignmentSQL:
                                var obj1 = Json.Parse<AccountAssignment>(queue.Data.ToString());
                                if (obj1 != null)
                                    res = await StoreFactory.Create<AccountAssignmentDataStore>().AddAsync(obj1, true);
                                if (res)
                                {
                                    ack(null);
                                }
                                else
                                {
                                    ack(new Exception());
                                }
                                break;
                            case ActionName.UpdateAccountAssignmentSQL:
                                var obj2 = Json.Parse<AccountAssignment>(queue.Data.ToString());
                                if (obj2 != null)
                                    res = await StoreFactory.Create<AccountAssignmentDataStore>().UpdateAsync(obj2, true);
                                if (res || obj2 == null)
                                {
                                    ack(null);
                                }
                                else
                                {
                                    ack(new Exception());
                                }
                                break;
                            case ActionName.RemoveAccountAssignmentSQL:
                                var obj3 = Json.Parse<AccountAssignment>(queue.Data.ToString());
                                if (obj3 != null)
                                    res = await StoreFactory.Create<AccountAssignmentDataStore>().RemoveAsync(obj3, true);
                                if (res || obj3 == null)
                                {
                                    ack(null);
                                }
                                else
                                {
                                    ack(new Exception());
                                }
                                break;
                            default:
                                ack(new Exception());
                                break;
                        }
                    }
                }
                catch (Exception ex)
                {
                    ack(ex);
                    Logger.Sensitive(ex, ex.Message);
                }
            });
            #endregion

            ExchangeFactory.CreateStream(new EQConfiguration { Topic = TopicName.ROLEKINGHUB, Recovery = true }, async (msg, ack) =>
            {
                try
                {
                    //TODO somethings
                    var queue = Json.Parse<Entities.Queue<object>>(msg.Payload.ToString());
                    var res = false;
                    if (queue != null && queue.Data != null)
                    {
                        switch (queue.Action)
                        {

                            case ActionName.SetRoleKingHubAccount:
                                var obj = Json.Parse<Account>(queue.Data.ToString());
                                if (obj != null)
                                    res = await KingHubRepository.SetUserRolesAsync(obj, true);
                                if (res || obj == null)
                                {
                                    ack(null);
                                }
                                else
                                {
                                    ack(new Exception());
                                }
                                break;
                            default:
                                ack(new Exception());
                                break;
                        }
                    }
                    else
                    {
                        ack(new Exception());
                    }
                }
                catch (Exception ex)
                {
                    ack(ex);
                    Logger.Sensitive(ex, ex.Message);
                }
            });

            ExchangeFactory.CreateStream(new EQConfiguration { Topic = TopicName.SQL_NEWSCRAWLERCONFIG, Recovery = true }, async (msg, ack) => 
            {              
                try
                {
                    //TODO somethings
                    var queue = Json.Parse<Entities.Queue<object>>(msg.Payload.ToString());
                    var res = false;
                    if (queue != null && queue.Data != null)
                    {
                        switch (queue.Action)
                        {
                            case ActionName.AddNewsCrawlerConfigSQL:
                                var obj1 = Json.Parse<NewCrawlerConfigurationSearch>(queue.Data.ToString());
                                if (obj1 != null)
                                    res = await StoreFactory.Create<NewsCrawlerConfigDataStore>().AddConfigAsync(obj1, true);
                                if (res || obj1 == null)
                                {
                                    ack(null);
                                }
                                else
                                {
                                    ack(new Exception());
                                }
                                break;
                            case ActionName.UpdateNewsCrawlerConfigSQL:
                                var obj2 = Json.Parse<NewCrawlerConfigurationSearch>(queue.Data.ToString());
                                if (obj2 != null)
                                    res = await StoreFactory.Create<NewsCrawlerConfigDataStore>().UpdateConfigAsync(obj2, true);
                                if (res || obj2 == null)
                                {
                                    ack(null);
                                }
                                else
                                {
                                    ack(new Exception());
                                }
                                break;
                            default:
                                ack(new Exception());
                                break;
                        }
                    }
                }
                catch (Exception ex)
                {
                    ack(ex);
                    Logger.Sensitive(ex, ex.Message);
                }
            });

            #region AutoShare
            ExchangeFactory.CreateStream(new EQConfiguration { Topic = TopicName.AUTOSHAREVIDEO, Recovery = true }, async (msg, ack) =>
            {
                try
                {
                    //TODO somethings
                    var queue = Json.Parse<Entities.Queue<object>>(msg.Payload.ToString());
                    var res = false;
                    if (queue != null && queue.Data != null)
                    {
                        switch (queue.Action)
                        {
                            case ActionName.AutoShareVideo:
                                var obj = Json.Parse<QueueData<long, string, string, Media.Action>>(queue.Data.ToString());
                                if (obj != null && obj.Data1 > 0 && !string.IsNullOrEmpty(obj.Data2))
                                {
                                    var videoObj = await VideoRepository.GetVideoSearchByIdAsync(obj.Data1);
                                    if (videoObj != null)
                                    {
                                        res = await VideoRepository.AutoShare(videoObj, obj.Data2, obj.Data3, obj.Data4);
                                    }
                                    else
                                    {
                                        ack(null);
                                    }
                                }

                                if (res || obj == null || obj.Data1 == 0 || string.IsNullOrEmpty(obj.Data2))
                                {
                                    ack(null);
                                }
                                else
                                {
                                    ack(new Exception());
                                }
                                break;
                            default:
                                ack(new Exception());
                                break;
                        }
                    }
                    else
                    {
                        ack(new Exception());
                    }
                }
                catch (Exception ex)
                {
                    ack(ex);
                    Logger.Sensitive(ex, ex.Message);
                }
            });

            ExchangeFactory.CreateStream(new EQConfiguration { Topic = TopicName.AUTOSHAREVIDEOPLAYLIST, Recovery = true }, async (msg, ack) =>
            {
                try
                {
                    //TODO somethings
                    var queue = Json.Parse<Entities.Queue<object>>(msg.Payload.ToString());
                    var res = false;
                    if (queue != null && queue.Data != null)
                    {
                        switch (queue.Action)
                        {
                            case ActionName.AutoShareVideoPlaylist:
                                var obj = Json.Parse<QueueData<long, string, string, Media.Action>>(queue.Data.ToString());
                                if (obj != null && obj.Data1 > 0 && !string.IsNullOrEmpty(obj.Data2))
                                {
                                    var videoObj = await VideoPlaylistRepository.GetVideoPlaylistSearchByIdAsync(obj.Data1);
                                    if (videoObj != null)
                                    {
                                        res = await VideoPlaylistRepository.AutoShare(videoObj, obj.Data2, obj.Data3, obj.Data4);
                                    }
                                    else
                                    {
                                        ack(null);
                                    }
                                }

                                if (res || obj == null || obj.Data1 == 0 || string.IsNullOrEmpty(obj.Data2))
                                {
                                    ack(null);
                                }
                                else
                                {
                                    ack(new Exception());
                                }

                                break;
                            default:
                                ack(new Exception());
                                break;
                        }
                    }
                    else
                    {
                        ack(new Exception());
                    }
                }
                catch (Exception ex)
                {
                    ack(ex);
                    Logger.Sensitive(ex, ex.Message);
                }
            });

            ExchangeFactory.CreateStream(new EQConfiguration { Topic = TopicName.AUTOSHAREPOST, Recovery = true }, async (msg, ack) =>
            {
                try
                {
                    //TODO somethings
                    var queue = Json.Parse<Entities.Queue<object>>(msg.Payload.ToString());
                    var res = false;
                    if (queue != null && queue.Data != null)
                    {
                        switch (queue.Action)
                        {
                            case ActionName.AutoSharePost:
                                var obj = Json.Parse<QueueData<long, string, string, Media.Action>>(queue.Data.ToString());
                                if (obj != null && obj.Data1 > 0 && !string.IsNullOrEmpty(obj.Data2))
                                {
                                    var postObj = await PostRepository.GetPostSearchByIdAsync(obj.Data1);
                                    if (postObj != null)
                                    {
                                        res = await PostRepository.AutoShare(postObj, obj.Data2, obj.Data3, obj.Data4);
                                    }
                                    else
                                    {
                                        ack(null);
                                    }
                                }

                                if (res || obj == null || obj.Data1 == 0 || string.IsNullOrEmpty(obj.Data2))
                                {
                                    ack(null);
                                }
                                else
                                {
                                    ack(new Exception());
                                }

                                break;
                            default:
                                ack(new Exception());
                                break;
                        }
                    }
                    else
                    {
                        ack(new Exception());
                    }
                }
                catch (Exception ex)
                {
                    ack(ex);
                    Logger.Sensitive(ex, ex.Message);
                }
            });

            ExchangeFactory.CreateStream(new EQConfiguration { Topic = TopicName.AUTOSHARESHARELINK, Recovery = true }, async (msg, ack) =>
            {
                try
                {
                    //TODO somethings
                    var queue = Json.Parse<Entities.Queue<object>>(msg.Payload.ToString());
                    var res = false;
                    if (queue != null && queue.Data != null)
                    {
                        switch (queue.Action)
                        {
                            case ActionName.AutoShareShareLink:
                                var obj = Json.Parse<QueueData<long, string, string, Media.Action>>(queue.Data.ToString());
                                if (obj != null && obj.Data1 > 0 && !string.IsNullOrEmpty(obj.Data2))
                                {
                                    var linkObj = await ShareLinkRepository.GetShareLinkSearchByIdAsync(obj.Data1);
                                    if (linkObj != null)
                                    {
                                        res = await ShareLinkRepository.AutoShare(linkObj, obj.Data2, obj.Data3, obj.Data4);
                                    }
                                    else
                                    {
                                        ack(null);
                                    }
                                }

                                if (res || obj == null || obj.Data1 == 0 || string.IsNullOrEmpty(obj.Data2))
                                {
                                    ack(null);
                                }
                                else
                                {
                                    ack(new Exception());
                                }
                                break;
                            default:
                                ack(new Exception());
                                break;
                        }
                    }
                    else
                    {
                        ack(new Exception());
                    }
                }
                catch (Exception ex)
                {
                    ack(ex);
                    Logger.Sensitive(ex, ex.Message);
                }
            });

            ExchangeFactory.CreateStream(new EQConfiguration { Topic = TopicName.AUTOSHAREBEAM, Recovery = true }, async (msg, ack) =>
            {
                try
                {
                    //TODO somethings
                    var queue = Json.Parse<Entities.Queue<object>>(msg.Payload.ToString());
                    var res = false;
                    if (queue != null && queue.Data != null)
                    {
                        switch (queue.Action)
                        {
                            case ActionName.AutoShareBeam:
                                var obj = Json.Parse<QueueData<long, string, string, Media.Action>>(queue.Data.ToString());
                                if (obj != null && obj.Data1 > 0 && !string.IsNullOrEmpty(obj.Data2) && !string.IsNullOrEmpty(obj.Data3))
                                {
                                    var beam = await BeamRepository.GetBeamSearchByIdAsync(obj.Data1);
                                    if (beam != null)
                                        res = await BeamRepository.AutoShare(beam, obj.Data2, obj.Data3, obj.Data4);
                                    else
                                    {
                                        ack(null);
                                    }
                                }
                                if (res || obj == null || obj.Data1 > 0 || string.IsNullOrEmpty(obj.Data2))
                                {
                                    ack(null);
                                }
                                else
                                {
                                    ack(new Exception());
                                }

                                break;
                            default:
                                ack(new Exception());
                                break;
                        }
                    }
                    else
                    {
                        ack(new Exception());
                    }
                }
                catch (Exception ex)
                {
                    ack(ex);
                    Logger.Sensitive(ex, ex.Message);
                }
            });

            ExchangeFactory.CreateStream(new EQConfiguration { Topic = TopicName.AUTOSHAREARTICLE, Recovery = true }, async (msg, ack) =>
            {
                try
                {
                    //TODO somethings
                    var queue = Json.Parse<Entities.Queue<object>>(msg.Payload.ToString());
                    var res = false;
                    if (queue != null && queue.Data != null)
                    {
                        switch (queue.Action)
                        {
                            case ActionName.AutoShareArticle:
                                var obj = Json.Parse<QueueData<long, string, string, Media.Action>>(queue.Data.ToString());
                                if (obj != null && obj.Data1 > 0 && !string.IsNullOrEmpty(obj.Data2))
                                {
                                    var article = await ArticleRepository.GetArticleSearchByIdAsync(obj.Data1);                                    
                                    if (article != null)
                                    {
                                        res = await ArticleRepository.AutoShare(article, obj.Data2, obj.Data3, obj.Data4, null);
                                    }
                                    else
                                    {
                                        ack(null);
                                    }
                                }
                                if (res || obj == null || obj.Data1 == 0 || string.IsNullOrEmpty(obj.Data2))
                                {
                                    ack(null);
                                }
                                else
                                {
                                    ack(new Exception());
                                }

                                break;
                            default:
                                ack(new Exception());
                                break;
                        }
                    }
                    else
                    {
                        ack(new Exception());
                    }
                }
                catch (Exception ex)
                {
                    ack(ex);
                    Logger.Sensitive(ex, ex.Message);
                }
            });

            ExchangeFactory.CreateStream(new EQConfiguration { Topic = TopicName.AUTOSHAREALBUM, Recovery = true }, async (msg, ack) =>
            {
                try
                {
                    //TODO somethings
                    var queue = Json.Parse<Entities.Queue<object>>(msg.Payload.ToString());
                    var res = false;
                    if (queue != null && queue.Data != null)
                    {
                        switch (queue.Action)
                        {
                            case ActionName.AutoShareAlbum:
                                var obj = Json.Parse<QueueData<long, string, string, Media.Action>>(queue.Data.ToString());
                                if (obj != null && obj.Data1 > 0 && !string.IsNullOrEmpty(obj.Data2))
                                {
                                    var album = await AlbumRepository.GetDetailByIdAsync(obj.Data1);
                                    res = await AlbumRepository.AutoShare(album, obj.Data2, obj.Data3, obj.Data4);
                                }

                                if (res || obj == null || obj.Data1 == 0 || string.IsNullOrEmpty(obj.Data2))
                                {
                                    ack(null);
                                }
                                else
                                {
                                    ack(new Exception());
                                }

                                break;
                            //case ActionName.AutoShareAlbumOld:
                            //    var obj2 = Json.Parse<QueueData<long, string, string, Media.Action>>(queue.Data.ToString());
                            //    if (obj2 != null && obj2.Data1 > 0 && !string.IsNullOrEmpty(obj2.Data2))
                            //    {
                            //        var album = await AlbumRepository.GetDetailByIdAsync(obj2.Data1);
                            //        res = await AlbumRepository.AutoShare2(album, obj2.Data2, obj2.Data3, obj2.Data4);
                            //    }

                            //    if (res || obj2 == null || obj2.Data1 == 0 || string.IsNullOrEmpty(obj2.Data2))
                            //    {
                            //        ack(null);
                            //    }
                            //    else
                            //    {
                            //        ack(new Exception());
                            //    }

                            //    break;
                            default:
                                ack(new Exception());
                                break;
                        }
                    }
                    else
                    {
                        ack(new Exception());
                    }
                }
                catch (Exception ex)
                {
                    ack(ex);
                    Logger.Sensitive(ex, ex.Message);
                }
            });

            ExchangeFactory.CreateStream(new EQConfiguration { Topic = TopicName.AUTOSHAREGALLERY, Recovery = true }, async (msg, ack) =>
            {
                try
                {
                    //TODO somethings
                    var queue = Json.Parse<Entities.Queue<object>>(msg.Payload.ToString());
                    var res = false;
                    if (queue != null && queue.Data != null)
                    {
                        switch (queue.Action)
                        {
                            case ActionName.AutoShareGallery:
                                var obj = Json.Parse<QueueData<long, string, string, Media.Action>>(queue.Data.ToString());
                                if (obj != null && obj.Data1 > 0 && !string.IsNullOrEmpty(obj.Data2))
                                {
                                    var album = await GalleryRepository.GetDetailByIdAsync(obj.Data1);
                                    res = await GalleryRepository.AutoShare(album, obj.Data2, obj.Data3, obj.Data4);
                                }

                                if (res || obj == null || obj.Data1 == 0 || string.IsNullOrEmpty(obj.Data2))
                                {
                                    ack(null);
                                }
                                else
                                {
                                    ack(new Exception());
                                }

                                break;
                            default:
                                ack(new Exception());
                                break;
                        }
                    }
                    else
                    {
                        ack(new Exception());
                    }
                }
                catch (Exception ex)
                {
                    ack(ex);
                    Logger.Sensitive(ex, ex.Message);
                }
            });

            ExchangeFactory.CreateStream(new EQConfiguration { Topic = TopicName.AUTOSHAREMEDIAUNIT, Recovery = true }, async (msg, ack) =>
            {
                try
                {
                    //TODO somethings
                    var queue = Json.Parse<Entities.Queue<object>>(msg.Payload.ToString());
                    var res = false;
                    if (queue != null && queue.Data != null)
                    {
                        switch (queue.Action)
                        {
                            case ActionName.AutoShareMediaUnit:
                                var obj = Json.Parse<QueueData<long, string, string, Media.Action>>(queue.Data.ToString());
                                if (obj != null && obj.Data1 > 0 && !string.IsNullOrEmpty(obj.Data2))
                                {
                                    var mediaObj = await MediaUnitRepository.GetDetailByIdAsync(obj.Data1);
                                    if (mediaObj != null)
                                    {
                                        res = await MediaUnitRepository.AutoShare(mediaObj, obj.Data2, obj.Data3, obj.Data4);
                                    }
                                    else
                                    {
                                        ack(null);
                                    }
                                }
                                if (res || obj == null || obj.Data1 == 0 || string.IsNullOrEmpty(obj.Data2))
                                {
                                    ack(null);
                                }
                                else
                                {
                                    ack(new Exception());
                                }

                                break;
                            default:
                                ack(new Exception());
                                break;
                        }
                    }
                    else
                    {
                        ack(new Exception());
                    }
                }
                catch (Exception ex)
                {
                    ack(ex);
                    Logger.Sensitive(ex, ex.Message);
                }
            });

            ExchangeFactory.CreateStream(new EQConfiguration { Topic = TopicName.AUTOSHAREPHOTO, Recovery = true }, async (msg, ack) =>
            {
                try
                {
                    //TODO somethings
                    var queue = Json.Parse<Entities.Queue<object>>(msg.Payload.ToString());
                    var res = false;
                    if (queue != null && queue.Data != null)
                    {
                        switch (queue.Action)
                        {
                            case ActionName.AutoSharePhoto:
                                var obj = Json.Parse<QueueData<long, string, string, Media.Action>>(queue.Data.ToString());
                                if (obj != null && obj.Data1 > 0 && !string.IsNullOrEmpty(obj.Data2))
                                {
                                    var photoObj = await PhotoRepository.GetDetailByIdAsync(obj.Data1);
                                    if (photoObj != null)
                                    {
                                        res = await PhotoRepository.AutoShare(photoObj, obj.Data2, obj.Data3, obj.Data4);
                                    }
                                    else
                                    {
                                        ack(null);
                                    }
                                }

                                if (res || obj == null || obj.Data1 == 0 || string.IsNullOrEmpty(obj.Data2))
                                {
                                    ack(null);
                                }
                                else
                                {
                                    ack(new Exception());
                                }

                                break;
                            case ActionName.AutoSharePhotoOld:
                                var obj2 = Json.Parse<QueueData<long, string, string, Media.Action>>(queue.Data.ToString());
                                if (obj2 != null && obj2.Data1 > 0 && !string.IsNullOrEmpty(obj2.Data2))
                                {
                                    var photoObj = await PhotoRepository.GetDetailByIdAsync(obj2.Data1);
                                    if (photoObj != null)
                                    {
                                        res = await PhotoRepository.AutoShare2(photoObj, obj2.Data2, obj2.Data3, obj2.Data4);
                                    }
                                    else
                                    {
                                        ack(null);
                                    }
                                }

                                if (res || obj2 == null || obj2.Data1 == 0 || string.IsNullOrEmpty(obj2.Data2))
                                {
                                    ack(null);
                                }
                                else
                                {
                                    ack(new Exception());
                                }

                                break;
                            default:
                                ack(new Exception());
                                break;
                        }
                    }
                    else
                    {
                        ack(new Exception());
                    }
                }
                catch (Exception ex)
                {
                    ack(ex);
                    Logger.Sensitive(ex, ex.Message);
                }
            });
            #endregion

            #region PushBoardToChannel
            ExchangeFactory.CreateStream(new EQConfiguration { Topic = TopicName.PUSHBOARDTOCHANNEL, Recovery = true }, async (msg, ack) =>
            {
                try
                {
                    //TODO somethings
                    var queue = Json.Parse<Entities.Queue<object>>(msg.Payload.ToString());
                    var res = false;
                    if (queue != null && queue.Data != null)
                    {
                        switch (queue.Action)
                        {
                            case ActionName.PushBoardToChannel:
                                var data = Json.Parse<QueueData<string, string, string, PushInfo>>(queue.Data.ToString());
                                if (data != null)
                                {
                                    res = await BoardRepository.PushBoardToChannel(data.Data1, data.Data2, data.Data3, data.Data4);
                                    if (res)
                                    {
                                        ack(null);
                                    }
                                    else
                                    {
                                        ack(new Exception());
                                    }
                                }
                                else
                                {
                                    ack(new Exception());
                                }
                                break;
                            case ActionName.AddNewsToBoard:
                                var queueData4 = Json.Parse<QueueData<long, string, long, long>>(queue.Data.ToString());
                                if (queueData4 != null)
                                {
                                    res = await BoardRepository.AddNewsAsync(queueData4.Data1, queueData4.Data2, queueData4.Data3, queueData4.Data4, "no.ip");
                                    if (res)
                                    {
                                        ack(null);
                                    }
                                    else
                                    {
                                        ack(new Exception());
                                    }
                                }
                                else
                                {
                                    ack(new Exception());
                                }
                                break;
                            case ActionName.PushChangeNotify:
                                var mediId = Json.Parse<long>(queue.Data.ToString());
                                if (mediId > 0)
                                {
                                    res = await BoardRepository.PushChangeNotifyAsync(mediId);
                                    if (res)
                                    {
                                        ack(null);
                                    }
                                    else
                                    {
                                        ack(new Exception());
                                    }
                                }
                                else
                                {
                                    ack(new Exception());
                                }
                                break;
                            default:
                                ack(new Exception());
                                break;
                        }
                    }
                    else
                    {
                        ack(new Exception());
                    }
                }
                catch (Exception ex)
                {
                    ack(ex);
                    Logger.Sensitive(ex, ex.Message);
                }
            });
            #endregion

            #region LogAction
            ExchangeFactory.CreateStream(new EQConfiguration { Topic = TopicName.LOGACTION, Recovery = true }, async (msg, ack) =>
            {
                try
                {
                    //TODO somethings
                    var queue = Json.Parse<Entities.Queue<object>>(msg.Payload.ToString());
                    var res = false;
                    if (queue != null && queue.Data != null)
                    {
                        switch (queue.Action)
                        {
                            case ActionName.InsertLog:
                                var data = Json.Parse<LogActionEntity>(queue.Data.ToString());
                                if (data != null)
                                {
                                    res = await LogActionRepository.InsertAsync(data);
                                    if (res)
                                    {
                                        ack(null);
                                    }
                                    else
                                    {
                                        ack(new Exception());
                                    }
                                }
                                else
                                {
                                    ack(new Exception());
                                }
                                break;
                            default:
                                ack(new Exception());
                                break;
                        }
                    }
                    else
                    {
                        ack(new Exception());
                    }
                }
                catch (Exception ex)
                {
                    ack(ex);
                    Logger.Sensitive(ex, ex.Message);
                }
            });
            #endregion

            #region Delete Index Temp
            ExchangeFactory.CreateStream(new EQConfiguration { Topic = TopicName.DELETE_INDEX_TEMP, Recovery = true }, async (msg, ack) =>
            {
                try
                {
                    //TODO somethings
                    var queue = Json.Parse<Entities.Queue<object>>(msg.Payload.ToString());
                    var res = false;
                    if (queue != null && queue.Data != null)
                    {
                        switch (queue.Action)
                        {
                            case ActionName.SetExpire:
                                var tempKey = queue.Data.ToString();
                                if (!string.IsNullOrEmpty(tempKey))
                                {
                                    res = await StoreFactory.Create<DeleteTempCacheStore>().SetExpireAsync(tempKey);
                                    if (res)
                                    {
                                        ack(null);
                                    }
                                    else
                                    {
                                        ack(new Exception());
                                    }
                                }
                                else
                                {
                                    ack(new Exception());
                                }
                                break;
                            default:
                                ack(new Exception());
                                break;
                        }
                    }
                    else
                    {
                        ack(new Exception());
                    }
                }
                catch (Exception ex)
                {
                    ack(ex);
                    Logger.Sensitive(ex, ex.Message);
                }
            });
            #endregion

            ExchangeFactory.CreateStream(new EQConfiguration { Topic = TopicName.SQL_CONVERT, Recovery = true }, async (msg, ack) =>
            {
                try
                {
                    //TODO somethings
                    var queue = Json.Parse<Entities.Queue<object>>(msg.Payload.ToString());
                    var res = false;
                    if (queue != null && queue.Data != null)
                    {
                        switch (queue.Action)
                        {
                            case ActionName.ConvertDataSQL:
                                var acc = Json.Parse<Account>(queue.Data.ToString());
                                if (acc != null)
                                {
                                    res = await StoreFactory.Create<SystemDataStore>().ConvertDataAsync(acc);
                                    if (res)
                                    {
                                        ack(null);
                                    }
                                    else
                                    {
                                        ack(new Exception());
                                    }
                                }
                                else
                                {
                                    ack(new Exception());
                                }
                                break;
                            case ActionName.InitAll:
                                res = await StoreFactory.Create<SystemDataStore>().InitAll();
                                if (res)
                                {
                                    ack(null);
                                }
                                else
                                {
                                    ack(new Exception());
                                }
                                break;
                            case ActionName.UpdateUserId:
                                var data = Json.Parse<QueueData<long, long, object, object>>(queue.Data.ToString());
                                if (data != null)
                                    res = await StoreFactory.Create<SystemDataStore>().UpdateUserIdAsync(data.Data1, data.Data2);
                                if (res || data == null)
                                {
                                    ack(null);
                                }
                                else
                                {
                                    ack(new Exception());
                                }
                                break;
                            default:
                                ack(new Exception());
                                break;
                        }
                    }
                    else
                    {
                        ack(new Exception());
                    }
                }
                catch (Exception ex)
                {
                    ack(ex);
                    Logger.Sensitive(ex, ex.Message);
                }
            });

            ExchangeFactory.CreateStream(new EQConfiguration { Topic = TopicName.SYNC_USERINFO, Recovery = true }, async (msg, ack) =>
            {
                try
                {
                    //TODO somethings
                    var queue = Json.Parse<Entities.Queue<object>>(msg.Payload.ToString());
                    var res = false;
                    if (queue != null && queue.Data != null)
                    {
                        switch (queue.Action)
                        {
                            //case ActionName.GetUserInfoAndUpdate:
                            //    var userId = Json.Parse<long>(queue.Data.ToString());
                            //    if (userId > 0)
                            //    {
                            //        res = await SecurityRepository.GetUserInfoAndUpdateAsync(userId);
                            //        if (res)
                            //        {
                            //            ack(null);
                            //        }
                            //        else
                            //        {
                            //            ack(new Exception());
                            //        }
                            //    }
                            //    else
                            //    {
                            //        ack(new Exception());
                            //    }
                            //    break;
                            case ActionName.UpdateUserInfoOnApp:
                                var data = Json.Parse<QueueData<AccountSearch, string, UserProfile, object>>(queue.Data.ToString());
                                if (data != null && data.Data1 != null && !string.IsNullOrEmpty(data.Data2))
                                {
                                    res = await SecurityRepository.UpdateUserInfoOnAppAsync(data.Data1, data.Data3, data.Data2);
                                    if (res)
                                    {
                                        await Functions.Function.AddToQueue(ActionName.RetryUpdateUserInfoOnApp, TopicName.RETRY_SYNC_USERINFO, data);
                                        ack(null);
                                    }
                                    else
                                    {
                                        ack(new Exception());
                                    }
                                }
                                else
                                {
                                    ack(new Exception());
                                }
                                break;
                            case ActionName.RetryUpdateUserInfoOnApp:
                                var dataRetry = Json.Parse<QueueData<AccountSearch, string, UserProfile, object>>(queue.Data.ToString());
                                if (dataRetry != null && dataRetry.Data1 != null && !string.IsNullOrEmpty(dataRetry.Data2))
                                {
                                    res = await SecurityRepository.UpdateUserInfoOnAppAsync(dataRetry.Data1, dataRetry.Data3, dataRetry.Data2);
                                    if (res)
                                    {
                                        ack(null);
                                    }
                                    else
                                    {
                                        ack(new Exception());
                                    }
                                }
                                else
                                {
                                    ack(new Exception());
                                }
                                break;
                            case ActionName.ApprovedOnApp:
                                var dataApproved = Json.Parse<QueueData<long, int, object, object>>(queue.Data.ToString());
                                if (dataApproved != null && dataApproved.Data1 > 0 && dataApproved.Data2 > 0)
                                {
                                    res = await SecurityRepository.ApprovedOnAppAsync(dataApproved.Data1, dataApproved.Data2);
                                    if (res)
                                    {
                                        ack(null);
                                    }
                                    else
                                    {
                                        ack(new Exception());
                                    }
                                }
                                else
                                {
                                    ack(new Exception());
                                }
                                break;
                            case ActionName.AddUserMember:
                                var userMemberId = Json.Parse<long>(queue.Data.ToString());
                                var accountDb = await SecurityRepository.GetByIdAsync(userMemberId);
                                if (accountDb == null || accountDb.Id == 0)
                                {
                                    var userInfo = await SecurityRepository.GetUserInfo(userMemberId);
                                    if (userInfo != null && !string.IsNullOrEmpty(userInfo.User_id))
                                    {
                                        if (long.TryParse(userInfo.User_id, out long _userId))
                                        {
                                            var account = new Account
                                            {
                                                Id = _userId,
                                                Avatar = userInfo.Avatar,
                                                UserName = userInfo.Username,
                                                FullName = userInfo.Full_name,
                                                Type = (int)AccountType.ReadOnly,
                                                CreatedDate = DateTime.Now,
                                                Email = userInfo.Email,
                                                Mobile = userInfo.Phone,
                                                Status = (int)AccountStatus.Actived,
                                                Class = (int)AccountClass.Normal
                                            };

                                            var result = await SecurityRepository.AddAsync(account);
                                            if (result == ErrorCodes.Success)
                                            {
                                                ack(null);
                                            }
                                            else
                                            {
                                                ack(new Exception());
                                            }
                                        }
                                        else
                                        {
                                            ack(new Exception());
                                        }
                                    }
                                    else
                                    {
                                        ack(new Exception());
                                    }
                                }
                                else
                                {
                                    ack(null);
                                }

                                break;
                            case ActionName.ChangePageOwner:
                                var dataInfo = Json.Parse<QueueData<long, long, long, object>>(queue.Data.ToString());
                                if (dataInfo != null && dataInfo.Data1 > 0 && dataInfo.Data2 >= 0 && dataInfo.Data3 > 0)
                                {
                                    res = await SecurityRepository.ChangePageOwnerAsync(dataInfo.Data1, dataInfo.Data2, dataInfo.Data3);
                                    if (res)
                                    {
                                        ack(null);
                                    }
                                    else
                                    {
                                        ack(new Exception());
                                    }
                                }
                                else
                                {
                                    ack(new Exception());
                                }
                                break;
                            case ActionName.InsertPageOwnerToApp:
                                var ownerInfo = Json.Parse<QueueData<long, long, byte?, string>>(queue.Data.ToString());
                                if (ownerInfo != null && ownerInfo.Data1 > 0 && ownerInfo.Data2 > 0)
                                {
                                    res = await SecurityRepository.InsertPageOwnerToAppAsync(ownerInfo.Data1, ownerInfo.Data2, ownerInfo.Data3, ownerInfo.Data4);
                                    if (res)
                                    {
                                        ack(null);
                                    }
                                    else
                                    {
                                        ack(new Exception());
                                    }
                                }
                                else
                                {
                                    ack(new Exception());
                                }
                                break;
                            case ActionName.SettingCommentOnApp:
                                var dataSetting = Json.Parse<QueueData<long, Byte?, object, object>>(queue.Data.ToString());
                                if (dataSetting != null && dataSetting.Data1 > 0 && dataSetting.Data2 != null)
                                {
                                    var restric = AppSettings.Current.ChannelConfiguration.ApiCommentSetting.Mode.PostCheck;
                                    switch (dataSetting.Data2)
                                    {
                                        case (Byte)CommentMode.PreCheck:
                                            restric = AppSettings.Current.ChannelConfiguration.ApiCommentSetting.Mode.PreCheck;
                                            break;
                                        case (Byte)CommentMode.Disable:
                                            restric = AppSettings.Current.ChannelConfiguration.ApiCommentSetting.Mode.Disable;
                                            break;
                                    }
                                    res = await SecurityRepository.SettingCommentOnAppAsync(dataSetting.Data1, restric);
                                    if (res)
                                    {
                                        ack(null);
                                    }
                                    else
                                    {
                                        ack(new Exception());
                                    }
                                }
                                else
                                {
                                    ack(null);
                                }
                                break;
                            case ActionName.SendCrm:
                                var data5 = Json.Parse<QueueData<string, Account, List<Account>, object>>(queue.Data.ToString());
                                if (data5 != null && data5.Data2 != null)
                                {
                                    res = await SecurityRepository.SendCRMAsync(data5.Data2.Id, data5.Data1, data5.Data2.FullName, data5.Data2.Mobile, data5.Data2.Email, (AccountType?)data5.Data2.Type, data5.Data3);
                                    if (res)
                                    {
                                        ack(null);
                                    }
                                    else
                                    {
                                        ack(new Exception());
                                    }
                                }
                                else
                                {
                                    ack(null);
                                }
                                break;
                            case ActionName.CheckUserInfoOnApp:
                                var data6 = Json.Parse<QueueData<long, string, string, object>>(queue.Data.ToString());
                                if (data6 != null)
                                {
                                    res = await SystemRepository.CheckUserInfoOnAppAsync(data6.Data1, data6.Data2, data6.Data3);
                                    if (res)
                                    {
                                        ack(null);
                                    }
                                    else
                                    {
                                        ack(new Exception());
                                    }
                                }
                                else
                                {
                                    ack(null);
                                }
                                break;
                            case ActionName.SetProfileIsPageOnApp:
                                var data7 = Json.Parse<QueueData<long, long, string, bool>>(queue.Data.ToString());
                                if (data7 != null)
                                {
                                    res = await SecurityRepository.PushSettingProfileToAppAsync(data7.Data1, data7.Data2, data7.Data3, data7.Data4);
                                    if (res)
                                    {
                                        ack(null);
                                    }
                                    else
                                    {
                                        ack(new Exception());
                                    }
                                }
                                else
                                {
                                    ack(null);
                                }
                                break;
                            case ActionName.UpdateProfileUrl:
                                var data8 = Json.Parse<QueueData<string, string, object, object>>(queue.Data.ToString());
                                if (data8 != null)
                                {
                                    if (long.TryParse(data8.Data1, out long accountId))
                                    {
                                        var userProfile = await SecurityRepository.GetProfileAsync(accountId);
                                        if (userProfile != null)
                                        {
                                            userProfile.ProfileUrl = data8.Data2;
                                            res = await SecurityRepository.UpdateProfileUrlAsync(userProfile);
                                            if (res)
                                            {
                                                ack(null);
                                            }
                                            else
                                            {
                                                ack(new Exception());
                                            }
                                        }
                                        else
                                        {
                                            ack(null);
                                        }
                                    }
                                    else
                                    {
                                        ack(null);
                                    }
                                }
                                else
                                {
                                    ack(null);
                                }
                                break;
                            //case ActionName.UpdatePageTypeToApp:
                            //    var ownerInfo = Json.Parse<QueueData<long, long, byte?, object>>(queue.Data.ToString());
                            //    if (ownerInfo != null && ownerInfo.Data1 > 0 && ownerInfo.Data2 > 0)
                            //    {
                            //        res = await SecurityRepository.InsertPageOwnerToAppAsync(ownerInfo.Data1, ownerInfo.Data2, ownerInfo.Data3);
                            //        if (res)
                            //        {
                            //            ack(null);
                            //        }
                            //        else
                            //        {
                            //            ack(new Exception());
                            //        }
                            //    }
                            //    else
                            //    {
                            //        ack(new Exception());
                            //    }
                            //    break;
                            default:
                                ack(new Exception());
                                break;
                        }
                    }
                    else
                    {
                        ack(new Exception());
                    }
                }
                catch (Exception ex)
                {
                    ack(ex);
                    Logger.Sensitive(ex, ex.Message);
                }
            });

            ExchangeFactory.CreateStream(new EQConfiguration { Topic = TopicName.RETRY_SYNC_USERINFO, Recovery = true }, async (msg, ack) =>
            {
                try
                {
                    //TODO somethings
                    var queue = Json.Parse<Entities.Queue<object>>(msg.Payload.ToString());
                    var res = false;
                    if (queue != null && queue.Data != null)
                    {
                        switch (queue.Action)
                        {
                            case ActionName.RetryUpdateUserInfoOnApp:
                                var dataRetry = Json.Parse<QueueData<AccountSearch, string, UserProfile, object>>(queue.Data.ToString());
                                if (dataRetry != null && dataRetry.Data1 != null && !string.IsNullOrEmpty(dataRetry.Data2))
                                {
                                    res = await SecurityRepository.UpdateUserInfoOnAppAsync(dataRetry.Data1, dataRetry.Data3, dataRetry.Data2);
                                    if (res)
                                    {
                                        ack(null);
                                    }
                                    else
                                    {
                                        ack(new Exception());
                                    }
                                }
                                else
                                {
                                    ack(new Exception());
                                }
                                break;
                            default:
                                ack(new Exception());
                                break;
                        }
                    }
                    else
                    {
                        ack(new Exception());
                    }
                }
                catch (Exception ex)
                {
                    ack(ex);
                    Logger.Sensitive(ex, ex.Message);
                }
            });

            #region Push page info to adtech
            ExchangeFactory.CreateStream(new EQConfiguration { Topic = TopicName.PUSTPAGEINFO, Recovery = true }, async (msg, ack) =>
            {
                try
                {
                    //TODO somethings
                    var queue = Json.Parse<Entities.Queue<object>>(msg.Payload.ToString());
                    var res = false;
                    if (queue != null && queue.Data != null)
                    {
                        switch (queue.Action)
                        {
                            case ActionName.PushPageOwnerAndTypeToAdtech:
                                var data = Json.Parse<QueueData<long, string, byte?, string>>(queue.Data.ToString());
                                if (data != null && data.Data1 > 0 && !string.IsNullOrEmpty(data.Data2) && data.Data3 !=null)
                                {
                                    res = await SystemToolkitRepository.InsertAndUpdatePageOwnerAndTypeToAppAsync(data.Data1, data.Data2, data.Data3, data.Data4);
                                    if (res)
                                    {
                                        ack(null);
                                    }
                                    else
                                    {
                                        ack(new Exception());
                                    }
                                }
                                else
                                {
                                    ack(null);
                                }
                                break;
                            default:
                                ack(null);
                                break;
                        }
                    }
                    else
                    {
                        ack(new Exception());
                    }
                }
                catch (Exception ex)
                {
                    ack(ex);
                    Logger.Sensitive(ex, ex.Message);
                }
            });
            #endregion
        }
    }
}
