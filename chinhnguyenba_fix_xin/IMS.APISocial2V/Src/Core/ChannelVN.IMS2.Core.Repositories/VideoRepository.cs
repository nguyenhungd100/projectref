﻿using ChannelVN.IMS2.Core.Entities;
using ChannelVN.IMS2.Core.Entities.Distribution;
using ChannelVN.IMS2.Core.Entities.ErrorCode;
using ChannelVN.IMS2.Core.Entities.Media;
using ChannelVN.IMS2.Core.Entities.Security;
using ChannelVN.IMS2.Core.Entities.Statistic;
using ChannelVN.IMS2.Core.Entities.Videos;
using ChannelVN.IMS2.Core.Models.CacheStore.Videos;
using ChannelVN.IMS2.Core.Models.DataStore.Videos;
using ChannelVN.IMS2.Core.Models.ExternalAPI;
using ChannelVN.IMS2.Core.Models.SearchStore.Videos;
using ChannelVN.IMS2.Core.Repositories.Constants;
using ChannelVN.IMS2.Core.Repositories.Functions;
using ChannelVN.IMS2.Foundation.Common;
using ChannelVN.IMS2.Foundation.Common.Configuration;
using ChannelVN.IMS2.Foundation.Data;
using ChannelVN.IMS2.Foundation.Data.Factories;
using ChannelVN.IMS2.Foundation.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using static ChannelVN.IMS2.Core.Entities.ErrorCode.ErrorMapping;

namespace ChannelVN.IMS2.Core.Repositories
{
    public class VideoRepository : Repository
    {
        public static async Task<ErrorCodes> AddAsync(VideoSearch data, string clientIP)
        {
            var returnValue = ErrorCodes.AddError;
            try
            {
                returnValue = await StoreFactory.Create<VideoCacheStore>().AddAsync(data);
                if (returnValue == ErrorCodes.Success)
                {
                    var res = await StoreFactory.Create<VideoSearchStore>().AddAsync(data);
                    if (!res)
                    {
                        //Push queue
                        await Function.AddToQueue(ActionName.AddVideoES, TopicName.ES_VIDEO, data.Id);
                    }
                    res = await StoreFactory.Create<VideoDataStore>().AddAsync(data);
                    if (!res)
                    {
                        //Push queue
                        await Function.AddToQueue(ActionName.AddVideoSQL, TopicName.SQL_VIDEO, data.Id);
                    }

                    //if (data.Status == (int)NewsStatus.Archive && AppSettings.Current.Distribution.Automation.Enabled)
                    //{
                    //    await Function.AddToQueue(ActionName.AutoShareVideo, TopicName.AUTOSHAREVIDEO, new QueueData<long, string, long[], object>
                    //    {
                    //        Data1 = data.Id,
                    //        Data2 = clientIP
                    //    });
                    //}
                }
                return returnValue;
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
                return returnValue;
            }
        }

        public static Task<VideoSearch> GetVideoSearchByIdAsync(long id)
        {
            return StoreFactory.Create<VideoCacheStore>().GetVideoFullInfoAsync(id);
        }

        public static async Task<ErrorCodes> UpdateAsync(Video data, long officerId, string clientIP, string sessionId)
        {
            var returnVl = ErrorCodes.BusinessError;
            try
            {             
                returnVl = await StoreFactory.Create<VideoCacheStore>().UpdateV2Async(data, officerId, new Action<int>(async (oldStatus) =>
                {
                    if (AppSettings.Current.Distribution.Automation.Enabled)
                    {
                        if (oldStatus == (int)NewsStatus.Published)
                        {
                            var action = Media.Action.update;
                            await Function.AddToQueue(ActionName.AutoShareVideo, TopicName.AUTOSHAREVIDEO, new QueueData<long, string, string, Media.Action>
                            {
                                Data1 = data.Id,
                                Data2 = clientIP,
                                Data3 = sessionId,
                                Data4 = action
                            });

                            await Function.AddToQueue(ActionName.PushChangeNotify, TopicName.PUSHBOARDTOCHANNEL, data.Id);
                        }
                    }
                }));
                if (returnVl == ErrorCodes.Success)
                {
                    await Function.AddToQueue(ActionName.UpdateVideoES, TopicName.ES_VIDEO, data.Id);
                    await Function.AddToQueue(ActionName.UpdateVideoSQL, TopicName.SQL_VIDEO, data.Id);
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
                returnVl = ErrorCodes.Exception;
            }
            return returnVl;
        }

        //public static Task<PagingDataResult<string>> GetListFileNameSharedAsync(GetViewsByDistribution getViewsByDistribution, long? accountId)
        //{
        //    return StoreFactory.Create<VideoSearchStore>().GetListFileNameSharedAsync(getViewsByDistribution, accountId);
        //}

        public static async Task<PagingDataResult<Video>> GetListRelationAsync(string videoIds, int? pageIndex, int? pageSize)
        {
            var returnValue = new PagingDataResult<Video>();
            returnValue.Total = videoIds?.Split(",").Count() ?? 0;
            try
            {
                returnValue.Data = (await StoreFactory.Create<VideoCacheStore>().GetMultiVideoAsync(videoIds?.Split(",").ToList()))
                   ?.Skip(((pageIndex ?? 1) - 1) * (pageSize ?? 20)).Take(pageSize ?? 20).ToList();
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
            }
            return returnValue;
        }

        public static Task<PagingDataResult<VideoSearch>> GetListDistribution(SearchNews searchEntity, Account acc)
        {
            return StoreFactory.Create<VideoSearchStore>().GetListDistribution(searchEntity, acc);
        }

        public static async Task<bool> RemoveVideoOnPegax(string ApiUrl)
        {
            var returnValue = false;
            try
            {
                using (var httClient = new HttpClient())
                {
                    var result = await httClient.GetAsync(ApiUrl);
                }
            }
            catch (Exception)
            {

            }
            return returnValue;
        }


        public static Task<long> GetCountByStatus(string status, long officialId, Account acc)
        {
            return StoreFactory.Create<VideoSearchStore>().GetCountByStatus(status, officialId, acc);
        }

        public static async Task<ErrorCodes> UpdateStatusAsync(VideoSearch data, int statusOld, string clientIP)
        {
            var returnCode = ErrorCodes.BusinessError;
            try
            {
                var returnValue = await StoreFactory.Create<VideoCacheStore>().UpdateStatusAsync(Mapper<Video>.Map(data, new Video()), statusOld);
                if (returnValue)
                {
                    returnCode = ErrorCodes.Success;
                    await Function.AddToQueue(ActionName.UpdateVideoStatusES, TopicName.ES_VIDEO, data.Id);
                    await Function.AddToQueue(ActionName.UpdateVideoStatusSQL, TopicName.SQL_VIDEO, data.Id);

                    //auto phan phoi khi action publish  
                    if (AppSettings.Current.Distribution.Automation.Enabled)
                    {
                        if (data.Status == (int)NewsStatus.Published
                        || (data.Status == (int)NewsStatus.Remove && (statusOld == (int)NewsStatus.Published || statusOld == (int)NewsStatus.Archive))
                        || (data.Status == (int)NewsStatus.Draft && (statusOld == (int)NewsStatus.Published || statusOld == (int)NewsStatus.Archive)))
                        {
                            await Function.AddToQueue(ActionName.AutoShareVideo, TopicName.AUTOSHAREVIDEO, new QueueData<long, string, long[], object>
                            {
                                Data1 = data.Id,
                                Data2 = clientIP
                            });
                        }
                    }
                }
                return returnCode;
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
                return ErrorMapping.ErrorCodes.Exception;
            }
        }

        public static async Task<ErrorCodes> UpdateStatusV2Async(long id, NewsStatus status, long userId, string clientIP, string sessionId)
        {
            var returnCode = ErrorCodes.BusinessError;
            try
            {
                returnCode = await StoreFactory.Create<VideoCacheStore>().UpdateStatusV2Async(id, status, userId, new Action<VideoSearch, int>(async (data, oldStatus) =>
                {
                    //auto phan phoi khi action publish       
                    if (AppSettings.Current.Distribution.Automation.Enabled)
                    {
                        if (status == NewsStatus.Published
                         || (data.Status == (int)NewsStatus.Remove && oldStatus == (int)NewsStatus.Published)
                         || (data.Status == (int)NewsStatus.Draft && oldStatus == (int)NewsStatus.Published))
                        {
                            var action = Media.Action.add;
                            if (oldStatus == (int)NewsStatus.Published)
                            {
                                action = Media.Action.update;
                            }
                            if (status == NewsStatus.Remove)
                            {
                                action = Media.Action.deletebymedia;
                            }
                            //await AutoShare(data, clientIP);
                            await Function.AddToQueue(ActionName.AutoShareVideo, TopicName.AUTOSHAREVIDEO, new QueueData<long, string, string, Media.Action>
                            {
                                Data1 = data.Id,
                                Data2 = clientIP,
                                Data3 = sessionId,
                                Data4 = action
                            });
                        }
                    }
                }));

                if (returnCode == ErrorCodes.Success)
                {
                    await Function.AddToQueue(ActionName.UpdateVideoStatusES, TopicName.ES_VIDEO, id);
                    await Function.AddToQueue(ActionName.UpdateVideoStatusSQL, TopicName.SQL_VIDEO, id);
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
                returnCode = ErrorCodes.Exception;
            }
            return returnCode;
        }

        public static async Task<bool> AutoShare(VideoSearch data, string clientIP, string sessionId, Media.Action action)
        {
            try
            {
                var listItem = Json.Parse<PublishData>(data.PublishData);
                var itemMediaUnit = new List<ItemMediaUnit>();
                if (listItem != null && listItem.Items?.Count > 0)
                {
                    foreach (var item in listItem.Items)
                    {
                        itemMediaUnit.Add(new ItemMediaUnit
                        {
                            Id = data.Id.ToString(),
                            Title = item.Title,
                            ContentType = item.ContentType ?? item.Content_Type,
                            Duration = item.Duration,
                            Link = item.Link,
                            Thumb = item.Thumb,
                            Width = item.Width,
                            Height = item.Height,
                            TypeMedia = item.Type,
                            Position = item.Position,
                            IsPlay = item.IsPlay,
                            CreatedDate = data.DistributionDate ?? DateTime.Now
                        });
                    }
                }

                var user_created = await SecurityRepository.GetByIdAsync(long.Parse(data.NewsInAccount?.LastOrDefault()?.AccountId.ToString() ?? data.CreatedBy));
                var payloadDataShare = new PayloadDataShare
                {
                    DistributionId = AppSettings.Current.Distribution.Automation.DistributorId,
                    TemplateId = 0,
                    Title = string.IsNullOrEmpty(listItem.Title) ? data.Name : listItem.Title,
                    Type = NewsType.Video,
                    MetaDataShared = new List<MetaDataShared>
                    {
                        new MetaDataShared
                        {
                            ObjectId = data.Id,
                            Title = listItem.Caption,
                            Name = data.Name,
                            ZoneId = 0,
                            DistributionDate = data.DistributionDate,
                            CardType = (int)CardType.Video,
                            Frameid = 0,
                            Tags = data.Tags,
                            UserId = user_created?.Id??0,
                            UserName = user_created?.UserName,
                            FullName = user_created?.FullName,
                            AuthorAvatar = user_created?.Avatar,
                            IsDeleted = data.Status == (int)NewsStatus.Remove||data.Status == (int)NewsStatus.Draft ? 1 : (data.Status == (int)NewsStatus.Archive ? 2 : 0),
                            ItemMediaUnit = itemMediaUnit,
                            BoardIds = null,
                            CaptionExt = listItem.CaptionExt,
                            CommentMode = data.CommentMode
                        }
                    }
                };

                var distribution = await DistributionRepository.GetByIdAsync(payloadDataShare.DistributionId);
                var newsDistribution = data.NewsDistribution?.Where(p => p.DistributorId == payloadDataShare.DistributionId).FirstOrDefault();
                var itemStreamDistribution = new ItemStreamDistribution()
                {
                    Id = newsDistribution != null ? newsDistribution.ItemStreamId : Generator.ItemStreamDistributionId(),
                    PublishStatus = data.Status == (int)NewsStatus.Published ? (int)DistributionPublishStatus.Accept : (int)DistributionPublishStatus.Reject,
                    CreatedDate = DateTime.Now,
                    ItemStreamTempId = payloadDataShare.TemplateId,
                    Title = payloadDataShare.Title,
                    MetaData = Json.Stringify(payloadDataShare.MetaDataShared),
                    Type = payloadDataShare.Type
                };

                var result = await DistributionRepository.ShareVideoAsync(itemStreamDistribution, distribution, payloadDataShare, clientIP, sessionId, action);
                if (result)
                {
                    //insert log
                    await Function.AddToQueue(ActionName.InsertLog, TopicName.LOGACTION, new LogActionEntity
                    {
                        ObjectId = data.Id.ToString(),
                        Account = data.ModifiedBy,
                        CreatedDate = DateTime.SpecifyKind(DateTime.Now, DateTimeKind.Unspecified),
                        Ip = clientIP,
                        Message = "Success.",
                        Title = data.Name,
                        SourceId = data.ModifiedBy,
                        ActionTypeDetail = (int)ActionType.Share,
                        ActionText = ActionType.Share.ToString(),
                        Type = (int)LogContentType.Video
                    });
                }
                return result;
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
                return false;
            }

        }
        public static Task<Video> GetByIdAsync(long id)
        {
            return StoreFactory.Create<VideoCacheStore>().GetByIdAsync(id);
        }
        public static Task<VideoSearch> GetById3Async(long id)
        {
            return StoreFactory.Create<VideoCacheStore>().GetById3Async(id);
        }

        public static async Task<PagingDataResult<VideoSearchReturn>> SearchAsync(SearchNews data, long userId, bool isGetDistribution)
        {
            try
            {
                var dataReturn = new PagingDataResult<VideoSearchReturn>();
                if (isGetDistribution == false && string.IsNullOrEmpty(data.Keyword) && data.OrderBy != SearchNewsOrderBy.DescDistributionDate
                    && data.OrderBy != SearchNewsOrderBy.AscDistributionDate && data.OrderBy != SearchNewsOrderBy.DescModifiedDate && data.OrderBy != SearchNewsOrderBy.AscModifiedDate)
                {
                    dataReturn = await StoreFactory.Create<VideoCacheStore>().SearchAsync(data, userId, new Action<string>(async (tempKey) =>
                    {
                        await Function.AddToQueue(ActionName.SetExpire, TopicName.DELETE_INDEX_TEMP, tempKey);
                    }));
                }
                else
                {
                    var dataSearch = await StoreFactory.Create<VideoSearchStore>().SearchAsync(data, userId, isGetDistribution);
                    if (dataSearch?.Data?.Count() > 0)
                    {
                        var listData = await GetListVideoSearchByIdsAsync(dataSearch.Data.Select(p => p.Id.ToString()).ToList(), dataSearch);
                        if (listData != null)
                        {
                            dataReturn.Total = dataSearch.Total;
                            dataReturn.Data = listData.Select(p => Mapper<VideoSearchReturn>.Map(p, new VideoSearchReturn())).ToList();
                        }
                    }
                }
                return dataReturn;
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
                return null;
            }
        }

        public static async Task<PagingDataResult<Video>> GetListApproveAsync(SearchNews data, long accountId)
        {
            try
            {
                var dataReturn = new PagingDataResult<Video>();
                if (string.IsNullOrEmpty(data.Keyword) && data.OrderBy != SearchNewsOrderBy.DescDistributionDate
                    && data.OrderBy != SearchNewsOrderBy.AscDistributionDate && data.OrderBy != SearchNewsOrderBy.DescModifiedDate && data.OrderBy != SearchNewsOrderBy.AscModifiedDate)
                {
                    dataReturn = await StoreFactory.Create<VideoCacheStore>().GetListApproveAsync(data, accountId, new Action<string>(async (tempKey) =>
                    {
                        await Function.AddToQueue(ActionName.SetExpire, TopicName.DELETE_INDEX_TEMP, tempKey);
                    }));
                }
                else
                {
                    dataReturn = await StoreFactory.Create<VideoCacheStore>().GetListApproveAsync(data, accountId, new Action<string>(async (tempKey) =>
                    {
                        await Function.AddToQueue(ActionName.SetExpire, TopicName.DELETE_INDEX_TEMP, tempKey);
                    }));
                }
                return dataReturn;
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
                return null;
            }
        }

        public static Task<List<Video>> GetListByIdsAsync(List<string> ids)
        {
            return StoreFactory.Create<VideoCacheStore>().GetListByIdsAsync(ids);
        }
        public static Task<List<VideoSearch>> GetListVideoSearchByIdsAsync(List<string> ids, PagingDataResult<VideoSearch> listData)
        {
            return StoreFactory.Create<VideoCacheStore>().GetListVideoSearchByIdsAsync(ids, listData);
        }

        public static List<VideoSearch> GetListVideoSearchByIds(List<string> ids, PagingDataResult<VideoSearch> listData)
        {
            return StoreFactory.Create<VideoCacheStore>().GetListVideoSearchByIds(ids, listData);
        }

        public static Task<PagingDataResult<VideoSearch>> MyVideoAsync(SearchNews data, Account acc)
        {
            return StoreFactory.Create<VideoSearchStore>().MyVideoAsync(data, acc);
        }
        
        public static async Task<bool> AddToPlaylistAsync(long videoId, List<VideoPlaylist> dataDb)
        {
            var returnValue = false;
            try
            {
                returnValue = await StoreFactory.Create<VideoPlaylistCacheStore>().AddVideoAsync(videoId, dataDb);
                if (returnValue)
                {
                    await Function.AddToQueue(ActionName.AddVideoToPlaylistSQL, TopicName.SQL_VIDEO, new QueueData<long, List<VideoPlaylist>, object, object>
                    {
                        Data1 = videoId,
                        Data2 = dataDb
                    });

                    if (dataDb != null)
                    {
                        foreach(var playlist in dataDb)
                        {
                            if(playlist?.Status == (int)NewsStatus.Published)
                            {
                                await Function.AddToQueue(ActionName.AutoShareVideoPlaylist, TopicName.AUTOSHAREVIDEOPLAYLIST, new QueueData<long, string, string, Media.Action>
                                {
                                    Data1 = playlist.Id,
                                    Data2 = "192.168.25.291",
                                    Data3 = "sessionId",
                                    Data4 = Media.Action.update
                                });
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
            }
            return returnValue;
        }

        public static async Task<bool> PushVideoToKingHub(string sessionId, PayloadDataShare playloadDataShare, Media.Action action)
        {
            try
            {
                if (playloadDataShare != null && playloadDataShare.MetaDataShared?.Count > 0)
                {
                    var media = playloadDataShare.MetaDataShared.FirstOrDefault();

                    var is_comment = AppSettings.Current.ChannelConfiguration.ApiCommentSetting.Mode.PostCheck;
                    if (media.CommentMode == CommentMode.Disable)
                    {
                        is_comment = AppSettings.Current.ChannelConfiguration.ApiCommentSetting.Mode.Disable;
                    }
                    else
                    {
                        if (media.CommentMode == CommentMode.PreCheck) is_comment = AppSettings.Current.ChannelConfiguration.ApiCommentSetting.Mode.PreCheck;
                    }

                    var post = new
                    {
                        posts = new List<object>{ new {
                            media_id = media.ObjectId.ToString(),
                            title = media.Title??string.Empty,
                            mediaunit_name = media.Name??string.Empty,
                            card_type = (int) CardType.Video,
                            preview=new{ },
                            media_unit_desc = media.ItemMediaUnit?.Select(s=> new {
                                id = s.Id??string.Empty,
                                title = s.Title??string.Empty,
                                link = s.Link??string.Empty,
                                thumb = s.Thumb??string.Empty,
                                type = s.TypeMedia??string.Empty,
                                duration = s.Duration??string.Empty,
                                width = s.Width,
                                height = s.Height,
                                content_type = (int?) s.ContentType ?? (int)ContentType.Video,
                                position = s.Position,
                                is_play = s.IsPlay,                                
                                item_desc = s.Description??string.Empty,
                                created_at = (s.CreatedDate??DateTime.Now).ToString("yyyy-MM-ddTHH:mm:ssZ")
                            }).ToList(),
                            channel_id = media.ChannelId??string.Empty,
                            frame_id = media.Frameid,
                            tags = media.Tags??string.Empty,
                            user_id = media.UserId.ToString(),
                            user_name = media.UserName??string.Empty,
                            full_name = media.FullName??string.Empty,
                            avatar = media.AuthorAvatar??string.Empty,
                            cat_id = media.ZoneId?.ToString()??string.Empty,
                            create_time = Utility.ConvertToTimestamp(media.DistributionDate??DateTime.Now),
                            cat_name = media.ZoneName??string.Empty,
                            extension = new {
                                status = media.CaptionExt??new Caption[]{},
                                flag = new
                                {
                                    is_comment
                                },
                                is_web = 1,
                                boards=media.BoardIds != null && media.BoardIds.Length > 0 ? string.Join(",", media.BoardIds) : string.Empty                                
                            },
                        }}
                    };

                    //hearder thay vì session-id , các anh truyền vào user-id của page hoặc user nhé
                    sessionId = media.UserId.ToString();

                    if (action == Media.Action.add)
                    {
                        var errorCodeReturn = await Media.Add(media.ObjectId.ToString(), sessionId, CardType.Video, post);

                        if (errorCodeReturn == ErrorCodes.DataExist)
                        {
                            action = Media.Action.update;
                        }
                        else
                        {
                            return errorCodeReturn == ErrorCodes.Success ? true : false;
                        }
                    }

                    if (action == Media.Action.update)
                    {
                        return await Media.Update(media.ObjectId.ToString(), sessionId, CardType.Video, post) == ErrorCodes.Success ? true : false;
                    }

                    if (action == Media.Action.deletebymedia)
                    {
                        return await Media.Delete(media.ObjectId.ToString(), sessionId, CardType.Video) == ErrorCodes.Success ? true : false;
                    }
                    //action ko hỗ trợ
                    return false;
                }

                return false;
            }
            catch (Exception ex)
            {
                Logger.Error(ex, "PushBlogToKingHub: " + ex.Message);
                return false;
            }
        }
    }
}
