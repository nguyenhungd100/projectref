﻿using ChannelVN.IMS2.Core.Entities;
using ChannelVN.IMS2.Core.Entities.Distribution;
using ChannelVN.IMS2.Core.Entities.Gallery;
using ChannelVN.IMS2.Core.Entities.Media;
using ChannelVN.IMS2.Core.Entities.Security;
using ChannelVN.IMS2.Core.Models.CacheStore.Gallery;
using ChannelVN.IMS2.Core.Models.DataStore.Gallery;
using ChannelVN.IMS2.Core.Models.ExternalAPI;
using ChannelVN.IMS2.Core.Models.SearchStore.Gallery;
using ChannelVN.IMS2.Core.Repositories.Constants;
using ChannelVN.IMS2.Core.Repositories.Functions;
using ChannelVN.IMS2.Foundation.Common;
using ChannelVN.IMS2.Foundation.Common.Configuration;
using ChannelVN.IMS2.Foundation.Data;
using ChannelVN.IMS2.Foundation.Data.Factories;
using ChannelVN.IMS2.Foundation.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static ChannelVN.IMS2.Core.Entities.ErrorCode.ErrorMapping;

namespace ChannelVN.IMS2.Core.Repositories
{
    public class GalleryRepository: Repository
    {
        public static async Task<ErrorCodes> AddAsync(GallerySearch data, string clientIP)
        {
            try
            {
                var returnValue = await StoreFactory.Create<GalleryCacheStore>().AddAsync(data);
                if (returnValue == ErrorCodes.Success)
                {
                    var res = await StoreFactory.Create<GallerySearchStore>().AddAsync(data);
                    if (!res)
                    {
                        //push queue
                        await Function.AddToQueue(ActionName.AddGalleryES, TopicName.ES_GALLERY, data.Id);
                    }

                    res = await StoreFactory.Create<GalleryDataStore>().AddAsync(data);
                    if (!res)
                    {
                        //push queue
                        await Function.AddToQueue(ActionName.AddGallerySQL, TopicName.SQL_GALLERY, data.Id);
                    }
                    //if (data.Status == (int)NewsStatus.Archive && AppSettings.Current.Distribution.Automation.Enabled)
                    //{
                    //    await Function.AddToQueue(ActionName.AutoShareGallery, TopicName.AUTOSHAREGALLERY, new QueueData<long, string, long[], object>
                    //    {
                    //        Data1 = data.Id,
                    //        Data2 = clientIP
                    //    });
                    //    //await AutoShare(data, clientIP);
                    //}
                }
                return returnValue;
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
                return ErrorCodes.Exception;
            }
        }

        public static async Task<ErrorCodes> UpdateAsync(GallerySearch data, string clientIP, long OfficerId, string sessionId)
        {
            try
            {
                var returnValue = await StoreFactory.Create<GalleryCacheStore>().UpdateV2Async(data, OfficerId, new Action<int>(async (oldStatus) => {
                    //auto phan phoi khi action publish      
                    if (AppSettings.Current.Distribution.Automation.Enabled)
                    {
                        if (oldStatus == (int)NewsStatus.Published)
                        {
                            var action = Media.Action.update;
                            await Function.AddToQueue(ActionName.AutoShareGallery, TopicName.AUTOSHAREGALLERY, new QueueData<long, string, string, Media.Action>
                            {
                                Data1 = data.Id,
                                Data2 = clientIP,
                                Data3 = sessionId,
                                Data4 = action
                            });

                            await Function.AddToQueue(ActionName.PushChangeNotify, TopicName.PUSHBOARDTOCHANNEL, data.Id);
                        }
                    }
                }));
                if (returnValue == ErrorCodes.Success)
                {
                    await Function.AddToQueue(ActionName.UpdateGalleryES, TopicName.ES_GALLERY, data.Id);
                    await Function.AddToQueue(ActionName.UpdateGallerySQL, TopicName.SQL_GALLERY, data.Id);                    
                }
                return returnValue;
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
                return ErrorCodes.Exception;
            }
        }

        public static async Task<bool> AutoShare(GallerySearch data, string clientIP, string sessionId, Media.Action action)
        {
            var returnValue = false;
            try
            {
                var listItem = Json.Parse<PublishData>(data.PublishData);
                var cover = Json.Parse<Cover>(data.Cover);
                var itemAlbum = new List<ItemMediaUnit>();
                if (listItem != null && listItem.Items?.Count > 0)
                {
                    foreach (var item in listItem.Items)
                    {
                        itemAlbum.Add(new ItemMediaUnit
                        {
                            Id = item.Id,
                            Title = item.Title,
                            ContentType = item.ContentType ?? item.Content_Type,
                            Duration = item.Duration,
                            Link = item.Link,
                            Thumb = item.Thumb,
                            Width = item.Width,
                            Height = item.Height,
                            TypeMedia = item.Type,
                            Position = item.Position,
                            IsPlay = item.IsPlay,
                            IsHeartDrop = item.IsHeartDrop,
                            CreatedDate = data.ModifiedDate?? DateTime.Now //data.PhotoInAlbum?.Where(p => p.PhotoId.ToString().Equals(item.Id))?.FirstOrDefault()?.PublishedDate ?? DateTime.Now
                        });
                    }

                    var pageAccount = await SecurityRepository.GetByIdAsync(long.Parse(data.NewsInAccount?.LastOrDefault()?.AccountId.ToString()));
                    var payloadDataShare = new PayloadDataShare
                    {
                        DistributionId = AppSettings.Current.Distribution.Automation.DistributorId,
                        TemplateId = data.TemplateId ?? (int)TemplateId.Album2,
                        Title = listItem.Caption,
                        Type = NewsType.Album,
                        MetaDataShared = new List<MetaDataShared>
                        {
                            new MetaDataShared
                            {
                                ObjectId = data.Id,
                                Title = data.Name,
                                Name = data.Name,
                                ZoneId = 0,
                                DistributionDate = data.DistributionDate,
                                CardType = data.CardType??22,
                                Frameid = data.TemplateId ?? 0 ,
                                Tags = data.Tags,
                                UserName = pageAccount?.UserName,
                                UserId = pageAccount?.Id??0,
                                FullName = pageAccount?.FullName,
                                Description = data.Description,
                                AuthorAvatar = pageAccount?.Avatar,
                                IsDeleted = data.Status == (int)NewsStatus.Remove||data.Status == (int)NewsStatus.Draft ? 1: (data.Status ==(int)NewsStatus.Archive ? 2 : 0),
                                BoardIds = null,
                                ItemMediaUnit = itemAlbum,
                                CaptionExt = listItem.CaptionExt,
                                Cover = cover,
                                CommentMode = data.CommentMode,
                                LabelType = (LabelType?) pageAccount.LabelMode// data.LabelType
                            }
                        }
                    };

                    var distribution = await DistributionRepository.GetByIdAsync(payloadDataShare.DistributionId);

                    //check itemStream => update
                    var itemStreamId = data.NewsDistribution?.Where(s => s.DistributorId == payloadDataShare.DistributionId).FirstOrDefault();
                    var itemStreamDistribution = new ItemStreamDistribution()
                    {
                        Id = itemStreamId?.ItemStreamId ?? Generator.ItemStreamDistributionId(),
                        PublishStatus = (int)DistributionPublishStatus.Accept,
                        CreatedDate = itemStreamId?.SharedDate ?? DateTime.Now,
                        ModifiedDate = DateTime.Now,
                        ItemStreamTempId = payloadDataShare.TemplateId,
                        Title = payloadDataShare.Title,
                        MetaData = Json.Stringify(payloadDataShare.MetaDataShared),
                        Type = payloadDataShare.Type
                    };

                    returnValue = await DistributionRepository.ShareGalleryAsync(itemStreamDistribution, distribution, payloadDataShare, clientIP, sessionId, action);
                    if (returnValue)
                    {
                        await Function.AddToQueue(ActionName.InsertLog, TopicName.LOGACTION, new LogActionEntity
                        {
                            ObjectId = data.Id.ToString(),
                            Account = data.ModifiedBy,
                            CreatedDate = DateTime.SpecifyKind(DateTime.Now, DateTimeKind.Unspecified),
                            Ip = clientIP,
                            Message = "Success.",
                            Title = data.Name,
                            SourceId = data.NewsInAccount?.FirstOrDefault()?.NewsId.ToString(),
                            ActionTypeDetail = (int)ActionType.Share,
                            ActionText = ActionType.Share.ToString(),
                            Type = (int)LogContentType.Gallery
                        });
                    }
                }
                else
                {
                    returnValue = true;
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
            }
            return returnValue;
        }

        public static async Task<ErrorCodes> UpdateStatusV2Async(long id, NewsStatus status, long userId, string clientIP, string sessionId)
        {
            var returnCode = ErrorCodes.BusinessError;
            try
            {
                returnCode = await StoreFactory.Create<GalleryCacheStore>().UpdateStatusV2Async(id, status, userId, new Action<GallerySearch, int>(async (data, oldStatus) =>
                {
                    //auto phan phoi khi action publish   
                    if (AppSettings.Current.Distribution.Automation.Enabled)
                    {
                        if ((status == NewsStatus.Published)
                             || (status == NewsStatus.Remove && oldStatus == (int)NewsStatus.Published)
                             || (status == NewsStatus.Draft && oldStatus == (int)NewsStatus.Published))
                        {
                            var action = Media.Action.add;
                            if (status == NewsStatus.Published && oldStatus == (int)NewsStatus.Published)
                            {
                                action = Media.Action.update;
                            }
                            if (status == NewsStatus.Remove)
                            {
                                action = Media.Action.deletebymedia;
                            }
                            await Function.AddToQueue(ActionName.AutoShareGallery, TopicName.AUTOSHAREGALLERY, new QueueData<long, string, string, Media.Action>
                            {
                                Data1 = data.Id,
                                Data2 = clientIP,
                                Data3=sessionId,
                                Data4=action
                            });
                        }
                    }
                }));

                if (returnCode == ErrorCodes.Success)
                {
                    await Function.AddToQueue(ActionName.UpdateStatusGalleryES, TopicName.ES_GALLERY, id);
                    await Function.AddToQueue(ActionName.UpdateStatusGallerySQL, TopicName.SQL_GALLERY, id);
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
                returnCode = ErrorCodes.Exception;
            }
            return returnCode;
        }
        public static Task<Gallery> GetByIdAsync(long id)
        {
            return StoreFactory.Create<GalleryCacheStore>().GetByIdAsync(id);
        }


        public static Task<GallerySearch> GetDetailByIdAsync(long id)
        {
            return StoreFactory.Create<GalleryCacheStore>().GetAlbumFullInfoAsync(id);
        }
        public static async Task<PagingDataResult<GallerySearch>> SearchAsync(SearchNews search, long accountId)
        {
            try
            {
                var dataReturn = new PagingDataResult<GallerySearch>();
                if (string.IsNullOrEmpty(search.Keyword) && search.OrderBy != SearchNewsOrderBy.DescDistributionDate
                    && search.OrderBy != SearchNewsOrderBy.AscDistributionDate && search.OrderBy != SearchNewsOrderBy.DescModifiedDate && search.OrderBy != SearchNewsOrderBy.AscModifiedDate)
                {
                    dataReturn = await StoreFactory.Create<GalleryCacheStore>().SearchAsync(search, accountId, async (tempkey) =>
                    {
                        await Function.AddToQueue(ActionName.SetExpire, TopicName.DELETE_INDEX_TEMP, tempkey);
                    });
                }
                else
                {
                    var dataSearch = await StoreFactory.Create<GallerySearchStore>().SearchAsync(search, accountId);
                    if (dataSearch?.Data?.Count > 0)
                    {
                        dataSearch.Data = await StoreFactory.Create<GalleryCacheStore>().GetListAlbumSearchByIdsAsync(dataSearch.Data.Select(p => p.Id.ToString()).ToList());
                        dataReturn.Total = dataSearch.Total;
                        dataReturn.Data = dataSearch.Data.Select(p => Mapper<GallerySearch>.Map(p, new GallerySearch())).ToList();
                    }
                }
                return dataReturn;
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
                return null;
            }
        }

        public static async Task<bool> PushGalleryToKingHub(string sessionId, PayloadDataShare playloadDataShare, Media.Action action)
        {
            try
            {
                if (playloadDataShare != null && playloadDataShare.MetaDataShared?.Count > 0)
                {
                    var media = playloadDataShare.MetaDataShared.FirstOrDefault();

                    var is_comment = AppSettings.Current.ChannelConfiguration.ApiCommentSetting.Mode.PostCheck;
                    if (media.CommentMode == CommentMode.Disable)
                    {
                        is_comment = AppSettings.Current.ChannelConfiguration.ApiCommentSetting.Mode.Disable;
                    }
                    else
                    {
                        if (media.CommentMode == CommentMode.PreCheck) is_comment = AppSettings.Current.ChannelConfiguration.ApiCommentSetting.Mode.PreCheck;
                    }
                    var cardType = CardType.Gallery1;
                    if (media.CardType == (int)CardType.Gallery2)
                        cardType = CardType.Gallery2;

                    var post = new
                    {
                        posts = new List<object>{ new {
                            media_id = media.ObjectId.ToString(),
                            title = media.Title??string.Empty,
                            mediaunit_name = media.Name??string.Empty,
                            card_type = (int)cardType,
                            preview=new{ },
                            media_unit_desc = media.ItemMediaUnit?.Select(s=> new {
                                id = s.Id ?? string.Empty,
                                title = s.Title ?? string.Empty,
                                link = s.Link ?? string.Empty,
                                thumb = string.IsNullOrEmpty(s.Thumb) ? s.Link : s.Thumb,
                                width = s.Width,
                                height = s.Height,
                                content_type = s.ContentType == ContentType.Image ? (int)s.ContentType : (int)ContentType.Photo,
                                created_at = (s.CreatedDate??DateTime.Now).ToString("yyyy-MM-ddTHH:mm:ssZ")
                            }).ToList(),
                            channel_id = media.ChannelId??string.Empty,
                            frame_id = media.Frameid,
                            tags = media.Tags??string.Empty,
                            user_id = media.UserId.ToString(),
                            user_name = media.UserName??string.Empty,
                            full_name = media.FullName??string.Empty,
                            avatar = media.AuthorAvatar??string.Empty,
                            cat_id = media.ZoneId?.ToString()??string.Empty,
                            create_time = Utility.ConvertToTimestamp(media.DistributionDate??DateTime.Now),
                            cat_name = media.ZoneName??string.Empty,
                            extension = new {
                                status = media.CaptionExt??new Caption[]{ },
                                flag = new
                                {
                                    is_comment
                                },
                                is_web = 1,
                                boards=media.BoardIds != null && media.BoardIds.Length > 0 ? string.Join(",", media.BoardIds) : string.Empty,
                                gallery = new {
                                    cover = new {
                                        link = media?.Cover?.Src??string.Empty,
                                        width = media?.Cover?.Width??0,
                                        height = media?.Cover?.Height??0,
                                        thumb = media?.Cover?.Thumb??string.Empty
                                    },
                                    description = media.Description
                                },
                            },
                        }}
                    };

                    //hearder thay vì session-id , các anh truyền vào user-id của page hoặc user nhé
                    sessionId = media.UserId.ToString();

                    if (action == Media.Action.add)
                    {
                        var errorCodeReturn = await Media.Add(media.ObjectId.ToString(), sessionId, cardType, post);
                        if (errorCodeReturn == ErrorCodes.DataExist)
                        {
                            action = Media.Action.update;
                        }
                        else
                        {
                            return errorCodeReturn == ErrorCodes.Success ? true : false;
                        }
                    }

                    if (action == Media.Action.update)
                    {
                        return await Media.Update(media.ObjectId.ToString(), sessionId, cardType, post) == ErrorCodes.Success ? true : false;
                    }

                    if (action == Media.Action.deletebymedia)
                    {
                        return await Media.Delete(media.ObjectId.ToString(), sessionId, cardType) == ErrorCodes.Success ? true : false;
                    }
                    //action ko hỗ trợ
                    return false;
                }

                return false;
            }
            catch (Exception ex)
            {
                Logger.Error(ex, "PushBlogToKingHub: " + ex.Message);
                return false;
            }
        }

    }
}
