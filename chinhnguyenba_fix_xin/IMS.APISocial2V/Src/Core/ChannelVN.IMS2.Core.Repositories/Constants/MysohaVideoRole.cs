﻿namespace ChannelVN.IMS2.Core.Repositories.Constants
{
    public enum MysohaVideoRole
    {
        Editor = 1,
        Manager = 2,
        Collaborator = 3
    }
}
