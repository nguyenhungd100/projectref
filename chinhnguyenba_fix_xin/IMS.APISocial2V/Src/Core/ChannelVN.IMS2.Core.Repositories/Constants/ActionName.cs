﻿namespace ChannelVN.IMS2.Core.Repositories.Constants
{
    public class ActionName
    {
        #region Account
        //Account
        public const string AddAccountES = "add_account_es";
        public const string AddAccountSQL = "add_account_sql";
        public const string AddOfficialAccountES = "add_official_account_es";
        public const string AddOfficialAccountSQL = "add_official_account_sql";
        public const string AddProfileES = "add_profile_es";
        public const string AddProfileSQL = "add_profile_sql";
        public const string UpdateAccountES = "update_account_es";
        public const string UpdateAccountSQL = "update_account_sql";
        public const string UpdateAccountTypeSQL = "update_account_type_sql";
        public const string AcceptOrRejectES = "accept_or_reject_es";
        public const string AcceptOrRejectSQL = "accept_or_reject_sql";
        public const string UpdateAccountStatusES = "update_account_status_es";
        public const string UpdateAccountStatusSQL = "update_account_status_sql";
        public const string AddAccountMemberSQL = "add_account_member_sql";
        public const string UpdateAccountMemberSQL = "update_account_member_sql";
        public const string UpdateAccountMemberES = "update_account_member_es";
        public const string RemoveMemberSQL = "remove_member_sql";
        public const string RemoveMemberES = "remove_member_es";
        public const string GetUserInfoAndUpdate = "get_user_info_and_update";
        public const string RemoveMemberOnAllPageES = "remove_member_on_all_page_es";
        public const string RemoveMemberOnAllPageSQL = "remove_member_on_all_page_sql";
        public const string SetProfileIsPageOnAppSQL = "set_profile_is_page_on_app_sql";
        public const string SetProfileIsPageOnApp = "set_profile_is_page_on_app";
        public const string UpdateProfileUrl = "update_profile_url";
        public const string UpdateProfileUrlSql = "update_profile_url_sql";
        public const string UpdatePageTypeToApp = "update_page_type_to_app";
        public const string UpdateAccessToken = "update_access_token";
        public const string UpdateAccountClassSQL = "update_account_class_sql";

        public const string LockOrUnlockMemberSQL = "lock_member_sql";
        public const string LockOrUnlockMemberES = "lock_member_es";
        public const string LockOrUnlockMemberOnAllPageES = "lock_or_unlock_member_on_all_page_es";
        public const string LockOrUnlockMemberOnAllPageSQL = "lock_or_unlock_member_on_all_page_sql";

        public const string UpdateUserInfoOnApp = "update_user_info_on_app";
        public const string ApprovedOnApp = "approved_on_app";
        public const string AddUserMember = "add_user_member";
        public const string RetryUpdateUserInfoOnApp = "retry_update_user_info_on_app";
        public const string ChangePageOwner = "change_page_owner";
        public const string InsertPageOwnerToApp = "insert_page_owner_to_app";
        public const string SettingCommentOnApp = "setting_comment_on_app";
        public const string CheckUserInfoOnApp = "check_user_info_on_app";

        public const string ChangePageOwnerES = "change_page_owner_es";
        public const string ChangePageOwnerSQL = "change_page_owner_sql";

        public const string ConfigPageES = "config_page_es";
        public const string ConfigPageSQL = "config_page_sql";

        public const string PushPageOwnerAndTypeToAdtech = "push_page_owner_and_type_to_adtech";

        //chua dung den
        public const string SetRoleKingHubAccount = "set_role_kinghub_account";
        //public const string AcceptOrRejectOrLockWriterES = "accept_or_reject_or_lock_writer_es";
        //public const string AcceptOrRejectOrLockWriterSQL = "accept_or_reject_or_lock_writer_sql";
        //public const string DeactiveAccountES = "deactive_account_es";
        //public const string DeactiveAccountSQL = "deactive_account_sql";
        //public const string AddAccountMemberES = "add_account_member_es";

        //public const string UpdateAccountMemberES = "update_account_member_es";


        //end chua dung
        #endregion

        #region Category
        //public const string AddCategoryES = "add_category_es";
        //public const string AddCategorySQL = "add_category_sql";
        //public const string UpdateCategoryES = "update_category_es";
        //public const string UpdateCategorySQL = "update_category_sql";
        #endregion

        #region Distributor
        //Distribution//
        public const string AddDistributionSQL = "add_distribution_sql";
        public const string UpdateDistributionSQL = "update_distribution_sql";
        #endregion

        #region Article
        //article
        public const string AddArticleES = "add_article_es";
        public const string AddArticleSQL = "add_article_sql";
        public const string UpdateArticleES = "update_article_es";
        public const string UpdateArticleSQL = "update_article_sql";
        public const string UpdateStatusArticleES = "update_status_article_es";
        public const string UpdateStatusArticleSQL = "update_status_article_sql";
        //public const string PublishNewsES = "publish_news_es";
        //public const string PublishNewsSQL = "publish_news_sql";
        public const string ShareArticleES = "share_article_es";
        public const string ShareArticleSQL = "share_article_sql";

        #endregion

        #region ItemStream
        public const string AcceptItemStreamES = "accept_itemstream_es";
        public const string AcceptItemStreamSQL = "accept_itemstream_sql";
        public const string RejectItemStreamES = "reject_itemstream_es";
        public const string RejectItemStreamSQL = "reject_itemstream_sql";
        #endregion

        #region Tag
        //Tag
        public const string AddTagES = "add_tag_es";
        public const string AddTagSQL = "add_tag_sql";
        public const string UpdateTagES = "update_tag_es";
        public const string UpdateTagSQL = "update_tag_sql";
        //End Tag
        #endregion

        #region Playlist
        //Playlist
        public const string AddVideoPlaylistES = "add_video_playlist_es";
        public const string AddVideoPlaylistSQL = "add_video_playlist_sql";
        public const string UpdateVideoPlaylistStatusES = "update_video_playlist_status_es";
        public const string UpdateVideoPlaylistStatusSQL = "update_video_playlist_status_sql";
        public const string UpdateVideoPlaylistES = "update_video_playlist_es";
        public const string UpdateVideoPlaylistSQL = "update_video_playlist_sql";
        public const string ShareVideoPlaylistES = "share_video_playlist_es";
        public const string ShareVideoPlaylistSQL = "share_video_playlist_sql";
        public const string AddVideoToPlaylistSQL = "add_video_to_playlist_sql";
        #endregion

        #region Video
        //Video
        public const string AddVideoES = "add_video_es";
        public const string AddVideoSQL = "add_video_sql";
        public const string UpdateVideoES = "update_video_es";
        public const string UpdateVideoSQL = "update_video_sql";
        public const string UpdateVideoStatusES = "update_video_status_es";
        public const string UpdateVideoStatusSQL = "update_video_status_sql";
        public const string ShareVideoES = "share_video_es";
        public const string ShareVideoSQL = "share_video_sql";
        //public const string PublishVideoES = "publish_video_es";
        //public const string PublishVideoSQL = "publish_video_sql";

        //public const string SharePostES = "share_post_es";
        #endregion

        #region Post
        //Post
        public const string AddPostES = "add_post_es";
        public const string AddPostSQL = "add_post_sql";
        public const string UpdatePostES = "update_post_es";
        public const string UpdatePostSQL = "update_post_sql";
        public const string UpdatePostStatusES = "update_post_status_es";
        public const string UpdatePostStatusSQL = "update_post_status_sql";
        public const string SharePostES = "share_post_es";
        public const string SharePostSQL = "share_post_sql";
        #endregion

        #region Media unit
        //Media unit
        public const string AddMediaUnitES = "add_media_unit_es";
        public const string AddMediaUnitSQL = "add_media_unit_sql";
        public const string UpdateMediaUnitES = "update_media_unit_es";
        public const string UpdateMediaUnitSQL = "update_media_unit_sql";
        public const string UpdateStatusMediaUnitES = "update_status_media_unit_es";
        public const string UpdateStatusMediaUnitSQL = "update_status_media_unit_sql";
        public const string ShareMediaUnitES = "share_media_unit_es";
        public const string ShareMediaUnitSQL = "share_media_unit_sql";
        #endregion

        #region Album
        //Album
        public const string AddAlbumES = "add_album_es";
        public const string AddAlbumSQL = "add_album_sql";
        public const string UpdateAlbumES = "update_album_es";
        public const string UpdateAlbumSQL = "update_album_sql";
        public const string UpdateStatusAlbumES = "update_status_album_es";
        public const string UpdateStatusAlbumSQL = "update_status_album_sql";
        public const string ShareAlbumES = "share_album_es";
        public const string ShareAlbumSQL = "share_album_sql";
        public const string AddPhotoToAlbumSQL = "add_photo_to_album_sql";
        #endregion

        #region Gallery
        //Album
        public const string AddGalleryES = "add_gallery_es";
        public const string AddGallerySQL = "add_Gallery_sql";
        public const string UpdateGalleryES = "update_Gallery_es";
        public const string UpdateGallerySQL = "update_Gallery_sql";
        public const string UpdateStatusGalleryES = "update_status_Gallery_es";
        public const string UpdateStatusGallerySQL = "update_status_Gallery_sql";
        public const string ShareGalleryES = "share_Gallery_es";
        public const string ShareGallerySQL = "share_Gallery_sql";
        public const string AddPhotoToGallerySQL = "add_photo_to_Gallery_sql";
        #endregion

        #region Photo
        //Photo//
        public const string AddPhotoES = "add_photo_es";
        public const string AddPhotoSQL = "add_photo_sql";
        public const string UpdatePhotoES = "update_photo_es";
        public const string UpdatePhotoSQL = "update_photo_sql";
        public const string UpdateStatusPhotoES = "update_status_photo_es";
        public const string UpdateStatusPhotoSQL = "update_status_photo_sql";
        public const string SharePhotoSQL = "share_photo_sql";
        public const string SharePhotoES = "share_photo_es";
        #endregion

        #region Template
        public const string AddTemplateES = "add_template_es";
        public const string AddTemplateRedis = "add_template_redis";
        public const string UpdateTemplateES = "update_template_es";
        public const string UpdateTemplateRedis = "update_template_redis";
        public const string UpdateStatusTemplateES = "update_status_template_es";

        public const string UpdateStatusTemplateSQL = "update_status_template_sql";
        #endregion

        #region ShareLink
        public const string AddShareLinkES = "add_sharelink_es";
        public const string AddShareLinkSQL = "add_sharelink_sql";
        public const string UpdateShareLinkES = "update_sharelink_es";
        public const string UpdateShareLinkSQL = "update_sharelink_sql";
        public const string UpdateShareLinkStatusES = "update_sharelink_status_es";
        public const string UpdateShareLinkStatusSQL = "update_sharelink_status_sql";
        public const string ShareShareLinkES = "share_sharelink_es";
        public const string ShareShareLinkSQL = "share_sharelink_sql";
        #endregion

        #region Board
        public const string AddBoardES = "add_board_es";
        public const string AddBoardSQL = "add_board_sql";
        public const string UpdateBoardES = "update_board_es";
        public const string UpdateBoardSQL = "update_board_sql";
        public const string AddNewsToBroadES = "add_news_to_board_es";
        public const string AddNewsToBoardSQL = "add_news_to_board_sql";
        public const string DeleteNewsInBroadES = "delete_news_in_board_es";
        public const string DeleteNewsInBoardSQL = "delete_news_in_board_sql";
        public const string PushBoardToChannel = "push_board_to_channel";
        public const string UpdateMediaSQL = "update_media_sql";
        public const string InsertMediaSQL = "insert_media_sql";

        public const string AddNewsToBoard = "add_news_to_board";

        public const string PushChangeNotify = "push_change_notify";

        public const string PushTelegramNotify = "push_telegram_notify";
        public const string PushNotificationPostUsers = "push_notification_post_users";
        #endregion

        #region Beam
        public const string AddBeamES = "add_beam_es";
        public const string AddBeamSQL = "add_beam_sql";
        public const string UpdateBeamES = "update_beam_es";
        public const string UpdateBeamSQL = "update_beam_sql";
        public const string UpdateStatusBeamES = "update_status_beam_es";
        public const string UpdateStatusBeamSQL = "update_status_beam_sql";
        public const string ShareBeamSQL = "share_beam_sql";
        public const string ShareBeamES = "share_beam_es";
        #endregion

        # region Share
        //PushVideoToChannel
        public const string ShareMediaUnit = "share_mediaunit";
        public const string ShareArticle = "share_article";
        public const string ShareVideo = "share_video";
        public const string SharePlayList = "share_playlist";
        public const string SharePhoto = "share_photo";
        public const string SharePost = "share_post";
        public const string ShareShareLink = "share_sharelink";
        public const string ShareAlbum = "share_album";
        public const string ShareBeam = "share_beam";
        public const string ShareGallery = "share_gallery";
        #endregion

        #region AutoShare
        public const string AutoShareVideo = "auto_share_video";
        public const string AutoShareVideoPlaylist = "auto_share_video_playlist";
        public const string AutoSharePost = "auto_share_post";
        public const string AutoShareShareLink = "auto_share_sharelink";
        public const string AutoShareBeam = "auto_share_beam";
        public const string AutoShareArticle = "auto_share_article";

        public const string AutoShareAlbum = "auto_share_album";
        public const string AutoShareAlbumOld = "auto_share_album_old";//tam
        public const string AutoShareMediaUnit = "auto_share_media_unit";
        public const string AutoSharePhoto = "auto_share_photo";
        public const string AutoSharePhotoOld = "auto_share_photo_old";//tạm
        public const string AutoShareGallery = "auto_share_gallery";
        #endregion

        #region Logaction
        public const string InsertLog = "insert_log";
        #endregion

        #region Delete Index Temp
        public const string SetExpire = "set_expire";
        #endregion

        #region System data
        public const string ConvertDataSQL = "convert_data_sql";
        public const string UpdateUserId = "update_user_id";
        public const string InitAll = "init_all";
        #endregion

        #region Send CRM
        public const string SendCrm = "sendcrm";
        #endregion

        #region NewsCrawler        
        public const string AddNewsCrawlerES = "add_newscrawler_es";
        public const string AddNewsCrawlerSQL = "add_newscrawler_sql";
        public const string UpdateNewsCrawlerES = "update_newscrawler_es";
        public const string UpdateNewsCrawlerSQL = "update_newscrawler_sql";

        public const string AddNewsCrawlerConfigES = "add_newscrawlerconfig_es";
        public const string AddNewsCrawlerConfigSQL = "add_newscrawlerconfig_sql";
        public const string UpdateNewsCrawlerConfigES = "update_newscrawlerconfig_es";
        public const string UpdateNewsCrawlerConfigSQL = "update_newscrawlerconfig_sql";
        public const string AddLinkAutoNewsCrawler = "add_link_auto";
        #endregion

        #region NewsOnHome
        public const string AddNewsOnHomeES = "add_newsonhome_es";
        public const string AddNewsOnHomeSQL = "add_newsonhome_sql";
        public const string UpdateNewsOnHomeES = "update_newsonhome_es";
        public const string UpdateNewsOnHomeSQL = "update_newsonhome_sql";
        #endregion

        #region AccountAssignment
        public const string AddAccountAssignmentES = "add_AccountAssignment_es";
        public const string AddAccountAssignmentSQL = "add_AccountAssignment_sql";
        public const string UpdateAccountAssignmentES = "update_AccountAssignment_es";
        public const string UpdateAccountAssignmentSQL = "update_AccountAssignment_sql";
        public const string RemoveAccountAssignmentES = "remove_AccountAssignment_es";
        public const string RemoveAccountAssignmentSQL = "remove_AccountAssignment_sql";
        #endregion
    }

    public class TopicName
    {
        public const string SHARE = "share";
        public const string ROLEKINGHUB = "setrolekinghub";

        public const string SYNC_USERINFO = "sync_userinfo";
        public const string RETRY_SYNC_USERINFO = "retry_sync_userinfo";
        
        public const string ES_ARTICLE = "es_article";
        public const string ES_POST = "es_post";
        public const string ES_VIDEO = "es_video";
        public const string ES_PLAYLIST = "es_playlist";
        public const string ES_TAG = "es_tag";
        public const string ES_MEDIA_UNIT = "es_media_unit";
        public const string ES_ALBUM = "es_album";
        public const string ES_PHOTO = "es_photo";
        public const string ES_TEMPLATE = "es_template";
        public const string ES_ACCOUNT = "es_account";
        public const string ES_ITEMSTREAM = "es_itemstream";
        public const string ES_DISTRIBUTOR = "es_distributor";
        public const string ES_SHARE_LINK = "es_share_link";
        public const string ES_BOARD = "es_board";
        public const string ES_BEAM = "es_beam";
        public const string ES_GALLERY = "es_gallery";
        public const string ES_NEWSCRAWLER = "es_newscrawler";
        public const string ES_NEWSCRAWLERCONFIG = "es_newscrawlerconfig";
        public const string ES_NEWSONHOME = "es_newsonhome";
        public const string ES_ACCOUNTASSIGNMENT = "es_accountassignment";

        public const string CHECK_AUTONEWSCRAWLER_LINK = "es_checkautonewscrawlerlink";

        public const string REDIS_TEMPLATE = "redis_template";

        public const string SQL_ARTICLE = "sql_article";
        public const string SQL_POST = "sql_post";
        public const string SQL_VIDEO = "sql_video";
        public const string SQL_PLAYLIST = "sql_playlist";
        public const string SQL_TAG = "sql_tag";
        public const string SQL_MEDIA_UNIT = "sql_media_unit";
        public const string SQL_ALBUM = "sql_album";
        public const string SQL_PHOTO = "sql_photo";
        public const string SQL_ACCOUNT = "sql_account";
        public const string SQL_ITEMSTREAM = "sql_itemstream";
        public const string SQL_DISTRIBUTOR = "sql_distributor";
        public const string SQL_TEMPLATE = "sql_template";
        public const string SQL_SHARE_LINK = "sql_share_link";
        public const string SQL_BOARD = "sql_broad";
        public const string SQL_BEAM = "sql_beam";
        public const string SQL_GALLERY = "sql_gallery";
        public const string SQL_NEWSCRAWLER = "sql_newscrawler";
        public const string SQL_NEWSCRAWLERCONFIG = "sql_newscrawlerconfig";
        public const string SQL_CONVERT = "sql_convert";
        public const string SQL_NEWSONHOME = "sql_newsonhome";
        public const string SQL_ACCOUNTASSIGNMENT = "sql_accountassignment";

        //public const string AUTOSHARE = "auto_share";
        public const string AUTOSHAREVIDEO = "auto_share_video";
        public const string AUTOSHAREVIDEOPLAYLIST = "auto_share_video_playlist";
        public const string AUTOSHAREPOST = "auto_share_post";
        public const string AUTOSHARESHARELINK = "auto_share_sharelink";
        public const string AUTOSHAREBEAM = "auto_share_beam";
        public const string AUTOSHAREARTICLE = "auto_share_article";
        public const string AUTOSHAREALBUM = "auto_share_album";
        public const string AUTOSHAREMEDIAUNIT = "auto_share_media_unit";
        public const string AUTOSHAREPHOTO = "auto_share_photo";
        public const string AUTOSHAREGALLERY = "auto_share_gallery";
        //PushBoardToChannel
        public const string PUSHBOARDTOCHANNEL = "push_board_to_channel";

        public const string LOGACTION = "log_action";

        //Delete Index Temp
        public const string DELETE_INDEX_TEMP = "delete_index_temp";
        public const string PUSTPAGEINFO = "push_page_info";

        public const string PUSHTELEGRAMNOTIFY = "push_telegram_notify";
        public const string PUSHNOTIFICATIONAPP = "push_notification_app";
    }
}
