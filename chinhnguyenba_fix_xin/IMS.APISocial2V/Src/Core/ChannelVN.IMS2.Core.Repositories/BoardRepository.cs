﻿using ChannelVN.IMS2.Core.Entities;
using ChannelVN.IMS2.Core.Entities.Board;
using ChannelVN.IMS2.Core.Entities.Security;
using ChannelVN.IMS2.Core.Models.DataStore.Boards;
using ChannelVN.IMS2.Core.Models.SearchStore.Boards;
using ChannelVN.IMS2.Core.Repositories.Constants;
using ChannelVN.IMS2.Core.Repositories.Functions;
using ChannelVN.IMS2.Foundation.Common;
using ChannelVN.IMS2.Foundation.Common.Configuration;
using ChannelVN.IMS2.Foundation.Data;
using ChannelVN.IMS2.Foundation.Data.Factories;
using ChannelVN.IMS2.Foundation.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using static ChannelVN.IMS2.Core.Entities.ErrorCode.ErrorMapping;

namespace ChannelVN.IMS2.Core.Repositories
{
    public class BoardRepository : Repository
    {
        public static async Task<ErrorCodes> AddAsync(BoardSearch data, long userId, string clientIP)
        {
            var returnValue = ErrorCodes.AddError;
            try
            {
                returnValue = await StoreFactory.Create<BoardCacheStore>().AddAsync(Mapper<Board>.Map(data, new Board()), userId, new Action<long>(async (ownerId) =>
                {
                    //push board to channel
                    var content = new FormUrlEncodedContent(new
                    {
                        userid = data.CreatedBy,
                        ownerid = ownerId,
                        boardname = data.Name,
                        boardid = data.Id,
                        avatar = Json.Stringify(new { url = data.Avatar, widh = BoardConstant.AvatarWidth, heigh = BoardConstant.AvatarHeight, content_type = (int)ContentType.Image }),
                        cover = Json.Stringify(new { url = data.Cover, widh = BoardConstant.CoverWidth, heigh = BoardConstant.CoverHeight, content_type = (int)ContentType.Image }),
                    }.ConvertToKeyValuePair());

                    string query = await content?.ReadAsStringAsync();
                    await Function.AddToQueue(ActionName.PushBoardToChannel, TopicName.PUSHBOARDTOCHANNEL, new QueueData<string, string, string, PushInfo>
                    {
                        Data1 = query,
                        Data2 = BoardAction.Add,
                        Data3 = clientIP,
                        Data4 = new PushInfo
                        {
                            ObjectId = data.Id,
                            ModifiedBy = data.CreatedBy
                        }
                    });
                }));

                if (returnValue == ErrorCodes.Success)
                {
                    var res = await StoreFactory.Create<BoardSearchStore>().AddAsync(data);
                    if (!res)
                    {
                        //Push queue
                        await Function.AddToQueue(ActionName.AddBoardES, TopicName.ES_BOARD, data.Id);
                    }

                    res = await StoreFactory.Create<BoardDataStore>().AddAsync(data);
                    if (!res)
                    {
                        //Push queue
                        await Function.AddToQueue(ActionName.AddBoardSQL, TopicName.SQL_BOARD, data.Id);
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
                returnValue = ErrorCodes.Exception;
            }
            return returnValue;
        }


        public static async Task<ErrorCodes> UpdateAsync(Board data, long officerId, string clientIP)
        {
            var returnVL = ErrorCodes.BusinessError;
            try
            {
                var returnValue = await StoreFactory.Create<BoardCacheStore>().UpdateAsync(data, officerId);
                if (returnValue == ErrorCodes.Success)
                {
                    returnVL = ErrorCodes.Success;

                    await Function.AddToQueue(ActionName.UpdateBoardES, TopicName.ES_BOARD, data.Id);
                    await Function.AddToQueue(ActionName.UpdateBoardSQL, TopicName.SQL_BOARD, data.Id);

                    //push board to channel
                    if (data.Status == (int)BoardStatus.Actived)
                    {
                        var content = new FormUrlEncodedContent(new
                        {
                            userid = data.CreatedBy,
                            boardid = data.Id,
                            boardname = data.Name
                        }.ConvertToKeyValuePair());
                        string query = await content?.ReadAsStringAsync();
                        await Function.AddToQueue(ActionName.PushBoardToChannel, TopicName.PUSHBOARDTOCHANNEL, new QueueData<string, string, string, PushInfo>
                        {
                            Data1 = query,
                            Data2 = BoardAction.Update,
                            Data3 = clientIP,
                            Data4 = new PushInfo
                            {
                                ObjectId = data.Id,
                                ModifiedBy = data.ModifiedBy
                            }
                        });
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
                returnVL = ErrorCodes.Exception;
            }
            return returnVL;
        }
        public static async Task<ErrorCodes> UpdateMediaAsync(IEnumerable<long> listAdd, IEnumerable<long> listDelete, Board board)
        {
            var returnVL = ErrorCodes.BusinessError;
            try
            {
                var returnValue = await StoreFactory.Create<BoardCacheStore>().UpdateMediaAsync(listAdd, listDelete, board);
                if (returnValue)
                {
                    returnVL = ErrorCodes.Success;
                    await Function.AddToQueue(ActionName.UpdateBoardSQL, TopicName.SQL_BOARD, new QueueData<IEnumerable<long>, IEnumerable<long>, Board, object>
                    {
                        Data1 = listAdd,
                        Data2 = listDelete,
                        Data3 = board
                    });
                    //push board to channel
                    if (board.Status == (int)BoardStatus.Actived)
                    {
                        //if(listAdd!=null && listAdd.Count() > 0)
                        //{
                        //    var content = new FormUrlEncodedContent(new
                        //    {
                        //        userid = board.CreatedBy,
                        //        boardid = board.Id,
                        //        listposts = string.Join(",", listAdd)
                        //    }.ConvertToKeyValuePair());
                        //    string query = await content?.ReadAsStringAsync();
                        //    await Function.AddToQueue(ActionName.PushBoardToChannel, TopicName.PUSHBOARDTOCHANNEL, new QueueData<string, string, object, object>
                        //    {
                        //        Data1 = query,
                        //        Data2 = "add_news_multi"
                        //    });
                        //}
                        //if (listDelete != null && listDelete.Count() > 0)
                        //{
                        //    var content = new FormUrlEncodedContent(new
                        //    {
                        //        userid = board.CreatedBy,
                        //        boardid = board.Id,
                        //        listposts = string.Join(",", listDelete)
                        //    }.ConvertToKeyValuePair());
                        //    string query = await content?.ReadAsStringAsync();
                        //    await Function.AddToQueue(ActionName.PushBoardToChannel, TopicName.PUSHBOARDTOCHANNEL, new QueueData<string, string, object, object>
                        //    {
                        //        Data1 = query,
                        //        Data2 = "delete_news_multi"
                        //    });
                        //}
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
                returnVL = ErrorCodes.Exception;
            }
            return returnVL;
        }

        public static async Task<bool> InsertMediaAsync(long mediaId, List<Board> dataDb)
        {
            var returnValue = false;
            try
            {
                returnValue = await StoreFactory.Create<BoardCacheStore>().InsertMediaAsync(mediaId, dataDb);
                if (returnValue)
                {
                    await Function.AddToQueue(ActionName.InsertMediaSQL, TopicName.SQL_BOARD, new QueueData<long, List<Board>, object, object>
                    {
                        Data1 = mediaId,
                        Data2 = dataDb
                    });
                }
                return returnValue;
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
                return returnValue;
            }
        }

        public static Task<List<Board>> GetListByIdAsync(long[] boardIds)
        {
            return StoreFactory.Create<BoardCacheStore>().GetListByIdAsync(boardIds);
        }

        public static async Task<ErrorCodes> UpdateStatusAsync(BoardSearch data, int oldStatus, string clientIP)
        {
            var returnVL = ErrorCodes.BusinessError;
            try
            {
                var returnValue = await StoreFactory.Create<BoardCacheStore>().UpdateStatusAsync(Mapper<Board>.Map(data, new Board()), oldStatus);
                if (returnValue)
                {
                    returnVL = ErrorCodes.Success;

                    await Function.AddToQueue(ActionName.UpdateBoardES, TopicName.ES_BOARD, data.Id);
                    await Function.AddToQueue(ActionName.UpdateBoardSQL, TopicName.SQL_BOARD, data.Id);

                    //push board to channel
                    if (data.Status == (int)BoardStatus.UnActived)
                    {
                        var content = new FormUrlEncodedContent(new
                        {
                            userid = data.CreatedBy,
                            boardid = data.Id
                        }.ConvertToKeyValuePair());
                        string query = await content?.ReadAsStringAsync();
                        await Function.AddToQueue(ActionName.PushBoardToChannel, TopicName.PUSHBOARDTOCHANNEL, new QueueData<string, string, string, PushInfo>
                        {
                            Data1 = query,
                            Data2 = BoardAction.Delete,
                            Data3 = clientIP,
                            Data4 = new PushInfo
                            {
                                ObjectId = data.Id,
                                ModifiedBy = data.ModifiedBy
                            }
                        });
                    }
                    else
                    {
                        if (data.Status != oldStatus)
                        {
                            var content = new FormUrlEncodedContent(new
                            {
                                userid = data.CreatedBy,
                                boardname = data.Name,
                                boardid = data.Id,
                                avatar = Json.Stringify(new { url = data.Avatar, widh = BoardConstant.AvatarWidth, heigh = BoardConstant.AvatarHeight, content_type = (int)ContentType.Image }),
                                cover = Json.Stringify(new { url = data.Cover, widh = BoardConstant.CoverWidth, heigh = BoardConstant.CoverHeight, content_type = (int)ContentType.Image }),
                            }.ConvertToKeyValuePair());
                            string query = await content?.ReadAsStringAsync();
                            await Function.AddToQueue(ActionName.PushBoardToChannel, TopicName.PUSHBOARDTOCHANNEL, new QueueData<string, string, string, PushInfo>
                            {
                                Data1 = query,
                                Data2 = BoardAction.Add,
                                Data3 = clientIP,
                                Data4 = new PushInfo
                                {
                                    ObjectId = data.Id,
                                    ModifiedBy = data.CreatedBy
                                }
                            });
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
                returnVL = ErrorCodes.Exception;
            }
            return returnVL;
        }

        public static Task<PagingDataResult<BoardSearch>> ListAsync(long userId, int? status, int? pageIndex, int? pageSize)
        {
            return StoreFactory.Create<BoardCacheStore>().ListAsync(userId, status, pageIndex, pageSize, new Action<string>(async (tempKey) =>
            {
                await Function.AddToQueue(ActionName.SetExpire, TopicName.DELETE_INDEX_TEMP, tempKey);
            }));
        }


        public static Task<Board> GetByIdAsync(long boardId)
        {
            return StoreFactory.Create<BoardCacheStore>().GetByIdAsync(boardId);
        }

        public static async Task<bool> AddNewsAsync(long boardId, string ids, long officerId, long userId, string clientIP)
        {
            var returnValue = false;
            try
            {
                returnValue = await StoreFactory.Create<BoardCacheStore>().AddNewsAsync(boardId, ids, officerId);
                if (returnValue)
                {
                    await Function.AddToQueue(ActionName.AddNewsToBoardSQL, TopicName.SQL_BOARD, new QueueData<long, string, long, object>
                    {
                        Data1 = boardId,
                        Data2 = ids,
                        Data3 = officerId
                    });

                    //push news to board
                    if (!string.IsNullOrEmpty(ids))
                    {
                        var content = new FormUrlEncodedContent(new
                        {
                            userid = officerId.ToString(),
                            boardid = boardId.ToString(),
                            listposts = ids
                        }.ConvertToKeyValuePair());
                        string query = await content?.ReadAsStringAsync();
                        await Function.AddToQueue(ActionName.PushBoardToChannel, TopicName.PUSHBOARDTOCHANNEL, new QueueData<string, string, string, PushInfo>
                        {
                            Data1 = query,
                            Data2 = BoardAction.AddMultiPost,
                            Data3 = clientIP,
                            Data4 = new PushInfo
                            {
                                ObjectId = boardId,
                                ModifiedBy = userId.ToString()
                            }
                        });
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
                returnValue = false;
            }
            return returnValue;
        }

        public static async Task<bool> DeleteNewsAsync(long officerId, long userId, long boardId, string newsIds, string clientIP)
        {
            var returnValue = false;
            try
            {
                returnValue = await StoreFactory.Create<BoardCacheStore>().DeleteNewsAsync(officerId, boardId, newsIds);
                if (returnValue)
                {
                    await Function.AddToQueue(ActionName.DeleteNewsInBoardSQL, TopicName.SQL_BOARD, new QueueData<long, long, string, object>
                    {
                        Data1 = userId,
                        Data2 = boardId,
                        Data3 = newsIds
                    });

                    //push news to board
                    if (!string.IsNullOrEmpty(newsIds))
                    {
                        var content = new FormUrlEncodedContent(new
                        {
                            userid = officerId.ToString(),
                            boardid = boardId.ToString(),
                            listposts = newsIds
                        }.ConvertToKeyValuePair());
                        string query = await content?.ReadAsStringAsync();
                        await Function.AddToQueue(ActionName.PushBoardToChannel, TopicName.PUSHBOARDTOCHANNEL, new QueueData<string, string, string, PushInfo>
                        {
                            Data1 = query,
                            Data2 = BoardAction.DeleteMultiPost,
                            Data3 = clientIP,
                            Data4 = new PushInfo
                            {
                                ObjectId = boardId,
                                ModifiedBy = userId.ToString()
                            }
                        });
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
                returnValue = false;
            }
            return returnValue;
        }

        public static Task<PagingDataResult<NewsAllSearch>> NewsInBoardAsync(long officerId, long boardId, int? pageIndex, int? pageSize)
        {
            return StoreFactory.Create<BoardCacheStore>().NewsInBoardAsync(officerId, boardId, pageIndex, pageSize);
        }
        public static Task<long[]> NewsInBoardAsync(long boardId)
        {
            return StoreFactory.Create<BoardCacheStore>().NewsInBoardAsync(boardId);
        }
        public static Task<BoardSearch> GetBoardSearchByIdAsync(long id)
        {
            return StoreFactory.Create<BoardCacheStore>().GetBoardSearchByIdAsync(id);
        }
        public static async Task<PagingDataResult<BoardSearch>> SearchAsync(SearchBoard data, long userId)
        {
            var returnValue = default(PagingDataResult<BoardSearch>);
            try
            {
                if (string.IsNullOrEmpty(data.Keyword) && (data.OrderBy == null || data.OrderBy == SearchNewsOrderBy.AscCreatedDate || data.OrderBy == SearchNewsOrderBy.DescCreatedDate))
                {
                    returnValue = await StoreFactory.Create<BoardCacheStore>().SearchAsync(data, userId, new Action<string>(async (tempKey) =>
                    {
                        await Function.AddToQueue(ActionName.SetExpire, TopicName.DELETE_INDEX_TEMP, tempKey);
                    }));
                }
                else
                {
                    var resultSearch = await StoreFactory.Create<BoardSearchStore>().SearchAsync(data, userId);
                    if (resultSearch?.Data?.Count > 0)
                    {
                        returnValue = new PagingDataResult<BoardSearch>();
                        returnValue.Total = resultSearch.Total;
                        returnValue.Data = await StoreFactory.Create<BoardCacheStore>().GetListByIdsAsync(resultSearch.Data.Select(p => p.EncryptId).ToList());
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
            }
            return returnValue;
        }

        public static Task<bool> CheckBoardOfUser(long officerId, long userId, long boardId)
        {
            return StoreFactory.Create<BoardCacheStore>().CheckBoardOfUser(officerId, userId, boardId);
        }

        public static Task<bool> CheckBoardOfUser(long officerId, long userId, long[] boardIds)
        {
            return StoreFactory.Create<BoardCacheStore>().CheckBoardOfUser(officerId, userId, boardIds);
        }

        public static async Task<bool> PushBoardToChannel(string query, string action, string clientIP, PushInfo pushInfo)
        {
            var returnValue = false;
            ActionType? actionType = default(ActionType);
            try
            {
                using (var client = new HttpClient())
                {
                    //string api = action;
                    switch (action?.ToLower())
                    {
                        case BoardAction.Add:
                            actionType = ActionType.PushBoardToApp;
                            break;
                        case BoardAction.Update:
                            actionType = ActionType.UpdateBoardInApp;
                            break;
                        case BoardAction.Delete:
                            actionType = ActionType.DeleteBoardInApp;
                            break;
                        case BoardAction.AddMultiPost:
                            actionType = ActionType.AddPostsToBoardOnApp;
                            break;
                        case BoardAction.DeleteMultiPost:
                            actionType = ActionType.DeletePostsInBoardOnApp;
                            break;
                        default:
                            break;
                    }
                    if (!string.IsNullOrEmpty(action))
                    {
                        client.Timeout = TimeSpan.FromSeconds(AppSettings.Current.ChannelConfiguration.HttpClientTimeOut);
                        string path = AppSettings.Current.ChannelConfiguration.KingHubBoardApiSetting.ApiUrl + "/" + action + "?" + query;
                        var response = await client.GetAsync(path);
                        if (!response.IsSuccessStatusCode)
                        {
                            response.EnsureSuccessStatusCode();
                        }
                        if (response?.StatusCode == System.Net.HttpStatusCode.OK)
                        {
                            var result = Json.Parse<KingHubBoardResponse<object>>(await response.Content?.ReadAsStringAsync());
                            if (result?.Status == 1)
                                returnValue = true;
                            else
                                Logger.Sensitive(actionType + ": action=" + action + "; query=" + query + ")", await response?.Content?.ReadAsStringAsync());
                        }
                        else
                        {
                            Logger.Sensitive(actionType +": action=" + action + "; query=" + query + ")", await response?.Content?.ReadAsStringAsync());
                        }
                    }
                    else
                    {
                        Logger.Sensitive(actionType + ": action=" + action + "; query=" + query + ")", "Action is null");
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
                Logger.Error(actionType +": action =>" +action+"; query =>"+query);
            }
            if (returnValue)
            {
                await Function.AddToQueue(ActionName.InsertLog, TopicName.LOGACTION, new LogActionEntity
                {
                    ObjectId = pushInfo.ObjectId.ToString(),
                    Account = pushInfo.ModifiedBy,
                    CreatedDate = DateTime.SpecifyKind(DateTime.Now, DateTimeKind.Unspecified),
                    Ip = clientIP,
                    Message = "Success.",
                    Title = "",
                    SourceId = pushInfo.ModifiedBy,
                    ActionTypeDetail = (int?)actionType ?? 0,
                    ActionText = actionType?.ToString(),
                    Type = (int)LogContentType.Board
                });
            }
            return returnValue;
        }

        public static async Task<bool> PushChangeNotifyAsync(long mediaId)
        {
            var returnValue = false;
            string path = AppSettings.Current.ChannelConfiguration.KingHubBoardApiSetting.ApiUrl + "/listenchange?mediaid=" + mediaId;
            try
            {
                using (var client = new HttpClient())
                {
                    client.Timeout = TimeSpan.FromSeconds(AppSettings.Current.ChannelConfiguration.HttpClientTimeOut);

                    var response = await client.GetAsync(path);
                    if (!response.IsSuccessStatusCode)
                    {
                        response.EnsureSuccessStatusCode();
                    }
                    if (response?.StatusCode == System.Net.HttpStatusCode.OK)
                    {
                        returnValue = true;
                    }
                    else
                    {
                        Logger.Sensitive("PushChangeNotify: mediaId=" + mediaId + ")", await response?.Content?.ReadAsStringAsync());
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Sensitive(ex, ex.Message);
                Logger.Sensitive("PushChangeNotify", new { mediaId = mediaId , url = path });
            }
            return returnValue;
        }
    }
}
