﻿using ChannelVN.IMS2.Core.Entities.Statistics;
using ChannelVN.IMS2.Core.Models.CacheStore.Security;
using ChannelVN.IMS2.Core.Models.CacheStore.Statistics;
using ChannelVN.IMS2.Core.Models.SearchStore.Statistics;
using ChannelVN.IMS2.Foundation.Common;
using ChannelVN.IMS2.Foundation.Data;
using ChannelVN.IMS2.Foundation.Data.Factories;
using ChannelVN.IMS2.Foundation.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ChannelVN.IMS2.Core.Repositories
{
    public class StatisticsRepository: Repository
    {
        public static Task<Dictionary<DateTime, NewsCount2[]>> PublishNewsCountAsync(GetNewsCount search)
        {
            return StoreFactory.Create<StatisticsCacheStore>().PublishNewsCountAsync(search);
        }

        public static Task<Dictionary<DateTime, NewsCount[]>> PublishNewsCountOfUserOnPageAsync(GetNewsCountOfUser search)
        {
            return StoreFactory.Create<StatisticsCacheStore>().PublishNewsCountOfUserOnPageAsync(search);
        }
        
        public static async Task<PagingDataResult<object>> ListPageAsync(SearchPage search)
        {
            var returnValue = new PagingDataResult<object>();
            try
            {
                var result = await StoreFactory.Create<StatisticsSearchStore>().ListPageAsync(search);

                returnValue.Total = result.Total;

                if (result != null && result.Data != null && result.Data.Count() > 0)
                {
                    var list = result.Data.Select(p => p.Id.ToString());
                    returnValue.Data = await StoreFactory.Create<AccountCacheStore>().GetListSimpleByIdsAsync(list.ToList());
                }
            }
            catch(Exception ex)
            {
                Logger.Error(ex, ex.Message);
            }
            return returnValue;
        }
    }
}
