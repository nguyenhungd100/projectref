﻿using ChannelVN.IMS2.Core.Entities;
using ChannelVN.IMS2.Core.Entities.Board;
using ChannelVN.IMS2.Core.Entities.Security;
using ChannelVN.IMS2.Core.Entities.Statistics;
using ChannelVN.IMS2.Core.Models.CacheStore;
using ChannelVN.IMS2.Core.Models.CacheStore.Album;
using ChannelVN.IMS2.Core.Models.CacheStore.Articles;
using ChannelVN.IMS2.Core.Models.CacheStore.Beams;
using ChannelVN.IMS2.Core.Models.CacheStore.Distribution;
using ChannelVN.IMS2.Core.Models.CacheStore.Gallery;
using ChannelVN.IMS2.Core.Models.CacheStore.Media;
using ChannelVN.IMS2.Core.Models.CacheStore.Photo;
using ChannelVN.IMS2.Core.Models.CacheStore.Posts;
using ChannelVN.IMS2.Core.Models.CacheStore.Security;
using ChannelVN.IMS2.Core.Models.CacheStore.ShareLinks;
using ChannelVN.IMS2.Core.Models.CacheStore.Videos;
using ChannelVN.IMS2.Core.Models.DataStore;
using ChannelVN.IMS2.Core.Models.DataStore.Boards;
using ChannelVN.IMS2.Core.Models.DataStore.Distribution;
using ChannelVN.IMS2.Core.Models.ExternalAPI;
using ChannelVN.IMS2.Core.Models.SearchStore;
using ChannelVN.IMS2.Core.Models.SearchStore.Security;
using ChannelVN.IMS2.Core.Repositories.Constants;
using ChannelVN.IMS2.Core.Repositories.Functions;
using ChannelVN.IMS2.Foundation.Common;
using ChannelVN.IMS2.Foundation.Common.Configuration;
using ChannelVN.IMS2.Foundation.Data;
using ChannelVN.IMS2.Foundation.Data.Factories;
using ChannelVN.IMS2.Foundation.Logging;
using StackExchange.Redis;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace ChannelVN.IMS2.Core.Repositories
{
    public class SystemRepository : Repository
    {
        public static async Task<bool> InitSQLDataFromRedis(string tableName)
        {
            var returnValue = false;
            try
            {
                switch (tableName?.ToLower())
                {
                    case "newsdistribution":
                        var listNewsDistribution = await StoreFactory.Create<NewsDistributionCacheStore>().GetAllNewsDistributionAsync();
                        var count = listNewsDistribution.Select(p => new { p.DistributorId, p.NewsId, p.ItemStreamId }).Distinct().ToList();
                        returnValue = await StoreFactory.Create<NewsDistributionDataStore>().InitDataAsync(listNewsDistribution);
                        break;
                    default:
                        break;
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
            }
            return returnValue;
        }
        public static async Task<bool> PushToAppAsync(string tableName, string ids, string clientIP)
        {
            string sessionId = "sessionid";
            var returnValue = false;
            Random rnd = new Random();
            try
            {
                switch (tableName?.ToLower())
                {
                    case "board":
                        //push board to channel
                        var list = new List<Board>();
                        if (string.IsNullOrEmpty(ids))
                        {
                            list = await StoreFactory.Create<BoardCacheStore>().GetAllAsync();
                        }
                        else
                        {
                            list = (await StoreFactory.Create<BoardCacheStore>().GetListByIdsAsync(ids.Split(",").ToList()))?.Select(p=>Mapper<Board>.Map(p, new Board()))?.ToList();
                        }
                       
                        if (list != null)
                        {
                            foreach (var board in list)
                            {
                                var content = new FormUrlEncodedContent(new
                                {
                                    userid = board.CreatedBy,
                                    boardname = board.Name,
                                    boardid = board.Id,
                                    avatar = Json.Stringify(new { url = board.Avatar, widh = BoardConstant.AvatarWidth, heigh = BoardConstant.AvatarHeight, content_type = (int)ContentType.Image }),
                                    cover = Json.Stringify(new { url = board.Cover, widh = BoardConstant.CoverWidth, heigh = BoardConstant.CoverHeight, content_type = (int)ContentType.Image }),
                                }.ConvertToKeyValuePair());
                                string query = await content?.ReadAsStringAsync();
                                await Function.AddToQueue(ActionName.PushBoardToChannel, TopicName.PUSHBOARDTOCHANNEL, new QueueData<string, string, string, PushInfo>
                                {
                                    Data1 = query,
                                    Data2 = "add",
                                    Data3 = clientIP,
                                    Data4 = new PushInfo
                                    {
                                        ObjectId = board.Id,
                                        ModifiedBy = board.CreatedBy
                                    }
                                });
                            }
                        }
                        break;
                    case "article":
                        //push board to channel
                        var listId = new RedisValue[] { };
                        if (!string.IsNullOrEmpty(ids))
                        {
                            listId = ids.Split(",").Select(p=>(RedisValue)p).ToArray();
                        }
                        else
                        {
                            listId = await StoreFactory.Create<ArticleCacheStore>().GetAllAsync();
                        }
                       
                        if (listId != null)
                        {
                            for(int i=0;i< listId.Count(); i++)
                            {
                                if(long.TryParse(listId[i],out long id))
                                {
                                    //QueueData<long, string, string, Media.Action>
                                    await Function.AddToQueue(ActionName.AutoShareArticle, TopicName.AUTOSHAREARTICLE, new QueueData<long, string, string, Media.Action>
                                    {
                                        Data1 = id,
                                        Data2 = clientIP,
                                        Data3 = sessionId,
                                        Data4 = Media.Action.update
                                    });
                                }
                            }
                        }
                        break;
                    case "sharelink":
                        //push sharelink to channel
                        var listLinkId = new RedisValue[] { };
                        if (!string.IsNullOrEmpty(ids))
                        {
                            listLinkId = ids.Split(",").Select(p => (RedisValue)p).ToArray();
                        }
                        else
                        {
                            listLinkId = await StoreFactory.Create<ShareLinkCacheStore>().GetAllAsync();
                        }

                        if (listLinkId != null)
                        {
                            for (int i = 0; i < listLinkId.Count(); i++)
                            {
                                if (long.TryParse(listLinkId[i], out long id))
                                {
                                    await Function.AddToQueue(ActionName.AutoShareShareLink, TopicName.AUTOSHARESHARELINK, new QueueData<long, string, string, Media.Action>
                                    {
                                        Data1 = id,
                                        Data2 = clientIP,
                                        Data3 = sessionId,
                                        Data4 = Media.Action.update
                                    });
                                }
                            }
                        }
                        break;
                    case "playlist":
                        //push playlist to channel
                        var listPlaylistId = new RedisValue[] { };
                        if (!string.IsNullOrEmpty(ids))
                        {
                            listPlaylistId = ids.Split(",").Select(p => (RedisValue)p).ToArray();
                        }
                        else
                        {
                            listPlaylistId = await StoreFactory.Create<VideoPlaylistCacheStore>().GetAllAsync();
                        }

                        if (listPlaylistId != null)
                        {
                            for (int i = 0; i < listPlaylistId.Count(); i++)
                            {
                                if (long.TryParse(listPlaylistId[i], out long id))
                                {
                                    await Function.AddToQueue(ActionName.AutoShareVideoPlaylist, TopicName.AUTOSHAREVIDEOPLAYLIST, new QueueData<long, string, string, Media.Action>
                                    {
                                        Data1 = id,
                                        Data2 = clientIP,
                                        Data3 = sessionId,
                                        Data4 = Media.Action.update
                                    });
                                }
                            }
                        }
                        break;
                    case "video":
                        //push board to channel
                        var listVideotId = new RedisValue[] { };
                        if (!string.IsNullOrEmpty(ids))
                        {
                            listVideotId = ids.Split(",").Select(p => (RedisValue)p).ToArray();
                        }
                        else
                        {
                            listVideotId = await StoreFactory.Create<VideoCacheStore>().GetAllAsync();
                        }

                        if (listVideotId != null)
                        {
                            for (int i = 0; i < listVideotId.Count(); i++)
                            {
                                if (long.TryParse(listVideotId[i], out long id))
                                {
                                    await Function.AddToQueue(ActionName.AutoShareVideo, TopicName.AUTOSHAREVIDEO, new QueueData<long, string, string, Media.Action>
                                    {
                                        Data1 = id,
                                        Data2 = clientIP,
                                        Data3 = sessionId,
                                        Data4 = Media.Action.update
                                    });
                                }
                            }
                        }
                        break;
                    case "beam":
                        //push board to channel
                        var listBeamId = new RedisValue[] { };
                        if (!string.IsNullOrEmpty(ids))
                        {
                            listBeamId = ids.Split(",").Select(p => (RedisValue)p).ToArray();
                        }
                        else
                        {
                            listBeamId = await StoreFactory.Create<BeamCacheStore>().GetAllAsync();
                        }

                        if (listBeamId != null)
                        {
                            for (int i = 0; i < listBeamId.Count(); i++)
                            {
                                if (long.TryParse(listBeamId[i], out long id))
                                {
                                    await Function.AddToQueue(ActionName.AutoShareBeam, TopicName.AUTOSHAREBEAM, new QueueData<long, string, string, Media.Action>
                                    {
                                        Data1 = id,
                                        Data2 = clientIP,
                                        Data3 = sessionId,
                                        Data4 = Media.Action.update
                                    });
                                }
                            }
                        }
                        break;
                    case "photo":
                        //push photo to channel
                        var listPhotoId = new RedisValue[] { };
                        if (!string.IsNullOrEmpty(ids))
                        {
                            listPhotoId = ids.Split(",").Select(p => (RedisValue)p).ToArray();
                        }
                        else
                        {
                            listPhotoId = await StoreFactory.Create<PhotoCacheStore>().GetAllAsync();
                        }

                        if (listPhotoId != null)
                        {
                            for (int i = 0; i < listPhotoId.Count(); i++)
                            {
                                if (long.TryParse(listPhotoId[i], out long id))
                                {
                                    await Function.AddToQueue(ActionName.AutoSharePhoto, TopicName.AUTOSHAREPHOTO, new QueueData<long, string, string, Media.Action>
                                    {
                                        Data1 = id,
                                        Data2 = clientIP,
                                        Data3 = sessionId,
                                        Data4 = Media.Action.update
                                    });
                                }
                            }
                        }
                        break;
                    case "mediaunit":
                        //push mediaunit to channel
                        var listMediaUnitId = new RedisValue[] { };
                        if (!string.IsNullOrEmpty(ids))
                        {
                            listMediaUnitId = ids.Split(",").Select(p => (RedisValue)p).ToArray();
                        }
                        else
                        {
                            listMediaUnitId = await StoreFactory.Create<MediaUnitCacheStore>().GetAllAsync();
                        }

                        if (listMediaUnitId != null)
                        {
                            for (int i = 0; i < listMediaUnitId.Count(); i++)
                            {
                                if (long.TryParse(listMediaUnitId[i], out long id))
                                {
                                    await Function.AddToQueue(ActionName.AutoShareMediaUnit, TopicName.AUTOSHAREMEDIAUNIT, new QueueData<long, string, string, Media.Action>
                                    {
                                        Data1 = id,
                                        Data2 = clientIP,
                                        Data3 = sessionId,
                                        Data4 = Media.Action.update
                                    });
                                }
                            }
                        }
                        break;
                    case "gallery":
                        //push mediaunit to channel
                        var listGalleryId = new RedisValue[] { };
                        if (!string.IsNullOrEmpty(ids))
                        {
                            listGalleryId = ids.Split(",").Select(p => (RedisValue)p).ToArray();
                        }
                        else
                        {
                            listGalleryId = await StoreFactory.Create<GalleryCacheStore>().GetAllAsync();
                        }

                        if (listGalleryId != null)
                        {
                            for (int i = 0; i < listGalleryId.Count(); i++)
                            {
                                if (long.TryParse(listGalleryId[i], out long id))
                                {
                                    await Function.AddToQueue(ActionName.AutoShareGallery, TopicName.AUTOSHAREGALLERY, new QueueData<long, string, string, Media.Action>
                                    {
                                        Data1 = id,
                                        Data2 = clientIP,
                                        Data3 = sessionId,
                                        Data4 = Media.Action.update
                                    });
                                }
                            }
                        }
                        break;
                    case "post":
                        //push mediaunit to channel
                        var listPostId = new RedisValue[] { };
                        if (!string.IsNullOrEmpty(ids))
                        {
                            listPostId = ids.Split(",").Select(p => (RedisValue)p).ToArray();
                        }
                        else
                        {
                            listPostId = await StoreFactory.Create<PostCacheStore>().GetAllAsync();
                        }

                        if (listPostId != null)
                        {
                            for (int i = 0; i < listPostId.Count(); i++)
                            {
                                if (long.TryParse(listPostId[i], out long id))
                                {
                                    await Function.AddToQueue(ActionName.AutoSharePost, TopicName.AUTOSHAREPOST, new QueueData<long, string, string, Media.Action>
                                    {
                                        Data1 = id,
                                        Data2 = clientIP,
                                        Data3 = sessionId,
                                        Data4 = Media.Action.update
                                    });
                                }
                            }
                        }
                        break;
                    case "album":
                        //push mediaunit to channel
                        var listAlbumId = new RedisValue[] { };
                        if (!string.IsNullOrEmpty(ids))
                        {
                            listAlbumId = ids.Split(",").Select(p => (RedisValue)p).ToArray();
                        }
                        else
                        {
                            listAlbumId = await StoreFactory.Create<AlbumCacheStore>().GetAllAsync2();
                        }

                        if (listAlbumId != null)
                        {
                            for (int i = 0; i < listAlbumId.Count(); i++)
                            {
                                if (long.TryParse(listAlbumId[i], out long id))
                                {
                                    await Function.AddToQueue(ActionName.AutoShareAlbum, TopicName.AUTOSHAREALBUM, new QueueData<long, string, string, Media.Action>
                                    {
                                        Data1 = id,
                                        Data2 = clientIP,
                                        Data3 = sessionId,
                                        Data4 = Media.Action.update
                                    });
                                }
                            }
                        }
                        break;
                    default:
                        break;
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
            }
            return returnValue;
        }

        public static async Task<bool> AddToAppAsync(string tableName, string ids, string clientIP)
        {
            string sessionId = "sessionid";
            var returnValue = false;
            Random rnd = new Random();
            try
            {
                switch (tableName?.ToLower())
                {
                    case "board":
                        //push board to channel
                        var list = new List<Board>();
                        if (string.IsNullOrEmpty(ids))
                        {
                            list = await StoreFactory.Create<BoardCacheStore>().GetAllAsync();
                        }
                        else
                        {
                            list = (await StoreFactory.Create<BoardCacheStore>().GetListByIdsAsync(ids.Split(",").ToList()))?.Select(p => Mapper<Board>.Map(p, new Board()))?.ToList();
                        }

                        if (list != null)
                        {
                            foreach (var board in list)
                            {
                                var content = new FormUrlEncodedContent(new
                                {
                                    userid = board.CreatedBy,
                                    boardname = board.Name,
                                    boardid = board.Id,
                                    avatar = Json.Stringify(new { url = board.Avatar, widh = BoardConstant.AvatarWidth, heigh = BoardConstant.AvatarHeight, content_type = (int)ContentType.Image }),
                                    cover = Json.Stringify(new { url = board.Cover, widh = BoardConstant.CoverWidth, heigh = BoardConstant.CoverHeight, content_type = (int)ContentType.Image }),
                                }.ConvertToKeyValuePair());
                                string query = await content?.ReadAsStringAsync();
                                await Function.AddToQueue(ActionName.PushBoardToChannel, TopicName.PUSHBOARDTOCHANNEL, new QueueData<string, string, string, PushInfo>
                                {
                                    Data1 = query,
                                    Data2 = "add",
                                    Data3 = clientIP,
                                    Data4 = new PushInfo
                                    {
                                        ObjectId = board.Id,
                                        ModifiedBy = board.CreatedBy
                                    }
                                });
                            }
                        }
                        break;
                    case "article":
                        //push board to channel
                        var listId = new RedisValue[] { };
                        if (!string.IsNullOrEmpty(ids))
                        {
                            listId = ids.Split(",").Select(p => (RedisValue)p).ToArray();
                        }
                        else
                        {
                            listId = await StoreFactory.Create<ArticleCacheStore>().GetAllAsync();
                        }

                        if (listId != null)
                        {
                            for (int i = 0; i < listId.Count(); i++)
                            {
                                if (long.TryParse(listId[i], out long id))
                                {
                                    //QueueData<long, string, string, Media.Action>
                                    await Function.AddToQueue(ActionName.AutoShareArticle, TopicName.AUTOSHAREARTICLE, new QueueData<long, string, string, Media.Action>
                                    {
                                        Data1 = id,
                                        Data2 = clientIP,
                                        Data3 = sessionId,
                                        Data4 = Media.Action.add
                                    });
                                }
                            }
                        }
                        break;
                    case "sharelink":
                        //push sharelink to channel
                        var listLinkId = new RedisValue[] { };
                        if (!string.IsNullOrEmpty(ids))
                        {
                            listLinkId = ids.Split(",").Select(p => (RedisValue)p).ToArray();
                        }
                        else
                        {
                            listLinkId = await StoreFactory.Create<ShareLinkCacheStore>().GetAllAsync();
                        }

                        if (listLinkId != null)
                        {
                            for (int i = 0; i < listLinkId.Count(); i++)
                            {
                                if (long.TryParse(listLinkId[i], out long id))
                                {
                                    await Function.AddToQueue(ActionName.AutoShareShareLink, TopicName.AUTOSHARESHARELINK, new QueueData<long, string, string, Media.Action>
                                    {
                                        Data1 = id,
                                        Data2 = clientIP,
                                        Data3 = sessionId,
                                        Data4 = Media.Action.add
                                    });
                                }
                            }
                        }
                        break;
                    case "playlist":
                        //push playlist to channel
                        var listPlaylistId = new RedisValue[] { };
                        if (!string.IsNullOrEmpty(ids))
                        {
                            listPlaylistId = ids.Split(",").Select(p => (RedisValue)p).ToArray();
                        }
                        else
                        {
                            listPlaylistId = await StoreFactory.Create<VideoPlaylistCacheStore>().GetAllAsync();
                        }

                        if (listPlaylistId != null)
                        {
                            for (int i = 0; i < listPlaylistId.Count(); i++)
                            {
                                if (long.TryParse(listPlaylistId[i], out long id))
                                {
                                    await Function.AddToQueue(ActionName.AutoShareVideoPlaylist, TopicName.AUTOSHAREVIDEOPLAYLIST, new QueueData<long, string, string, Media.Action>
                                    {
                                        Data1 = id,
                                        Data2 = clientIP,
                                        Data3 = sessionId,
                                        Data4 = Media.Action.add
                                    });
                                }
                            }
                        }
                        break;
                    case "video":
                        //push board to channel
                        var listVideotId = new RedisValue[] { };
                        if (!string.IsNullOrEmpty(ids))
                        {
                            listVideotId = ids.Split(",").Select(p => (RedisValue)p).ToArray();
                        }
                        else
                        {
                            listVideotId = await StoreFactory.Create<VideoCacheStore>().GetAllAsync();
                        }

                        if (listVideotId != null)
                        {
                            for (int i = 0; i < listVideotId.Count(); i++)
                            {
                                if (long.TryParse(listVideotId[i], out long id))
                                {
                                    await Function.AddToQueue(ActionName.AutoShareVideo, TopicName.AUTOSHAREVIDEO, new QueueData<long, string, string, Media.Action>
                                    {
                                        Data1 = id,
                                        Data2 = clientIP,
                                        Data3 = sessionId,
                                        Data4 = Media.Action.add
                                    });
                                }
                            }
                        }
                        break;
                    case "beam":
                        //push board to channel
                        var listBeamId = new RedisValue[] { };
                        if (!string.IsNullOrEmpty(ids))
                        {
                            listBeamId = ids.Split(",").Select(p => (RedisValue)p).ToArray();
                        }
                        else
                        {
                            listBeamId = await StoreFactory.Create<BeamCacheStore>().GetAllAsync();
                        }

                        if (listBeamId != null)
                        {
                            for (int i = 0; i < listBeamId.Count(); i++)
                            {
                                if (long.TryParse(listBeamId[i], out long id))
                                {
                                    await Function.AddToQueue(ActionName.AutoShareBeam, TopicName.AUTOSHAREBEAM, new QueueData<long, string, string, Media.Action>
                                    {
                                        Data1 = id,
                                        Data2 = clientIP,
                                        Data3 = sessionId,
                                        Data4 = Media.Action.add
                                    });
                                }
                            }
                        }
                        break;
                    case "photo":
                        //push photo to channel
                        var listPhotoId = new RedisValue[] { };
                        if (!string.IsNullOrEmpty(ids))
                        {
                            listPhotoId = ids.Split(",").Select(p => (RedisValue)p).ToArray();
                        }
                        else
                        {
                            listPhotoId = await StoreFactory.Create<PhotoCacheStore>().GetAllAsync();
                        }

                        if (listPhotoId != null)
                        {
                            for (int i = 0; i < listPhotoId.Count(); i++)
                            {
                                if (long.TryParse(listPhotoId[i], out long id))
                                {
                                    await Function.AddToQueue(ActionName.AutoSharePhoto, TopicName.AUTOSHAREPHOTO, new QueueData<long, string, string, Media.Action>
                                    {
                                        Data1 = id,
                                        Data2 = clientIP,
                                        Data3 = sessionId,
                                        Data4 = Media.Action.add
                                    });
                                }
                            }
                        }
                        break;
                    case "mediaunit":
                        //push mediaunit to channel
                        var listMediaUnitId = new RedisValue[] { };
                        if (!string.IsNullOrEmpty(ids))
                        {
                            listMediaUnitId = ids.Split(",").Select(p => (RedisValue)p).ToArray();
                        }
                        else
                        {
                            listMediaUnitId = await StoreFactory.Create<MediaUnitCacheStore>().GetAllAsync();
                        }

                        if (listMediaUnitId != null)
                        {
                            for (int i = 0; i < listMediaUnitId.Count(); i++)
                            {
                                if (long.TryParse(listMediaUnitId[i], out long id))
                                {
                                    await Function.AddToQueue(ActionName.AutoShareMediaUnit, TopicName.AUTOSHAREMEDIAUNIT, new QueueData<long, string, string, Media.Action>
                                    {
                                        Data1 = id,
                                        Data2 = clientIP,
                                        Data3 = sessionId,
                                        Data4 = Media.Action.add
                                    });
                                }
                            }
                        }
                        break;
                    case "gallery":
                        //push mediaunit to channel
                        var listGalleryId = new RedisValue[] { };
                        if (!string.IsNullOrEmpty(ids))
                        {
                            listGalleryId = ids.Split(",").Select(p => (RedisValue)p).ToArray();
                        }
                        else
                        {
                            listGalleryId = await StoreFactory.Create<GalleryCacheStore>().GetAllAsync();
                        }

                        if (listGalleryId != null)
                        {
                            for (int i = 0; i < listGalleryId.Count(); i++)
                            {
                                if (long.TryParse(listGalleryId[i], out long id))
                                {
                                    await Function.AddToQueue(ActionName.AutoShareGallery, TopicName.AUTOSHAREGALLERY, new QueueData<long, string, string, Media.Action>
                                    {
                                        Data1 = id,
                                        Data2 = clientIP,
                                        Data3 = sessionId,
                                        Data4 = Media.Action.add
                                    });
                                }
                            }
                        }
                        break;
                    case "post":
                        //push mediaunit to channel
                        var listPostId = new RedisValue[] { };
                        if (!string.IsNullOrEmpty(ids))
                        {
                            listPostId = ids.Split(",").Select(p => (RedisValue)p).ToArray();
                        }
                        else
                        {
                            listPostId = await StoreFactory.Create<PostCacheStore>().GetAllAsync();
                        }

                        if (listPostId != null)
                        {
                            for (int i = 0; i < listPostId.Count(); i++)
                            {
                                if (long.TryParse(listPostId[i], out long id))
                                {
                                    await Function.AddToQueue(ActionName.AutoSharePost, TopicName.AUTOSHAREPOST, new QueueData<long, string, string, Media.Action>
                                    {
                                        Data1 = id,
                                        Data2 = clientIP,
                                        Data3 = sessionId,
                                        Data4 = Media.Action.add
                                    });
                                }
                            }
                        }
                        break;
                    case "album":
                        //push mediaunit to channel
                        var listAlbumId = new RedisValue[] { };
                        if (!string.IsNullOrEmpty(ids))
                        {
                            listAlbumId = ids.Split(",").Select(p => (RedisValue)p).ToArray();
                        }
                        else
                        {
                            listAlbumId = await StoreFactory.Create<AlbumCacheStore>().GetAllAsync2();
                        }

                        if (listAlbumId != null)
                        {
                            for (int i = 0; i < listAlbumId.Count(); i++)
                            {
                                if (long.TryParse(listAlbumId[i], out long id))
                                {
                                    await Function.AddToQueue(ActionName.AutoShareAlbum, TopicName.AUTOSHAREALBUM, new QueueData<long, string, string, Media.Action>
                                    {
                                        Data1 = id,
                                        Data2 = clientIP,
                                        Data3 = sessionId,
                                        Data4 = Media.Action.add
                                    });
                                }
                            }
                        }
                        break;
                    default:
                        break;
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
            }
            return returnValue;
        }

        public static async Task<bool> PushDropHeartAsync(string tableName, string ids, string clientIP)
        {
            var returnValue = false;
            Random rnd = new Random();
            try
            {
                switch (tableName?.ToLower())
                {
                    case "album":
                        //push board to channel
                        var listAlbum = await StoreFactory.Create<AlbumCacheStore>().GetListAlbumSearchByIdsAsync(ids?.Split(",")?.ToList());
                        
                        if (listAlbum != null)
                        {
                            for (int i = 0; i < listAlbum.Count(); i++)
                            {
                                await Function.AddToQueue(ActionName.AutoShareAlbumOld, TopicName.AUTOSHAREALBUM, new QueueData<long, string, long[], object>
                                {
                                    Data1 = listAlbum[i].Id,
                                    Data2 = clientIP,
                                    Data3 = null,
                                    Data4 = rnd.Next(1, 3)
                                });
                            }
                        }
                        break;
                    default:
                        break;
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
            }
            return returnValue;
        }
        public static async Task<bool> ExecuteQueryAsync(string query)
        {
            return await StoreFactory.Create<SystemDataStore>().ExecuteQueryAsync(query);
        }

        public static async Task<RedisValue[]> ListQueueAsync(string index, long start, long stop)
        {
            return await StoreFactory.Create<SystemCacheStore>().ListQueueAsync(index, start, stop);
        }
        public static async Task<object> GetAsync(string query)
        {
            return await StoreFactory.Create<SystemDataStore>().GetAsync(query);
        }
        public static async Task<bool> UpdateOwnerPageAsync(string query)
        {
            var returnValue = false;
            try
            {
                var listAccount = await StoreFactory.Create<SystemDataStore>().GetListAsync<Account>(query);
                for(var i = 0; i< listAccount.Count(); i++)
                {
                    try
                    {
                        await Function.AddToQueue(ActionName.ChangePageOwner, TopicName.SYNC_USERINFO, new QueueData<long, long, long, object>
                        {
                            Data1 = listAccount[i].Id,
                            Data2 = 0,
                            Data3 = long.Parse(listAccount[i].CreatedBy)
                        });
                    }
                    catch(Exception ex)
                    {
                        Logger.Error(ex, ex.Message);
                    }
                }
                returnValue = true;
            }
            catch(Exception ex)
            {
                Logger.Error(ex, ex.Message);
                returnValue = false;
            }
            return returnValue;
        }

        public static async Task<bool> CheckUserInfoOnAppAsync(long id, string avatar, string mobile)
        {
            try
            {
                using (var client = new HttpClient())
                {
                    client.Timeout = TimeSpan.FromSeconds(AppSettings.Current.ChannelConfiguration.HttpClientTimeOut);
                    string path = AppSettings.Current.ChannelConfiguration.LoginSetting.ApiUrl + id;
                    var response = await client.GetAsync(path);
                    if (!response.IsSuccessStatusCode)
                    {
                        response.EnsureSuccessStatusCode();
                    }
                    if (response?.StatusCode == System.Net.HttpStatusCode.OK)
                    {
                        var content = await response.Content.ReadAsStringAsync();
                        var userData = Json.Parse<LoginResponse>(content);
                        if (userData != null && userData.Status == 1 && userData.Data != null && !string.IsNullOrEmpty(userData.Data.User_id))
                        {
                            if (avatar != null && avatar.Equals(userData.Data.Avatar) && userData.Data.User_id.ToLower().Equals(id.ToString()))
                            {

                            }
                            else
                            {
                                if((!string.IsNullOrEmpty(userData.Data.Avatar) || !string.IsNullOrEmpty(avatar)) && userData.Data.User_id.ToLower().Equals(id.ToString()))
                                {
                                    Logger.Fatal(Json.Stringify(new
                                    {
                                        Id = id,
                                        Mobile = mobile,
                                        FullName = userData.Data.Full_name,
                                        AvatarCms = avatar,
                                        AvatarKingHub = userData.Data.Avatar
                                    }));

                                    //userAccount.FullName = string.IsNullOrEmpty(userData.Data.Full_name) ? userAccount.FullName : userData.Data.Full_name;
                                    //userAccount.Mobile = string.IsNullOrEmpty(userData.Data.Phone) ? userAccount.Mobile : userData.Data.Phone;
                                    //Logger.Fatal("{id_cms:" + id + ",mobile:\""++"\"avatar_cms:\"" + avatar + "\",id_kinghub:" + userData.Data.User_id + ",avatar_kinghub:\"" + userData.Data.Avatar + "\"}");
                                }
                            }
                            //userAccount.Avatar = string.IsNullOrEmpty(userData.Data.Avatar) ? userAccount.Avatar : userData.Data.Avatar;
                        }
                    }
                }
            }
            catch(Exception ex)
            {
                Logger.Sensitive(ex, ex.Message);
            }
            return true;
        }

        public static Task<Dictionary<string, Dictionary<int, int>>> CountNewsOrderAsync(string ids)
        {
            try
            {
                return StoreFactory.Create<NewsOnHomeSearchStore>().CountNewsOrderAsync(ids);
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
                return null;
            }
        }

        public static async Task<PagingDataResult<object>> ListPageManagerAsync(SearchPage search)
        {
            var returnValue = new PagingDataResult<object>();
            try
            {
                var result = await StoreFactory.Create<AccountSearchStore>().ListPageManagerAsync(search);

                returnValue.Total = result.Total;

                if (result != null && result.Data != null && result.Data.Count() > 0)
                {
                    var list = result.Data.Select(p => p.Id.ToString());
                    returnValue.Data = await StoreFactory.Create<AccountCacheStore>().GetListSimpleByIdsAsync(list.ToList());
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
            }
            return returnValue;
        }
    }
}
