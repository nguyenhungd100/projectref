﻿using ChannelVN.IMS2.Core.Entities;
using ChannelVN.IMS2.Core.Models.CacheStore;
using ChannelVN.IMS2.Core.Models.SearchStore;
using ChannelVN.IMS2.Core.Repositories.Constants;
using ChannelVN.IMS2.Core.Repositories.Functions;
using ChannelVN.IMS2.Foundation.Common;
using ChannelVN.IMS2.Foundation.Data;
using ChannelVN.IMS2.Foundation.Data.Factories;
using ChannelVN.IMS2.Foundation.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ChannelVN.IMS2.Core.Repositories
{
    public class TagRepository : Repository
    {
        public static async Task<bool> AddAsync(Tag data)
        {
            var returnValue = false;
            try
            {
                returnValue = await StoreFactory.Create<TagCacheStore>().AddAsync(data);
                if (returnValue)
                {
                    await Function.AddToQueue(ActionName.AddTagES, TopicName.ES_TAG, data);
                    await Function.AddToQueue(ActionName.AddTagSQL, TopicName.SQL_TAG, data);
                }
                return returnValue;
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
                return returnValue;
            }

        }

        public static Task<Tag> GetByNameAsync(string name)
        {
            return StoreFactory.Create<TagSearchStore>().GetByNameAsync(name);
        }

        public static async Task<bool> UpdateAsync(Tag data)
        {
            try
            {
                var returnValue = await StoreFactory.Create<TagCacheStore>().UpdateAsync(data);
                if (returnValue)
                {
                    await Function.AddToQueue(ActionName.UpdateTagES, TopicName.ES_TAG, data);
                    await Function.AddToQueue(ActionName.UpdateTagSQL, TopicName.SQL_TAG, data);
                }
                return returnValue;
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
                return false;
            }
        }

        public static Task<Tag> GetByIdAsync(long id)
        {
            return StoreFactory.Create<TagCacheStore>().GetByIdAsync(id);
        }

        public static async Task<PagingDataResult<Tag>> SearchAsync(string keyword, int? status, int pageIndex, int pageSize)
        {
            var dataReturn = new PagingDataResult<Tag>();
            try
            {
                var dataSearch = await StoreFactory.Create<TagSearchStore>().SearchAsync(keyword, status, pageIndex, pageSize);
                if (dataSearch?.Data?.Count() > 0)
                {
                    var listData = await GetListByIdsAsync(dataSearch.Data.Select(p => p.Id.ToString()).ToList());
                    if (listData != null)
                    {
                        dataReturn.Total = dataSearch.Total;
                        dataReturn.Data = listData;
                    }
                }
                return await StoreFactory.Create<TagSearchStore>().SearchAsync(keyword, status, pageIndex, pageSize);
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
                return null;
            }
        }

        public static Task<List<Tag>> GetListByIdsAsync(List<string> ids)
        {
            return StoreFactory.Create<TagCacheStore>().GetListByIdsAsync(ids);
        }
    }
}
