﻿using ChannelVN.IMS2.Core.Entities;
using ChannelVN.IMS2.Core.Entities.Media;
using ChannelVN.IMS2.Core.Entities.Security;
using ChannelVN.IMS2.Core.Models.CacheStore;
using ChannelVN.IMS2.Core.Models.ExternalAPI;
using ChannelVN.IMS2.Core.Models.SearchStore;
using ChannelVN.IMS2.Core.Repositories.Constants;
using ChannelVN.IMS2.Core.Repositories.Functions;
using ChannelVN.IMS2.Foundation.Common;
using ChannelVN.IMS2.Foundation.Data;
using ChannelVN.IMS2.Foundation.Data.Factories;
using ChannelVN.IMS2.Foundation.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ChannelVN.IMS2.Core.Repositories
{
    public class NewsRepository : Repository
    {
        public static async Task<PagingDataResult<NewsAllSearch>> SearchAsync(SearchNews data, long accountId, bool isDistribution)
        {
            try
            {
                var dataReturn = new PagingDataResult<NewsAllSearch>();
                if (string.IsNullOrEmpty(data.Keyword) && data.OrderBy != SearchNewsOrderBy.DescDistributionDate && data.OrderBy != SearchNewsOrderBy.AscDistributionDate && data.OrderBy != SearchNewsOrderBy.DescModifiedDate && data.OrderBy != SearchNewsOrderBy.AscModifiedDate)
                {
                    dataReturn = await StoreFactory.Create<NewsCacheStore>().SearchAsync(data, accountId, new Action<string>(async (tempKey) =>
                     {
                         await Function.AddToQueue(ActionName.SetExpire, TopicName.DELETE_INDEX_TEMP, tempKey);
                     }), isDistribution);
                }
                else
                {
                    dataReturn = await StoreFactory.Create<NewsSearchStore>().SearchAsync(data, accountId, isDistribution);
                    if (dataReturn?.Data?.Count() > 0)
                    {
                        dataReturn.Data = await StoreFactory.Create<NewsCacheStore>().GetListByKeysAndIdsAsync(dataReturn.Data);
                    }
                }
                return dataReturn;
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
                return null;
            }
        }
        public static async Task<PagingDataResult<NewsAllSearch>> NewsOnPageAsync(SearchNews data, long accountId)
        {
            try
            {
                var dataReturn = new PagingDataResult<NewsAllSearch>();
                if (string.IsNullOrEmpty(data.Keyword) && data.OrderBy != SearchNewsOrderBy.DescDistributionDate && data.OrderBy != SearchNewsOrderBy.AscDistributionDate && data.OrderBy != SearchNewsOrderBy.DescModifiedDate && data.OrderBy != SearchNewsOrderBy.AscModifiedDate)
                {
                    dataReturn = await StoreFactory.Create<NewsCacheStore>().NewsOnPageAsync(data, accountId, new Action<string>(async (tempKey) =>
                    {
                        await Function.AddToQueue(ActionName.SetExpire, TopicName.DELETE_INDEX_TEMP, tempKey);
                    }));
                }
                else
                {
                    dataReturn = await StoreFactory.Create<NewsSearchStore>().NewsOnPageAsync(data, accountId);
                    if (dataReturn?.Data?.Count() > 0)
                    {
                        dataReturn.Data = await StoreFactory.Create<NewsCacheStore>().GetListByKeysAndIdsAsync(dataReturn.Data);
                    }
                }
                return dataReturn;
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
                return null;
            }
        }

        public static Task<List<NewsAllSearch>> GetListNewsAsync(List<string> ids)
        {
            return StoreFactory.Create<NewsCacheStore>().GetListNewsAllSearchAsync(ids);
        }

        public static async Task<bool> ChangeDistributionDateAsync(long newsId, DateTime distributionDate, long accountId, string clientIP, string sessionId="")
        {
            var cartType = default(int?);
            var logType = 0;
            var returnValue = await StoreFactory.Create<NewsCacheStore>().ChangeDistributionDateAsync(newsId, distributionDate, accountId, new Action<int?>((data)=>
            {
                cartType = data;
            }));

            if (returnValue)
            {
                if(cartType != null)
                {
                    switch (cartType)
                    {
                        case (int)CardType.Album:
                            await Function.AddToQueue(ActionName.UpdateAlbumES, TopicName.ES_ALBUM, newsId);
                            await Function.AddToQueue(ActionName.UpdateAlbumSQL, TopicName.SQL_ALBUM, newsId);

                            await Function.AddToQueue(ActionName.AutoShareAlbum, TopicName.AUTOSHAREALBUM, new QueueData<long, string, string, Media.Action>
                            {
                                Data1 = newsId,
                                Data2 = clientIP,
                                Data3 = sessionId,
                                Data4 = Media.Action.update
                            });
                            logType = (int)LogContentType.Album;
                            break;
                        case (int)CardType.Article:
                            await Function.AddToQueue(ActionName.UpdateArticleES, TopicName.ES_ARTICLE, newsId);
                            await Function.AddToQueue(ActionName.UpdateArticleSQL, TopicName.SQL_ARTICLE, newsId);

                            await Function.AddToQueue(ActionName.AutoShareArticle, TopicName.AUTOSHAREARTICLE, new QueueData<long, string, string, Media.Action>
                            {
                                Data1 = newsId,
                                Data2 = clientIP,
                                Data3 = sessionId,
                                Data4 = Media.Action.update
                            });
                            logType = (int)LogContentType.Article;
                            break;
                        case (int)CardType.ListNews:
                            await Function.AddToQueue(ActionName.UpdateBeamES, TopicName.ES_BEAM, newsId);
                            await Function.AddToQueue(ActionName.UpdateBeamSQL, TopicName.SQL_BEAM, newsId);

                            await Function.AddToQueue(ActionName.AutoShareBeam, TopicName.AUTOSHAREBEAM, new QueueData<long, string, string, Media.Action>
                            {
                                Data1 = newsId,
                                Data2 = clientIP,
                                Data3 = sessionId,
                                Data4 = Media.Action.update
                            });
                            logType = (int)LogContentType.Beam;
                            break;
                        case (int)CardType.VideoAndImage:
                            await Function.AddToQueue(ActionName.UpdateMediaUnitES, TopicName.ES_MEDIA_UNIT, newsId);
                            await Function.AddToQueue(ActionName.UpdateMediaSQL, TopicName.SQL_MEDIA_UNIT, newsId);

                            await Function.AddToQueue(ActionName.AutoShareMediaUnit, TopicName.AUTOSHAREMEDIAUNIT, new QueueData<long, string, string, Media.Action>
                            {
                                Data1 = newsId,
                                Data2 = clientIP,
                                Data3 = sessionId,
                                Data4 = Media.Action.update
                            });
                            logType = (int)LogContentType.MediaUnit;
                            break;
                        case (int)CardType.Photo:
                            await Function.AddToQueue(ActionName.UpdatePhotoES, TopicName.ES_PHOTO, newsId);
                            await Function.AddToQueue(ActionName.UpdatePhotoSQL, TopicName.SQL_PHOTO, newsId);

                            await Function.AddToQueue(ActionName.AutoSharePhoto, TopicName.AUTOSHAREPHOTO, new QueueData<long, string, string, Media.Action>
                            {
                                Data1 = newsId,
                                Data2 = clientIP,
                                Data3 = sessionId,
                                Data4 = Media.Action.update
                            });
                            logType = (int)LogContentType.Photo;
                            break;
                        case (int)CardType.Text:
                            await Function.AddToQueue(ActionName.UpdatePostES, TopicName.ES_POST, newsId);
                            await Function.AddToQueue(ActionName.UpdatePostSQL, TopicName.SQL_POST, newsId);

                            await Function.AddToQueue(ActionName.AutoSharePost, TopicName.AUTOSHAREPOST, new QueueData<long, string, string, Media.Action>
                            {
                                Data1 = newsId,
                                Data2 = clientIP,
                                Data3 = sessionId,
                                Data4 = Media.Action.update
                            });
                            logType = (int)LogContentType.Post;
                            break;
                        case (int)CardType.ShareNews:
                            await Function.AddToQueue(ActionName.UpdateShareLinkES, TopicName.ES_SHARE_LINK, newsId);
                            await Function.AddToQueue(ActionName.UpdateShareLinkSQL, TopicName.SQL_SHARE_LINK, newsId);

                            await Function.AddToQueue(ActionName.AutoShareShareLink, TopicName.AUTOSHARESHARELINK, new QueueData<long, string, string, Media.Action>
                            {
                                Data1 = newsId,
                                Data2 = clientIP,
                                Data3 = sessionId,
                                Data4 = Media.Action.update
                            });
                            logType = (int)LogContentType.ShareLink;
                            break;
                        case (int)CardType.PlaylistVideo:
                            await Function.AddToQueue(ActionName.UpdateVideoPlaylistES, TopicName.ES_PLAYLIST, newsId);
                            await Function.AddToQueue(ActionName.UpdateVideoPlaylistSQL, TopicName.SQL_PLAYLIST, newsId);

                            await Function.AddToQueue(ActionName.AutoShareVideoPlaylist, TopicName.AUTOSHAREVIDEOPLAYLIST, new QueueData<long, string, string, Media.Action>
                            {
                                Data1 = newsId,
                                Data2 = clientIP,
                                Data3 = sessionId,
                                Data4 = Media.Action.update
                            });
                            logType = (int)LogContentType.PlayList;
                            break;
                        case (int)CardType.Video:
                            await Function.AddToQueue(ActionName.UpdateVideoES, TopicName.ES_VIDEO, newsId);
                            await Function.AddToQueue(ActionName.UpdateVideoSQL, TopicName.SQL_VIDEO, newsId);

                            await Function.AddToQueue(ActionName.AutoShareVideo, TopicName.AUTOSHAREVIDEO, new QueueData<long, string, string, Media.Action>
                            {
                                Data1 = newsId,
                                Data2 = clientIP,
                                Data3 = sessionId,
                                Data4 = Media.Action.update
                            });
                            logType = (int)LogContentType.Video;
                            break;
                        case (int)CardType.Gallery1:
                        case (int)CardType.Gallery2:
                            await Function.AddToQueue(ActionName.UpdateGalleryES, TopicName.ES_GALLERY, newsId);
                            await Function.AddToQueue(ActionName.UpdateGallerySQL, TopicName.SQL_GALLERY, newsId);

                            await Function.AddToQueue(ActionName.AutoShareGallery, TopicName.AUTOSHAREGALLERY, new QueueData<long, string, string, Media.Action>
                            {
                                Data1 = newsId,
                                Data2 = clientIP,
                                Data3 = sessionId,
                                Data4 = Media.Action.update
                            });
                            logType = (int)LogContentType.Gallery;
                            break;                        
                    }


                    //insert log
                    await Function.AddToQueue(ActionName.InsertLog, TopicName.LOGACTION, new LogActionEntity
                    {
                        ObjectId = newsId.ToString(),
                        Account = accountId.ToString(),
                        CreatedDate = DateTime.SpecifyKind(DateTime.Now, DateTimeKind.Unspecified),
                        Ip = clientIP,
                        Message = "Success.",
                        Title = "",
                        SourceId = accountId.ToString(),
                        ActionTypeDetail = (int)ActionType.ChangeDistributionDate,
                        ActionText = ActionType.UpdateStatus.ToString(),
                        Type = logType
                    });
                }
            }
            return returnValue;
        }

        public static Task<Dictionary<string, int?>> GetNewsOnHomeStatusAsync(IEnumerable<long> listId)
        {
            try
            {
                return StoreFactory.Create<NewsOnHomeSearchStore>().GetNewsOnHomeStatusAsync(listId);
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
                return null;
            }
        }
    }
}
