﻿using ChannelVN.IMS2.Core.Entities.Media;
using ChannelVN.IMS2.Core.Models.CacheStore.Media;
using ChannelVN.IMS2.Core.Models.DataStore.Media;
using ChannelVN.IMS2.Core.Models.SearchStore.Media;
using ChannelVN.IMS2.Core.Repositories.Constants;
using ChannelVN.IMS2.Core.Repositories.Functions;
using ChannelVN.IMS2.Foundation.Common;
using ChannelVN.IMS2.Foundation.Data;
using ChannelVN.IMS2.Foundation.Data.Factories;
using ChannelVN.IMS2.Foundation.Logging;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ChannelVN.IMS2.Core.Repositories
{
    public class TemplateRepository : Repository
    {
        public static async Task<bool> AddAsync(Template data)
        {
            try
            {
                var result = await Task.Run(async () =>
                {
                    return await StoreFactory.Create<TemplateDataStore>().AddAsync(data);

                }).ContinueWith(async returnValue =>
                {
                    if (returnValue.Result.ReturnValue)
                    {
                        data.Id = Utility.ConvertToInt(returnValue.Result.Data);
                        var res = await StoreFactory.Create<TemplateSearchStore>().AddAsync(data);
                        if (!res)
                        {
                            //push queue
                            await Function.AddToQueue(ActionName.AddTemplateES, TopicName.ES_TEMPLATE, data);
                        }

                        res = await StoreFactory.Create<TemplateCacheStore>().AddAsync(data);
                        if (!res)
                        {
                            //push queue
                            await Function.AddToQueue(ActionName.AddTemplateRedis, TopicName.REDIS_TEMPLATE, data);
                        }
                    }
                    return returnValue;
                });

                return result.Result.Result.ReturnValue;
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
                return false;
            }
        }

        public static async Task<bool> UpdateAsync(Template data)
        {
            try
            {
                var result = await Task.Run(async () =>
                {
                    return await StoreFactory.Create<TemplateDataStore>().UpdateAsync(data);
                }).ContinueWith(async returnValue =>
                {
                    if (returnValue.Result)
                    {
                        var res = await StoreFactory.Create<TemplateSearchStore>().UpdateAsync(data);
                        if (!res)
                        {
                            //push queue
                            await Function.AddToQueue(ActionName.UpdateTemplateES, TopicName.ES_TEMPLATE, data);
                        }

                        res = await StoreFactory.Create<TemplateCacheStore>().UpdateAsync(data);
                        if (!res)
                        {
                            //push queue
                            await Function.AddToQueue(ActionName.UpdateTemplateRedis, TopicName.REDIS_TEMPLATE, data);
                        }
                    }
                    return returnValue;
                });

                return await result.Result;
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
                return false;
            }
        }

        public static async Task<bool> UpdateStatusAsync(Template data)
        {
            try
            {
                var result = await Task.Run(async () =>
                {
                    return await StoreFactory.Create<TemplateCacheStore>().UpdateStatusAsync(data);
                }).ContinueWith(async returnValue =>
                {
                    if (returnValue.Result)
                    {
                        var res = await StoreFactory.Create<TemplateSearchStore>().UpdateStatusAsync(data);
                        if (!res)
                        {
                            await Function.AddToQueue(ActionName.UpdateStatusTemplateES, TopicName.ES_TEMPLATE, data);
                        }

                        res = await StoreFactory.Create<TemplateDataStore>().UpdateStatusAsync(data);
                        if (!res)
                        {
                            await Function.AddToQueue(ActionName.UpdateStatusTemplateSQL, TopicName.SQL_TEMPLATE, data);
                        }
                    }
                    return returnValue;
                });

                return await result.Result;

            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
                return false;
            }
        }

        public static Task<Template> GetByIdAsync(long id)
        {
            return StoreFactory.Create<TemplateCacheStore>().GetByIdAsync(id);
        }

        public static Task<List<Template>> GetListByIdsAsync(List<string> ids)
        {
            return StoreFactory.Create<TemplateCacheStore>().GetListByIdsAsync(ids);
        }

        public static Task<PagingDataResult<Template>> SearchAsync(string keyword, int CategoryId, int? status, int pageIndex, int pageSize)
        {
            try
            {
                if (string.IsNullOrEmpty(keyword))
                {
                    return StoreFactory.Create<TemplateCacheStore>().SearchAsync(CategoryId, status, pageIndex, pageSize);
                }
                else
                {
                    return StoreFactory.Create<TemplateSearchStore>().SearchAsync(keyword, CategoryId, status, pageIndex, pageSize);
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
                return Task.FromResult<PagingDataResult<Template>>(null);
            }
        }
    }
}
