﻿using ChannelVN.IMS2.Core.Entities.Security;
using ChannelVN.IMS2.Core.Repositories.Constants;
using ChannelVN.IMS2.Core.Repositories.Functions;
using ChannelVN.IMS2.Foundation.Common.Configuration;
using ChannelVN.IMS2.Foundation.Data;
using ChannelVN.IMS2.Foundation.Logging;
using ChannelVN.IMS2.Foundation.Toolkit.Messaging;
using ChannelVN.IMS2.Foundation.Toolkit.Messaging.ExchangeQueue;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace ChannelVN.IMS2.Core.Repositories
{
    public class KingHubRepository : Repository
    {
        private static string APP_KEY = "OVP-APP-KEY";
        private static string SECRET_KEY = "OVP-SECRET-KEY";
        public static async Task AddToQueue(Account account)
        {
            await Function.AddToQueue(ActionName.SetRoleKingHubAccount, TopicName.ROLEKINGHUB, account);
            //var queue = new Entities.Queue<Account>
            //{
            //    Action = ActionName.SetRoleKingHubAccount,// "add_es_account",
            //    Data = account,
            //    CreatedDate = DateTime.Now
            //};
            //ExchangeFactory.Send(new EQMessage<object>
            //{
            //    Headers = new EQMessageHeaders { Topic = TopicName.ROLEKINGHUB },
            //    Body = queue
            //}, (err, effected) =>
            //{
            //    //TODO somethings
            //    if (err != null)
            //    {
            //        Logger.Error("queue:" + TopicName.ROLEKINGHUB + err.Message);
            //    }
            //});
        }

        public static async Task<bool> SetUserRolesAsync(Account account, bool isQueue = false)
        {
            var _returnValue = false;
            //ConcurrentDictionary<long, Account> _accounts = new ConcurrentDictionary<long, Account>();
            try
            {
                var role = await MapRoleAsync(account.Type, account.Status, account.IsSystem, account.IsFullPermission);
                if (role == null)
                    return _returnValue;

                string accessToken = await GetAccessTokenAsync(account.Id, account.UserName);
                if (string.IsNullOrEmpty(accessToken))
                {

                    await AddToQueue(account);
                    //_accounts.AddOrUpdate(account.Id, account, (k, v) => account);
                    return _returnValue;
                }

                using (var httpClient = new HttpClient())
                {
                    //httpClient.DefaultRequestHeaders.CacheControl.NoCache = true;
                    httpClient.DefaultRequestHeaders.Add(APP_KEY, AppSettings.Current.ChannelConfiguration.KingHubVideoSettings.Appkey);
                    httpClient.DefaultRequestHeaders.Add(SECRET_KEY, AppSettings.Current.ChannelConfiguration.KingHubVideoSettings.Secretkey);

                    var postModel = new
                    {
                        token = accessToken,
                        roles = new[] { role.id.ToString() }
                    };

                    var content = new StringContent(JsonConvert.SerializeObject(postModel), Encoding.UTF8, "application/json");
                    var response = await httpClient.PostAsync(AppSettings.Current.ChannelConfiguration.KingHubVideoSettings.SetUserRoleUrl, content);
                    string jsonResponse = await response.Content.ReadAsStringAsync();

                    JObject obj = JObject.Parse(jsonResponse);
                    if (obj["success"].ToString() == "True")
                    {
                        //Thử remove khỏi dic nếu update thành công
                        //_accounts.TryRemove(account.Id, out var acc);
                        _returnValue = true;
                    }
                    //else
                    //{
                    //    await AddToQueue(account);
                    //}
                }
            }
            catch (Exception ex)
            {
                if(!isQueue) Logger.Error(ex,ex.Message);
            }
            //if (_returnValue) return _returnValue;
            //_accounts.AddOrUpdate(account.Id, account, (k, v) => account);

            return _returnValue;
        }

        private static async Task<KingHubRole> MapRoleAsync(byte? accountType, int? status, bool? isSystem, bool? isFullPermission)
        {
            List<KingHubRole> _roles = await GetListRolesAsync();

            if (isSystem.HasValue && isSystem == true
                && isFullPermission.HasValue && isFullPermission == true)
                return _roles.FirstOrDefault(x => x.type == "administrator");

            string roleType = string.Empty;

            if (status.Value == 0 || status.Value == 2)
            {
                roleType = "collaborator";
            }
            else
            {
                if (accountType != null)
                {
                    switch (accountType.Value)
                    {
                        case 0:
                            roleType = "collaborator";
                            break;
                        case 1:
                            roleType = "editor";
                            break;
                        case 2:
                            roleType = "manager";
                            break;

                        default:
                            return null;
                    }
                }
            }
            return _roles.FirstOrDefault(x => x.type == roleType);
        }

        private static async Task<List<KingHubRole>> GetListRolesAsync()
        {
            var _returnValue = default(List<KingHubRole>);
            try
            {
                //lock(_lockUpdate)
                //{

                using (var httpClient = new HttpClient())
                {
                    //httpClient.DefaultRequestHeaders.CacheControl.NoCache = true;
                    httpClient.DefaultRequestHeaders.Add(APP_KEY, AppSettings.Current.ChannelConfiguration.KingHubVideoSettings.Appkey);
                    httpClient.DefaultRequestHeaders.Add(SECRET_KEY, AppSettings.Current.ChannelConfiguration.KingHubVideoSettings.Secretkey);

                    var response = await httpClient.GetAsync(AppSettings.Current.ChannelConfiguration.KingHubVideoSettings.GetListRolesUrl);
                    string jsonResponse = await response.Content.ReadAsStringAsync();

                    JObject obj = JObject.Parse(jsonResponse);
                    if (obj["success"].ToString() == "True")
                    {
                        var dataObj = obj["data"];
                        _returnValue = JsonConvert.DeserializeObject<List<KingHubRole>>(dataObj.ToString());
                    }
                }
                // }
            }
            catch (Exception ex)
            {
                Logger.Error(ex,ex.Message);
            }

            return _returnValue;
        }

        public static async Task<string> GetAccessTokenAsync(long userId, string userName)
        {
            var _token = string.Empty;
            using (var httpClient = new HttpClient())
            {
                //httpClient.DefaultRequestHeaders.CacheControl.NoCache = true;
                httpClient.DefaultRequestHeaders.Add(APP_KEY, AppSettings.Current.ChannelConfiguration.KingHubVideoSettings.Appkey);
                httpClient.DefaultRequestHeaders.Add(SECRET_KEY, AppSettings.Current.ChannelConfiguration.KingHubVideoSettings.Secretkey);

                var pars = new List<KeyValuePair<string, string>>(); ;
                pars.Add(new KeyValuePair<string, string>("userId", userId.ToString()));
                pars.Add(new KeyValuePair<string, string>("name", userName));

                var formData = new FormUrlEncodedContent(pars);
                var response = await httpClient.PostAsync(AppSettings.Current.ChannelConfiguration.KingHubVideoSettings.PostAccessTokenUrl, formData);
                string jsonResponse = await response.Content.ReadAsStringAsync();

                JObject obj = JObject.Parse(jsonResponse);
                if (obj["success"].ToString() == "True")
                {
                    var dataObj = obj["data"];
                    _token = dataObj["token"].ToString();
                }
            }

            return _token;
        }
    }

    public class KingHubRole
    {
        public int id { get; set; }
        public string type { get; set; }
        public string name { get; set; }
        public string description { get; set; }
        public string status { get; set; }
    }
}
