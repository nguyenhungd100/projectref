﻿using ChannelVN.IMS2.Core.Entities;
using ChannelVN.IMS2.Core.Entities.AccountAssignment;
using ChannelVN.IMS2.Core.Models.CacheStore;
using ChannelVN.IMS2.Core.Models.CacheStore.Security;
using ChannelVN.IMS2.Core.Models.SearchStore;
using ChannelVN.IMS2.Core.Repositories.Constants;
using ChannelVN.IMS2.Core.Repositories.Functions;
using ChannelVN.IMS2.Foundation.Common;
using ChannelVN.IMS2.Foundation.Data;
using ChannelVN.IMS2.Foundation.Data.Factories;
using ChannelVN.IMS2.Foundation.Logging;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace ChannelVN.IMS2.Core.Repositories
{
    public class AccountAssignmentRepository : Repository
    {
        public static async Task<bool> AddAsync(AccountAssignment data)
        {
            var returnValue = false;
            try
            {                
                returnValue = await StoreFactory.Create<AccountAssignmentCacheStore>().AddAsync(data);
                if (returnValue)
                {
                    await Function.AddToQueue(ActionName.AddAccountAssignmentES, TopicName.ES_ACCOUNTASSIGNMENT, data);
                    await Function.AddToQueue(ActionName.AddAccountAssignmentSQL, TopicName.SQL_ACCOUNTASSIGNMENT, data);
                }
                return returnValue;
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
                return returnValue;
            }
        }
        public static async Task<bool> UpdateAsync(AccountAssignment data)
        {
            try
            {
                var returnValue = await StoreFactory.Create<AccountAssignmentCacheStore>().UpdateAsync(data);
                if (returnValue)
                {
                    await Function.AddToQueue(ActionName.UpdateAccountAssignmentES, TopicName.ES_ACCOUNTASSIGNMENT, data);
                    await Function.AddToQueue(ActionName.UpdateAccountAssignmentSQL, TopicName.SQL_ACCOUNTASSIGNMENT, data);
                }
                return returnValue;
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
                return false;
            }
        }
        public static async Task<bool> RemoveAsync(AccountAssignment data)
        {
            var returnValue = false;
            try
            {
                returnValue = await StoreFactory.Create<AccountAssignmentCacheStore>().RemoveAsync(data);
                if (returnValue)
                {
                    await Function.AddToQueue(ActionName.RemoveAccountAssignmentES, TopicName.ES_ACCOUNTASSIGNMENT, data);
                    await Function.AddToQueue(ActionName.RemoveAccountAssignmentSQL, TopicName.SQL_ACCOUNTASSIGNMENT, data);
                }
                return returnValue;
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
                return returnValue;
            }
        }
        public static async Task<PagingDataResult<AccountAssignment>> SearchAsync(long accountId, long officerId, string assignedBy, int pageIndex, int pageSize)
        {
            try
            {
                return await StoreFactory.Create<AccountAssignmentSearchStore>().SearchAsync(accountId, officerId, assignedBy, pageIndex, pageSize);
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
                return null;
            }
        }

        public static async Task<PagingDataResult<object>> ListPageAsync(long accountId, long officerId, string assignedBy, int pageIndex, int pageSize)
        {
            var returnValue = new PagingDataResult<object>();
            try
            {
                var result = await StoreFactory.Create<AccountAssignmentSearchStore>().SearchAsync(accountId, officerId, assignedBy, pageIndex, pageSize);
                if (result != null && result.Data != null && result.Data.Count() > 0)
                {
                    returnValue.Total = result.Total;
                    var list = result.Data.Select(p => p.OfficerId.ToString());
                    returnValue.Data = await StoreFactory.Create<AccountCacheStore>().GetListSimpleByIdsAsync(list.ToList());
                }
                return returnValue;
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
                return null;
            }
        }
        public static Task<AccountAssignment> GetByIdAsync(string id)
        {
            return StoreFactory.Create<AccountAssignmentSearchStore>().GetByIdAsync(id);
        }       

        public static Task<bool> IsPageExistAsync(long accountId)
        {
            return StoreFactory.Create<AccountAssignmentSearchStore>().IsPageExistAsync(accountId);
        }
    }
}
