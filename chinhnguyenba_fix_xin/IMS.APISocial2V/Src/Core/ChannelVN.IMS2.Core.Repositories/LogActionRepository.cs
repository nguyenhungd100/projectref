﻿using ChannelVN.IMS2.Foundation.Logging;
using System.Threading.Tasks;

namespace ChannelVN.IMS2.Core.Repositories
{
    public static class LogActionRepository
    {
        public static Task<bool> InsertAsync(LogActionEntity log)
        {
            return LogActionNodeJs.InsertLogAsync(log);
        }
    }
}
