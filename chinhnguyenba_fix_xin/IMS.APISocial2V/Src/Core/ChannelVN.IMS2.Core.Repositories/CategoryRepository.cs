﻿using ChannelVN.IMS2.Core.Models.DataStore;
using ChannelVN.IMS2.Foundation.Data;
using ChannelVN.IMS2.Foundation.Data.Factories;
using System.Threading.Tasks;

namespace ChannelVN.IMS2.Core.Repositories
{
    public class CategoryRepository : Repository
    {
        public static Task<bool> Save()
        {
            var result = StoreFactory.Create<CategoryDataStore>().AddAsync();
            return result;
        }
    }
}
