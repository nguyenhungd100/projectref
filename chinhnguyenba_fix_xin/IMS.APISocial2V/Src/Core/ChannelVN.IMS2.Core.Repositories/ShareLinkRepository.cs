﻿using ChannelVN.IMS2.Core.Entities;
using ChannelVN.IMS2.Core.Entities.Distribution;
using ChannelVN.IMS2.Core.Entities.Media;
using ChannelVN.IMS2.Core.Entities.Security;
using ChannelVN.IMS2.Core.Entities.ShareLinks;
using ChannelVN.IMS2.Core.Models.CacheStore.ShareLinks;
using ChannelVN.IMS2.Core.Models.DataStore.ShareLinks;
using ChannelVN.IMS2.Core.Models.ExternalAPI;
using ChannelVN.IMS2.Core.Models.SearchStore.ShareLinks;
using ChannelVN.IMS2.Core.Repositories.Constants;
using ChannelVN.IMS2.Core.Repositories.Functions;
using ChannelVN.IMS2.Foundation.Common;
using ChannelVN.IMS2.Foundation.Common.Configuration;
using ChannelVN.IMS2.Foundation.Data;
using ChannelVN.IMS2.Foundation.Data.Factories;
using ChannelVN.IMS2.Foundation.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using static ChannelVN.IMS2.Core.Entities.ErrorCode.ErrorMapping;

namespace ChannelVN.IMS2.Core.Repositories
{
    public class ShareLinkRepository : Repository
    {
        public static async Task<ErrorCodes> AddAsync(ShareLinkSearch data, string clientIP)
        {
            var returnValue = ErrorCodes.AddError;
            try
            {
                returnValue = await StoreFactory.Create<ShareLinkCacheStore>().AddAsync(data);
                if (returnValue == ErrorCodes.Success)
                {
                    var res = await StoreFactory.Create<ShareLinkSearchStore>().AddAsync(data);
                    if (!res)
                    {
                        //Push queue
                        await Function.AddToQueue(ActionName.AddShareLinkES, TopicName.ES_SHARE_LINK, data.Id);
                    }

                    res = await StoreFactory.Create<ShareLinkDataStore>().AddAsync(data);
                    if (!res)
                    {
                        //Push queue
                        await Function.AddToQueue(ActionName.AddShareLinkSQL, TopicName.SQL_SHARE_LINK, data.Id);
                    }

                    //if (data.Status == (int)NewsStatus.Archive && AppSettings.Current.Distribution.Automation.Enabled)
                    //{
                    //    //cho nay chua sua
                    //    await Function.AddToQueue(ActionName.AutoShareShareLink, TopicName.AUTOSHARESHARELINK, new QueueData<long, string, long[], object>
                    //    {
                    //        Data1 = data.Id,
                    //        Data2 = clientIP
                    //    });
                    //}
                }
                return returnValue;
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
                return returnValue;
            }
        }
        public static async Task<ErrorCodes> UpdateAsync(ShareLink data, long officerId, string clientIP, string sessionId)
        {
            try
            {
                var returnValue = await StoreFactory.Create<ShareLinkCacheStore>().UpdateV2Async(data, officerId, new Action<int>(async (oldStatus) => {
                    if (AppSettings.Current.Distribution.Automation.Enabled)
                    {
                        if (oldStatus == (int)NewsStatus.Published)
                        {
                            var action = Media.Action.update;
                            await Function.AddToQueue(ActionName.AutoShareShareLink, TopicName.AUTOSHARESHARELINK, new QueueData<long, string, string, Media.Action>
                            {
                                Data1 = data.Id,
                                Data2 = clientIP,
                                Data3 = sessionId,
                                Data4 = action
                            });

                            await Function.AddToQueue(ActionName.PushChangeNotify, TopicName.PUSHBOARDTOCHANNEL, data.Id);
                        }
                    }
                }));
                if (returnValue == ErrorCodes.Success)
                {
                    await Function.AddToQueue(ActionName.UpdateShareLinkES, TopicName.ES_SHARE_LINK, data.Id);
                    await Function.AddToQueue(ActionName.UpdateShareLinkSQL, TopicName.SQL_SHARE_LINK, data.Id);
                }
                return returnValue;
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
                return ErrorCodes.Exception;
            }
        }
        public static Task<ShareLinkSearch> GetShareLinkSearchByIdAsync(long id)
        {
            return StoreFactory.Create<ShareLinkCacheStore>().GetFullInfoAsync(id);
        }

        public static async Task<ErrorCodes> UpdateStatusAsync(ShareLinkSearch data, int statusOld, string clientIP)
        {
            var returnCode = ErrorCodes.BusinessError;
            try
            {
                var returnValue = await StoreFactory.Create<ShareLinkCacheStore>().UpdateStatusAsync(Mapper<ShareLink>.Map(data, new ShareLink()), statusOld);
                if (returnValue)
                {
                    returnCode = ErrorCodes.Success;
                    await Function.AddToQueue(ActionName.UpdateShareLinkStatusES, TopicName.ES_SHARE_LINK, data.Id);
                    await Function.AddToQueue(ActionName.UpdateShareLinkStatusSQL, TopicName.SQL_SHARE_LINK, data.Id);

                    //auto phan phoi khi action publish     
                    if (AppSettings.Current.Distribution.Automation.Enabled)
                    {
                        if (data.Status == (int)NewsStatus.Published
                         || (data.Status == (int)NewsStatus.Remove && (statusOld == (int)NewsStatus.Published || statusOld == (int)NewsStatus.Archive))
                         || (data.Status == (int)NewsStatus.Draft && (statusOld == (int)NewsStatus.Published || statusOld == (int)NewsStatus.Archive)))
                        {
                            var action = Media.Action.add;
                            if (statusOld == (int)NewsStatus.Published)
                            {
                                action = Media.Action.update;
                            }
                            await Function.AddToQueue(ActionName.AutoShareShareLink, TopicName.AUTOSHARESHARELINK, new QueueData<long, string, long[], Media.Action>
                            {
                                Data1 = data.Id,
                                Data2 = clientIP,
                                Data4 = action
                            });
                        }
                    }
                }
                return returnCode;
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
                return ErrorCodes.Exception;
            }
        }

        public static async Task<ErrorCodes> UpdateStatusV2Async(long id, NewsStatus status, long userId, string clientIP, string sessionId)
        {
            var returnCode = ErrorCodes.BusinessError;
            try
            {
                returnCode = await StoreFactory.Create<ShareLinkCacheStore>().UpdateStatusV2Async(id, status, userId, new Action<ShareLinkSearch, int>(async (data, oldStatus) =>
                {
                    //auto phan phoi khi action publish         
                    if (AppSettings.Current.Distribution.Automation.Enabled)
                    {
                        if (status == NewsStatus.Published
                         || (data.Status == (int)NewsStatus.Remove && oldStatus == (int)NewsStatus.Published)
                         || (data.Status == (int)NewsStatus.Draft && oldStatus == (int)NewsStatus.Published))
                        {
                            var action = Media.Action.add;
                            if (status == NewsStatus.Published && oldStatus == (int)NewsStatus.Published)
                            {
                                action = Media.Action.update;
                            }
                            if (status == NewsStatus.Remove)
                            {
                                action = Media.Action.deletebymedia;
                            }
                            await Function.AddToQueue(ActionName.AutoShareShareLink, TopicName.AUTOSHARESHARELINK, new QueueData<long, string, string, Media.Action>
                            {
                                Data1 = data.Id,
                                Data2 = clientIP,
                                Data3 = sessionId,
                                Data4 = action
                            });
                        }
                    }
                }));

                if (returnCode == ErrorCodes.Success)
                {
                    await Function.AddToQueue(ActionName.UpdateShareLinkStatusES, TopicName.ES_SHARE_LINK, id);
                    await Function.AddToQueue(ActionName.UpdateShareLinkStatusSQL, TopicName.SQL_SHARE_LINK, id);
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
                returnCode = ErrorCodes.Exception;
            }
            return returnCode;
        }

        public static async Task<bool> AutoShare(ShareLinkSearch data, string clientIP, string sessionId, Media.Action action)
        {
            try
            {
                var listItem = Json.Parse<PublishData>(data.PublishData);
                var itemMediaUnit = new List<ItemMediaUnit>();
                if (listItem != null && listItem.Items?.Count > 0)
                {
                    foreach (var item in listItem.Items)
                    {
                        itemMediaUnit.Add(new ItemMediaUnit
                        {
                            Id = data.Id.ToString(),
                            Title = item.Title,
                            Link = item.Link,
                            Link_instantview = item.Link_instantview,
                            Image = item.Image,
                            Source = item.Source,
                            DataType = item.DataType,
                            original_url = item.original_url,
                            CreatedDate = data.DistributionDate ?? DateTime.Now
                        });
                    }
                }

                var user_created = await SecurityRepository.GetByIdAsync(long.Parse(data.NewsInAccount?.LastOrDefault()?.AccountId.ToString() ?? data.CreatedBy));
                var payloadDataShare = new PayloadDataShare
                {
                    DistributionId = AppSettings.Current.Distribution.Automation.DistributorId,
                    TemplateId = 0,
                    Title = data.Title,
                    Type = NewsType.ShareLink,
                    MetaDataShared = new List<MetaDataShared>
                    {
                        new MetaDataShared
                        {
                            ObjectId = data.Id,
                            Title = listItem.Caption,
                            ZoneId = 0,
                            DistributionDate = data.DistributionDate,
                            CardType = (int)CardType.ShareNews,
                            Frameid = 0,
                            Tags = data.Tags,
                            UserId = user_created?.Id??0,
                            UserName = user_created?.UserName,
                            FullName = user_created?.FullName,
                            AuthorAvatar = user_created?.Avatar,
                            IsDeleted = data.Status == (int)NewsStatus.Remove||data.Status == (int)NewsStatus.Draft ? 1 : (data.Status == (int)NewsStatus.Archive ? 2 : 0),
                            ItemMediaUnit = itemMediaUnit,
                            BoardIds = null,
                            CaptionExt= listItem.CaptionExt,
                            CommentMode = data.CommentMode
                        }
                    }
                };
                var distribution = await DistributionRepository.GetByIdAsync(payloadDataShare.DistributionId);
                var newsDistribution = data.NewsDistribution?.Where(p => p.DistributorId == payloadDataShare.DistributionId).FirstOrDefault();
                var itemStreamDistribution = new ItemStreamDistribution()
                {
                    Id = newsDistribution != null ? newsDistribution.ItemStreamId : Generator.ItemStreamDistributionId(),
                    PublishStatus = data.Status == (int)NewsStatus.Published ? (int)DistributionPublishStatus.Accept : (int)DistributionPublishStatus.Reject,
                    CreatedDate = DateTime.Now,
                    ItemStreamTempId = payloadDataShare.TemplateId,
                    Title = payloadDataShare.Title,
                    MetaData = Json.Stringify(payloadDataShare.MetaDataShared),
                    Type = payloadDataShare.Type
                };
                var result = await DistributionRepository.ShareShareLinkAsync(itemStreamDistribution, distribution, payloadDataShare, sessionId, action);
                if (result)
                {
                    //insert log
                    await Function.AddToQueue(ActionName.InsertLog, TopicName.LOGACTION, new LogActionEntity
                    {
                        ObjectId = data.Id.ToString(),
                        Account = data.ModifiedBy,
                        CreatedDate = DateTime.SpecifyKind(DateTime.Now, DateTimeKind.Unspecified),
                        Ip = clientIP,
                        Message = "Success.",
                        Title = data.Title,
                        SourceId = data.ModifiedBy,
                        ActionTypeDetail = (int)ActionType.Share,
                        ActionText = ActionType.Share.ToString(),
                        Type = (int)LogContentType.ShareLink
                    });
                }
                return result;
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
                return false;
            }

        }

        public static async Task<PagingDataResult<ShareLinkSearchReturn>> SearchAsync(SearchNews data, long userId, bool isGetDistribution)
        {
            try
            {
                var dataReturn = new PagingDataResult<ShareLinkSearchReturn>();
                if (isGetDistribution == false && string.IsNullOrEmpty(data.Keyword) && data.OrderBy != SearchNewsOrderBy.DescDistributionDate
                    && data.OrderBy != SearchNewsOrderBy.AscDistributionDate && data.OrderBy != SearchNewsOrderBy.DescModifiedDate && data.OrderBy != SearchNewsOrderBy.AscModifiedDate)
                {
                    dataReturn = await StoreFactory.Create<ShareLinkCacheStore>().SearchAsync(data, userId, new Action<string>(async (tempKey) =>
                    {
                        await Function.AddToQueue(ActionName.SetExpire, TopicName.DELETE_INDEX_TEMP, tempKey);
                    }));
                }
                else
                {
                    var dataSearch = await StoreFactory.Create<ShareLinkSearchStore>().SearchAsync(data, userId, isGetDistribution);
                    if (dataSearch?.Data?.Count() > 0)
                    {
                        var listData = await GetListByIdsAsync(dataSearch.Data.Select(p => p.Id.ToString()).ToList());
                        if (listData != null)
                        {
                            dataSearch.Data = listData.Select(p => Mapper<ShareLinkSearch>.Map(p, dataSearch.Data.Where(c => c.Id == p.Id).FirstOrDefault())).ToList();
                        }
                        dataReturn.Total = dataSearch.Total;
                        dataReturn.Data = dataSearch.Data.Select(p => Mapper<ShareLinkSearchReturn>.Map(p, new ShareLinkSearchReturn())).ToList();
                    }
                }
                return dataReturn;
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
                return null;
            }
        }

        public static async Task<PagingDataResult<ShareLink>> GetListApproveAsync(SearchNews data, long accountId)
        {
            try
            {
                var dataReturn = new PagingDataResult<ShareLink>();
                if (string.IsNullOrEmpty(data.Keyword) && data.OrderBy != SearchNewsOrderBy.DescDistributionDate
                    && data.OrderBy != SearchNewsOrderBy.AscDistributionDate && data.OrderBy != SearchNewsOrderBy.DescModifiedDate && data.OrderBy != SearchNewsOrderBy.AscModifiedDate)
                {
                    dataReturn = await StoreFactory.Create<ShareLinkCacheStore>().GetListApproveAsync(data, accountId, new Action<string>(async (tempKey) =>
                    {
                        await Function.AddToQueue(ActionName.SetExpire, TopicName.DELETE_INDEX_TEMP, tempKey);
                    }));
                }
                else
                {
                    dataReturn = await StoreFactory.Create<ShareLinkCacheStore>().GetListApproveAsync(data, accountId, new Action<string>(async (tempKey) =>
                    {
                        await Function.AddToQueue(ActionName.SetExpire, TopicName.DELETE_INDEX_TEMP, tempKey);
                    }));
                }
                return dataReturn;
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
                return null;
            }
        }

        public static Task<List<ShareLink>> GetListByIdsAsync(List<string> ids)
        {
            return StoreFactory.Create<ShareLinkCacheStore>().GetListByIdsAsync(ids);
        }

        public static async Task<bool> PushShareLinkToKingHub(string sessionId, PayloadDataShare playloadDataShare, Media.Action action)
        {
            try
            {
                if (playloadDataShare != null && playloadDataShare.MetaDataShared?.Count > 0)
                {
                    var media = playloadDataShare.MetaDataShared.FirstOrDefault();

                    var is_comment = AppSettings.Current.ChannelConfiguration.ApiCommentSetting.Mode.PostCheck;
                    if (media.CommentMode == CommentMode.Disable)
                    {
                        is_comment = AppSettings.Current.ChannelConfiguration.ApiCommentSetting.Mode.Disable;
                    }
                    else
                    {
                        if (media.CommentMode == CommentMode.PreCheck) is_comment = AppSettings.Current.ChannelConfiguration.ApiCommentSetting.Mode.PreCheck;
                    }
                    
                    var post = new
                    {
                        posts = new List<object>{ new {
                            media_id = media.ObjectId.ToString(),
                            title = media.Title??string.Empty,
                            mediaunit_name = media.Name??string.Empty,
                            card_type = (int) CardType.ShareNews,
                            media_unit_desc=new List<object>{ },
                            preview = media.ItemMediaUnit?.Select(s=> new {
                                id = s.Id??string.Empty,
                                title = s.Title??string.Empty,
                                link = s.Link??string.Empty,
                                link_instantview = s.Link_instantview,
                                image = new {
                                    thumb = s.Image?.Thumb??"https://kingcontent.lotuscdn.vn/2019/10/19/no-thumb-01-15714835978471536613828.jpg", //default-thumb
                                    width = s.Image?.Width == 0 || s.Image?.Width == null ? 600 : s.Image?.Width,
                                    height = s.Image?.Height == 0 || s.Image?.Height == null ? 300 : s.Image?.Height,
                                    content_type = s.ContentType==ContentType.Image ? (int) s.ContentType : (int)ContentType.Photo
                                },
                                source = s.Source??string.Empty,
                                content_type = (int) ContentType.Linkshare,
                                data_type = s.DataType !=null ?(int) s.DataType :(int) DataType.Webview,
                                original_url = s.original_url??string.Empty,
                                created_at = (s.CreatedDate??DateTime.Now).ToString("yyyy-MM-ddTHH:mm:ssZ")
                            }).ToList().FirstOrDefault(),
                            channel_id = media.ChannelId??string.Empty,
                            frame_id = media.Frameid,
                            tags = media.Tags??string.Empty,
                            user_id = media.UserId.ToString(),
                            user_name = media.UserName??string.Empty,
                            full_name = media.FullName??string.Empty,
                            avatar = media.AuthorAvatar??string.Empty,
                            cat_id = media.ZoneId?.ToString()??string.Empty,
                            create_time = Utility.ConvertToTimestamp(media.DistributionDate??DateTime.Now),
                            cat_name = media.ZoneName??string.Empty,
                            extension = new {
                                status = media.CaptionExt??new Caption[]{},
                                flag = new
                                {
                                    is_comment
                                },
                                is_web = 1,
                                boards=media.BoardIds != null && media.BoardIds.Length > 0 ? string.Join(",", media.BoardIds) : string.Empty
                            },
                        }}
                    };

                    //hearder thay vì session-id , các anh truyền vào user-id của page hoặc user nhé
                    sessionId = media.UserId.ToString();

                    if (action == Media.Action.add)
                    {
                        var errorCodeReturn = await Media.Add(media.ObjectId.ToString(), sessionId, CardType.ShareNews, post);

                        if (errorCodeReturn == ErrorCodes.DataExist)
                        {                            
                            action = Media.Action.update;                            
                        }
                        else
                        {
                            return errorCodeReturn == ErrorCodes.Success ? true : false;
                        }
                    }

                    if (action == Media.Action.update)
                    {
                        return await Media.Update(media.ObjectId.ToString(), sessionId, CardType.ShareNews, post) == ErrorCodes.Success ? true : false;
                    }

                    if (action == Media.Action.deletebymedia)
                    {
                        return await Media.Delete(media.ObjectId.ToString(), sessionId, CardType.ShareNews) == ErrorCodes.Success ? true : false;
                    }
                    //action ko hỗ trợ
                    return false;
                }

                return false;
            }
            catch (Exception ex)
            {
                Logger.Error(ex, "PushBlogToKingHub: " + ex.Message);
                return false;
            }
        }

    }
}
