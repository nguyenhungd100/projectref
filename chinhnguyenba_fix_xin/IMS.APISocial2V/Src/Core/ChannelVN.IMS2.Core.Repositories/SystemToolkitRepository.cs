﻿using ChannelVN.IMS2.Core.Entities;
using ChannelVN.IMS2.Core.Entities.Board;
using ChannelVN.IMS2.Core.Entities.Security;
using ChannelVN.IMS2.Core.Models.CacheStore.Security;
using ChannelVN.IMS2.Core.Models.DataStore.Security;
using ChannelVN.IMS2.Core.Models.SearchStore;
using ChannelVN.IMS2.Foundation.Common;
using ChannelVN.IMS2.Foundation.Common.Configuration;
using ChannelVN.IMS2.Foundation.Data.Factories;
using ChannelVN.IMS2.Foundation.Logging;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;

namespace ChannelVN.IMS2.Core.Repositories
{
    public class SystemToolkitRepository
    {
        public async static Task<bool> InitRedisKeyMobileFromDB(List<Account> data)
        {
            return await StoreFactory.Create<SystemToolkitCacheStore>().InitRedisKeyMobileFromDB(data);
        }

        public async static Task<bool> InitRedisKeyEmailFromDB(List<Account> data)
        {
            return await StoreFactory.Create<SystemToolkitCacheStore>().InitRedisKeyEmailFromDB(data);
        }

        public async static Task<bool> DeleteRedisByKey(string key)
        {
            return await StoreFactory.Create<SystemToolkitCacheStore>().DeleteRedisByKey(key);
        }

        public async static Task<List<Account>> GetAllAccountAsync()
        {
            return await StoreFactory.Create<AccountDataStore>().GetAllAccountAsync();
        }

        public async static Task<PagingDataResult<NewsAllSearch>> SearchCheckPostAsync(SearchNews search)
        {
            return await StoreFactory.Create<NewsSearchStore>().SearchCheckPostAsync(search);
        }

        public static async Task<bool> InsertAndUpdatePageOwnerAndTypeToAppAsync(long userId, string ownerId, byte? userClass, string pageName)
        {
            var returnValue = false;
            var content = new FormUrlEncodedContent(new
            {
                userid = userId.ToString(),
                ownerid = ownerId,
                user_type = userClass?.ToString()??string.Empty,
                page_name = pageName
            }.ConvertToKeyValuePair());
            string query = await content?.ReadAsStringAsync();

            try
            {
                using (var client = new HttpClient())
                {
                    client.Timeout = TimeSpan.FromSeconds(AppSettings.Current.ChannelConfiguration.HttpClientTimeOut);
                    string path = AppSettings.Current.ChannelConfiguration.KingHubBoardApiSetting.ApiUrl + "/" + BoardAction.InsertUpdateOwner + "?" + query;
                    var response = await client.GetAsync(path);

                    if (!response.IsSuccessStatusCode)
                    {
                        response.EnsureSuccessStatusCode();
                    }

                    if (response?.StatusCode == HttpStatusCode.OK)
                    {
                        var result = Json.Parse<KingHubBoardResponse<object>>(await response.Content?.ReadAsStringAsync());
                        if (result?.Status == 1)
                            returnValue = true;
                        else
                            Logger.Sensitive("InsertOwnerPage => query: " + query + "; output:" + await response?.Content?.ReadAsStringAsync(), new
                            {
                                userId,
                                ownerId,
                                userClass
                            });
                    }
                    else
                    {
                        Logger.Sensitive("InsertOwnerPage => query: " + query + "; output:" + await response?.Content?.ReadAsStringAsync(), new
                        {
                            userId,
                            ownerId,
                            userClass
                        });
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Sensitive(ex, ex.Message);
            }
            return returnValue;
        }
    }
}
