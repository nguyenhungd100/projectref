﻿using ChannelVN.IMS2.Core.Entities;
using ChannelVN.IMS2.Core.Entities.Distribution;
using ChannelVN.IMS2.Core.Entities.Album;
using ChannelVN.IMS2.Core.Models.CacheStore.Album;
using ChannelVN.IMS2.Core.Models.DataStore.Album;
using ChannelVN.IMS2.Core.Models.SearchStore.Album;
using ChannelVN.IMS2.Core.Repositories.Constants;
using ChannelVN.IMS2.Core.Repositories.Functions;
using ChannelVN.IMS2.Foundation.Common;
using ChannelVN.IMS2.Foundation.Common.Configuration;
using ChannelVN.IMS2.Foundation.Data;
using ChannelVN.IMS2.Foundation.Data.Factories;
using ChannelVN.IMS2.Foundation.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ChannelVN.IMS2.Core.Entities.Media;
using ChannelVN.IMS2.Core.Entities.Photo;
using ChannelVN.IMS2.Core.Models.CacheStore.Photo;
using ChannelVN.IMS2.Core.Entities.Security;
using static ChannelVN.IMS2.Core.Entities.ErrorCode.ErrorMapping;
using ChannelVN.IMS2.Core.Models.ExternalAPI;

namespace ChannelVN.IMS2.Core.Repositories
{
    public class AlbumRepository : Repository
    {
        public static async Task<ErrorCodes> AddAsync(AlbumSearch data, string clientIP)
        {
            try
            {
                var returnValue = await StoreFactory.Create<AlbumCacheStore>().AddAsync(data);
                if (returnValue == ErrorCodes.Success)
                {
                    var res = await StoreFactory.Create<AlbumSearchStore>().AddAsync(data);
                    if (!res)
                    {
                        //push queue
                        await Function.AddToQueue(ActionName.AddAlbumES, TopicName.ES_ALBUM, data.Id);
                    }

                    res = await StoreFactory.Create<AlbumDataStore>().AddAsync(data);
                    if (!res)
                    {
                        //push queue
                        await Function.AddToQueue(ActionName.AddAlbumSQL, TopicName.SQL_ALBUM, data.Id);
                    }

                    //if (data.Status == (int)NewsStatus.Archive && AppSettings.Current.Distribution.Automation.Enabled)
                    //{
                    //    await Function.AddToQueue(ActionName.AutoShareAlbum, TopicName.AUTOSHAREALBUM, new QueueData<long, string, long[], object>
                    //    {
                    //        Data1 = data.Id,
                    //        Data2 = clientIP
                    //    });
                    //}
                }
                return returnValue;
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
                return ErrorCodes.Exception;
            }
        }

        public static async Task<ErrorCodes> UpdateAsync(AlbumSearch data, string clientIP, long OfficerId, string sessionId)
        {
            try
            {
                var returnValue = await StoreFactory.Create<AlbumCacheStore>().UpdateV2Async(data, OfficerId, new Action<int>(async (oldStatus) =>
                {
                    //auto phan phoi khi action publish      
                    if (AppSettings.Current.Distribution.Automation.Enabled)
                    {
                        if (oldStatus == (int)NewsStatus.Published)
                        {
                            var action = Media.Action.update;
                            await Function.AddToQueue(ActionName.AutoShareAlbum, TopicName.AUTOSHAREALBUM, new QueueData<long, string, string, Media.Action>
                            {
                                Data1 = data.Id,
                                Data2 = clientIP,
                                Data3 = sessionId,
                                Data4 = action
                            });

                            await Function.AddToQueue(ActionName.PushChangeNotify, TopicName.PUSHBOARDTOCHANNEL, data.Id);
                        }
                    }
                }));
                if (returnValue == ErrorCodes.Success)
                {
                    await Function.AddToQueue(ActionName.UpdateAlbumES, TopicName.ES_ALBUM, data.Id);
                    await Function.AddToQueue(ActionName.UpdateAlbumSQL, TopicName.SQL_ALBUM, data.Id);                    
                }
                return returnValue;
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
                return ErrorCodes.Exception;
            }
        }

        public static async Task<bool> AutoShare(AlbumSearch data, string clientIP, string sessionId, Media.Action action)
        {
            var returnValue = false;
            try
            {
                var listItem = Json.Parse<PublishData>(data.PublishData);
                var itemAlbum = new List<ItemMediaUnit>();
                if (listItem != null && listItem.Items?.Count > 0)
                {
                    foreach (var item in listItem.Items)
                    {
                        itemAlbum.Add(new ItemMediaUnit
                        {
                            Id = item.Id,
                            Title = item.Title,
                            ContentType = item.ContentType ?? item.Content_Type,
                            Duration = item.Duration,
                            Link = item.Link,
                            Thumb = item.Thumb,
                            Width = item.Width,
                            Height = item.Height,
                            TypeMedia = item.Type,
                            Position = item.Position,
                            IsPlay = item.IsPlay,
                            IsHeartDrop = item.IsHeartDrop,
                            CreatedDate = data.PhotoInAlbum?.Where(p => p.PhotoId.ToString().Equals(item.Id))?.FirstOrDefault()?.PublishedDate ?? DateTime.Now
                        });
                    }

                    var pageAccount = await SecurityRepository.GetByIdAsync(long.Parse(data.NewsInAccount?.LastOrDefault()?.AccountId.ToString()));
                    var payloadDataShare = new PayloadDataShare
                    {
                        DistributionId = AppSettings.Current.Distribution.Automation.DistributorId,
                        TemplateId = data.TemplateId ?? (int)TemplateId.Album2,
                        Title = listItem.Caption,
                        Type = NewsType.Album,
                        MetaDataShared = new List<MetaDataShared>
                        {
                            new MetaDataShared
                            {
                                ObjectId = data.Id,
                                Title = listItem.Caption,
                                Name = data.Name,
                                ZoneId = 0,
                                DistributionDate = data.DistributionDate,
                                CardType = (int)CardType.Album,
                                Frameid = data.TemplateId ?? 0 ,
                                Tags = data.Tags,
                                UserName = pageAccount?.UserName,
                                UserId = pageAccount?.Id??0,
                                FullName = pageAccount?.FullName,
                                AuthorAvatar = pageAccount?.Avatar,
                                IsDeleted = data.Status == (int)NewsStatus.Remove||data.Status == (int)NewsStatus.Draft ? 1: (data.Status ==(int)NewsStatus.Archive ? 2 : 0),
                                BoardIds = null,
                                ItemMediaUnit = itemAlbum,
                                CaptionExt = listItem.CaptionExt,
                                CommentMode = data.CommentMode,
                                LabelType = (LabelType?) pageAccount.LabelMode// data.LabelType
                            }
                        }
                    };

                    var distribution = await DistributionRepository.GetByIdAsync(payloadDataShare.DistributionId);

                    //check itemStream => update
                    var itemStreamId = data.NewsDistribution?.Where(s => s.DistributorId == payloadDataShare.DistributionId).FirstOrDefault();
                    var itemStreamDistribution = new ItemStreamDistribution()
                    {
                        Id = itemStreamId?.ItemStreamId ?? Generator.ItemStreamDistributionId(),
                        PublishStatus = (int)DistributionPublishStatus.Accept,
                        CreatedDate = itemStreamId?.SharedDate ?? DateTime.Now,
                        ModifiedDate = DateTime.Now,
                        ItemStreamTempId = payloadDataShare.TemplateId,
                        Title = payloadDataShare.Title,
                        MetaData = Json.Stringify(payloadDataShare.MetaDataShared),
                        Type = payloadDataShare.Type
                    };

                    returnValue = await DistributionRepository.ShareAlbumAsync(itemStreamDistribution, distribution, payloadDataShare, clientIP, sessionId, action);
                    if (returnValue)
                    {
                        await Function.AddToQueue(ActionName.InsertLog, TopicName.LOGACTION, new LogActionEntity
                        {
                            ObjectId = data.Id.ToString(),
                            Account = data.ModifiedBy,
                            CreatedDate = DateTime.SpecifyKind(DateTime.Now, DateTimeKind.Unspecified),
                            Ip = clientIP,
                            Message = "Success.",
                            Title = data.Name,
                            SourceId = data.ModifiedBy,
                            ActionTypeDetail = (int)ActionType.Share,
                            ActionText = ActionType.Share.ToString(),
                            Type = (int)LogContentType.Album
                        });
                    }
                }
                else
                {
                    returnValue = true;
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
            }
            return returnValue;
        }

        //public static async Task<bool> AutoShare2(AlbumSearch data, string clientIP, string sessionId, Media.Action action)
        //{
        //    var returnValue = false;
        //    try
        //    {
        //        var listItem = Json.Parse<PublishData>(data.PublishData);
        //        var itemAlbum = new List<ItemMediaUnit>();
        //        if (listItem != null && listItem.Items?.Count > 0)
        //        {
        //            foreach (var item in listItem.Items)
        //            {
        //                itemAlbum.Add(new ItemMediaUnit
        //                {
        //                    Id = item.Id,
        //                    Title = item.Title,
        //                    ContentType = item.ContentType ?? item.Content_Type,
        //                    Duration = item.Duration,
        //                    Link = item.Link,
        //                    Thumb = item.Thumb,
        //                    Width = item.Width,
        //                    Height = item.Height,
        //                    TypeMedia = item.Type,
        //                    Position = item.Position,
        //                    IsPlay = item.IsPlay,
        //                    IsHeartDrop = true,
        //                    CreatedDate = data.PhotoInAlbum?.Where(p => p.PhotoId.ToString().Equals(item.Id))?.FirstOrDefault()?.PublishedDate ?? DateTime.Now
        //                });
        //            }

        //            var pageAccount = await SecurityRepository.GetByIdAsync(long.Parse(data.NewsInAccount?.LastOrDefault()?.AccountId.ToString()));
        //            var payloadDataShare = new PayloadDataShare
        //            {
        //                DistributionId = AppSettings.Current.Distribution.Automation.DistributorId,
        //                TemplateId = data.TemplateId ?? (int)TemplateId.Album2,
        //                Title = listItem.Caption,
        //                Type = NewsType.Album,
        //                MetaDataShared = new List<MetaDataShared>
        //                {
        //                    new MetaDataShared
        //                    {
        //                        ObjectId = data.Id,
        //                        Title = listItem.Caption,
        //                        Name = data.Name,
        //                        ZoneId = 0,
        //                        DistributionDate = data.DistributionDate,
        //                        CardType = (int)CardType.Album,
        //                        Frameid = data.TemplateId ?? 0 ,
        //                        Tags = data.Tags,
        //                        UserName = pageAccount?.UserName,
        //                        UserId = pageAccount?.Id??0,
        //                        FullName = pageAccount?.FullName,
        //                        AuthorAvatar = pageAccount?.Avatar,
        //                        IsDeleted = data.Status == (int)NewsStatus.Remove||data.Status == (int)NewsStatus.Draft ? 1: (data.Status ==(int)NewsStatus.Archive ? 2 : 0),
        //                        BoardIds = null,
        //                        ItemMediaUnit = itemAlbum,
        //                        CaptionExt = listItem.CaptionExt,
        //                        CommentMode = data.CommentMode,
        //                        LabelType = (LabelType?) pageAccount.LabelMode// data.LabelType
        //                    }
        //                }
        //            };

        //            var distribution = await DistributionRepository.GetByIdAsync(payloadDataShare.DistributionId);

        //            //check itemStream => update
        //            var itemStreamId = data.NewsDistribution?.Where(s => s.DistributorId == payloadDataShare.DistributionId).FirstOrDefault();
        //            var itemStreamDistribution = new ItemStreamDistribution()
        //            {
        //                Id = itemStreamId?.ItemStreamId ?? Generator.ItemStreamDistributionId(),
        //                PublishStatus = (int)DistributionPublishStatus.Accept,
        //                CreatedDate = itemStreamId?.SharedDate ?? DateTime.Now,
        //                ModifiedDate = DateTime.Now,
        //                ItemStreamTempId = payloadDataShare.TemplateId,
        //                Title = payloadDataShare.Title,
        //                MetaData = Json.Stringify(payloadDataShare.MetaDataShared),
        //                Type = payloadDataShare.Type
        //            };

        //            returnValue = await DistributionRepository.ShareAlbumAsync(itemStreamDistribution, distribution, payloadDataShare, clientIP, sessionId, action);
        //            if (returnValue)
        //            {
        //                await Function.AddToQueue(ActionName.InsertLog, TopicName.LOGACTION, new LogActionEntity
        //                {
        //                    ObjectId = data.Id.ToString(),
        //                    Account = data.ModifiedBy,
        //                    CreatedDate = DateTime.SpecifyKind(DateTime.Now, DateTimeKind.Unspecified),
        //                    Ip = clientIP,
        //                    Message = "Success.",
        //                    Title = data.Name,
        //                    SourceId = data.ModifiedBy,
        //                    ActionTypeDetail = (int)ActionType.Share,
        //                    ActionText = ActionType.Share.ToString(),
        //                    Type = (int)LogContentType.Album
        //                });
        //            }
        //        }
        //        else
        //        {
        //            returnValue = true;
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        Logger.Error(ex, ex.Message);
        //    }
        //    return returnValue;
        //}

        public static Task<PagingDataResult<PhotoUnit>> PhotoInAlbumIdAsync(long albumId, int? pageIndex, int? pageSize)
        {
            return StoreFactory.Create<PhotoCacheStore>().PhotoInAlbumIdAsync(albumId, pageIndex, pageSize);
        }

        public static async Task<ErrorCodes> UpdateStatusV2Async(long id, NewsStatus status, long userId, string clientIP, string sessionId)
        {
            var returnCode = ErrorCodes.BusinessError;
            try
            {
                returnCode = await StoreFactory.Create<AlbumCacheStore>().UpdateStatusV2Async(id, status, userId, new Action<AlbumSearch, int>(async (data, oldStatus) =>
               {
                    //auto phan phoi khi action publish   
                    if (AppSettings.Current.Distribution.Automation.Enabled)
                   {
                       if ((status == NewsStatus.Published)
                            || (status == NewsStatus.Remove && oldStatus == (int)NewsStatus.Published)
                            || (status == NewsStatus.Draft && oldStatus == (int)NewsStatus.Published))
                       {
                           var action = Media.Action.add;
                           if (status == NewsStatus.Published && oldStatus == (int)NewsStatus.Published)
                           {
                               action = Media.Action.update;
                           }
                           if (status == NewsStatus.Remove)
                           {
                               action = Media.Action.deletebymedia;
                           }
                           await Function.AddToQueue(ActionName.AutoShareAlbum, TopicName.AUTOSHAREALBUM, new QueueData<long, string, string, Media.Action>
                           {
                               Data1 = data.Id,
                               Data2 = clientIP,
                               Data3=sessionId,
                               Data4=action
                           });
                       }
                   }
               }));

                if (returnCode == ErrorCodes.Success)
                {
                    await Function.AddToQueue(ActionName.UpdateStatusAlbumES, TopicName.ES_ALBUM, id);
                    await Function.AddToQueue(ActionName.UpdateStatusAlbumSQL, TopicName.SQL_ALBUM, id);
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
                returnCode = ErrorCodes.Exception;
            }
            return returnCode;
        }
        public static Task<Album> GetByIdAsync(long id)
        {
            return StoreFactory.Create<AlbumCacheStore>().GetByIdAsync(id);
        }


        public static Task<AlbumSearch> GetDetailByIdAsync(long id)
        {
            return StoreFactory.Create<AlbumCacheStore>().GetAlbumFullInfoAsync(id);
        }

        public static Task<Album> GetByIdTest(string accountId, int mode, long id)
        {
            return StoreFactory.Create<AlbumCacheStore>().GetByIdTest(accountId, mode, id);
        }
        public static Task<Album> GetByIdTest2(long accountId, int mode, long id)
        {
            return StoreFactory.Create<AlbumCacheStore>().GetByIdTest2(accountId, mode, id);
        }

        public static Task<List<Album>> GetListByIdAsync(long[] ids)
        {
            return StoreFactory.Create<AlbumCacheStore>().GetListByIdAsync(ids);
        }

        public static Task<List<AlbumSearch>> GetListAlbumSearchByIdsAsync(List<string> ids)
        {
            return StoreFactory.Create<AlbumCacheStore>().GetListAlbumSearchByIdsAsync(ids);
        }

        public static async Task<PagingDataResult<AlbumSearchReturn>> SearchAsync(SearchNews search, long accountId)
        {
            try
            {
                var dataReturn = new PagingDataResult<AlbumSearchReturn>();
                if (string.IsNullOrEmpty(search.Keyword) && search.OrderBy != SearchNewsOrderBy.DescDistributionDate
                    && search.OrderBy != SearchNewsOrderBy.AscDistributionDate && search.OrderBy != SearchNewsOrderBy.DescModifiedDate && search.OrderBy != SearchNewsOrderBy.AscModifiedDate)
                {
                    dataReturn = await StoreFactory.Create<AlbumCacheStore>().SearchAsync(search, accountId, async (tempkey) =>
                    {
                        await Function.AddToQueue(ActionName.SetExpire, TopicName.DELETE_INDEX_TEMP, tempkey);
                    });
                }
                else
                {
                    var dataSearch = await StoreFactory.Create<AlbumSearchStore>().SearchAsync(search, accountId);
                    if (dataSearch?.Data?.Count > 0)
                    {
                        dataSearch.Data = await GetListAlbumSearchByIdsAsync(dataSearch.Data.Select(p => p.Id.ToString()).ToList());
                        dataReturn.Total = dataSearch.Total;
                        dataReturn.Data = dataSearch.Data.Select(p => Mapper<AlbumSearchReturn>.Map(p, new AlbumSearchReturn())).ToList();
                    }
                }
                return dataReturn;
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
                return null;
            }
        }
        public static async Task<PagingDataResult<AlbumSearch>> GetListApproveAsync(SearchNews data, long accountId)
        {
            try
            {
                var dataReturn = new PagingDataResult<AlbumSearch>();
                if (string.IsNullOrEmpty(data.Keyword) && data.OrderBy != SearchNewsOrderBy.DescDistributionDate
                    && data.OrderBy != SearchNewsOrderBy.AscDistributionDate && data.OrderBy != SearchNewsOrderBy.DescModifiedDate && data.OrderBy != SearchNewsOrderBy.AscModifiedDate)
                {
                    dataReturn = await StoreFactory.Create<AlbumCacheStore>().GetListApproveAsync(data, accountId, new Action<string>(async (tempKey) =>
                    {
                        await Function.AddToQueue(ActionName.SetExpire, TopicName.DELETE_INDEX_TEMP, tempKey);
                    }));
                }
                else
                {
                    dataReturn = await StoreFactory.Create<AlbumCacheStore>().GetListApproveAsync(data, accountId, new Action<string>(async (tempKey) =>
                    {
                        await Function.AddToQueue(ActionName.SetExpire, TopicName.DELETE_INDEX_TEMP, tempKey);
                    }));
                }
                return dataReturn;
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
                return null;
            }
        }
        public static async Task<PagingDataResult<AlbumSearchReturn>> SearchV2Async(SearchNews search, long accountId)
        {
            try
            {
                var dataReturn = new PagingDataResult<AlbumSearchReturn>();
                if (string.IsNullOrEmpty(search.Keyword) && search.OrderBy != SearchNewsOrderBy.DescDistributionDate
                    && search.OrderBy != SearchNewsOrderBy.AscDistributionDate && search.OrderBy != SearchNewsOrderBy.DescModifiedDate && search.OrderBy != SearchNewsOrderBy.AscModifiedDate)
                {
                    if (search.OfficerId > 0)
                    {
                        if (!await SecurityRepository.CheckAccountMember(search.OfficerId ?? 0, accountId))
                        {
                            return dataReturn;
                        }
                    }
                    else
                    {
                        search.OfficerId = accountId;
                    }
                    dataReturn = await StoreFactory.Create<AlbumCacheStore>().SearchAsync(search, accountId, async (tempkey) =>
                    {
                        await Function.AddToQueue(ActionName.SetExpire, TopicName.DELETE_INDEX_TEMP, tempkey);
                    });
                }
                else
                {
                    var dataSearch = await StoreFactory.Create<AlbumSearchStore>().SearchAsync(search, accountId);
                    if (dataSearch?.Data?.Count > 0)
                    {
                        dataSearch.Data = await GetListAlbumSearchByIdsAsync(dataSearch.Data.Select(p => p.Id.ToString()).ToList());
                        dataReturn.Total = dataSearch.Total;
                        dataReturn.Data = dataSearch.Data.Select(p => Mapper<AlbumSearchReturn>.Map(p, new AlbumSearchReturn())).ToList();
                    }
                }
                return dataReturn;
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
                return null;
            }
        }
        public static Task<PagingDataResult<AlbumSearch>> ListDistributionAsync(SearchNews search, long accountId)
        {
            return StoreFactory.Create<AlbumSearchStore>().ListDistributionAsync(search, accountId);
        }

        public static Task<List<PhotoSearch>> GetListPhotoAsync(List<string> photoIds, int pageIndex, int pageSize)
        {
            return StoreFactory.Create<PhotoCacheStore>().GetListFullInfoByIdsAsync(photoIds?.Skip((pageIndex - 1) * pageSize)?.ToList());
        }

        public static async Task<bool> PushAlbumToKingHub(string sessionId, PayloadDataShare playloadDataShare, Media.Action action)
        {
            try
            {
                if (playloadDataShare != null && playloadDataShare.MetaDataShared?.Count > 0)
                {
                    var media = playloadDataShare.MetaDataShared.FirstOrDefault();

                    var is_comment = AppSettings.Current.ChannelConfiguration.ApiCommentSetting.Mode.PostCheck;
                    if (media.CommentMode == CommentMode.Disable)
                    {
                        is_comment = AppSettings.Current.ChannelConfiguration.ApiCommentSetting.Mode.Disable;
                    }
                    else
                    {
                        if (media.CommentMode == CommentMode.PreCheck) is_comment = AppSettings.Current.ChannelConfiguration.ApiCommentSetting.Mode.PreCheck;
                    }

                    var post = new
                    {
                        posts = new List<object>{ new {
                            media_id = media.ObjectId.ToString(),
                            title = media.Title??string.Empty,
                            mediaunit_name = media.Name??string.Empty,
                            card_type = (int)CardType.Album,
                            preview=new{ },
                            media_unit_desc = media.ItemMediaUnit?.Select(s=> new {
                                id = s.Id??string.Empty,
                                title = s.Title??string.Empty,
                                link = s.Link??string.Empty,
                                thumb = string.IsNullOrEmpty(s.Thumb) ? s.Link : s.Thumb,
                                width = s.Width,
                                height = s.Height,
                                content_type = s.ContentType == ContentType.Image ? (int) s.ContentType : (int)ContentType.Photo,
                                is_heart_drop = s.IsHeartDrop,
                                created_at = (s.CreatedDate??DateTime.Now).ToString("yyyy-MM-ddTHH:mm:ssZ")
                            }).ToList(),
                            channel_id = media.ChannelId??string.Empty,
                            frame_id = media.Frameid,
                            tags = media.Tags??string.Empty,
                            user_id = media.UserId.ToString(),
                            user_name = media.UserName??string.Empty,
                            full_name = media.FullName??string.Empty,
                            avatar = media.AuthorAvatar??string.Empty,
                            cat_id = media.ZoneId?.ToString()??string.Empty,
                            create_time = Utility.ConvertToTimestamp(media.DistributionDate??DateTime.Now),
                            cat_name = media.ZoneName??string.Empty,
                            extension = new {
                                status = media.CaptionExt??new Caption[]{},
                                flag = new
                                {
                                    is_comment
                                },
                                is_web = 1,
                                boards=media.BoardIds != null && media.BoardIds.Length > 0 ? string.Join(",", media.BoardIds) : string.Empty,
                                label = new
                                {
                                    id = ((int?)media.LabelType)?.ToString(),
                                    text = media.LabelType != null ? Enum.GetName(typeof(LabelType), media.LabelType)?.ToLower() : ""
                                }
                            },
                        }}
                    };

                    //hearder thay vì session-id , các anh truyền vào user-id của page hoặc user nhé
                    sessionId = media.UserId.ToString();

                    if (action == Media.Action.add)
                    {
                        var errorCodeReturn = await Media.Add(media.ObjectId.ToString(), sessionId, CardType.Album, post);
                        if (errorCodeReturn == ErrorCodes.DataExist)
                        {
                            action = Media.Action.update;
                        }
                        else
                        {
                            return errorCodeReturn == ErrorCodes.Success ? true : false;
                        }
                    }

                    if (action == Media.Action.update)
                    {
                        return await Media.Update(media.ObjectId.ToString(), sessionId, CardType.Album, post) == ErrorCodes.Success ? true : false;
                    }

                    if (action == Media.Action.deletebymedia)
                    {
                        return await Media.Delete(media.ObjectId.ToString(), sessionId, CardType.Album) == ErrorCodes.Success ? true : false;
                    }
                    //action ko hỗ trợ
                    return false;
                }

                return false;
            }
            catch (Exception ex)
            {
                Logger.Error(ex, "PushBlogToKingHub: " + ex.Message);
                return false;
            }
        }
    }
}
