﻿using ChannelVN.IMS2.Core.Entities;
using ChannelVN.IMS2.Core.Entities.Distribution;
using ChannelVN.IMS2.Core.Entities.ErrorCode;
using ChannelVN.IMS2.Core.Entities.Media;
using ChannelVN.IMS2.Core.Entities.Newsletters;
using ChannelVN.IMS2.Core.Entities.Security;
using ChannelVN.IMS2.Core.Models.CacheStore.Articles;
using ChannelVN.IMS2.Core.Models.DataStore.Articles;
using ChannelVN.IMS2.Core.Models.ExternalAPI;
using ChannelVN.IMS2.Core.Models.SearchStore.Articles;
using ChannelVN.IMS2.Core.Repositories.Constants;
using ChannelVN.IMS2.Core.Repositories.Functions;
using ChannelVN.IMS2.Foundation.Common;
using ChannelVN.IMS2.Foundation.Common.Configuration;
using ChannelVN.IMS2.Foundation.Data;
using ChannelVN.IMS2.Foundation.Data.Factories;
using ChannelVN.IMS2.Foundation.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using static ChannelVN.IMS2.Core.Entities.ErrorCode.ErrorMapping;

namespace ChannelVN.IMS2.Core.Repositories
{
    public class ArticleRepository : Repository
    {
        public static async Task<ErrorCodes> AddAsync(ArticleSearch data, string clientIP)
        {
            var returnValue = ErrorCodes.AddError;
            try
            {
                returnValue = await StoreFactory.Create<ArticleCacheStore>().AddAsync(data);
                if (returnValue == ErrorCodes.Success)
                {
                    var res = await StoreFactory.Create<ArticleSearchStore>().AddAsync(data);
                    if (!res)
                    {
                        await Function.AddToQueue(ActionName.AddArticleES, TopicName.ES_ARTICLE, data.Id);
                    }

                    res = await StoreFactory.Create<ArticleDataStore>().AddAsync(data);
                    if (!res)
                    {
                        await Function.AddToQueue(ActionName.AddArticleSQL, TopicName.SQL_ARTICLE, data.Id);
                    }

                    //if (data.Status == (int)NewsStatus.Archive && AppSettings.Current.Distribution.Automation.Enabled)
                    //{
                    //    await Function.AddToQueue(ActionName.AutoShareArticle, TopicName.AUTOSHAREARTICLE, new QueueData<long, string, long[], object>
                    //    {
                    //        Data1 = data.Id,
                    //        Data2 = clientIP
                    //    });
                    //}
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
                returnValue = ErrorCodes.Exception;
            }
            return returnValue;
        }

        public static Task<int> CountNewsByUserIdAsync(long userId, DateTime dateFrom, DateTime dateTo)
        {
            return StoreFactory.Create<ArticleSearchStore>().CountNewsByUserIdAsync(userId, dateFrom, dateTo);
        }

        public static async Task<ErrorCodes> UpdateAsync(Article data, string clientIP, long officerId, string sessionId)
        {
            var returnValue = ErrorCodes.BusinessError;
            var newsCache = StoreFactory.Create<ArticleCacheStore>();
            try
            {
                returnValue = await newsCache.UpdateV2Async(data, officerId, new Action<int>(async (oldStatus) => {
                    if (AppSettings.Current.Distribution.Automation.Enabled)
                    {
                        if (oldStatus == (int)NewsStatus.Published)
                        {
                            var action = Media.Action.update;
                            await Function.AddToQueue(ActionName.AutoShareArticle, TopicName.AUTOSHAREARTICLE, new QueueData<long, string, string, Media.Action>
                            {
                                Data1 = data.Id,
                                Data2 = clientIP,
                                Data3 = sessionId,
                                Data4 = action
                            });

                            await Function.AddToQueue(ActionName.PushChangeNotify, TopicName.PUSHBOARDTOCHANNEL, data.Id);
                        }
                    }
                }));
                if (returnValue == ErrorCodes.Success)
                {
                    await Function.AddToQueue(ActionName.UpdateArticleES, TopicName.ES_ARTICLE, data.Id);
                    await Function.AddToQueue(ActionName.UpdateArticleSQL, TopicName.SQL_ARTICLE, data.Id);                    
                }
                return returnValue;
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
                return ErrorCodes.Exception;
            }
        }

        public static async Task<bool> AutoShare(ArticleSearch data, string clientIP, string sessionId, Media.Action action, long[] boardIds = null)
        {
            try
            {
                var listItem = Json.Parse<PublishData>(data.PublishData);
                var itemMediaUnit = new List<ItemMediaUnit>();

                if (listItem != null && listItem.Items?.Count > 0)
                {
                    foreach (var item in listItem.Items)
                    {
                        itemMediaUnit.Add(new ItemMediaUnit
                        {
                            Id = data.Id.ToString(),
                            Title = item.Title,
                            Link = item.Link,
                            Link_instantview = item.Link_instantview,
                            Image = item.Image,
                            Source = item.Source,
                            ContentType = ContentType.Linkshare,
                            DataType = item.DataType ?? DataType.InstantView,
                            CreatedDate = data.DistributionDate ?? DateTime.Now
                        });
                    }
                }

                var pageAccount = await SecurityRepository.GetByIdAsync(long.Parse(data.NewsInAccount?.LastOrDefault()?.AccountId.ToString()));
                var payloadDataShare = new PayloadDataShare
                {
                    DistributionId = AppSettings.Current.Distribution.Automation.DistributorId,
                    TemplateId = 0,
                    Title = string.IsNullOrEmpty(listItem.Title) ? data.Title : listItem.Title,
                    Type = NewsType.Post,
                    MetaDataShared = new List<MetaDataShared>
                    {
                        new MetaDataShared
                        {
                            ObjectId = data.Id,
                            Title = string.IsNullOrEmpty(listItem.Caption) ? data.Title : listItem.Caption,
                            Name = data.Title,
                            ZoneId = 0,
                            DistributionDate = data.DistributionDate,
                            CardType = (int)CardType.Article,
                            Frameid = 0,
                            Tags = data.Tags,
                            UserId = pageAccount?.Id??0,
                            UserName = pageAccount?.UserName,
                            FullName = pageAccount?.FullName,
                            AuthorAvatar = pageAccount?.Avatar,
                            IsDeleted = data.Status == (int)NewsStatus.Remove||data.Status == (int)NewsStatus.Draft ? 1: (data.Status ==(int)NewsStatus.Archive ? 2 : 0),
                            ItemMediaUnit = itemMediaUnit,
                            BoardIds = boardIds,
                            CaptionExt = listItem.CaptionExt,
                            CommentMode = data.CommentMode
                        }
                    }
                };

                var distribution = await DistributionRepository.GetByIdAsync(payloadDataShare.DistributionId);
                var newsDistribution = data.NewsDistribution?.Where(p => p.DistributorId == payloadDataShare.DistributionId).FirstOrDefault();
                var itemStreamDistribution = new ItemStreamDistribution()
                {
                    Id = newsDistribution != null ? newsDistribution.ItemStreamId : Generator.ItemStreamDistributionId(),
                    PublishStatus = data.Status == (int)NewsStatus.Published ? (int)DistributionPublishStatus.Accept : (int)DistributionPublishStatus.Reject,
                    CreatedDate = DateTime.Now,
                    ItemStreamTempId = payloadDataShare.TemplateId,
                    Title = payloadDataShare.Title,
                    MetaData = Json.Stringify(payloadDataShare.MetaDataShared),
                    Type = payloadDataShare.Type
                };

                var result = await DistributionRepository.ShareArticleAsync(itemStreamDistribution, distribution, payloadDataShare, clientIP, sessionId, action);
                if (result)
                {
                    //insert log
                    await Function.AddToQueue(ActionName.InsertLog, TopicName.LOGACTION, new LogActionEntity
                    {
                        ObjectId = data.Id.ToString(),
                        Account = data.ModifiedBy,
                        CreatedDate = DateTime.SpecifyKind(DateTime.Now, DateTimeKind.Unspecified),
                        Ip = clientIP,
                        Message = "Success.",
                        Title = data.Title,
                        SourceId = data.ModifiedBy,
                        ActionTypeDetail = (int)ActionType.Share,
                        ActionText = ActionType.Share.ToString(),
                        Type = (int)LogContentType.Article
                    });
                }
                return result;
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
                return false;
            }
        }

        public static async Task<ArticleDetail> GetDetailByIdAsync(long id)
        {
            try
            {
                //sau viet lai
                var articleSearch = await StoreFactory.Create<ArticleSearchStore>().GetByIdAsync(id);
                var article = await StoreFactory.Create<ArticleCacheStore>().GetDetailByIdAsync(id);
                if (article != null)
                {
                    article.News = articleSearch != null ? Mapper<ArticleSearch>.Map(articleSearch, article.News) : article.News;
                }
                return article;
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
                return null;
            }
        }

        public static Task<int> GetPublishCountAsync(long authorId, int status, long? distributionId)
        {
            return StoreFactory.Create<ArticleSearchStore>().GetPublishCount(authorId, status, distributionId);
        }

        public static async Task<ErrorMapping.ErrorCodes> UpdateStatusAsync(ArticleSearch data, int statusOld, string clientIP)
        {
            var returnCode = ErrorCodes.BusinessError;
            try
            {
                var returnValue = await StoreFactory.Create<ArticleCacheStore>().UpdateStatusAsync(data, statusOld);
                if (returnValue)
                {
                    returnCode = ErrorCodes.Success;

                    await Function.AddToQueue(ActionName.UpdateStatusArticleES, TopicName.ES_ARTICLE, data.Id);
                    await Function.AddToQueue(ActionName.UpdateStatusArticleSQL, TopicName.SQL_ARTICLE, data.Id);
                    //auto phan phoi khi action publish   
                    if (AppSettings.Current.Distribution.Automation.Enabled)
                    {
                        if ((data.Status == (int)NewsStatus.Published)
                         || (data.Status == (int)NewsStatus.Remove && (statusOld == (int)NewsStatus.Published || statusOld == (int)NewsStatus.Archive))
                         || (data.Status == (int)NewsStatus.Draft && (statusOld == (int)NewsStatus.Published || statusOld == (int)NewsStatus.Archive)))
                        {
                            await Function.AddToQueue(ActionName.AutoShareArticle, TopicName.AUTOSHAREARTICLE, new QueueData<long, string, long[], object>
                            {
                                Data1 = data.Id,
                                Data2 = clientIP
                            });
                            //returnValue = await AutoShare(data, clientIP);
                            //if (!returnValue) returnCode = ErrorCodes.UpdateError;
                        }
                    }
                }
                return returnCode;
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
                return ErrorCodes.Exception;
            }
        }

        public static async Task<ErrorCodes> UpdateStatusV2Async(long id, NewsStatus status, long userId, string clientIP, string sessionId)
        {
            var returnCode = ErrorCodes.BusinessError;
            try
            {
                returnCode = await StoreFactory.Create<ArticleCacheStore>().UpdateStatusV2Async(id, status, userId, new Action<ArticleSearch, int>(async (data, statusOld) =>
                {
                    //auto phan phoi khi action publish    
                    if (AppSettings.Current.Distribution.Automation.Enabled)
                    {
                        if (status == NewsStatus.Published
                             || (data.Status == (int)NewsStatus.Remove && statusOld == (int)NewsStatus.Published)
                             || (data.Status == (int)NewsStatus.Draft && statusOld == (int)NewsStatus.Published))
                        {
                            var action = Media.Action.add;
                            if (status == NewsStatus.Published && statusOld == (int)NewsStatus.Published)
                            {
                                action = Media.Action.update;
                            }
                            if (status == NewsStatus.Remove)
                            {
                                action = Media.Action.deletebymedia;
                            }
                            await Function.AddToQueue(ActionName.AutoShareArticle, TopicName.AUTOSHAREARTICLE, new QueueData<long, string, string, Media.Action>
                            {
                                Data1 = data.Id,
                                Data2 = clientIP,
                                Data3 = sessionId,
                                Data4 = action
                            });
                        }
                    }
                }));
                if (returnCode == ErrorCodes.Success)
                {
                    await Function.AddToQueue(ActionName.UpdateStatusArticleES, TopicName.ES_ARTICLE, id);
                    await Function.AddToQueue(ActionName.UpdateStatusArticleSQL, TopicName.SQL_ARTICLE, id);
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
                returnCode = ErrorCodes.Exception;
            }
            return returnCode;
        }

        public static Task<List<Article>> GetListByIdsAsync(List<string> ids)
        {
            return StoreFactory.Create<ArticleCacheStore>().GetListByIdsAsync(ids);
        }

        public static Task<Article> GetByIdAsync(long id)
        {
            return StoreFactory.Create<ArticleCacheStore>().GetByIdAsync(id);
        }
        public static Task<Article> GetByIdAsync_Test(long id, int mode)
        {
            var result = Task.FromResult(default(Article));
            try
            {
                switch (mode)
                {
                    case 1: // mutil connection sql
                        result = StoreFactory.Create<ArticleDataStore>().GetByIdAsync(id);
                        // Logger.Information(Json.Stringify(result));

                        //var result = await StoreFactory.Create<ArticleCacheStore>().GetByIdAsync(id);
                        //return Mapper<Article>.Map(result, new Article());
                        break;
                    case 2: //Redis
                        result = StoreFactory.Create<ArticleCacheStore>().GetByIdAsync(id);
                        //Logger.Debug(Json.Stringify(result));
                        //return Mapper<Article>.Map(result, new Article());
                        break;
                    default:
                        break;
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
                return null;
            }
            return result;
        }

        public static Task<ArticleSearch> GetArticleSearchByIdAsync(long id)
        {
            return StoreFactory.Create<ArticleCacheStore>().GetArticleFullInfoAsync(id);
        }
        public static Task<ItemStreamDistribution> GetItemStreamByIdAsync(long id)
        {
            return StoreFactory.Create<ArticleCacheStore>().GetItemStreamByIdAsync(id);
        }

        public static Task<PagingDataResult<ArticleSearch>> GetByStatusAsync(SearchNews searchNews, long userId)
        {
            return StoreFactory.Create<ArticleSearchStore>().ListNewsByStatusAsync(searchNews, userId);
        }


        public static Task<PagingDataResult<ArticleSearch>> ListMyNewsAsync(long accountId, int pageIndex, int pageSize)
        {
            return StoreFactory.Create<ArticleSearchStore>().ListMyNewsAsync(accountId, pageIndex, pageSize);
        }
        public static Task<PagingDataResult<ArticleSearch>> ListNewsPublishAsync(long accountId, int pageIndex, int pageSize)
        {
            return StoreFactory.Create<ArticleSearchStore>().ListNewsPublishAsync(accountId, pageIndex, pageSize);
        }
        public static Task<PagingDataResult<ArticleSearch>> ListNewsByPublishStatusAsync(int publishStatus, long accountId, int pageIndex, int pageSize)
        {
            return StoreFactory.Create<ArticleSearchStore>().ListNewsByPublishStatus(publishStatus, accountId, pageIndex, pageSize);
        }

        public static Task<PagingDataResult<ArticleOnWall>> ListNewsByAuthorAsync(long accountId, long distributionId, int pageIndex, int pageSize)
        {
            return StoreFactory.Create<ArticleCacheStore>().ListNewsByAuthorAsync(accountId, distributionId, pageIndex, pageSize, new Action<string>(async (tempKey) =>
            {
                await Function.AddToQueue(ActionName.SetExpire, TopicName.DELETE_INDEX_TEMP, tempKey);
            }));
        }

        public static Task<PagingDataResult<ArticleSearch>> ListNewsByDistributionAsync(long distributionId, int publishStatus, long accountId, DateTime? FromDate, DateTime? ToDate, int pageIndex, int pageSize)
        {
            return StoreFactory.Create<ArticleSearchStore>().ListNewsByDistributionAsync(distributionId, publishStatus, accountId, FromDate, ToDate, pageIndex, pageSize);
        }
        public static async Task<PagingDataResult<ItemStreamDistribution>> ListItemStreamByDistributionAsync(GetNewsByDistribution getNewsByDistributionEntity)
        {
            try
            {
                var listItemStreamSearch = await StoreFactory.Create<ArticleSearchStore>().ListItemStreamByDistributionAsync(getNewsByDistributionEntity);
                if (listItemStreamSearch != null && listItemStreamSearch.Data != null && listItemStreamSearch.Data.Count > 0)
                {
                    var listItemStream = await StoreFactory.Create<ArticleCacheStore>().GetListItemStreamByIdsAsync(listItemStreamSearch.Data.Select(p => p.Id.ToString())?.ToList());
                    listItemStreamSearch.Data = listItemStream;
                }
                return listItemStreamSearch;
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
                return null;
            }
        }
        public static Task<long> GetCountItemStreamByDistributionAsync(long distributionId, int? PublishStatus)
        {
            return StoreFactory.Create<ArticleSearchStore>().GetCountItemStreamByDistributionAsync(distributionId, PublishStatus);
        }
        public static async Task<PagingDataResult<ArticleSearchReturn>> SearchAsync(SearchNews data, long accountId, bool isGetDistribution)
        {
            try
            {
                var dataReturn = new PagingDataResult<ArticleSearchReturn>();
                if (isGetDistribution == false && string.IsNullOrEmpty(data.Keyword) && data.OrderBy != SearchNewsOrderBy.DescDistributionDate
                    && data.OrderBy != SearchNewsOrderBy.AscDistributionDate && data.OrderBy != SearchNewsOrderBy.DescModifiedDate && data.OrderBy != SearchNewsOrderBy.AscModifiedDate)
                {
                    dataReturn = await StoreFactory.Create<ArticleCacheStore>().SearchAsync(data, accountId, new Action<string>(async (tempKey) =>
                    {
                        await Function.AddToQueue(ActionName.SetExpire, TopicName.DELETE_INDEX_TEMP, tempKey);
                    }));
                }
                else
                {
                    var dataSearch = await StoreFactory.Create<ArticleSearchStore>().SearchAsync(data, accountId, isGetDistribution);
                    if (dataSearch?.Data?.Count() > 0)
                    {
                        var listData = await GetListArticleSearchByIdsAsync(dataSearch.Data.Select(p => p.Id.ToString()).ToList(), dataSearch);
                        if (listData != null)
                        {
                            dataReturn.Total = dataSearch.Total;
                            dataReturn.Data = listData.Select(p => Mapper<ArticleSearchReturn>.Map(p, new ArticleSearchReturn())).ToList();
                        }
                    }
                }
                return dataReturn;
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
                return null;
            }
        }
        public static async Task<PagingDataResult<ArticleSearchReturn>> GetListApproveAsync(SearchNews data, long accountId)
        {
            try
            {
                var dataReturn = new PagingDataResult<ArticleSearchReturn>();
                if (string.IsNullOrEmpty(data.Keyword) && data.OrderBy != SearchNewsOrderBy.DescDistributionDate
                    && data.OrderBy != SearchNewsOrderBy.AscDistributionDate && data.OrderBy != SearchNewsOrderBy.DescModifiedDate && data.OrderBy != SearchNewsOrderBy.AscModifiedDate)
                {
                    dataReturn = await StoreFactory.Create<ArticleCacheStore>().GetListApproveAsync(data, accountId, new Action<string>(async (tempKey) =>
                    {
                        await Function.AddToQueue(ActionName.SetExpire, TopicName.DELETE_INDEX_TEMP, tempKey);
                    }));
                }
                else
                {
                    dataReturn = await StoreFactory.Create<ArticleCacheStore>().GetListApproveAsync(data, accountId, new Action<string>(async (tempKey) =>
                    {
                        await Function.AddToQueue(ActionName.SetExpire, TopicName.DELETE_INDEX_TEMP, tempKey);
                    }));
                }
                return dataReturn;
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
                return null;
            }
        }

        public static Task<List<ArticleSearch>> GetListArticleSearchByIdsAsync(List<string> ids, PagingDataResult<ArticleSearch> listData)
        {
            return StoreFactory.Create<ArticleCacheStore>().GetListArticleSearchByIdsAsync(ids, listData);
        }
        public static Task<PagingDataResult<string>> SearchItemStreamByIdAsync(long distributionId, AccountSearch account, DateTime? fromDate, DateTime? toDate)
        {
            return StoreFactory.Create<ArticleSearchStore>().SearchItemStreamByIdAsync(distributionId, account, fromDate, toDate);
        }

        public static async Task<bool> AcceptAsync(ItemStreamDistribution itemStream)
        {
            try
            {
                var returnValue = await StoreFactory.Create<ArticleCacheStore>().AcceptAsync(itemStream);//, listNews
                if (returnValue)
                {
                    await Function.AddToQueue(ActionName.AcceptItemStreamES, TopicName.ES_ARTICLE, itemStream);
                    await Function.AddToQueue(ActionName.AcceptItemStreamSQL, TopicName.SQL_ARTICLE, itemStream);
                }
                return returnValue;
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
                return false;
            }
        }

        public static async Task<bool> RejectAsync(ItemStreamDistribution itemStream)
        {
            try
            {
                itemStream.PublishStatus = (int)NewsDistributionPublishStatus.Reject;
                var returnValue = await StoreFactory.Create<ArticleCacheStore>().RejectAsync(itemStream);//, listNews.Data
                if (returnValue)
                {
                    await Function.AddToQueue(ActionName.RejectItemStreamES, TopicName.ES_ITEMSTREAM, itemStream);
                    await Function.AddToQueue(ActionName.RejectItemStreamSQL, TopicName.SQL_ITEMSTREAM, itemStream.Id);
                }
                return returnValue;
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
                return false;
            }
        }

        public static async Task<bool> PushToKingHub(string sessionId, PayloadDataShare playloadDataShare, Media.Action action)
        {
            try
            {
                if (playloadDataShare != null && playloadDataShare.MetaDataShared?.Count > 0)
                {
                    var media = playloadDataShare.MetaDataShared.FirstOrDefault();

                    var is_comment = AppSettings.Current.ChannelConfiguration.ApiCommentSetting.Mode.PostCheck;
                    if (media.CommentMode == CommentMode.Disable)
                    {
                        is_comment = AppSettings.Current.ChannelConfiguration.ApiCommentSetting.Mode.Disable;
                    }
                    else
                    {
                        if (media.CommentMode == CommentMode.PreCheck) is_comment = AppSettings.Current.ChannelConfiguration.ApiCommentSetting.Mode.PreCheck;
                    }

                    var post = new
                    {
                        posts = new List<object>{
                            new {
                                media_id = media.ObjectId.ToString(),
                                title = media.Title??string.Empty,
                                mediaunit_name = media.Name??string.Empty,
                                card_type = (int)CardType.Article,
                                preview = new{ },
                                media_unit_desc = media.ItemMediaUnit?.Select(s=> new {
                                    id = s.Id??string.Empty,
                                    title = s.Title??string.Empty,
                                    link = s.Link??string.Empty,
                                    link_instantview = s.Link_instantview,
                                    image = new { thumb = s.Image?.Thumb??string.Empty, width = s.Image?.Width == 0 || s.Image?.Width == null ? 600 : s.Image?.Width, height = s.Image?.Height == 0 || s.Image?.Height == null ? 300 : s.Image?.Height, content_type = s.ContentType == ContentType.Image ? (int)s.ContentType : (int)ContentType.Photo },
                                    source = s.Source??" ",
                                    content_type = (int)ContentType.Linkshare,
                                    data_type = s.DataType != null ? (int)s.DataType : (int)DataType.InstantView,
                                    created_at = (s.CreatedDate??DateTime.Now).ToString("yyyy-MM-ddTHH:mm:ssZ")
                                }).ToList(),
                                channel_id = media.ChannelId??string.Empty,
                                frame_id = media.Frameid,
                                tags = media.Tags??string.Empty,
                                user_id = media.UserId.ToString(),
                                user_name = media.UserName??string.Empty,
                                full_name = media.FullName??string.Empty,
                                avatar = media.AuthorAvatar??string.Empty,
                                cat_id = media.ZoneId?.ToString()??string.Empty,
                                create_time = Utility.ConvertToTimestamp(media.DistributionDate??DateTime.Now),
                                cat_name = media.ZoneName??string.Empty,
                                extension = new {
                                    status = media.CaptionExt??new Caption[]{},
                                    flag = new
                                    {
                                        is_comment
                                    },
                                    is_web = 1,
                                    boards=media.BoardIds != null && media.BoardIds.Length > 0 ? string.Join(",", media.BoardIds) : string.Empty
                                },
                            }
                        }
                    };

                    //hearder thay vì session-id , các anh truyền vào user-id của page hoặc user nhé
                    sessionId = media.UserId.ToString();

                    if (action == Media.Action.add)
                    {
                        var errorCodeReturn = await Media.Add(media.ObjectId.ToString(), sessionId, CardType.Article, post);
                        if(errorCodeReturn == ErrorCodes.DataExist)
                        {
                            action = Media.Action.update;
                        }
                        else
                        {
                            return errorCodeReturn == ErrorCodes.Success ? true : false;
                        }
                    }

                    if (action == Media.Action.update)
                    {
                        return await Media.Update(media.ObjectId.ToString(), sessionId, CardType.Article, post) == ErrorCodes.Success ? true : false;
                    }

                    if (action == Media.Action.deletebymedia)
                    {
                        return await Media.Delete(media.ObjectId.ToString(), sessionId, CardType.Article) == ErrorCodes.Success ? true : false;
                    }
                    //action ko hỗ trợ
                    return false;
                }

                return false;
            }
            catch (Exception ex)
            {
                Logger.Error(ex, "PushBlogToKingHub: " + ex.Message);
                return false;
            }
        }
    }
}
