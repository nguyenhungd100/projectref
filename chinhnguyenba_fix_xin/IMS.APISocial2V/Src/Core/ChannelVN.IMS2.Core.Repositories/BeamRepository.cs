﻿using ChannelVN.IMS2.Core.Entities;
using ChannelVN.IMS2.Core.Entities.Beam;
using ChannelVN.IMS2.Core.Entities.Distribution;
using ChannelVN.IMS2.Core.Entities.Media;
using ChannelVN.IMS2.Core.Entities.Security;
using ChannelVN.IMS2.Core.Models.CacheStore;
using ChannelVN.IMS2.Core.Models.CacheStore.Beams;
using ChannelVN.IMS2.Core.Models.DataStore.Beams;
using ChannelVN.IMS2.Core.Models.ExternalAPI;
using ChannelVN.IMS2.Core.Models.SearchStore;
using ChannelVN.IMS2.Core.Models.SearchStore.Beams;
using ChannelVN.IMS2.Core.Repositories.Constants;
using ChannelVN.IMS2.Core.Repositories.Functions;
using ChannelVN.IMS2.Foundation.Common;
using ChannelVN.IMS2.Foundation.Common.Configuration;
using ChannelVN.IMS2.Foundation.Data;
using ChannelVN.IMS2.Foundation.Data.Factories;
using ChannelVN.IMS2.Foundation.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using static ChannelVN.IMS2.Core.Entities.ErrorCode.ErrorMapping;

namespace ChannelVN.IMS2.Core.Repositories
{
    public class BeamRepository : Repository
    {
        public static async Task<ErrorCodes> AddAsync(BeamSearch data, string clientIP)
        {
            var returnValue = ErrorCodes.AddError;
            try
            {
                returnValue = await StoreFactory.Create<BeamCacheStore>().AddAsync(data);
                if (returnValue == ErrorCodes.Success)
                {
                    var res = await StoreFactory.Create<BeamSearchStore>().AddAsync(data);
                    if (!res)
                    {
                        //Push queue
                        await Function.AddToQueue(ActionName.AddBeamES, TopicName.ES_BEAM, data.Id);
                    }
                    res = await StoreFactory.Create<BeamDataStore>().AddAsync(data);
                    if (!res)
                    {
                        //Push queue
                        await Function.AddToQueue(ActionName.AddBeamSQL, TopicName.SQL_BEAM, data.Id);
                    }

                    //if (data.Status == (int)NewsStatus.Archive && AppSettings.Current.Distribution.Automation.Enabled)
                    //{
                    //    await Function.AddToQueue(ActionName.AutoShareBeam, TopicName.AUTOSHAREBEAM, new QueueData<long, string, long[], object>
                    //    {
                    //        Data1 = data.Id,
                    //        Data2 = clientIP
                    //    });
                    //    //await AutoShare(data);
                    //}
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
                returnValue = ErrorCodes.Exception;
            }
            return returnValue;
        }

        public static async Task<ErrorCodes> UpdateAsync(Beam data, ArticleInBeam[] articleInBeams, string clientIP, long officerId, string sessionId)
        {
            var returnValue = ErrorCodes.UpdateError;
            try
            {
                returnValue = await StoreFactory.Create<BeamCacheStore>().UpdateV2Async(data, articleInBeams, officerId, new Action<int>(async (oldStatus) => {
                    if (AppSettings.Current.Distribution.Automation.Enabled)
                    {
                        if (oldStatus == (int)NewsStatus.Published)
                        {
                            var action = Media.Action.update;
                            await Function.AddToQueue(ActionName.AutoShareBeam, TopicName.AUTOSHAREBEAM, new QueueData<long, string, string, Media.Action>
                            {
                                Data1 = data.Id,
                                Data2 = clientIP,
                                Data3 = sessionId,
                                Data4 = action
                            });

                            await Function.AddToQueue(ActionName.PushChangeNotify, TopicName.PUSHBOARDTOCHANNEL, data.Id);
                        }
                    }
                }));
                if (returnValue == ErrorCodes.Success)
                {
                    await Function.AddToQueue(ActionName.UpdateBeamES, TopicName.ES_BEAM, data.Id);
                    await Function.AddToQueue(ActionName.UpdateBeamSQL, TopicName.SQL_BEAM, data.Id);                    
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
                returnValue = ErrorCodes.Exception;
            }
            return returnValue;
        }


        public static async Task<PagingDataResult<NewsAll>> SearchNewsAsync(SearchNews data, long userId)
        {
            try
            {
                var dataReturn = new PagingDataResult<NewsAll>();
                if (string.IsNullOrEmpty(data.Keyword) && data.OrderBy != SearchNewsOrderBy.DescDistributionDate
                    && data.OrderBy != SearchNewsOrderBy.AscDistributionDate && data.OrderBy != SearchNewsOrderBy.DescModifiedDate && data.OrderBy != SearchNewsOrderBy.AscModifiedDate)
                {
                    dataReturn = await StoreFactory.Create<NewsCacheStore>().SearchNewsAsync(data, new Action<string>(async (tempKey) =>
                    {
                        await Function.AddToQueue(ActionName.SetExpire, TopicName.DELETE_INDEX_TEMP, tempKey);
                    }));
                }
                else
                {
                    var dataSearch = await StoreFactory.Create<NewsSearchStore>().SearchNewsAsync(data, userId);
                    if (dataSearch?.Data?.Count() > 0)
                    {
                        var listData = await StoreFactory.Create<NewsCacheStore>().GetListNewsAsync(dataSearch.Data.Select(p => p.Id.ToString()).ToList());
                        dataReturn.Total = dataSearch.Total;
                        dataReturn.Data = listData;
                    }
                }
                return dataReturn;
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
                return null;
            }
        }

        public static Task<PagingDataResult<BeamSearch>> GetListRelationAsync(string beamIds, int? pageIndex, int? pageSize)
        {
            return StoreFactory.Create<BeamSearchStore>().GetListRelationAsync(beamIds, pageIndex, pageSize);
        }

        public static Task<PagingDataResult<NewsAll>> ArticleInBeamAsync(long beamId, int? pageIndex, int? pageSize)
        {
            return StoreFactory.Create<BeamCacheStore>().ArticleInBeamAsync(beamId, pageIndex ?? 0, pageSize ?? 0);//, pageIndex, pageSize);
        }

        public static Task<long> GetCountByStatusAsync(int status, long officialId, long userId)
        {
            return StoreFactory.Create<BeamSearchStore>().GetCountByStatusAsync(status, officialId, userId);
        }

        public static Task<PagingDataResult<BeamSearch>> GetListDistributionAsync(SearchNews searchEntity, long userId)
        {
            return StoreFactory.Create<BeamSearchStore>().GetListDistributionAsync(searchEntity, userId);
        }


        public static async Task<ErrorCodes> UpdateStatusV2Async(long id, NewsStatus status, long userId, string clientIP, string sessionId)
        {
            var returnCode = ErrorCodes.BusinessError;
            try
            {
                returnCode = await StoreFactory.Create<BeamCacheStore>().UpdateStatusV2Async(id, status, userId, new Action<BeamSearch, int>(async (data, oldStatus) =>
                {
                    //auto phan phoi khi action publish         
                    if (AppSettings.Current.Distribution.Automation.Enabled)
                    {
                        if ((status == NewsStatus.Published)
                         || (data.Status == (int)NewsStatus.Remove && oldStatus == (int)NewsStatus.Published)
                         || (data.Status == (int)NewsStatus.Draft && oldStatus == (int)NewsStatus.Published))
                        {
                            var action = Media.Action.add;
                            if(status == NewsStatus.Published && oldStatus == (int)NewsStatus.Published)
                            {
                                action = Media.Action.update;
                            }
                            if (status == NewsStatus.Remove)
                            {
                                action = Media.Action.deletebymedia;
                            }
                            await Function.AddToQueue(ActionName.AutoShareBeam, TopicName.AUTOSHAREBEAM, new QueueData<long, string, string, Media.Action>
                            {
                                Data1 = data.Id,
                                Data2 = clientIP,
                                Data3 = sessionId,
                                Data4 = action
                            });
                        }
                    }
                }));

                if (returnCode == ErrorCodes.Success)
                {
                    await Function.AddToQueue(ActionName.UpdateStatusBeamES, TopicName.ES_BEAM, id);
                    await Function.AddToQueue(ActionName.UpdateStatusBeamSQL, TopicName.SQL_BEAM, id);
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
                returnCode = ErrorCodes.Exception;
            }
            return returnCode;
        }

        public static async Task<bool> AutoShare(BeamSearch data, string clientIP, string sessionId, Media.Action action)
        {
            var result = false;
            try
            {
                var listItem = Json.Parse<PublishData>(data.PublishData);
                var itemMediaUnit = new List<ItemMediaUnit>();
                if (listItem != null && listItem.Items?.Count > 0)
                {
                    foreach (var item in listItem.Items)
                    {
                        itemMediaUnit.Add(new ItemMediaUnit
                        {
                            Id = item.Id,
                            Title = item.Title,
                            DataType = DataType.InstantView,
                            Image = item.Image,
                            Link = item.Link,
                            ContentType = item.ContentType ?? item.Content_Type,
                            //Body = item.Body,//bổ sung,
                            CreatedDate = data.ArticleInBeam?.Where(p => p.ArticleId.ToString().Equals(item.Id))?.FirstOrDefault()?.PublishedDate ?? DateTime.Now
                        });
                    }
                    var pageAccount = await SecurityRepository.GetByIdAsync(long.Parse(data.NewsInAccount?.LastOrDefault()?.AccountId.ToString()));
                    var payloadDataShare = new PayloadDataShare
                    {
                        DistributionId = AppSettings.Current.Distribution.Automation.DistributorId,
                        TemplateId = 0,
                        Title = listItem.Title,
                        Type = NewsType.Video,
                        MetaDataShared = new List<MetaDataShared>
                        {
                            new MetaDataShared
                            {
                                ObjectId = data.Id,
                                Title = listItem.Caption,
                                Name = data.Name,
                                ZoneId = 0,
                                DistributionDate = data.DistributionDate,
                                CardType = (int)CardType.ListNews,
                                Frameid = 0,
                                Tags = data.Tags,
                                UserId = pageAccount?.Id??0,
                                UserName = pageAccount?.UserName,
                                FullName = pageAccount?.FullName,
                                AuthorAvatar = pageAccount?.Avatar,
                                IsDeleted = data.Status == (int)NewsStatus.Remove||data.Status == (int)NewsStatus.Draft ? 1 : (data.Status == (int)NewsStatus.Archive ? 2 : 0),
                                ItemMediaUnit = itemMediaUnit,
                                BoardIds = null,
                                CaptionExt = listItem.CaptionExt,
                                CommentMode = data.CommentMode
                            }
                        }
                    };

                    var distribution = await DistributionRepository.GetByIdAsync(payloadDataShare.DistributionId);
                    var newsDistribution = data.NewsDistribution?.Where(p => p.DistributorId == payloadDataShare.DistributionId).FirstOrDefault();
                    var itemStreamDistribution = new ItemStreamDistribution()
                    {
                        Id = newsDistribution != null ? newsDistribution.ItemStreamId : Generator.ItemStreamDistributionId(),
                        PublishStatus = data.Status == (int)NewsStatus.Published ? (int)DistributionPublishStatus.Accept : (int)DistributionPublishStatus.Reject,
                        CreatedDate = DateTime.Now,
                        ItemStreamTempId = payloadDataShare.TemplateId,
                        Title = payloadDataShare.Title,
                        MetaData = Json.Stringify(payloadDataShare.MetaDataShared),
                        Type = payloadDataShare.Type
                    };
                    result = await DistributionRepository.ShareBeamAsync(itemStreamDistribution, distribution, payloadDataShare, data, clientIP, sessionId, action);
                    if (result)
                    {
                        //insert log
                        await Function.AddToQueue(ActionName.InsertLog, TopicName.LOGACTION, new LogActionEntity
                        {
                            ObjectId = data.Id.ToString(),
                            Account = data.ModifiedBy,
                            CreatedDate = DateTime.SpecifyKind(DateTime.Now, DateTimeKind.Unspecified),
                            Ip = clientIP,
                            Message = "Success.",
                            Title = data.Name,
                            SourceId = data.ModifiedBy,
                            ActionTypeDetail = (int)ActionType.Share,
                            ActionText = ActionType.Share.ToString(),
                            Type = (int)LogContentType.Beam
                        });
                    }
                }
                else
                {
                    result = true;
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
                result = false;
            }
            return result;
        }
        public static Task<Beam> GetByIdAsync(long id)
        {
            return StoreFactory.Create<BeamCacheStore>().GetByIdAsync(id);
        }

        public static async Task<PagingDataResult<BeamSearchReturn>> SearchAsync(SearchNews data, long userId, bool isGetDistribution)
        {
            try
            {
                var dataReturn = new PagingDataResult<BeamSearchReturn>();
                if (isGetDistribution == false && string.IsNullOrEmpty(data.Keyword) && data.OrderBy != SearchNewsOrderBy.DescDistributionDate
                    && data.OrderBy != SearchNewsOrderBy.AscDistributionDate && data.OrderBy != SearchNewsOrderBy.DescModifiedDate && data.OrderBy != SearchNewsOrderBy.AscModifiedDate)
                {
                    dataReturn = await StoreFactory.Create<BeamCacheStore>().SearchAsync(data, userId, new Action<string>(async (tempKey) =>
                    {
                        await Function.AddToQueue(ActionName.SetExpire, TopicName.DELETE_INDEX_TEMP, tempKey);
                    }));
                }
                else
                {
                    var dataSearch = await StoreFactory.Create<BeamSearchStore>().SearchAsync(data, userId, isGetDistribution);
                    if (dataSearch?.Data?.Count() > 0)
                    {
                        var listData = await GetListByIdsAsync(dataSearch.Data.Select(p => p.Id.ToString()).ToList());
                        if (listData != null)
                        {
                            dataSearch.Data = listData.Select(p => Mapper<BeamSearch>.Map(p, dataSearch.Data.Where(c => c.Id == p.Id).FirstOrDefault())).ToList();
                        }
                        dataReturn.Total = dataSearch.Total;
                        dataReturn.Data = dataSearch.Data.Select(p => Mapper<BeamSearchReturn>.Map(p, new BeamSearchReturn())).ToList();
                    }
                }
                return dataReturn;
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
                return null;
            }
        }

        public static async Task<PagingDataResult<BeamSearch>> GetListApproveAsync(SearchNews data, long accountId)
        {
            try
            {
                var dataReturn = new PagingDataResult<BeamSearch>();
                if (string.IsNullOrEmpty(data.Keyword) && data.OrderBy != SearchNewsOrderBy.DescDistributionDate
                    && data.OrderBy != SearchNewsOrderBy.AscDistributionDate && data.OrderBy != SearchNewsOrderBy.DescModifiedDate && data.OrderBy != SearchNewsOrderBy.AscModifiedDate)
                {
                    dataReturn = await StoreFactory.Create<BeamCacheStore>().GetListApproveAsync(data, accountId, new Action<string>(async (tempKey) =>
                    {
                        await Function.AddToQueue(ActionName.SetExpire, TopicName.DELETE_INDEX_TEMP, tempKey);
                    }));
                }
                else
                {
                    dataReturn = await StoreFactory.Create<BeamCacheStore>().GetListApproveAsync(data, accountId, new Action<string>(async (tempKey) =>
                    {
                        await Function.AddToQueue(ActionName.SetExpire, TopicName.DELETE_INDEX_TEMP, tempKey);
                    }));
                }
                return dataReturn;
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
                return null;
            }
        }

        public static Task<List<Beam>> GetListByIdsAsync(List<string> ids)
        {
            return StoreFactory.Create<BeamCacheStore>().GetListByIdsAsync(ids);
        }
        public static Task<List<BeamSearch>> GetListBeamSearchByIdsAsync(List<string> ids)
        {
            return StoreFactory.Create<BeamCacheStore>().GetListBeamSearchByIdsAsync(ids);
        }

        public static Task<BeamSearch> GetBeamSearchByIdAsync(long id)
        {
            return StoreFactory.Create<BeamCacheStore>().GetBeamFullInfoAsync(id);
        }

        public static async Task<bool> PushToKingHub(string sessionId, PayloadDataShare playloadDataShare, Media.Action action)
        {
            try
            {
                if (playloadDataShare != null && playloadDataShare.MetaDataShared?.Count > 0)
                {
                    var media = playloadDataShare.MetaDataShared.FirstOrDefault();

                    var is_comment = AppSettings.Current.ChannelConfiguration.ApiCommentSetting.Mode.PostCheck;
                    if (media.CommentMode == CommentMode.Disable)
                    {
                        is_comment = AppSettings.Current.ChannelConfiguration.ApiCommentSetting.Mode.Disable;
                    }
                    else
                    {
                        if (media.CommentMode == CommentMode.PreCheck) is_comment = AppSettings.Current.ChannelConfiguration.ApiCommentSetting.Mode.PreCheck;
                    }
;

                    var post = new
                    {
                        posts = new List<object>{
                            new {
                                media_id = media.ObjectId.ToString(),
                                title = media.Title??string.Empty,
                                mediaunit_name = media.Name??string.Empty,
                                card_type = (int) CardType.ListNews,//beam
                                preview = new{ },
                                media_unit_desc = media.ItemMediaUnit?.Select(s=> new {
                                   id = s.Id??string.Empty,
                                    title = s.Title??string.Empty,
                                    data_type = s.DataType!=null ? (int)s.DataType :(int) DataType.InstantView,
                                    image = new { thumb = s.Image?.Thumb??string.Empty, width = s.Image?.Width == 0 || s.Image?.Width == null ? 600 : s.Image?.Width,height = s.Image?.Height == 0 || s.Image?.Height == null ? 300 : s.Image?.Height, content_type = (s.ContentType ==ContentType.Photo || s.ContentType ==ContentType.Image) ? (int)s.ContentType : (int)ContentType.Photo},
                                    link = s.Link??string.Empty,
                                    content_type = s.ContentType == ContentType.News ? (int)s.ContentType: (int)ContentType.Linkshare,
                                    created_at = (s.CreatedDate??DateTime.Now).ToString("yyyy-MM-ddTHH:mm:ssZ")
                                }).ToList(),
                                channel_id = media.ChannelId??string.Empty,
                                frame_id = media.Frameid,
                                tags = media.Tags??string.Empty,
                                user_id = media.UserId.ToString(),
                                user_name = media.UserName??string.Empty,
                                full_name = media.FullName??string.Empty,
                                avatar = media.AuthorAvatar??string.Empty,
                                cat_id = media.ZoneId?.ToString()??string.Empty,
                                create_time = Utility.ConvertToTimestamp(media.DistributionDate??DateTime.Now),
                                cat_name = media.ZoneName??string.Empty,
                                extension = new {
                                    status = media.CaptionExt??new Caption[]{},
                                    flag = new
                                    {
                                        is_comment
                                    },
                                    is_web = 1,
                                    boards=media.BoardIds != null && media.BoardIds.Length > 0 ? string.Join(",", media.BoardIds) : string.Empty
                                },
                            }
                        }
                    };

                    //hearder thay vì session-id , các anh truyền vào user-id của page hoặc user nhé
                    sessionId = media.UserId.ToString();

                    if (action == Media.Action.add)
                    {
                        var errorCodeReturn = await Media.Add(media.ObjectId.ToString(), sessionId, CardType.ListNews, post);
                        if (errorCodeReturn == ErrorCodes.DataExist)
                        {
                            action = Media.Action.update;
                        }
                        else
                        {
                            return errorCodeReturn == ErrorCodes.Success ? true : false;
                        }
                    }

                    if (action == Media.Action.update)
                    {
                        return await Media.Update(media.ObjectId.ToString(), sessionId, CardType.ListNews, post) == ErrorCodes.Success ? true : false;
                    }

                    if (action == Media.Action.deletebymedia)
                    {
                        return await Media.Delete(media.ObjectId.ToString(), sessionId, CardType.ListNews) == ErrorCodes.Success ? true : false;
                    }
                    //action ko hỗ trợ
                    return false;
                }

                return false;
            }
            catch (Exception ex)
            {
                Logger.Error(ex, "PushBlogToKingHub: " + ex.Message);
                return false;
            }
        }
    }
}
