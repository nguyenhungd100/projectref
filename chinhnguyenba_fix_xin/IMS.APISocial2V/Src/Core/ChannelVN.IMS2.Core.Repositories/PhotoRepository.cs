﻿using ChannelVN.IMS2.Core.Entities;
using ChannelVN.IMS2.Core.Entities.Album;
using ChannelVN.IMS2.Core.Entities.Distribution;
using ChannelVN.IMS2.Core.Entities.Media;
using ChannelVN.IMS2.Core.Entities.Photo;
using ChannelVN.IMS2.Core.Entities.Security;
using ChannelVN.IMS2.Core.Models.CacheStore.Album;
using ChannelVN.IMS2.Core.Models.CacheStore.Photo;
using ChannelVN.IMS2.Core.Models.DataStore.Photo;
using ChannelVN.IMS2.Core.Models.ExternalAPI;
using ChannelVN.IMS2.Core.Models.SearchStore.Photo;
using ChannelVN.IMS2.Core.Repositories.Constants;
using ChannelVN.IMS2.Core.Repositories.Functions;
using ChannelVN.IMS2.Foundation.Common;
using ChannelVN.IMS2.Foundation.Common.Configuration;
using ChannelVN.IMS2.Foundation.Data;
using ChannelVN.IMS2.Foundation.Data.Factories;
using ChannelVN.IMS2.Foundation.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using static ChannelVN.IMS2.Core.Entities.ErrorCode.ErrorMapping;

namespace ChannelVN.IMS2.Core.Repositories
{
    public class PhotoRepository : Repository
    {
        public static async Task<ErrorCodes> AddAsync(PhotoSearch data, string clientIP)
        {
            try
            {
                var returnValue = await StoreFactory.Create<PhotoCacheStore>().AddAsync(Mapper<PhotoUnit>.Map(data, new PhotoUnit()), data);
                if (returnValue == ErrorCodes.Success)
                {
                    var res = await StoreFactory.Create<PhotoSearchStore>().AddAsync(data);
                    if (!res)
                    {
                        //push queue
                        await Function.AddToQueue(ActionName.AddPhotoES, TopicName.ES_PHOTO, data.Id);
                    }

                    res = await StoreFactory.Create<PhotoDataStore>().AddAsync(data);
                    if (!res)
                    {
                        //push queue
                        await Function.AddToQueue(ActionName.AddPhotoSQL, TopicName.SQL_PHOTO, data.Id);
                    }

                    //auto phan phoi khi action Archive                    
                    //if (data.Status == (int)NewsStatus.Archive && AppSettings.Current.Distribution.Automation.Enabled)
                    //{
                    //    await Function.AddToQueue(ActionName.AutoSharePhoto, TopicName.AUTOSHAREPHOTO, new QueueData<long, string, long[], object>
                    //    {
                    //        Data1 = data.Id,
                    //        Data2 = clientIP
                    //    });
                    //    //AutoShare(data, clientIP);
                    //}
                }
                return returnValue;
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
                return ErrorCodes.Exception;
            }
        }

        public static async Task<ErrorCodes> UpdateAsync(PhotoUnit data, long officerId, string clientIP, string sessionId)
        {
            var returnV1 = ErrorCodes.BusinessError;
            try
            {
                returnV1 = await StoreFactory.Create<PhotoCacheStore>().UpdateV2Async(data, officerId, new Action<int>(async (oldStatus) =>
                {
                    if (AppSettings.Current.Distribution.Automation.Enabled)
                    {
                        if (oldStatus == (int)NewsStatus.Published)
                        {
                            var action = Media.Action.update;
                            await Function.AddToQueue(ActionName.AutoSharePhoto, TopicName.AUTOSHAREPHOTO, new QueueData<long, string, string, Media.Action>
                            {
                                Data1 = data.Id,
                                Data2 = clientIP,
                                Data3 = sessionId,
                                Data4 = action
                            });
                            await Function.AddToQueue(ActionName.PushChangeNotify, TopicName.PUSHBOARDTOCHANNEL, data.Id);
                        }
                    }
                }));
                if (returnV1 == ErrorCodes.Success)
                {
                    //auto phan phoi khi action publish    
                    await Function.AddToQueue(ActionName.UpdatePhotoES, TopicName.ES_PHOTO, data.Id);
                    await Function.AddToQueue(ActionName.UpdatePhotoSQL, TopicName.SQL_PHOTO, data.Id);
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
                return ErrorCodes.Exception;
            }
            return returnV1;
        }

        public static async Task<bool> AutoShare(PhotoSearch data, string clientIP, string sessionId, Media.Action action)
        {
            var returnValue = false;
            try
            {
                var listItem = Json.Parse<PublishData>(data.PublishData);
                var itemsPhoto = new List<ItemMediaUnit>();
                if (listItem != null && listItem.Items?.Count > 0)
                {
                    foreach (var item in listItem.Items)
                    {
                        itemsPhoto.Add(new ItemMediaUnit
                        {
                            Id = data.Id.ToString(),
                            Title = item.Title,
                            ContentType = item.ContentType ?? item.Content_Type,
                            Link = item.Link,
                            Thumb = item.Thumb,
                            Width = item.Width,
                            Height = item.Height,
                            IsHeartDrop = item.IsHeartDrop,
                            CreatedDate = data.DistributionDate ?? DateTime.Now
                        });
                    }

                    var pageAccount = await SecurityRepository.GetByIdAsync(long.Parse(data.NewsInAccount?.LastOrDefault()?.AccountId.ToString()));
                    var payloadDataShare = new PayloadDataShare
                    {
                        DistributionId = AppSettings.Current.Distribution.Automation.DistributorId,
                        TemplateId = 0,
                        Title = listItem.Caption,
                        Type = NewsType.Photo,
                        MetaDataShared = new List<MetaDataShared>
                        {
                            new MetaDataShared
                            {
                                ObjectId = data.Id,
                                Title = listItem.Caption,
                                ZoneId = 0,
                                DistributionDate = data.DistributionDate,
                                CardType = (int)CardType.Photo,
                                Tags = data.Tags,
                                UserId = pageAccount?.Id??0,
                                UserName = pageAccount?.UserName,
                                FullName = pageAccount?.FullName,
                                AuthorAvatar = pageAccount?.Avatar,
                                IsDeleted = data.Status == (int)NewsStatus.Remove || data.Status == (int)NewsStatus.Draft ? 1 : (data.Status == (int)NewsStatus.Archive ? 2 : 0),
                                ItemMediaUnit = itemsPhoto,
                                BoardIds = null,
                                CaptionExt = listItem.CaptionExt,
                                CommentMode = data.CommentMode,
                                LabelType = (LabelType?)pageAccount.LabelMode
                            }
                        }
                    };

                    var distribution = await DistributionRepository.GetByIdAsync(payloadDataShare.DistributionId);

                    if (distribution != null)
                    {
                        //check itemStream => update
                        var itemStreamId = data.NewsDistribution?.Where(s => s.DistributorId == payloadDataShare.DistributionId).FirstOrDefault();

                        var itemStreamDistribution = new ItemStreamDistribution()
                        {
                            Id = itemStreamId?.ItemStreamId ?? Generator.ItemStreamDistributionId(),
                            PublishStatus = (int)DistributionPublishStatus.Accept,
                            CreatedDate = DateTime.Now,
                            ItemStreamTempId = payloadDataShare.TemplateId,
                            Title = payloadDataShare.Title,
                            MetaData = Json.Stringify(payloadDataShare.MetaDataShared),
                            Type = payloadDataShare.Type
                        };

                        returnValue = await DistributionRepository.SharePhotoAsync(itemStreamDistribution, distribution, payloadDataShare, clientIP, sessionId, action);
                        if (returnValue)
                        {
                            //insert log
                            await Function.AddToQueue(ActionName.InsertLog, TopicName.LOGACTION, new LogActionEntity
                            {
                                ObjectId = data.Id.ToString(),
                                Account = data.ModifiedBy,
                                CreatedDate = DateTime.SpecifyKind(DateTime.Now, DateTimeKind.Unspecified),
                                Ip = clientIP,
                                Message = "Success.",
                                Title = data.Caption,
                                SourceId = data.ModifiedBy,
                                ActionTypeDetail = (int)ActionType.Share,
                                ActionText = ActionType.Share.ToString(),
                                Type = (int)LogContentType.Photo
                            });
                        }
                    }
                    else
                    {
                        returnValue = false;
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
            }
            return returnValue;
        }

        public static async Task<bool> AutoShare2(PhotoSearch data, string clientIP, string sessionId, Media.Action action)// push old data
        {
            var returnValue = false;
            try
            {
                var listItem = Json.Parse<PublishData>(data.PublishData);
                var itemsPhoto = new List<ItemMediaUnit>();
                if (listItem != null && listItem.Items?.Count > 0)
                {
                    foreach (var item in listItem.Items)
                    {
                        itemsPhoto.Add(new ItemMediaUnit
                        {
                            Id = data.Id.ToString(),
                            Title = item.Title,
                            ContentType = item.ContentType ?? item.Content_Type,
                            Link = item.Link,
                            Thumb = item.Thumb,
                            Width = item.Width,
                            Height = item.Height,
                            IsHeartDrop = true,
                            CreatedDate = data.DistributionDate ?? DateTime.Now
                        });
                    }

                    var pageAccount = await SecurityRepository.GetByIdAsync(long.Parse(data.NewsInAccount?.LastOrDefault()?.AccountId.ToString()));
                    var payloadDataShare = new PayloadDataShare
                    {
                        DistributionId = AppSettings.Current.Distribution.Automation.DistributorId,
                        TemplateId = 0,
                        Title = listItem.Caption,
                        Type = NewsType.Photo,
                        MetaDataShared = new List<MetaDataShared>
                        {
                            new MetaDataShared
                            {
                                ObjectId = data.Id,
                                Title = listItem.Caption,
                                ZoneId = 0,
                                DistributionDate = data.DistributionDate,
                                CardType = (int)CardType.Photo,
                                Tags = data.Tags,
                                UserId = pageAccount?.Id??0,
                                UserName = pageAccount?.UserName,
                                FullName = pageAccount?.FullName,
                                AuthorAvatar = pageAccount?.Avatar,
                                IsDeleted = data.Status == (int)NewsStatus.Remove || data.Status == (int)NewsStatus.Draft ? 1 : (data.Status == (int)NewsStatus.Archive ? 2 : 0),
                                ItemMediaUnit = itemsPhoto,
                                BoardIds = null,
                                CaptionExt = listItem.CaptionExt,
                                CommentMode = data.CommentMode,
                                LabelType = (LabelType?)pageAccount.LabelMode
                            }
                        }
                    };

                    var distribution = await DistributionRepository.GetByIdAsync(payloadDataShare.DistributionId);

                    if (distribution != null)
                    {
                        //check itemStream => update
                        var itemStreamId = data.NewsDistribution?.Where(s => s.DistributorId == payloadDataShare.DistributionId).FirstOrDefault();

                        var itemStreamDistribution = new ItemStreamDistribution()
                        {
                            Id = itemStreamId?.ItemStreamId ?? Generator.ItemStreamDistributionId(),
                            PublishStatus = (int)DistributionPublishStatus.Accept,
                            CreatedDate = DateTime.Now,
                            ItemStreamTempId = payloadDataShare.TemplateId,
                            Title = payloadDataShare.Title,
                            MetaData = Json.Stringify(payloadDataShare.MetaDataShared),
                            Type = payloadDataShare.Type
                        };

                        returnValue = await DistributionRepository.SharePhotoAsync(itemStreamDistribution, distribution, payloadDataShare, clientIP, sessionId, action);
                        if (returnValue)
                        {
                            //insert log
                            await Function.AddToQueue(ActionName.InsertLog, TopicName.LOGACTION, new LogActionEntity
                            {
                                ObjectId = data.Id.ToString(),
                                Account = data.ModifiedBy,
                                CreatedDate = DateTime.SpecifyKind(DateTime.Now, DateTimeKind.Unspecified),
                                Ip = clientIP,
                                Message = "Success.",
                                Title = data.Caption,
                                SourceId = data.ModifiedBy,
                                ActionTypeDetail = (int)ActionType.Share,
                                ActionText = ActionType.Share.ToString(),
                                Type = (int)LogContentType.Photo
                            });
                        }
                    }
                    else
                    {
                        returnValue = false;
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
            }
            return returnValue;
        }
        public static async Task<ErrorCodes> UpdateStatusV2Async(long id, NewsStatus status, long userId, string clientIP, string sessionId)
        {
            var returnCode = ErrorCodes.BusinessError;
            try
            {
                returnCode = await StoreFactory.Create<PhotoCacheStore>().UpdateStatusV2Async(id, status, userId, new Action<PhotoSearch, int>(async (data, oldStatus) =>
                {
                    //auto phan phoi khi action publish    
                    if (AppSettings.Current.Distribution.Automation.Enabled)
                    {
                        if (status == NewsStatus.Published
                            || (data.Status == (int)NewsStatus.Remove && oldStatus == (int)NewsStatus.Published)
                            || (data.Status == (int)NewsStatus.Draft && oldStatus == (int)NewsStatus.Published))
                        {
                            var action = Media.Action.add;
                            if (oldStatus == (int)NewsStatus.Published)
                            {
                                action = Media.Action.update;
                            }
                            if (status == NewsStatus.Remove)
                            {
                                action = Media.Action.deletebymedia;
                            }
                            //await AutoShare(data, clientIP);
                            await Function.AddToQueue(ActionName.AutoSharePhoto, TopicName.AUTOSHAREPHOTO, new QueueData<long, string, string, Media.Action>
                            {
                                Data1 = data.Id,
                                Data2 = clientIP,
                                Data3 = sessionId,
                                Data4 = action
                            });
                        }
                    }
                }));

                if (returnCode == ErrorCodes.Success)
                {
                    await Function.AddToQueue(ActionName.UpdateStatusPhotoES, TopicName.ES_PHOTO, id);
                    await Function.AddToQueue(ActionName.UpdateStatusPhotoSQL, TopicName.SQL_PHOTO, id);
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
                returnCode = ErrorCodes.Exception;
            }
            return returnCode;
        }

        public static Task<PhotoUnit> GetByIdAsync(long id)
        {
            return StoreFactory.Create<PhotoCacheStore>().GetByIdAsync(id);
        }

        public static Task<PhotoSearch> GetDetailByIdAsync(long id)
        {
            return StoreFactory.Create<PhotoCacheStore>().GetPhotoFullInfoAsync(id);
        }

        public static Task<List<PhotoUnit>> GetListByIdsAsync(List<string> ids)
        {
            return StoreFactory.Create<PhotoCacheStore>().GetListByIdsAsync(ids);
        }

        public static Task<List<PhotoSearch>> GetListPhotoSearchByIdsAsync(List<string> ids)
        {
            return StoreFactory.Create<PhotoCacheStore>().GetListPhotoSearchByIdsAsync(ids);
        }

        public static async Task<PagingDataResult<PhotoSearchReturn>> SearchAsync(SearchNews search, long accountId)
        {
            try
            {
                var dataReturn = new PagingDataResult<PhotoSearchReturn>();
                if (string.IsNullOrEmpty(search.Keyword) && search.OrderBy != SearchNewsOrderBy.DescDistributionDate
                    && search.OrderBy != SearchNewsOrderBy.AscDistributionDate && search.OrderBy != SearchNewsOrderBy.DescModifiedDate && search.OrderBy != SearchNewsOrderBy.AscModifiedDate)
                {
                    dataReturn = await StoreFactory.Create<PhotoCacheStore>().SearchAsync(search, accountId, new Action<string>(async (tempKey) =>
                    {
                        await Function.AddToQueue(ActionName.SetExpire, TopicName.DELETE_INDEX_TEMP, tempKey);
                    }));
                }
                else
                {
                    var dataSearch = await StoreFactory.Create<PhotoSearchStore>().SearchAsync(search, accountId);
                    if (dataSearch?.Data?.Count > 0)
                    {
                        dataSearch.Data = await GetListPhotoSearchByIdsAsync(dataSearch.Data.Select(p => p.Id.ToString()).ToList());
                        dataReturn.Total = dataSearch.Total;
                        dataReturn.Data = dataSearch.Data.Select(p => Mapper<PhotoSearchReturn>.Map(p, new PhotoSearchReturn())).ToList();
                    }
                }
                return dataReturn;
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
                return null;
            }
        }

        public static async Task<PagingDataResult<PhotoUnit>> GetListApproveAsync(SearchNews data, long accountId)
        {
            try
            {
                var dataReturn = new PagingDataResult<PhotoUnit>();
                if (string.IsNullOrEmpty(data.Keyword) && data.OrderBy != SearchNewsOrderBy.DescDistributionDate
                    && data.OrderBy != SearchNewsOrderBy.AscDistributionDate && data.OrderBy != SearchNewsOrderBy.DescModifiedDate && data.OrderBy != SearchNewsOrderBy.AscModifiedDate)
                {
                    dataReturn = await StoreFactory.Create<PhotoCacheStore>().GetListApproveAsync(data, accountId, new Action<string>(async (tempKey) =>
                    {
                        await Function.AddToQueue(ActionName.SetExpire, TopicName.DELETE_INDEX_TEMP, tempKey);
                    }));
                }
                else
                {
                    dataReturn = await StoreFactory.Create<PhotoCacheStore>().GetListApproveAsync(data, accountId, new Action<string>(async (tempKey) =>
                    {
                        await Function.AddToQueue(ActionName.SetExpire, TopicName.DELETE_INDEX_TEMP, tempKey);
                    }));
                }
                return dataReturn;
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
                return null;
            }
        }

        public static Task<PagingDataResult<PhotoSearch>> ListDistributionAsync(SearchNews search, long accountId)
        {
            return StoreFactory.Create<PhotoSearchStore>().ListDistributionAsync(search, accountId);
        }
        public static async Task<bool> AddToAlbumAsync(long photoId, List<Album> dataDb)
        {
            var returnValue = false;
            try
            {
                returnValue = await StoreFactory.Create<AlbumCacheStore>().AddPhotoAsync(photoId, dataDb);
                if (returnValue)
                {
                    await Function.AddToQueue(ActionName.AddPhotoToAlbumSQL, TopicName.SQL_PHOTO, new QueueData<long, List<Album>, object, object>
                    {
                        Data1 = photoId,
                        Data2 = dataDb
                    });

                    if (dataDb!=null)
                    {
                        foreach(var album in dataDb)
                        {
                            await Function.AddToQueue(ActionName.AutoShareAlbum, TopicName.AUTOSHAREALBUM, new QueueData<long, string, string, Media.Action>
                            {
                                Data1 = album.Id,
                                Data2 = "192.168.25.191",
                                Data3 = "sessionid",
                                Data4 = Media.Action.update
                            });
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
            }
            return returnValue;
        }

        public static async Task<bool> PushPhotoToKingHub(string sessionId, PayloadDataShare playloadDataShare, Media.Action action)
        {
            try
            {
                if (playloadDataShare != null && playloadDataShare.MetaDataShared?.Count > 0)
                {
                    var media = playloadDataShare.MetaDataShared.FirstOrDefault();

                    var is_comment = AppSettings.Current.ChannelConfiguration.ApiCommentSetting.Mode.PostCheck;
                    if (media.CommentMode == CommentMode.Disable)
                    {
                        is_comment = AppSettings.Current.ChannelConfiguration.ApiCommentSetting.Mode.Disable;
                    }
                    else
                    {
                        if (media.CommentMode == CommentMode.PreCheck) is_comment = AppSettings.Current.ChannelConfiguration.ApiCommentSetting.Mode.PreCheck;
                    }

                    var post = new
                    {
                        posts = new List<object>{ new {
                            media_id = media.ObjectId.ToString(),
                            title = media.Title??string.Empty,
                            mediaunit_name = media.Name??string.Empty,
                            card_type = (int)CardType.Photo,
                            preview=new{ },
                            media_unit_desc = media.ItemMediaUnit?.Select(s=> new {
                                id = s.Id ?? string.Empty,
                                title = s.Title ?? string.Empty,
                                link = (string.IsNullOrEmpty(s.Link) ? s.Thumb : s.Link) ?? string.Empty,
                                thumb = s.Thumb ?? string.Empty,
                                width = s.Width,
                                height = s.Height,
                                content_type = s.ContentType != null && (s.ContentType == ContentType.Image || s.ContentType == ContentType.Photo) ? (int)s.ContentType : (int)ContentType.Photo,
                                is_heart_drop = s.IsHeartDrop,
                                created_at = (s.CreatedDate??DateTime.Now).ToString("yyyy-MM-ddTHH:mm:ssZ")
                            }).ToList(),
                            channel_id = media.ChannelId??string.Empty,
                            frame_id = media.Frameid,
                            tags = media.Tags??string.Empty,
                            user_id = media.UserId.ToString(),
                            user_name = media.UserName??string.Empty,
                            full_name = media.FullName??string.Empty,
                            avatar = media.AuthorAvatar??string.Empty,
                            cat_id = media.ZoneId?.ToString()??string.Empty,
                            create_time = Utility.ConvertToTimestamp(media.DistributionDate??DateTime.Now),
                            cat_name = media.ZoneName??string.Empty,
                            extension = new {
                                status = media.CaptionExt??new Caption[]{},
                                flag = new
                                {
                                    is_comment
                                },
                                is_web = 1,
                                boards=media.BoardIds != null && media.BoardIds.Length > 0 ? string.Join(",", media.BoardIds) : string.Empty,
                                label = new
                                {
                                    id = ((int?)media.LabelType)?.ToString(),
                                    text = media.LabelType != null ? Enum.GetName(typeof(LabelType), media.LabelType)?.ToLower() : ""
                                }
                            },
                        }}
                    };

                    //hearder thay vì session-id , các anh truyền vào user-id của page hoặc user nhé
                    sessionId = media.UserId.ToString();

                    if (action == Media.Action.add)
                    {
                        var errorCodeReturn = await Media.Add(media.ObjectId.ToString(), sessionId, CardType.Photo, post);

                        if (errorCodeReturn == ErrorCodes.DataExist)
                        {
                            action = Media.Action.update;
                        }
                        else
                        {
                            return errorCodeReturn == ErrorCodes.Success ? true : false;
                        }
                    }

                    if (action == Media.Action.update)
                    {
                        return await Media.Update(media.ObjectId.ToString(), sessionId, CardType.Photo, post) == ErrorCodes.Success ? true : false;
                    }

                    if (action == Media.Action.deletebymedia)
                    {
                        return await Media.Delete(media.ObjectId.ToString(), sessionId, CardType.Photo) == ErrorCodes.Success ? true : false;
                    }
                    //action ko hỗ trợ
                    return false;
                }

                return false;
            }
            catch (Exception ex)
            {
                Logger.Error(ex, "PushBlogToKingHub: " + ex.Message);
                return false;
            }
        }
    }
}
