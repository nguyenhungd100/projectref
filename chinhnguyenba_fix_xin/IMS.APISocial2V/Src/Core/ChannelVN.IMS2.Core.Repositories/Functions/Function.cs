﻿using ChannelVN.IMS2.Core.Entities.Security;
using ChannelVN.IMS2.Core.Repositories.Constants;
using ChannelVN.IMS2.Foundation.Logging;
using ChannelVN.IMS2.Foundation.Toolkit.Messaging;
using ChannelVN.IMS2.Foundation.Toolkit.Messaging.ExchangeQueue;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChannelVN.IMS2.Core.Repositories.Functions
{
    public static class Function
    {
        public static Task AddToQueue<T>(string action, string topic, T data)
        {
            try
            {
                var queue = new Entities.Queue<T>
                {
                    Action = action,
                    Data = data,
                    CreatedDate = DateTime.Now
                };

                ExchangeFactory.Send(new EQMessage<object>
                {
                    Headers = new EQMessageHeaders { Topic = topic },
                    Body = queue
                }, (err, effected) =>
                {
                    //TODO somethings
                    if (err != null)
                    {
                        Logger.Error(err, "queue:" + action + err.Message);
                    }
                });
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
            }
            return Task.CompletedTask;
        }

        public static Task PushAccountClassToAdtech(Account account)
        {
            try
            {
                if (account.Type == (int)AccountType.Official)
                {
                    if (long.TryParse(account.CreatedBy, out long accountId))
                    {
                        if (accountId > 0)
                        {
                            AddToQueue(ActionName.InsertPageOwnerToApp, TopicName.SYNC_USERINFO, new QueueData<long, long, byte?, string>
                            {
                                Data1 = account.Id,
                                Data2 = accountId,
                                Data3 = account.Class,
                                Data4 = account.FullName
                            });
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
            }
            return Task.CompletedTask;
        }
    }
}
