﻿using ChannelVN.IMS2.Core.Entities.Board;
using ChannelVN.IMS2.Core.Entities.Security;
using ChannelVN.IMS2.Core.Entities.Security.KingHub;
using ChannelVN.IMS2.Core.Models.CacheStore.Security;
using ChannelVN.IMS2.Core.Models.DataStore;
using ChannelVN.IMS2.Core.Models.DataStore.Security;
using ChannelVN.IMS2.Core.Models.SearchStore.Security;
using ChannelVN.IMS2.Core.Repositories.Constants;
using ChannelVN.IMS2.Core.Repositories.Functions;
using ChannelVN.IMS2.Foundation.Common;
using ChannelVN.IMS2.Foundation.Common.Configuration;
using ChannelVN.IMS2.Foundation.Data;
using ChannelVN.IMS2.Foundation.Data.Factories;
using ChannelVN.IMS2.Foundation.Logging;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Security.Authentication;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;
using static ChannelVN.IMS2.Core.Entities.ErrorCode.ErrorMapping;

namespace ChannelVN.IMS2.Core.Repositories
{
    public class SecurityRepository : Repository
    {
        public static Task<Dictionary<long, ObjectTmp>> GetAllAccountAsync()
        {
            return StoreFactory.Create<AccountCacheStore>().GetAllAccountAsync();
        }

        public static Task<Dictionary<long, ObjectTmp>> GetAllPageAsync()
        {
            return StoreFactory.Create<AccountCacheStore>().GetAllPageAsync();
        }

        public static Task<UserProfile> GetProfileAsync(long id)
        {
            return StoreFactory.Create<AccountCacheStore>().GetProfileAsync(id);
        }

        public async static Task<bool> ConvertDataAsync(Account data)
        {
            //Init data

            var result = await StoreFactory.Create<SystemDataStore>().ConvertDataAsync(data);
            if (result)
            {
                await Function.AddToQueue(ActionName.InitAll, TopicName.SQL_CONVERT, "");
                return true;
            }
            else
            {
                return false;
            }
        }

        public static Task<List<AccountMember>> ListMembersAsync(long officerId)
        {
            return StoreFactory.Create<AccountCacheStore>().ListMembersAsync(officerId);
        }

        public static async Task<ErrorCodes> AddAsync(Account data)
        {
            var returnVl = ErrorCodes.BusinessError;
            try
            {
                var returnValue = await StoreFactory.Create<AccountCacheStore>().AddAsync(data);
                if (returnValue)
                {
                    returnVl = ErrorCodes.Success;
                    var res = await StoreFactory.Create<AccountSearchStore>().AddAsync(Mapper<AccountSearch>.Map(data, new AccountSearch()));
                    if (!res)
                    {
                        //push queue
                        await Function.AddToQueue(ActionName.AddAccountES, TopicName.ES_ACCOUNT, data);
                    }

                    res = await StoreFactory.Create<AccountDataStore>().AddAsync(data);
                    if (!res)
                    {
                        //push queue
                        await Function.AddToQueue(ActionName.AddAccountSQL, TopicName.SQL_ACCOUNT, data);
                    }
                }

                return returnVl;
            }
            catch (Exception ex)
            {
                Logger.Sensitive(ex, ex.Message);
                return ErrorCodes.Exception;
            }
        }

        public static async Task<ErrorCodes> AddOfficialAsync(Account officerAccount, UserProfile officerProfile, long currentAccoutId)
        {
            var returnVl = ErrorCodes.AddOfficerError;
            try
            {
                var accountMember = new AccountMember
                {
                    OfficerId = officerAccount.Id,
                    MemberId = currentAccoutId,
                    JoinedDate = DateTime.Now,
                    Role = AccountMemberRole.Owner,
                    Permissions = MemberRole.Roles[AccountMemberRole.Owner]?.ToArray()
                };
                var returnValue = await StoreFactory.Create<AccountCacheStore>().AddOfficialAsync(officerAccount, officerProfile, accountMember);
                if (returnValue)
                {
                    returnVl = ErrorCodes.Success;
                    var accountSearch = new AccountSearch();
                    accountSearch = Mapper<AccountSearch>.Map(officerAccount, new AccountSearch());

                    var res = await StoreFactory.Create<AccountSearchStore>().AddAsync(accountSearch);
                    if (!res)
                    {
                        //push queue
                        await Function.AddToQueue(ActionName.AddOfficialAccountES, TopicName.ES_ACCOUNT, officerAccount);
                    }

                    res = await StoreFactory.Create<AccountDataStore>().AddOfficialAsync(officerAccount, officerProfile, accountMember);
                    if (!res)
                    {
                        //push queue
                        await Function.AddToQueue(ActionName.AddOfficialAccountSQL, TopicName.SQL_ACCOUNT, new QueueData<Account, UserProfile, AccountMember, object> { Data1 = officerAccount, Data2 = officerProfile, Data3 = accountMember });
                    }

                    await Function.PushAccountClassToAdtech(officerAccount);
                }

                return returnVl;
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
                return ErrorCodes.Exception;
            }
        }

        public static async Task<ErrorCodes> SaveProfileAsync(UserProfile data, Account userCurrent)
        {
            var accountCache = StoreFactory.Create<AccountCacheStore>();
            try
            {
                var returnValue = await StoreFactory.Create<AccountCacheStore>().SaveProfileAsync(data, userCurrent);
                if (returnValue)
                {
                    var accountSearch = await StoreFactory.Create<AccountSearchStore>().GetByIdAsync(userCurrent.Id);
                    var res = await StoreFactory.Create<AccountSearchStore>().UpdateAsync(Mapper<AccountSearch>.Map(userCurrent, accountSearch ?? new AccountSearch()));
                    if (!res)
                    {
                        //push queue
                        await Function.AddToQueue(ActionName.AddProfileES, TopicName.ES_ACCOUNT, userCurrent);
                    }
                    res = await StoreFactory.Create<AccountDataStore>().SaveProfileAsync(data, userCurrent);
                    if (!res)
                    {
                        //push queue
                        await Function.AddToQueue(ActionName.AddProfileSQL, TopicName.SQL_ACCOUNT, new QueueData<Account, UserProfile, object, object> { Data1 = userCurrent, Data2 = data });
                    }
                    return ErrorCodes.Success;
                }

                return ErrorCodes.BusinessError;
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
                return ErrorCodes.Exception;
            }
        }

        public static async Task<ErrorCodes> UpdateAsync(AccountSearch data, UserProfile profile, bool isUpdatePage = false, string mobileOld = null, string emailOld = null, string sessionId = null)
        {
            try
            {
                var returnValue = await StoreFactory.Create<AccountCacheStore>().UpdateAsync(Mapper<Account>.Map(data, new Account()), profile, mobileOld, emailOld);
                if (returnValue)
                {
                    var res = await StoreFactory.Create<AccountSearchStore>().UpdateAsync(data);
                    if (!res)
                    {
                        //push queue
                        await Function.AddToQueue(ActionName.UpdateAccountES, TopicName.ES_ACCOUNT, data);
                    }

                    res = await StoreFactory.Create<AccountDataStore>().UpdateAsync(data, profile);
                    if (!res)
                    {
                        //push queue
                        await Function.AddToQueue(ActionName.UpdateAccountSQL, TopicName.SQL_ACCOUNT, new QueueData<AccountSearch, UserProfile, object, object> { Data1 = data, Data2 = profile });
                    }

                    if (!string.IsNullOrEmpty(sessionId) && isUpdatePage)
                    {
                        //update thông tin page
                        await Function.AddToQueue(ActionName.UpdateUserInfoOnApp, TopicName.SYNC_USERINFO, new QueueData<AccountSearch, string, UserProfile, object>
                        {
                            Data1 = data,
                            Data2 = sessionId,
                            Data3 = profile
                        });

                        //update comment mode setting
                        await Function.AddToQueue(ActionName.SettingCommentOnApp, TopicName.SYNC_USERINFO, new QueueData<long, Byte?, object, object>
                        {
                            Data1 = data.Id,
                            Data2 = data.CommentMode
                        });

                        //bắn class sang Adtech
                        await Function.PushAccountClassToAdtech(Mapper<Account>.Map(data, new Account()));
                    }

                    return ErrorCodes.Success;
                }

                return ErrorCodes.BusinessError;
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
                return ErrorCodes.Exception;
            }
        }

        public static async Task<ErrorCodes> ConfigPageAsync(long officerId, int mode, long accountId)
        {
            try
            {
                var returnValue = await StoreFactory.Create<AccountCacheStore>().ConfigPageAsync(officerId, mode, accountId);
                if (returnValue)
                {
                    await Function.AddToQueue(ActionName.ConfigPageES, TopicName.ES_ACCOUNT, new QueueData<long, int, long, object>
                    {
                        Data1 = officerId,
                        Data2 = mode,
                        Data3 = accountId
                    });

                    await Function.AddToQueue(ActionName.ConfigPageSQL, TopicName.SQL_ACCOUNT, new QueueData<long, int, long, object>
                    {
                        Data1 = officerId,
                        Data2 = mode,
                        Data3 = accountId
                    });

                    return ErrorCodes.Success;
                }

                return ErrorCodes.UpdateError;
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
                return ErrorCodes.Exception;
            }
        }
        public static async Task<bool> UpdateUserInfoOnAppAsync(AccountSearch account, UserProfile profile, string sessionId)
        {
            var returnValue = false;
            try
            {
                using (var client = new HttpClient())
                {
                    var multipartFormDataContent = new MultipartFormDataContent();

                    var data = new
                    {
                        avatar = account.Avatar ?? string.Empty,
                        birthday = profile?.BirthDay?.ToString(AppSettings.Current.ChannelConfiguration.BirthdayFormat)??string.Empty,
                        cover = account.Banner ?? string.Empty,
                        dev = AppSettings.Current.ChannelConfiguration.ApiUserKingHub.Mode.ToString(),
                        full_name = account.FullName ?? string.Empty,
                        job_position = profile?.Title??string.Empty,
                        living_place = profile?.Address??string.Empty,
                        school = profile?.School??string.Empty,
                        sex = profile?.Gender == 0 ? "FEMALE" : "MALE",
                        user_id = account.EncryptId ?? string.Empty,
                        user_status = string.Empty,
                        username = account.UserName ?? string.Empty,
                        workplace = profile?.WorkPlace??string.Empty
                    };

                    //var values = new[]
                    //   {
                    //        new KeyValuePair<string, string>("user_id", account.EncryptId??string.Empty),
                    //        new KeyValuePair<string, string>("username", account.UserName??string.Empty),
                    //        new KeyValuePair<string, string>("sex", "MALE"),
                    //        new KeyValuePair<string, string>("full_name", account.FullName??string.Empty),
                    //        new KeyValuePair<string, string>("avatar", account.Avatar??string.Empty),
                    //        new KeyValuePair<string, string>("cover", account.Banner??string.Empty),
                    //        new KeyValuePair<string, string>("dev", AppSettings.Current.ChannelConfiguration.ApiUserKingHub.Mode.ToString())
                    //    };

                    var values = data.ConvertToKeyValuePair();
                    foreach (var keyValuePair in values)
                    {
                        multipartFormDataContent.Add(new StringContent(keyValuePair.Value),
                            String.Format("\"{0}\"", keyValuePair.Key));
                    }

                    //Logger.Debug("Account Data=>" + Json.Stringify(account));
                    //Logger.Debug("Profile Data=>" + Json.Stringify(profile));
                    //var hashData = string.Format("POST/update-userinfoavatar={0}&cover={1}&dev={2}&full_name={3}&sex={4}&user_id={5}&username={6}", data.avatar ?? string.Empty, data.cover ?? string.Empty, data.dev, data.full_name ?? string.Empty, data.sex, data.user_id, data.username);
                    var hashData = data.BuildHMAQueyString("POST/update-userinfo");
                    var hashKey = AppSettings.Current.ChannelConfiguration.ApiUserKingHub.TokenGenUser;
                    var hmac = Encryption.HMACSHA256Encode(hashData, hashKey);

                    client.DefaultRequestHeaders.Add("hmac", hmac);
                    client.DefaultRequestHeaders.Add("session-id", sessionId);

                   // Logger.Debug("Sync user info to app =>" + Json.Stringify(data));
                    //Logger.Debug("hashData=>" + hashData);
                    //Logger.Debug("hmac=>" + hmac);

                    var requestUri = AppSettings.Current.ChannelConfiguration.ApiUserKingHub.UrlUpdate;
                    var response = await client.PostAsync(requestUri, multipartFormDataContent);

                    if (!response.IsSuccessStatusCode)
                    {
                        response.EnsureSuccessStatusCode();
                    }

                    if (response?.StatusCode == System.Net.HttpStatusCode.OK)
                    {
                        var stringContent = await response.Content?.ReadAsStringAsync();
                        var userData = Json.Parse<AccountRespone>(stringContent);
                        if (userData != null && userData.Status == 1)
                        {
                            returnValue = true;
                        }
                    }
                    else
                    {
                        Logger.Sensitive("Responce sync user info to app =>" + await response?.Content?.ReadAsStringAsync(), account);
                    }
                }
            }
            catch (Exception ex)
            {
                //Logger.Debug(ex ,"Responce sync user info to app =>" +ex.Message);
                Logger.Sensitive(ex, ex.Message);
            }
            return returnValue;
        }


        public static async Task<bool> UpdateUserAvatarOnAppAsync(AccountSearch account, UserProfile profile, string sessionId)
        {
            var returnValue = false;
            try
            {
                var avatar = account.Avatar;
                account = await SecurityRepository.GetAccountSearchByIdAsync(account.Id);
                account.Avatar = avatar;
                profile = await SecurityRepository.GetProfileAsync(account.Id);
                using (var client = new HttpClient())
                {
                    var multipartFormDataContent = new MultipartFormDataContent();

                    var data = new
                    {
                        avatar = account.Avatar ?? string.Empty,
                        birthday = profile?.BirthDay?.ToString(AppSettings.Current.ChannelConfiguration.BirthdayFormat) ?? string.Empty,
                        cover = account.Banner ?? string.Empty,
                        dev = AppSettings.Current.ChannelConfiguration.ApiUserKingHub.Mode.ToString(),
                        full_name = account.FullName ?? string.Empty,
                        job_position = profile?.Title ?? string.Empty,
                        living_place = profile?.Address ?? string.Empty,
                        school = profile?.School ?? string.Empty,
                        sex = profile?.Gender == 0 ? "FEMALE" : "MALE",
                        user_id = account.EncryptId ?? string.Empty,
                        user_status = string.Empty,
                        username = account.UserName ?? string.Empty,
                        workplace = profile?.WorkPlace ?? string.Empty
                    };

                    //var values = new[]
                    //   {
                    //        new KeyValuePair<string, string>("user_id", account.EncryptId??string.Empty),
                    //        new KeyValuePair<string, string>("username", account.UserName??string.Empty),
                    //        new KeyValuePair<string, string>("sex", "MALE"),
                    //        new KeyValuePair<string, string>("full_name", account.FullName??string.Empty),
                    //        new KeyValuePair<string, string>("avatar", account.Avatar??string.Empty),
                    //        new KeyValuePair<string, string>("cover", account.Banner??string.Empty),
                    //        new KeyValuePair<string, string>("dev", AppSettings.Current.ChannelConfiguration.ApiUserKingHub.Mode.ToString())
                    //    };

                    var values = data.ConvertToKeyValuePair();
                    foreach (var keyValuePair in values)
                    {
                        multipartFormDataContent.Add(new StringContent(keyValuePair.Value),
                            String.Format("\"{0}\"", keyValuePair.Key));
                    }

                    Logger.Debug("Account Data=>" + Json.Stringify(account));
                    Logger.Debug("Profile Data=>" + Json.Stringify(profile));
                    //var hashData = string.Format("POST/update-userinfoavatar={0}&cover={1}&dev={2}&full_name={3}&sex={4}&user_id={5}&username={6}", data.avatar ?? string.Empty, data.cover ?? string.Empty, data.dev, data.full_name ?? string.Empty, data.sex, data.user_id, data.username);
                    var hashData = data.BuildHMAQueyString("POST/update-userinfo");
                    var hashKey = AppSettings.Current.ChannelConfiguration.ApiUserKingHub.TokenGenUser;
                    var hmac = Encryption.HMACSHA256Encode(hashData, hashKey);

                    client.DefaultRequestHeaders.Add("hmac", hmac);
                    client.DefaultRequestHeaders.Add("session-id", sessionId);

                    // Logger.Debug("Sync user info to app =>" + Json.Stringify(data));
                    //Logger.Debug("hashData=>" + hashData);
                    //Logger.Debug("hmac=>" + hmac);

                    var requestUri = AppSettings.Current.ChannelConfiguration.ApiUserKingHub.UrlUpdate;
                    var response = await client.PostAsync(requestUri, multipartFormDataContent);

                    if (!response.IsSuccessStatusCode)
                    {
                        response.EnsureSuccessStatusCode();
                    }

                    if (response?.StatusCode == System.Net.HttpStatusCode.OK)
                    {
                        var stringContent = await response.Content?.ReadAsStringAsync();
                        var userData = Json.Parse<AccountRespone>(stringContent);
                        if (userData != null && userData.Status == 1)
                        {
                            returnValue = true;
                        }

                        Logger.Debug("Responce sync user info to app 1 =>" + stringContent);
                    }
                    else
                    {
                        Logger.Debug("Responce sync user info to app 2 =>" + await response?.Content?.ReadAsStringAsync());
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Debug(ex, "Responce sync user info to app 3 =>" + ex.Message);
                Logger.Error(ex, ex.Message);
            }
            return returnValue;
        }

        public static async Task<bool> SettingCommentOnAppAsync(long pageId, int restrictionId)
        {
            var returnValue = false;
            var url = AppSettings.Current.ChannelConfiguration.ApiCommentSetting.UrlSettingMode;

            var jsonBody = Json.Stringify(new object[]
            {
                new {
                    entity_id = pageId,
                    type = 1,
                    settings = new object[]
                    {
                        new {
                            restriction_id = restrictionId,
                            active = 1
                        }
                    }
                }
            });
            try
            {
                using (var client = new HttpClient())
                {
                    client.Timeout = TimeSpan.FromSeconds(AppSettings.Current.ChannelConfiguration.HttpClientTimeOut);

                    var response = await client.PostAsync(url, new StringContent(jsonBody, Encoding.UTF8, "application/json"));

                    if (!response.IsSuccessStatusCode)
                    {
                        response.EnsureSuccessStatusCode();
                    }

                    if (response.StatusCode == HttpStatusCode.OK)
                    {
                        returnValue = true;
                    }
                    else
                    {
                        Logger.Sensitive("modify_settings(Adtech) => " + await response?.Content?.ReadAsStringAsync(), new
                        {
                            pageId,
                            restrictionId
                        });
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Sensitive(ex, ex.Message);
            }
            return returnValue;
        }

        public static async Task<ErrorCodes> AcceptOrRejectAsync(AccountSearch data)
        {
            try
            {

                var returnValue = await StoreFactory.Create<AccountCacheStore>().AcceptOrRejectAsync(Mapper<Account>.Map(data, new Account()));
                if (returnValue)
                {
                    var res = await StoreFactory.Create<AccountSearchStore>().AcceptOrRejectAsync(data);
                    if (!res)
                    {
                        //push queue
                        await Function.AddToQueue(ActionName.AcceptOrRejectES, TopicName.ES_ACCOUNT, data);
                    }

                    res = await StoreFactory.Create<AccountDataStore>().AcceptOrRejectAsync(data);
                    if (!res)
                    {
                        //push queue
                        await Function.AddToQueue(ActionName.AcceptOrRejectSQL, TopicName.SQL_ACCOUNT, data);
                    }

                    return ErrorCodes.Success;
                }

                return ErrorCodes.BusinessError;
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
                return ErrorCodes.Exception;
            }
        }

        public static async Task<ErrorCodes> UpdateAccountTypeAsync(Account accDb)
        {
            try
            {
                var returnValue = await StoreFactory.Create<AccountCacheStore>().UpdateAccountAsync(accDb);
                if (returnValue)
                {
                    var res = await StoreFactory.Create<AccountSearchStore>().UpdateAccountAsync(Mapper<AccountSearch>.Map(accDb, new AccountSearch()));
                    if (!res)
                    {
                        //push queue
                        await Function.AddToQueue(ActionName.UpdateAccountES, TopicName.ES_ACCOUNT, accDb);
                    }
                    //push queue
                    await Function.AddToQueue(ActionName.UpdateAccountTypeSQL, TopicName.SQL_ACCOUNT, accDb);

                    //push account class to adtech
                    await Function.PushAccountClassToAdtech(accDb);

                    return ErrorCodes.Success;
                }

                return ErrorCodes.BusinessError;
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
                return ErrorCodes.Exception;
            }
        }

        public static async Task<ErrorCodes> UpdateRoleAsync(Account accDb)
        {
            try
            {
                var returnValue = await StoreFactory.Create<AccountCacheStore>().UpdateAccountAsync(accDb);
                if (returnValue)
                {
                    var res = await StoreFactory.Create<AccountSearchStore>().UpdateAccountAsync(Mapper<AccountSearch>.Map(accDb, new AccountSearch()));
                    if (!res)
                    {
                        //push queue
                        await Function.AddToQueue(ActionName.UpdateAccountES, TopicName.ES_ACCOUNT, accDb);
                    }
                    //push queue
                    await Function.AddToQueue(ActionName.UpdateAccountTypeSQL, TopicName.SQL_ACCOUNT, accDb);

                    return ErrorCodes.Success;
                }

                return ErrorCodes.BusinessError;
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
                return ErrorCodes.Exception;
            }
        }
        
        public static async Task<ErrorCodes> UpdateAccountClassAsync(Account accDb)
        {
            try
            {
                var returnValue = await StoreFactory.Create<AccountCacheStore>().UpdateAccountAsync(accDb);
                if (returnValue)
                {
                    var res = await StoreFactory.Create<AccountSearchStore>().UpdateAccountAsync(Mapper<AccountSearch>.Map(accDb, new AccountSearch()));
                    if (!res)
                    {
                        //push queue
                        await Function.AddToQueue(ActionName.UpdateAccountES, TopicName.ES_ACCOUNT, accDb);
                    }
                    //push queue
                    await Function.AddToQueue(ActionName.UpdateAccountClassSQL, TopicName.SQL_ACCOUNT, accDb);

                    //push account class to adtech
                    await Function.PushAccountClassToAdtech(accDb);

                    return ErrorCodes.Success;
                }

                return ErrorCodes.BusinessError;
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
                return ErrorCodes.Exception;
            }
        }

        public static async Task<ErrorCodes> UpdateAccountOpenIdAsync(Account accDb)
        {
            try
            {
                var returnValue = await StoreFactory.Create<AccountCacheStore>().UpdateAccountOpentIdAsync(accDb);
                if (returnValue)
                {
                    returnValue = await StoreFactory.Create<AccountSearchStore>().UpdateAccountOpentIdAsync(Mapper<AccountSearch>.Map(accDb, new AccountSearch()));
                    returnValue = await StoreFactory.Create<AccountDataStore>().UpdateAccountOpentIdAsync(accDb, true);

                    return ErrorCodes.Success;
                }

                return ErrorCodes.BusinessError;
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
                return ErrorCodes.Exception;
            }
        }

        public static Task<AccountSearch> GetAccountByUserNameAsync(string username)
        {
            return StoreFactory.Create<AccountCacheStore>().GetUserByNameAsync(username);
        }

        public static Task<Account> GetByIdAsync(long id)
        {
            return StoreFactory.Create<AccountCacheStore>().GetByIdAsync(id);
        }

        public static async Task<AccountSimple> GetSimpleAccountAsync(long id)
        {
            var resutl = default(AccountSimple);
            try
            {
                resutl = await StoreFactory.Create<AccountCacheStore>().GetSimpleAccountAsync(id);
                //if (resutl?.Type == (int)AccountType.Official)
                //{
                //    var syncResult = await GetUserInfoAndUpdateAsync(id);
                //    resutl.Avatar = syncResult.Avatar;
                //    resutl.Email = syncResult.Email;
                //    resutl.FullName = syncResult.FullName;
                //    resutl.Mobile = syncResult.Mobile;
                //    resutl.UserName = syncResult.UserName;
                //}
            }
            catch(Exception ex)
            {
                Logger.Error(ex, ex.Message);
            }
            return resutl;
        }

        public static Task<AccountSearch> GetAccountSearchByIdAsync(long id)
        {
            return StoreFactory.Create<AccountCacheStore>().GetAccountSearchByIdAsync(id);
        }

        public static Task<AccountSearch> GetByIdESAsync(long officerId)
        {
            return StoreFactory.Create<AccountSearchStore>().GetByIdAsync(officerId);
        }
        public static Task<Double?> GetByUsernameAsync(string username)
        {
            return StoreFactory.Create<AccountCacheStore>().GetByUsernameAsync(username);
        }
        public static async Task<PagingDataResult<AccountSearch>> SearchAsync(SearchAccountEntity search)
        {
            try
            {
                var result = await StoreFactory.Create<AccountSearchStore>().SearchAsync(search);
                if (result != null && result.Data != null)
                {
                    var listRedis = await GetListByIdsAsync(result.Data.Select(p => p.Id.ToString()).ToList());
                    result.Data = listRedis.Select(p => Mapper<AccountSearch>.Map(p, new AccountSearch()))?.ToList();
                }
                return result;
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
                return null;
            }
        }
        public static Task<List<Account>> GetListByIdsAsync(List<string> ids)
        {
            return StoreFactory.Create<AccountCacheStore>().GetListByIdsAsync(ids);
        }
        public static Task<PagingDataResult<AccountSearch>> SearchByRegisterAccountStatusAsync(SearchAccountEntity search)
        {
            return StoreFactory.Create<AccountSearchStore>().SearchByRegisterAccountStatusAsync(search);
        }

        public static Task<long> GetCountByUserNameAsync(string userName)
        {
            return StoreFactory.Create<AccountSearchStore>().GetCountByUserNameAsync(userName);
        }
        public static async Task<ErrorCodes> UpdateStatusAsync(AccountSearch account)
        {
            try
            {
                var returnValue = await StoreFactory.Create<AccountCacheStore>().UpdateStatusAsync(account);
                if (returnValue)
                {
                    var res = await StoreFactory.Create<AccountSearchStore>().UpdateAsync(account);
                    if (!res)
                    {
                        //push queue
                        await Function.AddToQueue(ActionName.UpdateAccountStatusES, TopicName.ES_ACCOUNT, new Account { Id = account.Id});
                    }

                    res = await StoreFactory.Create<AccountDataStore>().UpdateStatusAsync(account);
                    if (!res)
                    {
                        //push queue
                        await Function.AddToQueue(ActionName.UpdateAccountStatusSQL, TopicName.SQL_ACCOUNT, new Account() { Id = account.Id});
                    }

                    //push account class to adtech
                    await Function.PushAccountClassToAdtech(Mapper<Account>.Map(account, new Account()));

                    return ErrorCodes.Success;
                }

                return ErrorCodes.BusinessError;
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
                return ErrorCodes.Exception;
            }
        }

        #region AccountMember
        public static Task<AccountMember> GetAccountMemberByIdAsync(string id)
        {
            return StoreFactory.Create<AccountCacheStore>().GetAccountMemberByIdAsync(id);
        }
        public static async Task<ErrorCodes> AddAccountMemberAsync(AccountMember accountMember, AccountSearch officerAcc)
        {
            try
            {
                var returnValue = await StoreFactory.Create<AccountCacheStore>().AddAccountMemberAsync(accountMember);
                if (returnValue)
                {
                    var res = await StoreFactory.Create<AccountSearchStore>().AddAsync(officerAcc);
                    if (!res)
                    {
                        //push queue
                        await Function.AddToQueue(ActionName.AddAccountES, TopicName.ES_ACCOUNT, officerAcc);
                    }

                    res = await StoreFactory.Create<AccountDataStore>().AddAccountMemberAsync(accountMember);
                    if (!res)
                    {
                        //push queue
                        await Function.AddToQueue(ActionName.AddAccountMemberSQL, TopicName.SQL_ACCOUNT, accountMember);
                    }

                    //add account member to db account
                    await Function.AddToQueue(ActionName.AddUserMember, TopicName.SYNC_USERINFO, accountMember.MemberId);

                    return ErrorCodes.Success;
                }

                return ErrorCodes.BusinessError;
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
                return ErrorCodes.Exception;
            }
        }

        public static async Task<ErrorCodes> Update_AccountMemberAsync(AccountMember accountMember)
        {
            try
            {
                var returnValue = await StoreFactory.Create<AccountCacheStore>().Update_AccountMemberAsync(accountMember);
                if (returnValue)
                {
                    var res = await StoreFactory.Create<AccountSearchStore>().Update_AccountMemberAsync(accountMember);
                    if (!res)
                    {
                        //push queue
                        await Function.AddToQueue(ActionName.UpdateAccountMemberES, TopicName.ES_ACCOUNT, accountMember);
                    }

                    res = await StoreFactory.Create<AccountDataStore>().UpdateAccountMemberAsync(accountMember);
                    if (!res)
                    {
                        //push queue
                        await Function.AddToQueue(ActionName.UpdateAccountMemberSQL, TopicName.SQL_ACCOUNT, accountMember);
                    }
                    return ErrorCodes.Success;
                }

                return ErrorCodes.BusinessError;
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
                return ErrorCodes.Exception;
            }
        }

        public static async Task<bool> RemoveMemberAsync(long officerId, long memberId)
        {
            try
            {
                var returnValue = await StoreFactory.Create<AccountCacheStore>().RemoveMemberAsync(officerId, memberId);
                if (returnValue)
                {
                    var res = await StoreFactory.Create<AccountSearchStore>().RemoveMemberAsync(officerId, memberId);
                    if (!res)
                    {
                        await Function.AddToQueue(ActionName.RemoveMemberES, TopicName.ES_ACCOUNT, new QueueData<long, long, object, object> { Data1 = officerId, Data2 = memberId });
                    }
                    //push queue sql
                    res = await StoreFactory.Create<AccountDataStore>().RemoveMemberAsync(officerId, memberId);
                    if (!res)
                    {
                        await Function.AddToQueue(ActionName.RemoveMemberSQL, TopicName.SQL_ACCOUNT, new QueueData<long, long, object, object> { Data1 = officerId, Data2 = memberId });
                    }
                }

                return returnValue;
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
                return false;
            }
        }

        public static async Task<ErrorCodes> RemoveMemberOnAllPageAsync(long accountId, long memberId)
        {
            try
            {
                var listOfficerId = default(IEnumerable<long>);
                var returnValue = await StoreFactory.Create<AccountCacheStore>().RemoveMemberOnAllPageAsync(accountId, memberId, new Action<IEnumerable<long>>((data)=>
                {
                    listOfficerId = data;
                }));

                if (returnValue == ErrorCodes.Success && listOfficerId != null && listOfficerId.Count() > 0)
                {
                    var res = await StoreFactory.Create<AccountSearchStore>().RemoveMemberOnAllPageAsync(listOfficerId, memberId);
                    if (!res)
                    {
                        await Function.AddToQueue(ActionName.RemoveMemberOnAllPageES, TopicName.ES_ACCOUNT, new QueueData<IEnumerable<long>, long, object, object> { Data1 = listOfficerId, Data2 = memberId });
                    }
                    //push queue sql
                    res = await StoreFactory.Create<AccountDataStore>().RemoveMemberOnAllPageAsync(listOfficerId, memberId);
                    if (!res)
                    {
                        await Function.AddToQueue(ActionName.RemoveMemberOnAllPageSQL, TopicName.SQL_ACCOUNT, new QueueData<IEnumerable<long>, long, object, object> { Data1 = listOfficerId, Data2 = memberId });
                    }
                }

                return returnValue;
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
                return ErrorCodes.Exception;
            }
        }

        public static async Task<bool> LockOrUnlockMemberAsync(AccountMember data)
        {
            try
            {
                var returnValue = await StoreFactory.Create<AccountCacheStore>().LockOrUnlockMemberAsync(data);
                if (returnValue)
                {
                    var res = await StoreFactory.Create<AccountSearchStore>().LockOrUnlockMemberAsync(data);
                    if (!res)
                    {
                        await Function.AddToQueue(ActionName.LockOrUnlockMemberES, TopicName.ES_ACCOUNT, data);
                    }
                    //push queue sql
                    res = await StoreFactory.Create<AccountDataStore>().LockOrUnlockMemberAsync(data);
                    if (!res)
                    {
                        await Function.AddToQueue(ActionName.LockOrUnlockMemberSQL, TopicName.SQL_ACCOUNT, data);
                    }
                }

                return returnValue;
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
                return false;
            }
        }

        public static async Task<ErrorCodes> LockOrUnlockMemberOnAllPageAsync(long accountId, long memberId, int act)
        {
            try
            {
                var listMember = default(IEnumerable<AccountMember>);
                var returnValue = await StoreFactory.Create<AccountCacheStore>().LockOrUnlockMemberOnAllPageAsync(accountId, memberId, act, new Action<IEnumerable<AccountMember>>((data) =>
                {
                    listMember = data;
                }));

                if (returnValue == ErrorCodes.Success && listMember != null && listMember.Count() > 0)
                {
                    var res = await StoreFactory.Create<AccountSearchStore>().LockOrUnlockMemberOnAllPageAsync(listMember.ToArray());
                    if (!res)
                    {
                        await Function.AddToQueue(ActionName.LockOrUnlockMemberOnAllPageES, TopicName.ES_ACCOUNT, listMember);
                    }
                    //push queue sql
                    res = await StoreFactory.Create<AccountDataStore>().LockOrUnlockMemberOnAllPageAsync(listMember.ToArray());
                    if (!res)
                    {
                        await Function.AddToQueue(ActionName.LockOrUnlockMemberOnAllPageSQL, TopicName.SQL_ACCOUNT, listMember);
                    }
                }

                return returnValue;
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
                return ErrorCodes.Exception;
            }
        }
        

        public static Task<AccountSearch> GetAccountESAsync(long officerId)
        {
            return StoreFactory.Create<AccountSearchStore>().GetByIdAsync(officerId);
        }
        public static Task<PagingDataResult<OfficialAccount>> MyOfficialAsync(string keyword, long userId, int pageIndex, int pageSize)
        {
            return StoreFactory.Create<AccountCacheStore>().MyOfficialAsync(keyword, userId, pageIndex, pageSize);
        }

        public static async Task<PagingDataResult<OfficialAccount>> MyPagesAsync(SearchAccountEntity search)
        {
            if (string.IsNullOrEmpty(search.Keyword))
            {
                return await StoreFactory.Create<AccountCacheStore>().MyPagesAsync(search);
            }
            else
            {
                var returnValue = new PagingDataResult<OfficialAccount>();
                var resultSearch = await StoreFactory.Create<AccountSearchStore>().SearchAsync(search);
                if(resultSearch!=null && resultSearch?.Data != null)
                {
                    returnValue.Total = resultSearch.Total;
                    returnValue.Data = resultSearch?.Data?.Select(p => Mapper<OfficialAccount>.Map(p, new OfficialAccount()))?.ToList();

                    var listProfile = await StoreFactory.Create<AccountCacheStore>().GetListProfileAsync(returnValue.Data?.Select(p=>p.Id.ToString())?.ToList());

                    returnValue.Data.ForEach(p=> {
                        p.IsOwner = (search.CreatedBy.ToString().Equals(p.CreatedBy));
                        p.ProfileUrl = listProfile?.Where(pr => pr.Id == p.Id)?.FirstOrDefault()?.ProfileUrl;
                    });
                }
                return returnValue;
            }
        }

        public static Task<List<OfficialAccount>> GetListPageOwnerAsync(long ownerId)
        {
            return StoreFactory.Create<AccountCacheStore>().GetListPageOwnerAsync(ownerId);
        }

        
        public static Task<bool> CheckAccountMember(long officialId, long memberId)
        {
            return StoreFactory.Create<AccountCacheStore>().CheckAccountMember(officialId, memberId);
        }

        public static Task<bool> CheckOwnerExist(long userId)
        {
            return StoreFactory.Create<AccountCacheStore>().CheckOwnerExist(userId);
        }
        public static Task<long[]> ListOwnerAsync(long userId)
        {
            return StoreFactory.Create<AccountCacheStore>().ListOwnerAsync(userId);
        }
        public async static Task<bool> ApprovedOnAppAsync(long userId, int role)
        {
            var returnValue = false;
            try
            {
                var ck = false;
                if (role == AppSettings.Current.ChannelConfiguration.ApiUserKingHub.RoleVip)
                {
                    ck = true;
                }
                else
                {
                    var isMember = await StoreFactory.Create<AccountCacheStore>().IsMemberAsync(userId);
                    if (!isMember)
                    {
                        ck = true;
                    }
                }

                if (ck)
                {
                    using (var client = new HttpClient())
                    {
                        client.DefaultRequestHeaders.Add("token", AppSettings.Current.ChannelConfiguration.ApiUserKingHub.TokenUpateRole);

                        var obj = new { user_id = userId, role_id = role };;
                        var response = await client.PostAsync(AppSettings.Current.ChannelConfiguration.ApiUserKingHub.UrlUpdateRole, new StringContent(Json.Stringify(obj), Encoding.UTF8, "application/json"));

                        if (!response.IsSuccessStatusCode)
                        {
                            response.EnsureSuccessStatusCode();
                        }

                        if (response?.StatusCode == System.Net.HttpStatusCode.OK)
                        {
                            var stringContent = await response.Content?.ReadAsStringAsync();
                            var userData = Json.Parse<UpdateRoleRespone>(stringContent);
                            if (userData != null && userData.Status == 1)
                            {
                                returnValue = true;
                            }
                        }
                        else
                        {
                            Logger.Sensitive("Update role => " + await response?.Content?.ReadAsStringAsync(), new { userId , role});
                        }
                    }
                }
                else
                {
                    returnValue = true;
                }
            }
            catch (Exception ex)
            {
                Logger.Sensitive(ex, ex.Message);
                //Logger.Debug(ex, ex.Message);
            }
            return returnValue;
        }

        public static async Task<DataResponse> GetUserInfo(long userId)
        {
            var returnValue = default(DataResponse);
            try
            {
                using (var client = new HttpClient())
                {
                    client.Timeout = TimeSpan.FromSeconds(AppSettings.Current.ChannelConfiguration.HttpClientTimeOut);
                    string path = AppSettings.Current.ChannelConfiguration.LoginSetting.ApiUrl + userId;
                    var response = await client.GetAsync(path);
                    if (!response.IsSuccessStatusCode)
                    {
                        response.EnsureSuccessStatusCode();
                    }
                    if (response?.StatusCode == System.Net.HttpStatusCode.OK)
                    {
                        var userData = Json.Parse<LoginResponse>(await response.Content?.ReadAsStringAsync());
                        if (userData != null && userData.Data != null && !string.IsNullOrEmpty(userData.Data.User_id))
                        {
                            returnValue = userData.Data;
                        }
                        else
                        {
                            Logger.Debug("Get user info => " + await response?.Content?.ReadAsStringAsync());
                        }
                    }
                    else
                    {
                        Logger.Debug("Get user info => " + await response?.Content?.ReadAsStringAsync());
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, "Get user info => " + ex.Message);
            }
            return returnValue;
        }

        public static async Task<object> GetUserInfoV2(long userId)
        {
            var returnValue = default(object);
            try
            {
                using (var client = new HttpClient())
                {
                    client.Timeout = TimeSpan.FromSeconds(AppSettings.Current.ChannelConfiguration.HttpClientTimeOut);
                    string path = AppSettings.Current.ChannelConfiguration.LoginSetting.ApiUrl + userId;
                    var response = await client.GetAsync(path);
                    if (!response.IsSuccessStatusCode)
                    {
                        response.EnsureSuccessStatusCode();
                    }
                    if (response?.StatusCode == System.Net.HttpStatusCode.OK)
                    {
                        returnValue = Json.Parse<object>(await response.Content?.ReadAsStringAsync());
                    }
                    else
                    {
                        Logger.Debug("GetUserInfoV2=> " + await response?.Content?.ReadAsStringAsync());
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, "GetUserInfoV2=> " + ex.Message);
            }
            return returnValue;
        }

        public static Task<PagingDataResult<AccountMemberInfo>> GetMembersAsync(long? officerId, long accountId, AccountMemberRole? role, string keyWord, int pageIndex, int pageSize)
        {
            return StoreFactory.Create<AccountCacheStore>().GetMembersAsync(officerId, accountId, role, keyWord, pageIndex, pageSize);
        }
        public static Task<List<AccountMemberInfo>> GetMembersAsync(long officerId)
        {
            return StoreFactory.Create<AccountCacheStore>().GetMembersAsync(officerId);
        }

        public static Task<List<AccountMemberInfo>> ApproversAsync(long officerId)
        {
            return StoreFactory.Create<AccountCacheStore>().ApproversAsync(officerId);
        }
        public static async Task<ErrorCodes> ChangeOwnerAsync(long officerId, long oldOwnerId, long newOwnerId, long userId)
        {
            var returnValue = ErrorCodes.UpdateError;
            try
            {
                returnValue = await StoreFactory.Create<AccountCacheStore>().ChangeOwnerAsync(officerId, oldOwnerId, newOwnerId, userId);
                if (returnValue == ErrorCodes.Success)
                {
                    var queueData = new QueueData<long, long, long, object>
                    {
                        Data1 = officerId,
                        Data2 = oldOwnerId,
                        Data3 = newOwnerId
                    };

                    await Function.AddToQueue(ActionName.ChangePageOwnerES, TopicName.ES_ACCOUNT, queueData);
                    await Function.AddToQueue(ActionName.ChangePageOwnerSQL, TopicName.SQL_ACCOUNT, queueData);

                    //update owner on Adtech
                    await Function.AddToQueue(ActionName.ChangePageOwner, TopicName.SYNC_USERINFO, queueData);
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
                returnValue = ErrorCodes.Exception;
            }
            return returnValue;
        }
        #endregion

        #region Sync UserInfo
        public static async Task<AccountSearch> GetUserInfoAndUpdateAsync(long userId)
        {
            var userAccount = default(AccountSearch);
            try
            {
                userAccount = await GetAccountSearchByIdAsync(userId);
                if (userAccount != null && userAccount.Type == (int)AccountType.Official)
                {
                    using (var client = new HttpClient())
                    {
                        client.Timeout = TimeSpan.FromSeconds(AppSettings.Current.ChannelConfiguration.HttpClientTimeOut);
                        string path = AppSettings.Current.ChannelConfiguration.LoginSetting.ApiUrl + userId;
                        var response = await client.GetAsync(path);
                        if (!response.IsSuccessStatusCode)
                        {
                            response.EnsureSuccessStatusCode();
                        }
                        if (response?.StatusCode == System.Net.HttpStatusCode.OK)
                        {
                            var content = await response.Content.ReadAsStringAsync();
                            var userData = Json.Parse<LoginResponse>(content);
                            if (userData != null && userData.Status == 1 && userData.Data != null && !string.IsNullOrEmpty(userData.Data.User_id))
                            {
                                userAccount.Avatar =  string.IsNullOrEmpty(userData.Data.Avatar) ? userAccount.Avatar : userData.Data.Avatar;
                                userAccount.Email = string.IsNullOrEmpty(userData.Data.Email) ? userAccount.Email : userData.Data.Email;
                                userAccount.FullName = string.IsNullOrEmpty(userData.Data.Full_name) ? userAccount.FullName : userData.Data.Full_name;
                                userAccount.Mobile = string.IsNullOrEmpty(userData.Data.Phone) ? userAccount.Mobile : userData.Data.Phone;
                                userAccount.UserName = string.IsNullOrEmpty(userData.Data.Username) ? userAccount.UserName : userData.Data.Username.ToLower(); 

                                DateTime? birthDay = null;
                                if (!string.IsNullOrEmpty(userData.Data.Birthday))
                                {
                                    try
                                    {
                                        birthDay = DateTime.ParseExact(userData.Data.Birthday, AppSettings.Current.ChannelConfiguration.BirthdayFormat, CultureInfo.InvariantCulture);
                                    }
                                    catch (Exception ex)
                                    {
                                        Logger.Error(ex, ex.Message);
                                    }
                                }

                                var profile = new UserProfile
                                {
                                    Address = userData.Data.Address,
                                    BirthDay = birthDay,
                                    City = userData.Data.City,
                                    Id = long.Parse(userData.Data.User_id),
                                    Gender = 1
                                };
                                var result1 = await UpdateAsync(userAccount, profile);
                                var result2 = await SaveProfileAsync(profile, Mapper<Account>.Map(userAccount, new Account()));

                                //returnValue = true;
                            }
                        }
                        else
                        {
                            Logger.Sensitive<long>("Get user info => " + await response?.Content?.ReadAsStringAsync(), userId);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Sensitive(ex, ex.Message);
            }
            return userAccount;
        }

        public static Task<bool> IsModifier(long officerId, long userId)
        {
            return StoreFactory.Create<AccountCacheStore>().IsModifier(officerId, userId);
        }

        public static async Task<bool> ChangePageOwnerAsync(long userId, long oldOwnerId, long newOwnerId)
        {
            var returnValue = false;
            var content = new FormUrlEncodedContent(new
            {
                userid = userId.ToString(),
                old_ownerid = oldOwnerId.ToString(),
                new_ownerid = newOwnerId.ToString()
            }.ConvertToKeyValuePair());
            string query = await content?.ReadAsStringAsync();

            try
            {
                using (var client = new HttpClient())
                {
                    client.Timeout = TimeSpan.FromSeconds(AppSettings.Current.ChannelConfiguration.HttpClientTimeOut);
                    string path = AppSettings.Current.ChannelConfiguration.KingHubBoardApiSetting.ApiUrl + "/" + BoardAction.UpdateOwner + "?" + query;
                    var response = await client.GetAsync(path);
                    if (!response.IsSuccessStatusCode)
                    {
                        response.EnsureSuccessStatusCode();
                    }
                    if (response?.StatusCode == HttpStatusCode.OK)
                    {
                        var result = Json.Parse<KingHubBoardResponse<object>>(await response.Content?.ReadAsStringAsync());
                        if (result?.Status == 1)
                            returnValue = true;
                        else
                            Logger.Sensitive("ChangePageOwner => query: " + query + "; output:" + await response?.Content?.ReadAsStringAsync(), new { userId , oldOwnerId , newOwnerId });
                    }
                    else
                    {
                        Logger.Sensitive("ChangePageOwner => query: " + query + "; output:" + await response?.Content?.ReadAsStringAsync(), new { userId, oldOwnerId, newOwnerId });
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Sensitive(ex, ex.Message);
            }
            return returnValue;
        }

        public static async Task<bool> InsertPageOwnerToAppAsync(long userId, long ownerId, byte? userClass, string fullName)
        {
            var returnValue = false;
            var content = new FormUrlEncodedContent(new
            {
                userid = userId.ToString(),
                ownerid = ownerId.ToString(),
                user_type = userClass?? (byte)AccountClass.Waiting,
                page_name = fullName
            }.ConvertToKeyValuePair());
            string query = await content?.ReadAsStringAsync();

            try
            {
                using (var client = new HttpClient())
                {
                    client.Timeout = TimeSpan.FromSeconds(AppSettings.Current.ChannelConfiguration.HttpClientTimeOut);
                    string path = AppSettings.Current.ChannelConfiguration.KingHubBoardApiSetting.ApiUrl + "/" + BoardAction.InsertUpdateOwner + "?" + query;
                    var response = await client.GetAsync(path);

                    if (!response.IsSuccessStatusCode)
                    {
                        response.EnsureSuccessStatusCode();
                    }

                    if (response?.StatusCode == HttpStatusCode.OK)
                    {
                        var result = Json.Parse<KingHubBoardResponse<object>>(await response.Content?.ReadAsStringAsync());
                        if (result?.Status == 1)
                            returnValue = true;
                        else
                            Logger.Sensitive("InsertOwnerPage => query: " + query + "; output:" + await response?.Content?.ReadAsStringAsync(), new
                            {
                                userId,
                                ownerId,
                                userClass
                            });
                    }
                    else
                    {
                        Logger.Sensitive("InsertOwnerPage => query: " + query + "; output:" + await response?.Content?.ReadAsStringAsync(), new
                        {
                            userId,
                            ownerId,
                            userClass
                        });
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Sensitive(ex, ex.Message);
            }
            return returnValue;
        }
        #endregion

        public static Task<long?> CheckMobileAsync(string mobile)
        {
            return StoreFactory.Create<AccountCacheStore>().CheckMobileAsync(mobile);
        }

        public static async Task<bool> SendCRMAsync(long userId, string userCurrentName, string userFullName, string phoneNumber, string email, AccountType? accountType, List<Account> pages)
        {
            var returnValue = false;
            var status = accountType == AccountType.Personal ? "Đã duyệt" : "Chưa duyệt";

            var listKenh = pages?.Select(p => new
            {
                link_kenh = string.Format("https://lotus.vn/w/profile/{0}.htm", p.Id),
                ten_kenh = p.FullName,
                tinh_trang = new[] { new { value = p.Status == (int)AccountStatus.Actived ? "Đã duyệt" : "Chưa duyệt" } }
            });

            var listKenhDuyetGanNhat = pages?.Where(p=>p.Status == (int)AccountStatus.Actived)?.Select(p => new
            {
                key = "Link page",
                value = string.Format("https://lotus.vn/w/profile/{0}.htm", p.Id)
            })?.Take(1);

            try
            {
                string body = @"{
                        ""table"" : ""data_customer"",
                        ""mappingBy"" : [ 
                            ""lotus_id"", 
                            ""phones"", 
                            ""emails""
                        ],
                        ""mergingFieldBy"" : {
                            ""kenh_lotus"" : [ 
                                ""link_kenh""
                            ]
                        },
                        ""data"" : [ 
                            {
                                ""fields"" : [ 
                                    {
                                        ""key"" : ""phones"",
                                        ""value"" : [ 
                                            {
                                                ""value"" : """ + phoneNumber + @"""
                                            }
                                        ]
                                    }, 
                                    {
                                        ""key"" : ""lotus_id"",
                                        ""value"" : """ + userId + @"""
                                    }, 
                                    {
                                        ""key"" : ""kenh_lotus"",
                                        ""value"" : "+Json.Stringify(listKenh) +@"
                                    }
                                ],
                                ""activity"" : {
                                    ""object_name"" : ""@Duyệt Page trên Lotus"",
                                    ""object_type"" : ""customer"",
                                    ""object_type_label"" : ""Đối tác"",
                                    ""action"" : ""duyet-page-cua-doi-tac"",
                                    ""action_label"" : ""Duyệt Page của đối tác"",
                                    ""object_tag"" : "+Json.Stringify(listKenhDuyetGanNhat) +@"
                                }
                            }
                        ]
                    }

                ";

                //var obj = new
                //{
                //    table = "data_customer",
                //    mappingBy = new[] {"lotus_id","phones","emails" },
                //    mergingFieldBy = new { }
                //}
                using (var httpClient = new HttpClient())
                {
                    var now = Utility.ConvertToTimestamp(DateTime.Now).ToString();
                    httpClient.DefaultRequestHeaders.Add("cb-access-key", "F3y85bR0352d8447303db0e37ebb65822fd3daf2RWWqCKap");
                    httpClient.DefaultRequestHeaders.Add("cb-project-token", "c2643097-444c-4a54-9e7e-c09e5d6b3d0d");
                    httpClient.DefaultRequestHeaders.Add("cb-access-sign", Encryption.HMACSHA512Encode(now + "c2643097-444c-4a54-9e7e-c09e5d6b3d0d", "326f1fe1f61a80f63b9d782e0a8b363483d247a4"));
                    httpClient.DefaultRequestHeaders.Add("cb-access-timestamp", now);

                    //var p = JsonConvert.DeserializeObject(data.ToString());
                    //var jsonObject = data.ToString();

                    var content = new StringContent(body, Encoding.UTF8, "application/json");
                    var result = await httpClient.PostAsync(AppSettings.Current.ChannelConfiguration.UrlUpdateInfoCrm, content);

                   
                    if (result.IsSuccessStatusCode)
                        returnValue = true;
                    else
                        returnValue = false;
                }
            }
            catch (Exception ex)
            {
                Logger.Sensitive(ex, ex.Message);
                returnValue = false;
            }
            return returnValue;
        }

        public static async Task<ErrorCodes> SetProfileIsPageOnAppAsync(long officerId, long userId, long memberId, bool isEnable)
        {
            var returnValue = ErrorCodes.UnknowError;
            var accountMember = default(Account);
            try
            {
                returnValue = await StoreFactory.Create<AccountCacheStore>().SetProfileIsPageOnAppAsync(officerId, userId, memberId, isEnable, new Action<Account>((data)=>
                {
                    accountMember = data;
                }));

                if (returnValue == ErrorCodes.Success)
                {
                    //Add vào queue bắn sang kinghub
                    await Function.AddToQueue(ActionName.SetProfileIsPageOnApp, TopicName.SYNC_USERINFO, new QueueData<long, long, string, bool>{
                        Data1 = officerId,
                        Data2 = memberId,
                        Data3 = accountMember?.Mobile,
                        Data4 = isEnable
                    });

                    await Function.AddToQueue(ActionName.SetProfileIsPageOnAppSQL, TopicName.SQL_ACCOUNT, memberId);
                }
            }
            catch(Exception ex)
            {
                Logger.Error(ex, ex.Message);
            }
            return returnValue;
        }

        public static async Task<bool> PushSettingProfileToAppAsync(long officerId, long userId, string mobile, bool isEnable)
        {
            var returnValue = false;
            try
            {
                using(var client = new HttpClient())
                {
                    client.Timeout = TimeSpan.FromSeconds(AppSettings.Current.ChannelConfiguration.HttpClientTimeOut);

                    var obj = new
                    {
                        email_or_phone = mobile,
                        page_id = officerId,
                        active = isEnable
                    }.ConvertToKeyValuePair();

                    var content = new FormUrlEncodedContent(obj);

                    var url = AppSettings.Current.ChannelConfiguration.ApiUserKingHub.UrlMapProfile; //"http://172.26.6.30:8000/analytics/kinghub-user-validation/page-validation/pages-mapping";

                    Logger.Debug("PushSettingProfileToAppAsync(Adtech) Url => " + url);
                    Logger.Debug("PushSettingProfileToAppAsync(Adtech) Data => " + Json.Stringify(obj));

                    var response = await client.PostAsync(url, content);

                    if (!response.IsSuccessStatusCode)
                    {
                        response.EnsureSuccessStatusCode();
                    }

                    if (response.StatusCode == HttpStatusCode.OK)
                    {
                        var dataResponse = Json.Parse<dynamic>(await response.Content.ReadAsStringAsync());
                        if(dataResponse != null && dataResponse.status == 1)
                        {
                            returnValue = true;
                        }
                    }
                    else
                    {
                        Logger.Sensitive("PushSettingProfileToAppAsync(Adtech) => " + await response?.Content?.ReadAsStringAsync(), new {

                            officerId,
                            userId,
                            mobile,
                            isEnable
                        });
                    }
                }
            }
            catch(Exception ex)
            {
                Logger.Sensitive(ex, ex.Message);
            }
            return returnValue;
        }

        public static async Task<bool> UpdateProfileUrlAsync(UserProfile profile)
        {
            var returnValue = false;
            try
            {
                returnValue = await StoreFactory.Create<AccountCacheStore>().UpdateProfileUrlAsync(profile);
                if (returnValue)
                {
                    var res = await StoreFactory.Create<AccountDataStore>().UpdateProfileUrlAsync(profile);
                    if(res == false)
                    {
                        await Function.AddToQueue(ActionName.UpdateProfileUrlSql, TopicName.SQL_ACCOUNT, profile.Id);
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
            }
            return returnValue;
        }

        public static Task<long> GetAccountInNewsAsync(long newsId)
        {
            return StoreFactory.Create<AccountCacheStore>().GetAccountInNewsAsync(newsId);
        }

        public static async Task<KinghubApiResponseData> GetSessionIdAsync(string mobile)
        {
            var returnValue = default(KinghubApiResponseData);
            try
            {
                var accesstoken = await StoreFactory.Create<AccountCacheStore>().GetAccessTokenAsync(mobile);
                using(var client = new HttpClient())
                {
                    client.Timeout = TimeSpan.FromSeconds(AppSettings.Current.ChannelConfiguration.HttpClientTimeOut);

                    var obj = new
                    {
                        access_token = accesstoken
                    };
                    var content = new FormUrlEncodedContent(obj.ConvertToKeyValuePair());

                    var url = AppSettings.Current.ChannelConfiguration.AuthenKingHubSetting.ApiUrl;

                    Logger.Debug("GetSessionIdAsync(KingHub) Url => " + url);
                    Logger.Debug("GetSessionIdAsync(KingHub) Data => " + Json.Stringify(obj));

                    var response = await client.PostAsync(url, content);

                    if (!response.IsSuccessStatusCode)
                    {
                        response.EnsureSuccessStatusCode();
                    }

                    if (response.StatusCode == HttpStatusCode.OK)
                    {
                        var dataResponse = Json.Parse<KinghubApiResponse>(await response.Content.ReadAsStringAsync());
                        if (dataResponse != null && dataResponse.Status == 1)
                        {
                            returnValue = dataResponse.Data;
                        }
                    }
                    else
                    {
                        Logger.Sensitive("GetSessionIdAsync(KingHub) => " + await response?.Content?.ReadAsStringAsync(), new
                        {
                            accesstoken
                        });
                    }
                }
            }
            catch(Exception ex)
            {
                Logger.Error(ex, ex.Message);
            }
            return returnValue;
        }

        public static async Task<bool> UpdateAccessTokenAsync(AccountSearch account)
        {
            var returnValue = false;
            try
            {
                var acc = Mapper<Account>.Map(account, new Account());
                await StoreFactory.Create<AccountCacheStore>().UpdateAsync(acc);
                await StoreFactory.Create<AccountSearchStore>().UpdateAsync(account);
                await StoreFactory.Create<AccountDataStore>().UpdateAccessTokenAsync(acc);
                returnValue = true;
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
            }
            return returnValue;
        }
    }
}
