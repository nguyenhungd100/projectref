﻿using ChannelVN.IMS2.Core.Entities;
using ChannelVN.IMS2.Core.Entities.Distribution;
using ChannelVN.IMS2.Core.Entities.Media;
using ChannelVN.IMS2.Core.Entities.Security;
using ChannelVN.IMS2.Core.Entities.Videos;
using ChannelVN.IMS2.Core.Models.CacheStore.Videos;
using ChannelVN.IMS2.Core.Models.DataStore.Videos;
using ChannelVN.IMS2.Core.Models.ExternalAPI;
using ChannelVN.IMS2.Core.Models.SearchStore.Videos;
using ChannelVN.IMS2.Core.Repositories.Constants;
using ChannelVN.IMS2.Core.Repositories.Functions;
using ChannelVN.IMS2.Foundation.Common;
using ChannelVN.IMS2.Foundation.Common.Configuration;
using ChannelVN.IMS2.Foundation.Data;
using ChannelVN.IMS2.Foundation.Data.Factories;
using ChannelVN.IMS2.Foundation.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using static ChannelVN.IMS2.Core.Entities.ErrorCode.ErrorMapping;

namespace ChannelVN.IMS2.Core.Repositories
{
    public class VideoPlaylistRepository : Repository
    {
        public static async Task<ErrorCodes> AddAsync(VideoPlaylistSearch data, string clientIP)
        {
            var returnValue = ErrorCodes.AddError;
            try
            {
                returnValue = await StoreFactory.Create<VideoPlaylistCacheStore>().AddAsync(data);
                if (returnValue == ErrorCodes.Success)
                {
                    var res = await StoreFactory.Create<VideoPlaylistSearchStore>().AddAsync(data);
                    if (!res)
                    {
                        //Push queue
                        await Function.AddToQueue(ActionName.AddVideoPlaylistES, TopicName.ES_PLAYLIST, data.Id);
                    }

                    res = await StoreFactory.Create<VideoPlaylistDataStore>().AddAsync(data);
                    if (!res)
                    {
                        //Push queue
                        await Function.AddToQueue(ActionName.AddVideoPlaylistSQL, TopicName.SQL_PLAYLIST, data.Id);
                    }

                    //if (data.Status == (int)NewsStatus.Archive && AppSettings.Current.Distribution.Automation.Enabled)
                    //{
                    //    await Function.AddToQueue(ActionName.AutoShareVideoPlaylist, TopicName.AUTOSHAREVIDEOPLAYLIST, new QueueData<long, string, long[], object>
                    //    {
                    //        Data1 = data.Id,
                    //        Data2 = clientIP
                    //    });                       
                    //}
                }
                return returnValue;
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
                return returnValue;
            }
        }

        public static async Task<ErrorCodes> UpdateAsync(VideoPlaylistSearch data, long officerId, string clientIP, string sessionId)
        {            
            try
            {
                var returnValue = await StoreFactory.Create<VideoPlaylistCacheStore>().UpdateAsync(data, officerId, new Action<int>(async (oldStatus) =>
                {
                    if (AppSettings.Current.Distribution.Automation.Enabled)
                    {
                        if (oldStatus == (int)NewsStatus.Published)
                        {
                            var action = Media.Action.update;
                            await Function.AddToQueue(ActionName.AutoShareVideoPlaylist, TopicName.AUTOSHAREVIDEOPLAYLIST, new QueueData<long, string, string, Media.Action>
                            {
                                Data1 = data.Id,
                                Data2 = clientIP,
                                Data3 = sessionId,
                                Data4 = action
                            });

                            await Function.AddToQueue(ActionName.PushChangeNotify, TopicName.PUSHBOARDTOCHANNEL, data.Id);
                        }
                    }
                }));
                if (returnValue == ErrorCodes.Success)
                {
                    await Function.AddToQueue(ActionName.UpdateVideoPlaylistES, TopicName.ES_PLAYLIST, data.Id);
                    await Function.AddToQueue(ActionName.UpdateVideoPlaylistSQL, TopicName.SQL_PLAYLIST, data.Id);                    
                }
                return returnValue;
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
                return ErrorCodes.Exception;
            }            
        }


        public static async Task<PagingDataResult<VideoInPlaylistSearchReturn>> SearchVideoAsync(SearchNews data, long userId)
        {
            try
            {
                var dataReturn = new PagingDataResult<VideoInPlaylistSearchReturn>();
                if (string.IsNullOrEmpty(data.Keyword) && data.OrderBy != SearchNewsOrderBy.DescDistributionDate
                    && data.OrderBy != SearchNewsOrderBy.AscDistributionDate && data.OrderBy != SearchNewsOrderBy.DescModifiedDate && data.OrderBy != SearchNewsOrderBy.AscModifiedDate)
                {
                    if (data.OfficerId > 0)
                    {
                        if (!await SecurityRepository.CheckAccountMember(data.OfficerId ?? 0, userId))
                        {
                            return dataReturn;
                        }
                    }
                    else
                    {
                        data.OfficerId = userId;
                    }
                    dataReturn = await StoreFactory.Create<VideoCacheStore>().SearchVideoAsync(data, userId, new Action<string>(async (tempKey) =>
                    {
                        await Function.AddToQueue(ActionName.SetExpire, TopicName.DELETE_INDEX_TEMP, tempKey);
                    }));
                }
                else
                {
                    var dataSearch = await StoreFactory.Create<VideoSearchStore>().SearchAsync(data, userId);
                    if (dataSearch?.Data?.Count() > 0)
                    {
                        var listData = await VideoRepository.GetListByIdsAsync(dataSearch.Data.Select(p => p.Id.ToString()).ToList());
                        if (listData != null)
                        {
                            dataSearch.Data = listData.Select(p => Mapper<VideoSearch>.Map(p, dataSearch.Data.Where(c => c.Id == p.Id).FirstOrDefault())).ToList();
                        }
                        dataReturn.Total = dataSearch.Total;
                        dataReturn.Data = dataSearch.Data.Select(p => Mapper<VideoInPlaylistSearchReturn>.Map(p, new VideoInPlaylistSearchReturn())).ToList();
                    }
                }
                return dataReturn;
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
                return null;
            }
        }

        public static Task<PagingDataResult<VideoPlaylistSearch>> GetListRelationAsync(string videoPlaylistIds, int? pageIndex, int? pageSize)
        {
            return StoreFactory.Create<VideoPlaylistSearchStore>().GetListRelationAsync(videoPlaylistIds, pageIndex, pageSize);
        }

        public static async Task<PagingDataResult<VideoInPlaylistSearchReturn>> GetListVideoAsync(List<string> videoIds, int? pageIndex, int? pageSize)
        {
            var returnValue = new PagingDataResult<VideoInPlaylistSearchReturn>();
            try
            {
                var listId = videoIds?.Skip(((pageIndex ?? 1) - 1) * (pageSize ?? 20))?.Take(pageSize ?? 20)?.ToList();
                var listSearch = await StoreFactory.Create<VideoCacheStore>().GetListByIdsAsync(listId);
                returnValue.Total = videoIds?.Count() ?? 0;
                returnValue.Data = listSearch.Select(p => Mapper<VideoInPlaylistSearchReturn>.Map(p, new VideoInPlaylistSearchReturn())).ToList();
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
            }
            return returnValue;
        }

        public static Task<PagingDataResult<Video>> GetVideoByPlaylistIdAsync(long playlistId, int? pageIndex, int? pageSize)
        {
            return StoreFactory.Create<VideoCacheStore>().GetListByPlaylistIdAsync(playlistId, pageIndex, pageSize);
        }

        public static Task<long> GetCountByStatusAsync(int status, long officialId, long userId)
        {
            return StoreFactory.Create<VideoPlaylistSearchStore>().GetCountByStatusAsync(status, officialId, userId);
        }

        public static Task<PagingDataResult<VideoPlaylistSearch>> GetListDistributionAsync(SearchNews searchEntity, long userId)
        {
            return StoreFactory.Create<VideoPlaylistSearchStore>().GetListDistributionAsync(searchEntity, userId);
        }


        public static async Task<ErrorCodes> UpdateStatusV2Async(long id, NewsStatus status, long userId, string clientIP, string sessionId)
        {
            var returnCode = ErrorCodes.BusinessError;
            try
            {
                returnCode = await StoreFactory.Create<VideoPlaylistCacheStore>().UpdateStatusV2Async(id, status, userId, new Action<VideoPlaylistSearch, int>(async (data, oldStatus) =>
                {
                    //auto phan phoi khi action publish     
                    if (AppSettings.Current.Distribution.Automation.Enabled)
                    {
                        if (status == NewsStatus.Published
                         || (data.Status == (int)NewsStatus.Remove && oldStatus == (int)NewsStatus.Published)
                         || (data.Status == (int)NewsStatus.Draft && oldStatus == (int)NewsStatus.Published))
                        {
                            var action = Media.Action.add;
                            if (status == NewsStatus.Published && oldStatus == (int)NewsStatus.Published)
                            {
                                action = Media.Action.update;
                            }
                            if (status == NewsStatus.Remove)
                            {
                                action = Media.Action.deletebymedia;
                            }
                            await Function.AddToQueue(ActionName.AutoShareVideoPlaylist, TopicName.AUTOSHAREVIDEOPLAYLIST, new QueueData<long, string, string, Media.Action>
                            {
                                Data1 = data.Id,
                                Data2 = clientIP,
                                Data3= sessionId,
                                Data4=action
                            });
                        }
                    }
                }));

                if (returnCode == ErrorCodes.Success)
                {
                    await Function.AddToQueue(ActionName.UpdateVideoPlaylistStatusES, TopicName.ES_PLAYLIST, id);
                    await Function.AddToQueue(ActionName.UpdateVideoPlaylistStatusSQL, TopicName.SQL_PLAYLIST, id);
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
                returnCode = ErrorCodes.Exception;
            }
            return returnCode;
        }

        public static async Task<bool> AutoShare(VideoPlaylistSearch data, string clientIP, string sessionId, Media.Action action)
        {
            var result = false;
            try
            {
                var listItem = Json.Parse<PublishData>(data.PublishData);
                var itemMediaUnit = new List<ItemMediaUnit>();
                if (listItem != null && listItem.Items?.Count > 0)
                {
                    foreach (var item in listItem.Items)
                    {
                        itemMediaUnit.Add(new ItemMediaUnit
                        {
                            Id = item.Id,
                            Title = item.Title,
                            ContentType = item.ContentType ?? item.Content_Type,
                            Duration = item.Duration,
                            Link = item.Link,
                            Thumb = item.Thumb,
                            Width = item.Width,
                            Height = item.Height,
                            TypeMedia = item.Type,
                            Position = item.Position,
                            IsPlay = item.IsPlay,
                            CreatedDate = data.VideoInPlaylist?.Where(p => p.VideoId.ToString().Equals(item.Id))?.FirstOrDefault()?.PlayOnTime ?? DateTime.Now,
                            Description = item.Description
                        });
                    }
                    var user_created = await SecurityRepository.GetByIdAsync(long.Parse(data.NewsInAccount?.LastOrDefault()?.AccountId.ToString() ?? data.CreatedBy));
                    var payloadDataShare = new PayloadDataShare
                    {
                        DistributionId = AppSettings.Current.Distribution.Automation.DistributorId,
                        TemplateId = 0,
                        Title = listItem.Title,
                        Type = NewsType.Video,
                        MetaDataShared = new List<MetaDataShared>
                    {
                        new MetaDataShared
                        {
                            ObjectId = data.Id,
                            Title = listItem.Caption,
                            Name = data.Name,
                            ZoneId = 0,
                            DistributionDate = data.DistributionDate,
                            CardType = (int)CardType.PlaylistVideo,
                            Frameid = 0,
                            Tags = data.Tags,
                            UserId = user_created?.Id??0,
                            UserName = user_created?.UserName,
                            FullName = user_created?.FullName,
                            AuthorAvatar = user_created?.Avatar,
                            IsDeleted = data.Status == (int)NewsStatus.Remove||data.Status == (int)NewsStatus.Draft ? 1 : (data.Status == (int)NewsStatus.Archive ? 2 : 0),
                            ItemMediaUnit = itemMediaUnit,
                            Cover = Json.Parse<Cover>(data.Cover),
                            Description = data.Description,
                            BoardIds = null,
                            CaptionExt = listItem.CaptionExt,
                            CommentMode = data.CommentMode
                        }
                    }
                    };

                    var distribution = await DistributionRepository.GetByIdAsync(payloadDataShare.DistributionId);
                    var newsDistribution = data.NewsDistribution?.Where(p => p.DistributorId == payloadDataShare.DistributionId).FirstOrDefault();
                    var itemStreamDistribution = new ItemStreamDistribution()
                    {
                        Id = newsDistribution != null ? newsDistribution.ItemStreamId : Generator.ItemStreamDistributionId(),
                        PublishStatus = data.Status == (int)NewsStatus.Published ? (int)DistributionPublishStatus.Accept : (int)DistributionPublishStatus.Reject,
                        CreatedDate = DateTime.Now,
                        ItemStreamTempId = payloadDataShare.TemplateId,
                        Title = payloadDataShare.Title,
                        MetaData = Json.Stringify(payloadDataShare.MetaDataShared),
                        Type = payloadDataShare.Type
                    };
                    result = await DistributionRepository.ShareVideoPlaylistAsync(itemStreamDistribution, distribution, payloadDataShare, clientIP, sessionId, action);
                    if (result)
                    {
                        //insert log
                        await Function.AddToQueue(ActionName.InsertLog, TopicName.LOGACTION, new LogActionEntity
                        {
                            ObjectId = data.Id.ToString(),
                            Account = data.ModifiedBy,
                            CreatedDate = DateTime.SpecifyKind(DateTime.Now, DateTimeKind.Unspecified),
                            Ip = clientIP,
                            Message = "Success.",
                            Title = data.Name,
                            SourceId = data.ModifiedBy,
                            ActionTypeDetail = (int)ActionType.Share,
                            ActionText = ActionType.Share.ToString(),
                            Type = (int)LogContentType.PlayList
                        });
                    }
                }
                else
                {
                    result = true;
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
                result = false;
            }

            return result;
        }
        public static Task<VideoPlaylist> GetByIdAsync(long id)
        {
            return StoreFactory.Create<VideoPlaylistCacheStore>().GetByIdAsync(id);
        }

        public static async Task<PagingDataResult<VideoPlaylistSearchReturn>> SearchAsync(SearchNews data, long userId, bool isGetDistribution)
        {
            try
            {
                var dataReturn = new PagingDataResult<VideoPlaylistSearchReturn>();
                if (isGetDistribution == false && string.IsNullOrEmpty(data.Keyword) && data.OrderBy != SearchNewsOrderBy.DescDistributionDate
                    && data.OrderBy != SearchNewsOrderBy.AscDistributionDate && data.OrderBy != SearchNewsOrderBy.DescModifiedDate && data.OrderBy != SearchNewsOrderBy.AscModifiedDate)
                {
                    dataReturn = await StoreFactory.Create<VideoPlaylistCacheStore>().SearchAsync(data, userId, new Action<string>(async (tempKey) =>
                    {
                        await Function.AddToQueue(ActionName.SetExpire, TopicName.DELETE_INDEX_TEMP, tempKey);
                    }));
                }
                else
                {
                    var dataSearch = await StoreFactory.Create<VideoPlaylistSearchStore>().SearchAsync(data, userId, isGetDistribution);
                    if (dataSearch?.Data?.Count() > 0)
                    {
                        var listData = await GetListByIdAsync(dataSearch.Data.Select(p => p.Id.ToString()).ToList());
                        if (listData != null)
                        {
                            dataSearch.Data = listData.Select(p => Mapper<VideoPlaylistSearch>.Map(p, dataSearch.Data.Where(c => c.Id == p.Id).FirstOrDefault())).ToList();
                        }
                        dataReturn.Total = dataSearch.Total;
                        dataReturn.Data = dataSearch.Data.Select(p => Mapper<VideoPlaylistSearchReturn>.Map(p, new VideoPlaylistSearchReturn())).ToList();
                    }
                }
                return dataReturn;
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
                return null;
            }
        }

        public static async Task<PagingDataResult<VideoPlaylistSearch>> GetListApproveAsync(SearchNews data, long accountId)
        {
            try
            {
                var dataReturn = new PagingDataResult<VideoPlaylistSearch>();
                if (string.IsNullOrEmpty(data.Keyword) && data.OrderBy != SearchNewsOrderBy.DescDistributionDate
                    && data.OrderBy != SearchNewsOrderBy.AscDistributionDate && data.OrderBy != SearchNewsOrderBy.DescModifiedDate && data.OrderBy != SearchNewsOrderBy.AscModifiedDate)
                {
                    dataReturn = await StoreFactory.Create<VideoPlaylistCacheStore>().GetListApproveAsync(data, accountId, new Action<string>(async (tempKey) =>
                    {
                        await Function.AddToQueue(ActionName.SetExpire, TopicName.DELETE_INDEX_TEMP, tempKey);
                    }));
                }
                else
                {
                    dataReturn = await StoreFactory.Create<VideoPlaylistCacheStore>().GetListApproveAsync(data, accountId, new Action<string>(async (tempKey) =>
                    {
                        await Function.AddToQueue(ActionName.SetExpire, TopicName.DELETE_INDEX_TEMP, tempKey);
                    }));
                }
                return dataReturn;
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
                return null;
            }
        }

        public static Task<List<VideoPlaylist>> GetListByIdAsync(List<string> ids)
        {
            return StoreFactory.Create<VideoPlaylistCacheStore>().GetListByIdsAsync(ids);
        }
        public static Task<List<VideoPlaylistSearch>> GetListPlaylistSearchByIdsAsync(List<string> ids)
        {
            return StoreFactory.Create<VideoPlaylistCacheStore>().GetListPlaylistSearchByIdsAsync(ids);
        }

        public static Task<VideoPlaylistSearch> GetVideoPlaylistSearchByIdAsync(long id)
        {
            return StoreFactory.Create<VideoPlaylistCacheStore>().GetVideoPlaylistFullInfoAsync(id);
        }

        public static async Task<bool> PushVideoPlaylistToKingHub(string sessionId, PayloadDataShare playloadDataShare, Media.Action action)
        {
            try
            {
                if (playloadDataShare != null && playloadDataShare.MetaDataShared?.Count > 0)
                {
                    var media = playloadDataShare.MetaDataShared.FirstOrDefault();

                    var is_comment = AppSettings.Current.ChannelConfiguration.ApiCommentSetting.Mode.PostCheck;
                    if (media.CommentMode == CommentMode.Disable)
                    {
                        is_comment = AppSettings.Current.ChannelConfiguration.ApiCommentSetting.Mode.Disable;
                    }
                    else
                    {
                        if (media.CommentMode == CommentMode.PreCheck) is_comment = AppSettings.Current.ChannelConfiguration.ApiCommentSetting.Mode.PreCheck;
                    }

                    var post = new
                    {
                        posts = new List<object>{ new {
                            media_id = media.ObjectId.ToString(),
                            title = media.Title??string.Empty,
                            mediaunit_name = media.Name??string.Empty,
                            card_type = (int) CardType.PlaylistVideo,
                            preview=new{ },
                            media_unit_desc = media.ItemMediaUnit?.Select(s=> new {
                                id = s.Id ?? string.Empty,
                                title = s.Title ?? string.Empty,
                                link = s.Link ?? string.Empty,
                                thumb = s.Thumb ?? string.Empty,
                                type = s.TypeMedia ?? string.Empty,
                                duration = s.Duration ?? string.Empty,
                                width = s.Width,
                                height = s.Height,
                                content_type = (int?)s.ContentType ?? (int)ContentType.Video,
                                position = s.Position,
                                is_play = s.IsPlay,                                        
                                item_desc = s.Description ?? string.Empty,
                                created_at = (s.CreatedDate??DateTime.Now).ToString("yyyy-MM-ddTHH:mm:ssZ")
                            }).ToList(),
                            channel_id = media.ChannelId??string.Empty,
                            frame_id = media.Frameid,
                            tags = media.Tags??string.Empty,
                            user_id = media.UserId.ToString(),
                            user_name = media.UserName??string.Empty,
                            full_name = media.FullName??string.Empty,
                            avatar = media.AuthorAvatar??string.Empty,
                            cat_id = media.ZoneId?.ToString()??string.Empty,
                            create_time = Utility.ConvertToTimestamp(media.DistributionDate??DateTime.Now),
                            cat_name = media.ZoneName??string.Empty,
                            extension = new {
                                status = media.CaptionExt??new Caption[]{},
                                flag = new
                                {
                                    is_comment
                                },
                                is_web = 1,
                                boards=media.BoardIds != null && media.BoardIds.Length > 0 ? string.Join(",", media.BoardIds) : string.Empty,
                                playlist_ext = new
                                {
                                    cover = new { url = media.Cover?.Url ?? string.Empty, width = media.Cover?.Width ?? 600, height = media.Cover?.Height ?? 300, content_type = 2 },
                                    desc = media.Description ?? string.Empty
                                }
                            },
                        }}
                    };

                    //hearder thay vì session-id , các anh truyền vào user-id của page hoặc user nhé
                    sessionId = media.UserId.ToString();

                    if (action == Media.Action.add)
                    {
                        var errorCodeReturn = await Media.Add(media.ObjectId.ToString(), sessionId, CardType.PlaylistVideo, post);

                        if (errorCodeReturn == ErrorCodes.DataExist)
                        {
                            action = Media.Action.update;
                        }
                        else
                        {
                            return errorCodeReturn == ErrorCodes.Success ? true : false;
                        }
                    }

                    if (action == Media.Action.update)
                    {
                        return await Media.Update(media.ObjectId.ToString(), sessionId, CardType.PlaylistVideo, post) == ErrorCodes.Success ? true : false;
                    }

                    if (action == Media.Action.deletebymedia)
                    {
                        return await Media.Delete(media.ObjectId.ToString(), sessionId, CardType.PlaylistVideo) == ErrorCodes.Success ? true : false;
                    }
                    //action ko hỗ trợ
                    return false;
                }

                return false;
            }
            catch (Exception ex)
            {
                Logger.Error(ex, "PushBlogToKingHub: " + ex.Message);
                return false;
            }
        }
    }
}
