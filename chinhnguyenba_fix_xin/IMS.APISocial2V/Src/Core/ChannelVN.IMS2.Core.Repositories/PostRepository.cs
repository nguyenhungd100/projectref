﻿using ChannelVN.IMS2.Core.Entities;
using ChannelVN.IMS2.Core.Entities.Distribution;
using ChannelVN.IMS2.Core.Entities.Media;
using ChannelVN.IMS2.Core.Entities.Posts;
using ChannelVN.IMS2.Core.Entities.Security;
using ChannelVN.IMS2.Core.Models.CacheStore.Distribution;
using ChannelVN.IMS2.Core.Models.CacheStore.Posts;
using ChannelVN.IMS2.Core.Models.DataStore.Posts;
using ChannelVN.IMS2.Core.Models.ExternalAPI;
using ChannelVN.IMS2.Core.Models.SearchStore.Posts;
using ChannelVN.IMS2.Core.Repositories.Constants;
using ChannelVN.IMS2.Core.Repositories.Functions;
using ChannelVN.IMS2.Foundation.Common;
using ChannelVN.IMS2.Foundation.Common.Configuration;
using ChannelVN.IMS2.Foundation.Data;
using ChannelVN.IMS2.Foundation.Data.Factories;
using ChannelVN.IMS2.Foundation.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using static ChannelVN.IMS2.Core.Entities.ErrorCode.ErrorMapping;

namespace ChannelVN.IMS2.Core.Repositories
{
    public class PostRepository : Repository
    {
        public static async Task<ErrorCodes> AddAsync(PostSearch data, string clientIP)
        {
            var returnValue = ErrorCodes.AddError;
            try
            {
                returnValue = await StoreFactory.Create<PostCacheStore>().AddAsync(Mapper<Post>.Map(data, new Post()), data);
                if (returnValue == ErrorCodes.Success)
                {
                    var res = await StoreFactory.Create<PostSearchStore>().AddAsync(data);
                    if (!res)
                    {
                        //Push queue
                        await Function.AddToQueue(ActionName.AddPostES, TopicName.ES_POST, data.Id);
                    }

                    res = await StoreFactory.Create<PostDataStore>().AddAsync(data);
                    if (!res)
                    {
                        //Push queue
                        await Function.AddToQueue(ActionName.AddPostSQL, TopicName.SQL_POST, data.Id);
                    }

                    //if (data.Status == (int)NewsStatus.Archive && AppSettings.Current.Distribution.Automation.Enabled)
                    //{
                    //    await Function.AddToQueue(ActionName.AutoSharePost, TopicName.AUTOSHAREPOST, new QueueData<long, string, long[], object>
                    //    {
                    //        Data1 = data.Id,
                    //        Data2 = clientIP
                    //    });
                    //}
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
                returnValue = ErrorCodes.Success;
            }
            return returnValue;
        }
        public static async Task<ErrorCodes> UpdateAsync(Post data, long officerId, string clientIP, string sessionId)
        {
            var returnValue = ErrorCodes.BusinessError;
            try
            {
                returnValue = await StoreFactory.Create<PostCacheStore>().UpdateV2Async(data, officerId, new Action<int>(async (oldStatus) => {
                    if (AppSettings.Current.Distribution.Automation.Enabled)
                    {
                        if (oldStatus == (int)NewsStatus.Published)
                        {
                            var action = Media.Action.update;
                            //cho nay chua sua
                            await Function.AddToQueue(ActionName.AutoSharePost, TopicName.AUTOSHAREPOST, new QueueData<long, string, string, Media.Action>
                            {
                                Data1 = data.Id,
                                Data2 = clientIP,
                                Data3 = sessionId,
                                Data4 = action
                            });

                            await Function.AddToQueue(ActionName.PushChangeNotify, TopicName.PUSHBOARDTOCHANNEL, data.Id);
                        }
                    }
                }));
                if (returnValue == ErrorCodes.Success)
                {
                    await Function.AddToQueue(ActionName.UpdatePostES, TopicName.ES_POST, data.Id);
                    await Function.AddToQueue(ActionName.UpdatePostSQL, TopicName.SQL_POST, data.Id);                    
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
                returnValue = ErrorCodes.Exception;
            }
            return returnValue;
        }
        public static Task<PostSearch> GetPostSearchByIdAsync(long id)
        {
            return StoreFactory.Create<PostCacheStore>().GetPostFullInfoAsync(id);
        }

        public static async Task<ErrorCodes> UpdateStatusAsync(PostSearch data, int statusOld, string clientIP)
        {
            var returnCode = ErrorCodes.BusinessError;
            try
            {
                var returnValue = await StoreFactory.Create<PostCacheStore>().UpdateStatusAsync(Mapper<Post>.Map(data, new Post()), statusOld);
                if (returnValue)
                {
                    returnCode = ErrorCodes.Success;
                    await Function.AddToQueue(ActionName.UpdatePostStatusES, TopicName.ES_POST, data.Id);
                    await Function.AddToQueue(ActionName.UpdatePostStatusSQL, TopicName.SQL_POST, data.Id);

                    //auto phan phoi khi action publish       
                    if (AppSettings.Current.Distribution.Automation.Enabled)
                    {
                        if (data.Status == (int)NewsStatus.Published
                         || (data.Status == (int)NewsStatus.Remove && (statusOld == (int)NewsStatus.Published || statusOld == (int)NewsStatus.Archive))
                         || (data.Status == (int)NewsStatus.Draft && (statusOld == (int)NewsStatus.Published || statusOld == (int)NewsStatus.Archive)))
                        {                            
                            await Function.AddToQueue(ActionName.AutoSharePost, TopicName.AUTOSHAREPOST, new QueueData<long, string, long[], object>
                            {
                                Data1 = data.Id,
                                Data2 = clientIP
                            });
                        }
                    }
                }
                return returnCode;
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
                return ErrorCodes.Exception;
            }
        }

        public static async Task<ErrorCodes> UpdateStatusV2Async(long id, NewsStatus status, long userId, string clientIP, string sessionId)
        {
            var returnCode = ErrorCodes.BusinessError;
            try
            {
                returnCode = await StoreFactory.Create<PostCacheStore>().UpdateStatusV2Async(id, status, userId, new Action<PostSearch, int>(async (data, oldStatus) =>
                {
                    //auto phan phoi khi action publish      
                    if (AppSettings.Current.Distribution.Automation.Enabled)
                    {
                        if (status == NewsStatus.Published
                         || (data.Status == (int)NewsStatus.Remove && oldStatus == (int)NewsStatus.Published)
                         || (data.Status == (int)NewsStatus.Draft && oldStatus == (int)NewsStatus.Published))
                        {
                            var action = Media.Action.add;
                            if (status == NewsStatus.Published && oldStatus == (int)NewsStatus.Published)
                            {
                                action = Media.Action.update;
                            }
                            if (status == NewsStatus.Remove)
                            {
                                action = Media.Action.deletebymedia;
                            }
                            //await AutoShare(data, clientIP);
                            await Function.AddToQueue(ActionName.AutoSharePost, TopicName.AUTOSHAREPOST, new QueueData<long, string, string, Media.Action>
                            {
                                Data1 = data.Id,
                                Data2 = clientIP,
                                Data3 = sessionId,
                                Data4 = action
                            });
                        }
                    }
                }));

                if (returnCode == ErrorCodes.Success)
                {
                    await Function.AddToQueue(ActionName.UpdatePostStatusES, TopicName.ES_POST, id);
                    await Function.AddToQueue(ActionName.UpdatePostStatusSQL, TopicName.SQL_POST, id);
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
                returnCode = ErrorCodes.Exception;
            }
            return returnCode;
        }

        public static async Task<bool> AutoShare(PostSearch data, string clientIP, string sessionId, Media.Action action)
        {
            try
            {
                var listItem = Json.Parse<PublishData>(data.PublishData);
                var user_created = await SecurityRepository.GetByIdAsync(long.Parse(data.NewsInAccount?.LastOrDefault()?.AccountId.ToString() ?? data.CreatedBy));
                var payloadDataShare = new PayloadDataShare
                {
                    DistributionId = AppSettings.Current.Distribution.Automation.DistributorId,
                    TemplateId = 0,
                    Title = data.Caption ?? listItem.Title,
                    Type = NewsType.Post,
                    MetaDataShared = new List<MetaDataShared>
                    {
                        new MetaDataShared
                        {
                            ObjectId = data.Id,
                            Title = data.Caption ?? listItem.Caption,
                            ZoneId = 0,
                            DistributionDate = data.DistributionDate,
                            CardType = (int)CardType.Text,
                            Frameid = 0,
                            Tags = data.Tags,
                            UserId = user_created?.Id??0,
                            UserName = user_created?.UserName,
                            FullName = user_created?.FullName,
                            AuthorAvatar = user_created?.Avatar,
                            IsDeleted = data.Status == (int)NewsStatus.Remove||data.Status == (int)NewsStatus.Draft ? 1 : (data.Status == (int)NewsStatus.Archive ? 2 : 0),
                            ItemMediaUnit = null,
                            BoardIds = null,
                            CaptionExt = listItem.CaptionExt,
                            CommentMode = data.CommentMode
                        }
                    }
                };
                var distribution = await DistributionRepository.GetByIdAsync(payloadDataShare.DistributionId);
                var newsDistribution = data.NewsDistribution?.Where(p => p.DistributorId == payloadDataShare.DistributionId).FirstOrDefault();
                var itemStreamDistribution = new ItemStreamDistribution()
                {
                    Id = newsDistribution != null ? newsDistribution.ItemStreamId : Generator.ItemStreamDistributionId(),
                    PublishStatus = data.Status == (int)NewsStatus.Published ? (int)DistributionPublishStatus.Accept : (int)DistributionPublishStatus.Reject,
                    CreatedDate = DateTime.Now,
                    ItemStreamTempId = payloadDataShare.TemplateId,
                    Title = payloadDataShare.Title,
                    MetaData = Json.Stringify(payloadDataShare.MetaDataShared),
                    Type = payloadDataShare.Type
                };
                var result = await SharePostAsync(itemStreamDistribution, distribution, payloadDataShare, clientIP, sessionId, action);
                if (result)
                {
                    //insert log
                    await Function.AddToQueue(ActionName.InsertLog, TopicName.LOGACTION, new LogActionEntity
                    {
                        ObjectId = data.Id.ToString(),
                        Account = data.ModifiedBy,
                        CreatedDate = DateTime.SpecifyKind(DateTime.Now, DateTimeKind.Unspecified),
                        Ip = clientIP,
                        Message = "Success.",
                        Title = data.Caption,
                        SourceId = data.ModifiedBy,
                        ActionTypeDetail = (int)ActionType.Share,
                        ActionText = ActionType.Share.ToString(),
                        Type = (int)LogContentType.Post
                    });
                }
                return result;
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
                return false;
            }

        }

        public static async Task<PagingDataResult<PostSearchReturn>> SearchAsync(SearchNews data, long userId, bool isGetDistribution)
        {
            try
            {
                var dataReturn = new PagingDataResult<PostSearchReturn>();
                if (isGetDistribution == false && string.IsNullOrEmpty(data.Keyword) && data.OrderBy != SearchNewsOrderBy.DescDistributionDate
                    && data.OrderBy != SearchNewsOrderBy.AscDistributionDate && data.OrderBy != SearchNewsOrderBy.DescModifiedDate && data.OrderBy != SearchNewsOrderBy.AscModifiedDate)
                {
                    dataReturn = await StoreFactory.Create<PostCacheStore>().SearchAsync(data, userId, new Action<string>(async (tempKey) =>
                    {
                        await Function.AddToQueue(ActionName.SetExpire, TopicName.DELETE_INDEX_TEMP, tempKey);
                    }));
                }
                else
                {
                    var dataSearch = await StoreFactory.Create<PostSearchStore>().SearchAsync(data, userId, isGetDistribution);
                    if (dataSearch?.Data?.Count() > 0)
                    {
                        var listData = await GetListByIdsAsync(dataSearch.Data.Select(p => p.Id.ToString()).ToList());
                        if (listData != null)
                        {
                            dataSearch.Data = listData.Select(p => Mapper<PostSearch>.Map(p, dataSearch.Data.Where(c => c.Id == p.Id).FirstOrDefault())).ToList();
                        }
                        dataReturn.Total = dataSearch.Total;
                        dataReturn.Data = dataSearch.Data.Select(p => Mapper<PostSearchReturn>.Map(p, new PostSearchReturn())).ToList();
                    }
                }
                return dataReturn;
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
                return null;
            }
        }

        public static async Task<PagingDataResult<Post>> GetListApproveAsync(SearchNews data, long accountId)
        {
            try
            {
                var dataReturn = new PagingDataResult<Post>();
                if (string.IsNullOrEmpty(data.Keyword) && data.OrderBy != SearchNewsOrderBy.DescDistributionDate
                    && data.OrderBy != SearchNewsOrderBy.AscDistributionDate && data.OrderBy != SearchNewsOrderBy.DescModifiedDate && data.OrderBy != SearchNewsOrderBy.AscModifiedDate)
                {
                    dataReturn = await StoreFactory.Create<PostCacheStore>().GetListApproveAsync(data, accountId, new Action<string>(async (tempKey) =>
                    {
                        await Function.AddToQueue(ActionName.SetExpire, TopicName.DELETE_INDEX_TEMP, tempKey);
                    }));
                }
                else
                {
                    dataReturn = await StoreFactory.Create<PostCacheStore>().GetListApproveAsync(data, accountId, new Action<string>(async (tempKey) =>
                    {
                        await Function.AddToQueue(ActionName.SetExpire, TopicName.DELETE_INDEX_TEMP, tempKey);
                    }));
                }
                return dataReturn;
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
                return null;
            }
        }

        public static Task<List<Post>> GetListByIdsAsync(List<string> ids)
        {
            return StoreFactory.Create<PostCacheStore>().GetListByIdsAsync(ids);
        }
        public static Task<List<PostSearch>> GetListPostSearchByIdsAsync(List<string> ids)
        {
            return StoreFactory.Create<PostCacheStore>().GetListPostSearchByIdsAsync(ids);
        }

        public static async Task<bool> PushPostToKingHub(string sessionId, PayloadDataShare playloadDataShare, Media.Action action)
        {
            try
            {
                if (playloadDataShare != null && playloadDataShare.MetaDataShared?.Count > 0)
                {
                    var media = playloadDataShare.MetaDataShared.FirstOrDefault();

                    var is_comment = AppSettings.Current.ChannelConfiguration.ApiCommentSetting.Mode.PostCheck;
                    if (media.CommentMode == CommentMode.Disable)
                    {
                        is_comment = AppSettings.Current.ChannelConfiguration.ApiCommentSetting.Mode.Disable;
                    }
                    else
                    {
                        if (media.CommentMode == CommentMode.PreCheck) is_comment = AppSettings.Current.ChannelConfiguration.ApiCommentSetting.Mode.PreCheck;
                    }

                    var post = new
                    {
                        posts = new List<object>{ new {
                            media_id = media.ObjectId.ToString(),
                            title = media.Title??string.Empty,
                            mediaunit_name = media.Name??string.Empty,
                            card_type = (int) CardType.Text,
                            preview=new{ },
                            media_unit_desc = new List<object>(),
                            channel_id = media.ChannelId??string.Empty,
                            frame_id = media.Frameid,
                            tags = media.Tags??string.Empty,
                            user_id = media.UserId.ToString(),
                            user_name = media.UserName??string.Empty,
                            full_name = media.FullName??string.Empty,
                            avatar = media.AuthorAvatar??string.Empty,
                            cat_id = media.ZoneId?.ToString()??string.Empty,
                            create_time = Utility.ConvertToTimestamp(media.DistributionDate??DateTime.Now),
                            cat_name = media.ZoneName??string.Empty,
                            extension = new {
                                status = media.CaptionExt??new Caption[]{},
                                flag = new
                                {
                                    is_comment
                                },
                                is_web = 1,
                                boards=media.BoardIds != null && media.BoardIds.Length > 0 ? string.Join(",", media.BoardIds) : string.Empty                                
                            },
                        }}
                    };

                    //hearder thay vì session-id , các anh truyền vào user-id của page hoặc user nhé
                    sessionId = media.UserId.ToString();

                    if (action == Media.Action.add)
                    {
                        var errorCodeReturn = await Media.Add(media.ObjectId.ToString(), sessionId, CardType.Text, post);

                        if (errorCodeReturn == ErrorCodes.DataExist)
                        {
                            action = Media.Action.update;
                        }
                        else
                        {
                            return errorCodeReturn == ErrorCodes.Success ? true : false;
                        }
                    }

                    if (action == Media.Action.update)
                    {
                        return await Media.Update(media.ObjectId.ToString(), sessionId, CardType.Text, post) == ErrorCodes.Success ? true : false;
                    }

                    if (action == Media.Action.deletebymedia)
                    {
                        return await Media.Delete(media.ObjectId.ToString(), sessionId, CardType.Text) == ErrorCodes.Success ? true : false;
                    }
                    //action ko hỗ trợ
                    return false;
                }

                return false;
            }
            catch (Exception ex)
            {
                Logger.Error(ex, "PushBlogToKingHub: " + ex.Message);
                return false;
            }
        }

        public static async Task<bool> SharePostAsync(ItemStreamDistribution itemStreamDistribution, Distributor distribution, PayloadDataShare playloadDataShare, string clientIP, string sessionId, Media.Action action)
        {
            var returnValue = false;
            var isUpdate = true;
            try
            {
                var listPost = new List<PostSearch>();
                foreach (var metaData in playloadDataShare.MetaDataShared)
                {
                    if (metaData.ObjectId > 0)
                    {
                        var newsDistribution = new NewsDistribution
                        {
                            DistributorId = distribution.Id,
                            DistributorName = distribution.Name,
                            NewsId = metaData.ObjectId,
                            ItemStreamId = itemStreamDistribution.Id,
                            SharedDate = itemStreamDistribution.CreatedDate,
                            SharedZone = metaData.ZoneId.ToString() + (!string.IsNullOrEmpty(metaData.ZoneRelation) ? ("," + metaData.ZoneRelation) : "")
                        };

                        var post = await GetPostSearchByIdAsync(metaData.ObjectId);
                        if (post != null)
                        {
                            if (post.DistributionDate == null) post.DistributionDate = DateTime.Now;
                            if (post.NewsDistribution == null)
                            {
                                isUpdate = false;
                                post.NewsDistribution = new[] { newsDistribution };
                            }
                            else
                            {
                                if (post.NewsDistribution.Count(w => w.DistributorId == newsDistribution.DistributorId && w.ItemStreamId == itemStreamDistribution.Id) == 0)
                                {
                                    isUpdate = false;
                                    post.NewsDistribution = post.NewsDistribution.Append(newsDistribution).ToArray();
                                }
                            }
                            listPost.Add(post);
                        }
                        else
                        {
                            return false;
                        }
                    }
                }
                if (listPost.Count > 0)
                {
                    returnValue = await StoreFactory.Create<DistributionCacheStore>().SharePostAsync(itemStreamDistribution, listPost, isUpdate);
                    if (returnValue)
                    {
                        //push to queue share video
                        await Function.AddToQueue(ActionName.SharePost, TopicName.SHARE, new QueueData<string, PayloadDataShare, string, Media.Action>
                        {
                            Data1 = Guid.NewGuid().ToString(),
                            Data2 = playloadDataShare,
                            Data3 = sessionId,
                            Data4 = action
                        });

                        await Function.AddToQueue(ActionName.SharePostES, TopicName.ES_POST, new QueueData<ItemStreamDistribution, List<PostSearch>, bool, object> { Data1 = itemStreamDistribution, Data2 = listPost, Data3 = isUpdate });

                        var listNewsDistribution = new List<NewsDistribution>();
                        foreach (var media in listPost)
                        {
                            var listTmp = media.NewsDistribution.Select(p => p).Where(p => p.ItemStreamId == itemStreamDistribution.Id && p.DistributorId == distribution.Id).ToList();
                            listTmp.ForEach(p => p.NewsId = media.Id);
                            listNewsDistribution.AddRange(listTmp);
                        }
                        //push queue
                        await Function.AddToQueue(ActionName.SharePostSQL, TopicName.SQL_POST, new QueueData<ItemStreamDistribution, List<NewsDistribution>, bool, object> { Data1 = itemStreamDistribution, Data2 = listNewsDistribution, Data3 = isUpdate });
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
            }
            return returnValue;
        }
    }
}
