﻿using ChannelVN.IMS2.Core.Entities;
using ChannelVN.IMS2.Core.Entities.Album;
using ChannelVN.IMS2.Core.Entities.Beam;
using ChannelVN.IMS2.Core.Entities.Distribution;
using ChannelVN.IMS2.Core.Entities.Gallery;
using ChannelVN.IMS2.Core.Entities.Media;
using ChannelVN.IMS2.Core.Entities.Newsletters;
using ChannelVN.IMS2.Core.Entities.Photo;
using ChannelVN.IMS2.Core.Entities.Posts;
using ChannelVN.IMS2.Core.Entities.Security;
using ChannelVN.IMS2.Core.Entities.ShareLinks;
using ChannelVN.IMS2.Core.Entities.Videos;
using ChannelVN.IMS2.Core.Models.CacheStore.Distribution;
using ChannelVN.IMS2.Core.Models.CacheStore.Videos;
using ChannelVN.IMS2.Core.Models.DataStore.Distribution;
using ChannelVN.IMS2.Core.Models.ExternalAPI;
using ChannelVN.IMS2.Core.Models.SearchStore.Articles;
using ChannelVN.IMS2.Core.Repositories.Constants;
using ChannelVN.IMS2.Core.Repositories.Functions;
using ChannelVN.IMS2.Foundation.Common;
using ChannelVN.IMS2.Foundation.Common.Configuration;
using ChannelVN.IMS2.Foundation.Data;
using ChannelVN.IMS2.Foundation.Data.Factories;
using ChannelVN.IMS2.Foundation.Logging;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace ChannelVN.IMS2.Core.Repositories
{
    public class DistributionRepository : Repository
    {
        public static async Task<bool> AddAsync(Distributor data)
        {
            try
            {
                var result = await Task.Run(async () =>
                {
                    return await StoreFactory.Create<DistributionCacheStore>().AddAsync(data);

                }).ContinueWith(async returnValue =>
                {
                    if (returnValue.Result)
                    {
                        var res = await StoreFactory.Create<DistributionDataStore>().AddAsync(data);
                        if (!res)
                        {
                            //push queue
                            await Function.AddToQueue(ActionName.AddDistributionSQL, TopicName.SQL_DISTRIBUTOR, data);
                        }
                    }
                    return returnValue;
                });

                return result.Result.Result;
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
                return false;
            }
        }

        public static async Task<bool> CheckItemStreamDistributionAsync(long distributionId, long itemStreamDistributionId)
        {

            try
            {
                var itemStream = await StoreFactory.Create<ArticleSearchStore>().GetItemStreamByIdAsync(itemStreamDistributionId);
                if (itemStream != null && itemStream.DistributorId.Contains(distributionId)) return true;
                return false;
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
                return false;
            }
        }

        public static async Task<bool> UpdateAsync(Distributor data)
        {
            try
            {
                var result = await Task.Run(async () =>
                {
                    return await StoreFactory.Create<DistributionCacheStore>().UpdateAsync(data);
                }).ContinueWith(async returnValue =>
                {
                    if (returnValue.Result)
                    {
                        var res = await StoreFactory.Create<DistributionDataStore>().UpdateAsync(data);
                        if (!res)
                        {
                            //push queue
                            await Function.AddToQueue(ActionName.UpdateDistributionSQL, TopicName.SQL_DISTRIBUTOR, data);
                        }
                    }
                    return returnValue;
                });

                return result.Result.Result;
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
                return false;
            }
        }

        public static Task<Distributor> GetByIdAsync(long id)
        {
            return StoreFactory.Create<DistributionCacheStore>().GetByIdAsync(id);
        }

        public static Task<List<Distributor>> SearchAsync()
        {
            return StoreFactory.Create<DistributionCacheStore>().GetAllAsync();
        }

        #region Share VideoPlaylist
        public static async Task<bool> ShareVideoPlaylistAsync(ItemStreamDistribution itemStreamDistribution, Distributor distribution, PayloadDataShare playloadDataShare, string clientIP, string sessionId, Media.Action action)
        {
            var returnValue = false;
            bool isUpdate = true;
            try
            {
                var videoPlaylistCache = StoreFactory.Create<VideoPlaylistCacheStore>();
                var listVideoPlaylist = new List<VideoPlaylistSearch>();

                foreach (var metaData in playloadDataShare.MetaDataShared)
                {
                    if (metaData.ObjectId > 0)
                    {
                        var newsDistribution = new NewsDistribution
                        {
                            DistributorId = distribution.Id,
                            DistributorName = distribution.Name,
                            NewsId = metaData.ObjectId,
                            ItemStreamId = itemStreamDistribution.Id,
                            SharedDate = itemStreamDistribution.CreatedDate,
                            SharedZone = metaData.ZoneId.ToString() + (!string.IsNullOrEmpty(metaData.ZoneRelation) ? ("," + metaData.ZoneRelation) : "")
                        };

                        var videoPlaylist = await VideoPlaylistRepository.GetVideoPlaylistSearchByIdAsync(metaData.ObjectId);
                        if (videoPlaylist != null)
                        {
                            if (videoPlaylist.DistributionDate == null) videoPlaylist.DistributionDate = DateTime.Now;
                            if (videoPlaylist.NewsDistribution == null)
                            {
                                isUpdate = false;
                                videoPlaylist.NewsDistribution = new[] { newsDistribution };
                            }
                            else
                            {
                                if (videoPlaylist.NewsDistribution.Count(w => w.DistributorId == newsDistribution.DistributorId && w.ItemStreamId == itemStreamDistribution.Id) == 0)
                                {
                                    isUpdate = false;
                                    videoPlaylist.NewsDistribution = videoPlaylist.NewsDistribution.Append(newsDistribution).ToArray();
                                }
                            }
                            listVideoPlaylist.Add(videoPlaylist);
                        }
                        else
                        {
                            return false;
                        }
                    }
                }
                if (listVideoPlaylist.Count > 0)
                {
                    returnValue = await StoreFactory.Create<DistributionCacheStore>().ShareVideoPlaylistAsync(itemStreamDistribution, listVideoPlaylist, isUpdate);
                    if (returnValue)
                    {
                        //push to queue share video
                        await Function.AddToQueue(ActionName.SharePlayList, TopicName.SHARE, new QueueData<string, PayloadDataShare, string, Media.Action>
                        {
                            Data1 = Guid.NewGuid().ToString(),
                            Data2 = playloadDataShare,
                            Data3 = sessionId,
                            Data4 = action
                        });

                        var listNewsDistribution = new List<NewsDistribution>();
                        foreach (var media in listVideoPlaylist)
                        {
                            var listTmp = media.NewsDistribution.Where(p => p.ItemStreamId == itemStreamDistribution.Id && p.ItemStreamId == itemStreamDistribution.Id).ToList();
                            listTmp.ForEach(p => p.NewsId = media.Id);
                            listNewsDistribution.AddRange(listTmp);
                        }
                        //push queue

                        await Function.AddToQueue(ActionName.ShareVideoPlaylistSQL, TopicName.SQL_PLAYLIST, new QueueData<ItemStreamDistribution, List<NewsDistribution>, bool, object> { Data1 = itemStreamDistribution, Data2 = listNewsDistribution, Data3 = isUpdate });
                    }

                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
            }
            return returnValue;
        }
        public static async Task<bool> PushVideoPlaylistToChannel(string sessionId, PayloadDataShare playloadDataShare, Distributor distribution, string clientIP)
        {
            var lstObj = new List<object>();
            var returnValue = false;
            try
            {
                if (distribution != null)
                {
                    var configMediaObj = Json.Parse<Dictionary<string, string>>(distribution?.Config.ApiMediaUnit);
                    if (configMediaObj == null) return false;

                    var configApiUrl = configMediaObj["ApiUrl"];
                    var configApiKey = configMediaObj["ApiKey"];
                    if (configApiUrl != null && !string.IsNullOrEmpty(configApiUrl) && configApiKey != null && !string.IsNullOrEmpty(configApiKey))
                    {
                        var listMediaKingHub = new List<FormUrlEncodedContent>();
                        if (playloadDataShare != null && playloadDataShare.MetaDataShared?.Count > 0)
                        {
                            //Logger.Debug(Json.Stringify(playloadDataShare.MetaDataShared));
                            foreach (var media in playloadDataShare.MetaDataShared)
                            {
                                var is_comment = (int)AppSettings.Current.ChannelConfiguration.ApiCommentSetting.Mode.PostCheck;
                                if (media.CommentMode == CommentMode.Disable)
                                {
                                    is_comment = (int)AppSettings.Current.ChannelConfiguration.ApiCommentSetting.Mode.Disable;
                                }
                                else
                                {
                                    if (media.CommentMode == CommentMode.PreCheck) is_comment = (int)AppSettings.Current.ChannelConfiguration.ApiCommentSetting.Mode.PreCheck;
                                }

                                var obj = new
                                {
                                    mediaid = media.ObjectId.ToString(),
                                    title = media.Title ?? string.Empty,
                                    obj_name = media.Name ?? string.Empty,
                                    cardtype = ((int)CardType.PlaylistVideo).ToString(),
                                    desc = Json.Stringify(media.ItemMediaUnit.Select(s => new {
                                        id = s.Id ?? string.Empty,
                                        title = s.Title ?? string.Empty,
                                        link = s.Link ?? string.Empty,
                                        thumb = s.Thumb ?? string.Empty,
                                        type = s.TypeMedia ?? string.Empty,
                                        duration = s.Duration ?? string.Empty,
                                        width = s.Width,
                                        height = s.Height,
                                        content_type = (int?)s.ContentType ?? (int)ContentType.Video,
                                        position = s.Position,
                                        is_play = s.IsPlay,
                                        created_at = (s.CreatedDate ?? DateTime.Now).ToString("yyyy-MM-ddTHH:mm:ssZ"),
                                        item_desc = s.Description ?? string.Empty
                                    }).ToList()),
                                    channelid = media.ChannelId ?? string.Empty,
                                    frameid = media.Frameid.ToString(),
                                    tags = media.Tags ?? string.Empty,
                                    userid = media.UserId.ToString(),
                                    username = media.UserName ?? string.Empty,
                                    fullname = media.FullName ?? string.Empty,
                                    avatar = media.AuthorAvatar ?? string.Empty,
                                    catid = media.ZoneId?.ToString() ?? string.Empty,
                                    isdeleted = media.IsDeleted.ToString(),
                                    createtime = (media.DistributionDate ?? DateTime.Now).ToString("yyyy-MM-ddTHH:mm:ssZ"),
                                    catname = media.ZoneName ?? string.Empty,
                                    ext = Json.StringifyFirstLowercase(new
                                    {
                                        playlist_ext = new
                                        {
                                            cover = new { url = media.Cover?.Url ?? string.Empty, width = media.Cover?.Width ?? 600, height = media.Cover?.Height ?? 300, content_type = 2 },
                                            desc = media.Description ?? string.Empty
                                        },
                                        status = media.CaptionExt,
                                        flag = new
                                        {
                                            is_comment = is_comment
                                        }
                                    }),
                                    boardids = media.BoardIds != null && media.BoardIds.Length > 0 ? string.Join(",", media.BoardIds) : string.Empty
                                };

                                lstObj.Add(obj);

                                listMediaKingHub.Add(new FormUrlEncodedContent(obj.ConvertToKeyValuePair()));
                            }
                        }
                        else
                        {
                            Logger.Error("Push Video Playlist: không có videoplaylist nào.");
                            return false;
                        }
                        foreach (var value in listMediaKingHub)
                        {
                            using (var client = new HttpClient())
                            {
                                try
                                {
                                    client.Timeout = TimeSpan.FromSeconds(AppSettings.Current.ChannelConfiguration.HttpClientTimeOut);
                                    var response = await client.PostAsync(configApiUrl, value);
                                    if (!response.IsSuccessStatusCode)
                                    {
                                        response.EnsureSuccessStatusCode();
                                    }
                                    var result = await response.Content.ReadAsStringAsync();
                                    if (!string.IsNullOrEmpty(result))
                                    {
                                        returnValue = Json.Parse<dynamic>(result).result;
                                        if (!returnValue)
                                        {
                                            Logger.Debug("Data push playlist => " + Json.Stringify(lstObj));
                                            Logger.Debug("PushVideoPlaylistToChannel ERROR => configApiUrl:" + configApiUrl + ", data:" + string.Join(",", playloadDataShare.MetaDataShared?.Select(p => p.ObjectId.ToString()).ToArray() ?? new string[] { }) + ", result:" + result);
                                        }
                                    }
                                }
                                catch (TaskCanceledException ex)
                                {
                                    Logger.Error(ex, "Push Video Playlist exception => " + Json.Stringify(new
                                    {
                                        data = lstObj,
                                        exception = ex.Message
                                    }));
                                }
                                catch (Exception ex)
                                {
                                    Logger.Error(ex, "Push Video Playlist exception => "+ Json.Stringify(new {
                                        data = lstObj,
                                        exception = ex.Message
                                    }));
                                }
                            }
                        }
                    }
                }
                if (returnValue)
                {
                    if (playloadDataShare != null && playloadDataShare.MetaDataShared?.Count > 0)
                    {
                        foreach (var media in playloadDataShare.MetaDataShared)
                        {
                            //insert log
                            await Function.AddToQueue(ActionName.InsertLog, TopicName.LOGACTION, new LogActionEntity
                            {
                                ObjectId = media.ObjectId.ToString(),
                                Account = media.UserId.ToString(),
                                CreatedDate = DateTime.SpecifyKind(DateTime.Now, DateTimeKind.Unspecified),
                                Ip = clientIP,
                                Message = "Success.",
                                Title = media.Title,
                                Status = returnValue ? 1 : 0,
                                SourceId = media.UserId.ToString(),
                                ActionTypeDetail = (int)ActionType.PushToApp,
                                ActionText = ActionType.PushToApp.ToString(),
                                Type = (int)LogContentType.PlayList
                            });
                        }
                    }

                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, "Push Video Playlist exception :" + ex.Message);
            }
            return returnValue;
        }
        #endregion

        #region Share Video
        public static async Task<bool> ShareVideoAsync(ItemStreamDistribution itemStreamDistribution, Distributor distribution, PayloadDataShare playloadDataShare, string clientIP, string sessionId, Media.Action action, bool isShareKingHub = true)
        {
            var returnValue = false;
            var isUpdate = isShareKingHub ? true : false;
            try
            {
                var listVideo = new List<VideoSearch>();
                foreach (var metaData in playloadDataShare.MetaDataShared)
                {
                    if (metaData.ObjectId > 0)
                    {
                        var newsDistribution = new NewsDistribution
                        {
                            DistributorId = distribution.Id,
                            DistributorName = distribution.Name,
                            NewsId = metaData.ObjectId,
                            ItemStreamId = itemStreamDistribution.Id,
                            SharedDate = itemStreamDistribution.CreatedDate,
                            SharedZone = metaData.ZoneId.ToString() + (!string.IsNullOrEmpty(metaData.ZoneRelation) ? ("," + metaData.ZoneRelation) : "")
                        };

                        var video = await VideoRepository.GetVideoSearchByIdAsync(metaData.ObjectId);
                        if (video != null)
                        {
                            if (video.DistributionDate == null) video.DistributionDate = DateTime.Now;
                            if (video.NewsDistribution == null)
                            {
                                isUpdate = false;
                                video.NewsDistribution = new[] { newsDistribution };
                            }
                            else
                            {
                                if (video.NewsDistribution.Count(w => w.DistributorId == newsDistribution.DistributorId && w.ItemStreamId == itemStreamDistribution.Id) == 0)
                                {
                                    isUpdate = false;
                                    video.NewsDistribution = video.NewsDistribution.Append(newsDistribution).ToArray();
                                }
                            }
                            listVideo.Add(video);
                        }
                        else
                        {
                            return false;
                        }
                    }
                }
                if (listVideo.Count > 0)
                {
                    returnValue = await StoreFactory.Create<DistributionCacheStore>().ShareVideoAsync(itemStreamDistribution, listVideo, isUpdate);
                    if (returnValue)
                    {
                        await Function.AddToQueue(ActionName.ShareVideoES, TopicName.ES_VIDEO, new QueueData<ItemStreamDistribution, List<VideoSearch>, bool, object> { Data1 = itemStreamDistribution, Data2 = listVideo, Data3 = isUpdate });

                        var listNewsDistribution = new List<NewsDistribution>();
                        foreach (var media in listVideo)
                        {
                            var listTmp = media.NewsDistribution.Where(p => p.ItemStreamId == itemStreamDistribution.Id && playloadDataShare.DistributionId == distribution.Id).ToList();
                            listTmp.ForEach(p => p.NewsId = media.Id);
                            listNewsDistribution.AddRange(listTmp);
                        }
                        //push queue
                        await Function.AddToQueue(ActionName.ShareVideoSQL, TopicName.SQL_VIDEO, new QueueData<ItemStreamDistribution, List<NewsDistribution>, bool, object> { Data1 = itemStreamDistribution, Data2 = listNewsDistribution, Data3 = isUpdate });

                        //push to queue share video
                        await Function.AddToQueue(ActionName.ShareVideo, TopicName.SHARE, new QueueData<string, PayloadDataShare, string, Media.Action>
                        {
                            Data1 = Guid.NewGuid().ToString(),
                            Data2 = playloadDataShare,
                            Data3 = sessionId,
                            Data4 = action
                        });
                    }

                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
            }
            return returnValue;
        }
        public static async Task<bool> PushVideoToChannel(string sessionId, PayloadDataShare playloadDataShare, Distributor distribution, string clientIP)
        {
            var lstObj = new List<object>();
            var returnValue = false;
            try
            {
                if (distribution != null)
                {
                    var configMediaObj = Json.Parse<Dictionary<string, string>>(distribution?.Config.ApiMediaUnit);
                    if (configMediaObj == null) return false;

                    var configApiUrl = configMediaObj["ApiUrl"];
                    var configApiKey = configMediaObj["ApiKey"];
                    if (configApiUrl != null && !string.IsNullOrEmpty(configApiUrl) && configApiKey != null && !string.IsNullOrEmpty(configApiKey))
                    {
                        var listMediaKingHub = new List<FormUrlEncodedContent>();
                        if (playloadDataShare != null && playloadDataShare.MetaDataShared?.Count > 0)
                        {
                            foreach (var media in playloadDataShare.MetaDataShared)
                            {
                                var is_comment = (int)AppSettings.Current.ChannelConfiguration.ApiCommentSetting.Mode.PostCheck;
                                if (media.CommentMode == CommentMode.Disable)
                                {
                                    is_comment = (int)AppSettings.Current.ChannelConfiguration.ApiCommentSetting.Mode.Disable;
                                }
                                else
                                {
                                    if (media.CommentMode == CommentMode.PreCheck) is_comment = (int)AppSettings.Current.ChannelConfiguration.ApiCommentSetting.Mode.PreCheck;
                                }
                                
                                var obj = new
                                {
                                    mediaid = media.ObjectId.ToString(),
                                    title = media.Title??string.Empty,
                                    obj_name = media.Name??string.Empty,
                                    cardtype = ((int)CardType.Video).ToString(),
                                    desc = Json.Stringify(media.ItemMediaUnit.Select(s=> new{
                                        id = s.Id??string.Empty,
                                        title = s.Title??string.Empty,
                                        link = s.Link??string.Empty,
                                        thumb = s.Thumb??string.Empty,
                                        type = s.TypeMedia??string.Empty,
                                        duration = s.Duration??string.Empty,
                                        width = s.Width,
                                        height = s.Height,
                                        content_type = (int?) s.ContentType ?? (int)ContentType.Video,
                                        position = s.Position,
                                        is_play = s.IsPlay,
                                        created_at = (s.CreatedDate??DateTime.Now).ToString("yyyy-MM-ddTHH:mm:ssZ"),
                                        item_desc = s.Description??string.Empty
                                    }).ToList()),
                                    channelid = media.ChannelId??string.Empty,
                                    frameid = media.Frameid.ToString(),
                                    tags = media.Tags??string.Empty,
                                    userid = media.UserId.ToString(),
                                    username = media.UserName??string.Empty,
                                    fullname = media.FullName??string.Empty,
                                    avatar = media.AuthorAvatar??string.Empty,
                                    catid = media.ZoneId?.ToString()??string.Empty,
                                    isdeleted = media.IsDeleted.ToString(),
                                    createtime = (media.DistributionDate??DateTime.Now).ToString("yyyy-MM-ddTHH:mm:ssZ"),
                                    catname = media.ZoneName??string.Empty,
                                    boardids = media.BoardIds != null && media.BoardIds.Length > 0 ? string.Join(",", media.BoardIds) : string.Empty,
                                    ext = Json.StringifyFirstLowercase(new {
                                        status = media.CaptionExt,
                                        flag = new
                                        {
                                            is_comment = is_comment
                                        }
                                    }),
                                };

                                lstObj.Add(obj);
                                listMediaKingHub.Add(new FormUrlEncodedContent(obj.ConvertToKeyValuePair()));
                            }
                        }
                        else
                        {
                            Logger.Error("Push Video: video không tồn tại.");
                            return false;
                        }
                        foreach (var value in listMediaKingHub)
                        {
                            using (var client = new HttpClient())
                            {
                                try
                                {
                                    client.Timeout = TimeSpan.FromSeconds(AppSettings.Current.ChannelConfiguration.HttpClientTimeOut);
                                    var response = await client.PostAsync(configApiUrl, value);
                                    if (!response.IsSuccessStatusCode)
                                    {
                                        response.EnsureSuccessStatusCode();
                                    }
                                    var result = await response.Content.ReadAsStringAsync();
                                    if (!string.IsNullOrEmpty(result))
                                    {
                                        returnValue = Json.Parse<dynamic>(result).result;
                                        if (!returnValue)
                                        {
                                            Logger.Debug("Data push video => " + Json.Stringify(lstObj));
                                            Logger.Debug("PushVideoToChannel ERROR => configApiUrl:" + configApiUrl + ", data:" + string.Join(",", playloadDataShare.MetaDataShared?.Select(p => p.ObjectId.ToString()).ToArray() ?? new string[] { }) + ", result:" + result);
                                        }
                                    }
                                }
                                catch (Exception ex)
                                {
                                    Logger.Error(ex, "Push video exception => " + Json.Stringify(new
                                    {
                                        data = lstObj,
                                        exception = ex.Message
                                    }));
                                }
                            }
                        }
                    }
                }
                if (returnValue)
                {
                    if (playloadDataShare != null && playloadDataShare.MetaDataShared?.Count > 0)
                    {
                        foreach (var media in playloadDataShare.MetaDataShared)
                        {
                            //insert log
                            await Function.AddToQueue(ActionName.InsertLog, TopicName.LOGACTION, new LogActionEntity
                            {
                                ObjectId = media.ObjectId.ToString(),
                                Account = media.UserId.ToString(),
                                CreatedDate = DateTime.SpecifyKind(DateTime.Now, DateTimeKind.Unspecified),
                                Ip = clientIP,
                                Message = "Success.",
                                Title = media.Title,
                                Status = returnValue ? 1 : 0,
                                SourceId = media.UserId.ToString(),
                                ActionTypeDetail = (int)ActionType.PushToApp,
                                ActionText = ActionType.PushToApp.ToString(),
                                Type = (int)LogContentType.Video
                            });
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, "Push video exception => videoId:" + playloadDataShare?.MetaDataShared?.FirstOrDefault()?.ObjectId + ", exception:" + ex.Message);
            }
            return returnValue;
        }
        public static async Task<bool> PushVideoToOtherChannel(string sessionId, PayloadDataShare playloadDataShare, Distributor distribution)
        {
            var returnValue = false;
            var messageResult = "";
            try
            {
                if (distribution != null)
                {
                    var configVideoObj = Json.Parse<Dictionary<string, string>>(distribution?.Config?.ApiVideo);
                    if (configVideoObj == null) return false;
                    string configChannelType = null;
                    configVideoObj.TryGetValue("Type", out configChannelType);
                    var configApiUrl = configVideoObj["ApiUrl"];
                    var configApiKey = configVideoObj["ApiKey"];
                    if (configApiUrl != null && !string.IsNullOrEmpty(configApiUrl) && configApiKey != null && !string.IsNullOrEmpty(configApiKey))
                    {
                        var listVideo = await StoreFactory.Create<VideoCacheStore>().GetMultiVideoAsync(playloadDataShare.MetaDataShared?.Select(p => p.ObjectId.ToString())?.ToList());
                        var payLoad = new List<PayLoadShareVideo>();
                        var listVideoPegax = new List<FormUrlEncodedContent>();
                        if (listVideo != null || listVideo.Count == 0)
                        {
                            foreach (var video in listVideo)
                            {
                                int.TryParse(configChannelType, out int distributionType);
                                var objTrailer = new object();
                                if (string.IsNullOrEmpty(video.Trailer))
                                {
                                    objTrailer = new
                                    {
                                        video_id = video.Id,
                                        video_thumb = playloadDataShare.MetaDataShared?.Where(p => p.ObjectId == video.Id).FirstOrDefault()?.Avatar,
                                        video_filename = video.FileName,
                                        video_namespace = "KingContent",
                                        video_duration = video.Duration,
                                        video_title = playloadDataShare.MetaDataShared?.Where(p => p.ObjectId == video.Id).FirstOrDefault()?.Name,
                                        video_description = playloadDataShare.MetaDataShared?.Where(p => p.ObjectId == video.Id).FirstOrDefault()?.Description,
                                        type_square = 0,
                                        video_format = 0,
                                        video_detail_format = 0,
                                        video_url = AppSettings.Current.ChannelConfiguration.VideoDomain + video.FileName,
                                        @namespace = "KingContent",
                                        policyContentId = "",
                                        vid = video.FileName,
                                        keyVideo = video.KeyVideo
                                    };
                                }
                                switch (distributionType)
                                {
                                    case (int)DistributionType.Pegax:
                                        configVideoObj.TryGetValue("Username", out string username);

                                        var content = new FormUrlEncodedContent(new[]
                                        {
                                            new KeyValuePair<string, string>("email",username),
                                            new KeyValuePair<string, string>("password",configApiKey),
                                            new KeyValuePair<string, string>("NewsId",video.Id.ToString()),
                                            new KeyValuePair<string, string>("title",playloadDataShare.MetaDataShared?.Where(p => p.ObjectId == video.Id).FirstOrDefault()?.Name),
                                            new KeyValuePair<string, string>("ZoneId",(playloadDataShare.MetaDataShared?.Where(p => p.ObjectId == video.Id).FirstOrDefault()?.ZoneId.ToString()) ?? "0"),
                                            new KeyValuePair<string, string>("Url",video.Url),
                                            new KeyValuePair<string, string>("Sapo", playloadDataShare.MetaDataShared?.Where(p => p.ObjectId == video.Id).FirstOrDefault()?.Description),
                                            new KeyValuePair<string, string>("content",video.KeyVideo),
                                            new KeyValuePair<string, string>("Avatar",playloadDataShare.MetaDataShared?.Where(p => p.ObjectId == video.Id).FirstOrDefault()?.Avatar),
                                            new KeyValuePair<string, string>("AvatarDesc",video.MetaAvatar),
                                            new KeyValuePair<string, string>("Avatar1",""),
                                            new KeyValuePair<string, string>("Avatar2",""),
                                            new KeyValuePair<string, string>("Avatar3",""),
                                            new KeyValuePair<string, string>("Avatar4",""),
                                            new KeyValuePair<string, string>("Avatar5",""),
                                            new KeyValuePair<string, string>("ChannelID","0"),
                                            new KeyValuePair<string, string>("SourceName","pegax"),
                                            new KeyValuePair<string, string>("tags",""),
                                            new KeyValuePair<string, string>("NewsType","1000"),
                                            new KeyValuePair<string, string>("MediaType","0"),
                                            new KeyValuePair<string, string>("PublishDate",(video.DistributionDate ?? DateTime.Now).ToString("yyyy-MM-dd HH:mm:ss")),
                                            new KeyValuePair<string, string>("Lenght",video.Duration),
                                            new KeyValuePair<string, string>("Filename", video.FileName),
                                            new KeyValuePair<string, string>("Video_key",video.KeyVideo),
                                            new KeyValuePair<string, string>("Trailer",string.IsNullOrEmpty(video.Trailer) ? Json.Stringify(objTrailer) : video.Trailer),
                                            new KeyValuePair<string, string>("width",!string.IsNullOrEmpty(video.Size) ? video.Size.Split('x')[0] : "1080"),
                                            new KeyValuePair<string, string>("height",!string.IsNullOrEmpty(video.Size) ? video.Size.Split('x')[1] : "720"),
                                            new KeyValuePair<string, string>("Source","pegax"),
                                            new KeyValuePair<string, string>("Author", string.IsNullOrEmpty(video.Author) ? "Ánh Dương" : video.Author),
                                            new KeyValuePair<string, string>("ZoneName","")
                                        });
                                        listVideoPegax.Add(content);
                                        break;
                                    default:
                                        var videoShare = new VideoShare();
                                        videoShare.AllowAd = video.AllowAdv ?? false;
                                        videoShare.Author = string.IsNullOrEmpty(video.Author) ? "Ánh Dương" : video.Author;
                                        videoShare.Avatar = playloadDataShare.MetaDataShared?.Where(p => p.ObjectId == video.Id).FirstOrDefault()?.Avatar;
                                        videoShare.AvatarShareFacebook = "";
                                        videoShare.CreatedBy = video.CreatedBy;
                                        videoShare.CreatedDate = video.CreatedDate ?? DateTime.Now;
                                        videoShare.Description = playloadDataShare.MetaDataShared?.Where(p => p.ObjectId == video.Id).FirstOrDefault()?.Description;
                                        videoShare.DisplayStyle = video.DisplayStyle;
                                        videoShare.DistributionDate = playloadDataShare.MetaDataShared?.Where(p => p.ObjectId == video.Id).FirstOrDefault()?.DistributionDate ?? DateTime.Now;
                                        videoShare.Duration = video.Duration;
                                        videoShare.EditedBy = video.CreatedBy;
                                        videoShare.EditedDate = video.CreatedDate ?? DateTime.Now;
                                        videoShare.FileName = video.FileName;
                                        videoShare.GroupName = "";
                                        videoShare.HashId = "";
                                        videoShare.HtmlCode = video.HtmlCode;
                                        videoShare.IsConverted = video.IsConverted ?? false;
                                        videoShare.IsNewProcessed = true;
                                        videoShare.IsRemoveLogo = video.IsRemovedLogo ?? false;
                                        videoShare.KeyVideo = video.KeyVideo;
                                        videoShare.LastModifiedBy = video.ModifiedBy;
                                        videoShare.LastModifiedDate = video.ModifiedDate ?? DateTime.Now;
                                        videoShare.Location = video.Location;
                                        videoShare.MetaAvatar = video.MetaAvatar;
                                        videoShare.Mode = video.PublishMode ?? 0;
                                        videoShare.Name = playloadDataShare.MetaDataShared?.Where(p => p.ObjectId == video.Id).FirstOrDefault()?.Name;//
                                        videoShare.NewsId = video.Id;
                                        videoShare.OriginalId = video.OriginalId ?? 0;
                                        videoShare.OriginalUrl = video.OriginalUrl;
                                        videoShare.PlayOnTime = DateTime.Now;
                                        videoShare.Pname = "";
                                        videoShare.PolicyContentId = video.PolicyContentId;
                                        videoShare.Priority = 0;
                                        videoShare.PublishBy = video.CreatedBy;
                                        videoShare.PublishDate = video.DistributionDate ?? DateTime.Now;
                                        videoShare.Size = video.Size;
                                        videoShare.Source = string.IsNullOrEmpty(video.Source) ? "Trí Thức Trẻ" : video.Source;
                                        videoShare.Status = video.Status ?? 0;
                                        videoShare.Tags = "";
                                        videoShare.TrailerUrl = video.Trailer;
                                        videoShare.Type = video.Type ?? 0;
                                        videoShare.UnsignName = video.UnsignName;
                                        videoShare.Url = videoShare.Url;
                                        videoShare.VideoFolderId = 0;
                                        videoShare.VideoRelation = video.VideoRelation?.Replace(",", ";");
                                        videoShare.Views = 0;
                                        videoShare.ZoneId = (playloadDataShare.MetaDataShared?.Where(p => p.ObjectId == video.Id).FirstOrDefault()?.ZoneId) ?? 0;
                                        videoShare.ZoneName = playloadDataShare.MetaDataShared?.Where(p => p.ZoneId == video.Id).FirstOrDefault()?.ZoneName;
                                        videoShare.Namespace = "KingContent";
                                        payLoad.Add(new PayLoadShareVideo
                                        {
                                            Video = videoShare,
                                            TagIdList = "",
                                            PlaylistIdList = playloadDataShare.MetaDataShared?.Where(p => p.ObjectId == video.Id).FirstOrDefault()?.PlaylistRelation,
                                            ZoneIdList = playloadDataShare.MetaDataShared?.Where(p => p.ObjectId == video.Id).FirstOrDefault()?.ZoneRelation
                                        });
                                        break;
                                }
                            }
                        }
                        else
                        {
                            Logger.Error("Push Video: Video không tồn tại.");
                            return false;
                        }

                        switch (configChannelType)
                        {
                            case "1":
                                try
                                {
                                    using (var client = new HttpClient())
                                    {
                                        client.Timeout = TimeSpan.FromSeconds(AppSettings.Current.ChannelConfiguration.HttpClientTimeOut);
                                        foreach (var value in listVideoPegax)
                                        {
                                            var response = await client.PostAsync(configApiUrl, value);
                                            response.EnsureSuccessStatusCode();

                                            var result = await response.Content.ReadAsStringAsync();
                                            if (("ok").Equals(result))
                                            {
                                                returnValue = true;
                                                //Logger.Information("success:" + returnValue + ", channel:" + configApiUrl + ", data:" + Json.Stringify(value));
                                            }
                                            messageResult = result;
                                        }
                                    }
                                }
                                catch (Exception ex)
                                {
                                    Logger.Error(ex, "Push video exception:" + ex.Message);
                                }
                                break;
                            default:
                                try
                                {
                                    using (var client = new HttpClient())
                                    {
                                        client.Timeout = TimeSpan.FromSeconds(AppSettings.Current.ChannelConfiguration.HttpClientTimeOut);
                                        client.DefaultRequestHeaders.Add("Authorization", configApiKey);
                                        var data = new PayLoadShareVideoEntity { Data = payLoad };
                                        var content = new FormUrlEncodedContent(new[]
                                        {
                                            new KeyValuePair<string, string>("SessionId",sessionId.ToString()),
                                            new KeyValuePair<string, string>("PayLoad",Json.Stringify(data)),
                                            new KeyValuePair<string, string>("Action","share")
                                        });
                                        var resultData = await (await client.PostAsync(configApiUrl, content))?.Content?.ReadAsStringAsync();
                                        var result = Json.Parse<ActionResponse>(resultData);
                                        if (result != null)
                                        {
                                            returnValue = result.Success;
                                            messageResult = result?.Message;
                                        }
                                        else
                                        {
                                            Logger.Error("Push video return ActionResponse null:" + resultData);
                                        }
                                        Logger.Debug("PushVideoToChannel => result:" + Json.Stringify(result) + ", channel:" + configApiUrl);
                                    }
                                }
                                catch (Exception ex)
                                {
                                    Logger.Error(ex, "Push video exception:" + ex.Message);
                                }
                                break;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, "Push video exception :" + ex.Message);
            }
            return returnValue;
        }
        #endregion

        #region Share ShareLink
        public static async Task<bool> ShareShareLinkAsync(ItemStreamDistribution itemStreamDistribution, Distributor distribution, PayloadDataShare playloadDataShare, string sessionId, Media.Action action)
        {
            var returnValue = false;
            var isUpdate = true;
            try
            {
                var listPost = new List<ShareLinkSearch>();
                foreach (var metaData in playloadDataShare.MetaDataShared)
                {
                    if (metaData.ObjectId > 0)
                    {
                        var newsDistribution = new NewsDistribution
                        {
                            DistributorId = distribution.Id,
                            DistributorName = distribution.Name,
                            NewsId = metaData.ObjectId,
                            ItemStreamId = itemStreamDistribution.Id,
                            SharedDate = itemStreamDistribution.CreatedDate,
                            SharedZone = metaData.ZoneId.ToString() + (!string.IsNullOrEmpty(metaData.ZoneRelation) ? ("," + metaData.ZoneRelation) : "")
                        };

                        var post = await ShareLinkRepository.GetShareLinkSearchByIdAsync(metaData.ObjectId);
                        if (post != null)
                        {
                            if (post.DistributionDate == null) post.DistributionDate = DateTime.Now;
                            if (post.NewsDistribution == null)
                            {
                                isUpdate = false;
                                post.NewsDistribution = new[] { newsDistribution };
                            }
                            else
                            {
                                if (post.NewsDistribution.Count(w => w.DistributorId == newsDistribution.DistributorId && w.ItemStreamId == itemStreamDistribution.Id) == 0)
                                {
                                    isUpdate = false;
                                    post.NewsDistribution = post.NewsDistribution.Append(newsDistribution).ToArray();
                                }
                            }
                            listPost.Add(post);
                        }
                        else
                        {
                            return false;
                        }
                    }
                }
                if (listPost.Count > 0)
                {
                    returnValue = await StoreFactory.Create<DistributionCacheStore>().ShareShareLinkAsync(itemStreamDistribution, listPost, isUpdate);
                    if (returnValue)
                    {
                        //push to queue share video
                        await Function.AddToQueue(ActionName.ShareShareLink, TopicName.SHARE, new QueueData<string, PayloadDataShare, string, Media.Action>
                        {
                            Data1 = sessionId,
                            Data2 = playloadDataShare,
                            Data3 = sessionId,
                            Data4 = action
                        });

                        await Function.AddToQueue(ActionName.ShareShareLinkES, TopicName.ES_SHARE_LINK, new QueueData<ItemStreamDistribution, List<ShareLinkSearch>, bool, object> { Data1 = itemStreamDistribution, Data2 = listPost, Data3 = isUpdate });

                        var listNewsDistribution = new List<NewsDistribution>();
                        foreach (var media in listPost)
                        {
                            var listTmp = media.NewsDistribution.Select(p => p).Where(p => p.ItemStreamId == itemStreamDistribution.Id && p.DistributorId == distribution.Id).ToList();
                            listTmp.ForEach(p => p.NewsId = media.Id);
                            listNewsDistribution.AddRange(listTmp);
                        }
                        //push queue
                        await Function.AddToQueue(ActionName.ShareShareLinkSQL, TopicName.SQL_SHARE_LINK, new QueueData<ItemStreamDistribution, List<NewsDistribution>, bool, object> { Data1 = itemStreamDistribution, Data2 = listNewsDistribution, Data3 = isUpdate });
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
            }
            return returnValue;
        }
        public static async Task<bool> PushShareLinkToChannel(string sessionId, PayloadDataShare playloadDataShare, Distributor distribution, string clientIP)
        {
            var lstObj = new List<object>();
            var returnValue = false;
            try
            {
                if (distribution != null)
                {
                    var configMediaObj = Json.Parse<Dictionary<string, string>>(distribution?.Config.ApiMediaUnit);
                    if (configMediaObj == null) return false;

                    var configApiUrl = configMediaObj["ApiUrl"];
                    var configApiKey = configMediaObj["ApiKey"];
                    if (configApiUrl != null && !string.IsNullOrEmpty(configApiUrl) && configApiKey != null && !string.IsNullOrEmpty(configApiKey))
                    {
                        var listMediaKingHub = new List<FormUrlEncodedContent>();
                        if (playloadDataShare != null && playloadDataShare.MetaDataShared?.Count > 0)
                        {
                            foreach (var media in playloadDataShare.MetaDataShared)
                            {
                                var is_comment = (int)AppSettings.Current.ChannelConfiguration.ApiCommentSetting.Mode.PostCheck;
                                if (media.CommentMode == CommentMode.Disable)
                                {
                                    is_comment = (int)AppSettings.Current.ChannelConfiguration.ApiCommentSetting.Mode.Disable;
                                }
                                else
                                {
                                    if (media.CommentMode == CommentMode.PreCheck) is_comment = (int)AppSettings.Current.ChannelConfiguration.ApiCommentSetting.Mode.PreCheck;
                                }

                                var obj = new
                                {
                                    mediaid = media.ObjectId.ToString(),
                                    title = media.Title??string.Empty,
                                    obj_name = media.Name??string.Empty,
                                    cardtype = ((int) CardType.ShareNews).ToString(),
                                    desc = Json.Stringify(media.ItemMediaUnit?.Select(s=> new {
                                        id = s.Id??string.Empty,
                                        title = s.Title??string.Empty,
                                        link = s.Link??string.Empty,
                                        image = new { thumb = s.Image?.Thumb??string.Empty, width = s.Image?.Width == 0 || s.Image?.Width == null ? 600 : s.Image?.Width, height = s.Image?.Height == 0 || s.Image?.Height == null ? 300 : s.Image?.Height, content_type = s.ContentType==ContentType.Image ? (int) s.ContentType : (int)ContentType.Photo },
                                        source = s.Source??string.Empty,
                                        content_type = (int) ContentType.Linkshare,
                                        data_type = s.DataType !=null ?(int) s.DataType :(int) DataType.Webview,
                                        original_url = s.original_url??string.Empty,
                                        created_at = (s.CreatedDate??DateTime.Now).ToString("yyyy-MM-ddTHH:mm:ssZ")
                                    }).ToList()),
                                    channelid = media.ChannelId??string.Empty,
                                    frameid = media.Frameid.ToString(),
                                    tags = media.Tags??string.Empty,
                                    userid = media.UserId.ToString(),
                                    username = media.UserName??string.Empty,
                                    fullname = media.FullName??string.Empty,
                                    avatar = media.AuthorAvatar??string.Empty,
                                    catid = media.ZoneId?.ToString()??string.Empty,
                                    isdeleted = media.IsDeleted.ToString(),
                                    createtime = (media.DistributionDate??DateTime.Now).ToString("yyyy-MM-ddTHH:mm:ssZ"),
                                    catname = media.ZoneName??string.Empty,
                                    boardids = media.BoardIds != null && media.BoardIds.Length > 0 ? string.Join(",", media.BoardIds) : string.Empty,
                                    ext = Json.StringifyFirstLowercase(new {
                                        status = media.CaptionExt,
                                        flag = new
                                        {
                                            is_comment = is_comment
                                        }
                                    }),
                                };
                                lstObj.Add(obj);
                                listMediaKingHub.Add(new FormUrlEncodedContent(obj.ConvertToKeyValuePair()));
                            }
                        }
                        else
                        {
                            Logger.Error("Push ShareLink: ShareLink không tồn tại.");
                            return false;
                        }

                        foreach (var value in listMediaKingHub)
                        {
                            using (var client = new HttpClient())
                            {
                                try
                                {
                                    client.Timeout = TimeSpan.FromSeconds(AppSettings.Current.ChannelConfiguration.HttpClientTimeOut);
                                    var response = await client.PostAsync(configApiUrl, value);
                                    if (!response.IsSuccessStatusCode)
                                    {
                                        response.EnsureSuccessStatusCode();
                                    }
                                    var result = await response.Content.ReadAsStringAsync();
                                    if (!string.IsNullOrEmpty(result))
                                    {
                                        returnValue = Json.Parse<dynamic>(result).result;
                                        if (!returnValue)
                                        {
                                            Logger.Debug("Data push sharelink =>" + Json.Stringify(lstObj));
                                            Logger.Debug("PushVideoToChannel ERROR => configApiUrl:" + configApiUrl + ", data:" + string.Join(",", playloadDataShare.MetaDataShared?.Select(p => p.ObjectId.ToString()).ToArray() ?? new string[] { }) + ", result:" + result);
                                        }
                                    }
                                }
                                catch (Exception ex)
                                {
                                    Logger.Error(ex, "Push ShareLink exception => " + Json.Stringify(new
                                    {
                                        data = lstObj,
                                        exception = ex.Message
                                    }));
                                }
                            }
                        }
                    }
                }

                if (returnValue)
                {
                    if (playloadDataShare != null && playloadDataShare.MetaDataShared?.Count > 0)
                    {
                        foreach (var media in playloadDataShare.MetaDataShared)
                        {
                            //insert log
                            await Function.AddToQueue(ActionName.InsertLog, TopicName.LOGACTION, new LogActionEntity
                            {
                                ObjectId = media.ObjectId.ToString(),
                                Account = media.UserId.ToString(),
                                CreatedDate = DateTime.SpecifyKind(DateTime.Now, DateTimeKind.Unspecified),
                                Ip = clientIP,
                                Message = "Success.",
                                Title = media.Title,
                                Status = returnValue ? 1 : 0,
                                SourceId = media.UserId.ToString(),
                                ActionTypeDetail = (int)ActionType.PushToApp,
                                ActionText = ActionType.PushToApp.ToString(),
                                Type = (int)LogContentType.ShareLink
                            });
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, "Push ShareLink exception => id: " + playloadDataShare?.MetaDataShared?.FirstOrDefault()?.ObjectId + ", exception:" + ex.Message);
            }
            return returnValue;
        }
        #endregion

        #region Share Post
        public static async Task<bool> SharePostAsync(ItemStreamDistribution itemStreamDistribution, Distributor distribution, PayloadDataShare playloadDataShare, string clientIP)
        {
            var returnValue = false;
            var isUpdate = true;
            try
            {
                var listPost = new List<PostSearch>();
                foreach (var metaData in playloadDataShare.MetaDataShared)
                {
                    if (metaData.ObjectId > 0)
                    {
                        var newsDistribution = new NewsDistribution
                        {
                            DistributorId = distribution.Id,
                            DistributorName = distribution.Name,
                            NewsId = metaData.ObjectId,
                            ItemStreamId = itemStreamDistribution.Id,
                            SharedDate = itemStreamDistribution.CreatedDate,
                            SharedZone = metaData.ZoneId.ToString() + (!string.IsNullOrEmpty(metaData.ZoneRelation) ? ("," + metaData.ZoneRelation) : "")
                        };

                        var post = await PostRepository.GetPostSearchByIdAsync(metaData.ObjectId);
                        if (post != null)
                        {
                            if (post.DistributionDate == null) post.DistributionDate = DateTime.Now;
                            if (post.NewsDistribution == null)
                            {
                                isUpdate = false;
                                post.NewsDistribution = new[] { newsDistribution };
                            }
                            else
                            {
                                if (post.NewsDistribution.Count(w => w.DistributorId == newsDistribution.DistributorId && w.ItemStreamId == itemStreamDistribution.Id) == 0)
                                {
                                    isUpdate = false;
                                    post.NewsDistribution = post.NewsDistribution.Append(newsDistribution).ToArray();
                                }
                            }
                            listPost.Add(post);
                        }
                        else
                        {
                            return false;
                        }
                    }
                }
                if (listPost.Count > 0)
                {
                    returnValue = await StoreFactory.Create<DistributionCacheStore>().SharePostAsync(itemStreamDistribution, listPost, isUpdate);
                    if (returnValue)
                    {
                        //push to queue share video
                        await Function.AddToQueue(ActionName.SharePost, TopicName.SHARE, new QueueData<string, PayloadDataShare, Distributor, string>
                        {
                            Data1 = Guid.NewGuid().ToString(),
                            Data2 = playloadDataShare,
                            Data3 = distribution,
                            Data4 = clientIP
                        });

                        await Function.AddToQueue(ActionName.SharePostES, TopicName.ES_POST, new QueueData<ItemStreamDistribution, List<PostSearch>, bool, object> { Data1 = itemStreamDistribution, Data2 = listPost, Data3 = isUpdate });

                        var listNewsDistribution = new List<NewsDistribution>();
                        foreach (var media in listPost)
                        {
                            var listTmp = media.NewsDistribution.Select(p => p).Where(p => p.ItemStreamId == itemStreamDistribution.Id && p.DistributorId == distribution.Id).ToList();
                            listTmp.ForEach(p => p.NewsId = media.Id);
                            listNewsDistribution.AddRange(listTmp);
                        }
                        //push queue
                        await Function.AddToQueue(ActionName.SharePostSQL, TopicName.SQL_POST, new QueueData<ItemStreamDistribution, List<NewsDistribution>, bool, object> { Data1 = itemStreamDistribution, Data2 = listNewsDistribution, Data3 = isUpdate });
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
            }
            return returnValue;
        }
        public static async Task<bool> PushPostToChannel(string sessionId, PayloadDataShare playloadDataShare, Distributor distribution, string clientIP)
        {
            var lstObj = new List<object>();
            var returnValue = false;
            try
            {
                if (distribution != null)
                {
                    var configMediaObj = Json.Parse<Dictionary<string, string>>(distribution?.Config.ApiMediaUnit);
                    if (configMediaObj == null) return false;

                    var configApiUrl = configMediaObj["ApiUrl"];
                    var configApiKey = configMediaObj["ApiKey"];
                    if (configApiUrl != null && !string.IsNullOrEmpty(configApiUrl) && configApiKey != null && !string.IsNullOrEmpty(configApiKey))
                    {
                        var listMediaKingHub = new List<FormUrlEncodedContent>();
                        if (playloadDataShare != null && playloadDataShare.MetaDataShared?.Count > 0)
                        {
                            foreach (var media in playloadDataShare.MetaDataShared)
                            {
                                var is_comment = (int)AppSettings.Current.ChannelConfiguration.ApiCommentSetting.Mode.PostCheck;
                                if (media.CommentMode == CommentMode.Disable)
                                {
                                    is_comment = (int)AppSettings.Current.ChannelConfiguration.ApiCommentSetting.Mode.Disable;
                                }
                                else
                                {
                                    if (media.CommentMode == CommentMode.PreCheck) is_comment = (int)AppSettings.Current.ChannelConfiguration.ApiCommentSetting.Mode.PreCheck;
                                }
                                
                                var obj = new 
                                {
                                    mediaid = media.ObjectId.ToString(),
                                    title = media.Title??string.Empty,
                                    obj_name = media.Name??string.Empty,
                                    cardtype = ((int)CardType.Text).ToString(),
                                    desc = Json.Stringify(media.ItemMediaUnit?.Select(s=> new{}).ToList()),
                                    channelid = media.ChannelId??string.Empty,
                                    frameid = media.Frameid.ToString(),
                                    tags = media.Tags??string.Empty,
                                    userid = media.UserId.ToString(),
                                    username = media.UserName??string.Empty,
                                    fullname = media.FullName??string.Empty,
                                    avatar = media.AuthorAvatar??string.Empty,
                                    catid = media.ZoneId?.ToString()??string.Empty,
                                    isdeleted = media.IsDeleted.ToString(),
                                    createtime = (media.DistributionDate??DateTime.Now).ToString("yyyy-MM-ddTHH:mm:ssZ"),
                                    catname = media.ZoneName??string.Empty,
                                    boardids = media.BoardIds != null && media.BoardIds.Length > 0 ? string.Join(",", media.BoardIds) : string.Empty,
                                    ext = Json.StringifyFirstLowercase(new {
                                        status = media.CaptionExt,
                                        flag = new
                                        {
                                            is_comment = is_comment
                                        }
                                    }),
                                };

                                lstObj.Add(obj);
                                listMediaKingHub.Add(new FormUrlEncodedContent(obj.ConvertToKeyValuePair()));
                            }
                        }
                        else
                        {
                            Logger.Error("Push Post: Post không tồn tại.");
                            return false;
                        }
                        foreach (var value in listMediaKingHub)
                        {
                            using (var client = new HttpClient())
                            {
                                try
                                {
                                    client.Timeout = TimeSpan.FromSeconds(AppSettings.Current.ChannelConfiguration.HttpClientTimeOut);
                                    var response = await client.PostAsync(configApiUrl, value);
                                    if (!response.IsSuccessStatusCode)
                                    {
                                        //Logger.Debug("PushPostToChannel 1.=>postId:" + playloadDataShare?.MetaDataShared?.FirstOrDefault()?.ObjectId + ", response: " + Json.Stringify(response));
                                        response.EnsureSuccessStatusCode();
                                    }
                                    var result = await response.Content.ReadAsStringAsync();
                                    if (!string.IsNullOrEmpty(result))
                                    {
                                        returnValue = Json.Parse<dynamic>(result).result;
                                        if (!returnValue)
                                        {
                                            Logger.Debug("Data push post => " + Json.Stringify(lstObj));
                                            Logger.Debug("PushPostToChannel ERROR => configApiUrl:" + configApiUrl + ", data:" + string.Join(",", playloadDataShare.MetaDataShared?.Select(p => p.ObjectId.ToString()).ToArray() ?? new string[] { }) + ", result:" + result);
                                        }
                                    }
                                }
                                catch (Exception ex)
                                {
                                    Logger.Error(ex, "Push post exception => "+Json.Stringify(new
                                    {
                                        data = lstObj,
                                        exception = ex.Message
                                    }));
                                }
                            }
                        }
                    }
                }
                if (returnValue)
                {
                    if (playloadDataShare != null && playloadDataShare.MetaDataShared?.Count > 0)
                    {
                        foreach (var media in playloadDataShare.MetaDataShared)
                        {
                            //insert log
                            await Function.AddToQueue(ActionName.InsertLog, TopicName.LOGACTION, new LogActionEntity
                            {
                                ObjectId = media.ObjectId.ToString(),
                                Account = media.UserId.ToString(),
                                CreatedDate = DateTime.SpecifyKind(DateTime.Now, DateTimeKind.Unspecified),
                                Ip = clientIP,
                                Message = "Success.",
                                Title = media.Title,
                                Status = returnValue ? 1 : 0,
                                SourceId = media.UserId.ToString(),
                                ActionTypeDetail = (int)ActionType.PushToApp,
                                ActionText = ActionType.PushToApp.ToString(),
                                Type = (int)LogContentType.Post
                            });
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, "Push post exception :postId:" + playloadDataShare?.MetaDataShared?.FirstOrDefault()?.ObjectId + ", exception:" + ex.Message);
            }
            return returnValue;
        }
        #endregion

        #region Share MediaUnit
        public static async Task<bool> ShareMediaUnitAsync(ItemStreamDistribution itemStreamDistribution, Distributor distribution, PayloadDataShare playloadDataShare, string clientIP, string sessionId, Media.Action action)
        {
            var returnValue = false;
            try
            {
                var listMedia = new List<MediaUnitSearch>();
                foreach (var metaData in playloadDataShare.MetaDataShared)
                {
                    if (metaData.ObjectId > 0)
                    {
                        var newsDistribution = new NewsDistribution
                        {
                            DistributorId = distribution.Id,
                            DistributorName = distribution.Name,
                            NewsId = metaData.ObjectId,
                            ItemStreamId = itemStreamDistribution.Id,
                            SharedDate = itemStreamDistribution.CreatedDate,
                            SharedZone = metaData.ZoneId.ToString() + (!string.IsNullOrEmpty(metaData.ZoneRelation) ? ("," + metaData.ZoneRelation) : "")
                        };

                        var media = await MediaUnitRepository.GetDetailByIdAsync(metaData.ObjectId);
                        if (media != null)
                        {
                            metaData.DistributionDate = metaData.DistributionDate.HasValue ? metaData.DistributionDate : media.CreatedDate;

                            if (media.DistributionDate == null) media.DistributionDate = DateTime.Now;

                            if (media.AllowSale == true || !string.IsNullOrEmpty(media.CallPhone))
                            {
                                metaData.ShopInfo = new ShopInfo
                                {
                                    Phone = media.CallPhone,
                                    Email = media.Email
                                };
                            }
                            if (media.NewsDistribution == null)
                                media.NewsDistribution = new[] { newsDistribution };
                            else
                            {
                                if (media.NewsDistribution.Count(w => w.DistributorId == newsDistribution.DistributorId && w.ItemStreamId != itemStreamDistribution.Id) == 0)
                                    media.NewsDistribution = media.NewsDistribution.Append(newsDistribution).ToArray();
                            }
                            listMedia.Add(media);
                        }
                    }
                }
                if (listMedia.Count > 0)
                {
                    returnValue = await StoreFactory.Create<DistributionCacheStore>().ShareMediaUnitAsync(itemStreamDistribution, listMedia);
                    if (returnValue)
                    {
                        //push queue trước khi đẩy sang kênh
                        await Function.AddToQueue(ActionName.ShareMediaUnit, TopicName.SHARE, new QueueData<string, PayloadDataShare, string, Media.Action>
                        {
                            Data1 = Guid.NewGuid().ToString(),
                            Data2 = playloadDataShare,
                            Data3 = sessionId,
                            Data4 = action
                        });

                        await Function.AddToQueue(ActionName.ShareMediaUnitES, TopicName.ES_MEDIA_UNIT, new QueueShareDataUnit<MediaUnitSearch> { ItemStreamDistribution = itemStreamDistribution, ListMedia = listMedia });
                        //push queue
                        await Function.AddToQueue(ActionName.ShareMediaUnitSQL, TopicName.SQL_MEDIA_UNIT, new QueueShareDataUnit<MediaUnitSearch> { ItemStreamDistribution = itemStreamDistribution, ListMedia = listMedia });
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
            }
            return returnValue;
        }
        public static async Task<bool> PushMediaUnitToChannel(string sessionId, PayloadDataShare playloadDataShare, Distributor distribution, string clientIP)
        {
            var lstObj = new List<object>();
            var returnValue = false;
            try
            {
                if (distribution != null)
                {
                    var configMediaObj = Json.Parse<Dictionary<string, string>>(distribution?.Config.ApiMediaUnit);
                    if (configMediaObj == null) return false;

                    var configApiUrl = configMediaObj["ApiUrl"];
                    var configApiKey = configMediaObj["ApiKey"];
                    if (configApiUrl != null && !string.IsNullOrEmpty(configApiUrl) && configApiKey != null && !string.IsNullOrEmpty(configApiKey))
                    {
                        var listMediaKingHub = new List<FormUrlEncodedContent>();
                        if (playloadDataShare != null && playloadDataShare.MetaDataShared?.Count > 0)
                        {
                            foreach (var media in playloadDataShare.MetaDataShared)
                            {
                                var is_comment = (int)AppSettings.Current.ChannelConfiguration.ApiCommentSetting.Mode.PostCheck;
                                if (media.CommentMode == CommentMode.Disable)
                                {
                                    is_comment = (int)AppSettings.Current.ChannelConfiguration.ApiCommentSetting.Mode.Disable;
                                }
                                else
                                {
                                    if (media.CommentMode == CommentMode.PreCheck) is_comment = (int)AppSettings.Current.ChannelConfiguration.ApiCommentSetting.Mode.PreCheck;
                                }

                                var obj = new
                                {
                                    mediaid = media.ObjectId.ToString(),
                                    title = media.Title??string.Empty,
                                    obj_name = media.Name??string.Empty??string.Empty,
                                    cardtype = ((int)CardType.VideoAndImage).ToString(),
                                    desc = Json.Stringify(media.ItemMediaUnit.Select(s=> new{
                                        id = s.Id??string.Empty,
                                        title = s.Title??string.Empty,
                                        link = s.Link??string.Empty,
                                        thumb = s.Thumb??string.Empty,
                                        type = s.TypeMedia??string.Empty,
                                        duration = s.Duration??string.Empty,
                                        width = s.Width,
                                        height = s.Height,
                                        content_type = s.ContentType!=null && (s.ContentType==ContentType.Image || s.ContentType==ContentType.Photo || s.ContentType==ContentType.Video) ? (int) s.ContentType : (int) ContentType.Photo,
                                        position = s.Position,
                                        is_play = s.IsPlay,
                                        created_at = (s.CreatedDate??DateTime.Now).ToString("yyyy-MM-ddTHH:mm:ssZ")
                                    }).ToList()),
                                    channelid = media.ChannelId??string.Empty,
                                    frameid = media.Frameid.ToString(),
                                    tags = media.Tags??string.Empty,
                                   // shopinfo = media.ShopInfo!=null ? Json.Stringify(new { phone = media.ShopInfo?.Phone??string.Empty, email = media.ShopInfo?.Email??string.Empty}) : string.Empty,
                                    userid = media.UserId.ToString(),
                                    username = media.UserName??string.Empty,
                                    fullname = media.FullName??string.Empty,
                                    avatar = media.AuthorAvatar??string.Empty,
                                    catid = media.ZoneId?.ToString()??string.Empty,
                                    isdeleted = media.IsDeleted.ToString(),
                                    createtime = (media.DistributionDate??DateTime.Now).ToString("yyyy-MM-ddTHH:mm:ssZ"),
                                    catname = media.ZoneName??string.Empty,
                                    boardids = media.BoardIds != null && media.BoardIds.Length > 0 ? string.Join(",", media.BoardIds) : string.Empty,
                                    ext = Json.StringifyFirstLowercase(new {
                                        status = media.CaptionExt,
                                        flag = new
                                        {
                                            is_comment = is_comment
                                        }
                                    }),
                                };

                                lstObj.Add(obj);
                                listMediaKingHub.Add(new FormUrlEncodedContent(obj.ConvertToKeyValuePair()));
                            }
                        }
                        else
                        {
                            Logger.Error("Push Media Unit: Media không tồn tại.");
                            return false;
                        }
                        foreach (var value in listMediaKingHub)
                        {
                            using (var client = new HttpClient())
                            {
                                try
                                {
                                    client.Timeout = TimeSpan.FromSeconds(AppSettings.Current.ChannelConfiguration.HttpClientTimeOut);
                                    var response = await client.PostAsync(configApiUrl, value);
                                    if (!response.IsSuccessStatusCode)
                                    {
                                        response.EnsureSuccessStatusCode();
                                    }
                                    var result = await response.Content.ReadAsStringAsync();
                                    if (!string.IsNullOrEmpty(result))
                                    {
                                        returnValue = Json.Parse<dynamic>(result).result;
                                        if (!returnValue)
                                        {
                                            Logger.Sensitive("Push media unit not success.", lstObj);
                                        }
                                    }
                                }
                                catch (Exception ex)
                                {
                                    Logger.Sensitive(ex, "Push media unit exception => " + Json.Stringify(new
                                    {
                                        data = lstObj,
                                        exception = ex.Message
                                    }));
                                }
                            }
                        }
                    }
                }
                if (returnValue)
                {
                    if (playloadDataShare != null && playloadDataShare.MetaDataShared?.Count > 0)
                    {
                        foreach (var media in playloadDataShare.MetaDataShared)
                        {
                            //insert log
                            await Function.AddToQueue(ActionName.InsertLog, TopicName.LOGACTION, new LogActionEntity
                            {
                                ObjectId = media.ObjectId.ToString(),
                                Account = media.UserId.ToString(),
                                CreatedDate = DateTime.SpecifyKind(DateTime.Now, DateTimeKind.Unspecified),
                                Ip = clientIP,
                                Message = "Success.",
                                Title = media.Title,
                                Status = returnValue ? 1 : 0,
                                SourceId = media.UserId.ToString(),
                                ActionTypeDetail = (int)ActionType.PushToApp,
                                ActionText = ActionType.PushToApp.ToString(),
                                Type = (int)LogContentType.MediaUnit
                            });
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Sensitive(ex, "Push media unit exception :" + ex.Message);
            }
            return returnValue;
        }
        #endregion

        #region Share Album
        public static async Task<bool> ShareAlbumAsync(ItemStreamDistribution itemStreamDistribution, Distributor distribution, PayloadDataShare playloadDataShare, string clientIP, string sessionId, Media.Action action)
        {
            var returnValue = false;
            try
            {
                var listMedia = new List<AlbumSearch>();
                foreach (var metaData in playloadDataShare.MetaDataShared)
                {
                    if (metaData.ObjectId > 0)
                    {
                        var newsDistribution = new NewsDistribution
                        {
                            DistributorId = distribution.Id,
                            DistributorName = distribution.Name,
                            NewsId = metaData.ObjectId,
                            ItemStreamId = itemStreamDistribution.Id,
                            SharedDate = itemStreamDistribution.CreatedDate,
                            SharedZone = metaData.ZoneId.ToString() + (!string.IsNullOrEmpty(metaData.ZoneRelation) ? ("," + metaData.ZoneRelation) : "")
                        };

                        var media = await AlbumRepository.GetDetailByIdAsync(metaData.ObjectId);
                        if (media != null)
                        {
                            metaData.DistributionDate = metaData.DistributionDate.HasValue ? metaData.DistributionDate : media.CreatedDate;

                            if (media.DistributionDate == null) media.DistributionDate = DateTime.Now;

                            if (media.NewsDistribution == null)
                                media.NewsDistribution = new[] { newsDistribution };
                            else
                            {
                                if (media.NewsDistribution.Count(w => w.DistributorId == newsDistribution.DistributorId && w.ItemStreamId != itemStreamDistribution.Id) == 0)
                                    media.NewsDistribution = media.NewsDistribution.Append(newsDistribution).ToArray();
                            }
                            listMedia.Add(media);
                        }
                    }
                }
                if (listMedia.Count > 0)
                {
                    returnValue = await StoreFactory.Create<DistributionCacheStore>().ShareAlbumAsync(itemStreamDistribution, listMedia);
                    if (returnValue)
                    {
                        //push queue trước khi đẩy sang kênh
                        await Function.AddToQueue(ActionName.ShareAlbum, TopicName.SHARE, new QueueData<string, PayloadDataShare, string, Media.Action>
                        {
                            Data1 = Guid.NewGuid().ToString(),
                            Data2 = playloadDataShare,
                            Data3 = sessionId,
                            Data4 = action
                        });

                        await Function.AddToQueue(ActionName.ShareAlbumES, TopicName.ES_ALBUM, new QueueShareDataUnit<AlbumSearch> { ItemStreamDistribution = itemStreamDistribution, ListMedia = listMedia });

                        //push queue
                        await Function.AddToQueue(ActionName.ShareAlbumSQL, TopicName.SQL_ALBUM, new QueueShareDataUnit<AlbumSearch> { ItemStreamDistribution = itemStreamDistribution, ListMedia = listMedia });
                    }

                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
            }
            return returnValue;
        }
        public static async Task<bool> PushAlbumToChannel(string sessionId, PayloadDataShare playloadDataShare, Distributor distribution, string clientIP)
        {
            var lstObj = new List<object>();
            var returnValue = false;
            try
            {
                if (distribution != null)
                {
                    var configMediaObj = Json.Parse<Dictionary<string, string>>(distribution?.Config.ApiMediaUnit);
                    if (configMediaObj == null) return false;

                    var configApiUrl = configMediaObj["ApiUrl"];
                    var configApiKey = configMediaObj["ApiKey"];
                    if (configApiUrl != null && !string.IsNullOrEmpty(configApiUrl) && configApiKey != null && !string.IsNullOrEmpty(configApiKey))
                    {
                        var listMediaKingHub = new List<FormUrlEncodedContent>();
                        if (playloadDataShare != null && playloadDataShare.MetaDataShared?.Count > 0)
                        {
                            foreach (var media in playloadDataShare.MetaDataShared)
                            {
                                var is_comment = (int)AppSettings.Current.ChannelConfiguration.ApiCommentSetting.Mode.PostCheck;
                                if (media.CommentMode == CommentMode.Disable)
                                {
                                    is_comment = (int)AppSettings.Current.ChannelConfiguration.ApiCommentSetting.Mode.Disable;
                                }
                                else
                                {
                                    if (media.CommentMode == CommentMode.PreCheck) is_comment = (int)AppSettings.Current.ChannelConfiguration.ApiCommentSetting.Mode.PreCheck;
                                }

                                var obj = new 
                                {
                                    mediaid = media.ObjectId.ToString(),
                                    title = media.Title??string.Empty,
                                    obj_name = media.Name??string.Empty,
                                    cardtype = ((int)CardType.Album).ToString(),
                                    desc = Json.Stringify(media.ItemMediaUnit?.Select(s=> new{
                                        id = s.Id??string.Empty,
                                        title = s.Title??string.Empty,
                                        link = s.Link??string.Empty,
                                        thumb = string.IsNullOrEmpty(s.Thumb) ? s.Link : s.Thumb,
                                        width = s.Width,
                                        height = s.Height,
                                        content_type = s.ContentType == ContentType.Image ? (int) s.ContentType : (int)ContentType.Photo,
                                        is_heart_drop = s.IsHeartDrop,
                                        created_at = (s.CreatedDate??DateTime.Now).ToString("yyyy-MM-ddTHH:mm:ssZ")
                                    }).ToList()),
                                    channelid = media.ChannelId??string.Empty,
                                    frameid = media.Frameid==0 ? "1" : media.Frameid.ToString(),
                                    tags = media.Tags??string.Empty,
                                    userid = media.UserId.ToString(),
                                    username = media.UserName??string.Empty,
                                    fullname = media.FullName??string.Empty,
                                    avatar = media.AuthorAvatar??string.Empty,
                                    catid = media.ZoneId?.ToString()??string.Empty,
                                    isdeleted = media.IsDeleted.ToString(),
                                    createtime = (media.DistributionDate??DateTime.Now).ToString("yyyy-MM-ddTHH:mm:ssZ"),
                                    catname = media.ZoneName??string.Empty,
                                    boardids = media.BoardIds != null && media.BoardIds.Length > 0 ? string.Join(",", media.BoardIds) : string.Empty,
                                    ext = Json.StringifyFirstLowercase(new {
                                        status = media.CaptionExt,
                                        flag = new
                                        {
                                            is_comment = is_comment
                                        },
                                        label = new
                                        {
                                            id = ((int?)media.LabelType)?.ToString(),
                                            text = media.LabelType != null ? Enum.GetName(typeof(LabelType), media.LabelType)?.ToLower() : ""
                                        }
                                    }),
                                };

                                lstObj.Add(obj);
                                listMediaKingHub.Add(new FormUrlEncodedContent(obj.ConvertToKeyValuePair()));
                            }
                        }
                        else
                        {
                            Logger.Error("Push Album: Album không tồn tại.");
                            return false;
                        }

                        using (var client = new HttpClient())
                        {
                            client.Timeout = TimeSpan.FromSeconds(AppSettings.Current.ChannelConfiguration.HttpClientTimeOut);
                            foreach (var value in listMediaKingHub)
                            {
                                try
                                {
                                    var response = await client.PostAsync(configApiUrl, value);
                                    if (!response.IsSuccessStatusCode)
                                    {
                                        response.EnsureSuccessStatusCode();
                                    }
                                    var result = await response.Content.ReadAsStringAsync();
                                    if (!string.IsNullOrEmpty(result))
                                    {
                                        returnValue = Json.Parse<dynamic>(result).result;
                                        if (!returnValue)
                                        {
                                            Logger.Sensitive("Data push Album not success", lstObj);
                                        }
                                    }
                                }
                                catch (Exception ex)
                                {
                                    Logger.Sensitive(ex, "Push Album exception => " + Json.Stringify(new
                                    {
                                        data = lstObj,
                                        exception = ex.Message
                                    }));
                                }
                            }
                        }
                    }
                }
                if (returnValue)
                {
                    if (playloadDataShare != null && playloadDataShare.MetaDataShared?.Count > 0)
                    {
                        foreach (var media in playloadDataShare.MetaDataShared)
                        {
                            //insert log
                            await Function.AddToQueue(ActionName.InsertLog, TopicName.LOGACTION, new LogActionEntity
                            {
                                ObjectId = media.ObjectId.ToString(),
                                Account = media.UserId.ToString(),
                                CreatedDate = DateTime.SpecifyKind(DateTime.Now, DateTimeKind.Unspecified),
                                Ip = clientIP,
                                Message = "Success.",
                                Title = media.Title,
                                Status = returnValue ? 1 : 0,
                                SourceId = media.UserId.ToString(),
                                ActionTypeDetail = (int)ActionType.PushToApp,
                                ActionText = ActionType.PushToApp.ToString(),
                                Type = (int)LogContentType.Album
                            });
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Sensitive(ex, "Push Album exception :" + ex.Message);
            }
            return returnValue;
        }
        #endregion

        #region gallery
        public static async Task<bool> ShareGalleryAsync(ItemStreamDistribution itemStreamDistribution, Distributor distribution, PayloadDataShare playloadDataShare, string clientIP, string sessionId, Media.Action action)
        {
            var returnValue = false;
            try
            {
                var listMedia = new List<GallerySearch>();
                foreach (var metaData in playloadDataShare.MetaDataShared)
                {
                    if (metaData.ObjectId > 0)
                    {
                        var newsDistribution = new NewsDistribution
                        {
                            DistributorId = distribution.Id,
                            DistributorName = distribution.Name,
                            NewsId = metaData.ObjectId,
                            ItemStreamId = itemStreamDistribution.Id,
                            SharedDate = itemStreamDistribution.CreatedDate,
                            SharedZone = metaData.ZoneId.ToString() + (!string.IsNullOrEmpty(metaData.ZoneRelation) ? ("," + metaData.ZoneRelation) : "")
                        };

                        var media = await GalleryRepository.GetDetailByIdAsync(metaData.ObjectId);
                        if (media != null)
                        {
                            metaData.DistributionDate = metaData.DistributionDate.HasValue ? metaData.DistributionDate : media.CreatedDate;

                            if (media.DistributionDate == null) media.DistributionDate = DateTime.Now;

                            if (media.NewsDistribution == null)
                                media.NewsDistribution = new[] { newsDistribution };
                            else
                            {
                                if (media.NewsDistribution.Count(w => w.DistributorId == newsDistribution.DistributorId && w.ItemStreamId != itemStreamDistribution.Id) == 0)
                                    media.NewsDistribution = media.NewsDistribution.Append(newsDistribution).ToArray();
                            }
                            listMedia.Add(media);
                        }
                    }
                }
                if (listMedia.Count > 0)
                {
                    returnValue = await StoreFactory.Create<DistributionCacheStore>().ShareGalleryAsync(itemStreamDistribution, listMedia);
                    if (returnValue)
                    {
                        //push queue trước khi đẩy sang kênh
                        await Function.AddToQueue(ActionName.ShareGallery, TopicName.SHARE, new QueueData<string, PayloadDataShare, string, Media.Action>
                        {
                            Data1 = Guid.NewGuid().ToString(),
                            Data2 = playloadDataShare,
                            Data3 = sessionId,
                            Data4 = action
                        });

                        await Function.AddToQueue(ActionName.ShareGalleryES, TopicName.ES_GALLERY, new QueueShareDataUnit<GallerySearch> { ItemStreamDistribution = itemStreamDistribution, ListMedia = listMedia });

                        //push queue
                        await Function.AddToQueue(ActionName.ShareGallerySQL, TopicName.SQL_GALLERY, new QueueShareDataUnit<GallerySearch> { ItemStreamDistribution = itemStreamDistribution, ListMedia = listMedia });
                    }

                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
            }
            return returnValue;
        }
        public static async Task<bool> PushGalleryToChannel(string sessionId, PayloadDataShare playloadDataShare, Distributor distribution, string clientIP)
        {
            var lstObj = new List<object>();
            var returnValue = false;
            try
            {
                if (distribution != null)
                {
                    var configMediaObj = Json.Parse<Dictionary<string, string>>(distribution?.Config.ApiMediaUnit);
                    if (configMediaObj == null) return false;

                    var configApiUrl = configMediaObj["ApiUrl"];
                    var configApiKey = configMediaObj["ApiKey"];
                    if (configApiUrl != null && !string.IsNullOrEmpty(configApiUrl) && configApiKey != null && !string.IsNullOrEmpty(configApiKey))
                    {
                        var listMediaKingHub = new List<FormUrlEncodedContent>();
                        if (playloadDataShare != null && playloadDataShare.MetaDataShared?.Count > 0)
                        {
                            foreach (var media in playloadDataShare.MetaDataShared)
                            {
                                var is_comment = (int)AppSettings.Current.ChannelConfiguration.ApiCommentSetting.Mode.PostCheck;
                                if (media.CommentMode == CommentMode.Disable)
                                {
                                    is_comment = (int)AppSettings.Current.ChannelConfiguration.ApiCommentSetting.Mode.Disable;
                                }
                                else
                                {
                                    if (media.CommentMode == CommentMode.PreCheck) is_comment = (int)AppSettings.Current.ChannelConfiguration.ApiCommentSetting.Mode.PreCheck;
                                }

                                var obj = new
                                {
                                    mediaid = media.ObjectId.ToString(),
                                    title = media.Title ?? string.Empty,
                                    obj_name = media.Name ?? string.Empty,
                                    cardtype = media.CardType.ToString(),
                                    desc = Json.Stringify(media.ItemMediaUnit?.Select(s => new {
                                        id = s.Id ?? string.Empty,
                                        title = s.Title ?? string.Empty,
                                        link = s.Link ?? string.Empty,
                                        thumb = string.IsNullOrEmpty(s.Thumb) ? s.Link : s.Thumb,
                                        width = s.Width,
                                        height = s.Height,
                                        content_type = s.ContentType == ContentType.Image ? (int)s.ContentType : (int)ContentType.Photo,
                                        created_at = (s.CreatedDate ?? DateTime.Now).ToString("yyyy-MM-ddTHH:mm:ssZ")
                                    }).ToList()),
                                    channelid = media.ChannelId ?? string.Empty,
                                    frameid = media.Frameid == 0 ? "1" : media.Frameid.ToString(),
                                    tags = media.Tags ?? string.Empty,
                                    userid = media.UserId.ToString(),
                                    username = media.UserName ?? string.Empty,
                                    fullname = media.FullName ?? string.Empty,
                                    avatar = media.AuthorAvatar ?? string.Empty,
                                    catid = media.ZoneId?.ToString() ?? string.Empty,
                                    isdeleted = media.IsDeleted.ToString(),
                                    createtime = (media.DistributionDate ?? DateTime.Now).ToString("yyyy-MM-ddTHH:mm:ssZ"),
                                    catname = media.ZoneName ?? string.Empty,
                                    boardids = media.BoardIds != null && media.BoardIds.Length > 0 ? string.Join(",", media.BoardIds) : string.Empty,
                                    ext = Json.StringifyFirstLowercase(new
                                    {
                                        status = media.CaptionExt,
                                        flag = new
                                        {
                                            is_comment = is_comment
                                        },
                                        gallery = new {
                                            cover = new {
                                                link = media?.Cover?.Src??string.Empty,
                                                width = media?.Cover?.Width??0,
                                                height = media?.Cover?.Height??0,
                                                thumb = media?.Cover?.Thumb??string.Empty
                                            },
                                            description = media.Description
                                        },
                                        is_web = 1
                                    }),
                                };

                                //Logger.Debug("Data push Gallery KeyValuePair =>" + Json.Stringify(obj.ConvertToKeyValuePair()));

                                lstObj.Add(obj);
                                listMediaKingHub.Add(new FormUrlEncodedContent(obj.ConvertToKeyValuePair()));
                            }
                        }
                        else
                        {
                            Logger.Error("Push Gallery: Gallery không tồn tại.");
                            return false;
                        }

                        using (var client = new HttpClient())
                        {
                            client.Timeout = TimeSpan.FromSeconds(AppSettings.Current.ChannelConfiguration.HttpClientTimeOut);
                            foreach (var value in listMediaKingHub)
                            {
                                try
                                {
                                    var response = await client.PostAsync(configApiUrl, value);
                                    if (!response.IsSuccessStatusCode)
                                    {
                                        //Logger.Debug("PushAlbumToChannel 1.=> response: " + Json.Stringify(response));
                                        response.EnsureSuccessStatusCode();
                                    }
                                    var result = await response.Content.ReadAsStringAsync();
                                    if (!string.IsNullOrEmpty(result))
                                    {
                                        returnValue = Json.Parse<dynamic>(result).result;
                                        if (!returnValue)
                                        {
                                            Logger.Debug("Data push Gallery =>" + Json.Stringify(lstObj));
                                            //Logger.Debug("PushGalleryToChannel ERROR => configApiUrl:" + configApiUrl + ", data:" + string.Join(",", playloadDataShare.MetaDataShared?.Select(p => p.ObjectId.ToString()).ToArray() ?? new string[] { }) + ", result:" + result);
                                        }
                                    }
                                }
                                catch (Exception ex)
                                {
                                    Logger.Error(ex, "Push Gallery exception => " + Json.Stringify(new
                                    {
                                        data = lstObj,
                                        exception = ex.Message
                                    }));
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, "Push Gallery exception :" + ex.Message);
            }
            if (returnValue)
            {
                Logger.Debug("Data Gallery=>"+Json.Stringify(lstObj));
            }
            return returnValue;
        }
        #endregion

        #region Share News
        public static async Task<bool> ShareArticleAsync(ItemStreamDistribution itemStreamDistribution, Distributor distribution, PayloadDataShare playloadDataShare, string clientIP, string sessionId, Media.Action action)
        {
            var returnValue = false;
            var isUpdate = true;
            try
            {
                var listMedia = new List<ArticleSearch>();
                foreach (var metaData in playloadDataShare.MetaDataShared)
                {
                    if (metaData.ObjectId > 0)
                    {
                        var newsDistribution = new NewsDistribution
                        {
                            DistributorId = distribution.Id,
                            DistributorName = distribution.Name,
                            NewsId = metaData.ObjectId,
                            ItemStreamId = itemStreamDistribution.Id,
                            SharedDate = itemStreamDistribution.CreatedDate,
                            SharedZone = metaData.ZoneId.ToString() + (!string.IsNullOrEmpty(metaData.ZoneRelation) ? ("," + metaData.ZoneRelation) : "")
                        };

                        var media = await ArticleRepository.GetArticleSearchByIdAsync(metaData.ObjectId);
                        if (media != null)
                        {
                            if (media.DistributionDate == null) media.DistributionDate = DateTime.Now;
                            if (media.NewsDistribution == null)
                            {
                                isUpdate = false;
                                media.NewsDistribution = new[] { newsDistribution };
                            }
                            else
                            {
                                if (media.NewsDistribution.Count(w => w.DistributorId == newsDistribution.DistributorId && w.ItemStreamId == itemStreamDistribution.Id) == 0)
                                {
                                    isUpdate = false;
                                    media.NewsDistribution = media.NewsDistribution.Append(newsDistribution).ToArray();
                                }
                            }
                            listMedia.Add(media);
                        }
                    }
                }
                if (listMedia.Count > 0)
                {
                    returnValue = await StoreFactory.Create<DistributionCacheStore>().ShareArticleAsync(itemStreamDistribution, listMedia);
                    if (returnValue)
                    {
                        //push queue trước khi đẩy sang kênh
                        await Function.AddToQueue(ActionName.ShareArticle, TopicName.SHARE, new QueueData<string, PayloadDataShare, string, Media.Action>
                        {
                            Data1 = Guid.NewGuid().ToString(),
                            Data2 = playloadDataShare,
                            Data3 = sessionId,
                            Data4 = action
                        });

                        await Function.AddToQueue(ActionName.ShareArticleES, TopicName.ES_ARTICLE, new QueueData<ItemStreamDistribution, List<ArticleSearch>, bool, object>() { Data1 = itemStreamDistribution, Data2 = listMedia, Data3 = isUpdate });

                        var listNewsDistribution = new List<NewsDistribution>();
                        foreach (var media in listMedia)
                        {
                            var listTmp = media.NewsDistribution.Select(p => p).Where(p => p.ItemStreamId == itemStreamDistribution.Id && p.DistributorId == distribution.Id).ToList();
                            listTmp.ForEach(p => p.NewsId = media.Id);
                            listNewsDistribution.AddRange(listTmp);
                        }
                        //push queue
                        //chinhnb tạm bỏ    
                        await Function.AddToQueue(ActionName.ShareArticleSQL, TopicName.SQL_ARTICLE, new QueueData<ItemStreamDistribution, List<NewsDistribution>, bool, object> { Data1 = itemStreamDistribution, Data2 = listNewsDistribution, Data3 = isUpdate });
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
            }
            return returnValue;
        }
        public static async Task<bool> PushArticleToChannel(string sessionId, PayloadDataShare playloadDataShare, Distributor distribution, string clientIP)
        {
            var lstObj = new List<object>();
            var returnValue = false;
            try
            {
                if (distribution != null)
                {
                    var configMediaObj = Json.Parse<Dictionary<string, string>>(distribution?.Config.ApiMediaUnit);
                    if (configMediaObj == null) return false;

                    var configApiUrl = configMediaObj["ApiUrl"];
                    var configApiKey = configMediaObj["ApiKey"];
                    if (configApiUrl != null && !string.IsNullOrEmpty(configApiUrl) && configApiKey != null && !string.IsNullOrEmpty(configApiKey))
                    {
                        var listMediaKingHub = new List<FormUrlEncodedContent>();
                        if (playloadDataShare != null && playloadDataShare.MetaDataShared?.Count > 0)
                        {
                            foreach (var media in playloadDataShare.MetaDataShared)
                            {
                                var is_comment = (int)AppSettings.Current.ChannelConfiguration.ApiCommentSetting.Mode.PostCheck;
                                if (media.CommentMode == CommentMode.Disable)
                                {
                                    is_comment = (int)AppSettings.Current.ChannelConfiguration.ApiCommentSetting.Mode.Disable;
                                }
                                else
                                {
                                    if (media.CommentMode == CommentMode.PreCheck) is_comment = (int)AppSettings.Current.ChannelConfiguration.ApiCommentSetting.Mode.PreCheck;
                                }

                                var obj = new
                                {
                                    mediaid = media.ObjectId.ToString(),
                                    title = media.Title??string.Empty,
                                    obj_name = media.Name??string.Empty,
                                    cardtype = ((int)CardType.Article).ToString(),
                                    desc = Json.Stringify(media.ItemMediaUnit.Select(s=> new {
                                        id = s.Id??string.Empty,
                                        title = s.Title??string.Empty,
                                        link = s.Link??string.Empty,
                                        image = new { thumb = s.Image?.Thumb??string.Empty, width = s.Image?.Width == 0 || s.Image?.Width == null ? 600 : s.Image?.Width, height = s.Image?.Height == 0 || s.Image?.Height == null ? 300 : s.Image?.Height, content_type = s.ContentType == ContentType.Image ? (int)s.ContentType : (int)ContentType.Photo },
                                        source = s.Source??string.Empty,
                                        content_type = (int)ContentType.Linkshare,
                                        data_type = s.DataType != null ? (int)s.DataType : (int)DataType.InstantView,
                                        created_at = (s.CreatedDate??DateTime.Now).ToString("yyyy-MM-ddTHH:mm:ssZ")
                                    }).ToList()),
                                    frameid = media.Frameid.ToString(),
                                    tags = media.Tags??string.Empty,
                                    userid = media.UserId.ToString(),
                                    username = media.UserName??string.Empty,
                                    fullname = media.FullName??string.Empty,
                                    avatar = media.AuthorAvatar??string.Empty,
                                    catid = media.ZoneId?.ToString()??string.Empty,
                                    isdeleted = media.IsDeleted.ToString(),
                                    createtime = (media.DistributionDate??DateTime.Now).ToString("yyyy-MM-ddTHH:mm:ssZ"),
                                    catname = media.ZoneName??string.Empty,
                                    boardids = media.BoardIds != null && media.BoardIds.Length > 0 ? string.Join(",", media.BoardIds) : string.Empty,
                                    ext = Json.StringifyFirstLowercase(new {
                                        status = media.CaptionExt,
                                        flag = new
                                        {
                                            is_comment = is_comment
                                        }
                                    }),
                                };

                                lstObj.Add(obj);
                                listMediaKingHub.Add(new FormUrlEncodedContent(obj.ConvertToKeyValuePair()));
                            }
                        }
                        else
                        {
                            Logger.Error("Push Article: Article không tồn tại.");
                            return false;
                        }
                        foreach (var value in listMediaKingHub)
                        {
                            using (var client = new HttpClient())
                            {
                                try
                                {
                                    client.Timeout = TimeSpan.FromSeconds(AppSettings.Current.ChannelConfiguration.HttpClientTimeOut);

                                    var response = await client.PostAsync(configApiUrl, value);
                                    if (!response.IsSuccessStatusCode)
                                    {
                                        //Logger.Debug("PushArticleToChannel 1.=> response: " + Json.Stringify(response));
                                        response.EnsureSuccessStatusCode();
                                    }
                                    var result = await response.Content.ReadAsStringAsync();
                                    if (!string.IsNullOrEmpty(result))
                                    {
                                        returnValue = Json.Parse<dynamic>(result).result;
                                        if (!returnValue)
                                        {
                                            Logger.Sensitive("Data push Article not success", lstObj);
                                        }
                                    }
                                }
                                catch (Exception ex)
                                {
                                    Logger.Sensitive(ex, "Data push Article =>" + Json.Stringify(new
                                    {
                                        data = lstObj,
                                        exception = ex.Message
                                    }));
                                }
                            }
                        }
                    }
                }
                if (returnValue)
                {
                    if (playloadDataShare != null && playloadDataShare.MetaDataShared?.Count > 0)
                    {
                        foreach (var media in playloadDataShare.MetaDataShared)
                        {
                            //insert log
                            await Function.AddToQueue(ActionName.InsertLog, TopicName.LOGACTION, new LogActionEntity
                            {
                                ObjectId = media.ObjectId.ToString(),
                                Account = media.UserId.ToString(),
                                CreatedDate = DateTime.SpecifyKind(DateTime.Now, DateTimeKind.Unspecified),
                                Ip = clientIP,
                                Message = "Success.",
                                Title = media.Title,
                                Status = returnValue ? 1 : 0,
                                SourceId = media.UserId.ToString(),
                                ActionTypeDetail = (int)ActionType.PushToApp,
                                ActionText = ActionType.PushToApp.ToString(),
                                Type = (int)LogContentType.Article
                            });
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Sensitive(ex, "Push Article exception :" + ex.Message);
            }
            return returnValue;
        }
        #endregion

        #region Share Photo
        public static async Task<bool> SharePhotoAsync(ItemStreamDistribution itemStreamDistribution, Distributor distribution, PayloadDataShare playloadDataShare, string clientIP, string sessionId, Media.Action action)
        {
            var returnValue = false;
            try
            {
                var listMedia = new List<PhotoSearch>();
                foreach (var metaData in playloadDataShare.MetaDataShared)
                {
                    if (metaData.ObjectId > 0)
                    {
                        var newsDistribution = new NewsDistribution
                        {
                            DistributorId = distribution.Id,
                            DistributorName = distribution.Name,
                            NewsId = metaData.ObjectId,
                            ItemStreamId = itemStreamDistribution.Id,
                            SharedDate = itemStreamDistribution.CreatedDate,
                            SharedZone = metaData.ZoneId.ToString() + (!string.IsNullOrEmpty(metaData.ZoneRelation) ? ("," + metaData.ZoneRelation) : "")
                        };

                        var media = await PhotoRepository.GetDetailByIdAsync(metaData.ObjectId);
                        if (media != null)
                        {
                            metaData.DistributionDate = metaData.DistributionDate.HasValue ? metaData.DistributionDate : media.CreatedDate;

                            if (media.DistributionDate == null) media.DistributionDate = DateTime.Now;

                            if (media.NewsDistribution == null)
                                media.NewsDistribution = new[] { newsDistribution };
                            else
                            {
                                if (media.NewsDistribution.Count(w => w.DistributorId == newsDistribution.DistributorId && w.ItemStreamId == itemStreamDistribution.Id) == 0)
                                    media.NewsDistribution = media.NewsDistribution.Append(newsDistribution).ToArray();
                            }
                            listMedia.Add(media);
                        }
                    }
                }
                if (listMedia.Count > 0)
                {
                    returnValue = await StoreFactory.Create<DistributionCacheStore>().SharePhotoAsync(itemStreamDistribution, listMedia);
                    if (returnValue)
                    {
                        //push queue trước khi đẩy sang kênh
                        await Function.AddToQueue(ActionName.SharePhoto, TopicName.SHARE, new QueueData<string, PayloadDataShare, string, Media.Action>
                        {
                            Data1 = Guid.NewGuid().ToString(),
                            Data2 = playloadDataShare,
                            Data3 = sessionId,
                            Data4 = action
                        });

                        await Function.AddToQueue(ActionName.SharePhotoES, TopicName.ES_PHOTO, new QueueShareDataUnit<PhotoSearch> { ItemStreamDistribution = itemStreamDistribution, ListMedia = listMedia });
                        //push queue
                        await Function.AddToQueue(ActionName.SharePhotoSQL, TopicName.SQL_PHOTO, new QueueShareDataUnit<PhotoSearch> { ItemStreamDistribution = itemStreamDistribution, ListMedia = listMedia });
                    }

                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
            }
            return returnValue;
        }
        public static async Task<bool> PushPhotoToChannel(string sessionId, PayloadDataShare playloadDataShare, Distributor distribution, string clientIP)
        {
            var lstObj = new List<object>();
            var returnValue = false;
            try
            {
                if (distribution != null)
                {
                    var configMediaObj = Json.Parse<Dictionary<string, string>>(distribution?.Config.ApiMediaUnit);
                    if (configMediaObj == null) return false;

                    var configApiUrl = configMediaObj["ApiUrl"];
                    var configApiKey = configMediaObj["ApiKey"];
                    if (configApiUrl != null && !string.IsNullOrEmpty(configApiUrl) && configApiKey != null && !string.IsNullOrEmpty(configApiKey))
                    {
                        var listMediaKingHub = new List<FormUrlEncodedContent>();
                        if (playloadDataShare != null && playloadDataShare.MetaDataShared?.Count > 0)
                        {
                            foreach (var media in playloadDataShare.MetaDataShared)
                            {
                                var is_comment = (int)AppSettings.Current.ChannelConfiguration.ApiCommentSetting.Mode.PostCheck;
                                if (media.CommentMode == CommentMode.Disable)
                                {
                                    is_comment = (int)AppSettings.Current.ChannelConfiguration.ApiCommentSetting.Mode.Disable;
                                }
                                else
                                {
                                    if (media.CommentMode == CommentMode.PreCheck) is_comment = (int)AppSettings.Current.ChannelConfiguration.ApiCommentSetting.Mode.PreCheck;
                                }

                                var obj = new
                                {
                                    mediaid = media.ObjectId.ToString(),
                                    title = media.Title ?? string.Empty,
                                    obj_name = media.Name ?? string.Empty,
                                    cardtype = ((int)CardType.Photo).ToString(),
                                    desc = Json.Stringify(media.ItemMediaUnit.Select(s => new {
                                        id = s.Id ?? string.Empty,
                                        title = s.Title ?? string.Empty,
                                        link = (string.IsNullOrEmpty(s.Link) ? s.Thumb : s.Link) ?? string.Empty,
                                        thumb = s.Thumb ?? string.Empty,
                                        width = s.Width,
                                        height = s.Height,
                                        content_type = s.ContentType != null && (s.ContentType == ContentType.Image || s.ContentType == ContentType.Photo) ? (int)s.ContentType : (int)ContentType.Photo,
                                        is_heart_drop = s.IsHeartDrop,
                                        created_at = (s.CreatedDate ?? DateTime.Now).ToString("yyyy-MM-ddTHH:mm:ssZ")

                                    }).ToList()),
                                    channelid = media.ChannelId ?? string.Empty,
                                    frameid = media.Frameid.ToString(),
                                    tags = media.Tags ?? string.Empty,
                                    userid = media.UserId.ToString(),
                                    username = media.UserName ?? string.Empty,
                                    fullname = media.FullName ?? string.Empty,
                                    avatar = media.AuthorAvatar ?? string.Empty,
                                    catid = media.ZoneId?.ToString() ?? string.Empty,
                                    isdeleted = media.IsDeleted.ToString(),
                                    createtime = (media.DistributionDate ?? DateTime.Now).ToString("yyyy-MM-ddTHH:mm:ssZ"),
                                    catname = media.ZoneName ?? string.Empty,
                                    boardids = media.BoardIds != null && media.BoardIds.Length > 0 ? string.Join(",", media.BoardIds) : string.Empty,
                                    ext = Json.StringifyFirstLowercase(new {
                                        status = media.CaptionExt,
                                        flag = new
                                        {
                                            is_comment = is_comment
                                        },
                                        label = new
                                        {
                                            id = ((int?)media.LabelType)?.ToString(),
                                            text = media.LabelType != null ? Enum.GetName(typeof(LabelType), media.LabelType)?.ToLower() : ""
                                        }
                                    })
                                };

                                lstObj.Add(obj);
                                listMediaKingHub.Add(new FormUrlEncodedContent(obj.ConvertToKeyValuePair()));
                            }
                        }
                        else
                        {
                            Logger.Error("Push Photo: Photo không tồn tại.");
                            return false;
                        }
                        foreach (var value in listMediaKingHub)
                        {
                            using (var client = new HttpClient())
                            {
                                try
                                {
                                    client.Timeout = TimeSpan.FromSeconds(AppSettings.Current.ChannelConfiguration.HttpClientTimeOut);
                                    var response = await client.PostAsync(configApiUrl, value);
                                    if (!response.IsSuccessStatusCode)
                                    {
                                        response.EnsureSuccessStatusCode();
                                    }
                                    var result = await response.Content.ReadAsStringAsync();
                                    if (!string.IsNullOrEmpty(result))
                                    {
                                        returnValue = Json.Parse<dynamic>(result).result;
                                        if (!returnValue)
                                        {
                                            Logger.Sensitive("Data push Photo not success.", lstObj);
                                        }
                                    }
                                }
                                catch (Exception ex)
                                {
                                    Logger.Sensitive(ex, "Data push Photo =>" + Json.Stringify(new
                                    {
                                        data = lstObj,
                                        exception = ex.Message
                                    }));
                                }
                            }
                        }
                    }
                }
                if (returnValue)
                {
                    if (playloadDataShare != null && playloadDataShare.MetaDataShared?.Count > 0)
                    {
                        foreach (var media in playloadDataShare.MetaDataShared)
                        {
                            //insert log
                            await Function.AddToQueue(ActionName.InsertLog, TopicName.LOGACTION, new LogActionEntity
                            {
                                ObjectId = media.ObjectId.ToString(),
                                Account = media.UserId.ToString(),
                                CreatedDate = DateTime.SpecifyKind(DateTime.Now, DateTimeKind.Unspecified),
                                Ip = clientIP,
                                Message = "Success.",
                                Title = media.Title,
                                Status = returnValue ? 1 : 0,
                                SourceId = media.UserId.ToString(),
                                ActionTypeDetail = (int)ActionType.PushToApp,
                                ActionText = ActionType.PushToApp.ToString(),
                                Type = (int)LogContentType.Photo
                            });
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Sensitive(ex, "Push Photo exception :" + ex.Message);
            }
            return returnValue;
        }
        #endregion

        #region Share Beam
        public static async Task<bool> ShareBeamAsync(ItemStreamDistribution itemStreamDistribution, Distributor distribution, PayloadDataShare payloadDataShare, BeamSearch data, string clientIP, string sessionId, Media.Action action)
        {
            var returnValue = false;
            bool isUpdate = true;
            try
            {
                var listBeam = new List<BeamSearch>();
                foreach (var metaData in payloadDataShare.MetaDataShared)
                {
                    if (metaData.ObjectId > 0)
                    {
                        var newsDistribution = new NewsDistribution
                        {
                            DistributorId = distribution.Id,
                            DistributorName = distribution.Name,
                            NewsId = metaData.ObjectId,
                            ItemStreamId = itemStreamDistribution.Id,
                            SharedDate = itemStreamDistribution.CreatedDate,
                            SharedZone = metaData.ZoneId.ToString() + (!string.IsNullOrEmpty(metaData.ZoneRelation) ? ("," + metaData.ZoneRelation) : "")
                        };

                        var beam = await BeamRepository.GetBeamSearchByIdAsync(metaData.ObjectId);
                        if (beam != null)
                        {
                            if (beam.DistributionDate == null) beam.DistributionDate = DateTime.Now;
                            if (beam.NewsDistribution == null)
                            {
                                isUpdate = false;
                                beam.NewsDistribution = new[] { newsDistribution };
                            }
                            else
                            {
                                if (beam.NewsDistribution.Count(w => w.DistributorId == newsDistribution.DistributorId && w.ItemStreamId == itemStreamDistribution.Id) == 0)
                                {
                                    isUpdate = false;
                                    beam.NewsDistribution = beam.NewsDistribution.Append(newsDistribution).ToArray();
                                }
                            }
                            listBeam.Add(beam);
                        }
                        else
                        {
                            return false;
                        }
                    }
                }
                if (listBeam.Count > 0)
                {
                    returnValue = await StoreFactory.Create<DistributionCacheStore>().ShareBeamAsync(itemStreamDistribution, listBeam, isUpdate);
                    if (returnValue)
                    {
                        //push to queue share video
                        await Function.AddToQueue(ActionName.ShareBeam, TopicName.SHARE, new QueueData<string, PayloadDataShare, string, Media.Action>
                        {
                            Data1 = Guid.NewGuid().ToString(),
                            Data2 = payloadDataShare,
                            Data3 = sessionId,
                            Data4 = action
                        });

                        await Function.AddToQueue(ActionName.ShareBeamES, TopicName.ES_BEAM, new QueueData<ItemStreamDistribution, List<BeamSearch>, bool, object> { Data1 = itemStreamDistribution, Data2 = listBeam, Data3 = isUpdate });

                        var listNewsDistribution = new List<NewsDistribution>();
                        foreach (var media in listBeam)
                        {
                            var listTmp = media.NewsDistribution.Where(p => p.ItemStreamId == itemStreamDistribution.Id && p.ItemStreamId == itemStreamDistribution.Id).ToList();
                            listTmp.ForEach(p => p.NewsId = media.Id);
                            listNewsDistribution.AddRange(listTmp);
                        }
                        //push queue
                        await Function.AddToQueue(ActionName.ShareBeamSQL, TopicName.SQL_BEAM, new QueueData<ItemStreamDistribution, List<NewsDistribution>, bool, object> { Data1 = itemStreamDistribution, Data2 = listNewsDistribution, Data3 = isUpdate });
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
            }
            return returnValue;
        }
        public static async Task<bool> PushBeamToChannel(string sessionId, PayloadDataShare playloadDataShare, Distributor distribution, string clientIP)
        {
            var lstObj = new List<object>();
            var returnValue = false;
            try
            {
                if (distribution != null)
                {
                    var configMediaObj = Json.Parse<Dictionary<string, string>>(distribution?.Config.ApiMediaUnit);
                    if (configMediaObj == null) return false;

                    var configApiUrl = configMediaObj["ApiUrl"];
                    var configApiKey = configMediaObj["ApiKey"];
                    if (configApiUrl != null && !string.IsNullOrEmpty(configApiUrl) && configApiKey != null && !string.IsNullOrEmpty(configApiKey))
                    {
                        var listMediaKingHub = new List<FormUrlEncodedContent>();
                        if (playloadDataShare != null && playloadDataShare.MetaDataShared?.Count > 0)
                        {
                            foreach (var media in playloadDataShare.MetaDataShared)
                            {
                                var is_comment = (int)AppSettings.Current.ChannelConfiguration.ApiCommentSetting.Mode.PostCheck;
                                if (media.CommentMode == CommentMode.Disable)
                                {
                                    is_comment = (int)AppSettings.Current.ChannelConfiguration.ApiCommentSetting.Mode.Disable;
                                }
                                else
                                {
                                    if (media.CommentMode == CommentMode.PreCheck) is_comment = (int)AppSettings.Current.ChannelConfiguration.ApiCommentSetting.Mode.PreCheck;
                                }

                                var obj = new
                                {
                                    mediaid = media.ObjectId.ToString(),
                                    title = media.Title??string.Empty,
                                    obj_name = media.Name??string.Empty,
                                    cardtype = ((int)CardType.ListNews).ToString(),
                                    desc = Json.Stringify(media.ItemMediaUnit.Select(s=> new {
                                        id = s.Id??string.Empty,
                                        title = s.Title??string.Empty,
                                        data_type = s.DataType!=null ? (int)s.DataType :(int) DataType.InstantView,
                                        image = new { thumb = s.Image?.Thumb??string.Empty, width = s.Image?.Width == 0 || s.Image?.Width == null ? 600 : s.Image?.Width,height = s.Image?.Height == 0 || s.Image?.Height == null ? 300 : s.Image?.Height, content_type = (s.ContentType ==ContentType.Photo || s.ContentType ==ContentType.Image) ? (int)s.ContentType : (int)ContentType.Photo},
                                        link = s.Link??string.Empty,
                                        content_type = s.ContentType == ContentType.News ? (int)s.ContentType: (int)ContentType.Linkshare,
                                        created_at = (s.CreatedDate??DateTime.Now).ToString("yyyy-MM-ddTHH:mm:ssZ")
                                    }).ToList()),
                                    channelid = media.ChannelId??string.Empty,
                                    frameid = media.Frameid.ToString(),
                                    tags = media.Tags??string.Empty,
                                    userid = media.UserId.ToString(),
                                    username = media.UserName??string.Empty,
                                    fullname = media.FullName??string.Empty,
                                    avatar = media.AuthorAvatar??string.Empty,
                                    catid = media.ZoneId?.ToString()??string.Empty,
                                    isdeleted = media.IsDeleted.ToString(),
                                    createtime = (media.DistributionDate??DateTime.Now).ToString("yyyy-MM-ddTHH:mm:ssZ"),
                                    catname = media.ZoneName??string.Empty,
                                    boardids = media.BoardIds != null && media.BoardIds.Length > 0 ? string.Join(",", media.BoardIds) : string.Empty,
                                    ext = Json.StringifyFirstLowercase(new {
                                        status = media.CaptionExt,
                                        flag = new
                                        {
                                            is_comment = is_comment
                                        }
                                    }),
                                };

                                Logger.Debug("Item beam =>" + Json.Stringify(media.ItemMediaUnit));
                                Logger.Debug("Object beam =>" + Json.Stringify(obj));
                                lstObj.Add(obj);
                                listMediaKingHub.Add(new FormUrlEncodedContent(obj.ConvertToKeyValuePair()));
                            }
                        }
                        else
                        {
                            Logger.Error("Push Beam: không có Beam nào.");
                            return false;
                        }

                        foreach (var value in listMediaKingHub)
                        {
                            using (var client = new HttpClient())
                            {
                                try
                                {
                                    client.Timeout = TimeSpan.FromSeconds(AppSettings.Current.ChannelConfiguration.HttpClientTimeOut);
                                    var response = await client.PostAsync(configApiUrl, value);
                                    Logger.Debug("FormUrlEndCode value beam =>" + await value.ReadAsStringAsync());
                                    if (!response.IsSuccessStatusCode)
                                    {
                                        response.EnsureSuccessStatusCode();
                                    }
                                    var result = await response.Content.ReadAsStringAsync();
                                    Logger.Debug("Response push beam =>" + await value.ReadAsStringAsync());
                                    if (!string.IsNullOrEmpty(result))
                                    {
                                        returnValue = Json.Parse<dynamic>(result).result;
                                        if (!returnValue)
                                        {
                                            Logger.Sensitive("Data push Beam not success.", lstObj);
                                        }
                                    }
                                }
                                catch (Exception ex)
                                {
                                    Logger.Debug("Response push beam exception =>" + ex.Message);
                                    Logger.Sensitive(ex, "Data push Beam =>" + Json.Stringify(new
                                    {
                                        data = lstObj,
                                        exception = ex.Message
                                    }));
                                }
                            }
                        }
                    }
                }
                if (returnValue)
                {
                    if (playloadDataShare != null && playloadDataShare.MetaDataShared?.Count > 0)
                    {
                        foreach (var media in playloadDataShare.MetaDataShared)
                        {
                            //insert log
                            await Function.AddToQueue(ActionName.InsertLog, TopicName.LOGACTION, new LogActionEntity
                            {
                                ObjectId = media.ObjectId.ToString(),
                                Account = media.UserId.ToString(),
                                CreatedDate = DateTime.SpecifyKind(DateTime.Now, DateTimeKind.Unspecified),
                                Ip = clientIP,
                                Message = "Success.",
                                Title = media.Title,
                                Status = returnValue ? 1 : 0,
                                SourceId = media.UserId.ToString(),
                                ActionTypeDetail = (int)ActionType.PushToApp,
                                ActionText = ActionType.PushToApp.ToString(),
                                Type = (int)LogContentType.Beam
                            });
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Sensitive(ex, "Push Beam exception :" + ex.Message);
            }
            return returnValue;
        }
        #endregion
    }
}
