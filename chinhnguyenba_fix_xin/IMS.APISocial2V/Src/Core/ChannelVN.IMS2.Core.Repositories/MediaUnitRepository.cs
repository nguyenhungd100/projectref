﻿using ChannelVN.IMS2.Core.Entities;
using ChannelVN.IMS2.Core.Entities.Distribution;
using ChannelVN.IMS2.Core.Entities.Media;
using ChannelVN.IMS2.Core.Entities.Security;
using ChannelVN.IMS2.Core.Models.CacheStore.Media;
using ChannelVN.IMS2.Core.Models.DataStore.Media;
using ChannelVN.IMS2.Core.Models.ExternalAPI;
using ChannelVN.IMS2.Core.Models.SearchStore.Media;
using ChannelVN.IMS2.Core.Repositories.Constants;
using ChannelVN.IMS2.Core.Repositories.Functions;
using ChannelVN.IMS2.Foundation.Common;
using ChannelVN.IMS2.Foundation.Common.Configuration;
using ChannelVN.IMS2.Foundation.Data;
using ChannelVN.IMS2.Foundation.Data.Factories;
using ChannelVN.IMS2.Foundation.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using static ChannelVN.IMS2.Core.Entities.ErrorCode.ErrorMapping;

namespace ChannelVN.IMS2.Core.Repositories
{
    public class MediaUnitRepository : Repository
    {
        public static async Task<ErrorCodes> AddAsync(MediaUnitSearch data, string clientIP)
        {
            try
            {
                var returnValue = await StoreFactory.Create<MediaUnitCacheStore>().AddAsync(data);
                if (returnValue == ErrorCodes.Success)
                {
                    var res = await StoreFactory.Create<MediaUnitSearchStore>().AddAsync(data);
                    if (!res)
                    {
                        //push queue
                        await Function.AddToQueue(ActionName.AddMediaUnitES, TopicName.ES_MEDIA_UNIT, data.Id);
                    }

                    res = await StoreFactory.Create<MediaUnitDataStore>().AddAsync(data);
                    if (!res)
                    {
                        //push queue
                        await Function.AddToQueue(ActionName.AddMediaUnitSQL, TopicName.SQL_MEDIA_UNIT, data.Id);
                    }

                    //auto phan phoi khi action publish                    
                    //if (data.Status == (int)NewsStatus.Archive && AppSettings.Current.Distribution.Automation.Enabled)
                    //{
                    //    await Function.AddToQueue(ActionName.AutoShareMediaUnit, TopicName.AUTOSHAREMEDIAUNIT, new QueueData<long, string, long[], object>
                    //    {
                    //        Data1 = data.Id,
                    //        Data2 = clientIP
                    //    });
                    //    //await AutoShare(data, clientIP);
                    //}
                }

                return returnValue;
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
                return ErrorCodes.Exception;
            }
        }

        public static async Task<ErrorCodes> UpdateAsync(MediaUnit data, long officerId, string clientIP, string sessionId)
        {
            try
            {
                var returnValue = await StoreFactory.Create<MediaUnitCacheStore>().UpdateV2Async(data, officerId, new Action<int>(async (oldStatus) => {
                    //auto phan phoi khi action publish   
                    if (AppSettings.Current.Distribution.Automation.Enabled)
                    {
                        if (oldStatus == (int)NewsStatus.Published)
                        {
                            var action = Media.Action.update;
                            await Function.AddToQueue(ActionName.AutoShareMediaUnit, TopicName.AUTOSHAREMEDIAUNIT, new QueueData<long, string, string, Media.Action>
                            {
                                Data1 = data.Id,
                                Data2 = clientIP,
                                Data3 = sessionId,
                                Data4 = action
                            });

                            await Function.AddToQueue(ActionName.PushChangeNotify, TopicName.PUSHBOARDTOCHANNEL, data.Id);
                        }
                    }
                }));
                if (returnValue == ErrorCodes.Success)
                {
                    await Function.AddToQueue(ActionName.UpdateMediaUnitES, TopicName.ES_MEDIA_UNIT, data.Id);
                    await Function.AddToQueue(ActionName.UpdateMediaUnitSQL, TopicName.SQL_MEDIA_UNIT, data.Id);                    
                }
                return returnValue;
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
                return ErrorCodes.Exception;
            }
        }

        public static async Task<bool> AutoShare(MediaUnitSearch data, string clientIP, string sessionId, Media.Action action)
        {
            var returnValue = false;
            try
            {
                var listItem = Json.Parse<PublishData>(data.PublishData);
                var itemMediaUnit = new List<ItemMediaUnit>();
                if (listItem != null && listItem.Items?.Count > 0)
                {
                    foreach (var item in listItem.Items)
                    {
                        itemMediaUnit.Add(new ItemMediaUnit
                        {
                            Id = item.Id,
                            Title = item.Title,
                            ContentType = item.ContentType ?? item.Content_Type,
                            Duration = item.Duration,
                            Link = item.Link,
                            Thumb = item.Thumb,
                            Width = item.Width,
                            Height = item.Height,
                            TypeMedia = item.Type,
                            Position = item.Position,
                            IsPlay = item.IsPlay,
                            CreatedDate = data.DistributionDate ?? DateTime.Now
                        });
                    }
                }

                var pageAccount = await SecurityRepository.GetByIdAsync(long.Parse(data.NewsInAccount?.LastOrDefault()?.AccountId.ToString()));
                var payloadDataShare = new PayloadDataShare
                {
                    DistributionId = AppSettings.Current.Distribution.Automation.DistributorId,
                    TemplateId = 0,
                    Title = listItem.Title,
                    Type = NewsType.MediaUnit,
                    MetaDataShared = new List<MetaDataShared>
                    {
                        new MetaDataShared
                        {
                            ObjectId = data.Id,
                            Title = listItem.Caption??data.Caption,
                            ZoneId = 0,
                            DistributionDate = data.DistributionDate,
                            CardType = (int)CardType.VideoAndImage,
                            Frameid = data.TemplateId,
                            Tags = data.Tags,
                            UserName = pageAccount?.UserName,
                            UserId = pageAccount?.Id??0,
                            FullName = pageAccount?.FullName,
                            AuthorAvatar = pageAccount?.Avatar,
                            IsDeleted = data.Status == (int)NewsStatus.Remove || data.Status == (int)NewsStatus.Draft ? 1 : (data.Status == (int)NewsStatus.Archive ? 2 : 0),
                            ItemMediaUnit = itemMediaUnit,
                            BoardIds = null,
                            CaptionExt = listItem.CaptionExt,
                            CommentMode = data.CommentMode
                        }
                    }
                };

                var distribution = await DistributionRepository.GetByIdAsync(payloadDataShare.DistributionId);

                //check itemStream => update
                var itemStreamId = data.NewsDistribution?.Where(s => s.DistributorId == payloadDataShare.DistributionId).FirstOrDefault();
                var itemStreamDistribution = new ItemStreamDistribution()
                {
                    Id = itemStreamId?.ItemStreamId ?? Generator.ItemStreamDistributionId(),
                    PublishStatus = (int)DistributionPublishStatus.Accept,
                    CreatedDate = DateTime.Now,
                    ModifiedDate = DateTime.Now,
                    ItemStreamTempId = payloadDataShare.TemplateId,
                    Title = payloadDataShare.Title,
                    MetaData = Json.Stringify(payloadDataShare.MetaDataShared),
                    Type = payloadDataShare.Type
                };

                returnValue = await DistributionRepository.ShareMediaUnitAsync(itemStreamDistribution, distribution, payloadDataShare, clientIP, sessionId, action);
                if (returnValue)
                {
                    //insert log
                    await Function.AddToQueue(ActionName.InsertLog, TopicName.LOGACTION, new LogActionEntity
                    {
                        ObjectId = data.Id.ToString(),
                        Account = data.ModifiedBy,
                        CreatedDate = DateTime.SpecifyKind(DateTime.Now, DateTimeKind.Unspecified),
                        Ip = clientIP,
                        Message = "Success.",
                        Title = data.Caption,
                        SourceId = data.ModifiedBy,
                        ActionTypeDetail = (int)ActionType.Share,
                        ActionText = ActionType.Share.ToString(),
                        Type = (int)LogContentType.MediaUnit
                    });
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
            }
            return returnValue;
        }


        public static async Task<ErrorCodes> UpdateStatusV2Async(long id, NewsStatus status, long userId, string clientIP, string sessionId)
        {
            var returnCode = ErrorCodes.BusinessError;
            try
            {
                returnCode = await StoreFactory.Create<MediaUnitCacheStore>().UpdateStatusV2Async(id, status, userId, new Action<MediaUnitSearch, int>(async (data, oldStatus) =>
                {
                    //auto phan phoi khi action publish    
                    if (AppSettings.Current.Distribution.Automation.Enabled)
                    {
                        if ((status == NewsStatus.Published)
                         || (data.Status == (int)NewsStatus.Remove && oldStatus == (int)NewsStatus.Published)
                         || (data.Status == (int)NewsStatus.Draft && oldStatus == (int)NewsStatus.Published))
                        {
                            var action = Media.Action.add;
                            if (status == NewsStatus.Published && oldStatus == (int)NewsStatus.Published)
                            {
                                action = Media.Action.update;
                            }
                            if (status == NewsStatus.Remove)
                            {
                                action = Media.Action.deletebymedia;
                            }
                            await Function.AddToQueue(ActionName.AutoShareMediaUnit, TopicName.AUTOSHAREMEDIAUNIT, new QueueData<long, string, string, Media.Action>
                            {
                                Data1 = data.Id,
                                Data2 = clientIP,
                                Data3 = sessionId,
                                Data4 = action
                            });
                        }
                    }
                }));

                if (returnCode == ErrorCodes.Success)
                {
                    await Function.AddToQueue(ActionName.UpdateStatusMediaUnitES, TopicName.ES_MEDIA_UNIT, id);
                    await Function.AddToQueue(ActionName.UpdateStatusMediaUnitSQL, TopicName.SQL_MEDIA_UNIT, id);
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
                returnCode = ErrorCodes.Exception;
            }
            return returnCode;
        }

        public static Task<MediaUnit> GetByIdAsync(long id)
        {
            return StoreFactory.Create<MediaUnitCacheStore>().GetByIdAsync(id);
        }

        public static Task<MediaUnitSearch> GetDetailByIdAsync(long id)
        {
            return StoreFactory.Create<MediaUnitCacheStore>().GetMediaUnitFullInfoAsync(id);
        }

        public static Task<List<MediaUnit>> GetListByIdsAsync(List<string> ids)
        {
            return StoreFactory.Create<MediaUnitCacheStore>().GetListByIdsAsync(ids);
        }

        public static Task<List<MediaUnitSearch>> GetListMediaUnitSearchByIdsAsync(List<string> ids)
        {
            return StoreFactory.Create<MediaUnitCacheStore>().GetListMediaUnitSearchByIdsAsync(ids);
        }

        public static async Task<PagingDataResult<MediaUnitReturn>> SearchAsync(SearchNews search, long accountId)
        {
            try
            {
                var dataReturn = new PagingDataResult<MediaUnitReturn>();
                if (string.IsNullOrEmpty(search.Keyword) && search.OrderBy != SearchNewsOrderBy.DescDistributionDate
                    && search.OrderBy != SearchNewsOrderBy.AscDistributionDate && search.OrderBy != SearchNewsOrderBy.DescModifiedDate && search.OrderBy != SearchNewsOrderBy.AscModifiedDate)
                {
                    dataReturn = await StoreFactory.Create<MediaUnitCacheStore>().SearchAsync(search, accountId, new Action<string>(async (tempKey) =>
                    {
                        await Function.AddToQueue(ActionName.SetExpire, TopicName.DELETE_INDEX_TEMP, tempKey);
                    }));
                }
                else
                {
                    var dataSearch = await StoreFactory.Create<MediaUnitSearchStore>().SearchAsync(search, accountId);
                    if (dataSearch?.Data?.Count > 0)
                    {
                        dataSearch.Data = await GetListMediaUnitSearchByIdsAsync(dataSearch.Data.Select(p => p.Id.ToString()).ToList());
                        dataReturn.Total = dataSearch.Total;
                        dataReturn.Data = dataSearch.Data.Select(p => Mapper<MediaUnitReturn>.Map(p, new MediaUnitReturn())).ToList();
                    }
                }
                return dataReturn;
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
                return null;
            }
        }

        public static async Task<PagingDataResult<MediaUnit>> GetListApproveAsync(SearchNews data, long accountId)
        {
            try
            {
                var dataReturn = new PagingDataResult<MediaUnit>();
                if (string.IsNullOrEmpty(data.Keyword) && data.OrderBy != SearchNewsOrderBy.DescDistributionDate
                    && data.OrderBy != SearchNewsOrderBy.AscDistributionDate && data.OrderBy != SearchNewsOrderBy.DescModifiedDate && data.OrderBy != SearchNewsOrderBy.AscModifiedDate)
                {
                    dataReturn = await StoreFactory.Create<MediaUnitCacheStore>().GetListApproveAsync(data, accountId, new Action<string>(async (tempKey) =>
                    {
                        await Function.AddToQueue(ActionName.SetExpire, TopicName.DELETE_INDEX_TEMP, tempKey);
                    }));
                }
                else
                {
                    dataReturn = await StoreFactory.Create<MediaUnitCacheStore>().GetListApproveAsync(data, accountId, new Action<string>(async (tempKey) =>
                    {
                        await Function.AddToQueue(ActionName.SetExpire, TopicName.DELETE_INDEX_TEMP, tempKey);
                    }));
                }
                return dataReturn;
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
                return null;
            }
        }

        public static Task<PagingDataResult<MediaUnitSearch>> ListDistributionAsync(SearchNews search, long accountId)
        {
            return StoreFactory.Create<MediaUnitSearchStore>().ListDistributionAsync(search, accountId);
        }

        public static async Task<bool> PushToKingHub(string sessionId, PayloadDataShare playloadDataShare, Media.Action action)
        {
            try
            {
                if (playloadDataShare != null && playloadDataShare.MetaDataShared?.Count > 0)
                {
                    var media = playloadDataShare.MetaDataShared.FirstOrDefault();

                    var is_comment = AppSettings.Current.ChannelConfiguration.ApiCommentSetting.Mode.PostCheck;
                    if (media.CommentMode == CommentMode.Disable)
                    {
                        is_comment = AppSettings.Current.ChannelConfiguration.ApiCommentSetting.Mode.Disable;
                    }
                    else
                    {
                        if (media.CommentMode == CommentMode.PreCheck) is_comment = AppSettings.Current.ChannelConfiguration.ApiCommentSetting.Mode.PreCheck;
                    }
;

                    var post = new
                    {
                        posts = new List<object>{
                            new {
                                media_id = media.ObjectId.ToString(),
                                title = media.Title??string.Empty,
                                mediaunit_name = media.Name??string.Empty,
                                card_type = (int) CardType.VideoAndImage,//mediaunit
                                preview = new{ },
                                media_unit_desc = media.ItemMediaUnit?.Select(s=> new {
                                   id = s.Id??string.Empty,
                                    title = s.Title??string.Empty,
                                    link = s.Link??string.Empty,
                                    thumb = s.Thumb??string.Empty,
                                    type = s.TypeMedia??string.Empty,
                                    duration = s.Duration??string.Empty,
                                    width = s.Width,
                                    height = s.Height,
                                    content_type = s.ContentType!=null && (s.ContentType==ContentType.Image || s.ContentType==ContentType.Photo || s.ContentType==ContentType.Video) ? (int) s.ContentType : (int) ContentType.Photo,
                                    position = s.Position,
                                    is_play = s.IsPlay,
                                    created_at = (s.CreatedDate??DateTime.Now).ToString("yyyy-MM-ddTHH:mm:ssZ")
                                }).ToList(),
                                channel_id = media.ChannelId??string.Empty,
                                frame_id = media.Frameid,
                                tags = media.Tags??string.Empty,
                                user_id = media.UserId.ToString(),
                                user_name = media.UserName??string.Empty,
                                full_name = media.FullName??string.Empty,
                                avatar = media.AuthorAvatar??string.Empty,
                                cat_id = media.ZoneId?.ToString()??string.Empty,
                                create_time = Utility.ConvertToTimestamp(media.DistributionDate??DateTime.Now),
                                cat_name = media.ZoneName??string.Empty,
                                extension = new {
                                     status = media.CaptionExt??new Caption[]{},
                                    flag = new
                                    {
                                        is_comment
                                    },
                                    is_web = 1,
                                    boards=media.BoardIds != null && media.BoardIds.Length > 0 ? string.Join(",", media.BoardIds) : string.Empty
                                },
                            }
                        }
                    };

                    //hearder thay vì session-id , các anh truyền vào user-id của page hoặc user nhé
                    sessionId = media.UserId.ToString();

                    if (action == Media.Action.add)
                    {
                        var errorCodeReturn = await Media.Add(media.ObjectId.ToString(), sessionId, CardType.VideoAndImage, post);
                        if (errorCodeReturn == ErrorCodes.DataExist)
                        {
                            action = Media.Action.update;
                        }
                        else
                        {
                            return errorCodeReturn == ErrorCodes.Success ? true : false;
                        }
                    }

                    if (action == Media.Action.update)
                    {
                        return await Media.Update(media.ObjectId.ToString(), sessionId, CardType.VideoAndImage, post) == ErrorCodes.Success ? true : false;
                    }

                    if (action == Media.Action.deletebymedia)
                    {
                        return await Media.Delete(media.ObjectId.ToString(), sessionId, CardType.VideoAndImage) == ErrorCodes.Success ? true : false;
                    }
                    //action ko hỗ trợ
                    return false;
                }

                return false;
            }
            catch (Exception ex)
            {
                Logger.Error(ex, "PushBlogToKingHub: " + ex.Message);
                return false;
            }
        }
    }
}
