﻿using ChannelVN.IMS2.Core.Entities;
using ChannelVN.IMS2.Core.Entities.ErrorCode;
using ChannelVN.IMS2.Core.Models.CacheStore;
using ChannelVN.IMS2.Core.Models.DataStore;
using ChannelVN.IMS2.Core.Models.SearchStore;
using ChannelVN.IMS2.Core.Repositories.Constants;
using ChannelVN.IMS2.Core.Repositories.Functions;
using ChannelVN.IMS2.Foundation.Common;
using ChannelVN.IMS2.Foundation.Common.Configuration;
using ChannelVN.IMS2.Foundation.Data;
using ChannelVN.IMS2.Foundation.Data.Factories;
using ChannelVN.IMS2.Foundation.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using static ChannelVN.IMS2.Core.Entities.ErrorCode.ErrorMapping;

namespace ChannelVN.IMS2.Core.Repositories
{
    public class NewsCrawlerRepository : Repository
    {
        public static async Task<bool> AddAsync(List<NewsCrawler> data)
        {
            var returnValue = false;
            try
            {
                returnValue = await StoreFactory.Create<NewsCrawlerCacheStore>().AddAsync(data);
                if (returnValue)
                {
                    await Function.AddToQueue(ActionName.AddNewsCrawlerES, TopicName.ES_NEWSCRAWLER, data);
                    await Function.AddToQueue(ActionName.AddNewsCrawlerSQL, TopicName.SQL_NEWSCRAWLER, data);
                }
                return returnValue;
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
                return returnValue;
            }
        }

        public static async Task<PagingDataResult<NewsCrawler>> SearchAsync(string keyword, long officerId, int? status, int pageIndex, int pageSize, OrderBy? orderBy, DateTime fromDate, DateTime toDate)
        {
            try
            {
                return await StoreFactory.Create<NewsCrawlerSearchStore>().SearchAsync(keyword, officerId, status, pageIndex, pageSize, orderBy, fromDate, toDate);
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
                return null;
            }
        }

        public static async Task<bool> UpdateAsync(NewsCrawler data)
        {
            try
            {
                var returnValue = await StoreFactory.Create<NewsCrawlerCacheStore>().UpdateAsync(data);
                if (returnValue)
                {
                    await Function.AddToQueue(ActionName.UpdateNewsCrawlerES, TopicName.ES_NEWSCRAWLER, data);
                    await Function.AddToQueue(ActionName.UpdateNewsCrawlerSQL, TopicName.SQL_NEWSCRAWLER, data);
                }
                return returnValue;
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
                return false;
            }
        }
        public static async Task<bool> SetScheduleAsync(NewsCrawler data)
        {
            try
            {
                return await StoreFactory.Create<NewsCrawlerCacheStore>().SetScheduleAsync(data);
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
                return false;
            }
        }
        public static async Task<bool> RemoverAsync(NewsCrawler data)
        {
            try
            {
                return await StoreFactory.Create<NewsCrawlerCacheStore>().SetScheduleAsync(data);
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
                return false;
            }
        }

        public static Task<NewsCrawler> GetByIdAsync(long id)
        {
            return StoreFactory.Create<NewsCrawlerSearchStore>().GetByIdAsync(id);
        }

        public static Task<NewsCrawler> GetByLinkAsync(string link, long officerId)
        {
            return StoreFactory.Create<NewsCrawlerSearchStore>().GetByLinkAsync(link, officerId);
        }

        public static Task<List<Tag>> GetListByIdsAsync(List<string> ids)
        {
            return StoreFactory.Create<TagCacheStore>().GetListByIdsAsync(ids);
        }

        public static async Task<ErrorCodes> AddConfigAsync(NewCrawlerConfigurationSearch data, string clientIp)
        {
            var returnValue = ErrorCodes.BusinessError;
            try
            {
                returnValue = await StoreFactory.Create<NewsCrawlerConfigCacheStore>().AddConfigAsync(data);
                if (returnValue == ErrorCodes.Success)
                {
                    await Function.AddToQueue(ActionName.AddNewsCrawlerConfigES, TopicName.ES_NEWSCRAWLERCONFIG, data);
                    await Function.AddToQueue(ActionName.AddNewsCrawlerConfigSQL, TopicName.SQL_NEWSCRAWLERCONFIG, data);
                }
                return returnValue;
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
                return returnValue;
            }
        }

        public static async Task<ErrorCodes> UpdateConfigAsync(NewCrawlerConfigurationSearch data)
        {
            var returnValue = ErrorCodes.BusinessError;
            try
            {
                var returnUpdate = await StoreFactory.Create<NewsCrawlerConfigCacheStore>().UpdateConfigAsync(data);
                if (returnUpdate == ErrorCodes.Success)
                {
                    await Function.AddToQueue(ActionName.UpdateNewsCrawlerConfigES, TopicName.ES_NEWSCRAWLERCONFIG, data);
                    await Function.AddToQueue(ActionName.UpdateNewsCrawlerConfigSQL, TopicName.SQL_NEWSCRAWLERCONFIG, data);
                }
                return returnUpdate;
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
                return returnValue;
            }
        }

        public static async Task<PagingDataResult<NewCrawlerConfigurationSearch>> SearchConfigAsync(string keyword, long officerId, int? status, int pageIndex, int pageSize, ConfigOrderBy? orderBy, DateTime fromDate, DateTime toDate)
        {
            try
            {
                return await StoreFactory.Create<NewsCrawlerConfigSearchStore>().SearchConfigAsync(keyword, officerId, status, pageIndex, pageSize, orderBy, fromDate, toDate);
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
                return null;
            }
        }

        public static Task<NewCrawlerConfigurationSearch> GetConfigByIdAsync(long id)
        {
            return StoreFactory.Create<NewsCrawlerConfigSearchStore>().GetConfigByIdAsync(id);
        }

        public static async Task<ErrorCodes> UpdateJobDateConfigAsync(long id, DateTime lastScanTime)
        {
            var returnValue = ErrorCodes.BusinessError;
            try
            {
                var data = await GetConfigByIdAsync(id);
                if (data != null)
                {
                    data.LastScanTime = lastScanTime;

                    returnValue = await StoreFactory.Create<NewsCrawlerConfigCacheStore>().UpdateConfigAsync(data);
                    if (returnValue == ErrorCodes.Success)
                    {
                        await StoreFactory.Create<NewsCrawlerConfigCacheStore>().UpdateTimeScanCrawlerAsync(data);
                        await Function.AddToQueue(ActionName.UpdateNewsCrawlerConfigES, TopicName.ES_NEWSCRAWLERCONFIG, data);
                        await Function.AddToQueue(ActionName.UpdateNewsCrawlerConfigSQL, TopicName.SQL_NEWSCRAWLERCONFIG, data);
                    }
                }
                return returnValue;
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
                return returnValue;
            }
        }

        public static NewCrawlerConfigurationSearch PopQueueAutoCrawlerConfig()
        {
            try
            {
                return StoreFactory.Create<NewsCrawlerConfigCacheStore>().PopQueueAutoCrawlerConfig();
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
                return null;
            }
        }

        public static void PushQueueAutoCrawlerConfig(List<NewCrawlerConfigurationSearch> listData)
        {
            StoreFactory.Create<NewsCrawlerConfigCacheStore>().PushQueueAutoCrawlerConfig(listData);
        }

        public static bool CheckExistQueueAutoCrawlerConfig()
        {
            return StoreFactory.Create<NewsCrawlerConfigCacheStore>().CheckExistQueueAutoCrawlerConfig();
        }

        public static async Task<bool> ExistListSearchConfigAsync()
        {
            var returnValue = true;
            try
            {
                returnValue = await StoreFactory.Create<NewsCrawlerConfigCacheStore>().ExistListSearchConfigAsync();
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
            }
            return returnValue;
        }

        public static async Task<List<NewCrawlerConfigurationSearch>> GetListConfigSearchFromCacheAsync()
        {
            var returnValue = new List<NewCrawlerConfigurationSearch>();
            try
            {
                returnValue = await StoreFactory.Create<NewsCrawlerConfigCacheStore>().GetListConfigSearchFromCacheAsync();
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
            }
            return returnValue;
        }

        public static async Task<bool> PushListConfigSearchToCacheAsync(List<NewCrawlerConfigurationSearch> data)
        {
            var returnValue = false;
            try
            {
                returnValue = await StoreFactory.Create<NewsCrawlerConfigCacheStore>().PushListConfigSearchToCacheAsync(data);
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
            }
            return returnValue;
        }
    }
}
