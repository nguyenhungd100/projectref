﻿using ChannelVN.IMS2.Core.Entities;
using ChannelVN.IMS2.Core.Entities.NewsOnHome;
using ChannelVN.IMS2.Core.Models.CacheStore;
using ChannelVN.IMS2.Core.Models.ExternalAPI;
using ChannelVN.IMS2.Core.Models.SearchStore;
using ChannelVN.IMS2.Core.Repositories.Constants;
using ChannelVN.IMS2.Core.Repositories.Functions;
using ChannelVN.IMS2.Foundation.Common;
using ChannelVN.IMS2.Foundation.Data;
using ChannelVN.IMS2.Foundation.Data.Factories;
using ChannelVN.IMS2.Foundation.Logging;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Linq;
using static ChannelVN.IMS2.Core.Entities.ErrorCode.ErrorMapping;

namespace ChannelVN.IMS2.Core.Repositories
{
    public class NewsOnHomeRepository : Repository
    {
        public static async Task<bool> AddAsync(NewsOnHome data)
        {
            var returnValue = false;
            try
            {
                returnValue = await StoreFactory.Create<NewsOnHomeCacheStore>().AddAsync(data);
                if (returnValue)
                {
                    await Function.AddToQueue(ActionName.AddNewsOnHomeES, TopicName.ES_NEWSONHOME, data);
                    await Function.AddToQueue(ActionName.AddNewsOnHomeSQL, TopicName.SQL_NEWSONHOME, data);
                }
                return returnValue;
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
                return returnValue;
            }
        }

        public static async Task<PagingDataResult<NewsOnHome>> SearchAsync(string keyword, List<string> officerIds, int? status, byte? officerClass, int pageIndex, int pageSize, OrderBy? orderBy, DateTime fromDate, DateTime toDate)
        {
            try
            {
                return await StoreFactory.Create<NewsOnHomeSearchStore>().SearchAsync(keyword, officerIds, status, officerClass, pageIndex, pageSize, orderBy, fromDate, toDate);
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
                return null;
            }
        }

        public static async Task<bool> UpdateAsync(NewsOnHome data)
        {
            try
            {
                var returnValue = await StoreFactory.Create<NewsOnHomeCacheStore>().UpdateAsync(data);
                if (returnValue)
                {
                    await Function.AddToQueue(ActionName.UpdateNewsOnHomeES, TopicName.ES_NEWSONHOME, data);
                    await Function.AddToQueue(ActionName.UpdateNewsOnHomeSQL, TopicName.SQL_NEWSONHOME, data);
                }
                return returnValue;
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
                return false;
            }
        }

        public static Task<NewsOnHome> GetByIdAsync(long id)
        {
            return StoreFactory.Create<NewsOnHomeSearchStore>().GetByIdAsync(id);
        }

        public static async Task<bool> PushTelegramNotify(long id, string note="")
        {
            try
            {
                var data = await Media.GetByMediaIds<dynamic>(id.ToString());
                if (data != null)
                {
                    var jsonData = data.result.data.FirstOrDefault();
                    var msg = new
                    {
                        jsonData.media_id,
                        jsonData.link_share,
                        pageId = jsonData.user.id,
                        pageName = jsonData.user.full_name
                    };
                    var dataPost = new Dictionary<string, string>()
                    {
                        { "msg", $"Bài post đang chờ duyệt: " +
                        $"</br><a href='{jsonData.link_share}'>{jsonData.link_share}</a>" +
                        $"</br>page <b>{jsonData.user.full_name}</b> {note}"},
                        { "data", Json.Stringify(msg)}
                    };
                    await TelegramApiService.PushTelegramAsync(dataPost);
                }

                return true;
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
                return false;
            }
        }

        public static async Task<PagingDataResult<NewsOnHome>> SearchNewsOnHome(string keyword, List<string> officerIds, int? status, byte? officerClass, int pageIndex, int pageSize, OrderBy? orderBy, DateTime fromDate, DateTime toDate)
        {
            try
            {
                return await StoreFactory.Create<NewsOnHomeSearchStore>().SearchAsync(keyword, officerIds, status, officerClass, pageIndex, pageSize, orderBy, fromDate, toDate);
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
                return null;
            }
        }

        public static async Task<bool> ExistListSearchCacheAsync()
        {
            var returnValue = true;
            try
            {
                returnValue = await StoreFactory.Create<NewsOnHomeCacheStore>().ExistListSearchCacheAsync();
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);              
            }
            return returnValue;
        }

        public static async Task<bool> PushListSearchToCacheAsync(List<NewsOnHome> data)
        {
            var returnValue = true;
            try
            {
                returnValue = await StoreFactory.Create<NewsOnHomeCacheStore>().PushListSearchToCacheAsync(data);
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
            }
            return returnValue;
        }

        public static async Task<List<NewsOnHome>> GetListSearchFromCacheAsync()
        {
            var returnValue = new List<NewsOnHome>();
            try
            {
                returnValue = await StoreFactory.Create<NewsOnHomeCacheStore>().GetListSearchFromCacheAsync();
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
            }
            return returnValue;
        }

        public static async Task<bool> NotificationPostUsers(NewsOnHome data)
        {
            try
            {
                var infoPost = await Media.GetById<NewsOnHomeSearch>(data.NewsId.ToString());
                //notify app
                var dataPost = new
                {
                    payload = new
                    {
                        payload = new
                        {
                            notification = new
                            {
                                title = infoPost?.result?.data?.FirstOrDefault().link_share,
                                body = infoPost?.result?.data?.FirstOrDefault().title,
                                image = infoPost?.result?.data?.FirstOrDefault().data?.FirstOrDefault().thumb
                            }
                        }
                    },
                    users = new List<string> {
                                infoPost?.result?.data?.FirstOrDefault().user?.id,
                                data.OrderedBy
                            }.ToArray()
                };
                var res = await NotificationApiService.SendPostUsers(data.NewsId.ToString(), dataPost);

                return res == ErrorCodes.Success?true:false;
            }
            catch
            {
                return false;
            }
        }

        public static NewsOnHome PopQueueJobNewsOnHome()
        {
            try
            {
                return StoreFactory.Create<NewsOnHomeCacheStore>().PopQueueJobNewsOnHome();
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
                return null;
            }
        }

        public static void PushQueueJobNewsOnHome(IEnumerable<NewsOnHome> listToScans)
        {
            StoreFactory.Create<NewsOnHomeCacheStore>().PushQueueJobNewsOnHome(listToScans);
        }

        public static bool CheckExistQueueJobNewsOnHome()
        {
            return StoreFactory.Create<NewsOnHomeCacheStore>().CheckExistQueueJobNewsOnHome();
        }
    }
}
