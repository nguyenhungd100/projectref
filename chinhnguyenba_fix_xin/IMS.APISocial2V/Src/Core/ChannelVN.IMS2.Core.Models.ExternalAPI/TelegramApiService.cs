﻿using ChannelVN.IMS2.Foundation.Common;
using ChannelVN.IMS2.Foundation.Common.Configuration;
using ChannelVN.IMS2.Foundation.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using static ChannelVN.IMS2.Core.Entities.ErrorCode.ErrorMapping;

namespace ChannelVN.IMS2.Core.Models.ExternalAPI
{
    public class TelegramApiService
    {
        public static async Task<ErrorCodes> PushTelegramAsync(Dictionary<string, string> dataPosts)
        {
            try
            {
                var apiPort = AppSettings.Current.ChannelConfiguration.TelegramNotifySetting.ApiUrl;
                if (apiPort != null && !string.IsNullOrEmpty(apiPort))
                {
                    using (var client = new HttpClient())
                    {
                        //hearder thay vì session-id , các anh truyền vào user-id của page hoặc user nhé
                        client.DefaultRequestHeaders.Add("sec_key", AppSettings.Current.ChannelConfiguration.TelegramNotifySetting.Seckey);                        
                        client.Timeout = TimeSpan.FromSeconds(AppSettings.Current.ChannelConfiguration.HttpClientTimeOut <= 0 ? 30 : AppSettings.Current.ChannelConfiguration.HttpClientTimeOut);

                        var keyValues = new List<KeyValuePair<string, string>>();
                        if (dataPosts != null && dataPosts.Count > 0)
                        {
                            foreach (KeyValuePair<string, string> pair in dataPosts)
                            {
                                keyValues.Add(new KeyValuePair<string, string>(pair.Key, pair.Value));
                            }
                        }
                        var content = new FormUrlEncodedContent(keyValues);

                        var response = await client.PostAsync(apiPort, content);
                        if (!response.IsSuccessStatusCode)
                        {
                            //TelegramNotification.SendMessageAsync(Json.Stringify(new { api = apiPort, media_id, response = response.ToString(), cardType }));
                            Logger.Debug("PushTelegramAsync IsSuccessStatusCode: =>" + Json.Stringify(new { api = apiPort, response = response.ToString() }));
                            response.EnsureSuccessStatusCode();
                        }
                        var result = await response.Content.ReadAsStringAsync();
                        if (!string.IsNullOrEmpty(result))
                        {
                            var obj = Json.Parse<TelegramDataResponse>(result);
                            if (obj != null && obj.status)
                            {
                                //TelegramNotification.SendMessageAsync(Json.Stringify(new { api = apiPort, media_id, kinghub_id = obj.result.data.FirstOrDefault().id, Success="Success." }));
                                Logger.Debug("PushTelegramAsync response success: =>" + Json.Stringify(new { api = apiPort, kinghub_id = obj.status, Success = "Success.", dataPosts }));
                                return ErrorCodes.Success;
                            }                            
                        }

                        //TelegramNotification.SendMessageAsync(Json.Stringify(new { api = apiPort, media_id, result, cardType }));
                        Logger.Debug("PushTelegramAsync response error: =>" + Json.Stringify(new { api = apiPort,  result, dataPosts }));

                        return ErrorCodes.BusinessError;
                    }
                }
                return ErrorCodes.BusinessError;
            }
            catch (Exception ex)
            {
                Logger.Error(ex, "PushTelegramAsync error: " + ex.Message);
                return ErrorCodes.Exception;
            }
        }
    }

    public class TelegramDataResponse
    {        
        public bool status { get; set; }
    }
}
