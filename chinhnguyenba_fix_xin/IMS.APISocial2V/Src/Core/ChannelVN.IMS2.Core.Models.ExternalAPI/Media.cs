﻿using ChannelVN.IMS2.Core.Entities.Media;
using ChannelVN.IMS2.Foundation.Logging;
using System;
using System.Threading.Tasks;
using static ChannelVN.IMS2.Core.Entities.ErrorCode.ErrorMapping;
using System.Net.Http;
using ChannelVN.IMS2.Foundation.Common.Configuration;
using ChannelVN.IMS2.Foundation.Common;
using TelegramNotification = ChannelVN.IMS2.Foundation.Logging.TelegramNotification;
using System.Text;
using System.Linq;
using System.Net;

namespace ChannelVN.IMS2.Core.Models.ExternalAPI
{
    public class Media
    {
        public enum Action
        {
            add,
            update,
            deletebymedia,
            getbymedia
        }

        public static async Task<ErrorCodes> Add(string media_id, string sessionId, CardType cardType, dynamic dataPosts)
        {
            try
            {
                var apiPort = string.Format("{0}{1}",AppSettings.Current.ChannelConfiguration.KingHubPostApiSetting.ApiUrl,Action.add);
                if (apiPort != null && !string.IsNullOrEmpty(apiPort))
                {
                    var res= ErrorCodes.BusinessError;
                    using (var client = new HttpClient())
                    {
                        //hearder thay vì session-id , các anh truyền vào user-id của page hoặc user nhé
                        client.DefaultRequestHeaders.Add("user-id", sessionId);
                        //client.DefaultRequestHeaders.Add("session-id", sessionId);
                        client.Timeout = TimeSpan.FromSeconds(AppSettings.Current.ChannelConfiguration.HttpClientTimeOut <= 0 ? 30 : AppSettings.Current.ChannelConfiguration.HttpClientTimeOut);
                        var response = await client.PostAsync(apiPort, new StringContent(Json.Stringify(dataPosts), Encoding.UTF8, "application/json"));
                        if (!response.IsSuccessStatusCode)
                        {
                            TelegramNotification.SendMessageAsync(Json.Stringify(new { api = apiPort, media_id, response = response.ToString(), cardType }));
                            Logger.Debug("Post add IsSuccessStatusCode: =>" + Json.Stringify(new { api = apiPort, media_id, response = response.ToString(), cardType }));
                            response.EnsureSuccessStatusCode();
                        }
                        var result = await response.Content.ReadAsStringAsync();
                        if (!string.IsNullOrEmpty(result))
                        {
                            var obj = Json.Parse<MediaDataResponse>(result);
                            if (obj != null && obj.status == 1)
                            {
                                //TelegramNotification.SendMessageAsync(Json.Stringify(new { api = apiPort, media_id, kinghub_id = obj.result.data.FirstOrDefault().id, Success="Success." }));
                                Logger.Debug("Post add response success: =>" + Json.Stringify(new { api = apiPort, media_id, kinghub_id = obj.result.data.FirstOrDefault().id, Success = "Success.", cardType, sessionId, dataPosts }));
                                return ErrorCodes.Success;
                            }
                            else
                            {
                                if(obj.code == (int)HttpStatusCode.InternalServerError)
                                {
                                    res = ErrorCodes.DataExist;
                                }
                            }
                        }
                        if(res!= ErrorCodes.DataExist)
                        {
                            TelegramNotification.SendMessageAsync(Json.Stringify(new { api = apiPort, media_id, result, cardType }));
                            Logger.Debug("Post add response error: =>" + Json.Stringify(new { api = apiPort, media_id, result, cardType, dataPosts }));
                        }

                        return res;
                    }
                }
                return ErrorCodes.BusinessError;
            }
            catch (Exception ex)
            {
                Logger.Error(ex, "Post error: " + ex.Message);
                return ErrorCodes.Exception;
            }
        }

        public static async Task<ErrorCodes> Update(string media_id, string sessionId, CardType cardType, dynamic dataPosts)
        {
            try
            {
                //Logger.Debug("dataPosts: =>" + Json.Stringify(dataPosts));
                var apiPort = string.Format("{0}{1}", AppSettings.Current.ChannelConfiguration.KingHubPostApiSetting.ApiUrl, Action.update);
                if (apiPort != null && !string.IsNullOrEmpty(apiPort))
                {
                    using (var client = new HttpClient())
                    {
                        client.DefaultRequestHeaders.Add("user-id", sessionId);
                        //client.DefaultRequestHeaders.Add("session-id", sessionId);
                        client.Timeout = TimeSpan.FromSeconds(AppSettings.Current.ChannelConfiguration.HttpClientTimeOut <= 0 ? 30 : AppSettings.Current.ChannelConfiguration.HttpClientTimeOut);
                        var response = await client.PostAsync(apiPort, new StringContent(Json.Stringify(dataPosts), Encoding.UTF8, "application/json"));
                        if (!response.IsSuccessStatusCode)
                        {
                            TelegramNotification.SendMessageAsync(Json.Stringify(new { api = apiPort, media_id, response = response.ToString(), cardType }));
                            Logger.Debug("Post update IsSuccessStatusCode: =>" + Json.Stringify(new { api = apiPort, media_id, response = response.ToString(), cardType }));
                            response.EnsureSuccessStatusCode();
                        }
                        var result = await response.Content.ReadAsStringAsync();
                        if (!string.IsNullOrEmpty(result))
                        {
                            var obj = Json.Parse<MediaDataResponse>(result);
                            if (obj != null && obj.status == 1)
                            {
                                //TelegramNotification.SendMessageAsync(Json.Stringify(new { api = apiPort, media_id, kinghub_id = obj.result.data.FirstOrDefault().id, Success="Success." }));
                                Logger.Debug("Post update response success: =>" + Json.Stringify(new { api = apiPort, media_id, kinghub_id = obj.result.data.FirstOrDefault().id, Success = "Success.", cardType, sessionId }));
                                return ErrorCodes.Success;
                            }
                        }

                        TelegramNotification.SendMessageAsync(Json.Stringify(new { api = apiPort, media_id, result, cardType }));
                        Logger.Debug("Post update response error: =>" + Json.Stringify(new { api = apiPort, media_id, result, cardType, dataPosts }));

                        return ErrorCodes.BusinessError;
                    }
                }
                return ErrorCodes.BusinessError;
            }
            catch (Exception ex)
            {
                Logger.Error(ex, "Post error: " + ex.Message);
                return ErrorCodes.Exception;
            }
        }

        public static async Task<ErrorCodes> Delete(string mediaId, string sessionId, CardType cardType)
        {
            try
            {
                var apiPort = string.Format("{0}{1}?media_id={2}", AppSettings.Current.ChannelConfiguration.KingHubPostApiSetting.ApiUrl, Action.deletebymedia, mediaId);
                if (apiPort != null && !string.IsNullOrEmpty(apiPort))
                {
                    using (var client = new HttpClient())
                    {
                        client.DefaultRequestHeaders.Add("user-id", sessionId);
                        //client.DefaultRequestHeaders.Add("session-id", sessionId);
                        client.Timeout = TimeSpan.FromSeconds(AppSettings.Current.ChannelConfiguration.HttpClientTimeOut <= 0 ? 30 : AppSettings.Current.ChannelConfiguration.HttpClientTimeOut);
                        var response = await client.DeleteAsync(apiPort);
                        if (!response.IsSuccessStatusCode)
                        {
                            TelegramNotification.SendMessageAsync(Json.Stringify(new { api = apiPort, media_id = mediaId, response = response.ToString(), cardType }));
                            Logger.Debug("Delete IsSuccessStatusCode: =>" + Json.Stringify(new { api = apiPort, media_id = mediaId, response = response.ToString(), cardType }));
                            response.EnsureSuccessStatusCode();
                        }
                        var result = await response.Content.ReadAsStringAsync();
                        if (!string.IsNullOrEmpty(result))
                        {
                            var obj = Json.Parse<MediaDataResponse>(result);
                            if (obj != null && obj.status == 1)
                            {
                                //TelegramNotification.SendMessageAsync(Json.Stringify(new { api = apiPort + string.Format("?media_id={0}", id), Success = "Success." }));
                                Logger.Debug("Delete response success: =>" + Json.Stringify(new { api = apiPort + string.Format("?media_id={0}", mediaId), Success = "Success.", cardType, sessionId }));
                                return ErrorCodes.Success;
                            }
                        }

                        TelegramNotification.SendMessageAsync(Json.Stringify(new { api = apiPort, media_id = mediaId, result, cardType }));
                        Logger.Debug("Delete response error: =>" + Json.Stringify(new { api = apiPort, media_id = mediaId, result, cardType }));

                        return ErrorCodes.BusinessError;
                    }
                }
                return ErrorCodes.BusinessError;
            }
            catch (Exception ex)
            {
                Logger.Error(ex, "Delete error: " + ex.Message);
                return ErrorCodes.Exception;
            }
        }

        public static async Task<MediaCardResponse<T>> GetById<T>(string mediaId, string sessionId="")
        {
            try
            {
                var apiPort = string.Format("{0}{1}?media_id={2}", AppSettings.Current.ChannelConfiguration.KingHubPostApiSetting.ApiUrl, Action.getbymedia, mediaId);
                if (apiPort != null && !string.IsNullOrEmpty(apiPort))
                {
                    using (var client = new HttpClient())
                    {
                        client.DefaultRequestHeaders.Add("user-id", sessionId);
                        //client.DefaultRequestHeaders.Add("session-id", sessionId);
                        client.Timeout = TimeSpan.FromSeconds(AppSettings.Current.ChannelConfiguration.HttpClientTimeOut <= 0 ? 30 : AppSettings.Current.ChannelConfiguration.HttpClientTimeOut);
                        var response = await client.GetAsync(apiPort);
                        if (!response.IsSuccessStatusCode)
                        {                           
                            response.EnsureSuccessStatusCode();
                        }
                        var result = await response.Content.ReadAsStringAsync();
                        if (!string.IsNullOrEmpty(result))
                        {
                            var obj = Json.Parse<MediaCardResponse<T>>(result);
                            if (obj != null)
                            {                                
                                return obj;
                            }
                        }
                        return default(MediaCardResponse<T>);
                    }
                }
                return default(MediaCardResponse<T>);
            }
            catch (Exception ex)
            {
                Logger.Error(ex, "GetById error: " + ex.Message);
                return default(MediaCardResponse<T>);
            }
        }

        public static async Task<MediaCardResponse<T>> GetByIds<T>(string mediaIds, string sessionId)
        {
            try
            {
                var apiPort = string.Format("{0}?mediaids={1}", AppSettings.Current.ChannelConfiguration.ApiInteractionsStatisticsSetting.CardInfo.Url, mediaIds);
                if (apiPort != null && !string.IsNullOrEmpty(apiPort))
                {
                    using (var client = new HttpClient())
                    {
                        client.DefaultRequestHeaders.Add("session-id", sessionId);
                        client.Timeout = TimeSpan.FromSeconds(AppSettings.Current.ChannelConfiguration.HttpClientTimeOut <= 0 ? 30 : AppSettings.Current.ChannelConfiguration.HttpClientTimeOut);
                        var response = await client.GetAsync(apiPort);
                        if (!response.IsSuccessStatusCode)
                        {                            
                            response.EnsureSuccessStatusCode();
                        }
                        var result = await response.Content.ReadAsStringAsync();
                        if (!string.IsNullOrEmpty(result))
                        {
                            var obj = Json.Parse<MediaCardResponse<T>>(result);
                            if (obj != null)
                            {                                
                                return obj;
                            }
                        }                        
                        Logger.Debug("GetByIds response error: =>" + Json.Stringify(new { api = apiPort, media_ids = mediaIds, result }));

                        return default(MediaCardResponse<T>);
                    }
                }
                return default(MediaCardResponse<T>);
            }
            catch (Exception ex)
            {
                Logger.Error(ex, "GetByIds error: " + ex.Message);
                return default(MediaCardResponse<T>);
            }
        }
        public static async Task<MediaCardResponse<T>> GetByMediaIds<T>(string mediaIds, string userId="")
        {
            try
            {
                var apiPort = string.Format("{0}?mediaids={1}", AppSettings.Current.ChannelConfiguration.ApiInteractionsStatisticsSetting.CardInfo.UrlIp, mediaIds);
                if (apiPort != null && !string.IsNullOrEmpty(apiPort))
                {
                    using (var client = new HttpClient())
                    {
                        client.DefaultRequestHeaders.Add("user-id", userId);
                        client.Timeout = TimeSpan.FromSeconds(AppSettings.Current.ChannelConfiguration.HttpClientTimeOut <= 0 ? 30 : AppSettings.Current.ChannelConfiguration.HttpClientTimeOut);
                        var response = await client.GetAsync(apiPort);
                        if (!response.IsSuccessStatusCode)
                        {
                            response.EnsureSuccessStatusCode();
                        }
                        var result = await response.Content.ReadAsStringAsync();
                        if (!string.IsNullOrEmpty(result))
                        {
                            var obj = Json.Parse<MediaCardResponse<T>>(result);
                            if (obj != null)
                            {
                                return obj;
                            }
                        }
                        Logger.Debug("GetByIds response error: =>" + Json.Stringify(new { api = apiPort, media_ids = mediaIds, result }));

                        return default(MediaCardResponse<T>);
                    }
                }
                return default(MediaCardResponse<T>);
            }
            catch (Exception ex)
            {
                Logger.Error(ex, "GetByIds error: " + ex.Message);
                return default(MediaCardResponse<T>);
            }
        }

        public static async Task<ErrorCodes> AddOnHome(string mediaId, string sessionId, int status, long accountId)
        {
            try
            {
                var apiPort = string.Format("{0}/{1}?postid={2}&userid={3}&status={4}", AppSettings.Current.ChannelConfiguration.ApiSetNewsOnHomeSetting.ApiUrl, "add", mediaId, accountId, status);                
                if (apiPort != null && !string.IsNullOrEmpty(apiPort))
                {                    
                    using (var client = new HttpClient())
                    {
                        client.DefaultRequestHeaders.Add("user-id", accountId.ToString());
                        //client.DefaultRequestHeaders.Add("session-id", sessionId);
                        client.Timeout = TimeSpan.FromSeconds(AppSettings.Current.ChannelConfiguration.HttpClientTimeOut <= 0 ? 30 : AppSettings.Current.ChannelConfiguration.HttpClientTimeOut);                        
                        var response = await client.GetAsync(apiPort);
                        if (!response.IsSuccessStatusCode)
                        {
                            TelegramNotification.SendMessageAsync(Json.Stringify(new { api = apiPort, media_id = mediaId, response = response.ToString() }));
                            Logger.Debug("AddOnHome IsSuccessStatusCode: =>" + Json.Stringify(new { api = apiPort, media_id = mediaId, response = response.ToString() }));
                            response.EnsureSuccessStatusCode();
                        }
                        var result = await response.Content.ReadAsStringAsync();
                        if (!string.IsNullOrEmpty(result))
                        {                            
                            var obj = Json.Parse<MediaDataResponse>(result);
                            if (obj != null && (obj.status == 1 || obj.status == 0))
                            {
                                //TelegramNotification.SendMessageAsync(Json.Stringify(new { api = apiPort + string.Format("?media_id={0}", id), Success = "Success." }));
                                Logger.Debug("AddOnHome response success: =>" + Json.Stringify(new { api = apiPort, media_id = mediaId, Success = "Success.", sessionId }));
                                return ErrorCodes.Success;
                            }
                        }

                        TelegramNotification.SendMessageAsync(Json.Stringify(new { api = apiPort, media_id = mediaId, result }));
                        Logger.Debug("AddOnHome response error: =>" + Json.Stringify(new { api = apiPort, media_id = mediaId, result }));

                        return ErrorCodes.BusinessError;
                    }
                }
                return ErrorCodes.BusinessError;
            }
            catch (Exception ex)
            {
                Logger.Error(ex, "AddOnHome error: " + ex.Message);
                return ErrorCodes.Exception;
            }
        }

        public static async Task<ErrorCodes> UpdateOnHome(string mediaId, string sessionId, int status, long accountId)
        {
            try
            {
                var apiPort = string.Format("{0}/{1}?postid={2}&userid={3}&status={4}", AppSettings.Current.ChannelConfiguration.ApiSetNewsOnHomeSetting.ApiUrl, "update", mediaId, accountId, status);
                if (apiPort != null && !string.IsNullOrEmpty(apiPort))
                {                    
                    using (var client = new HttpClient())
                    {
                        client.DefaultRequestHeaders.Add("user-id", accountId.ToString());
                        //client.DefaultRequestHeaders.Add("session-id", sessionId);
                        client.Timeout = TimeSpan.FromSeconds(AppSettings.Current.ChannelConfiguration.HttpClientTimeOut <= 0 ? 30 : AppSettings.Current.ChannelConfiguration.HttpClientTimeOut);                        
                        var response = await client.GetAsync(apiPort);
                        if (!response.IsSuccessStatusCode)
                        {
                            TelegramNotification.SendMessageAsync(Json.Stringify(new { api = apiPort, media_id = mediaId, response = response.ToString() }));
                            Logger.Debug("UpdateOnHome IsSuccessStatusCode: =>" + Json.Stringify(new { api = apiPort, media_id = mediaId, response = response.ToString() }));
                            response.EnsureSuccessStatusCode();
                        }
                        var result = await response.Content.ReadAsStringAsync();
                        if (!string.IsNullOrEmpty(result))
                        {                            
                            var obj = Json.Parse<MediaDataResponse>(result);
                            if (obj != null && obj.status == 1)
                            {
                                //TelegramNotification.SendMessageAsync(Json.Stringify(new { api = apiPort + string.Format("?media_id={0}", id), Success = "Success." }));
                                Logger.Debug("UpdateOnHome response success: =>" + Json.Stringify(new { api = apiPort ,media_id=mediaId, Success = "Success.", sessionId }));
                                return ErrorCodes.Success;
                            }
                        }

                        TelegramNotification.SendMessageAsync(Json.Stringify(new { api = apiPort, media_id = mediaId, result }));
                        Logger.Debug("UpdateOnHome response error: =>" + Json.Stringify(new { api = apiPort, media_id = mediaId, result }));

                        return ErrorCodes.BusinessError;
                    }
                }
                return ErrorCodes.BusinessError;
            }
            catch (Exception ex)
            {                
                Logger.Error(ex, "UpdateOnHome error: " + ex.Message);
                return ErrorCodes.Exception;
            }
        }
    }
}
