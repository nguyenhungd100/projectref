﻿using System.Collections.Generic;

namespace ChannelVN.IMS2.Core.Models.ExternalAPI
{
    public class MediaDataResponse
    {
        public MediaDataItemCollectionResponse result { get; set; }
        public int code { get; set; }
        public string message { get; set; }
        public int status { get; set; }
    }
    public class MediaDataItemCollectionResponse
    {
        public List<MediaDataItemResponse> data { get; set; }
    }
    public class MediaDataItemResponse
    {
        public string id { get; set; }
    }

    public class MediaCardResponse<T>
    {
        public MediaCardItemCollectionResponse<T> result { get; set; }
        public int code { get; set; }
        public string message { get; set; }
        public int status { get; set; }
    }
    public class MediaCardItemCollectionResponse<T>
    {
        public List<T> data { get; set; }
    }    
}
