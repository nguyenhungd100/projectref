﻿using ChannelVN.IMS2.Foundation.Common;
using ChannelVN.IMS2.Foundation.Common.Configuration;
using ChannelVN.IMS2.Foundation.Logging;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace ChannelVN.IMS2.Core.Models.ExternalAPI
{
    public class Bizfly
    {
        public static async Task<BizflyResponse> ActiveChatAsync(long pageId)
        {
            var returnValue = default(BizflyResponse);
            try
            {
                var config = AppSettings.Current.ChannelConfiguration.BizflyConfig;
                using(var client = new HttpClient())
                {
                    var url = config.UrlActive;

                    var timeStamp = DateTime.Now.ConvertToTimestamp();
                    var headerString = config.AppID + "_" + Encryption.Md5(config.ApiKey + "_" + timeStamp);
                    client.DefaultRequestHeaders.Add("authorization", headerString);

                    var body = Json.Stringify(new
                    {
                        page_id = pageId.ToString(),
                        ts = timeStamp
                    });

                    client.Timeout = TimeSpan.FromSeconds(AppSettings.Current.ChannelConfiguration.HttpClientTimeOut);

                    Logger.Debug("ActiveChatAsync=> url:" + url);
                    Logger.Debug("ActiveChatAsync=> body:" + body);

                    var response = await client.PostAsync(url, new StringContent(body, Encoding.UTF8, "application/json"));
                    if (!response.IsSuccessStatusCode)
                    {
                        response.EnsureSuccessStatusCode();
                    }

                    var astr = await response.Content?.ReadAsStringAsync();
                    Logger.Debug("ActiveChatAsync=> response:" + astr);

                    returnValue = Json.Parse<BizflyResponse>(astr);
                }
            }catch(Exception ex)
            {
                Logger.Sensitive(ex, ex.Message);
                returnValue = new BizflyResponse
                {
                    message = ex.Message
                };
            }
            return returnValue;
        }

        public static async Task<BizflyResponse> DeactiveChatAsync(long pageId)
        {
            var returnValue = default(BizflyResponse);
            try
            {
                var config = AppSettings.Current.ChannelConfiguration.BizflyConfig;
                using (var client = new HttpClient())
                {
                    var url = config.UrlDeactive;

                    var timeStamp = DateTime.Now.ConvertToTimestamp();
                    var headerString = config.AppID + "_" + Encryption.Md5(config.ApiKey + "_" + timeStamp);
                    client.DefaultRequestHeaders.Add("authorization", headerString);

                    var body = Json.Stringify(new
                    {
                        page_id = pageId.ToString(),
                        ts = timeStamp
                    });

                    client.Timeout = TimeSpan.FromSeconds(AppSettings.Current.ChannelConfiguration.HttpClientTimeOut);

                    Logger.Debug("DeactiveChatAsync=> url:" + url);
                    Logger.Debug("DeactiveChatAsync=> body:" + body);

                    var response = await client.PostAsync(url, new StringContent(body, Encoding.UTF8, "application/json"));
                    if (!response.IsSuccessStatusCode)
                    {
                        response.EnsureSuccessStatusCode();
                    }

                    var astr = await response.Content?.ReadAsStringAsync();
                    Logger.Debug("DeactiveChatAsync=> response:" + astr);

                    returnValue = Json.Parse<BizflyResponse>(astr);
                }
            }
            catch (Exception ex)
            {
                Logger.Sensitive(ex, ex.Message);
                returnValue = new BizflyResponse
                {
                    message = ex.Message
                };
            }
            return returnValue;
        }

        public static async Task<BizflyResponse> AddUserToPageAsync(long pageId, string email, string mobile)
        {
            var returnValue = default(BizflyResponse);
            try
            {
                var config = AppSettings.Current.ChannelConfiguration.BizflyConfig;
                using (var client = new HttpClient())
                {
                    var url = config.UrlAddUserToPage;

                    var timeStamp = DateTime.Now.ConvertToTimestamp();
                    var headerString = config.AppID + "_" + Encryption.Md5(config.ApiKey + "_" + timeStamp);
                    client.DefaultRequestHeaders.Add("authorization", headerString);

                    var body = Json.Stringify(new
                    {
                        page_id = pageId.ToString(),
                        email,
                        phone = mobile,
                        ts = timeStamp
                    });

                    client.Timeout = TimeSpan.FromSeconds(AppSettings.Current.ChannelConfiguration.HttpClientTimeOut);

                    Logger.Debug("AddUserToPageAsync=> url:" + url);
                    Logger.Debug("AddUserToPageAsync=> body:" + body);

                    var response = await client.PostAsync(url, new StringContent(body, Encoding.UTF8, "application/json"));
                    if (!response.IsSuccessStatusCode)
                    {
                        response.EnsureSuccessStatusCode();
                    }

                    var astr = await response.Content?.ReadAsStringAsync();
                    Logger.Debug("AddUserToPageAsync=> response:" + astr);

                    returnValue = Json.Parse<BizflyResponse>(astr);
                }
            }
            catch (Exception ex)
            {
                Logger.Sensitive(ex, ex.Message);
                returnValue = new BizflyResponse
                {
                    message = ex.Message
                };
            }
            return returnValue;
        }

        public static async Task<BizflyResponse> CheckActiveChatAsync(string pageIds)
        {
            var returnValue = default(BizflyResponse);
            try
            {
                var config = AppSettings.Current.ChannelConfiguration.BizflyConfig;
                using (var client = new HttpClient())
                {
                    var timeStamp = DateTime.Now.ConvertToTimestamp();
                    var url = config.UrlCheckActive;
                    var headerString = config.AppID + "_" + Encryption.Md5(config.ApiKey + "_" + timeStamp);
                    client.DefaultRequestHeaders.Add("authorization", headerString);

                    var body = Json.Stringify(new
                    {
                        ts = timeStamp,
                        page_ids = pageIds?.Split(",")
                    });

                    client.Timeout = TimeSpan.FromSeconds(AppSettings.Current.ChannelConfiguration.HttpClientTimeOut);

                    Logger.Debug("CheckActiveChatAsync=> url:" + url);
                    Logger.Debug("CheckActiveChatAsync=> body:" + body);

                    var response = await client.PostAsync(url, new StringContent(body, Encoding.UTF8, "application/json"));
                    if (!response.IsSuccessStatusCode)
                    {
                        response.EnsureSuccessStatusCode();
                    }

                    var astr = await response.Content?.ReadAsStringAsync();
                    Logger.Debug("CheckActiveChatAsync=> response:" + astr);

                    returnValue = Json.Parse<BizflyResponse>(astr);
                }
            }
            catch (Exception ex)
            {
                Logger.Sensitive(ex, ex.Message);
                returnValue = new BizflyResponse
                {
                    message = ex.Message
                };
            }
            return returnValue;
        }

        public static async Task<BizflyResponse> GetPageOfUserAsync(string email, string mobile)
        {
            var returnValue = default(BizflyResponse);
            try
            {
                var config = AppSettings.Current.ChannelConfiguration.BizflyConfig;
                using (var client = new HttpClient())
                {
                    var timeStamp = DateTime.Now.ConvertToTimestamp();
                    var url = config.UrlGetPageOfUser;
                    var headerString = config.AppID + "_" + Encryption.Md5(config.ApiKey + "_" + timeStamp);
                    client.DefaultRequestHeaders.Add("authorization", headerString);

                    var body = Json.Stringify(new
                    {
                        ts = timeStamp,
                        email,
                        phone = mobile
                    });

                    client.Timeout = TimeSpan.FromSeconds(AppSettings.Current.ChannelConfiguration.HttpClientTimeOut);

                    Logger.Debug("GetPageOfUserAsync=> url:" + url);
                    Logger.Debug("GetPageOfUserAsync=> body:" + body);

                    var response = await client.PostAsync(url, new StringContent(body, Encoding.UTF8, "application/json"));
                    if (!response.IsSuccessStatusCode)
                    {
                        response.EnsureSuccessStatusCode();
                    }

                    var astr = await response.Content?.ReadAsStringAsync();
                    Logger.Debug("GetPageOfUserAsync=> response:" + astr);

                    returnValue = Json.Parse<BizflyResponse>(astr);
                }
            }
            catch (Exception ex)
            {
                Logger.Sensitive(ex, ex.Message);
                returnValue = new BizflyResponse
                {
                    message = ex.Message
                };
            }
            return returnValue;
        }

        public class BizflyResponse
        {
            public int status { get; set; }
            public int successCode { get; set; }
            public int errorCode { get; set; }
            public string message { get; set; }
            public object data { get; set; }
        }
    }
}
