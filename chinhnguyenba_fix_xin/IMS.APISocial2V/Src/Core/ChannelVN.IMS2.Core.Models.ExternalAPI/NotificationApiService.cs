﻿using ChannelVN.IMS2.Foundation.Logging;
using System;
using System.Threading.Tasks;
using static ChannelVN.IMS2.Core.Entities.ErrorCode.ErrorMapping;
using System.Net.Http;
using ChannelVN.IMS2.Foundation.Common.Configuration;
using ChannelVN.IMS2.Foundation.Common;
using TelegramNotification = ChannelVN.IMS2.Foundation.Logging.TelegramNotification;
using System.Text;
using System.Linq;

namespace ChannelVN.IMS2.Core.Models.ExternalAPI
{
    public class NotificationApiService
    {
        public static async Task<ErrorCodes> SendPostUsers(string media_id, dynamic dataPosts)
        {
            try
            {
                var apiPort = AppSettings.Current.ChannelConfiguration.NotificationPostApiSetting.ApiUrl;
                if (apiPort != null && !string.IsNullOrEmpty(apiPort))
                {
                    var res = ErrorCodes.BusinessError;
                    using (var client = new HttpClient())
                    {                        
                        //client.DefaultRequestHeaders.Add("user-id", string.Empty);                        
                        client.Timeout = TimeSpan.FromSeconds(AppSettings.Current.ChannelConfiguration.HttpClientTimeOut <= 0 ? 30 : AppSettings.Current.ChannelConfiguration.HttpClientTimeOut);
                        var response = await client.PostAsync(apiPort, new StringContent(Json.Stringify(dataPosts), Encoding.UTF8, "application/json"));
                        if (!response.IsSuccessStatusCode)
                        {
                            TelegramNotification.SendMessageAsync(Json.Stringify(new { api = apiPort, media_id, response = response.ToString() }));
                            Logger.Debug("SendPostUsers IsSuccessStatusCode: =>" + Json.Stringify(new { api = apiPort, media_id, response = response.ToString() }));
                            response.EnsureSuccessStatusCode();
                        }
                        var result = await response.Content.ReadAsStringAsync();
                        if (!string.IsNullOrEmpty(result))
                        {
                            var obj = Json.Parse<MediaDataResponse>(result);
                            if (obj != null && obj.status == 1)
                            {
                                //TelegramNotification.SendMessageAsync(Json.Stringify(new { api = apiPort, media_id, kinghub_id = obj.result.data.FirstOrDefault().id, Success="Success." }));
                                Logger.Debug("SendPostUsers response success: =>" + Json.Stringify(new { api = apiPort, media_id, kinghub_id = obj.result.data.FirstOrDefault().id, Success = "Success.", dataPosts }));
                                return ErrorCodes.Success;
                            }                            
                        }
                        
                        TelegramNotification.SendMessageAsync(Json.Stringify(new { api = apiPort, media_id, result }));
                        Logger.Debug("SendPostUsers response error: =>" + Json.Stringify(new { api = apiPort, media_id, result, dataPosts }));                       

                        return res;
                    }
                }
                return ErrorCodes.BusinessError;
            }
            catch (Exception ex)
            {
                Logger.Error(ex, "Post error: " + ex.Message);
                return ErrorCodes.Exception;
            }
        }
    }
}
