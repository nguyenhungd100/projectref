﻿using ChannelVN.IMS2.Foundation.Common;
using System;
using System.Collections.Generic;
using System.Text;

namespace ChannelVN.IMS2.Foundation.Security
{
    public static class BlackListToken
    {
        public static void AddElement(string token)
        {
            try
            {
                Data.RemoveAll(p => Utility.ConvertSecondsUtcToDate((double?)Foundation.Security.SecureParser.Parse(token)?.exp, DateTimeKind.Local) < DateTime.Now);
                if (!Data.Contains(token))
                {
                    Data.Add(token);
                }
            }
            catch
            {
                
            }
        }

        public static List<string> Data = new List<string>();
    }
}
