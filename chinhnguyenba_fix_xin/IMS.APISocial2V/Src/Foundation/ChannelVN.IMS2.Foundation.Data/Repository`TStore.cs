﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ChannelVN.IMS2.Foundation.Data
{
    public abstract class Repository<TStore> : Repository where TStore : class, IStore, new()
    {
        public Repository()
        {
        }

        public TStore Store
        {
            get
            {
                return _store ?? (_store = Activator.CreateInstance<TStore>());
            }
        }

        private TStore _store = default(TStore);
    }
}
