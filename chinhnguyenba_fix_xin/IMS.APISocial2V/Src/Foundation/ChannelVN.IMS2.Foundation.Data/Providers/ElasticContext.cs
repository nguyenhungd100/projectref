﻿using ChannelVN.IMS2.Foundation.Data.Providers.ESClient;
using ChannelVN.IMS2.Foundation.Logging;
using Elasticsearch.Net;
using Nest;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace ChannelVN.IMS2.Foundation.Data.Providers
{
    public sealed class ElasticContext : ElasticClient, IContext, IESClient, IDisposable
    {
        #region Constructor

        public ElasticContext()
        {
        }
        public ElasticContext(ContextOptions options)
            : base(GetSettings(options)) => Options = options;

        #endregion

        #region Static supportation

        static ConnectionSettings GetSettings(ContextOptions options)
        {
            var connectString = (string)(options.GetValue("ConnectionString") ?? string.Empty);
            var requestTimeout = Int32.Parse((string)(options.GetValue("RequestTimeout") ?? "5")); //default => 5 seconds
            var nodes = new Uri[]
            {
                new Uri(connectString)
            };
            var pool = new StaticConnectionPool(nodes);
            return new ConnectionSettings(pool).RequestTimeout(TimeSpan.FromSeconds(requestTimeout));
        }
        #endregion

        #region Properties

        public string ConnectionString
        {
            get
            {
                return (string)(Options.GetValue("ConnectionString") ?? string.Empty);
            }
        }

        public char SeparateChar
        {
            get
            {
                return (char)(Options.GetValue("SeparateChar") ?? '_');
            }
        }

        public string Namespace
        {
            get
            {
                return ((string)(Options.GetValue("Namespace") ?? string.Empty)).Trim();
            }
            set
            {
                if (!Options.ContainsKey("Namespace"))
                {
                    Options.Add("Namespace", value);
                }
                else
                {
                    Options["Namespace"] = value;
                }
            }
        }
        #endregion

        #region Public methods

        public string NameOf(Object entity)
        {
            var name = "";
            if (entity != null)
            {
                if (String.IsNullOrEmpty(Namespace))
                    name = entity.GetType().Name.ToLower();
                else
                    name = $"{Namespace}{SeparateChar}{entity.GetType().Name}".ToLower();
            }
            return name;
        }

        public string NameOf(params string[] values)
        {
            var name = "";
            if (null != values)
            {
                var strValue = String.Join(SeparateChar, values);
                if (!String.IsNullOrEmpty(strValue))
                {
                    if (String.IsNullOrEmpty(Namespace))
                        name = strValue.ToLower();
                    else
                        name = $"{Namespace}{SeparateChar}{strValue}".ToLower();
                }
            }
            return name;
        }

        public byte[] GetBytes(string value)
        {
            if (String.IsNullOrEmpty(value))
            {
                return null;
            }
            return Encoding.ASCII.GetBytes(value);
        }
        #endregion

        #region IContext implementation

        public ContextOptions Options { get; set; }
        #endregion

        #region IDisposable implementation

        public void Dispose()
        {
            //throw new NotImplementedException();
        }
        #endregion

        #region IESClient implementation

        public bool Create<T>(T document) where T : class
        {
            try
            {
                string stringId = NameOf(typeof(T).Name);
                var index = Index(document, i => i.Index(stringId).Refresh(Elasticsearch.Net.Refresh.True));

                return index.IsValid;
            }
            catch
            {
                return false;
            }
        }
        public async Task<bool> CreateAsync<T>(T document) where T : class
        {
            try
            {
                string stringId = NameOf(typeof(T).Name);
                var index = await IndexAsync(document, i => i.Index(stringId).Refresh(Elasticsearch.Net.Refresh.True));

                return index.IsValid;
            }
            catch
            {
                return false;
            }
        }

        public bool Create<T>(IList<T> documents) where T : class
        {
            try
            {
                var stringId = NameOf(typeof(T).Name);

                var bulkIndexer = new BulkDescriptor();
                foreach (var document in documents)
                {
                    bulkIndexer.Index<T>(i => i
                        .Document(document)
                        .Index(stringId)
                        );
                }

                var index = Bulk(bulkIndexer.Refresh(Elasticsearch.Net.Refresh.True));
                return index.IsValid;
            }
            catch
            {
                return false;
            }
        }

        public async Task<bool> CreateManyAsync<T>(IList<T> documents) where T : class
        {
            try
            {
                var stringId = NameOf(typeof(T).Name);

                var bulkIndexer = new BulkDescriptor();
                bulkIndexer.IndexMany(documents, (d, doc) => d.Document(doc).Index(stringId));

                var index = await BulkAsync(bulkIndexer.Refresh(Elasticsearch.Net.Refresh.True));
                return index.IsValid;
            }
            catch
            {
                return false;
            }
        }

        #region Settings Index
        public bool CreateIndexSettings<T>(string fieldNameAnalyzer, IList<string> filterAnalyzer = null) where T : class
        {
            try
            {
                if (filterAnalyzer == null)
                {
                    filterAnalyzer = new List<string>()
                    {
                        "lowercase", "asciifolding"
                    };
                }
                var customAnlyzer = new CustomAnalyzer
                {
                    Tokenizer = "standard",
                    Filter = filterAnalyzer
                };

                var stringId = NameOf(typeof(T).Name);

                if (!IndexExists(stringId).Exists)
                {
                    var index = CreateIndex(stringId, i => i
                        .Settings(s => s
                            .Analysis(a => a
                                .Analyzers(al => al
                                    .UserDefined("analyzer_userdefind", customAnlyzer)
                                )
                            )
                            .Setting("index.max_result_window", 999999999)
                        )
                        .Mappings(ms => ms
                            .Map<T>(m => m
                                .AutoMap()
                                .Properties(p => p
                                    .Text(s => s
                                        .Name(fieldNameAnalyzer)
                                        .Fields(f => f
                                            .Text(ss => ss
                                                .Name("folded")
                                                .Analyzer("analyzer_userdefind")
                                            )
                                        )
                                    )
                                    .Text(s => s
                                        .Name("author")
                                        .Fields(f => f
                                            .Text(ss => ss
                                                .Name("folded")
                                                .Analyzer("analyzer_userdefind")
                                            )
                                        )
                                    )
                                )
                            )
                        )
                    );

                    return index.IsValid;
                }
                return false;
            }
            catch
            {
                return false;
            }
        }
        public async Task<bool> CreateIndexSettingsAsync<T>(string fieldNameAnalyzer, IList<string> filterAnalyzer = null) where T : class
        {
            try
            {
                if (filterAnalyzer == null)
                {
                    filterAnalyzer = new List<string>()
                    {
                        "lowercase", "asciifolding"
                    };
                }
                var customAnlyzer = new CustomAnalyzer
                {
                    Tokenizer = "standard",
                    Filter = filterAnalyzer
                };

                var stringId = NameOf(typeof(T).Name);

                if (!IndexExists(stringId).Exists)
                {
                    var index = await CreateIndexAsync(stringId, i => i
                         .Settings(s => s
                             .Analysis(a => a
                                 .Analyzers(al => al
                                     .UserDefined("analyzer_userdefind", customAnlyzer)
                                 )
                             )
                             .Setting("index.max_result_window", 999999999)
                         )
                         .Mappings(ms => ms
                             .Map<T>(m => m
                                 .AutoMap()
                                 //.SourceField(s=>s.Includes(new[] { "name"}))
                                 .Properties(p => p
                                     .Text(s => s
                                         .Name(fieldNameAnalyzer)
                                         .Fields(f => f
                                             .Text(ss => ss
                                                 .Name("folded")
                                                 .Analyzer("analyzer_userdefind")
                                             )
                                         )
                                     )
                                     .Text(s => s
                                         .Name("author")
                                         .Fields(f => f
                                             .Text(ss => ss
                                                 .Name("folded")
                                                 .Analyzer("analyzer_userdefind")
                                             )
                                         )
                                     )
                                 )
                             )
                         )
                    );

                    return index.IsValid;
                }
                return false;
            }
            catch
            {
                return false;
            }
        }
        public bool CreateIndexSettings<T>(string fieldNameAnalyzer, string fieldNameAnalyzer2, IList<string> filterAnalyzer = null) where T : class
        {
            try
            {
                if (filterAnalyzer == null)
                {
                    filterAnalyzer = new List<string>()
                    {
                        "lowercase", "asciifolding"
                    };
                }
                var customAnlyzer = new CustomAnalyzer
                {
                    Tokenizer = "standard",
                    Filter = filterAnalyzer
                };

                var stringId = NameOf(typeof(T).Name);

                if (!IndexExists(stringId).Exists)
                {
                    var index = CreateIndex(stringId, i => i
                        .Settings(s => s
                            .Analysis(a => a
                                .Analyzers(al => al
                                    .UserDefined("analyzer_userdefind", customAnlyzer)
                                )
                            )
                            .Setting("index.max_result_window", 999999999)
                        )
                        .Mappings(ms => ms
                            .Map<T>(m => m
                                .AutoMap()
                                .Properties(p => p
                                    .Text(s => s
                                        .Name(fieldNameAnalyzer)
                                        .Fields(f => f
                                            .Text(ss => ss
                                                .Name("folded")
                                                .Analyzer("analyzer_userdefind")
                                            )
                                        )
                                    )
                                    .Text(s => s
                                        .Name(fieldNameAnalyzer2)
                                        .Fields(f => f
                                            .Text(ss => ss
                                                .Name("folded")
                                                .Analyzer("analyzer_userdefind")
                                            )
                                        )
                                    )
                                )
                            )
                        )
                    );

                    return index.IsValid;
                }
                return true;
            }
            catch
            {
                return false;
            }
        }
        public bool CreateIndexSettings<T>(IList<string> listFieldNameAnalyzer, IList<string> filterAnalyzer = null) where T : class
        {
            try
            {
                if (filterAnalyzer == null)
                {
                    filterAnalyzer = new List<string>()
                    {
                        "lowercase", "asciifolding"
                    };
                }
                var customAnlyzer = new CustomAnalyzer
                {
                    Tokenizer = "standard",
                    Filter = filterAnalyzer
                };

                var stringId = NameOf(typeof(T).Name);

                var index = CreateIndex(stringId, i => i
                    .Settings(s => s
                        .Analysis(a => a
                            .Analyzers(al => al
                                .UserDefined("analyzer_userdefind", customAnlyzer)
                            )
                        )
                        .Setting("index.max_result_window", 999999999)
                    )
                    .Mappings(ms => ms
                        .Map<T>(m => m
                            .AutoMap()
                            .Properties(p => p
                                .Text(s => s
                                    .Name(listFieldNameAnalyzer[0])
                                    .Fields(f => f
                                        .Text(ss => ss
                                            .Name("folded")
                                            .Analyzer("analyzer_userdefind")
                                        )
                                    )
                                )
                                .Text(s => s
                                    .Name(listFieldNameAnalyzer[1])
                                    .Fields(f => f
                                        .Text(ss => ss
                                            .Name("folded")
                                            .Analyzer("analyzer_userdefind")
                                        )
                                    )
                                )
                            )
                        )
                    )
                );

                return index.IsValid;
            }
            catch
            {
                return false;
            }
        }
        #endregion

        public async Task<bool> UpdateAsync<T>(long id, dynamic updateDocument) where T : class
        {
            try
            {
                var stringId = NameOf(typeof(T).Name);
                var response = await UpdateAsync<T, dynamic>(id, u => u.Index(stringId).Doc(updateDocument).Refresh(Elasticsearch.Net.Refresh.True));

                return response.IsValid;
            }
            catch
            {
                return false;
            }
        }

        public async Task<bool> UpdateAsync<T>(string id, dynamic updateDocument) where T : class
        {
            try
            {
                var stringId = NameOf(typeof(T).Name);
                var response = await UpdateAsync<T, dynamic>(id, u => u.Index(stringId).Doc(updateDocument).Refresh(Elasticsearch.Net.Refresh.True));

                return response.IsValid;
            }
            catch
            {
                return false;
            }
        }
        public bool DeleteIndex(string nameIndex)
        {
            try
            {
                return DeleteIndex(NameOf(nameIndex));
            }
            catch
            {
                return false;
            }
        }

        public bool DeleteMapping<T>(T document) where T : class
        {
            try
            {
                var stringId = NameOf(typeof(T).Name);
                var response = Delete<T>(document, i => i.Index(stringId).Refresh(Elasticsearch.Net.Refresh.True));

                return response.IsValid;
            }
            catch
            {
                return false;
            }
        }

        public bool DeleteMapping<T>(string id) where T : class
        {
            try
            {
                var stringId = NameOf(typeof(T).Name);
                var response = Delete<T>(id, i => i.Index(stringId).Refresh(Elasticsearch.Net.Refresh.True));

                return response.IsValid;
            }
            catch
            {
                return false;
            }
        }
        public async Task<bool> DeleteMappingAsync<T>(string id) where T : class
        {
            try
            {
                var stringId = NameOf(typeof(T).Name);
                var response = await DeleteAsync<T>(id, i => i.Index(stringId).Refresh(Elasticsearch.Net.Refresh.True));

                return response.IsValid;
            }
            catch
            {
                return false;
            }
        }

        public bool DeleteMappingMany<T>(IList<T> documents) where T : class
        {
            try
            {
                var stringId = NameOf(typeof(T).Name);
                var response = this.DeleteMany(documents, stringId);

                return response.IsValid;
            }
            catch
            {
                return false;
            }
        }

        public bool DeleteMappingMany<T>(IList<string> listId) where T : class
        {
            try
            {
                var isValid = false;
                foreach (var id in listId)
                {
                    isValid |= DeleteMapping<T>(id);
                }

                return isValid;
            }
            catch
            {
                return false;
            }
        }

        public T Get<T>(T document) where T : class
        {
            try
            {
                var stringId = NameOf(typeof(T).Name);
                var response = Get<T>(document, i => i.Index(stringId).Refresh());
                if (response.IsValid)
                {
                    return response.Source;
                }
                return default(T);
            }
            catch
            {
                return default(T);
            }
        }
        public T Get<T>(long id) where T : class
        {
            try
            {
                var stringId = NameOf(typeof(T).Name);
                var response = Get<T>(id, i => i.Index(stringId).Refresh());
                if (response.IsValid)
                {
                    return response.Source;
                }
                return default(T);
            }
            catch
            {
                return default(T);
            }
        }
        public async Task<T> GetAsync<T>(long id) where T : class
        {
            try
            {
                var stringId = NameOf(typeof(T).Name);
                var response = await GetAsync<T>(id, i => i.Index(stringId).Refresh());
                if (response.IsValid)
                {
                    return response.Source;
                }
                return default(T);
            }
            catch
            {
                return default(T);
            }
        }
        public T Get<T>(string id) where T : class
        {
            try
            {
                var stringId = NameOf(typeof(T).Name);
                var response = Get<T>(id, i => i.Index(stringId).Refresh());
                if (response.IsValid)
                {
                    return response.Source;
                }
                return default(T);
            }
            catch
            {
                return default(T);
            }
        }
        public async Task<T> GetAsync<T>(string id) where T : class
        {
            try
            {
                var stringId = NameOf(typeof(T).Name);
                var response = await GetAsync<T>(id, i => i.Index(stringId).Refresh());
                if (response.IsValid)
                {
                    return response.Source;
                }
                return default(T);
            }
            catch
            {
                return default(T);
            }
        }
        public ESClientEntity.SearchResult<T> SearchEx<T>(SearchDescriptor<T> searchRequest) where T : class
        {
            try
            {
                var stringId = NameOf(typeof(T).Name);

                searchRequest.Index(stringId);

                var response = Search<T>(searchRequest);
                if (response.IsValid)
                {
                    var searchResult = new ESClientEntity.SearchResult<T>
                    {
                        Total = response.Total,
                        Page = 0,
                        Data = (List<T>)response.Documents,
                        ElapsedMilliseconds = response.Took
                    };

                    return searchResult;
                }
                return default(ESClientEntity.SearchResult<T>);
            }
            catch
            {
                return default(ESClientEntity.SearchResult<T>);
            }
        }
        public async Task<ESClientEntity.SearchResult<T>> SearchExAsync<T>(SearchDescriptor<T> searchRequest) where T : class
        {
            try
            {
                var stringId = NameOf(typeof(T).Name);

                searchRequest.Index(stringId);

                //Console.WriteLine("Start SearchAsync: " + DateTime.Now.ToString("HH:mm:ss.fff"));
                var response = await SearchAsync<T>(searchRequest);
                //Console.WriteLine("End SearchAsync: " + DateTime.Now.ToString("HH:mm:ss.fff"));
                if (response.IsValid)
                {
                    //Console.WriteLine("Start Parser: " + DateTime.Now.ToString("HH:mm:ss.fff"));
                    var searchResult = new ESClientEntity.SearchResult<T>
                    {
                        Total = response.Total,
                        Page = 0,
                        Data = (List<T>)response.Documents,
                        ElapsedMilliseconds = response.Took
                    };
                    //Console.WriteLine("End Parser: " + DateTime.Now.ToString("HH:mm:ss.fff"));
                    return searchResult;
                }
                return default(ESClientEntity.SearchResult<T>);
            }
            catch (Exception ex)
            {
                Logger.Error("SearchExAsync => " + ex.Message);
                return default(ESClientEntity.SearchResult<T>);
            }
        }
        public ESClientEntity.SearchResult<T> SearchEx<T>(string indexName, Nest.SearchDescriptor<T> searchRequest) where T : class
        {
            try
            {
                searchRequest.Index(indexName);

                var response = Search<T>(searchRequest);
                if (response.IsValid)
                {
                    var searchResult = new ESClientEntity.SearchResult<T>
                    {
                        Total = response.Total,
                        Page = 0,
                        Data = (List<T>)response.Documents,
                        ElapsedMilliseconds = response.Took
                    };

                    return searchResult;
                }

                return default(ESClientEntity.SearchResult<T>);
            }
            catch
            {
                return default(ESClientEntity.SearchResult<T>);
            }
        }

        public ESClientEntity.SearchResult<T> SearchEx<T>(string keyword, string[] fields) where T : class
        {
            try
            {
                var stringId = NameOf(typeof(T).Name);

                var searchRequest = new SearchDescriptor<T>().Index(stringId);

                searchRequest.Query(q => q
                    .QueryString(qs => qs
                        .Query("*" + keyword + "*")
                        .Fields(f => f.Fields(fields))
                        .DefaultOperator(Operator.Or)
                    )
                );

                var response = Search<T>(searchRequest);
                if (response.IsValid)
                {
                    var searchResult = new ESClientEntity.SearchResult<T>
                    {
                        Total = response.Total,
                        Page = 0,
                        Data = (List<T>)response.Documents,
                        ElapsedMilliseconds = response.Took
                    };

                    return searchResult;
                }

                return default(ESClientEntity.SearchResult<T>);
            }
            catch
            {
                return default(ESClientEntity.SearchResult<T>);
            }
        }

        public ESClientEntity.SearchResult<T> SearchEx<T>(string keyword, string[] fields, IDictionary<string, dynamic> sort, IDictionary<string, dynamic> term, IDictionary<string, dynamic> terms, DateTime from, DateTime to, string fieldDate) where T : class
        {
            try
            {
                string stringId = NameOf(typeof(T).Name);

                var searchRequest = new SearchDescriptor<T>().Index(stringId);

                if (sort != null && sort.Count > 0)
                {
                    var listKey = new List<string>(term.Keys);
                    foreach (var key in listKey)
                    {
                        if (key == "desc")
                        {
                            searchRequest.Sort(s => s.Descending(term[key]));
                        }
                        else if (key == "asc")
                        {
                            searchRequest.Sort(s => s.Ascending(term[key]));
                        }
                    }
                }

                var term1 = new TermQuery();
                var term2 = new TermQuery();
                var term3 = new TermQuery();
                var term4 = new TermQuery();
                var term5 = new TermQuery();
                var term6 = new TermQuery();
                var term7 = new TermQuery();
                var term8 = new TermQuery();
                var term9 = new TermQuery();
                var term10 = new TermQuery();

                if (term != null && term.Count > 0)
                {
                    var listKey = new List<string>(term.Keys);
                    var i = 1;
                    foreach (var key in listKey)
                    {
                        switch (i)
                        {
                            case 1: term1.Field = key; term1.Value = term[key]; break;
                            case 2: term2.Field = key; term2.Value = term[key]; break;
                            case 3: term3.Field = key; term3.Value = term[key]; break;
                            case 4: term4.Field = key; term4.Value = term[key]; break;
                            case 5: term5.Field = key; term5.Value = term[key]; break;
                            case 6: term6.Field = key; term6.Value = term[key]; break;
                            case 7: term7.Field = key; term7.Value = term[key]; break;
                            case 8: term8.Field = key; term8.Value = term[key]; break;
                            case 9: term9.Field = key; term9.Value = term[key]; break;
                            case 10: term10.Field = key; term10.Value = term[key]; break;
                        }
                        i++;
                    }
                }

                var terms1 = new TermsQuery();
                var terms2 = new TermsQuery();
                var terms3 = new TermsQuery();
                var terms4 = new TermsQuery();
                var terms5 = new TermsQuery();

                if (terms != null && terms.Count > 0)
                {
                    var listKey = new List<string>(terms.Keys);
                    var i = 1;
                    foreach (var key in listKey)
                    {
                        switch (i)
                        {
                            case 1: terms1.Field = key; terms1.Terms = term[key]; break;
                            case 2: terms2.Field = key; terms2.Terms = term[key]; break;
                            case 3: terms3.Field = key; terms3.Terms = term[key]; break;
                            case 4: terms4.Field = key; terms4.Terms = term[key]; break;
                            case 5: terms5.Field = key; terms5.Terms = term[key]; break;
                        }
                        i++;
                    }
                }

                var range = new DateRangeQuery();
                range.Field = fieldDate;

                if (from != DateTime.MinValue && to != DateTime.MinValue)
                {
                    range.GreaterThanOrEqualTo = from;
                    range.LessThanOrEqualTo = to;
                }
                else
                {
                    if (from != DateTime.MinValue)
                    {
                        range.GreaterThanOrEqualTo = from;
                    }
                    else if (to != DateTime.MinValue)
                    {
                        range.LessThanOrEqualTo = to;
                    }
                }

                searchRequest.Query(q => q
                    .QueryString(mp => mp
                        .Query("*" + keyword + "*")
                        .Fields(f => f.Fields(fields))
                        .DefaultOperator(Operator.Or)
                    ) && range && term1 && term2 && term3 && term4 && term5 && term6 && term7 && term8 && term8 && term10
                    && terms1 && terms2 && terms3 && terms4 && terms5
                );

                var response = Search<T>(searchRequest);

                if (response.IsValid)
                {
                    var searchResult = new ESClientEntity.SearchResult<T>
                    {
                        Total = response.Total,
                        Page = 0,
                        Data = (List<T>)response.Documents,
                        ElapsedMilliseconds = response.Took
                    };

                    return searchResult;
                }

                return default(ESClientEntity.SearchResult<T>);
            }
            catch
            {
                return default(ESClientEntity.SearchResult<T>);
            }
        }

        public ESClientEntity.SearchResult<T> SearchPaging<T>(string keyword, string[] fields, IDictionary<string, dynamic> sort, IDictionary<string, dynamic> term, IDictionary<string, dynamic> terms, DateTime from, DateTime to, string fieldDate, int page, int pageSize) where T : class
        {
            try
            {
                var stringId = NameOf(typeof(T).Name);

                var searchRequest = new SearchDescriptor<T>().Index(stringId);

                if (sort != null && sort.Count > 0)
                {
                    var listKey = new List<string>(sort.Keys);
                    foreach (var key in listKey)
                    {
                        if (key == "desc")
                        {
                            searchRequest.Sort(s => s.Descending(sort[key]));
                        }
                        else if (key == "asc")
                        {
                            searchRequest.Sort(s => s.Ascending(sort[key]));
                        }
                    }
                }

                var term1 = new TermQuery();
                var term2 = new TermQuery();
                var term3 = new TermQuery();
                var term4 = new TermQuery();
                var term5 = new TermQuery();
                var term6 = new TermQuery();
                var term7 = new TermQuery();
                var term8 = new TermQuery();
                var term9 = new TermQuery();
                var term10 = new TermQuery();

                if (term != null && term.Count > 0)
                {
                    var listKey = new List<string>(term.Keys);
                    var i = 1;
                    foreach (var key in listKey)
                    {
                        switch (i)
                        {
                            case 1: term1.Field = key; term1.Value = term[key]; break;
                            case 2: term2.Field = key; term2.Value = term[key]; break;
                            case 3: term3.Field = key; term3.Value = term[key]; break;
                            case 4: term4.Field = key; term4.Value = term[key]; break;
                            case 5: term5.Field = key; term5.Value = term[key]; break;
                            case 6: term6.Field = key; term6.Value = term[key]; break;
                            case 7: term7.Field = key; term7.Value = term[key]; break;
                            case 8: term8.Field = key; term8.Value = term[key]; break;
                            case 9: term9.Field = key; term9.Value = term[key]; break;
                            case 10: term10.Field = key; term10.Value = term[key]; break;
                        }
                        i++;
                    }
                }

                var terms1 = new TermsQuery();
                var terms2 = new TermsQuery();
                var terms3 = new TermsQuery();
                var terms4 = new TermsQuery();
                var terms5 = new TermsQuery();

                if (terms != null && terms.Count > 0)
                {
                    var listKey = new List<string>(terms.Keys);
                    var i = 1;
                    foreach (var key in listKey)
                    {
                        switch (i)
                        {
                            case 1: terms1.Field = key; terms1.Terms = terms[key]; break;
                            case 2: terms2.Field = key; terms2.Terms = terms[key]; break;
                            case 3: terms3.Field = key; terms3.Terms = terms[key]; break;
                            case 4: terms4.Field = key; terms4.Terms = terms[key]; break;
                            case 5: terms5.Field = key; terms5.Terms = terms[key]; break;
                        }
                        i++;
                    }
                }

                searchRequest.Query(q => term1 && term2 && term3 && term4 && term5 && term6 && term7 && term8 && term8 && term10
                    && terms1 && terms2 && terms3 && terms4 && terms5
                ).From((page - 1) * pageSize).Size(pageSize);

                var response = Search<T>(searchRequest);

                if (response.IsValid)
                {
                    var searchResult = new ESClientEntity.SearchResult<T>
                    {
                        Total = response.Total,
                        Page = page,
                        Data = (List<T>)response.Documents,
                        ElapsedMilliseconds = response.Took
                    };

                    return searchResult;
                }

                return default(ESClientEntity.SearchResult<T>);
            }
            catch
            {
                return default(ESClientEntity.SearchResult<T>);
            }
        }

        public ESClientEntity.SearchResult<T> SearchPaging<T>(string keyword, string[] fields, IDictionary<string, dynamic> sort, IDictionary<string, dynamic> term, int page, int pageSize) where T : class
        {
            try
            {
                var stringId = NameOf(typeof(T).Name);
                var searchRequest = new SearchDescriptor<T>().Index(stringId);

                if (sort != null && sort.Count > 0)
                {
                    var listKey = new List<string>(sort.Keys);
                    foreach (var key in listKey)
                    {
                        if (key == "desc")
                        {
                            searchRequest.Sort(s => s.Descending(sort[key]));
                        }
                        else if (key == "asc")
                        {
                            searchRequest.Sort(s => s.Ascending(sort[key]));
                        }
                    }
                }

                var term1 = new TermQuery();
                var term2 = new TermQuery();
                var term3 = new TermQuery();
                var term4 = new TermQuery();
                var term5 = new TermQuery();
                var term6 = new TermQuery();
                var term7 = new TermQuery();
                var term8 = new TermQuery();
                var term9 = new TermQuery();
                var term10 = new TermQuery();

                if (term != null && term.Count > 0)
                {
                    var listKey = new List<string>(term.Keys);
                    var i = 1;
                    foreach (var key in listKey)
                    {
                        switch (i)
                        {
                            case 1: term1.Field = key; term1.Value = term[key]; break;
                            case 2: term2.Field = key; term2.Value = term[key]; break;
                            case 3: term3.Field = key; term3.Value = term[key]; break;
                            case 4: term4.Field = key; term4.Value = term[key]; break;
                            case 5: term5.Field = key; term5.Value = term[key]; break;
                            case 6: term6.Field = key; term6.Value = term[key]; break;
                            case 7: term7.Field = key; term7.Value = term[key]; break;
                            case 8: term8.Field = key; term8.Value = term[key]; break;
                            case 9: term9.Field = key; term9.Value = term[key]; break;
                            case 10: term10.Field = key; term10.Value = term[key]; break;
                        }
                        i++;
                    }
                }

                searchRequest.Query(q => q
                    .QueryString(mp => mp
                        .Query("*" + keyword + "*")
                        .Fields(f => f.Fields(fields))
                        .DefaultOperator(Operator.Or)
                    ) && term1 && term2 && term3 && term4 && term5 && term6 && term7 && term8 && term8 && term10
                ).From((page - 1) * pageSize).Size(pageSize);

                var response = Search<T>(searchRequest);

                if (response.IsValid)
                {
                    var searchResult = new ESClientEntity.SearchResult<T>
                    {
                        Total = response.Total,
                        Page = page,
                        Data = (List<T>)response.Documents,
                        ElapsedMilliseconds = response.Took
                    };

                    return searchResult;
                }

                return default(ESClientEntity.SearchResult<T>);
            }
            catch
            {
                return default(ESClientEntity.SearchResult<T>);
            }
        }

        public ESClientEntity.SearchResult<T> SearchPaging<T>(string keyword, int status, string[] fields, int start, int rows, string statusField = "status", string descendingField = "created_date") where T : class
        {
            try
            {
                if (start <= 0) start = 1;

                var stringId = NameOf(typeof(T).Name);
                var searchRequest = new SearchDescriptor<T>().Index(stringId);

                if (status == -1)
                {
                    searchRequest.Query(q => q
                        .QueryString(qs => qs
                            .Query("*" + keyword + "*")
                            .Fields(f => f.Fields(fields))
                            .MinimumShouldMatch(MinimumShouldMatch.Percentage(100))
                            .DefaultOperator(Operator.And)
                        )
                    ).Skip(start - 1).Size(rows);
                }
                else
                {
                    searchRequest.Query(q => q
                        .QueryString(qs => qs
                            .Query("*" + keyword + "*")
                            .Fields(f => f.Fields(fields))
                            .MinimumShouldMatch(MinimumShouldMatch.Percentage(100))
                            .DefaultOperator(Operator.And)
                        ) && q
                        .Term(t => t
                            .Field(statusField)
                            .Value(status)
                        )
                    ).Skip(start - 1).Size(rows);
                }

                searchRequest.Sort(s => s.Descending(descendingField));

                var response = Search<T>(searchRequest);
                if (response.IsValid)
                {
                    var searchResult = new ESClientEntity.SearchResult<T>
                    {
                        Total = response.Total,
                        Page = start,
                        Data = (List<T>)response.Documents,
                        ElapsedMilliseconds = response.Took
                    };

                    return searchResult;
                }
                return default(ESClientEntity.SearchResult<T>);
            }
            catch
            {
                return default(ESClientEntity.SearchResult<T>);
            }
        }

        public ESClientEntity.SearchResult<T> SearchStatus<T>(string keyword, int status, string[] fields, int page, int pageSize, string statusField = "status") where T : class
        {
            try
            {
                var stringId = NameOf(typeof(T).Name);

                var searchRequest = new SearchDescriptor<T>().Index(stringId);
                searchRequest.Query(q => q
                    .MultiMatch(mp => mp
                        .Query(keyword)
                        .Fields(f => f.Fields(fields))
                        .Operator(Operator.Or)
                    ) && q
                    .Term(t => t
                        .Field(statusField)
                        .Value(status)
                    )
                ).From(page - 1).Size(pageSize);

                var response = Search<T>(searchRequest);

                if (response.IsValid)
                {
                    var searchResult = new ESClientEntity.SearchResult<T>
                    {
                        Total = response.Total,
                        Page = page,
                        Data = (List<T>)response.Documents,
                        ElapsedMilliseconds = response.Took
                    };

                    return searchResult;
                }
                return default(ESClientEntity.SearchResult<T>);
            }
            catch
            {
                return default(ESClientEntity.SearchResult<T>);
            }
        }

        public ESClientEntity.SearchResult<T> SearchPagingGroupBy<T>(string query, string[] fields, string groupBy, int page, int pageSize, string orderBy = null, string sortBy = null, string aggregationField = "by_groupBy") where T : class
        {
            try
            {
                var stringId = NameOf(typeof(T).Name);

                var searchRequest = new SearchDescriptor<T>().Index(stringId);

                if (orderBy != null)
                {
                    orderBy = orderBy.ToLower();
                    if (sortBy == null || (sortBy != null && sortBy.ToUpper() == "ASC"))
                        searchRequest.Sort(s => s.Ascending(orderBy));
                    if (sortBy != null && sortBy.ToUpper() == "DESC")
                        searchRequest.Sort(s => s.Descending(orderBy));
                }
                searchRequest.Query(q => q
                    .MultiMatch(mp => mp
                        .Query(query)
                        .Fields(f => f.Fields(fields))
                    )
                )
                .Aggregations(a => a
                    .Terms(aggregationField, t => t
                        .Field(groupBy)
                        .Size(int.MaxValue)
                    )
                )
                .From(page - 1).Size(pageSize);

                var response = Search<T>(searchRequest);

                if (response.IsValid)
                {
                    var itemsGroup = response.Aggregations.Terms(aggregationField);

                    var searchResult = new ESClientEntity.SearchResult<T>
                    {
                        Total = response.Total,
                        Page = page,
                        Data = response.Documents,
                        ElapsedMilliseconds = response.Took
                    };

                    return searchResult;
                }
                return default(ESClientEntity.SearchResult<T>);
            }
            catch
            {
                return default(ESClientEntity.SearchResult<T>);
            }
        }
        #endregion
    }
}