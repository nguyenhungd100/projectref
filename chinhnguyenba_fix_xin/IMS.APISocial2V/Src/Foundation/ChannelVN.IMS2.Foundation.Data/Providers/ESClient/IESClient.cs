﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ChannelVN.IMS2.Foundation.Data.Providers.ESClient
{
    public interface IESClient
    {
        #region Create

        bool Create<T>(T document) where T : class;
        Task<bool> CreateAsync<T>(T document) where T : class;
        Task<bool> CreateManyAsync<T>(IList<T> document) where T : class;
        bool CreateIndexSettings<T>(string fieldNameAnalyzer, IList<string> filterAnalyzer = null) where T : class;
        Task<bool> CreateIndexSettingsAsync<T>(string fieldNameAnalyzer, IList<string> filterAnalyzer = null) where T : class;
        #endregion

        #region Update

        Task<bool> UpdateAsync<T>(long id, dynamic updateDocument) where T : class;

        #endregion

        #region Delete

        bool DeleteIndex(string nameIndex);
        bool DeleteMapping<T>(T document) where T : class;
        bool DeleteMapping<T>(string id) where T : class;
        bool DeleteMappingMany<T>(IList<T> documents) where T : class;
        bool DeleteMappingMany<T>(IList<string> listId) where T : class;

        #endregion

        #region get a object

        T Get<T>(T document) where T : class;
        T Get<T>(string id) where T : class;
        Task<T> GetAsync<T>(string id) where T : class;
        T Get<T>(long id) where T : class;
        Task<T> GetAsync<T>(long id) where T : class;
        #endregion

        #region Search

        ESClientEntity.SearchResult<T> SearchEx<T>(Nest.SearchDescriptor<T> searchRequest) where T : class;
        Task<ESClientEntity.SearchResult<T>> SearchExAsync<T>(Nest.SearchDescriptor<T> searchRequest) where T : class;
        ESClientEntity.SearchResult<T> SearchEx<T>(string indexName, Nest.SearchDescriptor<T> searchRequest) where T : class;
        ESClientEntity.SearchResult<T> SearchEx<T>(string keyword, string[] fields) where T : class;
        ESClientEntity.SearchResult<T> SearchEx<T>(string keyword, string[] fields, IDictionary<string, dynamic> sort, IDictionary<string, dynamic> term, IDictionary<string, dynamic> terms, DateTime from, DateTime to, string fieldDate) where T : class;
        ESClientEntity.SearchResult<T> SearchPaging<T>(string keyword, string[] fields, IDictionary<string, dynamic> sort, IDictionary<string, dynamic> term, IDictionary<string, dynamic> terms, DateTime from, DateTime to, string fieldDate, int page, int pageSize) where T : class;
        ESClientEntity.SearchResult<T> SearchPaging<T>(string keyword, string[] fields, IDictionary<string, dynamic> sort, IDictionary<string, dynamic> term, int page, int pageSize) where T : class;
        ESClientEntity.SearchResult<T> SearchPaging<T>(string keyword, int status, string[] fields, int start, int rows, string statusField = "status", string descendingField = "created_date") where T : class;
        ESClientEntity.SearchResult<T> SearchStatus<T>(string keyword, int status, string[] fields, int page, int pageSize, string statusField = "status") where T : class;
        ESClientEntity.SearchResult<T> SearchPagingGroupBy<T>(string query, string[] fields, string groupBy, int page, int pageSize, string orderBy = null, string sortBy = null, string aggregationField = "by_groupBy") where T : class;

        #endregion
    }
}