﻿using StackExchange.Redis;
using System;
using System.Collections.Generic;
using System.Text;

namespace ChannelVN.IMS2.Foundation.Data.Providers
{
    public class CacheHelper
    {
        static CacheHelper()
        {
            lazyConnection = new Lazy<ConnectionMultiplexer>(() =>
            {
                return ConnectionMultiplexer.Connect("10.3.11.128:2011,password = fdsf34@4563gd53SFKKJgjs,defaultDatabase = 5"); //
            });
        }

        private static Lazy<ConnectionMultiplexer> lazyConnection;

        public static ConnectionMultiplexer Connection
        {
            get
            {
                return lazyConnection.Value;
            }
        }
    }
}
