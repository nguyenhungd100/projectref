﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ChannelVN.IMS2.Foundation.Data.Providers
{
    public sealed class SolrContext : Context
    {
        #region Constructor

        public SolrContext()
        {
        }

        public SolrContext(ContextOptions options)
            : base(options)
        {
        }
        #endregion
    }
}
