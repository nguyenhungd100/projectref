﻿using System;
using System.Collections.Generic;
using System.Reflection;

namespace ChannelVN.IMS2.Foundation.Data.Providers.SqlClient
{
    [Serializable]
    public class EntityMappingInfo
    {
        public EntityMappingInfo()
        {
            Properties = new Dictionary<string, PropertyInfo>();
            ColumnNames = new Dictionary<string, string>();
        }

        public string CacheByProperty { get; set; }
        public int CacheTimeOutMultiplier { get; set; }
        public IDictionary<string, string> ColumnNames { private set; get; }
        public string ObjectType { get; set; }
        public string PrimaryKey { get; set; }
        public IDictionary<string, PropertyInfo> Properties { private set; get; }
        public string TableName { get; set; }
    }
}
