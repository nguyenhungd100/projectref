﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ChannelVN.IMS2.Foundation.Data
{
    public class ContextOptions : Dictionary<string, object>
    {
        public ContextOptions()
        {
        }

        public object GetValue(string key)
        {
            if (TryGetValue(key, out object value))
            {
                return value;
            }
            return null;
        }

        public bool SetValue(string key, object value)
        {
            return TryAdd(key, value);
        }
    }
}
