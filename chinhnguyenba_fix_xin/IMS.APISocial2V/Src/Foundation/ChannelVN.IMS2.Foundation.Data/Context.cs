﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ChannelVN.IMS2.Foundation.Data
{
    public abstract class Context : IContext, IDisposable
    {
        // implement MySql/MsSql connector

        public Context()
            : this(new ContextOptions())
        {
        }

        public Context(ContextOptions options) => Options = options;

        public virtual void Dispose()
        {
            //throw new NotImplementedException();
        }

        /// <summary>
        /// Đối tượng quản lý tham số chung
        /// </summary>
        public ContextOptions Options { get; set; }
    }
}
