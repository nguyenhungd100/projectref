﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json.Schema;
using Newtonsoft.Json.Serialization;
using System;
using System.Linq;

namespace ChannelVN.IMS2.Foundation.Common
{
    public static class Json
    {
        public static string Stringify(object @object, string dateTimeFormat)
        {
            try
            {
                return JsonConvert.SerializeObject(@object, new IsoDateTimeConverter { DateTimeFormat = dateTimeFormat });
            }
            catch
            {
                return string.Empty;
            }
        }

        public static string Stringify(object @object)
        {
            try
            {
                return JsonConvert.SerializeObject(@object, MicrosoftDateFormatSettings);
            }
            catch
            {
                return string.Empty;
            }
        }

        public static string StringifyFirstLowercase(object @object)
        {
            try
            {
                return JsonConvert.SerializeObject(@object, MicrosoftDateFormatAndPropertyNameSettings);
            }
            catch
            {
                return string.Empty;
            }
        }

        public static T Parse<T>(string jsonString)
        {
            try
            {
                return JsonConvert.DeserializeObject<T>(jsonString, MicrosoftDateFormatSettings);
            }
            catch(Exception ex)
            {
                return default(T);
            }
        }

        public static T Parse<T>(string jsonString, string dateTimeFormat)
        {
            try
            {
                return JsonConvert.DeserializeObject<T>(jsonString, new IsoDateTimeConverter { DateTimeFormat = dateTimeFormat });
            }
            catch
            {
                return default(T);
            }
        }

        private static readonly JsonSerializerSettings MicrosoftDateFormatSettings = new JsonSerializerSettings
        {
            DateFormatHandling = DateFormatHandling.MicrosoftDateFormat
        };

        private static readonly JsonSerializerSettings MicrosoftDateFormatAndPropertyNameSettings = new JsonSerializerSettings
        {
            DateFormatHandling = DateFormatHandling.MicrosoftDateFormat,
            ContractResolver = new LowercaseContractResolver()
        };

        public static dynamic ConvertToObject(string content)
        {
            try
            {
                return JObject.Parse(content);
            }
            catch
            {
                return new JObject();
            }
        }

        public static dynamic ConvertToObject(object content)
        {
            try
            {
                return JObject.FromObject(content);
            }
            catch
            {
                return new JObject();
            }
        }

        public static dynamic ConvertToArray(string content)
        {
            try
            {
                return JArray.Parse(content);
            }
            catch
            {
                return new JArray();
            }
        }

        public static dynamic ConvertToArray(object content)
        {
            try
            {
                return JArray.FromObject(content);
            }
            catch
            {
                return new JArray();
            }
        }

        public static bool HasProperty(dynamic @object, string propertyName)
        {
            return (@object as JObject).ContainsKey(propertyName);
        }
    }

    public class LowercaseContractResolver : DefaultContractResolver
    {
        protected override string ResolvePropertyName(string propertyName)
        {
            return Char.ToLower(propertyName.First()) + propertyName.Substring(1);
        }
    }

    public static class JsonExtensions
    {
        public static bool TestValidate<T>(T obj, JSchema schema, SchemaValidationEventHandler handler = null, JsonSerializerSettings settings = null)
        {
            using (var writer = new NullJsonWriter())
            using (var validatingWriter = new JSchemaValidatingWriter(writer) { Schema = schema })
            {
                int count = 0;
                if (handler != null)
                    validatingWriter.ValidationEventHandler += handler;
                validatingWriter.ValidationEventHandler += (o, a) => count++;
                JsonSerializer.CreateDefault(settings).Serialize(validatingWriter, obj);
                return count == 0;
            }
        }
    }

    // Used to enable Json.NET to traverse an object hierarchy without actually writing any data.
    class NullJsonWriter : JsonWriter
    {
        public NullJsonWriter()
            : base()
        {
        }

        public override void Flush()
        {
            // Do nothing.
        }
    }
}
