﻿using System;

namespace ChannelVN.IMS2.Foundation.Common
{
    public class ActionResponse<TPayload>
    { 
        public bool Success { get; set; }
        public int ErrorCode { get; set; }
        public string Message { get; set; }
        public TPayload Data { get; set; }

        public ActionResponse()
        {
            Success = false;
            ErrorCode = 0;
            Message = "";
            Data = default(TPayload);
        }

        public static ActionResponse<TPayload> CreateErrorResponse(int errorCode, string message = "")
        {
            return new ActionResponse<TPayload>
            {
                Success = false,
                Message = (string.IsNullOrEmpty(message) ? "Error!" : message),
                ErrorCode = errorCode,
                Data = default(TPayload)
            };
        }

        public static ActionResponse<TPayload> CreateErrorResponse(int errorCode, TPayload data, string message = "")
        {
            return new ActionResponse<TPayload>
            {
                Success = false,
                Message = (string.IsNullOrEmpty(message) ? "Error!" : message),
                ErrorCode = errorCode,
                Data = data
            };
        }

        public static ActionResponse<TPayload> CreateErrorResponse(string message)
        {
            return CreateErrorResponse(9997, message);
        }

        public static ActionResponse<TPayload> CreateErrorResponse(Exception ex)
        {
            return CreateErrorResponse(9998, ex.Message);
        }

        public static ActionResponse<TPayload> CreateErrorResponseForInvalidRequest()
        {
            return CreateErrorResponse(9996, "Invalid request!");
        }

        public static ActionResponse<TPayload> CreateSuccessResponse(TPayload data, string message = "")
        {
            return new ActionResponse<TPayload>
            {
                Success = true,
                Message = string.IsNullOrEmpty(message) ? "Success!" : message,
                ErrorCode = 0,
                Data = data
            };
        }

        public static ActionResponse<TPayload> CreateSuccessResponse(string message)
        {
            return new ActionResponse<TPayload>
            {
                Success = true,
                Message = string.IsNullOrEmpty(message) ? "Success!" : message,
                ErrorCode = 0
            };
        }

        public static ActionResponse<TPayload> CreateSuccessResponse()
        {
            return CreateSuccessResponse("Success!");
        }
    }
}
