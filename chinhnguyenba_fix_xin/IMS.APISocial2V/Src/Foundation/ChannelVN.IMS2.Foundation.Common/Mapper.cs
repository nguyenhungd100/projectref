﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;

namespace ChannelVN.IMS2.Foundation.Common
{
    public static class Mapper<TypeOut>
    {
        public static TypeOut Map<TypeIn>(TypeIn objIn, TypeOut objOut)
        {
            try
            {
                if (objIn == null) return default(TypeOut);
                if (objOut == null) objOut = (TypeOut)Activator.CreateInstance(typeof(TypeOut));
                var tIn = typeof(TypeIn);
                var typeInProperties = tIn.GetProperties().ToDictionary(p => p.Name, p => p);

                var tOut = typeof(TypeOut);
                var typeOutProperties = tOut.GetProperties().ToDictionary(p => p.Name, p => p);

                var listName = typeInProperties.Select(s => s.Value.Name)?.ToList() ?? new List<string>();

                foreach (var p in typeOutProperties)
                {
                    if (listName.Contains(p.Value.Name))
                    {
                        try
                        {
                            p.Value.SetValue(
                            objOut,
                            objIn.GetType().GetProperty(p.Value.Name).GetValue(objIn, null));
                        }
                        catch (Exception) { }
                    }
                }
            }
            catch (Exception) { }
            return objOut;
        }

        public static TypeOut MapNewsAll<TypeIn>(TypeIn objIn, TypeOut objOut)
        {
            try
            {
                if (objOut == null) objOut = (TypeOut)Activator.CreateInstance(typeof(TypeOut));
                var tIn = typeof(TypeIn);
                var typeInProperties = tIn.GetProperties().ToDictionary(p => p.Name, p => p);

                var tOut = typeof(TypeOut);
                var typeOutProperties = tOut.GetProperties().ToDictionary(p => p.Name, p => p);

                var listName = typeInProperties.Select(s => s.Value.Name)?.ToList() ?? new List<string>();

                foreach (var p in typeOutProperties)
                {
                    if (p.Value.Name.Equals("Keyword"))
                    {
                        try
                        {
                            if (objIn.GetType().GetProperty("Caption") != null)
                            {
                                p.Value.SetValue(
                                    objOut,
                                    objIn.GetType().GetProperty("Caption").GetValue(objIn, null)
                                );
                            }
                            else
                            if (objIn.GetType().GetProperty("Name") != null)
                            {
                                p.Value.SetValue(
                                    objOut,
                                    objIn.GetType().GetProperty("Name").GetValue(objIn, null)
                                );
                            }
                            else
                            if (objIn.GetType().GetProperty("Title") != null)
                            {
                                p.Value.SetValue(
                                    objOut,
                                    objIn.GetType().GetProperty("Title").GetValue(objIn, null)
                                );
                            }
                        }
                        catch (Exception) { }
                    }
                    else
                    {
                        if (p.Value.Name.Equals("Avatar"))
                        {
                            try
                            {
                                if (objIn.GetType().GetProperty("Avatar") != null)
                                {
                                    p.Value.SetValue(
                                        objOut,
                                        objIn.GetType().GetProperty("Avatar").GetValue(objIn, null)
                                    );
                                }
                                else
                                {
                                    p.Value.SetValue(
                                        objOut,
                                        objIn.GetType().GetProperty("Avatar1").GetValue(objIn, null)
                                    );
                                }
                            }
                            catch (Exception) { }
                        }
                        else
                        {
                            if (listName.Contains(p.Value.Name))
                            {
                                try
                                {
                                    p.Value.SetValue(
                                    objOut,
                                    objIn.GetType().GetProperty(p.Value.Name).GetValue(objIn, null));
                                }
                                catch (Exception) { }
                            }
                        }
                    }
                }
            }
            catch (Exception) { }
            return objOut;
        }
    }
}
