﻿using System.Text.RegularExpressions;

namespace ChannelVN.IMS2.Foundation.Common
{
    public static class EscapeQueryBase
    {
        public static string EscapeSearchQuery(string query)
        {
            if (string.IsNullOrWhiteSpace(query)) return query;

            var sb = Regex.Replace(query, @"([^\w\s])", " ");

            return sb.ToString();
        }
        public static string EscapeSearchQuery2(string query)
        {
            if (string.IsNullOrWhiteSpace(query)) return query;

            var sb = Regex.Replace(query, @"([^\w\s.@])", " ");

            return sb.ToString();
        }
    }

}
