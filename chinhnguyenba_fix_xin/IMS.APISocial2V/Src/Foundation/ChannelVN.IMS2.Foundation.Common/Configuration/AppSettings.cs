﻿namespace ChannelVN.IMS2.Foundation.Common.Configuration
{
    public sealed class AppSettings
    {
        public string AllowedHosts { get; set; }
        public Application Application { get; set; }
        public Server Server { get; set; }
        public MailServer MailServer { get; set; }

        public ServiceConfiguration ServiceConfiguration { get; set; }
        public ChannelConfiguration ChannelConfiguration { get; set; }

        public static AppSettings Current { get; set; }

        public Distribution Distribution { get; set; }

        public JobAutoPushQueue JobAutoPushQueue { get; set; }

        public TelegramNotification TelegramNotification { get; set; }        

        public JobAutoNewsCrawlerSetting JobAutoNewsCrawlerSetting { get; set; }

        public JobWarningLateApproveSetting JobWarningLateApproveSetting { get; set; }

        public bool EnableTracing { get; set; }

        public AppSettings()
        {
            Current = this;
        }
    }

    public sealed class JobWarningLateApproveSetting 
    {
        public bool Enabled { get; set; }
        public int TimeInterval { get; set; }
        public int TimeSpacingSearch { get; set; }
        public TimeLateDetermine[] TimeLateDetermine { get; set; }
    }

    public sealed class TimeLateDetermine
    {
        public int Level { get; set; }
        public int TimeDelay { get; set; }
    }

    public sealed class JobAutoNewsCrawlerSetting
    {
        public bool Enabled { get; set; }
        public int TimeInterval { get; set; }
        public int TimeSpacingSeachConfig { get; set; }
    }    

    public sealed class TelegramNotification
    {
        public bool Enabled { get; set; }
        public string RootApiUrl { get; set; }
        public string ApiKey { get; set; }
        public string BotToken { get; set; }
        public string BotInGroup { get; set; }
    }

    public sealed class JobAutoPushQueue
    {
        public bool Enabled { get; set; }
        public int TimeInterval { get; set; }
    }

    public sealed class Distribution
    {
        public Automation Automation { get; set; }        
    }

    public sealed class Automation
    {
        public bool Enabled { get; set; }
        public long DistributorId { get; set; }       
    }

    public sealed class Server
    {
        public string IPAddress { get; set; }
        public int Port { get; set; }
    }

    public sealed class MailServer
    {
        public string ServerMail { get; set; }
        public int Port { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public string From { get; set; }
    }

    public sealed class Application
    {
        public string Id { get; set; }
        public string Name { get; set; }
    }

    public sealed class VietID
    {
        public string Host { get; set; }
        public string ClientID { get; set; }
        public string ClientSecret { get; set; }
    }
}
