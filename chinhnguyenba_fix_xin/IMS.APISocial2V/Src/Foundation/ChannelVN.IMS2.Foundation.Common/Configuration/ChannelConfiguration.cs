﻿using System.Collections.Generic;

namespace ChannelVN.IMS2.Foundation.Common.Configuration
{
    public sealed class ChannelConfiguration
    {
        public string SecretKey { get; set; }
        public string ApiSystemKey { get; set; }
        public bool RemoveHtmlAndScriptTag { get; set; }
        public int OtpLevel { get; set; }
        public ConnectionStrings ConnectionStrings { get; set; }
        public LogActionSettings LogActionSettings { get; set; }
        public NewsVersionSettings NewsVersionSettings { get; set; }
        public ContentDeliverySettings ContentDeliverySettings { get; set; }
        public CommonSettings CommonSettings { get; set; }
        public string DateFormat { get; set; }
        public int HttpClientTimeOut { get; set; }
        public string BirthdayFormat { get; set; }
        public Dictionary<string, Consumer> Consumers { get; set; }
        public KingHubVideoSettings KingHubVideoSettings { get; set; }
        public ViewVideoSettings ViewVideoSettings { get; set; }
        public string VideoDomain { get; set; }
        public KingHubBoardApiSetting KingHubBoardApiSetting { get; set; }

        public EditorApiSetting EditorApiSetting { get; set; }
        public LoginSetting LoginSetting { get; set; }
        public AuthenKingHubSetting AuthenKingHubSetting { get; set; }
        public int ExpireIndexTemp { get; set; }

        public string ApiUpdateUserId { get; set; }
        public string ApiInitData { get; set; }
        public ApiUserKingHub ApiUserKingHub { get; set; }
        public ApiCommentSetting ApiCommentSetting { get; set; }
        public ApiInteractionsStatisticsSetting ApiInteractionsStatisticsSetting { get; set; }
        //public string ApiPushGallery { get; set; }
        public bool AutoApprovedPage { get; set; }
        public string SecretKeyFilename { get; set; }
        public string UrlUpdateInfoCrm { get; set; }
        public LocalSettings LocalSettings { get; set; }
        public string ApiNewsCrawlerUrl { get; set; }        

        public RemoveXSSConfig RemoveXSSConfig { get; set; }

        public AuthenVietIDSettings AuthenVietIDSettings { get; set; }
        public AddUserKingHubSetting AddUserKingHubSetting { get; set; }

        public ApiSetNewsOnHomeSetting ApiSetNewsOnHomeSetting { get; set; }
        public BizflyConfig BizflyConfig { get; set; }
        public ApiPlatformConfig ApiPlatformConfig { get; set; }

        public KingHubPostApiSetting KingHubPostApiSetting { get; set; }
        public TelegramNotifySetting TelegramNotifySetting { get; set; }
        public NotificationPostApiSetting NotificationPostApiSetting { get; set; }
    }

    public sealed class KingHubPostApiSetting
    {
        public string ApiUrl { get; set; }
    }

    public sealed class NotificationPostApiSetting
    {
        public string ApiUrl { get; set; }       
    }

    public sealed class TelegramNotifySetting
    {
        public string ApiUrl { get; set; }
        public string Seckey { get; set; }
    }

    public sealed class ApiSetNewsOnHomeSetting
    {
        public string ApiUrl { get; set; }
    }

    public sealed class AddUserKingHubSetting
    {
        public string ApiUrl { get; set; }       
    }

    public sealed class AuthenVietIDSettings
    {
        public string ApiUrl { get; set; }
        public string ClientID { get; set; }
        public string ClientSecret { get; set; }
        public string Pin { get; set; }
    }

    public sealed class EditorApiSetting
    {
        public string ApiUrl { get; set; }        
    }
    public sealed class LoginSetting
    {
        public string ApiUrl { get; set; }
    }
    public sealed class AuthenKingHubSetting
    {
        public string ApiUrl { get; set; }
    }
    public sealed class ApiUserKingHub
    {
        public string UrlCreate { get; set; }
        public string UrlUpdate { get; set; }
        public string UrlUpdateRole { get; set; }
        public string UrlSearchUser { get; set; }
        public string UrlGetUserInfo { get; set; }
        public string UrlMapProfile { get; set; }
        public string SecretkeyCMS { get; set; }
        public int Mode { get; set; }
        public int RoleVip { get; set; }
        public int RoleNormal { get; set; }
        public string TokenUpateRole { get; set; }
        public string TokenGenUser { get; set; }
    }
    public sealed class ConnectionStrings
    {
        public string CmsMainDb { get; set; }
        public string CmsMainCacheDb { get; set; }
        public string CmsMainSearchDb { get; set; }
        public string CmsQueueDb { get; set; }
    }

    public sealed class LogActionSettings
    {
        public int ApiEnabled { get; set; }
        public string ApiUrl { get; set; }
        public string ApiChannel { get; set; }
        public string ApiSecretKey { get; set; }
    }

    public sealed class NewsVersionSettings
    {
        public int ApiEnabled { get; set; }
        public string ApiUrl { get; set; }
        public string ApiChannel { get; set; }
        public string ApiSecretKey { get; set; }
    }

    public sealed class ContentDeliverySettings
    {
        public int AllowPush { get; set; }
        public string Url { get; set; }
    }

    public sealed class CommonSettings
    {
        public string UrlFormatNews { get; set; }
        public string UrlFormatTag { get; set; }
        public string UrlFormatPlayList { get; set; }
        public string UrlFormatVideo { get; set; }
        public string UrlFormatMediaUnit { get; set; }
        public string UrlFormatPhoto { get; set; }
    }

    public sealed class Consumer
    {
        public long? ConsumerId { get; set; }
        public int Type { get; set; }
    }

    public sealed class KingHubVideoSettings
    {
        public string Appkey { get; set; }
        public string Secretkey { get; set; }
        public string PostAccessTokenUrl { get; set; }
        public string GetListRolesUrl { get; set; }
        public string SetUserRoleUrl { get; set; }
    }

    public sealed class ViewVideoSettings
    {
        public string AllDate { get; set; }
        public string ByDate { get; set; }
        public string TopVideo { get; set; }
        public int Mode { get; set; }
    }
    public sealed class KingHubBoardApiSetting
    {
        public string ApiUrl { get; set; }
    }
    public sealed class ApiCommentSetting
    {
        public string UrlGetCommentByPostId { get; set; }
        public string UrlDeleteComment { get; set; }
        public string UrlRestoreComment { get; set; }
        public string UrlUpdateComment { get; set; }
        public string UrlGetDetailComment { get; set; }
        public string UrlSearchComment { get; set; }
        public string UrlSettingMode { get; set; }
        public string UrlApproveComment { get; set; }
        public string UrlReplyComment { get; set; }
        public string UrlBlockSearch { get; set; }
        public string UrlClassifySearch { get; set; }
        public string UrlTopMemberSearch { get; set; }
        public string UrlMemberSearch { get; set; }
        public string UrlBlockUpdate { get; set; }
        public string UrlClassifyUpdate { get; set; }
        public string UrlTopMemberUpdate { get; set; }
       
        public string UrlGetCount { get; set; }
        public Mode Mode { get; set; }
    }

    public sealed class Mode
    {
        public int Disable { get; set; }
        public int PreCheck { get; set; }
        public int PostCheck { get; set; }
    }
    
    public sealed class ApiInteractionsStatisticsSetting
    {
        public string UrlGetFollowerOnPage { get; set; }
        public string UrlGetInteractions { get; set; }
        public string UrlGetPostCount { get; set; }
        public string UrlGetRepostCount { get; set; }
        public string UrlGetPostCountOfAllPostOfPage { get; set; }
        public string UrlGetRepostCountOfAllPostOfPage { get; set; }
        public ApiInfo TopPostOfPage { get; set; }
        public ApiInfo TopReachOfPostOfPage { get; set; }
        public ApiInfo CardInfo { get; set; }
        public ApiInfo UserOverview { get; set; }
        public ApiInfo OverViewAction { get; set; }
        public ApiInfo ViewProfile { get; set; }
        public ApiInfo ViewProfileByAge { get; set; }
        public ApiInfo ViewProfileByGender { get; set; }
        public ApiInfo ViewProfileByDevice { get; set; }
        public ApiInfo ViewProfileByLocation { get; set; }
        public ApiInfo Follow { get; set; }
    }

    public sealed class ApiInfo
    {
        public string Url { get; set; }
        public string UrlIp { get; set; }
        public string ApiKey { get; set; }
    }

    public sealed class LocalSettings
    {
        public bool? Enable { get; set; }
        public long[] WhiteListPage { get; set; }
    }

    public sealed class RemoveXSSConfig
    {
        public string[] WhiteListDomain { get; set; }
        public SpecialTag[] SpecialPropertyValidate { get; set; }

        public string AllowAttributes { get; set; }
        public string Allowtags { get; set; }
        public string[] RemoveAttributes { get; set; }
        public string[] RemoveTags { get; set; }
    }
    public sealed class SpecialTag
    {
        public string Name { get; set; }
        public string Type { get; set; }
    }

    public sealed class BizflyConfig
    {
        public string UrlActive { get; set; }
        public string UrlAddUserToPage { get; set; }
        public string UrlDeactive { get; set; }
        public string UrlCheckActive { get; set; }
        public string UrlGetPageOfUser { get; set; }
        public int AppID { get; set; }
        public string ApiKey { get; set; }
    }

    public sealed class ApiPlatformConfig
    {
        public string GetFollower { get; set; }
    }
}

