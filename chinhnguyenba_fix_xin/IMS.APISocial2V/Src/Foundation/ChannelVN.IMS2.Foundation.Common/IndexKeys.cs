﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ChannelVN.IMS2.Foundation.Common
{
    public class IndexKeys
    {
        private const string Prefix = "index";
        private const string _SeparateChar = ":";
        public static string KeyCreatedBy<T>(string createdBy) => Prefix + _SeparateChar + typeof(T).Name.ToLower() + _SeparateChar + "createdby"+ _SeparateChar + createdBy;
        public static string KeyStatus<T>(int? status) => Prefix + _SeparateChar + typeof(T).Name.ToLower() + _SeparateChar+ "status"+ _SeparateChar + status;
        public static string KeyDistributorid<T>(long? distributorid) => Prefix + _SeparateChar + typeof(T).Name.ToLower() + _SeparateChar + "distributorid"+ _SeparateChar + distributorid;
        public static string KeyDistributorid<T>() => Prefix + _SeparateChar + typeof(T).Name.ToLower() + _SeparateChar + "distributorid";
        public static string KeyType<T>(int type) => Prefix + _SeparateChar + typeof(T).Name.ToLower() + _SeparateChar + "type" + _SeparateChar + type;
        public static string KeyAccountInNews(long newsId) => Prefix + _SeparateChar + "newsinaccount" + _SeparateChar + "newsid" + _SeparateChar + newsId;
        public static string KeyNewsInAccount(long? accountId) => Prefix + _SeparateChar + "newsinaccount" + _SeparateChar + "accountid" + _SeparateChar + accountId;
        public static string KeyNewsDistribution(long newsId) => Prefix + _SeparateChar + "newsdistribution" + _SeparateChar + "newsid" + _SeparateChar + newsId;
        public static string KeyArticleInBeam(long beamId) => Prefix + _SeparateChar + "articleinbeam" + _SeparateChar + "beamid" + _SeparateChar + beamId;
        public static string KeyNewsInBoard(long boardId) => Prefix + _SeparateChar + "newsinboard"+ _SeparateChar + "boardid" + _SeparateChar + boardId;
        public static string KeyPublishedDate<T>() => Prefix + _SeparateChar + typeof(T).Name.ToLower() + _SeparateChar + "publisheddate";
        public static string KeySharedDate<T>() => Prefix + _SeparateChar + typeof(T).Name.ToLower() + _SeparateChar + "shareddate";
        public static string KeyCreatedDate<T>() => Prefix + _SeparateChar + typeof(T).Name.ToLower() + _SeparateChar + "createddate";

        public static string KeyOfficerOfAccount<T>(long? accountId) => Prefix + _SeparateChar + typeof(T).Name.ToLower()+ _SeparateChar + "officer" + _SeparateChar + "memberid" + _SeparateChar + accountId;
        public static string KeyAccountMemberInOfficer<T>(long? officerId) => Prefix + _SeparateChar + typeof(T).Name.ToLower() + _SeparateChar + "member" + _SeparateChar + "officerid" + _SeparateChar + officerId;

        //page sở hữu
        public static string KeyOwnerOfficer(long ownerId) => Prefix + _SeparateChar +"officer" + _SeparateChar + "ownerid"  + _SeparateChar + ownerId;

        //key news chờ approve trong 1 officer
        public static string KeyApprove<T>(long? officerId) => Prefix + _SeparateChar + typeof(T).Name.ToLower() + _SeparateChar + "approve" + _SeparateChar + "officerid" + _SeparateChar + officerId;

        //key news chờ 1 user approve trong 1 officer
        public static string KeyApproveInAccount<T>(long? officerId, long? userId) => Prefix + _SeparateChar + typeof(T).Name.ToLower() + _SeparateChar + "approve" + _SeparateChar + "officerid" + _SeparateChar + officerId  +_SeparateChar + "approvedby" + _SeparateChar + userId;

        //key news da approve cua 1 user trong 1 officer
        public static string KeyApprovedInAccount<T>(long? officerId, long? userId) => Prefix + _SeparateChar + typeof(T).Name.ToLower() + _SeparateChar + "approved" + _SeparateChar + "officerid" + _SeparateChar + officerId + _SeparateChar + "approvedby" + _SeparateChar + userId;

        public static string KeyPhotoInAlbum<T>(long? albumId) => Prefix + _SeparateChar + typeof(T).Name.ToLower() + _SeparateChar + "albumid" + _SeparateChar + albumId;
        public static string KeyVideoInPlaylist<T>(long? playlistId) => Prefix + _SeparateChar + typeof(T).Name.ToLower() + _SeparateChar + "playlistid" + _SeparateChar + playlistId;

        public static string KeyPhoneNumberInSytem<T>() => Prefix + _SeparateChar + typeof(T).Name.ToLower() + _SeparateChar + "phonenumbers";
        public static string KeyEmailInSytem<T>() => Prefix + _SeparateChar + typeof(T).Name.ToLower() + _SeparateChar + "emails";
        public static string BuildKey(params string[] values)
        {
            return string.Join(SeparateChar, values);
        }

        public static string SeparateChar => _SeparateChar;
    }
}
