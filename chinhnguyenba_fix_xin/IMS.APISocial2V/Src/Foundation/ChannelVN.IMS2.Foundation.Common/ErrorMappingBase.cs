﻿using System.Collections;

namespace ChannelVN.IMS2.Foundation.Common
{
    public abstract class ErrorMappingBase<TCode> : DictionaryBase
    {
        public string this[TCode code]
        {
            get
            {
                return InnerHashtable.ContainsKey(code)
                    ? InnerHashtable[code].ToString()
                    : "Không tìm thấy thông tin ứng với mã lỗi";
            }
        }

        protected ErrorMappingBase()
        {
            #region General errors

            InnerHashtable[ErrorCodeBase.Success] = "Xử lý thành công";
            InnerHashtable[ErrorCodeBase.UnknowError] = "Lỗi không xác định";
            InnerHashtable[ErrorCodeBase.Exception] = "Lỗi trong quá trình xử lý";
            InnerHashtable[ErrorCodeBase.BusinessError] = "Lỗi nghiệp vụ";
            InnerHashtable[ErrorCodeBase.InvalidRequest] = "Yêu cầu không hợp lệ";
            InnerHashtable[ErrorCodeBase.TimeoutSession] = "Phiên làm việc của bạn đã hết.";
            InnerHashtable[ErrorCodeBase.DuplicateError] = "Đã tồi tại";

            #endregion

            InitErrorMapping();
        }

        public enum ErrorCodeBase
        {
            Success = 0,
            UnknowError = 9999,
            Exception = 9998,
            BusinessError = 9997,
            InvalidRequest = 9996,
            TimeoutSession = -100,
            DuplicateError = 1985,
            UpdateError = 9995,
            AddError = 9994
        }

        protected abstract void InitErrorMapping();
    }
}
