﻿using ChannelVN.IMS2.Foundation.Common.Configuration;
using Ganss.XSS;
using HtmlAgilityPack;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Data;
using System.Dynamic;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Text.RegularExpressions;
using System.Web;

namespace ChannelVN.IMS2.Foundation.Common
{
    public static class Utility
    {
        const string UniChars = "àáảãạâầấẩẫậăằắẳẵặèéẻẽẹêềếểễệđìíỉĩịòóỏõọôồốổỗộơờớởỡợùúủũụưừứửữựỳýỷỹỵÀÁẢÃẠÂẦẤẨẪẬĂẰẮẲẴẶÈÉẺẼẸÊỀẾỂỄỆĐÌÍỈĨỊÒÓỎÕỌÔỒỐỔỖỘƠỜỚỞỠỢÙÚỦŨỤƯỪỨỬỮỰỲÝỶỸỴÂĂĐÔƠƯ";
        const string UnsignedChars = "aaaaaaaaaaaaaaaaaeeeeeeeeeeediiiiiooooooooooooooooouuuuuuuuuuuyyyyyAAAAAAAAAAAAAAAAAEEEEEEEEEEEDIIIIIOOOOOOOOOOOOOOOOOUUUUUUUUUUUYYYYYAADOOU";

        public static string UnicodeToUnsignedAndDash(string s)
        {
            try
            {
                const string strChar = "abcdefghijklmnopqrstxyzuvxw0123456789/- ";
                s = UnicodeToUnsigned(s.ToLower().Trim());
                var sReturn = "";
                for (int i = 0; i < s.Length; i++)
                {
                    if (strChar.IndexOf(s[i]) > -1)
                    {
                        if (s[i] != ' ')
                            sReturn += s[i];
                        else if (i > 0 && s[i - 1] != ' ' && s[i - 1] != '-')
                            sReturn += "-";
                    }
                }
                return sReturn.Replace("--", "-").Replace("/", "-");
            }
            catch
            {
                return string.Empty;
            }
        }

        public static string UnicodeToUnsigned(string s)
        {
            var retVal = "";
            for (int i = 0; i < s.Length; i++)
            {
                var pos = UniChars.IndexOf(s[i].ToString());
                if (pos >= 0)
                    retVal += UnsignedChars[pos];
                else
                    retVal += s[i];
            }
            return retVal;
        }

        public static bool PropertyExists(dynamic obj, string name)
        {
            if (obj == null) return false;
            if (obj is IDictionary<string, Newtonsoft.Json.Linq.JToken> jdict)
            {
                return jdict.ContainsKey(name);
            }
            if (obj is IDictionary<string, object> odict)
            {
                return odict.ContainsKey(name);
            }
            return obj.GetType().GetProperty(name) != null;
        }

        public static bool IsValidEmail(string email)
        {
            if (string.IsNullOrEmpty(email))
            {
                return false;
            }
            const string pattern = "^([0-9a-zA-Z]([-.\\w]*[0-9a-zA-Z])*@([0-9a-zA-Z][-\\w]*[0-9a-zA-Z]\\.)+[a-zA-Z]{2,9})$";
            return Regex.IsMatch(email, pattern);
        }

        public static string RemoveStrHtmlTags(this string inputString)
        {
            if (!string.IsNullOrEmpty(inputString))
            {
                inputString = inputString.Trim();
                if (inputString != "")
                {
                    inputString = Regex.Replace(inputString, @"<(.|\n)*?>", string.Empty);
                }
            }
            return inputString;
        }


        public static string ReplaceSpecialCharater(string inputString)
        {
            inputString = inputString.Trim();
            inputString = inputString.Replace(@"\", @"\\");
            inputString = inputString.Replace("\"", "&quot;");
            inputString = inputString.Replace("“", "&ldquo;");
            inputString = inputString.Replace("”", "&rdquo;");
            inputString = inputString.Replace("‘", "&lsquo;");
            inputString = inputString.Replace("’", "&rsquo;");
            inputString = inputString.Replace("'", "&#39;");
            return inputString;
        }

        public static string WrapString(string inputString, int length)
        {
            var str = string.Empty;
            var startIndex = 0;
            for (var i = (int)Math.Ceiling(inputString.Length / ((double)length)); (startIndex < inputString.Length) && (i > 1); i--)
            {
                str = str + inputString.Substring(startIndex, length) + ' ';
                startIndex += length;
            }
            return (str + inputString.Substring(startIndex));
        }

        public static double DateDiff(DateTime dateFrom, DateTime dateTo, string instant)
        {
            var diff = dateTo - dateFrom;
            double result = 0;
            switch (instant.ToLower())
            {
                case "d":
                    result = diff.TotalDays;
                    break;
                case "h":
                    result = diff.TotalHours;
                    break;
                case "m":
                    result = diff.TotalMinutes;
                    break;
                case "s":
                    result = diff.TotalSeconds;
                    break;
            }
            return result;
        }

        public static int CountWords(string stringInput)
        {
            if (string.IsNullOrEmpty(stringInput)) return 0;
            // Replaces all HTML tags
            stringInput = RemoveStrHtmlTags(stringInput);
            // Counts all words
            var collection = Regex.Matches(stringInput, @"[\S]+");
            // Returns number words.
            return collection.Count;
        }

        #region Convert Data

        public static string ConvertToString(object value)
        {
            return null == value ? string.Empty : value.ToString();
        }

        public static int ConvertToInt(object value)
        {
            int returnValue;
            if (null == value || !int.TryParse(value.ToString(), out returnValue))
            {
                returnValue = 0;
            }
            return returnValue;
        }

        public static long ConvertToLong(object value)
        {
            long returnValue;
            if (null == value || !long.TryParse(value.ToString(), out returnValue))
            {
                returnValue = 0;
            }
            return returnValue;
        }

        public static short ConvertToShort(object value)
        {
            short returnValue;
            if (null == value || !short.TryParse(value.ToString(), out returnValue))
            {
                returnValue = 0;
            }
            return returnValue;
        }

        public static decimal ConvertToDecimal(object value)
        {
            decimal returnValue;
            if (null == value || !decimal.TryParse(value.ToString(), out returnValue))
            {
                returnValue = 0;
            }
            return returnValue;
        }

        public static double ConvertToDouble(object value)
        {
            double returnValue;
            if (null == value || !double.TryParse(value.ToString(), out returnValue))
            {
                returnValue = 0;
            }
            return returnValue;
        }

        public static DateTime ConvertToDateTime(object value)
        {
            DateTime returnValue;

            if (null == value || !DateTime.TryParse(value.ToString(), out returnValue))
            {
                returnValue = DateTime.MinValue;
            }
            return returnValue;
        }

        public static DateTime? ConvertToDateTime(object value, string format)
        {
            var returnValue = default(DateTime?);
            if (null != value && DateTime.TryParseExact(value.ToString(), format, new CultureInfo("en-US"),DateTimeStyles.None, out DateTime dateTime))
            {
                returnValue = dateTime;
            }
            return returnValue;
        }

        public static DateTime ConvertToDateTimeDefaultDateNow(object value)
        {
            DateTime returnValue;

            if (null == value || !DateTime.TryParse(value.ToString(), out returnValue))
            {
                returnValue = DateTime.Now;
            }
            return returnValue;
        }

        public static TimeSpan ConvertToTimeSpan(object value)
        {
            TimeSpan timeSpan;
            if (null == value || !TimeSpan.TryParse(value.ToString(), out timeSpan))
            {
                timeSpan = TimeSpan.MinValue;
            }
            return timeSpan;

        }

        public static long ConvertToTimestamp(this DateTime dateTime)
        {
            return (long)(dateTime.ToUniversalTime().Subtract(new DateTime(1970, 1, 1))).TotalMilliseconds;
        }        

        public static long ConvertToTimestamp10(this DateTime dateTime)
        {
            var timeSpan = (dateTime - new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc));
            
            return (long)timeSpan.TotalSeconds;
        }

        public static bool ConvertToBoolean(object value)
        {
            bool returnValue;
            if (null == value || !bool.TryParse(value.ToString(), out returnValue))
            {
                returnValue = false;
            }
            return returnValue;
        }

        public static byte ConvertToByte(object value)
        {
            byte returnValue;
            if (null == value || !byte.TryParse(value.ToString(), out returnValue))
            {
                returnValue = 0;
            }
            return returnValue;
        }

        public static double DateTimeToUnixTimestamp(DateTime dateTime, long precision = TimeSpan.TicksPerSecond)
        {
            var unixEpoch = new DateTime(1970, 1, 1, 0, 0, 0, 0, DateTimeKind.Utc);
            var unixTimestampInTicks = (dateTime.ToUniversalTime() - unixEpoch).Ticks;
            return (double)unixTimestampInTicks / precision;
        }

        public static DateTime UnixTimestampToDateTime(double unixTime, long precision = TimeSpan.TicksPerSecond)
        {
            var unixEpoch = new DateTime(1970, 1, 1, 0, 0, 0, 0, DateTimeKind.Utc);
            var unixTimestampInTicks = (long)(unixTime * precision);
            return new DateTime(unixEpoch.Ticks + unixTimestampInTicks, DateTimeKind.Utc);
        }

        public static string Base64Decode(string encodedValue)
        {
            var encodedBytes = FromBase64String(encodedValue);
            return System.Text.Encoding.UTF8.GetString(encodedBytes);
        }

        private static byte[] FromBase64String(string input)
        {
            string working = input.Replace('-', '+').Replace('_', '/');
            while (working.Length % 3 != 0)
            {
                working += '=';
            }
            try
            {
                return Convert.FromBase64String(working);
            }
            catch
            {
                // .Net core 2.1 bug
                // https://github.com/dotnet/corefx/issues/30793
                try
                {
                    return Convert.FromBase64String(input.Replace('-', '+').Replace('_', '/'));
                }
                catch { }
                try
                {
                    return Convert.FromBase64String(input.Replace('-', '+').Replace('_', '/') + "=");
                }
                catch { }
                try
                {
                    return Convert.FromBase64String(input.Replace('-', '+').Replace('_', '/') + "==");
                }
                catch { }

                return null;
            }
        }

        public static IEnumerable<DateTime> EachDay(DateTime from, DateTime thru)
        {
            for (var day = from.Date; day.Date <= thru.Date; day = day.AddDays(1))
                yield return day;
        }

        public static DateTime? ConvertSecondsToDate(double? seconds, DateTimeKind kind = DateTimeKind.Unspecified)
        {
            if (seconds == null || seconds == 0) return null;
            DateTime start = new DateTime(1970, 1, 1, 0, 0, 0, kind);
            DateTime date = DateTime.SpecifyKind(start.AddSeconds(seconds ?? 0), kind);
            return date;
        }

        public static DateTime? ConvertSecondsUtcToDate(double? seconds, DateTimeKind kind = DateTimeKind.Unspecified)
        {
            if (seconds == null || seconds == 0) return null;
            DateTime start = new DateTime(1970, 1, 1, 0, 0, 0, kind);
            DateTime startUtc = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
            DateTime date = DateTime.SpecifyKind(start.AddSeconds((startUtc - start.ToUniversalTime()).TotalSeconds + seconds ?? 0), kind);
            return date;
        }

        public static bool ValidateStringIds(string strIds)
        {
            var returnValue = true;
            if (string.IsNullOrEmpty(strIds)) return returnValue;
            try
            {
                var arrId = strIds.Split(",");
                foreach(var id in arrId)
                {
                    if(!long.TryParse(id,out long result))
                    {
                        returnValue = false;
                    }
                }
            }catch(Exception) {
                returnValue = false;
            }
            return returnValue;
        }
        #endregion

        #region Object to KeyValuePair
        public static List<KeyValuePair<string, string>> ConvertToKeyValuePair(this object me)
        {
            List<KeyValuePair<string, string>> result = new List<KeyValuePair<string, string>>();
            try
            {
                foreach (var property in me.GetType().GetProperties())
                {
                    result.Add(new KeyValuePair<string, string>(Char.ToLowerInvariant(property.Name.First())+ property.Name.Substring(1), property.GetValue(me)?.ToString()));
                }
            }
            catch (Exception ex) {}
            return result;
        }

        public static List<KeyValuePair<string, string>> ConvertToKeyValuePairForGetMethod(this object me)
        {
            List<KeyValuePair<string, string>> result = new List<KeyValuePair<string, string>>();
            try
            {
                foreach (var property in me.GetType().GetProperties())
                {
                    if(!string.IsNullOrEmpty(property.GetValue(me)?.ToString()))
                    {
                        result.Add(new KeyValuePair<string, string>(Char.ToLowerInvariant(property.Name.First()) + property.Name.Substring(1), property.GetValue(me)?.ToString()));
                    }
                }
            }
            catch (Exception ex) { }
            return result;
        }

        public static string BuildHMAQueyString(this object me, string prefix)
        {
            var strResult = prefix;
            try
            {
                var index = 0;
                foreach (var property in me.GetType().GetProperties().OrderBy(p=>p.Name))
                {
                    if(index == 0)
                    {
                        strResult += property.Name.ToLower() + "=" + property.GetValue(me)?.ToString();
                    }
                    else
                    {
                        strResult += "&"+ property.Name.ToLower() + "=" + property.GetValue(me)?.ToString();
                    }
                    index++;
                }
            }
            catch (Exception ex) { }
            return strResult;
        }


        public static bool JSONEquals(this object obj, object another)
        {
            if (ReferenceEquals(obj, another)) return true;
            if ((obj == null) || (another == null)) return false;
            if (obj.GetType() != another.GetType()) return false;

            var objJson = Json.Stringify(obj);
            var anotherJson = Json.Stringify(another);

            return objJson == anotherJson;
        }
        #endregion

        #region Clonable

        public static object CloneObject(object objSource)
        {
            //Get the type of source object and create a new instance of that type
            var typeSource = objSource.GetType();
            var objTarget = Activator.CreateInstance(typeSource);

            //Get all the properties of source object type
            var propertyInfo = typeSource.GetProperties(BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance);

            //Assign all source property to taget object 's properties
            foreach (PropertyInfo property in propertyInfo)
            {
                //Check whether property can be written to
                if (property.CanWrite)
                {
                    //Check whether property type is value type, enum or string type
                    if (property.PropertyType.IsValueType || property.PropertyType.IsEnum || property.PropertyType.Equals(typeof(System.String)))
                    {
                        property.SetValue(objTarget, property.GetValue(objSource, null), null);
                    }
                    //else property type is object/complex types, so need to recursively call this method until the end of the tree is reached
                    else
                    {
                        var objPropertyValue = property.GetValue(objSource, null);
                        if (objPropertyValue == null)
                        {
                            property.SetValue(objTarget, null, null);
                        }
                        else
                        {
                            property.SetValue(objTarget, CloneObject(objPropertyValue), null);
                        }
                    }
                }
            }
            return objTarget;
        }
        #endregion

        public static string RemoveScriptTag(this string str)
        {
            if (!string.IsNullOrEmpty(str))
            {
                str = Regex.Replace(str.Trim(), @"<script[^<]*</script>", "", RegexOptions.IgnoreCase);
            }
            return str;
        }

        private static Regex _regex = new Regex(@"\\u(?<Value>[a-zA-Z0-9]{4})", RegexOptions.Compiled);
        public static string Decoder(this string value)
        {
            return _regex.Replace(
                value,
                m => ((char)int.Parse(m.Groups["Value"].Value, NumberStyles.HexNumber)).ToString()
            );
        }

        #region Xss
        public static string GetDomain(this string str)
        {
            try
            {
                var uri = new Uri(str);
                str = uri.Host;
                if (!string.IsNullOrEmpty(str)) str = str.Replace("http://", string.Empty).Replace("https://", string.Empty).Replace("www.", string.Empty).ToLower();
            }
            catch (Exception ex) { }

            return str ?? string.Empty;
        }

        private static string[] WhiteListDomain = AppSettings.Current.ChannelConfiguration.RemoveXSSConfig.WhiteListDomain;
        private static SpecialTag[] SpecialTagValidate = AppSettings.Current.ChannelConfiguration.RemoveXSSConfig.SpecialPropertyValidate;
        //private static string AllowAttributes = AppSettings.Current.ChannelConfiguration.RemoveXSSConfig.AllowAttributes;
        //private static string Allowtags = AppSettings.Current.ChannelConfiguration.RemoveXSSConfig.Allowtags;

        private static string[] RemovewAttributes = AppSettings.Current.ChannelConfiguration.RemoveXSSConfig.RemoveAttributes;
        private static string[] RemoveTags = AppSettings.Current.ChannelConfiguration.RemoveXSSConfig.RemoveTags;
        public static dynamic ClearObjectJson(dynamic input)
        {
            try
            {
                // Deserialize the input json string to an object
                //input = Newtonsoft.Json.JsonConvert.DeserializeObject(input);

                // Json Object could either contain an array or an object or just values
                // For the field names, navigate to the root or the first element
                //input = input.Root ?? input.First ?? input;

                if (input != null)
                {
                    // Get to the first element in the array
                    //bool isArray = true;

                    //while (isArray)
                    //{
                    //    input = input.First ?? input;

                    //    if (input.GetType() == typeof(Newtonsoft.Json.Linq.JObject) ||
                    //    input.GetType() == typeof(Newtonsoft.Json.Linq.JValue) ||
                    //    input == null)
                    //        isArray = false;
                    //}

                    // check if the object is of type JObject. 
                    // If yes, read the properties of that JObject
                    if (input.GetType() == typeof(Newtonsoft.Json.Linq.JObject))
                    {
                        // Create JObject from object
                        Newtonsoft.Json.Linq.JObject inputJson =
                            Newtonsoft.Json.Linq.JObject.FromObject(input);

                        // Read Properties
                        var properties = inputJson.Properties();

                        // Loop through all the properties of that JObject
                        foreach (var property in properties)
                        {
                            // Check if there are any sub-fields (nested)
                            // i.e. the value of any field is another JObject or another JArray
                            if (property.Value.GetType() == typeof(Newtonsoft.Json.Linq.JObject) || property.Value.GetType() == typeof(Newtonsoft.Json.Linq.JArray))
                            {
                                if (property.Value.GetType() == typeof(Newtonsoft.Json.Linq.JArray))
                                {
                                    var arr = new JArray();
                                    var first = property.Value.First;
                                    var tmp = ClearObjectJson(Newtonsoft.Json.JsonConvert.DeserializeObject(Json.Stringify(first)));
                                    arr.Add(tmp);

                                    var next = first.Next;
                                    while (next != null)
                                    {
                                        var tmpNext = ClearObjectJson(Newtonsoft.Json.JsonConvert.DeserializeObject(Json.Stringify(next)));
                                        arr.Add(tmpNext);
                                        next = next.Next;
                                        //var tmp = Newtonsoft.Json.JsonConvert.DeserializeObject();
                                    }
                                    property.Value = arr;
                                    //var tmp = Newtonsoft.Json.JsonConvert.DeserializeObject(Json.Stringify(property.Value));
                                    //property.Value = ClearObjectJson(tmp);
                                }
                                else
                                {
                                    property.Value = ClearObjectJson(Newtonsoft.Json.JsonConvert.DeserializeObject(property.Value.ToString()));
                                }
                                // If yes, enter the recursive loop to extract sub-field names

                            }
                            else
                            {
                                // If there are no sub-fields, the property name is the field name
                                var type = property.Value.Type;
                                var prop = SpecialTagValidate.FirstOrDefault(p => p.Name.ToLower().Equals(property.Name.ToLower()));
                                if (prop != null && prop.Type.ToLower().Equals("text"))
                                {
                                    property.Value = HttpUtility.HtmlDecode(property.Value.ToString().Decoder()).ClearSpecialField();
                                }
                                else
                                {
                                    if (type == JTokenType.String)
                                    {
                                        property.Value = HttpUtility.HtmlDecode(property.Value.ToString().Decoder()).ClearAllHtml();
                                    }
                                }
                            }
                        }
                        input = inputJson;
                    }
                    else
                    if (input.GetType() == typeof(Newtonsoft.Json.Linq.JArray))
                    {
                        var arr = new JArray();
                        var first = input.Value.First;
                        var tmpfirst = ClearObjectJson(Newtonsoft.Json.JsonConvert.DeserializeObject(Json.Stringify(first)));
                        arr.Add(tmpfirst);
                        var next = first.Next;
                        while (next != null)
                        {
                            var tmpNext = ClearObjectJson(Newtonsoft.Json.JsonConvert.DeserializeObject(Json.Stringify(next)));
                            arr.Add(tmpNext);

                            next = next.Next;
                            //var tmp = Newtonsoft.Json.JsonConvert.DeserializeObject();
                        }
                        input.Value = arr;
                        //var tmp = Newtonsoft.Json.JsonConvert.DeserializeObject(Json.Stringify(property.Value));
                        //property.Value = ClearObjectJson(tmp);
                    }
                    //else
                    //{
                    //    property.Value = ClearObjectJson(Newtonsoft.Json.JsonConvert.DeserializeObject(property.Value.ToString()));
                    //}
                }
            }
            catch (Exception ex)
            {
                input = null;
                //throw ex;
            }

            return input;
        }

        public static string ClearAllHtml(this string str)
        {
            try
            {
                if (string.IsNullOrEmpty(str)) return null;
                var sanitizer = new HtmlSanitizer("".Split(","));
                str = sanitizer.Sanitize(str, "", null, true);
                str = HttpUtility.HtmlDecode(str);
                str = sanitizer.Sanitize(str);
            }
            catch (Exception ex)
            {
                str = string.Empty;
                //throw ex;
            }

            return str;
        }

        public static string ClearHtml(this string str)
        {
            try
            {
                if (string.IsNullOrEmpty(str)) return null;
                var sanitizer = new HtmlSanitizer(RemoveTags.Select(p => p.Trim()), null, RemovewAttributes);
                str = sanitizer.Sanitize(str);
                str = HttpUtility.HtmlDecode(str);
                str = sanitizer.Sanitize(str);
            }
            catch (Exception ex)
            {
                str = string.Empty;
                //throw ex;
            }

            return str;
        }

        public static string ClearSpecialField(this string str)
        {

            if (string.IsNullOrEmpty(str)) return null;
            try
            {
                str = str.ClearHtml();

                var doc = new HtmlDocument();
                doc.LoadHtml(str);
                var listIframe = doc.DocumentNode.SelectNodes("//iframe");
                var listEmbed = doc.DocumentNode.SelectNodes("//embed");


                var blacklistIframe = listIframe?.Where(p => WhiteListDomain?.Where(w => p.GetAttributeValue("src", "").GetDomain().Equals(w.ToLower())).Count() == 0);
                var blacklistEmbed = listEmbed?.Where(p => WhiteListDomain?.Where(w => p.GetAttributeValue("src", "").GetDomain().Equals(w.ToLower())).Count() == 0);

                List<string> xpathIframes = new List<string>();
                if (blacklistIframe != null)
                {
                    foreach (var note in blacklistIframe)
                    {
                        xpathIframes.Add(note.XPath);
                    }
                    foreach (var xpath in xpathIframes)
                    {
                        doc.DocumentNode.SelectSingleNode(xpath)?.Remove();
                    }
                }

                List<string> xpathEmbeds = new List<string>();
                if (blacklistEmbed != null)
                {
                    foreach (var note in blacklistEmbed)
                    {
                        xpathEmbeds.Add(note.XPath);
                    }
                    foreach (var xpath in xpathEmbeds)
                    {
                        doc.DocumentNode.SelectSingleNode(xpath)?.Remove();
                    }
                }


                var listHrefXpathRemove = doc.DocumentNode.SelectNodes("//a")?.Where(p => p.GetAttributeValue("href", "").ToLower().Contains("javascript"))?.Select(p => p.XPath);
                if (listHrefXpathRemove != null)
                {
                    foreach (var xpath in listHrefXpathRemove)
                    {
                        doc.DocumentNode.SelectSingleNode(xpath)?.Remove();
                    }
                }

                str = doc.DocumentNode.OuterHtml;
            }
            catch (Exception ex)
            {
                str = string.Empty;
                //throw ex;
            }

            return str;
        }
        #endregion
    }
}
