﻿using ChannelVN.IMS2.Foundation.Toolkit.Messaging.ReserveStream;
using System;

namespace ChannelVN.IMS2.Foundation.Toolkit.Messaging.ExchangeQueue
{
    public class EQOptions : StreamOptions, ICloneable
    {
        public ExchangeType Type { get; set; }

        public EQOptions() : base()
        {
            Type = ExchangeType.Direct;
        }

        public virtual object Clone()
        {
            return Common.Utility.CloneObject(this);
        }
    }
}
