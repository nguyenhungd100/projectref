﻿using ChannelVN.IMS2.Foundation.Common.Configuration;
using ChannelVN.IMS2.Foundation.Logging;
using ChannelVN.IMS2.Foundation.Toolkit.Messaging.ReserveStream;
using System;

namespace ChannelVN.IMS2.Foundation.Toolkit.Messaging
{
    public class QueueFactory
    {
        private static Stream<object> _instance = null;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="handler"></param>
        /// <returns></returns>
        public static Stream<object> CreateInstance(Action<Element<object>, Action<Exception>> handler)
        {
            if(_instance == null)
            {
                var connectString = AppSettings.Current.ChannelConfiguration.ConnectionStrings.CmsQueueDb;
                if (string.IsNullOrEmpty(connectString))
                {
                    connectString = AppSettings.Current.ChannelConfiguration.ConnectionStrings.CmsMainCacheDb;
                }

                _instance = new Stream<object>("WebApi." + (AppSettings.Current.ServiceConfiguration.Namespace ?? ""), new StreamOptions {
                    Mode = StorageMode.Redis,
                    Engine = StorageEngine.Parse(connectString),
                    Retry = 8,
                    LIMIT_POOL_THREAD = (Environment.ProcessorCount < 1 ? 1 : Environment.ProcessorCount),
                    LIMIT_STREAM = 4294967296
                });

                _instance.SetHandler(handler);

                _instance.OnRaise += (sender, e) =>
                {
                    Logger.Information($"OnRaise.e => {e.Element.Id}");
                };
                _instance.OnError += (sender, e) =>
                {
                    Logger.Error($"OnError.e => {e.Error.Message}");
                };
                _instance.OnDrain += (sender, e) =>
                {
                    Logger.Debug($"onDrain.e => Clean!!!");
                };
            }
            return _instance;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="element"></param>
        /// <param name="callback"></param>
        /// <returns></returns>
        public static Stream<object> Send(object element, Action<Exception, int> callback = null)
        {
            return _instance.AddElement(element, callback);
        }
    }
}
