﻿using ChannelVN.IMS2.Foundation.Data;
using ChannelVN.IMS2.Foundation.Data.Providers;
using System;

namespace ChannelVN.IMS2.Foundation.Toolkit.Messaging.ReserveStream.Databases
{
    public class RedisStorage<TPayload> : Storage<TPayload>
    {
        private ElementStore _store = null;

        public RedisStorage(StorageOptions options) : base(options)
        {
            if (Options.Engine == null)
            {
                Options.Engine = new StorageEngine { Host = "localhost", Port = 6379 };
            }
            if (string.IsNullOrEmpty(Options.Engine.Database))
            {
                Options.Engine.Database = "0";
            }
            if (Options.ExpiresIn <= 0)
            {
                Options.ExpiresIn = 3600 * 24 * 30; //30days
            }
            if (string.IsNullOrEmpty(Options.PrefixStream))
            {
                Options.PrefixStream = "message";
            }
            if (Options.SeparateChar == default(char))
            {
                Options.SeparateChar = ':';
            }
            _store = new ElementStore(Options);
        }

        public RedisContext GetContext()
            => _store.GetContext();

        public override void Push(Element<TPayload> element, Action<Exception, long> callback)
        {
            var messagesActor = $"{Options.PrefixStream}{Options.SeparateChar}{Options.Stream}".ToLower();
            try
            {
                var messageKey = Common.Json.Stringify(element);
                var result = false;

                //using (var context = GetContext())
                var context = GetContext();
                {
                    result = context.Multi(trans =>
                    {
                        trans.ListLeftPushAsync(context.NameOf(messagesActor), messageKey);
                        trans.KeyExpireAsync(context.NameOf(messagesActor), DateTime.UtcNow.AddSeconds(Options.ExpiresIn));
                    });
                }
                callback(null, result ? 1 : 0);
            }
            catch (Exception ex)
            {
                callback(ex, 0);
            }
        }

        public override void Pop(Action<Exception, Element<TPayload>> callback)
        {
            var messagesActor = $"{Options.PrefixStream}{Options.SeparateChar}{Options.Stream}".ToLower();
            try
            {
                Element<TPayload> returnValue = null;
                //using (var context = GetContext())
                var context = GetContext();
                {
                    var messageKey = context.ListRightPopLeftPush(messagesActor, messagesActor);
                    if (string.IsNullOrEmpty(messageKey))
                    {
                        returnValue = null;
                    }
                    else
                    {
                        var result = context.ListRemove(messagesActor, messageKey, 1);
                        if (result > 0)
                        {
                            returnValue = Common.Json.Parse<Element<TPayload>>(messageKey);
                        }
                        else
                        {
                            returnValue = null;
                        }
                    }
                }
                callback(null, returnValue);
            }
            catch (Exception ex)
            {
                callback(ex, null);
            }
        }

        public override void Count(Action<Exception, long> callback)
        {
            var messagesActor = $"{Options.PrefixStream}{Options.SeparateChar}{Options.Stream}".ToLower();
            try
            {
                long length = 0;

                //using (var context = GetContext())
                var context = GetContext();
                {
                    length = context.ListLength(messagesActor);
                }
                callback(null, length);
            }
            catch (Exception ex)
            {
                callback(ex, 0);
            }
        }

        public override void Backup(Element<TPayload> element, Action<Exception, long> callback)
        {
            var messagesRecoveryActor = $"{Options.PrefixStream}{Options.SeparateChar}recovery{Options.SeparateChar}{Options.Stream}".ToLower();
            try
            {
                var messageKey = Common.Json.Stringify(element);
                var result = false;

                //using (var context = GetContext())
                var context = GetContext();
                {
                    result = context.Multi(trans =>
                    {
                        trans.ListLeftPushAsync(context.NameOf(messagesRecoveryActor), messageKey);
                        trans.KeyExpireAsync(context.NameOf(messagesRecoveryActor), DateTime.UtcNow.AddSeconds(Options.ExpiresIn));
                    });
                }
                callback(null, result ? 1 : 0);
            }
            catch (Exception ex)
            {
                callback(ex, 0);
            }
        }

        public override void Restore(Action<Exception, Element<TPayload>> callback)
        {
            var messagesActor = $"{Options.PrefixStream}{Options.SeparateChar}{Options.Stream}".ToLower();
            var messagesRecoveryActor = $"{Options.PrefixStream}{Options.SeparateChar}recovery{Options.SeparateChar}{Options.Stream}".ToLower();

            Element<TPayload> entry = null;
            do
            {
                try
                {
                    var messageKey = "";

                    //using (var context = GetContext())
                    var context = GetContext();
                    {
                        context.Exec(db => {
                            messageKey = db.ListRightPopLeftPush(context.NameOf(messagesRecoveryActor), context.NameOf(messagesActor));
                            db.KeyExpireAsync(context.NameOf(messagesActor), DateTime.UtcNow.AddSeconds(Options.ExpiresIn));
                        });
                    }
                    if (string.IsNullOrEmpty(messageKey))
                    {
                        entry = null;
                    }
                    else
                    {
                        entry = Common.Json.Parse<Element<TPayload>>(messageKey);
                    }
                    callback(null, entry);
                }
                catch (Exception ex)
                {
                    callback(ex, null);
                }
            }
            while (entry != null);
        }

        internal class ElementStore : Store<RedisContext, ElementStore>
        {
            public ElementStore()
            {
            }

            public ElementStore(StorageOptions options)
            {
                if (string.IsNullOrEmpty(options.Engine.Password))
                {
                    Configuration.Add(STO_CONNECTIONSTRING, $"{options.Engine.Host}:{options.Engine.Port},defaultDatabase={options.Engine.Database}");
                }
                else
                {
                    Configuration.Add(STO_CONNECTIONSTRING, $"{options.Engine.Host}:{options.Engine.Port},password={options.Engine.Password},defaultDatabase={options.Engine.Database}");
                }
                Configuration.Add("SeparateChar", options.SeparateChar);
            }
        }
    }
}
