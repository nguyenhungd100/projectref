﻿using ChannelVN.IMS2.Foundation.Common.Configuration;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using Telegram.Bot;
using Telegram.Bot.Types;

namespace ChannelVN.IMS2.Foundation.Logging
{
    public class TelegramNotification
    {
        //public async static void SendMessageAsync(string message)
        //{
        //    await Task.Run(async ()=>
        //    {
        //        //Thread.CurrentThread.IsBackground = true;
        //        try
        //        {
        //            var enabled = AppSettings.Current.TelegramNotification?.Enabled;
        //            var configRootApiUrl = AppSettings.Current.TelegramNotification?.RootApiUrl;
        //            var apiKey = AppSettings.Current.TelegramNotification?.ApiKey;

        //            if (enabled != null && enabled.Value && configRootApiUrl != null && !string.IsNullOrEmpty(configRootApiUrl))
        //            {
        //                var content = new FormUrlEncodedContent(new[]
        //                {
        //                    new KeyValuePair<string, string>("ServiceNamespace","KingHub"),
        //                    new KeyValuePair<string, string>("Content",DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + " => " + message)
        //                });

        //                using (var client = new HttpClient())
        //                {
        //                    client.DefaultRequestHeaders.Add("Authorization", apiKey);
        //                    client.Timeout = TimeSpan.FromSeconds(AppSettings.Current.ChannelConfiguration.HttpClientTimeOut);

        //                    var response = await client.PostAsync(configRootApiUrl + "message", content);
        //                    if (!response.IsSuccessStatusCode)
        //                    {
        //                        response.EnsureSuccessStatusCode();
        //                    }

        //                    var rep = await response.Content.ReadAsStringAsync();
        //                    if (!string.IsNullOrEmpty(rep))
        //                    {
        //                        //returnValue = Json.Parse<dynamic>(rep).successful;
        //                        //Logger.Information("SendMessageAsync => success:" + rep + ", channel: Kinghub, message:" + message);
        //                    }
        //                }
        //            }
        //        }
        //        catch (Exception ex)
        //        {
        //            Logger.Information("SendMessageAsync => exception :" + ex.Message);
        //        }
        //    });
        //}

        private static readonly Telegram _notifier = null;

        private static readonly bool _enabled = false;
        private static readonly string _botToken = string.Empty;
        private static readonly string _botInGroup = string.Empty;

        static TelegramNotification()
        {
            if(_notifier == null)
            {
                _enabled = (bool)AppSettings.Current.TelegramNotification?.Enabled;
                _botToken = AppSettings.Current.TelegramNotification?.BotToken;
                _botInGroup = AppSettings.Current.TelegramNotification?.BotInGroup;

                if (_enabled == true && !string.IsNullOrEmpty(_botToken) && !string.IsNullOrEmpty(_botInGroup))
                {
                    _notifier = new Telegram(_botToken, _botInGroup);
                }
            }
        }

        public static Task<bool> SendMessageAsync(string message)
        {
            return Task.Run(() =>
            {
                try
                {
                    if(_enabled)
                    {
                        return _notifier.SendMessage(message, _botInGroup);
                    }
                }
                catch (Exception ex)
                {
                    Logger.Error(ex, "SendMessageAsync");
                }
                return false;
            });
        }
    }
}
