﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ChannelVN.IMS2.Foundation.Logging
{
    public enum ActionType
    {
        Insert = 1,
        Update = 2,
        UpdateStatus = 3,
        UpdateMediaInBoard = 4,
        Share = 5,
        PushToApp = 6,
        //board
        PushBoardToApp = 7,
        UpdateBoardInApp = 8,
        DeleteBoardInApp = 9,
        AddPostsToBoardOnApp = 10,
        DeletePostsInBoardOnApp = 11,
        //add media to board
        AddMediaToBoard = 12,
        DeleteMediaFromBoard = 20,
        //photo
        AddPhotoToAlbum = 13,
        //video
        AddVideoToPlaylist = 14,
        //comment
        DeleteComment = 15,
        UpdateComment = 16,
        ApproveComment = 17,
        CreateComment = 18,
        RestoreComment = 19,
        //update post

        ChangeDistributionDate = 40,
        AcceptItemStream = 41, 
        RejectItemStream = 42,
        SaveCategory = 43,
        UploadGalleryImage = 44
    }
}
