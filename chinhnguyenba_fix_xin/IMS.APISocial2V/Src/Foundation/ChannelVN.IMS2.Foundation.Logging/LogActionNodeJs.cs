﻿using ChannelVN.IMS2.Foundation.Common.Configuration;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace ChannelVN.IMS2.Foundation.Logging
{
    public class LogActionNodeJs
    {
        public static async Task<bool> InsertLogAsync(LogActionEntity log)
        {
            try
            {
                var client = new RestClient();
                var sb = new StringBuilder();
                sb.AppendFormat("channel_id={0}", client.Channel_Id);
                sb.AppendFormat("&object_id={0}", log.ObjectId);
                sb.AppendFormat("&source_id={0}", log.SourceId);
                sb.AppendFormat("&source_name={0}", log.SourceName);
                sb.AppendFormat("&destination_id={0}", log.DestinationId);
                sb.AppendFormat("&destination_name={0}", log.DestinationName);
                sb.AppendFormat("&created_by={0}", log.Account);
                sb.AppendFormat("&object_type={0}", log.Type);
                sb.AppendFormat("&action_type={0}", log.ActionTypeDetail);
                sb.AppendFormat("&action_text={0}", log.ActionText);
                sb.AppendFormat("&message={0}", log.Message);
                sb.AppendFormat("&description={0}", log.Description);
                sb.AppendFormat("&ip_log={0}", log.Ip);
                sb.AppendFormat("&machine_name={0}", log.UserAgent);
                sb.AppendFormat("&secret_key={0}", client.SecretKey);

                client.PostData = sb.ToString();
                client.ActionName = "add";
                var data = JsonConvert.DeserializeObject<ApiActionResponse>(await client.MakeRequestAsync());
                return data.Success;
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
                return false;
            }
        }
        public static async Task<List<LogActionEntity>> SearchLog(string keyword, int pageIndex, int pageSize, long objectId, string sourceId, int[] actionTypeDetail, int[] type, DateTime dateFrom, DateTime dateTo)//, ref int totalRows
        {
            try
            {
                var client = new RestClient();
                var sb = new StringBuilder();
                //var actionTypeStr = string.Format("&action_type={0}", actionTypeDetail?.FirstOrDefault());
                //var typeStr = string.Format("&object_type={0}", type?.FirstOrDefault());



                sb.AppendFormat("channel_id={0}", client.Channel_Id);
                if (objectId > 0)
                    sb.AppendFormat("&object_id={0}", objectId);
                if (type.Count() == 1)
                    sb.AppendFormat("&object_type={0}", type?.FirstOrDefault());
                //if (type.Count() > 1)
                //{
                //    sb.Append("&&(");
                //    sb.Append($"object_type={type[0]}");
                //    for (int i = 1; i < type.Length; i++)
                //    {
                //        sb.Append("||").Append(string.Format("object_type={0}",type[i]));
                //    }
                //    sb.Append(")");
                //}
                if (actionTypeDetail.Count() == 1)
                    sb.AppendFormat("&action_type={0}", actionTypeDetail?.FirstOrDefault());
                //if (actionTypeDetail.Count() > 1)
                //{
                //    sb.Append("&&(");
                //    sb.Append($"action_type={actionTypeDetail[0]}");
                //    for (int i = 1; i < actionTypeDetail.Length; i++)
                //    {
                //        sb.Append("||").Append(string.Format("action_type={0}", actionTypeDetail[i]));
                //    }
                //    sb.Append(")");
                //}
                sb.AppendFormat("&source_id={0}", sourceId);
                sb.AppendFormat("&page_index={0}", pageIndex);
                sb.AppendFormat("&page_size={0}", pageSize);
                if (dateFrom > new DateTime(1980, 1, 1))
                    sb.AppendFormat("&created_date_from={0}", dateFrom.ToString("yyyy/MM/dd"));
                if (dateTo > new DateTime(1980, 1, 1))
                    sb.AppendFormat("&created_date_to={0}", dateTo.ToString("yyyy/MM/dd"));
                sb.AppendFormat("&secret_key={0}", client.SecretKey);
                sb.AppendFormat("&keyword={0}", keyword);

                client.PostData = sb.ToString();
                client.ActionName = "search";

                var ParseObject = JsonConvert.DeserializeObject<ResponseActivity>(await client.MakeRequestAsync());
                if (ParseObject == null) ParseObject = new ResponseActivity();
                //totalRows = 0;                
                var listLog = new List<LogActionEntity>();
                foreach (var item in ParseObject.data)
                {
                    listLog.Add(new LogActionEntity
                    {
                        ActionText = item.ActionText,
                        ActionTypeDetail = item.ActionTypeDetail,
                        ObjectId = item.ObjectId.ToString(),
                        CreatedDate = item.CreatedDate,
                        DestinationId = item.DestinationId,
                        DestinationName = item.DestinationName,
                        EncryptId = item.Id,
                        Message = item.Message,
                        SourceId = item.SourceId,
                        SourceName = item.SourceName,
                        Type = item.Type,
                        OwnerId = item.OwnerId,
                        Description = item.Description,
                        Ip = item.Ip,
                        UserAgent = item.UserAgent
                    });
                }
                return listLog;

            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
                return new List<LogActionEntity>();
            }
        }
        public class ApiActionResponse
        {
            [DataMember]
            public bool Success { get; set; }
            [DataMember]
            public int Code { get; set; }
            [DataMember]
            public string Message { get; set; }
            [DataMember]
            public string Data { get; set; }
            public ApiActionResponse()
            {
                Success = false;
                Code = 0;
                Message = "";
                Data = "";
            }
        }
        [DataContract]
        public class ResponseActivity
        {
            [DataMember]
            public bool Success { get; set; }
            [DataMember]
            public int Code { get; set; }
            [DataMember]
            public string Message { get; set; }
            [DataMember]
            public List<ResponseLogActionEntity> data { get; set; }
            public ResponseActivity()
            {
                Success = false;
                Message = "";
                data = new List<ResponseLogActionEntity>();
            }
        }
    }
    public enum HttpMethod
    {
        GET,
        POST,
        PUT,
        DELETE
    }
    public class RestClient
    {
        public string EndPoint { get; set; }
        public HttpMethod Method { get; set; }
        public string ContentType { get; set; }
        public string PostData { get; set; }
        public string ActionName { get; set; }
        public string Channel_Id { get; set; }
        public string SecretKey { get; set; }
        public RestClient()
        {
            EndPoint = AppSettings.Current.ChannelConfiguration.LogActionSettings.ApiUrl;
            Method = HttpMethod.POST;
            ContentType = "application/x-www-form-urlencoded; charset=utf-8";
            Channel_Id = AppSettings.Current.ChannelConfiguration.LogActionSettings.ApiChannel;
            SecretKey = AppSettings.Current.ChannelConfiguration.LogActionSettings.ApiSecretKey;
            PostData = "";
            ActionName = "";
        }
        public async Task<string> MakeRequestAsync()
        {
            try
            {
                if (AppSettings.Current.ChannelConfiguration.LogActionSettings.ApiEnabled == 1)
                {
                    var request = (HttpWebRequest)WebRequest.Create(EndPoint + ActionName);

                    request.Method = Method.ToString();
                    request.ContentLength = 0;
                    request.ContentType = ContentType;
                    if (!string.IsNullOrEmpty(PostData) && Method == HttpMethod.POST)
                    {
                        var bytes = UTF8Encoding.UTF8.GetBytes(PostData);
                        request.ContentLength = bytes.Length;

                        using (var writeStream = await request.GetRequestStreamAsync())
                        {
                            writeStream.Write(bytes, 0, bytes.Length);
                        }
                    }

                    using (var response = (HttpWebResponse)await request.GetResponseAsync())
                    {
                        var responseValue = string.Empty;

                        if (response.StatusCode != HttpStatusCode.OK)
                        {
                            var message = string.Format("Request failed. Received HTTP {0}", response.StatusCode);
                            throw new ApplicationException(message);
                        }

                        using (var responseStream = response.GetResponseStream())
                        {
                            if (responseStream != null)
                                using (var reader = new StreamReader(responseStream))
                                {
                                    responseValue = reader.ReadToEnd();
                                }
                        }
                        return responseValue;
                    }
                }
                return "";
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
                return "";
            }
        }
    }

    public class ResponseLogActionEntity
    {

        [JsonProperty("id")]
        public string Id { get; set; }

        [JsonProperty("object_id")]
        public long ObjectId { get; set; }

        [JsonProperty("source_id")]
        public string SourceId { get; set; }

        [JsonProperty("source_name")]
        public string SourceName { get; set; }

        [JsonProperty("destination_id")]
        public string DestinationId { get; set; }

        [JsonProperty("destination_name")]
        public string DestinationName { get; set; }

        [JsonProperty("created_date")]
        public DateTime CreatedDate { get; set; }

        [JsonProperty("action_text")]
        public string ActionText { get; set; }

        [JsonProperty("message")]
        public string Message { get; set; }

        [JsonProperty("created_by")]
        public string OwnerId { get; set; }

        [JsonProperty("object_type")]
        public int Type { get; set; }

        [JsonProperty("action_type")]
        public int ActionTypeDetail { get; set; }

        [JsonProperty("description")]
        public string Description { get; set; }

        [JsonProperty("machine_name")]
        public string UserAgent { get; set; }

        [JsonProperty("ip_log")]
        public string Ip { get; set; }
    }
    public class LogActionEntity
    {
        public long Id { get; set; }
        public string ObjectId { get; set; }
        public string EncryptId { get { return ObjectId.ToString(); } set { } }
        public string SourceId { get; set; }        // id page
        public string SourceName { get; set; }
        public string DestinationId { get; set; }
        public string DestinationName { get; set; }
        public DateTime CreatedDate { get; set; }
        public string ActionText { get; set; }
        public string Message { get; set; }
        public string OwnerId { get; set; }      // id owner page  
        public int Type { get; set; }
        public int ActionTypeDetail { get; set; }
        public long GroupTimeLine { get; set; }
        public string Description { get; set; }
        public string UserAgent { get; set; }
        public string Ip { get; set; }
        public string Title { get; set; }
        public string Avatar { get; set; }
        public int Status { get; set; }
        public string Otp { get; set; }
        public string Account { get; set; }
    }
}
