﻿using System;
using System.Net;
using System.Net.Http;

namespace ChannelVN.IMS2.Foundation.Logging
{
    internal class Telegram
    {
        readonly string _telegramAPI = String.Empty;
        readonly string _botToken = String.Empty;
        readonly string _channel = String.Empty;

        public Telegram(string botToken, string channel) : this(null, botToken, channel)
        { }

        public Telegram(string telegramAPI, string botToken, string channel)
        {
            _telegramAPI = string.IsNullOrEmpty(telegramAPI) ? "https://api.telegram.org/bot" : telegramAPI;
            _botToken = botToken; //"772839112:AAG-56b429Vd4NE45BaEKXDHHwimaJpJZLs"
            _channel = channel; //"-336561176"
        }

        /// <summary>
        /// Send a message to a Telegram chat/channel
        /// </summary>
        /// <param name="msg">Message text</param>
        /// <param name="sendTo">Recepient</param>
        public bool SendMessage(string msg, string sendTo)
        {
            try
            {
                msg = WebUtility.UrlEncode(msg);

                using (var httpClient = new HttpClient())
                {
                    var res = httpClient.GetAsync($"{_telegramAPI}{_botToken}/sendMessage?chat_id={sendTo}&text={msg}&parse_mode=HTML&disable_web_page_preview=true").Result;
                    if (res.StatusCode != HttpStatusCode.OK)
                    {
                        //string content = res.Content.ReadAsStringAsync().Result;
                        //string status = res.StatusCode.ToString();
                        throw new Exception($"Couldn't send a message via Telegram. Response from Telegram API: {res.Content.ReadAsStringAsync().Result}");
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex.Message);
                return false;
            }
            return true;
        }

        /// <summary>
        /// Send a picture to a Telegram chat/channel
        /// </summary>
        /// <param name="picture">URL of the image to send</param>
        /// <param name="sendTo">Recepient</param>
        public bool SendPicture(string picture, string sendTo)
        {
            try
            {
                picture = WebUtility.UrlEncode(picture);

                using (var httpClient = new HttpClient())
                {
                    var res = httpClient.GetAsync($"{_telegramAPI}{_botToken}/sendPhoto?chat_id={sendTo}&photo={picture}").Result;
                    if (res.StatusCode != HttpStatusCode.OK)
                    {
                        //string content = res.Content.ReadAsStringAsync().Result;
                        //string status = res.StatusCode.ToString();
                        throw new Exception($"Couldn't send a picture to Telegram. Response from Telegram API: {res.Content.ReadAsStringAsync().Result}");
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex.Message);
                return false;
            }
            return true;
        }
    }
}
