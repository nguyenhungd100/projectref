﻿using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using ChannelVN.IMS2.Core.Entities.Album;
using ChannelVN.IMS2.Foundation.Common;
using ChannelVN.IMS2.WebApi;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.TestHost;
using Xunit;

namespace ChannelVN.IMS2.UnitTest.Controllers
{
    public class AlbumController
    {
        private readonly string tokenValue = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxNzExNTM3MjQiLCJqdGkiOiIwYTMzZWFjNy00ZTAxLTQwNzctYjUwZC0wMjUwN2FlZDAxNjciLCJhdXQiOiJ7XCJQZXJtaXNzaW9uc1wiOltcIjBcIl0sXCJSb2xlc1wiOltcIlN1YnNjcmliZXJzXCJdfSIsImV4cCI6MTU4OTYwMTg2NCwiaXNzIjoiVmNjb3JwIiwiYXVkIjoibG9jYWxob3N0LGdpMS5jbm5kLnZuLGdpMi5jbm5kLnZuIn0.cHxq03wjb7R8XtZNmR-8JDxuzq9M38y3uhJB_mZn7j8";
        private readonly string[] args;
        private readonly TestServer _server;
        private readonly HttpClient _client;

        public AlbumController()
        {
            _server = new TestServer(Program.CreateWebHostBuilder(args));
            _client = _server.CreateClient();
            _client.DefaultRequestHeaders.Add("Authorization", tokenValue);
        }
        public static IEnumerable<object[]> AlbumTestData =>
        new List<object[]>
        {
            new object[] { new AlbumSearch {
                Url="url1",
                OriginalUrl="OriginalUrl1",
                Author="Author1",
                CategoryId=0,
                Location="Location1",
                PublishData="{\"items\":[{\"id\":\"15580596865630001\",\"link\":\"http://kingcontent.mediacdn.vn/2019/5/16/8-1557997696415179604859.jpg\",\"width\":1080,\"height\":931,\"content_type\":2},{\"id\":\"15580596865630002\",\"link\":\"http://kingcontent.mediacdn.vn/2019/5/16/60347811101590169960118401808244092757344256n-15580042093951387521465.jpg\",\"width\":960,\"height\":960,\"content_type\":2}]}",
                Tags="",
                Name="Name1",
                Description="Description1",
                TemplateId=1,
                MetaAvatar="{}",
                MetaData="{}"
            },true}
            ,
            new object[] { new AlbumSearch {
                Url="url2",
                OriginalUrl="OriginalUrl2",
                Author="Author2",
                CategoryId=0,
                Location="Location2",
                PublishData="{\"items\":[{\"id\":\"15580596865630001\",\"link\":\"http://kingcontent.mediacdn.vn/2019/5/16/8-1557997696415179604859.jpg\",\"width\":1080,\"height\":931,\"content_type\":2},{\"id\":\"15580596865630002\",\"link\":\"http://kingcontent.mediacdn.vn/2019/5/16/60347811101590169960118401808244092757344256n-15580042093951387521465.jpg\",\"width\":960,\"height\":960,\"content_type\":2}]}",
                Tags="",
                Name="Name2",
                Description="Description2",
                TemplateId=1,
                MetaAvatar="{}",
                MetaData="{}"
            },true}
        };

        [Theory]
        //[ClassData(typeof(CalculatorTestData))]
        [MemberData(nameof(AlbumTestData))]
        public async Task SaveTestAsync(AlbumSearch data1, bool data2)
        {
            var album = Utility.ConvertToKeyValuePair(data1);

            var content = new FormUrlEncodedContent(album);

            var response = await _client.PostAsync("/api/album/save", content);
            response.EnsureSuccessStatusCode();

            var responseString = await response.Content.ReadAsStringAsync();

            var okResult = Json.Parse<ActionResponse<object>>(responseString);

            Assert.IsType<ActionResponse<object>>(okResult);
            Assert.True(okResult.Success == data2);
        }
    }
}