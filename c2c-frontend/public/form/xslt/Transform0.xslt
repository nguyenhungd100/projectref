<?xml version='1.0'?>

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

	<xsl:variable name="c_panel_color" select="000000"/>

	<xsl:template match="/">
		<html>
			<head></head>
			<body>
				<form class="border border-top-0 ">

					<xsl:for-each select="form/line">
						<div class="border-bottom p-2 {@css}">
							<xsl:if test="@label!=''">
								<div class="text-center mb-2">
									<b><xsl:value-of select="@label"/></b>
									<p><xsl:value-of select="@subtitle"/></p>
								</div>
							</xsl:if>

							<div class="d-flex align-items-center mb-2">

								<xsl:if test="@cols='1'">

									<xsl:for-each select="item">

										<xsl:if test="@type !='TextBox'">
											<label class="rs-control-label d-inline text-right pr-2 col-2">
												<xsl:value-of select="@name"/>
											</label>
										</xsl:if>

										<xsl:if test="@type='TextBox'">
											<div class="col-2"></div>
										</xsl:if>

										<xsl:call-template name="find_control">

											<xsl:with-param name="ControlType">
												<xsl:value-of select="@type"/>
											</xsl:with-param>
										</xsl:call-template>

									</xsl:for-each>
								</xsl:if>

								<xsl:if test="@cols='2'">

									<xsl:for-each select="item">

										<xsl:if test="@type !='TextBox'">
											<label class="rs-control-label d-inline text-right pr-2 col-2">
												<xsl:value-of select="@name"/>
											</label>
										</xsl:if>

										<xsl:if test="@type='TextBox'">
											<div class="col-2"></div>
										</xsl:if>

										<xsl:call-template name="find_control">

											<xsl:with-param name="ControlType">
												<xsl:value-of select="@type"/></xsl:with-param>
										</xsl:call-template>
									</xsl:for-each>

								</xsl:if>
							</div>

							<xsl:if test="@cols='3'">
								<div class="row align-items-center mb-2">

									<xsl:for-each select="cols">
										<div class="col-6">

											<xsl:for-each select="item">

												<xsl:if test="@type !='TextBox'">
													<label class="rs-control-label d-inline text-right pr-2 col-2">
														<xsl:value-of select="@name"/>
													</label>
												</xsl:if>

												<xsl:if test="@type='TextBox'">
													<div class="col-2"></div>
												</xsl:if>

												<xsl:call-template name="find_control">

													<xsl:with-param name="ControlType">
														<xsl:value-of select="@type"/>
													</xsl:with-param>
												</xsl:call-template>
											</xsl:for-each>
										</div>
									</xsl:for-each>
								</div>
							</xsl:if>

							<xsl:if test="@cols='multi'">

								<xsl:for-each select="item">
									<div class="d-flex align-items-center mt-2">

										<xsl:if test="@type !='TextBox'">
											<label class="rs-control-label d-inline text-right pr-2 col-2">
												<xsl:value-of select="@name"/>
											</label>
										</xsl:if>

										<xsl:if test="@type='TextBox'">
											<div class="col-2"></div>
										</xsl:if>

										<xsl:call-template name="find_control">

											<xsl:with-param name="ControlType">
												<xsl:value-of select="@type"/>
											</xsl:with-param>
										</xsl:call-template>
									</div>

								</xsl:for-each>
							</xsl:if>

							<xsl:if test="@type='table'">
								<table class="table table-bordered" id="{@id}">
									<thead class="thead-light">
										<tr class="text-center">

											<xsl:for-each select="thead">
												<th><xsl:value-of select="@name"/></th>
											</xsl:for-each>
											<th>Xóa</th>
										</tr>
									</thead>
									<tbody >

										<xsl:for-each select="row">
											<tr class="text-center" id="{@id}">

												<xsl:for-each select="item">
													<td>

														<xsl:call-template name="find_control">

															<xsl:with-param name="ControlType">
																<xsl:value-of select="@type"/>
															</xsl:with-param>
														</xsl:call-template>
													</td>
												</xsl:for-each>
												<td class="align-middle">
													<i class="rs-icon cursor-pointer rs-icon-trash-o" style="color: red;cursor:pointer;display: none" title="button_delete"/>
												</td>
											</tr>
										</xsl:for-each>
									</tbody>
								</table>
								<button type="button" class="rs-btn rs-btn-default rs-btn-blue" id="add_item" Event="Add_Item">
									<i class="rs-icon mr-2 rs-icon-plus-circle"/>
									Thêm mới
									<span class="rs-ripple-pond">
										<span class="rs-ripple"/>
									</span>
								</button>
							</xsl:if>
						</div>
					</xsl:for-each>
				</form>
			</body>
		</html>
	</xsl:template>

	<xsl:template name="find_control">

		<xsl:param name="ControlType"/>

		<xsl:choose>

			<xsl:when test="$ControlType = 'Input'">
				<xsl:call-template name="CreateInput"/>
			</xsl:when>

			<xsl:when test="$ControlType = 'Date'">
				<xsl:call-template name="CreateInputDate"/>
			</xsl:when>

			<xsl:when test="$ControlType = 'Textarea'">
				<xsl:call-template name="CreateTextarea"/>
			</xsl:when>

			<xsl:when test="$ControlType = 'TextBox'">
				<xsl:call-template name="CreateTextBox"/>
			</xsl:when>

			<xsl:when test="$ControlType = 'Select'">
				<xsl:call-template name="CreateSelect"/>
			</xsl:when>

			<xsl:when test="$ControlType = 'Text'">
				<xsl:call-template name="CreateText"/>
			</xsl:when>

			<xsl:otherwise>
				This objetc [<xsl:value-of select="$ControlType"/>] not found
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>

	<xsl:template name="CreateInput">
		<input type="@inputtype" class="rs-input  d-inline {@css}" value="{@value}" id="{@id}"/>
	</xsl:template>

	<xsl:template name="CreateInputDate">
		<div class="rs-picker-date rs-picker-default rs-picker-placement-bottom-left rs-picker-toggle-wrapper">
			<input type="date" class="rs-input {@css}" value="{@value}" id="{@id}"/>
		</div>
	</xsl:template>

	<xsl:template name="CreateTextarea">
		<div class="rs-form-group d-block justify-content-center  {@css} pl-0" role="group">
			<div class="d-inline">
				<textarea id="{@id}" name="invention-name" rows="4" type="text" class="rs-input  d-inline" value="{@value}"></textarea>
			</div>
		</div>
	</xsl:template>

	<xsl:template name="CreateTextBox">
		<div class="d-inline text-left  pl-0 {@css}">
			<label class="checkbox-custom mr-2 d-inline">
				<input type="checkbox" id="{@id}"/>
				<span class="checkmark"></span>
			</label>

			<xsl:if test="@type='TextBox'">
				<label class="rs-control-label d-inline text-left">
					<xsl:value-of select="@name"/>
				</label>
			</xsl:if>
		</div>
	</xsl:template>

	<xsl:template name="CreateSelect">
		<select class="rs-input {@css}" id="{@id}">
			<option>Việt Nam</option>
			<option>Campuchia</option>
			<option>Laos</option>
			<option>Myanmar</option>
		</select>
	</xsl:template>

	<xsl:template name="CreateText">
		<span id="{@id}" class="{@css}">
			<xsl:value-of select="@value"/>
		</span>
	</xsl:template>

</xsl:stylesheet>
