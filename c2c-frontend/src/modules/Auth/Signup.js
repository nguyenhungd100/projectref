import React from "react";
import {
	Card,
	CardBody,
	Container
} from "reactstrap";

import { Icon, ControlLabel, Button, Nav, IconButton } from "rsuite";
import { Form, Field } from "react-final-form";
import { InputField } from "components/common/FinalFormComponent";
import { Color } from "constants/common";
import userApi from "api/userApi";
import { CommonContext } from "context";
import postApi from "api/postApi";
import firebaseApi from "api/firebaseApi"
export default class extends React.Component {
	static contextType = CommonContext;
	state = { email: '' }
	onSubmit = values => {
		this.onSave(values)
	};

	onPressEnter = e => {
		if (e.key === "Enter") {
			this.form.submit();
		}
	};

	onSave = async (values) => {
		const { loading, unloading, error, success, profile } = this.context;
		loading();
		let response = await userApi.addClient(values)
		unloading();
		if (response.success) {
			this.setState({ email: values.email });
		} else {
			error(
				response.message
					? response.message
					: "Có lỗi xảy ra. Vui lòng thử lại!"
			);
		}

	};
	pushMessage = async (model) => {
		await firebaseApi.send(model)
	}
	onSignin = () => {
		this.props.history.push("/auth/login")
	}
	onVerifiOTP = async (values) => {
		const { loading, unloading, error, success } = this.context;
		loading();
		let response = await userApi.verify(values)
		unloading();
		if (response.success) {
			if (response.data) {
				this.pushMessage({ userId: response.data.id, userName: response.data.fullName || response.data.email })
			}

			success("Bạn đã đăng ký thành công, vui lòng đăng nhập vào hệ thống")
			this.onSignin();
		} else {
			error(
				response.message
					? response.message
					: "Có lỗi xảy ra. Vui lòng thử lại!"
			);
		}

	};
	render() {
		const { email } = this.state;
		return (
			<>
				<Container>
					<div className="d-flex">
						<Card className="bg-white" style={{ width: "40%" }}>
							{!email ?

								<CardBody className="p-4">
									<div style={{ display: 'flow-root' }}>
										<Button appearance="ghost" style={{ float: 'right', color: Color.global, borderColor: Color.global }} onClick={this.onSignin}>Đăng nhập</Button>
									</div>
									<h5 className="title " style={{ fontWeight: "bold" }}>
										Đăng ký
								</h5>
									<span>
										Chào mừng bạn đến với chúng tôi, đăng nhập hoặc đăng ký để đăng những
										sản phẩm và dịch vụ mà bạn quan tâm.
								</span>
									<Form onSubmit={this.onSubmit} initialValues={{ isUserSide: true, password: '' }}
										validate={values => {
											const errors = {};
											if (!values.fullName) {
												errors.fullName = "Vui lòng nhập họ tên.";
											}
											if (!values.email) {
												errors.email = "Vui lòng nhập email của bạn";
											}
											if (!values.password || values.password.length < 6) {
												errors.password = "Mật khẩu phải có độ dài từ 6 ký tự trở lên.";
											}
											if (!values.veriPassword || (values.veriPassword !== values.password)) {
												errors.veriPassword = "Mật khẩu không khớp.";
											}
											return errors;
										}}

									>
										{({
											handleSubmit,
											pristine,
											invalid,
											values,
											form: {
												mutators: { push, pop }
											},
											form
										}) => {
											this.form = form;

											return (
												<form onSubmit={handleSubmit} className="mt-4" onKeyPress={this.onPressEnter}>
													<div className="mb-4">
														<ControlLabel className="form-text-lable mb-2">Họ tên</ControlLabel>
														<Field
															name="fullName"
															className="col-12"
															component={InputField}
															placeholder="Nhập họ tên"
														/>
													</div>
													<div className="mb-4">
														<ControlLabel className="form-text-lable mb-2">Email</ControlLabel>
														<Field
															name="email"
															className="col-12"
															component={InputField}
															placeholder="Nhập email"
														/>
													</div>
													<div className="mb-4">
														<ControlLabel className="form-text-lable mb-2">
															Mật khẩu
													</ControlLabel>
														<Field
															name="password"
															className="col-12"
															component={InputField}
															placeholder="Nhập mật khẩu"
															type="password"
														/>
													</div>
													<div className="mb-4">
														<ControlLabel className="form-text-lable mb-2">
															Nhập lại mật khẩu
													</ControlLabel>
														<Field
															name="veriPassword"
															className="col-12"
															component={InputField}
															placeholder="Nhập lại mật khẩu"
															type="password"
														/>
													</div>

													<Button
														style={{
															background: Color.gGlobal,
															color: "#fff",
															width: "100%",
															fontWeight: "bold"
														}}
														onClick={form.submit}
													>
														<IconButton size="xs" icon={<Icon icon="save" size="xs" />} color={Color.global} circle />{` Lưu lại`}
													</Button>
												</form>
											);
										}}
									</Form>

								</CardBody>
								:
								<CardBody className="p-4">
									<div style={{ display: 'flow-root' }}>
										<Button appearance="ghost" style={{ float: 'right', color: Color.global, borderColor: Color.global }} onClick={this.onSignin}>Đăng nhập</Button>
									</div>
									<h5 className="title " style={{ fontWeight: "bold" }}>
										Xác thực email
								</h5>
									<span>
										Chúng tôi đã gửi một mã xác thực tới email của bạn, vui lòng kiểm tra email và nhập mã OTP.
								</span>
									<Form onSubmit={this.onVerifiOTP} initialValues={{ otp: '', email: this.state.email }}
										validate={values => {
											const errors = {};
											if (!values.otp) {
												errors.otp = "Vui lòng nhập mã OTP.";
											}
											return errors;
										}}
									>
										{({
											handleSubmit,
											values,
											form
										}) => {
											this.formComfirm = form;

											return (
												<form onSubmit={handleSubmit} className="mt-4" onKeyPress={this.onPressEnter}>
													<div className="mb-4">
														<ControlLabel className="form-text-lable mb-2">Nhập OTP</ControlLabel>
														<Field
															name="otp"
															className="col-12"
															component={InputField}
															placeholder="Mã OTP của bạn"
														/>
													</div>

													<Button
														style={{
															background: Color.gGlobal,
															color: "#fff",
															width: "100%",
															fontWeight: "bold"
														}}
														onClick={form.submit}
													>
														<IconButton size="xs" icon={<Icon icon="envelope" size="xs" />} color={Color.global} circle />{` Hoàn tất`}
													</Button>
												</form>
											);
										}}
									</Form>
								</CardBody>
							}
						</Card>
						<div>
						<img src={"/assets/images/signup.jpg"} alt="" style={{ height: window.innerHeight * 0.8, width:window.innerWidth * 0.40 }} />
							{false &&  <img src={postApi.getImage("9406ec5c-ac9f-4518-87b6-72a3d47b82bb", parseInt(window.innerWidth * 0.40), parseInt(window.innerHeight * 0.8))} alt="" style={{ height: "100%" }} />}
						</div>
					</div>
				</Container>
			</>
		);
	}
}
