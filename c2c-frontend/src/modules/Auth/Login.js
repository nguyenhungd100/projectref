import React from "react";
import {
	Card,
	CardHeader,
	CardBody,
	Input,
	Container,
	Row,
	Col
} from "reactstrap";

import { Avatar, Icon, ControlLabel, FormGroup, Button, Nav, IconButton } from "rsuite";
import { Form, Field } from "react-final-form";
import {
	InputField,
	DateField,
	SelectField,
	RadioGroupField,
	InputNumberField
} from "components/common/FinalFormComponent";
import { fetchAccessToken } from "api/AuthenApi";
import { Color } from "constants/common";
import { CommonContext } from "context";
import { log } from "util";
import postApi from "api/postApi"
export default class extends React.Component {
	static contextType = CommonContext;
	onSubmit = values => {
		this.onLogin(values)
	};
	handleSelect = active => {
		if (active === 4) this.props.history.push("/app");
		this.setState({ active });
	};
	onPressEnter = e => {
		if (e.key === "Enter") {
			this.form.submit();

		}
	};

	onLogin = async (values) => {
		const { email, password } = values
		const { loading, unloading, error, success, profile } = this.context;
		loading();
		let response = await fetchAccessToken(email, password);
		console.log(response,"response");
		unloading();
		if (response.success) {
			localStorage.setItem("token", response.response_data);
			window.location.href = "/";
		} else {
			error(
				response.message
					? response.message
					: "Tài khoản hoặc mật khẩu không đúng. Vui lòng thử lại!"
			);
		}
	}
	onSignup = () => {
		this.props.history.push("/auth/sign-up")
	}

	render() {
		return (
			<>
				<Container>
					<div className="d-flex">
						<Card className="bg-white" style={{ width: "40%" }}>
							<CardBody className="p-4">
								<div style={{ display: 'flow-root' }}>
									<Button appearance="ghost" style={{ float: 'right', color: Color.global, borderColor: Color.global }} onClick={this.onSignup}>Đăng ký</Button>
								</div>
								<h5 className="title " style={{ fontWeight: "bold" }}>
									Đăng nhập
								</h5>
								<span>
									Chào mừng bạn đến với chúng tôi, đăng nhập hoặc đăng ký để đăng những
									sản phẩm và dịch vụ mà bạn quan tâm.
								</span>
								<Form onSubmit={this.onSubmit} initialValues={{ email: "", password: '' }}>
									{({
										handleSubmit,
										pristine,
										invalid,
										values,
										form: {
											mutators: { push, pop }
										},
										form
									}) => {
										this.form = form;

										return (
											<form onSubmit={handleSubmit} className="mt-4" onKeyPress={this.onPressEnter}>

												<div className="mb-4">
													<ControlLabel className="form-text-lable mb-2">Email</ControlLabel>
													<Field
														name="email"
														className="col-12"
														component={InputField}
														placeholder="Nhập email"
													/>
												</div>
												<div className="mb-4">
													<ControlLabel className="form-text-lable mb-2">
														Mật khẩu
													</ControlLabel>
													<Field
														name="password"
														className="col-12"
														component={InputField}
														placeholder="Nhập mật khẩu"
														type="password"
													/>
												</div>

												<Button
													style={{
														background: Color.gGlobal,
														color: "#fff",
														width: "100%",
														fontWeight: "bold"
													}}
													onClick={form.submit}
												>
													<IconButton size="xs" icon={<Icon icon="save" size="xs" />} color={Color.global} circle /> Đăng nhập
												</Button>
											</form>
										);
									}}
								</Form>
							</CardBody>
						</Card>
						<div style={{ width: "60%", height: "100%" }}>
						<img src={"/assets/images/signup.jpg"} alt="" style={{ height: window.innerHeight * 0.8, width:window.innerWidth * 0.40 }} />

					{false &&  <img src={postApi.getImage("9406ec5c-ac9f-4518-87b6-72a3d47b82bb", parseInt(window.innerWidth * 0.40), parseInt(window.innerHeight * 0.8))} alt="" style={{ height: "100%" }} />}
						</div>
					</div>
				</Container>
			</>
		);
	}
}
