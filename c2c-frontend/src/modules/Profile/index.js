import React, { useState, useEffect } from "react";

import {
	Card,
	CardHeader,
	CardBody,
	Input,
	Container,
	Row,
	Col
} from "reactstrap";

import { Avatar, Icon, ControlLabel, FormGroup, Button, Nav, Placeholder } from "rsuite";
import { Form, Field } from "react-final-form";
import {
	InputField,
	DateField,
	SelectField,
	RadioGroupField,
	InputNumberField
} from "components/common/FinalFormComponent";
import { Color } from "constants/common";
import ProfileEdit from "./ProfileEdit";
import { Switch, Route } from "react-router-dom";
import { AuthContext } from "context";
import postApi from "api/postApi"
import fileApi from "api/fileApi"
const { Paragraph } = Placeholder

const ListMyPost = React.lazy(() => import("modules/Profile/ListMyPost"));
const AddStore = React.lazy(() => import("modules/Store/AddStore"));


const links = [
	{ id: 308, key: "/:action/profile", name: "/profile", component: ProfileEdit, title: "Trang cá nhân", icon: "user-circle-o" },
	{ id: 1, key: "/:action/pending", name: "/pending", component: ListMyPost, title: "Tin chờ duyệt", icon: "order-form" },
	{ id: 5, key: "/:action/approve", name: "/approve", component: ListMyPost, title: "Tin đã duyệt", icon: "thumbs-up" },
	{ id: 10, key: "/:action/reject", name: "/reject", component: ListMyPost, title: "Tin bị từ chối", icon: "info" },
	{ id: 20, key: "/:action/info-save", name: "/info-save", component: ListMyPost, title: "Tin đã lưu", icon: "star" },
	{ id: 30, title: "Cửa hàng", icon: "briefcase" }
];
const CustomNav = ({ active, onSelect, ...props }) => {
	const [activeItem, setActive] = useState(active);
	function onClickItem(i) {
		setActive(i);
		if (onSelect) onSelect(i);
	}
	return (
		<Nav {...props} vertical activeKey={activeItem} onSelect={onClickItem}>
			{links.map(i => (
				<Nav.Item
					eventKey={i.id}
					key={i.id}
					icon={<Icon icon={i.icon} style={{ color: Color.global }} />}
				>
					<span
						className="text-16"
						style={{ color: activeItem === i.id ? Color.global : "unset" }}
					>
						{i.title}
					</span>
				</Nav.Item>
			))}
		</Nav>
	);
};
class Profile extends React.Component {
	static contextType = AuthContext;
	state = { active: 308, loading: true };
	onSubmit = values => {

	};
	UNSAFE_componentWillMount = () => {
		this.setTime = setTimeout(() => {
			let { profile } = this.context;
			if (!profile) window.location.href = "/"
		}, 300);
	}
	componentDidMount = async () => {
		this.setTime = setTimeout(async () => {
			let { profile = {} } = this.context
			let filter = {
				userId: profile && profile.id,
				pageIndex: 1,
				pageSize: 1
			}
			if (profile && profile.id) {
				let res = await postApi.listStore(filter);
				if (res && res.success) {
					if (res.data && res.data.records && res.data.records.length > 0) {
						this.setState({ shopPage: res.data.records[0] });
					}
				}
				this.onRun()
			}
			this.setState({ loading: false })
		}, 1000);

	}
	componentWillUnmount = () => {
		clearTimeout(this.setTime)
	}
	handleSelect = (active) => {
		let { shopPage } = this.state;
		let link = links.find(i => i.id == active);
		setTimeout(() => {
			if (active === 30 && shopPage) this.props.history.push("/user/stores/" + shopPage.id);
			else if (active === 30 && !shopPage) this.props.history.push(`${this.props.match.path}/add-store`);
			else {
				if (link) this.props.history.push(`${this.props.match.path}/${link.id}${link.name}`);
			}
			this.setState({ active });
		}, 800);


	};
	onRun = e => {
		this.setTime = setTimeout(() => {
			const { profile } = this.context;
			let backgroundImage = `url(${(profile && profile.imageId) ? fileApi.getImage(profile.imageId, 500, 400) : "/assets/images/product/MT-03/1.png}"})`
			this.setState({ style: { backgroundImage } })
		}, 500);

	};

	onSave = () => { };
	render() {
		const { active, loading, style } = this.state;
		let items = links.filter(i => i.key);
		return (
			<>
				<div
					className="header-profile  pt-2 pb-2 pr-4 pl-4"
					style={style}
				>
					<span className="mask bg-gradient-default opacity-8" />
				</div>
				<Container style={{ marginTop: -150, marginBottom: 100, minHeight: window.innerHeight * 0.7 }}>
					<Row>
						<Col xl="3" className="mb-4">
							<div className="category leftbar m-0 bg-white">
								<span className="title p-3 text-16">Thông tin</span>
								<CustomNav
									reversed
									appearance="subtle"
									active={active}
									onSelect={this.handleSelect}
								/>
							</div>
						</Col>
						<Col className="order-xl-2 mb-5 mb-xl-0 mb-4" xl="9">
							<Card className="card-profile shadow" style={{ borderRadius: 31, padding: 10, }}>
								<Switch>
									{items.map((item, index) => {
										return (
											<Route
												key={"r" + index}
												path={`${this.props.match.path}${item.key}`}
												render={props => (
													<item.component
														{...props}
														history={this.props.history}
														onRun={this.onRun}
														activeTab={this.state.activeTab}
													/>
												)}
												exact={true}
											/>
										);
									})}

									<Route
										key={"r" + "store"}
										path={`${this.props.match.path}${'/add-store'}`}
										render={props => {

											return loading ? < Paragraph style={{ height: 300 }} rows={5} rows={5} graph active /> : (
												<AddStore
													{...props}
													activeTab={this.state.activeTab}
												/>
											)
										}}
										exact={true}
									/>
								</Switch>
							</Card>
						</Col>
					</Row>
				</Container>
			</>
		);
	}


}

export default Profile;
