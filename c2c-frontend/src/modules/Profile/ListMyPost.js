import React, { Component } from "react";
import { Categories } from "constants/common";
import { Link } from "react-router-dom";
import ItemProduct from "components/common/ItemProduct"
import postApi from "api/postApi"
import { CommonContext } from "context";

const styles = { width: 100 };
export default class extends Component {
	static contextType = CommonContext;
	state = {
		type: "",
		viewList: "list",
		filter: {
			status: 5,
			orderByLowestPrice: true,
			sortDesc: true,
			pageIndex: 1,
			pageSize: 100
		},
		action: '',
		data: []
	};
	componentDidMount = () => {
		this.setTime = setTimeout(() => {
			let { action } = this.props.match.params;
			let status = this.props;
			let { profile = {} } = this.context;
			if (action && Number(action) < 11) this.state.filter.status = Number(action);
			this.state.filter.createdBy = profile.id;
			if (action && action == 20) {
				this.state.filter.createdBy = null;
				this.state.filter.userCurrent = profile.id;
				this.state.filter.isGetListCare = true;
			}
			else this.state.filter.isGetListCare = false;
			this.setState({ action })
			this.onSearch();
		}, 500);

	};
	onGetTitle = (type) => {
		let item = Categories.find(i => i.type === type);
		return item ? item : Categories[0]
	}
	onView = view => {
		this.setState({ viewList: view });
	};
	onSearch = async () => {
		this.setTime = setTimeout(async () => {
			let { filter } = this.state;
			let { loading, unloading, error, profile } = this.context;
			if (!filter.createdBy && profile) filter.createdBy = profile.id;
			if (!filter.createdBy) return;
			loading();
			let res = await postApi.list(filter);
			unloading();
			if (res && res.success) {
				let data = res.data.records;
				this.setState({ data })
			} else {
				error(res ? res.message : "", "error");
			}
		}, 500);

	};
	componentWillUnmount = () => {
		clearTimeout(this.setTime)
	}
	gotoDetail = (item) => {
		let type = Categories.find(i => i.value == item.categoryId).type || Categories[0].type
		this.props.history.push(`/product/${type}/${item.id}`);
	};
	cleanSave = async (id) => {
		let { loading, unloading, error, success } = this.context;
		loading();
		let model = { postId: id, isCaring: false }
		let res = await postApi.caring(model);
		unloading();
		if (res && res.success) {
			success("Bỏ lưu tin thành công.");
			this.onSearch()
		} else {
			error(res ? res.message : "", "error");
		}

	}
	render() {
		const { type, viewList, data, action } = this.state;
		return (
			<div className="products-list" style={{ minHeight: window.innerHeight * 0.7 }}>
				<ul className="products-listItem">
					{data.map((i, index) => {
						let { listImageIds = [] } = i;
						let image = (listImageIds && listImageIds[0]) ? postApi.getImage(listImageIds[0], 200, 200) : '';
						i.image = image;
						return <ItemProduct data={i} key={index} like={action == 20} onClick={() => this.gotoDetail(i)} infoSave={() => this.cleanSave(i.id)} />
					})}
				</ul>
			</div>
		);
	}
}
