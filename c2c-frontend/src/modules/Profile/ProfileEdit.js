import React, { useState, useEffect, useContext, useRef } from "react";

import {
    Card,
    CardBody,
    Row,
    Col
} from "reactstrap";

import { Icon, ControlLabel, Button, Nav } from "rsuite";
import { Form, Field } from "react-final-form";
import {
    InputField,
    DateField,
    RadioGroupField,
} from "components/common/FinalFormComponent";
import { Color } from "constants/common";
import PropTypes from 'prop-types';
import fileApi from "api/fileApi"
import { CommonContext, AuthContext } from "context";
import userApi from "api/userApi";
import { update } from "final-form-arrays";
import numeral from "numeral"
function ProfileEdit(props) {
    const inputImage = useRef(null);
    const contextApi = useContext(CommonContext) || {};
    async function onSubmit(values) {
        let { loading, unloading, error, success } = contextApi;
        loading();
        let res = await userApi.updateProfile(values);
        unloading();
        if (res && res.success) {
            success("Cập nhật tài khoản thành công")
            contextApi.getProfile();
            if (props.onRun) props.onRun()
        } else {
            error(res ? res.message : "", "error");
        }
    }
    async function saveImage(e) {
        let { loading, unloading, error, success } = contextApi;

        const file = e.target.files[0];
        if (contextApi && contextApi.profile) {
            loading();
            let res = await fileApi.save(file, contextApi.profile.id);
            unloading();
            if (res && res.success) contextApi.getProfile()
        }

    }
    const onButtonClick = () => {
        if (inputImage) inputImage.current.click();
    };
    function initForm() {

    }
    useEffect(() => {


    }, [])
    return (
        <>
            <Row className="justify-content-center">
                <div className="profile-round">
                    <img src={(contextApi.profile && contextApi.profile.imageId) ? fileApi.getImage(contextApi.profile.imageId, 100, 100) : "/assets/images/user.png"} />
                </div>
                <div className="profile-round-camera cursor-pointer">
                    <Icon
                        icon="camera"
                        style={{ fontSize: 22, color: "#fff", backgroundColor: "#000" }}
                        onClick={onButtonClick}
                    />
                </div>
            </Row>
            <CardBody
                className="pt-0 pt-md-4"
                style={{ backgroundColor: "#f7fafc", borderRadius: 31 }}
            >
                <div className="category leftbar" style={{ justifyContent: "space-between", display: "flex" }}>
                    <span className="text-16">Số dư</span>
                    {contextApi.profile && contextApi.profile.id && <span className="text-16"><span style={{ color: "red" }}>{numeral(contextApi.profile.assetsTotal || 0).format('0,0')} đ</span> </span>}
                </div>
                <Form onSubmit={onSubmit} initialValues={contextApi.profile}>
                    {({
                        handleSubmit,
                        values,
                        form: {
                            mutators: { push, pop }
                        },
                        form
                    }) => {
                        return (
                            <form onSubmit={handleSubmit} className="p-4">
                                <Row className="mb-4">
                                    <Col sm="6">
                                        <ControlLabel className="form-text-lable mb-2">
                                            Họ Tên
                                        </ControlLabel>
                                        <Field
                                            name="fullName"
                                            className="col-12"
                                            component={InputField}
                                            placeholder="Họ tên"
                                        />
                                    </Col>
                                    <Col sm="6">
                                        <ControlLabel className="form-text-lable mb-2">
                                            Ngày sinh
                                        </ControlLabel>
                                        <Field
                                            name="dateOfBirth"
                                            className="col-12 p-0"
                                            component={DateField}
                                            placeholder="Ngày sinh"
                                            formatDate="DD/MM/YYYY"
                                            style={{
                                                opacity: 1,
                                                padding: "8px 15px",
                                                border: "1px solid #cccccc",
                                                fontSize: 14,

                                                borderRadius: 10
                                            }}
                                            block
                                        />
                                    </Col>
                                </Row>
                                <Row className="mb-4">
                                    <Col sm="6">
                                        <ControlLabel className="form-text-lable mb-2">
                                            Số điện thoại
                                    </ControlLabel>
                                        <Field
                                            name="mobile"
                                            className="col-12"
                                            component={InputField}
                                            placeholder="Số điện thoại"
                                        />
                                    </Col>

                                    <Col sm="6">
                                        <ControlLabel className="form-text-lable mb-2">
                                            Email
                                    </ControlLabel>
                                        <Field
                                            disabled
                                            name="email"
                                            className="col-12"
                                            component={InputField}
                                            placeholder="email"
                                        />
                                    </Col>
                                </Row>
                                <Row className="mb-4">
                                    <Col sm="12">
                                        <ControlLabel className="form-text-lable mb-2">
                                            Giới tính
                                        </ControlLabel>
                                        <Field
                                            inline
                                            name="gender"
                                            className="col-12 p-0"
                                            component={RadioGroupField}
                                            data={[
                                                { value: 1, label: "Nam" },
                                                { value: 2, label: "Nữ" },
                                                { value: 3, label: "Khác" }
                                            ]}
                                        />
                                    </Col>
                                </Row>
                                <input ref={inputImage} type={'file'} onChange={saveImage} hidden />
                                <Button
                                    style={{
                                        background: Color.gGlobal,
                                        color: "#fff",
                                        float: "right"
                                    }}
                                    onClick={form.submit}
                                >
                                    <Icon icon="save" /> Lưu lại
                                </Button>
                            </form>
                        );
                    }}
                </Form>

                <div className="text-center" />
            </CardBody>
        </>
    )
}

export default ProfileEdit
