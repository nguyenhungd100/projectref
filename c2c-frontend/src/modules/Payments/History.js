import React, { Component } from 'react';
import { Banks } from "constants/common";
import {
    Card, CardText, CardHeader, CardBody,
    CardTitle, Table,
} from 'reactstrap';
import { Radio, RadioGroup, IconButton, Icon, Message } from 'rsuite';
import arrayMutators from "final-form-arrays";
import { Form, Field } from "react-final-form";
import { Color } from "constants/common"
import { CommonContext } from "context";
import transactionApi from "api/transactionApi"
import moment from "moment";
import numeral from "numeral"
export default class History extends Component {
    static contextType = CommonContext;
    state = { data: [] }
    onSubmits = () => {
        this.props.history.push("/payment/user")
    }
    getOrderCode = async (str) => {
        if (!str) return false;
        let temp = str.indexOf("order_code=");
        if (temp < 0) return false;
        let str1 = str.substring(temp);
        let temp1 = str1.indexOf("&order_id");
        if (temp1 < 0) return false;
        let orderCode = str1.substring(11, temp1);
        return orderCode ? orderCode : false;
    }
    checkOut = async (orderCode) => {
        let { loading, unloading, profile } = this.context;
        loading();
        let res = await transactionApi.checkOut({ orderCode });
        if (res && res.success) {
            this.getData();
        }
        unloading();
    }
    componentDidMount = async () => {
        this.getData();
        this.setTime = setTimeout(async () => {
            let { location } = this.props;
            let orderCode = await this.getOrderCode(location.search);
            if (orderCode) {
                this.checkOut(orderCode);
                this.props.history.replace("/payment/history");
            }

        }, 600);
    }
    getData = async () => {
        this.tiems = setTimeout(async () => {
            let { loading, unloading, error, success, profile, getProfile } = this.context;
            if (!profile) return;
            loading();
            let res = await transactionApi.list(profile.id);
            unloading();
            if (res && res.success) {
                let data = res.data || [];
                this.setState({ data })
                getProfile();
            } else {
                error(res ? res.message : "", "error");
            }
        }, 600);

    };
    componentWillUnmount = () => {
        clearTimeout(this.tiems);
        clearTimeout(this.setTime);
    }
    render() {
        const { data = [] } = this.state;
        return (
            <div className="container_fullwidth">
                <div className="container">
                    <Form
                        onSubmit={this.onSubmits}
                        mutators={{
                            ...arrayMutators
                        }}
                    >
                        {({
                            handleSubmit,
                            values,
                            form: {
                                mutators: { push, pop }
                            },
                            form
                        }) => {
                            this.form = { ...form, values };
                            let { profile } = this.context;
                            return (
                                <form onSubmit={handleSubmit}>

                                    <div className="row" style={{ minHeight: window.innerHeight * 0.8 }}>
                                        <div className="col-md-9">
                                            <div className="category leftbar" style={{ justifyContent: "space-between", display: "flex" }}>
                                                <span className="text-16">Lịch sử thanh toán</span>
                                                {profile && profile.id && <span className="text-16">Số dư: <span style={{ color: "red" }}>{numeral(profile.assetsTotal || 0).format('0,0')} đ</span> </span>}
                                            </div>
                                            <div className="category leftbar" style={{ minHeight: window.innerHeight * 0.7 }}>
                                                <Table bordered>
                                                    <thead style={{ background: Color.gGlobal, color: "#fff", fontWeight: 'bold' }}>
                                                        <tr>
                                                            <th></th>
                                                            <th>Ngày thanh toán</th>
                                                            <th>Số tiến giao dịch</th>
                                                            <th>Loại giao dịch</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        {data.map((i, index) => (
                                                            <tr key={index}>
                                                                <td scope="row">{index + 1}</td>
                                                                <td>{moment(i.transactionTime).format("DD-MM-YYYY")}</td>
                                                                <td>{numeral(i.transactionAmount).format('0,0')}</td>
                                                                <td><div style={{ padding: 10 }} className={i.transactionType == 1 ? "rs-message-info" : "rs-message-warning"}>{i.messageType}</div></td>
                                                            </tr>
                                                        ))}
                                                    </tbody>
                                                </Table>
                                            </div>
                                        </div>
                                        {

                                            <div className="col-md-3">
                                                <div className="category leftbar">
                                                    <span className="title p-3 text-16">Chọn gói thanh toán</span>
                                                    <Field name="card">
                                                        {({ input }) => (
                                                            <RadioGroup
                                                                name="package"
                                                                value={input.value}
                                                                onChange={value => {
                                                                    input.onChange(value)
                                                                }}
                                                            >
                                                                <Card body className="text-right mb-2">
                                                                    <CardTitle> <Radio value={2}><span className="title text-16">Gói 1</span></Radio></CardTitle>
                                                                    <CardText><span className="text-14s">Nạp 2.000 đ - Giá 2.000 đ</span></CardText>
                                                                </Card>

                                                                <Card body className="text-right mb-2">
                                                                    <CardTitle> <Radio value={5}><span className="title text-16">Gói 2</span></Radio></CardTitle>
                                                                    <CardText><span className="text-14s">Nạp 5.000 đ - Giá 5.000 đ</span></CardText>
                                                                </Card>
                                                                <Card body className="text-right mb-2">
                                                                    <CardTitle> <Radio value={10}><span className="title text-16">Gói 3</span></Radio></CardTitle>
                                                                    <CardText><span className="text-14s">Nạp 10.000 đ - Giá 10.000 đ</span></CardText>
                                                                </Card>

                                                                <Card body className="text-right mb-2">
                                                                    <CardTitle> <Radio value={20}><span className="title text-16">Gói 4</span></Radio></CardTitle>
                                                                    <CardText><span className="text-14s">Nạp 20.000 đ - Giá 20.000 đ</span></CardText>
                                                                </Card>
                                                            </RadioGroup>
                                                        )}
                                                    </Field>
                                                    <div className="text-center">
                                                        <button
                                                            className="button add-cart text-center pr-2 pl-2 mt-4"
                                                            type="button"
                                                            style={{ background: Color.gGlobal, color: "#fff" }}
                                                            onClick={form.submit}
                                                        >
                                                            <IconButton icon={<Icon icon="credit-card" />} circle /> <span style={{ fontWeight: 'bold' }}> Thanh toán</span>
                                                        </button>

                                                    </div>
                                                </div>
                                            </div>
                                        }
                                    </div>
                                </form>
                            );
                        }}
                    </Form>

                </div>
            </div>
        );

    }
}

