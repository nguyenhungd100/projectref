import React, { Component } from 'react';
import { Banks } from "constants/common";
import {
    Card, CardImg, CardText, CardBody,
    CardTitle, CardSubtitle,
} from 'reactstrap';
import { Radio, RadioGroup, IconButton, Icon } from 'rsuite';
import arrayMutators from "final-form-arrays";
import { Form, Field } from "react-final-form";
import { Color } from "constants/common"
import { CommonContext } from "context";
import transactionApi from '../../api/transactionApi';
import numeral from "numeral"
export default class Payment extends Component {
    static contextType = CommonContext;
    onSubmits = async (values) => {
        let { orderCode, bankCode } = values;
        let { loading, unloading, error, success, profile } = this.context;
        if (!profile) return error("Bạn cần đăng nhập để thực hiện chức năng này");
        if (!values.bankCode) return error("Bạn cần chọn ngân hàng để thanh toán");
        if (!values.orderCode) return error("Bạn cần chọn gói thanh toán");
        loading();
        let res = await transactionApi.checkUrl({ bankCode, orderCode });
        unloading();
        if (res && res.success) {
            window.open(res.data, "_parent ")

        }
        else error(res && res.message ? res.message : "Vui lòng thử lại.")
    }
    render() {

        return (
            <div className="container_fullwidth">
                <div className="container">
                    <Form
                        onSubmit={this.onSubmits}
                        mutators={{
                            ...arrayMutators
                        }}
                    >
                        {({
                            handleSubmit,
                            pristine,
                            invalid,
                            values,
                            form: {
                                mutators: { push, pop }
                            },
                            form
                        }) => {
                            this.form = { ...form, values };
                            let { profile = {}, shopPage } = this.context;
                            return (
                                <form onSubmit={handleSubmit}>

                                    <div className="row">
                                        <div className="col-md-9">
                                            <div className="category leftbar" style={{ justifyContent: "space-between", display: "flex" }}>
                                                <span className="text-16">Thanh toán</span>
                                                {profile && profile.id && <span className="text-16">Số dư: <span style={{ color: "red" }}>{numeral(profile.assetsTotal || 0).format('0,0')} đ</span> </span>}
                                            </div>
                                            <div className="category leftbar">
                                                <span className="title p-3 text-16">Chọn thẻ ngân hàng nôi địa</span>
                                                <Field name="bankCode">
                                                    {({ input }) => (
                                                        <RadioGroup
                                                            value={input.value}
                                                            onChange={value => {
                                                                input.onChange(value)
                                                            }}
                                                        >
                                                            <div className="row">
                                                                {Banks.map(i => (
                                                                    <div className="col-2 mb-2 p-2" key={i.value} >
                                                                        <Card>
                                                                            <Radio value={i.value} />
                                                                            <CardImg top width="80%" src={`/assets/images/bank/${i.value}.png`} alt={i.lable} />
                                                                        </Card>

                                                                    </div>
                                                                ))}
                                                            </div>
                                                        </RadioGroup>
                                                    )}

                                                </Field>
                                            </div>
                                        </div>
                                        <div className="col-md-3">
                                            <div className="category leftbar">
                                                <span className="title p-3 text-16">Chọn gói thanh toán</span>
                                                <Field name="orderCode">
                                                    {({ input }) => (
                                                        <RadioGroup
                                                            name="package"
                                                            value={input.value}
                                                            onChange={value => {
                                                                input.onChange(value)
                                                            }}
                                                        >
                                                            <Card body className="text-right mb-2">
                                                                <CardTitle> <Radio value={2}><span className="title text-16">Gói 1</span></Radio></CardTitle>
                                                                <CardText><span className="text-14s">Nạp 2.000 đ - Giá 2.000 đ</span></CardText>
                                                            </Card>

                                                            <Card body className="text-right mb-2">
                                                                <CardTitle> <Radio value={5}><span className="title text-16">Gói 2</span></Radio></CardTitle>
                                                                <CardText><span className="text-14s">Nạp 5.000 đ - Giá 5.000 đ</span></CardText>
                                                            </Card>
                                                            <Card body className="text-right mb-2">
                                                                <CardTitle> <Radio value={10}><span className="title text-16">Gói 3</span></Radio></CardTitle>
                                                                <CardText><span className="text-14s">Nạp 10.000 đ - Giá 10.000 đ</span></CardText>
                                                            </Card>

                                                            <Card body className="text-right mb-2">
                                                                <CardTitle> <Radio value={20}><span className="title text-16">Gói 4</span></Radio></CardTitle>
                                                                <CardText><span className="text-14s">Nạp 20.000 đ - Giá 20.000 đ</span></CardText>
                                                            </Card>
                                                        </RadioGroup>
                                                    )}
                                                </Field>

                                            </div>
                                        </div>
                                    </div>
                                    <div className="row">
                                        <div className="col-md-9 text-center">
                                            <div className="category leftbar">
                                                <button
                                                    className="button add-cart text-center pr-4 pl-4"
                                                    type="button"
                                                    style={{ background: Color.gGlobal, color: "#fff" }}
                                                    onClick={form.submit}
                                                >
                                                    <IconButton icon={<Icon icon="credit-card" />} circle /> <span className="text-16"> Thanh toán</span>
                                                </button>
                                            </div>

                                        </div>
                                    </div>

                                </form>
                            );
                        }}
                    </Form>

                </div>
            </div>
        );

    }
}

