import React, { Component } from "react";
import arrayMutators from "final-form-arrays";
import { Form, Field } from "react-final-form";
import {
	InputField,
	DateField,
	SelectField,
	RSTextAreaField,
	UploaderField,
	InputNumberField,
	CurrencyField
} from "components/common/FinalFormComponent";
import {
	Modal,
	Icon,
	Button,
	FormGroup,
	ControlLabel,
	Notification,
	ButtonToolbar,
	FlexboxGrid,
	Uploader,
	Message
} from "rsuite";
import SlideShow from "../Home/SlideShow";
import { CommonContext, AuthContext } from "context";
import { Cities, Color, Categories } from "constants/common";
import postApi from "api/postApi";
import { log } from "util";
const getBase64 = file => {
	return new Promise((resolve, reject) => {
		const reader = new FileReader();
		reader.readAsDataURL(file);
		reader.onload = () => resolve(reader.result);
		reader.onerror = error => reject(error);
	});
};
class CreateForm extends Component {
	static contextType = CommonContext;
	UNSAFE_componentWillMount = () => {
		// let { isAuth, error } = this.context;
		// if (!isAuth) this.props.history.push("/Auth");
		// error("Vui lòng đăng nhập trước khi đăng tin.")
	};
	componentDidMount = async () => {
		this.setTime = setTimeout(() => {
			let { profile = {}, shopPage } = this.context;
			let values = {};
			values.email = profile.email;
			values.fullName = profile.fullName;
			values.mobile = profile.mobile
			values.shopPageId = profile.shopPageId;
			values.categoryId = profile.categoryId;
			this.form.initialize(values);
		}, 1000);
	}
	componentWillUnmount = () => {
		clearTimeout(this.setTime)
	}
	onChange = async (files, b, c) => {
		console.log(files);
	};
	onSubmits = values => {
		this.onSave(values);
	};
	onSave = async (values) => {
		let { loading, unloading, error, success, profile, getProfile } = this.context;
		loading();

		let res = await postApi.save(values);
		unloading();
		if (res && res.success) {
			success("Cám ơn bạn, tin của bạn sẽ sớm được đăng lên khi được duyệt.");
			this.form.initialize({});
			if (getProfile) getProfile();
		} else {
			error(res ? res.message : "", "error");
		}
	};
	onUpload = async (files) => {
		let { values } = this.form;
		let { imageIds = [] } = values;
		let { error, loading, unloading } = this.context;
		loading();
		if(!files)return;
		let res = await postApi.upImage(files);
		unloading();
		if (res && res.success && res.data) {
			imageIds.push(res.data)
			this.form.change("imageIds", imageIds);
		}

	}

	render() {
		return (
			<div>
				<SlideShow />
				<div className="container_fullwidth">
				{false &&
					<div className="ec-animate" >
						<h1>
							<span>Đăng  </span>
							<span>Tin </span>
							<span>Đê </span>
						</h1>
					</div>
				}
					<div className="container">
						<div className="simple-cta pullup" style={{ width: 254 }}>
							▾ Đăng tin ▾
						</div>

						<div className="ec-card">
							<Form
								onSubmit={this.onSubmits}
								mutators={{
									...arrayMutators
								}}
							// initialValues={{ imageIds: [] }}
							>
								{({
									handleSubmit,
									pristine,
									invalid,
									values,
									form: {
										mutators: { push, pop }
									},
									form
								}) => {
									this.form = { ...form, values };
									let { profile = {}, shopPage } = this.context;
									values.email = profile.email;
									values.fullName = profile.fullName;
									values.mobile = profile.mobile;
									if (values.shopPageId) values.categoryId = profile.categoryId;
									return (
										<form onSubmit={handleSubmit}>
											<FormGroup className="d-flex align-items-center mb-3">
												<ControlLabel

													className="offset-1 col-2 text-left required"
													placeholder="Tiêu đề"
												>
													Tiêu đề
												</ControlLabel>
												<Field name="name" component={InputField} className="col-8" />
											</FormGroup>
											<FormGroup className="d-flex align-items-center mb-3">
												<ControlLabel className="offset-1 col-2 text-left">
													Khu vực
												</ControlLabel>
												<Field
													name="provinceId"
													component={SelectField}
													placeholder="Khu vực"
													valueKey="value"
													labelKey="name"
													data={Cities}
													block
													className="col-3 p-0"
												/>
												<ControlLabel className="col-2 text-left">
													Nhóm sản phẩm
												</ControlLabel>
												<Field
													disabled={values.shopPageId}
													name="categoryId"
													component={SelectField}
													placeholder="Nhóm sản phẩm"
													valueKey="value"
													labelKey="title"
													data={Categories}
													block
													className="col-3 p-0"
												/>
											</FormGroup>
											<FormGroup className="d-flex align-items-center mb-3">
												<ControlLabel className="offset-1 col-2 text-left">
													Nội dung mô tả
												</ControlLabel>
												<Field
													name="content"
													component={InputField}
													placeholder="Nội dung mô tả"
													componentClass="textarea"
													rows={4}
													style={{ resize: "auto" }}
													className="col-8"
													block
												/>
											</FormGroup>
											<FormGroup className="d-flex align-items-center mb-3">
												<ControlLabel

													className="offset-1 col-2 text-left require"
												>
													Giá bán(đ)
												</ControlLabel>
												<Field
													name="price"
													thousandSeparator={true}
													component={CurrencyField}
													placeholder="Giá bán"
													className="col-8"
													style={{ height: 40 }}
													block
												/>
											</FormGroup>
											<FormGroup className="d-flex align-items-center mb-3">
												<ControlLabel

													className="offset-1 col-2 text-left require"
												>
													Ảnh sản phẩm
												</ControlLabel>
												<Field
													name="file"
													component={UploaderField}
													shouldQueueUpdate={(old, newFile) => {
														this.onUpload(newFile)
													}}
													placeholder="Chọn ảnh"
													className="col-9 p-0"
													autoUpload={false}
													icon="camera-retro"
													size="lg"
													styleIcon={{ color: Color.global }}
													listType="picture"
													onRemove={(a, b) => {
														console.log(a, 'a', b);

													}}
													multiple={false}
												/>
											</FormGroup>
											{
												shopPage &&
												<FormGroup className="d-flex align-items-center mb-3">
													<ControlLabel className="offset-1 col-2 text-left">
														Đăng với tư cách
												</ControlLabel>
													<Field
														name="shopPageId"
														component={SelectField}
														placeholder="Người đăng"
														labelKey="lable"
														valueKey="value"
														data={[{ lable: "Cá nhân", value: '' }, { lable: shopPage.name, value: shopPage.id }]}
														block
														className="col-8 p-0"
													/>
												</FormGroup>
											}
											<FormGroup className="d-flex align-items-center mb-3">
												<ControlLabel className="offset-1 col-2 text-left">
													Tên người đăng
												</ControlLabel>
												<Field
													disabled
													name="fullName"
													component={InputField}
													placeholder="Tên người bán"
													className="col-8"
												/>
											</FormGroup>

											<FormGroup className="d-flex align-items-center mb-3">
												<ControlLabel className="offset-1 col-2 text-left">
													Email
												</ControlLabel>
												<Field
													disabled
													name="email"
													component={InputField}
													placeholder="Email"
													className="col-8"
												/>
											</FormGroup>

											<FormGroup className="d-flex align-items-center mb-3">
												<ControlLabel className="offset-1 col-2 text-left ">
													Phone
												</ControlLabel>
												<Field
													disabled
													name="mobile"
													component={InputField}
													placeholder="Phone"
													className="col-8"
												/>
											</FormGroup>
											<FormGroup className="d-flex align-items-center mb-3">
												<ControlLabel className="offset-1 col-2 text-left" />
												<button
													className="button add-cart text-left"
													type="button"
													style={{ background: Color.gGlobal, color: "#fff" }}
													onClick={form.submit}
												>
													Đăng tin
												</button>
												<Message type="warning" description="Bạn sẽ bị trừ 1,000 đ khi đăng tin." />
											</FormGroup>
										</form>
									);
								}}
							</Form>
						</div>
					</div>
				</div>
			</div>
		);
	}
}
export default CreateForm;
