import React, {useState, useEffect} from "react";
import {
    Card,
    CardHeader,
    CardBody,
    Input,
    Container,
    Row,
    Col
} from "reactstrap";
import {
    Avatar,
    Icon,
    ControlLabel,
    FormGroup,
    Button,
    ButtonToolbar,
    Nav,
    Message
} from "rsuite";
import {Form, Field} from "react-final-form";
import {
    InputField,
    DateField,
    SelectField,
    RadioGroupField,
    InputNumberField,
    TextAreaField
} from "components/common/FinalFormComponent";
import {Color} from "constants/common";
import {Link} from "react-router-dom";
import ListMyPost from "../Profile/ListMyPost";
import {CommonContext} from "context";
import fileApi from "api/fileApi"
import postApi from "api/postApi"
import moment from "moment";
import numeral from "numeral"
import {Categories} from "constants/common"
const ListPro = new Array(8).fill(0);
export default class extends React.Component {
    static contextType = CommonContext;
    state = {
        active: 1,
        data: {},
        list: [],
        loading: true
    };
    componentDidMount = () => {
        this.onSearch();
        setTimeout(() => {
            this.getPost();
        }, 500);
    }
    onSubmit = values => {
        this.onSave(values);
    };
    savePost = async (id, isCare) => {
        let { filter } = this.state;
        let { onSearch } = this.props;
        let { loading, unloading, error, success, profile } = this.context;
        if (!profile || (profile && !profile.id)) {
            error("Bạn cần đăng nhập để thực hiện chức năng này");
            return;
        }
        loading();
        let model = { postId: id, isCaring: !isCare }
        let res = await postApi.caring(model);
        unloading();
        if (res && res.success) {
            success(!isCare ? "Lưu tin thành công." : "Bỏ lưu tin thành công.");
            onSearch();
        } else {
            error(res ? res.message : "", "error");
        }
    };
    getPost = async () => {

        let {id} = this.props.match.params;
        let filter = {
            status: 5,
            orderByLowestPrice: true,
            sortDesc: true,
            pageIndex: 1,
            pageSize: 50,
            shopPageId: id
        }
        let {loading, unloading, error, success,profile} = this.context;
        let myProfile = localStorage.getItem("profile");
		if(!profile)profile = JSON.parse(myProfile);
		if (profile && profile.id) {
			filter.userCurrent = profile.id
		};

        loading();
        let res = await postApi.list(filter);
        unloading();
        if (res && res.success) {
            let data = res.data.records;
            this.setState({list: data, loading: false})
        } else {
            error(
                res
                ? res.message
                : "",
            "error");
        }
    }
    handleSelect = active => {
        if (active === 4)
            this.props.history.push("/app");
        this.setState({active});
    };
    onPressEnter = e => {};
    saveImage = async (e) => {
        let {id} = this.props.match.params;
        const file = e.target.files[0];
        let res = await fileApi.save(file, '', id);
        if (res && res.success)
            this.onSearch();
        }
    onSearch = async () => {
        let {id} = this.props.match.params;
        let {loading, unloading, error, success} = this.context;
        loading();
        let res = await postApi.getByIdStore(id);
        unloading();
        if (res && res.success) {
            let {data} = res;
            this.setState({data})
        } else {
            error(
                res
                ? res.message
                : "",
            "error");
        }
    }

    gotoDetail = (item) => {
        let type = Categories.find(i => i.value == item.categoryId).type || Categories[0].type
        this.props.history.push(`/product/${type}/${item.id}`);
    };
    render() {
        const {active, data, isEdit, list, loading} = this.state;
        let {id} = this.props.match.params;
        const {profile} = this.context;
        let initValues = {
            ...data,
            mobile: profile
                ? profile.mobile
                : ''
        };
        return (<> < Container > <div style={{
                width: "100%",
                height: 300,
                position: 'relative'
            }}>
            <img src={data.imageId
                    ? fileApi.getImage(data.imageId, 800, 400)
                    : `/assets/images/store.jpg`} alt="Product Name" style={{
                    width: "100%",
                    height: 350
                }}/>
        </div>
        <Row>
            <Col xl="4" className="mb-4">
                <div className="category leftbar m-0 bg-white " style={{
                        borderRadius: 26
                    }}>
                    <span className="title  text-16">{initValues.name}</span>
                    <div className="d-flex  w-100 mb-4 justify-content-between">
                        <span style={{
                                fontWeight: "bold",
                                color: "#8e8e93"
                            }}>
                            Số điện thoại:{" "}
                        </span>
                        <span style={{
                                fontWeight: "bold"
                            }}>{profile && profile.mobile}</span>
                    </div>

                    <div className="d-flex  w-100 mb-2 justify-content-between">
                        <span style={{
                                fontWeight: "bold",
                                color: "#8e8e93"
                            }}>
                            Giới thiệu:{" "}
                        </span>
                    </div>
                    <span>
                        {initValues.description}
                    </span>
                </div>
            </Col>
            <Col className="order-xl-2 mb-5 mb-xl-0 mb-4" xl="8">
                <Card className="card-profile shadow" style={{
                        borderRadius: 31,
                        minHeight: window.innerHeight * 0.8
                    }}>
                    <CardBody>
                        <div className="products-list">
                            {!loading && list && list.length === 0 && <Col><Message showIcon="showIcon" type="info" description="Cửa hàng chưa có sản phẩm nào."/></Col>}
                            <Row>
                                {
                                    list.map((i, index) => {
                                        let {
                                            listImageIds = []
                                        } = i;
                                        let image = (listImageIds && listImageIds[0])
                                            ? postApi.getImage(listImageIds[0], 200, 200)
                                            : '';
                                        i.image = image;
                                        return (<Col xs={4}>
                                            <div >
                                                <div className="products">
                                                    <div className="thumbnail" onClick={() => this.gotoDetail(i)} style={{cursor:'pointer'}}>
                                                        {
                                                            i.listImageIds && i.listImageIds[0]
                                                                ? <img src={postApi.getImage(i.listImageIds[0])} alt="Product Name" style={{
                                                                            borderRadius: 15
                                                                        }}/>
                                                                : <img src={`/assets/images/noimage.png`} alt="Product Name" style={{
                                                                            borderRadius: 15,
                                                                            width: 200,
                                                                            height: 200
                                                                        }}/>
                                                        }
                                                    </div>
                                                    <div style={{
                                                            display: "block",
                                                            flexDirection: "column"
                                                        }} className="ml-2">
                                                        <div className="productname text-left">{i.name}</div>
                                                        <h4 className="price text-left">{numeral(i.price).format("0,0")}</h4>
                                                        <button className="button wishlist" type="button" style={{ background: i.isCare ? "#fe5252" : '', color: '#fff' }} onClick={() => { this.savePost(i.id, i.isCare) }}>
                                                            <i className="fa fa-heart-o" />
                                                        </button>
                                                        <span>{moment(i.createdDate).format("DD-MM-YYYY")}</span>
                                                    </div>
                                                </div>
                                            </div>
                                        </Col>)
                                    })
                                }
                            </Row>
                        </div>
                    </CardBody>
                </Card>
            </Col>
        </Row>
    </Container> < />);
    }
}
