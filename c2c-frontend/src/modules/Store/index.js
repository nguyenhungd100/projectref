import React, { useState, useEffect } from "react";

import {
	Card,
	CardHeader,
	CardBody,
	Input,
	Container,
	Row,
	Col
} from "reactstrap";

import { Avatar, Icon, ControlLabel, FormGroup, Button, ButtonToolbar, Nav, Message } from "rsuite";
import { Form, Field } from "react-final-form";
import {
	InputField,
	DateField,
	SelectField,
	RadioGroupField,
	InputNumberField,
	TextAreaField
} from "components/common/FinalFormComponent";
import { Color } from "constants/common";
import { Link } from "react-router-dom";
import ListMyPost from "../Profile/ListMyPost";
import { CommonContext } from "context";
import fileApi from "api/fileApi"
import postApi from "api/postApi"
import moment from "moment";
import numeral from "numeral"
import { Categories } from "constants/common"
const ListPro = new Array(8).fill(0);
export default class extends React.Component {
	static contextType = CommonContext;
	state = { active: 1, data: {}, list: [], loading: true };
	componentDidMount = () => {
		this.onSearch();
		setTimeout(() => {
			this.getPost();
		}, 500);
	}
	onSubmit = values => {
		this.onSave(values);
	};
	getPost = async () => {
		let { id } = this.props.match.params;
		let filter = {
			status: '',
			orderByLowestPrice: true,
			sortDesc: true,
			pageIndex: 1,
			pageSize: 50,
			shopPageId: id

		}
		let { loading, unloading, error, success } = this.context;
		loading();
		let res = await postApi.list(filter);
		unloading();
		if (res && res.success) {
			let data = res.data.records;


			this.setState({ list: data, loading: false })
		} else {
			error(res ? res.message : "", "error");
		}
	}
	handleSelect = active => {
		if (active === 4) this.props.history.push("/app");
		this.setState({ active });
	};
	onPressEnter = e => { };
	saveImage = async (e) => {
		let { id } = this.props.match.params;
		const file = e.target.files[0];
		let res = await fileApi.save(file, '', id);
		if (res && res.success) this.onSearch();
	}
	onSearch = async () => {
		let { id } = this.props.match.params;
		let { loading, unloading, error, success } = this.context;
		loading();
		let res = await postApi.getByIdStore(id);
		unloading();
		if (res && res.success) {
			let { data } = res;
			this.setState({ data })
		} else {
			error(res ? res.message : "", "error");
		}

	}
	onClickCamera = () => {
		if (this.inputImage) this.inputImage.click();
	}
	onSave = (values) => { console.log(values, "values"); };
	gotoDetail = (item) => {
		let type = Categories.find(i => i.value == item.categoryId).type || Categories[0].type
		this.props.history.push(`/product/${type}/${item.id}`);
	};
	render() {
		const { active, data, isEdit, list, loading } = this.state;
		let { id } = this.props.match.params;
		const { profile } = this.context;
		let initValues = { ...data, mobile: profile ? profile.mobile : '', };

		return (
			<>
				<Container>
					<div style={{ width: "100%", height: 300, position: 'relative' }}>
						<img
							src={data.imageId ? fileApi.getImage(data.imageId, 800, 400) : `/assets/images/store.jpg`}
							alt="Product Name"
							style={{ width: "100%", height: 350 }}
						/>
						<div className="profile-round-camera cursor-pointer" style={{ top: "100%", left: "95%" }} onClick={this.onClickCamera}>
							<Icon
								icon="camera"
								style={{ fontSize: 22, color: "#fff", backgroundColor: "#000" }}

							/>
						</div>
					</div>
					<input ref={refs => this.inputImage = refs} type={'file'} onChange={this.saveImage} hidden />
					<Row>
						{false &&
							<Col xl="4" className="mb-4">
								<div
									className="category leftbar m-0 bg-white "
									style={{ borderRadius: 26 }}
								>
									<Form onSubmit={this.onSubmit} initialValues={initValues}>
										{({
											handleSubmit,
											values,
											form: {
												mutators: { push, pop }
											},
											form
										}) => {
											return (
												<form onSubmit={handleSubmit} className="p-2">
													<span className="title  text-16">{initValues.name}
														{
															false &&
															<Button
																appearance="primary"
																style={{ float: "right" }}
																onClick={form.submit}
															>
																<Icon icon="edit" />
															</Button>
														}

													</span>

													<Row className="mb-4">
														<ControlLabel className="form-text-lable mb-2 col-5">
															Số điện thoại
													</ControlLabel>
														<span>{initValues.mobile}</span>
														{isEdit &&
															<Field
																name="mobile"
																className="col-7"
																component={InputField}
																placeholder="Số điện thoại"
															/>
														}
													</Row>
													<Row className="mb-4">
														<ControlLabel className="form-text-lable mb-2 col-5">
															Địa chỉ
													</ControlLabel>
														{isEdit &&
															<Field
																name="name"
																className="col-7"
																component={InputField}
																placeholder="Địa chỉ"
															/>
														}
													</Row>
													<Row className="mb-4">
														<ControlLabel className="form-text-lable mb-2 col-5">
															Giới thiệu
													</ControlLabel>
														<p>{initValues.description}</p>
														{isEdit &&
															<Field
																name="description"
																className="col-7"
																component={TextAreaField}
																placeholder="Giới thiệu"
															/>
														}
													</Row>
													<ButtonToolbar>
														<Button
															block

															style={{ float: "right", backgroundColor: Color.global, color: '#fff' }}
															onClick={() => { this.props.history.push(`/post-info/?${id}`) }}
														>
															<Icon icon="plus" /> Đăng bài
													</Button>
													</ButtonToolbar>
												</form>
											);
										}}
									</Form>

									{/* <span className="title  text-16">{data.name}</span>
								<div className="d-flex  w-100 mb-4 justify-content-between">
									<span style={{ fontWeight: "bold", color: "#8e8e93" }}>
										Số điện thoại:{}
									</span>
									<span style={{ fontWeight: "bold" }}>{profile && profile.mobile}</span>
								</div>
								<div className="d-flex  w-100 mb-4 justify-content-between">
									<span style={{ fontWeight: "bold", color: "#8e8e93" }}>Địa chỉ: </span>
									<span style={{ fontWeight: "bold" }}>{data.name}</span>
								</div>
								<div className="d-flex  w-100 mb-2 justify-content-between">
									<span style={{ fontWeight: "bold", color: "#8e8e93" }}>
										Giới thiệu:{" "}
									</span>
								</div>
								<span>
									{data.description}
								</span> */}
								</div>
							</Col>
						}

						<Col xl="4" className="mb-4">
							<div
								className="category leftbar m-0 bg-white "
								style={{ borderRadius: 26 }}
							>
								<span className="title  text-16">{initValues.name}</span>
								<div className="d-flex  w-100 mb-4 justify-content-between">
									<span style={{ fontWeight: "bold", color: "#8e8e93" }}>
										Số điện thoại:{" "}
									</span>
									<span style={{ fontWeight: "bold" }}>{profile && profile.mobile}</span>
								</div>
								{false &&
									<div className="d-flex  w-100 mb-4 justify-content-between">
										<span style={{ fontWeight: "bold", color: "#8e8e93" }}>Danh mục: </span>
										<span style={{ fontWeight: "bold" }}>số 1 Hoàng Hoa Thám , Hà nội</span>
									</div>
								}
								<div className="d-flex  w-100 mb-2 justify-content-between">
									<span style={{ fontWeight: "bold", color: "#8e8e93" }}>
										Giới thiệu:{" "}
									</span>
								</div>
								<span>
									{initValues.description}
								</span>
								<ButtonToolbar>
									<Button
										block
										style={{ float: "right", backgroundColor: Color.global, color: '#fff' }}
										onClick={() => { this.props.history.push(`/post-info/?${id}`) }}
									>
										<Icon icon="plus" /> Đăng bài
													</Button>
								</ButtonToolbar>
							</div>
						</Col>


						<Col className="order-xl-2 mb-5 mb-xl-0 mb-4" xl="8">
							<Card className="card-profile shadow" style={{ borderRadius: 31, minHeight: window.innerHeight * 0.8 }}>
								<CardBody>
									<div className="products-list">
										{!loading && list && list.length === 0 && <Col><Message showIcon type="info" description="Cửa hàng chưa có sản phẩm nào." /></Col>}

										<Row>
											{list.map((i, index) => {

												let { listImageIds = [] } = i;
												let image = (listImageIds && listImageIds[0]) ? postApi.getImage(listImageIds[0], 200, 200) : '';
												i.image = image;

												return (<Col xs={4} >
													<div onClick={() => this.gotoDetail(i)}>
														<div className="products">
															<div className="thumbnail" onClick={() => this.gotoDetail(i)}>
																{i.listImageIds && i.listImageIds[0] ?

																	<img
																		src={postApi.getImage(i.listImageIds[0])}
																		alt="Product Name"
																		style={{ borderRadius: 15 }}
																	/>
																	:
																	<img
																		src={`/assets/images/noimage.png`}
																		alt="Product Name"
																		style={{ borderRadius: 15, width: 200, height: 200 }}
																	/>
																}
															</div>

															<div
																style={{
																	display: "block",
																	flexDirection: "column",
																}}
																className="ml-2"
															>
																<div className="productname text-left">{i.name}</div>
																<h4 className="price text-left">{numeral(i.price).format("0,0")}</h4>
																<span>{moment(i.createdDate).format("DD-MM-YYYY")}</span> <span style={{ color: i.status == 1 ? "blue" : i.status == 5 ? '#ccc' : 'red', fontWeight: "bold" }}>|{i.status == 1 ? "Chờ duyệt" : i.status == 5 ? "Đã duyệt" : "Bị từ chối"}</span>

															</div>
														</div>
													</div>
												</Col>)
											}
											)}

										</Row>
									</div>
								</CardBody>
							</Card>
						</Col>
					</Row>
				</Container>
			</>
		);
	}
}
