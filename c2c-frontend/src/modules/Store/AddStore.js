import React, { useState, useEffect, useContext } from "react";

import {
    Card,
    CardBody,
    Row,
    Col
} from "reactstrap";

import { Icon, ControlLabel, Button, Nav, Placeholder, Message } from "rsuite";
import { Form, Field } from "react-final-form";
import {
    InputField,
    DateField,
    SelectField,
    RadioGroupField,
    RSTextAreaField
} from "components/common/FinalFormComponent";
import { Cities, Color, Categories } from "constants/common";
import PropTypes from 'prop-types'
import { CommonContext, AuthContext } from "context";
import postApi from "api/postApi";
import { useHistory } from "react-router-dom";
import { log } from "util";

const { Paragraph } = Placeholder;
function AddStore(props) {
    const history = useHistory();
    const contextApi = useContext(CommonContext) || {};
    const [loading, setLoading] = useState(true)
    async function onSubmit(values) {
        let { loading, unloading, error, success, setShopage, getProfile } = contextApi
        loading();
        let res = await postApi.addStore(values);
        unloading();
        if (res && res.success) {
            success("Tạo cửa hàng thành công.");
            if (setShopage) setShopage();
            if (getProfile) getProfile();
            if (res.data) history.push(`/user/stores/${res.data}`)
        } else {
            error(res ? res.message : "", "error");
        }
    }
    useEffect(() => {
        setLoading(false)
    }, [])
    return (
        <>
            {loading ?
                < Paragraph style={{ height: 300 }} rows={5} rows={5} graph active />
                :
                <>
                    <Row className="justify-content-center">
                        <div className="profile-round">
                            <img src="/assets/images/product/MT-03/1.png" />
                        </div>
                        <div className="profile-round-camera">
                            <Icon
                                icon="camera"
                                style={{ fontSize: 22, color: "#fff", backgroundColor: "#000" }}
                            />
                        </div>
                    </Row>
                    <CardBody
                        className="pt-0 pt-md-4"
                        style={{ backgroundColor: "#f7fafc", borderRadius: 31 }}
                    >
                        <Col><Message showIcon type="info" description="Bạn sẽ bị trừ 5,000 đ khi tạo cửa hàng." /></Col>
                        <Form onSubmit={onSubmit} >
                            {({
                                handleSubmit,
                                values,
                                form: {
                                    mutators: { push, pop }
                                },
                                form
                            }) => {
                                return (
                                    <form onSubmit={handleSubmit} className="p-4">
                                        <Row className="mb-4">
                                            <Col sm="6">
                                                <ControlLabel className="form-text-lable mb-2">
                                                    Tên cửa hàng
                                        </ControlLabel>
                                                <Field
                                                    name="name"
                                                    className="col-12"
                                                    component={InputField}
                                                    placeholder="Tên cửa hàng"
                                                />
                                            </Col>
                                            <Col sm="6">
                                                <ControlLabel className="form-text-lable mb-2">
                                                    Danh mục
                                        </ControlLabel>
                                                <Field
                                                    name="categoryId"
                                                    component={SelectField}
                                                    placeholder="Nhóm sản phẩm"
                                                    valueKey="value"
                                                    labelKey="title"
                                                    data={Categories}
                                                    block
                                                />
                                            </Col>
                                        </Row>
                                        <Row className="mb-4">
                                            <Col sm="12">
                                                <ControlLabel className="form-text-lable mb-2">
                                                    Mô tả
                                                </ControlLabel>
                                                <Field
                                                    name="description"
                                                    component={RSTextAreaField}
                                                    placeholder="Nội dung mô tả"
                                                    rows={4}
                                                    style={{ resize: "auto", width: "100%", border: "1px solid #cccccc !important" }}
                                                    className=" p-0"
                                                    block
                                                />
                                            </Col>
                                        </Row>

                                        <Button
                                            style={{
                                                background: Color.gGlobal,
                                                color: "#fff",
                                                float: "right"
                                            }}
                                            onClick={form.submit}
                                        >
                                            <Icon icon="save" /> Lưu lại
                                </Button>
                                    </form>
                                );
                            }}
                        </Form>
                        <div className="text-center" />
                    </CardBody>
                </>
            }
        </>

    )
}

export default AddStore
