import React, { Component } from "react";

class SlideShow extends Component {
	render() {
		return (
			<div className="hero-content  pt-2 pb-2 pr-4 pl-4">
				<div className="container">
					<div className="row">
						<div className="col-md-8">
							<h1 className="bold-headline margin-top-0 hero-item slideshow">
								Luôn luôn lắng nghe, luôn luôn thấu hiểu
							</h1>
							<div className="hero-item text-left slideshow">
								<h3>Chợ điện tử mang đến những giá trị đích thực</h3>
							</div>
						</div>
						<div className="col-md-4 ">
							<img src="/assets/images/bg1.png" style={{ height: 282 }} />
						</div>
					</div>
				</div>
			</div>
		);
	}
}
export default SlideShow;
