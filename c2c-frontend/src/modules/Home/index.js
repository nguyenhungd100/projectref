import React, { useState } from "react";
import { Link } from "react-router-dom";
import SlideShow from "./SlideShow";
import { Categories } from "constants/common";
import categoryApi from "api/categoryApi";
import { CommonContext } from "context";
import Chat from "chat"
import { MessageBox } from 'react-chat-elements'
class Home extends React.Component {
	static contextType = CommonContext;
	componentDidMount = async () => {
		let { loading, unloading, error, success } = this.context;
		loading();
		let res = await categoryApi.list();
		unloading();
	}
	render() {
		return (
			<div>
				<SlideShow />
				{false &&
					<Chat />
				}
				<div className="container_fullwidth">
					<div className="container">
						<div className="simple-cta pullup">▾ Khám phá danh mục ▾</div>
						<div className="row category">
							{Categories.map(i => (
								<div className={(i.isCol4 ? `col-md-4 ` : `col-md-3 `) + "col-sm-6"}>
									<Link to={`/product/${i.type}`}>
										<div
											className="products"
											style={{
												minHeight: 150,
												textAlign: "center",
												padding: 5,
												cursor: "pointer",
												color: "black",
												opacity: 0.8
											}}
										>
											<img src={i.image} alt={i.title} style={{ height: 100 }} />
											<h4
												style={{
													fontSize: 24,
													fontWeight: 700,
													opacity: 0.8,
													textDecoration: "none"
												}}
											>
												{i.title}
											</h4>
										</div>
									</Link>
								</div>
							))}
						</div>
					</div>
				</div>
			</div>
		);
	}
}
export default Home;
