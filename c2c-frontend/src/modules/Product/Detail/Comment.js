import React, { useState, useEffect, useContext } from "react";
import {
    Icon,
    Avatar,
    Button,
    FormGroup,
    ControlLabel,
    ButtonToolbar,
    IconButton,
    Divider,
    Input,
} from "rsuite";
import { CommonContext } from "context";
import moment from "moment";
import commentApi from "api/commentApi"
export default function Comment(props) {
    const contextApi = useContext(CommonContext);
    const [comments, setComments] = useState([]);
    const [content, setContent] = useState("");


    useEffect(() => {
        getData();
    }, []);

    const getData = async () => {
        if (props.id) {
            let { profile } = contextApi;
            let model = { postId: props.id, userCurrentId: profile && profile.id };
            let res = await commentApi.list(model);
            if (res && res.success) {
                if (res.data) setComments(res.data.sort((a, b) => {
                    a = new Date(a.createdDate);
                    b = new Date(b.createdDate);
                    return a > b ? -1 : a < b ? 1 : 0;
                }))
            }
        }

    };
    const onSubmit = async () => {
        let { profile, success, error } = contextApi;
        if (!profile || !profile.id) return error("Vui lòng đăng nhập để có thể sử dụng chức năng này.")
        if (!content) return;
        let model = { postId: props.id, content };
        let res = await commentApi.save(model);
        if (res && res.success) {
            success("Cảm ơn bạn đã để lại bình luận.");
            setContent("");
            getData();
        } else error("Hệ thống có lỗi xảy ra, vui lòng thử lại.")
    }
    const ItemComment = ({ data }) => {
        return (
            <div className="mb-4">
                <div className="d-flex">
                    <Avatar
                        circle
                        src="https://avatars2.githubusercontent.com/u/12592949?s=460&v=4"
                    />
                    <strong className="ml-2">{data.createdByName}</strong>
                </div>
                <p style={{ paddingLeft: 50, fontSize: 13, lineHeight: "24px" }}>{data.content}</p>
                <div style={{ paddingLeft: 50, float: 'left' }}>
                    <Divider vertical />
                    <a style={{ fontSize: "smaller" }}>{moment(data.createdDate).format("DD-MM-YYYY")}</a>

                </div>
            </div>
        )
    }
    return (
        <>
            <div className="mb-4" style={{ border: '1px solid #ccc', borderRadius: 10, }}>
                <div style={{ width: "100%", }}>
                    <Input
                        componentClass="textarea"
                        value={content}
                        rows={3}
                        placeholder="Nhập nội dung và enter"
                        onChange={value => {
                            setContent(value)
                        }}
                        style={{ minWidth: '100%', borderRightWidth: 0, borderTopWidth: 0, borderLeftWidth: 0, borderBottom: '1px solid #ccc' }}
                        className="col-12 mr-3 textarea-customs"
                    />
                </div>
                <div style={{ padding: 10 }}>
                    <ButtonToolbar className="d-flex" style={{ justifyContent: "flex-end" }}>
                        <Button color="orange" onClick={onSubmit}> Gửi </Button>
                    </ButtonToolbar>

                </div>
            </div>
            {
                comments.map((i, index) => (
                    <ItemComment key={index} data={i} />
                ))
            }

        </>
    );
}
