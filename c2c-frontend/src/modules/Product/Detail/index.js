import React, { Component } from "react";
import {Link} from 'react-router-dom'
import ImageGallery from "react-image-gallery";
import "react-image-gallery/styles/css/image-gallery.css";
import {
	Avatar,
	Icon,
	RadioGroup,
	Radio,
	Placeholder,
	Button,
	IconButton,
	ButtonToolbar, Modal, Input, Notification
} from "rsuite";
import SimpleMap from "./SimpleMap";
import Comment from "./Comment"
import Image3d from "components/common/Image3d"
import postApi from "api/postApi";
import { CommonContext } from "context";
import numeral from "numeral";
import moment from "moment";
import { mydatabase } from 'components/configFirebase/firebase';
import { log } from "util";
import {Color} from "constants/common"
const { Paragraph } = Placeholder;
class ProductDetail extends Component {
	static contextType = CommonContext;
	constructor(props) {
		super(props)

		this.state = {
			model: {
				reason: ""
			}
		}
		this.close = this.close.bind(this);
		this.open = this.open.bind(this);
	}
	close() {
		this.setState({ show: false });
	}
	open() {
		this.setState({ show: true });
	}
	state = { view: "slide", data: {}, images: [], loading: true, show: false };
	messageRef = mydatabase.ref(`Converation/messages`);
	topicRef = mydatabase.ref(`Converation/topics`);
	componentDidMount = () => {
		let { id, type } = this.props.match.params;
		this.onSearch(id);
		this.seTime = setTimeout(() => {
			this.setState({ loading: false })
		}, 800);
	}
	componentWillUnmount = () => {
		clearTimeout(this.seTime)
	}

	onSearch = async (id) => {
		let { loading, unloading, error, success } = this.context;
		loading();
		let res = await postApi.getById(id);
		unloading();
		if (res && res.success) {
			let { data = {} } = res;
			let { listImageIds = [] } = data;
			let images = listImageIds.map(i => {
				return {
					original: postApi.getImage(i, 500, 350),
					thumbnail: postApi.getImage(i, 100, 100)
				}
			});
			let image3D = listImageIds.map(i => {
				return {
					original: postApi.getImage(i),
					thumbnail: postApi.getImage(i)
				}
			});
			this.setState({ data, images, image3D })

		} else {
			error(res ? res.message : "", "error");
		}
	};
	handleChange = (view) => {
		this.setState({ view })
	}
	initTopic = topicModel => {
		const { profile } = this.context;
		const { topicKey, messageKey, userName } = topicModel;
		const update = {
		};
		let model = {
			userId: topicModel.userId,
			createdImageId: profile.imageId,
			messageKey,
			createdDate: new Date(),
			createdId: profile.id,
			[profile.id]: profile.id,
			[topicModel.userId]: topicModel.userId,
			createnName: profile.fullName,
			userName,
			topicKey
		}

		update[`Converation/topics/${topicKey}`] = model;
		mydatabase.ref().update(update);
	};

	initMessage = newMessageKey => {
		const { profile } = this.context;
		const initMessageModel = {
			sender: {
				name: profile.fullName,
				imageId: profile.imageId,
				id: profile.id
			},
			senderId: profile.id,
			createdDate: new Date(),
			content: "Xin chào, tôi muốn liên hệ với bạn",
		};
		this.messageRef.child(newMessageKey).push(initMessageModel);
	};
	onAddTopic = topicModel => {

		const newMessageKey = this.messageRef.push().key;
		const newTopicKey = this.topicRef.push().key;
		topicModel.messageKey = newMessageKey;
		topicModel.topicKey = newTopicKey;
		this.initMessage(newMessageKey);
		this.initTopic(topicModel);

	};

	onShowMessage = async (data) => {
		const { profile, openChat } = this.context;
		if (profile && profile.id && profile.id !== data.createdBy) {
			this.topicRef
				.orderByChild(profile.id.toString())
				.equalTo(profile.id)
				.on("value", topicsObj => {
					if (!topicsObj.val()) {
						this.onAddTopic({ userName: data.createdUserName, userId: data.createdBy });
						setTimeout(() => {
							openChat();
						}, 200);
						return;
					}
					const topics = Object.values(topicsObj.val());
					let item = topics.find(i => i[data.createdBy]);
					if (!item)
						this.onAddTopic({ userName: data.createdUserName, userId: data.createdBy });

					setTimeout(() => {
						openChat();
					}, 200);
					return;
				});
		}
	}
	onDenounce = async () => {
		let { model } = this.state;
		let { id } = this.props.match.params;
		model.postId = id;

		let res = await postApi.denounce(model)
		if (res && res.success) {
			this.close();
			Notification.success({
				title: "Thông báo",
				description: "Báo cáo tin đăng thành công."
			});
		}
		else {
			Notification.error({
				title: "Có lỗi xảy ra",
				description: res && res.message,
			});
		}
	}
	renderModal = () => {
		return (
			<div className="modal-container">
				<Modal show={this.state.show} onHide={this.close}>
					<Modal.Header>
						<Modal.Title>Bài đăng này có vấn đề gì?</Modal.Title>
					</Modal.Header>
					<Modal.Body>
						<Input componentClass="textarea"
							rows={3}
							onChange={(text) => this.setState({ model: { ...this.state.model, reason: text } })}
							style={{ width: "100%" }}
							placeholder="Mô tả..." />
					</Modal.Body>
					<Modal.Footer>
						<Button onClick={this.onDenounce} appearance="primary">
							Gửi đi
            </Button>
						<Button onClick={this.close} appearance="subtle">
							Hủy
            </Button>
					</Modal.Footer>
				</Modal>
			</div>
		)
	}


	render() {
		let { view, data = {}, images, image3D, loading } = this.state;
		const isAuth = localStorage.getItem("token");
		return (
			<div className="container_fullwidth">
				<div className="container">
					{!loading ?
						<div class="row">
							<div className="col-8 no-border" style={{ paddingBottom: 10 }}>
								<RadioGroup
									inline
									name="radioList"
									value={this.state.view}
									onChange={this.handleChange}
									appearance="picker"
								>
									<Radio value="slide" className="border-right">{("Danh sách")}</Radio>
									<Radio value="3d">{("Dạng 3D")}</Radio>

								</RadioGroup>
								<div className="mt-3">
									{
										view === "3d" ?
											<Image3d data={image3D} />
											:
											<ImageGallery items={images} thumbnailPosition={"right"} autoPlay={true} isRTL={true} showThumbnails={true} infinite={false} useTranslate3D={true} lazyLoad={true} />
									}
								</div>
							</div>
							<div className="col-sm-4 mt-3">
								<div class="products-description" style={{ marginTop: 34 }}>
									<div className="d-flex justify-content-between" >
										<div className="d-flex">
											<Avatar
												circle
												src= {data.createdImageId?postApi.getImage(data.createdImageId, 50, 50):"https://avatars2.githubusercontent.com/u/12592949?s=460&v=4"}
											/>
											<div style={{ flexDirection: "column", paddingLeft: 10 }}>
												<h6 className="pr-3">{data.shopPageNameInUser?data.shopPageNameInUser:data.createdUserName}</h6>
												<span className="pr-3">{moment(data.createdDate).format("DD-MM-YYYY")}</span>
											</div>

										</div>
										{data.shopPageIdInUser &&
											<div>
												<Link to={`/products/stores/${data.shopPageIdInUser}`}> <Button appearance="ghost" style={{color: Color.global,borderColor: Color.global}}>Xem trang</Button>
												</Link>
											</div>
										}
									</div>
									<h5 class="name" />
									<div className="row text-center">
										<div className="col-sm-4 align-items-center">
											<p>{data.shopPageName ? data.shopPageName : "Cá nhân"}</p>
											<Icon icon="user" />
										</div>
										<div className="col-sm-4">
											<p>Đánh giá</p>
											<img alt="" src="/assets/images/star.png" />
										</div>
										<div className="col-sm-4">
											<p>Phản hồi</p>
											<p>0 %</p>
										</div>
									</div>
									<hr class="border" />
									<div class="price">
										<div className="d-flex">
											<Icon
												icon="phone-square"
												style={{ color: "#3fbaae", marginRight: 10, fontSize: 24 }}
											/>
											<h5 style={{ color: "#3fbaae" }}>{data.mobileUser}</h5>
										</div>
									</div>
									<hr class="border" />
									<div class="wided">
										<div className="d-flex cursor-pointer" onClick={() => { this.onShowMessage(data) }}>
											<Icon
												icon="comments"
												style={{ color: "#3fbaae", marginRight: 10, fontSize: 24 }}
											/>
											<h5 style={{ color: "#3fbaae", fontWeight: "bold" }}>
												Liên hệ
										</h5>
										</div>
									</div>
								</div>
							</div>
							<div className="col-sm-8">
								<div class="products-description">
									<h3 class="name">{data.name}</h3>
									<p>
										<img alt="" src="/assets/images/star.png" />
										<a class="review_num" href="#">
											{moment(data.createdDate).format("DD-MM-YYYY")}
										</a>
									</p>

									<h5>
										{data.content}
									</h5>
									<hr class="border" />
									<div class="price">
										<h6>Giá:
										<span class="new_price">
												{numeral(data.price ? data.price : 0).format('0,00')}
												<sup>đ</sup>
											</span>
										</h6>
									</div>
									<hr class="border" />
									<div class="d-flex">
										<Icon
											icon="map"
											style={{ color: "#3fbaae", marginRight: 10, fontSize: 16, padding: 0, marginBottom: 10 }}
										/>
										<h6>Khu vực: {data.provinceName}</h6>
									</div>
									{isAuth &&
										<><hr class="border" />
											<span><em>Tin đăng này đã được kiểm duyệt. Nếu gặp vấn đề, vui lòng báo cáo tin đăng hoặc liên hệ CSKH để được trợ giúp.</em></span>
											<div class="d-flex justify-content-center">
												<IconButton onClick={this.open} icon={<Icon icon="exclamation-triangle" />} color="red" appearance="ghost">Báo cáo tin đăng</IconButton>
											</div>
										</>}
									<hr class="border" />
									<h6 style={{ fontWeight: 'bold', paddingBottom: 10 }}>Hỏi và đáp</h6>
									<Comment id={data.id} />
								</div>
							</div>
						</div>
						: <Paragraph style={{ height: 500 }} rows={5} rows={5} graph active />
					}
					{this.state.show && this.renderModal()}
				</div>
			</div>
		);
	}
}
export default ProductDetail;
