import React, { Component } from "react";
import { Categories } from "constants/common";
import { Link, Switch, Route } from "react-router-dom";
import { Nav, Dropdown, Row, Col, Icon, Pagination } from "rsuite";
import { CommonContext } from "context";
import ListProduct from "./ListProduct";
import postApi from "api/postApi";
import Filter from "./Filter";
import { Color } from "constants/common"
const CustomNav = ({ active, onSelect, ...props }) => {
	return (
		<Nav {...props} vertical activeKey={active} onSelect={onSelect}>
			{Categories.map(i => (
				<Nav.Item key={i.type} eventKey={i.type} icon={<Icon icon={i.icon} style={{ color: active === i.type ? Color.global : "unset" }} />}>
					<span className="text-16" style={{ color: active === i.type ? Color.global : "unset" }} >{i.title}</span>
				</Nav.Item>
			))}
		</Nav>
	);
};
class Navigation extends Component {
	static contextType = CommonContext;
	state = {
		type: "",
		viewList: "list",
		filter: {
			categoryId: 0,
			status: 5,
			orderByLowestPrice: null,
			sortDesc: true,
			pageIndex: 1,
			pageSize: 100
		},
		data: []
	};
	componentDidMount = () => {
		this.onGetTye(this.props);

	};
	onGetTye = (props) => {
		const { match } = props;
		const { params = {} } = match;
		let item = this.onGetTitle(params.type);
		this.state.filter.categoryId = item.value;
		this.setState({ type: params.type, title: item.title });
		this.onSearch();
	}

	onSearch = async () => {
		let { filter } = this.state;
		let { loading, unloading, error, success, profile,setProfile } = this.context;
		let myProfile = localStorage.getItem("profile");
		if(!profile)profile = JSON.parse(myProfile);
		if (profile && profile.id) {
			filter.userCurrent = profile.id
			setProfile(profile)
		};
		loading();
		let res = await postApi.list(filter);
		unloading();
		if (res && res.success) {
			let data = res.data.records;
			this.setState({ data })
		} else {
			error(res ? res.message : "", "error");
		}
	};
	onView = view => {
		this.setState({ viewList: view });
	};
	gotoDetail = (id) => {
		const { type } = this.state;
		this.props.history.push(`${type}/${id}`);
	};
	handleSelect = (type) => {
		this.props.history.replace(`${type}`)
	}
	onGetTitle = (type) => {
		let item = Categories.find(i => i.type === type);
		return item ? item : Categories[0]
	}
	onChangeFilter = (name, value) => {
		this.state.filter[name] = value
		this.onSearch()
	}

	componentWillReceiveProps = (nextprops) => {
		if (nextprops && nextprops.location && this.props.location.pathname !== nextprops.location.pathname)
			this.onGetTye(nextprops)

	}
	render() {
		const { type, title, data } = this.state;
		return (
			<div className="container_fullwidth">
				<div className="container">
					<div className="row">
						<div className="col-md-3">
							<div className="category leftbar">
								<span className="title p-3 text-16">Danh mục</span>
								<CustomNav
									reversed
									appearance="subtle"
									active={type}
									onSelect={this.handleSelect}
								/>
							</div>
							<div className="leftbanner">
								<img src="/assets/images/friday2.jpg" alt="" />
							</div>
						</div>
						<div className="col-md-9">
							<div className="clearfix" />
							<h1 className="text-28">{title}</h1>
							<Filter onChange={this.onChangeFilter} />
							<ListProduct data={data} onClick={this.gotoDetail} onSearch={this.onSearch} />
						</div>
					</div>
					<div className="clearfix" />
				</div>
			</div>
		);
	}
}
export default Navigation;
