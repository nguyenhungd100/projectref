import React, { Component } from "react";
import { Link } from "react-router-dom";
import arrayMutators from "final-form-arrays";

import { Form, Field } from "react-final-form";
import {
	InputField,
	DateField,
	SelectField,
	InputNumberField,
	CurrencyField
} from "components/common/FinalFormComponent";
import {
	Table,
	Modal,
	Button,
	FormGroup,
	ControlLabel,
	Icon,
	Notification,
	Row,
	Col,
	Input
} from "rsuite";
import { Cities } from "constants/common"

export default class Filter extends Component {
	onSubmit = () => { };

	onChange = (name, value) => {
		const { onChange } = this.props;
		clearTimeout(this.change)
		this.change = setTimeout(() => {
			onChange(name, value)
		}, 1500);
	}
	componentWillMount = () => {
		clearTimeout(this.change)
	};
	render() {

		return (
			<div className="p-3" style={{ border: "1px solid #e1e1e1", borderRadius: 10 }}>
				<Form
					onSubmit={this.onSubmit}
					mutators={{
						...arrayMutators
					}}

				>
					{({
						handleSubmit,
						pristine,
						invalid,
						values,
						form: {
							mutators: { push, pop }
						},
						form
					}) => {
						this.form = form;

						return (
							<form onSubmit={handleSubmit}>
								<div className="row">
									<Field
										name="provinceId"
										component={SelectField}
										placeholder="Khu vực"
										data={Cities.map(i => { i.label = i.name; return i })}
										block
										style={{ borderColor: "#cccccc" }}
										className="col-sm-3"
										handleOnChange={(value) => { this.onChange("provinceId", value) }}
									/>
									<Field
										name="priceMin"
										component={CurrencyField}
										placeholder="Giá tối thiểu"
										thousandSeparator={true}
										suffix={' đ'}
										max={values.priceMax}
										className="pr-2 pl-2 mr-3"
										style={{ width: '20%', borderRadius: 5 }}
										handleOnChange={(value) => { this.onChange("priceMin", value) }}
									/>
									<Field
										name="priceMax"
										component={CurrencyField}
										thousandSeparator={true}
										suffix={' đ'}
										placeholder="Giá tối đa"
										max={values.priceMax}
										className="pr-2 pl-2"
										style={{ width: '20%', borderRadius: 5 }}
										handleOnChange={(value) => { this.onChange("priceMax", value) }}

									/>

									<Field
										name="orderByLowestPrice"
										component={SelectField}
										placeholder="Lọc theo giá"
										data={[
											{ value: true, label: "Giá tăng dần" },
											{ value: false, label: "Giá giảm dần" }
										]}
										handleOnChange={(value) => { this.onChange("orderByLowestPrice", value) }}
										block
										className="col-3"
									/>
								</div>
							</form>
						);
					}}
				</Form>
			</div>
		);
	}
}
