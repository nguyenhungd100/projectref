import React, { Component } from "react";
import { Categories } from "constants/common";
import { Link } from "react-router-dom";
import { Nav, Dropdown, Row, Col, Icon, Pagination } from "rsuite";
import postApi from "api/postApi"
import { CommonContext } from "context";
import { Color } from "constants/common"
import numeral from "numeral"
const styles = { width: 100 };

class ListProduct extends Component {
	static contextType = CommonContext;
	state = {
		type: "",
		viewList: "list"
	};
	componentDidMount = () => { };
	getImage = (id) => {
		return postApi.getImage(id, 200, 200)
	}
	onView = view => {
		this.setState({ viewList: view });
	};
	gotoDetail = (id) => {
		this.props.onClick(id);
	};

	savePost = async (id, isCare) => {
		let { filter } = this.state;
		let { onSearch } = this.props;
		let { loading, unloading, error, success, profile } = this.context;
		if (!profile || (profile && !profile.id)) {
			error("Bạn cần đăng nhập để thực hiện chức năng này");
			return;
		}
		loading();
		let model = { postId: id, isCaring: !isCare }
		let res = await postApi.caring(model);
		unloading();
		if (res && res.success) {
			success(!isCare ? "Lưu tin thành công." : "Bỏ lưu tin thành công.");
			onSearch();
		} else {
			error(res ? res.message : "", "error");
		}
	};
	render() {
		const { data = [] } = this.props;
		const { type, viewList } = this.state;
		return (
			<div className="products-list">
				<div className="toolbar">
					<div className="sorter">
						<div className="view-mode">
							<Link
								className={`list ${viewList === "list" ? "active" : ""}`}
								onClick={() => this.onView("list")}
							>
								List
							</Link>
							<Link
								className={`grid ${viewList !== "list" ? "active" : ""}`}
								onClick={() => this.onView("gird")}
							>
								Grid
							</Link>
						</div>
						{false && (
							<>
								<div className="sort-by">
									Sort by :
									<select name="">
										<option value="Default" selected>
											Default
										</option>
										<option value="Name">Name</option>
										<option value="Price">Price</option>
									</select>
								</div>
								<div className="limiter">
									Show :
									<select name="">
										<option value="3" selected>
											3
										</option>
										<option value="6">6</option>
										<option value="9">9</option>
									</select>
								</div>
							</>
						)}
					</div>

				</div>
				{viewList === "gird" ? (
					<Row>
						{data.map((i, index) => (
							<Col xs={8} key={index}>
								<div key={index} className="cursor-pointer">
									<div className="products">
										<div className="thumbnail" onClick={() => this.gotoDetail(i.id)}>
											<Link to="/">
												{i.listImageIds && i.listImageIds[0] ?

													<img
														src={this.getImage(i.listImageIds[0])}
														alt="Product Name"
														style={{ borderRadius: 15 }}
													/>
													:
													<img
														src={`/assets/images/noimage.png`}
														alt="Product Name"
														style={{ borderRadius: 15, width: 200, height: 200 }}
													/>
												}
											</Link>
										</div>
										<div className="productname" onClick={() => this.gotoDetail(i.id)}>{i.name}</div>
										<h4 className="price">{numeral(i.price ? i.price : 0).format('0,00')}
											<sup>đ</sup></h4>
										<div className="button_group d-flex justify-content-between">
										{false &&
											<button className="button add-cart" type="button">
												Liên hệ
											</button>
										}
											<button className="button wishlist" type="button" style={{ background: i.isCare ? "#fe5252" : '', color: '#fff' }} onClick={() => { this.savePost(i.id, i.isCare) }}>
												<i className="fa fa-heart-o" />
											</button>
										</div>
									</div>
								</div>
							</Col>
						))}
					</Row>
				) : (
						<ul className="products-listItem">
							{data.map((i, index) => (
								<li className="products cursor-pointer" key={index} >
									{false && <div className="offer">Liên hệ</div>}
									<div className="thumbnail" onClick={() => this.gotoDetail(i.id)}>
										{i.listImageIds && i.listImageIds[0] ?

											<img
												src={this.getImage(i.listImageIds[0])}
												alt="Product Name"
												style={{ borderRadius: 15 }}
											/>
											:
											<img
												src={`/assets/images/noimage.png`}
												alt="Product Name"
												style={{ borderRadius: 15, width: 200, height: 200 }}
											/>
										}
									</div>
									<div className="product-list-description">
										<div className="productname" onClick={() => this.gotoDetail(i.id)}>{i.name}</div>
										<p>
											<img src="/assets/images/star.png" alt="" />
										</p>
										<p style={{ fontSize: 13, lineHeight: "24px" }}>
											{i.content}
										</p>
										<div className="list_bottom d-flex justify-content-between">
											<div className="price">
												<span className="new_price">
													{numeral(i.price ? i.price : 0).format('0,00')}
													<sup>đ</sup>
												</span>
											</div>
											<div className="button_group ">
												<button className="button favorite" style={{ background: i.isCare ? "#fe5252" : '', color: '#fff' }} onClick={() => { this.savePost(i.id, i.isCare) }}>
													<i className="fa fa-heart-o" />
												</button>
											</div>
										</div>
									</div>
								</li>
							))}
						</ul>
					)
				}
				{
					data && data.length > 10 &&
					<div className="toolbar">
						<div className="sorter bottom">
							<div className="view-mode">
								<Link to="/" className="list active">
									List
							</Link>
								<Link to="/Đồ%20văn%20phòng" className="grid">
									Grid
							</Link>
							</div>
							{false && (
								<>
									<div className="sort-by">
										Sort by :
									<select name="">
											<option value="Default" selected>
												Default
										</option>
											<option value="Name">Name</option>
											<option value="Price">Price</option>
										</select>
									</div>
									<div className="limiter">
										Show :
									<select name="">
											<option value="3" selected>
												3
										</option>
											<option value="6">6</option>
											<option value="9">9</option>
										</select>
									</div>
								</>
							)}
						</div>
						<div className="pager">
							<Link to="/" className="prev-page">
								<i className="fa fa-angle-left" />
							</Link>
							<Link to="/" className="active">
								1
						</Link>
							<Link to="/">2</Link>
							<Link to="/">3</Link>
							<Link to="/" className="next-page">
								<i className="fa fa-angle-right" />
							</Link>
						</div>
					</div>
				}

			</div >
		);
	}
}
export default ListProduct;
