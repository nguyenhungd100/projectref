import React from "react";
import {
    Card,
    CardHeader,
    CardFooter,
    Container,
    Row,
} from "reactstrap";
import {
    Table,
    Modal,
    Button,
    FormGroup,
    ControlLabel,
    Icon,
    Notification,
    ButtonToolbar,
    IconButton,
    Whisper, Popover
} from "rsuite";

import { Form, Field } from "react-final-form";
import {
    InputField,
    DateField,
    SelectField,
    InputNumberField,
    CheckboxField
} from "components/common/FinalFormComponent";
import { CommonContext } from "context";
import moment from "moment";
import roleApi from "api/roleApi";

const { Column, HeaderCell, Cell, Pagination } = Table;

export default class Permission extends React.Component {
    static contextType = CommonContext;
    constructor(props) {
        super(props);
        this.state = {
            dataRole: {},
            permissions: [],
            role: [],
            filter: {
                paging: {
                    pageSize: 25,
                    currentPage: 1
                }
            },
            init: [],
        };
        this.obj = {
            saveRoleInfos: []
        }
        this.change = 0;
    }

    componentDidMount() {
        this.getData();
    }
    getData = async () => {
        let { role = [], permissions = [] } = this.state;
        let res = await roleApi.list(this.state.filter)
        if (res && res.success) {
            if (res.data && res.data.permissionTitle) {
                let key = Object.keys(res.data.permissionTitle);
                if (key && key.length > 0) {
                    permissions = key.map(i => {
                        return { id: i, value: res.data.permissionTitle[i] }
                    })
                }
            }
            if (res.data && res.data.records) {
                role = res.data.records;

            }
        }
        this.setState({ role, permissions })
        // if (res && res.success) {
        //     this.setState({
        //         data: res.data.records,
        //         loading: false
        //     })
        // }
    };
    onHandleFormSubmit = (form) => {
        form.submit();
    };
    onSubmit = async (values) => {
        if (!this.change) {
            Notification.error({
                title: "Thông báo",
                description: "Bạn chưa thay đổi gì"
            });
            return;
        }
        let { list = [] } = this;
        this.obj.saveRoleInfos = list;
        let res = await roleApi.save(this.obj);
        if (res && res.success) {
            this.getData();
            Notification.success({
                title: "Thông báo",
                description: "Lưu dữ liệu thành công."
            });
        }
        else {
            Notification.error({
                title: "Thông báo",
                description: "Không thành công"
            });
        }
        // this.initData = values;
        // this.onSave(values);
    }
    handleOnChange = (value, iid, jid) => {
        this.change++;
        if (!this.list) this.list = [];
        let item = this.list.find(i => i.roleId === iid);
        if (item) {
            item.data = {
                ...(item.data),
                [jid]: value
            }
        } else {
            let model = { roleId: iid, data: { [jid]: value } };
            this.list.push(model)
        }

    }
    render() {
        const { loading, data, } = this.state;
        return (
            <>
                <Form
                    onSubmit={this.onSubmit}
                // mutators={{
                //     ...arrayMutators
                // }}
                >
                    {({
                        handleSubmit,
                        form: {
                            mutators: { push }
                        },
                        values,
                        form }) => {
                        this.form = { ...form, values };
                        this.values = values;
                        return (
                            <form onSubmit={handleSubmit}>
                                <Container className="mt-4" fluid>

                                    <Row>
                                        <div className="col">
                                            <Card className="shadow">
                                                <CardHeader className="bg-white border-0">
                                                    <div className="d-flex justify-content-between mb-1">
                                                        <h5>
                                                            <Icon icon="th-list" size="1x" style={{ marginRight: 20 }} />
                                                            Bảng Phân Quyền
										</h5>
                                                        <ButtonToolbar>
                                                            <Button
                                                                appearance="primary"
                                                                onClick={(e) => this.onHandleFormSubmit(form)}
                                                            >
                                                                <Icon icon="plus" className="mr-1" />
                                                                Lưu lại
											</Button>

                                                        </ButtonToolbar>
                                                    </div>
                                                </CardHeader>


                                                <div style={{ overflowX: 'auto', width: 'auto' }}>
                                                    <table className="table table-bordered table-hover p-2">
                                                        <thead style={{ background: "#5588aa", color: "white", fontSize: "12px", height: "120px" }} >
                                                            <tr align="center">
                                                                <th style={{ minWidth: 100, verticalAlign: "middle" }} rowSpan="2">Quyền/Vai trò</th>
                                                                {this.state.permissions.map((i, index) => {
                                                                    return (
                                                                        <th key={index + ""}
                                                                            style={{ minWidth: 100, verticalAlign: "middle" }}
                                                                            rowSpan="2">{i.value}</th>
                                                                    )
                                                                })}


                                                            </tr>

                                                        </thead>
                                                        <tbody>
                                                            {this.state.role.map((i, index) => {
                                                                let keys = Object.keys(i.permissionInfo || {});

                                                                // let arr = []
                                                                // if (key && key.length > 0) {
                                                                //     arr = key.map(i => {
                                                                //         return { id: i, value: i.permissionInfo && i.permissionInfo[i] }
                                                                //     })
                                                                // }

                                                                return <tr key={index + ""}>
                                                                    <td>{i.roleName}</td>
                                                                    {(keys).map((j, index1) => {
                                                                        return (
                                                                            <td key={index + "-" + index1} style={{ textAlign: 'center' }}>
                                                                                <Field
                                                                                    handleOnChange={(value) => { this.handleOnChange(value, i.id, j) }}
                                                                                    name={"row" + j + i.id}
                                                                                    component={CheckboxField}
                                                                                    defaultValue={i.permissionInfo[j]}
                                                                                />
                                                                            </td>
                                                                        )
                                                                    })}
                                                                </tr>
                                                            })}
                                                        </tbody>
                                                    </table>
                                                </div>


                                            </Card>
                                        </div>
                                    </Row>
                                </Container>
                            </form>
                        )
                    }}
                </Form>
            </>
        );
    }
}
