import React, { Component } from "react";
import { Drawer, Button, FormGroup, ControlLabel } from "rsuite";
import { Form, Field } from "react-final-form";
import { InputField, SelectField } from "components/common/FinalFormComponent";
import { Cities, Categories } from "constants/common";
import { CommonContext } from "context";
export default class extends React.Component {
	static contextType = CommonContext;
	constructor(props) {
		super(props);
		this.state = {
			backdrop: false,
			show: false,
			cities: []
		};
	}
	componentDidMount = () => {
		// this.getCities();
		// let self = this;
		// setTimeout(() => {
		// 	if (this.form) this.onSearchDoctype();
		// }, 5000);
	};
	close = () => {
		this.setState({
			show: false
		});
	};

	// getCities = async () => {
	// 	let {error} = this.context;
	// 	let res = await cityApi.list({
	// 		paging: {
	// 			pageSize: 70,
	// 			currentPage: 1
	// 		}
	// 	});
	// 	if (res && res.success) {
	// 		let {data = {}} = res;
	// 		this.setState({
	// 			cities: data.list
	// 		});
	// 	}
	// };

	show = () => {
		this.setState({ show: true });
	};
	onSave = values => {
		const { onSave } = this.props;
		this.close();
		if (onSave) onSave(values);
	};
	render() {
		let listCategories = Categories
		listCategories.unshift({ title: "Tất cả", value: '' });
		let { filter, onHiden } = this.props;
		return (
			<Drawer backdrop={true} show={true} onHide={onHiden} size="xs">
				<Drawer.Header>
					<Drawer.Title>Bộ lọc</Drawer.Title>
				</Drawer.Header>
				<Drawer.Body>
					<Form onSubmit={this.onSave} initialValues={filter}>
						{({ handleSubmit, pristine, invalid, values, form }) => {
							this.form = { ...form, values };
							return (
								<form onSubmit={handleSubmit}>
									<FormGroup className="mb-3">
										<Field
											className="col-12"
											name="search"
											component={InputField}
											placeholder="Nội dung tìm kiếm"
										/>
									</FormGroup>
									<FormGroup className="mb-3">
										<Field
											name="categoryId"
											component={SelectField}
											data={listCategories || []}
											style={{ width: "100%" }}
											valueKey="value"
											labelKey="title"
											placeholder="Chọn danh mục"
										/>
									</FormGroup>
									{false &&
										<FormGroup className="mb-3">
											<Field
												name="districtId"
												component={SelectField}
												placeholder="Khu vực"
												data={Cities.map(i => { i.label = i.name; return i })}
												style={{ borderColor: "#cccccc" }}
												style={{ width: "100%" }}
											/>
										</FormGroup>
									}

								</form>
							);
						}}
					</Form>
				</Drawer.Body>
				<Drawer.Footer>
					<Button
						onClick={() => {
							this.form.submit();
						}}
						appearance="primary"
					>
						Tìm kiếm
					</Button>
					<Button onClick={onHiden} appearance="subtle">
						Hủy
					</Button>
				</Drawer.Footer>
			</Drawer>
		);
	}
}
