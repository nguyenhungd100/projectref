import React from "react";
import {
	Badge,
	Card,
	CardHeader,
	CardFooter,
	DropdownMenu,
	DropdownItem,
	UncontrolledDropdown,
	DropdownToggle,
	Media,
	Pagination as Paginations,
	PaginationItem,
	PaginationLink,
	Progress,
	Container,
	Row,
	UncontrolledTooltip
} from "reactstrap";
import {
	Table,
	Modal,
	Button,
	FormGroup,
	ControlLabel,
	Icon,
	Notification,
	ButtonToolbar,
	Col,
	IconButton,
	Checkbox,
	Input
} from "rsuite";

import { Form, Field } from "react-final-form";
import {
	InputField,
	DateField,
	SelectField,
	InputNumberField
} from "components/common/FinalFormComponent";
import { CommonContext } from "context";
import Detail from "./Detail";
import moment from "moment";
import Filter from "./Filter";
import numeral from "numeral";
import postApi from "api/postApi"
import { onGetTye } from "constants/common"
import getType from "jest-get-type";
const { Column, HeaderCell, Cell, Pagination } = Table;
const rowKey = "id";
const CheckCell = ({ rowData, onChange, checkedKeys, dataKey, ...props }) => (
	<Cell {...props} style={{ padding: 0 }}>
		<div style={{ lineHeight: "46px" }}>
			<Checkbox
				value={rowData[dataKey]}
				inline
				onChange={onChange}
				checked={checkedKeys.some(item => item === rowData[dataKey])}
			/>
		</div>
	</Cell>
);

const ExpandCell = ({
	rowData,
	dataKey,
	expandedRowKeys,
	onChange,
	...props
}) => (
		<Cell {...props}>
			<IconButton
				size="xs"
				appearance="subtle"
				onClick={() => {
					onChange(rowData);
				}}
				icon={<Icon icon={expandedRowKeys ? "minus-square-o" : "plus-square-o"} />}
			/>
		</Cell>
	);
class Tables extends React.Component {
	static contextType = CommonContext;
	constructor(props) {
		super(props);
		this.state = {
			data: [

			],
			checkedKeys: [],
			filter: {
				paging: {
					pageSize: 25,
					pageIndex: 1
				}
			},
			expandedRowKeys: [],
			showFilter: false
		};
		this.handleExpanded = this.handleExpanded.bind(this);
	}

	componentDidMount() {
		this.getData();
	}
	componentWillReceiveProps = (nextprops) => {
		if (nextprops && nextprops.match && this.props.match.params !== nextprops.match.params) {
			let { type } = nextprops.match.params;
			this.getData(onGetTye(type).id)
		}
	}
	getData = async (status) => {
		let { filter } = this.state;
		let { type } = this.props.match.params;
		filter.status = status ? status : onGetTye(type).id;

		let { loading, unloading, error } = this.context;
		loading();
		let res = await postApi.list(filter);
		this.setState({ filter })
		unloading();
		if (res && res.success) {
			let data = res.data.records;
			this.setState({ data })
		} else {
			error(res ? res.message : "", "error");
		}
	};
	onSearch = (valuse) => {
		this.state.filter = { ...valuse }
		this.getData();
	};

	onAction = async (id, reject) => {
		let { loading, unloading } = this.context;
		let action = postApi.approve;
		if (reject) action = postApi.reject;
		loading();
		let res = await action(id);
		unloading();
		return this.showNotify(res);
	}
	onActionShow = (id, name) => {
		let model = { title: 'Bạn có chắc chắn muốn duyệt tin này?', text: "Bài đăng sẽ được hiển thị trên website sau khi duyệt." }
		switch (name) {
			case 'approve':
				return this.onConfirm(model, id, this.onAction)
			case 'reject':
				model = { title: 'Bạn có chắc chắn từ chối tin này?', text: "Bài đăng sẽ không được hiển thị trên website khi từ chối." }
				return this.onConfirm(model, id, this.onAction, true)
			case 'delete':
				model = { title: 'Bạn có chắc chắn xóa?', text: "Bài đăng sẽ không được phục hồi sau khi xóa." }
				return this.onConfirm(model, id, this.onDelete)
		}
	}
	onConfirm = async (model, id, action, reject) => {
		const { showComfirm } = this.context;
		let isDelete = await showComfirm(model);
		if (isDelete) action(id, reject)
	}
	onDelete = async (id) => {
		let { loading, unloading } = this.context;
		loading();
		let itemDeletes = {};
		itemDeletes.ids = [id];
		// let res = await postApi.delete([id]);
		let res = await postApi.delete(itemDeletes);
		unloading();
		return this.showNotify(res);
	}
	onApprove = async (id) => {
		let { loading, unloading } = this.context;
		loading();
		let res = await postApi.approve(id)
		unloading();
		return this.showNotify(res);
	}
	showNotify = (res) => {
		let { error, success } = this.context;
		if (res && res.success) {
			success("Thao tác thành công.")
			this.getData();
		} else {
			error(res ? res.message : "", "error");
		}
	};

	onSubmit = values => {
		console.log(values);
	};

	onPressEnter = e => {
		this.onSearch();
	};

	onSave = () => { };

	// onDelete = data => {
	// 	this.confirmBox.current.show();
	// };

	handleChangePage = currentPage => {
		this.state.filter.paging.currentPage = currentPage;
		this.getData();
	};
	handleChangeLength = pageSize => {
		this.state.filter.paging.pageSize = pageSize;
		this.state.filter.paging.currentPage = 1;
		this.getData();
	};
	handleExpanded(rowData, dataKey) {
		const { expandedRowKeys } = this.state;

		let open = false;
		const nextExpandedRowKeys = [];

		expandedRowKeys.forEach(key => {
			if (key === rowData[rowKey]) {
				open = true;
			} else {
				nextExpandedRowKeys.push(key);
			}
		});

		if (!open) {
			nextExpandedRowKeys.push(rowData[rowKey]);
		}
		this.setState({
			expandedRowKeys: nextExpandedRowKeys
		});
	}
	handleCheckAll = (value, checked) => {
		const { data } = this.state;
		const checkedKeys = checked ? data.map(item => item.id) : [];
		this.setState({
			checkedKeys
		});
	};
	handleCheck = (value, checked) => {
		const { checkedKeys } = this.state;
		const nextCheckedKeys = checked
			? [...checkedKeys, value]
			: checkedKeys.filter(item => item !== value);

		this.setState({
			checkedKeys: nextCheckedKeys
		});
	};
	showFilter = () => {
		this.setState({ showFilter: !this.state.showFilter })
	};
	render() {
		const { expandedRowKeys, data, checkedKeys, filter, showFilter } = this.state;
		const { onActionShow } = this;
		const { loading, list } = this.state;
		var paging = this.state.filter.paging;
		let checked = false;
		let indeterminate = false;

		if (checkedKeys.length === data.length) {
			checked = true;
		} else if (checkedKeys.length === 0) {
			checked = false;
		} else if (checkedKeys.length > 0 && checkedKeys.length < data.length) {
			indeterminate = true;
		}
		return (
			<>
				<Container className="mt-4" fluid>
					<Row>
						<div className="col">
							<Card className="shadow">
								<CardHeader className="bg-white border-0">
									<div className="d-flex justify-content-between mb-1">
										<h5 className="text-16">
											<Icon icon="th-list" size="1x" style={{ marginRight: 20 }} />
											{`Danh sách tin ${onGetTye('', filter.status).name} `}
										</h5>

										<ButtonToolbar>
											<Button
												appearance="primary"
												onClick={() => {
													this.showFilter();
												}}
											>
												<Icon icon="filter" className="mr-1" />
												Bộ lọc
											</Button>
										</ButtonToolbar>
									</div>
								</CardHeader>
								<div className="mt-2 p-2 mb-1 border ">
									<Table
										height={window.innerHeight * 0.66}
										data={data}
										rowKey={rowKey}
										expandedRowKeys={expandedRowKeys}
										onRowClick={data => {
											console.log(data);
										}}
										rowExpandedHeight={400}
										renderRowExpanded={rowData => {
											return <Detail data={rowData} onConfirm={onActionShow} />;
										}}
									>
										{false &&
											<Column width={50} align="center">
												<HeaderCell style={{ padding: 0 }}>
													<div style={{ lineHeight: 40 }}>
														<Checkbox
															inline
															checked={checked}
															indeterminate={indeterminate}
															onChange={this.handleCheckAll}
														/>
													</div>
												</HeaderCell>
												<CheckCell
													dataKey="id"
													checkedKeys={checkedKeys}
													onChange={this.handleCheck}
												/>
											</Column>
										}
										<Column width={70} align="center">
											<HeaderCell />
											<ExpandCell
												dataKey="id"
												expandedRowKeys={expandedRowKeys}
												onChange={this.handleExpanded}
											/>
										</Column>

										<Column width={130} flexGrow={2}>
											<HeaderCell>Tiêu đề</HeaderCell>
											<Cell dataKey="name" />
										</Column>

										<Column width={130} flexGrow={2}>
											<HeaderCell>Danh mục</HeaderCell>
											<Cell dataKey="categoryName" />
										</Column>

										<Column width={200} flexGrow={2}>
											<HeaderCell>Người đăng</HeaderCell>
											<Cell dataKey="createdUserName" />
										</Column>

										<Column width={200} flexGrow={2}>
											<HeaderCell>Thời gian</HeaderCell>
											<Cell dataKey="createdDate" >
												{rowData => (
													<span>{rowData.createdDate ? moment(rowData.createdDate).format("DD-MM-YYYY") : ''}</span>
												)}
											</Cell>
										</Column>

										<Column width={200} flexGrow={2}>
											<HeaderCell />
											<Cell dataKey="companyName" />
										</Column>
									</Table>
								</div>
								<CardFooter className="py-4">
									<Pagination
										lengthMenu={[
											{
												value: 25,
												label: 25
											},
											{
												value: 50,
												label: 50
											}
										]}
										activePage={paging.currentPage}
										displayLength={paging.pageSize}
										total={paging.rowsCount}
										onChangePage={this.handleChangePage}
										onChangeLength={this.handleChangeLength}
									/>
								</CardFooter>
							</Card>
						</div>
					</Row>
					{
						showFilter &&
						<Filter
							ref={refs => (this.filterComp = refs)}
							filter={this.state.filter}
							onSave={this.onSearch}
							onHiden={this.showFilter}
						/>
					}

				</Container>
			</>
		);
	}
}

export default Tables;
