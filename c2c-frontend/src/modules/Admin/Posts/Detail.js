import React, { Component } from "react";
import {
	Badge,
	Card,
	CardHeader,
	CardFooter,
	DropdownMenu,
	DropdownItem,
	UncontrolledDropdown,
	DropdownToggle,
	Media,
	Pagination as Paginations,
	PaginationItem,
	PaginationLink,
	CardText,
	Progress,
	Container,
	Row,
	UncontrolledTooltip,
	CardTitle
} from "reactstrap";
import {
	Table,
	Modal,
	Button,
	FormGroup,
	ControlLabel,
	Icon,
	Notification,
	ButtonToolbar,
	Col,
	IconButton,
	Input,
	Avatar
} from "rsuite";
import moment from "moment";
import postApi from "api/postApi"
import { Cities } from "constants/common";
class Detail extends Component {
	render() {
		const { data = { listImageIds: [] }, onConfirm } = this.props;
		let images = data.listImageIds.map(i => {
			return {
				original: postApi.getImage(i, 300, 300),
				thumbnail: postApi.getImage(i, 50, 50)
			}
		});
		let image = images ? images[0] : ''
		return (
			<div style={{ height: "auto" }}>
				<Row>
					<Col sm="5">
						<img
							src={image ? image.original : "/assets/images/noimage.png"}
							width="300"
							height="300"
							style={{ borderRadius: 10 }}
						/>
					</Col>
					<Col sm="2">
						<div style={{ display: "flex", flexDirection: "column", maxHeight: 200 }}>
							{images.map((i, index) => (
								<Avatar key={index} src={i.thumbnail} className="mb-2" />
							))}
						</div>
					</Col>
					<Col sm="17">
						<div>
							<Col sm="24">
								<h5 className="mb-4">
									<b>{data.name}</b>
								</h5>
							</Col>
						</div>
						<Col sm="12">
							<Row className="mb-4">
								<div className="col-4">Khu vực: </div>
								<div className="col-8">{data.provinceName}</div>
							</Row>
							<Row className="mb-4">
								<div className="col-4">Thời gian: </div>
								<div className="col-8">{data.createdDate ? moment(data.createdDate).format("DD-MM-YYYY") : ''}</div>
							</Row>
						</Col>
						<Col sm="12">
							<Row className="mb-4">
								<div className="col-4">Người đăng: </div>
								<div className="col-8">{data.createdUserName}</div>
							</Row>
							<Row className="mb-4">
								<div className="col-4">Email: </div>
								<div className="col-8">{data.createdEmail}</div>
							</Row>
							{
								data.shopPageId &&
								<Row className="mb-4">
									<div className="col-4">Tên cửa hàng: </div>
									<div className="col-8">{data.shopPageName}</div>
								</Row>
							}

						</Col>
						<Col sm="24">
							<Row className="mb-4">
								<div className="col-2">Mô tả </div>
								<div className="col-10">
									{data.content}
								</div>
							</Row>
						</Col>
					</Col>
				</Row>
				<Row
					style={{
						width: "100%",
						display: "flex",
						justifyContent: "flex-end"
					}}
				>
					<ButtonToolbar style={{ float: "right" }}>
						{
							data.status === 1 &&
							<>
								<Button onClick={() => {
									if (onConfirm) {
										onConfirm(data.id, 'approve')
									}
								}} color="blue" >
									<Icon icon="chevron-circle-down" /> Duyệt
								</Button>
								<Button onClick={() => {
									if (onConfirm) {
										onConfirm(data.id, 'reject')
									}
								}} color="yellow">
									<Icon icon="info" /> Từ chối
								</Button>
							</>
						}

						<Button onClick={() => {
							if (onConfirm) {
								onConfirm(data.id, "delete")
							}
						}} color="red">
							<Icon icon="trash-o" /> Xóa
						</Button>
					</ButtonToolbar>
				</Row>
			</div>
		);
	}
}
export default Detail;
