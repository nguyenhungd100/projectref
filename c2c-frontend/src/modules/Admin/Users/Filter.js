import React, { Component } from "react";
import { Drawer, Button, ControlLabel, Icon } from "rsuite";
import { Form, Field } from "react-final-form";
import { InputField, SelectField } from "components/common/FinalFormComponent";
import { FormGroup } from 'reactstrap';
import { CommonContext } from "context";
export default class extends React.Component {
	static contextType = CommonContext;
	constructor(props) {
		super(props);
		this.state = {
			backdrop: false,
			show: false,
			cities: []
		};
	}
	componentDidMount = () => {
		// this.getCities();
		// let self = this;
		// setTimeout(() => {
		// 	if (this.form) this.onSearchDoctype();
		// }, 5000);
	};
	// getCities = async () => {
	// 	let {error} = this.context;
	// 	let res = await cityApi.list({
	// 		paging: {
	// 			pageSize: 70,
	// 			currentPage: 1
	// 		}
	// 	});
	// 	if (res && res.success) {
	// 		let {data = {}} = res;
	// 		this.setState({
	// 			cities: data.list
	// 		});
	// 	}
	// };

	onSave = values => {
		const { onSave } = this.props;
		this.close();
		if (onSave) onSave(values);
	};
	onSubmit = (values) => {
		this.props.onSearch(values)
	}

	onHandleSubmit = (form) => {
		form.submit();
		this.props.onHide();
	}
	render() {
		const { roles } = this.state;
		return (
			<Drawer
				size='xs'
				placement="right"
				show={this.props.show}
				onHide={this.props.onHide}
			>
				<Drawer.Header>
					<Drawer.Title>Lọc dữ liệu</Drawer.Title>
				</Drawer.Header>
				<Form onSubmit={this.onSubmit} initialValues={this.props.filter}>
					{({ handleSubmit, form, submitting, pristine }) => {
						return (
							<form onSubmit={handleSubmit}>
								<Drawer.Body>
									<FormGroup>
										<Field
											name="textSearch"
											component={InputField}
											placeholder="Search..."
										/>
									</FormGroup>
									<FormGroup>
										<Field
											name="status"
											component={SelectField}
											placeholder="Trạng thái"
											data={[
												{ value: 1, label: "Chưa mở khóa" },
												{ value: 2, label: "Hoạt động" },
												{ value: 3, label: "Đã khóa" }
											]}
											block
										/>
									</FormGroup>


								</Drawer.Body>
								<Drawer.Footer>
									<Button color="green" appearance="primary" onClick={(e) => this.onHandleSubmit(form)}>
										<Icon icon="search" />&nbsp;&nbsp;Tìm kiếm
                        </Button>
									<Button onClick={this.props.onHide} appearance="subtle">
										<Icon icon="ban" />&nbsp;&nbsp;Đóng
                        </Button>
								</Drawer.Footer>
							</form>
						)
					}}
				</Form>
			</Drawer>
		)
	}
}
