import React from "react";
import {
	Card,
	CardHeader,
	CardFooter,
	Container,
	Row,
} from "reactstrap";
import {
	Table,
	Modal,
	Button,
	FormGroup,
	ControlLabel,
	Icon,
	Notification,
	ButtonToolbar,
	IconButton,
	Whisper, Popover
} from "rsuite";

import { Form, Field } from "react-final-form";
import {
	InputField,
	DateField,
	SelectField,
	InputNumberField
} from "components/common/FinalFormComponent";
import { CommonContext } from "context";
import moment from "moment";
import userApi from "api/userApi";
import Filter from './Filter'
import firebaseApi from "api/firebaseApi";
const { Column, HeaderCell, Cell, Pagination } = Table;
const Roles = [

	{
		"id": ";4;",
		"roleName": "Admin",
		"title": null,
		"description": null,

		"displayOrder": 1
	},
	{
		"id": ";5;",
		"roleName": "Bộ phận hỗ trợ",
		"title": null,

		"displayOrder": 3
	},
	{
		"id": ";7;",
		"roleName": "Bộ phận kiểm duyệt",
		"displayOrder": 4
	}
]
class Tables extends React.Component {
	static contextType = CommonContext;
	constructor(props) {
		super(props);
		this.state = {
			data: [],
			showFilter: false,
			filter: {
				paging: {
					pageSize: 50,
					currentPage: 1
				}
			}
		};
	}

	componentDidMount() {
		this.getData();
		this.createDumyData();
	}

	openFilter = () => {
		this.setState({
			showFilter: true
		})
	}

	closeFilter = () => {
		this.setState({
			showFilter: false,
		})
	}

	createDumyData = () => {

	};

	onSearch = () => {
		// this.state.filter.paging.currentPage = 1;
		this.getData();
	};

	getData = async () => {
		let res = await userApi.list(this.state.filter)
		if (res && res.success) {
			this.setState({
				data: res.data.records,
				loading: false
			})
		}
	};

	onSubmit = values => {
		this.onSave(values);
	};
	pushMessage = async (model) => {
		await firebaseApi.send(model)
	}
	onSave = async (values) => {
		let isClient = values.isUserSide;
		if (isClient) values.userRoles = ";2;"
		let res = await userApi.createUserOnCms(values)
		if (res && res.success) {
			this.close();
			this.getData();
			if (isClient && res.data) {
				this.pushMessage({ userId: res.data.id, userName: res.data.fullName || res.data.email })
			}
			Notification.success({
				title: "Thông báo",
				description: "Lưu dữ liệu thành công."
			});
		} else {
			Notification.error({
				title: "Thông báo",
				description: "Không thành công"
			});
		}
	}

	onPressEnter = e => {
		this.onSearch();
	};

	onHandleFormSubmit = (form) => {
		form.submit();
	};

	onActive = async data => {
		this[data.id].hide();
		let res = await userApi.lockAccount(data.id)
		console.log(1, res)
		if (res && res.success) {
			this.getData();
			Notification.success({
				title: "Thông báo",
				description: "Lưu dữ liệu thành công."
			});

		} else {
			Notification.error({
				title: "Thông báo",
				description: "Không thành công"
			});
		}
	};

	onAdd = () => {
		this.open("md");
	};

	onEdit = data => {
		this.setState({
			initialData: data
		})
		this[data.id].hide();
		this.open("md");
	};

	/**
	 * close form
	 */
	close = () => {
		this.setState({
			show: false
		});
	};

	/**
   open form
   */
	open = size => {
		this.setState({
			size,
			show: true
		});
	};

	handleChangePage = currentPage => {
		this.state.filter.paging.currentPage = currentPage;
		this.getData();
	};

	handleChangeLength = pageSize => {
		this.state.filter.paging.pageSize = pageSize;
		this.state.filter.paging.currentPage = 1;
		this.getData();
	};

	renderForm = () => {
		const { size, initialData } = this.state;
		return (
			<>
				<Modal size={size} show={this.state.show} onHide={this.close}>
					<Modal.Header>
						<Modal.Title>{initialData ? "CẬP NHẬT TÀI KHOẢN" : "TẠO TÀI KHOẢN"}</Modal.Title>
					</Modal.Header>
					<Form
						onSubmit={this.onSubmit}
						initialValues={initialData ? initialData : {}}
					>
						{({
							handleSubmit,
							pristine,
							invalid,
							values,
							form: {
								mutators: { push, pop }
							},
							form
						}) => {
							this.form = form;
							return (
								<form onSubmit={handleSubmit}>
									<Modal.Body>
										<FormGroup className="d-flex mb-2">
											<ControlLabel className="col-4">Loại tài khoản</ControlLabel>
											<Field
												name='isUserSide'
												component={SelectField}
												placeholder="Loại tài khoản"
												data={[
													{ value: true, label: "Khách hàng" },
													{ value: false, label: "Quản trị hệ thống" },
												]}
												className="col-8 p-0"
											/>
										</FormGroup>
										{!values.isUserSide &&
											<FormGroup className="d-flex mb-2">
												<ControlLabel className="col-4">Vai trò</ControlLabel>
												<Field
													name='userRoles'
													component={SelectField}
													placeholder="Vai trò"
													data={Roles}
													className="col-8 p-0"
													valueKey={"id"}
													labelKey={"roleName"}
												/>
											</FormGroup>
										}

										<FormGroup className="d-flex mb-2">
											<ControlLabel className="col-4">Họ tên</ControlLabel>
											<Field
												name="fullName"
												className="col-8"
												component={InputField}
												placeholder="Họ tên"
											/>
										</FormGroup>
										<FormGroup className="d-flex mb-2">
											<ControlLabel className="col-4">Ngày sinh</ControlLabel>
											<Field
												name="dateOfBirth"
												className="col-8 p-0"
												component={DateField}
												placeholder="Ngày sinh"
												formatDate="DD/MM/YYYY"
												style={{
													opacity: 1,
													padding: "8px 15px",
													border: "1px solid #cccccc",
													fontSize: 14,

													borderRadius: 10
												}}
												block
											/>
										</FormGroup>
										<FormGroup className="d-flex mb-2">
											<ControlLabel className="col-4">Email</ControlLabel>
											<Field
												name="email"
												className="col-8"
												component={InputField}
												placeholder="Email"
											/>
										</FormGroup>
										<FormGroup className="d-flex mb-2">
											<ControlLabel className="col-4">Mật Khẩu</ControlLabel>
											<Field
												name="password"
												type='password'
												className="col-8"
												component={InputField}
												placeholder="Mật Khẩu"
											/>
										</FormGroup>
									</Modal.Body>
									<Modal.Footer>
										<Button onClick={(e) => this.onHandleFormSubmit(form)} appearance="primary">
											Lưu lại
										</Button>
										<Button onClick={this.close} appearance="subtle">
											Hủy
										</Button>
									</Modal.Footer>
								</form>
							);
						}}
					</Form>
				</Modal>
			</>
		);
	};
	onSearchF = (values) => {
		this.setState(({ filter }) => ({
			filter: {
				...filter,
				...values
			}
		}), () => this.getData())

	}
	render() {
		const { loading, data, filter } = this.state;
		var paging = this.state.filter.paging;
		let { showFilter } = this.state;
		return (
			<>
				<Container className="mt-4" fluid>
					<Filter
						show={showFilter}
						filter={filter}
						onHide={this.closeFilter}
						onSearch={this.onSearchF}
					/>
					<Row>
						<div className="col">
							<Card className="shadow">
								<CardHeader className="bg-white border-0">
									<div className="d-flex justify-content-between mb-1">
										<h5>
											<Icon icon="th-list" size="1x" style={{ marginRight: 20 }} />
											Danh sách tài khoản
										</h5>
										<ButtonToolbar>
											<Button
												appearance="primary"
												onClick={() => {
													this.onAdd("md");
												}}
											>
												<Icon icon="plus" className="mr-1" />
												Thêm mới
											</Button>
											<Button onClick={() => this.openFilter()}>
												<Icon icon={"filter"} />&nbsp;&nbsp;Bộ Lọc
              </Button>
										</ButtonToolbar>
									</div>
								</CardHeader>
								<div className="mt-2 p-2 mb-1 border ">
									<Table
										cellBordered
										// autoHeight
										wordWrap={true}
										height={window.innerHeight * 0.7}
										data={data}
										loading={loading}
									>
										<Column flexGrow={2}>
											<HeaderCell>Họ tên</HeaderCell>
											<Cell dataKey="fullName" />
										</Column>

										<Column width={200} align="left">
											<HeaderCell>Ngày sinh</HeaderCell>
											<Cell>
												{rowData => {
													return <span>{rowData.dateOfBirth && moment(rowData.dateOfBirth).format("DD-MM-YYYY")}</span>;
												}}
											</Cell>
										</Column>

										<Column flexGrow={2}>
											<HeaderCell>Email</HeaderCell>
											<Cell dataKey="email" />
										</Column>

										<Column flexGrow={2}>
											<HeaderCell>Hoạt động</HeaderCell>
											<Cell>
												{rowData => {
													return <span>{rowData.status == 1 ? "Chưa mở khóa" : (rowData.status == 2) ? "Hoạt động" : "Đã khóa"}</span>
												}}
											</Cell>
										</Column>
										<Column width={120} style={{ height: 72 }} fixed="right" align="center">
											<HeaderCell>{"Action"}</HeaderCell>
											<Cell onClick={e => e.stopPropagation()}>
												{rowData =>
													<Whisper placement="auto" trigger="click"
														onClick={e => e.stopPropagation()}
														triggerRef={ref => { this[rowData.id] = ref }} speaker={
															<Popover>
																<IconButton color="green"
																	appearance="default"
																	block
																	icon={<Icon icon="edit2" />}
																	onClick={(e) => {
																		e.stopPropagation();
																		this.onEdit(rowData);
																	}}
																>
																	Cập nhật
                        										  </IconButton>

																<IconButton
																	color="cyan"
																	appearance="default"
																	block
																	icon={rowData.status == 2 ? <Icon icon="lock" /> : <Icon icon="unlock" />}
																	onClick={(e) => {
																		e.stopPropagation();
																		this.onActive(rowData);
																	}}>
																	{rowData.status == 2 ? "Khóa tài khoản" : "Mở Khóa tài khoản"}
																</IconButton>
															</Popover>
														}>
														<IconButton color="green" appearance="ghost" icon={<Icon icon="arrow-down" />} />
													</Whisper>}
											</Cell>
										</Column>
									</Table>

									{this.state.show && this.renderForm()}
								</div>
								<CardFooter className="py-4">
									<Pagination
										lengthMenu={[
											{
												value: 25,
												label: 25
											},
											{
												value: 50,
												label: 50
											}
										]}
										activePage={paging.currentPage}
										displayLength={paging.pageSize}
										total={paging.rowsCount}
										onChangePage={this.handleChangePage}
										onChangeLength={this.handleChangeLength}
									/>
								</CardFooter>
							</Card>
						</div>
					</Row>
				</Container>
			</>
		);
	}
}

export default Tables;
