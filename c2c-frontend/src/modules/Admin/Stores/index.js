import React from "react";
import {
	Badge,
	Card,
	CardHeader,
	CardFooter,
	DropdownMenu,
	DropdownItem,
	UncontrolledDropdown,
	DropdownToggle,
	Media,
	Pagination as Paginations,
	PaginationItem,
	PaginationLink,
	Progress,
	Container,
	Row,
	UncontrolledTooltip
} from "reactstrap";
import {
	Table,
	Modal,
	Button,
	FormGroup,
	ControlLabel,
	Icon,
	Notification,
	ButtonToolbar,
	Col,
	Input, IconButton, Whisper, Popover
} from "rsuite";

import { Form, Field } from "react-final-form";
import {
	InputField,
	DateField,
	SelectField,
	InputNumberField,
	RSTextAreaField
} from "components/common/FinalFormComponent";
import { CommonContext } from "context";
import moment from "moment";
import Filter from "./Filter";
import categoryApi from "api/categoryApi";
import postApi from "api/postApi";
const { Column, HeaderCell, Cell, Pagination } = Table;
class Tables extends React.Component {
	static contextType = CommonContext;
	constructor(props) {
		super(props);
		this.state = {
			data: [],
			category: [],
			showFilter: false,
			filter: {
				paging: {
					pageSize: 25,
					currentPage: 1
				}
			}
		};
	}


	componentDidMount() {
		this.getDataCategory();
		this.getData();
	}
	getDataCategory = async () => {
		let { category = [] } = this.state;
		let res = await categoryApi.list();
		if (res && res.success) {
			category = (res.data || []).map(i => {
				if (i && i.categoryChildrens) {
					i.children = i.categoryChildrens;
				}
				return i;
			});
		}
		this.setState({ category })
	}
	onSearch = (values) => {
		this.setState(({ filter }) => ({
			filter: {
				...filter,
				...values
			}
		}), () => this.getData())

	}

	getData = async () => {
		let res = await postApi.listStore(this.state.filter)
		if (res && res.success) {
			this.setState({
				data: res.data.records,
				loading: false
			})
		}
	};
	showFilter = () => {
		this.filterComp.show();
	};
	onSubmit = values => {
		console.log(values);
	};

	onPressEnter = e => {
		this.onSearch();
	};


	onDelete = data => {
		this[data.id].hide();
		// this.confirmBox.current.show();
	};

	onAdd = () => {
		this.open("md");
	};

	onEdit = data => {
		this[data.id].hide();
		this.setState({
			initialData: data
		})
		this.open("md");
	};

	/**
	 * close form
	 */
	close = () => {
		this.setState({
			show: false
		});
	};
	/**
   open form
   */
	open = size => {
		this.setState({
			size,
			show: true
		});
	};

	handleChangePage = currentPage => {
		console.log(currentPage);
		this.state.filter.paging.currentPage = currentPage;
		this.getData();
	};
	handleChangeLength = pageSize => {
		this.state.filter.paging.pageSize = pageSize;
		this.state.filter.paging.currentPage = 1;
		this.getData();
	};
	onHandleFormSubmit = (form) => {
		form.submit();
	};
	onSubmit = values => {
		this.onSave(values);
	};
	onSave = async (values) => {
		let res = await postApi.addStore(values)
		if (res && res.success) {
			this.close();
			this.getData();
			Notification.success({
				title: "Thông báo",
				description: "Lưu dữ liệu thành công."
			});
		} else {
			Notification.error({
				title: "Thông báo",
				description: "Không thành công"
			});
		}
	}
	renderForm = () => {
		const { size, category, initialData } = this.state;

		return (
			<>
				<Modal size={size} show={this.state.show} onHide={this.close}>
					<Modal.Header>
						<Modal.Title>{initialData ? "CẬP NHẬT CỬA HÀNG" : "THÊM CỦA HÀNG"}</Modal.Title>
					</Modal.Header>
					<Form
						onSubmit={this.onSubmit}
						initialValues={initialData ? initialData : {}}
					//decorators={[this.onChangeDependentField]}
					>
						{({
							handleSubmit,
							pristine,
							invalid,
							values,
							form: {
								mutators: { push, pop }
							},
							form
						}) => {
							this.form = form;

							return (
								<form onSubmit={handleSubmit}>
									<Modal.Body>
										<FormGroup className="d-flex mb-2">
											<ControlLabel className="col-4">Tên cửa hàng</ControlLabel>
											<Field
												name="name"
												className="col-8"
												component={InputField}
												placeholder="Tên cửa hàng"
											/>
										</FormGroup>

										{/* <FormGroup className="d-flex mb-2">
											<ControlLabel className="col-4">Tên đăng nhập</ControlLabel>
											<Field
												name="userName"
												className="col-8 p-0"
												component={SelectField}
												placeholder="Tên đăng nhập"
												options={[
													{ value: "value1", label: "label1" },
													{ value: "value2", label: "label2" }
												]}
												block
											/>
										</FormGroup> */}

										<FormGroup className="d-flex mb-2">
											<ControlLabel className="col-4">Danh mục chuyên</ControlLabel>
											<Field
												name="categoryId"
												className="col-8 p-0"
												labelKey="name"
												valueKey="id"
												defaultExpandAll
												component={SelectField}
												placeholder="Chọn danh mục "
												data={category}
												block
											/>
										</FormGroup>

										<FormGroup className="d-flex mb-2">
											<ControlLabel className="col-4 ">Mô tả</ControlLabel>
											<Field
												name="description"
												className="col-8 p-0"
												component={RSTextAreaField}
												placeholder="Mô tả"
											/>
										</FormGroup>

									</Modal.Body>
									<Modal.Footer>
										<Button onClick={(e) => this.onHandleFormSubmit(form)} appearance="primary">
											Lưu lại
						</Button>
										<Button onClick={this.close} appearance="subtle">
											Hủy
						</Button>
									</Modal.Footer>
								</form>
							);
						}}
					</Form>
				</Modal>
			</>
		);
	};

	render() {
		const { loading, data, category } = this.state;
		var { paging } = this.state.filter;
		return (
			<>
				<Container className="mt-4" fluid>
					<Row>
						<div className="col">
							<Card className="shadow">
								<CardHeader className="bg-white border-0">
									<div className="d-flex justify-content-between mb-1">
										<h5>
											<Icon icon="th-list" size="1x" style={{ marginRight: 20 }} />
											Danh sách cửa hàng
										</h5>
										<ButtonToolbar>
											<Button
												appearance="primary"
												onClick={() => {
													this.onAdd("md");
												}}
											>
												<Icon icon="plus" className="mr-1" />
												Thêm mới
											</Button>
											<Button
												onClick={() => {
													this.showFilter("md");
												}}
											>
												<Icon icon="filter" className="mr-1" />
												Bộ lọc
											</Button>
										</ButtonToolbar>
									</div>
								</CardHeader>
								<div className="mt-2 p-2 mb-1 border ">
									<Table
										wordWrap={true}
										height={window.innerHeight * 0.75}
										data={data}
										loading={loading}
									>
										<Column flexGrow={2}>
											<HeaderCell>Tên cửa hàng</HeaderCell>
											<Cell dataKey="name" />
										</Column>

										<Column flexGrow={2}>
											<HeaderCell>Ngày tạo</HeaderCell>
											<Cell>
												{rowData => {
													return <span>{rowData.createdDate && moment(rowData.createdDate).format("DD-MM-YYYY")}</span>;
												}}
											</Cell>
										</Column>

										<Column flexGrow={2}>
											<HeaderCell>Người tạo</HeaderCell>
											<Cell dataKey="createdUserName" />
										</Column>

										{/* <Column flexGrow={2}>
											<HeaderCell>Danh mục chuyên</HeaderCell>
											<Cell dataKey="categoryId" />
										</Column> */}

										<Column flexGrow={2}>
											<HeaderCell>Mô tả</HeaderCell>
											<Cell dataKey="description" />
										</Column>

										<Column width={120} fixed="right" align="center">
											<HeaderCell>{"Action"}</HeaderCell>
											<Cell onClick={e => e.stopPropagation()}>
												{rowData =>
													<Whisper placement="auto" trigger="click"
														onClick={e => e.stopPropagation()}
														triggerRef={ref => { this[rowData.id] = ref }} speaker={
															<Popover>
																<IconButton color="green"
																	appearance="default"
																	block
																	icon={<Icon icon="edit2" />}
																	onClick={(e) => {
																		e.stopPropagation();
																		this.onEdit(rowData);
																	}}
																>
																	Cập nhật
                          										</IconButton>

																<IconButton color="red" appearance="default" block icon={<Icon icon="trash-o" />}
																	onClick={(e) => { e.stopPropagation(); this.onDelete(rowData); }}
																>
																	Xóa
                          										</IconButton>
															</Popover>
														}>
														<IconButton color="green" appearance="ghost" icon={<Icon icon="arrow-down" />} />
													</Whisper>}
											</Cell>
										</Column>
									</Table>

									{this.renderForm()}
								</div>
								<CardFooter className="py-4">
									<Pagination
										lengthMenu={[
											{
												value: 25,
												label: 25
											},
											{
												value: 50,
												label: 50
											}
										]}
										activePage={paging.currentPage}
										displayLength={paging.pageSize}
										total={paging.rowsCount}
										onChangePage={this.handleChangePage}
										onChangeLength={this.handleChangeLength}
									/>
								</CardFooter>
							</Card>
						</div>
					</Row>
					<Filter
						ref={refs => (this.filterComp = refs)}
						category={category}
						filter={this.state.filter}
						onSave={this.onSearch}
					/>
				</Container>
			</>
		);
	}
}

export default Tables;
