import React from "react";
import {
	Badge,
	Card,
	CardHeader,
	CardFooter,
	DropdownMenu,
	DropdownItem,
	UncontrolledDropdown,
	DropdownToggle,
	Media,
	Pagination as Paginations,
	PaginationItem,
	PaginationLink,
	Progress,
	Container,
	Row,
	UncontrolledTooltip
} from "reactstrap";
import {
	Table,
	Modal,
	Button,
	FormGroup,
	ControlLabel,
	Icon,
	Notification,
	ButtonToolbar,
	Col,
	Input
} from "rsuite";

import {Form, Field} from "react-final-form";
import {
	InputField,
	DateField,
	SelectField,
	InputNumberField
} from "components/common/FinalFormComponent";
import {CommonContext} from "context";
import moment from "moment";
const {Column, HeaderCell, Cell, Pagination} = Table;

class Tables extends React.Component {
	static contextType = CommonContext;
	constructor(props) {
		super(props);
		this.state = {
			filter: {
				paging: {
					pageSize: 25,
					currentPage: 1
				}
			}
		};
	}

	createDumyData = () => {
		let data = [];
		for (let i = 0; i < 100; i++) {
			data.push({
				fullName: "Họ tên " + i,
				dateOfBirth: new Date(),
				email: "Email " + i,
				userName: "Tên đăng nhập " + i,
				passWord: "Mật Khẩu " + i,
				rePassWord: "Nhập lại mật khẩu " + i
			});
		}
		this.data = data;
	};
	componentDidMount() {
		this.createDumyData();
		this.getData();
	}

	onSearch = () => {
		this.state.filter.paging.currentPage = 1;
		this.getData();
	};

	getData = () => {
		// let {loading, unloading, error, success } = this.context;
		// loading();
		this.state.filter.paging.rowsCount = this.data.length;
		let paging = this.state.filter.paging;
		let start = (paging.currentPage - 1) * paging.pageSize;
		let end = start + paging.pageSize;
		if (end > this.data.length) end = this.data.length;
		let datalist = [];
		for (let i = start; i < end; i++) {
			datalist.push(this.data[i]);
		}
		this.setState({list: datalist});
		setTimeout(() => {
			// unloading()
		}, 2000);
	};

	onSubmit = values => {
		console.log(values);
	};

	onPressEnter = e => {
		this.onSearch();
	};

	onSave = () => {
		this.close();
		Notification.success({
			title: "Thông báo",
			description: "Lưu dữ liệu thành công."
		});
	};

	onDelete = data => {
		this.confirmBox.current.show();
	};

	onAdd = () => {
		this.open("md");
	};

	onEdit = data => {
		this.open("md");
	};

	/**
	 * close form
	 */
	close = () => {
		this.setState({
			show: false
		});
	};
	/**
   open form
   */
	open = size => {
		this.setState({
			size,
			show: true
		});
	};

	handleChangePage = currentPage => {
		console.log(currentPage);
		this.state.filter.paging.currentPage = currentPage;
		this.getData();
	};
	handleChangeLength = pageSize => {
		this.state.filter.paging.pageSize = pageSize;
		this.state.filter.paging.currentPage = 1;
		this.getData();
	};

	renderForm = () => {
		const {size} = this.state;

		return (
			<>
				<Modal size={size} show={this.state.show} onHide={this.close}>
					<Modal.Header>
						<Modal.Title>Title</Modal.Title>
					</Modal.Header>
					<Modal.Body>
						<Form
							onSubmit={this.onSubmit}
							initialValues={{}}
							//decorators={[this.onChangeDependentField]}
						>
							{({
								handleSubmit,
								pristine,
								invalid,
								values,
								form: {
									mutators: {push, pop}
								},
								form
							}) => {
								this.form = form;

								return (
									<form onSubmit={handleSubmit}>
										<FormGroup className="d-flex mb-2">
											<ControlLabel className="col-4">Họ tên</ControlLabel>
											<Field
												name="fullName"
												className="col-8"
												component={InputField}
												placeholder="Họ tên"
											/>
										</FormGroup>

										<FormGroup className="d-flex mb-2">
											<ControlLabel className="col-4">Ngày sinh</ControlLabel>
											<Field
												name="dateOfBirth"
												className="col-8 p-0"
												component={DateField}
												placeholder="Ngày sinh"
												formatDate="DD/MM/YYYY"
												style={{
													opacity: 1,
													padding: "8px 15px",
													border: "1px solid #cccccc",
													fontSize: 14,

													borderRadius: 10
												}}
												block
											/>
										</FormGroup>

										<FormGroup className="d-flex mb-2">
											<ControlLabel className="col-4">Email</ControlLabel>
											<Field
												name="email"
												className="col-8"
												component={InputField}
												placeholder="Email"
											/>
										</FormGroup>

										<FormGroup className="d-flex mb-2">
											<ControlLabel className="col-4">Tên đăng nhập</ControlLabel>
											<Field
												name="userName"
												className="col-8"
												component={InputField}
												placeholder="Tên đăng nhập"
											/>
										</FormGroup>

										<FormGroup className="d-flex mb-2">
											<ControlLabel className="col-4">Mật Khẩu</ControlLabel>
											<Field
												name="passWord"
												className="col-8"
												component={InputField}
												placeholder="Mật Khẩu"
											/>
										</FormGroup>

										<FormGroup className="d-flex mb-2">
											<ControlLabel className="col-4">Nhập lại mật khẩu</ControlLabel>
											<Field
												name="rePassWord"
												className="col-8"
												component={InputField}
												placeholder="Nhập lại mật khẩu"
											/>
										</FormGroup>
									</form>
								);
							}}
						</Form>
					</Modal.Body>
					<Modal.Footer>
						<Button onClick={() => this.onSave()} appearance="primary">
							Ok
						</Button>
						<Button onClick={this.close} appearance="subtle">
							Cancel
						</Button>
					</Modal.Footer>
				</Modal>
			</>
		);
	};

	render() {
		const {loading, list} = this.state;
		var paging = this.state.filter.paging;
		return (
			<>
				<Container className="mt-4" fluid>
					<Row>
						<div className="col">
							<Card className="shadow">
								<CardHeader className="bg-white border-0">
									<Row className="align-items-center">
										<Col xs="20">
											<h3 className="mb-0">My account</h3>
										</Col>
										<Col className="text-right" xs="4">
											<Button
												color="primary"
												href="#pablo"
												onClick={() => {
													this.onAdd("md");
												}}
												size="sm"
											>
												Thêm mới
											</Button>
										</Col>
									</Row>
								</CardHeader>
								<div className="mt-2 p-2 mb-1 border ">
									<Table height={window.innerHeight * 0.7} data={list} loading={loading}>
										<Column flexGrow={2}>
											<HeaderCell>Họ tên</HeaderCell>
											<Cell dataKey="fullName" />
										</Column>

										<Column width={200} align="left" resizable>
											<HeaderCell>Ngày sinh</HeaderCell>
											<Cell>
												{rowData => {
													return <div />;
												}}
											</Cell>
										</Column>

										<Column flexGrow={2}>
											<HeaderCell>Email</HeaderCell>
											<Cell dataKey="email" />
										</Column>

										<Column flexGrow={2}>
											<HeaderCell>Tên đăng nhập</HeaderCell>
											<Cell dataKey="userName" />
										</Column>

										<Column flexGrow={2}>
											<HeaderCell>Hoạt động</HeaderCell>
											<Cell dataKey="passWord" />
										</Column>
										<Column width={120} fixed="right" align="center">
											<HeaderCell>{"Action"}</HeaderCell>
											<Cell>
												{rowData => {
													return (
														<>
															<a
																href="javascript:void(0)"
																className="pd-5"
																onClick={() => {
																	this.onEdit(rowData);
																}}
															>
																<Icon icon="edit" />
															</a>
															<a
																href="javascript:void(0)"
																className="pd-5"
																onClick={() => {
																	this.onDelete(rowData);
																}}
															>
																<Icon icon="trash-o" />
															</a>
														</>
													);
												}}
											</Cell>
										</Column>
									</Table>

									{this.renderForm()}
								</div>
								<CardFooter className="py-4">
									<Pagination
										lengthMenu={[
											{
												value: 25,
												label: 25
											},
											{
												value: 50,
												label: 50
											}
										]}
										activePage={paging.currentPage}
										displayLength={paging.pageSize}
										total={paging.rowsCount}
										onChangePage={this.handleChangePage}
										onChangeLength={this.handleChangeLength}
									/>
								</CardFooter>
							</Card>
						</div>
					</Row>
				</Container>
			</>
		);
	}
}

export default Tables;
