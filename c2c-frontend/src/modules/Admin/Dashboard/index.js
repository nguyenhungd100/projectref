
import React from "react";
import classnames from "classnames";
import {
	Badge,
	Card,
	CardHeader,
	CardFooter,
	DropdownMenu,
	DropdownItem,
	UncontrolledDropdown,
	DropdownToggle,
	Media,
	Pagination as Paginations,
	PaginationItem,
	PaginationLink,
	Progress,
	Container,
	Row,
	UncontrolledTooltip
} from "reactstrap";
import {
	Table,
	Modal,
	Button,
	FormGroup,
	ControlLabel,
	Icon,
	Notification,
	ButtonToolbar,
	Col,
	Input, IconButton, Whisper, Popover
} from "rsuite";
import CurrencyFormat from 'react-currency-format';
import statisticApi from "api/statisticApi";
import Header from "components/Admin/Headers/Header";
import PropTypes from 'prop-types'
import { BarChart, Bar, XAxis, YAxis, CartesianGrid, Tooltip, Legend } from 'recharts'
var numeral = require('numeral');
class CustomTooltip extends React.Component {
	static propTypes = {
		type: PropTypes.string,
		payload: PropTypes.array,
		label: PropTypes.string,
	}

	render() {
		const { active } = this.props;
		if (active) {
			const { payload, label } = this.props;
			if (!payload) return null;
			return (
				<div style={{ padding: 10, borderRadius: 5, backgroundColor: '#e6e6e6' }}>
					{
						payload.map((item, index) => (
							<p key={index + 'key'}>
								{`${payload[index].name} : `}
								<CurrencyFormat
									value={payload[index].value ? numeral(payload[index].value).format('0,0') : 0}
									displayType={'text'}
									thousandSeparator={true}
									suffix={''}
								/>
							</p>
						))
					}

				</div>
			);
		}

		return null;
	}
}
export default class extends React.Component {
	constructor(props) {
		super(props)

		this.state = {
			model: {
				year: new Date().getFullYear()
			},
			workitemChart: [],
			heightChart: 0.6 * window.innerHeight,
			widthChart: 0.7 * window.innerWidth,
			totalNumberOfPosts: 0,
			totalNumberOfShops: 0,
			year: new Date().getFullYear(),
		}
	}
	componentDidMount() {
		this.getData();
	}
	getData = async () => {
		let { model, workitemChart, totalNumberOfPosts, totalNumberOfShops } = this.state;
		let res = await statisticApi.general(model)
		if (res && res.success) {
			workitemChart = ((res.data && res.data.statisticInfos) || []).map(({ month: name, numberOfPosts: uv, numberOfShopCreated: pv, ...rest }) => ({ name: "T" + name, uv: uv, pv: pv, ...rest }));
			totalNumberOfPosts = res.data && res.data.totalNumberOfPosts;
			totalNumberOfShops = res.data && res.data.totalNumberOfShops;
		}
		this.setState({ workitemChart, totalNumberOfPosts, totalNumberOfShops })
	}
	render() {
		let { heightChart, widthChart, workitemChart, totalNumberOfPosts, totalNumberOfShops, year } = this.state;
		return (
			<>
				<Header year={year} totalNumberOfPosts={totalNumberOfPosts} totalNumberOfShops={totalNumberOfShops} />
				<Container className="mt-4" fluid>
					<Row>
						<div className="col">
							<Card className="shadow">
								<CardHeader className="bg-white border-0">
									<div className="d-flex justify-content-between mb-1">
										<h5>
											<Icon icon="th-list" style={{ marginRight: 20 }} />
											Biểu đồ	thống kê
										</h5>

									</div>
								</CardHeader>
								<div className="mt-2 p-2 mb-1 border ">
									<BarChart
										width={widthChart} height={heightChart} data={workitemChart}
										margin={{ top: 20, right: 0, left: 0, bottom: 5 }}
									>
										<CartesianGrid strokeDasharray="3 3" />
										<XAxis dataKey="name" />
										<YAxis />
										<Tooltip content={<CustomTooltip />} />
										<Legend />
										<Bar dataKey="uv" fill="green" name="Số bài đăng " />
										<Bar dataKey="pv" fill="blue" name="Số cửa hàng tạo " />
									</BarChart>
								</div>

							</Card>
						</div>
					</Row>
				</Container>
			</>
		);
	}
}
