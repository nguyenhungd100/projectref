import React from "react";
import {
    Badge,
    Card,
    CardHeader,
    CardFooter,
    DropdownMenu,
    DropdownItem,
    UncontrolledDropdown,
    DropdownToggle,
    Media,
    Pagination as Paginations,
    PaginationItem,
    PaginationLink,
    Progress,
    Container,
    Row,
    UncontrolledTooltip
} from "reactstrap";
import {
    Table,
    Modal,
    Button,
    FormGroup,
    ControlLabel,
    Icon,
    Notification,
    ButtonToolbar,
    Col,
    Input, IconButton, Whisper, Popover
} from "rsuite";

import { Form, Field } from "react-final-form";
import {
    InputField,
    DateField,
    SelectField,
    InputNumberField,
    RSTextAreaField
} from "components/common/FinalFormComponent";
import { CommonContext } from "context";
import moment from "moment";
import statisticApi from "api/statisticApi";
const { Column, HeaderCell, Cell, Pagination } = Table;
class Tables extends React.Component {
    static contextType = CommonContext;
    constructor(props) {
        super(props);
        this.state = {
            data: [],
            category: [],
            showFilter: false,
            filter: {
                paging: {
                    pageSize: 25,
                    currentPage: 1
                }
            },
            model: {
                year: 2019
            }
        };
    }

    componentDidMount() {
        this.getData();
    }
    getData = async () => {
        let { model } = this.state;
        let res = await statisticApi.general(model)
    }



    render() {
        const { loading, data, category } = this.state;
        var { paging } = this.state.filter;
        return (
            <>
                <Container className="mt-4" fluid>
                    <Row>
                        <div className="col">
                            <Card className="shadow">
                                <CardHeader className="bg-white border-0">
                                    <div className="d-flex justify-content-between mb-1">
                                        <h5>
                                            <Icon icon="th-list" size="1x" style={{ marginRight: 20 }} />
                                            Dashboard
										</h5>

                                    </div>
                                </CardHeader>


                            </Card>
                        </div>
                    </Row>
                </Container>
            </>
        );
    }
}

export default Tables;
