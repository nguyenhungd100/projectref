import React from "react";
import {
    Card,
    CardHeader,
    CardFooter,
    Container,
    Row,
} from "reactstrap";
import {
    Table,
    Modal,
    Button,
    FormGroup,
    ControlLabel,
    Icon,
    Notification,
    ButtonToolbar,
    IconButton,
    Whisper, Popover
} from "rsuite";

import { Form, Field } from "react-final-form";
import {
    InputField,
    DateField,
    SelectField,
    InputNumberField
} from "components/common/FinalFormComponent";
import { CommonContext } from "context";
import moment from "moment";
import postApi from "api/postApi";
const { Column, HeaderCell, Cell, Pagination } = Table;

export default class extends React.Component {
    static contextType = CommonContext;
    constructor(props) {
        super(props);
        this.state = {
            data: [],
            showFilter: false,
            filter: {
                paging: {
                    pageSize: 50,
                    currentPage: 1
                }
            }
        };
    }

    componentDidMount() {
        this.getData();
    }

    getData = async () => {
        let res = await postApi.get_denounce_list(this.state.filter);
        if (res && res.success) {
            this.setState({
                data: res.success.records,
                loading: false
            })
        }
    };

    handleChangePage = currentPage => {
        this.setState({ filter: { ...this.state.filter, paging: { ...this.state.filter.paging, currentPage: currentPage } } }, () => this.getData())
    };

    handleChangeLength = pageSize => {
        this.state.filter.paging.pageSize = pageSize;
        this.state.filter.paging.currentPage = 1;
        this.getData();
    };
    // Anh viết nốt hàm onCLickRow với sửa lại cái icon của module này nhé :))
    onRowClick = (value) => {
        console.log(value, "value")
        // this.props.history.push("");
    }
    render() {
        const { loading, data, filter } = this.state;
        console.log(data, "datadata")
        var { paging } = this.state.filter;
        return (
            <>
                <Container className="mt-4" fluid>
                    <Row>
                        <div className="col">
                            <Card className="shadow">
                                <CardHeader className="bg-white border-0">
                                    <div className="d-flex justify-content-between mb-1">
                                        <h5>
                                            <Icon icon="th-list" size="1x" style={{ marginRight: 20 }} />
                                            Danh sách tin tố cáo
										</h5>
                                    </div>
                                </CardHeader>
                                <div className="mt-2 p-2 mb-1 border ">
                                    <Table
                                        cellBordered
                                        // autoHeight
                                        wordWrap={true}
                                        height={window.innerHeight * 0.7}
                                        data={data}
                                        onRowClick={this.onRowClick}
                                        loading={loading}
                                    >
                                        <Column flexGrow={2}>
                                            <HeaderCell>Tên bài đăng</HeaderCell>
                                            <Cell dataKey="postName" />
                                        </Column>


                                        <Column flexGrow={2}>
                                            <HeaderCell>Người tố cáo</HeaderCell>
                                            <Cell dataKey="userName" />
                                        </Column>
                                        <Column flexGrow={2}>
                                            <HeaderCell>Ngày tố cáo</HeaderCell>
                                            <Cell>
                                                {rowData => {
                                                    return <span>{rowData.createdDate && moment(rowData.createdDate).format("DD-MM-YYYY")}</span>;
                                                }}
                                            </Cell>
                                        </Column>
                                        <Column flexGrow={3}>
                                            <HeaderCell>Nội dung tố cáo</HeaderCell>
                                            <Cell dataKey="reason" />
                                        </Column>

                                    </Table>

                                </div>
                                <CardFooter className="py-4">
                                    <Pagination
                                        lengthMenu={[
                                            {
                                                value: 25,
                                                label: 25
                                            },
                                            {
                                                value: 50,
                                                label: 50
                                            }
                                        ]}
                                        activePage={paging.currentPage}
                                        displayLength={paging.pageSize}
                                        total={paging.rowsCount}
                                        onChangePage={this.handleChangePage}
                                        onChangeLength={this.handleChangeLength}
                                    />
                                </CardFooter>
                            </Card>
                        </div>
                    </Row>
                </Container>
            </>
        );
    }
}

