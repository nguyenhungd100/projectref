import React, { Component } from 'react';
import ChatComponent from "components/chatComponent";
import { CommonContext } from "context";
export default class Support extends Component {
    static contextType = CommonContext;
    render() {
        const { profile } = this.context;

        return (
            <div>{profile && <ChatComponent />}</div>
        );
    }
}
