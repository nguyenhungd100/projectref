import BaseApi from "./BaseApi";
const api = new BaseApi();
const { make, post, get, postWithoutToken, upload, multiUpload, getWithoutToken, del } = api;

const postApi = {
    caring: async model => {
        let url = make("Post", "caring");
        return await post(url, model);
    },
    getByIdStore: async (id) => {
        let url = make("Post", "shop_pages/" + id);
        return await getWithoutToken(url);
    },
    approve: async id => {
        let url = make("Post", "approve_post/" + id);
        return await post(url, id);
    },
    delete: async ids => {
        let url = make("Post", "delete_many");
        return await del(url, ids);
    },
    reject: async id => {
        let url = make("Post", "reject_post/" + id);
        return await post(url, id);
    },
    getImage: (id, width = 200, height = 200) => {
        const url = make("Post", "post_image/" + id, width.toString(), height.toString());
        return url;
    },
    upImage: async files => {
        const url = make("Post", "post_image", "save");
        return upload(url, files[0].blobFile);
    },
    list: async filter => {
        const url = make("Post", "search");
        return postWithoutToken(url, filter);
    },
    getById: async (id) => {
        let url = make("Post", "info/" + id);
        return await getWithoutToken(url);
    },
    save: async model => {
        let url = make("Post", "save");
        return await post(url, model);
    },
    listStore: async model => {
        let url = make("Post", "shop_pages", "search");
        return await post(url, model);
    },
    addStore: async model => {
        let url = make("Post", "shop_pages", "save");
        return await post(url, model);
    },
    denounce: async model => {
        let url = make("Post", "denounce");
        return await post(url, model);
    },
    get_denounce_list: async model => {
        let url = make("Post", "get_denounce_list");
        return await post(url, model);
    },
};

export default postApi;
