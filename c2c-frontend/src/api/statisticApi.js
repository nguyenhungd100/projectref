import BaseApi from './BaseApi';
const api = new BaseApi();
const { make, post, get, put, postWithoutToken } = api;
const statisticApi = {
    general: async filter => {
        const url = make("Statistic", "general");
        return post(url, filter);
    },
};

export default statisticApi;