import BaseApi from './BaseApi';

const baseApi = new BaseApi();
export const CompanyApi = {};

CompanyApi.getAllCompanies = async () => {
    try {
        const urlBase = baseApi.makeUrl('v1', 'company', 'get_all_companies');
        const result = await baseApi.get(urlBase);
        return result;
    } catch (error) {
        console.error(error);
    }
}
CompanyApi.getAllCompany = async (model) => {
    try {
        let urlBase = baseApi.makeUrl('v1', 'company', 'search-companies');
        let result = await baseApi.post(urlBase, model);
        return result;
    } catch (error) {
        console.error(error);
    }
}
CompanyApi.saveCompany = async (model) => {
    try {
        let urlBase = baseApi.makeUrl('v1', 'company', 'save-company');
        let result = await baseApi.post(urlBase, model);
        return result;
    } catch (error) {
        console.error(error);
    }
}
CompanyApi.deleteCompany = async (model) => {
    try {
        let urlBase = baseApi.makeUrl('v1', 'company', 'delete-multi-companies');
        let result = await baseApi.delete(urlBase, model);
        return result;
    } catch (error) {
        console.error(error);
    }
}
