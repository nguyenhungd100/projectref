import BaseApi from "./BaseApi";
const api = new BaseApi();
const { make, post, get, ImageWithoutToken, upload, multiUpload, getWithoutToken, del, makeFormData } = api;

const transactionApi = {

    checkUrl: async (model) => {
        let url = make("Transaction", "get_check_out_url");
        return await post(url, model);
    },
    list: async (userid) => {
        let url = make("Transaction", "transactions/" + userid);
        return await get(url);
    },
    checkOut: async (model) => {
        let url = make("Transaction", "verify_check_out");
        return await post(url, model);
    },
};

export default transactionApi;
