import Excel from "exceljs/dist/es5/exceljs.browser";

function exportExcelToDownload(workbook) {
    workbook.xlsx.writeBuffer({ base64: true })
        .then(function (xlsx64) {
            const a = document.createElement("a");
            const data = new Blob([xlsx64], {
                type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"
            });
            const url = URL.createObjectURL(data);
            a.href = url;
            a.download = "bang_thanh_toan.xlsx";
            document.body.appendChild(a);
            a.click();
        });
}

export function createExelPaymentBill(payments) {
    const workbook = new Excel.Workbook();
    const sheet = workbook.addWorksheet('Bảng thanh toán');

    sheet.columns = [
        { header: 'Hạng mục', key: 'name', width: 32, alignment: 'center' },
        { header: 'Số lượng', key: 'quantity', width: 10 },
        { header: 'Đơn vị.', key: 'calculationUnit', width: 20 },
        { header: 'Đơn giá', key: 'money', width: 20 },
        { header: 'Số lượng đã thanh toán', key: 'totalPaidQuantity', width: 30 },
        { header: 'Thành tiền', key: 'amount', width: 20 }
    ];

    payments.forEach(payment => {
        sheet.addRow({ ...payment });
    });
    exportExcelToDownload(workbook);
};
