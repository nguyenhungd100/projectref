import BaseApi from "./BaseApi";
const api = new BaseApi();
const { make, post, get, postWithoutToken, getWithoutToken } = api;

const categoryApi = {
    list: async filter => {
        const url = make("Category", "categories");
        return getWithoutToken(url, filter);
    },
    getById: async id => {
        let url = make("Category", "get-user-info", `?userId=${id}`);
        return await get(url);
    },
    listProvinces: async () => {
        let url = make("ProvinceAndDistrict", "provinces");
        return await get(url);
    },
    listDistricts: async () => {
        let url = make("ProvinceAndDistrict", "districts");
        return await get(url);
    },
    updateProfile: async model => {
        let url = make("Category", "update-profile");
        return await post(url, model);
    },
    reset_password: async (model) => {
        let url = make("Category", "reset-password");
        return await post(url, model);
    },
    change_password: async data => {
        let url = make("Category", "change-password");
        return await post(url, data);
    },
    delete: async id => {
        let url = make("Category", `${id}`, "remove");
        return await post(url);
    },
    addClient: async user => {
        let url = make("Category", "create_user_side");
        return await postWithoutToken(url, user);
    },
    update: async user => {
        let url = make("Category", "update-user");
        return await post(url, user);
    },
    getMyRole: async () => {
        let url = make("Category", "my-roles");
        return await get(url);
    },
    upadteMyRole: async model => {
        let url = make("Category", "update-role");
        return await post(url, model);
    },
    listroles: async () => {
        let filter = {
            roneName: "",
            sortBy: "",
            pageIndex: 1,
            pageSize: 100
        };

        let url = make("role", "list");
        return await post(url, filter);
    },
    listPermissions: async () => {
        let url = make("role", "roles", "list-permissions");
        return await get(url);
    },

    setUserCalendar: async model => {
        const url = make("Category", "calendar", "save");
        var result = await post(url, model);
        return result;
    },
    deleteUserCalendar: async id => {
        const url = make("Category", "calendar", "delete", id.toString());
        var result = await post(url);
        return result;
    },
    getUserCalendars: async userId => {
        const url = make("Category", userId.toString(), "calendars");
        var result = await get(url);
        return result.data;
    },
    getListCalendars: async filter => {
        const url = make("Category", "calendar", "get_list");
        return post(url, filter);
    },
    getCalendars: async () => {
        const url = make("Category", "calendar", "get");
        return get(url);
    },
    delegacy: async filter => {
        const url = make("Category", "delegacy");
        return post(url, filter);
    },
    evictDelegacy: async filter => {
        const url = make("Category", "evict-delegacy");
        return post(url, filter);
    },
    listDelegacy: async () => {
        const url = make("Category", "list-delegacy");
        return get(url);
    },

    //Ủy quyền cho chuyên viên
    permissionsList: async () => {
        const url = make("Category", "permissions");
        return get(url);
    },
    delegateUser: async filter => {
        const url = make("Category", "delegateuser");
        return post(url, filter);
    },
    getUsersDelegated: async () => {
        const url = make("Category", "getusersdelegated");
        return get(url);
    },
    removeDelegate: async filter => {
        const url = make("Category", "removedelegate");
        return post(url, filter);
    },

};

export default categoryApi;
