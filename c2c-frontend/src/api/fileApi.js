import BaseApi from "./BaseApi";
const api = new BaseApi();
const { make, post, get, ImageWithoutToken, upload, multiUpload, getWithoutToken, del, makeFormData } = api;

const fileApi = {

    save: async (file, userId, shopPageId) => {

        let url = make("Image", "save?" + (userId ? `UserId=${userId}` : `ShopPageId=${shopPageId}`));
        return await upload(url, file);
    },
    getImage: (id, width = 200, height = 200) => {
        if (!id) return "/assets/images/user.png";
        const url = make("Image/" + id, width.toString(), height.toString());
        return url;
    },
    upImage: async files => {
        const url = make("Post", "post_image", "save");
        return upload(url, files);
    },
};

export default fileApi;
