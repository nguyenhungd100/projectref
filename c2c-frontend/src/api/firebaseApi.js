
import React, { Component } from "react";

import { mydatabase } from 'components/configFirebase/firebase';
import { hasId } from "constants/common";
const messageRef = mydatabase.ref(`Converation/messages`);
const topicRef = mydatabase.ref(`Converation/topics`);
const initTopic = topicModel => {
    const { topicKey, messageKey, userName } = topicModel;
    const update = {
    };
    let model = {
        userId: topicModel.userId,
        createdImageId: "74e06a49-f3bf-4e3d-bf52-f025fcda254f",
        messageKey,
        createdDate: new Date(),
        createdId: hasId,
        [hasId]: hasId,
        [topicModel.userId]: topicModel.userId,
        createnName: "Bộ phận hỗ trợ",
        userName,
        topicKey
    }

    update[`Converation/topics/${topicKey}`] = model;
    mydatabase.ref().update(update);
};

const initMessage = newMessageKey => {

    const initMessageModel = {
        sender: {
            name: "Bộ phận hỗ trợ",
            imageId: "74e06a49-f3bf-4e3d-bf52-f025fcda254f",
            id: hasId
        },
        senderId: hasId,
        createdDate: new Date(),
        content: "Bộ phận hỗ trợ khách hàng xin kính chào quý khách.",
    };
    messageRef.child(newMessageKey).push(initMessageModel);
};
const onAddTopic = topicModel => {
    const newMessageKey = messageRef.push().key;
    const newTopicKey = topicRef.push().key;
    topicModel.messageKey = newMessageKey;
    topicModel.topicKey = newTopicKey;
    initMessage(newMessageKey);
    initTopic(topicModel);
    return true;
};



const firebaseApi = {
    send: async (model) => {
        let res = await onAddTopic(model)
        return res;
    }
};

export default firebaseApi
