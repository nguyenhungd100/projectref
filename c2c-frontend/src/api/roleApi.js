import BaseApi from './BaseApi';
const api = new BaseApi();
const { make, post, get, put, postWithoutToken } = api;
const roleApi = {
    list: async filter => {
        const url = make("Role", "search");
        return post(url, filter);
    },
    save: async filter => {
        const url = make("Role", "save");
        return post(url, filter);
    },
};

export default roleApi;