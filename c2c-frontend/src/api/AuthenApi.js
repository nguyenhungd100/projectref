
export function fetchAccessToken(uid, pwd) {
    return new Promise(function (resolve, reject) {

        let tokenUrl = process.env.REACT_APP_AUTH_URL;
        let client_id = "ro.client";
        let client_secret = "secret";
        let xhr = new XMLHttpRequest();
        xhr.onload = function (e) {
            let response_data = JSON.parse(xhr.response);
            if (xhr.status === 200 && response_data.access_token) {
                resolve({ success: true, response_data: JSON.stringify(response_data) });
            } else {
                resolve({ success: false, message: response_data && response_data.error_description?response_data.error_description:'' });
            }
        }
        xhr.onerror = function () {
            reject({
                status: this.status,
                statusText: xhr.statusText
            });
        };
        xhr.open("POST", tokenUrl);
        let data = {
            username: uid,
            password: pwd,
            grant_type: "password",
            scope: "openid offline_access exam_api",
            client_id: client_id,
            client_secret: client_secret
        };

        let body = "";
        for (let key in data) {
            if (body.length) {
                body += "&";
            }
            body += key + "=";
            body += encodeURIComponent(data[key]);
        }
        xhr.setRequestHeader("Authorization", "Basic " + btoa(client_id + ":" + client_secret));
        xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");

        xhr.send(body);
    });
}
