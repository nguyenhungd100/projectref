import BaseApi from "./BaseApi";
const api = new BaseApi();
const { make, post, get, put, postWithoutToken } = api;

const userApi = {
    list: async filter => {
        const url = make("user", "search");
        return post(url, filter);
    },
    getById: async id => {
        let url = make("user", "get-user-info", `?userId=${id}`);
        return await get(url);
    },
    profile: async () => {
        let url = make("User", "my_profile");
        return await get(url);
    },
    updateProfile: async model => {
        let url = make("user", "update-profile");
        return await post(url, model);
    },
    reset_password: async (model) => {
        let url = make("user", "reset-password");
        return await post(url, model);
    },
    change_password: async data => {
        let url = make("user", "change-password");
        return await post(url, data);
    },
    delete: async id => {
        let url = make("user", `${id}`, "remove");
        return await post(url);
    },
    addClient: async user => {
        let url = make("User", "create_user_side");
        return await postWithoutToken(url, user);
    },
    createUserOnCms: async user => {
        let url = make("User", "create_user_on_cms");
        return await post(url, user);
    },
    lockAccount: async userId => {
        let url = make("User", "lock_account/" + userId);
        return await put(url, userId);
    },
    verify: async user => {
        let url = make("User", "verify_otp_login");
        return await postWithoutToken(url, user);
    },
    update: async user => {
        let url = make("user", "update-user");
        return await post(url, user);
    },
    getMyRole: async () => {
        let url = make("user", "my-roles");
        return await get(url);
    },
    upadteMyRole: async model => {
        let url = make("user", "update-role");
        return await post(url, model);
    },
    updateProfile: async model => {
        let url = make("user", "update_my_profile");
        return await put(url, model);
    },

    listroles: async () => {
        let filter = {
            roneName: "",
            sortBy: "",
            pageIndex: 1,
            pageSize: 100
        };

        let url = make("role", "list");
        return await post(url, filter);
    },

    listPermissions: async () => {
        let url = make("role", "roles", "list-permissions");
        return await get(url);
    },

    setUserCalendar: async model => {
        const url = make("user", "calendar", "save");
        var result = await post(url, model);
        return result;
    },
    deleteUserCalendar: async id => {
        const url = make("user", "calendar", "delete", id.toString());
        var result = await post(url);
        return result;
    },
    getUserCalendars: async userId => {
        const url = make("user", userId.toString(), "calendars");
        var result = await get(url);
        return result.data;
    },
    getListCalendars: async filter => {
        const url = make("user", "calendar", "get_list");
        return post(url, filter);
    },
    getCalendars: async () => {
        const url = make("user", "calendar", "get");
        return get(url);
    },
    delegacy: async filter => {
        const url = make("user", "delegacy");
        return post(url, filter);
    },
    evictDelegacy: async filter => {
        const url = make("user", "evict-delegacy");
        return post(url, filter);
    },
    listDelegacy: async () => {
        const url = make("user", "list-delegacy");
        return get(url);
    },

    //Ủy quyền cho chuyên viên
    permissionsList: async () => {
        const url = make("user", "permissions");
        return get(url);
    },
    delegateUser: async filter => {
        const url = make("user", "delegateuser");
        return post(url, filter);
    },
    getUsersDelegated: async () => {
        const url = make("user", "getusersdelegated");
        return get(url);
    },
    removeDelegate: async filter => {
        const url = make("user", "removedelegate");
        return post(url, filter);
    },

};

export default userApi;
