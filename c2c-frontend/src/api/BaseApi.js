var urljoin = require('url-join');

export default class BaseApi {
    constructor() {
        this.baseUrl = process.env.REACT_APP_API_URL;
        // this.baseUrl =''
    }

    make = (...args) => {
        var path = urljoin(args);
        return urljoin(this.baseUrl, path);
    };
    call = async (url, method, model, noAuth) => {
        var access_token = undefined;
        try {
            var strToken = localStorage.getItem("token");
            var token = JSON.parse(strToken);

            if (token && token.access_token) access_token = token.access_token;
            else access_token = null;

            var headers = {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + access_token,
            };

            if (noAuth) {
                headers.Authorization = undefined
            }
            let request = {
                method: method,
                headers: headers,
            };
            if (method === 'POST' || method === "PUT" || "DELETE") {
                request.body = JSON.stringify(model)
            }
            if (method === "UPLOAD") {
                const headers = {
                    Authorization: "Bearer " + access_token
                };
                request.headers = headers;
                request.method = "POST";
                request.body = model;
            }
            let response = await fetch(url, request);
            if (!response.ok) {
                if (response.status == 401) {
                    // localStorage.removeItem("token");
                }
                // swal("Truy cập bị từ chối!", "Bạn không có quyền truy cập tính năng này!", "error");
                return;
            }
            let responseJson = await response.json();
            return responseJson;
        } catch (e) {
            if (!noAuth) {
                localStorage.removeItem("token");
                localStorage.removeItem("userid");
            }


        }
    };

    get = async (url) => {
        return await this.call(url, "GET");
    };
    post = async (url, model) => {
        return await this.call(url, "POST", model);
    };
    postWithoutToken = async (url, model) => {
        return await this.call(url, "POST", model, true);
    };
    getWithoutToken = async (url, model) => {
        return await this.call(url, "GET", model, true);
    };

    put = async (url, model) => {
        return await this.call(url, "PUT", model);
    };
    del = async (url, model) => {
        return await this.call(url, "DELETE", model);
    };

    getToken = () => {
        var token = localStorage.getItem('token');
        if (token === undefined) {
            window.location.href = '/';
            return;
        }

        return JSON.parse(token);
    };

    makeFormData(model) {
        const formData = new FormData();
        Object.entries(model)
            .forEach(([key, value]) => {
                formData.append(key, value);
            });

        return formData;
    }

    upload = async (url, file) => {
        let model = new FormData();
        model.append("Data", file);
        return await this.call(url, "UPLOAD", model);
    };
    multiUpload = async (url, files) => {
        console.log(files);
        let model = new FormData();
        let i = 0;
        files.forEach(file => {
            model.append("Data" + i, file);
            i++;
        });
        return await this.call(url, "UPLOAD", model);
    };

}
