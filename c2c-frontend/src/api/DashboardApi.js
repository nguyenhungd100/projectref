import BaseApi from './BaseApi';
const baseApi = new BaseApi();

export const DashboardApi = {};

DashboardApi.getOverallInfo = async () => {
    try {
        let urlBase = baseApi.makeUrl('v1', 'project-report', 'workitem-comleted-number-per-month');
        let result = await baseApi.get(urlBase);
        return result;
    } catch (error) {
        console.error(error);
    }
}
DashboardApi.getMonthlyCompletedWorkitems  = async () => {
    try {
        let urlBase = baseApi.makeUrl('v1', 'project-report', 'workitem-comleted-number-per-month');
        let result = await baseApi.get(urlBase);
        return result;
    } catch (error) {
        console.error(error);
    }
}
DashboardApi.getQuarter  = async () => {
    try {
        let urlBase = baseApi.makeUrl('v1', 'project-report', 'workitem-comleted-number-per-quarter');
        let result = await baseApi.get(urlBase);
        return result;
    } catch (error) {
        console.error(error);
    }
}
