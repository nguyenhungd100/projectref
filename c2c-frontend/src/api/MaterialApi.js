import BaseApi from './BaseApi';

const baseApi = new BaseApi();

export const MaterialApi = {};

MaterialApi.searchMaterials = async (model) => {
    try {
        let url = baseApi.makeUrl('v1', 'register-materials', 'search');
        let result = await baseApi.post(url,model);
        return result;
    } catch (error) {
        console.error(error);
    }
}

MaterialApi.getListByProjectId = async (projectId) => {
    try {
        let urlBase = baseApi.makeUrl("v1", "register-materials", "get-list-by-projectId", projectId);
        let result = await baseApi.get(urlBase);
        return result;
    } catch (error) {
        console.error(error);
    }
}

MaterialApi.approve = async (model) => {
    try {
        let urlBase = baseApi.makeUrl("v1", "register-materials", "approve-register-materials");
        let result = await baseApi.post(urlBase,model);
        return result;
    } catch (error) {
        console.error(error);
    }
}
MaterialApi.reject = async (model) => {
    try {
        let urlBase = baseApi.makeUrl("v1", "register-materials", "reject-register-materials");
        let result = await baseApi.post(urlBase,model);
        return result;
    } catch (error) {
        console.error(error);
    }
}

MaterialApi.updateMaterial = async (model, hashId) => {
    try {
        let urlBase = baseApi.makeUrl("v1", "register-materials", "update-register-materials/"+hashId);
        let result = await baseApi.put(urlBase,model);
        return result;
    } catch (error) {
        console.error(error);
    }
}
MaterialApi.deleteMaterial = async (model, hashId) => {

    try {
        let urlBase = baseApi.makeUrl("v1", "register-materials", "delete-multi-materials/"+hashId);
        let result = await baseApi.delete(urlBase,model);
        return result;
    } catch (error) {
        console.error(error);
    }
}

MaterialApi.registerMaterialsModel = async (model, projectId) => {
    try{
        let url = baseApi.makeUrl('v1', 'project', projectId,'register-materials');
        let results = await baseApi.post(url, model);
        return results;
    }catch(error){
        console.log(error);
    }
}
