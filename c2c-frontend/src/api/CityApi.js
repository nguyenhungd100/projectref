import BaseApi from './BaseApi';

export class CityApi extends BaseApi {

    list = async (filter) => {
        try {
            let urlBase = this.makeUrl('v1', 'thanhpho', 'list');
            let result = await this.post(urlBase, filter);
            return result;
        } catch (error) {
            console.error(error);
        }
    }

    getById = async (cityId) => {
        try {
            let urlBase = this.makeUrl('v1', 'thanhpho', 'get_by_id?thanhphoId=' + cityId);
            let result = await this.get(urlBase);
            return result;
        } catch (error) {
            console.log(error);
        }
    }

    deleteCity = async (cityIds) => {
        try {
            let urlBase = this.makeUrl('v1', 'thanhpho', 'delete');
            let result = await this.delete(urlBase, cityIds);
            return result;
        } catch (error) {
            console.log(error);
        }
    }

    add = async (cityModel) => {
        try {
            let urlBase = this.makeUrl('v1', 'thanhpho', 'add');
            let result = await this.post(urlBase, cityModel);
            return result;
        } catch (error) {
            console.log(error);
        }
    }

    update = async (cityModel) => {
        try {
            let urlBase = this.makeUrl('v1', 'thanhpho', 'update');
            let result = await this.put(urlBase, cityModel);
            return result;
        } catch (error) {
            console.log(error);
        }
    }
}
