import BaseApi from "./BaseApi";
const api = new BaseApi();
const { make, post, get, postWithoutToken, upload, multiUpload, getWithoutToken, del } = api;

const commentApi = {

    list: async filter => {
        const url = make("Comment", "get_comments");
        return postWithoutToken(url, filter);
    },
    save: async model => {
        let url = make("Comment", "save");
        return await post(url, model);
    },

};

export default commentApi;
