import React from "react";
const Support = React.lazy(() => import("./modules/Admin/Support"));
const Home = React.lazy(() => import("./modules/Home"));

const ListProduct = React.lazy(() => import("./modules/Product/List"));

const LinkError = React.lazy(() => import("./components/LinkError"));

const ProductDetail = React.lazy(() => import("./modules/Product/Detail"));

const CreateForm = React.lazy(() => import("./modules/PostInfo"));
const Login = React.lazy(() => import("./modules/Auth/Login"));
const Signup = React.lazy(() => import("./modules/Auth/Signup"));

const Dashboard = React.lazy(() => import("./modules/Admin/Dashboard"));
// const MyProfile = React.lazy(() => import("./modules/Admin/Dashboard/examples/Profile"));
const HistoryPayment = React.lazy(() => import("./modules/Payments/History"));
const Payment = React.lazy(() => import("./modules/Payments"));
const User = React.lazy(() => import("./modules/Admin/Users"));
const Store = React.lazy(() => import("./modules/Admin/Stores"));
const PostAdmin = React.lazy(() => import("./modules/Admin/Posts"));
const Permissions = React.lazy(() => import("./modules/Admin/Permissions"));
const Denounce = React.lazy(() => import("./modules/Admin/Denounce"));
const ListMyPost = React.lazy(() => import("./modules/Profile/ListMyPost"));
const UserStore = React.lazy(() => import("./modules/Store"));

const StoreView = React.lazy(() => import("./modules/Store/StoreView"));


const Account = React.lazy(() => import("./modules/Profile"));
const LiveComp = React.lazy(() => import("./modules/live"));

const routes = [
	{ layout: "/app", path: "/live", name: "live", component: LiveComp },
	{ layout: "/app", path: "/home", name: "Home", component: Home },
	{ layout: "/app", path: "/", name: "Home", component: Home, exact: true },
	{
		layout: "/app",
		path: "/link-error",
		name: "LinkError",
		component: LinkError,
		exact: true
	},
	{ layout: "/app", path: "/auth/login", name: "login", component: Login, exact: true },
	{ layout: "/app", path: "/auth/sign-up", name: "sign-up", component: Signup, exact: true },

	//Module

	{
		layout: "/app",
		path: "/post-info",
		name: "CreateForm",
		component: CreateForm,
		exact: true
	},
	{
		layout: "/app",
		path: "/product/:type",
		name: "ListProduct",
		component: ListProduct,
		exact: true
	},
	{
		layout: "/app",
		path: "/product/:type/:id",
		name: "ProductDetail",
		component: ProductDetail,
		exact: true
	},
	{ path: "/user/account", name: "Account", component: Account, layout: "/app" },
	{ path: "/user/account/:action/pending", name: "pending", component: ListMyPost, layout: "/app", exact: true },
	{ path: "/user/account/:action/approve", name: "approve", component: ListMyPost, layout: "/app", exact: true },
	{ path: "/user/account/:action/reject", name: "reject", component: ListMyPost, layout: "/app", exact: true },
	{ path: "/user/account/:action/info-save", name: "info-save", component: ListMyPost, layout: "/app", exact: true },
	{ path: "/user/stores/:id", name: "UserStore", component: UserStore, layout: "/app", exact: true },


	{ path: "/products/stores/:id", name: "store", component: StoreView, layout: "/app", exact: true },

	//payment
	{ path: "/payment/user", name: "payment", component: Payment, layout: "/app", exact: true },
	{ path: "/payment/history", name: "HistoryPayment", component: HistoryPayment, layout: "/app", exact: true },


	//admin
	{
		path: "/dashboard",
		name: "admin",
		component: Dashboard,
		exact: true,
		layout: "/admin"
	},
	{ path: "/admin", name: "admin", component: Dashboard, layout: "/admin", exact: true },
	{ path: "/admin/users", name: "user", component: User, layout: "/admin", exact: true },
	{ path: "/admin/stores", name: "stores", component: Store, layout: "/admin", exact: true },
	{ path: "/admin/post/:type", name: "PostAdmin", component: PostAdmin, layout: "/admin", exact: true },
	{ path: "/admin/permissions", name: "permisssions", component: Permissions, layout: "/admin", exact: true },
	{ path: "/admin/support", name: "support", component: Support, layout: "/admin", exact: true },
	{ path: "/admin/denounce", name: "denounce", component: Denounce, layout: "/admin", exact: true },
];

export default routes;
