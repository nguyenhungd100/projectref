import React from "react";
import { BrowserRouter, Route, Switch, Redirect } from "react-router-dom";
import "rsuite/dist/styles/rsuite-default.css";

import userApi from "api/userApi";
import Login from "modules/Auth/Login";
import Layout from "components/layouts";

import { CommonContext, AuthContext } from "context";

const DefaultLayout = React.lazy(() =>
	import("components/layouts/DefaultLayout/DefaultLayout")
);
const AdminLayout = React.lazy(() => import("components/layouts/AdminLayout"));
const loading = () => (
	<div className="animated fadeIn pt-3 text-center">Loading...</div>
);

class App extends React.Component {
	state = { isAuth: false, profile: {} };
	componentDidMount = async () => {
		const isAuth = localStorage.getItem("token");
		if (isAuth) {
			const result = await userApi.profile();
			if (result && result.success) {
				const profile = result.data;
				localStorage.setItem("profile", JSON.stringify(profile))
				this.setState({ profile });
			}
		}
		this.setState({ isAuth });
	};
	onLogin = () => {
		this.setState({ isAuth: true });
	};

	onLogout = () => {
		localStorage.removeItem("token");
		localStorage.removeItem("profile")
		this.setState({ isAuth: false });
		return (window.location.href = "/");
	};
	render() {
		const { isAuth, profile } = this.state;
		const { onLogout, onLogin } = this;
		return (
			<BrowserRouter>
				<React.Suspense fallback={loading()}>
					<AuthContext.Provider value={{ isAuth, onLogin, onLogout, profile }}>
						<Layout {...this.props} />
					</AuthContext.Provider>
				</React.Suspense>
			</BrowserRouter>
		);
	}
}

export default App;
