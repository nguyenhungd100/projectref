import React from "react";

const AuthContext = React.createContext({
  isAuth: false,
  login: null,
  logout: null,
  profile: null,
  hp: null
});

const CommonContext = React.createContext({
  loading: false,
  error:null,
  success:null
});

export { AuthContext, CommonContext };
