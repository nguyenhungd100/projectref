import { log } from "util";
import { constants } from "crypto";

const Countries = [
	{ name: "Afghanistan", code: "AF" },
	{ name: "Åland Islands", code: "AX" },
	{ name: "Albania", code: "AL" },
	{ name: "Algeria", code: "DZ" },
	{ name: "American Samoa", code: "AS" },
	{ name: "AndorrA", code: "AD" },
	{ name: "Angola", code: "AO" },
	{ name: "Anguilla", code: "AI" },
	{ name: "Antarctica", code: "AQ" },
	{ name: "Antigua and Barbuda", code: "AG" },
	{ name: "Argentina", code: "AR" },
	{ name: "Armenia", code: "AM" },
	{ name: "Aruba", code: "AW" },
	{ name: "Australia", code: "AU" },
	{ name: "Austria", code: "AT" },
	{ name: "Azerbaijan", code: "AZ" },
	{ name: "Bahamas", code: "BS" },
	{ name: "Bahrain", code: "BH" },
	{ name: "Bangladesh", code: "BD" },
	{ name: "Barbados", code: "BB" },
	{ name: "Belarus", code: "BY" },
	{ name: "Belgium", code: "BE" },
	{ name: "Belize", code: "BZ" },
	{ name: "Benin", code: "BJ" },
	{ name: "Bermuda", code: "BM" },
	{ name: "Bhutan", code: "BT" },
	{ name: "Bolivia", code: "BO" },
	{ name: "Bosnia and Herzegovina", code: "BA" },
	{ name: "Botswana", code: "BW" },
	{ name: "Bouvet Island", code: "BV" },
	{ name: "Brazil", code: "BR" },
	{ name: "British Indian Ocean Territory", code: "IO" },
	{ name: "Brunei Darussalam", code: "BN" },
	{ name: "Bulgaria", code: "BG" },
	{ name: "Burkina Faso", code: "BF" },
	{ name: "Burundi", code: "BI" },
	{ name: "Cambodia", code: "KH" },
	{ name: "Cameroon", code: "CM" },
	{ name: "Canada", code: "CA" },
	{ name: "Cape Verde", code: "CV" },
	{ name: "Cayman Islands", code: "KY" },
	{ name: "Central African Republic", code: "CF" },
	{ name: "Chad", code: "TD" },
	{ name: "Chile", code: "CL" },
	{ name: "China", code: "CN" },
	{ name: "Christmas Island", code: "CX" },
	{ name: "Cocos (Keeling) Islands", code: "CC" },
	{ name: "Colombia", code: "CO" },
	{ name: "Comoros", code: "KM" },
	{ name: "Congo", code: "CG" },
	{ name: "Congo, The Democratic Republic of the", code: "CD" },
	{ name: "Cook Islands", code: "CK" },
	{ name: "Costa Rica", code: "CR" },
	{ name: "Cote D'Ivoire", code: "CI" },
	{ name: "Croatia", code: "HR" },
	{ name: "Cuba", code: "CU" },
	{ name: "Cyprus", code: "CY" },
	{ name: "Czech Republic", code: "CZ" },
	{ name: "Denmark", code: "DK" },
	{ name: "Djibouti", code: "DJ" },
	{ name: "Dominica", code: "DM" },
	{ name: "Dominican Republic", code: "DO" },
	{ name: "Ecuador", code: "EC" },
	{ name: "Egypt", code: "EG" },
	{ name: "El Salvador", code: "SV" },
	{ name: "Equatorial Guinea", code: "GQ" },
	{ name: "Eritrea", code: "ER" },
	{ name: "Estonia", code: "EE" },
	{ name: "Ethiopia", code: "ET" },
	{ name: "Falkland Islands (Malvinas)", code: "FK" },
	{ name: "Faroe Islands", code: "FO" },
	{ name: "Fiji", code: "FJ" },
	{ name: "Finland", code: "FI" },
	{ name: "France", code: "FR" },
	{ name: "French Guiana", code: "GF" },
	{ name: "French Polynesia", code: "PF" },
	{ name: "French Southern Territories", code: "TF" },
	{ name: "Gabon", code: "GA" },
	{ name: "Gambia", code: "GM" },
	{ name: "Georgia", code: "GE" },
	{ name: "Germany", code: "DE" },
	{ name: "Ghana", code: "GH" },
	{ name: "Gibraltar", code: "GI" },
	{ name: "Greece", code: "GR" },
	{ name: "Greenland", code: "GL" },
	{ name: "Grenada", code: "GD" },
	{ name: "Guadeloupe", code: "GP" },
	{ name: "Guam", code: "GU" },
	{ name: "Guatemala", code: "GT" },
	{ name: "Guernsey", code: "GG" },
	{ name: "Guinea", code: "GN" },
	{ name: "Guinea-Bissau", code: "GW" },
	{ name: "Guyana", code: "GY" },
	{ name: "Haiti", code: "HT" },
	{ name: "Heard Island and Mcdonald Islands", code: "HM" },
	{ name: "Holy See (Vatican City State)", code: "VA" },
	{ name: "Honduras", code: "HN" },
	{ name: "Hong Kong", code: "HK" },
	{ name: "Hungary", code: "HU" },
	{ name: "Iceland", code: "IS" },
	{ name: "India", code: "IN" },
	{ name: "Indonesia", code: "ID" },
	{ name: "Iran, Islamic Republic Of", code: "IR" },
	{ name: "Iraq", code: "IQ" },
	{ name: "Ireland", code: "IE" },
	{ name: "Isle of Man", code: "IM" },
	{ name: "Israel", code: "IL" },
	{ name: "Italy", code: "IT" },
	{ name: "Jamaica", code: "JM" },
	{ name: "Japan", code: "JP" },
	{ name: "Jersey", code: "JE" },
	{ name: "Jordan", code: "JO" },
	{ name: "Kazakhstan", code: "KZ" },
	{ name: "Kenya", code: "KE" },
	{ name: "Kiribati", code: "KI" },
	{ name: "Korea, Democratic People'S Republic of", code: "KP" },
	{ name: "Korea, Republic of", code: "KR" },
	{ name: "Kuwait", code: "KW" },
	{ name: "Kyrgyzstan", code: "KG" },
	{ name: "Lao People'S Democratic Republic", code: "LA" },
	{ name: "Latvia", code: "LV" },
	{ name: "Lebanon", code: "LB" },
	{ name: "Lesotho", code: "LS" },
	{ name: "Liberia", code: "LR" },
	{ name: "Libyan Arab Jamahiriya", code: "LY" },
	{ name: "Liechtenstein", code: "LI" },
	{ name: "Lithuania", code: "LT" },
	{ name: "Luxembourg", code: "LU" },
	{ name: "Macao", code: "MO" },
	{ name: "Macedonia, The Former Yugoslav Republic of", code: "MK" },
	{ name: "Madagascar", code: "MG" },
	{ name: "Malawi", code: "MW" },
	{ name: "Malaysia", code: "MY" },
	{ name: "Maldives", code: "MV" },
	{ name: "Mali", code: "ML" },
	{ name: "Malta", code: "MT" },
	{ name: "Marshall Islands", code: "MH" },
	{ name: "Martinique", code: "MQ" },
	{ name: "Mauritania", code: "MR" },
	{ name: "Mauritius", code: "MU" },
	{ name: "Mayotte", code: "YT" },
	{ name: "Mexico", code: "MX" },
	{ name: "Micronesia, Federated States of", code: "FM" },
	{ name: "Moldova, Republic of", code: "MD" },
	{ name: "Monaco", code: "MC" },
	{ name: "Mongolia", code: "MN" },
	{ name: "Montserrat", code: "MS" },
	{ name: "Morocco", code: "MA" },
	{ name: "Mozambique", code: "MZ" },
	{ name: "Myanmar", code: "MM" },
	{ name: "Namibia", code: "NA" },
	{ name: "Nauru", code: "NR" },
	{ name: "Nepal", code: "NP" },
	{ name: "Netherlands", code: "NL" },
	{ name: "Netherlands Antilles", code: "AN" },
	{ name: "New Caledonia", code: "NC" },
	{ name: "New Zealand", code: "NZ" },
	{ name: "Nicaragua", code: "NI" },
	{ name: "Niger", code: "NE" },
	{ name: "Nigeria", code: "NG" },
	{ name: "Niue", code: "NU" },
	{ name: "Norfolk Island", code: "NF" },
	{ name: "Northern Mariana Islands", code: "MP" },
	{ name: "Norway", code: "NO" },
	{ name: "Oman", code: "OM" },
	{ name: "Pakistan", code: "PK" },
	{ name: "Palau", code: "PW" },
	{ name: "Palestinian Territory, Occupied", code: "PS" },
	{ name: "Panama", code: "PA" },
	{ name: "Papua New Guinea", code: "PG" },
	{ name: "Paraguay", code: "PY" },
	{ name: "Peru", code: "PE" },
	{ name: "Philippines", code: "PH" },
	{ name: "Pitcairn", code: "PN" },
	{ name: "Poland", code: "PL" },
	{ name: "Portugal", code: "PT" },
	{ name: "Puerto Rico", code: "PR" },
	{ name: "Qatar", code: "QA" },
	{ name: "Reunion", code: "RE" },
	{ name: "Romania", code: "RO" },
	{ name: "Russian Federation", code: "RU" },
	{ name: "RWANDA", code: "RW" },
	{ name: "Saint Helena", code: "SH" },
	{ name: "Saint Kitts and Nevis", code: "KN" },
	{ name: "Saint Lucia", code: "LC" },
	{ name: "Saint Pierre and Miquelon", code: "PM" },
	{ name: "Saint Vincent and the Grenadines", code: "VC" },
	{ name: "Samoa", code: "WS" },
	{ name: "San Marino", code: "SM" },
	{ name: "Sao Tome and Principe", code: "ST" },
	{ name: "Saudi Arabia", code: "SA" },
	{ name: "Senegal", code: "SN" },
	{ name: "Serbia and Montenegro", code: "CS" },
	{ name: "Seychelles", code: "SC" },
	{ name: "Sierra Leone", code: "SL" },
	{ name: "Singapore", code: "SG" },
	{ name: "Slovakia", code: "SK" },
	{ name: "Slovenia", code: "SI" },
	{ name: "Solomon Islands", code: "SB" },
	{ name: "Somalia", code: "SO" },
	{ name: "South Africa", code: "ZA" },
	{ name: "South Georgia and the South Sandwich Islands", code: "GS" },
	{ name: "Spain", code: "ES" },
	{ name: "Sri Lanka", code: "LK" },
	{ name: "Sudan", code: "SD" },
	{ name: "Suriname", code: "SR" },
	{ name: "Svalbard and Jan Mayen", code: "SJ" },
	{ name: "Swaziland", code: "SZ" },
	{ name: "Sweden", code: "SE" },
	{ name: "Switzerland", code: "CH" },
	{ name: "Syrian Arab Republic", code: "SY" },
	{ name: "Taiwan, Province of China", code: "TW" },
	{ name: "Tajikistan", code: "TJ" },
	{ name: "Tanzania, United Republic of", code: "TZ" },
	{ name: "Thailand", code: "TH" },
	{ name: "Timor-Leste", code: "TL" },
	{ name: "Togo", code: "TG" },
	{ name: "Tokelau", code: "TK" },
	{ name: "Tonga", code: "TO" },
	{ name: "Trinidad and Tobago", code: "TT" },
	{ name: "Tunisia", code: "TN" },
	{ name: "Turkey", code: "TR" },
	{ name: "Turkmenistan", code: "TM" },
	{ name: "Turks and Caicos Islands", code: "TC" },
	{ name: "Tuvalu", code: "TV" },
	{ name: "Uganda", code: "UG" },
	{ name: "Ukraine", code: "UA" },
	{ name: "United Arab Emirates", code: "AE" },
	{ name: "United Kingdom", code: "GB" },
	{ name: "United States", code: "US" },
	{ name: "United States Minor Outlying Islands", code: "UM" },
	{ name: "Uruguay", code: "UY" },
	{ name: "Uzbekistan", code: "UZ" },
	{ name: "Vanuatu", code: "VU" },
	{ name: "Venezuela", code: "VE" },
	{ name: "Viet Nam", code: "VN" },
	{ name: "Virgin Islands, British", code: "VG" },
	{ name: "Virgin Islands, U.S.", code: "VI" },
	{ name: "Wallis and Futuna", code: "WF" },
	{ name: "Western Sahara", code: "EH" },
	{ name: "Yemen", code: "YE" },
	{ name: "Zambia", code: "ZM" },
	{ name: "Zimbabwe", code: "ZW" }
];
const Categories = [
	{
		value: 1,
		title: "Bất động sản",
		image: "/assets/images/list/bds.png",
		isCol4: true,
		icon: "paypal",
		type: 'bat-dong-san'
	},
	{
		value: 2,
		title: "Xe cộ",
		image: "/assets/images/list/xe.png",
		isCol4: true,
		icon: "cab",
		type: 'xe-co'
	},
	{
		value: 3,
		title: "Đồ điện tử",
		image: "/assets/images/list/dientu4.png",
		isCol4: true,
		icon: "steam",
		type: 'do-dien-tu'
	},
	{
		value: 4,
		title: "Việc làm",
		image: "/assets/images/list/vieclam.png",
		icon: "explore",
		type: 'viec-lam'
	},
	{
		value: 5,
		title: "Giải trí, Thể thao",
		image: "/assets/images/list/giaitri.png",
		icon: "creative",
		type: 'giai-tri-the-thao'
	},
	{
		value: 6,
		title: "Dịch vụ, Du lịch",
		image: "/assets/images/list/dulich2.png",
		icon: "plane",
		type: 'dich-vu-va-du-lich'
	},
	{
		value: 7,
		title: "Nội thất",
		image: "/assets/images/list/noithat2.png",
		icon: "bed",
		type: 'noi-that'
	},
	{
		value: 8,
		title: "Đồ văn phòng",
		image: "/assets/images/list/vanphong0.png",
		icon: "camera-retro",
		type: 'do-van-phong'
	},
	{
		value: 9,
		title: "Thời trang",
		image: "/assets/images/list/thoitrang00.png",
		icon: "heartbeat",
		type: 'thoi-trang'
	},
	{
		value: 10,
		title: "Mẹ và bé",
		image: "/assets/images/list/be0.jpg",
		icon: "child",
		type: 'me-va-be'
	},
	{
		value: 11,
		title: "Các dịch vụ khác",
		image: "/assets/images/list/giaitri2.png",
		icon: "snowflake-o",
		type: 'cac-dich-vu-khac'
	}
];
const Cities = [
	{
		value: 1,
		name: "Thành phố Hà Nội",
		level: "Thành phố Trung ương",
		sortOrder: 1
	},
	{
		value: 2,
		name: "Tỉnh Hà Giang",
		level: "Tỉnh",
		sortOrder: 2
	},
	{
		value: 3,
		name: "Tỉnh Cao Bằng",
		level: "Tỉnh",
		sortOrder: 3
	},
	{
		value: 4,
		name: "Tỉnh Bắc Kạn",
		level: "Tỉnh",
		sortOrder: 4
	},
	{
		value: 5,
		name: "Tỉnh Tuyên Quang",
		level: "Tỉnh",
		sortOrder: 5
	},
	{
		value: 6,
		name: "Tỉnh Lào Cai",
		level: "Tỉnh",
		sortOrder: 6
	},
	{
		value: 7,
		name: "Tỉnh Điện Biên",
		level: "Tỉnh",
		sortOrder: 7
	},
	{
		value: 8,
		name: "Tỉnh Lai Châu",
		level: "Tỉnh",
		sortOrder: 8
	},
	{
		value: 9,
		name: "Tỉnh Sơn La",
		level: "Tỉnh",
		sortOrder: 9
	},
	{
		value: 10,
		name: "Tỉnh Yên Bái",
		level: "Tỉnh",
		sortOrder: 10
	},
	{
		value: 11,
		name: "Tỉnh Hoà Bình",
		level: "Tỉnh",
		sortOrder: 11
	},
	{
		value: 12,
		name: "Tỉnh Thái Nguyên",
		level: "Tỉnh",
		sortOrder: 12
	},
	{
		value: 13,
		name: "Tỉnh Lạng Sơn",
		level: "Tỉnh",
		sortOrder: 13
	},
	{
		value: 14,
		name: "Tỉnh Quảng Ninh",
		level: "Tỉnh",
		sortOrder: 14
	},
	{
		value: 15,
		name: "Tỉnh Bắc Giang",
		level: "Tỉnh",
		sortOrder: 15
	},
	{
		value: 16,
		name: "Tỉnh Phú Thọ",
		level: "Tỉnh",
		sortOrder: 16
	},
	{
		value: 17,
		name: "Tỉnh Vĩnh Phúc",
		level: "Tỉnh",
		sortOrder: 17
	},
	{
		value: 18,
		name: "Tỉnh Bắc Ninh",
		level: "Tỉnh",
		sortOrder: 18
	},
	{
		value: 19,
		name: "Tỉnh Hải Dương",
		level: "Tỉnh",
		sortOrder: 19
	},
	{
		value: 20,
		name: "Thành phố Hải Phòng",
		level: "Thành phố Trung ương",
		sortOrder: 20
	},
	{
		value: 21,
		name: "Tỉnh Hưng Yên",
		level: "Tỉnh",
		sortOrder: 21
	},
	{
		value: 22,
		name: "Tỉnh Thái Bình",
		level: "Tỉnh",
		sortOrder: 22
	},
	{
		value: 23,
		name: "Tỉnh Hà Nam",
		level: "Tỉnh",
		sortOrder: 23
	},
	{
		value: 24,
		name: "Tỉnh Nam Định",
		level: "Tỉnh",
		sortOrder: 24
	},
	{
		value: 25,
		name: "Tỉnh Ninh Bình",
		level: "Tỉnh",
		sortOrder: 25
	},
	{
		value: 26,
		name: "Tỉnh Thanh Hóa",
		level: "Tỉnh",
		sortOrder: 26
	},
	{
		value: 27,
		name: "Tỉnh Nghệ An",
		level: "Tỉnh",
		sortOrder: 27
	},
	{
		value: 28,
		name: "Tỉnh Hà Tĩnh",
		level: "Tỉnh",
		sortOrder: 28
	},
	{
		value: 29,
		name: "Tỉnh Quảng Bình",
		level: "Tỉnh",
		sortOrder: 29
	},
	{
		value: 30,
		name: "Tỉnh Quảng Trị",
		level: "Tỉnh",
		sortOrder: 30
	},
	{
		value: 31,
		name: "Tỉnh Thừa Thiên Huế",
		level: "Tỉnh",
		sortOrder: 31
	},
	{
		value: 32,
		name: "Thành phố Đà Nẵng",
		level: "Thành phố Trung ương",
		sortOrder: 32
	},
	{
		value: 33,
		name: "Tỉnh Quảng Nam",
		level: "Tỉnh",
		sortOrder: 33
	},
	{
		value: 34,
		name: "Tỉnh Quảng Ngãi",
		level: "Tỉnh",
		sortOrder: 34
	},
	{
		value: 35,
		name: "Tỉnh Bình Định",
		level: "Tỉnh",
		sortOrder: 35
	},
	{
		value: 36,
		name: "Tỉnh Phú Yên",
		level: "Tỉnh",
		sortOrder: 36
	},
	{
		value: 37,
		name: "Tỉnh Khánh Hòa",
		level: "Tỉnh",
		sortOrder: 37
	},
	{
		value: 38,
		name: "Tỉnh Ninh Thuận",
		level: "Tỉnh",
		sortOrder: 38
	},
	{
		value: 39,
		name: "Tỉnh Bình Thuận",
		level: "Tỉnh",
		sortOrder: 39
	},
	{
		value: 40,
		name: "Tỉnh Kon Tum",
		level: "Tỉnh",
		sortOrder: 40
	},
	{
		value: 41,
		name: "Tỉnh Gia Lai",
		level: "Tỉnh",
		sortOrder: 41
	},
	{
		value: 42,
		name: "Tỉnh Đắk Lắk",
		level: "Tỉnh",
		sortOrder: 42
	},
	{
		value: 43,
		name: "Tỉnh Đắk Nông",
		level: "Tỉnh",
		sortOrder: 43
	},
	{
		value: 44,
		name: "Tỉnh Lâm Đồng",
		level: "Tỉnh",
		sortOrder: 44
	},
	{
		value: 45,
		name: "Tỉnh Bình Phước",
		level: "Tỉnh",
		sortOrder: 45
	},
	{
		value: 46,
		name: "Tỉnh Tây Ninh",
		level: "Tỉnh",
		sortOrder: 46
	},
	{
		value: 47,
		name: "Tỉnh Bình Dương",
		level: "Tỉnh",
		sortOrder: 47
	},
	{
		value: 48,
		name: "Tỉnh Đồng Nai",
		level: "Tỉnh",
		sortOrder: 48
	},
	{
		value: 49,
		name: "Tỉnh Bà Rịa - Vũng Tàu",
		level: "Tỉnh",
		sortOrder: 49
	},
	{
		value: 50,
		name: "Thành phố Hồ Chí Minh",
		level: "Thành phố Trung ương",
		sortOrder: 50
	},
	{
		value: 51,
		name: "Tỉnh Long An",
		level: "Tỉnh",
		sortOrder: 51
	},
	{
		value: 52,
		name: "Tỉnh Tiền Giang",
		level: "Tỉnh",
		sortOrder: 52
	},
	{
		value: 53,
		name: "Tỉnh Bến Tre",
		level: "Tỉnh",
		sortOrder: 53
	},
	{
		value: 54,
		name: "Tỉnh Trà Vinh",
		level: "Tỉnh",
		sortOrder: 54
	},
	{
		value: 55,
		name: "Tỉnh Vĩnh Long",
		level: "Tỉnh",
		sortOrder: 55
	},
	{
		value: 56,
		name: "Tỉnh Đồng Tháp",
		level: "Tỉnh",
		sortOrder: 56
	},
	{
		value: 57,
		name: "Tỉnh An Giang",
		level: "Tỉnh",
		sortOrder: 57
	},
	{
		value: 58,
		name: "Tỉnh Kiên Giang",
		level: "Tỉnh",
		sortOrder: 58
	},
	{
		value: 59,
		name: "Thành phố Cần Thơ",
		level: "Thành phố Trung ương",
		sortOrder: 59
	},
	{
		value: 60,
		name: "Tỉnh Hậu Giang",
		level: "Tỉnh",
		sortOrder: 60
	},
	{
		value: 61,
		name: "Tỉnh Sóc Trăng",
		level: "Tỉnh",
		sortOrder: 61
	},
	{
		value: 62,
		name: "Tỉnh Bạc Liêu",
		level: "Tỉnh",
		sortOrder: 62
	},
	{
		value: 63,
		name: "Tỉnh Cà Mau",
		level: "Tỉnh",
		sortOrder: 63
	}
];
const Color = {
	global: "#f5a742",
	gGlobal: "linear-gradient(to right, #d06b10, #f5a742)",
}
const Status = [
	{
		name: "đợi duyệt",
		id: 1,
		lable: "draft"
	},
	{
		name: "đã duyệt",
		id: 5,
		lable: "approved"
	},
	{
		name: "bị từ chối",
		id: 10,
		lable: "reject"
	},
]
const onGetTye = (name, id) => {
	let temp = name ? name : Number(id);
	let item = Status.find(i => i[name ? 'lable' : 'id'] === temp);
	return item ? item : { name: "" }
}
const Banks = [

	{ value: "VCB", lable: "Ngân hàng TMCP Ngoại Thương Việt Nam(Vietcombank)" },
	{ value: "DAB", lable: "Ngân hàng TMCP Đông Á(DongA Bank)" },
	{ value: "TCB", lable: "Ngân hàng TMCP Kỹ Thương(Techcombank)" },
	{ value: "MB", lable: "Ngân hàng TMCP Quân Đội(MB)" },
	{ value: "VIB", lable: "Ngân hàng TMCP Quốc tế(VIB)" },
	{ value: "VTB", lable: "Ngân hàng TMCP Công Thương(VietinBank)" },
	{ value: "EXB", lable: "Ngân hàng TMCP Xuất Nhập Khẩu(Eximbank)" },
	{ value: "ACB", lable: "Ngân hàng TMCP Á Châu(ACB)" },
	{ value: "HDB", lable: "Ngân hàng TMCP Phát Triển Nhà TP.Hồ Chí Minh(HDBank)" },
	{ value: "MSB", lable: "Ngân hàng TMCP Hàng Hải(MariTimeBank)" },
	{ value: "NVB", lable: "Ngân hàng TMCP Nam Việt(NaviBank)" },
	{ value: "VAB", lable: "Ngân hàng TMCP Việt Á(VietA Bank)" },
	{ value: "VPB", lable: "Ngân hàng TMCP Việt Nam Thịnh Vượng(VPBank)" },
	{ value: "SCB", lable: "Ngân hàng TMCP Sài Gòn Thương Tính(Sacombank)" },
	{ value: "GPB", lable: "Ngân hàng TMCP Dầu Khí(GPBank)" },
	{ value: "AGB", lable: "Ngân hàng Nông nghiệp và Phát triển Nông thôn(Agribank)" },
	{ value: "BIDV", lable: "Ngân hàng Đầu tư và Phát triển Việt Nam(BIDV)" },
	{ value: "OJB", lable: "Ngân hàng TMCP Đại Dương(OceanBank)" },
	// { value: "PGB", lable: "Ngân Hàng TMCP Xăng Dầu Petrolimex(PGBank)" },
	// { value: "SHB", lable: "Ngân hàng TMCP Sài Gòn - Hà Nội(SHB)" },
	// { value: "SEB", lable: "Ngân hàng TMCP Đông Nam Á(SeaBank)" },
	// { value: "TPB", lable: "Ngân hàng TMCP Tiên Phong(TienPhong Bank)" }

]
const hasId = 16;
export { Countries, Categories, Cities, Color, Status, onGetTye, Banks, hasId };
