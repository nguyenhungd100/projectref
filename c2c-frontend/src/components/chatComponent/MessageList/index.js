import React, { useEffect, useState, useContext, useRef } from 'react';
import Compose from '../Compose';
import Toolbar from '../Toolbar';
import ToolbarButton from '../ToolbarButton';
import Message from '../Message';
import moment from 'moment';
import { CommonContext } from "context";
import { mydatabase } from 'components/configFirebase/firebase';
import './MessageList.css';
import {
  Icon,
  Button,
  FormGroup,
  ControlLabel,
  ButtonToolbar,
  IconButton,
  Input,
} from "rsuite";
import { Color } from "constants/common"
import fileApi from "api/fileApi"
export default function MessageList(props) {
  const [messages, setMessages] = useState([]);
  const [content, setContent] = useState('');
  const contextApi = useContext(CommonContext);

  const refContainer = useRef({});
  const refScroll = useRef({})
  const { profile } = contextApi;

  useEffect(() => {
    setTimeout(() => {
      const { topicKey, messageKey } = props.listKey;
      getData(messageKey)

    }, 200);

  }, [])

  function getData(messageKey) {
    if (!messageKey || !profile) return;
    const messageRef = mydatabase.ref(`Converation/messages/${messageKey}`);
    messageRef
      .limitToLast(300)
      .on('value', messagesObj => {
        if (messagesObj && messagesObj.val()) {
          const messages = Object.values(messagesObj.val());
          setMessages(messages)
        }
      });

  }
  const senMessager = (model) => {
    const { messageKey } = props.listKey;
    const message = {
      sender: {
        name: profile.fullName,
        imageId: profile.imageId,
        id: profile.id
      },
      senderId: profile.id,
      createdDate: new Date(),
      ...model
    };
    const messageRef = mydatabase.ref(`Converation/messages/${messageKey}`);
    messageRef.push(message);

  }
  async function saveImage(e) {
    const { data } = props;
    const file = e.target.files[0];
    if (contextApi && contextApi.profile) {
      let res = await fileApi.upImage(file);
      if (res && res.success) {
        senMessager({ imageId: res.data })
      }
      else {
        contextApi.error("Có lỗi, vui lòng thử lại")
      }
    }

  }
  const onButtonClick = () => {
    if (refContainer) refContainer.current.click();
  };
  const onPressEnter = e => {
    if (e.key === "Enter") {
      senMessager({ content })
      setContent('');
      setTimeout(() => {
        if (props.onSetCroll) props.onSetCroll();
      }, 300);
    }
  };
  return (
    <div className="message-list">
      <Toolbar
        title="Nội dung"
        rightItems={[
          <ToolbarButton key="info" icon="ion-ios-information-circle-outline" />,
          <ToolbarButton key="video" icon="ion-ios-videocam" />,
          <ToolbarButton key="phone" icon="ion-ios-call" />
        ]}
      />

      <div className="message-list-container" >{
        messages.map((i, index) => {
          i.isSender = i.senderId === profile.id;
          return (
            <Message
              key={index}
              index={index}
              data={i}
            />
          )
        })
      }
      </div>
      <input ref={refContainer} type={'file'} onChange={saveImage} hidden />
      <div style={{ position: "fixed", bottom: 0 }}>
        <FormGroup className="d-flex align-items-center mb-3">
          <Input
            componentClass="textarea"
            value={content}
            rows={3}
            placeholder="Nhập nội dung và enter"
            onChange={value => {
              setContent(value)
            }}
            style={{ minWidth: 510 }}
            className="col-12 mr-3"
            onKeyPress={onPressEnter}
          />
          <ButtonToolbar>
            <IconButton icon={<Icon icon="file-image-o " />} color="blue" circle onClick={onButtonClick} />
          </ButtonToolbar>
        </FormGroup>
      </div>

    </div>
  );
}
