import React, { useEffect, useContext } from 'react';
import shave from 'shave';
import fileApi from "api/fileApi";
import { CommonContext } from "context";
import './ConversationListItem.css';

export default function ConversationListItem(props) {
  const contextApi = useContext(CommonContext)
  useEffect(() => {
    shave('.conversation-snippet', 20);
  })
  const { profile } = contextApi;
  const { userId, createdImageId, userImageId, userName, createnName, topicKey, messageKey } = props.data;

  return (
    <div className={`conversation-list-item ${props.active ? ' active-conversation' : ''}`} onClick={() => props.onClick(topicKey, messageKey, props.data)}>
      <img className="conversation-photo" src={(profile.id === userId) ? fileApi.getImage(createdImageId, 50, 50) : fileApi.getImage(userImageId, 50, 50)} />
      <div className="conversation-info">
        <h1 className="conversation-title">{(profile.id === userId) ? createnName : userName}</h1>
        <p className="conversation-snippet">... </p>
      </div>
    </div>
  );
}