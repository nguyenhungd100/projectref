import React, { useState, useEffect, useContext } from "react";
import ConversationSearch from "../ConversationSearch";
import ConversationListItem from "../ConversationListItem";
import Toolbar from "../Toolbar";
import ToolbarButton from "../ToolbarButton";
import "./ConversationList.css";
import { CommonContext } from "context";
import { mydatabase } from 'components/configFirebase/firebase';


export default function ConversationList(props) {
	const contextApi = useContext(CommonContext);
	const [conversations, setConversations] = useState([]);
	useEffect(() => {
		getConversations();
	}, []);

	const getConversations = () => {
		const { profile } = contextApi;
		if (!profile) return;
		const messageRef = mydatabase.ref(`Converation/topics`);
		messageRef
			.limitToLast(300)
			.orderByChild(profile.id.toString())
			.equalTo(profile.id)
			.on("value", topicsObj => {
				if (!topicsObj.val()) {
					return;
				}
				const topics = Object.values(topicsObj.val());
				topics.sort((a, b) => b.lastModify - a.lastModify);
				const selecedTopicKey = topics[0].topicKey;
				setConversations(topics);
			});
	};

	return (
		<div className="conversation-list">
			<Toolbar
				title="Danh sách"
				leftItems={[<ToolbarButton key="cog" icon="ion-ios-cog" />]}
				rightItems={[<ToolbarButton key="add" icon="ion-ios-add-circle-outline" />]}
			/>

			{conversations.map((conversation, index) => (
				<ConversationListItem key={index} data={conversation} onClick={props.onClick} active={props.active === conversation.messageKey} />
			))}
		</div>
	);
}
