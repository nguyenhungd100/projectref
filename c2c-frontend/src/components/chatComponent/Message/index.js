import React from 'react';
import moment from 'moment';
import './Message.css';
import fileApi from "api/fileApi"
export default function Message(props) {
  const {
    data,
    index
  } = props;

  const friendlyTimestamp = moment(data.createdSince).format('DD-MM-YYY HH:mm');
  return (
    <div className={[

      `${index === 0 ? "" : "message"}`,
      `${data.isSender ? ' mine' : ''}`,
      `${data.isSender ? ' start' : ' end'}`,
      `${index === 1 ? ' width-100s' : ''}`

    ].join(' ')}>
      {

        <div className="timestamps">
          {friendlyTimestamp}
        </div>
      }
      <div className="bubble-container">
        {!data.imageId ?
          <div className={index === 0 ? 'first-messager' : "bubble"} title={friendlyTimestamp}>
            {data.content}
          </div>
          : <img src={fileApi.getImage(data.imageId, 150, 150)} alt="chodientuc2c" className="image-message " />
        }

      </div>
    </div>
  );
}
