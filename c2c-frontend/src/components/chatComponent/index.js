import React from 'react';
import Messenger from './Messenger';

export default function() {
    return (
      <div className="App">
        <Messenger />
      </div>
    );
}
