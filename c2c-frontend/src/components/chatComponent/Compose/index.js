import React from "react";
import "./Compose.css";
import ToolbarButton from '../ToolbarButton';
export default function Compose(props) {
	return (
		<div className="compose">
			<input
				type="text"
				className="compose-input"
				placeholder="Nhập nội dung"
			/>
			<ToolbarButton key="photo" icon="ion-ios-camera" />,
        <ToolbarButton key="image" icon="ion-ios-image" />,
        <ToolbarButton key="audio" icon="ion-ios-mic" />,
        <ToolbarButton key="money" icon="ion-ios-card" />,
        <ToolbarButton key="games" icon="ion-logo-game-controller-b" />,
        <ToolbarButton key="emoji" icon="ion-ios-happy" />
		</div>
	);
}
