import React, { PureComponent } from 'react'

import ConversationList from '../ConversationList';
import MessageList from '../MessageList';
import './Messenger.css';



class Messenger extends PureComponent {
  constructor(props) {
    super(props)

    this.state = {
      data: {},
      loading: false
    }
  }
  onClick = (topicKey, messageKey, data) => {
    this.setState({ loading: true })
    this.setState({ topicKey, messageKey, data })
    this.setTime = setTimeout(() => {
      this.setState({ loading: false })
    }, 200);
  }
  onSetCroll = () => {
    if (this.refScroll) {
      this.refScroll.scrollTop = this.refScroll.scrollHeight;
    }
  }
  componentWillUnmount = () => {
    clearTimeout(this.setTime)
  }
  handleScrolls = (e) => {
    if (this.refScroll) {
      this.refScroll.scrollTop = e.target.scrollTop;

    }

  }
  render() {
    const { topicKey, messageKey, data, loading } = this.state;
    return (
      <div className="messenger d-flex">
        <div className="scrollable sidebar" style={{
          width: "30%", borderRightColor: '#ccc',
          borderRightStyle: "dashed", borderRightWidth: 1
        }}>
          <ConversationList onClick={this.onClick} active={messageKey} />
        </div>
        <div className="scrollable content" style={{ width: "70%", marginBottom: 100 }} ref={refs => this.refScroll = refs} onScroll={this.handleScrolls}>
          {
            topicKey && messageKey && !loading &&
            <MessageList listKey={{ topicKey, messageKey }} data={data} onSetCroll={this.onSetCroll} />
          }
        </div>
      </div>
    );
  }
}

export default Messenger

