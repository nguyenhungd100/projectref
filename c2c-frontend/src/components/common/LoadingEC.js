import React, { Component } from 'react'
import Loading from 'react-loading-bar'
import 'react-loading-bar/dist/index.css'

export default class LoadingEC extends Component {
    state = {
        show: false
    }

    onShow = () => {
        this.setState({ show: true })
    }

    onHide = () => {
        this.setState({ show: false })
    }

    render() {
        return (
            <div>
                <Loading
                    show={this.state.show}
                    color="red"
                />
            </div>
        )
    }
}