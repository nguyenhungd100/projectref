import React from 'react'
import Moment from "react-moment";

export class DateMoment extends React.Component {
	render() {
		let props = this.props;
		let format = props.format;

		if (props.format === undefined || props.format === null) {
			format = "DD/MM/YYYY";
		}
		if (props.value === undefined || props.value === null)
			return (
				null
			)
		else {
			return (
				<Moment format={format}>{props.value}</Moment>
			)
		}
	}
}