import React, {Component} from "react";
import "./style.css";
const modelCss = [
	{index: 0, css: "right"},
	{index: 1, css: "front"},
	{index: 2, css: "back"},
	{index: 3, css: "top"},
	{index: 4, css: "bottom"},
	{index: 5, css: "left"}
];
export default class extends Component {
	state = {list: []};
	componentDidMount() {
		let list = [];
		let {data = []} = this.props;
		let temp = data.length;
		if (temp > 6) {
			list = data.slice(0, 6).map((i, index) => {
				return {image: i.original, class: modelCss[index].css};
			});
		}
		if (temp !== 0 && temp < 6) {
			for (let i = 0; i < modelCss.length; i++) {
				if (data[i]) {
					list[i] = {
						image: data[i].original,
						class: modelCss[i].css
					};
				} else {
					list[i] = {
						image: data[0].original,
						class: modelCss[i].css
					};
				}
			}
		}
		this.setState({list});
	}
	render() {
		return (
			<div align="center" style={{direction: "ltr"}}>
				<div className="wrap">
					<div className="cube">
						{this.state.list.map((i, index) => (
							<div
								className={i.class}
								style={{
									background: `url(${i.image})`,
									backgroundSize: "100% 100%",
									backgroundRepeat: "no-repeat"
								}}
							/>
						))}

						<div className="back" />
						<div className="top" />
						<div className="bottom" />
						<div className="left" />
						<div className="right" />
					</div>
				</div>
			</div>
		);
	}
}
