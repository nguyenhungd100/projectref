import React, {useState, useEffect} from "react";

function TextRolling(props) {
	console.log(props);
	const data = props.data || [];
	const component = props.type || "span";
	useEffect(() => {
		var words = "";
		var wordArray = [];
		var currentWord = 0;
		setTimeout(() => {
			words = document.getElementsByClassName("word");
			for (var i = 0; i < words.length; i++) {
				splitLetters(words[i]);
			}
			changeWord();
			setInterval(changeWord, 5000);
		}, 1000);

		function changeWord() {
			var cw = wordArray[currentWord];
			var nw =
				currentWord == words.length - 1 ? wordArray[0] : wordArray[currentWord + 1];
			for (var i = 0; i < cw.length; i++) {
				animateLetterOut(cw, i);
			}

			for (var i = 0; i < nw.length; i++) {
				nw[i].className = "letter behind";
				nw[0].parentElement.style.opacity = 1;
				animateLetterIn(nw, i);
			}

			currentWord = currentWord == wordArray.length - 1 ? 0 : currentWord + 1;
		}

		function animateLetterOut(cw, i) {
			setTimeout(function() {
				cw[i].className = "letter out";
			}, i * 80);
		}

		function animateLetterIn(nw, i) {
			setTimeout(function() {
				nw[i].className = "letter in";
			}, 340 + i * 80);
		}

		function splitLetters(word) {
			var content = word.innerHTML;
			word.innerHTML = "";
			var letters = [];
			for (var i = 0; i < content.length; i++) {
				var letter = document.createElement(component);
				letter.className = "letter";
				letter.innerHTML = content.charAt(i);
				if(content.charAt(i) =="" || content.charAt(i) == " "){
					letter.innerHTML	='&nbsp;'
				}

				word.appendChild(letter);
				letters.push(letter);
			}

			wordArray.push(letters);
		}
	});

	return <component className ={`word ${props.className}`}>{props.data}</component>;
}

export default TextRolling;
