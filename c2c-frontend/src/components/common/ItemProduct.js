import React, { memo, useEffect, useState } from 'react'
import { Categories } from "constants/common";
import { Link } from "react-router-dom";
import { Nav, Dropdown, Row, Col, Icon, Pagination, Divider, Tooltip, Whisper } from "rsuite";
import postApi from "api/postApi";
import numeral from "numeral";
import { Placeholder } from "rsuite";
import moment from "moment"
const { Paragraph } = Placeholder

function ItemProduct(props) {
    const { data, like, contact } = props
    const [loading, setLoading] = useState(true)
    useEffect(() => {
        setTimeout(() => {
            setLoading(false)
        }, 600);
    }, [])
    return (
        <>{loading ?
            <Paragraph style={{ height: 300 }} rows={5} rows={5} graph active />
            :
            <li className="products" >
                {contact &&
                    <div className="offer">Liên hệ</div>
                }
                <div className="thumbnail" onClick={props.onClick}>
                    <img
                        style={{ borderRadius: 10 }}
                        src={data.image}
                        alt="Product Name"
                    />
                </div>
                <div className="product-list-description">
                    <div className="productname" onClick={props.onClick}>{data.name}</div>
                    {false &&
                        <p>
                            <img src="/assets/images/star.png" alt="" />
                            <Link to="/" className="review_num">
                                Lượt xem
				        	</Link>
                        </p>
                    }

                    <p onClick={props.onClick}>{data.content}</p>
                    <div className="list_bottom d-flex justify-content-between">
                        <div className="price">
                            <span className="new_price">
                                {numeral(data.price ? data.price : 0).format('0,00')}
                                <sup>đ</sup>
                            </span>
                        </div>
                        {
                            like && (<div className="button_group ">
                                <Whisper placement="top" trigger="hover" speaker={<Tooltip>
                                    Bỏ lưu tin
                                </Tooltip>}>
                                    <button className="button favorite" onClick={props.infoSave} style={{ background: "#fe5252" }} title="">
                                        <i className="fa fa-heart-o" />
                                    </button>
                                </Whisper>

                            </div>)
                        }

                    </div>
                    <div style={{ float: 'right' }}>
                        <a>{moment(data.createdDate).format("DD-MM-YYYY")}</a>
                        <Divider vertical />
                        <a>{data.provinceName}</a>
                        <Divider vertical />

                    </div>
                </div>
            </li>
        }
        </>
    )
}


export default memo(ItemProduct)
