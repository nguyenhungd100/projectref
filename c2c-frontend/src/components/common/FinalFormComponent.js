import React from "react";
import {
	TreePicker,
	DatePicker,
	Input,
	Checkbox,
	InputNumber,
	CheckPicker,
	Uploader,
	Button,
	Radio,
	RadioGroup,
	Icon
} from "rsuite";
import CurrencyFormat from 'react-currency-format';

import CKEditor from "ckeditor4-react";

const CurrencyField = React.memo(props => {
	let { input, meta, data, handleOnChange, disabled, ...rest } = props;
	return (
		<React.Fragment>

			<CurrencyFormat
				{...input}
				{...rest}
				value={input.value}
				disabled={disabled}
				onValueChange={(values) => {
					const { formattedValue, value } = values;
					input.onChange(formattedValue);
					if (handleOnChange) handleOnChange(value);
				}}
			/>
			{meta.error && meta.touched && <span className="error">{meta.error}</span>}
		</React.Fragment>
	);
});
const DateField = React.memo(props => {
	let { input, meta, formatDate, handleOnChange, ...rest } = props;
	let val = input.value ? new Date(input.value) : null;
	return (
		<React.Fragment>
			<DatePicker
				{...rest}
				value={val}
				format={formatDate}
				onChange={value => {
					input.onChange(value);
					if (handleOnChange) handleOnChange(value);
				}}
			/>
			{meta.error && meta.touched && <span className="error">{meta.error}</span>}
		</React.Fragment>
	);
});

const SelectField = React.memo(props => {
	let { input, meta, data, handleOnChange, disabled, ...rest } = props;
	return (
		<React.Fragment>
			<TreePicker
				{...input}
				{...rest}
				data={data || []}
				disabled={disabled}
				onChange={value => {
					input.onChange(value);
					if (handleOnChange) handleOnChange(value);
				}}
			/>
			{meta.error && meta.touched && <span className="error">{meta.error}</span>}
		</React.Fragment>
	);
});

const InputField = React.memo(props => {
	let { input, meta, data, handleOnChange, disabled, ...rest } = props;
	return (
		<React.Fragment>
			<Input
				{...input}
				{...rest}
				value={input.value}
				disabled={disabled}
				onChange={value => {
					input.onChange(value);
					if (handleOnChange) handleOnChange(value);
				}}
			/>
			{meta.error && meta.touched && <span className="error">{meta.error}</span>}
		</React.Fragment>
	);
});

const CheckPickerField = React.memo(props => {
	let { input, meta, data, handleOnChange, ...rest } = props;
	return (
		<React.Fragment>
			<CheckPicker
				{...input}
				{...rest}
				value={input.value}
				onChange={value => {
					debugger;
					input.onChange(value);
					if (handleOnChange) handleOnChange(value);
				}}
			/>

			{meta.error && meta.touched && <span className="error">{meta.error}</span>}
		</React.Fragment>
	);
});

const TextAreaField = React.memo(
	({ input, meta, rows, handleOnChange, ...rest }) => (
		<React.Fragment>
			<textarea
				{...input}
				{...rest}
				rows={rows || 4}
				onChange={e => {
					input.onChange(e.target.value);
					if (handleOnChange) handleOnChange(e.target.value);
				}}
			/>
			{meta.error && meta.touched && <span className="error">{meta.error}</span>}
		</React.Fragment>
	)
);

// Textarea của rsuite

const RSTextAreaField = React.memo(props => {
	let {
		input,
		meta,
		data,
		rows,
		className,
		handleOnChange,
		disabled,
		...rest
	} = props;
	return (
		<div className={className}>
			<Input
				componentClass="textarea"
				{...input}
				{...rest}
				disabled={disabled}
				value={input.value}
				rows={rows || 4}
				onChange={value => {
					input.onChange(value);
					if (handleOnChange) handleOnChange(value);
				}}
			/>

			{meta.error && meta.touched && <span className="error">{meta.error}</span>}
		</div>
	);
});

const TreePickerField = React.memo(
	({ input, meta, data, handleOnChange, ...rest }) => {
		return (
			<React.Fragment>
				<TreePicker
					{...input}
					{...rest}
					data={data || []}
					onChange={value => {
						input.onChange(value);
						if (handleOnChange) handleOnChange(value);
					}}
				/>

				{meta.error && meta.touched && <span className="error">{meta.error}</span>}
			</React.Fragment>
		);
	}
);

const CheckboxField = React.memo(
	({ input, meta, label, handleOnChange, ...rest }) => {
		return (
			<React.Fragment>
				<Checkbox
					{...input}
					{...rest}
					checked={input.value}
					onChange={(_value, checked) => {
						input.onChange(checked);
						if (handleOnChange) handleOnChange(checked);
					}}
				>
					{label}
				</Checkbox>
				{meta.error && meta.touched && <span className="error">{meta.error}</span>}
			</React.Fragment>
		);
	}
);

const InputNumberField = ({
	input,
	meta,
	data,
	label,
	handleOnChange,
	...rest
}) => {
	return (
		<React.Fragment>
			<InputNumber
				{...input}
				{...rest}
				value={input.value}
				onChange={value => {
					input.onChange(value);
					if (handleOnChange) handleOnChange(value);
				}}
			>
				{label}
			</InputNumber>

			{meta.error && meta.touched && <span className="error">{meta.error}</span>}
		</React.Fragment>
	);
};

const UploaderField = React.memo(props => {
	let {
		input,
		meta,
		listType,
		action,
		handleOnChange,
		multiple,
		icon,
		styleIcon,
		size,
		...rest
	} = props;
	return (
		<React.Fragment>
			<Uploader
				{...rest}
				fileList={input.value || []}
				listType={listType}
				multiple={multiple}
				action={action}
				onChange={value => {
					input.onChange(value);
					if (handleOnChange) handleOnChange(value);
				}}
			>
				<Button>
					<Icon icon={icon} size={size} style={styleIcon} />
				</Button>
			</Uploader>
			{meta.error && meta.touched && <span className="error">{meta.error}</span>}
		</React.Fragment>
	);
});
const RadioGroupField = React.memo(
	({ input, meta, data, handleOnChange, ...rest }) => {
		return (
			<React.Fragment>
				<RadioGroup
					{...input}
					{...rest}
					onChange={value => {
						input.onChange(value);
						if (handleOnChange) handleOnChange(value);
					}}
				>
					{data.map((radio, index) => (
						<Radio key={index} value={radio.value} disabled={radio.disabled}>
							{radio.label}
						</Radio>
					))}
				</RadioGroup>

				{meta.error && meta.touched && <span className="error">{meta.error}</span>}
			</React.Fragment>
		);
	}
);
const CKEditorField = React.memo(
	({ input, meta, rows, handleOnChange, onChange, name, ...rest }) => (
		<React.Fragment>
			<CKEditor
				{...input}
				name="hahah"
				config={{
					toolbarGroups: [
						{ name: "clipboard", groups: ["clipboard", "undo"] },
						{ name: "editing", groups: ["find", "selection", "spellchecker"] },
						{ name: "links" },
						{ name: "insert" },
						{ name: "forms" },
						{ name: "tools" },
						{ name: "document", groups: ["mode", "document", "doctools"] },
						{ name: "others" },
						"/",
						{ name: "basicstyles", groups: ["basicstyles", "cleanup"] },
						{
							name: "paragraph",
							groups: ["list", "indent", "blocks", "align", "bidi"]
						},
						{ name: "styles" },
						{ name: "colors" },
						{ name: "redo" }
					],
					height: 100,
					entities_latin: false
				}}
				onBeforeLoad={(CKEDITOR) => (CKEDITOR.disableAutoInline = true)}
				data={input.value}
				{...rest}
				onChange={event => {
					event.editor.resize('100%', '400');
					var writer = event.editor.dataProcessor.writer;
					console.log(event.editor, "event.editor");

					// The character sequence to use for every indentation step.
					writer.indentationChars = "\t";

					// The way to close self-closing tags, like <br/>.
					writer.selfClosingEnd = " />";

					// The character sequence to be used for line breaks.
					writer.lineBreakChars = "\n";

					// The writing rules for the <p> tag.
					writer.setRules("p", {
						// Indicates that this tag causes indentation on line breaks inside of it.
						indent: true,

						// Inserts a line break before the <p> opening tag.
						breakBeforeOpen: true,

						// Inserts a line break after the <p> opening tag.
						breakAfterOpen: true,

						// Inserts a line break before the </p> closing tag.
						breakBeforeClose: false,

						// Inserts a line break after the </p> closing tag.
						breakAfterClose: true
					});
					let data = event.editor.getData();
					input.onChange(data);
					if (onChange) {

						setTimeout(() => {
							onChange(data, this.props.name);
							if (handleOnChange) handleOnChange(event, this.props.name);
						}, 100);
					}
				}}
			/>
			<br />
			{meta.error && meta.touched && <span className="error">{meta.error}</span>}
		</React.Fragment>
	)
);
export {
	DateField,
	SelectField,
	TextAreaField,
	RSTextAreaField,
	TreePickerField,
	InputField,
	CheckboxField,
	InputNumberField,
	CheckPickerField,
	UploaderField,
	RadioGroupField,
	CKEditorField,
	CurrencyField
};
