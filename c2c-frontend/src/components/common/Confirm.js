import React from "react";
import { Modal, Icon, Button } from "rsuite";

class Confirm extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      show: false
    };
    this.hide = this.hide.bind(this);
    this.show = this.show.bind(this);
  }

  hide() {
    this.setState({ show: false });
  }

  show() {
    this.setState({ show: true });
  }

  onOK = () => {
    this.hide();
    let { onOk } = this.props;
    if (onOk) onOk();
  }

  render() {
    let { message } = this.props;
    return (
      <div className="modal-container">
        <Modal
          backdrop="static"
          show={this.state.show}
          onHide={this.close}
          size="xs"
        >
          <Modal.Body className="d-flex align-items-center justify-content-center">
            <Icon
              icon="remind"
              style={{
                color: "#ffb300",
                fontSize: 32,
                marginRight: 20
              }}
            />
            {"  "}
            {message}
          </Modal.Body>
          <Modal.Footer>
            <Button onClick={this.onOK} appearance="primary">
              Ok
            </Button>
            <Button onClick={this.hide} appearance="subtle">
              Cancel
            </Button>
          </Modal.Footer>
        </Modal>
      </div>
    );
  }
}

export default Confirm
