import React, { Component } from 'react';

export default class ReportViewer extends Component {

    componentDidMount() {
        var data = { 
            OrderDate: new Date(2019,4,14),           
            BranchId: 6,            
        };

        window.jQuery('#reportViewer1')
            .telerik_ReportViewer({
                serviceUrl: 'http://tqq-report.vnsolution.tech/api/reports/',
                reportSource: {
                    report: 'GoodsReport.trdp?filter=' + btoa(JSON.stringify(data))
                },
                scale: 1.0,
                sendEmail: { enabled: true }
            });
    }

    render() {
        return <div id="reportViewer1"></div>
    }
}

