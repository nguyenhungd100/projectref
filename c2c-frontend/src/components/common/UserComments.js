import React, { Component } from 'react'
import {
    Button,
    Icon
} from 'rsuite';
export default class UserComments extends Component {
    render() {
        return (
            <div className="row mb-3 pt-3">
                <div className="col-1">
                    <img
                        className="img-avatar-drawer"
                        src="/assets/images/6.jpg"
                        alt="avatar-user-nano"
                    />
                </div>
                <div className="col">
                    <div className="row mb-3">
                        <div className="col-1 name-comment">
                            <b>Duy</b>
                        </div>
                        <div className="col time-comment">
                            <span>Comment one hour ago</span>
                        </div>
                    </div>
                    <div className="row">
                        <div className="col">
                            <span>nội dung cmt 1</span>
                        </div>
                    </div>
                </div>
                <div className="col-1">
                    <Button color="red">
                        <Icon icon="trash2"/>
                    </Button>
                </div>
            </div>
        )
    }
}
