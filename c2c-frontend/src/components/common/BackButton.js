import React, { Component, Fragment } from 'react';
import { Icon, Button } from "rsuite";
import i18n from 'i18n'
import Confirm from './Confirm'

export default class extends Component {
    constructor(props){
        super(props);
        this.confirmBack = React.createRef();
    }
    render() {
        const { type, history } = this.props;
        return (
            <Fragment>
                <Button color="orange" onClick={() => this.confirmBack.current.show()}>
                    <Icon icon="arrow-circle-left" className="mr-1" />
                    {i18n.t("Quay lại")}
                </Button>
                <Confirm
                    ref={this.confirmBack}
                    message={<p className="text-center">
                        {i18n.t("Đơn chưa được lưu, quay lại sẽ làm mất thông tin đã điền")}<br/>
                        {i18n.t("Click Ok nếu đồng ý chuyển hướng, Cancel để hủy hành động")}
                        </p>}
                    onOk={() => history.push({
                        pathname: "/document/fill-record",
                        type: type
                    })
                    }
                />
            </Fragment>
        )
    }
}