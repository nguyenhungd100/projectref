import React, { Component } from 'react'
import {List , Message,Icon} from "rsuite";
import {mydatabase} from "components/configFirebase/firebase"
import {CommonContext} from "context"
import { withRouter } from "react-router";


class Notification extends Component {
    static contextType = CommonContext
    state={notifications:[]}
    componentWillReceiveProps(nextProps) {
      this.onLoadNotiFromFireBase()
    }
    componentDidMount = ()=>{

        this.onLoadNotiFromFireBase();
    }
    onLoadNotiFromFireBase = async() => {
        let {profile} = this.context;

        const notiRef = mydatabase.ref(`notifications/u-${profile.id}`);
            notiRef
            .limitToLast(100)
            .on("value", topicsObj => {
              if (!topicsObj.val()) {
                  return;
              }
              let notifications = Object.values(topicsObj.val()) ||[];
              notifications= notifications.sort((a,b)=>new Date(b.createdOn) - new Date(a.createdOn))
              this.setState({notifications })
            });
    };
    onClick = (item)=>{
        let {profile} = this.context;
        const {onHidden} = this.props;
        if(profile.onCms){
            if(item.type === 2)
                window.location.href = "/admin/post/draft"
        }else {
            if(item.type === 3 ){
                this.props.history.push("/user/account/5/approve")
            }
            if(item.type === 4){
                this.props.history.push("/user/account/10/reject")
            }
        }

        if(onHidden)onHidden();

    }
   render() {
       const {notifications} = this.state;
    return (
        <List>
          {notifications.map((item,index)=>
            <List.Item key={index} index={index} style={{cursor:'pointer'}} onClick={()=>{this.onClick(item)}}>
                <p>{item.content}</p>
            </List.Item>
           )}
        </List>
    );
  }
}
export default withRouter(Notification)
