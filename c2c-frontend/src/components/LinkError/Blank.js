import React from "react";
import {Icon} from "rsuite";

function Blank() {

	return (
			<div className="container text-center p-2" style={{marginTop: '5%'}} >
				<div className="align-item-center ">
				<Icon icon="building2" size="5x" style={{color: "#ccc"}}/>
				</div>
				<h2 className="mb-2 text-secondary">Hmm... trang trống</h2>
				<h6 className="mb-4 text-secondary">Dữ liệu trang trống</h6>
			</div>
	);
}
export default Blank;
