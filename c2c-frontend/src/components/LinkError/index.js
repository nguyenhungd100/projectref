import React from "react";
import { Icon } from "rsuite";

function LinkError() {

	return (
		<div className="container text-center p-2" style={{ marginTop: '5%', height: window.innerHeight * 0.74 }} >

			<div className="align-item-center ">
				<img alt="404" src="/assets/images/404.svg" style={{ color: 'white' }} />
			</div>
			<h2 className="mb-2">Rất tiếc, trang này không tồn tại</h2>
			<h6 className="mb-4">Liên kết bạn vừa thực hiện không tồn tại hoặc đã bị xóa</h6>
		</div>
	);
}
export default LinkError;
