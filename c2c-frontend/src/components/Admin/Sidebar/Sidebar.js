import React from "react";
import {NavLink as NavLinkRRD, Link} from "react-router-dom";
// nodejs library to set properties for components
import {PropTypes} from "prop-types";

// reactstrap components
import {
	Button,
	Card,
	CardHeader,
	CardBody,
	CardTitle,
	Collapse,
	DropdownMenu,
	DropdownItem,
	UncontrolledDropdown,
	DropdownToggle,
	FormGroup,
	Form,
	Input,
	InputGroupAddon,
	InputGroupText,
	InputGroup,
	Media,
	NavbarBrand,
	Navbar,
	NavItem,
	NavLink,
	Nav,
	Progress,
	Table,
	Container,
	Row,
	Col
} from "reactstrap";

var ps;
const listMenu = [
	{
		path: "/admin",
		name: "Dashboard",
		icon: "ni ni-tv-2 text-primary"
	},
	{
		path: "/admin/users",
		name: "Người dùng",
		icon: "ni ni-planet text-blue"
	},
	{
		path: "/admin/tables",
		name: "Bài đăng",
		icon: "ni ni-pin-3 text-orange"
	},
	{
		path: "/user-profile",
		name: "User Profile",
		icon: "ni ni-single-02 text-yellow"
	},
	{
		path: "/tables",
		name: "Tables",
		icon: "ni ni-bullet-list-67 text-red"
	},
	{
		path: "/login",
		name: "Login",
		icon: "ni ni-key-25 text-info"
	},
	{
		path: "/register",
		name: "Register",
		icon: "ni ni-circle-08 text-pink"
	}
];
class Sidebar extends React.Component {
	state = {
		collapseOpen: false
	};
	constructor(props) {
		super(props);
		this.activeRoute.bind(this);
	}
	// verifies if routeName is the one active (in browser input)
	activeRoute(routeName) {
		return this.props.location.pathname.indexOf(routeName) > -1 ? "active" : "";
	}
	// toggles collapse between opened and closed (true/false)
	toggleCollapse = () => {
		this.setState({
			collapseOpen: !this.state.collapseOpen
		});
	};
	// closes the collapse
	closeCollapse = () => {
		this.setState({
			collapseOpen: false
		});
	};
	// creates the links that appear in the left menu / Sidebar
	createLinks = () => {
		return listMenu.map((prop, key) => {
			return (
				<NavItem key={key}>
					<NavLink
						to={prop.path}
						tag={NavLinkRRD}
						onClick={this.closeCollapse}
						activeClassName="active"
					>
						<i className={prop.icon} />
						{prop.name}
					</NavLink>
				</NavItem>
			);
		});
	};
	render() {
		const {bgColor, routes, logo} = this.props;
		let navbarBrandProps;
		if (logo && logo.innerLink) {
			navbarBrandProps = {
				to: logo.innerLink,
				tag: Link
			};
		} else if (logo && logo.outterLink) {
			navbarBrandProps = {
				href: logo.outterLink,
				target: "_blank"
			};
		}
		return (
			<Navbar
				className="navbar-vertical fixed-left navbar-light bg-white"
				expand="md"
				id="sidenav-main"
			>
				<Container fluid>
					<button
						className="navbar-toggler"
						type="button"
						onClick={this.toggleCollapse}
					>
						<span className="navbar-toggler-icon" />
					</button>
					{logo ? (
						<NavbarBrand className="pt-0" {...navbarBrandProps}>
							<img alt={logo.imgAlt} className="navbar-brand-img" src={require("assets/img/logo.png")} />
						</NavbarBrand>
					) : null}
					<Nav className="align-items-center d-md-none">
						<UncontrolledDropdown nav>
							<DropdownToggle nav>
								<Media className="align-items-center">
									<span className="avatar avatar-sm rounded-circle">
										<img alt="..." src={require("assets/img/theme/team-1-800x800.jpg")} />
									</span>
								</Media>
							</DropdownToggle>
							<DropdownMenu className="dropdown-menu-arrow" right>
								<DropdownItem className="noti-title" header tag="div">
									<h6 className="text-overflow m-0">Welcome!</h6>
								</DropdownItem>
								<DropdownItem to="/admin/user-profile" tag={Link}>
									<i className="ni ni-single-02" />
									<span>My profile</span>
								</DropdownItem>
								<DropdownItem divider />
								<DropdownItem href="#pablo" onClick={e => e.preventDefault()}>
									<i className="ni ni-user-run" />
									<span>Logout</span>
								</DropdownItem>
							</DropdownMenu>
						</UncontrolledDropdown>
					</Nav>

					<Collapse navbar isOpen={this.state.collapseOpen}>
						<div className="navbar-collapse-header d-md-none">
							<Row>
								{logo ? (
									<Col className="collapse-brand" xs="6">
										{logo.innerLink ? (
											<Link to={logo.innerLink}>
												<img alt={logo.imgAlt} src={logo.imgSrc} />
											</Link>
										) : (
											<a href={logo.outterLink}>
												<img alt={logo.imgAlt} src={logo.imgSrc} />
											</a>
										)}
									</Col>
								) : null}
								<Col className="collapse-close" xs="6">
									<button
										className="navbar-toggler"
										type="button"
										onClick={this.toggleCollapse}
									>
										<span />
										<span />
									</button>
								</Col>
							</Row>
						</div>

						<Nav navbar>{this.createLinks(routes)}</Nav>
						<hr className="my-3" />
					</Collapse>
				</Container>
			</Navbar>
		);
	}
}

Sidebar.defaultProps = {
	routes: [{}]
};

Sidebar.propTypes = {
	routes: PropTypes.arrayOf(PropTypes.object),
	logo: PropTypes.shape({
		innerLink: PropTypes.string,
		outterLink: PropTypes.string,
		imgSrc: PropTypes.string.isRequired,
		imgAlt: PropTypes.string.isRequired
	})
};

export default Sidebar;
