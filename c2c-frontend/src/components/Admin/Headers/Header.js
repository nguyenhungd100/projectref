
import React from "react";

import { Card, CardBody, CardTitle, Container, Row, Col } from "reactstrap";
class Header extends React.Component {
  render() {
    let { totalNumberOfShops = 0, totalNumberOfPosts = 0, year } = this.props;
    return (
      <>

        <Container fluid>
          <div className="header-body mt-4">

            <Row>
              {/* <Col lg="8" xl="4">
                  <Card className="card-stats mb-4 mb-xl-0">
                    <CardBody>
                      <Row>
                        <div className="col">
                          <CardTitle
                            tag="h5"
                            className="text-uppercase text-muted mb-0"
                          >
                            Người dùng
                          </CardTitle>
                          <span className="h2 font-weight-bold mb-0">
                            350,897
                          </span>
                        </div>
                        <Col className="col-auto">
                          <div className="icon icon-shape bg-danger text-white rounded-circle shadow">
                            <i className="fas fa-chart-bar" />
                          </div>
                        </Col>
                      </Row>
                      <p className="mt-3 mb-0 text-muted text-sm">
                        <span className="text-success mr-2">
                          <i className="fa fa-arrow-up" /> 3.48%
                        </span>{" "}
                        <span className="text-nowrap">Cửa hàng</span>
                      </p>
                    </CardBody>
                  </Card>
                </Col> */}
              <Col lg="8" xl="6">
                <Card className="card-stats mb-4 mb-xl-0">
                  <CardBody>
                    <Row>
                      <div className="col">
                        <CardTitle
                          tag="h5"
                          className="text-uppercase text-muted mb-0"
                        >
                          Cửa hàng
                          </CardTitle>
                        <span className="h2 font-weight-bold mb-0">
                          {totalNumberOfShops}
                        </span>
                      </div>
                      <Col className="col-auto">
                        <div className="icon icon-shape bg-warning text-white rounded-circle shadow">
                          <i className="fas fa-chart-pie" />
                        </div>
                      </Col>
                    </Row>
                    <p className="mt-3 mb-0 text-muted text-sm">
                      <span className="text-danger mr-2">
                        {/* <i className="fas fa-arrow-down" /> */}
                        {/* 3.48% */}
                      </span>{" "}
                      <span className="text-nowrap">{year ? "năm " + year : ""}</span>
                    </p>
                  </CardBody>
                </Card>
              </Col>
              <Col lg="8" xl="6">
                <Card className="card-stats mb-4 mb-xl-0">
                  <CardBody>
                    <Row>
                      <div className="col">
                        <CardTitle
                          tag="h5"
                          className="text-uppercase text-muted mb-0"
                        >
                          Bài đăng
                          </CardTitle>
                        <span className="h2 font-weight-bold mb-0">{totalNumberOfPosts}</span>
                      </div>
                      <Col className="col-auto">
                        <div className="icon icon-shape bg-yellow text-white rounded-circle shadow">
                          <i className="fas fa-users" />
                        </div>
                      </Col>
                    </Row>
                    <p className="mt-3 mb-0 text-muted text-sm">
                      <span className="text-warning mr-2">
                        {/* <i className="fas fa-arrow-down" /> */}
                        {/* 1.10% */}
                      </span>{" "}
                      <span className="text-nowrap">{year ? "năm " + year : ""}</span>
                    </p>
                  </CardBody>
                </Card>
              </Col>
            </Row>
          </div>
        </Container>

      </>
    );
  }
}

export default Header;
