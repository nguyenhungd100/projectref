import React, { memo, useEffect, useState, useContext, Component, Suspense } from "react";
import DefaultLayout from "./DefaultLayout/DefaultLayout";
import AdminLayout from "./AdminLayout";
import { Redirect, Route, Switch } from "react-router-dom";
import { CommonContext, AuthContext } from "context";
import LoadingProgressBar from "components/common/LoadingProgressBar";
import LoadingEC from "components/common/LoadingEC";

import { Notification, Drawer, Button, Icon, Placeholder } from "rsuite";
import routes from "routes";
import LinkError from "components/LinkError";
import { Color, hasId } from "constants/common";
import categoryApi from "api/categoryApi";
import userApi from "api/userApi";
import postApi from "api/postApi";
import chat from "chat"
import ChatComponent from "components/chatComponent";
import Swal from "sweetalert2";
import LoadingBar from 'react-top-loading-bar';
const { Paragraph } = Placeholder;
const AppRoute = ({ component: Component, layout: Layout, ...rest }) => {
	return <Route {...rest} render={props => <Component {...props} />} />;
};

class Layout extends Component {
	static contextType = AuthContext;
	state = { animate: "animate-in", temp: true, categories: [], shopPage: '', };
	componentDidMount = async () => {
		this.getCategories();
		let seft = this;
		let { profile } = this.context;
		let myProfile = localStorage.getItem("profile");
		if(!profile)profile = JSON.parse(myProfile);
		if (profile && profile.id) this.setProfile(profile)
		else {
			const isAuth = localStorage.getItem("token");
			if (isAuth) {
				const result = await userApi.profile();
				if (result && result.success) {
					const profile = result.data;
		
					this.setState({ profile });
					seft.onSearchPage(profile)
				}
			}
		}
	};
	onSearchPage = async (profile) => {
		let filter = {
			userId: profile && profile.id,
			pageIndex: 1,
			pageSize: 1
		}
		if (profile && profile.id) {
			let res = await postApi.listStore(filter);
			if (res && res.success) {
				if (res.data && res.data.records && res.data.records.length > 0) {
					this.setState({ shopPage: res.data.records[0] });
				}
			}
		}

	}
	setShopage = () => {
		if (this.state.profile) this.onSearchPage(this.state.profile)
	}
	getProfile = async () => {
		const isAuth = localStorage.getItem("token");
		if (isAuth) {
			const result = await userApi.profile();
			if (result && result.success) {
				const profile = result.data;
				localStorage.setItem("profile", JSON.stringify(profile))
				this.setState({ profile });
			}
		}
	}
	showComfirm = async (model) => {
		let { title, text, confirm, cancel } = model;
		let isConfirm = await Swal.fire({
			title: '<p class="text-16" style="color:red;">' + title + "</p>",
			html: '<p class="text-16">' + text + "<p>",
			showCloseButton: confirm ? confirm : true,
			showCancelButton: cancel ? cancel : true,
			focusConfirm: false,
			confirmButtonText: "Đồng ý",
			cancelButtonText: "Bỏ qua"
		});

		return isConfirm && isConfirm.value ? true : false;
	}
	getCategories = async () => {
		let res = await categoryApi.list();
		if (res && res.success && res.data) this.setCategories(res.data);

	}
	changeLanguage = lng => {
		let self = this;
		let { pathname } = window.location;
		setTimeout(() => {
			self.props.history.push(pathname);
		}, 200);
	};

	loadingComponet = () => (
		<Paragraph style={{ marginTop: 30, height: 500 }} rows={5} graph active />
	);
	success = message => {
		return Notification.success({
			title: "Thông báo",
			description: message ? message : "Thao tác thành công."
		});
	};
	error = message => {
		return Notification.error({
			title: "Thông báo",
			description: message ? message : "Có lỗi xảy ra."
		});
	};
	loading = () => {
		setTimeout(() => {
			if (this.LoadingBaru) this.LoadingBaru.continuousStart()
			// this.loadingBarEC.onShow();
		}, 300);
	};
	unloading = () => {
		setTimeout(() => {
			// this.loadingBarEC.onHide();
			if (this.LoadingBaru) this.LoadingBaru.complete();
		}, 300);
	};
	setProfile = profile => {
		this.setState({ profile });
	};
	close = () => {
		this.setState({
			show: false
		});
	};
	toggleDrawer = () => {
		this.setState({ show: true });
	};
	setCategories = (categories) => {
		this.setState({ categories });
	}

	render() {
		const contextApi = this.context;
		const { profile } = this.state;
		let { pathname } = window.location;
		let isHeader = true;
		let Layout = DefaultLayout;
		if (pathname.indexOf("/admin") === 0) {
			isHeader = false;
			Layout = AdminLayout;
		}
		let isCms = (profile && profile.id && profile.onCms && profile.id == hasId);
		let noCms = (profile && !profile.onCms && profile.id);

		return (
			<div id="home">
				<div className="wrapper ui-scrollbar">
					<CommonContext.Provider
						value={{
							success: this.success,
							error: this.error,
							unloading: this.unloading,
							loading: this.loading,
							setProfile: this.setProfile,
							profile: this.state.profile,
							categories: this.state.categories,
							setCategories: this.setCategories,
							showComfirm: this.showComfirm,
							getProfile: this.getProfile,
							shopPage: this.state.shopPage,
							openChat: this.toggleDrawer,
							setShopage: this.setShopage
						}}
					>

						<Suspense fallback={this.loadingComponet}>
							<Layout isHeader={isHeader}>
								<Switch>
									{routes.map((route, index) => (
										<AppRoute
											contextApi={contextApi}
											component={route.render}
											layout={route.layout}
											{...route}
											key={"routes-" + index}
										/>
									))}
								</Switch>
							</Layout>
							{
								(isCms || noCms) &&
								<div
									className="sc-launcher "
									style={{ zIndex: 6, cursor: "pointer", backgroundColor: Color.global }}
									onClick={this.toggleDrawer}
								>
									<img
										className="sc-closed-icon"
										src="/assets/images/messenger-white.png"
										style={{ width: 45, height: 45, right: 32, bottom: 32 }}
									/>
								</div>
							}
						</Suspense>
						{this.state.show && (
							<Drawer size="lg" show={this.state.show} onHide={this.close}>
								<Drawer.Header className="d-flex justify-content-between">
									<Drawer.Title>
										<Icon
											icon="minus-circle"
											size="2x"
											style={{ color: Color.global, cursor: "pointer" }}
											onClick={this.close}
										/>
									</Drawer.Title>
								</Drawer.Header>
								<Drawer.Body
									className="overflow-hidden mt-0"
									style={{ overflow: "hidden", marginTop: 0 }}
								>
									<ChatComponent />
								</Drawer.Body>
							</Drawer>
						)}
						{
							false &&
							<LoadingEC ref={refs => {
								this.loadingBarEC = refs;
							}} />
						}

						<LoadingBar
							height={3}
							color='#f11946'
							onRef={ref => (this.LoadingBaru = ref)}
						/>
						<LoadingProgressBar
							ref={refs => {
								this.loadingBar = refs;
							}}
						/>
					</CommonContext.Provider>
				</div>
			</div>
		);
	}
}
export default Layout
