import React, { Component } from 'react';
import { Link } from "react-router-dom";
export default class DefaulFooter extends Component {
    render() {
        return (
            <div className="footer">
                {false &&
                    <div className="footer-info">
                        <div className="container">
                            <div className="row">
                                <div className="col-md-3">
                                    <div className="footer-logo"><Link to="/" ><img src="/assets/images/logo2.png" /></Link></div>
                                </div>
                                <div className="col-md-3 col-sm-6">
                                    <h4 className="title"><strong>Thông tin</strong></h4>
                                    <p>Thanh Hóa, Việt Nam</p>
                                    <p>Số DT : (084) 123456789</p>
                                    <p>Email : marketpro@gmail.com</p>
                                </div>
                                <div className="col-md-3 col-sm-6">
                                    <h4 className="title"><strong>Hỗ trợ</strong></h4>
                                    <ul className="support">
                                        <li><Link to="/" >FAQ1</Link></li>
                                        <li><Link to="/" >Mua bán</Link></li>
                                        <li><Link to="/" >Đăng bài</Link></li>
                                        <li><Link to="/" >Thanh toán</Link></li>
                                    </ul>
                                </div>
                                <div className="col-md-3">
                                    <h4 className="title"><strong>Nhận tin mới nhất </strong></h4>
                                    <p>Nhập email của bạn để có những tin mới nhất về Market-pro</p>
                                    <form className="newsletter">
                                        <input type="text" name="" placeholder="Email của bạn..." />
                                        <input type="submit" value="Gửi" className="button" />
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                }
                <div className="copyright-info">
                    <div className="wrap-footer">
                        <div className="container">
                            <div className="row">
                                <div className="col-md-6">
                                    <p>Copyright © 2019. Designed by <Link to="/" >Team Nguyễn Hùng</Link>. All rights reseved</p>
                                </div>
                                <div className="col-md-6">
                                    <ul className="social-icon">
                                        <li><Link to="/" className="linkedin"></Link></li>
                                        <li><Link to="/" className="google-plus"></Link></li>
                                        <li><Link to="/" className="twitter"></Link></li>
                                        <li><Link to="/" className="facebook"></Link></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}
