import React, { Component } from "react";
import { Link } from "react-router-dom";
import { Avatar, Whisper, Popover, IconButton, Icon, Badge } from "rsuite";
import { AuthContext } from "context";
import { Color } from "constants/common"
import fileApi from "api/fileApi"
import "./style.css";
import Notification from "components/Notifycation"
const Speaker = ({ content,onHidden, ...props }) => {
  return (
    <Popover title="Thông báo" {...props} >
     	<p style={{width:300}}></p>
	  <Notification onHidden={onHidden}/>
    </Popover>
  );
};
const PopUser = ({ content, onLogout, onClose, profile, ...props }) => {
	return (
		<Popover title={content} {...props} >
			<p>
				<Link to="/user/account/0308/profile"
					onClick={onClose}
					className="cursor-pointer rs-control-label"
					style={{ borderTopWidth: 1, fontWeight: "400", fontSize: 14, paddingTop: 10, textDecoration: "none", color: "#2f2f2f" }}
				>
					<IconButton size="xs" icon={<Icon icon="user-circle-o" size="xs" />} color={"blue"} circle />{` Tài khoản của tôi`}

				</Link>

			</p>
			{
				profile && profile.onCms &&
				< p >
					<Link to="/admin/post/draft"
						onClick={() => { window.location.href = "/admin/post/draft" }}
						className="cursor-pointer rs-control-label"
						style={{ borderTopWidth: 1, fontWeight: "400", fontSize: 14, paddingTop: 10, textDecoration: "none", color: "#2f2f2f" }}
					>
						<IconButton size="xs" icon={<Icon icon="user-circle-o" size="xs" />} color={"blue"} circle />{` Admin`}

					</Link>

				</p>
			}
			{profile && !profile.onCms &&
				<>
					<p>
						<Link to="/payment/user"
							onClick={onClose}
							className="cursor-pointer rs-control-label"
							style={{ borderTopWidth: 1, fontWeight: "400", fontSize: 14, paddingTop: 10, textDecoration: "none", color: "#2f2f2f" }}
						>
							<IconButton size="xs" icon={<Icon icon="paypal" size="xs" />} color={"blue"} circle />{` Thanh toán`}

						</Link>

					</p>
					<p>
						<Link to="/payment/history"
							onClick={onClose}
							className="cursor-pointer rs-control-label"
							style={{ borderTopWidth: 1, fontWeight: "400", fontSize: 14, paddingTop: 10, textDecoration: "none", color: "#2f2f2f" }}
						>
							<IconButton size="xs" icon={<Icon icon="paypal" size="xs" />} color={"blue"} circle />{` Lịch sử thanh toán`}

						</Link>

					</p>
				</>
			}

			<p style={{ borderTopWidth: 1, fontWeight: "400", fontSize: 14, paddingTop: 10, textDecoration: "none", color: "#2f2f2f" }}
				className="cursor-pointer"
				onClick={onLogout}
			>
				<IconButton size="xs" icon={<Icon icon="sign-out" size="xs" />} color={"orange"} circle />{` Đăng xuất`}

			</p>
		</Popover >
	);
};

export default class DefaultHeader extends Component {
	static contextType = AuthContext;
	state = { showSearch: false, classHeader: "transparent-header" };
	componentDidMount = () => {
		let self = this;
		document.addEventListener("scroll", e => {
			self.changeStyle();
		});
	};
	hiddenSearch = () => {
		this.setState({ showSearch: !this.state.showSearch });
	};
	changeStyle = () => {
		let self = this;
		let { classHeader } = self.state;
		let scrollY = window.scrollY;
		if (scrollY > 60 && classHeader) self.setState({ classHeader: "" });
		else if (scrollY < 60 && !classHeader) {
			self.setState({ classHeader: "transparent-header" });
		}
	};
	onLogout = () => {
		this.context.onLogout();
	};
	onClose = () => {
		if (this.popUser) this.popUser.hide();
	}
	onHidden = ()=>{
		if(this.popNotify)this.popNotify.hide();
	}
	render() {
		const { showSearch, classHeader } = this.state;
		return (
			<header className={`navbar ${classHeader}`} id="main-nav" role="navigation">
				<div className="navbar-left">
					<Link
						to="/"
						className=" animated"
						style={{ alignContent: "stretch", paddingLeft: 40 }}
					>
						<img src="/assets/images/logo/logo.png" alt="FlatShop"  />
					</Link>
				</div>
				<div className="navbar-right">
					<ul>
					{ false &&
						<li>
							<div
								aria-label="Search"
								className="text-header cursor-pointer "
								onClick={this.hiddenSearch}
							>
								Tìm kiếm <i className="fa fa-search ml-1" />
							</div>
						</li>
					}
					<li className="nosubmenu">
						<Link to="/" className="text-header">
							Trang chủ
						</Link>
					</li>
						<div className="sign-up-container">

							<AuthContext.Consumer>
								{context => {
									let { isAuth, profile = {} } = context;
									if (isAuth)
										return (
											<>
												<li className="nosubmenu">
													<Link to="/post-info" className="text-header" >
														Đăng tin
													</Link>
												</li>
													<li className="nosubmenu">
														<Badge content={"New"} style={{marginRight:20, width:40,height:40}}>
															<Whisper
															triggerRef={refs => this.popNotify = refs}
															trigger="click"
															placement={"auto"}
															speaker={<Speaker onHidden={this.onHidden}/>}
														  >
															<IconButton icon={<Icon icon="bell" size="2x" style={{ color: "#ffffff" }} />} circle style={{background:Color.global, width:40,height:40}} size="md" />
														</Whisper>
													</Badge>
												</li>

												<li className="nosubmenu cursor-pointer ml-4">
													<Whisper
														triggerRef={refs => this.popUser = refs}
														trigger="click"
														placement={"bottom"}
														speaker={<PopUser content={`${profile.fullName}`} onLogout={this.onLogout} onClose={this.onClose} profile={profile} />}
													>
														<Avatar
															circle
															size="md"
															src={(profile && profile.imageId) ? fileApi.getImage(profile.imageId, 50, 50) : "https://avatars2.githubusercontent.com/u/12592949?s=460&v=4"}
														/>
													</Whisper>
												</li>
											</>
										)
									return (
										<>
											<li className="nosubmenu">
												<Link to="/auth/login" className="text-header">
													Đăng nhập
												</Link>
											</li>
											<li className="nosubmenu ">
												<Link to="/auth/sign-up" className="text-header">
													Đăng ký
												</Link>
											</li>
										</>
									);
								}}
							</AuthContext.Consumer>


						</div>
					</ul>
				</div>
				<div className="mobile-icons" >
					<button
						aria-label="Search"
						className="search-icon js-search-toggle"
						type="button"
						style={{ background: 'transparent' }}
					>
						<i className="fas fa-search" />
					</button>
					<button className="mobile-icon" id="mobile-icon" style={{ background: 'transparent' }}>
						<div className="icon-bar" />
						<div className="icon-bar" />
						<div className="icon-bar" />
					</button>
				</div>
				<div className="display-none mobile-menu" id="mobile-menu">
					<button className="exit-icon-container" id="exit-icon">
						<img
							alt="Gitlab x icon svg"
							className="exit-icon"
							src="/images/icons/x.svg"
						/>
					</button>
					<ul>
						<li>
							<div
								aria-label="Search"
								className="text-header cursor-pointer "
								onClick={this.hiddenSearch}
							>
								Tìm kiếm <i className="fa fa-search ml-1" />
							</div>
						</li>
						<div className="sign-up-container">
							<li className="nosubmenu">
								<Link to="/post-info" className="text-header" >
									Đăng tin miễn phí
								</Link>
							</li>
							<AuthContext.Consumer>
								{context => {
									let { isAuth } = context;
									if (isAuth)
										return (
											<li className="nosubmenu cursor-pointer">
												<Whisper
													trigger="click"
													placement={"bottom"}
													speaker={<PopUser onClick={this.onLogout} content="Nhật Linh" />}
												>
													<Avatar
														circle
														src="https://avatars2.githubusercontent.com/u/12592949?s=460&v=4"
													/>
												</Whisper>
											</li>
										)
									return (
										<>
											<li className="nosubmenu">
												<Link to="/Auth" className="text-header">
													Đăng nhập
												</Link>
											</li>
											<li className="nosubmenu ">
												<Link to="/Auth" className="text-header">
													Đăng ký
												</Link>
											</li>
										</>
									);
								}}
							</AuthContext.Consumer>


						</div>
					</ul>
				</div>
				{
					showSearch && (
						<div className="search-box js-search-box search-visible text-header-icon">
							<i className="fa fa-search search-box-icon" />
							<input
								className="search-input js-search text-header-icon"
								placeholder="Tìm kiếm..."
							/>
							<button
								aria-label="Close search"
								className="search-close js-search-toggle"
								type="button"
								onClick={this.hiddenSearch}
							>
								<i className="fa fa-times" />
							</button>
						</div>
					)
				}
			</header >
		);
	}
}
