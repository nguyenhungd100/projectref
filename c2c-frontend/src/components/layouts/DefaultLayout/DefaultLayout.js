import React, { Component, Suspense } from "react";
import { Redirect, Route, Switch } from "react-router-dom";
import { CommonContext, AuthContext } from "context";
import { Placeholder } from "rsuite"
import routes from "routes";
const { Paragraph } = Placeholder;

const DefaultHeader = React.lazy(() => import("./DefaultHeader"));
const DefaulFooter = React.lazy(() => import("./DefaulFooter"));

const LinkError = React.lazy(() => import("components/LinkError"));

class DefaultLayout extends Component {
	static contextType = CommonContext;
	state = { animate: "animate-in", temp: true, categories: [], shopPage: '' };

	changeLanguage = lng => {
		let self = this;
		let { pathname } = window.location;
		setTimeout(() => {
			self.props.history.push(pathname);
		}, 200);
	};

	loadingComponet = () => (
		<div className="animated fadeIn pt-1 text-center">Loading...</div>
	);
	loadingComp = () => (
		<Paragraph style={{ marginTop: 30, height: 500 }} rows={5} graph active />
	);

	render() {
		let { pathname } = window.location;

		return (
			<>
				<Suspense fallback={this.loadingComponet}>
					<div id="home">
						<div className="wrapper ui-scrollbar">
							<DefaultHeader changeLanguage={this.changeLanguage} />
							<div className="hero-container blhero2 skhero1" style={{ minHeight: window.innerHeight * 0.85 }}>
								<div>
									<Switch>
										{routes
											.map((route, idx) => {
												return (
													<Route
														key={idx}
														path={route.path}
														exact={route.exact}
														name={route.name}
														render={props => <route.component {...props} key={idx} />}
													/>
												)
											})}
										<Route to={pathname} render={props => <LinkError {...props} />} />
									</Switch>
								</div>
							</div>
						</div>
					</div>
					<DefaulFooter />
				</Suspense>
			</>
		);
	}
}
export default DefaultLayout;
