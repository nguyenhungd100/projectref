import {
	Navbar,
	Dropdown,
	Nav,
	Button,
	Icon,
	Container,
	Sidebar,
	Header,
	Content,
	Avatar,
	Sidenav,
	Popover,
	Whisper,
	Badge,
	IconButton
} from "rsuite";
import React from "react";
import { Route, Link } from "react-router-dom";
import { AuthContext } from "context";
import routes from "routes";
import fileApi from "api/fileApi";
import Notification from "components/Notifycation"

const headerStyles = {
	padding: 18,
	fontSize: 16,
	height: 56,
	background: "#3498ff",
	color: " #fff",
	whiteSpace: "nowrap",
	overflow: "hidden"
};

const iconStyles = {
	width: 56,
	height: 56,
	lineHeight: "56px",
	textAlign: "center"
};
const Speaker = ({ content ,onHidden, ...props }) => {
  return (
    <Popover title="Thông báo" {...props} >
     	<p style={{width:300}}></p>
	  <Notification onHidden={onHidden}/>
    </Popover>
  );
};


const PopUser = ({ content, onLogout, onClose, profile, ...props }) => {
	return (
		<Popover style={{ width: 150 }} title={content} {...props} >
			<p>
				<Link to="/"
					onClick={onClose}
					className="cursor-pointer rs-control-label"
					style={{ borderTopWidth: 1, fontWeight: "400", fontSize: 14, paddingTop: 10, textDecoration: "none", color: "#2f2f2f" }}
				>
					<IconButton size="xs" icon={<Icon icon="user-circle-o" size="xs" />} color={"blue"} circle />{` Tài khoản`}

				</Link>
			</p>

			<p style={{ borderTopWidth: 1, fontWeight: "400", fontSize: 14, paddingTop: 10, textDecoration: "none", color: "#2f2f2f" }}
				className="cursor-pointer"
				onClick={onLogout}
			>
				<IconButton size="xs" icon={<Icon icon="sign-out" size="xs" />} color={"orange"} circle />{` Đăng xuất`}

			</p>
		</Popover >
	);
};
const NavToggle = ({ expand, onChange, onClick }) => {
	return (
		<Navbar appearance="subtle" className="nav-toggle">
			<Navbar.Body>
				<Nav>
					<Dropdown
						placement="rightEnd"
						trigger="click"
						renderTitle={children => {
							return <Icon style={iconStyles} icon="cog" />;
						}}
					>
						<Dropdown.Item onClick={onClick} >Đăng xuất</Dropdown.Item>
					</Dropdown>
				</Nav>

				<Nav pullRight>
					<Nav.Item onClick={onChange} style={{ width: 56, textAlign: "center" }}>
						<Icon icon={expand ? "angle-left" : "angle-right"} />
					</Nav.Item>
				</Nav>
			</Navbar.Body>
		</Navbar>
	);
};
class Admin extends React.Component {
	static contextType = AuthContext;
	constructor(props) {
		super(props);
		this.state = {
			expand: true
		};
		this.handleToggle = this.handleToggle.bind(this);
	}
	handleToggle() {
		this.setState({
			expand: !this.state.expand
		});
	}
	logout = () => {
		localStorage.removeItem("token");
		return (window.location.href = "/");
	}
	goHome = () => {
		window.location.href = "/"
	}
	getRoutes = routes => {
		return routes
			.map((prop, key) => {
				if (prop.layout === "/admin") {
					return (
						<Route
							path={prop.path}
							component={prop.component}
							key={key}
							exact={prop.exact}
						/>
					);
				}
			});
	};
	onHidden = ()=>{
		if(this.popNotify)this.popNotify.hide();
	}
	render() {
		const { expand } = this.state;
		return (
			<div className="show-fake-browser sidebar-page">
				<Container>
					<Sidebar
						style={{ display: "flex", flexDirection: "column" }}
						width={expand ? 260 : 56}
						collapsible
					>
						<Sidenav.Header>
							<div style={headerStyles} onClick={this.goHome}>
								<Icon icon="logo-analytics" size="lg" style={{ verticalAlign: 0 }} />
								<span style={{ marginLeft: 12 }}> Admin ChodientuC2C</span>
							</div>
						</Sidenav.Header>
						<Sidenav
							expanded={expand}
							defaultOpenKeys={["3"]}
							defaultActiveKey="2"
							appearance="subtle"
						>
							<Sidenav.Body>
								<Nav>
									<Nav.Item eventKey="1"
										componentClass={Link}
										to="/admin"
										active icon={<Icon icon="dashboard" />}>
										Dashboard
									</Nav.Item>
									<Nav.Item
										eventKey="2"
										componentClass={Link}
										to="/admin/users"
										icon={<Icon icon="group" />}
									>
										Tài khoản
									</Nav.Item>
									<Nav.Item
										eventKey="3"
										componentClass={Link}
										to="/admin/stores"
										icon={<Icon icon="suitcase" />}
									>
										Cửa hàng
									</Nav.Item>
									<Dropdown
										eventKey="4"
										trigger="hover"
										title="Bài đăng"
										icon={<Icon icon="magic" />}
										placement="rightTop"
									>
										<Dropdown.Item
											eventKey="3-1"
											componentClass={Link}
											to="/admin/post/draft"
										>
											Chưa duyệt
										</Dropdown.Item>
										<Dropdown.Item
											eventKey="3-2"
											componentClass={Link}
											to="/admin/post/approved"
										>
											Đã duyệt
										</Dropdown.Item>
										<Dropdown.Item
											eventKey="3-3"
											componentClass={Link}
											to="/admin/post/reject"
										>
											Tù chối
										</Dropdown.Item>
									</Dropdown>
									<Nav.Item
										eventKey="4"
										componentClass={Link}
										to="/admin/denounce"
										icon={<Icon icon="object-group" />}>
										Danh sách tin tố cáo
									</Nav.Item>
									<Nav.Item
										eventKey="5"
										componentClass={Link}
										to="/admin/permissions"
										icon={<Icon icon="object-group" />}>
										Bảng phân quyền
									</Nav.Item>
								</Nav>
							</Sidenav.Body>
						</Sidenav>
						<NavToggle expand={expand} onChange={this.handleToggle} onClick={this.logout} />
					</Sidebar>

					<Container>
						<Header>
							<Navbar appearance="inverse" className="justify-content-between">
								<Navbar.Header />
								<Navbar.Body>


									<Nav pullRight style={{ marginRight: 20, paddingTop: 10, display:'flex' }}>


										<AuthContext.Consumer>
											{context => {
												let { isAuth, profile = {} } = context;
												if (isAuth)
													return (
														<>
														<Badge content={"New"} style={{marginRight:20, width:40,height:40}}>
															<Whisper
															triggerRef={refs => this.popNotify = refs}
															trigger="click"
															placement={"auto"}
															speaker={<Speaker onHidden={this.onHidden} />}
														  >
															<IconButton icon={<Icon icon="bell" size="2x" style={{ color: '#3498ff' }} />} circle style={{background:"#ffffff", width:40,height:40}} size="md" />
														  </Whisper>
													  </Badge>

														<Whisper
															triggerRef={refs => this.popUser = refs}
															trigger="click"
															placement={"bottom"}
															speaker={<PopUser content={`Xin chào ${profile.fullName}`} onLogout={this.logout} onClose={this.onClose} profile={profile} />}
														>
															<Avatar
																circle
																size="md"
																src={(profile && profile.imageId) ? fileApi.getImage(profile.imageId, 50, 50) : "https://avatars2.githubusercontent.com/u/12592949?s=460&v=4"}
															/>
														</Whisper>
														</>
													)
											}}
										</AuthContext.Consumer>
									</Nav>

								</Navbar.Body>
							</Navbar>
						</Header>
						<Content>{this.getRoutes(routes)}</Content>
					</Container>
				</Container>
			</div>
		);
	}
}

export default Admin;
