import React from "react";

import { mydatabase } from "./firebase";
import { UserApi } from "api";

const userApi = new UserApi();

export class Notifications extends React.Component {
  state = {
    notifications: [],
    selectedNoti: null
  };

  componentDidMount = () => {
    this.onLoadNotifications();
  };

  onLoadNotifications = async () => {
    const userModel = await this.onLoadUserProfile();
    if (userModel) {
      const userId = userModel.id;
      this.onLoadNotiFromFireBase(userId);
    }
  };

  onLoadUserProfile = async () => {
    const resposne = await userApi.get_my_profile();
    if (resposne && resposne.success) {
      return resposne.data;
    }
    return null;
  };

  onLoadNotiFromFireBase = userId => {
    const notiRef = mydatabase.ref(`Notification/user-${userId}`);
    notiRef.on("value", data => {
      if (data.val() !== null) {
        const notifications = Object.entries(data.val()).map(
          ([key, value]) => ({ notiKey: key, ...value, userId })
        );
        this.setState({ notifications });
      }
    });
  };

  render() {
    return (
      <div className="board-wrapper">
        <div className="board-main-content">
          <div className="board-canvas">
            <div id="board" className="u-fancy-scrollbar ui-sortable">
              {this.renderActionsBar()}
              {this.renderNotiItemList()}
            </div>
          </div>
        </div>
      </div>
    );
  }

  renderActionsBar = () => {
    return (
      <div className="list-wrapper" style={{ width: "35%" }}>
        <div className="list">
          <div className="list-header u-clearfix is-menu-shown">
            <div className="list-header-target" />
            <h2 dir="auto">Tìm kiếm</h2>
          </div>
          <div className="list-cards u-fancy-scrollbar u-clearfix ui-sortable" />
        </div>
      </div>
    );
  };

  renderNotiItemList = () => {
    const { notifications } = this.state;
    return (
      <div className="list-wrapper" style={{ width: "63%" }}>
        <div className="list">
          <div className="list-header u-clearfix is-menu-shown">
            <div className="list-header-target" />
            <h2 dir="auto">Thông báo</h2>
          </div>
          <div className="list-cards u-fancy-scrollbar u-clearfix ui-sortable">
            {notifications.map(notiModel => this.renderNotiItem(notiModel))}
          </div>
        </div>
      </div>
    );
  };

  renderNotiItem = notiModel => {
    return (
      <div
        key={notiModel.notiKey}
        className="list-card-custom ui-droppable"
        style={{ margin: 5 }}
      >
        <div className="list-card-details">
          <div className="list-card-label" />
          <span className="list-card-title" dir="auto">
            <span className="card-short-id hide">#1</span>
            {notiModel.Data}
          </span>
          <div className="badges">
            <div className="badge is-icon-only" title="Thẻ đã có miêu tả.">
              <span className="badge-icon icon-sm icon-description" />
            </div>
          </div>
        </div>
      </div>
    );
  };
}
