import firebase from 'firebase/app'
import 'firebase/database';

const config = {
    apiKey: "AIzaSyDb68c3yG-aXbc_3OF86Xf7mM90G113v6w",
    authDomain: "chodientuc2c-3b9ba.firebaseapp.com",
    databaseURL: "https://chodientuc2c-3b9ba.firebaseio.com",
    projectId: "chodientuc2c-3b9ba",
    storageBucket: "chodientuc2c-3b9ba.appspot.com",
    messagingSenderId: "1011961952677",
    appId: "1:1011961952677:web:e23e1da24c026cfb5f504c",
    measurementId: "G-XDX8VQK94H"
};

firebase.initializeApp(config);

const mydatabase = firebase.database();

const initDiscussions = async (model, userId) => {
    const msgKey = await initMessage(userId);
    initTopic(model, msgKey);
}

//crate message
const initMessage = (userId) => {
    const msgRef = mydatabase.ref(`Converation/messages`);
    const msgKey = msgRef.push().key;
    const now = new Date();
    const initMsg = {
        sender: {
            name: "linhha",
            avatarUrl: "",
            id: 5
        },
        userId,
        createdSince: now,
        content: 'Xin chào, tôi muốn kết nối với bạn'
    }
    msgRef.child(msgKey).push(initMsg);
    return msgKey;
}
// model = {
//     createdId,
//     createName,
//     createdSince: now.toUTCString(),
//     userId,
//     userName,
//     topicKey,
//     lastMessage: ''}
//create group chat


const initTopic = (model, msgKey) => {
    const topicRef = mydatabase.ref(`Converation/topics`);
    const topicKey = topicRef.push().key;
    const now = new Date();
    const topicModel = {
        ...model,
        createdSince: now.toUTCString(),
        messageKey: msgKey,
        topicKey
    }

    const update = {};
    update[`Converation/topics/${topicKey}`] = topicModel;
    mydatabase.ref().update(update);
}

export { mydatabase, firebase, initDiscussions, initTopic };
