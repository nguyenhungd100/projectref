﻿using IdentityModel;
using IdentityServer4.Models;
using IdentityServer4.Validation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using PCC1.PMS.Domain.Entity;
using PCC1.PMS.Domain.Models.Users;
using PCC1.PMS.Domain.Services.Roles;
using PCC1.PMS.Domain.Services.Users;
using PCC1.PMS.Framework.Utils;

namespace PCC1.PMS.Authentication.IdentityServer
{
    public class ResourceOwnerPasswordValidator : IResourceOwnerPasswordValidator
    {
        //repository to get user from db
        private readonly IAuthorizationService _authorizationService;
        private readonly IRolesService _rolesService;

        public ResourceOwnerPasswordValidator(IAuthorizationService authorizationService, IRolesService rolesService)
        {
            _authorizationService = authorizationService; //DI
            _rolesService = rolesService;
        }

        //this is used to validate your user account with provided grant at /connect/token
        public async Task ValidateAsync(ResourceOwnerPasswordValidationContext context)
        {
            try
            {
                //get your user model from db (by username - in my case its email)
                //var user = await _userService.SingleAsync(c => c.Email == context.UserName || c.Mobile == context.UserName);
                UserModel userModel = null;

                //check if password match - remember to hash password if stored as hash in db
                if (_authorizationService.VerifyPassword(context.UserName, EncryptUtil.EncryptMD5(context.Password), userModel: ref userModel))
                {
                    if (userModel != null)
                    {
                        context.Result = new GrantValidationResult(
                        subject: userModel.Id.ToString(),
                        authenticationMethod: "custom",
                        claims: Config.GetUserClaims(userModel, _rolesService));
                    }
                    else
                    {
                        context.Result = new GrantValidationResult(TokenRequestErrors.InvalidGrant, "User does not exist.");
                    }
                }
                else
                {
                    context.Result = new GrantValidationResult(TokenRequestErrors.InvalidGrant, "Incorrect password");
                }
            }
            catch (Exception ex)
            {
                context.Result = new GrantValidationResult(TokenRequestErrors.InvalidRequest, "Invalid request");
            }
        }
    }
}
