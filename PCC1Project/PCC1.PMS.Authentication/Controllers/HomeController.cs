﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace PCC1.PMS.Authentication.Controllers
{
    public class HomeController : Controller
    {
        /// <summary>
        /// About system infor
        /// </summary>
        /// <returns></returns>
        [Route("/")]
        [HttpGet]
        public string Index()
        {
            return "PCC1 PMS AUTHORIZATION SERVER";
        }

        [Route("/Error")]
        [HttpGet]
        public string Error()
        {
            return "INTERNAL SERVER ERROR. PLEASE CONTACT WITH ADMIN";
        }
    }
}