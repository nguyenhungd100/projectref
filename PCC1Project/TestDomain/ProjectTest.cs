﻿using MicroOrm.Dapper.Repositories;
using MicroOrm.Dapper.Repositories.SqlGenerator;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using PCC1.PMS.Domain.Entity;
using PCC1.PMS.Domain.Enums;
using PCC1.PMS.Domain.Models.Projects;
using PCC1.PMS.Domain.Repositories;
using PCC1.PMS.Domain.Services.Projects;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using System.Threading.Tasks;

namespace TestDomain
{
    [TestClass]
    public class ProjectTest
    {
        private static IDbConnection CreateConnection()
        {
            var connection = new SqlConnection("Server=35.229.137.189;Database=PCC1_PMS_DB;user=pcc1_dbo;" +
                "password=2pSrBfFutans2wBM;MultipleActiveResultSets=true");
            return connection;
        }

        //private readonly IDapperRepository<ProjectUser> _projectUserRepository;
        private readonly IDbConnection connection = CreateConnection();
        private readonly ISqlGenerator<Project> sqlGenerator1 = new SqlGenerator<Project>();
        private readonly ISqlGenerator<ProjectUser> sqlGenerator2 = new SqlGenerator<ProjectUser>();
        private readonly ISqlGenerator<ProjectPlan> sqlGenerator3 = new SqlGenerator<ProjectPlan>();
        private readonly ISqlGenerator<ProjectReport> sqlGenerator4 = new SqlGenerator<ProjectReport>();

        public ProjectTest()
        {
            _projectRepository = new ProjectRepository(connection, sqlGenerator1);
            _projectUserRepository = new ProjectUserRepository(connection, sqlGenerator2);
            _projectPlanRepository = new ProjectPlanRepository(connection, sqlGenerator3);
            _projectReportRepository = new ProjectReportRepository(connection, sqlGenerator4);
        }
        private IProjectRepository _projectRepository;
        private IDapperRepository<ProjectUser> _projectUserRepository;
        private IDapperRepository<ProjectPlan> _projectPlanRepository;
        private IDapperRepository<ProjectReport> _projectReportRepository;






        /// <summary>
        /// Test khởi tạo dự án không được để để status khác Init
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        public void TestMethod1()
        {
            //ProjectService _projectService = new ProjectService(_projectRepository,
            //    _projectUserRepository,
            //    _projectPlanRepository,
            //    _projectReportRepository);
            //ProjectModel projectModel = new ProjectModel();
            //projectModel.Name = "s";
            //projectModel.Type = TypeProject.DuAnCongTy;
            //projectModel.Status = ProjectStatus.Running;
            //ProjectModel result = null;
            //try
            //{
            //    result = await _projectService.InitProject(projectModel);
            //}
            //catch (Exception ex)
            //{
            //    Assert.IsTrue(result == null, ex.GetBaseException().ToString());
            //}
            throw new NotImplementedException();
        }

        /// <summary>
        /// Test khởi dự án không được thêm các trường gia hạn hợp đồng( date start, date end),
        /// tổng giá trị hợp đồng, địa chỉ( chỉ dành cho cán bộ quản lý dự án)
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        public void TestMethod2()
        {
            //ProjectService _projectService = new ProjectService(_projectRepository,
            //    _projectUserRepository,
            //    _projectPlanRepository,
            //    _projectReportRepository);
            //ProjectModel projectModel = new ProjectModel();
            //projectModel.Name = "s";
            //projectModel.Type = TypeProject.DuAnCongTy;
            //projectModel.Status = ProjectStatus.Init;
            //projectModel.DateEnd = DateTime.Now; /*Datetime("3/7/2018");*/

            //ProjectModel result = null;
            //try
            //{
            //    result = await _projectService.InitProject(projectModel);                
            //}
            //catch (Exception ex) { }
            //Assert.IsTrue(result == null);


            throw new NotImplementedException();
            //var project = _projectRepository.FindById(projectModel.Id);
            //Assert.Equals(projectModel.Status, ProjectStatus.Init);
        }

        [TestMethod]
        public void TestMethod3()
        {

        }


















































        //private readonly IProjectService _projectService;

        //private readonly ProjectRepository _projectRepository ;
        //public ProjectTest()
        //{
        //    _projectService = new ProjectService(_projectRepository);
        //}

        //[TestMethod]
        //public async Task InitProjectService()
        //{
        //    ProjectModel projectModel = new ProjectModel()
        //    {
        //        Name = "s",
        //        Type = TypeProject.DuAnCongTy,
        //        Status = ProjectStatus.Running
        //    };
        //    var result = await _projectService.InitProject(projectModel);
        //    if (result.Item2 == false)
        //    {
        //        Assert.Equals(projectModel.Status, ProjectStatus.Init);
        //    }

        //}
    }
}
