﻿using System;
using System.Collections.Generic;
using System.IO;
using Autofac;
using Autofac.Extensions.DependencyInjection;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Serilog;
using Swashbuckle.AspNetCore.Swagger;
using PCC1.PMS.Api.Authorization;
using PCC1.PMS.Api.AutofacModules;
using PCC1.PMS.Api.Configuration;
using PCC1.PMS.Api.Helpers;
using PCC1.PMS.Domain.Services;
using PCC1.PMS.Domain.Services.Roles;
using PCC1.PMS.Domain.Services.Roles.Implementation;

namespace PCC1.PMS.Api
{
    public class Startup
    {
        public Startup(IConfiguration configuration, IHostingEnvironment env)
        {
            Configuration = configuration;
            DomainMaps.Config();


            var builder = new ConfigurationBuilder()
                    .SetBasePath(Directory.GetCurrentDirectory())
                    .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true)
                    .AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: true)
                    .AddEnvironmentVariables();

            Configuration = builder.Build();

            Log.Logger = new LoggerConfiguration()
                        .ReadFrom.Configuration(Configuration)
                        //.WriteTo.Sentry("https://f65f908fde9940f0b2b16c2045643ea9@sentry.io/1260343")
                        .Enrich.FromLogContext()
                        .CreateLogger();
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public IServiceProvider ConfigureServices(IServiceCollection services)
        {

            services.AddMemoryCache();
            services
                .AddMvc(config => {
                    config.Filters.Add(typeof(GlobalExceptionFilter));
                })
                .SetCompatibilityVersion(CompatibilityVersion.Version_2_1);

            services.AddSwaggerGen(config =>
            {
                config.SwaggerDoc("v1", new Info { Title = "PCC1 PMS Api v1", Version = "V1" });
                config.IncludeXmlComments(string.Format("{0}\\PCC1.PMS.Api.xml", AppDomain.CurrentDomain.BaseDirectory));
                config.IncludeXmlComments(string.Format("{0}\\PCC1.PMS.Domain.xml", AppDomain.CurrentDomain.BaseDirectory));
            });

            // Add our Config object so it can be injected
            var appSettingSection = Configuration.GetSection("AppSettings");
            services.Configure<AppSettings>(appSettingSection);
            var appSettings = new AppSettings();
            appSettingSection.Bind(appSettings);

            services.AddAuthentication("Bearer")
                   .AddIdentityServerAuthentication(options =>
                   {
                       options.Authority = appSettings.AuthenticationServer;
                       options.RequireHttpsMetadata = false;
                       options.ApiName = "pcc1_api";
                   });

            //Authorize user access
            services.AddAuthorization(options =>
            {
                IRolesService rolesService = new RolesService();
                var permissions = rolesService.ListPermissions();
                var all = new List<Permission>();
                permissions.ForEach(k => all.AddRange(k.Permissions));
                foreach (var permission in all)
                {
                    var policyName = permission.Code.ToString();
                    options.AddPolicy(policyName,
                        policy => policy.Requirements.Add(new PermissionRequirement(permission.Code)));
                }
            });

            // Create an Autofac Container and push the framework services
            var containerBuilder = new ContainerBuilder();
            containerBuilder.Populate(services);
            containerBuilder.RegisterModule(new DomainModule(Configuration.GetConnectionString("DefaultConnection")));
            //containerBuilder.RegisterModule(new FrameworkModule(
            //    Configuration.GetConnectionString("RedisConnection"), 
            //    appSettings.RedisDbNumber));

            containerBuilder.RegisterModule(new NotificationModule(appSettings.FirebaseUrl));
            containerBuilder.RegisterModule(new SendMailModule(appSettings.EmailAddress, appSettings.EmailPass));
            services.AddCors();

            // Build the container and return an IServiceProvider from Autofac
            var container = containerBuilder.Build();
            return container.Resolve<IServiceProvider>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            app.AddSentryContext();

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseHsts();
            }

            app.UseCors(builder => builder
                    .AllowAnyOrigin()
                    .AllowAnyMethod()
                    .AllowAnyHeader()
                    .AllowCredentials());

            app.UseAuthentication();
            //app.UseHttpsRedirection();
            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=Index}/{id?}");
            });

            //implement docs for api
            app.UseSwagger();
            app.UseSwaggerUI(config => { config.SwaggerEndpoint("v1/swagger.json", "PCC1 PMS Api v1"); config.DocExpansion(DocExpansion.None); config.ShowExtensions();});
        }
    }
}
