﻿using Autofac;
using MicroOrm.Dapper.Repositories;
using MicroOrm.Dapper.Repositories.SqlGenerator;
using System.Data;
using System.Data.SqlClient;
using PCC1.PMS.Domain.Repositories;
using PCC1.PMS.Domain.Services.Users.Implementation;
using PCC1.PMS.Domain.Services.Projects;
using PCC1.PMS.Domain.Services.Roles.Implementation;
using PCC1.PMS.Domain.Services.ProjectUsers;
using PCC1.PMS.Domain.Services.WorkItems.Implementation;
using PCC1.PMS.Domain.Services.FileStorages;
using PCC1.PMS.Domain.Services.ProjectReports;
using PCC1.PMS.Domain.Services.RegisterMaterials.Implementation;
using PCC1.PMS.Domain.Services.Common;
using PCC1.PMS.Domain.Services.Common.Impelementation;
using PCC1.PMS.Domain.Services.Companies.Implementation;
using PCC1.PMS.Domain.Services.SafelyReports.Implementation;
using PCC1.PMS.Domain.Services.ImageLibraries.Implementation;
using PCC1.PMS.Domain.Services.TrackingMoneyAdvances.Implementation;

namespace PCC1.PMS.Api.AutofacModules
{
    public class DomainModule : Module
    {
        private readonly string _strCnn;

        public DomainModule(string strCnn)
        {
            this._strCnn = strCnn;
        }
        protected override void Load(ContainerBuilder builder)
        {
            base.Load(builder);

            builder.Register(c => new SqlConnection(_strCnn)).As<IDbConnection>().InstancePerLifetimeScope();

            // Register your own services within Autofac
            builder.RegisterGeneric(typeof(SqlGenerator<>)).AsImplementedInterfaces().InstancePerLifetimeScope();
            builder.RegisterGeneric(typeof(DapperRepository<>)).AsImplementedInterfaces().InstancePerLifetimeScope();
            builder.RegisterType<UserRepository>().AsImplementedInterfaces().InstancePerLifetimeScope();
            builder.RegisterType<RoleRepository>().AsImplementedInterfaces().InstancePerLifetimeScope();
            builder.RegisterType<ProjectRepository>().AsImplementedInterfaces().InstancePerLifetimeScope(); 
            builder.RegisterType<ProjectUserRepository>().AsImplementedInterfaces().InstancePerLifetimeScope();
            builder.RegisterType<WorkitemRepository>().AsImplementedInterfaces().InstancePerLifetimeScope();
            builder.RegisterType<FileRepository>().AsImplementedInterfaces().InstancePerLifetimeScope();
            //builder.RegisterType<ProjectPlanRepository>().AsImplementedInterfaces().InstancePerLifetimeScope();
            //builder.RegisterType<ProjectReportRepository>().AsImplementedInterfaces().InstancePerLifetimeScope();
            builder.RegisterType<ProjectReportRepository>().AsImplementedInterfaces().InstancePerLifetimeScope();
            builder.RegisterType<RegisterMaterialsRepository>().AsImplementedInterfaces().InstancePerLifetimeScope();
            builder.RegisterType<CompanyRepository>().AsImplementedInterfaces().InstancePerLifetimeScope();
            builder.RegisterType<SafelyReportRepository>().AsImplementedInterfaces().InstancePerLifetimeScope();
            builder.RegisterType<ImageLibraryRepository>().AsImplementedInterfaces().InstancePerLifetimeScope();
            builder.RegisterType<TrackingMoneyAdvanceRepository>().AsImplementedInterfaces().InstancePerLifetimeScope();

            builder.RegisterType<HashidService>().AsImplementedInterfaces();
            builder.RegisterType<UserService>().AsImplementedInterfaces();
            builder.RegisterType<RolesService>().AsImplementedInterfaces();
            builder.RegisterType<ProjectService>().AsImplementedInterfaces();
            builder.RegisterType<ProjectUserService>().AsImplementedInterfaces();
            builder.RegisterType<WorkItemService>().AsImplementedInterfaces();
            builder.RegisterType<FileStorageService>().AsImplementedInterfaces();
            builder.RegisterType<ProjectReportService>().AsImplementedInterfaces();
            builder.RegisterType<RegisterMaterialsService>().AsImplementedInterfaces();
            builder.RegisterType<CompanyService>().AsImplementedInterfaces();
            builder.RegisterType<SafelyReportService>().AsImplementedInterfaces();
            builder.RegisterType<ImageLibraryService>().AsImplementedInterfaces();
            builder.RegisterType<TrackingMoneyAdvanceService>().AsImplementedInterfaces();
            //builder.RegisterType<NotificationService>().AsImplementedInterfaces();

        }
    }
}
