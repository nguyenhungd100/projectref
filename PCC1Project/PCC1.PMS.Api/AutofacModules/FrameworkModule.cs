﻿using Autofac;
using PCC1.PMS.Framework.Caching;

namespace PCC1.PMS.Api.AutofacModules
{
    /// <summary>
    /// 
    /// </summary>
    public class FrameworkModule : Module
    {
        private readonly string _strCnn;
        private readonly int db;
        private readonly string proxyServer;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="redisCnnStr"></param>
        /// <param name="redisDb"></param>
        public FrameworkModule(string redisCnnStr, int redisDb)
        {
            _strCnn = redisCnnStr;
            this.db = redisDb;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="builder"></param>
        protected override void Load(ContainerBuilder builder)
        {
            base.Load(builder);

            builder.Register(c => new RedisCacheManager(_strCnn, db)).As<IRedisCacheManager>().InstancePerLifetimeScope();
        }
    }
}