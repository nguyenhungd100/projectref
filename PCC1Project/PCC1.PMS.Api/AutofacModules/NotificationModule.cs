﻿using Autofac;
using PCC1.PMS.Domain.Services.Common;
using PCC1.PMS.Domain.Services.Common.Impelementation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PCC1.PMS.Api.AutofacModules
{
    public class NotificationModule : Module
    {
        private readonly string _firebaseUrl;

        public NotificationModule(string firebaseUrl)
        {
            _firebaseUrl = firebaseUrl;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="builder"></param>
        protected override void Load(ContainerBuilder builder)
        {
            base.Load(builder);

            builder.Register(c => new NotificationService(_firebaseUrl)).As<INotificationService>().InstancePerLifetimeScope();
        }
    }
}
