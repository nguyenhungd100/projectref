﻿using Autofac;
using PCC1.PMS.Domain.Services.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PCC1.PMS.Api.AutofacModules
{
    public class SendMailModule : Module
    {

        private readonly string _emailAddress;
        private readonly string _emailPass;

        public SendMailModule(string emailAddress, string emailPass)
        {
            _emailAddress = emailAddress;
            _emailPass = emailPass;
        }
       
        protected override void Load(ContainerBuilder builder)
        {
            base.Load(builder);

            builder.Register(c => new SendMailService(_emailAddress, _emailPass)).As<ISendMailService>().InstancePerLifetimeScope();
        }
    }
}
