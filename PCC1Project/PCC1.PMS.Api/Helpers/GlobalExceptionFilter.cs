﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.Filters;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using PCC1.PMS.Api.Modules;
using PCC1.PMS.Domain.Services;

namespace PCC1.PMS.Api.Helpers
{
    public class GlobalExceptionFilter : IExceptionFilter
    {
        public void OnException(ExceptionContext context)
        {
            var exceptionType = context.Exception.GetType();
            if (exceptionType == typeof(ServiceException))
            {
                string message = context.Exception.Message;
                var status = HttpStatusCode.OK;

                context.ExceptionHandled = true;
                HttpResponse response = context.HttpContext.Response;
                response.StatusCode = (int)status;
                response.ContentType = "application/json";
                var err = new { success = false, message };
                response.WriteAsync(JsonConvert.SerializeObject(err));
            }
            else
            {
                var status = HttpStatusCode.Accepted;
                context.ExceptionHandled = true;
                HttpResponse response = context.HttpContext.Response;
                response.StatusCode = (int)status;
                response.ContentType = "application/json";
                var err = new { success = false, message = context.Exception.Message };
                response.WriteAsync(JsonConvert.SerializeObject(err));
            }
        }
    }
}
