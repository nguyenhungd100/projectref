﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using PCC1.PMS.Api.Helpers;
using PCC1.PMS.Domain.Enums;
using PCC1.PMS.Domain.Models.File;
using PCC1.PMS.Domain.Models.MediaStorage;
using PCC1.PMS.Domain.Models.Notification;
using PCC1.PMS.Domain.Models.ProjectFolderDashboard;
using PCC1.PMS.Domain.Models.Users;
using PCC1.PMS.Domain.Services.Common;
using PCC1.PMS.Domain.Services.FileStorages;
using PCC1.PMS.Domain.Services.Projects;
using PCC1.PMS.Domain.Services.Roles;
using Swashbuckle.AspNetCore.Annotations;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace PCC1.PMS.Api.Modules.FileStorages
{
    [Route("api/v1/[controller]")]
    public class FileStorageController : Controller
    {
        private readonly IHashidService _hashidService;
        private readonly IFileStorageService _fileStorageService;
        private readonly IProjectService _projectService;

        public FileStorageController(IHashidService hashidService, IFileStorageService fileStorageService, IProjectService projectService)
        {
            _hashidService = hashidService;
            _fileStorageService = fileStorageService;
            _projectService = projectService;
        }

        /// <summary>
        /// Thêm hình ảnh công trình từ phía cán bộ quản lý dự án
        /// </summary>
        /// <param name="hashId"></param>
        /// <param name="mediaModel"></param>
        /// <returns></returns>
        [HttpPost("/api/v1/file-storage/project/{hashId}/add-images")]
        [Authorize]
        [SwaggerResponse((int)System.Net.HttpStatusCode.OK, Type = typeof(ResultBase<bool>))]
        public IActionResult AddProjectImages(string hashId, MediaModel mediaModel)
        {
            var userId = HttpContext.User.Identity.GetUserId();
            if (_projectService.CheckHasPermissionProject(hashId, userId, PermissionCode.UPDATE_PROJECT_IMAGE))
            {
                mediaModel.PictureType = ProjectPictureType.HinhAnhCongTrinhTuCBQLDA;
                mediaModel.ProjectId = _hashidService.Decode(hashId);
                var result = _fileStorageService.AddProjectImages(mediaModel);
            }
            else throw new Exception("Bạn không có quyền thực hiện tính năng này");
            
            return Json(new { success = true, mediaModel });
        }

        /// <summary>
        /// Thêm tài liệu cho dự án
        /// </summary>
        /// <param name="mediaModel"></param>
        /// <param name="hashId"></param>
        /// <returns></returns>
        [HttpPost("/api/v1/file-storage/project/{hashId}/add-documents")]
        [Authorize]
        [SwaggerResponse((int)System.Net.HttpStatusCode.OK, Type = typeof(ResultBase<bool>))]
        public IActionResult AddProjectDocuments(string hashId,MediaModel mediaModel)
        {
            mediaModel.ProjectId = _hashidService.Decode(hashId);
            var isArchives = false;
            var userId = 0;
            if (HttpContext.User.HasPermission(PermissionCode.UPLOAD_ARCHIVES))
            {
                isArchives = true;
                userId = HttpContext.User.Identity.GetUserId();
            } 
            var result = _fileStorageService.AddProjectDocuments(mediaModel, isArchives, userId);
            return Json(new { success = true, mediaModel });
        }

        /// <summary>
        /// B1 : Get list project picture by projectId
        /// </summary>
        /// <param name="hashId"></param>
        /// <returns></returns>
        [HttpGet("/api/v1/file-storage/project/{hashId}/get-pictures")]
        [Authorize]
        [SwaggerResponse((int)System.Net.HttpStatusCode.OK, Type = typeof(ResultBase<List<ProjectPictureModel>>))]
        public async Task<IActionResult> GetProjectPictures(string hashId)
        {
            var pid = _hashidService.Decode(hashId);
            var res = await _fileStorageService.GetProjectPictures(pid);
            return Json(new { success = true, data = res });
        }

        /// <summary>
        ///  B1 : Get list project document by projectId
        /// </summary>
        /// <param name="hashId"></param>
        /// <param name="folder"></param>
        /// <returns></returns>
        [HttpGet("/api/v1/file-storage/project/{hashId}/get-documents-in-folder")]
        [Authorize]
        [SwaggerResponse((int)System.Net.HttpStatusCode.OK, Type = typeof(ResultBase<List<ProjectDocumentModel>>))]
        public async Task<IActionResult> GetProjectDocuments(string hashId, Guid? folder)
        {
            var pid = _hashidService.Decode(hashId);
            var res = await _fileStorageService.GetProjectDocuments(pid, folder);
            return Json(new { success = true, data = res });
        }

        /// <summary>
        /// B2 : Get File by fileId
        /// </summary>
        /// <param name="fileId"></param>
        /// <returns></returns>
        [HttpGet("/api/v1/file-storage/get-file-by-id")]
        [Authorize]
        [SwaggerResponse((int)System.Net.HttpStatusCode.OK, Type = typeof(ResultBase<FileModel>))]
        public IActionResult GetFile(Guid fileId)
        {
            var res = _fileStorageService.GetFile(fileId);
            return Json(new { success = true, data = res });
        }

        /// <summary>
        /// B3 : Get binary file Data by storageId
        /// </summary>
        /// <param name="storageId"></param>
        /// <returns></returns>
        [HttpGet("/api/v1/file-storage/get-binary-file-storage-by-id")]
        [SwaggerResponse((int)System.Net.HttpStatusCode.OK, Type = typeof(ResultBase<BinaryFileStorageModel>))]
        public FileResult GetBinaryFileStorage(int storageId)
        {
            var res1 = _fileStorageService.GetBinaryFileStorage(storageId);
            var res2 = _fileStorageService.GetMimeAndName(storageId);
            return File(res1.Data, res2.Item1,res2.Item2);
        }

        /// <summary>
        /// Update User Avartar
        /// </summary>
        /// <param name="userImageModel"></param>
        /// <returns></returns>
        [Authorize]
        [HttpPost("/api/v1/file-storage/update-avatar-user")]
        [SwaggerResponse((int)System.Net.HttpStatusCode.OK, Type = typeof(ResultBase<>))]
        public async Task<ActionResult> UpdateImageAsync(MediaUserAvartarModel userImageModel)
        {
            var userId = HttpContext.User.Identity.GetUserId();
            userImageModel.UserId = userId;
            var result = await _fileStorageService.UpdateAvatarUserAsync(userImageModel);
            return Json(new { success = result });
        }

        [Authorize]
        [HttpGet("/api/v1/file-storage/find-images")]
        [SwaggerResponse((int)System.Net.HttpStatusCode.OK, Type = typeof(ResultBase<BinaryFileStorageModel>))]
        public IActionResult FindImages(string images)
        {
            var listImages = _fileStorageService.FindImages(images);
            return Json(new { success = true, data = listImages });
        }

        [HttpGet("/api/v1/file-storage/image-by-fileid")]
        public IActionResult ImageByFileId(Guid fileId)
        {
            var images = _fileStorageService.ImageByFileId(fileId);
            return File(images.Data, "image/jpeg");

        }

        /// <summary>
        /// Xóa tài liệu
        /// </summary>
        /// <param name="removeDocuments"></param>
        /// <param name="hashId"></param>
        /// <returns></returns>
        [HttpDelete("/api/v1/file-storage/remove-documents/{hashId}")]
        [Authorize]
        [SwaggerResponse((int)System.Net.HttpStatusCode.OK, Type = typeof(ResultBase<bool>))]
        public IActionResult RemoveDocuments([FromBody]IEnumerable<RemoveDocumentModel> removeDocuments, [FromRoute] string hashId)
        {
            var result = false;
            var userId = HttpContext.User.Identity.GetUserId();
            if (_projectService.CheckHasPermissionProject(hashId, userId, PermissionCode.DELETE_DOCUMENT))
            {
                removeDocuments.ToList().ForEach(r =>
                {
                    r.UserId = HttpContext.User.Identity.GetUserId();
                });
                result = _fileStorageService.RemoveDocuments(removeDocuments);
            }
            else throw new Exception("Bạn không có quyền thực hiện tính năng này");
            return Json(new { success = result });
        }

        /// <summary>
        /// Danh sách các thư mục của dự án(danh sách công văn)
        /// </summary>
        /// <returns></returns>
        [HttpGet("/api/v1/file-storage/list-project-folder-dash-broad")]
        [Authorize]
        [SwaggerResponse((int)System.Net.HttpStatusCode.OK, Type = typeof(ResultBase<ProjectFolderDashboardResultModel>))]
        public IActionResult ListProjectFolderDashboard()
        {
            var result = _fileStorageService.ListProjectFolderDashboard();
            return Json(new { success = true, data = result });
        }

        /// <summary>
        /// Upload công văn
        /// </summary>
        /// <param name="uploadArchives"></param>
        /// <returns></returns>
        [HttpPost("/api/v1/file-storage/upload-archives")]
        [Authorize]
        [SwaggerResponse((int)System.Net.HttpStatusCode.OK, Type = typeof(ResultBase<bool>))]
        public IActionResult UploadArchives(UploadArchivesModel uploadArchives)
        {
            var result = false;
            if (HttpContext.User.HasPermission(PermissionCode.UPLOAD_ARCHIVES))
            {
                if (!ModelState.IsValid) throw new Exception(ModelState.GetErrorsMessage());
                var userId = HttpContext.User.Identity.GetUserId();
                var projectId = _hashidService.Decode(uploadArchives.HashId);
                result = _fileStorageService.UploadArchives(uploadArchives, projectId, userId);
            }
            else throw new Exception("Bạn không có quyền thực hiện tính năng này");
            return Json(new { success = result });
        }
    }
}

