﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using PCC1.PMS.Domain.Models.ImageLibraries;
using PCC1.PMS.Domain.Services.ImageLibraries;
using Swashbuckle.AspNetCore.Annotations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PCC1.PMS.Api.Modules.ImageLibrary
{
    [Route("api/v1/[controller]")]
    public class ImageLibraryController : Controller
    {
        private readonly IImageLibraryService _imageLibraryService;

        public ImageLibraryController(IImageLibraryService imageLibraryService)
        {
            _imageLibraryService = imageLibraryService;
        }

        /// <summary>
        /// Lấy hình ảnh theo id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("/api/v1/imageLibrary/getById")]
        [SwaggerResponse((int)System.Net.HttpStatusCode.OK, Type = typeof(ResultBase<FileContentResult>))]
        public IActionResult GetImageLibraryById(int id)
        {
            var result = _imageLibraryService.ImageLibraryFindById(id);
            return File(result.Item1, result.Item2);
        }

        ///// <summary>
        ///// Thêm mới một hình ảnh
        ///// </summary>
        ///// <param name="file"></param>
        ///// <returns></returns>
        //[HttpPost("/api/v1/imageLibrary/add-a-imageLibrary")]
        //[Authorize]
        //[SwaggerResponse((int)System.Net.HttpStatusCode.OK, Type = typeof(ResultBase<bool>))]
        //public IActionResult AddAImageLibrary(IFormFile file)
        //{
        //    var result = _imageLibraryService.AddImageLibrary(file);
        //    return Json(new { success = result });
        //}
    }
}
