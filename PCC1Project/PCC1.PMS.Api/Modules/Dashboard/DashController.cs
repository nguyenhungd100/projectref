﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using PCC1.PMS.Api.Helpers;
using PCC1.PMS.Domain.Models.Projects;
using PCC1.PMS.Domain.Services.Projects;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace PCC1.PMS.Api.Modules.Dashboard
{
    [Route("api/[controller]")]
    public class DashController : Controller
    {
        private readonly IProjectService _projectService;

        public DashController(IProjectService projectService)
        {
            _projectService = projectService;
        }
        /// <summary>
        /// Get your projects
        /// </summary>
        /// <returns></returns>
        [HttpGet("/api/v1/project/get-your-projects")]
        [Authorize]
        [Swashbuckle.AspNetCore.Annotations.SwaggerResponse((int)HttpStatusCode.OK, Type = typeof(ResultBase<List<ProjectModel>>))]
        public ActionResult GetYourProjects()
        {
            var userId = HttpContext.User.Identity.GetUserId();
            if (HttpContext.User.HasPermission(Domain.Services.Roles.PermissionCode.ACCESS_ALL_PROJECTS))
            {
                var projects = _projectService.GetRuningProjects();
                return Json(new { success = true, data = projects, title = "Các dự án đang chạy" });
            }
            else
            {
                var projects = _projectService.GetYourProjects(userId);
                return Json(new { success = true, data = projects, title = "Dự án bạn đang tham gia" });
            }
        }
    }
}
