﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using PCC1.PMS.Api.Helpers;
using PCC1.PMS.Domain.Entity;
using PCC1.PMS.Domain.Models.TrackingMoneyAdvances; 
using PCC1.PMS.Domain.Services.Common;
using PCC1.PMS.Domain.Services.Projects;
using PCC1.PMS.Domain.Services.Roles;
using PCC1.PMS.Domain.Services.TrackingMoneyAdvances;
using Swashbuckle.AspNetCore.Annotations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PCC1.PMS.Api.Modules.TrackingMoneyAdvances
{
    [Route("/api/v1/[controller]")]
    public partial class TrackingMoneyAdvanceController : Controller
    {
        private readonly ITrackingMoneyAdvanceService _trackingMoneyAdvanceService;
        private readonly IProjectService _projectService;
        private readonly IHashidService _hashidService;

        public TrackingMoneyAdvanceController(ITrackingMoneyAdvanceService trackingMoneyAdvanceService,
            IProjectService projectService, IHashidService hashidService)
        {
            _trackingMoneyAdvanceService = trackingMoneyAdvanceService;
            _projectService = projectService;
            _hashidService = hashidService;
        }

        /// <summary>
        /// Tìm kiếm theo dõi tạm ứng
        /// </summary>
        /// <param name="search"></param>
        /// <returns></returns>
        [Authorize]
        [HttpPost("/api/v1/searh-tracking-money-advance")]
        [SwaggerResponse((int)System.Net.HttpStatusCode.OK, Type = typeof(ResultBase<SearchTrackingMoneyAdvanceResult>))]
        public IActionResult SearhTrackingMoneyAdvance([FromBody] SearchTrackingMoneyAdvanceModel search)
        {
            if (!ModelState.IsValid) throw new Exception(ModelState.GetErrorsMessage());
            var result = _trackingMoneyAdvanceService.SearhTrackingMoneyAdvance(search);
            return Json(new { success = true, data = result });
        }

        /// <summary>
        /// Thêm theo dõi thanh toán tạm ứng
        /// </summary>
        /// <param name="saveModel"></param>
        /// <returns></returns>
        [Authorize]
        [HttpPost("/api/v1/insert-tracking-money-advance")]
        [SwaggerResponse((int)System.Net.HttpStatusCode.OK, Type = typeof(ResultBase<bool>))]
        public IActionResult SaveTrackingMoneyAdvance([FromBody]SaveTrackingMoneyAdvanceModel saveModel)
        {
            var result = false;
            var userId = HttpContext.User.Identity.GetUserId();
            if (_projectService.CheckHasPermissionProject(saveModel.HashId, userId, PermissionCode.UPDATE_TRACKING_MONEY_ADVANCE))
            {
                if (!ModelState.IsValid) throw new Exception(ModelState.GetErrorsMessage());
                saveModel.CreatedBy = HttpContext.User.Identity.GetUserId();
                saveModel.ProjectId = _hashidService.Decode(saveModel.HashId);
                result = _trackingMoneyAdvanceService.InsertTrackingMoneyAdvance(saveModel);
            }
            else throw new Exception("Bạn không có quyền thực hiện tính năng này");
            return Json(new { success = result });
        }

        /// <summary>
        /// Sửa theo dõi thanh toán tạm ứng
        /// </summary>
        /// <param name="saveModel"></param>
        /// <returns></returns>
        [Authorize]
        [HttpPut("/api/v1/update-tracking-money-advance")]
        [SwaggerResponse((int)System.Net.HttpStatusCode.OK, Type = typeof(ResultBase<bool>))]
        public IActionResult UpdateTrackingMoneyAdvance([FromBody]SaveTrackingMoneyAdvanceModel saveModel)
        {
            var result = false;
            var userId = HttpContext.User.Identity.GetUserId();
            if (_projectService.CheckHasPermissionProject(saveModel.HashId, userId, PermissionCode.UPDATE_TRACKING_MONEY_ADVANCE))
            {
                if (!ModelState.IsValid) throw new Exception(ModelState.GetErrorsMessage());
                saveModel.CreatedBy = HttpContext.User.Identity.GetUserId();
                saveModel.ProjectId = _hashidService.Decode(saveModel.HashId);
                result = _trackingMoneyAdvanceService.UpdateTrackingMoneyAdvance(saveModel);
            }
            else throw new Exception("Bạn không có quyền thực hiện tính năng này");
            return Json(new { success = result });
        }

        /// <summary>
        /// Xóa theo dõi thanh toán tạm ứng
        /// </summary>
        /// <param name="deleteModels"></param>
        /// <returns></returns>
        [Authorize]
        [HttpDelete("/api/v1/delete-tracking-money-advance")]
        [SwaggerResponse((int)System.Net.HttpStatusCode.OK, Type = typeof(ResultBase<bool>))]
        public IActionResult DeleteTrackingMoneyAdvance([FromBody]List<DeleteTrackingMoneyAdvanceModel> deleteModels)
        {
            var result = false;
            if (!ModelState.IsValid) throw new Exception(ModelState.GetErrorsMessage());
            var hashId = deleteModels[0].HashId;
            var userId = HttpContext.User.Identity.GetUserId();
            if (_projectService.CheckHasPermissionProject(hashId, userId, PermissionCode.UPDATE_TRACKING_MONEY_ADVANCE))
            {             
                deleteModels.ForEach(c => { c.ProjectId = _hashidService.Decode(c.HashId); });
                result = _trackingMoneyAdvanceService.DeleteTrackingMoneyAdvance(deleteModels);
            }
            else throw new Exception("Bạn không có quyền thực hiện tính năng này");
            return Json(new { success = result });
        }

        /// <summary>
        /// Gửi notify theo dõi tiền tạm ứng kể từ sau 7 ngày kí hợp đồng từ các công ty
        /// </summary>
        /// <returns></returns>
        [Authorize]
        [HttpGet("/api/v1/send-notify-tracking-advance-money")]
        [SwaggerResponse((int)System.Net.HttpStatusCode.OK, Type = typeof(ResultBase<bool>))]
        public IActionResult SendNotifyTrackingAvanceMoney()
        {
            var result = _trackingMoneyAdvanceService.SendNotifyTrackingAvanceMoney();
            return Json(new { success = result });
        }
    }
}
