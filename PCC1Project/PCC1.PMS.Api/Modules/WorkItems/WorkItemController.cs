﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using PCC1.PMS.Api.Helpers;
using PCC1.PMS.Domain.Enums;
using PCC1.PMS.Domain.Models.EstimationPlans;
using PCC1.PMS.Domain.Models.Projects;
using PCC1.PMS.Domain.Models.Revenues;
using PCC1.PMS.Domain.Models.Statistical;
using PCC1.PMS.Domain.Models.Statistical.StatisticalActuals;
using PCC1.PMS.Domain.Models.WorkitemByCompany;
using PCC1.PMS.Domain.Models.Workitems;
using PCC1.PMS.Domain.Models.Workitems.AccumulateCompleteQuarter;
using PCC1.PMS.Domain.Services.Common;
using PCC1.PMS.Domain.Services.Projects;
using PCC1.PMS.Domain.Services.Roles;
using PCC1.PMS.Domain.Services.WorkItems;
using PCC1.PMS.Framework.Utils;
using Swashbuckle.AspNetCore.Annotations;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace PCC1.PMS.Api.Modules.WorkItems
{
    [Route("api/[controller]")]
    public class WorkitemController : Controller
    {
        private readonly IWorkItemService _workItemService;
        private readonly INotificationService _notificationService;
        private readonly IHashidService _hashidService;
        private readonly IProjectService _projectService;

        public WorkitemController(IWorkItemService workItemService,
            INotificationService notificationService,
            IHashidService hashidService,
            IProjectService projectService)
        {
            this._workItemService = workItemService;
            _notificationService = notificationService;
            _hashidService = hashidService;
            _projectService = projectService;
        }

        /// <summary>
        /// Danh sách phân giao hạng mục theo công ty trong dự án
        /// </summary>
        /// <param name="companyId"></param>
        /// <param name="hashid"></param>
        /// <returns></returns>
        [Authorize]
        [HttpGet("/api/v1/workitem/list-by-companyId/{hashid}")]
        [SwaggerResponse((int)System.Net.HttpStatusCode.OK, Type = typeof(ResultBase<ListWorkitemByCompanyModel>))]
        public IActionResult ListWorkitemByCompanyId(int companyId, string hashid)
        {
            var projectId = _hashidService.Decode(hashid);
            var result = _workItemService.ListWorkitemByCompanyId(companyId, projectId);
            return Json(new { success = true, data = result });
        }

        /// <summary>
        /// Danh sách tất cả các hạng mục
        /// </summary>
        /// <returns></returns>
        [Authorize]
        [HttpGet("/api/v1/workitem/list-all")]
        [SwaggerResponse((int)System.Net.HttpStatusCode.OK, Type = typeof(ResultBase<WorkItemModel>))]
        public async Task<IActionResult> ListWorkItems()
        {
            if (!ModelState.IsValid) throw new Exception(ModelState.GetErrorsMessage());
            var res = await _workItemService.ListWorkitem();
            return Json(new { success = true, data = res });
        }

        /// <summary>
        /// Danh sách tất cả các hạng mục theo dự án
        /// </summary>
        /// <param name="hashId"></param>
        /// <returns></returns>
        [Authorize]
        [HttpGet("/api/v1/project/{hashId}/workitems")]
        [SwaggerResponse((int)System.Net.HttpStatusCode.OK, Type = typeof(ResultBase<List<WorkitemInfoModel>>))]
        public IActionResult GetWorkitems(string hashId)
        {

            if (!ModelState.IsValid) throw new Exception(ModelState.GetErrorsMessage());
            var projectId = _hashidService.Decode(hashId);
            var res = _workItemService.GetProjectWorkitems(projectId);
            return Json(new { success = true, data = res });
        }

        ///// <summary>
        ///// Danh sách tất cả hạng mục cha theo dự án
        ///// </summary>
        ///// <param name="hashid"></param>
        ///// <returns></returns>
        //[Authorize]
        //[HttpGet("/api/v1/project/{hashid}/list-parent-workitems")]
        //[SwaggerResponse((int)System.Net.HttpStatusCode.OK, Type = typeof(ResultBase<List<WorkItemModel>>))]
        //public IActionResult GetListParentWorkitems(string hashid)
        //{
        //    if (!ModelState.IsValid) throw new Exception(ModelState.GetErrorsMessage());
        //    var projectId = _hashidService.Decode(hashid);
        //    var res = _workItemService.GetListParentWorkitems(projectId);
        //    return Json(new { success = true, data = res });
        //}

        /// <summary>
        /// Danh sách các hạng con theo hạng mục cha
        /// </summary>
        /// <param name="workitemGroupId"></param>
        /// <returns></returns>
        [Authorize]
        [HttpGet("/api/v1/workitem/{workitemGroupId}/list-children")]
        [SwaggerResponse((int)System.Net.HttpStatusCode.OK, Type = typeof(ResultBase<List<WorkItemModel>>))]
        public IActionResult GetListChildrenWorkitems(int workitemGroupId)
        {
            var res = _workItemService.GetListChildrenWorkitems(workitemGroupId);
            return Json(new { success = true, data = res });

        }

        /// <summary>
        /// Bảng phân giao khối lượng cho các công ty trong dự án
        /// </summary>
        /// <param name="hashId"></param>
        /// <param name="checkCompanyId"></param>
        /// <returns></returns>
        [Authorize]
        [HttpGet("/api/v1/project/{hashId}/assignments")]
        [HttpGet("/api/v1/project/{hashId}/assignments/{checkCompanyId}")]
        [SwaggerResponse((int)System.Net.HttpStatusCode.OK, Type = typeof(ResultBase<List<WorkitemAssignmentWithHeading>>))]
        public IActionResult ListWorkitemAssignments(string hashId, int? checkCompanyId)
        {

            if (!ModelState.IsValid) throw new Exception(ModelState.GetErrorsMessage());
            var projectId = _hashidService.Decode(hashId);
            var res = _workItemService.GetWorkitemAssignments(projectId, checkCompanyId);
            return Json(new { success = true, data = res });
        }

        /// <summary>
        /// Dowload Bảng phân giao khối lượng cho các công ty trong dự án
        /// </summary>
        /// <param name="hashId"></param>
        /// <param name="checkCompanyId"></param>
        /// <returns></returns>
        [HttpGet("/api/v1/project/{hashId}/dowload-excel-assignments")]
        [HttpGet("/api/v1/project/{hashId}/dowload-excel-assignments/{checkCompanyId}")]
        [SwaggerResponse((int)System.Net.HttpStatusCode.OK, Type = typeof(ResultBase<List<WorkitemAssignmentWithHeading>>))]
        public IActionResult DowloadExcelAssignments(string hashId, int? checkCompanyId)
        {

            if (!ModelState.IsValid) throw new Exception(ModelState.GetErrorsMessage());
            var projectId = _hashidService.Decode(hashId);
            var res = _workItemService.DowloadExcelAssignments(projectId, checkCompanyId);
            var contentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
            return File(res.Item1, contentType, "Bảng phân giao khối lượng cho các công ty trong dự án " + res.Item2 + ".xlsx");
        }




        /// <summary>
        /// Thông tin hạng mục theo Id
        /// </summary>
        /// <param name="workItemId"></param>
        /// <returns></returns>
        [Authorize]
        [HttpGet("/api/v1/workitem/get-by-id")]
        [SwaggerResponse((int)System.Net.HttpStatusCode.OK, Type = typeof(ResultBase<WorkItemModel>))]
        public async Task<IActionResult> WorkItemById(int workItemId)
        {
            if (!ModelState.IsValid) throw new Exception(ModelState.GetErrorsMessage());
            var res = await _workItemService.WorkItemById(workItemId);
            return Json(new { success = true, data = res });
        }

        /// <summary>
        /// Thêm mới hạng mục
        /// </summary>
        /// <param name="workItemAddModel"></param>
        /// <param name="hashId"></param>
        /// <returns></returns>
        [Authorize]
        [HttpPost("/api/v1/workitem/add/{hashId}")]
        [SwaggerResponse((int)System.Net.HttpStatusCode.OK, Type = typeof(ResultBase<>))]
        public async Task<IActionResult> AddWorkItem([FromBody] AddWorkitemModel workItemAddModel, [FromRoute] string hashId)
        {
            if (!ModelState.IsValid) throw new Exception(ModelState.GetErrorsMessage());
            var userId = HttpContext.User.Identity.GetUserId();
            if (_projectService.CheckHasPermissionProject(hashId, userId, PermissionCode.UPDATE_WORKITEM))
            {
                workItemAddModel.ProjectId = _hashidService.Decode(workItemAddModel.HashId);
                var res = await _workItemService.AddWorkItem(workItemAddModel, userId);
            }
            else throw new Exception("Bạn không có quyền thực hiện tính năng này");
            return Json(new { success = true });
        }

        /// <summary>
        /// Cập nhật hạng mục bằng file excel
        /// </summary>
        /// <param name="formFile"></param>
        /// <param name="hashId"></param>
        /// <returns></returns>
        [Authorize]
        [HttpPost("/api/v1/workitem/project/{hashId}/import-excel")]
        [SwaggerResponse((int)System.Net.HttpStatusCode.OK, Type = typeof(ResultBase<bool>))]
        public IActionResult ImportWorkItems(IFormFile formFile, string hashId)
        {
            var res = false;
            var userId = HttpContext.User.Identity.GetUserId();
            if (_projectService.CheckHasPermissionProject(hashId, userId, PermissionCode.UPDATE_WORKITEM))
            {
                if (!ModelState.IsValid) throw new Exception(ModelState.GetErrorsMessage());
                var projectId = _hashidService.Decode(hashId);
                res = _workItemService.ImportExcelWorkItems(formFile, projectId, userId);
            }
            else throw new Exception("Bạn không có quyền thực hiện tính năng này");
            return Json(new { success = res });
        }


        /// <summary>
        /// Cập nhật hạng mục
        /// </summary>
        /// <param name="workItemModel"></param>
        /// <returns></returns>
        [Authorize]
        [HttpPut("/api/v1/workitem/update")]
        [SwaggerResponse((int)System.Net.HttpStatusCode.OK, Type = typeof(ResultBase<>))]
        public IActionResult UpdateWorkItem([FromBody] WorkItemModel workItemModel)
        {
            var res = false;
            var userId = HttpContext.User.Identity.GetUserId();
            if (_projectService.CheckHasPermissionProject(workItemModel.HashId, userId, PermissionCode.UPDATE_WORKITEM))
            {
                if (!ModelState.IsValid) throw new Exception(ModelState.GetErrorsMessage());
                workItemModel.ProjectId = _hashidService.Decode(workItemModel.HashId);
                userId = HttpContext.User.Identity.GetUserId();
                res = _workItemService.UpdateWorkItem(workItemModel, userId);

            }
            else throw new Exception("Bạn không có quyền thực hiện tính năng này");
            return Json(new { success = res });
        }

        /// <summary>
        /// Xóa hạng mục
        /// </summary>
        /// <param name="workItemDeleteModel"></param>
        /// <returns></returns>
        [Authorize(Policy = nameof(PermissionCode.UPDATE_WORKITEM))]
        [HttpDelete("/api/v1/workitem/delete")]
        [SwaggerResponse((int)System.Net.HttpStatusCode.OK, Type = typeof(ResultBase<bool>))]
        public IActionResult DeleteWorkItem([FromBody] DeleteWorkitemModel workItemDeleteModel)
        {
            if (!ModelState.IsValid) throw new Exception(ModelState.GetErrorsMessage());
            var userId = HttpContext.User.Identity.GetUserId();
            var res = _workItemService.DeleteWorkItem(workItemDeleteModel, userId);
            return Json(new { success = res });
        }

        /// <summary>
        /// Hiển thị thanh toán 1 hạng mục
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [Authorize]
        [HttpGet("/api/v1/workitem/view-pay-one/{id}")]
        [SwaggerResponse((int)System.Net.HttpStatusCode.OK, Type = typeof(ResultBase<SettlementWorkitemModel>))]
        public async Task<IActionResult> ViewPayOneWorkItem(int id)
        {
            if (!ModelState.IsValid) throw new Exception(ModelState.GetErrorsMessage());
            var result = await _workItemService.ViewPayOneWorkItem(id);
            return Json(new { success = result });
        }

        /// <summary>
        /// Hiển thị thanh toán hạng mục theo lần
        /// </summary>
        /// <param name="hashId"></param>
        /// <returns></returns>
        [Authorize]
        [HttpGet("/api/v1/project/{hashId}/view-settlements-on-times")]
        [SwaggerResponse((int)System.Net.HttpStatusCode.OK, Type = typeof(ResultBase<SettlementViewModel>))]
        public IActionResult GetProjectSettlements(string hashId)
        {
            if (!ModelState.IsValid) throw new Exception(ModelState.GetErrorsMessage());
            var projectId = _hashidService.Decode(hashId);
            var result = _workItemService.ViewPayAllWorkItem(projectId);
            return Json(new { success = true, data = result });
        }

        /// <summary>
        /// Thanh toán nhiều hạng mục theo lần
        /// </summary>
        /// <param name="settlementModels"></param>
        /// <param name="hashId"></param>
        /// <returns></returns>
        [Authorize]
        [HttpPost("/api/v1/workitem/performing-settlement-on-times/{hashId}")]
        [SwaggerResponse((int)System.Net.HttpStatusCode.OK, Type = typeof(ResultBase<bool>))]
        public IActionResult SettlementWorkitem([FromBody]IEnumerable<PerformingSettlementModel> settlementModels, [FromRoute] string hashId)
        {
            var result = false;
            var userId = HttpContext.User.Identity.GetUserId();
            if (_projectService.CheckHasPermissionProject(hashId, userId, PermissionCode.UPDATE_PROJECT_SETTLEMENT))
            {
                if (!ModelState.IsValid) throw new Exception(ModelState.GetErrorsMessage());
                settlementModels.ToList().ForEach(c =>
                {
                    c.PerformingUser = HttpContext.User.Identity.GetUserId();
                    c.Type = SettlementType.SettlementTimes;
                });
                result = _workItemService.SettlementWorkitem(settlementModels);
            }
            else throw new Exception("Bạn không có quyền thực hiện tính năng này");
            return Json(new { success = result });
        }

        /// <summary>
        /// Hiển thị sản lượng trong dự án theo tháng và kế hoạch dự kiến tháng tới( nếu có)
        /// </summary>
        /// <param name="month"></param>
        /// <param name="hashid"></param>
        /// <param name="year"></param>
        /// <returns></returns>     
        [HttpGet("/api/v1/project/{hashid}/{year}/{month}/check-workitem-completed")]
        [Authorize]
        [SwaggerResponse((int)System.Net.HttpStatusCode.OK, Type = typeof(ResultBase<YieldInMonthModel>))]
        public IActionResult CheckCompletedNumberPerMonth(string hashid, int month, int year)
        {
            if (!ModelState.IsValid) throw new Exception(ModelState.GetErrorsMessage());
            var checkWorkitem = new CheckCompletedPerMonthModel();
            checkWorkitem.ProjectId = _hashidService.Decode(hashid);
            checkWorkitem.Month = month;
            checkWorkitem.Year = year;
            var result = _workItemService.CheckCompletedNumberPerMonth(checkWorkitem);
            return Json(new { success = true, data = result });
        }


        /// <summary>
        /// Tải file excel sản lượng tháng và kế hoạch dự kiến tháng tới
        /// </summary>
        /// <param name="hashid"></param>
        /// <param name="month"></param>
        /// <param name="year"></param>
        /// <returns></returns>
        [HttpGet("/api/v1/project/{hashid}/{year}/{month}/export-workitem-completed-to-excel")]
        [SwaggerResponse((int)System.Net.HttpStatusCode.OK, Type = typeof(ResultBase<IFormFile>))]
        public FileStreamResult ExportCompletedNumberMonthToExcel(string hashid, int month, int year)
        {
            var checkWorkitem = new CheckCompletedPerMonthModel();
            checkWorkitem.ProjectId = _hashidService.Decode(hashid);
            checkWorkitem.Month = month;
            checkWorkitem.Year = year;
            var stream = _workItemService.ExportCompletedNumberMonthToExcel(checkWorkitem);
            var contentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
            stream.Position = 0;
            return File(stream, contentType, "Sản lượng hoàn thành tháng " + month + "-" + year + ".xlsx");
        }

        /// <summary>
        /// Hiển thị sản lượng trong dự án theo quý và kế hoạch dự kiến quý tới( nếu có)
        /// </summary>
        /// <param name="quarter"></param>
        /// <param name="hashid"></param>
        /// <param name="year"></param>
        /// <returns></returns>     
        [HttpGet("/api/v1/project/{hashid}/{year}/{quarter}/estimate-workitem-complete-with-quarter")]
        [Authorize]
        [SwaggerResponse((int)System.Net.HttpStatusCode.OK, Type = typeof(ResultBase<YieldInQuarterModel>))]
        public IActionResult EstimateWorkitemCompleteQuarter(string hashid, int quarter, int year)
        {
            if (!ModelState.IsValid) throw new Exception(ModelState.GetErrorsMessage());
            var checkWorkitem = new CheckCompletedPerQuarterModel();
            checkWorkitem.ProjectId = _hashidService.Decode(hashid);
            checkWorkitem.Quarter = quarter;
            checkWorkitem.Year = year;
            var result = _workItemService.EstimateWorkitemCompleteQuarter(checkWorkitem);
            return Json(new { success = true, data = result });
        }

        /// <summary>
        /// Tải file excel sản lượng quý và kế hoạch dự kiến quý tới
        /// </summary>
        /// <param name="hashid"></param>
        /// <param name="quarter"></param>
        /// <param name="year"></param>
        /// <returns></returns>
        [HttpGet("/api/v1/project/{hashid}/{year}/{quarter}/export-quarter-completed-to-excel")]
        [SwaggerResponse((int)System.Net.HttpStatusCode.OK, Type = typeof(ResultBase<IFormFile>))]
        public FileStreamResult ExportCompletedNumberQuarterToExcel(string hashid, int quarter, int year)
        {
            var checkWorkitem = new CheckCompletedPerQuarterModel();
            checkWorkitem.ProjectId = _hashidService.Decode(hashid);
            checkWorkitem.Quarter = quarter;
            checkWorkitem.Year = year;
            var stream = _workItemService.ExportCompletedNumberQuarterToExcel(checkWorkitem);
            var contentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
            stream.Position = 0;
            return File(stream, contentType, "Sản lượng hoàn thành quý " + quarter + "-" + year + ".xlsx");
        }

        /// <summary>
        /// Lên kế hoạch sản lượng theo tháng 
        /// </summary>
        /// <param name="model"></param>
        /// <param name="hashId"></param>
        /// <returns></returns>
        [Authorize]
        [HttpPost("/api/v1/misc/estimate-monthly-output/{hashId}")]
        [SwaggerResponse((int)System.Net.HttpStatusCode.OK, Type = typeof(ResultBase<bool>))]
        public IActionResult EstimationCompleteForMonth([FromBody]List<EstimationModel> model, [FromRoute] string hashId)
        {
            var result = false;
            var userId = HttpContext.User.Identity.GetUserId();
            if (_projectService.CheckHasPermissionProject(hashId, userId, PermissionCode.ESTIMATE_WORKITEM_COMPLETED_IN_MONTH))
            {
                result = _workItemService.EstimateMonthlyOutput(model);
            }
            else throw new Exception("Bạn không có quyền thực hiện tính năng này");
            return Json(new { success = result });
        }

        /// <summary>
        /// Lên kế hoạch sản lượng theo quý
        /// </summary>
        /// <param name="model"></param>
        /// <param name="hashId"></param>
        /// <returns></returns>
        [Authorize]
        [HttpPost("/api/v1/misc/estimate-quarter-output/{hashId}")]
        [SwaggerResponse((int)System.Net.HttpStatusCode.OK, Type = typeof(ResultBase<bool>))]
        public IActionResult EstimationCompleteForQuarter([FromBody]List<EstimationModel> model, [FromRoute] string hashId)
        {
            var result = false;
            var userId = HttpContext.User.Identity.GetUserId();
            if (_projectService.CheckHasPermissionProject(hashId, userId, PermissionCode.ESTIMATE_WORKITEM_COMPLETED_IN_QUARTER))
            {
                result = _workItemService.EstimationCompleteForQuarter(model);
            }
            else throw new Exception("Bạn không có quyền thực hiện tính năng này");
            return Json(new { success = result });
        }

        /// <summary>
        /// Hiển thị danh sách thanh toán hạng mục theo quý và kế hoạch ước lượng thanh toán quý tiếp theo(nếu có)
        /// </summary>
        /// <param name="hashid"></param>
        /// <param name="year"></param>
        /// <param name="quarter"></param>
        /// <returns></returns>
        [HttpGet("/api/v1/project/{hashid}/{year}/{quarter}/settlement-plan-with-quarterly")]
        [Authorize]
        [SwaggerResponse((int)System.Net.HttpStatusCode.OK, Type = typeof(ResultBase<IEnumerable<WorkitemSettlementPerQuarterModel>>))]
        public IActionResult ShowSettlementPlanWithQuarterly(string hashid, int year, int quarter)
        {
            var projectId = _hashidService.Decode(hashid);
            var result = _workItemService.ShowSettlementPlanWithQuarterly(projectId, year, quarter);
            return Json(new { success = true, data = result });
        }

        /// <summary>
        /// Tải file excel báo cáo thanh toán theo quý và kế hoạch dự kiến thanh toán quý tới
        /// </summary>
        /// <param name="hashid"></param>
        /// <param name="year"></param>
        /// <param name="quarter"></param>
        /// <returns></returns>
        [HttpGet("/api/v1/project/{hashid}/{year}/{quarter}/export-settlement-to-excel")]
        [SwaggerResponse((int)System.Net.HttpStatusCode.OK, Type = typeof(ResultBase<IFormFile>))]
        public IActionResult ExportSettlementPlanWithQuarterlyToExcel(string hashid, int year, int quarter)
        {
            var projectId = _hashidService.Decode(hashid);
            var result = _workItemService.ExportSettlementPlanWithQuarterlyToExcel(projectId, year, quarter);
            return File(result, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
        }


        ///// <summary>
        ///// Lên kế hoạch hoàn thành thanh toán theo quý
        ///// </summary>
        ///// <param name="quarterEstimations"></param>
        ///// <returns></returns>
        //[Authorize(Policy = nameof(PermissionCode.ESTIMATE_SETTLEMENT_QUARTER))]
        //[HttpPost("/api/v1/estimation/add-for-quarter-settlement")]
        //[SwaggerResponse((int)System.Net.HttpStatusCode.OK, Type = typeof(ResultBase<bool>))]
        //public IActionResult EstimationSettlementForQuarter([FromBody]IEnumerable<EstimationModel> quarterEstimations)
        //{
        //    var result = _workItemService.EstimationSettlementForQuarter(quarterEstimations);


        //    return Json(new { success = result });
        //}


        /// <summary>
        /// Dowload file excel mẫu khối lương hạng mục
        /// </summary>
        /// <returns></returns>
        [HttpGet("/api/v1/workitem/dowload-sample-file")]
        [SwaggerResponse((int)System.Net.HttpStatusCode.OK, Type = typeof(ResultBase<bool>))]
        public IActionResult DowloadSampleFile()
        {
            var filePath = @"C:\PCC1-Temp\Khối lượng mẫu.xlsx";
            var fileName = "Khối lượng mẫu.xlsx";
            var mimeType = "application/vnd.ms-excel";
            return File(new FileStream(filePath, FileMode.Open), mimeType, fileName);
        }

        /// <summary>
        /// Phân giao hạng mục cho nhiều công ty
        /// </summary>
        /// <param name="model"></param>
        /// <param name="hashId"></param>
        /// <returns></returns>
        [HttpPost("/api/v1/project/{hashId}/assignments/update")]
        [Authorize]
        [SwaggerResponse((int)System.Net.HttpStatusCode.OK, Type = typeof(ResultBase<bool>))]
        public IActionResult UpdateAssignments([FromBody] List<AssignmentUpdateModel> model, [FromRoute] string hashId)
        {
            var result = false;
            var userId = HttpContext.User.Identity.GetUserId();
            if (_projectService.CheckHasPermissionProject(hashId, userId, PermissionCode.ASSiGNMENT_WORKITEM))
            {
                if (!ModelState.IsValid) throw new Exception(ModelState.GetErrorsMessage());
                result = _workItemService.UpdateAssignments(model);
            }
            else throw new Exception("Bạn không có quyền sử dụng tính năng này");
            return Json(new { success = result });
        }

        [HttpGet("/api/v1/testroman")]
        public IActionResult TestRoman(string input)
        {
            return (HeadingExcels.IsHeading2(input)) ? Json(true) : Json(false);
        }



        /// <summary>
        /// Chia hạng mục theo danh mục tiêu đề
        /// </summary>
        /// <returns></returns>
        [HttpGet("/api/v1/divide-workitem-by-category/{hashid}")]
        [SwaggerResponse((int)System.Net.HttpStatusCode.OK, Type = typeof(ResultBase<IEnumerable<WorkitemWithHeadingModel>>))]
        public IActionResult DivideWorkitem(string hashid)
        {
            var projectId = _hashidService.Decode(hashid);
            var result = _workItemService.DivideWorkitem(projectId);
            return Json(new { success = true, data = result });
        }


        /// <summary>
        /// Danh sách doanh thu dự kiến theo quý
        /// </summary>
        /// <returns></returns>
        [HttpGet("/api/v1/list-revenue-estimate/{hashid}/{year}")]
        [SwaggerResponse((int)System.Net.HttpStatusCode.OK, Type = typeof(ResultBase<RevenueEstimateResultModel>))]
        public IActionResult GetRevenueEstimate(string hashid, int year)
        {
            var projectId = _hashidService.Decode(hashid);
            var result = _workItemService.GetRevenueEstimate(projectId, year);
            return Json(new { success = true, data = result });
        }

        /// <summary>
        /// Xuất excel thanh toán theo lần
        /// </summary>
        /// <param name="hashId"></param>
        /// <returns></returns>
        [Authorize]
        [HttpGet("/api/v1/")]
        [SwaggerResponse((int)System.Net.HttpStatusCode.OK, Type = typeof(ResultBase<SettlementRevenueResultModel>))]
        public IActionResult ExportEstimateRevenueToExcel(string hashId)
        {
            var result = new MemoryStream();
            if (!ModelState.IsValid) throw new Exception(ModelState.GetErrorsMessage());
            var projectId = _hashidService.Decode(hashId);
            result = _workItemService.ExportSettlementWithTimeToExcel(projectId);
            return Json(new { success = true, data = result });
        }

        /// <summary>
        /// Cập nhật ước lượng doanh thu dự kiến theo quý
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [Authorize]
        [HttpPost("/api/v1/update-workitem-revenue")]
        [SwaggerResponse((int)System.Net.HttpStatusCode.OK, Type = typeof(ResultBase<bool>))]
        public IActionResult UpdateWorkItemRevenueEstimate([FromBody]UpdateWorkItemRevenueModel model)
        {
            var result = false;
            var userId = HttpContext.User.Identity.GetUserId();           
            if (_projectService.CheckHasPermissionProject(model.HashId, userId, PermissionCode.UPDATE_REVENUE_ESTIMATE))
            {
                if (!ModelState.IsValid) throw new Exception(ModelState.GetErrorsMessage());
                model.ProjectId = _hashidService.Decode(model.HashId);
                result = _workItemService.UpdateWorkItemRevenueEstimate(model);
            }            
            else throw new Exception("Bạn không có quyền thực hiện tính năng này");
            return Json(new { success = result });
        }



        /// <summary>
        /// Xuất excel kế hoạch thanh toán theo quý
        /// </summary>
        /// <param name="hashId"></param>
        /// <param name="year"></param>
        /// <returns></returns>
        [HttpGet("/api/v1/export-settlement-quarter-to-excel")]
        [SwaggerResponse((int)System.Net.HttpStatusCode.OK, Type = typeof(ResultBase<SettlementRevenueResultModel>))]
        public IActionResult ExportEstimateRevenueQuarterToExcel(string hashId, int year)
        {
            var result = new MemoryStream();
            if (!ModelState.IsValid) throw new Exception(ModelState.GetErrorsMessage());
            var projectId = _hashidService.Decode(hashId);
            result = _workItemService.ExportEstimateRevenueQuarterToExcel(projectId, year);
            var contentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
            result.Position = 0;
            return File(result, contentType, "Doanh thu dự kiến năm " + year + ".xlsx");

        }


        /// <summary>
        /// Cập nhật thanh toán hạng mục theo lần
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [Authorize]
        [HttpPost("/api/v1/update-workitem-settlement")]
        [SwaggerResponse((int)System.Net.HttpStatusCode.OK, Type = typeof(ResultBase<bool>))]
        public IActionResult SettlementWorkitemWithTimes([FromBody] UpdateWorkitemSettlementModel model)
        {
            var result = false;         
            var userId = HttpContext.User.Identity.GetUserId();
            if (_projectService.CheckHasPermissionProject(model.Hashid, userId, PermissionCode.UPDATE_SETTLEMENT_WITH_TIME))
            {
                if (!ModelState.IsValid) throw new Exception(ModelState.GetErrorsMessage());
                model.ProjectId = _hashidService.Decode(model.Hashid);
                result = _workItemService.SettlementWorkitemWithTimes(model);
            }
            else throw new Exception("Bạn không có quyền thực hiện tính năng này");
            return Json(new { success = result });
        }

        /// <summary>
        /// List theo dõi doanh thu theo lần thanh toán
        /// </summary>
        /// <param name="hashId"></param>
        /// <returns></returns>
        [Authorize]
        [HttpGet("/api/v1/list-workitem-settlement/{hashId}")]
        [SwaggerResponse((int)System.Net.HttpStatusCode.OK, Type = typeof(ResultBase<SettlementRevenueResultModel>))]
        public IActionResult ListSettlementWorkItemByTime(string hashId)
        {
            var result = new SettlementRevenueResultModel();
            if (!ModelState.IsValid) throw new Exception(ModelState.GetErrorsMessage());
            var projectId = _hashidService.Decode(hashId);
            result = _workItemService.ListSettlementWorkItemByTime(projectId);
            return Json(new { success = true, data = result });
        }

        /// <summary>
        /// Xuất excel thanh toán theo lần
        /// </summary>
        /// <param name="hashId"></param>
        /// <returns></returns>
        [HttpGet("/api/v1/export-settlement-with-times-to-excel")]
        [SwaggerResponse((int)System.Net.HttpStatusCode.OK, Type = typeof(ResultBase<SettlementRevenueResultModel>))]
        public IActionResult ExportSettlementWirthTimeToExcel(string hashId)
        {
            var result = new MemoryStream();
            if (!ModelState.IsValid) throw new Exception(ModelState.GetErrorsMessage());
            var projectId = _hashidService.Decode(hashId);
            result = _workItemService.ExportSettlementWithTimeToExcel(projectId);
            var contentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
            return File(result, contentType, "Thanh toán theo lần.xlsx");
        }

        /// <summary>
        /// Danh sách fill ước lượng sang thanh toán
        /// </summary>
        /// <param name="fillEstimate"></param>
        /// <returns></returns>
        [Authorize]
        [HttpPost("/api/v1/fill-estimate-to-settlement")]
        [SwaggerResponse((int)System.Net.HttpStatusCode.OK, Type = typeof(ResultBase<List<FillEstimateToSettlementResult>>))]
        public IActionResult FillEstimateToSettlement([FromBody] FillEstimateToSettlementModel fillEstimate)
        {
            if (!ModelState.IsValid) throw new Exception(ModelState.GetErrorsMessage());
            fillEstimate.ProjectId = _hashidService.Decode(fillEstimate.HashId);
            var result = _workItemService.FillEstimateToSettlement(fillEstimate);
            return Json(new { success = true, data = result });
        }

        /// <summary>
        /// Tổng hợp sản lượng của một dự án
        /// </summary>
        /// <param name="hashid"></param>
        /// <param name="checkStatistical"></param>
        /// <returns></returns>
        [HttpPost("/api/v1/statistical-yield-one-project/{hashid}")]
        [Authorize]
        [SwaggerResponse((int)System.Net.HttpStatusCode.OK, Type = typeof(ResultBase<StatisticalYieldOneProject>))]
        public IActionResult StatisticalYieldOneProject([FromRoute]string hashid, [FromBody]CheckStatisticalWithTime checkStatistical)
        {
            var projectId = _hashidService.Decode(hashid);
            var result = _workItemService.StatisticalYieldOneProject(projectId, checkStatistical);
            return Json(new { success = true, data = result });
        }

        /// <summary>
        /// Xuất excel tổng hợp sản lượng của một dự án
        /// </summary>
        /// <param name="hashId"></param>
        /// <param name="startDate"></param>
        /// <param name="endDate"></param>
        /// <returns></returns>
        [HttpGet("/api/v1/export-statistical-yield-one-projects-to-excel/{hashId}")]
        [SwaggerResponse((int)System.Net.HttpStatusCode.OK, Type = typeof(ResultBase<Stream>))]
        public IActionResult ExportStatisticalYieldAProjectToExcel(string hashId, DateTime? startDate, DateTime? endDate)
        {
            if (!ModelState.IsValid) throw new Exception(ModelState.GetErrorsMessage());
            var projectId = _hashidService.Decode(hashId);
            var result = _workItemService.ExportStatisticalYieldOneProjectToExcel(projectId, startDate, endDate);
            var contentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
            result.Position = 0;
            return File(result, contentType, "Tổng hợp sản lượng dự án.xlsx");
        }

        /// <summary>
        /// Tổng hợp sản lượng của tất cả các dự án
        /// </summary>
        /// <param name="checkStatistical"></param>
        /// <returns></returns>
        [Authorize]
        [HttpPost("/api/v1/statistical-yield-all-projects")]
        [SwaggerResponse((int)System.Net.HttpStatusCode.OK, Type = typeof(ResultBase<StatisticalYieldAllModel>))]
        public IActionResult StatisticalYieldAllProjects([FromBody] CheckStatisticalWithTime checkStatistical)
        {
            if (!ModelState.IsValid) throw new Exception(ModelState.GetErrorsMessage());
            var result = _workItemService.StatisticalYieldAllProjects(checkStatistical);
            return Json(new { success = true, data = result });
        }

        /// <summary>
        /// Xuất excel tổng hợp sản lượng của tất cả các dự án
        /// </summary>
        /// <param name="startDate"></param>
        /// <param name="endDate"></param>B
        /// <returns></returns>
        [HttpGet("/api/v1/export-statistical-yield-all-projects-to-excel")]
        [SwaggerResponse((int)System.Net.HttpStatusCode.OK, Type = typeof(ResultBase<Stream>))]
        public IActionResult ExportStatisticalYieldAllProjectToExcel(DateTime? startDate, DateTime? endDate)
        {
            if (!ModelState.IsValid) throw new Exception(ModelState.GetErrorsMessage());
            var result = _workItemService.ExportStatisticalYieldAllProjectToExcel(startDate, endDate);
            var contentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
            return File(result, contentType, "Tổng hợp sản lượng tất cả các dự án.xlsx");
        }

        /// <summary>
        /// Tổng hợp tiến độ thi công( hoàn thành, đang thi công, chưa thi công) của các công ty trong dự án theo thời gian
        /// </summary>
        /// <param name="hashId"></param>
        /// <param name="checkStatistical"></param>
        /// <returns></returns>
        [HttpPost("/api/v1/statistics-actual-of-construction-in-the-project/{hashId}")]
        [Authorize]
        [SwaggerResponse((int)System.Net.HttpStatusCode.OK, Type = typeof(StatisticsActualOfConstructionModel))]
        public IActionResult StatisticsActualOfConstructionInTheProject([FromRoute]string hashId, [FromBody]CheckStatisticalWithTime checkStatistical)
        {
            var projectId = _hashidService.Decode(hashId);
            var result = _workItemService.StatisticsActualOfConstructionInTheProject(projectId, checkStatistical);
            return Json(new { success = true, data = result });
        }

        /// <summary>
        /// Tổng hợp tiến độ thi công( báo cáo tháng)
        /// </summary>
        /// <returns></returns>
        [HttpPost("/api/v1/workitem/report-monthly-construction-in-project")]
        [Authorize]
        [SwaggerResponse((int)System.Net.HttpStatusCode.OK, Type = typeof(IEnumerable<StatisticsActualOfContruction>))]
        public IActionResult ReportMonthlyContructionInProject([FromBody] CheckMonthlyReport checkMonthly)
        {
            if (!ModelState.IsValid) throw new Exception(ModelState.GetErrorsMessage());
            if (string.IsNullOrEmpty(checkMonthly.HashId)) throw new Exception("Yêu cầu chọn dự án");
            checkMonthly.ProjectId = _hashidService.Decode(checkMonthly.HashId);
            var result = _workItemService.ReportMonthlyContructionInProject(checkMonthly);
            return Json(new { success = true, data = result });
        }


        /// <summary>
        /// Xuất excel tổng hợp tiến độ thi công( hoàn thành, đang thi công, chưa thi công) của các công ty trong dự án theo thời gian
        /// </summary>
        /// <param name="hashId"></param>
        /// <param name="month"></param>
        /// <param name="year"></param>
        /// <returns></returns>
        [HttpGet("/api/v1/export-report-monthly-construction-in-project/{hashId}/{month}/{year}")]
        [SwaggerResponse((int)System.Net.HttpStatusCode.OK, Type = typeof(ResultBase<Stream>))]
        public IActionResult ExportStatisticsActualOfConstructionInTheProjectToExcel(string hashId, int? month, int year)
        {
            if (string.IsNullOrEmpty(hashId)) throw new Exception("Chưa chọn dự án");
            var projectId = _hashidService.Decode(hashId);
            var result = _workItemService.ExportReportMonthlyContructionInProjectToExcel(projectId, month, year);
            var contentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
            return File(result, contentType, "Báo cáo tiến độ thi công theo tháng.xlsx");
        }

        /// <summary>
        /// Sản lượng từng công ty trong dự án
        /// </summary>
        /// <param name="hashId"></param>
        /// <param name="checkStatistical"></param>
        /// <returns></returns>
        [HttpPost("/api/v1/workitem/{hashId}/statics-yield-with-company")]
        [Authorize]
        [SwaggerResponse((int)System.Net.HttpStatusCode.OK, Type = typeof(StatisticYieldWithCompanyModel))]
        public IActionResult StaticsYieldWithCompany([FromRoute]string hashId, [FromBody]CheckStatisticalWithTime checkStatistical)
        {
            var projectId = _hashidService.Decode(hashId);
            var result = _workItemService.StaticsYieldWithCompany(projectId, checkStatistical);

            return Json(new { success = true, data = result });
        }

        [HttpGet("/api/v1/workitem/auto-deploy2222")]
        public IActionResult TestAuto()
        {
            return Json(new { data = "haha" });
        }
    }
}

