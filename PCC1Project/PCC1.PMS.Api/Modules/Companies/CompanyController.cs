﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using MicroOrm.Dapper.Repositories;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using PCC1.PMS.Api.Helpers;
using PCC1.PMS.Domain.Entity;
using PCC1.PMS.Domain.Models.Companies;
using PCC1.PMS.Domain.Services.Common;
using PCC1.PMS.Domain.Services.Companies;
using PCC1.PMS.Domain.Services.Roles;
using Swashbuckle.AspNetCore.Annotations;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace PCC1.PMS.Api.Modules.Companies
{
    [Route("api/[controller]")]
    public class CompanyController : Controller
    {

        private readonly ICompanyService _companyService;
        private readonly IDapperRepository<Project> _projectRepository;
        private readonly IHashidService _hashidService;

        public CompanyController(ICompanyService companyService,
            IDapperRepository<Project> projectRepository,
            IHashidService hashIdService)
        {
            _companyService = companyService;
            _projectRepository = projectRepository;
            _hashidService = hashIdService;
        }
        // <summary>
        // Get all companies
        // </summary>
        // <returns></returns>
        [HttpGet("/api/v1/company/get_all_companies")]
        [Authorize]
        [SwaggerResponse((int)HttpStatusCode.OK, Type = typeof(ResultBase<>))]
        public async Task<IActionResult> GetCompanies()
        {
            //var userModel = _userService.GetById(userId);

            var result = await _companyService.GetAllCommpany();
            return Json(new { success = true, data = result });
        }

        [HttpGet("/api/v1/test")]
        public IActionResult TestImage()
        {
            return new FileStreamResult(new FileStream(@"D:\Mock Images\images.jpg", FileMode.Open), "image/jpg");
        }

        /// <summary>
        /// Lấy thông tin công ty
        /// </summary>
        /// <param name="companyId"></param>
        /// <returns></returns>
        [HttpGet("/api/v1/company/get-info")]
        [Authorize]
        [SwaggerResponse((int)HttpStatusCode.OK, Type = typeof(ResultBase<CompanyModel>))]
        public IActionResult GetInfoCompany(int companyId)
        {
            var result = _companyService.GetInfoCompany(companyId);
            return Json(new { success = true, data = result });
        }

        /// <summary>
        /// Tìm kiếm công ty
        /// </summary>
        /// <param name="search"></param>
        /// <returns></returns>
        [HttpPost("/api/v1/company/search-companies")]
        [Authorize]
        [SwaggerResponse((int)HttpStatusCode.OK, Type = typeof(ResultBase<SearchCompanyResult>))]
        public IActionResult SearchCompanies([FromBody]SearchCompanyModel search)
        {
            if (!ModelState.IsValid) throw new Exception(ModelState.GetErrorsMessage());
            var result = _companyService.SearchCompanies(search);
            return Json(new { success = true, data = result });
        }


        /// <summary>
        /// Danh sách công ty theo mã dự án
        /// </summary>
        /// <param name="hashId"></param>
        /// <returns></returns>
        [HttpGet("/api/v1/project/{hashId}/list-company")]
        [Authorize]
        [SwaggerResponse((int)HttpStatusCode.OK, Type = typeof(ResultBase<CompanyModel>))]
        public IActionResult ListCompany(string hashId)
        {
            var pid = _hashidService.Decode(hashId);
            var result = _companyService.ListCompanyByProjectId(pid);
            return Json(new { success = true, data = result });
        }


        [Authorize]
        [HttpGet("/api/v1/company/list-assignments-by-workitemId")]
        [SwaggerResponse((int)HttpStatusCode.OK, Type = typeof(ResultBase<List<CompanyModel>>))]
        public IActionResult ListCompanyAssigned(int workItemId)
        {
            var result = _companyService.ListCompanyAssigned(workItemId);
            return Json(new { success = true, data = result });
        }

        /// <summary>
        /// Cập nhật thông tin công ty
        /// </summary>
        /// <param name="company"></param>
        /// <returns></returns>
        [Authorize]
        [HttpPost("/api/v1/company/save-company")]
        [SwaggerResponse((int)HttpStatusCode.OK, Type = typeof(ResultBase<bool>))]
        public IActionResult SaveCompany([FromBody]CompanyModel company)
        {
            if (HttpContext.User.HasPermission(PermissionCode.UPDATE_COMPANY))
            {
                if (!ModelState.IsValid) throw new Exception(ModelState.GetErrorsMessage());
                var result = _companyService.SaveCompany(company);
                return Json(new { success = result });
            }
            else throw new Exception("Bạn không có quyền thực hiện tính năng này");
        }


        /// <summary>
        /// Xóa thông tin công ty
        /// </summary>
        /// <param name="companies"></param>
        /// <returns></returns>
        [Authorize]
        [HttpDelete("/api/v1/company/delete-multi-companies")]
        [SwaggerResponse((int)HttpStatusCode.OK, Type = typeof(ResultBase<bool>))]
        public IActionResult DeleteCompany([FromBody]List<DeleteCompanyModel> companies)
        {
            if (HttpContext.User.HasPermission(PermissionCode.UPDATE_COMPANY))
            {
                if (!ModelState.IsValid) throw new Exception(ModelState.GetErrorsMessage());
                var result = _companyService.DeleteCompany(companies);
                return Json(new { success = result });
            }
            else
            {
                throw new Exception("Bạn không có quyền thực hiện tính năng này");
            }
        }
    }
}
