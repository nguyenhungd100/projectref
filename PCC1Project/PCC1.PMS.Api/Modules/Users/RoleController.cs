﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using PCC1.PMS.Api.Helpers;
using PCC1.PMS.Domain.Models.Users;
using PCC1.PMS.Domain.Services.Roles;
using Swashbuckle.AspNetCore.Annotations;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace PCC1.PMS.Api.Modules.Users
{
    [Route("api/[controller]")]
    public class RoleController : Controller
    {
        private readonly IRolesService _rolesService;

        public RoleController(IRolesService rolesService)
        {
            _rolesService = rolesService;
        }
        /// <summary>
        /// Filter roles
        /// </summary>
        /// <returns></returns>
        [HttpPost("/api/v1/role/list")]
        //[Authorize(Policy = nameof(PermissionCode.MANAGE_ROLES))]
        [SwaggerResponse((int)System.Net.HttpStatusCode.OK, Type = typeof(ResultBase<RoleFilterResult>))]
        public ActionResult FilterRoles([FromBody]RoleFilterModel roleFilterModel)
        {
            var res = _rolesService.Filter(roleFilterModel);

            return Json(new { success = true, data = res });
        }
        /// <summary>
        /// Get role by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [SwaggerResponse((int)System.Net.HttpStatusCode.OK, Type = typeof(ResultBase<RoleModel>))]
        //[Authorize(Policy = nameof(PermissionCode.MANAGE_ROLES))]
        [Route("v1/roles/get-role")]
        [HttpGet]
        public ActionResult GetRole(int id)
        {
            var res = _rolesService.GetById(id);
            return Json(new { success = true, data = res });
        }

        /// <summary>
        /// Update roles
        /// </summary>
        /// <param name="role"></param>
        /// <returns></returns>
        [SwaggerResponse((int)System.Net.HttpStatusCode.OK, Type = typeof(ResultBase<bool>))]
        [Authorize]
        [Route("v1/roles/update")]
        [HttpPut]
        public async Task<ActionResult> UpdateAsync([FromBody]RoleModel role)
        {
            var res = false;
            if (HttpContext.User.HasPermission(PermissionCode.MANAGE_ROLES))
                res = await _rolesService.UpdateAsync(role);
            else throw new Exception("Bạn không có quyền thực hiện tính năng này");
            return Json(new { success = res });
        }

        /// <summary>
        /// Get permissions
        /// </summary>
        /// <returns></returns>
        [SwaggerResponse((int)System.Net.HttpStatusCode.OK, Type = typeof(ResultBase<List<PermissionGroup>>))]
        //[Authorize(Policy = nameof(PermissionCode.MANAGE_ROLES))]
        [Route("v1/roles/list-permissions")]
        [HttpGet]
        public ActionResult ListPermissions()
        {
            var res = _rolesService.ListPermissions();

            return Json(new { success = true, data = res });
        }
    }
}
