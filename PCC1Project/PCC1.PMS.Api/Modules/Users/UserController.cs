﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Swashbuckle.AspNetCore.Annotations;
using PCC1.PMS.Api.Helpers;
using PCC1.PMS.Domain.Models;
using PCC1.PMS.Domain.Models.Users;
using PCC1.PMS.Domain.Services.Roles;
using PCC1.PMS.Domain.Services.Users;
using System;
using System.Net;
using System.Linq;
using PCC1.PMS.Domain.Services.Roles.Implementation;
using System.Threading.Tasks;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace PCC1.PMS.Api.Modules.Users
{
    /// <summary>
    /// User api to access user's profile and manage user's account
    /// </summary>
    [Route("api/v1/[controller]")]
    public class UserController : Controller
    {
        private readonly IUserService _userService;
        private readonly IRolesService _rolesService;
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="userService"></param>
        /// <param name="rolesService"></param>
        public UserController(IUserService userService, IRolesService rolesService)
        {
            _userService = userService;
            _rolesService = rolesService;

        }
        /// <summary>
        /// Get user's profile detail with authenticated user
        /// </summary>
        /// <returns></returns>
        [HttpGet("/api/v1/user/get")]
        public IEnumerable<string> Get()
        {

            return new string[] { "value1", "value2" };
        }

        /// <summary>
        /// Get user current loged user info
        /// </summary>
        /// <returns></returns>
        [HttpGet("/api/v1/user/get_my_profile")]
        [Authorize]
        [SwaggerResponse((int)HttpStatusCode.OK, Type = typeof(ResultBase<UserModel>))]
        public ActionResult GetMyProfile()
        {
            var userId = HttpContext.User.Identity.GetUserId();
            var userModel = _userService.GetById(userId);
            var permissions = HttpContext.User.GetPermissions();
            userModel.Permissions = permissions;

            return Json(new { success = true, data = userModel });
        }

        /// <summary>
        /// Trả về role user đăng nhập
        /// </summary>
        /// <returns></returns>
        [HttpGet("/api/v1/user/get-my-roles")]
        [Authorize]
        [SwaggerResponse((int)HttpStatusCode.OK, Type = typeof(ResultBase<RoleModel>))]
        public ActionResult GetMyRoles()
        {
            var userId = HttpContext.User.Identity.GetUserId();
            var user = _userService.GetById(userId);

            var roleIds = new List<int>();
            var roles = new List<RoleModel>();

            if (!String.IsNullOrEmpty(user.UserRoles))
            {
                roleIds = user.UserRoles.Split(new string[] { ";" }, StringSplitOptions.RemoveEmptyEntries)
                    .Select(c => Convert.ToInt32(c)).ToList();
                roles = _rolesService.List(roleIds.ToArray());
            }

            return Json(new { success = true, data = roles });
        }

        /// <summary>
        /// Get user info by id
        /// </summary>
        /// <returns></returns>
        [HttpGet("/api/v1/user/get-user-info")]
        [Authorize]
        [SwaggerResponse((int)HttpStatusCode.OK, Type = typeof(ResultBase<UserModel>))]
        public ActionResult GetUserInfo(long userId)
        {
            var userModel = _userService.GetById(userId);
            var permissions = HttpContext.User.GetPermissions();
            userModel.Permissions = permissions;

            return Json(new { success = true, data = userModel });
        }

        /// <summary>
        /// Search users
        /// </summary>
        /// <param name="userSearchModel"></param>
        /// <returns></returns>
        [HttpPost("/api/v1/user/search")]
        [Authorize]
        [SwaggerResponse((int)HttpStatusCode.OK, Type = typeof(ResultBase<List<UserModel>>))]
        public ActionResult Search([FromBody]UserSearchModel userSearchModel)
        {
            userSearchModel.UserId = HttpContext.User.Identity.GetUserId();
            var result = _userService.Search(userSearchModel);
            return Json(new { success = true, data = result });
        }

        /// <summary>
        /// Add user( Admin)
        /// </summary>
        /// <param name="createUserModel"></param>
        /// <returns></returns>
        [HttpPost("/api/v1/user/add-user")]
        [Authorize(Policy = nameof(PermissionCode.MANAGE_USERS))]
        [SwaggerResponse((int)HttpStatusCode.OK, Type = typeof(ResultBase<long>))]
        public ActionResult CreateUser([FromBody]CreateUserModel createUserModel)
        {
            if (!ModelState.IsValid) throw new Exception(ModelState.GetErrorsMessage());

            var result = _userService.CreateUser(createUserModel);
            return Json(new { success = true, data = result });
        }

        /// <summary>
        /// Add user( Admin)
        /// </summary>
        /// <param name="updateUserModel"></param>
        /// <returns></returns>
        [HttpPost("/api/v1/user/update-user")]
        [Authorize(Policy = nameof(PermissionCode.MANAGE_USERS))]
        [SwaggerResponse((int)HttpStatusCode.OK, Type = typeof(ResultBase<UserModel>))]
        public ActionResult UpdateUser([FromBody]UpdateUserModel updateUserModel)
        {
            if (!ModelState.IsValid) throw new Exception(ModelState.GetErrorsMessage());
            var result = _userService.UpdateUser(updateUserModel);
            return Json(new { success = true, data = result });
        }

        /// <summary>
        /// Change password
        /// </summary>
        /// <param name="changePasswordModel"></param>
        /// <returns></returns>
        [HttpPost("/api/v1/user/change-password")]
        [SwaggerResponse((int)HttpStatusCode.OK, Type = typeof(ResultBase<bool>))]
        [Authorize]
        public ActionResult ChangePassword([FromBody]ChangePasswordModel changePasswordModel)
        {
            if (!ModelState.IsValid) throw new Exception(ModelState.GetErrorsMessage());

            var userId = HttpContext.User.Identity.GetUserId();
            changePasswordModel.UserId = userId;

            var result = _userService.ChangePassword(changePasswordModel);
            return Json(new { success = result });
        }



        /// <summary>
        /// Change password
        /// </summary>
        /// <param name="changePasswordModel"></param>
        /// <returns></returns>
        [HttpPost("/api/v1/user/reset-password")]
        [SwaggerResponse((int)HttpStatusCode.OK, Type = typeof(ResultBase<bool>))]
        [Authorize(Policy = nameof(PermissionCode.MANAGE_USERS))]
        public ActionResult ResetPassword([FromBody]ChangePasswordModel changePasswordModel)
        {
            if (!ModelState.IsValid) throw new Exception(ModelState.GetErrorsMessage());
            var result = _userService.ChangePassword(changePasswordModel);
            return Json(new { success = result });
        }

        /// <summary>
        /// Verify OTP
        /// </summary>
        /// <param name="verifyOTPModel"></param>
        /// <returns></returns>
        [HttpPost("/api/v1/user/verify-otp")]
        [SwaggerResponse((int)HttpStatusCode.OK, Type = typeof(ResultBase<>))]
        public ActionResult VerifyOTP([FromBody]VerifyOTPModel verifyOTPModel)
        {
            if (!ModelState.IsValid) throw new Exception(ModelState.GetErrorsMessage());

            var result = _userService.ActiveAccount(verifyOTPModel.UserId, verifyOTPModel.OTP);
            return Json(new { success = result });
        }

        /// <summary>
        /// Resend OTP
        /// </summary>
        /// <returns></returns>
        [HttpPost("/api/v1/user/resend-otp")]
        [SwaggerResponse((int)HttpStatusCode.OK, Type = typeof(ResultBase<>))]
        public ActionResult ResendOTP([FromBody]string phone)
        {
            _userService.ResendOTP(phone);
            return Json(new { success = true });
        }
        /// <summary>
        /// Update user location
        /// </summary>
        /// <param name="locationModel"></param>
        /// <returns></returns>
        //[Authorize]
        [HttpPost("/api/v1/user/update-location")]
        [SwaggerResponse((int)HttpStatusCode.OK, Type = typeof(ResultBase<>))]
        public ActionResult UpdateLocation([FromBody]LocationModel locationModel)
        {
            var userId = HttpContext.User.Identity.GetUserId();
            var result = _userService.UpdateLocation(userId, locationModel);
            return Json(new { success = result });
        }


        /// <summary>
        /// Update user location
        /// </summary>
        /// <param name="tokenModel"></param>
        /// <returns></returns>
        [Authorize]
        [HttpPost("/api/v1/user/update-token")]
        [SwaggerResponse((int)HttpStatusCode.OK, Type = typeof(ResultBase<>))]
        public ActionResult UpdateToken([FromBody]UpdateTokenModel tokenModel)
        {
            var userId = HttpContext.User.Identity.GetUserId();
            var result = _userService.UpdateToken(userId, tokenModel);
            return Json(new { success = result });
        }

        /// <summary>
        /// Update user profile
        /// </summary>
        /// <param name="userProfileModel"></param>
        /// <returns></returns>
        [Authorize]
        [HttpPost("/api/v1/user/update-profile")]
        [SwaggerResponse((int)System.Net.HttpStatusCode.OK, Type = typeof(ResultBase<>))]
        public ActionResult UpdateProfile([FromBody]UpdateUserProfileModel userProfileModel)
        {
            var userId = HttpContext.User.Identity.GetUserId();
            userProfileModel.UserId = userId;
            var result = _userService.UpdateProfile(userProfileModel);
            return Json(new { success = result });
        }

        [Authorize]
        [HttpPost("/api/v1/user/update-role")]
        [SwaggerResponse((int)System.Net.HttpStatusCode.OK, Type = typeof(ResultBase<bool>))]
        public async Task<ActionResult> UpdateRole([FromBody] UpdateRoleModel model)
        {
            var result = await _userService.UpdateRole(model.Id, model.UserRoles);
            return Json(new { success = result });
        }

        //[Authorize]
        //[HttpPost("/api/v1/user/get-role-for-all-user")]
        //[SwaggerResponse((int)System.Net.HttpStatusCode.OK, Type = typeof(ResultBase<List<RoleModel>>))]
        //public List<RoleModel> GetRoleForAllUser(int[] userId)
        //{

        //}

        [HttpPost("/api/v1/user/remove-user")]
        [SwaggerResponse((int)System.Net.HttpStatusCode.OK, Type = typeof(ResultBase<bool>))]
        [Authorize(Policy = nameof(PermissionCode.MANAGE_USERS))]
        public IActionResult RemoveUser([FromBody] UserRemoveModel userRemoveModel)
        {
            if (!ModelState.IsValid) throw new Exception(ModelState.GetErrorsMessage());

            var currentUser = HttpContext.User.Identity.GetUserId();
            if (currentUser == userRemoveModel.Id) return Json(new { success = false, message = "Bạn không thể xóa tài khoản này!" });
            var result = _userService.RemoveUser(userRemoveModel);
            return Json(new { success = result });
        }  

        [HttpPost("/api/v1/user/forgot-password")]
        [SwaggerResponse((int)System.Net.HttpStatusCode.OK, Type = typeof(ResultBase<bool>))]
        public IActionResult SendOtpToEmail([FromBody]ForgotPasswordModel model)
        {
            if (!ModelState.IsValid) throw new Exception(ModelState.GetErrorsMessage());
            var result = _userService.SendOtpToEmail(model);
            return Json(new { success = result });
        }

        [HttpPost("/api/v1/verify-otp-password")]
        [SwaggerResponse((int)System.Net.HttpStatusCode.OK, Type = typeof(ResultBase<bool>))]
        public IActionResult VerifyOTPPassword([FromBody]VerifyOTPPasswordModel verifyOTP)
        {
            var result = _userService.VerifyOTPPassword(verifyOTP);
            return Json(new { success = result });
        }

        [HttpGet("/api/v1/department/list")]
        [Authorize]
        [SwaggerResponse((int)System.Net.HttpStatusCode.OK, Type = typeof(ResultBase<IEnumerable<DepartmentModel>>))]
        public IActionResult GetDepartments()
        {
            var result = _userService.GetDepartments();
            return Json(new { success = true, data = result });
        }
    }
}
