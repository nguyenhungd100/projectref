﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using PCC1.PMS.Api.Helpers;
using PCC1.PMS.Domain.Models.ProjectReports;
using PCC1.PMS.Domain.Models.SafelyReports;
using PCC1.PMS.Domain.Models.Statistical;
using PCC1.PMS.Domain.Services.Common;
using PCC1.PMS.Domain.Services.Projects;
using PCC1.PMS.Domain.Services.Roles;
using PCC1.PMS.Domain.Services.SafelyReports;
using Swashbuckle.AspNetCore.Annotations;

namespace PCC1.PMS.Api.Modules.SafelyReports
{
    [Route("api/v1/[controller]")]
    public class SafelyReportController : Controller
    {
        private readonly ISafelyReportService _safelyReportService;
        private readonly IHashidService _hashidService;
        private readonly IProjectService _projectService;

        public SafelyReportController(ISafelyReportService safelyReportService,
            IHashidService hashidService,
            IProjectService projectService)
        {
            _safelyReportService = safelyReportService;
            _hashidService = hashidService;
            _projectService = projectService;
        }

        /// <summary>
        /// Danh sách lỗi an toàn vệ sinh đã được định nghĩa
        /// </summary>
        /// <returns></returns>       
        [HttpGet("/api/v1/safely-report/list-violation-defined")]
        [Authorize]
        [SwaggerResponse((int)System.Net.HttpStatusCode.OK, Type = typeof(ResultBase<ViolationDefinedModel>))]
        public IActionResult ViolationDefined()
        {
            var result = _safelyReportService.ListViolationDefined();
            return Json(new { success = true, data = result });
        }

        /// <summary>
        /// Tìm kiếm báo cáo an toàn theo mã
        /// </summary>
        /// <param name="reportId"></param>
        /// <returns></returns>
        [SwaggerResponse((int)System.Net.HttpStatusCode.OK, Type = typeof(ResultBase<SafelyReportInfoModel>))]
        [Authorize]
        [HttpGet("/api/v1/safely-report/{reportId}")]
        public IActionResult GetSafelyReportById(int reportId)
        {
            var result = _safelyReportService.GetSafelyReportById(reportId);
            return Json(new { success = true, data = result });
        }

        /// <summary>
        /// Tìm kiếm báo cáo an toàn vệ sinh lao động
        /// </summary>
        /// <param name="search"></param>
        /// <returns></returns>
        [HttpPost("/api/v1/project-report/search-safely-report")]
        [Authorize]
        [SwaggerResponse((int)System.Net.HttpStatusCode.OK, Type = typeof(ResultBase<SearchSafelyReportResult>))]
        public IActionResult SearchSafelyReport([FromBody] SearchSafelyReportModel search)
        {
            if (HttpContext.User.HasPermission(PermissionCode.VIEW_ALL_SAFELYREPORT))
            {
            }
            else
            {
                search.CreatedBy = HttpContext.User.Identity.GetUserId();
            }
            var result = _safelyReportService.SearchSafelyReport(search);
            return Json(new { success = true, data = result });
        }

        ///// <summary>
        ///// Thống kê số lượng lỗi an toàn vi phạm của công ty cụ thể
        ///// </summary>
        ///// <param name="statistical"></param>
        ///// <returns></returns>
        //[HttpPost("/api/v1/project-report/statistically-safely-report")]
        //[Authorize]
        //[SwaggerResponse((int)System.Net.HttpStatusCode.OK, Type = typeof(ResultBase<StatisticalViolationType>))]
        //public IActionResult StatisticalSafelyReport([FromBody] StatisticalSafelyReportModel statistical)
        //{
        //    if (!String.IsNullOrEmpty(statistical.ProjectHashId))
        //        statistical.ProjectId = _hashidService.Decode(statistical.ProjectHashId);
        //    var result = _safelyReportService.StatisticalSafelyReport(statistical);
        //    return Json(new { success = true, data = result });
        //}

        /// <summary>
        /// Cập nhật báo cáo an toàn vệ sinh lao động
        /// </summary>
        /// <param name="updateSafely"></param>
        /// <param name="hashid"></param>
        /// <returns></returns>
        [HttpPost("/api/v1/project/{hashid}/update-safely-report")]
        [Authorize]
        [SwaggerResponse((int)System.Net.HttpStatusCode.OK, Type = typeof(ResultBase<bool>))]
        public IActionResult UpdateSafelyReport([FromBody]UpdateSafelyReportModel updateSafely, string hashid)
        {
            var userId = HttpContext.User.Identity.GetUserId();
            var result = (false, 0);
            if (_projectService.CheckHasPermissionProject(hashid, userId, PermissionCode.UPDATE_SAFELYREPORT))
            {
                if (!ModelState.IsValid) throw new Exception(ModelState.GetErrorsMessage());
                updateSafely.ProjectId = _hashidService.Decode(hashid);
                updateSafely.CreatedBy = userId;
                result = _safelyReportService.UpdateSafelyReport(updateSafely);
            }
            else throw new Exception("Bạn không có quyền thực hiện tính năng này");
            return Json(new { success = result.Item1, data = result.Item2 });
        }

        /// <summary>
        /// Xóa báo cáo an toàn vệ sinh
        /// </summary>
        /// <param name="safelyReportDeleteModel"></param>
        /// <param name="hashId"></param>
        /// <returns></returns>
        [HttpDelete("/api/v1/project-report/delete-safely-report/{hashId}")]
        [Authorize]
        [SwaggerResponse((int)System.Net.HttpStatusCode.OK, Type = typeof(ResultBase<bool>))]
        public IActionResult DeleteSafelyReport([FromBody] DeleteSafelyReportModel safelyReportDeleteModel, [FromRoute] string hashId)
        {
            var result = false;
            var userId = HttpContext.User.Identity.GetUserId();
            if (_projectService.CheckHasPermissionProject(hashId, userId, PermissionCode.UPDATE_SAFELYREPORT))
            {
                if (!ModelState.IsValid) throw new Exception(ModelState.GetErrorsMessage());
                result = _safelyReportService.DeleteSafelyReport(safelyReportDeleteModel);
            }
            else throw new Exception("Bạn không có quyền thực hiện tính năng này");
            return Json(new { success = result });
        }

        /// <summary>
        /// Danh sách bản nháp( người dùng đăng nhập)
        /// </summary>
        /// <returns></returns>
        [HttpGet("/api/v1/safely-report/list-draft-current-user/{projectHashId}")]
        [Authorize]
        [SwaggerResponse((int)System.Net.HttpStatusCode.OK, Type = typeof(ResultBase<List<SafelyDraftModel>>))]
        public IActionResult ListDraft(string projectHashId)
        {
            int user = HttpContext.User.Identity.GetUserId();
            int projectId = _hashidService.Decode(projectHashId);
            var result = _safelyReportService.ListDraft(user, projectId);
            return Json(new { success = true, data = result });
        }

        /// <summary>
        /// Cập nhật bản nháp ảnh an toàn vệ sinh
        /// </summary>
        /// <param name="updateSafelyDraft"></param>
        /// <returns></returns>
        [HttpPost("/api/v1/safely-report/update-draft")]
        [Authorize]
        [SwaggerResponse((int)System.Net.HttpStatusCode.OK, Type = typeof(ResultBase<bool>))]
        public IActionResult UpdateSafelyDraft([FromBody] UpdateSafelyDraft updateSafelyDraft)
        {
            var result = false;
            var userId = HttpContext.User.Identity.GetUserId();
            if (_projectService.CheckHasPermissionProject(updateSafelyDraft.ProjectHashId, userId, PermissionCode.UPDATE_SAFELYREPORT))
            {
                if (!ModelState.IsValid) throw new Exception(ModelState.GetErrorsMessage());

                updateSafelyDraft.CreatedBy = HttpContext.User.Identity.GetUserId();
                updateSafelyDraft.ProjectId = _hashidService.Decode(updateSafelyDraft.ProjectHashId);
                result = _safelyReportService.UpdateSafelyDraft(updateSafelyDraft);
            }
            else throw new Exception("Bạn không có quyền thực hiện tính năng này");

            return Json(new { success = result });
        }

        /// <summary>
        /// Xóa bản nháp an toàn vệ sinh
        /// </summary>
        /// <param name="deleteMultiDraft"></param>
        /// <returns></returns>
        [HttpPost("/api/v1/safely-report/delete-multi-draft")]
        [Authorize]
        [SwaggerResponse((int)System.Net.HttpStatusCode.OK, Type = typeof(ResultBase<bool>))]
        public IActionResult DeleteMultilSafelyDraft([FromBody]DeleteMultiDraftModel deleteMultiDraft)
        {
            if (!ModelState.IsValid) throw new Exception(ModelState.GetErrorsMessage());
            deleteMultiDraft.UserId = HttpContext.User.Identity.GetUserId();
            var result = _safelyReportService.DeleteMultilSafelyDraft(deleteMultiDraft);
            return Json(new { success = result });
        }

        /// <summary>
        /// Tổng hợp 1 báo cáo an toàn vệ sinh lao động
        /// </summary>
        /// <param name="safelyReportId"></param>
        /// <returns></returns>
        [HttpGet("/api/v1/safely-report/statistic-in-1-report")]
        [Authorize]
        [SwaggerResponse((int)System.Net.HttpStatusCode.OK, Type = typeof(ResultBase<List<SafelyViolationTypeStatistical>>))]
        public IActionResult StatisticalOneSafelyReport(int safelyReportId)
        {
            if (!ModelState.IsValid) throw new Exception(ModelState.GetErrorsMessage());
            var result = _safelyReportService.StatisticalOneSafelyReport(safelyReportId);
            return Json(new { success = true, data = result });
        }


        /// <summary>
        /// Tổng hợp báo cáo an toàn vệ sinh trong 1 dự án
        /// </summary>
        /// <param name="searchStatistic"></param>
        /// <returns></returns>
        [HttpPost("/api/v1/safely-report/statistic-in-1-project")]
        [Authorize]
        [SwaggerResponse((int)System.Net.HttpStatusCode.OK, Type = typeof(ResultBase<StatisticalViolationModel>))]
        public IActionResult StatisticalSafelyReportInProject([FromBody]SearchStatisticSafelyReportOneProject searchStatistic)
        {
            if (!ModelState.IsValid) throw new Exception(ModelState.GetErrorsMessage());
            searchStatistic.ProjectId = _hashidService.Decode(searchStatistic.HashId);
            var result = _safelyReportService.StatisticalSafelyReportInProject(searchStatistic);
            return Json(new { success = true, data = result });
        }

        /// <summary>
        /// Xuất excel tổng hợp báo cáo an toàn vệ sinh lao động trong một dự án
        /// </summary>
        /// /// <param name="hashId"></param>
        /// <param name="startDate"></param>
        /// <param name="endDate"></param>
        /// <returns></returns>
        //[Authorize]
        [HttpGet("/api/v1/export-safely-report-statistic-in-1-project/{hashId}")]
        [SwaggerResponse((int)System.Net.HttpStatusCode.OK, Type = typeof(ResultBase<Stream>))]
        public IActionResult ExportStatisticalSafelyReportInProject(string hashId, DateTime? startDate, DateTime? endDate)
        {
            if (!ModelState.IsValid) throw new Exception(ModelState.GetErrorsMessage());
            var projectId = _hashidService.Decode(hashId);
            var result = _safelyReportService.ExportStatisticalSafelyReportInProject(projectId, startDate, endDate);
            var contentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
            return File(result.Item1, contentType, "Tổng hợp báo cáo công tác an toàn vệ sinh công trình " + result.Item2 + ".xlsx");
        }



        /// <summary>
        /// Tổng hợp báo cáo an toàn vệ sinh trong tất cả các dự án
        /// </summary>
        /// <returns></returns>
        [HttpPost("/api/v1/safely-report/statistic-in-all-project")]
        [Authorize]
        [SwaggerResponse((int)System.Net.HttpStatusCode.OK, Type = typeof(ResultBase<StatisticalViolationAllProjectModel>))]
        public IActionResult StatisticalSafelyReportAllProject([FromBody] CheckStatisticalWithTime searchStatistic)
        {
            if (!ModelState.IsValid) throw new Exception(ModelState.GetErrorsMessage());
            var result = _safelyReportService.StatisticalSafelyReportAllProject(searchStatistic);
            return Json(new { success = true, data = result });
        }

        /// <summary>
        /// Xuất excel tổng hợp báo cáo an toàn vệ sinh lao động của tất cả các dự án
        /// </summary>
        /// <param name="startDate"></param>
        /// <param name="endDate"></param>
        /// <returns></returns>
        [HttpGet("/api/v1/export-safely-report-statistic-in-all-project")]
        [SwaggerResponse((int)System.Net.HttpStatusCode.OK, Type = typeof(ResultBase<Stream>))]
        public IActionResult ExportStatisticalSafelyReportAllProject(DateTime? startDate, DateTime? endDate)
        {
            if (!ModelState.IsValid) throw new Exception(ModelState.GetErrorsMessage());
            var result = _safelyReportService.ExportStatisticalSafelyReportAllProject(startDate, endDate);
            var contentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
            return File(result, contentType, "Tổng hợp đánh giá thực hiện công tác an toàn vệ sinh.xlsx");
        }

        /// <summary>
        /// Gửi báo cáo an toàn vệ sinh
        /// </summary>
        /// <param name="report"></param>
        /// <param name="hashId"></param>
        /// <returns></returns>
        [Authorize]
        [HttpPost("/api/v1/safely-report/submit-report/{hashId}")]
        [SwaggerResponse((int)System.Net.HttpStatusCode.OK, Type = typeof(ResultBase<bool>))]
        public IActionResult SumitSafelyReport([FromBody] SubmitSafelyReport report, [FromRoute] string hashId)
        {
            var result = false;
            var userId = HttpContext.User.Identity.GetUserId();
            if (_projectService.CheckHasPermissionProject(hashId, userId, PermissionCode.UPDATE_SAFELYREPORT))
            {
                if (!ModelState.IsValid) throw new Exception(ModelState.GetErrorsMessage());
                report.SubmitUser = HttpContext.User.Identity.GetUserId();
                result = _safelyReportService.SumitSafelyReport(report);
            }
            else throw new Exception("Bạn không có quyền thực hiện tính năng này");
            return Json(new { success = result });
        }

        /// <summary>
        /// Phê duyệt báo cáo an toàn vệ sinh
        /// </summary>
        /// <param name="report"></param>
        /// <param name="hashId"></param>
        /// <returns></returns>
        [Authorize]
        [HttpPost("/api/v1/safely-report/approve-report/{hashId}")]
        [SwaggerResponse((int)System.Net.HttpStatusCode.OK, Type = typeof(ResultBase<bool>))]
        public IActionResult ApproveSafelyReport([FromBody] ApproveSafelyReport report, [FromRoute] string hashId)
        {
            var result = false;
            var userId = HttpContext.User.Identity.GetUserId();
            if (_projectService.CheckHasPermissionProject(hashId, userId, PermissionCode.APPROVE_SAFELYREPORT))
            {
                if (!ModelState.IsValid) throw new Exception(ModelState.GetErrorsMessage());
                report.ApproveUser = HttpContext.User.Identity.GetUserId();
                result = _safelyReportService.ApproveSafelyReport(report);
            }
            else throw new Exception("Bạn không có quyền thực hiện tính năng này");
            return Json(new { success = result });
        }

        /// <summary>
        /// Từ chối báo cáo an toàn vệ sinh
        /// </summary>
        /// <param name="report"></param>
        /// <param name="hashId"></param>
        /// <returns></returns>
        [Authorize]
        [HttpPost("/api/v1/safely-report/reject-report/{hashId}")]
        [SwaggerResponse((int)System.Net.HttpStatusCode.OK, Type = typeof(ResultBase<bool>))]
        public IActionResult ApproveSafelyReport([FromBody] RejectSafelyReport report, [FromRoute] string hashId)
        {
            var result = false;
            var userId = HttpContext.User.Identity.GetUserId();
            if (_projectService.CheckHasPermissionProject(hashId, userId, PermissionCode.APPROVE_SAFELYREPORT))
            {
                if (!ModelState.IsValid) throw new Exception(ModelState.GetErrorsMessage());
                report.RejectUser = HttpContext.User.Identity.GetUserId();
                result = _safelyReportService.RejectSafelyReport(report);
            }
            else throw new Exception("Bạn không có quyền thực hiện tính năng này");
            return Json(new { success = result });
        }

        [Authorize]
        [HttpPost("/api/v1/safely-report/translate-fileid-safely-images")]
        [SwaggerResponse((int)System.Net.HttpStatusCode.OK, Type = typeof(ResultBase<bool>))]
        public IActionResult TranslateSafelyImage()
        {
            var result = _safelyReportService.TranslateSafelyImage();
            return Json(new { success = result });
        }

        [Authorize]
        [HttpPost("/api/v1/safely-report/translate-fileid-safely-images-draft")]
        [SwaggerResponse((int)System.Net.HttpStatusCode.OK, Type = typeof(ResultBase<bool>))]
        public IActionResult TranslateSafelyImageDraft()
        {
            var result = _safelyReportService.TranslateSafelyImageDraft();
            return Json(new { success = true });
        }

        [Authorize]
        [HttpDelete("/api/v1/safely-report/delete-image")]
        [SwaggerResponse((int)System.Net.HttpStatusCode.OK, Type = typeof(ResultBase<bool>))]
        public IActionResult DeleteSafelyReportImage(Guid imageId, int imageInfoId)
        {
            var result = _safelyReportService.DeleteSafelyReportImage(imageId, imageInfoId);
            return Json(new { success = result });
        }

        [Authorize]
        [HttpDelete("/api/v1/safely-report/delete-image-draft")]
        [SwaggerResponse((int)System.Net.HttpStatusCode.OK, Type = typeof(ResultBase<bool>))]
        public IActionResult DeleteSafelyReportDraftImage(Guid imageId, int imageInfoId)
        {
            var result = _safelyReportService.DeleteSafelyReportDraftImage(imageId, imageInfoId);
            return Json(new { success = result });
        }

        /// <summary>
        /// Gửi bản nháp báo cáo an toàn vệ sinh trên mobile
        /// </summary>
        /// <param name="imageInfoIds"></param>
        /// <returns></returns>
        [Authorize]
        [HttpPost("/api/v1/safely-report/submit-safely-report-on-mobile")]
        [SwaggerResponse((int)System.Net.HttpStatusCode.OK, Type = typeof(ResultBase<bool>))]
        public IActionResult SubmitSafelyReportDraft([FromBody] IEnumerable<int> imageInfoIds)
        {
            var userId = HttpContext.User.Identity.GetUserId();
            var result = _safelyReportService.SubmitSafelyReportDraft(imageInfoIds, userId);
            return Json(new { success = result });
        }
    }
}