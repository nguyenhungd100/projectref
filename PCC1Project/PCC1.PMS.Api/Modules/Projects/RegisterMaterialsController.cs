﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using PCC1.PMS.Api.Helpers;
using PCC1.PMS.Domain.Models.Materials;
using PCC1.PMS.Domain.Services.Common;
using PCC1.PMS.Domain.Services.RegisterMaterials;
using PCC1.PMS.Domain.Services.Roles;
using Swashbuckle.AspNetCore.Annotations;

// For more information on enabling Web API for empty projects, visit https://go.micrhưosoft.com/fwlink/?LinkID=397860

namespace PCC1.PMS.Api.Modules.Projects
{
    public partial class ProjectController
    {
        /// <summary>
        /// Tìm kiếm danh sách vật tư
        /// </summary>
        /// <returns></returns>
        [HttpPost("/api/v1/register-materials/search")]
        [Authorize]
        [SwaggerResponse((int)HttpStatusCode.OK, Type = typeof(ResultBase<SearchRegisterMaterialResult>))]
        public IActionResult SearchRegisterMaterial([FromBody]SearchRegisterMaterialModel search)
        {
            var result = new SearchRegisterMaterialResult();
            if (HttpContext.User.HasPermission(PermissionCode.VIEW_ALL_REGISTER_MATERIALS))
            {               
            }
            else search.UserId = HttpContext.User.Identity.GetUserId();
            if (!ModelState.IsValid) throw new Exception(ModelState.GetErrorsMessage());
            if (!string.IsNullOrEmpty(search.HashId))
                search.ProjectId = _hashidService.Decode(search.HashId);        
            result = _registerMaterialsService.Search(search);
            return Json(new { success = true, data = result });           
        }

        /// <summary>
        /// Thông tin vật tư
        /// </summary>
        /// <param name="materialId"></param>
        /// <returns></returns>
        [HttpGet("/api/v1/register-materials/get-by-id/{materialId}")]
        [Authorize]
        [SwaggerResponse((int)HttpStatusCode.OK, Type = typeof(ResultBase<MaterialModel>))]
        public IActionResult GetInfoById(string materialId)
        {
            if (!ModelState.IsValid) throw new Exception(ModelState.GetErrorsMessage());
            var id = _hashidService.Decode(materialId);
            var result = _registerMaterialsService.GetRegisterMaterialById(id);
            return Json(new { success = true, data = result });
        }

        /// <summary>
        /// Tạo mới phiếu đăng ký vật tư
        /// </summary>
        /// <param name="hashId"></param>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost("/api/v1/project/{hashId}/register-materials")]
        [Authorize]
        [SwaggerResponse((int)HttpStatusCode.OK, Type = typeof(ResultBase<bool>))]
        public IActionResult RegisterMaterials(string hashId, [FromBody]RegisterMaterialsModel model)
        {
            var result = false;
            var userId = HttpContext.User.Identity.GetUserId();
            if (_projectService.CheckHasPermissionProject(hashId, userId, PermissionCode.REGISTER_MATTERIALS))
            {
                var pid = _hashidService.Decode(hashId);
                if (!ModelState.IsValid) throw new Exception(ModelState.GetErrorsMessage());
                model.ProjectId = pid;
                result = _registerMaterialsService.AddRegisterMaterials(model, userId);
            }                                           
            else throw new Exception("Bạn không có quyền thực hiện tính năng này");          
            return Json(new { success = result });
        }

        /// <summary>
        /// Sửa phiếu đăng ký vật tư
        /// </summary>
        /// <param name="update"></param>
        /// <param name="hashId"></param>
        /// <returns></returns>
        [HttpPut("/api/v1/register-materials/update-register-materials/{hashId}")]
        [Authorize]
        [SwaggerResponse((int)HttpStatusCode.OK, Type = typeof(ResultBase<bool>))]
        public IActionResult UpdateRegisterMaterials([FromBody]UpdateMaterialModel update, [FromRoute] string hashId)
        {
            var result = false;
            var userId = HttpContext.User.Identity.GetUserId();
            if (_projectService.CheckHasPermissionProject(hashId, userId, PermissionCode.REGISTER_MATTERIALS))
            {
                if (!ModelState.IsValid) throw new Exception(ModelState.GetErrorsMessage());              
                result = _registerMaterialsService.UpdateRegisterMaterials(update, userId);
            }
            else throw new Exception("Bạn không có quyền thực hiện tính năng này");
            return Json(new { success = result });
        }

        /// <summary>
        /// Xóa nhiều phiếu đăng ký vật tư
        /// </summary>
        /// <param name="materialModels"></param>
        /// <param name="hashId"></param>
        /// <returns></returns>
        [HttpDelete("/api/v1/register-materials/delete-multi-materials/{hashId}")]
        [Authorize]
        [SwaggerResponse((int)HttpStatusCode.OK, Type = typeof(ResultBase<bool>))]
        public IActionResult DeleteMultiMaterialsAsync([FromBody] List<DeleteMaterialModel> materialModels,[FromRoute] string hashId)
        {
            var result = false;
            var userId = HttpContext.User.Identity.GetUserId();
            if (_projectService.CheckHasPermissionProject(hashId, userId, PermissionCode.REGISTER_MATTERIALS))
            {
                if (!ModelState.IsValid) throw new Exception(ModelState.GetErrorsMessage());
                result = _registerMaterialsService.DeleteMultiRegisterMaterials(materialModels, userId);
            }
            else throw new Exception("Bạn không có quyền thực hiện tính năng này");          
            return Json(new { success = result });
        }

        /// <summary>
        /// Duyệt phiếu đăng ký vật tư
        /// </summary>
        /// <param name="approveModels"></param>
        /// <returns></returns>
        [HttpPost("/api/v1/register-materials/approve-register-materials")]
        [Authorize]
        [SwaggerResponse((int)HttpStatusCode.OK, Type = typeof(ResultBase<bool>))]
        public IActionResult ApprovingRegisterMaterial([FromBody]IEnumerable<ApproveRegisterMaterialModel> approveModels)
        {
            var result = false;
            if (HttpContext.User.HasPermission(PermissionCode.APPROVE_MATTERIALS))
            {
                if (!ModelState.IsValid) throw new Exception(ModelState.GetErrorsMessage());
                foreach (var item in approveModels)
                {
                    item.ApproveUser = HttpContext.User.Identity.GetUserId();
                }
                result = _registerMaterialsService.ApproveRegisterMaterial(approveModels);
            }
            else throw new Exception("Bạn không có quyền thực hiện tính năng này");
            return Json(new { success = result });
        }

        /// <summary>
        /// Từ chối phiếu đăng ký vật tư
        /// </summary>
        /// <param name="approveModels"></param>
        /// <returns></returns>
        [HttpPost("/api/v1/register-materials/reject-register-materials")]
        [Authorize]
        [SwaggerResponse((int)HttpStatusCode.OK, Type = typeof(ResultBase<bool>))]
        public IActionResult RejectRegisterMaterial([FromBody]IEnumerable<ApproveRegisterMaterialModel> approveModels)
        {
            var result = false;
            if (HttpContext.User.HasPermission(PermissionCode.APPROVE_MATTERIALS))
            {
                if (!ModelState.IsValid) throw new Exception(ModelState.GetErrorsMessage());
                foreach (var item in approveModels)
                {
                    item.ApproveUser = HttpContext.User.Identity.GetUserId();
                }
                result = _registerMaterialsService.RejectRegisterMaterial(approveModels);
            }
            else throw new Exception("Bạn không có quyền thực hiện tính năng này");
            return Json(new { success = result });
        }

    }
}
