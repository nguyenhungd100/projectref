﻿using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using PCC1.PMS.Api.Helpers;
using PCC1.PMS.Domain.Models.File;
using PCC1.PMS.Domain.Models.MediaStorage;
using PCC1.PMS.Domain.Models.ProjectReports;
using PCC1.PMS.Domain.Models.Projects;
using PCC1.PMS.Domain.Services.ProjectReports;
using PCC1.PMS.Domain.Services.Roles;
using Swashbuckle.AspNetCore.Annotations;
using PCC1.PMS.Domain.Services.Common;
using System.Linq;
using Microsoft.AspNetCore.Http;
using System.Text;
using PCC1.PMS.Domain.Models.ProjectReports.DefineMachineryManpower;
using PCC1.PMS.Domain.Models.ProjectReports.StatisticsHumanResource;
using PCC1.PMS.Domain.Models.Workitems;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace PCC1.PMS.Api.Modules.Projects
{
    public partial class ProjectController
    {

        /// <summary>
        /// Thông tin báo cáo theo mã
        /// </summary>
        /// <param name="reportId"></param>
        /// <returns></returns>
        [HttpGet("/api/v1/project-report/{reportId}")]
        [Authorize]
        [SwaggerResponse((int)System.Net.HttpStatusCode.OK, Type = typeof(ResultBase<ProjectReportResultModel>))]
        public IActionResult ProjectReportById(int reportId)
        {
            var result = _projectReportService.GetReport(reportId);
            return Json(new { success = true, data = result });
        }

        /// <summary>
        /// Lọc danh sách báo cáo
        /// </summary>
        /// <param name="projectReportSearchModel"></param>
        /// <returns></returns>
        [HttpPost("/api/v1/project-report/search")]
        [Authorize]
        [SwaggerResponse((int)System.Net.HttpStatusCode.OK, Type = typeof(ResultBase<SearchProjectReportsResult>))]
        public IActionResult SearchReportProject([FromBody] SearchProjectReportsModel projectReportSearchModel)
        {
            if (!ModelState.IsValid) throw new Exception(ModelState.GetErrorsMessage());
            if (!HttpContext.User.HasPermission(PermissionCode.VIEW_ALL_REPORTS))
            {
                projectReportSearchModel.CreatedBy = HttpContext.User.Identity.GetUserId();
            }
            var userCurrent = HttpContext.User.Identity.GetUserId();
            var result = _projectReportService.Search(projectReportSearchModel, userCurrent);
            return Json(new { success = true, data = result });
        }

        /// <summary>
        /// Lọc danh sách báo cáo của người dùng dang đăng nhập
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost("/api/v1/project-report/search/current-user")]
        [Authorize]
        [SwaggerResponse((int)System.Net.HttpStatusCode.OK, Type = typeof(ResultBase<SearchProjectReportsResult>))]
        public IActionResult GetCurrentUserProjectReports([FromBody] SearchProjectReportsModel model)
        {
            if (!ModelState.IsValid) throw new Exception(ModelState.GetErrorsMessage());
            model.CreatedBy = HttpContext.User.Identity.GetUserId();
            var result = _projectReportService.Search(model, null);
            return Json(new { success = true, data = result });
        }

        /// <summary>
        /// Lấy danh sách báo cáo đã duyệt
        /// </summary>
        /// <param name="projectReportSearchModel"></param>
        /// <returns></returns>
        [HttpPost("/api/v1/project-report/view-approved-reports")]
        [Authorize]
        [SwaggerResponse((int)System.Net.HttpStatusCode.OK, Type = typeof(ResultBase<SearchProjectReportsResult>))]
        public IActionResult GetApprovedReports([FromBody] SearchProjectReportsModel projectReportSearchModel)
        {
            var result = new SearchProjectReportsResult();
            if (!ModelState.IsValid) throw new Exception(ModelState.GetErrorsMessage());
            var userId = HttpContext.User.Identity.GetUserId();
            if (_projectService.CheckHasPermissionProject(projectReportSearchModel.ProjectHashId, userId, PermissionCode.APPROVE_REPORTS))
            {
                projectReportSearchModel.Status = Domain.Enums.ProjectReportStatus.Approved;
                result = _projectReportService.Search(projectReportSearchModel, null);
            }
            return Json(new { success = true, data = result });
        }

        /// <summary>
        /// Danh sách nhân sự máy móc đã được định nghĩa
        /// </summary>
        /// <returns></returns>       
        [HttpGet("/api/v1/project-report/list-machinery-manpower-defined")]
        [Authorize]
        [SwaggerResponse((int)System.Net.HttpStatusCode.OK, Type = typeof(ResultBase<MachineryManpowerDefinedModel>))]
        public IActionResult MachineryManpowerDefined()
        {
            var result = _projectReportService.MachineryManpowerDefined();
            return Json(new { success = true, data = result });
        }

        /// <summary>
        /// Tạo báo cáo, do ông  chct tạo, trong đó bao gồm cả reportWorkItem(có cả ảnh) và reportPlan
        /// </summary>
        /// <param name="saveReportModel"></param>
        /// <returns></returns>
        [HttpPost("/api/v1/project-report/save-report")]
        [Authorize]
        [SwaggerResponse((int)System.Net.HttpStatusCode.OK, Type = typeof(ResultBase<int>))]
        public IActionResult SaveReport([FromBody] SaveReportModel saveReportModel)
        {
            var result = 0;
            if (!ModelState.IsValid) throw new Exception(ModelState.GetErrorsMessage());
            var userId = HttpContext.User.Identity.GetUserId();
            saveReportModel.UserId = userId;
            if (_projectService.CheckHasPermissionProject(saveReportModel.HashId, userId, PermissionCode.CREATE_REPORT))
            {
                result = _projectReportService.SaveReportAll(saveReportModel, userId);
            }
            else throw new Exception("Bạn không có quyền thực hiện tính năng này");
            return Json(new { success = true, data = result });
        }

        /// <summary>
        /// Gửi báo cáo cho trưởng ban chỉ huy công trình phê duyệt
        /// </summary>
        /// <returns></returns>
        [HttpPost("/api/v1/project-report/send-to-site-manager/{hashId}")]
        [Authorize]
        [SwaggerResponse((int)System.Net.HttpStatusCode.OK, Type = typeof(ResultBase<bool>))]
        public IActionResult SendingToSiteManager([FromBody] SendToSiteManagerModel model, [FromRoute] string hashId)
        {
            var result = false;
            if (!ModelState.IsValid) throw new Exception(ModelState.GetErrorsMessage());
            var userId = HttpContext.User.Identity.GetUserId();
            if (_projectService.CheckHasPermissionProject(hashId, userId, PermissionCode.SUBMIT_REPORTS))
            {
                result = _projectReportService.SubmitReport(model, userId);
            }
            else throw new Exception("Bạn không có quyền thực hiện tính năng này");

            return Json(new { success = result });
        }

        /// <summary>
        /// Hiển thị danh sách báo cáo từ các bộ phận khác cho trưởng ban chỉ huy công trình
        /// </summary>
        /// <returns></returns>
        [HttpGet("/api/v1/project-report/list-report-for-site-manager")]
        [Authorize]
        [SwaggerResponse((int)System.Net.HttpStatusCode.OK, Type = typeof(ResultBase<IEnumerable<ProjectReportModel>>))]
        public IActionResult ListReportForSiteManager()
        {
            if (!ModelState.IsValid) throw new Exception(ModelState.GetErrorsMessage());
            var result = _projectReportService.ListReportForSiteManager();
            return Json(new { success = result });
        }

        /// <summary>
        /// Phê duyệt báo cáo
        /// </summary>
        /// <param name="approveReportModel"></param>
        /// <param name="hashId"></param>
        /// <returns></returns>
        [HttpPost("/api/v1/project-report/approve-report/{hashId}")]
        [Authorize]
        [SwaggerResponse((int)System.Net.HttpStatusCode.OK, Type = typeof(ResultBase<bool>))]
        public IActionResult ApproveReport([FromBody] ApproveReportModel approveReportModel, [FromRoute] string hashId)
        {
            var userId = HttpContext.User.Identity.GetUserId();
            if (_projectService.CheckHasPermissionProject(hashId, userId, PermissionCode.APPROVE_REPORTS))
            {
                if (!ModelState.IsValid) throw new Exception(ModelState.GetErrorsMessage());
                approveReportModel.UserId = userId;
                var result = _projectReportService.ApproveReport(approveReportModel);
            }
            else throw new Exception("Bạn không có quyền thực hiện tính năng này");
            return Json(new { success = true });
        }

        /// <summary>
        /// Phê duyệt báo cáo
        /// </summary>
        /// <param name="rejectReportModel"></param>
        /// <param name="hashId"></param>
        /// <returns></returns>
        [HttpPost("/api/v1/project-report/reject-report/{hashId}")]
        [Authorize]
        [SwaggerResponse((int)System.Net.HttpStatusCode.OK, Type = typeof(ResultBase<bool>))]
        public IActionResult RejectReport([FromBody] RejectReportModel rejectReportModel, [FromRoute] string hashId)
        {
            var userId = HttpContext.User.Identity.GetUserId();
            if (_projectService.CheckHasPermissionProject(hashId, userId, PermissionCode.APPROVE_REPORTS))
            {
                if (!ModelState.IsValid) throw new Exception(ModelState.GetErrorsMessage());
                rejectReportModel.UserId = userId;
                var result = _projectReportService.RejectReport(rejectReportModel);
            }
            else throw new Exception("Bạn không có quyền thực hiện tính năng này");
            return Json(new { success = true });
        }

        //[HttpGet("/api/v1/test2610")]
        //public IActionResult test()
        //{
        //    return new PhysicalFileResult(@"C:\Users\hung\Desktop\Mock Images\images.jpg", "image/jpeg");
        //}

        /// <summary>
        /// Xóa báo cáo
        /// </summary>
        /// <param name="deleteReportModel"></param>
        /// <param name="hashId"></param>
        /// <returns></returns>
        [HttpPost("/api/v1/project-report/delete-report/{hashId}")]
        [Authorize]
        [SwaggerResponse((int)System.Net.HttpStatusCode.OK, Type = typeof(ResultBase<bool>))]
        public IActionResult DeleteReport([FromBody] DeleteReportModel deleteReportModel, [FromRoute] string hashId)
        {
            if (!ModelState.IsValid) throw new Exception(ModelState.GetErrorsMessage());
            var userId = HttpContext.User.Identity.GetUserId();
            var result = false;
            if (_projectService.CheckHasPermissionProject(hashId, userId, PermissionCode.DELETE_REPORTS)
                || _projectReportService.CheckRightpersonReports(userId, deleteReportModel.ProjectReportId))
            {
                var isCommandLead = (_projectService.CheckHasPermissionProject(hashId, userId, PermissionCode.DELETE_REPORTS));
                result = _projectReportService.DeleteReport(deleteReportModel, isCommandLead);
            }
            else throw new Exception("Bạn không có quyền thực hiện tính năng này");
            return Json(new { success = result });
        }

        /// <summary>
        /// Xóa mục tiến độ khỏi báo cáo
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost("/api/v1/project-report/delete-report-plan")]
        [Authorize]
        [SwaggerResponse((int)System.Net.HttpStatusCode.OK, Type = typeof(ResultBase<bool>))]
        public IActionResult DeleteReportPlan([FromBody] DeleteProjectReportPlanModel model)
        {
            if (!ModelState.IsValid) throw new Exception(ModelState.GetErrorsMessage());
            var result = _projectReportService.DeleteReportPlan(model);
            return Json(new { success = result });
        }


        //[Authorize]
        //[HttpDelete("/api/v1/project-report/remove-workitem-report-image")]
        //[SwaggerResponse((int)System.Net.HttpStatusCode.OK, Type = typeof(ResultBase<bool>))]
        //public IActionResult RemoveWorkitemReportImage([FromBody]List<int> ImageIds)
        //{
        //    if (!ModelState.IsValid) throw new Exception(ModelState.GetErrorsMessage());
        //    var userId = HttpContext.User.Identity.GetUserId();
        //    var result = _projectReportService.RemoveWorkitemReportImage(ImageIds, userId);
        //    return Json(new { success = result });
        //}

        /// <summary>
        /// Xóa mục tiến độ khỏi báo cáo
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost("/api/v1/project-report/delete-report-workitem")]
        [Authorize]
        [SwaggerResponse((int)System.Net.HttpStatusCode.OK, Type = typeof(ResultBase<bool>))]
        public IActionResult DeleteReportWorkitem([FromBody] DeleteWorkitemReportModel model)
        {
            if (!ModelState.IsValid) throw new Exception(ModelState.GetErrorsMessage());
            var result = _projectReportService.DeleteReportItem(model);
            return Json(new { success = result });
        }



        /// <summary>
        /// Kiểm tra tiến độ tổng thể của dự án( Trả về % hiện tại của dự án, trạng thái nhanh chậm, số chênh lệch % nhanh chậm)
        /// </summary>
        /// <param name="checkProjectProgress"></param>     
        /// <returns></returns>
        [HttpGet("/api/v1/project-report/project/{hashId}/check-progress-expected")]
        [Authorize]
        [SwaggerResponse((int)System.Net.HttpStatusCode.OK, Type = typeof(ResultBase<ProgressResult>))]
        public IActionResult CheckProgressProjectExpected(CheckProjectProgressModel checkProjectProgress)
        {
            if (!ModelState.IsValid) throw new Exception(ModelState.GetErrorsMessage());
            var result = _projectReportService.CheckProgressProjectExpected(checkProjectProgress);
            return Json(new { success = true, data = result });
        }

        /// <summary>
        /// Trả về số lượng dự án đạt, vượt, chậm tiến độ
        /// </summary>
        /// <returns></returns>
        [HttpGet("/api/v1/prject-report/check-all-project-progress")]
        [Authorize]
        [SwaggerResponse((int)System.Net.HttpStatusCode.OK, Type = typeof(ResultBase<NumberProjectProgressResult>))]
        public IActionResult CheckAllProjectProgress()
        {
            var result = _projectReportService.CheckAllProjectProgress();
            return Json(new { success = true, data = result });
        }


        /// <summary>
        /// Kiểm tra tiến độ theo plan( Trả về % hiện tại của plan, trạng thái nhanh chậm, số chênh lệch % nhanh chậm)
        /// </summary>
        /// <param name="planId"></param>
        /// <returns></returns>
        [HttpGet("/api/v1/project-report/check-plan-progress")]
        [Authorize]
        [SwaggerResponse((int)System.Net.HttpStatusCode.OK, Type = typeof(ResultBase<ProgressResult>))]
        public IActionResult CheckProgressPlan(Guid planId)
        {
            if (!ModelState.IsValid) throw new Exception(ModelState.GetErrorsMessage());
            var result = _projectReportService.CheckProgressPlanExpected(planId);
            return Json(new { success = true, data = result });
        }


        //[HttpPost("/api/v1/project-report/update-plan-percentage")]
        //[Authorize]
        //[SwaggerResponse((int)System.Net.HttpStatusCode.OK, Type = typeof(ResultBase<>))]
        //public IActionResult UpdatePlanPercentage(ProjectPlanModel projectPlanModel)
        //{
        //    var result = _projectReportService.UpdatePlanPercentage(projectPlanModel);
        //    return Json(new { success = true, data = result });
        //}

        /// <summary>
        /// Tạo ảnh báo cáo( trả về list id của ảnh báo cáo)
        /// </summary>
        /// <param name="hashId"></param>
        /// <param name="mediaModel"></param>
        /// <returns></returns>
        [HttpPost("/api/v1/project-report/project/{hashId}/create-picture")]
        [Authorize]
        [SwaggerResponse((int)System.Net.HttpStatusCode.OK, Type = typeof(ResultBase<bool>))]
        public IActionResult CreateReportPicture(string hashId, MediaModel mediaModel)
        {
            if (!ModelState.IsValid) throw new Exception(ModelState.GetErrorsMessage());
            var userId = HttpContext.User.Identity.GetUserId();
            mediaModel.ProjectId = _hashidService.Decode(hashId);
            var result = _projectReportService.CreateReportImage(mediaModel, userId);
            return Json(new { success = true, data = result });
        }


        /// <summary>
        /// Tính tiến độ hạng mục hoàn thành theo từng tháng( dựa vào báo cáo) của tất cả các dự án và kế hoạch sản lượng theo tháng( dashboard)
        /// </summary>
        /// <returns></returns>
        [HttpGet("/api/v1/project-report/workitem-comleted-number-per-month")]
        [Authorize]
        [SwaggerResponse((int)System.Net.HttpStatusCode.OK, Type = typeof(ResultBase<List<WorkitemPerMonthModel>>))]
        public ActionResult<IEnumerable<WorkitemPerMonthModel>> WorkitemReportPerMonth()
        {
            int year = DateTime.UtcNow.Year;
            var result = _projectReportService.WorkitemReportPerMonth(year);
            //return Json(new { success = true, data = result });
            return result.Item1.ToList();
        }

        /// <summary>
        /// Tính tiến độ hạng mục hoàn thành theo từng quý( dựa vào báo cáo) của tất cả các dự án và kế hoạch sản lượng theo quý( dashboard)
        /// </summary>
        /// <returns></returns>
        [HttpGet("/api/v1/project-report/workitem-comleted-number-per-quarter")]
        [Authorize]
        [SwaggerResponse((int)System.Net.HttpStatusCode.OK, Type = typeof(ResultBase<List<WorkitemPerQuarterModel>>))]
        public ActionResult<IEnumerable<WorkitemPerQuarterModel>> WorkitemReportPerQuarter()
        {
            int year = DateTime.UtcNow.Year;
            var result = _projectReportService.WorkitemReportPerQuarter(year);
            //return Json(new { success = true, data = result });
            return result.ToList();
        }

        /// <summary>
        /// Danh sách ảnh theo từng báo cáo
        /// </summary>
        /// <param name="projectReportId"></param>
        /// <returns></returns>
        [HttpGet("/api/v1/project-report/list-images-by-reportId")]
        [SwaggerResponse((int)System.Net.HttpStatusCode.OK, Type = typeof(ResultBase<List<BinaryFileStorageModel>>))]
        public IActionResult ListImageReportByReportId(int projectReportId)

        {
            if (!ModelState.IsValid) throw new Exception(ModelState.GetErrorsMessage());
            var result = _projectReportService.ListReportImageByReportId(projectReportId);
            return Json(new { success = true, data = result });
        }

        /// <summary>
        /// Danh sách ảnh theo từng báo cáo hạng mục
        /// </summary>
        [HttpGet("/api/v1/project-report/list-images-by-workitemReportId")]
        [SwaggerResponse((int)System.Net.HttpStatusCode.OK, Type = typeof(ResultBase<List<BinaryFileStorageModel>>))]
        public IActionResult ListImageByWorkitemReportId(int workitemReportId)
        {
            if (!ModelState.IsValid) throw new Exception(ModelState.GetErrorsMessage());
            var result = _projectReportService.ListImageByWorkitemReportId(workitemReportId);
            return Json(new { success = true, data = result });
        }

        /// <summary>
        /// Nhân bản báo cáo( trả về id báo cáo mới)
        /// </summary>
        /// <param name="reportModel"></param>
        /// <returns></returns>   
        [Authorize]
        [HttpPost("/api/v1/project-report/clone-report")]
        [SwaggerResponse((int)System.Net.HttpStatusCode.OK, Type = typeof(ResultBase<int>))]
        public IActionResult CloneReport([FromBody] CloneReportModel reportModel)
        {
            var result = 0;
            if (HttpContext.User.HasPermission(PermissionCode.CREATE_REPORT))
            {
                if (!ModelState.IsValid) throw new Exception(ModelState.GetErrorsMessage());
                var userId = HttpContext.User.Identity.GetUserId();
                reportModel.UserId = userId;
                result = _projectReportService.CloneReport(reportModel);
            }
            else throw new Exception("Bạn không có quyền thực hiện tính năng này");
            return Json(new { success = true, data = result });
        }

        /// <summary>
        /// Test viết chữ lên ảnh
        /// </summary>
        /// <param name="image"></param>
        /// <returns></returns>
        [HttpGet("/api/v1/project-report/test-write-text-on-picture")]
        [SwaggerResponse((int)System.Net.HttpStatusCode.OK, Type = typeof(ResultBase<bool>))]
        public IActionResult TestWriteText(string image)
        {
            var result = _projectReportService.WriteTextImage(image);
            return File(result, "image/jpeg");
        }

        /// <summary>
        /// Tổng hợp nhân lực thi công theo từng đơn vị trong mỗi công trình
        /// </summary>
        /// <param name="check"></param>
        /// <returns></returns>
        [HttpPost("/api/v1/project-report/statistical-human-resource")]
        //[Authorize]
        [SwaggerResponse((int)System.Net.HttpStatusCode.OK, Type = typeof(ResultBase<StatisticsHumanResourceModel>))]
        public IActionResult StatisticalHumanResource([FromBody]CheckStatiscalHumanResource check)
        {
            var result = _projectReportService.StatisticalHumanResource(check);
            return Json(new { success = true, data = result });
        }

        [HttpGet("/api/v1/project-report/auto-fill-with-company/{hashId}/{companyId}")]
        [Authorize]
        [SwaggerResponse((int)System.Net.HttpStatusCode.OK, Type = typeof(ResultBase<AutoFillReportWithCompanyModel>))]
        public IActionResult AuToFillReportWithCompany(string hashId, int? companyId)
        {
            var result = _projectReportService.AuToFillReportWithCompany(hashId, companyId);
            return Json(new { success = true, data = result });
        }

        [HttpPost("/api/v1/project-report/{reportId}/marked-read")]
        [Authorize]
        [SwaggerResponse((int)System.Net.HttpStatusCode.OK, Type = typeof(ResultBase<bool>))]
        public IActionResult MarkedReadReport(int reportId)
        {
            var userId = HttpContext.User.Identity.GetUserId();
            var result = _projectReportService.MarkedReadReport(reportId, userId);
            return Json(new { success = true, data = result });
        }

        //[HttpGet("/api/v1/project-report/show-report-in-dashboard")]
        //[Authorize]
        //[SwaggerResponse((int)System.Net.HttpStatusCode.OK, Type = typeof(ResultBase<IEnumerable<ShowReportOnDardboardModel>>))]
        //public IActionResult ShowReportInDardboard()
        //{
        //    var userId = HttpContext.User.Identity.GetUserId();
        //    var result = _projectReportService.ShowReportInDardboard(userId);
        //    return Json(new { success = true, data = result });
        //}


        [HttpGet("/api/v1/project-report/test-place")]
        [SwaggerResponse((int)System.Net.HttpStatusCode.OK, Type = typeof(ResultBase<bool>))]
        public IActionResult MarkedReadReport()
        {
            //var input = new List<string>();
            //input.Add("01");
            //input.Add("02");
            //input.Add("01");
            //input.Add("01");
            //input.Add("02");
            //var t = _projectReportService.JoinStringPlaceCollection(input.ToArray());

            var k = "02 -  06 - 77";
            int CalculateMarkerBeetweenTwoPlace(string input)
            {
                var result = 0;
                var str = input.Replace(" ", "");

                var test = str.Split(new char[] { '-' }, StringSplitOptions.RemoveEmptyEntries).Select(c => Convert.ToInt32(c)).ToArray();

                result = Math.Abs((test[0] - test[1])) + 1;
                return result;
            }

           
            //ValidatePlace(k);

            return Json(new { success = true, data = CalculateMarkerBeetweenTwoPlace(k) });
        }


    }
}
