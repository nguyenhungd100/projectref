﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using PCC1.PMS.Api.Configuration;
using PCC1.PMS.Api.Helpers;
using PCC1.PMS.Domain.Models;
using PCC1.PMS.Domain.Models.Companies;
using PCC1.PMS.Domain.Models.File;
using PCC1.PMS.Domain.Models.Projects;
using PCC1.PMS.Domain.Models.ProjectUsers;
using PCC1.PMS.Domain.Models.Users;
using PCC1.PMS.Domain.Services.Common;
using PCC1.PMS.Domain.Services.ProjectReports;
using PCC1.PMS.Domain.Services.Projects;
using PCC1.PMS.Domain.Services.ProjectUsers;
using PCC1.PMS.Domain.Services.RegisterMaterials;
using PCC1.PMS.Domain.Services.Roles;
using Serilog;
using Swashbuckle.AspNetCore.Annotations;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace PCC1.PMS.Api.Modules.Projects
{
    [Route("api/v1/[controller]")]
    public partial class ProjectController : Controller
    {
        private readonly IOptions<AppSettings> _settings;
        private readonly IHashidService _hashidService;
        private readonly IProjectService _projectService;
        private readonly INotificationService _notificationService;
        private readonly IRegisterMaterialsService _registerMaterialsService;
        private readonly IProjectReportService _projectReportService;

        public ProjectController(IOptions<AppSettings> settings,
            IHashidService hashidService,
            IProjectService projectService,
            INotificationService notificationService,
            IRegisterMaterialsService registerMaterialsService,
            IProjectReportService projectReportService)
        {
            _settings = settings;
            _hashidService = hashidService;
            _projectService = projectService;
            _notificationService = notificationService;
            _registerMaterialsService = registerMaterialsService;
            _projectReportService = projectReportService;
        }

        /// <summary>
        /// Tìm kiếm dự án
        /// </summary>
        /// <returns></returns>
        [HttpPost("/api/v1/project/search")]
        [Authorize]
        [SwaggerResponse((int)HttpStatusCode.OK, Type = typeof(ResultBase<ProjectModel>))]
        public ActionResult Search([FromBody] SearchProjectModel projectSearchModel)
        {
            int userCurrent = 0;
            if (HttpContext.User.HasPermission(PermissionCode.ACCESS_ALL_PROJECTS))
            {
                userCurrent = HttpContext.User.Identity.GetUserId();
            }
            else
            {
                projectSearchModel.UserId = HttpContext.User.Identity.GetUserId();
            }
            var result = _projectService.Search(projectSearchModel, userCurrent);
            return Json(new { success = true, data = result });
        }

        /// <summary>
        /// Lấy thông tin dự án theo mã
        /// </summary>
        /// <param name="hashId"></param>
        /// <returns></returns>
        [HttpGet("/api/v1/project/{hashId}")]
        [Authorize]
        [SwaggerResponse((int)System.Net.HttpStatusCode.OK, Type = typeof(ResultBase<ProjectModel>))]
        public async Task<IActionResult> GetProjectById(string hashId)
        {
            var userId = HttpContext.User.Identity.GetUserId();


            var test1 = _projectService.CheckHasPermissionProject(hashId, userId, PermissionCode.ACCESS_ALL_PROJECTS);
            var test2 = _projectService.GetProjectRole(hashId, userId);


            var result = await _projectService.GetByHashId(hashId);
            return Json(new { success = true, data = result });
        }


        /// <summary>
        /// Cập nhật thông tin dự án
        /// </summary>
        /// <param name="hashId"></param>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPut("/api/v1/project/{hashId}/update-project-info")]
        [Authorize(Policy = nameof(PermissionCode.UPDATE_PROJECT_INFO))]
        [SwaggerResponse((int)System.Net.HttpStatusCode.OK, Type = typeof(ResultBase<bool>))]
        public async Task<IActionResult> UpdateProject(string hashId, [FromBody]UpdateProjectModel model)
        {
            var result = false;
            var userId = HttpContext.User.Identity.GetUserId();
            if (_projectService.CheckHasPermissionProject(hashId, userId, PermissionCode.UPDATE_PROJECT_INFO))
            {
                if (!ModelState.IsValid) throw new Exception(ModelState.GetErrorsMessage());
                model.ProjectId = _hashidService.Decode(hashId);
                result = await _projectService.UpdateProject(model);
            }
            else throw new Exception("Bạn không có quyền thực hiện tính năng này");
            return Json(new { success = result });
        }

        [HttpPost("/api/v1/project/{hashId}/update-companies-in-project")]
        [Authorize]
        [SwaggerResponse((int)System.Net.HttpStatusCode.OK, Type = typeof(ResultBase<bool>))]
        public IActionResult UpdateCompaniesInProject(string hashId, [FromBody]UpdateCompanyInProject model)
        {
            var result = false;
            var userId = HttpContext.User.Identity.GetUserId();
            if (_projectService.CheckHasPermissionProject(hashId, userId, PermissionCode.UPDATE_COMPANIES_IN_PROJECT))
            {
                if (!ModelState.IsValid) throw new Exception(ModelState.GetErrorsMessage());
                model.ProjectId = _hashidService.Decode(hashId);
                result = _projectService.UpdateCompaniesInProject(model);
            }
            else throw new Exception("Bạn không có quyền thực hiện tính năng này");
            return Json(new { success = result });
        }

        /// <summary>
        /// Khởi tạo thông tin cơ bản cho dự án
        /// </summary>
        /// <param name="projectAddModel"></param>
        /// <returns></returns>

        [HttpPost("/api/v1/project/add_project")]
        [Authorize]
        [SwaggerResponse((int)System.Net.HttpStatusCode.OK, Type = typeof(ResultBase<>))]
        public IActionResult InitProject([FromBody] ProjectAddModel projectAddModel)
        {
            var result = new ProjectModel();
            if (HttpContext.User.HasPermission(PermissionCode.INIT_PROJECT))
            {
                if (!ModelState.IsValid) throw new Exception(ModelState.GetErrorsMessage());
                projectAddModel.CreatedBy = HttpContext.User.Identity.GetUserId();
                result = _projectService.InitInfoProject(projectAddModel);
            }
            else throw new Exception("Bạn không có quyền thực hiện tính năng này");
            return Json(new { success = true, data = result });
        }

        /// <summary>
        /// Xóa dự án
        /// </summary>
        /// <param name="hashId"></param>
        /// <returns></returns>
        [HttpDelete("/api/v1/project/{hashId}/delete")]
        [Authorize]
        [SwaggerResponse((int)HttpStatusCode.OK, Type = typeof(ResultBase<bool>))]
        public IActionResult DeleteProject(string hashId)
        {
            var result = false;
            if (HttpContext.User.HasPermission(PermissionCode.DELETE_PROJECT))
            {
                if (!ModelState.IsValid) throw new Exception(ModelState.GetErrorsMessage());
                result = _projectService.DeleteProject(_hashidService.Decode(hashId));
            }
            else throw new Exception("Bạn không có quyền thực hiện tính năng này");
            return Json(new { success = true, data = result });
        }

        /// <summary>
        /// Lấy danh sách nhân sự theo dự án
        /// </summary>
        /// <param name="hashId"></param>
        /// <returns></returns>
        [Authorize]
        [HttpGet("/api/v1/project/{hashId}/members")]
        [SwaggerResponse((int)System.Net.HttpStatusCode.OK, Type = typeof(ResultBase<List<UserModel>>))]
        public IActionResult GetMembers(string hashId)
        {
            var pid = _hashidService.Decode(hashId);
            var res = _projectService.GetProjectUsers(pid);
            return Json(new { success = true, data = res });
        }

        /// <summary>
        /// Thêm 1 nhân sự cho dự án
        /// </summary>
        /// <param name="hashId"></param>
        /// <param name="members"></param>
        /// <returns></returns>
        [HttpPost("/api/v1/project/{hashId}/add-member")]
        [Authorize]
        [SwaggerResponse((int)System.Net.HttpStatusCode.OK, Type = typeof(ResultBase<bool>))]
        public async Task<IActionResult> AddMember(string hashId, [FromBody] List<AddProjectMembersModel> members)
        {
            var result = false;
            var userId = HttpContext.User.Identity.GetUserId();
            if (_projectService.CheckHasPermissionProject(hashId, userId, PermissionCode.SETUP_PROJECT_USERS))
            {
                if (!ModelState.IsValid) throw new Exception(ModelState.GetErrorsMessage());
                foreach (var member in members)
                {
                    member.ProjectId = _hashidService.Decode(hashId);
                    member.CurrentUserId = HttpContext.User.Identity.GetUserId();
                    result = await _projectService.AddMember(member);
                }
            }
            else throw new Exception("Bạn không có quyền thực hiện tính năng này");
            return Json(new { success = result });
        }

        /// <summary>
        /// Update 1 personnal from Project
        /// </summary>
        /// <param name="projectUserModel"></param>
        /// <returns></returns>
        [HttpPut("/api/v1/project/update-personnal")]
        [Authorize]
        [SwaggerResponse((int)System.Net.HttpStatusCode.OK, Type = typeof(ResultBase<bool>))]
        public async Task<IActionResult> UpdatePersonnalProject([FromBody]ProjectUserModel projectUserModel)
        {
            var result = false;
            var userId = HttpContext.User.Identity.GetUserId();
            var hashId = _hashidService.Encode(projectUserModel.ProjectId);
            if (_projectService.CheckHasPermissionProject(hashId, userId, PermissionCode.SETUP_PROJECT_USERS))
            {
                if (!ModelState.IsValid) throw new Exception(ModelState.GetErrorsMessage());
                result = await _projectService.UpdatePersonnal(projectUserModel);
            }

            return Json(new { success = result });
        }

        /// <summary>
        /// Update many personnal from Project
        /// </summary>
        /// <param name="projectUserModels"></param>
        /// <returns></returns>
        [HttpPut("/api/v1/project/update-multi-personnal")]
        [Authorize]
        [SwaggerResponse((int)System.Net.HttpStatusCode.OK, Type = typeof(ResultBase<bool>))]
        public async Task<IActionResult> UpdateMultiPersonnalProject([FromBody]List<ProjectUserModel> projectUserModels)
        {

            if (!ModelState.IsValid) throw new Exception(ModelState.GetErrorsMessage());

            var userId = HttpContext.User.Identity.GetUserId();
            var hashId = _hashidService.Encode(projectUserModels[0].ProjectId);
            var result = false;

            if (_projectService.CheckHasPermissionProject(hashId, userId, PermissionCode.SETUP_PROJECT_USERS))
            {
                result = await _projectService.UpdateMultiPersonnal(projectUserModels);
            }
            else throw new Exception("Bạn không có quyền thực hiện tính năng này");
            return Json(new { success = result });
        }

        /// <summary>
        /// Xóa nhiều nhân sự từ 1 dự án
        /// </summary>
        /// <param name="hashId"></param>
        /// <param name="deleteProjectUserModel"></param>
        /// <returns></returns>
        [HttpDelete("/api/v1/project/{hashId}/remove-members")]
        [Authorize]
        [SwaggerResponse((int)System.Net.HttpStatusCode.OK, Type = typeof(ResultBase<bool>))]
        public IActionResult RemoveProjectMembers(string hashId, [FromBody] RemoveProjectMembersModel deleteProjectUserModel)
        {
            if (!ModelState.IsValid) throw new Exception(ModelState.GetErrorsMessage());
            var userId = HttpContext.User.Identity.GetUserId();
            var result = false;
            if (_projectService.CheckHasPermissionProject(hashId, userId, PermissionCode.SETUP_PROJECT_USERS))
            {
                var pid = _hashidService.Decode(hashId);
                deleteProjectUserModel.ProjectId = pid;
                deleteProjectUserModel.ExecutingUserId = HttpContext.User.Identity.GetUserId();
                result = _projectService.RemoveProjectMembers(deleteProjectUserModel);
            }
            else throw new Exception("Bạn không có quyền thực hiện tính năng này");
            return Json(new { success = result });
        }

        /// <summary>
        /// Cập nhật tổng thể dự án từ cán bộ quản lý dự án( tổng tiến độ công trình, tổng giá trị hợp đồng, vị trí)
        /// </summary>
        /// <param name="projectOverallModel"></param>
        /// <returns></returns>
        [HttpPost("/api/v1/project/update-overall-project")]
        [Authorize]
        [SwaggerResponse((int)System.Net.HttpStatusCode.OK, Type = typeof(ResultBase<bool>))]
        public async Task<IActionResult> UpdateOverallProject([FromBody]ProjectOverallModel projectOverallModel)
        {
            var result = false;
            if (!ModelState.IsValid) throw new Exception(ModelState.GetErrorsMessage());
            var hashId = _hashidService.Encode(projectOverallModel.Id);
            var userId = HttpContext.User.Identity.GetUserId();
            if (_projectService.CheckHasPermissionProject(hashId, userId, PermissionCode.UPDATE_PROJECT_INFO))
            {
                result = await _projectService.UpdateOverallProject(projectOverallModel, userId);
            }
            else throw new Exception("Bạn không có quyền thực hiện tính năng này");
            return Json(new { success = result });
        }

        /// <summary>
        /// Kiểm tra dự án có được phê duyệt hay không
        /// </summary>
        /// <param name="hashId"></param>
        /// <returns></returns>
        [HttpGet("/api/v1/project/{hashId}/check-ability-approving-project")]
        [Authorize]
        [SwaggerResponse((int)HttpStatusCode.OK, Type = typeof(ResultBase<bool>))]
        public IActionResult CheckAbilityApprovingProject(string hashId)
        {
            if (!ModelState.IsValid) throw new Exception(ModelState.GetErrorsMessage());
            var projectId = _hashidService.Decode(hashId);
            var result = _projectService.CheckAbilityApprovingProject(projectId);
            return Json(new { success = result });
        }

        /// <summary>
        /// Phê duyệt dự án từ phía tổng công ty
        /// </summary>
        /// <param name="hashId"></param>
        /// <returns></returns>
        [HttpPost("/api/v1/project/{hashId}/approve")]
        [Authorize]
        [SwaggerResponse((int)HttpStatusCode.OK, Type = typeof(ResultBase<bool>))]
        public IActionResult ApproveProject(/*ApproveProjectModel approveProjectModel*/ string hashId)
        {
            var userId = HttpContext.User.Identity.GetUserId();
            var result = false;
            if (_projectService.CheckHasPermissionProject(hashId, userId, PermissionCode.UPDATE_PROJECT_INFO))
            {
                if (!ModelState.IsValid) throw new Exception(ModelState.GetErrorsMessage());
                var projectId = _hashidService.Decode(hashId);
                var approveUser = HttpContext.User.Identity.GetUserId();
                result = _projectService.ApproveProject(projectId, approveUser);
            }
            else throw new Exception("Bạn không có quyền thực hiện tính năng này");
            return Json(new { success = result });
        }

        /// <summary>
        /// Thêm mới tiến độ chi tiết( mpp file) từ trưởng ban chỉ huy công trình
        /// </summary>
        /// <param name="model"></param>
        /// <param name="hashId"></param>
        /// <returns></returns>
        [HttpPost("/api/project/{hashId}/upload-plan-file")]
        [Authorize]
        [SwaggerResponse((int)HttpStatusCode.OK, Type = typeof(ResultBase<bool>))]
        public async Task<IActionResult> UploadPlanFile(string hashId, UploadPlanFileModel model)
        {
            var success = false;
            var userId = HttpContext.User.Identity.GetUserId();
            if (_projectService.CheckHasPermissionProject(hashId, userId, PermissionCode.UPDATE_PROJECT_PLAN))
            {
                var projectId = _hashidService.Decode(hashId);
                var parseResult = string.Empty;

                //if (model.MPPFile.ContentType != "application/vnd.ms-project")
                //if(!model.MPPFile.FileName.EndsWith(".mpp")) throw new Exception("File này không phải file Microft Project");
                //else
                //{
                #region call parsing api
                string folder = @"C:\PCC1-Temp\";
                try
                {
                    Directory.CreateDirectory(folder);
                    using (FileStream output = new FileStream(folder + model.MPPFile.FileName, FileMode.Create))
                    {
                        model.MPPFile.OpenReadStream().CopyTo(output);
                    }
                    Guid guidPlan = Guid.NewGuid();
                    Uri uri = new Uri(_settings.Value.MPPApi + "?fileId=" + guidPlan + " &fileName= " + model.MPPFile.FileName + "");
                    HttpWebRequest myRequest = (HttpWebRequest)WebRequest.Create(uri);
                    myRequest.Method = "GET";
                    HttpWebResponse response = (HttpWebResponse)myRequest.GetResponse();
                    using (StreamReader streamReader = new StreamReader(response.GetResponseStream()))
                    {
                        parseResult = streamReader.ReadToEnd().ToString();
                    }
                }
                catch (Exception ex)
                {
                    Log.Error(ex.ToString());
                    throw ex;
                }
                #endregion
                var planParsingModel = JsonConvert.DeserializeObject<PlanParsingModel>(parseResult);

                success = await _projectService.NewPlans(projectId, userId, planParsingModel);
            }
            else throw new Exception("Bạn không có quyền truy cập tính năng này");
            return Json(new { success });
        }

        /// <summary>
        /// Cập nhật plan
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost("/api/v1/project/update-plan")]
        [Authorize]
        [SwaggerResponse((int)HttpStatusCode.OK, Type = typeof(ResultBase<bool>))]
        public IActionResult UpdatePlan([FromBody]UpdatePlanModel model)
        {
            var result = false;
            var userId = HttpContext.User.Identity.GetUserId();
            var hashId = model.HashId;
            if (_projectService.CheckHasPermissionProject(hashId, userId, PermissionCode.UPDATE_PROJECT_PLAN))
            {
                if (!ModelState.IsValid) throw new Exception(ModelState.GetErrorsMessage());
                model.ProjectId = _hashidService.Decode(model.HashId);
                result = _projectService.UpdatePlan(model);
            }
            else throw new Exception("Bạn không có quyền thực hiện tính năng này");
            return Json(new { success = result });
        }

        /// <summary>
        /// Lấy số lần cập nhật plan cuối của dự án
        /// </summary>
        /// <param name="hashId"></param>
        /// <returns></returns>
        [HttpGet("/api/v1/project/{hashId}/last-plans-no")]
        [Authorize]
        [SwaggerResponse((int)HttpStatusCode.OK, Type = typeof(ResultBase<int>))]
        public IActionResult GetProjectPlanByProjectId(string hashId)
        {
            var pid = _hashidService.Decode(hashId);
            var result = _projectService.CheckLastPlanNo(pid);
            return Json(new { success = true, data = result });
        }

        /// <summary>
        /// Lấy danh sách plan theo dự án và số lần cập nhật plan cuối
        /// </summary>
        /// <param name="hashId"></param>
        /// <param name="no"></param>
        /// <returns></returns>
        [HttpGet("/api/v1/project/{hashId}/plans")]
        [HttpGet("/api/v1/project/{hashId}/plans/{no}")]
        [Authorize]
        [SwaggerResponse((int)HttpStatusCode.OK, Type = typeof(ResultBase<List<ProjectPlanModel>>))]
        public async Task<IActionResult> GetProjectPlanByProjectId(string hashId, int? no)
        {
            var pid = _hashidService.Decode(hashId);
            var result = await _projectService.GetProjectPlan(pid, no);
            return Json(new { success = true, data = result });
        }

        /// <summary>
        /// Lấy danh sách plan theo dự án và số lần cập nhật plan cuối theo biểu đồ gantt
        /// </summary>
        /// <param name="hashId"></param>
        /// <param name="no"></param>
        /// <returns></returns>
        [HttpGet("/api/v1/project/{hashId}/plans-with-gantt")]
        [HttpGet("/api/v1/project/{hashId}/plans-with-gantt/{no}")]
        [Authorize]
        [SwaggerResponse((int)HttpStatusCode.OK, Type = typeof(ResultBase<List<ProjectPlanResulModel>>))]
        public IActionResult GetProjectPlanByProjectIdWithGantt(string hashId, int? no)
        {
            var pid = _hashidService.Decode(hashId);
            var result = _projectService.GetProjectPlanWithGantt(pid, no);
            return Json(new { success = true, data = result });
        }

        /// <summary>
        /// Lấy danh sách plan theo parentId
        /// </summary>
        /// <param name="parentId"></param>
        /// <returns></returns>
        [HttpGet("/api/v1/project/plans/parent/{parentId}")]
        [Authorize]
        [SwaggerResponse((int)HttpStatusCode.OK, Type = typeof(ResultBase<List<ProjectPlanResulModel>>))]
        public async Task<IActionResult> GetProjectPlanByParentId(Guid? parentId)
        {
            var result = await _projectService.GetProjectPlanByParentId(parentId);
            return Json(new { success = true, data = result });
        }

        /// <summary>
        /// Lấy thông tin plan theo Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("/api/v1/project/plan/{id}")]
        [Authorize]
        [SwaggerResponse((int)HttpStatusCode.OK, Type = typeof(ResultBase<List<ProjectPlanModel>>))]
        public async Task<IActionResult> GetProjectPlanById(Guid? id)
        {
            if (!ModelState.IsValid) throw new Exception(ModelState.GetErrorsMessage());
            var result = await _projectService.GetProjectPlanById(id);
            return Json(new { success = true, data = result });
        }

        /// <summary>
        /// Cho phép 1 plan được phép hiển thị trên thiết bị di động
        /// </summary>
        /// <param name="allowShowOnMobileModel"></param>
        /// <returns></returns>
        [HttpPost("/api/v1/project-plans/allow-show-on-mobile")]
        [Authorize]
        [SwaggerResponse((int)HttpStatusCode.OK, Type = typeof(ResultBase<bool>))]
        public IActionResult AllowShowOnMobile([FromBody]AllowShowOnMobileModel allowShowOnMobileModel)
        {
            if (!ModelState.IsValid) throw new Exception(ModelState.GetErrorsMessage());
            var userId = HttpContext.User.Identity.GetUserId();
            var result = _projectService.AllowShowOnMobile(allowShowOnMobileModel, userId);
            return Json(new { success = result });
        }

        /// <summary>
        /// Cho phép nhiều plan hiển thị trên thiết bị di động
        /// </summary>
        /// <param name="allowShowOnMobileModels"></param>
        /// <returns></returns>
        [HttpGet("/api/v1/project-plans/allow-show-many-plan-on-mobile")]
        [Authorize]
        [SwaggerResponse((int)HttpStatusCode.OK, Type = typeof(ResultBase<bool>))]
        public IActionResult AllowShowManyPlanOnMobile(List<AllowShowOnMobileModel> allowShowOnMobileModels)
        {
            if (!ModelState.IsValid) throw new Exception(ModelState.GetErrorsMessage());
            var userId = HttpContext.User.Identity.GetUserId();
            var result = _projectService.AllowManyPlanShowOnMobile(allowShowOnMobileModels, userId);
            return Json(new { success = result });
        }
        /// <summary>
        /// Thêm mới plan
        /// </summary>
        /// <param name="projectPlanModel"></param>
        /// <returns></returns>
        [HttpPost("/api/v1/project-plans/add-plan")]
        [Authorize]
        [SwaggerResponse((int)HttpStatusCode.OK, Type = typeof(ResultBase<bool>))]
        public async Task<IActionResult> AddProjectPlan([FromBody]ProjectPlanModel projectPlanModel)
        {
            if (!ModelState.IsValid) throw new Exception(ModelState.GetErrorsMessage());
            var userId = HttpContext.User.Identity.GetUserId();
            var result = await _projectService.AddProjectPlan(projectPlanModel, userId);
            return Json(new { success = result });
        }

        /// <summary>
        /// Xóa plan
        /// </summary>
        /// <param name="projectPlanModel"></param>
        /// <returns></returns>
        [HttpDelete("/api/v1/project-plans/delete")]
        [Authorize]
        [SwaggerResponse((int)HttpStatusCode.OK, Type = typeof(ResultBase<bool>))]
        public async Task<IActionResult> DeleteProjectPlan([FromBody] ProjectPlanModel projectPlanModel)
        {
            if (!ModelState.IsValid) throw new Exception(ModelState.GetErrorsMessage());
            var userId = HttpContext.User.Identity.GetUserId();
            var result = await _projectService.DeleteProjectPlan(projectPlanModel, userId);
            return Json(new { success = result });
        }

        /// <summary>
        /// Tạo mới thư mục cho dự án
        /// </summary>
        /// <param name="hashId"></param>
        /// <param name="projectFolderModel"></param>
        /// <returns></returns>
        [HttpPost("/api/v1/project/{hashId}/create-folder")]
        [Authorize]
        [SwaggerResponse((int)HttpStatusCode.OK, Type = typeof(ResultBase<bool>))]
        public IActionResult CreateFolderForProject(string hashId, [FromBody]ProjectFolderModel projectFolderModel)
        {
            if (!ModelState.IsValid) throw new Exception(ModelState.GetErrorsMessage());
            projectFolderModel.ProjectId = _hashidService.Decode(hashId);
            var userId = HttpContext.User.Identity.GetUserId();
            var result = _projectService.CreateFolder(projectFolderModel, userId);
            return Json(new { success = result.Item1 });
        }

        [HttpPost("/api/v1/project/delete-folder/{hashId}")]
        [Authorize]
        [SwaggerResponse((int)HttpStatusCode.OK, Type = typeof(ResultBase<bool>))]
        public IActionResult DeleteFolder(string folderId, string hashId)
        {
            var result = false;
            var userId = HttpContext.User.Identity.GetUserId();
            if (_projectService.CheckHasPermissionProject(hashId, userId, PermissionCode.DELETE_FOLDER))
            {             
                result = _projectService.DeleteFolder(folderId);
            }
            else throw new Exception("Bạn không có quyền thực hiện tính năng này");
            return Json(new { success = result });
        }


        /// <summary>
        /// Lấy thư mục theo dự án và đường dẫn
        /// </summary>
        /// <param name="hashId"></param>
        /// <param name="parentId"></param>
        /// <returns></returns>
        [HttpGet("/api/v1/project/{hashId}/folders")]
        [HttpGet("/api/v1/project/{hashId}/folders/{parentId}")]
        [Authorize]
        [SwaggerResponse((int)HttpStatusCode.OK, Type = typeof(ResultBase<List<ProjectFolderModel>>))]
        public async Task<IActionResult> GetFolders(string hashId, Guid? parentId)
        {

            var pid = _hashidService.Decode(hashId);
            var folders = await _projectService.GetFolders(pid, parentId);
            return Json(new { success = true, data = folders });
        }




        /// <summary>
        /// Lấy danh sách kế hoạch cấp 1
        /// </summary>
        /// <param name="hashId"></param>
        /// <param name="no"></param>
        /// <returns></returns>
        [Authorize]
        [HttpGet("/api/v1/project/{hashId}/list-plan-level-1")]
        [SwaggerResponse((int)HttpStatusCode.OK, Type = typeof(ResultBase<List<ProjectPlanModel>>))]
        public IActionResult GetListLevel1(string hashId, int? no)
        {
            var pid = _hashidService.Decode(hashId);
            var result = _projectService.GetListPlanLevel1(pid, no);
            return Json(new { success = true, data = result });
        }

        /// <summary>
        /// Kiểm tra user đăng nhập có tồn tại trong dự án
        /// </summary>
        /// <param name="hashId"></param>
        /// <returns></returns>
        [HttpGet("/api/v1/project/{hashId}/check-exist-user-current-login")]
        [Authorize]
        [SwaggerResponse((int)HttpStatusCode.OK, Type = typeof(ResultBase<bool>))]
        public IActionResult ExistUserCurrentInProject(string hashId)
        {
            var pid = _hashidService.Decode(hashId);
            var userId = HttpContext.User.Identity.GetUserId();
            var result = _projectService.ExistUserCurrentInProject(pid, userId);
            return Json(new { success = true, data = result });
        }


        /// <summary>
        /// Hoàn thành dự án
        /// </summary>
        /// <param name="hashid"></param>
        /// <returns></returns>
        [HttpPost("/api/v1/project/{hashid}/finishing-the-project")]
        [Authorize]
        [SwaggerResponse((int)HttpStatusCode.OK, Type = typeof(ResultBase<bool>))]
        public IActionResult FinishingTheProject(string hashid)
        {
            var result = false;
            var userId = HttpContext.User.Identity.GetUserId();
            if (_projectService.CheckHasPermissionProject(hashid, userId, PermissionCode.FINISHING_PROJECT))              
            {
                var projectId = _hashidService.Decode(hashid);
                result = _projectService.FinishingTheProject(projectId);
            }
            else throw new Exception("Bạn không có quyền truy cập tính năng này");
            return Json(new { success = result });
        }

        /// <summary>
        /// Thông tin thư mục
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("/api/v1/get-info-folder")]
        [SwaggerResponse((int)HttpStatusCode.OK, Type = typeof(ResultBase<InfoFolderModel>))]
        public IActionResult GetInfoFoler(string id)
        {
            var res = _projectService.GetInfoFoler(id);
            return Json(new { success = true, data = res });
        }


        /// <summary>
        /// Danh sách tất cả các dự án
        /// </summary>
        /// <returns></returns>
        [Authorize]
        [HttpGet("/api/v1/get-list-project")]
        [SwaggerResponse((int)HttpStatusCode.OK, Type = typeof(ResultBase<IEnumerable<ProjectInfoMiniModel>>))]
        public IActionResult ListProjects()
        {
            var res = _projectService.ListProjects();
            return Json(new { success = true, data = res });
        }

        /// <summary>
        /// Chuyển trạng thái dự án thành chưa duyệt( dành cho admin)
        /// </summary>
        [Authorize]
        [HttpPost("/api/v1/change-project-status-for-admin")]
        [SwaggerResponse((int)HttpStatusCode.OK, Type = typeof(ResultBase<bool>))]
        public IActionResult ChangeStatusProjectToInit([FromBody] ChangeProjectStatusModel statusModel)
        {
            var result = false;
            if (HttpContext.User.HasPermission(PermissionCode.CHANGE_PROJECT_STATUS))
            {
                statusModel.ProjectId = _hashidService.Decode(statusModel.HashId);
                result = _projectService.ChangeStatusProjectToInit(statusModel);
            }
            else throw new Exception("bạn không có quyền thực hiện tính năng này");
            return Json(new { success = result });
        }

        /// <summary>
        /// Lấy quyền trong dự án
        /// </summary>
        [Authorize]
        [HttpPost("/api/v1/get-project-roles")]
        [SwaggerResponse((int)HttpStatusCode.OK, Type = typeof(ResultBase<string>))]
        public IActionResult GetProjectRole(string hashId)
        {
            var access = HttpContext.User.HasPermission(PermissionCode.ACCESS_ALL_PROJECTS);
            var userId = HttpContext.User.Identity.GetUserId();
            var result = _projectService.GetProjectRole(hashId, userId);
            return Json(new { success = true, data = result.Item1, permissions = result.Item2,
                accessProject = (access ? true : result.Item3), isBoss = result.Item4, companyId =  result.Item5});
        }
    }

}

