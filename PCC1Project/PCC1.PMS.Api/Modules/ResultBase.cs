﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PCC1.PMS.Api.Modules
{
    public class ResultBase<T>
    {
        public bool success { set; get; }
        public T data { set; get; }
        public string message { set; get; }
    }
}