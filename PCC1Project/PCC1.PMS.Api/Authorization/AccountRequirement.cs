﻿using Microsoft.AspNetCore.Authorization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using PCC1.PMS.Domain.Services.Roles;

namespace PCC1.PMS.Api.Authorization
{
    public class PermissionRequirement : AuthorizationHandler<PermissionRequirement>, IAuthorizationRequirement
    {
        public PermissionCode Permission { get; }

        public PermissionRequirement(PermissionCode permission)
        {
            Permission = permission;
        }

        protected override async Task HandleRequirementAsync(AuthorizationHandlerContext context, PermissionRequirement requirement)
        {
            // Your logic here... or anything else you need to do.
            //if (context.User.IsInRole("Agent"))
            //{
            //    context.Succeed(requirement);
            //    return;
            //}

            if (context.User == null)
            {
                // no user authorizedd. Alternatively call context.Fail() to ensure a failure 
                // as another handler for this requirement may succeed
                context.Fail();
                return;
            }

            var myPermission = context.User.Claims.FirstOrDefault(c => c.Type == "permissions");
            if (myPermission == null)
            {
                context.Fail();
                return;
            }

            bool hasPermission = myPermission.Value.Contains(";" + requirement.Permission.GetHashCode() + ";");
            if (hasPermission)
            {
                context.Succeed(requirement);
            }
            else
            {
                context.Fail();
            }
        }
    }
}
