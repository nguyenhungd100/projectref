﻿using System;
using Microsoft.AspNetCore.Mvc;
using PCC1.PMS.Domain.Services.Users;
using Serilog;

namespace PCC1.PMS.Api.Controllers
{
    [ApiExplorerSettings(IgnoreApi = true)]
    public class DeployController : ControllerBase
    {
        public ActionResult<string> Deploy()
        {
            System.Diagnostics.Process.Start(@"C:\Deployments\PCC1\API-RELEASE.bat");

            return "API begin deploying...";
        }
    }
}
