﻿using Dapper;
using MicroOrm.Dapper.Repositories;
using Microsoft.AspNetCore.Http;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using PCC1.PMS.Domain.Entity;
using PCC1.PMS.Domain.Enums;
using PCC1.PMS.Domain.Models.Companies;
using PCC1.PMS.Domain.Models.EstimationPlans;
using PCC1.PMS.Domain.Models.Notification;
using PCC1.PMS.Domain.Models.Projects;
using PCC1.PMS.Domain.Models.Revenues;
using PCC1.PMS.Domain.Models.Statistical;
using PCC1.PMS.Domain.Models.Statistical.StatisticalActuals;
using PCC1.PMS.Domain.Models.WorkitemByCompany;
using PCC1.PMS.Domain.Models.Workitems;
using PCC1.PMS.Domain.Models.Workitems.AccumulateCompleteMonth;
using PCC1.PMS.Domain.Models.Workitems.AccumulateCompleteQuarter;
using PCC1.PMS.Domain.Repositories;
using PCC1.PMS.Domain.Services.Common;
using PCC1.PMS.Domain.Services.Projects;
using PCC1.PMS.Domain.Shared;
using PCC1.PMS.Framework.Extensions;
using PCC1.PMS.Framework.Utils;
using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace PCC1.PMS.Domain.Services.WorkItems.Implementation
{
    public class WorkItemService : IWorkItemService
    {
        public readonly IWorkitemRepository _workitemRepository;
        public readonly IDapperRepository<Project> _projectRepository;
        public readonly IDapperRepository<User> _userRepository;
        public readonly IProjectService _projectService;
        public readonly IDapperRepository<ProjectUser> _projectUserRepository;
        private readonly IDapperRepository<Company> _companyRepository;
        private readonly IDapperRepository<ProjectCompany> _projectCompanyRepository;
        private readonly IDapperRepository<SettlementWorkitem> _settlementWorkitemRepository;
        private readonly IDapperRepository<WorkitemCompany> _workitemCompanyRepository;
        private readonly IDapperRepository<EstimationPlan> _estimationPlanRepository;
        private readonly IDapperRepository<ProjectReport> _projectReportRepository;
        private readonly IDapperRepository<ProjectReportWorkitem> _projectReportWorkitemRepository;
        private readonly IDapperRepository<ProjectReportPlan> _projectReportPlanRepository;
        private readonly IDapperRepository<AssignmentError> _assignmentErrorRepository;
        public readonly IHashidService _hashIdService;

        public WorkItemService(IWorkitemRepository workItemRepository,
            IDapperRepository<Project> projectRepository,
            IProjectService projectService,
            IDapperRepository<User> userRepository,
            IDapperRepository<ProjectUser> projectUserRepository,
            IDapperRepository<WorkitemCompany> workItemCompanyRepository,
            IDapperRepository<Company> companyRepository,
            IDapperRepository<ProjectCompany> projectCompanyRepository,
            IDapperRepository<SettlementWorkitem> settlementWorkitemRepository,
            IDapperRepository<EstimationPlan> estimationPlanRepository,
            IDapperRepository<ProjectReport> projectReportRepository,
            IDapperRepository<ProjectReportWorkitem> projectReportWorkitemRepository,
            IDapperRepository<ProjectReportPlan> projectReportPlanRepository,
            IDapperRepository<AssignmentError> assignmentErrorRepository,
            IHashidService hashIdService)
        {
            _workitemRepository = workItemRepository;
            _projectRepository = projectRepository;
            _projectService = projectService;
            _userRepository = userRepository;
            _projectUserRepository = projectUserRepository;
            _companyRepository = companyRepository;
            _projectCompanyRepository = projectCompanyRepository;
            _settlementWorkitemRepository = settlementWorkitemRepository;
            _estimationPlanRepository = estimationPlanRepository;
            _projectReportRepository = projectReportRepository;
            _projectReportWorkitemRepository = projectReportWorkitemRepository;
            _projectReportPlanRepository = projectReportPlanRepository;
            _assignmentErrorRepository = assignmentErrorRepository;
            _workitemCompanyRepository = workItemCompanyRepository;
            _hashIdService = hashIdService;
        }

        public async Task<List<WorkItemModel>> ListWorkitem()
        {
            var workItems = await _workitemRepository.FindAllAsync();

            var workItemModel = from p in workItems
                                select new WorkItemModel
                                {
                                    Id = p.Id,
                                    Name = p.Name,
                                    //ListWorkitemByCompanyId,
                                    Quantity = p.Quantity,
                                    TotalPaidQuantity = p.TotalPaidQuantity.Value,
                                    CalculationUnit = p.CalculationUnit,
                                    //Money = p.Money.Value,
                                    CompletedNumberTotal = p.CompletedNumberTotal
                                };
            return workItemModel.ToList();
        }

        /// <summary>
        /// Lấy vị trí của báo cáo hạng mục đã hoàn thành
        /// </summary>
        /// <param name="workitemId"></param
        /// <param name="reportWorkitems"></param>
        /// <returns></returns>
        private string FillWorkitemReportPlace(int workitemId, IEnumerable<ProjectReportWorkitem> reportWorkitemEquals)
        {
            var result = string.Empty;
            var collections = reportWorkitemEquals.Where(a => a.CompletePlace != null).Select(c => c.CompletePlace).ToList();
            if (collections.Count() > 0)
            {
                result = JoinStringPlaceCollection(collections.ToArray());
            }
            return result;
        }



        public static string JoinStringPlaceCollection(params string[] data)
        {
            StringBuilder builder = new StringBuilder();

            var numberStrResult = new List<string>();

            int curr = 0;
            foreach (var item in data)
            {
                if (!string.IsNullOrEmpty(item))
                {
                    var str = item.Replace(" ", "");
                    str = str.TrimStart(',');
                    str = str.TrimEnd(',');

                    List<string> numberStr = str.ToString().Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries).ToList();
                    numberStrResult.AddRange(numberStr);
                    ++curr;
                }
            }
            numberStrResult = numberStrResult.Distinct().ToList();
            return string.Join(',', numberStrResult);
        }

        public List<WorkitemInfoModel> GetProjectWorkitems(int projectId)
        {
            var project = _projectRepository.FindById(projectId);
            if (project == null) throw new ServiceException("Dự án không tồn tại");
            var workitems = _workitemRepository.FindAll(t => t.ProjectId == projectId).OrderBy(c => c.RowNumber.Value);
            var result = workitems.ToList().CloneToListModels<Workitem, WorkitemInfoModel>();


            var reports = _projectReportRepository.FindAll(c => c.ProjectId == projectId && c.Status == ProjectReportStatus.Approved);
            var reportIds = reports.Select(c => c.Id);
            var reportWorkitems = _projectReportWorkitemRepository.FindAll(c => reportIds.Contains(c.ProjectReportId) && c.CompletedNumber > 0);
            foreach (var res in result)
            {
                var reportWorkitemEquals = reportWorkitems.Where(c => c.WorkitemId == res.Id);
                res.PlaceCollection = FillWorkitemReportPlace(res.Id, reportWorkitemEquals);
                res.PlaceCountCollection = StringUtil.CountItemWhenSeperate(res.PlaceCollection);
            }
            return result;
        }

        public IEnumerable<WorkItemModel> GetListChildrenWorkitems(int workitemGroupId)
        {
            //var workitems = _workitemRepository.FindAll(c => c.WorkitemGroupId == workitemGroupId);
            return null;
        }

        //public IEnumerable<WorkItemModel> GetListParentWorkitems(int projectId)
        //{
        //   // var workitems = _workitemRepository.FindAll(c => c.ProjectId == projectId && c.WorkitemGroupId == null);
        //    return DomainMaps.Mapper.Map<List<WorkItemModel>>(workitems);
        //}

        public async Task<(bool, WorkItemAddNotificationModel)> AddWorkItem(AddWorkitemModel workItemAddModel, int userId)
        {
            var success = false;
            var firebaseModel = new WorkItemAddNotificationModel();
            var existProject = _projectRepository.FindById(workItemAddModel.ProjectId);
            if (existProject == null) throw new ServiceException("Dự án này không tồn tại");
            if (existProject.Status == ProjectStatus.Approved)
                throw new ServiceException("Dự án đã được duyệt không được phép cập nhật khối lượng");

            var existUserCurrent = _projectUserRepository.Find(c => c.UserId == userId && c.ProjectId == existProject.Id);
            if (existUserCurrent == null) throw new ServiceException("Bạn không thuộc dự án này nên không có quyền");

            if (workItemAddModel.WorkitemGroupId != null)
            {
                if (workItemAddModel.Quantity <= 0) throw new ServiceException("Không được để trống số lượng công trình");
            }

            var exist = await _workitemRepository.FindAllAsync(c => (c.Name == workItemAddModel.Name && c.ProjectId == workItemAddModel.ProjectId));
            if (exist.Count() > 0)
                throw new ServiceException("Hạng mục trong dự án đã trùng, bạn muốn cập nhật vào chỉnh sửa hạng mục");

            if (workItemAddModel.WorkitemGroupId.HasValue)
            {
                var existWorkitemGroup = _workitemRepository.FindById(workItemAddModel.WorkitemGroupId.Value);
                //if (existWorkitemGroup == null || existWorkitemGroup.WorkitemGroupId != null)
                //    throw new ServiceException("Hạng mục cha không đúng");
            }

            var entity = workItemAddModel.CloneToModel<AddWorkitemModel, Workitem>();
            entity.CompletedNumberTotal = 0;
            entity.TotalPaidQuantity = 0;
            success = await _workitemRepository.InsertAsync(entity);

            //if (entity.WorkitemGroupId.HasValue)
            //{
            //    var workitemParent = _workitemRepository.FindById(workItemAddModel.WorkitemGroupId);
            //    workitemParent.Money += workItemAddModel.Quantity * workItemAddModel.Money;
            //    await _workitemRepository.UpdateAsync(workitemParent);
            //}

            var userIdInProjects = _projectUserRepository.FindAll(c => c.ProjectId == entity.ProjectId).Select(c => c.UserId).ToList();
            List<int> listCBQLDA = new List<int>();
            foreach (var userIdInProject in userIdInProjects)
            {
                if (_userRepository.FindById(userIdInProject).UserRoles.Contains("4"))
                    listCBQLDA.Add(userIdInProject);
            }

            var addUserName = _userRepository.FindById(userId).FullName != null ?
                _userRepository.FindById(userId).FullName : _userRepository.FindById(userId).Email;
            firebaseModel = new WorkItemAddNotificationModel()
            {
                AddUserId = userId,
                AddUserName = addUserName,
                WorkitemName = entity.Name,
                ProjectId = entity.ProjectId,
                ProjectName = _projectRepository.FindById(entity.ProjectId).Name,
                ListCBQLDA = listCBQLDA
            };
            return (success, firebaseModel);
        }

        public bool UpdateWorkItem(WorkItemModel workItemModel, int userId)
        {
            if (workItemModel.Id <= 0) throw new ServiceException("Chưa chọn hạng mục");

            var existProject = _projectRepository.FindById(workItemModel.ProjectId);
            if (existProject == null) throw new ServiceException("Không tồn tại dự án này");

            var existUserCurrent = _projectUserRepository.Find(c => c.UserId == userId
                && c.ProjectId == existProject.Id);
            if (existUserCurrent == null) throw new ServiceException("Bạn không thuộc dự án này nên không có quyền");

            if (existProject.Status == ProjectStatus.Approved)
                throw new ServiceException("Dự án đã được duyệt không cập nhật được hạng mục");

            var existWorkitem = _workitemRepository.Find(c => c.Id == workItemModel.Id
                && c.ProjectId == workItemModel.ProjectId);
            if (existWorkitem == null) throw new ServiceException("Không có hạng mục đó trong dự án, yêu cầu kiểm tra lại");

            var success = false;

            if (HeadingExcels.DeteminedLevelHeading(workItemModel.STT) == 0)
            {
                throw new ServiceException("Số thứ tự hạng mục không đúng");
            }
            var entity = workItemModel.CloneToModel<WorkItemModel, Workitem>();
            entity.RowNumber = existWorkitem.RowNumber;
            success = _workitemRepository.Update(entity);

            return success;
        }

        public List<WorkitemAssignmentWithHeading> GetWorkitemAssignments(int projectId, int? checkCompanyId)
        {
            var project = _projectRepository.FindById(projectId);
            if (project == null) throw new ServiceException("Dự án không tồn tại");
            List<WorkitemWithHeadingModel> workitemWithHeadings = DivideWorkitem(projectId);

            var projectCompanies = _projectCompanyRepository.FindAll(p => p.ProjectId == projectId);
            var companyIds = projectCompanies.Select(p => p.CompanyId);
            var companies = _companyRepository.FindAll(c => companyIds.Contains(c.Id));

            var workitemCompanies = from a in _workitemCompanyRepository.FindAll(c => companyIds.Contains(c.CompanyId)) select a;


            var result = new List<WorkitemAssignmentWithHeading>();

            foreach (var workitemWithHeading in workitemWithHeadings)
            {

                var category = workitemWithHeading.WorkitemCategory;
                var workitemCategory = new WorkitemAssignmentModel
                {
                    Id = category.Id,
                    STT = category.STT,
                    WorkitemName = category.Name,
                    CalculationUnit = category.CalculationUnit,
                    LevelHeading = category.LevelHeading,
                    UnitPrice = category.UnitPrice,
                    IsBold = true
                };
                if (category.Quantity.HasValue && category.UnitPrice.HasValue)
                {
                    workitemCategory.Quantity = category.Quantity;
                    workitemCategory.AssignmentCompanies = new List<AssignmentCompanyModel>();
                    foreach (var companyId in companyIds)
                    {
                        var existAssignment = workitemCompanies.Where(c => c.WorkItemId == workitemCategory.Id
                                 && c.CompanyId == companyId).FirstOrDefault();
                        if (existAssignment != null)
                        {
                            workitemCategory.AssignmentCompanies.Add(new AssignmentCompanyModel
                            {
                                CompanyId = companyId,
                                CompanyName = companies.Where(c => c.Id == companyId).FirstOrDefault().Name,
                                AssignmentNumber = existAssignment.AssignedNumber,
                                CompletedNumber = existAssignment.CompletedNumber
                            });
                        }
                        else
                        {
                            workitemCategory.AssignmentCompanies.Add(new AssignmentCompanyModel
                            {
                                CompanyId = companyId,
                                CompanyName = companies.Where(c => c.Id == companyId).FirstOrDefault().Name,
                                AssignmentNumber = 0,
                                CompletedNumber = 0
                            });
                        }
                    }
                }




                var workitemSmallers = new List<WorkitemAssignmentModel>();

                foreach (var workitem in workitemWithHeading.WorkItemSmallerModels)
                {
                    var workitemResult = new WorkitemAssignmentModel();

                    workitemResult.Id = workitem.Id;
                    workitemResult.STT = workitem.STT;
                    workitemResult.WorkitemName = workitem.Name;
                    workitemResult.CalculationUnit = workitem.CalculationUnit;
                    workitemResult.LevelHeading = workitem.LevelHeading;
                    workitemResult.UnitPrice = workitem.UnitPrice;

                    if (workitem.UnitPrice.HasValue && workitem.Quantity.HasValue)
                    {
                        workitemResult.Quantity = workitem.Quantity;
                        workitemResult.AssignmentCompanies = new List<AssignmentCompanyModel>();
                        foreach (var companyId in companyIds)
                        {
                            var existAssignment = workitemCompanies.Where(c => c.WorkItemId == workitem.Id
                                     && c.CompanyId == companyId).FirstOrDefault();
                            if (existAssignment != null)
                            {
                                workitemResult.AssignmentCompanies.Add(new AssignmentCompanyModel
                                {
                                    CompanyId = companyId,
                                    CompanyName = companies.Where(c => c.Id == companyId).FirstOrDefault().Name,
                                    AssignmentNumber = existAssignment.AssignedNumber,
                                    CompletedNumber = existAssignment.CompletedNumber
                                });
                            }
                            else
                            {
                                workitemResult.AssignmentCompanies.Add(new AssignmentCompanyModel
                                {
                                    CompanyId = companyId,
                                    CompanyName = companies.Where(c => c.Id == companyId).FirstOrDefault().Name,
                                    AssignmentNumber = 0,
                                    CompletedNumber = 0
                                });
                            }
                        }
                    }
                    workitemSmallers.Add(workitemResult);
                }


                var addHeading = new WorkitemAssignmentWithHeading()
                {
                    WorkitemAssignmentCategory = workitemCategory,
                    WorkitemAssignmentSmallers = workitemSmallers,
                    Companies = companies.ToList().CloneToListModels<Company, CompanyModel>()
                };

                foreach (var companyId in companyIds)
                {
                    addHeading.Companies.Add(new CompanyModel
                    {
                        Id = companyId,
                        Name = companies.Where(c => c.Id == companyId).FirstOrDefault().Name
                    });
                }

                result.Add(addHeading);
            }

            if (checkCompanyId.HasValue)
            {
                var filterWorkitemIds = new List<int>();
                var filterWorkitemAssignments = _workitemCompanyRepository.FindAll(c => c.CompanyId == checkCompanyId.Value && (c.AssignedNumber > 0));
                if (filterWorkitemAssignments.Count() > 0)
                {
                    filterWorkitemIds = filterWorkitemAssignments.Select(c => c.WorkItemId).ToList();
                    foreach (var workitemSmallers in result)
                    {

                        workitemSmallers.WorkitemAssignmentSmallers = workitemSmallers.WorkitemAssignmentSmallers.Where(c => filterWorkitemIds.Contains(c.Id)).ToList();

                    }
                }

            }

            return result;
        }


        protected void SetBackgroundExportAssignmentToExcel(ExcelWorksheet worksheet, string projectName, List<string> companyNames)
        {
            var maxColumn = 4 + companyNames.Count() * 2;

            worksheet.Cells.Style.WrapText = true;

            using (var range = worksheet.Cells["A:O"])
            {
                range.Style.Font.SetFromFont(new Font("Times New Roman", 12));
            }
            using (var range = worksheet.Cells["A3:O3"])
            {
                range.Style.Font.SetFromFont(new Font("Times New Roman", 13));
                range.Style.Font.Bold = true;
                range.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                range.Style.VerticalAlignment = ExcelVerticalAlignment.Center;
            }
            using (var range = worksheet.Cells["A4:O4"])
            {
                range.Style.Font.SetFromFont(new Font("Times New Roman", 13));
                range.Style.Font.Bold = true;
                range.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                range.Style.VerticalAlignment = ExcelVerticalAlignment.Center;
            }


            worksheet.Cells.Style.WrapText = true;
            worksheet.Cells[3, 1].Value = "STT";
            worksheet.Cells[3, 2].Value = "Nội dung công việc";
            worksheet.Cells[3, 3].Value = "Đơn vị";
            worksheet.Cells[3, 4].Value = "Số lượng";


            //worksheet.Cells[1, 4, 1, maxColumn].Merge = true;
            worksheet.Cells[2, 1, 2, maxColumn].Merge = true;
            worksheet.Cells[2, 1, 2, maxColumn].Style.Font.Size = 18;
            worksheet.Cells[2, 1, 2, maxColumn].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            worksheet.Cells[2, 1, 2, maxColumn].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
            worksheet.Row(2).Height = 60;


            var row11 = worksheet.Cells[2, 1].RichText.Add("Bảng phân giao khối lượng các công ty" + "\r\n");
            row11.Size = 16;
            row11.Bold = true;
            var row12 = worksheet.Cells[2, 1].RichText.Add("(Công trình: " + projectName + ")" + "\r\n");
            row12.Size = 14;
            row12.Bold = true;
            worksheet.Cells[2, 1].AutoFitColumns();

            worksheet.Row(2).Style.Font.Bold = true;
            worksheet.Row(3).Height = 40;
            worksheet.Row(4).Height = 25;
            worksheet.Column(1).Width = 10;
            worksheet.Column(1).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            worksheet.Column(2).Width = 35;
            worksheet.Column(3).Width = 15;
            worksheet.Column(3).Style.Numberformat.Format = "#,##0.00";
            worksheet.Column(3).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            worksheet.Column(4).Width = 20;
            worksheet.Column(4).Style.Numberformat.Format = "#,##0.00";
            worksheet.Column(4).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;

            worksheet.Cells[3, 1, 4, 1].Merge = true;
            worksheet.Cells[3, 2, 4, 2].Merge = true;
            worksheet.Cells[3, 3, 4, 3].Merge = true;
            worksheet.Cells[3, 4, 4, 4].Merge = true;

            int k = 1;
            foreach (var companyName in companyNames)
            {
                worksheet.Cells[3, 4 + k, 3, 4 + k + 1].Merge = true;
                worksheet.Cells[3, 4 + k].Value = companyName;
                worksheet.Cells[3, 4 + k].Style.Font.SetFromFont(new Font("Times New Roman", 13));
                worksheet.Cells[3, 4 + k].Style.Font.Bold = true;
                worksheet.Cells[4, 4 + k].Value = "Hoàn thành";
                worksheet.Cells[4, 4 + k].Style.Font.SetFromFont(new Font("Times New Roman", 11));
                worksheet.Cells[4, 4 + k].Style.Font.Bold = true;
                worksheet.Cells[4, 4 + k + 1].Value = "Phân giao";
                worksheet.Cells[4, 4 + k + 1].Style.Font.SetFromFont(new Font("Times New Roman", 11));
                worksheet.Cells[4, 4 + k + 1].Style.Font.Bold = true;
                worksheet.Column(4 + k).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                worksheet.Column(4 + k).Width = 20;
                worksheet.Column(4 + k + 1).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                worksheet.Column(4 + k + 1).Width = 20;
                k = k + 2;
            }
            worksheet.View.FreezePanes(5, 1);
            TitleCompanyExcel.FillTitleCommanderWorks(worksheet, projectName, maxColumn, 2, 2);



        }

        public (MemoryStream, string) DowloadExcelAssignments(int projectId, int? checkCompanyId)
        {
            var project = _projectRepository.FindById(projectId);
            if (project == null) throw new ServiceException("Dự án không tồn tại");
            var dataAssignments = GetWorkitemAssignments(projectId, checkCompanyId);

            var stream = new MemoryStream();
            using (var excelPackage = new ExcelPackage(stream))
            {
                ExcelWorksheet worksheet = excelPackage.Workbook.Worksheets.Add("Bảng phân giao khối lượng");
                var companyNames = new List<string>();
                void LookCompanyName()
                {
                    foreach (var item in dataAssignments)
                    {
                        foreach (var check in item.WorkitemAssignmentSmallers)
                        {

                            if (check.AssignmentCompanies != null)
                            {
                                if (check.AssignmentCompanies.Count() > 0)
                                {
                                    check.AssignmentCompanies.ForEach(c =>
                                    {
                                        companyNames.Add(c.CompanyName);
                                    });
                                    return;
                                }
                            }

                        }
                    }
                }
                LookCompanyName();

                SetBackgroundExportAssignmentToExcel(worksheet, project.Name, companyNames);
                int i = 5;
                foreach (var dataAssignment in dataAssignments)
                {

                    worksheet.Cells[i, 1].Value = dataAssignment.WorkitemAssignmentCategory.STT;
                    worksheet.Cells[i, 2].Value = dataAssignment.WorkitemAssignmentCategory.WorkitemName;
                    worksheet.Cells[i, 3].Value = dataAssignment.WorkitemAssignmentCategory.CalculationUnit;
                    worksheet.Cells[i, 4].Value = dataAssignment.WorkitemAssignmentCategory.Quantity;
                    worksheet.Cells[i, 4].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;

                    if (dataAssignment.WorkitemAssignmentCategory.Quantity.HasValue
                        && dataAssignment.WorkitemAssignmentCategory.UnitPrice.HasValue)
                    {
                        if (dataAssignment.WorkitemAssignmentCategory.AssignmentCompanies != null)
                        {
                            if (dataAssignment.WorkitemAssignmentCategory.AssignmentCompanies.Count() > 0)
                            {
                                int q = 4;
                                foreach (var assignment in dataAssignment.WorkitemAssignmentCategory.AssignmentCompanies)
                                {
                                    worksheet.Cells[i, q + 1].Value = assignment.CompletedNumber;
                                    worksheet.Cells[i, q + 1 + 1].Value = assignment.AssignmentNumber;
                                    q = q + 2;
                                }
                            }
                        }
                    }

                    worksheet.Row(i).Style.Font.Bold = true;
                    if (dataAssignment.WorkitemAssignmentSmallers.Count() > 0)
                    {
                        worksheet.Row(i).Style.Font.Bold = true;
                        i = i + 1;

                        foreach (var item in dataAssignment.WorkitemAssignmentSmallers)
                        {
                            worksheet.Cells[i, 1].Value = item.STT;
                            worksheet.Cells[i, 2].Value = item.WorkitemName;
                            worksheet.Cells[i, 3].Value = item.CalculationUnit;
                            worksheet.Cells[i, 4].Value = item.Quantity;
                            worksheet.Cells[i, 4].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;

                            if (item.AssignmentCompanies != null)
                            {
                                if (item.AssignmentCompanies.Count() > 0)
                                {
                                    int q = 4;
                                    foreach (var assignment in item.AssignmentCompanies)
                                    {
                                        worksheet.Cells[i, q + 1].Value = assignment.CompletedNumber;
                                        worksheet.Cells[i, q + 1 + 1].Value = assignment.AssignmentNumber;
                                        q = q + 2;
                                    }
                                }
                            }
                            i++;
                        }
                        i = i - 1;
                    }
                    i++;
                }

                stream = new MemoryStream(excelPackage.GetAsByteArray());
            }
            return (stream, project.Name);
        }

        public async Task<WorkItemModel> WorkItemById(int id)
        {
            if (id <= 0) throw new ServiceException("Chưa chọn hạng mục");
            var entity = await _workitemRepository.FindByIdAsync(id);
            return entity.CloneToModel<Workitem, WorkItemModel>();
        }

        public bool DeleteWorkItem(DeleteWorkitemModel workItemDeleteModel, int userId)
        {
            var result = false;
            var trans = _workitemRepository.BeginTransaction();
            if (trans.Connection.State != ConnectionState.Open) trans.Connection.Open();
            try
            {
                if (workItemDeleteModel.Id <= 0) throw new ServiceException("Chưa chọn hạng mục");
                var existWorkItem = _workitemRepository.FindById(workItemDeleteModel.Id, trans);

                if (existWorkItem == null) throw new ServiceException("Hạng mục này không tồn tại");
                var existProject = _projectRepository.FindById(existWorkItem.ProjectId, trans);
                if (existProject.Status == ProjectStatus.Approved) throw new ServiceException("Dự án đã được duyệt nên không xóa được hạng mục");

                result = _workitemRepository.Delete(existWorkItem, trans);

                if (!result) trans.Rollback();
            }
            catch (Exception ex)
            {
                trans.Rollback();
                throw ex;
            }
            trans.Commit();

            return result;
        }



        public static ExcelRange ColumExcelContainString(ExcelWorksheet worksheet, int column, string inputString)
        {

            return worksheet.Cells[column, worksheet.Cells.First(c => StringUtil.RemoveWhitespace(c.Text.ToString()).ToLower().Contains(inputString.ToLower()))
                            .Start.Column];
        }
        private static List<WorkItemExcelModel> ReadFileExcelForImportWorkitem(ExcelWorksheet worksheet)
        {
            var workItemExcels = new List<WorkItemExcelModel>();
            int totalRows = worksheet.Dimension.End.Row;

            var index = 0;
            for (int i = 1; i <= totalRows; i++)
            {
                var spaceRow = worksheet.Cells[i, worksheet.Cells.First(c => c.Text.ToString().Contains("STT"))
                              .Start.Column].Text.ToString();
                if (HeadingExcels.IsHeading1(spaceRow))
                {
                    index = i;
                    break;
                }
            }

            for (int i = 1; i <= totalRows; i++)
            {

                if (i >= index && ColumExcelContainString(worksheet, i, "Nộidungcôngviệc").Text.Length > 0)
                {
                    ////var spaceRow = worksheet.Cells[i, worksheet.Cells.First(c => c.Text.ToString().Contains("STT"))
                    ////          .Start.Column].Text.ToString();
                    //if (worksheet.Cells[i, worksheet.Cells.First(c => c.Text.ToString().Contains("Nội dung công việc"))
                    //        .Start.Column].Text.Length > 0

                    var modelAdd = new WorkItemExcelModel();
                    if (ColumExcelContainString(worksheet, i, "STT").Text.Length > 0)
                    {
                        modelAdd.STT = ColumExcelContainString(worksheet, i, "STT").Text.ToString();
                    }

                    modelAdd.Name = ColumExcelContainString(worksheet, i, "Nộidungcôngviệc").Text.ToString();

                    if (ColumExcelContainString(worksheet, i, "Đơnvị").Text.Length > 0)
                    {
                        modelAdd.CalculationUnit = ColumExcelContainString(worksheet, i, "Đơnvị").Text.ToString();
                    }


                    var test = ColumExcelContainString(worksheet, i, "Khốilượng").Text;
                    if (ColumExcelContainString(worksheet, i, "Khốilượng").Text.Length > 0)
                    {
                        decimal quantity = 0;
                        //ColumExcelContainString(worksheet, i, "Khốilượng").Style.Numberformat.Format = "#,##0.00000000;(#,##0.00000000)";

                        if (decimal.TryParse(ColumExcelContainString(worksheet, i, "Khốilượng").Value.ToString(), out quantity) || ColumExcelContainString(worksheet, i, "Khối lượng").Text.ToString() == "-")
                        {
                            if (quantity < 0)
                            {
                                throw new ServiceException("Số lượng hạng mục dòng" + i + " trong file excel không được bé hơn 0");
                            }
                        }
                        else throw new ServiceException("Số lượng hạng mục dòng" + i + " trong file excel yêu cầu nhập số");


                        modelAdd.Quantity = quantity;
                    }

                    if (ColumExcelContainString(worksheet, i, "Đơngiá").Text.Length > 0)
                    {
                        decimal unitprice = 0;
                        if (decimal.TryParse(ColumExcelContainString(worksheet, i, "Đơngiá").Value.ToString(), out unitprice))
                        {
                            if (unitprice < 0)
                            {
                                throw new ServiceException("Đơn giá dòng " + i + " trong file excel không được bé hơn 0");
                            }
                        }
                        else throw new ServiceException("Đơn giá dòng " + i + " trong file excel yêu cầu nhập số");

                        modelAdd.UnitPrice = string.IsNullOrEmpty(ColumExcelContainString(worksheet, i, "Đơngiá").Value.ToString())
                            ? new decimal(0) : (decimal.Parse(ColumExcelContainString(worksheet, i, "Đơngiá").Value.ToString(),
                            NumberStyles.Number));
                    }

                    if (ColumExcelContainString(worksheet, i, "Thànhtiền").Text.Length > 0)
                    {
                        var t = ColumExcelContainString(worksheet, i, "Thànhtiền").Text.ToString();
                        decimal intoMoney = 0;
                        //ColumExcelContainString(worksheet, i, "Thànhtiền").Style.Numberformat.Format = "#,##0.00;(#,##0.00)";
                        if (decimal.TryParse(ColumExcelContainString(worksheet, i, "Thànhtiền").Value.ToString(), out intoMoney))
                        {
                            if (intoMoney < 0)
                            {
                                throw new ServiceException("Thành tiền dòng " + i + " trong file excel không được bé hơn 0");
                            }
                        }
                        else throw new ServiceException("Thành tiền dòng " + i + " trong file excel yêu cầu nhập số");
                        modelAdd.IntoMoney = string.IsNullOrEmpty(ColumExcelContainString(worksheet, i, "Thànhtiền").Value.ToString())
                            ? new decimal(0) : (decimal.Parse(ColumExcelContainString(worksheet, i, "Thànhtiền").Value.ToString(),
                            NumberStyles.Number));
                    }
                    modelAdd.LevelHeading = HeadingExcels.DeteminedLevelHeading(modelAdd.STT);

                    modelAdd.RowNumber = i;
                    workItemExcels.Add(modelAdd);
                }
                else continue;
            }

            var result = workItemExcels;
            return result;
        }

        private Workitem ConvertExcelToObjectWhenImportWorkitem(WorkItemExcelModel model, int projectId)
        {

            var test1 = !String.IsNullOrEmpty(model.Name) ? model.Name : null;
            var test3 = !String.IsNullOrEmpty(model.CalculationUnit) ? model.CalculationUnit : null;
            var test4 = !string.IsNullOrEmpty(model.Quantity.Value.ToString()) ? model.Quantity.Value : 0;
            var workitem = new Workitem();

            workitem.Name = !String.IsNullOrEmpty(model.Name) ? model.Name : null;
            workitem.CalculationUnit = !String.IsNullOrEmpty(model.CalculationUnit)
                           ? model.CalculationUnit : null;
            workitem.Quantity = !string.IsNullOrEmpty(model.Quantity.Value.ToString()) ? model.Quantity.Value : 0;
            //workitem.Money = !string.IsNullOrEmpty(model.Money.Value.ToString()) ? model.Money.Value : 0;
            //workitem.Money =  ? model.Money.Value : 0;
            workitem.ProjectId = projectId;
            workitem.CompletedNumberTotal = 0;
            workitem.TotalPaidQuantity = 0;
            return workitem;
        }

        public bool ImportExcelWorkItems(IFormFile formFile, int projectId, int userId)
        {
            var success = false;
            if (formFile == null) throw new ServiceException("Yêu cầu nhập file excel để cập nhật");
            if (formFile.ContentType == "application/vnd.ms-excel"
                || formFile.ContentType == "application/vnd.openxmlformats-officedocument." +
                "spreadsheetml.sheet")
            {
                var existProject = _projectRepository.FindById(projectId);
                if (existProject.Status == ProjectStatus.Approved)
                    throw new ServiceException("Dự án đã được duyệt không được cập nhật khối lượng");
                if (existProject != null)
                {

                    var stream = formFile.OpenReadStream();
                    var package = new ExcelPackage(stream);
                    ExcelWorksheet worksheet = package.Workbook.Worksheets[0];

                    var workItemExcels = new List<WorkItemExcelModel>();

                    // Phần đọc excel chính
                    workItemExcels = ReadFileExcelForImportWorkitem(worksheet);


                    var workitemExists = _workitemRepository.FindAll(c => c.ProjectId == projectId);

                    // Đánh dấu level heading
                    //workItemExcels.ForEach(m => { m.LevelHeading = HeadingExcels.DeteminedLevelHeading(m.STT); });

                    var updateStatus = false;
                    var deleteStatus = false;
                    var deleteReports = false;
                    var reports = _projectReportRepository.FindAll(c => c.ProjectId == projectId);
                    var reportIds = reports.Select(c => c.Id);
                    var reportPlanExists = _projectReportPlanRepository.FindAll(c => reportIds.Contains(c.ProjectReportId));
                    var reportWorkitemExists = _projectReportWorkitemRepository.FindAll(c => reportIds.Contains(c.ProjectReportId));
                    var trans = _workitemRepository.BeginTransaction();
                    if (trans.Connection.State != ConnectionState.Open) trans.Connection.Open();
                    try
                    {

                        if (workitemExists.ToList().Count() > 0)
                        {
                            foreach (var workitemExist in workitemExists)
                            {
                                deleteStatus = _workitemRepository.Delete(workitemExist, trans);
                                if (!deleteStatus) break;
                            }
                        }
                        else deleteStatus = true;

                        if (reports.Count() > 0)
                        {

                            foreach (var report in reports)
                            {
                                var deletePlans = false;
                                var deleteWorkitems = false;
                                var deleteReport = false;
                                var reportPlans = reportPlanExists.Where(c => c.ProjectReportId == report.Id);
                                if (reportPlans.Count() > 0)
                                {
                                    foreach (var reportPlan in reportPlans)
                                    {
                                        deletePlans = _projectReportPlanRepository.Delete(reportPlan, trans);
                                        if (!deletePlans) break;
                                    }
                                }
                                else deletePlans = true;
                                var reportWorkitems = reportWorkitemExists.Where(c => c.ProjectReportId == report.Id);
                                if (reportWorkitems.Count() > 0)
                                {
                                    foreach (var reportWorkitem in reportWorkitems)
                                    {
                                        deleteWorkitems = _projectReportWorkitemRepository.Delete(reportWorkitem, trans);
                                        if (!deleteWorkitems) break;
                                    }
                                }
                                else deleteWorkitems = true;
                                deleteReport = _projectReportRepository.Delete(report, trans);
                                if ((!deletePlans) || (!deleteWorkitems) || (!deleteReport))
                                {
                                    deleteReports = false;
                                    break;
                                }
                                else deleteReports = true;
                            }
                        }
                        else deleteReports = true;



                        foreach (var m in workItemExcels)
                        {
                            //var exist = workitemExists.Where(c => c.Name.Equals(m.Name) && c.LevelHeading == m.LevelHeading);
                            ////Trùng tên hạng mục và levelheading thì update, chưa tồn tại thì insert hạng mục
                            //if (workitemExists.Count() > 0 && exist.Count() > 0)
                            //{
                            //    var workitem = exist.FirstOrDefault();
                            //    workitem.Name = m.Name;                         
                            //    workitem.Quantity = m.Quantity;
                            //    workitem.CalculationUnit = m.CalculationUnit;
                            //    workitem.UnitPrice = m.UnitPrice;
                            //    workitem.IntoMoney = m.IntoMoney;
                            //    workitem.RowNumber = m.RowNumber;
                            //    updateStatus = _workitemRepository.Update(workitem, trans);
                            //    if (!updateStatus) break;
                            //}
                            //else
                            //{
                            var insertWorkitem = new Workitem
                            {
                                STT = string.IsNullOrEmpty(m.STT) ? null : m.STT,
                                Name = m.Name,
                                CalculationUnit = string.IsNullOrEmpty(m.CalculationUnit) ? null : m.CalculationUnit,
                                Quantity = m.Quantity.HasValue ? m.Quantity : null,
                                UnitPrice = m.UnitPrice.HasValue ? m.UnitPrice : null,
                                IntoMoney = m.IntoMoney.HasValue ? m.IntoMoney : null,
                                ProjectId = projectId,
                                TotalPaidQuantity = 0,
                                CompletedNumberTotal = 0,
                                LevelHeading = m.LevelHeading,
                                RowNumber = m.RowNumber
                            };
                            updateStatus = _workitemRepository.Insert(insertWorkitem, trans);
                            if (!updateStatus) break;
                            //}
                        }

                        success = (updateStatus && deleteStatus && deleteReports);

                        if (!success) trans.Rollback();
                    }
                    catch (Exception ex)
                    {
                        trans.Rollback();
                        throw ex;
                    }
                    trans.Commit();

                }
                else throw new ServiceException("Dự án không tồn tại");
            }
            else throw new ServiceException("File này không phải file excel");
            _projectService.SendingAbilityApproveProjectToUserCreated(projectId);
            return success;
        }

        private static DataTable ToDataTable(ExcelWorksheet ws, bool hasHeaderRow = true)
        {
            var tbl = new DataTable();
            foreach (var firstRowCell in ws.Cells[1, 1, 1, ws.Dimension.End.Column])
                tbl.Columns.Add(hasHeaderRow ?
                    firstRowCell.Text : string.Format("Column {0}", firstRowCell.Start.Column));
            var startRow = hasHeaderRow ? 2 : 1;
            for (var rowNum = startRow; rowNum <= ws.Dimension.End.Row; rowNum++)
            {
                var wsRow = ws.Cells[rowNum, 1, rowNum, ws.Dimension.End.Column];
                var row = tbl.NewRow();
                foreach (var cell in wsRow) row[cell.Start.Column - 1] = cell.Text;
                tbl.Rows.Add(row);
            }
            return tbl;
        }

        //public int CalPayWorkItemPercentage(int totalQuanlity, int totalPay)
        //{
        //    return (int)totalPay / totalQuanlity;
        //}


        public bool SettlementWorkitem(IEnumerable<PerformingSettlementModel> settlementModels)
        {
            var trans = _settlementWorkitemRepository.BeginTransaction();
            if (trans.Connection.State != System.Data.ConnectionState.Open) trans.Connection.Open();
            var success = false;
            try
            {
                foreach (var settlementModel in settlementModels)
                {
                    var workitem = _workitemRepository.FindById(settlementModel.WorkitemId, trans);
                    var projectStatus = _projectRepository.FindById(workitem.ProjectId, trans).Status;
                    if (projectStatus != ProjectStatus.Approved) throw new ServiceException("Dự án chưa được phê duyêt");
                    if (workitem == null) throw new ServiceException("Không tồn tại hạng mục này");

                    //if (workitem.WorkitemGroupId == null) throw new ServiceException("Hạng mục cha không được phép thanh toán");

                    var existUserCurrent = _projectUserRepository.Find(c => c.UserId == settlementModel.PerformingUser && c.ProjectId == workitem.ProjectId, trans);
                    if (existUserCurrent == null) new ServiceException("Bạn không thuộc dự án này nên không có quyền");

                    var payWorkitems = _settlementWorkitemRepository.FindAll(c => c.WorkitemId == settlementModel.WorkitemId
                        && c.Type == SettlementType.SettlementTimes, trans);

                    //var companyDelivery = _workitemCompanyRepository.Find(c => 
                    //    c.WorkItemId == settlementModel.WorkitemId, trans);

                    //var company = _companyRepository.FindById(settlementModel.CompanyId, trans);
                    //if (companyDelivery == null || companyDelivery.AssignedNumber == 0)
                    //    throw new ServiceException("Công ty " + company.Name + " chưa được phân giao cho hạng mục " + workitem.Name);

                    var action = false;
                    SettlementWorkitem payWorkitem = null;
                    if (settlementModel.Times.HasValue)
                        payWorkitem = _settlementWorkitemRepository.FindAll(c => c.WorkitemId == settlementModel.WorkitemId &&
                            c.Times == settlementModel.Times && c.Type == SettlementType.SettlementTimes, trans).FirstOrDefault();

                    decimal caclQuantity = 0;
                    if (payWorkitem != null)
                    {
                        caclQuantity = payWorkitems.Select(c => c.SettlementNumber).Sum() + (settlementModel.Quantity - payWorkitem.SettlementNumber);
                        payWorkitem.WorkitemId = settlementModel.WorkitemId;
                        payWorkitem.CompanyId = settlementModel.CompanyId;
                        payWorkitem.Times = settlementModel.Times.Value;
                        payWorkitem.SettlementNumber = settlementModel.Quantity;
                        payWorkitem.Type = settlementModel.Type;
                        action = _settlementWorkitemRepository.Update(payWorkitem, trans);
                    }
                    else
                    {
                        action = _settlementWorkitemRepository.Insert(new SettlementWorkitem()
                        {
                            WorkitemId = settlementModel.WorkitemId,
                            CompanyId = settlementModel.CompanyId,
                            Times = settlementModel.Times.Value,
                            SettlementNumber = settlementModel.Quantity,
                            SettlementDate = DateTime.UtcNow,
                            Type = settlementModel.Type
                        }, trans);
                        caclQuantity = payWorkitems.Select(c => c.SettlementNumber).Sum() + settlementModel.Quantity;
                    }

                    if (action)
                    {
                        if (caclQuantity > workitem.Quantity) throw new ServiceException("Tổng số lượng thanh toán các lần lớn hơn số lượng ban đầu trong hạng mục " + workitem.Name);
                        workitem.TotalPaidQuantity = caclQuantity;
                        success = _workitemRepository.Update(workitem, trans);
                    }
                    else trans.Rollback();
                }
            }
            catch (Exception ex)
            {
                trans.Rollback();
                throw ex;
            }
            trans.Commit();
            return success;
        }

        public async Task<SettlementWorkitemModel> ViewPayOneWorkItem(int workitemId)
        {
            var exist = await _workitemRepository.FindByIdAsync(workitemId);
            if (exist == null)
                throw new ServiceException("Không tìm thấy hạng mục này");
            var settlementWorkitem = _settlementWorkitemRepository.FindAll();
            var result = new SettlementWorkitemModel
            {
                Id = exist.Id,
                Name = exist.Name,
                ProjectId = exist.ProjectId,
                Quantity = exist.Quantity.Value,
                CalculationUnit = exist.CalculationUnit,
                //Money = exist.Money.Value,
                OrderSettlements = settlementWorkitem.Where(c => c.WorkitemId == workitemId).OrderBy(a => a.Times).
                    ToList().CloneToListModels<SettlementWorkitem, OrderSettlementWorkitemModel>(),
                TotalPaidQuantity = exist.TotalPaidQuantity.Value,
                //Amount = exist.Quantity.Value * exist.Money.Value,

            };
            return result;
        }

        public SettlementViewModel ViewPayAllWorkItem(int projectId)
        {
            var existProject = _projectRepository.FindById(projectId);
            if (existProject != null)
            {
                var entities = _workitemRepository.FindAll(c => c.ProjectId == projectId);
                var settlementWorkitem = _settlementWorkitemRepository.FindAll(c => c.Type == SettlementType.SettlementTimes);
                var maxOrder = 0;
                if (settlementWorkitem.Count() != 0)
                {
                    var times = from w in entities
                                join s in settlementWorkitem
                                on w.Id equals s.WorkitemId
                                select s.Times;
                    if (times.Count() > 0)
                        maxOrder = times.Max();
                }
                var results = new List<SettlementWorkitemModel>();

                decimal totalMoney = 0;

                //foreach (var p in entities)
                //{
                //    var result = new SettlementWorkitemModel
                //    {
                //        Id = p.Id,
                //        Name = p.Name,
                //        ProjectId = p.ProjectId,
                //        Quantity = p.Quantity.Value,
                //        CalculationUnit = p.CalculationUnit,
                //        Money = p.Money.Value,
                //        OrderSettlements = settlementWorkitem.Where(c => c.WorkitemId == p.Id).OrderBy(a => a.Times).
                //                    ToList().CloneToListModels<SettlementWorkitem, OrderSettlementWorkitemModel>(),
                //        TotalPaidQuantity = p.TotalPaidQuantity.Value,
                //        Amount = p.Quantity.Value * p.Money.Value
                //    };
                //    if (p.WorkitemGroupId.HasValue)
                //    {
                //        result.WorkitemGroupId = p.WorkitemGroupId.Value;
                //        results.Add(result);
                //        totalMoney += result.Amount;
                //    }
                //    else
                //    {
                //        results.Add(result);
                //    }
                //}

                return new SettlementViewModel
                {
                    SettlementWorkitems = results,
                    SettlementMoneyTotal = totalMoney,
                    SettlementMaxOrder = maxOrder
                };

            }
            else throw new ServiceException("Không tồn tại dự án này");
        }

        public bool CheckNumberWorkItemLessThanTotal(List<AssignmentUpdateModel> models, IDbTransaction trans,
            IEnumerable<WorkitemCompany> workitemCompanies, IEnumerable<Workitem> workitems)
        {
            var result = false;
            foreach (var model in models)
            {
                var workitem = workitems.FirstOrDefault(c => c.Id == model.WorkItemId);
                var total = workitemCompanies.Where(c => c.WorkItemId == model.WorkItemId)
                    .Select(d => d.AssignedNumber).Sum();

                if (total <= workitem.Quantity)
                    result = true;
                else
                    result = false;
                if (!result) break;
            }
            return result;
        }

        public bool CheckNumberWorkitemAndCompleteLessThanTotal(List<AssignmentUpdateModel> models, IDbTransaction trans,
            IEnumerable<WorkitemCompany> workitemCompanies, IEnumerable<Workitem> workitems)
        {
            var result = false;
            foreach (var model in models)
            {
                var workitem = workitems.FirstOrDefault(c => c.Id == model.WorkItemId);
                var total = workitemCompanies.Where(c => c.WorkItemId == model.WorkItemId)
                    .Select(d => d.AssignedNumber).Sum() + workitemCompanies.Where(c => c.WorkItemId == model.WorkItemId)
                    .Select(d => d.CompletedNumber).Sum();

                if (total <= workitem.Quantity)
                    result = true;
                else
                    result = false;
                if (!result) break;
            }
            return result;
        }

        public bool UpdateAssignments(List<AssignmentUpdateModel> model)
        {
            var result = false;
            var trans = _workitemCompanyRepository.BeginTransaction();

            if (trans.Connection.State != ConnectionState.Open) trans.Connection.Open();
            try
            {
                var workitemCompanies = _workitemCompanyRepository.FindAll(trans).ToList();
                var workitems = _workitemRepository.FindAll(trans);
                decimal totalAssign = 0;
                foreach (var assignment in model)
                {
                    totalAssign = workitemCompanies.Where(c => c.WorkItemId == assignment.WorkItemId).Select(c => c.AssignedNumber).Sum();
                    var workitem = workitems.FirstOrDefault(c => c.Id == assignment.WorkItemId);
                    if (workitem == null)
                        throw new ServiceException("Không tồn tại hạng mục này");
                    if (_companyRepository.FindById(assignment.CompanyId, trans) == null)
                        throw new ServiceException("Không tồn tại công ty này");

                    if (assignment.AssignmentNumber < 0) throw new ServiceException("Số lượng phân giao hạng mục " + workitem.Name + " không được nhỏ hơn 0");

                    var workitemCompany = workitemCompanies.FirstOrDefault(c => c.WorkItemId == assignment.WorkItemId
                        && c.CompanyId == assignment.CompanyId);
                    if (workitemCompany == null)
                    {
                        totalAssign += assignment.AssignmentNumber;
                        //CheckNumberWorkItemLessThanTotal(assignment.WorkItemId, assignment.AssignedNumber, trans);
                        var add = new WorkitemCompany
                        {
                            CompanyId = assignment.CompanyId,
                            WorkItemId = assignment.WorkItemId,
                            AssignedNumber = assignment.AssignmentNumber,
                            CompletedNumber = 0,
                            LastUpdateContentReport = string.Empty
                        };
                        result = _workitemCompanyRepository.Insert(add, trans);
                        if (!result) throw new ServiceException("Không phân giao được hạng mục này");
                        workitemCompanies.Add(add);
                    }
                    else
                    {
                        if (assignment.AssignmentNumber < workitemCompany.CompletedNumber)
                            throw new ServiceException("Số lượng hạng mục phân giao thay đổi không được nhỏ hơn số lượng đã hoàn thành trước đó");
                        //CheckNumberWorkItemLessThanTotal(assignment.WorkItemId, assignment.AssignedNumber - workItemCompany.AssignedNumber, trans);
                        totalAssign = totalAssign + (assignment.AssignmentNumber - workitemCompany.AssignedNumber);
                        workitemCompany.AssignedNumber = assignment.AssignmentNumber;
                        result = _workitemCompanyRepository.Update(workitemCompany, trans);
                        if (!result) throw new ServiceException("Không phân giao được hạng mục này");
                        //var change = workitemCompanies.FirstOrDefault(c => c.WorkItemId == workitemCompany.WorkItemId);
                        //change.AssignedNumber = assignment.AssignedNumber;
                    }

                    if (!result) break;
                }
                result = CheckNumberWorkItemLessThanTotal(model, trans, workitemCompanies, workitems);
                if (!result)
                {
                    throw new ServiceException("Tổng số lượng hạng mục phân giao lớn hơn số lượng hạng mục ban đầu");
                }
                result = CheckNumberWorkitemAndCompleteLessThanTotal(model, trans, workitemCompanies, workitems);
                if (!result)
                {
                    throw new ServiceException("Tổng số lượng hạng mục phân giao và số lượng hoàn thành trước đó lớn hơn số lượng hạng mục ban đầu");
                }

            }
            catch (Exception ex)
            {
                trans.Rollback();
                throw ex;
            }
            trans.Commit();
            return result;
        }

        public ListWorkitemByCompanyModel ListWorkitemByCompanyId(int companyId, int projectId)
        {
            var company = _companyRepository.FindById(companyId);
            if (company == null) throw new ServiceException("Công ty không tồn tại");
            var project = _projectRepository.FindById(projectId);
            if (project == null) throw new ServiceException("Dự án không tồn tại");

            var projectCompany = _projectCompanyRepository.Find(c => c.CompanyId == companyId &&
                c.ProjectId == projectId);

            if (projectCompany == null) throw new ServiceException("Công ty " + company.Name + " không thuộc dự án "
                + project.Name);

            var workitemCompanies = _workitemCompanyRepository.FindAll(c => c.CompanyId == companyId);
            var workitemIds = workitemCompanies.Select(c => c.WorkItemId);

            var workitemInProjects = _workitemRepository.FindAll(c => c.ProjectId == projectId);
            var listWorkitemIds = new List<int>();
            foreach (var id in workitemIds)
            {
                if (workitemInProjects.Select(c => c.Id).Contains(id))
                    listWorkitemIds.Add(id);
            }

            var result = new ListWorkitemByCompanyModel();
            if (listWorkitemIds.Count() > 0)
            {
                result = new ListWorkitemByCompanyModel()
                {
                    CompanyName = company.Name,

                };
                var workitemCompany = new WorkitemByCompanyModel();
                foreach (var w in listWorkitemIds)
                {
                    var workitem = _workitemRepository.FindById(w);
                    var workitemByCompany = workitemCompanies.Where(c => c.WorkItemId == w).FirstOrDefault();
                    workitemCompany = new WorkitemByCompanyModel
                    {
                        CompanyId = companyId,
                        WorkitemId = w,
                        WorkitemName = workitem.Name,
                        Quantity = workitem.Quantity.Value,
                        CalculationUnit = workitem.CalculationUnit,
                        CompletedNumber = workitemByCompany.CompletedNumber,
                        AssignmentNumber = workitemByCompany.AssignedNumber,
                        CompletedNumberTotal = workitemInProjects.FirstOrDefault(c => c.Id == w).CompletedNumberTotal.Value,



                    };
                    result.Workitems.Add(workitemCompany);
                }

                return result;
            }
            else return null;
        }

        public List<WorkitemCompletedReportMonthModel> WorkitemCompletedReportPerMonth(List<int> workitemIds, int month, int year, int projectId, IEnumerable<Company> companies)
        {
            var result = new List<WorkitemCompletedReportMonthModel>();
            var reports = _projectReportRepository.FindAll(c => c.Status == ProjectReportStatus.Approved && c.ProjectId == projectId);
            var reportFilters = reports.Where(c => c.CreatedOnUtc.Month == month && c.CreatedOnUtc.Year == year);
            var reportIds = reportFilters.Select(c => c.Id);
            var projectReportWorkitems = _projectReportWorkitemRepository.FindAll(rw => reportIds.Contains(rw.ProjectReportId));

            foreach (var workitemId in workitemIds)
            {

                var reportWorkitems = new List<ProjectReportWorkitem>();
                reportFilters.ToList().ForEach(r =>
                {
                    var reportWorkitem = projectReportWorkitems.Where(c => c.ProjectReportId == r.Id);
                    reportWorkitems.AddRange(reportWorkitem);
                });

                var addComplete = new WorkitemCompletedReportMonthModel
                {
                    WorkitemId = workitemId,
                    SumCompletedNumber = reportWorkitems.Where(c => c.WorkitemId == workitemId).Select(t => t.CompletedNumber).Sum()
                };
                if (workitemId == 22518)
                {

                }
                foreach (var company in companies)
                {
                    if (reportWorkitems.Count(c => c.CompanyId == company.Id && c.WorkitemId == workitemId) > 0)
                    {
                        addComplete.ReportWithCompanies.Add(new CompleteReportWithCompany
                        {
                            CompanyId = company.Id,
                            CompanyName = company.Name,
                            CompleteNumber = reportWorkitems.Where(c => c.CompanyId == company.Id && c.WorkitemId == workitemId).Sum(a => a.CompletedNumber)
                        });
                    }
                }

                result.Add(addComplete);
            }
            return result;
        }

        public List<WorkitemCompletedReportMonthModel> WorkitemAccumulatedPreviousMonth(List<int> workitemIds, int month, int year, int projectId, IEnumerable<Company> companies)
        {
            var result = new List<WorkitemCompletedReportMonthModel>();
            var reports = _projectReportRepository.FindAll(c => c.Status == ProjectReportStatus.Approved && c.ProjectId == projectId);
            var reportFilters = reports.Where(c => (c.CreatedOnUtc.Month < month && c.CreatedOnUtc.Year == year)
                || c.CreatedOnUtc.Year < year);
            var reportIds = reportFilters.Select(c => c.Id);
            var projectReportWorkitems = _projectReportWorkitemRepository.FindAll(rw => reportIds.Contains(rw.ProjectReportId));

            foreach (var workitemId in workitemIds)
            {

                var reportWorkitems = new List<ProjectReportWorkitem>();
                reportFilters.ToList().ForEach(r =>
                {
                    var reportWorkitem = projectReportWorkitems.Where(c => c.ProjectReportId == r.Id);
                    reportWorkitems.AddRange(reportWorkitem);
                });
                var addComplete = new WorkitemCompletedReportMonthModel
                {
                    WorkitemId = workitemId,
                    SumCompletedNumber = reportWorkitems.Where(c => c.WorkitemId == workitemId).Select(t => t.CompletedNumber).Sum()
                };

                foreach (var company in companies)
                {
                    if (reportWorkitems.Count(c => c.CompanyId == company.Id && c.WorkitemId == workitemId) > 0)
                    {
                        addComplete.ReportWithCompanies.Add(new CompleteReportWithCompany
                        {
                            CompanyId = company.Id,
                            CompanyName = company.Name,
                            CompleteNumber = reportWorkitems.Where(c => c.CompanyId == company.Id && c.WorkitemId == workitemId).Sum(a => a.CompletedNumber)
                        });
                    }
                }
                result.Add(addComplete);


            }
            return result;
        }

        public List<WorkitemCompletedReportQuarterModel> WorkitemCompletedReportPerQuarter(List<int> workitemIds, int quarter, int year, int projectId, IEnumerable<Company> companies)
        {
            var result = new List<WorkitemCompletedReportQuarterModel>();
            var reports = _projectReportRepository.FindAll(c => c.Status == ProjectReportStatus.Approved && c.ProjectId == projectId);
            var reportFilters = new List<ProjectReport>();
            if (quarter == 1)
            {
                reportFilters = reports.Where(c => (c.CreatedOnUtc.Month == 1 || c.CreatedOnUtc.Month == 2 || c.CreatedOnUtc.Month == 3) && c.CreatedOnUtc.Year == year).ToList();
            }
            if (quarter == 2)
            {
                reportFilters = reports.Where(c => (c.CreatedOnUtc.Month == 4 || c.CreatedOnUtc.Month == 5 || c.CreatedOnUtc.Month == 6) && c.CreatedOnUtc.Year == year).ToList();
            }
            if (quarter == 3)
            {
                reportFilters = reports.Where(c => (c.CreatedOnUtc.Month == 7 || c.CreatedOnUtc.Month == 8 || c.CreatedOnUtc.Month == 9) && c.CreatedOnUtc.Year == year).ToList();
            }
            if (quarter == 4)
            {
                reportFilters = reports.Where(c => (c.CreatedOnUtc.Month == 10 || c.CreatedOnUtc.Month == 11 || c.CreatedOnUtc.Month == 12) && c.CreatedOnUtc.Year == year).ToList();
            }
            var reportIds = reportFilters.Select(c => c.Id);
            var projectReportWorkitems = _projectReportWorkitemRepository.FindAll(rw => reportIds.Contains(rw.ProjectReportId));

            foreach (var workitemId in workitemIds)
            {

                var reportWorkitems = new List<ProjectReportWorkitem>();
                reportFilters.ToList().ForEach(r =>
                {
                    var reportWorkitem = projectReportWorkitems.Where(c => c.ProjectReportId == r.Id);
                    reportWorkitems.AddRange(reportWorkitem);
                });
                var addComplete = new WorkitemCompletedReportQuarterModel
                {
                    WorkitemId = workitemId,
                    SumCompletedNumber = reportWorkitems.Where(c => c.WorkitemId == workitemId).Select(t => t.CompletedNumber).Sum()
                };

                foreach (var company in companies)
                {
                    if (reportWorkitems.Count(c => c.CompanyId == company.Id && c.WorkitemId == workitemId) > 0)
                    {
                        addComplete.ReportWithCompanies.Add(new CompleteReportWithCompany
                        {
                            CompanyId = company.Id,
                            CompanyName = company.Name,
                            CompleteNumber = reportWorkitems.Where(c => c.CompanyId == company.Id && c.WorkitemId == workitemId).Sum(a => a.CompletedNumber)
                        });
                    }
                }
                result.Add(addComplete);
            }
            return result;
        }

        public List<WorkitemCompletedReportQuarterModel> WorkitemAccumulatePreviousQuarter(List<int> workitemIds, int quarter, int year, int projectId, IEnumerable<Company> companies)
        {
            var result = new List<WorkitemCompletedReportQuarterModel>();
            var reports = _projectReportRepository.FindAll(c => c.Status == ProjectReportStatus.Approved && c.ProjectId == projectId);
            var reportFilters = new List<ProjectReport>();
            if (quarter == 4)
            {
                reportFilters = reports.Where(c => (c.CreatedOnUtc.Month < 10 && c.CreatedOnUtc.Year == year) || c.CreatedOnUtc.Year < year).ToList();
            }
            if (quarter == 3)
            {
                reportFilters = reports.Where(c => (c.CreatedOnUtc.Month < 7 && c.CreatedOnUtc.Year == year) || c.CreatedOnUtc.Year < year).ToList();
            }
            if (quarter == 2)
            {
                reportFilters = reports.Where(c => (c.CreatedOnUtc.Month < 4 && c.CreatedOnUtc.Year == year) || c.CreatedOnUtc.Year < year).ToList();
            }
            if (quarter == 1)
            {
                reportFilters = reports.Where(c => c.CreatedOnUtc.Year < year).ToList();
            }
            var reportIds = reportFilters.Select(c => c.Id);
            var projectReportWorkitems = _projectReportWorkitemRepository.FindAll(rw => reportIds.Contains(rw.ProjectReportId));

            foreach (var workitemId in workitemIds)
            {

                var reportWorkitems = new List<ProjectReportWorkitem>();
                reportFilters.ToList().ForEach(r =>
                {
                    var reportWorkitem = projectReportWorkitems.Where(c => c.ProjectReportId == r.Id);
                    reportWorkitems.AddRange(reportWorkitem);
                });

                var addComplete = new WorkitemCompletedReportQuarterModel
                {
                    WorkitemId = workitemId,
                    SumCompletedNumber = reportWorkitems.Where(c => c.WorkitemId == workitemId).Select(t => t.CompletedNumber).Sum()
                };
                foreach (var company in companies)
                {
                    if (reportWorkitems.Count(c => c.CompanyId == company.Id && c.WorkitemId == workitemId) > 0)
                    {
                        addComplete.ReportWithCompanies.Add(new CompleteReportWithCompany
                        {
                            CompanyId = company.Id,
                            CompanyName = company.Name,
                            CompleteNumber = reportWorkitems.Where(c => c.CompanyId == company.Id && c.WorkitemId == workitemId).Sum(a => a.CompletedNumber)
                        });
                    }
                }
                result.Add(addComplete);
            }
            return result;
        }

        public List<YieldWithCompanyInProjectModel> CalcalateYieldWithCompanyByTime(int projectId, int? month, int? quarter, int year)
        {
            var result = new List<YieldWithCompanyInProjectModel>();
            var companies = _companyRepository.FindAll();
            var companyCheckIds = _projectCompanyRepository.FindAll(c => c.ProjectId == projectId).Select(c => c.CompanyId);
            var companyInProjects = companies.Where(c => companyCheckIds.Contains(c.Id));
            var workitems = _workitemRepository.FindAll(c => c.ProjectId == projectId);
            var reports = _projectReportRepository.FindAll(c => c.Status == ProjectReportStatus.Approved && c.ProjectId == projectId);
            var reportIds = reports.Select(c => c.Id);
            var reportWorkitems = _projectReportWorkitemRepository.FindAll(c => reportIds.Contains(c.ProjectReportId));


            var reportInTimes = new List<ProjectReport>();
            var reportWorkitemInTimes = new List<ProjectReportWorkitem>();
            var reportPrevious = new List<ProjectReport>();
            var reportWorkitemPrevious = new List<ProjectReportWorkitem>();
            if (month.HasValue)
            {
                reportInTimes = reports.Where(c => c.ProjectId == projectId && c.CreatedOnUtc.Month == month.Value && c.CreatedOnUtc.Year == year).ToList();
                var checkInTimeIds = reportInTimes.Select(c => c.Id);
                reportPrevious = reports.Where(c => (c.CreatedOnUtc.Month < month && c.CreatedOnUtc.Year == year) || c.CreatedOnUtc.Year < year).ToList();
                var checkPreviousIds = reportPrevious.Select(c => c.Id);

                reportWorkitemInTimes = reportWorkitems.Where(c => checkInTimeIds.Contains(c.ProjectReportId)).ToList();
                reportWorkitemPrevious = reportWorkitems.Where(c => checkPreviousIds.Contains(c.ProjectReportId)).ToList();
            }
            else
            {
                if (quarter == 1)
                {
                    reportInTimes = reports.Where(c => (c.CreatedOnUtc.Month == 1 || c.CreatedOnUtc.Month == 2 || c.CreatedOnUtc.Month == 3) && c.CreatedOnUtc.Year == year).ToList();
                    reportPrevious = reports.Where(c => c.CreatedOnUtc.Year < year).ToList();
                }
                if (quarter == 2)
                {
                    reportInTimes = reports.Where(c => (c.CreatedOnUtc.Month == 4 || c.CreatedOnUtc.Month == 5 || c.CreatedOnUtc.Month == 6) && c.CreatedOnUtc.Year == year).ToList();
                    reportPrevious = reports.Where(c => (c.CreatedOnUtc.Month < 4 && c.CreatedOnUtc.Year == year) || c.CreatedOnUtc.Year < year).ToList();
                }
                if (quarter == 3)
                {
                    reportInTimes = reports.Where(c => (c.CreatedOnUtc.Month == 7 || c.CreatedOnUtc.Month == 8 || c.CreatedOnUtc.Month == 9) && c.CreatedOnUtc.Year == year).ToList();
                    reportPrevious = reports.Where(c => (c.CreatedOnUtc.Month < 7 && c.CreatedOnUtc.Year == year) || c.CreatedOnUtc.Year < year).ToList();
                }
                if (quarter == 4)
                {
                    reportInTimes = reports.Where(c => (c.CreatedOnUtc.Month == 10 || c.CreatedOnUtc.Month == 11 || c.CreatedOnUtc.Month == 12) && c.CreatedOnUtc.Year == year).ToList();
                    reportPrevious = reports.Where(c => (c.CreatedOnUtc.Month < 10 && c.CreatedOnUtc.Year == year) || c.CreatedOnUtc.Year < year).ToList();
                }
                var checkInTimeIds = reportInTimes.Select(c => c.Id);

                var checkPreviousIds = reportPrevious.Select(c => c.Id);
                reportWorkitemInTimes = reportWorkitems.Where(rw => checkInTimeIds.Contains(rw.ProjectReportId)).ToList();
                reportWorkitemPrevious = reportWorkitems.Where(c => checkPreviousIds.Contains(c.ProjectReportId)).ToList();
            }
            foreach (var company in companyInProjects)
            {
                var statisticCompany = new YieldWithCompanyInProjectModel
                {
                    CompanyId = company.Id,
                    CompanyName = company.Name
                };
                var completeWithCompany = reportWorkitemInTimes.Where(c => c.CompanyId == company.Id);
                foreach (var item in completeWithCompany)
                {
                    statisticCompany.YieldByCompany += item.CompletedNumber * workitems.FirstOrDefault(c => c.Id == item.WorkitemId).UnitPrice.Value;
                }
                var completeBefore = reportWorkitemPrevious.Where(c => c.CompanyId == company.Id);
                foreach (var item in completeBefore)
                {
                    statisticCompany.AccumulatePreviousByCompany += item.CompletedNumber * workitems.FirstOrDefault(c => c.Id == item.WorkitemId).UnitPrice.Value;
                }
                statisticCompany.AccumulateCurrentByCompany = statisticCompany.YieldByCompany + statisticCompany.AccumulatePreviousByCompany;
                result.Add(statisticCompany);
            }
            return result;
        }

        public YieldInMonthModel CheckCompletedNumberPerMonth(CheckCompletedPerMonthModel checkWorkitem)
        {
            var workitemPerMonths = new YieldInMonthModel();

            var workitems = _workitemRepository.FindAll(c => c.ProjectId == checkWorkitem.ProjectId);

            var inputWorkitemIds = workitems.Select(c => c.Id).ToList();

            var workitemIds = workitems.Select(c => c.Id);

            var assignAlls = _workitemCompanyRepository.FindAll(c => workitemIds.Contains(c.WorkItemId));
            var workitemAssignIds = assignAlls.Select(c => c.WorkItemId).Distinct();

            var companyIds = assignAlls.Select(c => c.CompanyId).Distinct();

            var companies = _companyRepository.FindAll(c => companyIds.Contains(c.Id));

            var workitemCompleted = WorkitemCompletedReportPerMonth(inputWorkitemIds, checkWorkitem.Month, checkWorkitem.Year, checkWorkitem.ProjectId, companies);

            var completePrevious = WorkitemAccumulatedPreviousMonth(inputWorkitemIds, checkWorkitem.Month, checkWorkitem.Year, checkWorkitem.ProjectId, companies);



            var listEstimatePlan = _estimationPlanRepository.FindAll(c => workitemIds.Contains(c.WorkitemId)
                && c.Type == EstimationType.EstimationMonthComplete);



            if (workitems.Count() > 0)
            {
                decimal totalYield = 0;
                decimal totalYieldAccmulatePrevious = 0;
                decimal totalNextMonthYield = 0;
                decimal totalCompletedYield = 0;
                foreach (var workitem in workitems)
                {
                    var monthCurrentCompletedNumber = workitemCompleted.FirstOrDefault(c => c.WorkitemId == workitem.Id).SumCompletedNumber;
                    var yieldCurrentMonth = workitem.UnitPrice * monthCurrentCompletedNumber;
                    var completeAccumulatePrevious = completePrevious.FirstOrDefault(c => c.WorkitemId == workitem.Id).SumCompletedNumber;
                    var yieldAccumulatePrevious = workitem.UnitPrice * completeAccumulatePrevious;

                    var workitemPerMonth = new WorkitemCompletedPerMonth
                    {

                        WorkitemId = workitem.Id,
                        STT = workitem.STT,
                        WorkitemName = workitem.Name,
                        Quantity = workitem.Quantity.HasValue ? workitem.Quantity : null,
                        CalculationUnit = !string.IsNullOrEmpty(workitem.CalculationUnit) ? workitem.CalculationUnit : null,
                        UnitPrice = workitem.UnitPrice.HasValue ? workitem.UnitPrice : null,
                        LevelHeading = workitem.LevelHeading.Value,
                        CompletedNumberTotal = workitem.CompletedNumberTotal,
                    };

                    if (workitemPerMonth.UnitPrice.HasValue)
                    {
                        if (workitemPerMonth.CompletedNumberTotal.HasValue)
                        {
                            workitemPerMonth.CompletedYield = workitemPerMonth.UnitPrice.Value * workitemPerMonth.CompletedNumberTotal.Value;
                            totalCompletedYield += workitemPerMonth.CompletedYield.Value;
                        }
                        else workitemPerMonth.CompletedYield = 0;
                    }

                    if (workitemPerMonth.CompletedYield.HasValue)
                    {
                        workitemPerMonth.CompletedYield = NumberHelper.RoundDown(workitemPerMonth.CompletedYield.Value, 2);
                    }

                    if (workitemPerMonth.Quantity.HasValue)
                    {
                        workitemPerMonth.Quantity = NumberHelper.RoundDown(workitemPerMonth.Quantity.Value, 2);
                    }

                    if (workitemPerMonth.UnitPrice.HasValue)
                    {
                        workitemPerMonth.UnitPrice = NumberHelper.RoundDown(workitemPerMonth.UnitPrice.Value, 2);
                    }

                    if (workitemPerMonth.CompletedNumberTotal.HasValue)
                    {
                        workitemPerMonth.CompletedNumberTotal = NumberHelper.RoundDown(workitemPerMonth.CompletedNumberTotal.Value, 2);
                    }

                    if (HeadingExcels.DeteminedLevelHeading(workitemPerMonth.STT) == 1) workitemPerMonth.IsBold = true;
                    if (workitem.Quantity.HasValue && workitem.UnitPrice.HasValue)
                    {
                        workitemPerMonth.CurrentMonth = new WorkitemCompletedCurrentMonth
                        {
                            CompletedNumber = monthCurrentCompletedNumber,
                            Yield = yieldCurrentMonth.HasValue ? yieldCurrentMonth.Value : 0
                        };

                        workitemPerMonth.PreviousMonth = new AccumulatedPreviousMonth
                        {
                            AccumulatedNumber = completeAccumulatePrevious,
                            AccumulatedYield = yieldAccumulatePrevious.HasValue ? yieldAccumulatePrevious.Value : 0
                        };

                        workitemPerMonth.AccumulateCompleteCurrent = new AccumulateCompleteCurrentMonth
                        {
                            AccumulateCurrentNumber = workitemPerMonth.CurrentMonth.CompletedNumber + workitemPerMonth.PreviousMonth.AccumulatedNumber,
                            AccumulateCurrentYield = workitemPerMonth.CurrentMonth.Yield + workitemPerMonth.PreviousMonth.AccumulatedYield
                        };


                        if (workitemPerMonth.CurrentMonth.CompletedNumber.HasValue)
                        {
                            workitemPerMonth.CurrentMonth.CompletedNumber = NumberHelper.RoundDown(workitemPerMonth.CurrentMonth.CompletedNumber.Value, 2);
                        }

                        if (workitemPerMonth.CurrentMonth.Yield.HasValue)
                        {
                            workitemPerMonth.CurrentMonth.Yield = NumberHelper.RoundDown(workitemPerMonth.CurrentMonth.Yield.Value, 0);
                        }

                        if (workitemPerMonth.PreviousMonth.AccumulatedNumber.HasValue)
                        {
                            workitemPerMonth.PreviousMonth.AccumulatedNumber = NumberHelper.RoundDown(workitemPerMonth.PreviousMonth.AccumulatedNumber.Value, 2);
                        }

                        if (workitemPerMonth.PreviousMonth.AccumulatedYield.HasValue)
                        {
                            workitemPerMonth.PreviousMonth.AccumulatedYield = NumberHelper.RoundDown(workitemPerMonth.PreviousMonth.AccumulatedYield.Value, 0);
                        }

                        if (workitemPerMonth.AccumulateCompleteCurrent.AccumulateCurrentNumber.HasValue)
                        {
                            workitemPerMonth.AccumulateCompleteCurrent.AccumulateCurrentNumber = NumberHelper.RoundDown(workitemPerMonth.AccumulateCompleteCurrent.AccumulateCurrentNumber.Value, 2);
                        }

                        if (workitemPerMonth.AccumulateCompleteCurrent.AccumulateCurrentYield.HasValue)
                        {
                            workitemPerMonth.AccumulateCompleteCurrent.AccumulateCurrentYield = NumberHelper.RoundDown(workitemPerMonth.AccumulateCompleteCurrent.AccumulateCurrentYield.Value, 0);
                        }

                        if (workitemPerMonth.CompletedNumberTotal.HasValue)
                        {
                            workitemPerMonth.CompletedNumberTotal = NumberHelper.RoundDown(workitemPerMonth.CompletedNumberTotal.Value, 2);
                        }
                    }

                    if (yieldAccumulatePrevious.HasValue)
                    {
                        totalYieldAccmulatePrevious += yieldAccumulatePrevious.Value;
                    }

                    if (yieldCurrentMonth.HasValue)
                    {
                        totalYield += yieldCurrentMonth.Value;
                    }

                    var estimationPlan = new EstimationPlan();

                    if (workitem.Quantity.HasValue && workitem.UnitPrice.HasValue)
                    {
                        if (checkWorkitem.Month == 12)
                        {
                            estimationPlan = listEstimatePlan.FirstOrDefault(c => c.WorkitemId == workitem.Id && c.Month == 1
                                 && c.Year == checkWorkitem.Year + 1);
                        }
                        else
                        {
                            estimationPlan = listEstimatePlan.FirstOrDefault(c => c.WorkitemId == workitem.Id && c.Month == checkWorkitem.Month + 1
                                && c.Year == checkWorkitem.Year);
                        }

                        if (estimationPlan != null)
                        {
                            workitemPerMonth.NextMonth = new WorkitemEstimationNextMonth
                            {
                                EstimationNumber = estimationPlan.Quantity,
                                EstimationYield = estimationPlan.Quantity * workitem.UnitPrice.Value
                            };

                            workitemPerMonth.NextMonth.EstimationNumber = NumberHelper.RoundDown(workitemPerMonth.NextMonth.EstimationNumber, 2);
                            workitemPerMonth.NextMonth.EstimationYield = NumberHelper.RoundDown(workitemPerMonth.NextMonth.EstimationYield, 0);

                            totalNextMonthYield += workitemPerMonth.NextMonth.EstimationYield;
                            totalNextMonthYield = NumberHelper.RoundDown(totalNextMonthYield, 0);

                        }

                        else workitemPerMonth.NextMonth = new WorkitemEstimationNextMonth
                        {
                            EstimationNumber = 0,
                            EstimationYield = 0
                        };
                    }
                    // Ước lượng hoàn thành hạng mục theo tháng tới              

                    workitemPerMonths.CompletedPerMonth.Add(workitemPerMonth);
                }
                workitemPerMonths.TotalAccumulatePrevious = NumberHelper.RoundDown(totalYieldAccmulatePrevious, 0);
                workitemPerMonths.TotalYield = NumberHelper.RoundDown(totalYield, 0);
                workitemPerMonths.TotalAccumulateCurrent = NumberHelper.RoundDown(totalYieldAccmulatePrevious + totalYield, 0);
                workitemPerMonths.TotalNextMonthYield = NumberHelper.RoundDown(totalNextMonthYield, 0);
                workitemPerMonths.TotalCompletedYield = NumberHelper.RoundDown(totalCompletedYield, 0);

                workitemPerMonths.YieldWithCompany = CalcalateYieldWithCompanyByTime(checkWorkitem.ProjectId, checkWorkitem.Month, null, checkWorkitem.Year);

                var workitemWithHeadings = DivideWorkitem(checkWorkitem.ProjectId);

                foreach (var item in workitemWithHeadings)
                {
                    var checkIds = new List<int>();
                    checkIds.Add(item.WorkitemCategory.Id);
                    if (item.WorkItemSmallerModels.Count() > 0)
                    {
                        var smallerIds = item.WorkItemSmallerModels.Select(c => c.Id);
                        checkIds.AddRange(smallerIds);
                    }

                    var addCategory = new StatisticMonthCategoryModel
                    {
                        CategoryId = item.WorkitemCategory.Id,
                        CategoryName = item.WorkitemCategory.Name,
                        STT = item.WorkitemCategory.STT,
                        UnitPrice = item.WorkitemCategory.UnitPrice,
                        Quantity = item.WorkitemCategory.Quantity,
                        CalculationUnit = item.WorkitemCategory.CalculationUnit,
                    };

                    if (workitemAssignIds.Any(c => checkIds.Contains(c)))
                    {
                        addCategory.NotYetAssigned = false;
                        if (workitemAssignIds.Any(c => c == item.WorkitemCategory.Id) && workitemAssignIds.Count(c => checkIds.Contains(c)) == 1) // quy định chỉ 1 hạng mục lớn
                        {
                            var assigns = assignAlls.Where(c => c.WorkItemId == item.WorkitemCategory.Id);
                            foreach (var company in companies)
                            {
                                var assignIns = assigns.Where(c => c.CompanyId == company.Id);
                                if (assignIns.Count() > 0)
                                {
                                    var detailSmallers = new List<CompleteInMonthWithCompanyDetail>();
                                    var workitemIns = assignIns.Select(c => c.WorkItemId).Distinct();
                                    foreach (var id in workitemIns)
                                    {
                                        var first = workitems.FirstOrDefault(c => c.Id == id);

                                        var previousWorkitemIn = completePrevious.FirstOrDefault(c => c.WorkitemId == id);

                                        var previousCompanyIn = previousWorkitemIn.ReportWithCompanies.FirstOrDefault(c => c.CompanyId == company.Id);


                                        var currentWorkitemIn = workitemCompleted.FirstOrDefault(c => c.WorkitemId == id);

                                        var currentCompanyIn = currentWorkitemIn.ReportWithCompanies.FirstOrDefault(c => c.CompanyId == company.Id);

                                        var addDetail = new CompleteInMonthWithCompanyDetail
                                        {
                                            WorkitemId = id,
                                            WorkitemName = first.Name,
                                            STT = first.STT,
                                            CalculationUnit = first.CalculationUnit,
                                            UnitPrice = first.UnitPrice,
                                            Quantity = first.Quantity,
                                            IntoMoney = first.IntoMoney,
                                            LevelHeading = first.LevelHeading,
                                            CurrentMonth = new WorkitemCompletedCurrentMonth { },
                                            PreviousMonth = new AccumulatedPreviousMonth { },
                                            AccumulateCompleteCurrent = new AccumulateCompleteCurrentMonth { },
                                            NextMonth = new WorkitemEstimationNextMonth { }

                                        };
                                        var estimationPlan = new EstimationPlan();
                                        if (first.Quantity.HasValue && first.UnitPrice.HasValue)
                                        {
                                            if (checkWorkitem.Month == 12)
                                            {
                                                estimationPlan = listEstimatePlan.FirstOrDefault(c => c.WorkitemId == first.Id && c.Month == 1
                                                     && c.Year == checkWorkitem.Year + 1);
                                            }
                                            else
                                            {
                                                estimationPlan = listEstimatePlan.FirstOrDefault(c => c.WorkitemId == first.Id && c.Month == checkWorkitem.Month + 1
                                                    && c.Year == checkWorkitem.Year);
                                            }

                                            if (estimationPlan != null)
                                            {
                                                addDetail.NextMonth = new WorkitemEstimationNextMonth
                                                {
                                                    EstimationNumber = estimationPlan.Quantity,
                                                    EstimationYield = estimationPlan.Quantity * first.UnitPrice.Value
                                                };

                                                addDetail.NextMonth.EstimationNumber = NumberHelper.RoundDown(addDetail.NextMonth.EstimationNumber, 2);
                                                addDetail.NextMonth.EstimationYield = NumberHelper.RoundDown(addDetail.NextMonth.EstimationYield, 0);
                                            }

                                            else addDetail.NextMonth = new WorkitemEstimationNextMonth
                                            {
                                                EstimationNumber = 0,
                                                EstimationYield = 0
                                            };
                                        }

                                        if (previousCompanyIn != null)
                                        {
                                            addDetail.PreviousMonth.AccumulatedNumber = previousCompanyIn.CompleteNumber;
                                            addDetail.PreviousMonth.AccumulatedYield = previousCompanyIn.CompleteNumber * first.UnitPrice;
                                        }
                                        else
                                        {
                                            addDetail.PreviousMonth.AccumulatedNumber = 0;
                                            addDetail.PreviousMonth.AccumulatedYield = 0;
                                        }

                                        if (currentCompanyIn != null)
                                        {
                                            addDetail.CurrentMonth.CompletedNumber = currentCompanyIn.CompleteNumber;
                                            addDetail.CurrentMonth.Yield = currentCompanyIn.CompleteNumber * first.UnitPrice;
                                        }
                                        else
                                        {
                                            addDetail.CurrentMonth.CompletedNumber = 0;
                                            addDetail.CurrentMonth.Yield = 0;
                                        }

                                        addDetail.AccumulateCompleteCurrent.AccumulateCurrentNumber = addDetail.PreviousMonth.AccumulatedNumber + addDetail.CurrentMonth.CompletedNumber;
                                        addDetail.AccumulateCompleteCurrent.AccumulateCurrentYield = addDetail.PreviousMonth.AccumulatedYield + addDetail.CurrentMonth.Yield;

                                        detailSmallers.Add(addDetail);
                                    }

                                    var staticSmaller = new AccumulateMonthCompleteWithCompany
                                    {
                                        CompanyId = company.Id,
                                        CompanyName = company.Name,
                                        CompleteWithCompanyDetails = detailSmallers
                                    };

                                    addCategory.StatisticWithCompanies.Add(staticSmaller);
                                }
                            }
                        }
                        if (!workitemAssignIds.Any(c => c == item.WorkitemCategory.Id) && workitemAssignIds.Count(c => checkIds.Contains(c)) > 0)// phân giao có chứa hạng mục con
                        {
                            var assigns = assignAlls.Where(c => checkIds.Contains(c.WorkItemId));
                            foreach (var company in companies)
                            {
                                var assignIns = assigns.Where(c => c.CompanyId == company.Id);
                                if (assignIns.Count() > 0)
                                {
                                    var detailSmallers = new List<CompleteInMonthWithCompanyDetail>();
                                    var workitemIns = assignIns.Select(c => c.WorkItemId).Distinct();
                                    foreach (var id in workitemIns)
                                    {
                                        var first = workitems.FirstOrDefault(c => c.Id == id);

                                        var previousWorkitemIn = completePrevious.FirstOrDefault(c => c.WorkitemId == id);

                                        var previousCompanyIn = previousWorkitemIn.ReportWithCompanies.FirstOrDefault(c => c.CompanyId == company.Id);


                                        var currentWorkitemIn = workitemCompleted.FirstOrDefault(c => c.WorkitemId == id);

                                        var currentCompanyIn = currentWorkitemIn.ReportWithCompanies.FirstOrDefault(c => c.CompanyId == company.Id);

                                        var addDetail = new CompleteInMonthWithCompanyDetail
                                        {
                                            WorkitemId = id,
                                            WorkitemName = first.Name,
                                            STT = first.STT,
                                            CalculationUnit = first.CalculationUnit,
                                            UnitPrice = first.UnitPrice,
                                            Quantity = first.Quantity,
                                            IntoMoney = first.IntoMoney,
                                            LevelHeading = first.LevelHeading,
                                            CurrentMonth = new WorkitemCompletedCurrentMonth { },
                                            PreviousMonth = new AccumulatedPreviousMonth { },
                                            AccumulateCompleteCurrent = new AccumulateCompleteCurrentMonth { },
                                            NextMonth = new WorkitemEstimationNextMonth { }

                                        };
                                        var estimationPlan = new EstimationPlan();
                                        if (first.Quantity.HasValue && first.UnitPrice.HasValue)
                                        {
                                            if (checkWorkitem.Month == 12)
                                            {
                                                estimationPlan = listEstimatePlan.FirstOrDefault(c => c.WorkitemId == first.Id && c.Month == 1
                                                     && c.Year == checkWorkitem.Year + 1);
                                            }
                                            else
                                            {
                                                estimationPlan = listEstimatePlan.FirstOrDefault(c => c.WorkitemId == first.Id && c.Month == checkWorkitem.Month + 1
                                                    && c.Year == checkWorkitem.Year);
                                            }

                                            if (estimationPlan != null)
                                            {
                                                addDetail.NextMonth = new WorkitemEstimationNextMonth
                                                {
                                                    EstimationNumber = estimationPlan.Quantity,
                                                    EstimationYield = estimationPlan.Quantity * first.UnitPrice.Value
                                                };

                                                addDetail.NextMonth.EstimationNumber = NumberHelper.RoundDown(addDetail.NextMonth.EstimationNumber, 2);
                                                addDetail.NextMonth.EstimationYield = NumberHelper.RoundDown(addDetail.NextMonth.EstimationYield, 0);
                                            }

                                            else addDetail.NextMonth = new WorkitemEstimationNextMonth
                                            {
                                                EstimationNumber = 0,
                                                EstimationYield = 0
                                            };
                                        }

                                        if (previousCompanyIn != null)
                                        {
                                            addDetail.PreviousMonth.AccumulatedNumber = previousCompanyIn.CompleteNumber;
                                            addDetail.PreviousMonth.AccumulatedYield = previousCompanyIn.CompleteNumber * first.UnitPrice;
                                        }
                                        else
                                        {
                                            addDetail.PreviousMonth.AccumulatedNumber = 0;
                                            addDetail.PreviousMonth.AccumulatedYield = 0;
                                        }

                                        if (currentCompanyIn != null)
                                        {
                                            addDetail.CurrentMonth.CompletedNumber = currentCompanyIn.CompleteNumber;
                                            addDetail.CurrentMonth.Yield = currentCompanyIn.CompleteNumber * first.UnitPrice;
                                        }
                                        else
                                        {
                                            addDetail.CurrentMonth.CompletedNumber = 0;
                                            addDetail.CurrentMonth.Yield = 0;
                                        }

                                        addDetail.AccumulateCompleteCurrent.AccumulateCurrentNumber = addDetail.PreviousMonth.AccumulatedNumber + addDetail.CurrentMonth.CompletedNumber;
                                        addDetail.AccumulateCompleteCurrent.AccumulateCurrentYield = addDetail.PreviousMonth.AccumulatedYield + addDetail.CurrentMonth.Yield;

                                        detailSmallers.Add(addDetail);
                                    }

                                    var staticSmaller = new AccumulateMonthCompleteWithCompany
                                    {
                                        CompanyId = company.Id,
                                        CompanyName = company.Name,
                                        CompleteWithCompanyDetails = detailSmallers
                                    };

                                    addCategory.StatisticWorkitemSmallerWithCompanies.Add(staticSmaller);
                                }
                            }
                        }
                    }
                    else // Chưa giao
                    {
                        var nextMonthCategory = new WorkitemEstimationNextMonth();
                        var estimateForCategory = listEstimatePlan.Where(c => c.WorkitemId == item.WorkitemCategory.Id);
                        var estimationPlan = new EstimationPlan();
                        if (estimateForCategory != null)
                        {
                            if (checkWorkitem.Month == 12)
                            {
                                estimationPlan = estimateForCategory.FirstOrDefault(c => c.Month == 1 && c.Year == checkWorkitem.Year + 1);
                            }
                            else
                            {
                                estimationPlan = estimateForCategory.FirstOrDefault(c => c.Month == checkWorkitem.Month + 1 && c.Year == checkWorkitem.Year);
                            }

                            if (estimationPlan != null)
                            {
                                nextMonthCategory.EstimationNumber = estimationPlan.Quantity;
                                nextMonthCategory.EstimationYield = NumberHelper.RoundDown(nextMonthCategory.EstimationNumber * item.WorkitemCategory.UnitPrice.Value, 0);
                            }
                        }
                        addCategory.NextMonth = nextMonthCategory;
                        foreach (var smaller in item.WorkItemSmallerModels)
                        {
                            var nextMonthSmaller = new WorkitemEstimationNextMonth();
                            var estimationPlanSmaller = new EstimationPlan();
                            if (checkWorkitem.Month == 12)
                            {
                                estimationPlanSmaller = listEstimatePlan.FirstOrDefault(c => c.WorkitemId == smaller.Id && c.Month == 1 && c.Year == checkWorkitem.Year + 1);
                            }
                            else
                            {
                                estimationPlanSmaller = listEstimatePlan.FirstOrDefault(c => c.WorkitemId == smaller.Id && c.Month == checkWorkitem.Month + 1 && c.Year == checkWorkitem.Year);
                            }

                            if (estimationPlanSmaller != null)
                            {
                                nextMonthSmaller.EstimationNumber = estimationPlanSmaller.Quantity;
                                nextMonthSmaller.EstimationYield = NumberHelper.RoundDown(nextMonthSmaller.EstimationNumber * smaller.UnitPrice.Value, 0);
                            }
                            smaller.NextMonth = nextMonthSmaller;
                        }
                        addCategory.ExceptionNotAssigneds = item.WorkItemSmallerModels;

                    }
                    workitemPerMonths.StatisticCategories.Add(addCategory);
                    //workitemPerMonths.StatisticCategories.Add()
                }



            }
            return workitemPerMonths;
        }

        public void BindingFormatForExcel(ExcelWorksheet worksheet, int? month, int? quarter, int year, string projectName)
        {
            worksheet.Cells.Style.WrapText = true;

            using (var range = worksheet.Cells["A:K"])
            {
                range.Style.Font.SetFromFont(new Font("Times New Roman", 13));
            }
            using (var range = worksheet.Cells["A2:K2"])
            {
                range.Style.Font.SetFromFont(new Font("Times New Roman", 14));
                range.Style.Font.Bold = true;
                range.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                range.Style.VerticalAlignment = ExcelVerticalAlignment.Center;
            }

            worksheet.Cells.Style.WrapText = true;
            worksheet.Cells[3, 1].Value = "STT";
            worksheet.Cells[3, 2].Value = "Hạng mục";
            worksheet.Cells[3, 3].Value = "Số lượng";
            worksheet.Cells[3, 4].Value = "Đơn vị tính";
            worksheet.Cells[3, 5].Value = "Đơn giá (VNĐ)";
            worksheet.Cells[3, 6].Value = "Lũy kế";
            worksheet.Cells[3, 7].Value = "Sản lượng( Lũy kế)";
            if (month.HasValue)
            {
                var estimateMonth = 0;
                var estimateYear = 0;
                if (month != 12)
                {
                    worksheet.Cells[3, 10].Value = "Khối lượng dự kiến T" + (month + 1) + "-" + year;
                    worksheet.Cells[3, 11].Value = "Sản lượng T" + (month + 1) + "-" + year + " (VNĐ)";
                    estimateMonth = month.Value + 1;
                    estimateYear = year;
                }
                else
                {
                    worksheet.Cells[3, 10].Value = "Khối lượng dự kiến T" + 1 + "-" + (year + 1);
                    worksheet.Cells[3, 11].Value = "Sản lượng T1" + "-" + (year + 1) + " (VNĐ)";
                    estimateMonth = 1;
                    estimateYear = year + 1;
                }


                var row11 = worksheet.Cells[2, 1].RichText.Add("Tổng hợp sản lượng tháng " + month + " và kế hoạch tháng " + estimateMonth + " năm " + estimateYear + "\r\n");
                row11.Size = 16;
                row11.Bold = true;
                var row12 = worksheet.Cells[2, 1].RichText.Add("Công trình: " + projectName + "" + "\r\n");
                row12.Size = 14;
                row12.Bold = true;
                worksheet.Cells[2, 1].AutoFitColumns();
                worksheet.Row(3).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                worksheet.Row(3).Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                worksheet.Row(3).Style.Font.SetFromFont(new Font("Times New Roman", 14));
                worksheet.Row(3).Style.Font.Bold = true;
                worksheet.Cells[3, 8].Value = "Khối lượng T" + month + "-" + year;
                worksheet.Cells[3, 9].Value = "Sản lượng T" + month + "-" + year + " (VNĐ)";
            }
            else
            {
                worksheet.Cells[3, 8].Value = "Khối lượng đã thanh toán";
                worksheet.Cells[3, 9].Value = "Doanh thu";
                if (month != 12)
                {
                    worksheet.Cells[3, 10].Value = "Khối lượng dự kiến";
                    worksheet.Cells[3, 11].Value = "Doanh thu dự kiến";
                }
                else
                {
                    worksheet.Cells[3, 10].Value = "Khối lượng dự kiến";
                    worksheet.Cells[3, 11].Value = "Doanh thu dự kiến";
                }
            }
            var type = "###,###";
            //var type = "###,###";
            worksheet.Column(1).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            worksheet.Column(2).Width = 50;
            worksheet.Column(3).Width = 20;
            worksheet.Column(3).Style.Numberformat.Format = "#,##0.00";
            worksheet.Column(3).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            worksheet.Column(4).Width = 20;
            worksheet.Column(4).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            worksheet.Column(5).Width = 25;
            worksheet.Column(5).Style.Numberformat.Format = type;
            worksheet.Column(5).Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
            worksheet.Column(6).Width = 25;
            worksheet.Column(6).Style.Numberformat.Format = "#,##0.00";
            worksheet.Column(6).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            worksheet.Column(7).Width = 25;
            worksheet.Column(7).Style.Numberformat.Format = type;
            worksheet.Column(7).Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
            worksheet.Column(8).Width = 25;
            worksheet.Column(8).Style.Numberformat.Format = "#,##0.00";
            worksheet.Column(8).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            worksheet.Column(9).Width = 25;
            worksheet.Column(9).Style.Numberformat.Format = type;
            worksheet.Column(9).Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
            worksheet.Column(10).Width = 25;
            worksheet.Column(10).Style.Numberformat.Format = "#,##0.00";
            worksheet.Column(10).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            worksheet.Column(11).Width = 25;
            worksheet.Column(11).Style.Numberformat.Format = type;
            worksheet.Column(11).Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;

            worksheet.View.FreezePanes(4, 1);
            TitleCompanyExcel.FillTitleCommanderWorks(worksheet, projectName, 11, 2, 2);
        }



        public void BindingWorkitemComleteMonthOrQuarterExcel(ExcelWorksheet worksheet, int? month, int? quarter, int year, string projectName)
        {
            worksheet.Cells.Style.WrapText = true;

            using (var range = worksheet.Cells["A:N"])
            {
                range.Style.Font.SetFromFont(new Font("Times New Roman", 13));
            }
            using (var range = worksheet.Cells["A2:N2"])
            {
                range.Style.Font.SetFromFont(new Font("Times New Roman", 14));
                range.Style.Font.Bold = true;
                range.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                range.Style.VerticalAlignment = ExcelVerticalAlignment.Center;
            }

            worksheet.Cells.Style.WrapText = true;
            worksheet.Cells[3, 1].Value = "STT";
            worksheet.Cells[3, 2].Value = "Đơn vị thi công";
            worksheet.Cells[3, 3].Value = "Nội dung công việc";
            worksheet.Cells[3, 4].Value = "Số lượng";
            worksheet.Cells[3, 5].Value = "Đơn vị tính";
            worksheet.Cells[3, 6].Value = "Đơn giá (VNĐ)";



            if (month.HasValue)
            {
                var estimateMonth = 0;
                var estimateYear = 0;
                worksheet.Cells[3, 11].Value = "Lũy kế đến T" + month + "-" + year;
                worksheet.Cells[3, 9].Value = "Thực hiện T" + month + "-" + year;
                if (month != 12)
                {
                    worksheet.Cells[3, 13].Value = "Dự kiến T" + (month + 1) + "-" + year;
                    estimateMonth = month.Value + 1;
                    estimateYear = year;
                }
                else
                {
                    worksheet.Cells[3, 13].Value = "Dự kiến T" + 1 + "-" + (year + 1);
                    estimateMonth = 1;
                    estimateYear = year + 1;
                }
                if (month == 1)
                    worksheet.Cells[3, 7].Value = "Lũy kế đến T12-" + (year - 1);
                else
                    worksheet.Cells[3, 7].Value = "Lũy kế đến T" + (month - 1) + "-" + year;

                var row11 = worksheet.Cells[2, 1].RichText.Add("Tổng hợp sản lượng tháng " + month + " và kế hoạch tháng " + estimateMonth + " năm " + estimateYear + "\r\n");
                row11.Size = 16;
                row11.Bold = true;
                var row12 = worksheet.Cells[2, 1].RichText.Add("Công trình: " + projectName + "" + "\r\n");
                row12.Size = 14;
                row12.Bold = true;
                worksheet.Cells[2, 1].AutoFitColumns();
            }
            else
            {
                var estimateQuarter = 0;
                var estimateYear = 0;
                worksheet.Cells[3, 11].Value = "Lũy kế đến Q" + quarter + "-" + year;
                worksheet.Cells[3, 9].Value = "Thực hiện Q" + quarter + "-" + year;
                if (quarter != 4)
                {
                    worksheet.Cells[3, 13].Value = "Dự kiến Q" + (quarter + 1) + "-" + year;
                    estimateQuarter = quarter.Value + 1;
                    estimateYear = year;
                }
                else
                {
                    worksheet.Cells[3, 13].Value = "Dự kiến Q" + 1 + "-" + (year + 1);
                    estimateQuarter = 1;
                    estimateYear = year + 1;
                }
                if (quarter == 1)
                    worksheet.Cells[3, 7].Value = "Lũy kế đến Q4-" + (year - 1);
                else
                    worksheet.Cells[3, 7].Value = "Lũy kế đến Q" + (quarter - 1) + "-" + year;

                var row11 = worksheet.Cells[2, 1].RichText.Add("Tổng hợp sản lượng quý " + quarter + " và kế hoạch quý " + estimateQuarter + " năm " + estimateYear + "\r\n");
                row11.Size = 16;
                row11.Bold = true;
                var row12 = worksheet.Cells[2, 1].RichText.Add("Công trình: " + projectName + "" + "\r\n");
                row12.Size = 14;
                row12.Bold = true;
                worksheet.Cells[2, 1].AutoFitColumns();
            }
            worksheet.Row(3).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            worksheet.Row(3).Style.VerticalAlignment = ExcelVerticalAlignment.Center;
            worksheet.Row(3).Style.Font.SetFromFont(new Font("Times New Roman", 14));
            worksheet.Row(3).Style.Font.Bold = true;

            var type = "###,###";
            //var type = "###,###";
            worksheet.Column(1).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            worksheet.Column(2).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            worksheet.Column(2).Style.VerticalAlignment = ExcelVerticalAlignment.Center;
            worksheet.Column(2).Width = 25;
            worksheet.Column(3).Width = 50;
            worksheet.Column(4).Width = 20;
            worksheet.Column(4).Style.Numberformat.Format = "#,##0.00";
            worksheet.Column(4).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            worksheet.Column(5).Width = 20;
            worksheet.Column(5).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            worksheet.Column(6).Width = 25;
            worksheet.Column(6).Style.Numberformat.Format = type;
            worksheet.Column(6).Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
            worksheet.Column(7).Width = 25;
            worksheet.Column(7).Style.Numberformat.Format = "#,##0.00";
            worksheet.Column(7).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            worksheet.Column(8).Width = 25;
            worksheet.Column(8).Style.Numberformat.Format = type;
            worksheet.Column(8).Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
            worksheet.Column(9).Width = 25;
            worksheet.Column(9).Style.Numberformat.Format = "#,##0.00";
            worksheet.Column(9).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            worksheet.Column(10).Width = 25;
            worksheet.Column(10).Style.Numberformat.Format = type;
            worksheet.Column(10).Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
            worksheet.Column(11).Width = 25;
            worksheet.Column(11).Style.Numberformat.Format = "#,##0.00";
            worksheet.Column(11).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            worksheet.Column(12).Width = 25;
            worksheet.Column(12).Style.Numberformat.Format = type;
            worksheet.Column(12).Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
            worksheet.Column(13).Width = 25;
            worksheet.Column(13).Style.Numberformat.Format = "#,##0.00";
            worksheet.Column(13).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            worksheet.Column(14).Width = 25;
            worksheet.Column(14).Style.Numberformat.Format = type;
            worksheet.Column(14).Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;

            for (int v = 1; v <= 8; v++)
            {
                if (v % 2 == 1)
                {
                    worksheet.Cells[3, 6 + v, 3, 6 + v + 1].Merge = true;
                    worksheet.Cells[4, 6 + v].Value = "KL";
                }
                else
                {
                    worksheet.Cells[4, 6 + v].Value = "SL";
                }
                worksheet.Cells[4, 6 + v].Style.Font.Bold = true;
                worksheet.Cells[4, 6 + v].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                worksheet.Cells[4, 6 + v].Style.VerticalAlignment = ExcelVerticalAlignment.Center;

            }

            for (int i = 1; i <= 6; i++)
            {
                worksheet.Cells[3, i, 4, i].Merge = true;
            }

            worksheet.View.FreezePanes(5, 1);
            TitleCompanyExcel.FillTitleCommanderWorks(worksheet, projectName, 14, 3, 3);
        }



        public MemoryStream ExportCompletedNumberMonthToExcel(CheckCompletedPerMonthModel model)
        {
            var checkMonths = CheckCompletedNumberPerMonth(model);

            var project = _projectRepository.FindById(model.ProjectId);
            var stream = new MemoryStream();
            using (var excelPackage = new ExcelPackage(stream))
            {

                ExcelWorksheet worksheet = excelPackage.Workbook.Worksheets.Add("Báo cáo sản lượng theo tháng và kế hoạch tháng tới");
                BindingWorkitemComleteMonthOrQuarterExcel(worksheet, model.Month, null, model.Year, project.Name);
                int i = 5;

                foreach (var checkMonth in checkMonths.StatisticCategories)
                {
                    var index = 1;
                    if (checkMonth.StatisticWithCompanies.Count() == 0)
                    {
                        worksheet.Cells[i, 1].Value = checkMonth.STT;
                        worksheet.Cells[i, 3].Value = checkMonth.CategoryName;
                        worksheet.Cells[i, 5].Value = checkMonth.CalculationUnit;
                        worksheet.Cells[i, 4].Value = (!checkMonth.Quantity.HasValue || checkMonth.Quantity == 0) ? null : checkMonth.Quantity;
                        worksheet.Cells[i, 6].Value = (!checkMonth.UnitPrice.HasValue || checkMonth.UnitPrice == 0) ? null : checkMonth.UnitPrice;
                        worksheet.Cells[i, 3].Style.Font.Bold = true;
                        worksheet.Cells[i, 1].Style.Font.Bold = true;
                        if (checkMonth.NotYetAssigned == true)
                        {
                            worksheet.Cells[i, 2].Value = "Chưa giao";
                            worksheet.Cells[i, 2].Style.Font.Bold = true;
                        }

                        if (checkMonth.NextMonth != null)
                        {
                            worksheet.Cells[i, 13].Value = checkMonth.NextMonth.EstimationNumber;
                            worksheet.Cells[i, 14].Value = checkMonth.NextMonth.EstimationYield;
                        }
                        i++;
                    }

                    if (checkMonth.NotYetAssigned == true)
                    {
                        if (checkMonth.ExceptionNotAssigneds.Count() > 0)
                        {
                            foreach (var notAssign in checkMonth.ExceptionNotAssigneds)
                            {
                                worksheet.Cells[i, 1].Value = notAssign.STT;
                                worksheet.Cells[i, 3].Value = notAssign.Name;
                                worksheet.Cells[i, 5].Value = notAssign.CalculationUnit;
                                worksheet.Cells[i, 4].Value = (!notAssign.Quantity.HasValue || notAssign.Quantity == 0) ? null : notAssign.Quantity;
                                worksheet.Cells[i, 6].Value = (!notAssign.UnitPrice.HasValue || notAssign.UnitPrice == 0) ? null : notAssign.UnitPrice;
                                worksheet.Cells[i, 7].Value = 0;
                                worksheet.Cells[i, 9].Value = 0;
                                worksheet.Cells[i, 11].Value = 0;
                                if (notAssign.NextMonth != null)
                                {
                                    worksheet.Cells[i, 13].Value = notAssign.NextMonth.EstimationNumber;
                                    worksheet.Cells[i, 14].Value = notAssign.NextMonth.EstimationYield;
                                }
                                i++;
                            }
                        }
                    }
                    else
                    {
                        if (checkMonth.StatisticWorkitemSmallerWithCompanies.Count() > 0)
                        {
                            foreach (var item in checkMonth.StatisticWorkitemSmallerWithCompanies)
                            {
                                worksheet.Cells[i, 2].Value = item.CompanyName;
                                int k = i;
                                foreach (var detail in item.CompleteWithCompanyDetails)
                                {
                                    worksheet.Cells[i, 1].Value = index;
                                    worksheet.Cells[i, 3].Value = detail.WorkitemName;
                                    worksheet.Cells[i, 5].Value = detail.CalculationUnit;
                                    worksheet.Cells[i, 4].Value = (!detail.Quantity.HasValue || detail.Quantity == 0) ? null : detail.Quantity;
                                    worksheet.Cells[i, 6].Value = (!detail.UnitPrice.HasValue || detail.UnitPrice == 0) ? null : detail.UnitPrice;
                                    worksheet.Cells[i, 7].Value = detail.PreviousMonth.AccumulatedNumber.Value;
                                    worksheet.Cells[i, 8].Value = detail.PreviousMonth.AccumulatedYield.HasValue ? detail.PreviousMonth.AccumulatedYield.Value : 0;
                                    worksheet.Cells[i, 9].Value = detail.CurrentMonth.CompletedNumber.Value;
                                    worksheet.Cells[i, 10].Value = detail.CurrentMonth.Yield.HasValue ? detail.CurrentMonth.Yield.Value : 0;
                                    worksheet.Cells[i, 11].Value = detail.AccumulateCompleteCurrent.AccumulateCurrentNumber.Value;
                                    worksheet.Cells[i, 12].Value = detail.AccumulateCompleteCurrent.AccumulateCurrentYield.HasValue ? detail.AccumulateCompleteCurrent.AccumulateCurrentYield.Value : 0;
                                    worksheet.Cells[i, 13].Value = detail.NextMonth.EstimationNumber;
                                    worksheet.Cells[i, 14].Value = detail.NextMonth.EstimationYield;

                                    worksheet.Cells[k, 2, i, 2].Merge = true;
                                    i++;
                                    index++;
                                }
                            }
                        }
                        else
                        {
                            foreach (var item in checkMonth.StatisticWithCompanies)
                            {
                                worksheet.Cells[i, 2].Value = item.CompanyName;
                                foreach (var detail in item.CompleteWithCompanyDetails)
                                {
                                    worksheet.Cells[i, 1].Value = detail.STT;
                                    worksheet.Cells[i, 3].Value = detail.WorkitemName;
                                    worksheet.Cells[i, 5].Value = detail.CalculationUnit;
                                    worksheet.Cells[i, 4].Value = (!detail.Quantity.HasValue || detail.Quantity == 0) ? null : detail.Quantity;
                                    worksheet.Cells[i, 6].Value = (!detail.UnitPrice.HasValue || detail.UnitPrice == 0) ? null : detail.UnitPrice;
                                    worksheet.Cells[i, 7].Value = detail.PreviousMonth.AccumulatedNumber.Value;
                                    worksheet.Cells[i, 8].Value = detail.PreviousMonth.AccumulatedYield.HasValue ? detail.PreviousMonth.AccumulatedYield.Value : 0;
                                    worksheet.Cells[i, 9].Value = detail.CurrentMonth.CompletedNumber.Value;
                                    worksheet.Cells[i, 10].Value = detail.CurrentMonth.Yield.HasValue ? detail.CurrentMonth.Yield.Value : 0;
                                    worksheet.Cells[i, 11].Value = detail.AccumulateCompleteCurrent.AccumulateCurrentNumber.Value;
                                    worksheet.Cells[i, 12].Value = detail.AccumulateCompleteCurrent.AccumulateCurrentYield.HasValue ? detail.AccumulateCompleteCurrent.AccumulateCurrentYield.Value : 0;
                                    worksheet.Cells[i, 13].Value = detail.NextMonth.EstimationNumber;
                                    worksheet.Cells[i, 14].Value = detail.NextMonth.EstimationYield;

                                    worksheet.Cells[i, 1].Style.Font.Bold = true;
                                    worksheet.Cells[i, 3].Style.Font.Bold = true;
                                    i++;
                                }
                            }
                        }
                    }

                    //worksheet.Cells[i, 1].Value = checkMonth.STT;
                    //worksheet.Cells[i, 3].Value = checkMonth.WorkitemName;
                    //worksheet.Cells[i, 5].Value = checkMonth.CalculationUnit;
                    //worksheet.Cells[i, 4].Value = (!checkMonth.Quantity.HasValue || checkMonth.Quantity == 0)
                    //        ? null : checkMonth.Quantity;
                    //worksheet.Cells[i, 6].Value = (!checkMonth.UnitPrice.HasValue || checkMonth.UnitPrice == 0)
                    //         ? null : checkMonth.UnitPrice;

                    //if ((checkMonth.Quantity.HasValue && checkMonth.Quantity != 0) && (checkMonth.UnitPrice.HasValue && checkMonth.UnitPrice != 0))
                    //{
                    //    worksheet.Cells[i, 7].Value = checkMonth.PreviousMonth.AccumulatedNumber.Value;
                    //    worksheet.Cells[i, 8].Value = checkMonth.PreviousMonth.AccumulatedYield.HasValue ? checkMonth.PreviousMonth.AccumulatedYield.Value : 0;
                    //    worksheet.Cells[i, 9].Value = checkMonth.CurrentMonth.CompletedNumber.Value;
                    //    worksheet.Cells[i, 10].Value = checkMonth.CurrentMonth.Yield.HasValue ? checkMonth.CurrentMonth.Yield.Value : 0;
                    //    worksheet.Cells[i, 11].Value = checkMonth.AccumulateCompleteCurrent.AccumulateCurrentNumber.Value;
                    //    worksheet.Cells[i, 12].Value = checkMonth.AccumulateCompleteCurrent.AccumulateCurrentYield.HasValue ? checkMonth.AccumulateCompleteCurrent.AccumulateCurrentYield.Value : 0;
                    //    worksheet.Cells[i, 13].Value = checkMonth.NextMonth.EstimationNumber;
                    //    worksheet.Cells[i, 14].Value = checkMonth.NextMonth.EstimationYield;
                    //}

                    //if (checkMonth.LevelHeading == 1)
                    //{
                    //    worksheet.Cells[i, 1].Style.Font.Bold = true;
                    //    worksheet.Cells[i, 3].Style.Font.Bold = true;
                    //}
                    //else
                    //{
                    //    worksheet.Cells[i, 1].Style.Font.Bold = false;
                    //    worksheet.Cells[i, 3].Style.Font.Bold = false;
                    //}


                }



                var countCheck = checkMonths.CompletedPerMonth.Count();
                int next = i + 1;
                foreach (var item in checkMonths.YieldWithCompany)
                {
                    worksheet.Cells[next, 3].Value = item.CompanyName;
                    worksheet.Cells[next, 8].Value = item.AccumulatePreviousByCompany;
                    worksheet.Cells[next, 10].Value = item.YieldByCompany;
                    worksheet.Cells[next, 12].Value = item.AccumulateCurrentByCompany;
                    worksheet.Row(next).Style.Numberformat.Format = "#,###";
                    worksheet.Row(next).Style.Font.Bold = true;
                    next++;
                }

                worksheet.Cells[next + 1, 1].Value = "Tổng";
                worksheet.Cells[next + 1, 8].Value = checkMonths.TotalAccumulatePrevious;
                worksheet.Cells[next + 1, 10].Value = checkMonths.TotalYield;
                worksheet.Cells[next + 1, 12].Value = checkMonths.TotalAccumulateCurrent;
                worksheet.Cells[next + 1, 14].Value = checkMonths.TotalNextMonthYield;
                worksheet.Row(next + 1).Style.Numberformat.Format = "#,###";
                worksheet.Row(next + 1).Style.Font.Bold = true;

                stream = new MemoryStream(excelPackage.GetAsByteArray());

            }
            return stream;
        }

        public MemoryStream ExportCompletedNumberQuarterToExcel(CheckCompletedPerQuarterModel model)
        {
            var checkQuarters = EstimateWorkitemCompleteQuarter(model);

            var project = _projectRepository.FindById(model.ProjectId);
            var stream = new MemoryStream();
            using (var excelPackage = new ExcelPackage(stream))
            {

                ExcelWorksheet worksheet = excelPackage.Workbook.Worksheets.Add("Báo cáo sản lượng theo tháng và kế hoạch tháng tới");
                BindingWorkitemComleteMonthOrQuarterExcel(worksheet, null, model.Quarter, model.Year, project.Name);
                int i = 5;

                foreach (var checkQuarter in checkQuarters.StatisticCategories)
                {
                    var index = 1;
                    if (checkQuarter.StatisticWithCompanies.Count() == 0)
                    {
                        worksheet.Cells[i, 1].Value = checkQuarter.STT;
                        worksheet.Cells[i, 3].Value = checkQuarter.CategoryName;
                        worksheet.Cells[i, 5].Value = checkQuarter.CalculationUnit;
                        worksheet.Cells[i, 4].Value = (!checkQuarter.Quantity.HasValue || checkQuarter.Quantity == 0) ? null : checkQuarter.Quantity;
                        worksheet.Cells[i, 6].Value = (!checkQuarter.UnitPrice.HasValue || checkQuarter.UnitPrice == 0) ? null : checkQuarter.UnitPrice;
                        worksheet.Cells[i, 3].Style.Font.Bold = true;
                        worksheet.Cells[i, 1].Style.Font.Bold = true;
                        if (checkQuarter.NotYetAssigned == true)
                        {
                            worksheet.Cells[i, 2].Value = "Chưa giao";
                            worksheet.Cells[i, 2].Style.Font.Bold = true;
                        }
                        if (checkQuarter.NextQuarter != null)
                        {
                            worksheet.Cells[i, 13].Value = checkQuarter.NextQuarter.EstimationNumber;
                            worksheet.Cells[i, 14].Value = checkQuarter.NextQuarter.EstimationYield;
                        }
                        i++;
                    }
                    if (checkQuarter.NotYetAssigned == true)
                    {
                        if (checkQuarter.ExceptionNotAssigneds.Count() > 0)
                        {
                            foreach (var notAssign in checkQuarter.ExceptionNotAssigneds)
                            {
                                worksheet.Cells[i, 1].Value = notAssign.STT;
                                worksheet.Cells[i, 3].Value = notAssign.Name;
                                worksheet.Cells[i, 5].Value = notAssign.CalculationUnit;
                                worksheet.Cells[i, 4].Value = (!notAssign.Quantity.HasValue || notAssign.Quantity == 0) ? null : notAssign.Quantity;
                                worksheet.Cells[i, 6].Value = (!notAssign.UnitPrice.HasValue || notAssign.UnitPrice == 0) ? null : notAssign.UnitPrice;
                                worksheet.Cells[i, 7].Value = 0;
                                worksheet.Cells[i, 9].Value = 0;
                                worksheet.Cells[i, 11].Value = 0;
                                if (notAssign.NextQuarter != null)
                                {
                                    worksheet.Cells[i, 13].Value = notAssign.NextQuarter.EstimationNumber;
                                    worksheet.Cells[i, 14].Value = notAssign.NextQuarter.EstimationYield;
                                }
                                i++;
                            }
                        }

                    }
                    else
                    {
                        if (checkQuarter.StatisticWorkitemSmallerWithCompanies.Count() > 0)
                        {
                            foreach (var item in checkQuarter.StatisticWorkitemSmallerWithCompanies)
                            {
                                worksheet.Cells[i, 2].Value = item.CompanyName;
                                int k = i;
                                foreach (var detail in item.CompleteWithCompanyDetails)
                                {
                                    worksheet.Cells[i, 1].Value = index;
                                    worksheet.Cells[i, 3].Value = detail.WorkitemName;
                                    worksheet.Cells[i, 5].Value = detail.CalculationUnit;
                                    worksheet.Cells[i, 4].Value = (!detail.Quantity.HasValue || detail.Quantity == 0) ? null : detail.Quantity;
                                    worksheet.Cells[i, 6].Value = (!detail.UnitPrice.HasValue || detail.UnitPrice == 0) ? null : detail.UnitPrice;
                                    worksheet.Cells[i, 7].Value = detail.PreviousQuarter.AccumulatedNumber.Value;
                                    worksheet.Cells[i, 8].Value = detail.PreviousQuarter.AccumulatedYield.HasValue ? detail.PreviousQuarter.AccumulatedYield.Value : 0;
                                    worksheet.Cells[i, 9].Value = detail.CurrentQuarter.CompletedNumber.Value;
                                    worksheet.Cells[i, 10].Value = detail.CurrentQuarter.Yield.HasValue ? detail.CurrentQuarter.Yield.Value : 0;
                                    worksheet.Cells[i, 11].Value = detail.AccumulateCompleteCurrent.AccumulateCurrentNumber.Value;
                                    worksheet.Cells[i, 12].Value = detail.AccumulateCompleteCurrent.AccumulateCurrentYield.HasValue ? detail.AccumulateCompleteCurrent.AccumulateCurrentYield.Value : 0;
                                    worksheet.Cells[i, 13].Value = detail.NextQuarter.EstimationNumber;
                                    worksheet.Cells[i, 14].Value = detail.NextQuarter.EstimationYield;

                                    worksheet.Cells[k, 2, i, 2].Merge = true;
                                    i++;
                                    index++;
                                }
                            }
                        }
                        else
                        {
                            foreach (var item in checkQuarter.StatisticWithCompanies)
                            {
                                worksheet.Cells[i, 2].Value = item.CompanyName;
                                foreach (var detail in item.CompleteWithCompanyDetails)
                                {
                                    worksheet.Cells[i, 1].Value = detail.STT;
                                    worksheet.Cells[i, 3].Value = detail.WorkitemName;
                                    worksheet.Cells[i, 5].Value = detail.CalculationUnit;
                                    worksheet.Cells[i, 4].Value = (!detail.Quantity.HasValue || detail.Quantity == 0) ? null : detail.Quantity;
                                    worksheet.Cells[i, 6].Value = (!detail.UnitPrice.HasValue || detail.UnitPrice == 0) ? null : detail.UnitPrice;
                                    worksheet.Cells[i, 7].Value = detail.PreviousQuarter.AccumulatedNumber.Value;
                                    worksheet.Cells[i, 8].Value = detail.PreviousQuarter.AccumulatedYield.HasValue ? detail.PreviousQuarter.AccumulatedYield.Value : 0;
                                    worksheet.Cells[i, 9].Value = detail.CurrentQuarter.CompletedNumber.Value;
                                    worksheet.Cells[i, 10].Value = detail.CurrentQuarter.Yield.HasValue ? detail.CurrentQuarter.Yield.Value : 0;
                                    worksheet.Cells[i, 11].Value = detail.AccumulateCompleteCurrent.AccumulateCurrentNumber.Value;
                                    worksheet.Cells[i, 12].Value = detail.AccumulateCompleteCurrent.AccumulateCurrentYield.HasValue ? detail.AccumulateCompleteCurrent.AccumulateCurrentYield.Value : 0;
                                    worksheet.Cells[i, 13].Value = detail.NextQuarter.EstimationNumber;
                                    worksheet.Cells[i, 14].Value = detail.NextQuarter.EstimationYield;

                                    worksheet.Cells[i, 1].Style.Font.Bold = true;
                                    worksheet.Cells[i, 3].Style.Font.Bold = true;
                                    i++;
                                }
                            }
                        }
                    }
                }
                var countCheck = checkQuarters.CompletedPerQuarter.Count();
                int next = i + 1;
                foreach (var item in checkQuarters.YieldWithCompany)
                {
                    worksheet.Cells[next, 3].Value = item.CompanyName;
                    worksheet.Cells[next, 8].Value = item.AccumulatePreviousByCompany;
                    worksheet.Cells[next, 10].Value = item.YieldByCompany;
                    worksheet.Cells[next, 12].Value = item.AccumulateCurrentByCompany;
                    worksheet.Row(next).Style.Numberformat.Format = "#,###";
                    worksheet.Row(next).Style.Font.Bold = true;
                    next++;
                }

                worksheet.Cells[next + 1, 1].Value = "Tổng";
                worksheet.Cells[next + 1, 8].Value = checkQuarters.TotalAccumulatePrevious;
                worksheet.Cells[next + 1, 10].Value = checkQuarters.TotalYield;
                worksheet.Cells[next + 1, 12].Value = checkQuarters.TotalAccumulateCurrent;
                worksheet.Cells[next + 1, 14].Value = checkQuarters.TotalNextQuarterYield;
                worksheet.Row(next + 1).Style.Numberformat.Format = "#,###";
                worksheet.Row(next + 1).Style.Font.Bold = true;

                stream = new MemoryStream(excelPackage.GetAsByteArray());

            }
            return stream;
        }

        public YieldInQuarterModel EstimateWorkitemCompleteQuarter(CheckCompletedPerQuarterModel checkWorkitem)
        {
            var workitemPerQuarters = new YieldInQuarterModel();
            var workitems = _workitemRepository.FindAll(c => c.ProjectId == checkWorkitem.ProjectId);
            var inputWorkitemIds = workitems.Select(c => c.Id).ToList();

            var workitemIds = workitems.Select(c => c.Id);
            var assignAlls = _workitemCompanyRepository.FindAll(c => workitemIds.Contains(c.WorkItemId));
            var workitemAssignIds = assignAlls.Select(c => c.WorkItemId).Distinct();

            var companyIds = assignAlls.Select(c => c.CompanyId).Distinct();

            var companies = _companyRepository.FindAll(c => companyIds.Contains(c.Id));

            var workitemCompleted = WorkitemCompletedReportPerQuarter(inputWorkitemIds, checkWorkitem.Quarter, checkWorkitem.Year, checkWorkitem.ProjectId, companies);

            var completePrevious = WorkitemAccumulatePreviousQuarter(inputWorkitemIds, checkWorkitem.Quarter, checkWorkitem.Year, checkWorkitem.ProjectId, companies);

            var listEstimatePlan = _estimationPlanRepository.FindAll(c => workitemIds.Contains(c.WorkitemId) && c.Type == EstimationType.EstimationQuarterComplete);

            if (workitems.Count() > 0)
            {
                decimal totalYield = 0;
                decimal totalYieldAccmulatePrevious = 0;
                decimal totalNextMonthYield = 0;
                decimal totalCompletedYield = 0;
                foreach (var workitem in workitems)
                {
                    var quarterCurrentCompletedNumber = workitemCompleted.FirstOrDefault(c => c.WorkitemId == workitem.Id).SumCompletedNumber;
                    var yieldCurrentQuarter = workitem.UnitPrice * quarterCurrentCompletedNumber;
                    var completeAccumulatePrevious = completePrevious.FirstOrDefault(c => c.WorkitemId == workitem.Id).SumCompletedNumber;
                    var yieldAccumulatePrevious = workitem.UnitPrice * completeAccumulatePrevious;

                    var workitemPerQuarter = new WorkitemCompletedPerQuarter
                    {

                        WorkitemId = workitem.Id,
                        STT = workitem.STT,
                        WorkitemName = workitem.Name,
                        Quantity = workitem.Quantity.HasValue ? workitem.Quantity : null,
                        CalculationUnit = !string.IsNullOrEmpty(workitem.CalculationUnit) ? workitem.CalculationUnit : null,
                        UnitPrice = workitem.UnitPrice.HasValue ? workitem.UnitPrice : null,
                        LevelHeading = workitem.LevelHeading.Value,
                        CompletedNumberTotal = workitem.CompletedNumberTotal,
                    };

                    if (workitemPerQuarter.UnitPrice.HasValue)
                    {
                        if (workitemPerQuarter.CompletedNumberTotal.HasValue)
                        {
                            workitemPerQuarter.CompletedYield = workitemPerQuarter.UnitPrice.Value * workitemPerQuarter.CompletedNumberTotal.Value;
                            totalCompletedYield += workitemPerQuarter.CompletedYield.Value;
                        }
                        else workitemPerQuarter.CompletedYield = 0;
                    }

                    if (workitemPerQuarter.CompletedYield.HasValue)
                    {
                        workitemPerQuarter.CompletedYield = NumberHelper.RoundDown(workitemPerQuarter.CompletedYield.Value, 2);
                    }

                    if (workitemPerQuarter.Quantity.HasValue)
                    {
                        workitemPerQuarter.Quantity = NumberHelper.RoundDown(workitemPerQuarter.Quantity.Value, 2);
                    }

                    if (workitemPerQuarter.UnitPrice.HasValue)
                    {
                        workitemPerQuarter.UnitPrice = NumberHelper.RoundDown(workitemPerQuarter.UnitPrice.Value, 2);
                    }

                    if (workitemPerQuarter.CompletedNumberTotal.HasValue)
                    {
                        workitemPerQuarter.CompletedNumberTotal = NumberHelper.RoundDown(workitemPerQuarter.CompletedNumberTotal.Value, 2);
                    }

                    if (HeadingExcels.DeteminedLevelHeading(workitemPerQuarter.STT) == 1) workitemPerQuarter.IsBold = true;
                    if (workitem.Quantity.HasValue && workitem.UnitPrice.HasValue)
                    {
                        workitemPerQuarter.CurrentQuarter = new WorkitemCompletedCurrentQuarter
                        {
                            CompletedNumber = quarterCurrentCompletedNumber,
                            Yield = yieldCurrentQuarter.HasValue ? yieldCurrentQuarter.Value : 0
                        };

                        workitemPerQuarter.PreviousQuarter = new AccumulatePreviousQuarter
                        {
                            AccumulatedNumber = completeAccumulatePrevious,
                            AccumulatedYield = yieldAccumulatePrevious.HasValue ? yieldAccumulatePrevious.Value : 0
                        };

                        workitemPerQuarter.AccumulateCompleteCurrent = new AccumulateCompleteCurrentQuarter
                        {
                            AccumulateCurrentNumber = workitemPerQuarter.CurrentQuarter.CompletedNumber + workitemPerQuarter.PreviousQuarter.AccumulatedNumber,
                            AccumulateCurrentYield = workitemPerQuarter.CurrentQuarter.Yield + workitemPerQuarter.PreviousQuarter.AccumulatedYield
                        };

                        if (workitemPerQuarter.PreviousQuarter.AccumulatedNumber.HasValue)
                        {
                            workitemPerQuarter.PreviousQuarter.AccumulatedNumber = NumberHelper.RoundDown(workitemPerQuarter.PreviousQuarter.AccumulatedNumber.Value, 2);
                        }

                        if (workitemPerQuarter.PreviousQuarter.AccumulatedYield.HasValue)
                        {
                            workitemPerQuarter.PreviousQuarter.AccumulatedYield = NumberHelper.RoundDown(workitemPerQuarter.PreviousQuarter.AccumulatedYield.Value, 0);
                        }

                        if (workitemPerQuarter.CurrentQuarter.CompletedNumber.HasValue)
                        {
                            workitemPerQuarter.CurrentQuarter.CompletedNumber = NumberHelper.RoundDown(workitemPerQuarter.CurrentQuarter.CompletedNumber.Value, 2);
                        }

                        if (workitemPerQuarter.CurrentQuarter.Yield.HasValue)
                        {
                            workitemPerQuarter.CurrentQuarter.Yield = NumberHelper.RoundDown(workitemPerQuarter.CurrentQuarter.Yield.Value, 0);
                        }

                        if (workitemPerQuarter.AccumulateCompleteCurrent.AccumulateCurrentNumber.HasValue)
                        {
                            workitemPerQuarter.AccumulateCompleteCurrent.AccumulateCurrentNumber = NumberHelper.RoundDown(workitemPerQuarter.AccumulateCompleteCurrent.AccumulateCurrentNumber.Value, 2);
                        }

                        if (workitemPerQuarter.AccumulateCompleteCurrent.AccumulateCurrentYield.HasValue)
                        {
                            workitemPerQuarter.AccumulateCompleteCurrent.AccumulateCurrentYield = NumberHelper.RoundDown(workitemPerQuarter.AccumulateCompleteCurrent.AccumulateCurrentYield.Value, 0);
                        }


                        if (workitemPerQuarter.CompletedNumberTotal.HasValue)
                        {
                            workitemPerQuarter.CompletedNumberTotal = NumberHelper.RoundDown(workitemPerQuarter.CompletedNumberTotal.Value, 2);
                        }
                    }
                    if (yieldCurrentQuarter.HasValue)
                    {
                        totalYield += yieldCurrentQuarter.Value;
                    }
                    if (yieldAccumulatePrevious.HasValue)
                    {
                        totalYieldAccmulatePrevious += yieldAccumulatePrevious.Value;
                    }

                    var estimationPlan = new EstimationPlan();

                    if (workitem.Quantity.HasValue && workitem.UnitPrice.HasValue)
                    {
                        if (checkWorkitem.Quarter == 4)
                        {
                            estimationPlan = listEstimatePlan.FirstOrDefault(c => c.WorkitemId == workitem.Id && c.Quarter == 1
                                 && c.Year == checkWorkitem.Year + 1);
                        }
                        else
                        {
                            estimationPlan = listEstimatePlan.FirstOrDefault(c => c.WorkitemId == workitem.Id && c.Quarter == checkWorkitem.Quarter + 1
                                && c.Year == checkWorkitem.Year);
                        }

                        if (estimationPlan != null)
                        {
                            workitemPerQuarter.NextQuarter = new WorkitemEstimationNextQuarter
                            {
                                EstimationNumber = estimationPlan.Quantity,
                                EstimationYield = estimationPlan.Quantity * workitem.UnitPrice.Value
                            };

                            workitemPerQuarter.NextQuarter.EstimationNumber = NumberHelper.RoundDown(workitemPerQuarter.NextQuarter.EstimationNumber, 2);
                            workitemPerQuarter.NextQuarter.EstimationYield = NumberHelper.RoundDown(workitemPerQuarter.NextQuarter.EstimationYield, 0);

                            totalNextMonthYield += workitemPerQuarter.NextQuarter.EstimationYield;
                            totalNextMonthYield = NumberHelper.RoundDown(totalNextMonthYield, 0);

                        }

                        else workitemPerQuarter.NextQuarter = new WorkitemEstimationNextQuarter
                        {
                            EstimationNumber = 0,
                            EstimationYield = 0
                        };
                    }
                    // Ước lượng hoàn thành hạng mục theo tháng tới              

                    workitemPerQuarters.CompletedPerQuarter.Add(workitemPerQuarter);
                }
                workitemPerQuarters.TotalAccumulatePrevious = NumberHelper.RoundDown(totalYieldAccmulatePrevious, 0);
                workitemPerQuarters.TotalYield = NumberHelper.RoundDown(totalYield, 0);
                workitemPerQuarters.TotalAccumulateCurrent = NumberHelper.RoundDown(totalYieldAccmulatePrevious + totalYield, 0);
                workitemPerQuarters.TotalNextQuarterYield = NumberHelper.RoundDown(totalNextMonthYield, 0);
                workitemPerQuarters.TotalCompletedYield = NumberHelper.RoundDown(totalCompletedYield, 0);

                workitemPerQuarters.YieldWithCompany = CalcalateYieldWithCompanyByTime(checkWorkitem.ProjectId, null, checkWorkitem.Quarter, checkWorkitem.Year);


                var workitemWithHeadings = DivideWorkitem(checkWorkitem.ProjectId);

                foreach (var item in workitemWithHeadings)
                {
                    var checkIds = new List<int>();
                    checkIds.Add(item.WorkitemCategory.Id);
                    if (item.WorkItemSmallerModels.Count() > 0)
                    {
                        var smallerIds = item.WorkItemSmallerModels.Select(c => c.Id);
                        checkIds.AddRange(smallerIds);
                    }

                    var addCategory = new StatisticQuarterCategoryModel
                    {
                        CategoryId = item.WorkitemCategory.Id,
                        CategoryName = item.WorkitemCategory.Name,
                        STT = item.WorkitemCategory.STT,
                        UnitPrice = item.WorkitemCategory.UnitPrice,
                        Quantity = item.WorkitemCategory.Quantity,
                        CalculationUnit = item.WorkitemCategory.CalculationUnit,
                    };

                    if (workitemAssignIds.Any(c => checkIds.Contains(c)))
                    {
                        addCategory.NotYetAssigned = false;
                        if (workitemAssignIds.Any(c => c == item.WorkitemCategory.Id) && workitemAssignIds.Count(c => checkIds.Contains(c)) == 1) // quy định chỉ 1 hạng mục lớn
                        {
                            var assigns = assignAlls.Where(c => c.WorkItemId == item.WorkitemCategory.Id);
                            foreach (var company in companies)
                            {
                                var assignIns = assigns.Where(c => c.CompanyId == company.Id);
                                if (assignIns.Count() > 0)
                                {
                                    var detailSmallers = new List<CompleteInQuarterWithCompanyDetail>();
                                    var workitemIns = assignIns.Select(c => c.WorkItemId).Distinct();
                                    foreach (var id in workitemIns)
                                    {
                                        var first = workitems.FirstOrDefault(c => c.Id == id);

                                        var previousWorkitemIn = completePrevious.FirstOrDefault(c => c.WorkitemId == id);

                                        var previousCompanyIn = previousWorkitemIn.ReportWithCompanies.FirstOrDefault(c => c.CompanyId == company.Id);


                                        var currentWorkitemIn = workitemCompleted.FirstOrDefault(c => c.WorkitemId == id);

                                        var currentCompanyIn = currentWorkitemIn.ReportWithCompanies.FirstOrDefault(c => c.CompanyId == company.Id);

                                        var addDetail = new CompleteInQuarterWithCompanyDetail
                                        {
                                            WorkitemId = id,
                                            WorkitemName = first.Name,
                                            STT = first.STT,
                                            CalculationUnit = first.CalculationUnit,
                                            UnitPrice = first.UnitPrice,
                                            Quantity = first.Quantity,
                                            IntoMoney = first.IntoMoney,
                                            LevelHeading = first.LevelHeading,
                                            CurrentQuarter = new WorkitemCompletedCurrentQuarter { },
                                            PreviousQuarter = new AccumulatePreviousQuarter { },
                                            AccumulateCompleteCurrent = new AccumulateCompleteCurrentQuarter { },
                                            NextQuarter = new WorkitemEstimationNextQuarter { }

                                        };
                                        var estimationPlan = new EstimationPlan();
                                        if (first.Quantity.HasValue && first.UnitPrice.HasValue)
                                        {
                                            if (checkWorkitem.Quarter == 4)
                                            {
                                                estimationPlan = listEstimatePlan.FirstOrDefault(c => c.WorkitemId == first.Id && c.Quarter == 1
                                                    && c.Year == checkWorkitem.Year + 1);
                                            }
                                            else
                                            {
                                                estimationPlan = listEstimatePlan.FirstOrDefault(c => c.WorkitemId == first.Id && c.Quarter == checkWorkitem.Quarter + 1
                                                    && c.Year == checkWorkitem.Year);
                                            }

                                            if (estimationPlan != null)
                                            {
                                                addDetail.NextQuarter = new WorkitemEstimationNextQuarter
                                                {
                                                    EstimationNumber = estimationPlan.Quantity,
                                                    EstimationYield = estimationPlan.Quantity * first.UnitPrice.Value
                                                };

                                                addDetail.NextQuarter.EstimationNumber = NumberHelper.RoundDown(addDetail.NextQuarter.EstimationNumber, 2);
                                                addDetail.NextQuarter.EstimationYield = NumberHelper.RoundDown(addDetail.NextQuarter.EstimationYield, 0);
                                            }

                                            else addDetail.NextQuarter = new WorkitemEstimationNextQuarter
                                            {
                                                EstimationNumber = 0,
                                                EstimationYield = 0
                                            };
                                        }

                                        if (previousCompanyIn != null)
                                        {
                                            addDetail.PreviousQuarter.AccumulatedNumber = previousCompanyIn.CompleteNumber;
                                            addDetail.PreviousQuarter.AccumulatedYield = previousCompanyIn.CompleteNumber * first.UnitPrice;
                                        }
                                        else
                                        {
                                            addDetail.PreviousQuarter.AccumulatedNumber = 0;
                                            addDetail.PreviousQuarter.AccumulatedYield = 0;
                                        }

                                        if (currentCompanyIn != null)
                                        {
                                            addDetail.CurrentQuarter.CompletedNumber = currentCompanyIn.CompleteNumber;
                                            addDetail.CurrentQuarter.Yield = currentCompanyIn.CompleteNumber * first.UnitPrice;
                                        }
                                        else
                                        {
                                            addDetail.CurrentQuarter.CompletedNumber = 0;
                                            addDetail.CurrentQuarter.Yield = 0;
                                        }

                                        addDetail.AccumulateCompleteCurrent.AccumulateCurrentNumber = addDetail.PreviousQuarter.AccumulatedNumber + addDetail.CurrentQuarter.CompletedNumber;
                                        addDetail.AccumulateCompleteCurrent.AccumulateCurrentYield = addDetail.PreviousQuarter.AccumulatedYield + addDetail.CurrentQuarter.Yield;

                                        detailSmallers.Add(addDetail);
                                    }

                                    var staticSmaller = new AccumulateQuarterCompleteWithCompany
                                    {
                                        CompanyId = company.Id,
                                        CompanyName = company.Name,
                                        CompleteWithCompanyDetails = detailSmallers
                                    };

                                    addCategory.StatisticWithCompanies.Add(staticSmaller);
                                }
                            }
                        }
                        if (!workitemAssignIds.Any(c => c == item.WorkitemCategory.Id) && workitemAssignIds.Count(c => checkIds.Contains(c)) > 0)// phân giao có chứa hạng mục con
                        {
                            var assigns = assignAlls.Where(c => checkIds.Contains(c.WorkItemId));
                            foreach (var company in companies)
                            {
                                var assignIns = assigns.Where(c => c.CompanyId == company.Id);
                                if (assignIns.Count() > 0)
                                {
                                    var detailSmallers = new List<CompleteInQuarterWithCompanyDetail>();
                                    var workitemIns = assignIns.Select(c => c.WorkItemId).Distinct();
                                    foreach (var id in workitemIns)
                                    {
                                        var first = workitems.FirstOrDefault(c => c.Id == id);

                                        var previousWorkitemIn = completePrevious.FirstOrDefault(c => c.WorkitemId == id);

                                        var previousCompanyIn = previousWorkitemIn.ReportWithCompanies.FirstOrDefault(c => c.CompanyId == company.Id);


                                        var currentWorkitemIn = workitemCompleted.FirstOrDefault(c => c.WorkitemId == id);

                                        var currentCompanyIn = currentWorkitemIn.ReportWithCompanies.FirstOrDefault(c => c.CompanyId == company.Id);

                                        var addDetail = new CompleteInQuarterWithCompanyDetail
                                        {
                                            WorkitemId = id,
                                            WorkitemName = first.Name,
                                            STT = first.STT,
                                            CalculationUnit = first.CalculationUnit,
                                            UnitPrice = first.UnitPrice,
                                            Quantity = first.Quantity,
                                            IntoMoney = first.IntoMoney,
                                            LevelHeading = first.LevelHeading,
                                            CurrentQuarter = new WorkitemCompletedCurrentQuarter { },
                                            PreviousQuarter = new AccumulatePreviousQuarter { },
                                            AccumulateCompleteCurrent = new AccumulateCompleteCurrentQuarter { },
                                            NextQuarter = new WorkitemEstimationNextQuarter { }

                                        };
                                        var estimationPlan = new EstimationPlan();
                                        if (first.Quantity.HasValue && first.UnitPrice.HasValue)
                                        {
                                            if (checkWorkitem.Quarter == 4)
                                            {
                                                estimationPlan = listEstimatePlan.FirstOrDefault(c => c.WorkitemId == first.Id && c.Quarter == 1
                                                     && c.Year == checkWorkitem.Year + 1);
                                            }
                                            else
                                            {
                                                estimationPlan = listEstimatePlan.FirstOrDefault(c => c.WorkitemId == first.Id && c.Quarter == checkWorkitem.Quarter + 1
                                                    && c.Year == checkWorkitem.Year);
                                            }

                                            if (estimationPlan != null)
                                            {
                                                addDetail.NextQuarter = new WorkitemEstimationNextQuarter
                                                {
                                                    EstimationNumber = estimationPlan.Quantity,
                                                    EstimationYield = estimationPlan.Quantity * first.UnitPrice.Value
                                                };

                                                addDetail.NextQuarter.EstimationNumber = NumberHelper.RoundDown(addDetail.NextQuarter.EstimationNumber, 2);
                                                addDetail.NextQuarter.EstimationYield = NumberHelper.RoundDown(addDetail.NextQuarter.EstimationYield, 0);
                                            }

                                            else addDetail.NextQuarter = new WorkitemEstimationNextQuarter
                                            {
                                                EstimationNumber = 0,
                                                EstimationYield = 0
                                            };
                                        }

                                        if (previousCompanyIn != null)
                                        {
                                            addDetail.PreviousQuarter.AccumulatedNumber = previousCompanyIn.CompleteNumber;
                                            addDetail.PreviousQuarter.AccumulatedYield = previousCompanyIn.CompleteNumber * first.UnitPrice;
                                        }
                                        else
                                        {
                                            addDetail.PreviousQuarter.AccumulatedNumber = 0;
                                            addDetail.PreviousQuarter.AccumulatedYield = 0;
                                        }

                                        if (currentCompanyIn != null)
                                        {
                                            addDetail.CurrentQuarter.CompletedNumber = currentCompanyIn.CompleteNumber;
                                            addDetail.CurrentQuarter.Yield = currentCompanyIn.CompleteNumber * first.UnitPrice;
                                        }
                                        else
                                        {
                                            addDetail.CurrentQuarter.CompletedNumber = 0;
                                            addDetail.CurrentQuarter.Yield = 0;
                                        }

                                        addDetail.AccumulateCompleteCurrent.AccumulateCurrentNumber = addDetail.PreviousQuarter.AccumulatedNumber + addDetail.CurrentQuarter.CompletedNumber;
                                        addDetail.AccumulateCompleteCurrent.AccumulateCurrentYield = addDetail.PreviousQuarter.AccumulatedYield + addDetail.CurrentQuarter.Yield;

                                        detailSmallers.Add(addDetail);
                                    }

                                    var staticSmaller = new AccumulateQuarterCompleteWithCompany
                                    {
                                        CompanyId = company.Id,
                                        CompanyName = company.Name,
                                        CompleteWithCompanyDetails = detailSmallers
                                    };

                                    addCategory.StatisticWorkitemSmallerWithCompanies.Add(staticSmaller);
                                }
                            }
                        }
                    }
                    else // Chưa giao
                    {
                        var nextQuarterCategory = new WorkitemEstimationNextQuarter();
                        var estimateForCategory = listEstimatePlan.Where(c => c.WorkitemId == item.WorkitemCategory.Id);
                        var estimationPlan = new EstimationPlan();
                        if (estimateForCategory != null)
                        {
                            if (checkWorkitem.Quarter == 4)
                            {
                                estimationPlan = estimateForCategory.FirstOrDefault(c => c.Quarter == 1 && c.Year == checkWorkitem.Year + 1);
                            }
                            else
                            {
                                estimationPlan = estimateForCategory.FirstOrDefault(c => c.Quarter == checkWorkitem.Quarter + 1 && c.Year == checkWorkitem.Year);
                            }

                            if (estimationPlan != null)
                            {
                                nextQuarterCategory.EstimationNumber = estimationPlan.Quantity;
                                nextQuarterCategory.EstimationYield = NumberHelper.RoundDown(nextQuarterCategory.EstimationNumber * item.WorkitemCategory.UnitPrice.Value, 0);
                            }
                        }
                        addCategory.NextQuarter = nextQuarterCategory;
                        foreach (var smaller in item.WorkItemSmallerModels)
                        {
                            var nextQuarterSmaller = new WorkitemEstimationNextQuarter();
                            var estimationPlanSmaller = new EstimationPlan();
                            if (checkWorkitem.Quarter == 4)
                            {
                                estimationPlanSmaller = listEstimatePlan.FirstOrDefault(c => c.WorkitemId == smaller.Id && c.Quarter == 1 && c.Year == checkWorkitem.Year + 1);
                            }
                            else
                            {
                                estimationPlanSmaller = listEstimatePlan.FirstOrDefault(c => c.WorkitemId == smaller.Id && c.Quarter == checkWorkitem.Quarter + 1 && c.Year == checkWorkitem.Year);
                            }

                            if (estimationPlanSmaller != null)
                            {
                                nextQuarterSmaller.EstimationNumber = estimationPlanSmaller.Quantity;
                                nextQuarterSmaller.EstimationYield = NumberHelper.RoundDown(nextQuarterSmaller.EstimationNumber * smaller.UnitPrice.Value, 0);
                            }
                            smaller.NextQuarter = nextQuarterSmaller;
                        }
                        addCategory.ExceptionNotAssigneds = item.WorkItemSmallerModels;

                    }
                    workitemPerQuarters.StatisticCategories.Add(addCategory);
                    //workitemPerMonths.StatisticCategories.Add()
                }


            }
            return workitemPerQuarters;
        }

        public bool EstimateMonthlyOutput(List<EstimationModel> monthEstimations)
        {
            var trans = _estimationPlanRepository.BeginTransaction();
            if (trans.Connection.State != ConnectionState.Open) trans.Connection.Open();
            bool success = false;
            try
            {
                foreach (var monthEstimation in monthEstimations)
                {
                    var workitem = _workitemRepository.FindById(monthEstimation.WorkitemId, trans);
                    if (workitem == null) throw new ServiceException("Hạng mục này không tồn tại");
                    //if (workitem.WorkitemGroupId == null) throw new ServiceException("Không được lên kế hoạch hoàn thành cho hạng mục cha");
                    var workitemCompany = _workitemCompanyRepository.FindAll(c => c.WorkItemId == workitem.Id, trans);
                    var totalCompleted = workitemCompany.Select(c => c.CompletedNumber).Sum();
                    if (totalCompleted + monthEstimation.Quantity > workitem.Quantity)
                        throw new ServiceException("Tổng kế hoạch hoàn thành cho tháng này và số lượng hạng mục hoàn thành lớn hơn số lượng hạng mục ban đầu");

                    var estimation = _estimationPlanRepository.Find(c => c.WorkitemId == monthEstimation.WorkitemId && c.Type == EstimationType.EstimationMonthComplete
                        && c.Month == monthEstimation.Month && c.Year == monthEstimation.Year, trans);
                    if (estimation != null)
                    {
                        success = _estimationPlanRepository.Update(new EstimationPlan
                        {
                            Id = estimation.Id,
                            Name = monthEstimation.Name,
                            Month = monthEstimation.Month,
                            WorkitemId = monthEstimation.WorkitemId,
                            Quantity = monthEstimation.Quantity,
                            Year = monthEstimation.Year,
                            Type = EstimationType.EstimationMonthComplete
                        }, trans);
                    }
                    else success = _estimationPlanRepository.Insert(new EstimationPlan
                    {
                        Name = monthEstimation.Name,
                        Month = monthEstimation.Month,
                        WorkitemId = monthEstimation.WorkitemId,
                        Quantity = monthEstimation.Quantity,
                        Year = monthEstimation.Year,
                        Type = EstimationType.EstimationMonthComplete
                    }, trans);
                    if (!success) trans.Rollback();
                }
            }
            catch (Exception)
            {
                trans.Rollback();
                throw;
            }
            trans.Commit();
            return success;
        }
        public bool EstimationCompleteForQuarter(List<EstimationModel> quarterEstimations)
        {
            var trans = _estimationPlanRepository.BeginTransaction();
            if (trans.Connection.State != ConnectionState.Open) trans.Connection.Open();
            bool success = false;
            try
            {
                foreach (var quarterEstimation in quarterEstimations)
                {
                    var workitem = _workitemRepository.FindById(quarterEstimation.WorkitemId, trans);
                    if (workitem == null) throw new ServiceException("Hạng mục này không tồn tại");
                    //if (workitem.WorkitemGroupId == null) throw new ServiceException("Không được lên kế hoạch hoàn thành cho hạng mục cha");
                    var workitemCompany = _workitemCompanyRepository.FindAll(c => c.WorkItemId == workitem.Id, trans);
                    var totalCompleted = workitemCompany.Select(c => c.CompletedNumber).Sum();
                    if (totalCompleted + quarterEstimation.Quantity > workitem.Quantity)
                        throw new ServiceException("Tổng kế hoạch hoàn thành cho quý này và số lượng hạng mục hoàn thành lớn hơn số lượng hạng mục ban đầu");

                    var estimation = _estimationPlanRepository.Find(c => c.WorkitemId == quarterEstimation.WorkitemId && c.Type == EstimationType.EstimationQuarterComplete
                        && c.Quarter == quarterEstimation.Quarter && c.Year == quarterEstimation.Year, trans);
                    if (estimation != null)
                    {
                        success = _estimationPlanRepository.Update(new EstimationPlan
                        {
                            Id = estimation.Id,
                            Name = quarterEstimation.Name,
                            Quarter = quarterEstimation.Quarter,
                            WorkitemId = quarterEstimation.WorkitemId,
                            Quantity = quarterEstimation.Quantity,
                            Year = quarterEstimation.Year,
                            Type = EstimationType.EstimationQuarterComplete
                        }, trans);
                    }
                    else success = _estimationPlanRepository.Insert(new EstimationPlan
                    {
                        Name = quarterEstimation.Name,
                        Quarter = quarterEstimation.Quarter,
                        WorkitemId = quarterEstimation.WorkitemId,
                        Quantity = quarterEstimation.Quantity,
                        Year = quarterEstimation.Year,
                        Type = EstimationType.EstimationQuarterComplete
                    }, trans);
                    if (!success) trans.Rollback();
                }
            }
            catch (Exception)
            {
                trans.Rollback();
                throw;
            }
            trans.Commit();
            return success;
        }


        public IEnumerable<SettlementWorkitem> FilterSettlementByQuarter(int quarter, IEnumerable<SettlementWorkitem> settlementWorkitems)
        {
            var result = new List<SettlementWorkitem>();
            switch (quarter)
            {
                case 1:
                    result = settlementWorkitems.Where(c => c.SettlementDate.Month == 1 || c.SettlementDate.Month == 2
                        || c.SettlementDate.Month == 3).ToList();
                    break;
                case 2:
                    result = settlementWorkitems.Where(c => c.SettlementDate.Month == 4 || c.SettlementDate.Month == 5
                        || c.SettlementDate.Month == 6).ToList();
                    break;
                case 3:
                    result = settlementWorkitems.Where(c => c.SettlementDate.Month == 7 || c.SettlementDate.Month == 8
                        || c.SettlementDate.Month == 9).ToList();
                    break;
                case 4:
                    result = settlementWorkitems.Where(c => c.SettlementDate.Month == 10 || c.SettlementDate.Month == 11
                        || c.SettlementDate.Month == 12).ToList();
                    break;
            }
            return result;
        }

        public IEnumerable<WorkitemSettlementPerQuarterModel> ShowSettlementPlanWithQuarterly(int projectId, int year, int quarter)
        {
            var workitems = _workitemRepository.FindAll(c => c.ProjectId == projectId);

            var settlementForQuarterCurrents = new List<WorkitemSettlementPerQuarterModel>();
            foreach (var workitem in workitems)
            {
                var settlementWorkitems = _settlementWorkitemRepository.FindAll(c => c.WorkitemId == workitem.Id && c.Type == SettlementType.SettlementTimes);
                var settlementFilterByYears = new List<SettlementWorkitem>();
                var settlementFilterByQuarter = new SettlementWorkitem();
                var nextQuarterEstimate = new EstimationPlan();
                if (quarter == 4)
                {
                    nextQuarterEstimate = _estimationPlanRepository.Find(c => c.WorkitemId == workitem.Id && c.Quarter == 1
                           && c.Year == year + 1 && c.Type == EstimationType.QuarterRevenueEstimation);
                }
                else
                {
                    nextQuarterEstimate = _estimationPlanRepository.Find(c => c.WorkitemId == workitem.Id && c.Quarter == quarter + 1
                           && c.Year == year && c.Type == EstimationType.QuarterRevenueEstimation);
                }

                settlementFilterByQuarter = settlementWorkitems.Where(c => c.Year == year
                    && c.Quarter == quarter).FirstOrDefault();
                decimal settlementQuarter = 0;
                if (settlementFilterByQuarter != null)
                    settlementQuarter = settlementFilterByQuarter.SettlementNumber;
                var numberNextQuarter = new decimal();
                if (nextQuarterEstimate == null) numberNextQuarter = 0;
                else numberNextQuarter = nextQuarterEstimate.Quantity;
                var settlementForQuarterCurrent = new WorkitemSettlementPerQuarterModel
                {
                    WorkitemId = workitem.Id,
                    WorkitemName = workitem.Name,
                    Quantity = workitem.Quantity.Value,
                    TotalPaidQuantity = workitem.TotalPaidQuantity.Value,
                    CalculationUnit = workitem.CalculationUnit,
                    //Money = workitem.Money.Value,

                    QuarterCurrent = new SettlementForQuarterCurrent
                    {
                        AccumulatedNumber = settlementQuarter,
                        //Revenue = settlementQuarter * workitem.Money.Value
                    },
                    NextQuarter = new SettlementForNextQuarter
                    {
                        EstimateNumber = numberNextQuarter,
                        //EstimateRevenue = numberNextQuarter * workitem.Money.Value
                    }
                };
                //if (workitem.WorkitemGroupId.HasValue) settlementForQuarterCurrent.WorkitemGroupId = workitem.WorkitemGroupId.Value;
                //settlementForQuarterCurrents.Add(settlementForQuarterCurrent);
            };
            return settlementForQuarterCurrents;
        }

        public Stream ExportSettlementPlanWithQuarterlyToExcel(int projectId, int year, int quarter)
        {
            var project = _projectRepository.FindById(projectId);
            var settlements = ShowSettlementPlanWithQuarterly(projectId, year, quarter);
            var stream = new MemoryStream();
            using (var excelPackage = new ExcelPackage())
            {
                ExcelWorksheet worksheet = excelPackage.Workbook.Worksheets.Add("Báo cáo thanh toán theo quý và kế hoạch thanh toán quý tới");
                int i = 2;
                BindingFormatForExcel(worksheet, null, quarter, year, project.Name);

                foreach (var settlement in settlements)
                {
                    worksheet.Cells[i, 1].Value = i - 1;
                    worksheet.Cells[i, 2].Value = settlement.WorkitemName;
                    worksheet.Cells[i, 3].Value = settlement.Quantity;
                    worksheet.Cells[i, 4].Value = settlement.CalculationUnit;
                    worksheet.Cells[i, 5].Value = settlement.Money;
                    worksheet.Cells[i, 6].Value = settlement.QuarterCurrent.AccumulatedNumber;
                    worksheet.Cells[i, 7].Value = settlement.QuarterCurrent.Revenue;
                    worksheet.Cells[i, 8].Value = settlement.NextQuarter.EstimateNumber;
                    worksheet.Cells[i, 9].Value = settlement.NextQuarter.EstimateRevenue;

                    i++;
                }

                worksheet.Row(1).Height = 50;
                excelPackage.Save();
                stream = new MemoryStream(excelPackage.GetAsByteArray());
                return stream;
            }
        }

        public bool EstimationSettlementForQuarter(IEnumerable<EstimationModel> quarterEstimations, IDbTransaction trans)
        {
            var success = true;
            if (quarterEstimations.Count() > 0)
            {
                foreach (var quarterEstimation in quarterEstimations)
                {
                    var workitem = _workitemRepository.FindById(quarterEstimation.WorkitemId, trans);
                    //if (workitem.WorkitemGroupId == null) throw new ServiceException("Không được thanh toán hạng mục theo quý cho hạng mục cha");
                    var settlementWorkitems = _settlementWorkitemRepository.FindAll(c => c.WorkitemId == quarterEstimation.WorkitemId &&
                        c.Type == SettlementType.SettlementTimes, trans);
                    var estimationPlan = _estimationPlanRepository.Find(c => c.WorkitemId == quarterEstimation.WorkitemId
                        && c.Type == EstimationType.QuarterRevenueEstimation && c.Year == quarterEstimation.Year
                        && c.Quarter == quarterEstimation.Quarter, trans);

                    if (estimationPlan != null)
                        success = _estimationPlanRepository.Update(new EstimationPlan
                        {
                            Id = estimationPlan.Id,
                            Name = quarterEstimation.Name,
                            Quarter = quarterEstimation.Quarter,
                            WorkitemId = quarterEstimation.WorkitemId,
                            Quantity = quarterEstimation.Quantity,
                            Year = quarterEstimation.Year,
                            Type = EstimationType.QuarterRevenueEstimation
                        }, trans);
                    else success = _estimationPlanRepository.Insert(new EstimationPlan
                    {

                        Name = quarterEstimation.Name,
                        Quarter = quarterEstimation.Quarter,
                        WorkitemId = quarterEstimation.WorkitemId,
                        Quantity = quarterEstimation.Quantity,
                        Year = quarterEstimation.Year,
                        Type = EstimationType.QuarterRevenueEstimation
                    }, trans);
                }
            }
            return success;
        }

        public bool SettlementWorkitemForQuarter(IEnumerable<PerformingSettlementModel> settlementModels, IDbTransaction trans)
        {
            var success = true;
            if (settlementModels.Count() > 0)
            {

                foreach (var settlementModel in settlementModels)
                {
                    if (!settlementModel.Quarter.HasValue) throw new ServiceException("Chưa nhập số quý để thanh toán");
                    if (!settlementModel.Year.HasValue) throw new ServiceException("Chưa nhập số năm để thanh toán");
                    var existWorkitem = _workitemRepository.FindById(settlementModel.WorkitemId, trans);
                    if (existWorkitem == null) throw new ServiceException("Hạng mục không tồn tại");

                    var existSettlementQuarter = _settlementWorkitemRepository.Find(c => c.WorkitemId == settlementModel.WorkitemId
                        && c.Type == SettlementType.SettlementTimes && c.Quarter == settlementModel.Quarter.Value
                        && c.Year == settlementModel.Year, trans);

                    var settlementAll = _settlementWorkitemRepository.FindAll(c => c.WorkitemId == settlementModel.WorkitemId
                       && c.Type == SettlementType.SettlementTimes, trans);

                    var totalSettlement = settlementAll.Select(c => c.SettlementNumber).Sum();

                    var totalSettlementQuarter = _settlementWorkitemRepository.FindAll(c => c.WorkitemId == settlementModel.WorkitemId
                        && c.Type == SettlementType.SettlementTimes, trans).Select(c => c.SettlementNumber).Sum();

                    decimal calcQuantity = 0;
                    if (existSettlementQuarter != null)
                    {
                        calcQuantity = totalSettlement + (settlementModel.Quantity - existSettlementQuarter.SettlementNumber);
                        existSettlementQuarter.SettlementNumber = settlementModel.Quantity;
                        success = _settlementWorkitemRepository.Update(existSettlementQuarter, trans);
                    }
                    else
                    {
                        calcQuantity = totalSettlement + settlementModel.Quantity;
                        success = _settlementWorkitemRepository.Insert(new SettlementWorkitem
                        {
                            WorkitemId = settlementModel.WorkitemId,
                            SettlementNumber = settlementModel.Quantity,
                            SettlementDate = DateTime.UtcNow,
                            Quarter = settlementModel.Quarter.Value,
                            Year = settlementModel.Year.Value,
                            Type = SettlementType.SettlementTimes
                        }, trans);
                    }
                    var payTimes = _settlementWorkitemRepository.FindAll(c => c.WorkitemId == settlementModel.WorkitemId
                        && c.Quarter == null && c.Year == null, trans);
                    decimal totalPayTimes = 0;
                    if (payTimes.Count() > 0)
                    {
                        totalPayTimes = payTimes.Select(c => c.SettlementNumber).Sum();
                    }
                    if (success)
                    {
                        if (calcQuantity > existWorkitem.Quantity)
                            throw new ServiceException("Tổng số lượng thanh toán lớn hơn số lượng hạng mục");
                        existWorkitem.TotalPaidQuantity = calcQuantity + totalPayTimes;
                        success = _workitemRepository.Update(existWorkitem, trans);
                    }
                }
            }
            return success;
        }



        public IEnumerable<WorkItemModel> GetListParentWorkitems(int projectId)
        {
            throw new NotImplementedException();
        }

        public List<WorkitemWithHeadingModel> DivideWorkitem(int projectId)
        {
            var result = new List<WorkitemWithHeadingModel>();
            var workitemHeading = new WorkitemWithHeadingModel();
            workitemHeading.WorkItemSmallerModels = new List<WorkitemInfoModel>();
            var workitems = _workitemRepository.FindAll(c => c.ProjectId == projectId).OrderBy(c => c.RowNumber);
            var maxWorkitems = workitems.Where(c => c.LevelHeading == 1).OrderBy(c => c.RowNumber).FirstOrDefault();

            var workitemDivide = new List<Workitem>();

            int index = 1;

            var reports = _projectReportRepository.FindAll(c => c.ProjectId == projectId && c.Status == ProjectReportStatus.Approved);
            var reportIds = reports.Select(c => c.Id);
            var reportWorkitems = _projectReportWorkitemRepository.FindAll(c => reportIds.Contains(c.ProjectReportId) && c.CompletedNumber > 0);

            decimal totalIntoMoney = 0;
            foreach (var workitem in workitems)
            {
                if (workitem.IntoMoney.HasValue)
                {
                    totalIntoMoney = totalIntoMoney + workitem.IntoMoney.Value;
                }
                if (HeadingExcels.DeteminedLevelHeading(workitem.STT) == 1)
                {
                    index++;
                    if (index != 1)
                    {
                        workitemHeading = new WorkitemWithHeadingModel();
                        workitemHeading.WorkitemCategory = workitem.CloneToModel<Workitem, WorkitemInfoModel>();
                        if (workitemHeading.WorkitemCategory.UnitPrice.HasValue)
                        {
                            workitemHeading.WorkitemCategory.UnitPrice = NumberHelper.RoundDown(workitemHeading.WorkitemCategory.UnitPrice.Value, 2);
                        }
                        if (workitemHeading.WorkitemCategory.Quantity.HasValue)
                        {
                            workitemHeading.WorkitemCategory.Quantity = NumberHelper.RoundDown(workitemHeading.WorkitemCategory.Quantity.Value, 2);
                        }
                        if (workitemHeading.WorkitemCategory.IntoMoney.HasValue)
                        {
                            workitemHeading.WorkitemCategory.IntoMoney = NumberHelper.RoundDown(workitemHeading.WorkitemCategory.IntoMoney.Value, 2);
                        }
                        workitemHeading.WorkitemCategory.IsBold = true;
                        workitemHeading.WorkItemSmallerModels = new List<WorkitemInfoModel>();
                        result.Add(workitemHeading);
                        continue;
                    }
                }

                if (HeadingExcels.DeteminedLevelHeading(workitem.STT) != 1)
                {
                    var t = workitem.CloneToModel<Workitem, WorkitemInfoModel>();
                    if (t.UnitPrice.HasValue)
                    {
                        t.UnitPrice = NumberHelper.RoundDown(t.UnitPrice.Value, 2);
                    }
                    if (t.Quantity.HasValue)
                    {
                        t.Quantity = NumberHelper.RoundDown(t.Quantity.Value, 2);
                    }
                    if (t.IntoMoney.HasValue)
                    {
                        t.IntoMoney = NumberHelper.RoundDown(t.IntoMoney.Value, 2);
                    }
                    var reportWorkitemEquals = reportWorkitems.Where(c => c.WorkitemId == t.Id);
                    t.PlaceCollection = FillWorkitemReportPlace(t.Id, reportWorkitemEquals);
                    t.PlaceCountCollection = StringUtil.CountItemWhenSeperate(t.PlaceCollection);
                    workitemHeading.WorkItemSmallerModels.Add(t);
                }

            }
            foreach (var item in result)
            {

                item.TotalIntoMoney = NumberHelper.RoundDown(totalIntoMoney, 0);
            }
            return result;
        }


        public bool UpdateWorkItemRevenueEstimate(UpdateWorkItemRevenueModel model)
        {
            var result = false;
            var trans = _estimationPlanRepository.BeginTransaction();
            if (trans.Connection.State != ConnectionState.Open) trans.Connection.Open();
            try
            {
                var workitems = _workitemRepository.FindAll(c => c.ProjectId == model.ProjectId, trans);
                var workitemIds = workitems.Select(c => c.Id);
                var estimate = _estimationPlanRepository.FindAll(c => workitemIds.Contains(c.WorkitemId)
                    && c.Type == EstimationType.QuarterRevenueEstimation, trans);

                if (model.UpdateRevenueInfos.GroupBy(c => new { c.WorkItemId, c.Quarter, c.Year })
                   .Any(a => a.Count() > 1))
                    throw new ServiceException("Dữ liệu không thể trùng hạng mục, quý, và năm");

                var workitemIdUpdates = model.UpdateRevenueInfos.Select(c => c.WorkItemId);

                var workitemUpdates = workitems.Where(c => workitemIdUpdates.Contains(c.Id));

                foreach (var modelUpdate in model.UpdateRevenueInfos)
                {
                    var workitem = workitems.FirstOrDefault(c => c.Id == modelUpdate.WorkItemId);

                    if (workitem.Quantity.HasValue && workitem.UnitPrice.HasValue) { }
                    else
                    {
                        throw new ServiceException("Hạng mục phải có số lượng và giá");
                    }

                    if (workitem == null)
                        throw new ServiceException("Hạng mục không tồn tại");
                    else
                    {
                        if (modelUpdate.Quantity > workitem.Quantity) throw new ServiceException("Ước lượng quý " + modelUpdate.Quarter +
                            " trong hạng mục " + workitem.Name + " lớn hơn số lượng số lượng ban đầu");
                    }

                    var estimateExist = estimate.FirstOrDefault(c => c.WorkitemId == modelUpdate.WorkItemId && c.Year == modelUpdate.Year
                        && c.Quarter == modelUpdate.Quarter);


                    if (estimateExist != null)
                    {
                        estimateExist.Quantity = modelUpdate.Quantity;
                        result = _estimationPlanRepository.Update(estimateExist, trans);
                    }
                    else
                    {
                        result = _estimationPlanRepository.Insert(new EstimationPlan
                        {
                            WorkitemId = modelUpdate.WorkItemId,
                            Quarter = modelUpdate.Quarter,
                            Year = modelUpdate.Year,
                            Type = EstimationType.QuarterRevenueEstimation,
                            Quantity = modelUpdate.Quantity,
                            Name = "Doanh thu dự kiến quý " + modelUpdate.Quarter + "-" + modelUpdate.Year
                        }, trans);
                    }

                    if (!result)
                    {
                        trans.Rollback();
                        break;
                    }
                }

                var estimateNews = _estimationPlanRepository.FindAll(c => workitemIds.Contains(c.WorkitemId) && c.Type == EstimationType.QuarterRevenueEstimation, trans);
                foreach (var check in model.UpdateRevenueInfos)
                {
                    var totalEstimate = estimateNews.Where(c => c.WorkitemId == check.WorkItemId).Sum(a => a.Quantity);
                    var workitem = workitems.FirstOrDefault(c => c.Id == check.WorkItemId);

                    if (totalEstimate > workitem.Quantity) throw new ServiceException("Tổng ước lượng các quý không được lớn hơn số lượng hạng mục ban đầu");
                }
            }
            catch (Exception ex)
            {
                trans.Rollback();
                throw ex;
            }
            trans.Commit();
            return result;
        }

        public RevenueEstimateResultModel GetRevenueEstimate(int projectId, int year)
        {
            var result = new RevenueEstimateResultModel();
            var workitems = _workitemRepository.FindAll(c => c.ProjectId == projectId);
            var workitemIds = workitems.Select(c => c.Id);
            var estimates = _estimationPlanRepository.FindAll(c => workitemIds.Contains(c.WorkitemId) && c.Type == EstimationType.QuarterRevenueEstimation && c.Year == year);
            result.WorkitemRevenueInfos = new List<WorkitemRevenueInfoModel>();
            foreach (var workitem in workitems)
            {
                var modelAdd = new WorkitemRevenueInfoModel();
                modelAdd.WorkitemId = workitem.Id;
                modelAdd.STT = workitem.STT;
                modelAdd.LevelHeading = workitem.LevelHeading.HasValue ? workitem.LevelHeading.Value : 0;
                modelAdd.CalculationUnit = workitem.CalculationUnit;

                modelAdd.Quantity = workitem.Quantity.HasValue ? workitem.Quantity : null;
                if (workitem.Quantity.HasValue)
                {
                    modelAdd.Quantity = NumberHelper.RoundDown(workitem.Quantity.Value, 2);
                }
                modelAdd.WorkitemName = workitem.Name;

                if (workitem.UnitPrice.HasValue)
                {
                    modelAdd.UnitPrice = NumberHelper.RoundDown(workitem.UnitPrice.Value, 2);
                }
                if (HeadingExcels.DeteminedLevelHeading(modelAdd.STT) == 1) modelAdd.IsBold = true;
                modelAdd.QuarterRevenues = new List<QuarterRevenueInfo>();
                for (int i = 1; i <= 4; i++)
                {
                    var revenue = new QuarterRevenueInfo();
                    revenue.Quarter = i;
                    var check = estimates.FirstOrDefault(c => c.WorkitemId == workitem.Id && c.Quarter == i);
                    if (workitem.Quantity.HasValue && workitem.UnitPrice.HasValue)
                    {
                        if (check == null)
                            revenue.Quantity = 0;
                        else
                        {
                            revenue.Quantity = check.Quantity;
                            if (i == 1)
                            {
                                result.TotalEstimateRevenueQuarter1 += check.Quantity * workitem.UnitPrice.Value;
                                result.TotalEstimateRevenueQuarter1 = NumberHelper.RoundDown(result.TotalEstimateRevenueQuarter1, 0);
                            }

                            if (i == 2)
                            {
                                result.TotalEstimateRevenueQuarter2 += check.Quantity * workitem.UnitPrice.Value;
                                result.TotalEstimateRevenueQuarter2 = NumberHelper.RoundDown(result.TotalEstimateRevenueQuarter2, 0);
                            }

                            if (i == 3)
                            {
                                result.TotalEstimateRevenueQuarter3 += check.Quantity * workitem.UnitPrice.Value;
                                result.TotalEstimateRevenueQuarter3 = NumberHelper.RoundDown(result.TotalEstimateRevenueQuarter3, 0);
                            }

                            if (i == 4)
                            {
                                result.TotalEstimateRevenueQuarter4 += check.Quantity * workitem.UnitPrice.Value;
                                result.TotalEstimateRevenueQuarter4 = NumberHelper.RoundDown(result.TotalEstimateRevenueQuarter4, 2);
                            }
                        }
                        modelAdd.QuarterRevenues.Add(revenue);
                    }

                }
                result.WorkitemRevenueInfos.Add(modelAdd);
            }
            return result;
        }


        public bool SettlementWorkitemWithTimes(UpdateWorkitemSettlementModel model)
        {
            var result = false;

            var workitems = _workitemRepository.FindAll(c => c.ProjectId == model.ProjectId);
            var workitemIds = workitems.Select(c => c.Id);
            var test = model.UpdateSettlementInfos.GroupBy(c => new { c.WorkItemId, c.Times }).Where(a => a.Count() > 1).Count();
            if (model.UpdateSettlementInfos.GroupBy(c => new { c.WorkItemId, c.Times }).Any(a => a.Count() > 1))
                throw new ServiceException("Dữ liệu không được trùng số lần thanh toán và hạng mục");
            var trans = _settlementWorkitemRepository.BeginTransaction();

            if (trans.Connection.State != ConnectionState.Open) trans.Connection.Open();
            try
            {
                if (model.UpdateSettlementInfos.Count() > 0)
                {

                    var settlement = _settlementWorkitemRepository.FindAll(c => workitemIds.Contains(c.WorkitemId) && c.Type == SettlementType.SettlementTimes, trans);

                    var workItemIdUpdates = model.UpdateSettlementInfos.Select(c => c.WorkItemId);

                    var workItemUpdate = workitems.Where(c => workItemIdUpdates.Contains(c.Id));

                    foreach (var modelUpdate in model.UpdateSettlementInfos)
                    {
                        var workItem = workitems.FirstOrDefault(c => c.Id == modelUpdate.WorkItemId);
                        if (workItem.Quantity.HasValue && workItem.UnitPrice.HasValue) { }
                        else throw new ServiceException("Hạng mục phải có số lượng và đơn giá mới được thanh toán");

                        if (workItem == null) throw new ServiceException("Hạng mục không tồn tại");

                        else
                        {
                            if (modelUpdate.SettlementNumber > workItem.Quantity)
                                throw new ServiceException("Doanh thu lần " + modelUpdate.Times + " trong hạng mục " + workItem.Name + " lớn hơn số lượng");
                        }

                        var settlementExit = settlement.FirstOrDefault(c => c.WorkitemId == modelUpdate.WorkItemId && c.Times == modelUpdate.Times);
                        if (settlementExit != null)
                        {
                            settlementExit.SettlementNumber = modelUpdate.SettlementNumber;
                            result = _settlementWorkitemRepository.Update(settlementExit, trans);
                        }

                        else
                        {
                            var addModel = new SettlementWorkitem
                            {
                                WorkitemId = modelUpdate.WorkItemId,
                                Type = SettlementType.SettlementTimes,
                                SettlementNumber = modelUpdate.SettlementNumber,
                                Times = modelUpdate.Times
                            };
                            var timeExist = settlement.Where(c => workitemIds.Contains(c.WorkitemId) && c.Times == modelUpdate.Times);
                            if (timeExist.Count() > 0)
                            {
                                addModel.SettlementDate = timeExist.FirstOrDefault().SettlementDate;
                            }
                            else addModel.SettlementDate = modelUpdate.SettlementDate.ToUniversalTime();
                            result = _settlementWorkitemRepository.Insert(addModel, trans);
                        }

                        if (!result)
                        {
                            trans.Rollback();
                            break;
                        }

                    }

                    var settlementNews = _settlementWorkitemRepository.FindAll(c => workitemIds.Contains(c.WorkitemId) && c.Type == SettlementType.SettlementTimes, trans);

                    foreach (var check in model.UpdateSettlementInfos)
                    {
                        var totalSettlement = settlementNews.Where(c => c.WorkitemId == check.WorkItemId).Sum(c => c.SettlementNumber);
                        var workitem = workitems.FirstOrDefault(c => c.Id == check.WorkItemId);
                        if (totalSettlement > workitem.Quantity) throw new Exception("Tổng số lượng doanh thu các lần phải nhỏ hơn số lượng ban đầu");

                        workitem.TotalPaidQuantity = totalSettlement;
                        result = _workitemRepository.Update(workitem, trans);
                        if (!result)
                        {
                            trans.Rollback();
                            break;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                trans.Rollback();
                throw ex;
            }
            trans.Commit();
            return result;
        }

        public SettlementRevenueResultModel ListSettlementWorkItemByTime(int projectId)
        {
            var result = new SettlementRevenueResultModel();
            var workitems = _workitemRepository.FindAll(c => c.ProjectId == projectId);
            var workitemIds = workitems.Select(c => c.Id);
            var settlements = _settlementWorkitemRepository.FindAll(c => workitemIds.Contains(c.WorkitemId));
            var maxOrder = 0;
            if (settlements.Count() > 0)
                maxOrder = settlements.Max(c => c.Times);
            result.SettlementWorkitemInfos = new List<SettlementWorkitemInfoModel>();
            result.TotalRevenues = new List<TotalRevenueWithTimeModel>();


            foreach (var workitem in workitems)
            {
                var modelAdd = new SettlementWorkitemInfoModel
                {
                    WorkitemId = workitem.Id,
                    STT = workitem.STT,
                    LevelHeading = workitem.LevelHeading.Value,
                    WorkitemName = workitem.Name,
                    Quantity = workitem.Quantity,
                    TotalPaidQuantity = workitem.TotalPaidQuantity.Value,
                    CalculationUnit = workitem.CalculationUnit,
                    UnitPrice = workitem.UnitPrice
                };

                if (modelAdd.UnitPrice.HasValue)
                {
                    modelAdd.UnitPrice = NumberHelper.RoundDown(modelAdd.UnitPrice.Value, 2);
                }

                if (modelAdd.Quantity.HasValue)
                {
                    modelAdd.Quantity = NumberHelper.RoundDown(modelAdd.Quantity.Value, 2);
                }

                modelAdd.TotalPaidQuantity = NumberHelper.RoundDown(modelAdd.TotalPaidQuantity, 2);

                if (HeadingExcels.DeteminedLevelHeading(modelAdd.STT) == 1) modelAdd.IsBold = true;


                if (workitem.UnitPrice.HasValue || workitem.Quantity.HasValue)
                {
                    modelAdd.TimesRevenues = new List<TimesRevenueInfo>();
                    for (int i = 1; i <= maxOrder; i++)
                    {
                        var timeRevenueInfo = new TimesRevenueInfo();
                        timeRevenueInfo.Times = i;
                        var settlementExist = settlements.FirstOrDefault(c => c.WorkitemId == workitem.Id
                            && c.Times == i);
                        if (settlementExist != null)
                            timeRevenueInfo.SettlementNumber = settlementExist.SettlementNumber;
                        else timeRevenueInfo.SettlementNumber = 0;

                        modelAdd.TimesRevenues.Add(timeRevenueInfo);
                    }
                }

                result.SettlementWorkitemInfos.Add(modelAdd);
                result.TimeMax = maxOrder;
            }

            for (int i = 1; i <= result.TimeMax; i++)
            {
                var totalRevenue = new TotalRevenueWithTimeModel();
                foreach (var r in result.SettlementWorkitemInfos)
                {
                    if (r.Quantity.HasValue && r.UnitPrice.HasValue)
                    {
                        totalRevenue.Times = i;
                        totalRevenue.TotalValue += r.TimesRevenues.FirstOrDefault(c => c.Times == i).SettlementNumber
                            * r.UnitPrice.Value;
                    }
                }
                result.TotalRevenues.Add(totalRevenue);
            }

            return result;
        }

        public int DetermineQuater(DateTime time)
        {
            var result = 0;
            switch (time.Month)
            {
                case 1: result = 1; break;
                case 2: result = 1; break;
                case 3: result = 1; break;
                case 4: result = 2; break;
                case 5: result = 2; break;
                case 6: result = 2; break;
                case 7: result = 3; break;
                case 8: result = 3; break;
                case 9: result = 3; break;
                case 10: result = 4; break;
                case 11: result = 4; break;
                case 12: result = 4; break;
                default:
                    break;
            }
            return result;
        }

        public List<FillEstimateToSettlementResult> FillEstimateToSettlement(FillEstimateToSettlementModel fillEstimate)
        {
            var result = new List<FillEstimateToSettlementResult>();
            var workitems = _workitemRepository.FindAll(c => c.ProjectId == fillEstimate.ProjectId);
            var workitemIds = workitems.Select(c => c.Id);
            var estimates = _estimationPlanRepository.FindAll(c => workitemIds.Contains(c.WorkitemId)
                && c.Type == EstimationType.QuarterRevenueEstimation);

            var determineQuarter = DetermineQuater(fillEstimate.DaysToFill.ToLocalTime());
            var estimateFillByQuaters = estimates.Where(c => c.Quarter == determineQuarter);

            foreach (var workitem in workitems)
            {
                var modelFill = new FillEstimateToSettlementResult();
                modelFill.WorkitemId = workitem.Id;
                if (workitem.Quantity.HasValue && workitem.UnitPrice.HasValue)
                {
                    var estimateWorkitem = estimateFillByQuaters.FirstOrDefault(c => c.WorkitemId == workitem.Id);
                    if (estimateWorkitem != null)
                        modelFill.EstimateNumber = estimateWorkitem.Quantity;
                    else modelFill.EstimateNumber = 0;
                }

                result.Add(modelFill);
            }
            return result;
        }

        public void SetBackgroundSettlementWithTimeToExcel(ExcelWorksheet worksheet, int timeMax, string projectName)
        {
            worksheet.Cells.Style.WrapText = true;


            using (var range = worksheet.Cells["A:Y"])
            {
                range.Style.Font.SetFromFont(new Font("Times New Roman", 13));
            }
            using (var range = worksheet.Cells["A3:Y3"])
            {
                range.Style.Font.SetFromFont(new Font("Times New Roman", 14));
                range.Style.Font.Bold = true;
                range.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                range.Style.VerticalAlignment = ExcelVerticalAlignment.Center;
            }

            worksheet.Cells[3, 1].Value = "STT";
            worksheet.Cells[3, 2].Value = "Hạng mục";
            worksheet.Cells[3, 3].Value = "Số lượng";
            worksheet.Cells[3, 4].Value = "Đơn vị tính";
            worksheet.Cells[3, 5].Value = "Đơn giá (VNĐ)";

            worksheet.Row(3).Height = 60;
            worksheet.Row(3).Style.Font.Size = 14;
            var type = "###,###";
            worksheet.Column(1).Width = 10;
            worksheet.Column(1).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            worksheet.Column(2).Width = 50;
            worksheet.Column(3).Width = 20;
            worksheet.Column(3).Style.Numberformat.Format = "#,##0.00";
            worksheet.Column(3).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            worksheet.Column(4).Width = 20;
            worksheet.Column(4).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            worksheet.Column(5).Width = 25;
            worksheet.Column(5).Style.Numberformat.Format = type;
            worksheet.Column(5).Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;

            var row21 = worksheet.Cells[2, 1].RichText.Add("Tổng hợp theo dõi danh toán" + "\r\n");
            row21.Size = 16;
            row21.Bold = true;
            var row22 = worksheet.Cells[2, 1].RichText.Add("Công trình: " + projectName + "" + "\r\n");
            row22.Size = 14;
            row22.Bold = true;
            worksheet.Cells[2, 1].AutoFitColumns();



            worksheet.View.FreezePanes(4, 1);
            TitleCompanyExcel.FillTitleCommanderWorks(worksheet, projectName, 5 + timeMax, 2, 2);
            for (int i = 1; i <= timeMax; i++)
            {
                worksheet.Cells[3, 5 + i].Value = "Lần " + i;
                worksheet.Column(5 + i).Width = 20;
                worksheet.Column(5 + i).Style.Numberformat.Format = "#,##0.00";
                worksheet.Column(5 + i).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            }
        }

        public MemoryStream ExportSettlementWithTimeToExcel(int projectId)
        {
            var exportModels = ListSettlementWorkItemByTime(projectId);
            var project = _projectRepository.FindById(projectId);
            var stream = new MemoryStream();
            using (var excelPackage = new ExcelPackage(stream))
            {

                ExcelWorksheet worksheet = excelPackage.Workbook.Worksheets.Add("Báo cáo ước lượng thanh toán theo quý");
                SetBackgroundSettlementWithTimeToExcel(worksheet, exportModels.TimeMax, project.Name);
                int i = 4;
                var count = exportModels.SettlementWorkitemInfos.Count();
                foreach (var exportModel in exportModels.SettlementWorkitemInfos)
                {

                    worksheet.Cells[i, 1].Value = exportModel.STT;
                    worksheet.Cells[i, 2].Value = exportModel.WorkitemName;
                    worksheet.Cells[i, 3].Value = (!exportModel.Quantity.HasValue || exportModel.Quantity == 0) ? null : exportModel.Quantity;
                    worksheet.Cells[i, 4].Value = exportModel.CalculationUnit;
                    worksheet.Cells[i, 5].Value = (!exportModel.UnitPrice.HasValue || exportModel.UnitPrice == 0) ? null : exportModel.UnitPrice;

                    if (exportModel.LevelHeading == 1)
                    {
                        worksheet.Cells[i, 1].Style.Font.Bold = true;
                        worksheet.Cells[i, 2].Style.Font.Bold = true;
                    }
                    else
                    {
                        worksheet.Cells[i, 1].Style.Font.Bold = false;
                        worksheet.Cells[i, 2].Style.Font.Bold = false;
                    }
                    if (exportModel.TimesRevenues.Count() > 0)
                    {
                        if (!((!exportModel.Quantity.HasValue || exportModel.Quantity == 0) && (!exportModel.UnitPrice.HasValue || exportModel.UnitPrice == 0)))
                        {
                            for (int j = 1; j <= exportModels.TimeMax; j++)
                            {
                                worksheet.Cells[i, 5 + j].Value = exportModel.TimesRevenues.FirstOrDefault(t => t.Times == j).SettlementNumber;
                            }
                        }
                    }
                    i++;
                }
                for (int k = 1; k <= exportModels.TotalRevenues.Count(); k++)
                {
                    worksheet.Cells[count + 4, 5 + k].Value = exportModels.TotalRevenues.FirstOrDefault(a => a.Times == k).TotalValue;
                    worksheet.Cells[count + 4, 5 + k].Style.Numberformat.Format = "#,###";
                }
                worksheet.Cells[count + 4, 1].Value = "Tổng";
                worksheet.Row(count + 4).Style.Font.Bold = true;
                stream = new MemoryStream(excelPackage.GetAsByteArray());
            }
            return stream;
        }
        public void SetBackgroundEstimateRevenueQuarterToExcel(ExcelWorksheet worksheet, string projectName, int year)
        {
            worksheet.Cells.Style.WrapText = true;

            using (var range = worksheet.Cells["A:Y"])
            {
                range.Style.Font.SetFromFont(new Font("Times New Roman", 13));
            }
            using (var range = worksheet.Cells["A2:Y2"])
            {
                range.Style.Font.SetFromFont(new Font("Times New Roman", 14));
                range.Style.Font.Bold = true;
                range.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                range.Style.VerticalAlignment = ExcelVerticalAlignment.Center;
            }

            worksheet.Cells.Style.WrapText = true;
            worksheet.Cells[3, 1].Value = "STT";
            worksheet.Cells[3, 2].Value = "Hạng mục";
            worksheet.Cells[3, 3].Value = "Số lượng";
            worksheet.Cells[3, 4].Value = "Đơn vị tính";
            worksheet.Cells[3, 5].Value = "Đơn giá (VNĐ)";
            worksheet.Cells[3, 6].Value = "Quý 1";
            worksheet.Cells[3, 7].Value = "Quý 2";
            worksheet.Cells[3, 8].Value = "Quý 3";
            worksheet.Cells[3, 9].Value = "Quý 4";


            worksheet.Row(3).Height = 60;
            worksheet.Row(3).Style.Font.Bold = true;
            worksheet.Row(3).Style.Font.Size = 14;
            worksheet.Cells[2, 1, 2, 9].Merge = true;
            worksheet.Row(3).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            worksheet.Row(3).Style.VerticalAlignment = ExcelVerticalAlignment.Center;
            var row21 = worksheet.Cells[2, 1].RichText.Add("Tổng hợp doanh thu dự kiến năm " + year + "\r\n");
            row21.Size = 16;
            row21.Bold = true;
            var row22 = worksheet.Cells[2, 1].RichText.Add("Công trình: " + projectName + "" + "\r\n");
            row22.Size = 14;
            row22.Bold = true;
            worksheet.Cells[2, 1].AutoFitColumns();

            var type = "###,###";
            worksheet.Column(1).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            worksheet.Column(2).Width = 50;
            worksheet.Column(3).Width = 20;
            worksheet.Column(3).Style.Numberformat.Format = "#,##0.00";
            worksheet.Column(3).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            worksheet.Column(4).Width = 15;
            worksheet.Column(4).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            worksheet.Column(5).Width = 25;
            worksheet.Column(5).Style.Numberformat.Format = type;
            worksheet.Column(5).Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
            worksheet.Column(6).Width = 20;
            worksheet.Column(6).Style.Numberformat.Format = "#,##0.00";
            worksheet.Column(6).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            worksheet.Column(7).Width = 20;
            worksheet.Column(7).Style.Numberformat.Format = "#,##0.00";
            worksheet.Column(7).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            worksheet.Column(8).Width = 20;
            worksheet.Column(8).Style.Numberformat.Format = "#,##0.00";
            worksheet.Column(8).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            worksheet.Column(9).Width = 20;
            worksheet.Column(9).Style.Numberformat.Format = "#,##0.00";
            worksheet.Column(9).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;


            worksheet.View.FreezePanes(4, 1);
            TitleCompanyExcel.FillTitleCommanderWorks(worksheet, projectName, 9, 2, 2);
        }

        public MemoryStream ExportEstimateRevenueQuarterToExcel(int projectId, int year)
        {
            var exportModels = GetRevenueEstimate(projectId, year);
            var stream = new MemoryStream();
            var project = _projectRepository.FindById(projectId);
            using (var excelPackage = new ExcelPackage(stream))
            {
                ExcelWorksheet worksheet = excelPackage.Workbook.Worksheets.Add("Báo cáo ước lượng thanh toán theo quý");
                SetBackgroundEstimateRevenueQuarterToExcel(worksheet, project.Name, year);
                int i = 4;
                foreach (var exportModel in exportModels.WorkitemRevenueInfos)
                {

                    worksheet.Cells[i, 1].Value = exportModel.STT;
                    worksheet.Cells[i, 2].Value = exportModel.WorkitemName;
                    worksheet.Cells[i, 3].Value = (!exportModel.Quantity.HasValue || exportModel.Quantity == 0)
                            ? null : exportModel.Quantity;
                    worksheet.Cells[i, 4].Value = exportModel.CalculationUnit;
                    worksheet.Cells[i, 5].Value = (!exportModel.UnitPrice.HasValue || exportModel.UnitPrice == 0)
                            ? null : exportModel.UnitPrice;
                    if (!((!exportModel.Quantity.HasValue || exportModel.Quantity == 0) || (!exportModel.UnitPrice.HasValue || exportModel.UnitPrice == 0)))
                    {
                        var quarterRevenue = exportModel.QuarterRevenues;
                        foreach (var item in quarterRevenue)
                        {
                            for (int j = 1; j <= 4; j++)
                            {
                                if (item.Quarter == j)
                                    worksheet.Cells[i, 5 + j].Value = item.Quantity;
                            }
                        }
                    }

                    if (exportModel.LevelHeading == 1)
                    {
                        worksheet.Cells[i, 1].Style.Font.Bold = true;
                        worksheet.Cells[i, 2].Style.Font.Bold = true;
                    }
                    else
                    {
                        worksheet.Cells[i, 1].Style.Font.Bold = false;
                        worksheet.Cells[i, 2].Style.Font.Bold = false;
                    }
                    i++;
                }

                worksheet.Cells[exportModels.WorkitemRevenueInfos.Count() + 4, 1].Value = "Tổng";
                worksheet.Cells[exportModels.WorkitemRevenueInfos.Count() + 4, 6].Value = exportModels.TotalEstimateRevenueQuarter1;
                worksheet.Cells[exportModels.WorkitemRevenueInfos.Count() + 4, 7].Value = exportModels.TotalEstimateRevenueQuarter2;
                worksheet.Cells[exportModels.WorkitemRevenueInfos.Count() + 4, 8].Value = exportModels.TotalEstimateRevenueQuarter3;
                worksheet.Cells[exportModels.WorkitemRevenueInfos.Count() + 4, 9].Value = exportModels.TotalEstimateRevenueQuarter4;
                worksheet.Row(exportModels.WorkitemRevenueInfos.Count() + 4).Style.Numberformat.Format = "#,###";
                worksheet.Row(exportModels.WorkitemRevenueInfos.Count() + 4).Style.Font.Bold = true;

                stream = new MemoryStream(excelPackage.GetAsByteArray());

            }
            return stream;
        }

        public StatisticalYieldOneProject StatisticalYieldOneProject(int projectId, CheckStatisticalWithTime checkStatistical)
        {
            var result = new StatisticalYieldOneProject();
            var project = _projectRepository.FindById(projectId);
            if (project == null) throw new ServiceException("Dự án không tồn tại");
            var workitems = _workitemRepository.FindAll(c => c.ProjectId == project.Id);
            if (workitems.Count() > 0)
            {
                var workitemIds = workitems.Select(c => c.Id);
                if ((!checkStatistical.StartDate.HasValue) || (!checkStatistical.EndDate.HasValue))
                    throw new ServiceException("Yêu cầu chọn ngày bắt đầu và ngày kết thúc");

                var sd = checkStatistical.StartDate.Value.Date.ToUniversalTime();
                var ed = checkStatistical.EndDate.Value.Date.AddDays(2).AddTicks(-1).ToUniversalTime();

                var reports = _projectReportRepository.FindAllBetween(sd, ed, c => c.CreatedOnUtc, c => c.ProjectId == project.Id && c.Status == ProjectReportStatus.Approved);

                var reportWorkitems = new List<ProjectReportWorkitem>();
                if (reports.Count() > 0)
                {
                    var reportIds = reports.Select(c => c.Id);
                    reportWorkitems = _projectReportWorkitemRepository.FindAll(c => reportIds.Contains(c.ProjectReportId)).ToList();
                }
                foreach (var workitem in workitems)
                {
                    var res = new StatisticalWorkitemYield
                    {
                        STT = workitem.STT,
                        WorkitemId = workitem.Id,
                        WorkitemName = workitem.Name,
                        LevelHeading = workitem.LevelHeading.Value,
                        CalculationUnit = workitem.CalculationUnit,
                        Quantity = workitem.Quantity,
                        UnitPrice = workitem.UnitPrice
                    };

                    if (res.Quantity.HasValue)
                    {
                        res.Quantity = NumberHelper.RoundDown(res.Quantity.Value, 2);
                    }

                    if (HeadingExcels.DeteminedLevelHeading(res.STT) == 1) res.IsBold = true;
                    if (reportWorkitems.Count() > 0)
                    {
                        var reportWorkitemContains = reportWorkitems.Where(c => c.WorkitemId == workitem.Id);
                        if (reportWorkitemContains.Count() > 0)
                        {
                            if (workitem.Quantity.HasValue && workitem.UnitPrice.HasValue)
                            {
                                res.CompletedNumber = reportWorkitemContains.Sum(c => c.CompletedNumber);

                                res.WorkitemYield = reportWorkitemContains.Sum(c => c.CompletedNumber) * workitem.UnitPrice;

                                result.ProjectYield += res.WorkitemYield.Value;
                            }
                        }
                    }
                    result.StatisticalWorkitemYields.Add(res);
                }
            }

            result.ProjectYield = NumberHelper.RoundDown(result.ProjectYield, 0);

            foreach (var item in result.StatisticalWorkitemYields)
            {
                if (item.UnitPrice.HasValue)
                {
                    item.UnitPrice = NumberHelper.RoundDown(item.UnitPrice.Value, 2);
                }

                if (item.CompletedNumber.HasValue)
                {
                    item.CompletedNumber = NumberHelper.RoundDown(item.CompletedNumber.Value, 2);
                }
                if (item.WorkitemYield.HasValue)
                {
                    item.WorkitemYield = NumberHelper.RoundDown(item.WorkitemYield.Value, 0);
                }
            }
            return result;
        }

        public StatisticalYieldAllModel StatisticalYieldAllProjects(CheckStatisticalWithTime checkStatistical)
        {
            var result = new StatisticalYieldAllModel();
            if ((!checkStatistical.StartDate.HasValue) || (!checkStatistical.EndDate.HasValue)) throw new ServiceException("Yêu cầu nhập ngày bắt đầu và ngày kết thúc");
            var projects = _projectRepository.FindAll(c => c.Status == ProjectStatus.Approved);
            var projectIds = projects.Select(c => c.Id);
            if (projects.Count() > 0)
            {

                var workitems = _workitemRepository.FindAll(c => projectIds.Contains(c.ProjectId));
                if (workitems.Count() > 0)
                {
                    var workitemIds = workitems.Select(c => c.Id);
                    var startD = checkStatistical.StartDate.Value.Date.ToUniversalTime();
                    var endD = checkStatistical.EndDate.Value.Date.AddDays(2).AddTicks(-1).ToUniversalTime();
                    var reports = _projectReportRepository.FindAllBetween(startD, endD, c => c.CreatedOnUtc, c => projectIds.Contains(c.ProjectId)
                        && c.Status == ProjectReportStatus.Approved);
                    var reportWorkitems = new List<ProjectReportWorkitem>();
                    if (reports.Count() > 0)
                    {
                        var reportIds = reports.Select(c => c.Id);
                        reportWorkitems = _projectReportWorkitemRepository.FindAll(c => reportIds.Contains(c.ProjectReportId)
                            && workitemIds.Contains(c.WorkitemId)).ToList();
                    }
                    foreach (var project in projects)
                    {
                        decimal totalYieldInProject = 0;
                        var workitemInProjects = workitems.Where(c => c.ProjectId == project.Id);
                        var workitemInProjectIds = workitemInProjects.Select(c => c.Id);
                        var reportInProjects = reports.Where(c => c.ProjectId == project.Id);
                        var reportInProjectIds = reportInProjects.Select(c => c.Id);

                        decimal accumulated = 0;
                        if (workitemInProjects.Count() > 0)
                        {
                            foreach (var workitemInProject in workitemInProjects)
                            {
                                if (workitemInProject.TotalPaidQuantity.HasValue && workitemInProject.UnitPrice.HasValue)
                                {
                                    var workItemYield = workitemInProject.CompletedNumberTotal.Value * workitemInProject.UnitPrice.Value;
                                    accumulated = accumulated + workItemYield;
                                }
                            }
                        }
                        result.TotalAccumulatedAllProject += accumulated;
                        if (project.TotalContractValue.HasValue)
                            result.TotalContractValueAllProject += project.TotalContractValue.Value;

                        if (reportWorkitems.Count() > 0)
                        {
                            var reportWorkitemInProjects = reportWorkitems.Where(c => reportInProjectIds.Contains(c.ProjectReportId)
                            && workitemInProjectIds.Contains(c.WorkitemId));

                            foreach (var reportWorkitemInProject in reportWorkitemInProjects)
                            {
                                var workitem = workitemInProjects.FirstOrDefault(c => c.Id == reportWorkitemInProject.WorkitemId);
                                if (workitem.UnitPrice.HasValue && workitem.Quantity.HasValue)
                                    totalYieldInProject += reportWorkitemInProject.CompletedNumber
                                        * workitem.UnitPrice.Value;
                            }
                        }
                        result.TotalYieldAllWithTime += totalYieldInProject;

                        var res = new StatisticalAllProjectYieldModel
                        {
                            HashId = _hashIdService.Encode(project.Id),
                            ProjectName = project.Name,
                            TotalContractValue = project.TotalContractValue,
                            Accumulated = accumulated,
                            ProjectYieldWithTime = NumberHelper.RoundDown(totalYieldInProject, 0)
                        };


                        result.StatisticalAllProjectYields.Add(res);
                    }

                    result.TotalYieldAllWithTime = NumberHelper.RoundDown(result.TotalYieldAllWithTime, 0);
                    result.TotalAccumulatedAllProject = NumberHelper.RoundDown(result.TotalAccumulatedAllProject, 0);
                }
            }
            return result;
        }

        public void SetBackgroundExportStatisticalYieldAllProjectToExcel(ExcelWorksheet worksheet, DateTime? startDate, DateTime? endDate)
        {
            worksheet.Cells.Style.WrapText = true;

            using (var range = worksheet.Cells["A:F"])
            {
                range.Style.Font.SetFromFont(new Font("Times New Roman", 13));
            }
            using (var range = worksheet.Cells["A3:F3"])
            {
                range.Style.Font.SetFromFont(new Font("Times New Roman", 14));
                range.Style.Font.Bold = true;
                range.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                range.Style.VerticalAlignment = ExcelVerticalAlignment.Center;
            }

            worksheet.Cells.Style.WrapText = true;
            worksheet.Cells[3, 1].Value = "STT";
            worksheet.Cells[3, 2].Value = "Tên công trình";
            worksheet.Cells[3, 3].Value = "Giá trị hợp đồng(VNĐ)";
            worksheet.Cells[3, 4].Value = "Lũy kế đã thực hiện(VNĐ)";
            worksheet.Cells[3, 5].Value = "Kỳ này(VNĐ)";
            worksheet.Cells[3, 6].Value = "Ghi chú";

            var type = "###,###";
            worksheet.Cells["A2:F2"].Style.Font.Size = 18;
            worksheet.Cells["A2:F2"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            worksheet.Cells["A2:F2"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;


            worksheet.Row(2).Style.Font.Bold = true;


            worksheet.Column(1).Width = 10;
            worksheet.Column(1).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            worksheet.Column(2).Width = 50;
            worksheet.Column(2).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            worksheet.Column(3).Width = 30;
            worksheet.Column(3).Style.Numberformat.Format = type;
            worksheet.Column(3).Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
            worksheet.Column(4).Width = 35;
            worksheet.Column(4).Style.Numberformat.Format = type;
            worksheet.Column(4).Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
            worksheet.Column(5).Width = 25;
            worksheet.Column(5).Style.Numberformat.Format = type;
            worksheet.Column(5).Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
            worksheet.Column(6).Width = 25;
            worksheet.Column(6).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;


            TitleCompanyExcel.FillTitleTechnology(worksheet, 6, 2, 2);
            worksheet.View.FreezePanes(4, 1);
            var sd = startDate.Value.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture);
            var ed = endDate.Value.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture);
            worksheet.Cells[2, 1].Value = "Báo cáo sản lượng các dự án từ ngày " + sd + " đến ngày " + ed;

        }

        public MemoryStream ExportStatisticalYieldAllProjectToExcel(DateTime? startDate, DateTime? endDate)
        {
            var statisticalModels = StatisticalYieldAllProjects(new CheckStatisticalWithTime
            {
                StartDate = startDate,
                EndDate = endDate
            });
            var stream = new MemoryStream();
            using (var excelPackage = new ExcelPackage(stream))
            {
                ExcelWorksheet worksheet = excelPackage.Workbook.Worksheets.Add("Tổng hợp sản lượng các dự án");
                SetBackgroundExportStatisticalYieldAllProjectToExcel(worksheet, startDate.Value, endDate.Value);
                int i = 4;
                foreach (var statistical in statisticalModels.StatisticalAllProjectYields)
                {
                    worksheet.Cells[i, 1].Value = i - 3;
                    worksheet.Cells[i, 2].Value = statistical.ProjectName;
                    worksheet.Cells[i, 3].Value = statistical.TotalContractValue;
                    worksheet.Cells[i, 4].Value = statistical.Accumulated;
                    worksheet.Cells[i, 5].Value = statistical.ProjectYieldWithTime;
                    i++;
                }
                var indexLast = statisticalModels.StatisticalAllProjectYields.Count() + 4;
                worksheet.Cells[indexLast, 2].Value = "Tổng cộng: ";
                worksheet.Cells[indexLast, 3].Value = statisticalModels.TotalContractValueAllProject;
                worksheet.Cells[indexLast, 4].Value = statisticalModels.TotalAccumulatedAllProject;
                worksheet.Cells[indexLast, 5].Value = statisticalModels.TotalYieldAllWithTime;
                worksheet.Row(indexLast).Style.Font.Bold = true;
                worksheet.View.FreezePanes(4, 1);
                stream = new MemoryStream(excelPackage.GetAsByteArray());
            }

            return stream;
        }



        public MemoryStream ExportStatisticalYieldOneProjectToExcel(int projectId, DateTime? startDate, DateTime? endDate)
        {
            var statisticalModel = StatisticalYieldOneProject(projectId, new CheckStatisticalWithTime
            {
                StartDate = startDate,
                EndDate = endDate
            });
            var stream = new MemoryStream();
            using (var excelPackage = new ExcelPackage(stream))
            {
                ExcelWorksheet worksheet = excelPackage.Workbook.Worksheets.Add("Tổng hợp sản lượng của dự án");
                SetBackgroundExportStatisticalYieldOneProjectToExcel(worksheet, projectId, startDate.Value, endDate.Value);
                int i = 4;
                foreach (var statistical in statisticalModel.StatisticalWorkitemYields)
                {

                    worksheet.Cells[i, 1].Value = statistical.STT;
                    worksheet.Cells[i, 2].Value = statistical.WorkitemName;
                    worksheet.Cells[i, 3].Value = (!statistical.Quantity.HasValue || statistical.Quantity == 0)
                            ? null : statistical.Quantity;
                    worksheet.Cells[i, 4].Value = statistical.CalculationUnit;
                    worksheet.Cells[i, 5].Value = (!statistical.UnitPrice.HasValue || statistical.UnitPrice == 0)
                            ? null : statistical.UnitPrice;

                    worksheet.Cells[i, 6].Value = statistical.CompletedNumber;
                    worksheet.Cells[i, 7].Value = statistical.WorkitemYield;

                    if (statistical.LevelHeading == 1)
                    {
                        worksheet.Cells[i, 1].Style.Font.Bold = true;
                        worksheet.Cells[i, 2].Style.Font.Bold = true;
                    }
                    else
                    {
                        worksheet.Cells[i, 1].Style.Font.Bold = false;
                        worksheet.Cells[i, 2].Style.Font.Bold = false;
                    }
                    i++;
                }
                var indexLast = statisticalModel.StatisticalWorkitemYields.Count() + 5;
                //worksheet.Cells[indexLast, 1].Value = "Tổng";
                //worksheet.Cells[indexLast, 1].Style.Font.Bold = true;
                //worksheet.Cells[indexLast, 7].Value = statisticalModel.ProjectYield;
                //worksheet.Cells[indexLast, 7].Style.Font.Bold = true;
                //worksheet.Cells[indexLast, 7].Style.Numberformat.Format = "###,###";

                var staticCompany = StaticsYieldWithCompany(projectId, new CheckStatisticalWithTime
                {
                    StartDate = startDate,
                    EndDate = endDate
                });
                int k = indexLast + 1;
                foreach (var item in staticCompany.StatisticCompanyYieldInProjects)
                {
                    worksheet.Cells[k, 2].Value = item.CompanyName;
                    worksheet.Cells[k, 6].Value = item.CompleteNumberInCompany;
                    worksheet.Cells[k, 7].Value = item.YieldNumberInCompany;
                    worksheet.Cells[k, 2].Style.Font.Bold = true;
                    worksheet.Cells[k, 6].Style.Font.Bold = true;
                    worksheet.Cells[k, 7].Style.Font.Bold = true;
                    k++;
                }
                var last = k + 1;
                worksheet.Cells[last, 1].Value = "Tổng";
                worksheet.Cells[last, 1].Style.Font.Bold = true;
                worksheet.Cells[last, 6].Value = staticCompany.TotalCompleteNumberInCompany;
                worksheet.Cells[last, 6].Style.Font.Bold = true;
                worksheet.Cells[last, 6].Style.Numberformat.Format = "###,###";
                worksheet.Cells[last, 7].Value = statisticalModel.ProjectYield;
                worksheet.Cells[last, 7].Style.Font.Bold = true;
                worksheet.Cells[last, 7].Style.Numberformat.Format = "###,###";

                stream = new MemoryStream(excelPackage.GetAsByteArray());
            }
            return stream;
        }



        public void SetBackgroundExportStatisticalYieldOneProjectToExcel(ExcelWorksheet worksheet, int projectID, DateTime? startDate, DateTime? endDate)
        {
            var project = _projectRepository.FindById(projectID);
            worksheet.Cells.Style.WrapText = true;

            using (var range = worksheet.Cells["A:G"])
            {
                range.Style.Font.SetFromFont(new Font("Times New Roman", 13));
            }
            using (var range = worksheet.Cells["A3:G3"])
            {
                range.Style.Font.SetFromFont(new Font("Times New Roman", 14));
                range.Style.Font.Bold = true;
                range.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                range.Style.VerticalAlignment = ExcelVerticalAlignment.Center;
            }

            worksheet.Cells.Style.WrapText = true;
            worksheet.Cells[3, 1].Value = "TT";
            worksheet.Cells[3, 2].Value = "Hạng mục";
            worksheet.Cells[3, 3].Value = "Số lượng ";
            worksheet.Cells[3, 4].Value = "Đơn vị tính";
            worksheet.Cells[3, 5].Value = "Đơn giá(VNĐ)";
            worksheet.Cells[3, 6].Value = "Số lượng hoàn thành";
            worksheet.Cells[3, 7].Value = "Sản lượng(VNĐ)";


            var type = "###,###";
            worksheet.Cells["A2:G2"].Style.Font.Size = 18;
            worksheet.Cells["A2:G2"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            worksheet.Cells["A2:G2"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
            worksheet.Row(1).Height = 60;
            var sd = startDate.Value.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture);
            var ed = endDate.Value.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture);

            var row11 = worksheet.Cells[2, 1].RichText.Add("Tổng hợp sản lượng( " + sd + " - " + ed + ") " + "\r\n");
            row11.Size = 16;
            row11.Bold = true;
            var row12 = worksheet.Cells[2, 1].RichText.Add("Công trình: " + project.Name + "" + "\r\n");
            row12.Size = 14;
            row12.Bold = true;
            worksheet.Cells[2, 1].AutoFitColumns();


            worksheet.Column(1).Width = 10;
            worksheet.Column(1).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            worksheet.Column(2).Width = 50;
            worksheet.Column(3).Width = 20;
            worksheet.Column(3).Style.Numberformat.Format = "#,##0.00";
            worksheet.Column(3).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            worksheet.Column(4).Width = 15;
            worksheet.Column(4).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            worksheet.Column(5).Width = 20;
            worksheet.Column(5).Style.Numberformat.Format = type;
            worksheet.Column(5).Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
            worksheet.Column(6).Width = 30;
            worksheet.Column(6).Style.Numberformat.Format = "#,##0.00";
            worksheet.Column(6).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            worksheet.Column(7).Width = 25;
            worksheet.Column(7).Style.Numberformat.Format = type;
            worksheet.Column(7).Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
            TitleCompanyExcel.FillTitleCommanderWorks(worksheet, project.Name, 7, 2, 2);
            worksheet.View.FreezePanes(4, 1);
        }


        public void SetBackgroundExportStatisticsActualOfConstructionInTheProjectToExcel(ExcelWorksheet worksheet, int projectId, int? month, int? year)
        {
            var project = _projectRepository.FindById(projectId);
            worksheet.Cells.Style.WrapText = true;

            using (var range = worksheet.Cells["A:G"])
            {
                range.Style.Font.SetFromFont(new Font("Times New Roman", 13));
            }
            using (var range = worksheet.Cells["A3:G3"])
            {
                range.Style.Font.SetFromFont(new Font("Times New Roman", 14));
                range.Style.Font.Bold = true;
                range.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                range.Style.VerticalAlignment = ExcelVerticalAlignment.Center;
            }

            worksheet.Cells.Style.WrapText = true;
            worksheet.Cells[3, 1].Value = "STT";
            worksheet.Cells[3, 2].Value = "Đơn vị";
            worksheet.Cells[3, 3].Value = "KL đơn vị phân giao";
            worksheet.Cells[3, 4].Value = "KL Hoàn thành";
            worksheet.Cells[3, 5].Value = "Hoàn thành(VT)";
            worksheet.Cells[3, 6].Value = "Đang thi công(VT)";

            worksheet.Cells["A2:F2"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            worksheet.Cells["A2:F2"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
            worksheet.Cells["A3:F3"].Style.Font.Size = 14;
            worksheet.Cells["A3:F3"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            worksheet.Cells["A3:F3"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;

            var row21 = worksheet.Cells[2, 1].RichText.Add("Báo cáo tháng( cập nhật đến tháng " + month + " - " + year + " )" + "\r\n");
            row21.Bold = true;
            row21.Size = 20;
            var row22 = worksheet.Cells[2, 1].RichText.Add("\r\tCông trình: " + project.Name + "\r\n");
            worksheet.Cells[2, 1].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
            worksheet.Cells[2, 1].Style.VerticalAlignment = ExcelVerticalAlignment.Top;
            row22.Bold = true;
            row22.Size = 14;
            worksheet.Cells[2, 1].AutoFitColumns();


            worksheet.Row(3).Height = 45;
            worksheet.Column(1).Width = 10;
            worksheet.Column(1).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            worksheet.Column(2).Width = 50;
            worksheet.Column(3).Width = 25;
            worksheet.Column(3).Style.Numberformat.Format = "#,##0.00";
            worksheet.Column(3).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            worksheet.Column(4).Width = 25;
            worksheet.Column(4).Style.Numberformat.Format = "#,##0.00";
            worksheet.Column(4).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            worksheet.Column(5).Width = 25;
            worksheet.Column(5).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            worksheet.Column(6).Width = 25;
            worksheet.Column(6).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;

            TitleCompanyExcel.FillTitleCommanderWorks(worksheet, project.Name, 6, 2, 2);
            worksheet.View.FreezePanes(4, 1);
        }


        public MemoryStream ExportReportMonthlyContructionInProjectToExcel(int projectId, int? month, int? year)
        {
            var statisticalModel = ReportMonthlyContructionInProject(new CheckMonthlyReport
            {
                ProjectId = projectId,
                Month = month,
                Year = year
            });
            var stream = new MemoryStream();
            using (var excelPackage = new ExcelPackage(stream))
            {
                ExcelWorksheet worksheet = excelPackage.Workbook.Worksheets.Add("Báo cáo tháng");
                SetBackgroundExportStatisticsActualOfConstructionInTheProjectToExcel(worksheet, projectId, month, year);
                int i = 4;
                foreach (var statistical in statisticalModel)
                {

                    worksheet.Cells[i, 1].Value = statistical.STT;
                    worksheet.Cells[i, 1].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                    worksheet.Cells[i, 1].Style.Font.Bold = true;
                    worksheet.Cells[i, 2].Value = statistical.WorkitemCategoryName;
                    worksheet.Cells[i, 2].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                    worksheet.Cells[i, 2].Style.Font.Bold = true;
                    var k = i + 1;
                    int x = 1;
                    foreach (var company in statistical.ActualContruction)
                    {
                        worksheet.Cells[k, 1].Value = x;
                        worksheet.Cells[k, 2].Value = company.CompanyName;
                        worksheet.Cells[k, 3].Value = company.AssignmentNumber;
                        worksheet.Cells[k, 4].Value = company.StatisticsCompletedNumber;
                        worksheet.Cells[k, 5].Value = company.CompletePlaceCount;
                        worksheet.Cells[k, 6].Value = company.UnderPlaceCount;
                        k++;
                        x++;
                    }
                    i = k;
                }
                //var indexLast = statisticalModel.StatisticsActuals.Count() + 5;
                //var type = "#,##0.00";
                //worksheet.Cells[indexLast, 2].Value = "Tổng";
                //worksheet.Cells[indexLast, 2].Style.Font.Bold = true;

                //worksheet.Cells[indexLast, 3].Value = statisticalModel.TotalAssignment;
                //worksheet.Cells[indexLast, 3].Style.Font.Bold = true;
                //worksheet.Cells[indexLast, 3].Style.Numberformat.Format = type;
                //worksheet.Cells[indexLast, 3].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;

                //worksheet.Cells[indexLast, 4].Value = statisticalModel.TotalStatisticsCompleted;
                //worksheet.Cells[indexLast, 4].Style.Font.Bold = true;
                //worksheet.Cells[indexLast, 4].Style.Numberformat.Format = type;
                //worksheet.Cells[indexLast, 4].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;

                //worksheet.Cells[indexLast, 5].Value = statisticalModel.TotalCompletePlaceCount;
                //worksheet.Cells[indexLast, 5].Style.Font.Bold = true;
                //worksheet.Cells[indexLast, 5].Style.Numberformat.Format = type;
                //worksheet.Cells[indexLast, 5].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;

                //worksheet.Cells[indexLast, 6].Value = statisticalModel.TotalUnderPlaceCount;
                //worksheet.Cells[indexLast, 6].Style.Font.Bold = true;
                //worksheet.Cells[indexLast, 6].Style.Numberformat.Format = type;
                //worksheet.Cells[indexLast, 6].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;

                stream = new MemoryStream(excelPackage.GetAsByteArray());
            }
            return stream;
        }

        public StatisticYieldWithCompanyModel StaticsYieldWithCompany(int projectId, CheckStatisticalWithTime checkStatistical)
        {
            var result = new StatisticYieldWithCompanyModel();
            var project = _projectRepository.FindById(projectId);
            if (project == null) throw new ServiceException("Dự án không tồn tại");

            var workitems = _workitemRepository.FindAll(c => c.ProjectId == project.Id);
            if (workitems.Count() == 0) throw new ServiceException("Dự án chưa có hạng mục nào");

            if ((!checkStatistical.EndDate.HasValue) || (!checkStatistical.StartDate.HasValue))
                throw new ServiceException("Yêu cầu chọn khoảng thời gian để tổng hợp");

            var startD = checkStatistical.StartDate.Value.Date.ToUniversalTime();
            var endD = checkStatistical.EndDate.Value.Date.AddDays(2).AddTicks(-1).ToUniversalTime();

            var companies = _companyRepository.FindAll();
            var companyIds = companies.Select(c => c.Id);
            var companyProjects = _projectCompanyRepository.FindAll(c => c.ProjectId == projectId);
            if (companyProjects.Count() == 0)
                throw new ServiceException("Chưa có công ty nào tham gia dự án");
            var companyInProjects = companies.Where(c => companyProjects.Select(a => a.CompanyId).Distinct().Contains(c.Id));


            var reports = _projectReportRepository.FindAllBetween(startD, endD, c => c.CreatedOnUtc, c => c.ProjectId == project.Id
                && c.Status == ProjectReportStatus.Approved);

            foreach (var company in companyInProjects)
            {
                var addCompany = new StatisticCompanyYieldInProject
                {
                    CompanyId = company.Id,
                    CompanyName = company.Name
                };
                result.StatisticCompanyYieldInProjects.Add(addCompany);
            }

            if (reports.Count() > 0)
            {
                var reportIds = reports.Select(c => c.Id);
                var reportWorkitems = _projectReportWorkitemRepository.FindAll(c => reportIds.Contains(c.ProjectReportId));

                foreach (var item in result.StatisticCompanyYieldInProjects)
                {
                    var reportWorkitemWithCompany = reportWorkitems.Where(c => c.CompanyId == item.CompanyId);
                    decimal collectYield = 0;
                    foreach (var collect in reportWorkitemWithCompany)
                    {
                        collectYield = collectYield + collect.CompletedNumber * workitems.FirstOrDefault(c => c.Id == collect.WorkitemId).UnitPrice.Value;
                    }
                    item.CompleteNumberInCompany = reportWorkitemWithCompany.Sum(c => c.CompletedNumber);
                    item.YieldNumberInCompany = NumberHelper.RoundDown(collectYield, 0);
                    result.TotalCompleteNumberInCompany = result.TotalCompleteNumberInCompany + item.CompleteNumberInCompany;
                }
            }
            return result;
        }

        public StatisticsActualOfConstructionModel StatisticsActualOfConstructionInTheProject(int projectId, CheckStatisticalWithTime checkStatistical)
        {
            var result = new StatisticsActualOfConstructionModel();
            result.StatisticsActuals = new List<StatisticsActualOfConstructionInfo>();

            var project = _projectRepository.FindById(projectId);
            if (project == null) throw new ServiceException("Dự án không tồn tại");
            if (project.Status != ProjectStatus.Approved) throw new ServiceException("Dự án này chưa được phê duyệt");

            var workitems = _workitemRepository.FindAll(c => c.ProjectId == project.Id);
            if (workitems.Count() == 0) throw new ServiceException("Dự án này không có hạng mục");
            var workitemIds = workitems.Select(c => c.Id);
            var workitemCompanies = _workitemCompanyRepository.FindAll(c => workitemIds.Contains(c.WorkItemId));

            var projectCompanies = _projectCompanyRepository.FindAll(c => c.ProjectId == projectId);
            if (projectCompanies.Count() == 0) throw new ServiceException("Dự án này chưa có công ty nào tham gia");

            var companyCheckIds = projectCompanies.Select(c => c.CompanyId);
            var companyInProjects = _companyRepository.FindAll(c => companyCheckIds.Contains(c.Id));


            var startD = checkStatistical.StartDate.Value.Date.ToUniversalTime();
            var endD = checkStatistical.EndDate.Value.Date.AddDays(2).AddTicks(-1).ToUniversalTime();

            var reports = _projectReportRepository.FindAllBetween(startD, endD, c => c.CreatedOnUtc, c => c.ProjectId == project.Id
                && c.Status == ProjectReportStatus.Approved);


            var reportWorkitems = new List<ProjectReportWorkitem>();
            if (reports.Count() > 0)
            {
                var reportIds = reports.Select(c => c.Id);
                reportWorkitems = _projectReportWorkitemRepository.FindAll(c => reportIds.Contains(c.ProjectReportId) && workitemIds.Contains(c.WorkitemId)).ToList();
            }
            foreach (var company in companyInProjects)
            {
                var statistic = new StatisticsActualOfConstructionInfo
                {
                    CompanyId = company.Id,
                    CompanyName = company.Name,
                };

                var reportLastestForCompanyId = 0;
                void CalculateReportLastestForCompany()
                {
                    foreach (var report in reports.OrderByDescending(c => c.CreatedOnUtc))
                    {
                        var checkReportWorkitems = reportWorkitems.Where(c => c.CompanyId == company.Id && c.ProjectReportId == report.Id);
                        if (checkReportWorkitems.Count() > 0)
                        {
                            reportLastestForCompanyId = report.Id;
                            return;
                        }
                    }
                }
                CalculateReportLastestForCompany();



                var workitemByCompanies = workitemCompanies.Where(c => c.CompanyId == statistic.CompanyId);
                if (workitemByCompanies.Count() > 0)
                {
                    statistic.AssignmentNumber = workitemByCompanies.Sum(c => c.AssignedNumber);
                    result.TotalAssignment += statistic.AssignmentNumber;
                    if (reportWorkitems.Count() > 0)
                    {
                        var reportWorkitemForCompany = reportWorkitems.Where(c => c.CompanyId == company.Id);

                        statistic.StatisticsCompletedNumber = reportWorkitemForCompany.Sum(c => c.CompletedNumber);
                        result.TotalStatisticsCompleted += statistic.StatisticsCompletedNumber;

                        foreach (var reportWorkitem in reportWorkitemForCompany)
                        {
                            statistic.StatisticsCompletePlace = statistic.StatisticsCompletePlace + "," + reportWorkitem.CompletePlace;
                            if (reportWorkitem.ProjectReportId == reportLastestForCompanyId && reportLastestForCompanyId != 0)
                            {
                                statistic.StatisticsUnderPlace = statistic.StatisticsUnderPlace + "," + reportWorkitem.UnderPlace;
                            }

                        }
                        statistic.CompletePlaceCount = StringUtil.CountItemWhenSeperate(statistic.StatisticsCompletePlace);
                        result.TotalStatisticsCompletePlace = result.TotalStatisticsCompletePlace + "," + statistic.StatisticsCompletePlace;
                        result.TotalCompletePlaceCount = StringUtil.CountItemWhenSeperate(result.TotalStatisticsCompletePlace);

                        statistic.UnderPlaceCount = StringUtil.CountItemWhenSeperate(statistic.StatisticsUnderPlace);
                        result.TotalStatisticsUnderPlace = result.TotalStatisticsUnderPlace + "," + statistic.StatisticsUnderPlace;
                        result.TotalUnderPlaceCount = StringUtil.CountItemWhenSeperate(result.TotalStatisticsUnderPlace);
                    }
                }
                result.StatisticsActuals.Add(statistic);

            }
            return result;
        }

        public IEnumerable<StatisticsActualOfContruction> ReportMonthlyContructionInProject(CheckMonthlyReport checkMonthly)
        {
            var result = new List<StatisticsActualOfContruction>();

            var project = _projectRepository.FindById(checkMonthly.ProjectId);
            if (project == null) throw new ServiceException("Dự án không tồn tại");
            if (project.Status != ProjectStatus.Approved) throw new ServiceException("Dự án chưa được phê duyệt");

            var companyAll = _companyRepository.FindAll();

            var projectCompanies = _projectCompanyRepository.FindAll(c => c.ProjectId == checkMonthly.ProjectId);

            if (projectCompanies.Count() == 0) throw new ServiceException("Chưa có công ty nào tham gia dự án");

            var companyInProjects = companyAll.Where(c => projectCompanies.Select(a => a.CompanyId).Contains(c.Id));

            var divideCategory = DivideWorkitem(checkMonthly.ProjectId);

            var reports = _projectReportRepository.FindAll(c => c.ProjectId == checkMonthly.ProjectId && c.Status == ProjectReportStatus.Approved);



            if ((!checkMonthly.Month.HasValue) || (!checkMonthly.Year.HasValue)) throw new ServiceException("Chưa chọn tháng, năm cho báo cáo tổng hợp");

            var reportLessThanInMonths = reports.Where(c => c.CreatedOnUtc.Month <= checkMonthly.Month && c.CreatedOnUtc.Year <= checkMonthly.Year);

            var workitems = _workitemRepository.FindAll(c => c.ProjectId == checkMonthly.ProjectId);


            var reportWorkitems = new List<ProjectReportWorkitem>();
            if (reportLessThanInMonths.Count() > 0)
            {
                var reportIds = reportLessThanInMonths.Select(c => c.Id);

                reportWorkitems = _projectReportWorkitemRepository.FindAll(c => reportIds.Contains(c.ProjectReportId)).ToList();

                if (reportWorkitems.Count() > 0)
                {
                    var workitemInReports = reportWorkitems.Select(c => c.WorkitemId).Distinct();
                }
            }


            var companyInProjectIds = companyInProjects.Select(a => a.Id);

            var workitemIds = workitems.Select(b => b.Id);

            var assignCompanies = _workitemCompanyRepository.FindAll(c => companyInProjectIds.Contains(c.CompanyId) && workitemIds.Contains(c.WorkItemId));

            if (assignCompanies.Count() > 0)
            {
                foreach (var category in divideCategory)
                {
                    var workitemWithCategories = new List<int>();

                    workitemWithCategories.Append(category.WorkitemCategory.Id);
                    if (category.WorkItemSmallerModels.Count() > 0)
                    {
                        workitemWithCategories.AddRange(category.WorkItemSmallerModels.Select(c => c.Id));
                    }

                    var companyies = companyInProjects.Where(c => assignCompanies.Select(a => a.CompanyId).Contains(c.Id));
                    var addCategory = new StatisticsActualOfContruction
                    {
                        STT = category.WorkitemCategory.STT,
                        WorkitemCategoryId = category.WorkitemCategory.Id,
                        WorkitemCategoryName = category.WorkitemCategory.Name,

                    };
                    var assignCategory = assignCompanies.Where(c => workitemWithCategories.Contains(c.WorkItemId));
                    var assignCategoryCompanies = assignCompanies.Where(c => workitemWithCategories.Contains(c.WorkItemId)).Select(c => c.CompanyId);

                    var companyAssign = companyies.Where(c => assignCategoryCompanies.Contains(c.Id));

                    foreach (var company in companyAssign)
                    {
                        var assign = assignCategory.Where(c => c.CompanyId == company.Id);

                        var actual = new StatisticsActualOfContructionDetails
                        {
                            CompanyId = company.Id,
                            CompanyName = company.Name,

                        };
                        // actual.StatisticsCompletedNumber = assign.Sum(c => c.CompletedNumber) ?? 0;
                        actual.AssignmentNumber = assign.Sum(c => c.AssignedNumber);

                        var workitemAssignForCompany = assign.Select(c => c.WorkItemId);

                        if (reportWorkitems.Count() > 0)
                        {
                            var reportWorkitemForCompany = reportWorkitems.Where(c => c.CompanyId == company.Id && workitemWithCategories.Contains(c.WorkitemId));

                            var listUnderPlace = new List<string>();
                            var listCompletePlace = new List<string>();

                            if (reportWorkitemForCompany.Count() > 0)
                                actual.StatisticsCompletedNumber = reportWorkitemForCompany.Sum(c => c.CompletedNumber);

                            foreach (var workitemAssign in workitemAssignForCompany)
                            {
                                void CalculateCollect()
                                {
                                    var a = reportLessThanInMonths.OrderByDescending(c => c.CreatedOnUtc);
                                    foreach (var reportInMonth in reportLessThanInMonths.OrderByDescending(c => c.CreatedOnUtc))
                                    {
                                        var checkReportWorkitem = reportWorkitems.Where(c => c.ProjectReportId == reportInMonth.Id);

                                        var checkCollect = checkReportWorkitem.FirstOrDefault(c => c.WorkitemId == workitemAssign && c.CompanyId == company.Id);
                                        if (checkCollect != null)
                                        {
                                            listCompletePlace.Add(checkCollect.CompletePlace);
                                            listUnderPlace.Add(checkCollect.UnderPlace);

                                            return;
                                        }
                                    }
                                }
                                CalculateCollect();
                            }

                            actual.StatisticsCompletePlace = JoinStringPlaceCollection(listCompletePlace.ToArray());
                            actual.CompletePlaceCount = StringUtil.CountItemWhenSeperate(JoinStringPlaceCollection(listCompletePlace.ToArray()));

                            actual.StatisticsUnderPlace = JoinStringPlaceCollection(listUnderPlace.ToArray());
                            actual.UnderPlaceCount = StringUtil.CountItemWhenSeperate(JoinStringPlaceCollection(listUnderPlace.ToArray()));
                        }

                        addCategory.ActualContruction.Add(actual);
                    }
                    result.Add(addCategory);

                }
            }


            return result;
        }


    }
}
