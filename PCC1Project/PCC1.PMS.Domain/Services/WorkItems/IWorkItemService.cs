﻿using Microsoft.AspNetCore.Http;
using PCC1.PMS.Domain.Models.EstimationPlans;
using PCC1.PMS.Domain.Models.Notification;
using PCC1.PMS.Domain.Models.Projects;
using PCC1.PMS.Domain.Models.Revenues;
using PCC1.PMS.Domain.Models.Statistical;
using PCC1.PMS.Domain.Models.Statistical.StatisticalActuals;
using PCC1.PMS.Domain.Models.WorkitemByCompany;
using PCC1.PMS.Domain.Models.Workitems;
using PCC1.PMS.Domain.Models.Workitems.AccumulateCompleteQuarter;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading.Tasks;

namespace PCC1.PMS.Domain.Services.WorkItems
{
    public interface IWorkItemService
    {
        /// <summary>
        /// Danh sách tất cả các hạng mục
        /// </summary>
        /// <returns></returns>
        Task<List<WorkItemModel>> ListWorkitem();

        /// <summary>
        /// Danh sách tất cả hạng mục con theo hạng mục cha
        /// </summary>
        /// <param name="workitemGroupId"></param>
        /// <returns></returns>
        IEnumerable<WorkItemModel> GetListChildrenWorkitems(int workitemGroupId);

        /// <summary>
        /// Danh sách tất cả hạng mục cha theo dự án
        /// </summary>
        /// <param name="projectId"></param>
        /// <returns></returns>
        IEnumerable<WorkItemModel> GetListParentWorkitems(int projectId);

        /// <summary>
        /// Thêm mới hạng mục
        /// </summary>
        /// <param name="workItemAddModel"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        Task<(bool, WorkItemAddNotificationModel)> AddWorkItem(AddWorkitemModel workItemAddModel, int userId);

        bool UpdateWorkItem(WorkItemModel workItemModel, int userId);

        List<WorkitemAssignmentWithHeading> GetWorkitemAssignments(int projectId, int? checkCompanyId);

        (MemoryStream, string) DowloadExcelAssignments(int projectId, int? checkCompanyId);

        bool DeleteWorkItem(DeleteWorkitemModel workItemDeleteModel, int userId);

        bool ImportExcelWorkItems(IFormFile formFile, int projectId, int userId);     
       
        bool SettlementWorkitem(IEnumerable<PerformingSettlementModel> settlementModels);

        SettlementViewModel ViewPayAllWorkItem(int projectId);

        Task<SettlementWorkitemModel> ViewPayOneWorkItem(int workitemId);

        Task<WorkItemModel> WorkItemById(int id);

        bool UpdateAssignments(List<AssignmentUpdateModel> deliveryWorkItems);

        ListWorkitemByCompanyModel ListWorkitemByCompanyId(int companyId, int projectId);

        List<WorkitemInfoModel> GetProjectWorkitems(int projectId);

        bool EstimateMonthlyOutput(List<EstimationModel> model);

        bool EstimationCompleteForQuarter(List<EstimationModel> model);

        YieldInMonthModel CheckCompletedNumberPerMonth(CheckCompletedPerMonthModel model);

        YieldInQuarterModel EstimateWorkitemCompleteQuarter(CheckCompletedPerQuarterModel checkWorkitem);


        IEnumerable<WorkitemSettlementPerQuarterModel> ShowSettlementPlanWithQuarterly(int projectId,int year, int quarter);

        MemoryStream ExportCompletedNumberMonthToExcel(CheckCompletedPerMonthModel checkWorkitem);

        MemoryStream ExportCompletedNumberQuarterToExcel(CheckCompletedPerQuarterModel checkWorkitem);

        Stream ExportSettlementPlanWithQuarterlyToExcel(int projectId, int year, int quarter);

        List<WorkitemWithHeadingModel> DivideWorkitem(int projectId);

        bool UpdateWorkItemRevenueEstimate(UpdateWorkItemRevenueModel model);

        RevenueEstimateResultModel GetRevenueEstimate(int projectId, int year);

        bool SettlementWorkitemWithTimes(UpdateWorkitemSettlementModel model);

        SettlementRevenueResultModel ListSettlementWorkItemByTime(int projectId);

        List<FillEstimateToSettlementResult> FillEstimateToSettlement(FillEstimateToSettlementModel fillEstimate);

        MemoryStream ExportEstimateRevenueQuarterToExcel(int projectId, int year);

        MemoryStream ExportSettlementWithTimeToExcel(int projectId);

        StatisticalYieldOneProject StatisticalYieldOneProject(int projectId, CheckStatisticalWithTime checkStatistical);

        StatisticalYieldAllModel StatisticalYieldAllProjects(CheckStatisticalWithTime checkStatistical);

        MemoryStream ExportStatisticalYieldAllProjectToExcel(DateTime? startDate, DateTime? endDate);

        MemoryStream ExportStatisticalYieldOneProjectToExcel(int projectId, DateTime? startDate, DateTime? endDate);

        StatisticsActualOfConstructionModel StatisticsActualOfConstructionInTheProject(int projectId, CheckStatisticalWithTime checkStatistical);

        MemoryStream ExportReportMonthlyContructionInProjectToExcel(int projectId, int? startDate, int? endDate);

        StatisticYieldWithCompanyModel StaticsYieldWithCompany(int projectId, CheckStatisticalWithTime checkStatistical);

        IEnumerable<StatisticsActualOfContruction> ReportMonthlyContructionInProject(CheckMonthlyReport checkMonthly);
    }
}
