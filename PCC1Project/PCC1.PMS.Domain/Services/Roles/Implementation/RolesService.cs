
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Linq.Expressions;
using PCC1.PMS.Domain.Entity;
using MicroOrm.Dapper.Repositories;
using PCC1.PMS.Framework.Utils;
using PCC1.PMS.Domain.Repositories;
using PCC1.PMS.Domain.Models.Users;

namespace PCC1.PMS.Domain.Services.Roles.Implementation
{
    public class RolesService : IRolesService
    {
        private readonly IRoleRepository _roleRepository;

        public RolesService()
        {

        }
        public RolesService(IRoleRepository roleRepository)
        {
            _roleRepository = roleRepository;
        }

        public RoleFilterResult Filter(RoleFilterModel roleFilterModel)
        {
            var result = new RoleFilterResult();
            var roles = _roleRepository.FindAll().OrderBy(r=>r.DisplayOrder).ToList();
            result.Records = DomainMaps.Mapper.Map<List<RoleModel>>(roles);
            result.TotalRecord = roles.Count();
            return result;
        }

        public RoleModel GetById(int id)
        {
            var role = _roleRepository.FindById(id);
            return DomainMaps.Mapper.Map<RoleModel>(role);
        }

        public List<RoleModel> List(int[] ids)
        {
            var res = _roleRepository.FindIn(ids);
            return res.ToList().CloneToListModels<Role, RoleModel>();
        }

        public List<PermissionGroup> ListPermissions()
        {
            var groups = new List<PermissionGroup>();
            var groupCodes = Enum.GetValues(typeof(PermissionGroupCode)).Cast<PermissionGroupCode>().ToList();
            foreach (var groupCode in groupCodes)
            {
                groups.Add(new PermissionGroup()
                {
                    GroupId = (int)groupCode,
                    Name = groupCode.ToString(),
                });
            }

            var permissions = new List<Permission>();
            foreach(var p in Enum.GetValues(typeof(PermissionCode)).Cast<PermissionCode>())
            {
                var attr = p.GetAttributeOfType<PermissionDetailsAttribute>();
                permissions.Add(new Permission
                {
                    Code = p,
                    Name = attr.Name,
                    GroupId = (int)attr.Group
                });
            }

            //auto build permission properties for group
            foreach (var group in groups)
            {
                var groupPermissions = permissions.Where(c => c.GroupId == group.GroupId);
                group.Permissions = new List<Permission>();
                group.Permissions.AddRange(groupPermissions);
            }

            return groups;
        }

        public async Task<bool> UpdateAsync(RoleModel role)
        {
            //update to storage
            var entity = role.CloneToModel<RoleModel, Role>();
            return await _roleRepository.UpdateAsync(entity);
        }
    }
}

