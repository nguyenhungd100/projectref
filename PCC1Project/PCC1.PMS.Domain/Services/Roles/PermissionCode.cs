﻿namespace PCC1.PMS.Domain.Services.Roles
{
    public enum PermissionGroupCode
    {
        SYSTEM = 1,
        PROJECT = 11,
        REPORT = 12,
        DISCUSSION = 13,
        MATTERIAL = 14,
        ESTIMATE = 15,
        SAFELYREPORT = 16,
        SETTLEMENT = 17,
        COMPANY_MANAGEMENT = 18,
        TRACKING_MONEY_ADVANCE = 19
    }
    public enum PermissionCode
    {
        // System
        [PermissionDetails(PermissionGroupCode.SYSTEM, "Quản lý người dùng")]
        MANAGE_USERS = 1,
        [PermissionDetails(PermissionGroupCode.SYSTEM, "Quản lý vai trò")]
        MANAGE_ROLES = 5,

        [PermissionDetails(PermissionGroupCode.PROJECT, "Thay đổi trạng thái dự án")]
        CHANGE_PROJECT_STATUS = 10,

        // Project
        [PermissionDetails(PermissionGroupCode.PROJECT, "Khởi tạo dự án")]
        INIT_PROJECT = 100,
        [PermissionDetails(PermissionGroupCode.PROJECT, "Cài đặt nhân sự dự án")]
        SETUP_PROJECT_USERS = 101,
        [PermissionDetails(PermissionGroupCode.PROJECT, "Cập nhật thông tin dự án")]
        UPDATE_PROJECT_INFO = 102,
        [PermissionDetails(PermissionGroupCode.PROJECT, "Cập nhật khổi lượng dự án")]
        UPDATE_WORKITEM = 103,
        [PermissionDetails(PermissionGroupCode.PROJECT, "Cập nhật kế hoạch dự án")]
        UPDATE_PROJECT_PLAN = 104,
        [PermissionDetails(PermissionGroupCode.PROJECT, "Cập nhật bảng thanh toán")]
        UPDATE_PROJECT_SETTLEMENT = 105,
        [PermissionDetails(PermissionGroupCode.PROJECT, "Cập nhật hình ảnh dự án")]
        UPDATE_PROJECT_IMAGE = 106,
        [PermissionDetails(PermissionGroupCode.PROJECT, "Cập nhật công ty trong dựa án")]
        UPDATE_COMPANIES_IN_PROJECT = 107,
        [PermissionDetails(PermissionGroupCode.PROJECT, "Phân giao hạng mục")]
        ASSiGNMENT_WORKITEM = 108,
        [PermissionDetails(PermissionGroupCode.PROJECT, "Duyệt dự án")]
        APPROVE_PROJECT = 110,
        [PermissionDetails(PermissionGroupCode.PROJECT, "Xóa dự án")]
        DELETE_PROJECT = 120,
        [PermissionDetails(PermissionGroupCode.PROJECT, "Xem tất cả các dự án")]
        ACCESS_ALL_PROJECTS = 125,
        [PermissionDetails(PermissionGroupCode.PROJECT, "Hoàn thành dự án")]
        FINISHING_PROJECT = 130,
        [PermissionDetails(PermissionGroupCode.PROJECT, "Xóa thư mục trong dự án")]
        DELETE_FOLDER = 135,
        [PermissionDetails(PermissionGroupCode.PROJECT, "Xóa tài liệu trong dự án")]
        DELETE_DOCUMENT = 145,
        [PermissionDetails(PermissionGroupCode.PROJECT, "Upload công văn")]
        UPLOAD_ARCHIVES = 140,

        // Report
        [PermissionDetails(PermissionGroupCode.REPORT, "Lập báo cáo ngày")]
        CREATE_REPORT = 205,
        [PermissionDetails(PermissionGroupCode.REPORT, "Trình báo cáo ngày")]
        SUBMIT_REPORTS = 215,
        [PermissionDetails(PermissionGroupCode.REPORT, "Duyệt báo cáo ngày")]
        APPROVE_REPORTS = 210,
        [PermissionDetails(PermissionGroupCode.REPORT, "Xóa báo cáo ngày")]
        DELETE_REPORTS = 220,
        [PermissionDetails(PermissionGroupCode.REPORT, "Xem tất cả báo cáo ngày")]
        VIEW_ALL_REPORTS = 225,
        // Discussion
        [PermissionDetails(PermissionGroupCode.DISCUSSION, "Thêm chủ đề thảo luận")]
        ADD_TOPIC = 301,
        [PermissionDetails(PermissionGroupCode.DISCUSSION, "Xóa chủ đề thảo luận")]
        DELETE_TOPIC = 302,




        // Matterials
        [PermissionDetails(PermissionGroupCode.MATTERIAL, "Đăng ký vật tư")]
        REGISTER_MATTERIALS = 401,
        [PermissionDetails(PermissionGroupCode.MATTERIAL, "Duyệt vật tư")]
        APPROVE_MATTERIALS = 405,
        [PermissionDetails(PermissionGroupCode.MATTERIAL, "Xem tất cả phiếu đăng ký vật tư")]
        VIEW_ALL_REGISTER_MATERIALS = 410,


        //Estimate
        [PermissionDetails(PermissionGroupCode.ESTIMATE, "Cập nhật kế hoạch hoàn thành hạng mục theo tháng")]
        ESTIMATE_WORKITEM_COMPLETED_IN_MONTH = 501,

        [PermissionDetails(PermissionGroupCode.ESTIMATE, "Cập nhật kế hoạch hoàn thành hạng mục theo quý")]
        ESTIMATE_WORKITEM_COMPLETED_IN_QUARTER = 505,

        [PermissionDetails(PermissionGroupCode.SAFELYREPORT, "Cập nhật báo cáo an toàn vệ sinh lao động")]
        UPDATE_SAFELYREPORT = 601,

        [PermissionDetails(PermissionGroupCode.SAFELYREPORT, "Xem toàn bộ báo cáo an toàn vệ sinh")]
        VIEW_ALL_SAFELYREPORT = 605,


        [PermissionDetails(PermissionGroupCode.SAFELYREPORT, "Phê duyệt báo cáo an toàn vệ sinh")]
        APPROVE_SAFELYREPORT = 610,


        [PermissionDetails(PermissionGroupCode.ESTIMATE, "Cập nhật doanh thu dự kiến theo quý")]
        UPDATE_REVENUE_ESTIMATE = 701,

        [PermissionDetails(PermissionGroupCode.SETTLEMENT, "Cập nhật thanh toán hạng mục theo lần")]
        UPDATE_SETTLEMENT_WITH_TIME = 705,

        [PermissionDetails(PermissionGroupCode.COMPANY_MANAGEMENT, "Cập nhật công ty")]
        UPDATE_COMPANY = 801,

        [PermissionDetails(PermissionGroupCode.TRACKING_MONEY_ADVANCE, "Cập nhật theo dõi tiền tạm ứng")]
        UPDATE_TRACKING_MONEY_ADVANCE = 901,
    }
}