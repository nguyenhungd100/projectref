﻿using MicroOrm.Dapper.Repositories;
using Newtonsoft.Json;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using PCC1.PMS.Domain.Entity;
using PCC1.PMS.Domain.Enums;
using PCC1.PMS.Domain.Models.ProjectReports;
using PCC1.PMS.Domain.Models.SafelyReports;
using PCC1.PMS.Domain.Models.SafelyReports.SubmitOnMobile;
using PCC1.PMS.Domain.Models.Statistical;
using PCC1.PMS.Domain.Models.Statistical.StatisticalSafelyAllProjects;
using PCC1.PMS.Domain.Repositories;
using PCC1.PMS.Domain.Services.Common;
using PCC1.PMS.Domain.Services.Roles;
using PCC1.PMS.Domain.Shared;
using PCC1.PMS.Framework.Utils;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using File = PCC1.PMS.Domain.Entity.File;

namespace PCC1.PMS.Domain.Services.SafelyReports.Implementation
{
    public class SafelyReportService : ISafelyReportService
    {
        private readonly ISafelyReportRepository _safelyReportRepository;
        private readonly IDapperRepository<Project> _projectRepository;
        private readonly IDapperRepository<Company> _companyRepository;
        private readonly IDapperRepository<SafelyReportImage> _safelyReportImageRepository;
        private readonly IDapperRepository<SafelyErrorCollection> _safelyErrorCollectionRepository;
        private readonly IDapperRepository<User> _userRepository;
        private readonly IDapperRepository<ProjectCompany> _projectCompanyRepository;
        private readonly IDapperRepository<BinaryFileStorage> _binaryFileStorageRepository;
        private readonly IDapperRepository<Entity.File> _fileRepository;
        private readonly IDapperRepository<SafelyDraft> _safelyDraftRepository;
        private readonly IDapperRepository<SafelyErrorCollectionDraft> _safelyErrorCollectionDraftRepository;
        private readonly IDapperRepository<ProjectUser> _projectUserRepository;
        private readonly IHashidService _hashidService;
        private readonly IDapperRepository<SafelyImageDetail> _safelyImageDetailRepository;
        private readonly IDapperRepository<SafelyImageDraftDetail> _safelyImageDraftDetailRepository;

        public SafelyReportService(ISafelyReportRepository safelyReportRepository,
            IDapperRepository<Project> projectRepository,
            IDapperRepository<Company> companyRepository,
            IDapperRepository<SafelyReportImage> safelyReportImageRepository,
            IDapperRepository<SafelyErrorCollection> safelyErrorCollectionRepository,
            IDapperRepository<User> userRepository,
            IDapperRepository<ProjectCompany> projectCompanyRepository,
            IDapperRepository<BinaryFileStorage> binaryFileStorageRepository,
            IDapperRepository<Entity.File> fileRepository,
            IDapperRepository<SafelyDraft> safelyDraftRepository,
            IDapperRepository<SafelyErrorCollectionDraft> safelyErrorCollectionDraftRepository,
            IDapperRepository<ProjectUser> projectUserRepository,
            IHashidService hashidService,
            IDapperRepository<SafelyImageDetail> safelyImageDetailRepository,
            IDapperRepository<SafelyImageDraftDetail> safelyImageDraftDetailRepository)
        {

            _safelyReportRepository = safelyReportRepository;
            _projectRepository = projectRepository;
            _companyRepository = companyRepository;
            _safelyReportImageRepository = safelyReportImageRepository;
            _safelyErrorCollectionRepository = safelyErrorCollectionRepository;
            _userRepository = userRepository;
            _projectCompanyRepository = projectCompanyRepository;
            _binaryFileStorageRepository = binaryFileStorageRepository;
            _fileRepository = fileRepository;
            _safelyDraftRepository = safelyDraftRepository;
            _safelyErrorCollectionDraftRepository = safelyErrorCollectionDraftRepository;
            _projectUserRepository = projectUserRepository;
            _hashidService = hashidService;
            _safelyImageDetailRepository = safelyImageDetailRepository;
            _safelyImageDraftDetailRepository = safelyImageDraftDetailRepository;
        }

        public ViolationDefinedModel ListViolationDefined()
        {
            var violationDefined = new ViolationDefinedModel();

            foreach (var code in Enum.GetValues(typeof(ViolationType)).Cast<ViolationType>())
            {
                var attr = code.GetAttributeOfType<ViolationDetailAttribute>();
                if (attr.Group == ViolationGroupCode.ViolationHuman)
                {
                    violationDefined.ViolationHumans.Add(new ViolationHuman
                    {
                        Code = code,
                        Title = attr.Name,
                        ViolationNumber = 0
                    });
                }

                if (attr.Group == ViolationGroupCode.ViolationEnvironment)
                {
                    violationDefined.ViolationEnvironments.Add(new ViolationEnvironment
                    {
                        Code = code,
                        Title = attr.Name,
                        ViolationNumber = 0
                    });
                }
            }
            return violationDefined;
        }


        public bool AddSafelyReportImage(IDbTransaction transaction, UpdateSafelyReportModel updateSafelies, SafelyReportImageModel updateImage, int safelyReportId, string companyName)
        {
            var success = false;
            var updateViolation = false;

            if (updateSafelies.ImageInfo == null) throw new ServiceException("Báo cáo chưa chọn lỗi");


            int safelyImageId = 0;
            var safelyReportImage = new SafelyReportImage();
            if (updateImage.ImageInfoId.HasValue)
            {
                if (updateImage.ImageInfoId.Value != 0)
                {
                    safelyReportImage = _safelyReportImageRepository.FindById(updateImage.ImageInfoId.Value, transaction);
                    success = true;
                }
            }
            else
            {
                safelyReportImage = new SafelyReportImage
                {
                    ProjectId = updateSafelies.ProjectId,
                    CompanyId = updateImage.CompanyId,
                    //FileId = file.Id,
                    SafelyReportId = updateSafelies.Id.Value,
                    Note = updateImage.Note,
                    DayPhotographed = DateTime.UtcNow
                };
                if (updateImage.Coordinates != null)
                {
                    safelyReportImage.Lat = updateImage.Coordinates.Lat;
                    safelyReportImage.Lng = updateImage.Coordinates.Lng;
                }
                success = _safelyReportImageRepository.Insert(safelyReportImage, transaction);
            }

            var listAddImages = new List<Guid>();
            foreach (var image in updateImage.Images)
            {
                var base64 = image;
                if (base64.Length > 100)
                {
                    base64 = base64.Replace("data:image/jpeg;base64,", string.Empty);
                    base64 = base64.Replace("data:image/png;base64,", string.Empty);

                    byte[] decodedByteArray = Convert.FromBase64String(base64);
                    var binaryFileStorage = new BinaryFileStorage()
                    {
                        Data = decodedByteArray
                    };
                    var insertBinaryFile = _binaryFileStorageRepository.Insert(binaryFileStorage, transaction);

                    var company = _companyRepository.FindById(updateImage.CompanyId, transaction);
                    var file = new Entity.File()
                    {
                        Id = Guid.NewGuid(),
                        ProviderType = ProviderFileType.LocalStorage,
                        StorageId = binaryFileStorage.Id,
                        Mime = "image/jpeg",
                        Title = "Ảnh báo cáo an toàn vệ sinh cho công ty " + company.Name,
                        CreatedUtcTime = DateTime.UtcNow,
                        Size = base64.Length / 1024,
                        Status = FileStatus.NewPicture
                    };
                    var insertFile = _fileRepository.Insert(file, transaction);

                    if (insertFile)
                        listAddImages.Add(file.Id);
                }
            }

            safelyImageId = safelyReportImage.Id;

            if (listAddImages.Count() > 0)
            {
                var existImageDetail = _safelyImageDetailRepository.Find(c => c.SafelyImageId == safelyImageId, transaction);
                if (existImageDetail == null)
                {
                    var insertSafelyImageDetail = _safelyImageDetailRepository.Insert(new SafelyImageDetail
                    {
                        SafelyImageId = safelyReportImage.Id,
                        SafelyReportId = updateSafelies.Id.Value,
                        Files = JsonConvert.SerializeObject(listAddImages)
                    }, transaction);
                }
                else
                {
                    var sezializeFiles = JsonConvert.DeserializeObject<List<Guid>>(existImageDetail.Files);
                    sezializeFiles.AddRange(listAddImages);
                    existImageDetail.Files = JsonConvert.SerializeObject(sezializeFiles);
                    var update = _safelyImageDetailRepository.Update(existImageDetail, transaction);
                }
            }

            updateViolation = UpdateViolition(transaction, safelyImageId, updateImage.ViolationDefined, updateImage, companyName);

            return success && updateViolation;
        }


        public bool UpdateViolition(IDbTransaction transaction, int safelyImageId, ViolationDefinedModel violation, SafelyReportImageModel updateImage, string companyName)
        {
            var res = false;
            var violationExists = _safelyErrorCollectionRepository.FindAll(c => c.SafelyImageId == safelyImageId, transaction);
            var violationHumans = updateImage.ViolationDefined.ViolationHumans;
            var violationEnvironments = updateImage.ViolationDefined.ViolationEnvironments;

            if (violation.ViolationHumans.Count() == 0 && violation.ViolationEnvironments.Count() == 0)
                throw new ServiceException("Chưa chọn lỗi cho công ty " + companyName);

            if (violationHumans.Count() > 0)
            {

                foreach (var violationHuman in violationHumans)
                {
                    if (!violationExists.Select(v => v.Type).Contains(violationHuman.Code))
                    {
                        res = _safelyErrorCollectionRepository.Insert(new SafelyErrorCollection
                        {
                            ViolationQuantity = violationHuman.ViolationNumber,
                            SafelyImageId = safelyImageId,
                            Type = violationHuman.Code
                        }, transaction);
                        if (!res) break;
                    }

                    else
                    {
                        foreach (var violationExist in violationExists)
                        {
                            if (violationExist.Type.GetAttributeOfType<ViolationDetailAttribute>()
                             .Group == ViolationGroupCode.ViolationHuman && violationExist.Type == violationHuman.Code)
                                res = _safelyErrorCollectionRepository.Update(new SafelyErrorCollection
                                {
                                    Id = violationExist.Id,
                                    ViolationQuantity = violationHuman.ViolationNumber,
                                    SafelyImageId = safelyImageId,
                                    Type = violationExist.Type
                                }, transaction);
                            if (!res) break;
                        }

                    }
                }
            }
            if (violationEnvironments.Count() > 0)
            {
                foreach (var violationEnvironment in violationEnvironments)
                {
                    if (!violationExists.Select(v => v.Type).Contains(violationEnvironment.Code))
                    {
                        res = _safelyErrorCollectionRepository.Insert(new SafelyErrorCollection
                        {
                            ViolationQuantity = violationEnvironment.ViolationNumber,
                            SafelyImageId = safelyImageId,
                            Type = violationEnvironment.Code
                        }, transaction);
                        if (!res) break;
                    }

                    else
                    {
                        foreach (var violationExist in violationExists)
                        {
                            if (violationExist.Type.GetAttributeOfType<ViolationDetailAttribute>()
                             .Group == ViolationGroupCode.ViolationEnvironment && violationExist.Type == violationEnvironment.Code)
                                res = _safelyErrorCollectionRepository.Update(new SafelyErrorCollection
                                {
                                    Id = violationExist.Id,
                                    SafelyImageId = safelyImageId,
                                    Type = violationExist.Type,
                                    ViolationQuantity = violationEnvironment.ViolationNumber,
                                }, transaction);
                            if (!res) break;
                        }
                    }

                }
            }


            return res;
        }

        public (bool, int) UpdateSafelyReport(UpdateSafelyReportModel updateSafely)
        {
            var reportId = 0;
            var success = false;
            var addImage = false;
            var trans = _safelyReportRepository.BeginTransaction();
            if (trans.Connection.State != ConnectionState.Open) trans.Connection.Open();
            try
            {
                var project = _projectRepository.FindById(updateSafely.ProjectId, trans);
                if (project == null) throw new ServiceException("Dự án không tồn tại");
                if (project.Status != ProjectStatus.Approved) throw new ServiceException("Dự án chưa được duyệt");

                var entity = new SafelyReport();

                var images = new List<SafelyReportImage>();

                //if (updateSafely.Id.HasValue)
                //{
                //    images = _safelyReportImageRepository.FindAll(c => c.SafelyReportId == updateSafely.Id, trans).ToList();
                //    foreach (var image in images)
                //    {
                //        if (!updateSafely.ImageInfo.Select(c => c.Image).ToList().Contains(image.FileId.ToString()))
                //        {
                //            _safelyReportImageRepository.Delete(image, trans);
                //            var file = _fileRepository.FindById(image.FileId, trans);
                //            _fileRepository.Delete(file, trans);
                //            _binaryFileStorageRepository.Delete(new BinaryFileStorage { Id = file.StorageId }, trans);
                //        }
                //    }
                //}



                foreach (var updateImage in updateSafely.ImageInfo)
                {
                    var company = _companyRepository.FindById(updateImage.CompanyId, trans);
                    if (company == null) throw new ServiceException("Công ty không tồn tại");
                    var projectCompany = _projectCompanyRepository.Find(c => c.ProjectId == project.Id && c.CompanyId == company.Id, trans);
                    if (projectCompany == null) throw new ServiceException("Công ty " + company.Name + " chưa tham gia vào dự án này");

                    if (updateImage.ViolationDefined.ViolationHumans.Count() == 0 && updateImage.ViolationDefined.ViolationEnvironments.Count() == 0)
                        throw new ServiceException("Chưa chọn lỗi báo cáo cho công ty " + company.Name);

                    //if (string.IsNullOrEmpty(updateImage.Image)) throw new ServiceException("Chưa chọn ảnh báo cáo cho công ty " + company.Name);

                    if (updateSafely.Id.HasValue)
                    {

                        var existSafelyReport = _safelyReportRepository.FindById(updateSafely.Id.Value, trans);
                        if (existSafelyReport == null) throw new ServiceException("Báo cáo an toàn không tồn tại");
                        if (existSafelyReport.Status == SafelyReportStatus.Approve) throw new ServiceException("Không được chỉnh sửa báo cáo đã duyệt");
                        if (existSafelyReport != null)
                        {
                            if (existSafelyReport.CreatedBy != updateSafely.CreatedBy) throw new ServiceException("Bạn không phải người tạo báo cáo này nên không có quyền sửa");
                        }
                        existSafelyReport.ProjectId = updateSafely.ProjectId;
                        existSafelyReport.DisadvantagesFixingRequirement = updateSafely.DisadvantagesFixingRequirement;
                        existSafelyReport.Requests = updateSafely.Requests;
                        existSafelyReport.Status = SafelyReportStatus.Draft;
                        success = _safelyReportRepository.Update(existSafelyReport, trans);
                        entity.Id = existSafelyReport.Id;
                        reportId = entity.Id;
                    }
                    else
                    {
                        updateSafely.CreatedDate = DateTime.UtcNow;
                        updateSafely.CreatedBy = updateSafely.CreatedBy;
                        entity = new SafelyReport
                        {
                            ProjectId = updateSafely.ProjectId,
                            DisadvantagesFixingRequirement = updateSafely.DisadvantagesFixingRequirement,
                            Requests = updateSafely.Requests,
                            CreatedDate = updateSafely.CreatedDate,
                            CreatedBy = updateSafely.CreatedBy,

                            Status = SafelyReportStatus.Draft
                        };

                        success = _safelyReportRepository.Insert(entity, trans);
                        updateSafely.Id = entity.Id;
                        reportId = entity.Id;
                    }




                    addImage = AddSafelyReportImage(trans, updateSafely, updateImage, entity.Id, company.Name);
                    if (!success || !addImage)
                    {
                        trans.Rollback();
                        break;
                    }
                }

            }
            catch (Exception ex)
            {
                trans.Rollback();
                throw ex;
            }
            trans.Commit();
            return (success, reportId);
        }

        public bool DeleteSafelyReport(DeleteSafelyReportModel safelyReportDeleteModel)
        {
            var existSafelyReport = _safelyReportRepository.FindById(safelyReportDeleteModel.Id);
            if (existSafelyReport != null)
            {
                if (existSafelyReport.Status == SafelyReportStatus.Approve) throw new ServiceException("Không được xóa báo cáo đã duyệt");
                var entity = safelyReportDeleteModel.CloneToModel<DeleteSafelyReportModel, SafelyReport>();
                return _safelyReportRepository.Delete(entity);
            }
            else throw new ServiceException("Báo cáo an toàn vệ sinh này không tồn tại");
        }

        public SearchSafelyReportResult SearchSafelyReport(SearchSafelyReportModel search)
        {
            var result = new SearchSafelyReportResult();

            if (!string.IsNullOrEmpty(search.ProjectHashId))
            {
                search.ProjectId = _hashidService.Decode(search.ProjectHashId);
            }

            var companies = _companyRepository.FindAll();
            var safelyReports = _safelyReportRepository.Search(search);

            var safelyReportModels = safelyReports.ToList().CloneToListModels<SafelyReport, SafelyReportResultModel>();
            foreach (var safely in safelyReportModels)
            {
                safely.ProjectHashId = _hashidService.Encode(safely.ProjectId);
                var user = _userRepository.FindById(safely.CreatedBy);
                safely.CreatedUserName = user.FullName ?? user.Email;
                safely.ProjectName = _projectRepository.FindById(safely.ProjectId).Name;
            }

            result.Records = safelyReportModels;
            result.TotalRecord = search.TotalRecord;
            result.PageIndex = search.PageIndex;
            result.PageSize = search.PageSize;
            result.PageCount = result.TotalRecord / result.PageSize + (result.TotalRecord % result.PageSize > 0 ? 1 : 0);
            return result;
        }

        public List<SafelyReport> FilterSafelyReportByQuarter(List<SafelyReport> safelyReports, int quarter, int year)
        {
            var test = safelyReports.Select(c => c.CreatedDate.Month);
            var result = new List<SafelyReport>();
            switch (quarter)
            {
                case 1:
                    result = safelyReports.Where(c => c.CreatedDate.Month == 1 || c.CreatedDate.Month == 2 || c.CreatedDate.Month == 3).ToList();
                    break;
                case 2:
                    result = safelyReports.Where(c => c.CreatedDate.Month == 4 || c.CreatedDate.Month == 5 || c.CreatedDate.Month == 6).ToList();
                    break;
                case 3:
                    result = safelyReports.Where(c => c.CreatedDate.Month == 7 || c.CreatedDate.Month == 8 || c.CreatedDate.Month == 9).ToList();
                    break;
                case 4:
                    result = safelyReports.Where(c => c.CreatedDate.Month == 10 || c.CreatedDate.Month == 11 || c.CreatedDate.Month == 12).ToList();
                    break;
            }
            return result.Where(c => c.CreatedDate.Year == year).ToList();
        }

        public List<SafelyReport> FilterSafelyReportByDateTime(List<SafelyReport> reportModels, StatisticalSafelyReportModel statistical)
        {
            if (!statistical.Year.HasValue) statistical.Year = DateTime.UtcNow.Year;
            var result = new List<SafelyReport>();
            if (statistical.Month.HasValue)
            {
                result = reportModels.Where(c => c.CreatedDate.Month == statistical.Month.Value && c.CreatedDate.Year == statistical.Year).ToList();
            }
            if (statistical.Quarter.HasValue)
            {
                result = reportModels = FilterSafelyReportByQuarter(reportModels, statistical.Quarter.Value, statistical.Year.Value).ToList();
            };
            if (statistical.FromDate.HasValue && statistical.ToDate.HasValue)
            {
                var test = reportModels.Select(c => c.CreatedDate);
                result = reportModels.Where(c => c.CreatedDate < statistical.ToDate.Value && c.CreatedDate > statistical.FromDate.Value).ToList();
            }
            return result;
        }

        //public StatisticalViolationType CalculateViolationSafel(IEnumerable<SafelyReport> safelyReports)
        //{
        //    var statistic = new StatisticalViolationType();
        //    //safelyReports.ToList().ForEach(s =>
        //    //{
        //    //    statistic.SumThan3Month += s.Than3Month ?? 0;
        //    //    statistic.SumLessThan3Month += s.LessThan3Month ?? 0;
        //    //    statistic.SumHaveMedicalExamination += s.NotHaveMedicalExamination ?? 0;
        //    //    statistic.SumUndressed += s.Undressed ?? 0;
        //    //    statistic.SumNotWearingAHat += s.NotWearingAHat ?? 0;
        //    //    statistic.SumNotWearShoes += s.NotWearShoes ?? 0;
        //    //    statistic.SumNotWearingLeatherStraps += s.NotWearingLeatherStraps ?? 0;
        //    //    statistic.SumHaveAlcoholic += s.HaveAlcoholic ?? 0;
        //    //    if (s.NotWorkplacePartition == false)
        //    //        statistic.SumWorkplacePartition++;
        //    //    if (s.NotConstructionStandardCompliance == false)
        //    //        statistic.SumConstructionStandardCompliance++;
        //    //    if (s.NotFullMedicalEquipment == false)
        //    //        statistic.SumFullMedicalEquipment++;
        //    //    if (s.NotRolesOfPersionCompliance == false)
        //    //        statistic.SumRolesOfPersionCompliance++;
        //    //});
        //    return statistic;
        //}

        //public StatisticalViolationType StatisticalSafelyReport(StatisticalSafelyReportModel statistical)
        //{
        //    List<SafelyReport> safelyReports = new List<SafelyReport>();
        //    if (statistical.ProjectId.HasValue)
        //        safelyReports = _safelyReportRepository.FindAll(c => c.ProjectId == statistical.ProjectId.Value /*&& c.CompanyId == statistical.CompanyId*/).ToList();
        //    else
        //        safelyReports = _safelyReportRepository.FindAll(/*c => c.CompanyId == statistical.CompanyId*/).ToList();

        //    if ((statistical.FromDate.HasValue && statistical.ToDate.HasValue) || statistical.Quarter.HasValue || statistical.Month.HasValue)
        //        safelyReports = FilterSafelyReportByDateTime(safelyReports, statistical);
        //    return CalculateViolationSafel(safelyReports);
        //}

        public SafelyReportInfoModel GetSafelyReportById(int reportId)
        {
            var result = new SafelyReportInfoModel();
            var safely = _safelyReportRepository.FindById(reportId);
            if (safely == null) throw new ServiceException("Báo cáo an toàn không tồn tại");
            var safelyImages = _safelyReportImageRepository.FindAll(c => c.SafelyReportId == safely.Id);

            result.ProjectHashId = _hashidService.Encode(safely.ProjectId);
            result.ProjectName = _projectRepository.FindById(safely.ProjectId).Name;
            var user = _userRepository.FindById(safely.CreatedBy);
            result.CreatedUserName = user.FullName ?? user.Email;
            result.CreatedDate = safely.CreatedDate;
            result.DisadvantagesFixingRequirement = safely.DisadvantagesFixingRequirement;
            result.Requests = safely.Requests;
            result.Reason = safely.Reason;
            result.Status = safely.Status;

            var safelyImageIds = safelyImages.Select(c => c.Id);
            var safelyImageDetails = _safelyImageDetailRepository.FindAll(c => safelyImageIds.Contains(c.SafelyImageId));

            var violationTotals = _safelyErrorCollectionRepository.FindAll(c => safelyImageIds.Contains(c.SafelyImageId));

            foreach (var image in safelyImages)
            {
                var safelyImageModel = new SafelyReportImageModel
                {
                    CompanyId = image.CompanyId,
                    CompanyName = _companyRepository.FindById(image.CompanyId).Name,
                    Coordinates = new ImageCoordinates
                    {
                        Lat = image.Lat,
                        Lng = image.Lng
                    },
                    Note = image.Note,
                    DayPhotographed = image.DayPhotographed
                };
                safelyImageModel.ImageInfoId = image.Id;
                if (safelyImageDetails.Count() > 0)
                {
                    var imageIns = safelyImageDetails.FirstOrDefault(c => c.SafelyImageId == image.Id);
                    if (imageIns != null)
                    {
                        safelyImageModel.Images = JsonConvert.DeserializeObject<List<string>>(imageIns.Files);
                    }
                }

                result.UnitBuilds += safelyImageModel.CompanyName + ", ";
                var violations = violationTotals.Where(c => c.SafelyImageId == image.Id);
                safelyImageModel.ViolationDefined = new ViolationDefinedModel();
                violations.ToList().ForEach(v =>
                {

                    var attr = v.Type.GetAttributeOfType<ViolationDetailAttribute>();
                    if (attr.Group == ViolationGroupCode.ViolationHuman)
                    {
                        var violationHuman = new ViolationHuman
                        {
                            Code = v.Type,
                            Title = attr.Name,
                            ViolationNumber = v.ViolationQuantity ?? 0
                        };
                        safelyImageModel.ViolationDefined.ViolationHumans.Add(violationHuman);
                    }
                    if (attr.Group == ViolationGroupCode.ViolationEnvironment)
                    {
                        safelyImageModel.ViolationDefined.ViolationEnvironments.Add(new ViolationEnvironment
                        {
                            Code = v.Type,
                            Title = attr.Name,
                            ViolationNumber = v.ViolationQuantity ?? 0
                        });
                    }
                });

                result.ImageInfo.Add(safelyImageModel);
            }

            result.UnitBuilds = String.Join(",", result.ImageInfo.Select(c => c.CompanyName).ToArray());

            //safelyReportModel.CompanyName = _companyRepository.FindById(safely.CompanyId).Name;
            return result;
        }

        public IEnumerable<SafelyDraftModel> ListDraft(int user, int projectId)
        {
            var results = new List<SafelyDraftModel>();
            var entities = _safelyDraftRepository.FindAll(c => c.CreatedBy == user && c.ProjectId == projectId);
            var safelyDraftIds = entities.Select(c => c.Id);

            var safelyDraftImages = _safelyImageDraftDetailRepository.FindAll(c => safelyDraftIds.Contains(c.SafelyDraftId));

            var violationTotals = _safelyErrorCollectionDraftRepository.FindAll(c => safelyDraftIds.Contains(c.SafelyImageDraftId));

            var safelyDraftImageDetails = _safelyImageDraftDetailRepository.FindAll(c => safelyDraftIds.Contains(c.SafelyDraftId));

            var listFileIds = new List<Guid>();
            foreach (var item in safelyDraftImageDetails)
            {
                var fileDesializes = JsonConvert.DeserializeObject<List<Guid>>(item.Files);
                listFileIds.AddRange(fileDesializes);
            }
            var files = new List<File>();

            foreach (var item in listFileIds)
            {
                files.Add(_fileRepository.FindById(item));
            }
            var storageIds = files.Select(c => c.StorageId);
            var binaries = _binaryFileStorageRepository.FindAll(c => storageIds.Contains(c.Id));

            foreach (var e in entities)
            {
                var result = new SafelyDraftModel();
                result = new SafelyDraftModel
                {
                    Id = e.Id,
                    ProjectHashId = _hashidService.Encode(e.ProjectId)
                };

                var safelyDraftImageDetail = safelyDraftImageDetails.FirstOrDefault(c => c.SafelyDraftId == e.Id);

                var imageDraftModel = new SafelyReportImageModel
                {
                    CompanyId = e.CompanyId,
                    CompanyName = _companyRepository.FindById(e.CompanyId).Name,
                    Coordinates = new ImageCoordinates
                    {
                        Lat = e.Lat,
                        Lng = e.Lng
                    },
                    Note = e.Note,
                    DayPhotographed = e.DayPhotographed
                };
                if (!string.IsNullOrEmpty(safelyDraftImageDetail.Files))
                {
                    imageDraftModel.Images = JsonConvert.DeserializeObject<List<string>>(safelyDraftImageDetail.Files);
                }
                imageDraftModel.ImageInfoId = e.Id;


                var violations = violationTotals.Where(c => c.SafelyImageDraftId == e.Id);
                imageDraftModel.ViolationDefined = new ViolationDefinedModel();
                violations.ToList().ForEach(v =>
                {

                    var attr = v.Type.GetAttributeOfType<ViolationDetailAttribute>();
                    if (attr.Group == ViolationGroupCode.ViolationHuman)
                    {
                        var violationHuman = new ViolationHuman
                        {
                            Code = v.Type,
                            Title = attr.Name,
                            ViolationNumber = v.ViolationQuantity ?? 0
                        };
                        imageDraftModel.ViolationDefined.ViolationHumans.Add(violationHuman);
                    }
                    if (attr.Group == ViolationGroupCode.ViolationEnvironment)
                    {
                        imageDraftModel.ViolationDefined.ViolationEnvironments.Add(new ViolationEnvironment
                        {
                            Code = v.Type,
                            Title = attr.Name,
                            ViolationNumber = v.ViolationQuantity ?? 0
                        });
                    }
                });
                result.ImageInfo = imageDraftModel;
                results.Add(result);
            }
            return results;
        }

        public bool UpdateSafelyDraft(UpdateSafelyDraft updateSafelyDraft)
        {
            var success = false;
            var trans = _safelyDraftRepository.BeginTransaction();
            if (trans.Connection.State != ConnectionState.Open) trans.Connection.Open();
            try
            {
                var project = _projectRepository.FindById(updateSafelyDraft.ProjectId, trans);
                if (project == null) throw new ServiceException("Dự án không tồn tại");
                if (project.Status != ProjectStatus.Approved) throw new ServiceException("Dự án chưa được duyệt");

                var entity = new SafelyReport();
                var updateImage = updateSafelyDraft.ImageInfo;

                var company = _companyRepository.FindById(updateImage.CompanyId, trans);
                if (company == null) throw new ServiceException("Công ty không tồn tại");
                //var projectCompany = _projectCompanyRepository.Find(c => c.ProjectId == project.Id && c.CompanyId == company.Id, trans);
                //if (projectCompany == null) throw new ServiceException("Công ty " + company.Name + " chưa tham gia vào dự án này");


                updateSafelyDraft.CreatedDate = DateTime.UtcNow;
                updateSafelyDraft.CreatedBy = updateSafelyDraft.CreatedBy;


                //var images = _safelyReportImageRepository.FindAll(c => c.SafelyReportId == entity.Id, trans);
                //foreach (var image in images)
                //{
                //    if (!Guid.Equals(Guid.Parse(updateSafelyDraft.ImageInfo.Image), image.FileId))
                //    {
                //        _safelyReportImageRepository.Delete(image, trans);
                //        var file = _fileRepository.FindById(image.FileId, trans);
                //        _fileRepository.Delete(file, trans);
                //        _binaryFileStorageRepository.Delete(new BinaryFileStorage { Id = file.StorageId }, trans);
                //    }
                //}

                success = AddSafelyImageDraft(trans, updateSafelyDraft);
                if (!success)
                {
                    trans.Rollback();

                }
            }
            catch (Exception ex)
            {
                trans.Rollback();
                throw ex;
            }
            trans.Commit();
            return success;
        }

        public bool AddSafelyImageDraft(IDbTransaction transaction, UpdateSafelyDraft updateSafelyDraft)
        {
            var success = false;
            var updateViolation = false;

            if (updateSafelyDraft.ImageInfo == null) throw new ServiceException("Bản nháp chưa chọn lỗi");
            var updateImage = updateSafelyDraft.ImageInfo;

            var imageGuidId = string.Empty;

            var safelyDraft = new SafelyDraft();
            if (updateSafelyDraft.ImageInfo.ImageInfoId.HasValue)
            {
                if (updateSafelyDraft.ImageInfo.ImageInfoId != 0)
                {
                    safelyDraft = _safelyDraftRepository.FindById(updateSafelyDraft.ImageInfo.ImageInfoId.Value, transaction);
                    success = true;
                }
            }
            else
            {
                safelyDraft = new SafelyDraft
                {
                    ProjectId = updateSafelyDraft.ProjectId,
                    CompanyId = updateImage.CompanyId,
                    Note = updateImage.Note,
                    DayPhotographed = DateTime.UtcNow,
                    CreatedBy = updateSafelyDraft.CreatedBy
                };

                if (updateImage.Coordinates != null)
                {
                    safelyDraft.Lat = updateImage.Coordinates.Lat;
                    safelyDraft.Lng = updateImage.Coordinates.Lng;
                }
                success = _safelyDraftRepository.Insert(safelyDraft, transaction);
            }


            var listAddImages = new List<Guid>();
            foreach (var image in updateSafelyDraft.ImageInfo.Images)
            {
                var base64 = image;
                base64 = base64.Replace("data:image/jpeg;base64,", string.Empty);
                base64 = base64.Replace("data:image/png;base64,", string.Empty);

                if (base64.Length > 100)
                {
                    byte[] decodedByteArray = Convert.FromBase64String(base64);
                    var binaryFileStorage = new BinaryFileStorage()
                    {
                        Data = decodedByteArray
                    };
                    var insertBinaryFile = _binaryFileStorageRepository.Insert(binaryFileStorage, transaction);

                    var company = _companyRepository.FindById(updateImage.CompanyId, transaction);
                    var file = new Entity.File()
                    {
                        Id = Guid.NewGuid(),
                        ProviderType = ProviderFileType.LocalStorage,
                        StorageId = binaryFileStorage.Id,
                        Mime = "image/jpeg",
                        Title = "Ảnh bản nháp an toàn vệ sinh cho công ty " + company.Name,
                        CreatedUtcTime = DateTime.UtcNow,
                        Size = base64.Length / 1024,
                        Status = FileStatus.NewPicture
                    };
                    var insertFile = _fileRepository.Insert(file, transaction);
                    listAddImages.Add(file.Id);
                }
            }

            if (listAddImages.Count() > 0)
            {
                var existImageDraftDetail = _safelyImageDraftDetailRepository.Find(c => c.SafelyDraftId == safelyDraft.Id, transaction);
                if (existImageDraftDetail == null)
                {
                    var insertSafelyImageDetail = _safelyImageDraftDetailRepository.Insert(new SafelyImageDraftDetail
                    {
                        SafelyDraftId = safelyDraft.Id,
                        Files = JsonConvert.SerializeObject(listAddImages)
                    }, transaction);
                }
                else
                {
                    var sezializeFiles = JsonConvert.DeserializeObject<List<Guid>>(existImageDraftDetail.Files);
                    sezializeFiles.AddRange(listAddImages);
                    existImageDraftDetail.Files = JsonConvert.SerializeObject(sezializeFiles);
                    var update = _safelyImageDraftDetailRepository.Update(existImageDraftDetail, transaction);
                }
            }

            updateViolation = UpdateViolitionDraft(transaction, safelyDraft.Id, updateImage.ViolationDefined);

            return success && updateViolation;
        }

        public bool UpdateViolitionDraft(IDbTransaction transaction, int safelyDraftImageId, ViolationDefinedModel violation)
        {
            var res = false;
            var violationExists = _safelyErrorCollectionDraftRepository.FindAll(c => c.SafelyImageDraftId == safelyDraftImageId, transaction);
            var violationHumans = violation.ViolationHumans;
            var violationEnvironments = violation.ViolationEnvironments;


            if (violationHumans.Count() > 0)
            {
                foreach (var violationHuman in violationHumans)
                {
                    if (!violationExists.Select(v => v.Type).Contains(violationHuman.Code))
                    {
                        res = _safelyErrorCollectionDraftRepository.Insert(new SafelyErrorCollectionDraft
                        {
                            ViolationQuantity = violationHuman.ViolationNumber,
                            SafelyImageDraftId = safelyDraftImageId,
                            Type = violationHuman.Code
                        }, transaction);
                        if (!res) break;
                    }

                    else
                    {
                        foreach (var violationExist in violationExists)
                        {
                            if (violationExist.Type.GetAttributeOfType<ViolationDetailAttribute>()
                             .Group == ViolationGroupCode.ViolationHuman && violationExist.Type == violationHuman.Code)
                                res = _safelyErrorCollectionDraftRepository.Update(new SafelyErrorCollectionDraft
                                {
                                    Id = violationExist.Id,
                                    ViolationQuantity = violationHuman.ViolationNumber,
                                    SafelyImageDraftId = safelyDraftImageId,
                                    Type = violationExist.Type
                                }, transaction);
                            if (!res) break;
                        }

                    }
                }
            }
            if (violationEnvironments.Count() > 0)
            {
                foreach (var violationEnvironment in violationEnvironments)
                {
                    if (!violationExists.Select(v => v.Type).Contains(violationEnvironment.Code))
                    {

                        res = _safelyErrorCollectionDraftRepository.Insert(new SafelyErrorCollectionDraft
                        {
                            ViolationQuantity = violationEnvironment.ViolationNumber,
                            SafelyImageDraftId = safelyDraftImageId,
                            Type = violationEnvironment.Code
                        }, transaction);

                        if (!res) break;
                    }

                    else
                    {
                        foreach (var violationExist in violationExists)
                        {
                            if (violationExist.Type.GetAttributeOfType<ViolationDetailAttribute>()
                             .Group == ViolationGroupCode.ViolationEnvironment && violationExist.Type == violationEnvironment.Code)
                            {
                                res = _safelyErrorCollectionDraftRepository.Update(new SafelyErrorCollectionDraft
                                {
                                    Id = violationExist.Id,
                                    ViolationQuantity = violationEnvironment.ViolationNumber,
                                    SafelyImageDraftId = safelyDraftImageId,
                                    Type = violationExist.Type
                                }, transaction);

                            }


                            if (!res) break;
                        }

                    }
                }
            }
            return res;
        }

        public bool DeleteMultilSafelyDraft(DeleteMultiDraftModel deleteMultiDraft)
        {
            var success = false;
            var trans = _safelyDraftRepository.BeginTransaction();
            if (trans.Connection.State != ConnectionState.Open) trans.Connection.Open();
            try
            {
                if (deleteMultiDraft.SafelyDraftId.Count() > 0)
                {
                    var safelyDrafts = _safelyDraftRepository.FindAll(c => deleteMultiDraft.SafelyDraftId.Contains(c.Id), trans);
                    var safelyDraftIds = safelyDrafts.Select(c => c.Id);
                    var safelyErrors = _safelyErrorCollectionDraftRepository.FindAll(c => safelyDraftIds.Contains(c.SafelyImageDraftId), trans);
                    var imageDetails = _safelyImageDraftDetailRepository.FindAll(c => safelyDraftIds.Contains(c.SafelyDraftId), trans);


                    foreach (var del in deleteMultiDraft.SafelyDraftId)
                    {
                        var draft = safelyDrafts.First(c => c.Id == del);
                        if (draft.CreatedBy != deleteMultiDraft.UserId)
                            throw new ServiceException("Bạn không được phép xóa bản nháp người khác lập");
                        else
                        {
                            var deleteDraft = _safelyDraftRepository.Delete(draft, trans);
                            if (deleteDraft)
                            {
                                var deleteError = true;
                                var safelyErrorIns = safelyErrors.Where(c => c.SafelyImageDraftId == del).ToList();
                                foreach (var item in safelyErrorIns)
                                {
                                    deleteError = _safelyErrorCollectionDraftRepository.Delete(item, trans);
                                    if (!deleteError) break;
                                }

                                var deleteDetail = true;
                                var imageDetailIns = imageDetails.Where(c => c.SafelyDraftId == del).ToList();
                                foreach (var imageDetail in imageDetailIns)
                                {
                                    deleteDetail = _safelyImageDraftDetailRepository.Delete(imageDetail, trans);
                                    if (!deleteDetail) break;
                                }

                                if (deleteError && deleteDetail)
                                    success = true;
                            }
                        }

                        if (!success) break;
                    }
                }
                else throw new ServiceException("Không có bản nháp nào được chọn");
                if (!success) trans.Rollback();
            }
            catch (Exception)
            {

                trans.Rollback();
                throw;
            }
            trans.Commit();
            return success;
        }

        public List<SafelyViolationTypeStatistical> StatisticalOneSafelyReport(int safelyReportId)
        {
            var result = new List<SafelyViolationTypeStatistical>();
            var safelyReport = _safelyReportRepository.FindById(safelyReportId);
            if (safelyReport == null) throw new ServiceException("Báo cáo an toàn vệ sinh lao động không tồn tại");
            var images = _safelyReportImageRepository.FindAll(c => c.SafelyReportId == safelyReportId);
            var violations = new List<SafelyErrorCollection>();
            //var companyIds = new List<int>();
            //companyIds = images.Select(c => c.CompanyId).ToList();


            foreach (var type in (ViolationType[])Enum.GetValues(typeof(ViolationType)))
            {

                var res = new SafelyViolationTypeStatistical
                {

                    Type = type,
                    ViolationTypeName = type.GetAttributeOfType<ViolationDetailAttribute>().Name
                };

                if (images.Count() > 0)
                {

                    foreach (var image in images)
                    {
                        var statisticCompany = new StatisticViolationCompany();
                        statisticCompany.CompanyId = image.CompanyId;
                        statisticCompany.CompanyName = _companyRepository.FindById(image.CompanyId).Name;
                        var errorCollections = _safelyErrorCollectionRepository.FindAll(c => c.SafelyImageId == image.Id);
                        foreach (var error in errorCollections)
                        {
                            if (type.GetAttributeOfType<ViolationDetailAttribute>()
                                    .Group == ViolationGroupCode.ViolationEnvironment && error.Type == type)
                                statisticCompany.ViolationNumber += error.ViolationQuantity.Value;
                            if (type.GetAttributeOfType<ViolationDetailAttribute>()
                                    .Group == ViolationGroupCode.ViolationHuman && error.Type == type)
                                statisticCompany.ViolationNumber += error.ViolationQuantity.Value;
                        }
                        if (!res.Companies.Select(c => c.CompanyId).Contains(statisticCompany.CompanyId))
                            res.Companies.Add(statisticCompany);
                        else
                        {
                            //statisticCompany.ViolationNumber += statisticCompany.ViolationNumber;
                            //var test = res.Companies.Select(c => c.ViolationNumber);
                            res.Companies.Where(c => c.CompanyId == statisticCompany.CompanyId)
                                .FirstOrDefault().ViolationNumber += statisticCompany.ViolationNumber;
                        }


                    }
                }
                result.Add(res);
            }

            return result;
        }


        public StatisticalViolationModel StatisticalSafelyReportInProject(SearchStatisticSafelyReportOneProject searchStatistic)
        {
            var result = new StatisticalViolationModel();
            if (string.IsNullOrEmpty(searchStatistic.HashId)) throw new ServiceException("Yêu cầu nhập dự án");
            var project = _projectRepository.FindById(searchStatistic.ProjectId);
            if (project == null) throw new ServiceException("Dự án không tồn tại");
            var companyProjects = _projectCompanyRepository.FindAll(c => c.ProjectId == project.Id);
            if (companyProjects.Count() > 0)
            {
                var companyExists = _companyRepository.FindAll();
                var companies = new List<Company>();
                var safelyReports = new List<SafelyReport>();


                if (searchStatistic.EndDate.HasValue && searchStatistic.StartDate.HasValue)
                {
                    var startDate = searchStatistic.StartDate.Value.Date.ToUniversalTime();
                    var endDate = searchStatistic.EndDate.Value.Date.AddDays(2).AddTicks(-1).ToUniversalTime();
                    if (startDate > endDate) throw new ServiceException("Ngày bắt đầu không được lớn hơn ngày kết thúc");
                    safelyReports = _safelyReportRepository.FindAllBetween(startDate, endDate,
                            c => c.CreatedDate, c => c.ProjectId == project.Id && c.Status == SafelyReportStatus.Approve).ToList();
                }

                if ((!searchStatistic.StartDate.HasValue) || (!searchStatistic.EndDate.HasValue))
                    throw new ServiceException("Yêu cầu chọn ngày bắt đầu và ngày kết thúc");

                var safelyErrorCollections = new List<SafelyErrorCollection>();
                var images = new List<SafelyReportImage>();
                if (safelyReports.Count() > 0)
                {
                    var safelyReportIds = safelyReports.Select(c => c.Id);
                    images = _safelyReportImageRepository.FindAll(c => safelyReportIds.Contains(c.SafelyReportId)).ToList();

                    if (images.Count() > 0)
                    {
                        var imageIds = images.Select(c => c.Id);
                        safelyErrorCollections = _safelyErrorCollectionRepository.FindAll(c => imageIds.Contains(c.SafelyImageId)).ToList();
                    }
                }

                companyProjects.ToList().ForEach(m =>
                {
                    var company = companyExists.FirstOrDefault(c => c.Id == m.CompanyId);
                    companies.Add(company);
                });


                companies.ForEach(m =>
                {
                    var companyViolationTotal = new CompanyViolationTotalInfo
                    {
                        CompanyId = m.Id
                    };
                    result.CompanyViolationTotals.Add(companyViolationTotal);
                });

                foreach (var type in (ViolationType[])Enum.GetValues(typeof(ViolationType)))
                {
                    var res = new SafelyViolationTypeStatistical
                    {

                        Type = type,
                        ViolationTypeName = type.GetAttributeOfType<ViolationDetailAttribute>().Name
                    };

                    if (companies.Count() > 0)
                    {
                        foreach (var company in companies)
                        {
                            var rateNumber = 0;
                            var statisticCompany = new StatisticViolationCompany();
                            statisticCompany.CompanyId = company.Id;
                            statisticCompany.CompanyName = company.Name;

                            if (images.Count() > 0 && images.Select(c => c.CompanyId).Contains(company.Id))
                            {
                                var imageIds = images.Select(c => c.Id);
                                var errorAlls = safelyErrorCollections.Where(c => imageIds.Contains(c.SafelyImageId));
                                foreach (var image in images)
                                {
                                    if (image.CompanyId == company.Id)
                                    {
                                        var errorCollections = safelyErrorCollections.Where(c => c.SafelyImageId == image.Id);

                                        foreach (var error in errorCollections)
                                        {
                                            if (type.GetAttributeOfType<ViolationDetailAttribute>()
                                                    .Group == ViolationGroupCode.ViolationEnvironment && error.Type == type)
                                            {
                                                statisticCompany.ViolationNumber += error.ViolationQuantity.Value;
                                                result.CompanyViolationTotals.FirstOrDefault(c => c.CompanyId == company.Id).ViolationTotal += error.ViolationQuantity.Value;
                                            }

                                            if (type.GetAttributeOfType<ViolationDetailAttribute>()
                                                    .Group == ViolationGroupCode.ViolationHuman && error.Type == type)
                                            {
                                                statisticCompany.ViolationNumber += error.ViolationQuantity.Value;
                                                result.CompanyViolationTotals.FirstOrDefault(c => c.CompanyId == company.Id).ViolationTotal += error.ViolationQuantity.Value;
                                            }
                                        }
                                    }
                                }

                            }
                            else
                            {
                                statisticCompany.ViolationNumber = 0;
                            }

                            res.Companies.Add(statisticCompany);
                        }
                    }
                    result.SafelyViolations.Add(res);
                }

                //Rating
                foreach (var company in result.CompanyViolationTotals)
                {
                    var imageIds = images.Where(c => c.CompanyId == company.CompanyId).Select(c => c.Id);
                    var e = safelyErrorCollections.Where(c => imageIds.Contains(c.SafelyImageId) && c.ViolationQuantity > 0).Select(c => c.Type);

                    if (e.Count() > 0)
                    {
                        if (e.Contains(ViolationType.LessThan3Month) || e.Contains(ViolationType.Than3Month) || e.Contains(ViolationType.NotWearingLeatherStraps)
                    || e.Contains(ViolationType.NotConstructionStandardCompliance) || e.Contains(ViolationType.NotFullMedicalEquipment) || e.Contains(ViolationType.NotRolesOfPersionCompliance)
                    || e.Contains(ViolationType.NotRegisterForCommanderWorks) || e.Contains(ViolationType.PopularBeforeConstruction) || e.Contains(ViolationType.NotHaveATV))
                        {
                            result.CompanyViolationTotals.FirstOrDefault(c => c.CompanyId == company.CompanyId).Rating = "C";
                            continue;
                        }

                        if (e.Contains(ViolationType.Undressed) || e.Contains(ViolationType.NotWearingAHat) || e.Contains(ViolationType.NotWearShoes)
                        || e.Contains(ViolationType.HaveAlcoholic) || e.Contains(ViolationType.NotWorkplacePartition))

                        {
                            result.CompanyViolationTotals.FirstOrDefault(c => c.CompanyId == company.CompanyId).Rating = "B";
                            continue;
                        }
                    }
                    result.CompanyViolationTotals.FirstOrDefault(c => c.CompanyId == company.CompanyId).Rating = "A";
                }
            }
            return result;
        }

        public (MemoryStream, string) ExportStatisticalSafelyReportInProject(int projectId, DateTime? startDate, DateTime? endDate)
        {
            var hashId = _hashidService.Encode(projectId);
            var project = _projectRepository.FindById(projectId);
            if (project == null) throw new ServiceException("Dự án không tồn tại");
            var statisticalSafelyReportModels = StatisticalSafelyReportInProject(new SearchStatisticSafelyReportOneProject
            {
                ProjectId = projectId,
                StartDate = startDate,
                EndDate = endDate,
                HashId = hashId
            });
            var companyStaticIds = statisticalSafelyReportModels.CompanyViolationTotals.Select(c => c.CompanyId);
            var companies = _companyRepository.FindAll(c => companyStaticIds.Contains(c.Id));

            var stream = new MemoryStream();
            using (var excelPackage = new ExcelPackage(stream))
            {
                ExcelWorksheet worksheet = excelPackage.Workbook.Worksheets.Add("Tổng hợp báo cáo an toàn vệ sinh một dự án");
                SetBackgroundExportStatisticalSafelyReportInProjectToExcel(worksheet, projectId, startDate, endDate, companies);
                int i = 4;
                var count = statisticalSafelyReportModels.SafelyViolations.Count();
                foreach (var exportSafelyModels in statisticalSafelyReportModels.SafelyViolations)
                {

                    worksheet.Cells[i, 1].Value = i - 3;
                    worksheet.Cells[i, 2].Value = exportSafelyModels.ViolationTypeName;
                    var violationCompany = exportSafelyModels.Companies;
                    int j = 1;
                    foreach (var item in violationCompany)
                    {
                        worksheet.Cells[i, 2 + j].Value = item.ViolationNumber;
                        j++;
                    }
                    i++;
                }


                int k = 3;
                foreach (var item in statisticalSafelyReportModels.CompanyViolationTotals)
                {
                    worksheet.Cells[count + 4, k].Value = item.ViolationTotal;
                    k++;
                }

                worksheet.Cells[count + 4, 2].Value = "Tổng";
                worksheet.Row(count + 4).Style.Font.Bold = true;

                int h = 3;
                foreach (var item in statisticalSafelyReportModels.CompanyViolationTotals)
                {
                    worksheet.Cells[count + 5, h].Value = item.Rating;
                    h++;
                }

                worksheet.Cells[count + 5, 2].Value = "Xếp loại thực hiện công tác ATVSLĐ";
                worksheet.Row(count + 5).Style.Font.Bold = true;
                stream = new MemoryStream(excelPackage.GetAsByteArray());
            }
            return (stream, project.Name);
        }

        private void SetBackgroundExportStatisticalSafelyReportInProjectToExcel(ExcelWorksheet worksheet, int projectId, DateTime? startDate,
            DateTime? endDate, IEnumerable<Company> companies)
        {
            var project = _projectRepository.FindById(projectId);
            worksheet.Cells.Style.WrapText = true;

            using (var range = worksheet.Cells["A:O"])
            {
                range.Style.Font.SetFromFont(new Font("Times New Roman", 12));
            }
            using (var range = worksheet.Cells[3, 1, 100, companies.Count() + 2])
            {
                range.Style.Font.SetFromFont(new Font("Times New Roman", 14));
                range.Style.Font.Bold = false;
                range.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                range.Style.VerticalAlignment = ExcelVerticalAlignment.Center;
            }

            worksheet.Cells.Style.WrapText = true;
            worksheet.Cells[3, 1].Value = "STT";
            worksheet.Cells[3, 2].Value = "Lỗi vi phạm";

            var type = "General";

            worksheet.Cells[2, 1, 2, companies.Count() + 2].Style.Font.Size = 18;
            worksheet.Cells[2, 1, 2, companies.Count() + 2].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            worksheet.Cells[2, 1, 2, companies.Count() + 2].Style.VerticalAlignment = ExcelVerticalAlignment.Center;

            var sd = startDate.Value.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture);
            var ed = endDate.Value.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture);

            var row11 = worksheet.Cells[2, 1].RichText.Add("Bảng tổng hợp báo cáo công tác an toàn vệ sinh lao động(" + sd + " - " + ed + ")" + "\r\n");
            row11.Size = 16;
            row11.Bold = true;
            var row12 = worksheet.Cells[2, 1].RichText.Add("Công trình: " + project.Name + "" + "\r\n");
            row12.Size = 14;
            row12.Bold = true;
            worksheet.Cells[2, 1].AutoFitColumns();

            worksheet.Row(2).Style.Font.Bold = true;

            worksheet.Column(1).Width = 10;
            worksheet.Column(1).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            worksheet.Column(2).Width = 50;

            int i = 1;
            foreach (var item in companies)
            {
                worksheet.Cells[3, 2 + i].Value = item.Name;
                worksheet.Column(2 + i).Width = 20;
                worksheet.Column(2 + i).Style.Numberformat.Format = type;
                worksheet.Column(2 + i).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                i++;
            }
            if (companies.Count() + 2 <= 5)
            {
                int k = 1;
                foreach (var item in companies)
                {

                    worksheet.Column(2 + k).Width = 40;

                    k++;
                }
                if (companies.Count() + 2 <= 4)
                {
                    TitleCompanyExcel.FillTitleCommanderWorks(worksheet, project.Name, companies.Count() + 2, 2, 1);
                }
                else TitleCompanyExcel.FillTitleCommanderWorks(worksheet, project.Name, companies.Count() + 2, 2, 2);
            }
            else
            {
                TitleCompanyExcel.FillTitleCommanderWorks(worksheet, project.Name, companies.Count() + 2, 2, 2);
            }
            worksheet.View.FreezePanes(4, 1);
        }




        public StatisticalViolationAllProjectModel StatisticalSafelyReportAllProject(CheckStatisticalWithTime searchStatistic)
        {
            var result = new StatisticalViolationAllProjectModel();

            var projects = _projectRepository.FindAll(c => c.Status == ProjectStatus.Approved);
            var projectIds = projects.Select(c => c.Id);
            var companyProjects = _projectCompanyRepository.FindAll(c => projectIds.Contains(c.ProjectId));

            var companyRuns = _companyRepository.FindAll();


            result.StatisticCompanyWithProjects = new List<StatisticCompanyWithProject>();
            foreach (var project in projects)
            {
                var companyStatistic = new StatisticCompanyWithProject
                {
                    ProjectId = project.Id,
                    ProjectName = project.Name,
                    HashId = _hashidService.Encode(project.Id)
                };

                var checkProjects = StatisticalSafelyReportInProject(new SearchStatisticSafelyReportOneProject
                {
                    HashId = _hashidService.Encode(project.Id),
                    ProjectId = project.Id,
                    StartDate = searchStatistic.StartDate,
                    EndDate = searchStatistic.EndDate
                });

                foreach (var companyRun in companyRuns)
                {
                    //if(companyProjects.Count(c=>c.ProjectId == project.Id && c.CompanyId == companyRun.Id) > 0)
                    //{
                    var ratingCompany = new RatingCompanyViolation()
                    {
                        CompanyId = companyRun.Id,
                        CompanyName = companyRun.Name,

                    };

                    var checkCompany = checkProjects.CompanyViolationTotals.FirstOrDefault(c => c.CompanyId == companyRun.Id);
                    if (checkCompany == null)
                        ratingCompany.Rating = "-";
                    else
                        ratingCompany.Rating = checkCompany.Rating;
                    companyStatistic.RatingCompanyViolations.Add(ratingCompany);
                    //}
                }
                result.StatisticCompanyWithProjects.Add(companyStatistic);
            }

            foreach (var companyRun in companyRuns)
            {
                var ratingCompanyTotal = new RatingCompanyViolation
                {
                    CompanyId = companyRun.Id,
                    CompanyName = companyRun.Name
                };
                //var listRatingCompany = result.StatisticCompanyWithProjects.Select(c => c.RatingCompanyViolations.Where(a => a.CompanyId == companyRun.Id);
                var ratingCompany = result.StatisticCompanyWithProjects.Select(c => c.RatingCompanyViolations.Where(a => a.CompanyId == companyRun.Id));

                var ratingCompanies = new List<RatingCompanyViolation>();
                foreach (var r in result.StatisticCompanyWithProjects)
                {
                    var addRating = r.RatingCompanyViolations.FirstOrDefault(c => c.CompanyId == companyRun.Id);
                    if (addRating != null) ratingCompanies.Add(addRating);
                }

                var countRatingA = ratingCompanies.Count(c => c.Rating == "A");
                var countRatingB = ratingCompanies.Count(c => c.Rating == "B");
                var countRatingC = ratingCompanies.Count(c => c.Rating == "C");
                var countNotRating = ratingCompanies.Count(c => c.Rating == "-");

                if (countNotRating == ratingCompany.Count())
                {
                    ratingCompanyTotal.Rating = "-";
                    result.RatingTotals.Add(ratingCompanyTotal);
                    continue;
                }

                if (countRatingC > 0)
                {
                    ratingCompanyTotal.Rating = "C";
                    result.RatingTotals.Add(ratingCompanyTotal);
                    continue;
                }

                if (countRatingA >= 0.25 * countRatingB)
                {
                    ratingCompanyTotal.Rating = "A";
                    result.RatingTotals.Add(ratingCompanyTotal);
                    continue;
                }
                else
                {
                    ratingCompanyTotal.Rating = "B";
                    result.RatingTotals.Add(ratingCompanyTotal);
                    continue;
                }
            }
            return result;
        }

        public MemoryStream ExportStatisticalSafelyReportAllProject(DateTime? startDate, DateTime? endDate)
        {
            var statisticalSafelyReportModels = StatisticalSafelyReportAllProject(new CheckStatisticalWithTime
            {
                StartDate = startDate,
                EndDate = endDate
            });


            var stream = new MemoryStream();
            using (var excelPackage = new ExcelPackage(stream))
            {
                ExcelWorksheet worksheet = excelPackage.Workbook.Worksheets.Add("Tổng hợp báo cáo an toàn vệ sinh một dự án");
                SetBackgroundExportStatisticalSafelyReportAllProjectToExcel(worksheet, startDate.Value, endDate.Value,
                    statisticalSafelyReportModels.RatingTotals.Select(c => c.CompanyId));

                int i = 5;
                var count = statisticalSafelyReportModels.StatisticCompanyWithProjects.Count();
                foreach (var exportSafelyModels in statisticalSafelyReportModels.StatisticCompanyWithProjects)
                {
                    worksheet.Cells[i, 1].Value = i - 4;
                    worksheet.Cells[i, 2].Value = exportSafelyModels.ProjectName;
                    var violationCompany = exportSafelyModels.RatingCompanyViolations;
                    int j = 1;
                    foreach (var item in violationCompany)
                    {
                        worksheet.Cells[i, 2 + j].Value = item.Rating;
                        j++;
                    }
                    i++;
                }
                int k = 1;
                foreach (var item in statisticalSafelyReportModels.RatingTotals)
                {
                    worksheet.Cells[count + 6, 2 + k].Value = item.Rating;
                    k++;
                }

                worksheet.Cells[count + 6, 2].Value = "Tổng hợp xếp loại thực hiện công tác ATVSLĐ";
                worksheet.Row(count + 6).Style.Font.Bold = true;
                stream = new MemoryStream(excelPackage.GetAsByteArray());
            }


            return stream;
        }


        private void SetBackgroundExportStatisticalSafelyReportAllProjectToExcel(ExcelWorksheet worksheet, DateTime? startDate, DateTime? endDate, IEnumerable<int> companyIds)
        {
            var companies = _companyRepository.FindAll(c => companyIds.Contains(c.Id));
            worksheet.Cells.Style.WrapText = true;
            using (var range = worksheet.Cells["A:O"])
            {
                range.Style.Font.SetFromFont(new Font("Times New Roman", 13));
            }
            using (var range = worksheet.Cells[1, 1, 100, companyIds.Count() + 2])
            {
                range.Style.Font.SetFromFont(new Font("Times New Roman", 13));
                range.Style.Font.Bold = false;
                range.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                range.Style.VerticalAlignment = ExcelVerticalAlignment.Center;
            }

            worksheet.Cells.Style.WrapText = true;
            worksheet.Cells[3, 1].Value = "STT";
            worksheet.Cells[3, 2].Value = "Nội dung công việc";

            var type = "General";
            worksheet.Cells[2, 1, 2, companyIds.Count() + 2].Style.Font.Size = 16;
            worksheet.Cells[2, 1, 2, companyIds.Count() + 2].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            worksheet.Cells[2, 1, 2, companyIds.Count() + 2].Style.VerticalAlignment = ExcelVerticalAlignment.Center;

            worksheet.Cells[3, 1, 4, 1].Merge = true;
            worksheet.Cells[3, 2, 4, 2].Merge = true;
            worksheet.Cells[3, 3, 3, companyIds.Count() + 2].Merge = true;

            worksheet.Cells[3, 3].Value = "Xếp loại việc thực hiện công tác ATVSLĐ";

            var sd = startDate.Value.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture);
            var ed = endDate.Value.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture);

            TitleCompanyExcel.FillTitleTechnology(worksheet, companyIds.Count() + 2, 2, 6);
            worksheet.View.FreezePanes(5, 1);
            var row21 = worksheet.Cells[2, 1].RichText.Add("Bảng tổng hợp đánh giá thực hiện công tác an toàn vệ sinh lao động" + "\r\n");
            row21.Bold = true;
            var row22 = worksheet.Cells[2, 1].RichText.Add("(từ ngày " + sd + " đến ngày " + ed + ")\r\n");
            row22.Bold = true;

            worksheet.Row(1).Style.Font.Bold = true;
            worksheet.Row(2).Style.Font.Bold = true;
            worksheet.Row(3).Style.Font.Bold = true;
            worksheet.Row(4).Style.Font.Bold = true;

            worksheet.Column(1).Width = 10;
            //worksheet.Column(1).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            worksheet.Column(2).Width = 50;


            int i = 1;
            foreach (var companyId in companyIds)
            {
                if (i > companyIds.Count()) break;
                else
                {
                    worksheet.Cells[4, 2 + i].Value = companies.FirstOrDefault(c => c.Id == companyId).Name;
                    worksheet.Column(2 + i).Width = 10;
                    worksheet.Column(2 + i).Style.Numberformat.Format = type;
                    worksheet.Column(2 + i).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                }
                i++;
            }


        }

        public bool ApproveSafelyReport(ApproveSafelyReport report)
        {
            var reportExist = _safelyReportRepository.FindById(report.ReportId);
            if (reportExist == null) throw new ServiceException("Báo cáo an toàn vệ sinh không tồn tại");
            if (reportExist.Status == SafelyReportStatus.Approve) throw new ServiceException("Báo cáo an toàn đã được duyệt");
            var projectUser = _projectUserRepository.Find(c => c.ProjectId == reportExist.ProjectId && c.UserId == report.ApproveUser);
            if (projectUser == null) throw new ServiceException("Bạn không thuộc dự án này nên không có quyền");
            reportExist.Status = SafelyReportStatus.Approve;

            return _safelyReportRepository.Update(reportExist);

        }

        public bool RejectSafelyReport(RejectSafelyReport report)
        {
            var reportExist = _safelyReportRepository.FindById(report.ReportId);
            if (reportExist == null) throw new ServiceException("Báo cáo an toàn vệ sinh không tồn tại");
            if (reportExist.Status == SafelyReportStatus.Approve) throw new ServiceException("Báo cáo an toàn đã được duyệt");
            var projectUser = _projectUserRepository.Find(c => c.ProjectId == reportExist.ProjectId && c.UserId == report.RejectUser);
            if (projectUser == null) throw new ServiceException("Bạn không thuộc dự án này nên không có quyền");
            if (string.IsNullOrEmpty(report.Reason)) throw new ServiceException("Yêu cầu nhập lý do từ chối");

            reportExist.Reason = report.Reason;
            reportExist.Status = SafelyReportStatus.Reject;

            return _safelyReportRepository.Update(reportExist);
        }

        public bool SumitSafelyReport(SubmitSafelyReport report)
        {
            var reportExist = _safelyReportRepository.FindById(report.ReportId);
            if (reportExist == null) throw new ServiceException("Báo cáo an toàn vệ sinh không tồn tại");
            if (reportExist.Status == SafelyReportStatus.Approve) throw new ServiceException("Báo cáo an toàn đã được duyệt");
            var projectUser = _projectUserRepository.Find(c => c.ProjectId == reportExist.ProjectId && c.UserId == report.SubmitUser);
            if (projectUser == null) throw new ServiceException("Bạn không thuộc dự án này nên không có quyền");
            if (reportExist.CreatedBy != report.SubmitUser) throw new ServiceException("Bạn không phải là người tạo báo cáo này");
            reportExist.Reason = string.Empty;
            reportExist.Status = SafelyReportStatus.Submit;
            return _safelyReportRepository.Update(reportExist);
        }

        public bool TranslateSafelyImage()
        {
            var safelyReportImageExists = _safelyReportImageRepository.FindAll();
            foreach (var item in safelyReportImageExists)
            {
                if (item.FileId.HasValue)
                {
                    _safelyImageDetailRepository.Insert(new SafelyImageDetail
                    {
                        SafelyImageId = item.Id,
                        SafelyReportId = item.SafelyReportId,
                        Files = JsonConvert.SerializeObject(new List<Guid> { item.FileId.Value })
                    });
                }
            }
            return true;
        }

        public bool TranslateSafelyImageDraft()
        {
            var safelyDraftExists = _safelyDraftRepository.FindAll();
            foreach (var item in safelyDraftExists)
            {
                if (item.FileId.HasValue)
                {
                    _safelyImageDraftDetailRepository.Insert(new SafelyImageDraftDetail
                    {
                        SafelyDraftId = item.Id,
                        Files = JsonConvert.SerializeObject(new List<Guid> { item.FileId.Value })
                    });
                }
            }
            return true;
        }

        public bool DeleteSafelyReportImage(Guid imageId, int imageInfoId)
        {
            var result = false;
            var trans = _safelyReportImageRepository.BeginTransaction();
            if (trans.Connection.State != ConnectionState.Open) trans.Connection.Open();
            try
            {
                var safelyReportImage = _safelyReportImageRepository.FindById(imageInfoId, trans);

                var safelyReportImageDetail = _safelyImageDetailRepository.Find(c => c.SafelyImageId == imageInfoId, trans);
                if (safelyReportImageDetail != null)
                {
                    var deleteFile = false;
                    var deleteFileStorage = false;


                    var file = _fileRepository.FindById(imageId, trans);
                    var storage = _binaryFileStorageRepository.FindById(file.StorageId, trans);

                    deleteFile = _fileRepository.Delete(file, trans);
                    deleteFileStorage = _binaryFileStorageRepository.Delete(storage, trans);

                    if (deleteFile && deleteFileStorage)
                    {
                        var images = JsonConvert.DeserializeObject<List<Guid>>(safelyReportImageDetail.Files);
                        images = images.Except(images.Where(c => c == imageId)).ToList();
                        safelyReportImageDetail.Files = JsonConvert.SerializeObject(images);
                        result = _safelyImageDetailRepository.Update(safelyReportImageDetail, trans);
                    }
                    else result = false;
                }
                else result = false;
                if (!result)
                    trans.Rollback();
            }
            catch (Exception ex)
            {
                trans.Rollback();
                throw ex;
            }
            trans.Commit();
            return result;
        }

        public bool DeleteSafelyReportDraftImage(Guid imageId, int imageInfoId)
        {
            var result = false;
            var trans = _safelyDraftRepository.BeginTransaction();
            if (trans.Connection.State != ConnectionState.Open) trans.Connection.Open();
            try
            {
                var safelyReportImageDraft = _safelyDraftRepository.FindById(imageInfoId, trans);

                var safelyReportImageDraftDetail = _safelyImageDraftDetailRepository.Find(c => c.SafelyDraftId == imageInfoId, trans);
                if (safelyReportImageDraftDetail != null)
                {
                    var deleteFile = false;
                    var deleteFileStorage = false;


                    var file = _fileRepository.FindById(imageId, trans);
                    var storage = _binaryFileStorageRepository.FindById(file.StorageId, trans);

                    deleteFile = _fileRepository.Delete(file, trans);
                    deleteFileStorage = _binaryFileStorageRepository.Delete(storage, trans);

                    if (deleteFile && deleteFileStorage)
                    {
                        var images = JsonConvert.DeserializeObject<List<Guid>>(safelyReportImageDraftDetail.Files);
                        images = images.Except(images.Where(c => c == imageId)).ToList();
                        safelyReportImageDraftDetail.Files = JsonConvert.SerializeObject(images);
                        result = _safelyImageDraftDetailRepository.Update(safelyReportImageDraftDetail, trans);
                    }
                    else result = false;
                }
                else result = false;
                if (!result)
                    trans.Rollback();
            }
            catch (Exception ex)
            {
                trans.Rollback();
                throw ex;
            }
            trans.Commit();
            return result;
        }

        public bool SubmitSafelyReportDraft(IEnumerable<int> imageInfoIds, int userId)
        {
            var result = false;
            if (imageInfoIds.Count() > 0)
            {
                var trans = _safelyDraftRepository.BeginTransaction();
                if (trans.Connection.State != ConnectionState.Open) trans.Connection.Open();
                try
                {
                    var companyIds = new List<int>();

                    var test = _safelyDraftRepository.FindAll(trans);
                    var safelyDrafts = _safelyDraftRepository.FindAll(c => imageInfoIds.Contains(c.Id), trans);
                    var safelyDraftIds = safelyDrafts.Select(c => c.Id);
                    var safelyImageDraftDetails = _safelyImageDraftDetailRepository.FindAll(c => safelyDraftIds.Contains(c.SafelyDraftId), trans);
                    var safelyErrorCollectionDrafts = _safelyErrorCollectionDraftRepository.FindAll(c => safelyDraftIds.Contains(c.SafelyImageDraftId), trans);
                    companyIds = safelyDrafts.Select(c => c.CompanyId).Distinct().ToList();


                    var addReport = new SafelyReport
                    {
                        ProjectId = safelyDrafts.FirstOrDefault().ProjectId,
                        CreatedBy = userId,
                        CreatedDate = DateTime.UtcNow,
                        Status = SafelyReportStatus.Submit
                    };
                    var createReport = _safelyReportRepository.Insert(addReport, trans);
                    if (createReport)
                    {
                        foreach (var companyId in companyIds)
                        {

                            var addFileIds = new List<Guid>();
                            var safelyDraftByCompanies = safelyDrafts.Where(c => c.CompanyId == companyId);
                            if (safelyDraftByCompanies.Count() > 0)
                            {
                                var listViolations = new List<ViolationDetail>();
                                foreach (var safelyDraftByCompany in safelyDraftByCompanies)
                                {
                                    var collectionDraftByCompanies = safelyErrorCollectionDrafts.Where(c => c.SafelyImageDraftId == safelyDraftByCompany.Id);
                                    var imageDraftDetailByCompanies = safelyImageDraftDetails.Where(c => c.SafelyDraftId == safelyDraftByCompany.Id);
                                    if (imageDraftDetailByCompanies.Count() > 0)
                                    {
                                        foreach (var imageDraftDetailByCompany in imageDraftDetailByCompanies)
                                        {
                                            if (!string.IsNullOrEmpty(imageDraftDetailByCompany.Files))
                                            {
                                                var desializes = JsonConvert.DeserializeObject<List<Guid>>(imageDraftDetailByCompany.Files);
                                                addFileIds.AddRange(desializes);
                                            }
                                        }
                                    }
                                    if (collectionDraftByCompanies.Count() > 0)
                                    {
                                        foreach (var item in collectionDraftByCompanies)
                                        {
                                            if (!listViolations.Select(c=>c.Type).Contains(item.Type))
                                            {
                                                listViolations.Add(new ViolationDetail
                                                {
                                                    Type = item.Type,
                                                    Quantity = item.ViolationQuantity.Value
                                                });
                                            }
                                            else
                                            {                                             
                                                listViolations.Where(c => c.Type == item.Type).FirstOrDefault()
                                                    .Quantity += item.ViolationQuantity.Value;
                                            }
                                        }

                                    }
                                }
                                var addReportImage = new SafelyReportImage
                                {
                                    SafelyReportId = addReport.Id,
                                    ProjectId = addReport.ProjectId,
                                    CompanyId = companyId,
                                    Lat = safelyDraftByCompanies.FirstOrDefault().Lat,
                                    Lng = safelyDraftByCompanies.FirstOrDefault().Lng,
                                    Note = string.Join(", ", safelyDraftByCompanies.Select(c => c.Note)),
                                    DayPhotographed = safelyDraftByCompanies.Select(c => c.DayPhotographed).Min()
                                };
                                var resultReportImage = _safelyReportImageRepository.Insert(addReportImage, trans);
                                var addImageDetail = true;
                                if (resultReportImage)
                                {
                                    addImageDetail = _safelyImageDetailRepository.Insert(new SafelyImageDetail
                                    {
                                        SafelyReportId = addReport.Id,
                                        SafelyImageId = addReportImage.Id,
                                        Files = JsonConvert.SerializeObject(addFileIds)
                                    }, trans);
                                }
                                var addError = true;
                                foreach (var item in listViolations)
                                {
                                    addError = _safelyErrorCollectionRepository.Insert(new SafelyErrorCollection
                                    {
                                        SafelyImageId = addReportImage.Id,
                                        ViolationQuantity = item.Quantity,
                                        Type = item.Type
                                    }, trans);
                                    if (!addError) break;
                                }
                                if (resultReportImage && addImageDetail && addError)
                                {
                                    result = true;
                                    foreach (var item in safelyDraftByCompanies)
                                    {
                                        _safelyDraftRepository.Delete(item, trans);
                                        var safelyImageDraftDetailIns = safelyImageDraftDetails.Where(c => c.SafelyDraftId == item.Id);
                                        foreach (var safelyImageDraftDetailIn in safelyImageDraftDetailIns)
                                        {
                                            _safelyImageDraftDetailRepository.Delete(safelyImageDraftDetailIn, trans);
                                        }
                                        var safelyErrorCollectionDraftIns = safelyErrorCollectionDrafts.Where(c => c.SafelyImageDraftId == item.Id);
                                        foreach (var safelyErrorCollectionDraftIn in safelyErrorCollectionDraftIns)
                                        {
                                            _safelyErrorCollectionDraftRepository.Delete(safelyErrorCollectionDraftIn, trans);
                                        }

                                    }
                                }
                                else
                                    result = false;
                                if (!result)
                                    break;
                            }
                        }
                    }

                    if (!result) trans.Rollback();
                }
                catch (Exception ex)
                {
                    trans.Rollback();
                    throw ex;
                }
                trans.Commit();
            }
            return result;
        }
    }
}
