﻿using PCC1.PMS.Domain.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace PCC1.PMS.Domain.Services.SafelyReports
{
    [AttributeUsage(AttributeTargets.All, Inherited = false, AllowMultiple = false)]
    public class ViolationDetailAttribute : Attribute
    {
        private readonly ViolationGroupCode group;
        private readonly string name;

        public ViolationDetailAttribute(ViolationGroupCode group, string name)
        {
            this.group = group;
            this.name = name;
        }
        public virtual string Name
        {
            get { return name; }
        }
        public virtual ViolationGroupCode Group
        {
            get { return group; }
        }
    }

}
