﻿using PCC1.PMS.Domain.Models.ProjectReports;
using PCC1.PMS.Domain.Models.SafelyReports;
using PCC1.PMS.Domain.Models.Statistical;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace PCC1.PMS.Domain.Services.SafelyReports
{
    public interface ISafelyReportService
    {

        ViolationDefinedModel ListViolationDefined();
        
        SearchSafelyReportResult SearchSafelyReport(SearchSafelyReportModel search); 

        (bool, int) UpdateSafelyReport(UpdateSafelyReportModel updateSafely);

        bool DeleteSafelyReport(DeleteSafelyReportModel safelyReportDeleteModel);

        //StatisticalViolationType StatisticalSafelyReport(StatisticalSafelyReportModel statistical);

        SafelyReportInfoModel GetSafelyReportById(int reportId);

        IEnumerable<SafelyDraftModel> ListDraft(int user, int projectId);

        bool UpdateSafelyDraft(UpdateSafelyDraft updateSafelyDraft);

        bool DeleteMultilSafelyDraft(DeleteMultiDraftModel deleteMultiDraft);

        List<SafelyViolationTypeStatistical> StatisticalOneSafelyReport(int safelyReportId);

        StatisticalViolationModel StatisticalSafelyReportInProject(SearchStatisticSafelyReportOneProject searchStatistic);

        StatisticalViolationAllProjectModel StatisticalSafelyReportAllProject(CheckStatisticalWithTime searchStatistic);

        (MemoryStream, string) ExportStatisticalSafelyReportInProject(int projectId, DateTime? startDate, DateTime? endDate);

        MemoryStream ExportStatisticalSafelyReportAllProject(DateTime? startDate, DateTime? endDate);

        bool ApproveSafelyReport(ApproveSafelyReport report);

        bool RejectSafelyReport(RejectSafelyReport report);

        bool SumitSafelyReport(SubmitSafelyReport report);

        bool TranslateSafelyImage();
        bool TranslateSafelyImageDraft();
        bool DeleteSafelyReportImage(Guid imageId, int imageInfoId);
        bool DeleteSafelyReportDraftImage(Guid imageId, int imageInfoId);

        bool SubmitSafelyReportDraft(IEnumerable<int> imageInfoIds, int userId);
    }
}
