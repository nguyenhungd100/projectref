﻿using Microsoft.AspNetCore.Http;
using PCC1.PMS.Domain.Models.ImageLibraries;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace PCC1.PMS.Domain.Services.ImageLibraries
{
    public interface IImageLibraryService
    {
        (byte[], string) ImageLibraryFindById(int id);

        bool AddImageLibrary(IFormFile file);
    }
}
