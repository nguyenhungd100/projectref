﻿using Microsoft.AspNetCore.Http;
using PCC1.PMS.Domain.Entity;
using PCC1.PMS.Domain.Models.Companies;
using PCC1.PMS.Domain.Models.ImageLibraries;
using PCC1.PMS.Domain.Repositories;
using PCC1.PMS.Framework.Utils;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading.Tasks;

namespace PCC1.PMS.Domain.Services.ImageLibraries.Implementation
{
    public class ImageLibraryService : IImageLibraryService
    {
        private readonly IImageLibraryRepository _imageLibraryRepository;

        public ImageLibraryService(IImageLibraryRepository imageLibraryRepository)
        {
            _imageLibraryRepository = imageLibraryRepository;
        }

        public bool AddImageLibrary(IFormFile file)
        {
            var result = false;
            MemoryStream ms = null;
            using (ms = new MemoryStream())
            {
                byte[] fileByteImage = new byte[file.Length];
                file.CopyTo(ms);
                fileByteImage = ms.ToArray();
                result = _imageLibraryRepository.Insert(new ImageLibrary
                {
                    Data = fileByteImage,
                    Name = file.Name,
                    Mime = file.ContentType,
                    CreatedDate = DateTime.UtcNow
                });
            }
            return result;
        }

        public (byte[], string) ImageLibraryFindById(int id)
        {
            var result = _imageLibraryRepository.FindById(id);
            return (result.Data, result.Mime);
        }

    }
}
