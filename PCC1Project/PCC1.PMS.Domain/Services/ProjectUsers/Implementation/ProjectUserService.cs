﻿using PCC1.PMS.Domain.Entity;
using PCC1.PMS.Domain.Models.ProjectUsers;
using PCC1.PMS.Domain.Repositories;
using PCC1.PMS.Framework.Utils;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace PCC1.PMS.Domain.Services.ProjectUsers
{
    public class ProjectUserService : IProjectUserService
    {
        private readonly IProjectUserRepository _projectUserRepository;
        public ProjectUserService(IProjectUserRepository projectUserRepository)
        {
            _projectUserRepository = projectUserRepository;
        }
        
    }
}
