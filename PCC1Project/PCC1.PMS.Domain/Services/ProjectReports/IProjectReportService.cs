﻿using Microsoft.AspNetCore.Http;
using PCC1.PMS.Domain.Entity;
using PCC1.PMS.Domain.Models.File;
using PCC1.PMS.Domain.Models.MediaStorage;
using PCC1.PMS.Domain.Models.ProjectReports;
using PCC1.PMS.Domain.Models.ProjectReports.DefineMachineryManpower;
using PCC1.PMS.Domain.Models.ProjectReports.StatisticsHumanResource;
using PCC1.PMS.Domain.Models.Projects;
using PCC1.PMS.Domain.Models.Workitems;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text;
using System.Threading.Tasks;

namespace PCC1.PMS.Domain.Services.ProjectReports
{
    public interface IProjectReportService
    {
        Task<IEnumerable<ProjectReportModel>> GetListProjectReport(int projectId);

        ProjectReportResultModel GetReport(int reportId);

        SearchProjectReportsResult Search(SearchProjectReportsModel projectReportSearchModel, int? userCurrent);

        MachineryManpowerDefinedModel MachineryManpowerDefined();

        int SaveReportAll(SaveReportModel createReportModel, int userId);

        bool ApproveReport(ApproveReportModel approveReportModel);

        bool DeleteReport(DeleteReportModel deleteReportModel, bool isCommandLead);

        //bool SaveReportPlan(IEnumerable<SaveProjectReportPlanModel> reportPlans, int userId);
        bool DeleteReportPlan(DeleteProjectReportPlanModel model);

        // bool SaveReportWorkItem(IEnumerable<SaveWorkitemReportModel> models, int userId);
        bool DeleteReportItem(DeleteWorkitemReportModel model);


        bool CheckRightpersonReports(int userId, int reportId);

        bool RejectReport(RejectReportModel rejectReportModel);

        ProgressResult CheckProgressProjectExpected(CheckProjectProgressModel checkProjectProgress);

        ProgressResult CheckProgressPlanExpected(Guid planId);

        (bool, List<int>) CreateReportImage(MediaModel mediaModel, int userId);


        bool SubmitReport(SendToSiteManagerModel model, int userId);

        IEnumerable<ProjectReportModel> ListReportForSiteManager();

        NumberProjectProgressResult CheckAllProjectProgress();

        (IEnumerable<WorkitemPerMonthModel>,IEnumerable<Workitem>) WorkitemReportPerMonth(int year);

        IEnumerable<WorkitemPerQuarterModel> WorkitemReportPerQuarter(int year);

        IEnumerable<BinaryFileStorageModel> ListReportImageByReportId(int projectReportId);

        IEnumerable<BinaryFileStorageModel> ListImageByWorkitemReportId(int workitemReportId);

        int CloneReport(CloneReportModel reportModel);



        /// <summary>
        /// Test viết chữ lên ảnh
        /// </summary>
        /// <param name="image"></param>
        /// <returns></returns>
        byte[] WriteTextImage(string image);

        StatisticsHumanResourceModel StatisticalHumanResource(CheckStatiscalHumanResource check);

        AutoFillReportWithCompanyModel AuToFillReportWithCompany(string hashId, int? companyId);

        IEnumerable<ShowReportOnDardboardModel> ShowReportInDardboard(int userId);
        bool MarkedReadReport(int reportId, int userId);


        string JoinStringPlaceCollection(params string[] data);
        





        //bool UpdatePlanPercentage(ProjectPlanModel projectPlanModel);


    }
}
