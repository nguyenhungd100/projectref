﻿using MicroOrm.Dapper.Repositories;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore.Metadata.Internal;
using Newtonsoft.Json;
using PCC1.PMS.Domain.Entity;
using PCC1.PMS.Domain.Enums;
using PCC1.PMS.Domain.Models.Companies;
using PCC1.PMS.Domain.Models.File;
using PCC1.PMS.Domain.Models.MediaStorage;
using PCC1.PMS.Domain.Models.Notification;
using PCC1.PMS.Domain.Models.ProjectReports;
using PCC1.PMS.Domain.Models.ProjectReports.AutoFillReportWithCompany;
using PCC1.PMS.Domain.Models.ProjectReports.DefineMachineryManpower;
using PCC1.PMS.Domain.Models.ProjectReports.StatisticsHumanResource;
using PCC1.PMS.Domain.Models.Projects;
using PCC1.PMS.Domain.Models.Workitems;
using PCC1.PMS.Domain.Repositories;
using PCC1.PMS.Domain.Services.Common;
using PCC1.PMS.Domain.Services.FileStorages;
using PCC1.PMS.Domain.Services.Projects;
using PCC1.PMS.Domain.Services.Roles;
using PCC1.PMS.Domain.Services.WorkItems;
using PCC1.PMS.Framework.Extensions;
using PCC1.PMS.Framework.Utils;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using System.Net.Mail;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using File = PCC1.PMS.Domain.Entity.File;

namespace PCC1.PMS.Domain.Services.ProjectReports
{
    public class ProjectReportService : IProjectReportService
    {
        private readonly IHashidService _hashidService;
        private readonly IProjectReportRepository _projectReportRepository;
        private readonly IDapperRepository<ProjectReportPlan> _reportPlanRepository;
        private readonly IDapperRepository<ProjectReportWorkitem> _reportWorkitemRepository;
        private readonly IDapperRepository<ProjectPlan> _projectPlanRepository;
        private readonly IDapperRepository<ProjectUser> _projectUserRepository;
        private readonly IDapperRepository<Project> _projectRepository;
        private readonly IDapperRepository<Workitem> _workItemRepository;
        private readonly IFileStorageService _fileStorageService;
        private readonly IDapperRepository<User> _userRepository;
        private readonly IDapperRepository<Company> _companyRepository;
        private readonly IDapperRepository<WorkitemCompany> _workItemCompanyRepository;
        private readonly IDapperRepository<BinaryFileStorage> _binaryFileStorageRepository;
        private readonly IDapperRepository<Entity.File> _fileRepository;
        private readonly IDapperRepository<ReportImage> _reportImageRepository;
        private readonly IDapperRepository<ProjectCompany> _projectCompanyRepository;
        private readonly IDapperRepository<SafelyReportImage> _safelyReportImageRepository;
        private readonly ISafelyReportRepository _safelyReportRepository;
        private readonly IDapperRepository<EstimationPlan> _estimateRepository;
        private readonly IDapperRepository<MachineryManpowerReport> _machineryManpowerReportRepository;
        private readonly INotificationService _notificationService;
        private readonly IWorkItemService _workItemService;

        public ProjectReportService(
            IHashidService hashidService,
            IProjectReportRepository projectReportRepository,
            IDapperRepository<ProjectReportPlan> projectReportPlanRepository,
            IDapperRepository<ProjectReportWorkitem> projectReportWorkitemRepository,
            IDapperRepository<ProjectPlan> projectPlanRepository,
            IDapperRepository<ProjectUser> projectUserRepository,
            IFileStorageService fileStorageService,
            IDapperRepository<Project> projectRepository,
            IDapperRepository<Workitem> workItemRepository,
            IDapperRepository<User> userRepository,
            IDapperRepository<Company> companyRepository,
            IDapperRepository<WorkitemCompany> workItemCompanyRepository,
            IDapperRepository<BinaryFileStorage> binaryFileStorageRepository,
            IDapperRepository<Entity.File> fileRepository,
            IDapperRepository<ReportImage> reportImageRepository,
            IDapperRepository<ProjectCompany> projectCompanyRepository,
            IDapperRepository<SafelyReportImage> safelyReportImageRepository,
            ISafelyReportRepository safelyReportRepository,
            IDapperRepository<EstimationPlan> estimateRepository,
            IDapperRepository<MachineryManpowerReport> machineryManpowerReportRepository,
            INotificationService notificationService,
            IWorkItemService workItemService)
        {
            _hashidService = hashidService;
            _projectReportRepository = projectReportRepository;
            _reportPlanRepository = projectReportPlanRepository;
            _reportWorkitemRepository = projectReportWorkitemRepository;
            _projectPlanRepository = projectPlanRepository;
            _projectUserRepository = projectUserRepository;
            _fileStorageService = fileStorageService;
            _projectRepository = projectRepository;
            _workItemRepository = workItemRepository;
            _userRepository = userRepository;
            _companyRepository = companyRepository;
            _workItemCompanyRepository = workItemCompanyRepository;
            _binaryFileStorageRepository = binaryFileStorageRepository;
            _fileRepository = fileRepository;
            _reportImageRepository = reportImageRepository;
            _projectCompanyRepository = projectCompanyRepository;
            _safelyReportImageRepository = safelyReportImageRepository;
            _safelyReportRepository = safelyReportRepository;
            _estimateRepository = estimateRepository;
            _machineryManpowerReportRepository = machineryManpowerReportRepository;
            _notificationService = notificationService;
            _workItemService = workItemService;
        }

        public async Task<IEnumerable<ProjectReportModel>> GetListProjectReport(int projectId)
        {
            var entity = await _projectReportRepository.FindAllAsync(
                c => c.ProjectId == projectId);
            return entity.ToList().CloneToListModels<ProjectReport, ProjectReportModel>();
        }

        public string JoinStringPlaceCollection(params string[] data)
        {
            StringBuilder builder = new StringBuilder();

            var numberStrResult = new List<string>();

            int curr = 0;
            foreach (var item in data)
            {
                if (!string.IsNullOrEmpty(item))
                {
                    var str = item.Replace(" ", "");
                    str = str.TrimStart(',');
                    str = str.TrimEnd(',');

                    List<string> numberStr = str.ToString().Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries).ToList();
                    numberStrResult.AddRange(numberStr);
                    ++curr;
                }
            }
            numberStrResult = numberStrResult.Distinct().ToList();
            return string.Join(',', numberStrResult);
        }



        public static string DifferenceBetweenTheTwoPlaces(string place1, string place2)
        {
            var result = new List<string>();
            if (string.IsNullOrEmpty(place1))
            {
                List<string> str1Split = place1.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries).ToList();
                if (string.IsNullOrEmpty(place2))
                {
                    List<string> str2Split = place2.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries).ToList();

                    foreach (var str in str1Split)
                    {
                        if (!str2Split.Contains(str))
                        {
                            result.Add(str);
                        }
                    }
                }
            }
            return string.Join(',', result);
        }


        private IEnumerable<CollectCategoryWorkitemInReportModel> GetCollectCategoryWorkitemInReports(ProjectReport report, IEnumerable<WorkitemCompany> workitemCompanies, 
            IEnumerable<ReportWorkItemModel> reportWorkitems, List<WorkitemWithHeadingModel> workitemDivides)
        {
            var result = new List<CollectCategoryWorkitemInReportModel>();

            var companyIds = workitemCompanies.Select(c => c.CompanyId).Distinct();
                  

            foreach (var workitemDivide in workitemDivides)
            {
                var listIds = new List<int>();
                var workitemSmallInDivides = workitemDivide.WorkItemSmallerModels;
                if (workitemSmallInDivides.Count() > 0)
                {
                    listIds.AddRange(workitemSmallInDivides.Select(c => c.Id));
                }
                listIds.Add(workitemDivide.WorkitemCategory.Id);

                var workitemCompanyIns = workitemCompanies.Where(c => listIds.Contains(c.WorkItemId));

                var completePlace = new List<string>();
                var underPlace = new List<string>();

                foreach (var id in listIds)
                {
                    foreach (var companyId in companyIds)
                    {
                        var checkWorkitemCompanyIn = reportWorkitems.FirstOrDefault(c => c.CompanyId == companyId && c.WorkitemId == id);// nếu có vị trí trong báo cáo thì lấy vị trí trong báo cáo
                        if (checkWorkitemCompanyIn != null)
                        {
                            if (!string.IsNullOrEmpty(checkWorkitemCompanyIn.CompletePlace))
                            {
                                completePlace.Add(checkWorkitemCompanyIn.CompletePlace);
                            }
                            if (!string.IsNullOrEmpty(checkWorkitemCompanyIn.UnderPlace))
                            {
                                underPlace.Add(checkWorkitemCompanyIn.UnderPlace);
                            }
                        }
                        else // lấy trong phân giao
                        {
                            var checkAssignment = workitemCompanies.FirstOrDefault(c => c.CompanyId == companyId && c.WorkItemId == id);
                            if (checkAssignment != null)
                            {
                                if (!string.IsNullOrEmpty(checkAssignment.CompletePlace))
                                {
                                    completePlace.Add(checkAssignment.CompletePlace);
                                }
                                if (!string.IsNullOrEmpty(checkAssignment.UnderPlace))
                                {
                                    underPlace.Add(checkAssignment.UnderPlace);
                                }
                            }
                        }
                    }

                }
                var reportWorkitemIns = reportWorkitems.Where(c => listIds.Contains(c.WorkitemId));
                var add = new CollectCategoryWorkitemInReportModel
                {
                    STT = workitemDivide.WorkitemCategory.STT,
                    WorkitemCategoryName = workitemDivide.WorkitemCategory.Name,
                    CalculationUnit = workitemDivide.CalculationUnit
                };

                var beforeComplete = workitemCompanies.Where(c => listIds.Contains(c.WorkItemId));
                if (report.Status == ProjectReportStatus.Approved)
                {
                    if (workitemCompanyIns.Count() > 0)
                    {
                        add.CumulativeNumber = beforeComplete.Sum(c => c.CompletedNumber.Value);
                    }
                }
                else
                {
                    decimal before = 0;
                    decimal completeIn = 0;
                    if (reportWorkitemIns.Count() > 0)
                    {
                        completeIn = reportWorkitemIns.Sum(c => c.CompletedNumber);
                    }
                    if (beforeComplete.Count() > 0)
                    {
                        before = beforeComplete.Sum(c => c.CompletedNumber.Value);
                    }
                    add.CumulativeNumber = before + completeIn;
                }
                if (completePlace.Count() > 0 || underPlace.Count() > 0)
                {

                    add.CompletePlaceCollect = JoinStringPlaceCollection(completePlace.ToArray());

                    add.CompletePlaceCountCollect = StringUtil.CountItemWhenSeperate(JoinStringPlaceCollection(completePlace.ToArray()));

                    add.UnderPlaceCollect = JoinStringPlaceCollection(underPlace.ToArray());

                    add.UnderPlaceCountCollect = StringUtil.CountItemWhenSeperate(JoinStringPlaceCollection(underPlace.ToArray()));

                    add.PlaceType = workitemDivide.PlaceType;

                    result.Add(add);
                }

                


                //if (workitemDivide.WorkItemSmallerModels.Any(c => !string.IsNullOrEmpty(c.PlaceCollection) && c.CompletedNumberTotal > 0))
                //{

                //    var completePlace = workitemCompanies.Where(c => listIds.Contains(c.WorkItemId)).Select(a => a.CompletePlace).ToList().Append(workitemDivide.WorkitemCategory.PlaceCollection).ToList();

                //    var completePlaceReport = reportWorkitems.Select(c => c.CompletePlace).ToList();

                //    completePlace.AddRange(completePlaceReport);

                //    completePlace.ToArray();

                //    var underPlace = workitemCompanies.Where(c => listIds.Contains(c.WorkItemId)).Select(a => a.UnderPlace).ToList();

                //    var underPlaceReport = reportWorkitems.Select(c => c.UnderPlace).ToList();

                //    underPlace.AddRange(underPlaceReport);

                //    underPlace.ToArray();

                //    var add = new CollectCategoryWorkitemInReportModel
                //    {
                //        STT = workitemDivide.WorkitemCategory.STT,

                //        WorkitemCategoryName = workitemDivide.WorkitemCategory.Name,

                //        CompletePlaceCollect = JoinStringPlaceCollection(completePlace.ToArray()),

                //        CompletePlaceCountCollect = StringUtil.CountItemWhenSeperate(JoinStringPlaceCollection(completePlace.ToArray())),

                //        UnderPlaceCollect = JoinStringPlaceCollection(underPlace.ToArray()),

                //        UnderPlaceCountCollect = StringUtil.CountItemWhenSeperate(JoinStringPlaceCollection(underPlace.ToArray()))

                //    };
                //    //var x = add.CompletePlaceCollect;
                //    result.Add(add);
                //}
            }

            return result;
        }


        private IEnumerable<CollectWorkitemỈnReportWithCompany> GetCollectWorkitemỈnReportWithCompanies(List<WorkitemCompany> workitemCompanyExists,
            IEnumerable<Company> companies, IEnumerable<ReportWorkItemModel> reportWorkitems, List<WorkitemWithHeadingModel> workitemDivides, ProjectReport projectReport)
        {
            var result = new List<CollectWorkitemỈnReportWithCompany>();


            var companyDistinctIds = workitemCompanyExists/*.Where(a => a.CompletedNumber > 0)*/.Select(c => c.CompanyId).Distinct();

            var workitemDistinctIds = workitemCompanyExists/*.Where(a => a.CompletedNumber > 0)*/.Select(c => c.WorkItemId).Distinct();

            foreach (var companyId in companyDistinctIds)
            {
                var company = companies.FirstOrDefault(c => c.Id == companyId);
                var underPlace = new List<string>();
                var completePlace = new List<string>();

                var Note = reportWorkitems.Where(c => c.CompanyId == companyId && !string.IsNullOrEmpty(c.Content)).Select(a => a.Content).ToList();

                var addWorkitemWithCategories = new List<CollectWorkitemCategoryInCompany>();

                void ModifyCategoriesCompletePlaceWithCompany(int workitemId, string completePlaceInput)
                {
                    foreach (var divide in workitemDivides)
                    {
                        if (divide.WorkitemCategory.Id == workitemId || divide.WorkItemSmallerModels.Select(c => c.Id).Contains(workitemId))
                        {
                            if (addWorkitemWithCategories.Any(c => c.WorkitemCategoryId == divide.WorkitemCategory.Id))
                            {
                                var exist = addWorkitemWithCategories.FirstOrDefault(c => c.WorkitemCategoryId == divide.WorkitemCategory.Id);
                                exist.PlaceType = divide.PlaceType;
                                exist.CalculationUnit = divide.CalculationUnit;
                                exist.AddCategoryCompletePlace.Add(completePlaceInput);
                            }
                            else
                            {
                                addWorkitemWithCategories.Add(new CollectWorkitemCategoryInCompany
                                {
                                    STT = divide.WorkitemCategory.STT,
                                    WorkitemCategoryId = divide.WorkitemCategory.Id,
                                    WorkitemCategoryName = divide.WorkitemCategory.Name,
                                    AddCategoryCompletePlace = new List<string> { completePlaceInput },
                                    PlaceType = divide.PlaceType,
                                    CalculationUnit = divide.CalculationUnit
                                });
                            }
                        }
                    }
                }

                void ModifyCategoriesUnderPlaceWithCompany(int workitemId, string undePlaceInput)
                {
                    foreach (var divide in workitemDivides)
                    {
                        if (divide.WorkitemCategory.Id == workitemId || divide.WorkItemSmallerModels.Select(c => c.Id).Contains(workitemId))
                        {
                            if (addWorkitemWithCategories.Any(c => c.WorkitemCategoryId == divide.WorkitemCategory.Id))
                            {
                                var exist = addWorkitemWithCategories.FirstOrDefault(c => c.WorkitemCategoryId == divide.WorkitemCategory.Id);
                                exist.PlaceType = divide.PlaceType;
                                exist.CalculationUnit = divide.CalculationUnit;
                                exist.AddCategoryUnderPlace.Add(undePlaceInput);
                            }
                            else
                            {
                                addWorkitemWithCategories.Add(new CollectWorkitemCategoryInCompany
                                {
                                    STT = divide.WorkitemCategory.STT,
                                    WorkitemCategoryId = divide.WorkitemCategory.Id,
                                    WorkitemCategoryName = divide.WorkitemCategory.Name,
                                    AddCategoryUnderPlace = new List<string> { undePlaceInput },
                                    PlaceType = divide.PlaceType,
                                    CalculationUnit = divide.CalculationUnit
                                });
                            }
                        }
                    }
                }
                
                foreach (var workitemId in workitemDistinctIds)
                {
                    var checkConditionIn = reportWorkitems.FirstOrDefault(c => c.WorkitemId == workitemId && c.CompanyId == companyId);
                    if (checkConditionIn != null) // tồn tại hạng mục phân giao công ty trong báo cáo thì lấy trong báo cáo           
                    {
                        if (!string.IsNullOrEmpty(checkConditionIn.CompletePlace))
                        {
                            completePlace.Add(checkConditionIn.CompletePlace);
                            ModifyCategoriesCompletePlaceWithCompany(workitemId, checkConditionIn.CompletePlace);
                        }

                        if (!string.IsNullOrEmpty(checkConditionIn.UnderPlace))
                        {
                            underPlace.Add(checkConditionIn.UnderPlace);
                            ModifyCategoriesUnderPlaceWithCompany(workitemId, checkConditionIn.UnderPlace);
                        }
                    }
                    else // không có trong báo cáo lấy trong lần duyệt báo cáo trước
                    {
                        var checkAssignment = workitemCompanyExists.FirstOrDefault(c => c.WorkItemId == workitemId && c.CompanyId == companyId);
                        if (checkAssignment != null)
                        {
                            if (!string.IsNullOrEmpty(checkAssignment.CompletePlace))
                            {
                                completePlace.Add(checkAssignment.CompletePlace);
                                ModifyCategoriesCompletePlaceWithCompany(workitemId, checkAssignment.CompletePlace);
                            }
                            if (!string.IsNullOrEmpty(checkAssignment.UnderPlace))
                            {
                                underPlace.Add(checkAssignment.UnderPlace);
                                ModifyCategoriesUnderPlaceWithCompany(workitemId, checkAssignment.UnderPlace);
                            }
                        }
                    }
                }
                if (underPlace.Count() > 0)
                    underPlace.Reverse();
                if (completePlace.Count() > 0)
                    completePlace.Reverse();

               

                if (underPlace.Count() > 0 || completePlace.Count() > 0)
                {
                    foreach (var addCategory in addWorkitemWithCategories)
                    {
                        var listNoteIn = new List<string>();
                        addCategory.CategoryCompletePlace = JoinStringPlaceCollection(addCategory.AddCategoryCompletePlace.ToArray());

                        addCategory.CategoryCompletePlaceCount = StringUtil.CountItemWhenSeperate(addCategory.CategoryCompletePlace);

                        addCategory.CategoryUnderPlace = JoinStringPlaceCollection(addCategory.AddCategoryUnderPlace.ToArray());

                        addCategory.CategoryUnderPlaceCount = StringUtil.CountItemWhenSeperate(addCategory.CategoryUnderPlace);


                        var category = workitemDivides.FirstOrDefault(c => c.WorkitemCategory.Id == addCategory.WorkitemCategoryId);
                        var categoryId = category.WorkitemCategory.Id;
                        var workitemSmallerIds = category.WorkItemSmallerModels.Select(c => c.Id);
                        var workitemIds = new List<int>();
                        workitemIds.Add(categoryId);
                        if (workitemSmallerIds.Count()>0)
                        {
                            workitemIds.AddRange(workitemSmallerIds);
                        }

                        decimal cumulate = 0;
                        decimal completeIn = 0;
                        var workitemCompanyAssigns = workitemCompanyExists.Where(c => c.CompanyId == companyId && workitemIds.Contains(c.WorkItemId)); // Tất cả hạng mục trong đầu mục lớn phân giao cho 1 công ty
                        if (workitemCompanyAssigns.Count()>0)
                        {
                            workitemCompanyAssigns.ToList().ForEach(w =>
                            {
                                if (w.CompletedNumber.HasValue)
                                {
                                    cumulate = cumulate + w.CompletedNumber.Value;
                                }
                            });

                            if (projectReport.Status == ProjectReportStatus.Approved)
                            {
                                addCategory.CumulativeNumber = cumulate;
                            }
                            else
                            {
                                var workitemInReportWithCompany = reportWorkitems.Where(c => c.CompanyId == companyId && workitemIds.Contains(c.WorkitemId));
                                completeIn = workitemInReportWithCompany.Sum(c => c.CompletedNumber);
                                addCategory.CumulativeNumber = cumulate + completeIn;
                            }
                        }
                        var reportWorkitemInCompany = reportWorkitems.Where(c => workitemIds.Contains(c.WorkitemId) && c.CompanyId == companyId);
                        foreach (var item in reportWorkitemInCompany)
                        {
                            addCategory.Note.Add(item.Content);
                        }
                    }

                    var collectWorkitemWithCompany = new CollectWorkitemỈnReportWithCompany
                    {
                        Note = Note,
                        CompanyId = company.Id,
                        CompanyName = company.Name,
                        CompletePlaceWithCompany = JoinStringPlaceCollection(completePlace.ToArray()),
                        CompletePlaceCountWithCompany = StringUtil.CountItemWhenSeperate(JoinStringPlaceCollection(completePlace.ToArray())),
                        UnderPlaceWithCompany = JoinStringPlaceCollection(underPlace.ToArray()),
                        UnderPlaceCountWithCompany = StringUtil.CountItemWhenSeperate(JoinStringPlaceCollection(underPlace.ToArray())),
                        WorkitemCategoryInCompanies = addWorkitemWithCategories
                    };

                    result.Add(collectWorkitemWithCompany);
                }
            }


            //foreach (var item in companyDistinctIds)
            //{
            //    var company = companies.FirstOrDefault(c => c.Id == item);
            //    result.Add(new CollectWorkitemỈnReportWithCompany
            //    {
            //        CompanyId = company.Id,
            //        CompanyName = company.Name,
            //        //CompletePlaceWithCompany = JoinStringPlaceCollection(workitemCompanyExists.Where(c => c.CompanyId == item).Select(c => c.CompletePlace).ToArray()),

            //        //CompletePlaceCountWithCompany = StringUtil.CountItemWhenSeperate(JoinStringPlaceCollection(workitemCompanyExists.Where(c => c.CompanyId == item).Select(c => c.CompletePlace).ToArray())),

            //        //UnderPlaceWithCompany = JoinStringPlaceCollection(workitemCompanyExists.Where(c => c.CompanyId == item).Select(c => c.UnderPlace).ToArray()),

            //        //UnderPlaceCountWithCompany = StringUtil.CountItemWhenSeperate(JoinStringPlaceCollection(workitemCompanyExists.Where(c => c.CompanyId == item).Select(c => c.UnderPlace).ToArray())),

            //        Note = reportWorkitems.Where(c => c.CompanyId == item && !string.IsNullOrEmpty(c.Content)).Select(a => a.Content).ToList(),

            //        CompletePlaceWithCompany = JoinStringPlaceCollection(reportWorkitems.Where(c => c.CompanyId == item).Select(c => c.CompletePlace).ToArray()),

            //        CompletePlaceCountWithCompany = StringUtil.CountItemWhenSeperate(JoinStringPlaceCollection(reportWorkitems.Where(c => c.CompanyId == item).Select(c => c.CompletePlace).ToArray())),

            //        UnderPlaceWithCompany = JoinStringPlaceCollection(reportWorkitems.Where(c => c.CompanyId == item).Select(c => c.UnderPlace).ToArray()),

            //        UnderPlaceCountWithCompany = StringUtil.CountItemWhenSeperate(JoinStringPlaceCollection(reportWorkitems.Where(c => c.CompanyId == item).Select(c => c.UnderPlace).ToArray()))
            //    });
            //}


            return result;
        }

        public ProjectReportResultModel GetReport(int reportId)
        {

            var projectReport = _projectReportRepository.FindById(reportId);

            var reportPlanExists = _reportPlanRepository.FindAll(c => c.ProjectReportId == reportId);
            var reportworkitemExists = _reportWorkitemRepository.FindAll(c => c.ProjectReportId == reportId);

            var reportModel = projectReport.CloneToModel<ProjectReport, ProjectReportResultModel>();
            var reportWorkitems = new List<ReportWorkItemModel>();
            var reportPlans = new List<ReportPlanModel>();
            var collectWorkitems = new List<CollectNumberInReportWorkitemModel>();

            var plans = _projectPlanRepository.FindAll(c => c.ProjectId == projectReport.ProjectId);

            foreach (var item in reportPlanExists)
            {
                var projectPlan = plans.FirstOrDefault(c => c.Id == item.PlanId);
                reportPlans.Add(new ReportPlanModel()
                {
                    Id = item.Id,
                    ProjectReportId = item.ProjectReportId,
                    PlanId = item.PlanId,
                    CompletedPercentage = item.CompletedPercentage,
                    PercentageCurrent = projectPlan.CompletedPercentage,
                    Title = projectPlan.Title
                });
                reportModel.ReportPlans = reportPlans;
            }

            //var workItemRes = workitems.Where(i => i.ProjectReportId == reportModel.Id);

            var projects = _projectRepository.FindAll();
            var projectIds = projects.Select(c => c.Id);
            var workitems = _workItemRepository.FindAll(c => c.ProjectId == projectReport.ProjectId);
            var projectCompanies = _projectCompanyRepository.FindAll(c => c.ProjectId == projectReport.ProjectId);
            var checkCompanyIds = projectCompanies.Select(a => a.CompanyId);
            var companies = _companyRepository.FindAll(c => checkCompanyIds.Contains(c.Id));
            var companyIds = companies.Select(c => c.Id);
            var workitemCompanyExists = new List<WorkitemCompany>();
            if (workitems.Count() > 0)
            {
                var workitemIds = workitems.Select(c => c.Id);
                workitemCompanyExists = _workItemCompanyRepository.FindAll(c => workitemIds.Contains(c.WorkItemId)
                   && companyIds.Contains(c.CompanyId)).ToList();

                IEnumerable<int> reportworkitemExistIds = new List<int>();
                var reportImageExists = new List<ReportImage>();
                if (reportworkitemExists.Count() > 0)
                {
                    reportworkitemExistIds = reportworkitemExists.Select(c => c.Id);
                    reportImageExists = _reportImageRepository.FindAll(c => reportworkitemExistIds.Contains(c.ReportWorkitemId)).ToList();
                }

                var tryUnderPlaceCollect = new List<string>();




                foreach (var item in reportworkitemExists)
                {
                    var workitem = workitems.FirstOrDefault(c => c.Id == item.WorkitemId);
                    var company = companies.FirstOrDefault(c => c.Id == item.CompanyId);
                    var assignment = workitemCompanyExists.FirstOrDefault(c => c.WorkItemId == workitem.Id && c.CompanyId == company.Id);
                    var images = reportImageExists.Where(ri => ri.ReportWorkitemId == item.Id);

                    reportWorkitems.Add(new ReportWorkItemModel()
                    {
                        Id = item.Id,
                        ProjectReportId = item.ProjectReportId,
                        WorkitemId = item.WorkitemId,
                        CompletedNumber = item.CompletedNumber,
                        NumberAccumulatedWithCompany = workitemCompanyExists.FirstOrDefault(c => c.WorkItemId == item.WorkitemId
                            && c.CompanyId == item.CompanyId).CompletedNumber.Value,
                        CompletedNumberCurrent = workitem.CompletedNumberTotal,
                        CompletePlace = item.CompletePlace,
                        CompletePlaceCount = StringUtil.CountItemWhenSeperate(item.CompletePlace),
                        UnderPlace = item.UnderPlace,
                        UnderPlaceCount = StringUtil.CountItemWhenSeperate(item.UnderPlace),

                        CompanyId = item.CompanyId,

                        CompanyName = company.Name,
                        Name = workitem.Name,
                        CalculationUnit = workitem.CalculationUnit,
                        Content = item.Content,
                        Quantity = workitem.Quantity.Value,
                        AssignedNumber = assignment.AssignedNumber,
                        ApprovedCompletedNumber = assignment.CompletedNumber,
                        Images = from p in images
                                 select new WorkitemImageResultModel
                                 {
                                     Image = p.FileId,
                                     Lat = p.Lat,
                                     Lng = p.Lng,
                                     Message = p.Message
                                 },
                        PlaceType = item.PlaceType
                    });

                    tryUnderPlaceCollect.Add(item.UnderPlace);

                    reportModel.ReportWorkitems = reportWorkitems;

                    if (reportModel.CollectNumbers.Any(c => c.WorkitemId == item.WorkitemId))
                    {
                        var updateCollect = reportModel.CollectNumbers.FirstOrDefault(c => c.WorkitemId == item.WorkitemId);

                        // cần sửa
                        if (!string.IsNullOrEmpty(item.UnderPlace))
                        {
                            updateCollect.CompletePlaceTotal += ("," + item.CompletePlace);
                            updateCollect.UnderPlaceTotal += ("," + item.UnderPlace);
                        }

                    }
                    else
                    {
                        var addCollect = new CollectNumberInReportWorkitemModel
                        {
                            WorkitemId = item.WorkitemId,
                            CompletedNumberTotal = workitem.CompletedNumberTotal.Value,
                            CompletePlaceTotal = item.CompletePlace,
                            UnderPlaceTotal = item.UnderPlace

                        };

                        collectWorkitems.Add(addCollect);
                    }

                    reportModel.CollectNumbers = collectWorkitems;
                }

                if (tryUnderPlaceCollect.Count() > 0)
                {
                    reportModel.UnderPlaceCollectInReport = JoinStringPlaceCollection(tryUnderPlaceCollect.ToArray());
                    reportModel.UnderPlaceCountInReport = StringUtil.CountItemWhenSeperate(reportModel.UnderPlaceCollectInReport);
                }

            }
            reportModel.ProjectName = projects.FirstOrDefault(c => c.Id == reportModel.ProjectId).Name;

            var user = _userRepository.FindById(reportModel.CreatedBy);
            reportModel.Email = (user.Email = user.Email ?? null);
            reportModel.Name = (user.FullName = user.FullName ?? null);
            reportModel.ProjectHashId = _hashidService.Encode(reportModel.ProjectId);

            // Số nhân sự máy móc theo từng công ty
            var companyInMachineryData = _machineryManpowerReportRepository.FindAll(c => c.ReportId == reportId);
            if (companyInMachineryData.Count() > 0)
            {
                var companyInReport = companies.Where(c => companyInMachineryData.Select(a => a.CompanyId).Distinct().Contains(c.Id));
                var reportMachines = new List<MachineryManpowerDefinedModel>();
                var machineryInReports = _machineryManpowerReportRepository.FindAll(c => c.ReportId == reportId);

                var collects = new MachineryManpowerCollectModel();
                foreach (var company in companyInReport)
                {
                    var reportMachine = new MachineryManpowerDefinedModel();
                    reportMachine.CompanyId = company.Id;
                    reportMachine.CompanyName = company.Name;
                    var machineryManpowers = machineryInReports.Where(c => c.CompanyId == company.Id);
                    machineryManpowers.ToList().ForEach(m =>
                    {
                        var attr = m.Type.GetAttributeOfType<MachineryManpowerDetailAttribute>();
                        if (attr.Group == MachineryManpowerGroup.Manpower)
                        {
                            reportMachine.ManpowerDefineds.Add(new ManpowerDefined
                            {
                                Code = m.Type,
                                Title = m.Title,
                                Quantity = m.Quantity ?? 0
                            });
                            var checkManpower = collects.ManpowerCollects.FirstOrDefault(c => c.Code == m.Type);
                            if (checkManpower == null)
                            {
                                collects.ManpowerCollects.Add(new ManpowerDefined
                                {
                                    Code = m.Type,
                                    Title = m.Title,
                                    Quantity = m.Quantity ?? 0
                                });
                            }
                            else
                            {
                                checkManpower.Quantity += m.Quantity ?? 0;
                            }

                        }

                        if (attr.Group == MachineryManpowerGroup.Machine)
                        {
                            reportMachine.MachineryDefineds.Add(new MachineryDefined
                            {
                                Code = m.Type,
                                Title = attr.Name,
                                Quantity = m.Quantity ?? 0
                            });

                            var checkMachinery = collects.MachineryCollects.FirstOrDefault(c => c.Code == m.Type);
                            if (checkMachinery == null)
                            {
                                collects.MachineryCollects.Add(new MachineryDefined
                                {
                                    Code = m.Type,
                                    Title = attr.Name,
                                    Quantity = m.Quantity ?? 0
                                });
                            }
                            else
                            {
                                checkMachinery.Quantity += m.Quantity ?? 0;
                            }
                        }

                        if (attr.Group == MachineryManpowerGroup.MorePartManpower)
                        {
                            reportMachine.AddManpowerDefineds.Add(new AddManpowerDefined
                            {
                                Code = m.Type,
                                Title = m.Title,
                                Quantity = m.Quantity ?? 0
                            });

                            var checkAddManpower = collects.AddManpowerCollects.FirstOrDefault(c => c.Code == m.Type && c.Title == m.Title);
                            if (checkAddManpower == null)
                            {
                                collects.AddManpowerCollects.Add(new AddManpowerDefined
                                {
                                    Code = m.Type,
                                    Title = m.Title,
                                    Quantity = m.Quantity ?? 0
                                });
                            }
                            else
                            {
                                checkAddManpower.Quantity += m.Quantity ?? 0;
                            }
                        }

                        if (attr.Group == MachineryManpowerGroup.MorePartMachine)
                        {
                            reportMachine.AddMachineryDefineds.Add(new AddMachineryDefined
                            {
                                Code = m.Type,
                                Title = m.Title,
                                Quantity = m.Quantity ?? 0
                            });

                            var checkAddMachinery = collects.AddMachineryCollects.FirstOrDefault(c => c.Code == m.Type && c.Title == m.Title);
                            if (checkAddMachinery == null)
                            {
                                collects.AddMachineryCollects.Add(new AddMachineryDefined
                                {
                                    Code = m.Type,
                                    Title = m.Title,
                                    Quantity = m.Quantity ?? 0
                                });
                            }
                            else
                            {
                                checkAddMachinery.Quantity += m.Quantity ?? 0;
                            }
                        }
                    });
                    reportMachines.Add(reportMachine);
                }
                reportModel.MachineryManpowerCollects = collects;
                reportModel.ReportMachineryManpowers = reportMachines;
                //foreach (var item in reportModel.ReportMachineryManpowers.Select(c => c.ManpowerDefineds))
                //{
                //    var listAdds = new List<MachineryManpowerWithCompany>()
                //    {

                //    };
                //    foreach (var company in companyInReport)
                //    {
                //        var add = new MachineryManpowerWithCompany()
                //        {
                //            CompanyId = company.Id,
                //            CompanyName = company.Name,
                //            Quantity = item.Select(c=>c.MachineryManpowerWithCompanies)
                //        };
                //        listAdds.Add(add);

                //    }
                //    reportModel.MachineryManpowerCollects.ManpowerCollects.Add(new ManpowerDefined
                //    {
                //        MachineryManpowerWithCompanies = listAdds
                //    });
                //}

            }

            var projectCompanyNews = _projectCompanyRepository.FindAll(c => c.ProjectId == projectReport.ProjectId).Select(c => c.CompanyId);
            var workitemCompanyInProject = workitemCompanyExists.Where(c => projectCompanyNews.Contains(c.CompanyId));


            var workitemDivides = DeterminedPlaceType(projectReport.ProjectId);

            var reportAllInProjects = _projectReportRepository.FindAll(c => c.ProjectId == projectReport.ProjectId);
            var reportAllInProjectIds = reportAllInProjects.Select(c => c.Id);
            var reportWorkitemAlls = _reportWorkitemRepository.FindAll(c => reportAllInProjectIds.Contains(c.ProjectReportId));
            foreach (var workitemDivide in workitemDivides)
            {
                var workitemIdIns = new List<int>();
                workitemIdIns.Add(workitemDivide.WorkitemCategory.Id);
                if (workitemDivide.WorkItemSmallerModels.Count()>0)
                {
                    workitemIdIns.AddRange(workitemDivide.WorkItemSmallerModels.Select(c => c.Id));
                }

                var checkIn = reportWorkitemAlls.Where(c => workitemIdIns.Contains(c.WorkitemId));
                workitemDivide.PlaceType = "VT";
                if (checkIn.Any(c=>c.PlaceType =="Bộ"))
                {
                    workitemDivide.PlaceType = "Bộ";
                }
                if (checkIn.Any(c => c.PlaceType == "KN"))
                {
                    workitemDivide.PlaceType = "KN";
                }
            }

            // Tổng hợp theo từng công ty( bên trong mỗi công ty có danh sách đầu mục)
            reportModel.CollectWorkitemWithCompanies = GetCollectWorkitemỈnReportWithCompanies(workitemCompanyInProject/*.Where(c => c.CompletedNumber > 0)*/.ToList(), companies, reportModel.ReportWorkitems, workitemDivides, projectReport);

            // Tổng hợp đầu mục
            reportModel.CollectCategories = GetCollectCategoryWorkitemInReports(projectReport, workitemCompanyInProject/*.Where(c => c.CompletedNumber > 0)*/, reportModel.ReportWorkitems, workitemDivides);



            return reportModel;
        }

        private List<WorkitemWithHeadingModel> DeterminedPlaceType(int projectId)
        {
            var result = new List<WorkitemWithHeadingModel>();
            var reportAllInProjects = _projectReportRepository.FindAll(c => c.ProjectId == projectId);
            var reportAllInProjectIds = reportAllInProjects.Select(c => c.Id);
            var workitemDivides = _workItemService.DivideWorkitem(projectId);
            var reportWorkitemAlls = _reportWorkitemRepository.FindAll(c => reportAllInProjectIds.Contains(c.ProjectReportId));
            foreach (var workitemDivide in workitemDivides)
            {
                var workitemIdIns = new List<int>();
                workitemIdIns.Add(workitemDivide.WorkitemCategory.Id);
                if (workitemDivide.WorkItemSmallerModels.Count() == 0)
                {
                    workitemDivide.CalculationUnit = workitemDivide.CalculationUnit;
                }

                if (workitemDivide.WorkItemSmallerModels.Count() > 0)
                {
                    workitemIdIns.AddRange(workitemDivide.WorkItemSmallerModels.Select(c => c.Id));
                    workitemDivide.CalculationUnit = workitemDivide.WorkItemSmallerModels.FirstOrDefault().CalculationUnit;
                }

                var checkIn = reportWorkitemAlls.Where(c => workitemIdIns.Contains(c.WorkitemId));
                workitemDivide.PlaceType = "VT";
                if (checkIn.Any(c => c.PlaceType == "Bộ"))
                {
                    workitemDivide.PlaceType = "Bộ";
                }
                if (checkIn.Any(c => c.PlaceType == "KN"))
                {
                    workitemDivide.PlaceType = "KN";
                }
               
                result.Add(workitemDivide);
            }
            return result;
        }

        public SearchProjectReportsResult Search(SearchProjectReportsModel searchModel, int? userCurrent)
        {
            var result = new SearchProjectReportsResult();

            if (!string.IsNullOrEmpty(searchModel.ProjectHashId))
            {
                searchModel.ProjectId = _hashidService.Decode(searchModel.ProjectHashId);
            }

            var projectReports = _projectReportRepository.Search(searchModel);
            projectReports = projectReports.ToList().Except(projectReports.ToList().Where(c => c.CreatedBy != userCurrent && c.Status == ProjectReportStatus.Draft));// người khác ko đc xem bản nháp của người báo cáo

            searchModel.TotalRecord = projectReports.Count();

            var reportModels = projectReports.ToList().CloneToListModels<ProjectReport, ProjectReportResultModel>();

            var projects = _projectRepository.FindAll();

            reportModels.ForEach(rp => { rp.ProjectName = projects.FirstOrDefault(c => c.Id == rp.ProjectId).Name; });
            var users = new List<User>();
            if (reportModels.Count() > 0)
            {
                var userCreatedIds = reportModels.Select(c => c.CreatedBy);
                users = _userRepository.FindAll(c => userCreatedIds.Contains(c.Id)).ToList();
            }

            foreach (var report in reportModels)
            {
                //report.ProjectName = _projectRepository.FindById(report.ProjectId).Name;

                var user = users.FirstOrDefault(c => c.Id == report.CreatedBy);
                if (user == null) continue;
                report.Email = (user.Email = user.Email ?? null);
                report.Name = (user.FullName = user.FullName ?? null);
                report.ProjectHashId = _hashidService.Encode(report.ProjectId);
            }

            reportModels = reportModels.OrderByDescending(c => c.Id).Skip(searchModel.PageSize * (searchModel.PageIndex - 1)).Take(searchModel.PageSize).ToList();

            foreach (var item in reportModels)
            {
                if (string.IsNullOrEmpty(item.MarkedRead))
                {
                    item.IsRead = false;
                }
                else
                {
                    var desezializes = JsonConvert.DeserializeObject<List<int>>(item.MarkedRead);
                    if (desezializes.Contains(userCurrent.Value))
                        item.IsRead = true;
                    else
                        item.IsRead = false;
                }
            }
            result.Records = reportModels;
            result.TotalRecord = searchModel.TotalRecord;
            result.PageIndex = searchModel.PageIndex;
            result.PageSize = searchModel.PageSize;
            result.PageCount = result.TotalRecord / result.PageSize + (result.TotalRecord % result.PageSize > 0 ? 1 : 0);

            return result;
        }

        public bool ValidatePlace(string input)
        {
            var result = false;

            var strs = input.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries).ToList();
            foreach (var str in strs)
            {
                if (str.Contains("-"))
                {
                    var test = str.Replace(" ", string.Empty);
                    int outCheck = 0;
                    var check = test.Split(new char[] { '-' }, StringSplitOptions.RemoveEmptyEntries).ToList();

                    if (check.Count() != 2 || check.Any(c => int.TryParse(c, out outCheck) == false))
                    {
                        result = false;
                        if (!result)
                            break;
                    }
                    else result = true;
                }
                else result = true;
            }
            return result;
        }



        private bool SaveReportWorkItem(IEnumerable<SaveWorkitemReportModel> models, int userId, IDbTransaction dbTransaction) //báo cáo hạng mục của ông bộ phận giám sát
        {
            var success = true;

            if (models.Count() > 0)
            {
                var reportCheckIds = models.Select(c => c.ProjectReportId).Distinct();
                var reports = _projectReportRepository.FindAll(c => reportCheckIds.Contains(c.Id), dbTransaction);
                var reportIds = reports.Select(c => c.Id);


                var projectCheckIds = reports.Select(c => c.ProjectId);

                var projectExists = _projectRepository.FindAll(c => projectCheckIds.Contains(c.Id), dbTransaction);
                var projectExistIds = projectExists.Select(c => c.Id);

                var projectUsers = _projectUserRepository.FindAll(c => c.UserId == userId && projectExistIds.Contains(c.ProjectId), dbTransaction);
                var companies = _companyRepository.FindAll(dbTransaction);
                var companyIds = companies.Select(c => c.Id);


                var workitems = _workItemRepository.FindAll(c => projectCheckIds.Contains(c.ProjectId), dbTransaction);
                var workitemIds = workitems.Select(c => c.Id);

                var workitemCompanies = _workItemCompanyRepository.FindAll(c => workitemIds.Contains(c.WorkItemId) && companyIds.Contains(c.CompanyId), dbTransaction);

                var reportWorkitems = _reportWorkitemRepository.FindAll(c => reportIds.Contains(c.ProjectReportId), dbTransaction);
                var reportWorkitemIds = reportWorkitems.Select(c => c.Id);

                var reportImageExists = _reportImageRepository.FindAll(ri => reportWorkitemIds.Contains(ri.ReportWorkitemId), dbTransaction);

                foreach (var model in models)
                {
                    var report = reports.FirstOrDefault(c => c.Id == model.ProjectReportId);
                    var existUserCurrent = projectUsers.FirstOrDefault(c => c.ProjectId == report.ProjectId);
                    if (existUserCurrent == null) throw new ServiceException("Bạn không thuộc dự án này nên không có quyền thao tác");
                    if (report == null) throw new ServiceException("Báo cáo không tồn tại");

                    var company = companies.FirstOrDefault(c => c.Id == model.CompanyId);
                    if (company == null) throw new ServiceException("Công ty không tồn tại");

                    var existWorkItem = workitems.FirstOrDefault(c => c.Id == model.WorkitemId);
                    if (existWorkItem == null) throw new ServiceException("Hạng mục này không tồn tại");

                    if (!model.CompletedNumber.HasValue) throw new ServiceException("Số lượng hoàn thành hạng mục " + existWorkItem.Name + " không được để trống");
                    if (model.CompletedNumber.Value < 0)
                    {
                        throw new ServiceException("Số lượng báo cáo cho hạng mục " + existWorkItem.Name + " không được nhỏ hơn 0");
                    }

                    if (existWorkItem.ProjectId != report.ProjectId) throw new ServiceException("Báo cáo và hạng mục không cùng dự án");

                    var existWorkItemCompany = workitemCompanies.FirstOrDefault(c => c.WorkItemId == model.WorkitemId && c.CompanyId == model.CompanyId);
                    if (existWorkItemCompany == null) throw new ServiceException("Hạng mục chưa được phân giao cho công ty");

                    if (model.CompletedNumber.Value > existWorkItemCompany.AssignedNumber) throw new ServiceException("Số lượng hạng mục hoàn thành trong báo cáo không được lớn hơn số lượng được phân giao cho công ty");

                    if (model.CompletedNumber.Value + existWorkItemCompany.CompletedNumber > existWorkItemCompany.AssignedNumber)
                        throw new ServiceException("Tổng số lượng trong báo cáo theo hạng mục và số lượng hoàn thành không được lớn hơn số lượng phân giao");



                    if (report.Status == Enums.ProjectReportStatus.Approved) throw new ServiceException("Không thể sửa báo cáo đã duyệt");

                    if (!string.IsNullOrEmpty(model.CompletePlace))
                    {
                        model.CompletePlace = model.CompletePlace.Replace(" ", "");
                        model.CompletePlace = model.CompletePlace.TrimStart(',');
                        model.CompletePlace = model.CompletePlace.TrimEnd(',');
                        //Validate Place
                        if (!ValidatePlace(model.CompletePlace))
                        {
                            throw new ServiceException("Vị trí hoàn thành của công ty " + company.Name + " trong hạng mục " + existWorkItem.Name + " nhập không đúng");
                        }
                    }

                    if (!string.IsNullOrEmpty(model.UnderPlace))
                    {
                        model.UnderPlace = model.UnderPlace.Replace(" ", "");
                        model.UnderPlace = model.UnderPlace.TrimStart(',');
                        model.UnderPlace = model.UnderPlace.TrimEnd(',');
                        if (!ValidatePlace(model.UnderPlace))
                        {
                            throw new ServiceException("Vị trí đang thi công của công ty " + company.Name + " trong hạng mục " + existWorkItem.Name + " nhập không đúng");
                        }
                    }

                    

                   


                    var item = model.CloneToModel<SaveWorkitemReportModel, ProjectReportWorkitem>();

                    var errorMessage = "Không cập nhật được báo cáo cho hạng mục " + existWorkItem.Name + " cho công ty " + company.Name;

                    if (model.Id.HasValue)
                    {
                        if (!_reportWorkitemRepository.Update(item, dbTransaction)) throw new ServiceException(errorMessage);

                    }
                    else
                    {
                        var existWorkitemReport = reportWorkitems.FirstOrDefault(c => c.ProjectReportId == model.ProjectReportId && c.WorkitemId == model.WorkitemId
                           && c.CompanyId == model.CompanyId);
                        if (existWorkitemReport != null) throw new ServiceException("Báo cáo hạng mục " + existWorkItem.Name + " từ phía công ty "
                            + company.Name + " này đã tồn tại, bạn muốn thay đổi thì vào phần cập nhật");
                        if (!_reportWorkitemRepository.Insert(item, dbTransaction)) throw new ServiceException(errorMessage);
                    }

                    if (model.Images == null) continue;

                    // delete removed file
                    var imageGuidIds = model.Images.Select(a => a.Image).Where(a => a.Length < 100);
                    var reportImages = reportImageExists.Where(ri => ri.ReportWorkitemId == item.Id);
                    if (reportImages != null)
                    {
                        foreach (var reportImage in reportImages)
                        {
                            if (!imageGuidIds.Contains(reportImage.FileId.ToString()))
                            {
                                _reportImageRepository.Delete(reportImage, dbTransaction);
                                var file = _fileRepository.FindById(reportImage.FileId, dbTransaction);
                                _fileRepository.Delete(file, dbTransaction);
                                _binaryFileStorageRepository.Delete(b => b.Id == file.StorageId, dbTransaction);
                            }
                            else
                            {
                                var modelCompare = (from m in model.Images
                                                    where m.Image.Length < 100 & m.Image == reportImage.FileId.ToString()
                                                    select m).FirstOrDefault();
                                if (modelCompare != null)
                                {
                                    reportImage.Message = modelCompare.Message;
                                    _reportImageRepository.Update(reportImage, dbTransaction);
                                }
                            }

                        }
                    }

                    foreach (var image in model.Images)
                    {
                        if (image.Image.Length < 100) // is guid string
                            continue;

                        var base64 = image.Image;
                        base64 = base64.Replace("data:image/jpeg;base64,", string.Empty);
                        base64 = base64.Replace("data:image/png;base64,", string.Empty);

                        byte[] decodedByteArray = Convert.FromBase64String(base64);
                        var binaryFileStorage = new BinaryFileStorage()
                        {
                            Data = decodedByteArray
                        };
                        var insertBinaryFile = _binaryFileStorageRepository.Insert(binaryFileStorage, dbTransaction);

                        var file = new Entity.File()
                        {
                            Id = Guid.NewGuid(),
                            ProviderType = ProviderFileType.LocalStorage,
                            StorageId = binaryFileStorage.Id,
                            Mime = "image/jpeg",
                            Title = "Ảnh báo cáo cho hạng mục " + existWorkItem.Name + " ngày " + report.CreatedOnUtc,
                            CreatedUtcTime = DateTime.UtcNow,
                            Size = base64.Length / 1024,
                            Status = FileStatus.NewPicture
                        };
                        var insertFile = _fileRepository.Insert(file, dbTransaction);

                        var reportImage = new ReportImage()
                        {
                            ProjectReportId = report.Id,
                            ReportWorkitemId = item.Id,
                            FileId = file.Id,
                            Lat = image.Lat,
                            Lng = image.Lng,
                            Message = image.Message
                        };
                        var resImage = _reportImageRepository.Insert(reportImage, dbTransaction);
                        if (!resImage)
                        {
                            success = false;
                        }
                    }
                }
            }

            return success;
        }

        private bool SaveReportPlan(IEnumerable<SaveProjectReportPlanModel> reportPlans, int userId, IDbTransaction dbTransaction)
        {

            var result = true;
            if (reportPlans.Count() > 0)
            {
                var reportCheckIds = reportPlans.Select(c => c.ProjectReportId);
                var reportExists = _projectReportRepository.FindAll(c => reportCheckIds.Contains(c.Id), dbTransaction);

                var projectIds = reportExists.Select(c => c.ProjectId).Distinct();

                var planCheckIds = reportPlans.Select(c => c.PlanId);
                var plans = _projectPlanRepository.FindAll(c => planCheckIds.Contains(c.Id), dbTransaction);

                var reportPlanExists = _reportPlanRepository.FindAll(c => reportCheckIds.Contains(c.ProjectReportId), dbTransaction);

                var userExists = _projectUserRepository.FindAll(c => c.UserId == userId && projectIds.Contains(c.ProjectId), dbTransaction);

                foreach (var reportPlan in reportPlans)
                {
                    var report = reportExists.FirstOrDefault(c => c.Id == reportPlan.ProjectReportId);
                    if (report == null) throw new ServiceException("Báo cáo không tồn tại");

                    if (report.Status == ProjectReportStatus.Approved) throw new ServiceException("Không thể sửa báo cáo đã duyệt");

                    var existUserCurrent = userExists.FirstOrDefault(c => c.ProjectId == report.ProjectId);
                    if (existUserCurrent == null) throw new ServiceException("Bạn không thuộc dự án này nên không có quyền thao tác");

                    var existPlan = plans.FirstOrDefault(c => c.Id == reportPlan.PlanId);
                    if (existPlan == null) throw new ServiceException("Không tồn tại kế hoạch này");

                    if (existPlan.ProjectId != report.ProjectId) throw new ServiceException("Báo cáo và kế hoạch không cùng dự án");

                    var message = "Không cập nhật được báo cáo kế hoạch " + existPlan.Title;

                    if (reportPlan.CompletedPercentage < 0 || reportPlan.CompletedPercentage > 100)
                        throw new ServiceException("Số phần trăm tiến độ hoàn thành nhập vào không đúng");
                    if (reportPlan.CompletedPercentage < existPlan.CompletedPercentage) throw new ServiceException("Số % hoàn thành mới phải lớn hơn % hoàn thành hiện tại đã duyệt.");
                    if (reportPlan.Id.HasValue)
                    {
                        var entity = reportPlanExists.FirstOrDefault(c => c.Id == reportPlan.Id.Value);

                        entity.CompletedPercentage = reportPlan.CompletedPercentage;
                        result = _reportPlanRepository.Update(entity, dbTransaction);
                        if (!result) throw new ServiceException(message);

                    }
                    else
                    {
                        var existPlanReport = reportPlanExists.FirstOrDefault(c => c.PlanId == reportPlan.PlanId
                            && c.ProjectReportId == reportPlan.ProjectReportId);
                        if (existPlanReport != null)
                            throw new ServiceException("Kế hoạch " + existPlan.Title + " đã tồn tại trong báo cáo, bạn muốn thay đổi thì vào phần cập nhật");
                        var entity = new ProjectReportPlan
                        {
                            PlanId = reportPlan.PlanId,
                            CompletedPercentage = reportPlan.CompletedPercentage,
                            ProjectReportId = reportPlan.ProjectReportId
                        };
                        result = _reportPlanRepository.Insert(entity, dbTransaction);
                        if (!result) throw new ServiceException(message);
                    }
                }
            }

            return result;
        }

        private ProjectReportModel SaveReport(SaveReportModel saveReportModel, int userId, IDbTransaction dbTransaction)// SaveReportModel để lưu thông tin bảng ProjectReport
        {
            saveReportModel.ProjectId = _hashidService.Decode(saveReportModel.HashId);
            var success = false;
            var report = new ProjectReport();
            var project = _projectRepository.FindById(saveReportModel.ProjectId, dbTransaction);
            if (project == null) throw new ServiceException("Không tồn tại dự án");
            var existUserCurrent = _projectUserRepository.Find(c => c.UserId == userId && c.ProjectId == saveReportModel.ProjectId, dbTransaction);
            if (existUserCurrent == null) throw new ServiceException("Bạn không thuộc dự án này nên không có quyền thao tác");
            if (project.Status != ProjectStatus.Approved) throw new ServiceException("Dự án chưa được duyệt");
            if (saveReportModel.ProjectReportId.HasValue)
            {
                report = _projectReportRepository.FindById(saveReportModel.ProjectReportId.Value, dbTransaction);
                if (report.Status == Enums.ProjectReportStatus.Approved) throw new ServiceException("Không thể sửa báo cáo đã duyệt");
                report.ReportType = saveReportModel.ReportType;
                report.MorningWeather = saveReportModel.MorningWeather;
                report.AfternoonWeather = saveReportModel.AfternoonWeather;
                report.Note = saveReportModel.Note;
                report.Status = ProjectReportStatus.Draft;
                success = _projectReportRepository.Update(report, dbTransaction);
            }
            else
            {
                report = new ProjectReport
                {
                    CreatedOnUtc = DateTime.UtcNow,
                    CreatedBy = saveReportModel.UserId,
                    AfternoonWeather = saveReportModel.AfternoonWeather,
                    MorningWeather = saveReportModel.MorningWeather,
                    Note = saveReportModel.Note,
                    ProjectId = saveReportModel.ProjectId,
                    ReportType = saveReportModel.ReportType,
                    Status = Enums.ProjectReportStatus.Draft
                };
                success = _projectReportRepository.Insert(report, dbTransaction);
            }
            return DomainMaps.Mapper.Map<ProjectReportModel>(report);
        }

        private bool AddMachineyManpower(List<object> list, IDbTransaction trans, int companyId, int reportId, MachineryManpowerGroup group, int projectId)
        {
            var result = false;
            if (group == MachineryManpowerGroup.Manpower)
            {
                var listNews = list.Cast<ManpowerDefined>();
                foreach (var listNew in listNews)
                {
                    result = _machineryManpowerReportRepository.Insert(new MachineryManpowerReport
                    {
                        CompanyId = companyId,
                        ProjectId = projectId,
                        ReportId = reportId,
                        Type = listNew.Code,
                        Quantity = listNew.Quantity,
                        Title = listNew.Title
                    }, trans);
                    if (!result) break;
                }
            }

            if (group == MachineryManpowerGroup.MorePartManpower)
            {
                var listNews = list.Cast<AddManpowerDefined>();
                foreach (var listNew in listNews)
                {
                    result = _machineryManpowerReportRepository.Insert(new MachineryManpowerReport
                    {
                        CompanyId = companyId,
                        ReportId = reportId,
                        ProjectId = projectId,
                        Type = listNew.Code,
                        Quantity = listNew.Quantity,
                        Title = listNew.Title
                    }, trans);
                    if (!result) break;
                }
            }

            if (group == MachineryManpowerGroup.Machine)
            {
                var listNews = list.Cast<MachineryDefined>();
                foreach (var listNew in listNews)
                {
                    result = _machineryManpowerReportRepository.Insert(new MachineryManpowerReport
                    {
                        CompanyId = companyId,
                        ProjectId = projectId,
                        ReportId = reportId,
                        Type = listNew.Code,
                        Quantity = listNew.Quantity,
                        Title = listNew.Title
                    }, trans);
                    if (!result) break;
                }
            }

            if (group == MachineryManpowerGroup.MorePartMachine)
            {
                var listNews = list.Cast<AddMachineryDefined>();
                foreach (var listNew in listNews)
                {
                    result = _machineryManpowerReportRepository.Insert(new MachineryManpowerReport
                    {
                        CompanyId = companyId,
                        ProjectId = projectId,
                        ReportId = reportId,
                        Type = listNew.Code,
                        Quantity = listNew.Quantity,
                        Title = listNew.Title
                    }, trans);
                    if (!result) break;
                }
            }
            return result;
        }

        private bool SaveMachineryManpower(IEnumerable<MachineryManpowerDefinedModel> machineryManpowerDefineds, int reportId, int userId, int projectId, IDbTransaction trans)
        {
            var success = false;
            var machineryManpowerExists = _machineryManpowerReportRepository.FindAll(c => c.ReportId == reportId, trans);

            if (machineryManpowerExists.Count() == 0)
            {
                //machineryManpowerExists.Where(c => c.Type.GetAttributeOfType<MachineryManpowerDetailAttribute>()
                //                     .Group == MachineryManpowerGroup.Manpower).ToList().ForEach(m =>
                //{

                //    foreach (var item in machineryManpowerDefineds)
                //    {
                //        if ((!item.ManpowerDefineds.Select(c => c.Code).Contains(m.Type)) && item.CompanyId == m.CompanyId)
                //        {
                //            _machineryManpowerReportRepository.Delete(m, trans);
                //        }
                //    }
                //});

                //if (!imageGuidIds.Contains(reportImage.FileId.ToString()))
                //{
                //    _reportImageRepository.Delete(reportImage, dbTransaction);
                //    var file = _fileRepository.FindById(reportImage.FileId, dbTransaction);
                //    _fileRepository.Delete(file, dbTransaction);
                //    _binaryFileStorageRepository.Delete(b => b.Id == file.StorageId, dbTransaction);
                //}
                foreach (var item in machineryManpowerDefineds)
                {

                    var manpowers = item.ManpowerDefineds;
                    var machines = item.MachineryDefineds;
                    var addManpowers = item.AddManpowerDefineds;
                    var addMachines = item.AddMachineryDefineds;

                    if (manpowers.Count() > 0)
                    {
                        var hic = manpowers.Cast<object>();
                        List<object> obj1 = manpowers.Cast<object>().ToList();
                        success = AddMachineyManpower(obj1, trans, item.CompanyId.Value, reportId, MachineryManpowerGroup.Manpower, projectId);
                        if (!success) break;
                    }

                    if (machines.Count() > 0)
                    {
                        List<object> obj2 = machines.Cast<object>().ToList();
                        success = AddMachineyManpower(obj2, trans, item.CompanyId.Value, reportId, MachineryManpowerGroup.Machine, projectId);
                        if (!success) break;
                    }

                    if (addManpowers.Count() > 0)
                    {
                        List<object> obj3 = addManpowers.Cast<object>().ToList();
                        success = AddMachineyManpower(obj3, trans, item.CompanyId.Value, reportId, MachineryManpowerGroup.MorePartManpower, projectId);
                        if (!success) break;
                    }

                    if (addMachines.Count() > 0)
                    {
                        List<object> obj4 = addMachines.Cast<object>().ToList();
                        success = AddMachineyManpower(obj4, trans, item.CompanyId.Value, reportId, MachineryManpowerGroup.MorePartMachine, projectId);
                        if (!success) break;
                    }
                }
            }
            else
            {
                foreach (var item in machineryManpowerDefineds)
                {
                    var manpowers = item.ManpowerDefineds;
                    var machines = item.MachineryDefineds;
                    var addManpowers = item.AddManpowerDefineds;
                    var addMachines = item.AddMachineryDefineds;

                    if (manpowers.Count() > 0)
                    {
                        foreach (var manpower in manpowers)
                        {
                            if (machineryManpowerExists.Where(v => v.Type == manpower.Code && v.CompanyId == item.CompanyId).Count() == 0)
                            {
                                success = _machineryManpowerReportRepository.Insert(new MachineryManpowerReport
                                {
                                    CompanyId = item.CompanyId.Value,
                                    ReportId = reportId,
                                    ProjectId = projectId,
                                    Type = manpower.Code,
                                    Quantity = manpower.Quantity,
                                    Title = manpower.Title
                                }, trans);
                                if (!success) break;
                            }
                            else
                            {
                                foreach (var machineryManpowerExist in machineryManpowerExists.Where(c => c.CompanyId == item.CompanyId))
                                {
                                    if (machineryManpowerExist.Type.GetAttributeOfType<MachineryManpowerDetailAttribute>()
                                     .Group == MachineryManpowerGroup.Manpower && machineryManpowerExist.Type == manpower.Code)
                                        success = _machineryManpowerReportRepository.Update(new MachineryManpowerReport
                                        {
                                            Id = machineryManpowerExist.Id,
                                            ProjectId = projectId,
                                            Quantity = manpower.Quantity,
                                            Title = machineryManpowerExist.Title,
                                            Type = machineryManpowerExist.Type,
                                            CompanyId = machineryManpowerExist.CompanyId,
                                            ReportId = reportId
                                        }, trans);
                                    if (!success) break;
                                }
                            }
                        }


                    }
                    var manpowerExists = machineryManpowerExists.Where(c => c.Type.GetAttributeOfType<MachineryManpowerDetailAttribute>()
                                      .Group == MachineryManpowerGroup.Manpower && c.CompanyId == item.CompanyId);
                    manpowerExists.ToList().ForEach(m =>
                    {
                        if (item.ManpowerDefineds.Where(c => c.Code == m.Type && item.CompanyId == m.CompanyId).Count() == 0)
                        {
                            _machineryManpowerReportRepository.Delete(m, trans);
                        }

                    });


                    if (machines.Count() > 0)
                    {
                        foreach (var machine in machines)
                        {
                            if (machineryManpowerExists.Where(v => v.Type == machine.Code && v.CompanyId == item.CompanyId).Count() == 0)
                            {
                                success = _machineryManpowerReportRepository.Insert(new MachineryManpowerReport
                                {
                                    CompanyId = item.CompanyId.Value,
                                    ProjectId = projectId,
                                    ReportId = reportId,
                                    Type = machine.Code,
                                    Quantity = machine.Quantity,
                                    Title = machine.Title
                                }, trans);
                                if (!success) break;
                            }
                            else
                            {
                                foreach (var machineryManpowerExist in machineryManpowerExists.Where(c => c.CompanyId == item.CompanyId))
                                {
                                    if (machineryManpowerExist.Type.GetAttributeOfType<MachineryManpowerDetailAttribute>()
                                     .Group == MachineryManpowerGroup.Machine && machineryManpowerExist.Type == machine.Code)
                                        success = _machineryManpowerReportRepository.Update(new MachineryManpowerReport
                                        {
                                            Id = machineryManpowerExist.Id,
                                            ProjectId = projectId,
                                            Quantity = machine.Quantity,
                                            Title = machineryManpowerExist.Title,
                                            Type = machineryManpowerExist.Type,
                                            CompanyId = machineryManpowerExist.CompanyId,
                                            ReportId = reportId
                                        }, trans);
                                    if (!success) break;
                                }
                            }
                        }
                    }

                    var machineExists = machineryManpowerExists.Where(c => c.Type.GetAttributeOfType<MachineryManpowerDetailAttribute>()
                                     .Group == MachineryManpowerGroup.Machine && c.CompanyId == item.CompanyId);
                    machineExists.ToList().ForEach(m =>
                    {
                        if (item.MachineryDefineds.Where(c => c.Code == m.Type && item.CompanyId == m.CompanyId).Count() == 0)
                        {
                            _machineryManpowerReportRepository.Delete(m, trans);
                        }

                    });

                    if (addManpowers.Count() > 0)
                    {
                        foreach (var addManpower in addManpowers)
                        {
                            if (machineryManpowerExists.Where(v => v.Type == addManpower.Code && v.Title.Equals(addManpower.Title) && v.CompanyId == item.CompanyId).Count() == 0)
                            {
                                success = _machineryManpowerReportRepository.Insert(new MachineryManpowerReport
                                {
                                    CompanyId = item.CompanyId.Value,
                                    ProjectId = projectId,
                                    ReportId = reportId,
                                    Type = addManpower.Code,
                                    Quantity = addManpower.Quantity,
                                    Title = addManpower.Title
                                }, trans);
                                if (!success) break;
                            }
                            else
                            {
                                foreach (var machineryManpowerExist in machineryManpowerExists.Where(c => c.CompanyId == item.CompanyId))
                                {
                                    if (machineryManpowerExist.Type.GetAttributeOfType<MachineryManpowerDetailAttribute>()
                                     .Group == MachineryManpowerGroup.MorePartManpower && machineryManpowerExist.Type == addManpower.Code && machineryManpowerExist.Title == addManpower.Title)
                                        success = _machineryManpowerReportRepository.Update(new MachineryManpowerReport
                                        {
                                            Id = machineryManpowerExist.Id,
                                            ProjectId = projectId,
                                            Quantity = addManpower.Quantity,
                                            Title = machineryManpowerExist.Title,
                                            Type = machineryManpowerExist.Type,
                                            CompanyId = machineryManpowerExist.CompanyId,
                                            ReportId = reportId
                                        }, trans);
                                    if (!success) break;
                                }
                            }
                        }
                    }

                    var addManpowerExists = machineryManpowerExists.Where(c => c.Type.GetAttributeOfType<MachineryManpowerDetailAttribute>()
                                     .Group == MachineryManpowerGroup.MorePartManpower && c.CompanyId == item.CompanyId);
                    addManpowerExists.ToList().ForEach(m =>
                    {
                        if (item.AddManpowerDefineds.Where(c => c.Code == m.Type && item.CompanyId == m.CompanyId && c.Title.Equals(m.Title)).Count() == 0)
                        {
                            _machineryManpowerReportRepository.Delete(m, trans);
                        }

                    });

                    if (addMachines.Count() > 0)
                    {
                        foreach (var addMachine in addMachines)
                        {
                            if (machineryManpowerExists.Where(v => v.Type == addMachine.Code && v.Title.Equals(addMachine.Title) && v.CompanyId == item.CompanyId).Count() == 0)
                            {
                                success = _machineryManpowerReportRepository.Insert(new MachineryManpowerReport
                                {
                                    CompanyId = item.CompanyId.Value,
                                    ProjectId = projectId,
                                    ReportId = reportId,
                                    Type = addMachine.Code,
                                    Quantity = addMachine.Quantity,
                                    Title = addMachine.Title
                                }, trans);
                                if (!success) break;
                            }
                            else
                            {
                                foreach (var machineryManpowerExist in machineryManpowerExists.Where(c => c.CompanyId == item.CompanyId))
                                {
                                    if (machineryManpowerExist.Type.GetAttributeOfType<MachineryManpowerDetailAttribute>()
                                     .Group == MachineryManpowerGroup.MorePartMachine && machineryManpowerExist.Type == addMachine.Code && machineryManpowerExist.Title == addMachine.Title)
                                        success = _machineryManpowerReportRepository.Update(new MachineryManpowerReport
                                        {
                                            Id = machineryManpowerExist.Id,
                                            ProjectId = projectId,
                                            Quantity = addMachine.Quantity,
                                            Title = machineryManpowerExist.Title,
                                            Type = machineryManpowerExist.Type,
                                            CompanyId = machineryManpowerExist.CompanyId,
                                            ReportId = reportId
                                        }, trans);
                                    if (!success) break;
                                }
                            }
                        }
                    }

                    var addMachineExists = machineryManpowerExists.Where(c => c.Type.GetAttributeOfType<MachineryManpowerDetailAttribute>()
                                     .Group == MachineryManpowerGroup.MorePartMachine && c.CompanyId == item.CompanyId);
                    addMachineExists.ToList().ForEach(m =>
                    {
                        if (item.AddMachineryDefineds.Where(c => c.Code == m.Type && item.CompanyId == m.CompanyId && c.Title.Equals(m.Title)).Count() == 0)
                        {
                            _machineryManpowerReportRepository.Delete(m, trans);
                        }

                    });
                }

            }

            return success;
        }

        public MachineryManpowerDefinedModel MachineryManpowerDefined()
        {
            var result = new MachineryManpowerDefinedModel();
            foreach (var code in Enum.GetValues(typeof(MachineryManpowerType)).Cast<MachineryManpowerType>())
            {
                var attr = code.GetAttributeOfType<MachineryManpowerDetailAttribute>();
                if (attr.Group == MachineryManpowerGroup.Manpower)
                {
                    result.ManpowerDefineds.Add(new ManpowerDefined
                    {
                        Code = code,
                        Title = attr.Name,
                        Quantity = 0
                    });
                }

                if (attr.Group == MachineryManpowerGroup.MorePartManpower)
                {
                    result.AddManpowerDefineds.Add(new AddManpowerDefined
                    {
                        Code = code,
                        Title = attr.Name,
                        Quantity = 0
                    });
                }

                if (attr.Group == MachineryManpowerGroup.Machine)
                {
                    result.MachineryDefineds.Add(new MachineryDefined
                    {
                        Code = code,
                        Title = attr.Name,
                        Quantity = 0
                    });
                }

                if (attr.Group == MachineryManpowerGroup.MorePartMachine)
                {
                    result.AddMachineryDefineds.Add(new AddMachineryDefined
                    {
                        Code = code,
                        Title = attr.Name,
                        Quantity = 0
                    });
                }
            }
            return result;
        }

        public int SaveReportAll(SaveReportModel saveReportModel, int userId)
        {
            var trans = _reportWorkitemRepository.BeginTransaction();//thao tác trên transaction chứ k thao tác trực tiếp với CSDL
            if (trans.Connection.State != ConnectionState.Open) trans.Connection.Open();
            int result = 0;
            try
            {
                var successReport = SaveReport(saveReportModel, userId, trans);//lưu lại thông tin ProjectReport
                if (successReport != null)
                {
                    if (saveReportModel.ReportWorkitems.Count() > 0)
                    {
                        saveReportModel.ReportWorkitems.ToList().ForEach(rw => { rw.ProjectReportId = successReport.Id; });
                    }
                    if (saveReportModel.ReportPlans.Count() > 0)
                    {
                        saveReportModel.ReportPlans.ToList().ForEach(rw => { rw.ProjectReportId = successReport.Id; });
                    }
                    var successWorkitemReport = SaveReportWorkItem(saveReportModel.ReportWorkitems, userId, trans);//kiểm tra thông tin ProjectReportItem và ReportImages

                    var successReportPlan = SaveReportPlan(saveReportModel.ReportPlans, userId, trans);//

                    var successMachineryManpower = SaveMachineryManpower(saveReportModel.MachineryManpowerDefineds, successReport.Id, userId, saveReportModel.ProjectId, trans);

                    if ((!successWorkitemReport) || (!successReportPlan) || (!successMachineryManpower))
                    {
                        trans.Rollback();
                    }
                    else
                    {
                        result = successReport.Id;
                    }
                }
            }
            catch (Exception ex)
            {
                trans.Rollback();
                throw ex;
            }

            trans.Commit();//thực thi vào CSDL
            return result;
        }



        public bool SubmitReport(SendToSiteManagerModel model, int userId)
        {

            var report = _projectReportRepository.FindById(model.ProjectReportId);
            if (report == null) throw new ServiceException("Báo cáo không tồn tại");

            var projectUser = _projectUserRepository.Find(c => c.UserId == userId && c.ProjectId == report.ProjectId);
            if (projectUser == null) new ServiceException("Bạn không thuộc dự án này nên không có quyền thao tác");

            if (report.Status != ProjectReportStatus.Draft)
                throw new ServiceException("Báo cáo này đã được trình duyệt");

            var reportPlans = _reportPlanRepository.FindAll(c => c.ProjectReportId == model.ProjectReportId);
            var reportWorkitems = _reportWorkitemRepository.FindAll(c => c.ProjectReportId == model.ProjectReportId);
            if (reportPlans.Count() == 0 && reportWorkitems.Count() == 0) throw new ServiceException("Báo cáo phải có hạng mục hay tiến độ mới được phép trình duyệt");

            if (report.Status == ProjectReportStatus.Submited) throw new ServiceException("Báo cáo này đã được gửi cho trưởng ban chỉ huy công trình");
            if (report.CreatedBy != userId) throw new ServiceException("Bạn không phải là người tạo báo cáo này nên không có quyền");

            report.Status = ProjectReportStatus.Submited;
            var success = _projectReportRepository.Update(report);
            if (success)
            {
                var userSend = _userRepository.FindById(userId);
                var project = _projectRepository.FindById(report.ProjectId);
                var notification = new SubmitedReportEventModel
                {
                    Type = EventType.SubmitedReport,
                    UserSend = userSend.FullName ?? userSend.Email,
                    ProjectHashId = _hashidService.Encode(project.Id),
                    ProjectName = project.Name
                };
                var userIds = new List<int>();
                var listUserProject = _projectUserRepository.FindAll(c => c.ProjectId == report.ProjectId);
                var listUser = new List<User>();
                var test = _userRepository.FindById(71);
                listUserProject.ToList().ForEach(user =>
                {
                    listUser.Add(_userRepository.FindById(user.UserId));
                });
                var b = listUser.Where(c => c.UserRoles.Contains("5"));
                userIds.AddRange(b.Select(c => c.Id));
                _notificationService.AddNotification(userIds.ToList(), notification);
            }
            return success;
        }

        public IEnumerable<ProjectReportModel> ListReportForSiteManager()
        {
            var report = _projectReportRepository.FindAll(c => c.Status == ProjectReportStatus.Submited);
            if (report != null)
            {
                return report.ToList().CloneToListModels<ProjectReport, ProjectReportModel>();
            }
            else throw new ServiceException("Không có báo cáo nào được gửi đến cho trưởng ban chỉ huy công trình phê duyêt");
        }

        public bool DeleteReport(DeleteReportModel deleteReportModel, bool isCommandLead)
        {
            var result = false;
            var trans = _projectReportRepository.BeginTransaction();
            if (trans.Connection.State != ConnectionState.Open) trans.Connection.Open();
            try
            {
                var report = _projectReportRepository.FindById(deleteReportModel.ProjectReportId, trans);
                if (report == null) throw new ServiceException("Báo cáo không tồn tại");
                if (report.Status == ProjectReportStatus.Approved && !isCommandLead) throw new ServiceException("Không thể xóa báo cáo đã duyệt");
                var project = _projectRepository.FindById(report.ProjectId, trans);
                var reportInProjects = _projectReportRepository.FindAll(c => c.ProjectId == project.Id, trans);
                var workitems = _workItemRepository.FindAll(c => c.ProjectId == project.Id, trans);
                var workitemIds = workitems.Select(c => c.Id);

                var plans = _projectPlanRepository.FindAll(c => c.ProjectId == project.Id, trans);

                var reportWorkitems = _reportWorkitemRepository.FindAll(c => c.ProjectReportId == deleteReportModel.ProjectReportId, trans);

                var reportWorkitemInProjects = _reportWorkitemRepository.FindAll(c => workitemIds.Contains(c.WorkitemId), trans);


                var workitemCheckIds = reportWorkitems.Select(c => c.WorkitemId);
                var companyCheckIds = reportWorkitems.Select(c => c.CompanyId);

                var workitemCompanies = _workItemCompanyRepository.FindAll(c => workitemCheckIds.Contains(c.WorkItemId) && companyCheckIds.Contains(c.CompanyId), trans);

                var reportPlans = _reportPlanRepository.FindAll(c => c.ProjectReportId == deleteReportModel.ProjectReportId, trans);
                var planCheckIds = reportPlans.Select(c => c.PlanId);


                var machineryManpowerReports = _machineryManpowerReportRepository.FindAll(c => c.ReportId == deleteReportModel.ProjectReportId, trans);

                var WorkitemInReports = workitems.Where(c => workitemCheckIds.Contains(c.Id));
                var planInReports = plans.Where(c => planCheckIds.Contains(c.Id));



                if (isCommandLead && report.Status == ProjectReportStatus.Approved)
                {
                    foreach (var reportWorkitem in reportWorkitems)
                    {
                        var updateWorkitemCompany = false;
                        var updateWorkitem = false;
                        var workitemCompany = workitemCompanies.FirstOrDefault(c => c.CompanyId == reportWorkitem.CompanyId && c.WorkItemId == reportWorkitem.WorkitemId);
                        workitemCompany.CompletedNumber -= reportWorkitem.CompletedNumber;

                        updateWorkitemCompany = _workItemCompanyRepository.Update(workitemCompany, trans);

                        var workitem = workitems.FirstOrDefault(c => c.Id == reportWorkitem.WorkitemId);
                        workitem.CompletedNumberTotal -= reportWorkitem.CompletedNumber;
                        updateWorkitem = _workItemRepository.Update(workitem, trans);
                        if (updateWorkitem && updateWorkitemCompany)
                        {
                            result = _reportWorkitemRepository.Delete(reportWorkitem, trans);

                        }
                        else result = false;
                        if (!result) break;

                        var reportOthers = reportInProjects.Where(c => c.Id != reportWorkitem.ProjectReportId);
                        if (reportOthers.Where(c => c.Id != reportWorkitem.ProjectReportId && c.Status == ProjectReportStatus.Approved).OrderByDescending(c => c.CreatedOnUtc).Count() == 0)
                        {
                            workitemCompany.CompletePlace = string.Empty;
                            workitemCompany.UnderPlace = string.Empty;
                            workitemCompany.LastUpdateContentReport = string.Empty;
                            workitemCompany.LastUpdateCompletedNumber = 0;
                            _workItemCompanyRepository.Update(workitemCompany, trans);
                        }
                        foreach (var reports in reportOthers.Where(c => c.Id != reportWorkitem.ProjectReportId && c.Status == ProjectReportStatus.Approved)
                            .OrderByDescending(c => c.CreatedOnUtc))
                        {
                            var afterReportIdCheck = reports.Id;
                            var reportWorkitemAfterFirst = reportWorkitemInProjects.FirstOrDefault(c => c.CompanyId == reportWorkitem.CompanyId && c.WorkitemId == reportWorkitem.WorkitemId
                               && c.ProjectReportId == afterReportIdCheck);
                            //var test = reportWorkitemAfterFirst.First(c=>c.ProjectReportId)

                            if (reportWorkitemAfterFirst != null)
                            {
                                workitemCompany.CompletePlace = reportWorkitemAfterFirst.CompletePlace;
                                workitemCompany.UnderPlace = reportWorkitemAfterFirst.UnderPlace;
                                workitemCompany.LastUpdateContentReport = reportWorkitemAfterFirst.Content;
                                workitemCompany.LastUpdateCompletedNumber = reportWorkitemAfterFirst.CompletedNumber;
                                _workItemCompanyRepository.Update(workitemCompany, trans);
                            }
                            else
                            {
                                workitemCompany.CompletePlace = string.Empty;
                                workitemCompany.UnderPlace = string.Empty;
                                workitemCompany.LastUpdateContentReport = string.Empty;
                                workitemCompany.LastUpdateCompletedNumber = 0;
                                _workItemCompanyRepository.Update(workitemCompany, trans);
                            }

                        }
                    }



                    var planIdInReports = planInReports.Select(a => a.Id);
                    var reportPlanChecks = _reportPlanRepository.FindAll(c => planIdInReports.Contains(c.PlanId), trans);
                    var reportCheckTimes = reportInProjects.Where(c => reportPlanChecks.Select(a => a.ProjectReportId).Contains(c.Id));

                    foreach (var reportPlan in reportPlans)
                    {
                        // tách
                        var reportPlanCheckNows = reportPlanChecks.Where(c => c.PlanId == reportPlan.PlanId);
                        if (reportPlanCheckNows.Count() > 0)
                        {
                            var reportCheckTimeNows = reportCheckTimes.Where(c => reportPlanCheckNows.Select(a => a.ProjectReportId).Contains(c.Id));
                            if (reportCheckTimeNows.Where(c => c.CreatedOnUtc > report.CreatedOnUtc).Count() == 0)
                            {
                                reportCheckTimeNows = reportCheckTimeNows.Where(c => c.CreatedOnUtc != reportCheckTimeNows.Max(a => a.CreatedOnUtc)).ToList();
                                if (reportCheckTimeNows.Count() > 0)
                                {
                                    var reportCheckTimeNearest = reportCheckTimeNows.FirstOrDefault(c => c.CreatedOnUtc == reportCheckTimeNows.Max(a => a.CreatedOnUtc));
                                    var reportPlanNearest = reportPlanCheckNows.FirstOrDefault(c => c.ProjectReportId == reportCheckTimeNearest.Id && c.PlanId == reportPlan.PlanId);


                                    // lấy ra plan report có report gần đó nhất để cập nhật
                                    var plan = plans.FirstOrDefault(c => c.Id == reportPlanNearest.PlanId);
                                    plan.CompletedPercentage = reportPlanNearest.CompletedPercentage;
                                    _projectPlanRepository.Update(plan, trans);
                                    var planContinues = _projectPlanRepository.FindAll(c => c.ProjectId == report.ProjectId, trans).ToList();
                                    var updatePlanResult = CalcParentWhenApproveReportPlan(plan, trans, planContinues, true);
                                    var updateStatus = false;
                                    if (updatePlanResult)
                                    {
                                        plan.CompletedPercentage = reportPlanNearest.CompletedPercentage;
                                        updateStatus = _projectPlanRepository.Update(plan, trans);
                                    }

                                    if (!updateStatus)
                                    {
                                        result = false;
                                        throw new ServiceException("Không thể cập nhật % hoàn thành vào kế hoạch: " + plan.Title);
                                    }
                                }
                                else
                                {
                                    var plan = plans.FirstOrDefault(c => c.Id == reportPlan.PlanId);
                                    plan.CompletedPercentage = 0;
                                    var updatePlanResult = _projectPlanRepository.Update(plan, trans);
                                    var planContinues = _projectPlanRepository.FindAll(c => c.ProjectId == report.ProjectId, trans).ToList();
                                    updatePlanResult = CalcParentWhenApproveReportPlan(plan, trans, planContinues, true);
                                    if (!updatePlanResult)
                                    {
                                        result = false;
                                        throw new ServiceException("Không thể cập nhật % hoàn thành vào kế hoạch: " + plan.Title);
                                    }
                                }
                            }
                        }
                        result = _reportPlanRepository.Delete(reportPlan, trans);
                    }

                    foreach (var item in machineryManpowerReports)
                    {
                        result = _machineryManpowerReportRepository.Delete(item, trans);
                        if (!result) break;
                    }
                }

                if (report.Status != ProjectReportStatus.Approved)
                {
                    foreach (var item in reportPlans)
                    {
                        result = _reportPlanRepository.Delete(item, trans);
                        if (!result) break;
                    }

                    foreach (var item in reportWorkitems)
                    {
                        result = _reportWorkitemRepository.Delete(item, trans);
                        if (!result) break;
                    }

                    foreach (var item in machineryManpowerReports)
                    {
                        result = _machineryManpowerReportRepository.Delete(item, trans);
                        if (!result) break;
                    }
                }
                result = _projectReportRepository.Delete(report, trans);
                if (!result) trans.Rollback();
            }
            catch (Exception ex)
            {
                trans.Rollback();
                throw ex;
            }
            trans.Commit();


            return result;
        }

        public bool DeleteReportPlan(DeleteProjectReportPlanModel projectReportPlanDeleteModel)
        {
            var reportPlan = _reportPlanRepository.FindById(projectReportPlanDeleteModel.Id);
            if (reportPlan != null)
            {
                var report = _projectReportRepository.FindById(reportPlan.ProjectReportId);
                if (report != null)
                {
                    if (report.Status != Enums.ProjectReportStatus.Approved)
                    {
                        var item = projectReportPlanDeleteModel.CloneToModel<DeleteProjectReportPlanModel, ProjectReportPlan>();
                        return _reportPlanRepository.Delete(item);
                    }
                    else throw new ServiceException("Không thể chỉnh sửa báo cáo đã duyệt");
                }
                else throw new ServiceException("Báo cáo không tồn tại");
            }
            else throw new ServiceException("Báo cáo kế hoạch này không tồn tại");
        }

        public bool DeleteReportItem(DeleteWorkitemReportModel model)
        {
            var reportWorkItem = _reportWorkitemRepository.FindById(model.Id);
            if (reportWorkItem != null)
            {
                var report = _projectReportRepository.FindById(reportWorkItem.ProjectReportId);
                if (report != null)
                {
                    if (report.Status != Enums.ProjectReportStatus.Approved)
                    {
                        var item = model.CloneToModel<DeleteWorkitemReportModel, ProjectReportWorkitem>();

                        return _reportWorkitemRepository.Delete(item);
                    }
                    else throw new ServiceException("Không thể chỉnh sửa báo cáo đã duyệt");
                }
                else throw new ServiceException("Báo cáo không tồn tại");
            }
            else throw new ServiceException("Báo cáo hạng mục này không tồn tại");
        }

        /// <summary>
        /// Chỗ này cần sửa để tính
        /// </summary>
        /// <param name="plan"></param>
        /// <param name="transaction"></param>
        /// <param name="plans"></param>
        /// <returns></returns>
        private bool CalcParentWhenApproveReportPlan(ProjectPlan plan, IDbTransaction transaction, List<ProjectPlan> plans, bool? mayBeSmaller = null)
        {
            var success = false;
            //var plan = plans.First(p => p.Id == id);

            var childrens = plans.Where(p => p.ParentId == plan.Id);
            if (childrens.Count() > 0)
            {
                var totalCompleted = 0;
                var totalDuration = 0;
                foreach (var child in childrens)
                {
                    totalCompleted += child.Duration * child.CompletedPercentage.Value;
                    totalDuration += child.Duration;
                }
                if (mayBeSmaller.HasValue)
                {
                    if (mayBeSmaller.Value)
                    {
                        plan.CompletedPercentage = plan.CompletedPercentage > (totalCompleted / totalDuration)
                        ? (totalCompleted / totalDuration) : plan.CompletedPercentage;
                    }

                }
                else
                {
                    plan.CompletedPercentage = plan.CompletedPercentage > (totalCompleted / totalDuration)
                        ? plan.CompletedPercentage : (totalCompleted / totalDuration);
                }


                success = _projectPlanRepository.Update(plan, transaction);
            }
            else success = true;
            if (success)
            {
                var parent = plans.FirstOrDefault(c => c.Id == plan.ParentId);
                if (parent != null)
                {
                    if (mayBeSmaller.HasValue)
                    {
                        if (mayBeSmaller.Value)
                        {
                            CalcParentWhenApproveReportPlan(parent, transaction, plans, true);
                        }
                    }
                    else CalcParentWhenApproveReportPlan(parent, transaction, plans);
                }

            }
            return success;
        }


        public bool ApproveReport(ApproveReportModel approveReportModel)
        {
            var result = false;
            var report = _projectReportRepository.FindById(approveReportModel.ProjectReportId);
            if (report == null) throw new ServiceException("Báo cáo không tồn tại");
            var trans = _projectPlanRepository.BeginTransaction();
            if (trans.Connection.State != ConnectionState.Open) trans.Connection.Open();
            try
            {

                var user = _projectUserRepository.Find(pu => pu.ProjectId == report.ProjectId && pu.UserId == approveReportModel.UserId, trans);
                if (user == null) throw new ServiceException("Bạn chưa tham gia vào dự án này");

                if (report.Status == ProjectReportStatus.Approved) throw new ServiceException("Báo cáo đã được phê duyệt");

                // update plan completed percentage from report
                if (report.ReportType == ProjectReportType.ProgressReport)
                {
                    var reportPlans = _reportPlanRepository.FindAll(rp => rp.ProjectReportId == report.Id, trans);
                    if (reportPlans.Count() > 0)
                    {
                        var planCheckIds = reportPlans.Select(c => c.PlanId).Distinct();
                        var plans = _projectPlanRepository.FindAll(c => planCheckIds.Contains(c.Id), trans);
                        foreach (var reportPlan in reportPlans)
                        {
                            var plan = plans.FirstOrDefault(c => c.Id == reportPlan.PlanId);
                            if (plan != null)
                            {
                                if (plan.CompletedPercentage > reportPlan.CompletedPercentage)
                                    throw new ServiceException("Tiến độ " + plan.Title + " không được thấp hơn lần trước");
                                plan.CompletedPercentage = reportPlan.CompletedPercentage;
                                var updatePlanResult = _projectPlanRepository.Update(plan, trans);
                                var planContinues = _projectPlanRepository.FindAll(c => c.ProjectId == report.ProjectId, trans).ToList();
                                updatePlanResult = CalcParentWhenApproveReportPlan(plan, trans, planContinues);
                                if (!updatePlanResult) throw new ServiceException("Không thể cập nhật % hoàn thành vào kế hoạch: " + plan.Title);
                            }
                        }
                    }

                    var reportWorkItems = _reportWorkitemRepository.FindAll(rp => rp.ProjectReportId == report.Id, trans).ToList();
                    if (reportWorkItems.Count() > 0)
                    {
                        var workitemCheckIds = reportWorkItems.Select(c => c.WorkitemId);
                        var workitems = _workItemRepository.FindAll(c => workitemCheckIds.Contains(c.Id), trans);
                        reportWorkItems.ForEach(reportWorkitem =>
                        {
                            if (reportWorkitem != null)
                            {
                                var workItem = workitems.FirstOrDefault(c => c.Id == reportWorkitem.WorkitemId);
                                if (workItem != null)
                                {
                                    var totalNumber = reportWorkitem.CompletedNumber + workItem.CompletedNumberTotal;
                                    if (totalNumber <= workItem.Quantity)
                                    {
                                        var workitemCompany = _workItemCompanyRepository.Find(c => c.CompanyId == reportWorkitem.CompanyId
                                            && c.WorkItemId == reportWorkitem.WorkitemId, trans);
                                        workitemCompany.CompletedNumber += reportWorkitem.CompletedNumber;
                                        workitemCompany.CompletePlace = reportWorkitem.CompletePlace;
                                        workitemCompany.UnderPlace = reportWorkitem.UnderPlace;
                                        workitemCompany.LastUpdateContentReport = reportWorkitem.Content;
                                        workitemCompany.LastUpdateCompletedNumber = reportWorkitem.CompletedNumber;
                                        _workItemCompanyRepository.Update(workitemCompany, trans);
                                        workItem.CompletedNumberTotal += reportWorkitem.CompletedNumber;
                                        var updateWorkItem = _workItemRepository.Update(workItem, trans);
                                        if (!updateWorkItem) throw new ServiceException("Không thể cập nhật số lượng hoàn thành vào hạng mục ID: " + workItem.Id);
                                    }
                                    else throw new ServiceException("Báo cáo sai hạng mục ID: " + workItem.Id + ", tổng số hạng mục hoàn thành lớn hơn " +
                                        "tổng số hạng mục ban đầu");
                                }
                            }
                        });
                    }

                    var manMachineReports = _machineryManpowerReportRepository.FindAll(c => c.ReportId == report.Id, trans).ToList();
                    if (manMachineReports.Count() > 0)
                    {
                        manMachineReports.ForEach(m =>
                        {

                        });
                    }
                }

                report.Status = ProjectReportStatus.Approved;
                report.Reason = string.Empty;
                result = _projectReportRepository.Update(report, trans);
                if (!result)
                {
                    trans.Rollback();
                }
            }
            catch (Exception ex)
            {
                trans.Rollback();
                throw ex;
            }
            trans.Commit();

            var userApprove = _userRepository.FindById(approveReportModel.UserId);
            var project = _projectRepository.FindById(report.ProjectId);
            if (result)
            {
                var notificationModel = new ApproveReportEventModel
                {
                    Type = EventType.ApproveReport,
                    UserApproveId = approveReportModel.UserId,
                    UserApproveName = userApprove.FullName ?? userApprove.Email,
                    ProjectHashId = _hashidService.Encode(project.Id),
                    ProjectName = project.Name
                };
                var userIds = new List<int>();
                userIds.Add(report.CreatedBy);
                _notificationService.AddNotification(userIds, notificationModel);
            };
            return result;
        }


        public bool RejectReport(RejectReportModel rejectReportModel)
        {
            var report = _projectReportRepository.FindById(rejectReportModel.ProjectReportId);
            var user = _projectUserRepository.Find(pu => pu.ProjectId == report.ProjectId && pu.UserId == rejectReportModel.UserId);
            if (user == null) throw new ServiceException("Bạn chưa tham gia vào dự án này");

            report.Status = ProjectReportStatus.Rejected;
            report.Reason = rejectReportModel.Reason;

            var project = _projectRepository.FindById(report.ProjectId);
            var success = _projectReportRepository.Update(report);
            if (success)
            {
                var userReject = _userRepository.FindById(rejectReportModel.UserId);
                var notification = new RejectReportEventModel()
                {
                    Type = EventType.RejectReport,
                    UserRejectId = userReject.Id,
                    UserRejectName = userReject.FullName ?? userReject.Email,
                    ProjectHashId = _hashidService.Encode(project.Id),
                    ProjectName = project.Name,
                };
                var userIds = new List<int>();
                userIds.Add(report.CreatedBy);
                _notificationService.AddNotification(userIds, notification);
            }
            return success;
        }

        IEnumerable<ProjectPlanModel> _plans;
        public int CalcProgress(int projectId, int? planNo, IEnumerable<ProjectPlan> plans)
        {
            var planFilter = plans.Where(pp => pp.No == planNo
                && pp.Duration > 0 && (pp.ParentId == null || pp.ParentId == Guid.Empty));
            _plans = DomainMaps.Mapper.Map<List<ProjectPlanModel>>(planFilter);

            var rootPlan = _plans.First(p => p.ParentId == Guid.Empty);

            var percent = Calc(rootPlan.Id);
            return percent;
        }

        private int Calc(Guid id)
        {
            var plan = _plans.First(p => p.Id == id);
            if (plan.CompletedPercentage > 0) return plan.CompletedPercentage.Value;

            var children = _plans.Where(p => p.ParentId == id);

            if (children.Count() == 0) return 0;

            var totalCompleted = 0;
            var totalDuration = 0;
            foreach (var child in children)
            {
                var completed = Calc(child.Id);
                totalCompleted += child.Duration * completed;
                totalDuration += child.Duration;
            }

            return totalCompleted / totalDuration;
        }

        private int CalCurrentPlan(Guid id)
        {
            var existPlan = _projectPlanRepository.FindById(id);
            var plans = _projectPlanRepository.FindAll(c => c.ProjectId == existPlan.ProjectId && c.No == existPlan.No && c.Duration > 0);
            _plans = DomainMaps.Mapper.Map<List<ProjectPlanModel>>(plans);
            var rootPlan = _plans.FirstOrDefault();
            var percent = Calc(rootPlan.Id);
            return percent;
        }

        public int CalculatorPercentageTimeNow(DateTime startDate, DateTime endDate, DateTime timeCurrent)
        {
            int result = 0;
            if (timeCurrent < startDate)
                result = 0;
            if (timeCurrent > endDate)
                result = 100;
            if (startDate <= timeCurrent && timeCurrent <= endDate)
            {
                var SC = (timeCurrent - startDate).TotalDays;
                var SE = (endDate - startDate).TotalDays;
                result = (int)((SC / (float)SE) * 100);
            }

            return result;
        }

        public ProgressResult CheckProgressProjectExpected(CheckProjectProgressModel checkProjectProgress)
        {
            var projectId = _hashidService.Decode(checkProjectProgress.hashId);
            var existProject = _projectRepository.FindById(projectId);
            if (existProject != null)
            {
                checkProjectProgress.PlanNo = checkProjectProgress.PlanNo ?? existProject.LastPlanNo;
                var plans = _projectPlanRepository.FindAll(c => c.ProjectId == projectId && c.No == checkProjectProgress.PlanNo);
                ProgressStatus? message = null;
                int percentageDiff = 0;
                var percentageProgress = 0;
                if (plans.Count() > 0)
                {
                    var plantEntity = plans.Where(c => c.ParentId == null || c.ParentId == Guid.Empty).FirstOrDefault();
                    if (plantEntity.No == 0 || existProject.Status == ProjectStatus.Init)
                    {
                        message = ProgressStatus.NotPlan;
                    }
                    else
                    {
                        if (plantEntity.StartDate > DateTime.Now) message = ProgressStatus.NotImplemented;
                        else
                        {
                            percentageProgress = CalcProgress(projectId, checkProjectProgress.PlanNo, plans);
                            var percentageTimeNow = CalculatorPercentageTimeNow(plantEntity.StartDate, plantEntity.EndDate, DateTime.Now);
                            var beyound = percentageProgress - percentageTimeNow;
                            if (beyound == 0) message = ProgressStatus.OnSchedule;
                            if (beyound < 0)
                            {
                                message = ProgressStatus.SlowProgress;
                                percentageDiff = percentageTimeNow - percentageProgress;
                            }
                            if (beyound > 0)
                            {
                                message = ProgressStatus.BeyondProgress;
                                percentageDiff = percentageProgress - percentageTimeNow;
                            }
                            existProject.ProgressStatus = message;
                            _projectRepository.Update(existProject);
                        }
                    }
                }
                else
                {
                    message = ProgressStatus.NotPlan;
                }
                return new ProgressResult
                {
                    PercentageProgress = percentageProgress,
                    Status = message,
                    PercentageDiff = percentageDiff
                };
            }
            else throw new ServiceException("Dự án không tồn tại");
        }

        public NumberProjectProgressResult CheckAllProjectProgress()
        {
            var allProjects = _projectRepository.FindAll(c => c.Status != ProjectStatus.Stop);
            var allProjectIds = allProjects.Select(c => c.Id);
            var beyondProgress = 0; var slow = 0; var onschedule = 0; var notPlan = 0; var notImplemented = 0;


            var plans = _projectPlanRepository.FindAll(c => allProjectIds.Contains(c.ProjectId));
            foreach (var project in allProjects)
            {
                if (project.LastPlanNo == 0) notPlan++;
                else
                {
                    var checkProjectProgress = new CheckProjectProgressModel()
                    {
                        hashId = _hashidService.Encode(project.Id),
                        PlanNo = project.LastPlanNo
                    };
                    if (CheckProgressProjectExpected(checkProjectProgress).Status == ProgressStatus.NotImplemented)
                        notImplemented++;
                    if (CheckProgressProjectExpected(checkProjectProgress).Status == ProgressStatus.OnSchedule)
                        onschedule++;
                    if (CheckProgressProjectExpected(checkProjectProgress).Status == ProgressStatus.BeyondProgress)
                        beyondProgress++;
                    if (CheckProgressProjectExpected(checkProjectProgress).Status == ProgressStatus.SlowProgress)
                        slow++;
                }
            }
            return new NumberProjectProgressResult()
            {
                NotPlan = notPlan,
                OnSchedule = onschedule,
                NotImplemented = notImplemented,
                BeyondProgress = beyondProgress,
                Slow = slow
            };
        }

        public ProgressResult CheckProgressPlanExpected(Guid planId)
        {
            var planEntities = _projectPlanRepository.FindAll(c => c.Id == planId);
            var planEntity = planEntities.FirstOrDefault();
            if (planEntities != null)
            {
                ProgressStatus? message = null;
                int percentageDiff = 0;
                var percentageProgress = 0;
                if (planEntity.StartDate.ToUniversalTime() > DateTime.UtcNow) message = ProgressStatus.NotImplemented;
                else
                {
                    _plans = DomainMaps.Mapper.Map<List<ProjectPlanModel>>(planEntities);
                    var _plan = _plans.FirstOrDefault();
                    percentageProgress = CalCurrentPlan(_plan.Id);
                    var percentageTimeNow = CalculatorPercentageTimeNow(planEntity.StartDate.ToUniversalTime(),
                        planEntity.EndDate.ToUniversalTime(), DateTime.UtcNow);
                    var beyound = percentageProgress - percentageTimeNow;
                    if (beyound < 5 && beyound >= 0) message = ProgressStatus.OnSchedule;
                    if (beyound < 0)
                    {
                        message = ProgressStatus.SlowProgress;
                        percentageDiff = percentageTimeNow - percentageProgress;
                    }
                    if (beyound >= 5)
                    {
                        message = ProgressStatus.BeyondProgress;
                        percentageDiff = percentageProgress - percentageTimeNow;
                    }
                }
                return new ProgressResult
                {
                    PercentageProgress = percentageProgress,
                    Status = message,
                    PercentageDiff = percentageDiff
                };
            }
            else throw new ServiceException("Kế hoạch không tồn tại");
        }

        public (bool, List<int>) CreateReportImage(MediaModel mediaModel, int userId)
        {
            var existUserCurrent = _projectUserRepository.Find(c => c.UserId == userId && c.ProjectId == mediaModel.ProjectId);
            if (existUserCurrent != null)
            {
                mediaModel.PictureType = ProjectPictureType.HinhAnhBaoCao;
                return _fileStorageService.AddProjectImages(mediaModel);
            }
            else throw new ServiceException("Bạn không thuộc dự án này nên không có quyền thao tác");
        }

        public bool CheckRightpersonReports(int userId, int reportId)
        {
            var user = _projectReportRepository.FindById(reportId);
            return (user.CreatedBy == userId) ? true : false;
        }

        public (IEnumerable<WorkitemPerMonthModel>, IEnumerable<Workitem>) WorkitemReportPerMonth(int year)
        {
            var reports = _projectReportRepository.FindAll(c => c.Status == ProjectReportStatus.Approved);

            List<int> a = new List<int> { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12 };
            var modelResults = new List<WorkitemPerMonthModel>();
            a.Sort();
            var reportIds = reports.Select(c => c.Id);
            var reportWorkitems = _reportWorkitemRepository.FindAll(c => reportIds.Contains(c.ProjectReportId))
                .OrderByDescending(c => c.Id);

            var projects = _projectRepository.FindAll(c => c.Status != ProjectStatus.Stop
                && c.Status != ProjectStatus.Init).OrderByDescending(c => c.Id);
            var projectIds = projects.Select(c => c.Id);

            var workitems = _workItemRepository.FindAll(c => projectIds.Contains(c.ProjectId));
            var workitemIds = workitems.Select(c => c.Id);
            var estimateWorkitem = _estimateRepository.FindAll(c => workitemIds.Contains(c.WorkitemId)
                            && c.Year == year && c.Type == EstimationType.EstimationMonthComplete);

            foreach (var month in a)
            {
                var filterReports = reports.Where(f => f.CreatedOnUtc.Month == month && f.CreatedOnUtc.Year == year);
                decimal totalYieldAll = 0;
                if (filterReports.Count() > 0)
                {
                    foreach (var report in filterReports)
                    {
                        if (report.Id == 1413)
                        {

                        }
                        var reportWorkitemNews = reportWorkitems.Where(c => c.ProjectReportId == report.Id).ToList();
                        decimal totalYieldInReport = 0;
                        foreach (var reportWorkitem in reportWorkitemNews)
                        {
                            var workitem = workitems.FirstOrDefault(c => c.Id == reportWorkitem.WorkitemId);
                            if (workitem != null)
                            {
                                if (workitem.UnitPrice.HasValue && workitem.Quantity.HasValue)
                                {
                                    totalYieldInReport += workitem.UnitPrice.Value * reportWorkitem.CompletedNumber;
                                }
                            }
                        }
                        totalYieldAll += totalYieldInReport;
                    }
                }
                if (month == 2)
                {
                };
                //thử
                decimal totalEstimate = 0;
                decimal totalEstimateProject;
                foreach (var project in projects)
                {
                    totalEstimateProject = 0;
                    var workitemEntityNews = workitems.Where(c => c.ProjectId == project.Id);
                    foreach (var workitemEntity in workitemEntityNews)
                    {
                        var estimate = estimateWorkitem.FirstOrDefault(c => c.Month == month && c.WorkitemId == workitemEntity.Id);
                        if (estimate != null && workitemEntity.Quantity.HasValue && workitemEntity.UnitPrice.HasValue)
                            totalEstimateProject += estimate.Quantity * (decimal)workitemEntity.UnitPrice;
                    }
                    totalEstimate += totalEstimateProject;
                }

                var modelResult = new WorkitemPerMonthModel();
                modelResult.Title = "T" + month;
                modelResult.WorkitemCompleted = NumberHelper.RoundDown(totalYieldAll, 3);
                modelResult.EstimateNumber = NumberHelper.RoundDown(totalEstimate, 3);
                modelResults.Add(modelResult);
            }
            return (modelResults.ToArray(), workitems);
        }

        public IEnumerable<WorkitemPerQuarterModel> WorkitemReportPerQuarter(int year)
        {
            var result = new List<WorkitemPerQuarterModel>();
            var projects = _projectRepository.FindAll(c => c.Status != ProjectStatus.Stop
               && c.Status != ProjectStatus.Init).OrderByDescending(c => c.Id);
            var fromMonth = WorkitemReportPerMonth(year);
            var complete = fromMonth.Item1;
            var workitems = fromMonth.Item2;
            var workitemIds = workitems.Select(c => c.Id);
            var estimateWorkitem = _estimateRepository.FindAll(c => workitemIds.Contains(c.WorkitemId)
                            && c.Year == year && c.Type == EstimationType.EstimationQuarterComplete);
            List<int> a = new List<int> { 1, 2, 3, 4 };
            a.Sort();
            foreach (var quarter in a)
            {
                var workitemCompleteInMonth = new List<WorkitemPerMonthModel>();
                if (quarter == 1)
                {
                    workitemCompleteInMonth = complete.Where(c => c.Title == "T1" || c.Title == "T2" || c.Title == "T3").ToList();
                }
                if (quarter == 2)
                {
                    workitemCompleteInMonth = complete.Where(c => c.Title == "T4" || c.Title == "T5" || c.Title == "T6").ToList();
                }

                if (quarter == 3)
                {
                    workitemCompleteInMonth = complete.Where(c => c.Title == "T7" || c.Title == "T8" || c.Title == "T9").ToList();
                }
                if (quarter == 4)
                {
                    workitemCompleteInMonth = complete.Where(c => c.Title == "T10" || c.Title == "T11" || c.Title == "T12").ToList();
                }
                var addModel = new WorkitemPerQuarterModel
                {
                    Title = "Q" + quarter,
                    WorkitemCompleted = workitemCompleteInMonth.Sum(c => c.WorkitemCompleted)

                };


                decimal totalEstimate = 0;
                decimal totalEstimateProject;
                foreach (var project in projects)
                {
                    totalEstimateProject = 0;
                    var workitemEntityNews = workitems.Where(c => c.ProjectId == project.Id);
                    foreach (var workitemEntity in workitemEntityNews)
                    {
                        var estimate = estimateWorkitem.FirstOrDefault(c => c.Quarter == quarter && c.WorkitemId == workitemEntity.Id);
                        if (estimate != null && workitemEntity.Quantity.HasValue && workitemEntity.UnitPrice.HasValue)
                            totalEstimateProject += estimate.Quantity * (decimal)workitemEntity.UnitPrice;
                    }
                    totalEstimate += totalEstimateProject;
                }
                addModel.EstimateNumber = totalEstimate;
                result.Add(addModel);
            }

            return result;
        }


        public IEnumerable<BinaryFileStorageModel> ListReportImageByReportId(int projectReportId)
        {
            var report = _projectReportRepository.FindById(projectReportId);
            if (report == null) throw new ServiceException("Báo cáo không tồn tại");

            var reportImages = _reportImageRepository.FindAll(c => c.ProjectReportId == projectReportId);

            IEnumerable<File> fileExists = new List<File>();
            var result = new List<BinaryFileStorageModel>();
            if (reportImages.Count() > 0)
            {
                var reportImageIds = reportImages.Select(c => c.FileId);
                fileExists = _fileRepository.FindAll(c => reportImageIds.Contains(c.Id));
                var fileStorageIds = fileExists.Select(c => c.StorageId);
                var binaryFiles = _binaryFileStorageRepository.FindAll(c => fileStorageIds.Contains(c.Id));
                foreach (var item in reportImages)
                {
                    var file = fileExists.FirstOrDefault(c => c.Id == item.FileId);
                    var binarFile = binaryFiles.FirstOrDefault(c => c.Id == file.StorageId);
                    result.Add(binarFile.CloneToModel<BinaryFileStorage, BinaryFileStorageModel>());
                };
            }

            return result;
        }

        public IEnumerable<BinaryFileStorageModel> ListImageByWorkitemReportId(int workitemReportId)
        {
            var result = new List<BinaryFileStorageModel>();
            var reportImages = _reportImageRepository.FindAll(c => c.ReportWorkitemId == workitemReportId);
            if (reportImages.Count() > 0)
            {
                var fileIdInReportImages = reportImages.Select(c => c.FileId);
                var files = _fileRepository.FindAll(c => fileIdInReportImages.Contains(c.Id));
                var storageIdInFiles = files.Select(c => c.StorageId);
                var binaries = _binaryFileStorageRepository.FindAll(c => storageIdInFiles.Contains(c.Id));
                foreach (var item in reportImages)
                {
                    var file = files.FirstOrDefault(c => c.Id == item.FileId);
                    var binarFile = binaries.FirstOrDefault(c => c.Id == file.StorageId);
                    result.Add(binarFile.CloneToModel<BinaryFileStorage, BinaryFileStorageModel>());
                };
            }
            return result;
        }

        public void DeleteAllComponentInReport(int reportId)
        {
            IEnumerable<File> files = null;
            IEnumerable<BinaryFileStorage> binaryFiles = null;

            var trans = _reportWorkitemRepository.BeginTransaction();
            if (trans.Connection.State != ConnectionState.Open) trans.Connection.Open();
            try
            {
                var workitemReports = _reportWorkitemRepository.FindAll(c => c.ProjectReportId == reportId, trans);
                if (workitemReports.Count() > 0)
                {
                    workitemReports.ToList().ForEach(x => { _reportWorkitemRepository.Delete(x, trans); });
                    var reportImages = _reportImageRepository.FindAll(c => c.ProjectReportId == reportId, trans);

                    if (reportImages.Count() > 0)
                    {
                        var fileIdInReportImages = reportImages.Select(c => c.FileId);
                        var fileExists = _fileRepository.FindAll(c => fileIdInReportImages.Contains(c.Id), trans);

                        reportImages.ToList().ForEach(image =>
                        {
                            var file = fileExists.FirstOrDefault(c => c.Id == image.FileId);
                            files.ToList().Add(file);
                            binaryFiles.ToList().Add(_binaryFileStorageRepository.FindById(file.Id, trans));
                        });

                        reportImages.ToList().ForEach(x => { _reportImageRepository.Delete(x, trans); });
                        files.ToList().ForEach(x => { _fileRepository.Delete(x, trans); });
                        binaryFiles.ToList().ForEach(x => { _binaryFileStorageRepository.Delete(x, trans); });
                    }
                }
                var planReports = _reportPlanRepository.FindAll(c => c.ProjectReportId == reportId, trans);
                if (planReports.Count() > 0)
                {
                    planReports.ToList().ForEach(x => { _reportPlanRepository.Delete(x, trans); });
                }
            }
            catch (Exception ex)
            {
                trans.Rollback();
                throw ex;
            }
            trans.Commit();

        }

        public int CloneReport(CloneReportModel reportModel)
        {
            int result = 0;
            var success = false;
            var trans = _projectReportRepository.BeginTransaction();
            try
            {
                var report = _projectReportRepository.FindById(reportModel.ReportId, trans);
                if (report == null) throw new ServiceException("Bạn cần lập báo cáo để cập nhật");
                else
                {
                    var reportNew = new ProjectReport
                    {
                        ReportType = report.ReportType,
                        ProjectId = report.ProjectId,
                        CreatedBy = reportModel.UserId,
                        CreatedOnUtc = DateTime.UtcNow,
                        Status = ProjectReportStatus.Draft,
                        MorningWeather = report.MorningWeather,
                        AfternoonWeather = report.AfternoonWeather,
                        Note = report.Note,
                        Reason = report.Reason
                    };

                    var insertReport = _projectReportRepository.Insert(reportNew, trans);
                    if (insertReport)
                    {
                        success = insertReport;
                        var workitemReports = _reportWorkitemRepository.FindAll(c => c.ProjectReportId == report.Id, trans);
                        if (workitemReports.Count() > 0)
                        {
                            var workitemCheckIds = workitemReports.Select(c => c.WorkitemId);
                            var workitemExists = _workItemRepository.FindAll(c => workitemCheckIds.Contains(c.Id), trans);
                            workitemReports.ToList().ForEach(wr =>
                            {
                                var workitemExist = workitemExists.FirstOrDefault(c => c.Id == wr.WorkitemId);
                                if (wr.CompletedNumber + workitemExist.CompletedNumberTotal > workitemExist.Quantity)
                                    wr.CompletedNumber = workitemExist.Quantity.Value - workitemExist.CompletedNumberTotal.Value;
                                else
                                    wr.CompletedNumber = wr.CompletedNumber;
                                // wr.UnderContructNumber = 0;
                                wr.ProjectReportId = reportNew.Id;
                                //wr.Place = string.Empty;
                                success = _reportWorkitemRepository.Insert(wr, trans);
                                if (!success) trans.Rollback();
                            });
                        }
                        var planReports = _reportPlanRepository.FindAll(c => c.ProjectReportId == report.Id, trans);
                        if (planReports.Count() > 0)
                        {
                            planReports.ToList().ForEach(pr =>
                            {
                                pr.CompletedPercentage = 0;
                                pr.ProjectReportId = reportNew.Id;
                                success = _reportPlanRepository.Insert(pr, trans);
                                if (!success) trans.Rollback();
                            });
                        }
                        result = reportNew.Id;
                    }
                    else trans.Rollback();
                }
            }

            catch (Exception)
            {
                trans.Rollback();
                throw;
            }
            trans.Commit();
            return result;
        }

        public byte[] WriteTextImage(string image)
        {
            var decodeBinary = Convert.FromBase64String(image);
            var bitmap = ToBitmap(decodeBinary);
            var height = bitmap.Height;
            var width = bitmap.Width;

            using (var graphics = Graphics.FromImage(bitmap))
            {
                using (var arialFont = new Font("Arial", 13))
                {
                    graphics.DrawString("lat: ", arialFont, new SolidBrush(Color.DarkGoldenrod), new PointF(width / 2, 30));
                }
            }

            return ConvertBitMapToByteArray(bitmap);
        }

        public static Bitmap ToBitmap(byte[] byteArray)
        {
            using (var ms = new MemoryStream(byteArray))
            {
                var img = (Bitmap)Image.FromStream(ms);
                return img;
            }
        }

        public byte[] ConvertBitMapToByteArray(Bitmap bitmap)
        {
            byte[] result = null;

            if (bitmap != null)
            {
                using (var stream = new MemoryStream())
                {
                    bitmap.Save(stream, bitmap.RawFormat);
                    result = stream.ToArray();
                }
            }

            return result;
        }

        public StatisticsHumanResourceModel StatisticalHumanResource(CheckStatiscalHumanResource check)
        {
            if (!check.DateDetermine.HasValue) throw new ServiceException("Chưa chọn ngày để tổng hợp nhân lực thi công");

            var result = new StatisticsHumanResourceModel();
            var projects = _projectRepository.FindAll(c => c.Status == ProjectStatus.Approved);
            if (projects.Count() == 0) throw new ServiceException("Chưa có dự án nào được phê duyệt");
            var projectIds = projects.Select(c => c.Id);

            var companies = _companyRepository.FindAll();
            if (companies.Count() == 0) throw new ServiceException("Chưa có công ty nào");
            var companyIds = companies.Select(c => c.Id);

            var companyProjects = _projectCompanyRepository.FindAll(c => projectIds.Contains(c.ProjectId) && companyIds.Contains(c.CompanyId));
            if (companyProjects.Count() == 0) throw new ServiceException("Chưa có công ty nào nào tham gia dự án được phê duyệt");
            var companyInAllProjects = companies.Where(c => companyProjects.Select(a => a.CompanyId).Distinct().Contains(c.Id));

            var reports = _projectReportRepository.FindAll(c => c.CreatedOnUtc <= check.DateDetermine.Value.Date.AddDays(2).AddTicks(-1).ToUniversalTime()
                                                                && c.Status == ProjectReportStatus.Approved);
            if (reports.Count() == 0) throw new ServiceException("Chưa tồn tại báo cáo nào");
            var reportIds = reports.Select(c => c.Id);

            var humanResources = _machineryManpowerReportRepository.FindAll(c => reportIds.Contains(c.ReportId));



            foreach (var project in projects)
            {
                if (project.Id == 4228)
                {

                }
                var humanProject = new StatisticsHumanByProjectModel
                {
                    ProjectId = project.Id,
                    HashId = _hashidService.Encode(project.Id),
                    ProjectName = project.Name
                };

                var companyInProjectIds = companyProjects.Where(a => a.ProjectId == project.Id).Select(c => c.CompanyId).Distinct();
                var companyInProjects = companyInAllProjects.Where(c => companyInProjectIds.Contains(c.Id));
                var reportInProjects = reports.Where(c => c.ProjectId == project.Id);
                var reportTimeMax = reportInProjects.FirstOrDefault(c => c.CreatedOnUtc == reportInProjects.Select(b => b.CreatedOnUtc).Max());

                var manpowerInProjects = humanResources.Where(c => c.ProjectId == project.Id
                    && (c.Type.GetAttributeOfType<MachineryManpowerDetailAttribute>().Group == MachineryManpowerGroup.Manpower
                    || c.Type.GetAttributeOfType<MachineryManpowerDetailAttribute>().Group == MachineryManpowerGroup.MorePartManpower));
                var manpowerFilters = manpowerInProjects.Where(c => c.ReportId == reportTimeMax.Id);

                //var manpowerExacts = new List<MachineryManpowerReport>();


                //foreach (var companyExact in companyInProjects)
                //{


                //    foreach (var report in reportInProjects.OrderByDescending(c => c.CreatedOnUtc))
                //    {
                //        if (manpowerInProjects.Any(c => c.CompanyId == companyExact.Id && c.ReportId == report.Id))
                //        {
                //            var t = manpowerInProjects.Where(c => c.CompanyId == companyExact.Id && c.ReportId == report.Id);
                //            manpowerExacts.AddRange(manpowerInProjects.Where(c => c.CompanyId == companyExact.Id && c.ReportId == report.Id));
                //            break;
                //        }
                //    }

                //}


                foreach (var manpower in manpowerFilters)
                {
                    var categoryHumanResource = new ProjectCategoryHumanResource
                    {
                        Code = manpower.Type,
                        Title = manpower.Title
                    };


                    void AddHumanResource()
                    {
                        foreach (var company in companies)
                        {
                            if (companyInProjects.Count() > 0)
                            {
                                if (companyInProjects.Where(c => c.Id == company.Id).Count() > 0 && company.Id == manpower.CompanyId)
                                {
                                    //var tryCondition = humanProject.ProjectCategoryHumanResources.FirstOrDefault(c =>
                                    //    c.HumanResourceByCompanies.Where(a => a.CompanyId == company.Id).Count() > 0 && c.Code == manpower.Type);

                                    if (humanProject.ProjectCategoryHumanResources.FirstOrDefault(c =>
                                        c.HumanResourceByCompanies.Where(a => a.CompanyId == company.Id).Count() > 0 && c.Code == manpower.Type) == null)
                                    {
                                        categoryHumanResource.HumanResourceByCompanies.Add(new HumanResourceByCompanyModel
                                        {
                                            CompanyId = company.Id,
                                            CompanyName = company.Name,
                                            Quantity = manpower.Quantity
                                        });
                                    }
                                    else
                                    {
                                        if (manpower.Type.GetAttributeOfType<MachineryManpowerDetailAttribute>().Group == MachineryManpowerGroup.Manpower)
                                        {
                                            var update = humanProject.ProjectCategoryHumanResources.FirstOrDefault(c =>
                                                c.HumanResourceByCompanies.Where(a => a.CompanyId == company.Id).Count() > 0 && c.Code == manpower.Type
                                                && c.Code.GetAttributeOfType<MachineryManpowerDetailAttribute>().Group == MachineryManpowerGroup.Manpower);

                                            update.HumanResourceByCompanies.FirstOrDefault(c => c.CompanyId == company.Id).Quantity = manpower.Quantity;
                                        }

                                        if (manpower.Type.GetAttributeOfType<MachineryManpowerDetailAttribute>().Group == MachineryManpowerGroup.MorePartManpower)
                                        {
                                            var update = humanProject.ProjectCategoryHumanResources.FirstOrDefault(c =>
                                                c.HumanResourceByCompanies.Where(a => a.CompanyId == company.Id).Count() > 0 && c.Code == manpower.Type
                                                && c.Code.GetAttributeOfType<MachineryManpowerDetailAttribute>().Group == MachineryManpowerGroup.MorePartManpower
                                                && c.Title != manpower.Title);
                                            if (update != null)
                                            {
                                                update.HumanResourceByCompanies.FirstOrDefault(c => c.CompanyId == company.Id).Quantity = manpower.Quantity;
                                            }
                                        }
                                    }
                                }
                                else
                                {
                                    categoryHumanResource.HumanResourceByCompanies.Add(new HumanResourceByCompanyModel
                                    {
                                        CompanyId = company.Id,
                                        CompanyName = company.Name
                                    });
                                }
                            }
                        }
                    }


                    void AddInfoCategory(MachineryManpowerType type)
                    {

                        humanProject.ProjectCategoryHumanResources.Add(categoryHumanResource);

                    }

                    if (manpower.Type.GetAttributeOfType<MachineryManpowerDetailAttribute>().Group == MachineryManpowerGroup.Manpower)
                    {
                        if (humanProject.ProjectCategoryHumanResources.Where(c => c.Code == manpower.Type).Count() == 0)
                        {
                            AddInfoCategory(manpower.Type);
                        }
                        AddHumanResource();
                    }

                    if (manpower.Type.GetAttributeOfType<MachineryManpowerDetailAttribute>().Group == MachineryManpowerGroup.MorePartManpower)
                    {
                        if (humanProject.ProjectCategoryHumanResources.Where(c => c.Code == manpower.Type && manpower.Title == c.Title).Count() == 0)
                        {
                            AddInfoCategory(manpower.Type);
                        }
                        AddHumanResource();
                    }

                }
                result.StatisticsHumanByProjects.Add(humanProject);
            }

            foreach (var item in companies)
            {
                result.Companies.Add(new CompanyModel
                {
                    Id = item.Id,
                    Name = item.Name
                });
            }

            return result;
        }

        public AutoFillReportWithCompanyModel AuToFillReportWithCompany(string hashId, int? companyId)
        {
            var result = new AutoFillReportWithCompanyModel();
            if (string.IsNullOrEmpty(hashId)) throw new ServiceException("Yêu cầu nhập dự án");
            if (!companyId.HasValue) throw new ServiceException("Yêu cầu nhập công ty");

            var projectId = _hashidService.Decode(hashId);
            var project = _projectRepository.FindById(projectId);
            if (project == null) throw new ServiceException("Dự án không tồn tại");
            if (project.Status != ProjectStatus.Approved) throw new ServiceException("Dự án chưa được phê duyệt");


            var company = _companyRepository.FindById(companyId.Value);
            if (company == null) throw new ServiceException("Công ty không tồn tại");

            var projectCompany = _projectCompanyRepository.Find(c => c.CompanyId == companyId && c.ProjectId == projectId);
            if (projectCompany == null) throw new ServiceException("Công ty chưa tham gia dự án này");

            var workitems = _workItemRepository.FindAll(c => c.ProjectId == projectId);

            var workitemDivides = DeterminedPlaceType(projectId);
            

            if (workitems.Count() > 0)
            {
                result.CompanyId = companyId.Value;
                var workitemIds = workitems.Select(c => c.Id);
                var workitemAssignments = _workItemCompanyRepository.FindAll(c => c.CompanyId == companyId && workitemIds.Contains(c.WorkItemId));

                var workitemAssignmentIds = workitemAssignments.Select(c => c.Id);
                var reportWorkitems = _reportWorkitemRepository.FindAll(c => workitemAssignmentIds.Contains(c.WorkitemId) && c.CompanyId == company.Id);

                foreach (var item in workitemAssignments)
                {
                    if (workitemAssignments.FirstOrDefault(c => c.WorkItemId == item.WorkItemId).AssignedNumber > 0)
                    {
                        var addFill = new FillContentReportByCompany
                        {
                            CompanyId = companyId.Value,
                            WorkitemId = item.WorkItemId,
                            WorkitemName = workitems.FirstOrDefault(c => c.Id == item.WorkItemId).Name,
                            CalculationUnit = workitems.FirstOrDefault(c => c.Id == item.WorkItemId).CalculationUnit,
                            AssignmentNumber = workitemAssignments.FirstOrDefault(c => c.WorkItemId == item.WorkItemId).AssignedNumber,
                            NumberAccumulatedWithCompany = item.CompletedNumber.Value,
                            UnderPlace = item.UnderPlace,
                            CompletePlace = item.CompletePlace,
                            Content = item.LastUpdateContentReport
                        };
                        foreach (var workitemDivide in workitemDivides)
                        {
                            if (workitemDivide.WorkitemCategory.Id == addFill.WorkitemId || workitemDivide.WorkItemSmallerModels.Any(c=>c.Id == addFill.WorkitemId))
                            {
                                addFill.PlaceType = workitemDivide.PlaceType;
                            }
                        }
                        result.Workitems.Add(addFill);
                    }
                }
            }
            return result;
        }

        public IEnumerable<ShowReportOnDardboardModel> ShowReportInDardboard(int userId)
        {
            throw new NotImplementedException();
        }

        public bool MarkedReadReport(int reportId, int userId)
        {
            var result = false;
            var report = _projectReportRepository.FindById(reportId);
            var listUsers = new List<int>();
            if (report == null) throw new ServiceException("Báo cáo không tồn tại");
            if (!string.IsNullOrEmpty(report.MarkedRead))
            {
                var desezialize = JsonConvert.DeserializeObject<List<int>>(report.MarkedRead);
                if (!desezialize.Contains(userId))
                    listUsers.Add(userId);
            }
            else
            {
                listUsers.Add(userId);
            }
            report.MarkedRead = JsonConvert.SerializeObject(listUsers);
            result = _projectReportRepository.Update(report);
            return result;
        }









        ///// <summary>
        ///// 15/9/18  ---- Trả về danh sách list Plan parent( cha, ông nội...)
        ///// </summary>
        //List<ProjectPlan> p = new List<ProjectPlan>();
        //public List<ProjectPlan> FindListProjectPlanParentRecursive(ProjectPlan projectPlan)
        //{
        //    if (projectPlan.ParentId != Guid.Empty && projectPlan.ParentId != null)
        //    {
        //        var projectPlanCurrent = _projectPlanRepository.FindById(projectPlan.Id);
        //        projectPlanCurrent.CompletedPercentage = projectPlan.CompletedPercentage;

        //        var planParent = _projectPlanRepository.FindById(projectPlan.ParentId);
        //        IEnumerable<ProjectPlan> listChildren = _projectPlanRepository.FindAll(c => c.ParentId == planParent.Id);
        //        var totalCountDay = listChildren.Select(c => c.Duration).Sum();

        //        var listProjectPlanNew = listChildren.Where(c => c.Id != projectPlan.Id).ToList();
        //        listProjectPlanNew.Add(projectPlanCurrent);

        //        var countDayCurrent = listProjectPlanNew.Select(c => (c.Duration * (float)(c.CompletedPercentage == null ? 0 : c.CompletedPercentage)) / 100).Sum();

        //        var percentageParent = (countDayCurrent / (float)totalCountDay) * 100;

        //        planParent.CompletedPercentage = (int)(Math.Ceiling(percentageParent));

        //        p.Add(planParent);
        //        FindListProjectPlanParentRecursive(planParent);
        //    }
        //    return p;
        //}


        //public bool UpdatePlanPercentage(ProjectPlanModel projectPlanModel)
        //{
        //    var planCurrent = _projectPlanRepository.FindById(projectPlanModel.Id);

        //    var first = new List<ProjectPlan>();
        //    if (planCurrent.IsLeaf == true)
        //    {
        //        planCurrent.CompletedPercentage = projectPlanModel.CompletedPercentage;
        //        first.Add(planCurrent);
        //        var listParents = FindListProjectPlanParentRecursive(planCurrent);
        //        foreach (var listParent in listParents)
        //        {
        //            first.Add(listParent);
        //        }
        //    }
        //    return _projectPlanRepository.BulkUpdate(first);
        //}


    }
}
