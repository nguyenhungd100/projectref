﻿using AutoMapper;
using AutoMapper.EquivalencyExpression;
using PCC1.PMS.Domain.Entity;
using PCC1.PMS.Domain.Models.Projects;
using PCC1.PMS.Domain.Models.ProjectUsers;
using PCC1.PMS.Domain.Models.Materials;
using PCC1.PMS.Domain.Models.Users;
using PCC1.PMS.Domain.Models.Workitems;
using PCC1.PMS.Domain.Models.ProjectReports;
using PCC1.PMS.Domain.Models.Companies;
using PCC1.PMS.Domain.Models.ImageLibraries;

namespace PCC1.PMS.Domain.Services
{
    public class DomainMaps
    {
        public static IMapper Mapper { get; set; }
        public static void Config()
        {
            var config = new MapperConfiguration(cfg => {
                cfg.AddCollectionMappers();
                cfg.CreateMap<User, UserModel>();
                cfg.CreateMap<Project, ProjectModel>();
                cfg.CreateMap<ProjectUser, ProjectUserModel>();
                cfg.CreateMap<Workitem, WorkItemModel>();
                cfg.CreateMap<ProjectPlan, ProjectPlanModel>();
                cfg.CreateMap<ProjectReport, ProjectReportModel>();
                cfg.CreateMap<ProjectReportPlan, PlanReportModel>();
                cfg.CreateMap<ProjectReportWorkitem, WorkitemReportModel>();
                cfg.CreateMap<RegisterMaterial, MaterialModel>();
                cfg.CreateMap<Company, CompanyModel>();
                cfg.CreateMap<ImageLibrary, ImageLibraryModel>();


            });
            Mapper = config.CreateMapper();
        }
    }
}
