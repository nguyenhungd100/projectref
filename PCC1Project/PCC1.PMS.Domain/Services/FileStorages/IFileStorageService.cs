﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using PCC1.PMS.Domain.Entity;
using PCC1.PMS.Domain.Models.File;
using PCC1.PMS.Domain.Models.MediaStorage;
using PCC1.PMS.Domain.Models.ProjectFolderDashboard;
using PCC1.PMS.Domain.Models.Users;

namespace PCC1.PMS.Domain.Services.FileStorages
{
    public interface IFileStorageService
    {
        (bool, List<int>) AddProjectImages(MediaModel mediaModel);

        Task<IEnumerable<ProjectPicture>> GetProjectPictures(int projectId);

        bool AddProjectDocuments(MediaModel mediaModel, bool isArchives, int userId);

        Task<IEnumerable<ProjectDocument>> GetProjectDocuments(int projectId, Guid? folder);

        File GetFile(Guid fileId);

        BinaryFileStorage GetBinaryFileStorage(int storageId);

        (String, string) GetMimeAndName(int storageId);

        Task<bool> UpdateAvatarUserAsync(MediaUserAvartarModel userImageModel);

        IEnumerable<BinaryFileStorageModel> FindImages(string images);

        BinaryFileStorageModel ImageByFileId(Guid fileId);

        bool RemoveDocuments(IEnumerable<RemoveDocumentModel> removeDocuments);

        ProjectFolderDashboardResultModel ListProjectFolderDashboard();

        bool UploadArchives(UploadArchivesModel uploadArchives, int projectId, int userId);
    }
}
