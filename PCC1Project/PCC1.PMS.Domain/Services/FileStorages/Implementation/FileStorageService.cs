﻿using MicroOrm.Dapper.Repositories;
using OfficeOpenXml.FormulaParsing.Excel.Functions.DateTime;
using PCC1.PMS.Domain.Entity;
using PCC1.PMS.Domain.Enums;
using PCC1.PMS.Domain.Models.File;
using PCC1.PMS.Domain.Models.MediaStorage;
using PCC1.PMS.Domain.Models.Notification;
using PCC1.PMS.Domain.Models.ProjectFolderDashboard;
using PCC1.PMS.Domain.Models.Users;
using PCC1.PMS.Domain.Repositories;
using PCC1.PMS.Domain.Services.Common;
using PCC1.PMS.Domain.Services.Projects;
using PCC1.PMS.Framework.Utils;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PCC1.PMS.Domain.Services.FileStorages
{
    public class FileStorageService : IFileStorageService
    {
        private readonly IFileRepository _fileRepository;
        private readonly IDapperRepository<ProjectPicture> _projectPictureRepository;
        private readonly IDapperRepository<ProjectDocument> _projectDocumentRepository;
        private readonly IDapperRepository<BinaryFileStorage> _binaryFileStorageRepository;
        private readonly IDapperRepository<User> _userRepository;
        private readonly IDapperRepository<Project> _projectRepository;
        private readonly IProjectService _projectService;
        private readonly IDapperRepository<ProjectFolder> _projectFolderRepository;
        private readonly IDapperRepository<ProjectUser> _projectUserRepository;
        private readonly INotificationService _notificationService;
        private readonly IHashidService _hashidService;

        // private readonly IProjectRepository _projectRepository;

        public FileStorageService(IFileRepository fileRepository,
            IDapperRepository<ProjectPicture> projectPictureRepository,
            IDapperRepository<ProjectDocument> projectDocumentRepository,
            IDapperRepository<BinaryFileStorage> binaryFileStorageRepository,
            IDapperRepository<User> userRepository,
            IDapperRepository<Project> projectRepository,
            IProjectService projectService,
            IDapperRepository<ProjectFolder> projectFolderRepository,
            IDapperRepository<ProjectUser> projectUserRepository,
            INotificationService notificationService,
            IHashidService hashidService
            )
        {
            _fileRepository = fileRepository;
            _projectPictureRepository = projectPictureRepository;
            _projectDocumentRepository = projectDocumentRepository;
            _binaryFileStorageRepository = binaryFileStorageRepository;
            _userRepository = userRepository;
            _projectRepository = projectRepository;
            _projectService = projectService;
            _projectFolderRepository = projectFolderRepository;
            _projectUserRepository = projectUserRepository;
            _notificationService = notificationService;
            _hashidService = hashidService;
        }

        public async Task<IEnumerable<ProjectPicture>> GetProjectPictures(int projectId)
        {
            if (projectId <= 0)
            {
                throw new ServiceException("Hãy chọn dự án để lấy ảnh");
            }

            return await _projectPictureRepository.FindAllAsync(c => c.ProjectId == projectId
                && c.PictureType == ProjectPictureType.HinhAnhCongTrinhTuCBQLDA);
        }

        public async Task<IEnumerable<ProjectDocument>> GetProjectDocuments(int projectId, Guid? folder)
        {
            if (projectId <= 0)
            {
                throw new ServiceException("Hãy chọn dự án để lấy tài liệu");
            }

            if (folder == null || folder == Guid.Empty)
                return await _projectDocumentRepository.FindAllAsync(c => c.ProjectId == projectId);
            else
                return await _projectDocumentRepository.FindAllAsync(c => c.ProjectId == projectId
                    && c.Folder == folder);
        }

        public Entity.File GetFile(Guid FileId)
        {
            return _fileRepository.Find(c => c.Id == FileId);
        }

        public BinaryFileStorage GetBinaryFileStorage(int storageId)
        {
            return _binaryFileStorageRepository.Find(c => c.Id == storageId);
        }

        public (string, string) GetMimeAndName(int storageId)
        {
            var file = _fileRepository.Find(c => c.StorageId == storageId);
            var document = _projectDocumentRepository.Find(c => c.FileId == file.Id);
            return (_fileRepository.Find(c => c.StorageId == storageId).Mime, document.DocumentName);
        }


        public (bool, List<int>) AddProjectImages(MediaModel mediaModels)
        {
            var existProject = _projectRepository.FindById(mediaModels.ProjectId);
            if (existProject != null)
            {

                var idPic = new List<int>();
                var res1 = false;
                if (mediaModels.Data.Count() > 0)
                {
                    foreach (var mediaModel in mediaModels.Data)
                    {
                        var ms = new MemoryStream();
                        byte[] fileByteImage = new byte[mediaModel.Length];
                        mediaModel.CopyTo(ms);
                        fileByteImage = ms.ToArray();

                        #region Initialize BinaryFileStorage
                        BinaryFileStorage binaryFileStorage = new BinaryFileStorage();
                        binaryFileStorage.Data = fileByteImage;
                        res1 = _binaryFileStorageRepository.Insert(binaryFileStorage);
                        if (res1 == false)
                        {
                            throw new ServiceException("Không thêm được binary file storage");
                        }
                        #endregion
                        #region Initialize File
                        Entity.File file = new Entity.File()
                        {
                            Id = Guid.NewGuid(),
                            ProviderType = ProviderFileType.LocalStorage,
                            StorageId = binaryFileStorage.Id,
                            Mime = mediaModel.ContentType,
                            Title = mediaModels.Title,
                            CreatedUtcTime = mediaModels.CreatedUtcTime,
                            Size = (int)mediaModel.Length / 1024,
                            Status = mediaModels.Status
                        };

                        res1 = _fileRepository.Insert(file);
                        if (res1 == false)
                        {
                            throw new ServiceException("Không thêm được file");
                        }
                        #endregion
                        #region Initialize ProjectPicture
                        var projectPicture = new ProjectPicture()
                        {
                            ProjectId = mediaModels.ProjectId,
                            FileId = file.Id,
                            DisplayOrder = 1,
                            PictureType = mediaModels.PictureType
                        };

                        res1 = _projectPictureRepository.Insert(projectPicture);
                        idPic.Add(projectPicture.Id);
                        if (res1 == false)
                        {
                            throw new ServiceException("Không thêm được ProjectPicture");
                        }
                        #endregion
                    }
                }
                return (res1, idPic);

            }
            else throw new ServiceException("Dự án này không tồn tại");

        }

        public bool AddProjectDocuments(MediaModel mediaModels, bool isArchives, int userId)
        {
            var result = false;
            var trans = _binaryFileStorageRepository.BeginTransaction();
            if (trans.Connection.State != ConnectionState.Open) trans.Connection.Open();
            try
            {
                var existProject = _projectRepository.FindById(mediaModels.ProjectId, trans);
                if (existProject != null)
                {
                    if (mediaModels.Data.Count() > 0)
                    {
                        foreach (var mediaModel in mediaModels.Data)
                        {
                            var ms = new MemoryStream();
                            byte[] fileByteImage = new byte[mediaModel.Length];
                            mediaModel.CopyTo(ms);
                            fileByteImage = ms.ToArray();

                            var binaryFileStorage = new BinaryFileStorage();
                            binaryFileStorage.Data = fileByteImage;
                            result = _binaryFileStorageRepository.Insert(binaryFileStorage, trans);
                            if (result == false)
                            {
                                trans.Rollback();
                                break;
                            }
                            else
                            {
                                var file = new Entity.File()
                                {
                                    Id = Guid.NewGuid(),
                                    ProviderType = ProviderFileType.LocalStorage,
                                    StorageId = binaryFileStorage.Id,
                                    Mime = mediaModel.ContentType,
                                    Title = mediaModels.Title,
                                    CreatedUtcTime = mediaModels.CreatedUtcTime,
                                    Size = (int)mediaModel.Length / 1024,
                                    Status = mediaModels.Status
                                };
                                result = _fileRepository.Insert(file, trans);

                                if (result == false || file.Id == Guid.Empty)
                                {
                                    trans.Rollback();
                                    break;
                                }
                                else
                                {
                                    var projectDocument = new ProjectDocument()
                                    {
                                        ProjectId = mediaModels.ProjectId,
                                        FileId = file.Id,
                                        LastModified = DateTime.Now,
                                        LastModifyBy = mediaModels.LastModifyBy,
                                        DocumentName = mediaModel.FileName,
                                        Folder = mediaModels.Folder
                                    };

                                    result = _projectDocumentRepository.Insert(projectDocument, trans);
                                    if (result == false)
                                    {
                                        trans.Rollback();
                                        break;
                                    }
                                    else
                                    {
                                        foreach (var model in mediaModels.Data)
                                        {
                                            if (isArchives)
                                                result = PushNotificationUploadArchivesToAllMemberProject(model.FileName, mediaModels.ProjectId, userId, trans);
                                        }
                                    }
                                }
                            }
                        }
                    }
                    else throw new ServiceException("Yêu cầu chọn file cần tải lên");
                }
                else throw new ServiceException("Dự án này không tồn tại");
            }
            catch (Exception ex)
            {
                trans.Rollback();
                throw ex;
            }
            trans.Commit();
            return result;
        }

        public async Task<bool> UpdateAvatarUserAsync(MediaUserAvartarModel userImageModel)
        {
            var res1 = false;

            var ms = new MemoryStream();
            byte[] fileByteImage = new byte[userImageModel.Avatar.Length];
            userImageModel.Avatar.CopyTo(ms);
            fileByteImage = ms.ToArray();


            var exist = CheckExistAvatarInFile(userImageModel.UserId);

            if (exist == false)
            {
                //Initialize BinaryFileStorage
                BinaryFileStorage binaryFileStorage = new BinaryFileStorage();
                binaryFileStorage.Data = fileByteImage;
                res1 = await _binaryFileStorageRepository.InsertAsync(binaryFileStorage);
                if (res1 == false)
                {
                    throw new ServiceException("Không thêm được binary file storage");
                }

                //Initialize File
                Entity.File file = new Entity.File();
                file.Id = Guid.NewGuid();
                file.ProviderType = ProviderFileType.LocalStorage;
                file.StorageId = binaryFileStorage.Id;
                file.Mime = userImageModel.Avatar.ContentType;
                file.Title = "Ảnh đại diện";
                file.CreatedUtcTime = DateTime.Now;
                file.Size = (int)userImageModel.Avatar.Length / 1024;
                file.Status = FileStatus.NewPicture;
                res1 = await _fileRepository.InsertAsync(file);
                if (res1 == false)
                {
                    throw new ServiceException("Không thêm được file");
                }
                var user = _userRepository.FindById(userImageModel.UserId);
                user.Avatar = file.Id;
                res1 = await _userRepository.UpdateAsync(user);
                if (res1 == false)
                {
                    throw new ServiceException("Không thêm mới được Avatar");
                }
            }
            else
            {
                //Update File
                var user2 = _userRepository.FindById(userImageModel.UserId);
                var file = _fileRepository.FindById(user2.Avatar);
                file.ProviderType = ProviderFileType.LocalStorage;
                file.Mime = userImageModel.Avatar.ContentType;
                file.Title = "Ảnh đại diện";
                file.CreatedUtcTime = DateTime.Now;
                file.Size = (int)userImageModel.Avatar.Length / 1024;
                file.Status = FileStatus.NewPicture;
                res1 = await _fileRepository.UpdateAsync(file);
                if (res1 == false)
                {
                    throw new ServiceException("Không sửa được file");
                }

                //Update BinaryFileStorage  
                var binaryfile = _binaryFileStorageRepository.FindById(file.StorageId);
                binaryfile.Data = fileByteImage;
                res1 = await _binaryFileStorageRepository.UpdateAsync(binaryfile);
                if (res1 == false)
                {
                    throw new ServiceException("Không sửa được binary file storage");
                }
                var user = _userRepository.FindById(userImageModel.UserId);
                user.Avatar = file.Id;
                res1 = await _userRepository.UpdateAsync(user);
                if (res1 == false)
                {
                    throw new ServiceException("Không cập nhật được Avatar");
                }
            }
            return res1;
        }

        public bool CheckExistAvatarInFile(long userId)
        {
            var user = _userRepository.FindById(userId);

            var exist = _fileRepository.FindAll(c => c.Id == user.Avatar);

            return exist.Count() > 0 ? true : false;
        }

        public IEnumerable<BinaryFileStorageModel> FindImages(string images)
        {
            var listIdProjectPictures = images.Split(new string[] { ";" },
                StringSplitOptions.RemoveEmptyEntries).Select(c => Convert.ToInt32(c)).ToList();
            var binaryFileStorages = new List<BinaryFileStorage>();

            foreach (var idProjectPicture in listIdProjectPictures)
            {
                var projectPicture = _projectPictureRepository.FindById(idProjectPicture);
                var file = _fileRepository.FindById(projectPicture.FileId);
                var binaryFileStorage = _binaryFileStorageRepository.FindById(file.StorageId);
                binaryFileStorages.Add(binaryFileStorage);
            }

            return binaryFileStorages.CloneToListModels<BinaryFileStorage, BinaryFileStorageModel>();
        }

        public BinaryFileStorageModel ImageByFileId(Guid fileId)
        {
            var file = _fileRepository.FindById(fileId);
            if (file == null) throw new ServiceException("File không tồn tại");
            var binaryFile = _binaryFileStorageRepository.Find(c => c.Id == file.StorageId);
            return binaryFile.CloneToModel<BinaryFileStorage, BinaryFileStorageModel>();
        }

        public bool RemoveDocuments(IEnumerable<RemoveDocumentModel> removeDocuments)
        {
            var result = false;
            var trans = _projectDocumentRepository.BeginTransaction();
            if (trans.Connection.State != ConnectionState.Open) trans.Connection.Open();
            try
            {
                if (removeDocuments.Count() > 0)
                {
                    foreach (var remove in removeDocuments)
                    {
                        var doc = _projectDocumentRepository.FindById(remove.DocumentId, trans);
                        if (doc == null)
                        {
                            throw new ServiceException("Tài liệu không tồn tại");
                        }
                        result = _projectDocumentRepository.Delete(doc,trans);
                        if (doc.LastModifyBy != remove.UserId) throw new ServiceException("Bạn không được phép xóa file do người khác tạo");
                        else
                        {
                            var file = _fileRepository.FindById(doc.FileId, trans);
                            if(result) result = _fileRepository.Delete(file, trans);
                            var data = _binaryFileStorageRepository.FindById(file.StorageId, trans);
                            if (result) result = _binaryFileStorageRepository.Delete(data, trans);
                            if (!result)
                            {
                                trans.Rollback();
                                break;
                            }
                        }
                    }
                }
                else
                {
                    throw new ServiceException("Bạn chưa chọn tài liệu để xóa");
                }
            }
            catch (Exception)
            {
                trans.Rollback();
                throw;
            }
            trans.Commit();
            return result;
        }      

        public ProjectFolderDashboardResultModel ListProjectFolderDashboard()
        {
            var result = new ProjectFolderDashboardResultModel();

            var projects = _projectRepository.FindAll();
            var projectIds = projects.Select(c => c.Id);
            var projectDocumentEntities = _projectDocumentRepository.FindAll();
            var projectDocuments = projectDocumentEntities.Where(c => projectIds.Contains(c.ProjectId.Value));
            var listProjectFolder = _projectFolderRepository.FindAll(c => projectIds.Contains(c.ProjectId));

            foreach (var project in projects)
            {
                var folderArchives = listProjectFolder.FirstOrDefault(c => c.ParentId == null && c.Name.Equals("01. CÔNG VĂN, THÔNG BÁO")
                    && c.ProjectId == project.Id);


                var addProject = new FolderDashboardByProject
                {
                    Id = project.Id,
                    Name = project.Name,
                    ParentId = null,
                    TypeFolders = TypeFolder.Project
                };
                if (folderArchives != null)
                {
                    var folderAdd = new FolderDashboardByProject
                    {
                        Id = folderArchives.Id,
                        Name = folderArchives.Name,
                        ParentId = project.Id,
                        TypeFolders = TypeFolder.Folder,
                        HashId = _hashidService.Encode(folderArchives.ProjectId),
                        ViewLevel = 1
                    };
                    folderAdd.Childrens.AddRange(FillFolder(folderArchives.Id, listProjectFolder, project.Id, projectDocuments));
                    addProject.Childrens.Add(folderAdd);
                }
                result.ProjectFolderDashboard.Add(addProject);

            }
            return result;
        }

        private int level = 2;

        public List<FolderDashboardByProject> FillFolder(Guid? parentId, IEnumerable<ProjectFolder> listProjectFolders,
            int projectId, IEnumerable<ProjectDocument> projectDocuments)
        {
            var result = new List<FolderDashboardByProject>();
            var folderChildrens = listProjectFolders.Where(c => c.ParentId == parentId.Value);
            var documentChildrens = projectDocuments.Where(c => c.Folder == parentId.Value);
            if ((folderChildrens.Count() + documentChildrens.Count()) > 0)
            {
                var res = new FolderDashboardByProject();
                if (documentChildrens.Count() > 0)
                {
                    foreach (var documentChildren in documentChildrens)
                    {
                        res = new FolderDashboardByProject
                        {
                            Id = documentChildren.Id,
                            Name = documentChildren.DocumentName,
                            TypeFolders = TypeFolder.Document,
                            ParentId = parentId,
                            HashId = _hashidService.Encode(documentChildren.ProjectId.Value),
                            ViewLevel = level
                        };
                        result.Add(res);
                    }
                }
                if (folderChildrens.Count() > 0)
                {
                    foreach (var folderChildren in folderChildrens)
                    {
                        res = new FolderDashboardByProject
                        {
                            Id = folderChildren.Id,
                            Name = folderChildren.Name,
                            TypeFolders = TypeFolder.Folder,
                            ParentId = parentId,
                            HashId = _hashidService.Encode(folderChildren.ProjectId),
                            ViewLevel = level
                        };
                        result.Add(res);
                        level++;
                        res.Childrens.AddRange(FillFolder(Guid.Parse(res.Id.ToString()), listProjectFolders, projectId, projectDocuments));
                        level--;
                    }
                }
            }
            return result;
        }

        private bool PushNotificationUploadArchivesToAllMemberProject(string fileName, int projectId, int userId, IDbTransaction trans)
        { 
            var project = _projectRepository.FindById(projectId, trans);
            if (project == null) throw new ServiceException("Dự án không tồn tại");
            var userInProjects = _projectUserRepository.FindAll(c => c.ProjectId == projectId, trans);
            var userIds = userInProjects.Select(c => c.UserId).ToList();
            var uploadUser = _userRepository.FindById(userId, trans);
            var notificationModel = new UploadArchivesEventModel
            {            
                ProjectId = projectId,
                ProjectHashId = _hashidService.Encode(projectId),
                ProjectName = project.Name,
                CreatedBy = uploadUser.Id,
                CreatedUserName = !string.IsNullOrEmpty(uploadUser.FullName) ? uploadUser.FullName: uploadUser.Email,
                ArchivesName = fileName,
                Type = EventType.UploadArchives
            };
            return _notificationService.AddNotification(userIds, notificationModel);
        }

        public bool UploadArchives(UploadArchivesModel uploadArchives, int projectId, int userId)
        {
            var result = false;
            var project = _projectRepository.FindById(projectId);
            if (project == null) throw new ServiceException("Dự án không tồn tại");
            var trans = _fileRepository.BeginTransaction();
            if (trans.Connection.State != ConnectionState.Open) trans.Connection.Open();        
            try
            {
                if (uploadArchives.Data.Count() > 0)
                {
                    foreach (var model in uploadArchives.Data)
                    {
                        using (var ms = new MemoryStream())
                        {
                            byte[] fileByteImage = new byte[model.Length];
                            model.CopyTo(ms);
                            fileByteImage = ms.ToArray();

                            var addBinary = new BinaryFileStorage
                            {
                                Data = fileByteImage
                            };
                            result = _binaryFileStorageRepository.Insert(addBinary, trans);
                            if (result)
                            {
                                var addFile = new Entity.File
                                {
                                    Id = Guid.NewGuid(),
                                    ProviderType = ProviderFileType.BinaryStorage,
                                    StorageId = addBinary.Id,
                                    Mime = model.ContentType,
                                    Title = model.FileName,
                                    CreatedUtcTime = DateTime.UtcNow,
                                    Size = (int)model.Length / 1024,
                                    Status = FileStatus.NewArchives
                                };
                                result = _fileRepository.Insert(addFile, trans);
                                if (result)
                                {
                                    var addDocument = new ProjectDocument
                                    {
                                        ProjectId = projectId,
                                        DocumentName = model.FileName,
                                        Folder = uploadArchives.Folder,
                                        LastModified = DateTime.UtcNow,
                                        LastModifyBy = userId,
                                        FileId = addFile.Id,
                                    };
                                    result = _projectDocumentRepository.Insert(addDocument, trans);
                                }
                            }
                            if (!result)break;

                        }
                    }
                    if (!result)
                    {
                        trans.Rollback();
                    }
                    else
                    {
                        foreach (var model in uploadArchives.Data)
                        {
                            result = PushNotificationUploadArchivesToAllMemberProject(model.FileName, projectId, userId, trans);
                        }
                    }
                }
                else throw new Exception("Yêu cầu nhập file có nội dung");
            }
            catch (Exception ex)
            {
                trans.Rollback();
                throw ex;
            }
            trans.Commit();
            return result;
        }
    }
}

