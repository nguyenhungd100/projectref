﻿using PCC1.PMS.Domain.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace PCC1.PMS.Domain.Services.Projects
{
    [AttributeUsage(AttributeTargets.All, Inherited = false, AllowMultiple = false)]
    public class MachineryManpowerDetailAttribute : Attribute
    {
        private readonly MachineryManpowerGroup group;
        private readonly string name;

        public MachineryManpowerDetailAttribute(MachineryManpowerGroup group, string name)
        {
            this.group = group;
            this.name = name;
        }
        public virtual string Name
        {
            get { return name; }
        }
        public virtual MachineryManpowerGroup Group
        {
            get { return group; }
        }
    }

}
