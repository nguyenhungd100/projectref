﻿using MicroOrm.Dapper.Repositories;
using PCC1.PMS.Domain.Entity;
using PCC1.PMS.Domain.Enums;
using PCC1.PMS.Domain.Models;
using PCC1.PMS.Domain.Models.Companies;
using PCC1.PMS.Domain.Models.File;
using PCC1.PMS.Domain.Models.Notification;
using PCC1.PMS.Domain.Models.Projects;
using PCC1.PMS.Domain.Models.ProjectUsers;
using PCC1.PMS.Domain.Models.Users;
using PCC1.PMS.Domain.Repositories;
using PCC1.PMS.Domain.Services.Common;
using PCC1.PMS.Domain.Services.Roles;
using PCC1.PMS.Domain.Shared;
using PCC1.PMS.Framework.Extensions;
using PCC1.PMS.Framework.Utils;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace PCC1.PMS.Domain.Services.Projects
{
    public class ProjectService : IProjectService
    {
        private readonly IHashidService _hashidService;
        private readonly INotificationService _notificationService;
        private readonly IProjectRepository _projectRepository;
        private readonly IDapperRepository<ProjectUser> _projectUserRepository;
        private readonly IDapperRepository<ProjectPlan> _projectPlanRepository;
        private readonly IDapperRepository<ProjectReport> _projectReportRepository;
        private readonly IUserRepository _userRepository;
        private readonly IDapperRepository<ProjectFolder> _projectFolderRepository;
        private readonly IDapperRepository<ProjectPicture> _projectPictureRepository;
        private readonly IDapperRepository<Workitem> _workitemRepository;
        private readonly IDapperRepository<Company> _companyRepository;
        private readonly IDapperRepository<ProjectCompany> _projectCompanyRepository;
        private readonly IDapperRepository<ProjectReportWorkitem> _reportWorkitemRepository;
        private readonly IDapperRepository<ProjectReportPlan> _reportPlanRepository;
        private readonly IDapperRepository<SafelyReport> _safelyReportRepository;
        private readonly IDapperRepository<SafelyReportImage> _safelyReportImageRepository;
        private readonly IDapperRepository<WorkitemCompany> _workitemCompanyRepository;
        private readonly IDapperRepository<RegisterMaterial> _registerMaterialRepository;
        private readonly IRolesService _roleService;

        public ProjectService(IHashidService hashidService,
            INotificationService notificationService,
            IProjectRepository projectRepository,
            IDapperRepository<ProjectUser> projectUserRepository,
            IDapperRepository<ProjectPlan> projectPlanRepository,
            IDapperRepository<ProjectReport> projectReportRepository,
            IUserRepository userRepository,
            IDapperRepository<ProjectFolder> projectFolderRepository,
            IDapperRepository<ProjectPicture> projectPictureRepository,
            IDapperRepository<Workitem> workitemRepository,
            IDapperRepository<Company> companyRepository,
            IDapperRepository<ProjectCompany> projectCompanyRepository,
            IDapperRepository<ProjectReportWorkitem> reportWorkitemRepository,
            IDapperRepository<ProjectReportPlan> reportPlanRepository,
            IDapperRepository<SafelyReport> safelyReportRepository,
            IDapperRepository<SafelyReportImage> safelyReportImageRepository,
            IDapperRepository<WorkitemCompany> workitemCompanyRepository,
            IDapperRepository<RegisterMaterial> registerMaterialRepository,
            IRolesService roleService)
        {
            _hashidService = hashidService;
            _notificationService = notificationService;
            _projectRepository = projectRepository;
            _projectUserRepository = projectUserRepository;
            _projectPlanRepository = projectPlanRepository;
            _projectReportRepository = projectReportRepository;
            _userRepository = userRepository;
            _projectFolderRepository = projectFolderRepository;
            _projectPictureRepository = projectPictureRepository;
            _workitemRepository = workitemRepository;
            _companyRepository = companyRepository;
            _projectCompanyRepository = projectCompanyRepository;
            _reportWorkitemRepository = reportWorkitemRepository;
            _reportPlanRepository = reportPlanRepository;
            _safelyReportRepository = safelyReportRepository;
            _safelyReportImageRepository = safelyReportImageRepository;
            _workitemCompanyRepository = workitemCompanyRepository;
            _registerMaterialRepository = registerMaterialRepository;
            _roleService = roleService;
        }

        public ProjectService(IProjectRepository projectRepository)
        {
            _projectRepository = projectRepository;
        }

        public ProjectService()
        {
        }

        public async Task<ProjectModel> GetProjectById(int projectId)
        {
            var project = await _projectRepository.FindByIdAsync(projectId);
            var model = project.CloneToModel<Project, ProjectModel>();
            model.HashId = _hashidService.Encode(projectId);

            var projectCompanies = _projectCompanyRepository.FindAll(cp => cp.ProjectId == project.Id);
            var companyIds = projectCompanies.Select(p => p.CompanyId);
            var companies = _companyRepository.FindAll(c => companyIds.Contains(c.Id));
            if (companies != null && companies.Any())
            {
                model.Companies = DomainMaps.Mapper.Map<List<CompanyModel>>(companies);
            }
            if (project.DateEnd.HasValue)
                model.DateEnd = DateTime.SpecifyKind(project.DateEnd.Value, DateTimeKind.Utc);
            if (project.DateStart.HasValue)
                model.DateStart = DateTime.SpecifyKind(project.DateStart.Value, DateTimeKind.Utc);
            return model;
        }

        public SearchProjectResult Search(SearchProjectModel model, int userCurrent)
        {
            var result = new SearchProjectResult();
            List<Project> projects = new List<Project>();
            var projectNews = new List<Project>();
            int totalRecords = 0;
            projects = _projectRepository.Search(model, out totalRecords).ToList();
            if (model.UserId.HasValue)
            {
                var projectUsers = _projectUserRepository.FindAll(pu => pu.UserId == model.UserId.Value);
                var projectIds = projectUsers.Select(pu => pu.ProjectId).ToList();

                foreach (var projectId in projectIds)
                {
                    var project = projects.Where(c => c.Id == projectId).FirstOrDefault();
                    if (project != null)
                        projectNews.Add(project);
                }

                projects = projectNews;
                model.TotalRecord = projectNews.Count();
            }
            else
            {
                var userX = _userRepository.Find(c => c.Id == userCurrent);
                if (userX.CompanyId.HasValue && userX.CompanyId > 0 && userX.CompanyId != 19)//filter doi voi cac don vi khac PCC1
                {
                    List<Project> projectParticipations = new List<Project>();
                    var projectUsers = _projectUserRepository.FindAll(pu => pu.UserId == userX.Id);
                    var projectIds = projectUsers.Select(pu => pu.ProjectId).ToList();
                    foreach (var projectId in projectIds)
                    {
                        var project = projects.Where(c => c.Id == projectId).FirstOrDefault();
                        if (project != null)
                            projectParticipations.Add(project);
                    }

                    if (projectParticipations.Count() > 0)
                    {
                        projects = projects.Where(c => c.CreatedBy == userX.Id).ToList();
                        projects.AddRange(projectParticipations);
                        projects = new HashSet<Project>(projects).ToList();
                    }
                    else
                    {
                        projects = projects.Where(c => c.CreatedBy == userX.Id).ToList();
                    }
                }

                model.TotalRecord = projects.Count();
            }

            projects = projects.OrderByDescending(c => c.Id).Skip(model.PageSize * (model.PageIndex - 1)).Take(model.PageSize).ToList();

            result.Records = DomainMaps.Mapper.Map<List<ProjectModel>>(projects);

            foreach (var r in result.Records)
            {
                r.HashId = _hashidService.Encode(r.Id);
                if (r.DateEnd.HasValue)
                    r.DateEnd = DateTime.SpecifyKind(r.DateEnd.Value, DateTimeKind.Utc);
                if (r.DateStart.HasValue)
                    r.DateStart = DateTime.SpecifyKind(r.DateStart.Value, DateTimeKind.Utc);

            }
            result.TotalRecord = model.TotalRecord;
            result.PageIndex = model.PageIndex;
            result.PageSize = model.PageSize;
            result.PageCount = result.TotalRecord / result.PageSize + (result.TotalRecord % result.PageSize > 0 ? 1 : 0);
            return result;
        }



        public async Task<bool> UpdateProject(UpdateProjectModel model)
        {
            var project = _projectRepository.FindById(model.ProjectId);
            if (project == null) throw new ServiceException("Dự án không tồn tại");



            if (!string.IsNullOrEmpty(model.Name)) project.Name = model.Name;
            if (model.TotalContractValue.HasValue) project.TotalContractValue = model.TotalContractValue.Value;
            if (!string.IsNullOrEmpty(model.Scale)) project.Scale = model.Scale;
            if (model.DateStart.HasValue) project.DateStart = model.DateStart.Value;
            if (model.DateEnd.HasValue) project.DateStart = model.DateEnd.Value;
            if (!string.IsNullOrEmpty(model.Location)) project.Location = model.Location;

            if (model.DateEnd.HasValue && model.DateEnd.HasValue)
            {
                if (model.DateStart.Value > model.DateEnd.Value) throw new ServiceException("Thời gian bắt đầu kết thúc không hợp lệ");
            }
            if (!string.IsNullOrEmpty(model.DesignConsultant)) project.DesignConsultant = model.DesignConsultant;
            if (!string.IsNullOrEmpty(model.Investor)) project.Investor = model.Investor;

            if (model.DateStart.HasValue) project.DateStart = model.DateStart.Value;
            if (model.DateEnd.HasValue) project.DateEnd = model.DateEnd.Value;

            if (!await _projectRepository.UpdateAsync(project)) return false;



            return true;
        }

        public bool UpdateCompaniesInProject(UpdateCompanyInProject model)
        {
            var result = false;

            if (model.CompanyIds.Count() > 0)
            {
                var trans = _projectCompanyRepository.BeginTransaction();

                try
                {
                    var checkCompanies = model.CompanyIds;
                    var companyProjects = _projectCompanyRepository.FindAll(c => c.ProjectId == model.ProjectId, trans);
                    var companies = _companyRepository.FindAll(trans);


                    if (companyProjects.Count() > 0)
                    {
                        var companyProjectIds = companyProjects.Select(a => a.CompanyId);

                        var reports = _projectReportRepository.FindAll(c => c.ProjectId == model.ProjectId, trans);
                        var reportIds = reports.Select(c => c.Id);


                        var reportCompanyInProjects = _reportWorkitemRepository.FindAll(c => companyProjectIds.Contains(c.CompanyId) && reportIds.Contains(c.ProjectReportId), trans);

                        foreach (var item in model.CompanyIds)
                        {
                            if (!companyProjects.Select(c => c.CompanyId).Contains(item.Value))
                            {
                                result = _projectCompanyRepository.Insert(new ProjectCompany
                                {
                                    ProjectId = model.ProjectId,
                                    CompanyId = item.Value
                                }, trans);
                                if (!result) break;
                            }
                            else result = true;
                        }
                        foreach (var companyProject in companyProjects)
                        {
                            if (!model.CompanyIds.Contains(companyProject.CompanyId))
                            {
                                if (reportCompanyInProjects.Where(c => c.CompanyId == companyProject.CompanyId).Count() == 0)
                                {
                                    result = _projectCompanyRepository.Delete(companyProject, trans);
                                    var workitems = _workitemRepository.FindAll(c => c.ProjectId == model.ProjectId, trans);
                                    if (workitems.Count() > 0)
                                    {
                                        var workitemIds = workitems.Select(c => c.Id);
                                        var workitemCompanies = _workitemCompanyRepository.FindAll(c => c.CompanyId == companyProject.CompanyId
                                            && workitemIds.Contains(c.WorkItemId), trans);
                                        workitemCompanies.ToList().ForEach(w =>
                                        {
                                            _workitemCompanyRepository.Delete(w, trans);
                                        });
                                    }

                                }
                                else
                                {
                                    var companyName = companies.FirstOrDefault(c => c.Id == companyProject.CompanyId).Name;
                                    throw new ServiceException("Không xóa được công ty " + companyName + " trong dự án, yêu cầu xóa các báo cáo ngày liên quan đến công ty này");
                                }
                                if (!result) break;
                            }
                            else result = true;
                        }

                    }
                    else
                    {
                        var newProjectCompanies = from c in model.CompanyIds
                                                  select new ProjectCompany
                                                  {
                                                      ProjectId = model.ProjectId,
                                                      CompanyId = c.Value
                                                  };
                        _projectCompanyRepository.BulkInsert(newProjectCompanies, trans);
                        result = true;
                    }
                    if (!result) trans.Rollback();
                }
                catch (Exception)
                {
                    trans.Rollback();
                    throw;
                }
                trans.Commit();
            }
            return result;
        }

        public ProjectModel InitInfoProject(ProjectAddModel projectAddModel)
        {
            var entity = projectAddModel.CloneToModel<ProjectAddModel, Project>();
            entity.Status = ProjectStatus.Init;
            entity.LastPlanNo = 0;
            var res = _projectRepository.Insert(entity);
            if (res)
            {
                var model = entity.CloneToModel<Project, ProjectModel>();
                model.HashId = _hashidService.Encode(entity.Id);
                var entityProjectUser = new ProjectUser()
                {
                    UserId = projectAddModel.CreatedBy,
                    ProjectId = entity.Id,
                    DisplayOrder = 1,
                    ProjectRoles = ";1;"
                };
                _projectUserRepository.Insert(entityProjectUser);
                CreateAllFolderWhenInitializeProject(entity.Id, projectAddModel.CreatedBy);
                return model;

            }
            else throw new ServiceException("Khởi tạo dự án không thành công");
        }

        //public string JoinRolesInProject(List<string> input)
        //{
        //    var result = ";";

        //    if(input.Count() > 0)
        //    {
        //        result = input.Split(new string[] { ";" }, StringSplitOptions.RemoveEmptyEntries)
        //            .Select(c => Convert.ToInt32(c)).ToList();
        //    }
        //    return result;
        //}

        public List<UserModel> GetProjectUsers(int projectId)
        {
            if (projectId <= 0)
                throw new ServiceException("Yêu cầu chọn dự án");
            IEnumerable<ProjectUser> projectUsers = new List<ProjectUser>();
            Expression<Func<ProjectUser, bool>> filter = t => t.Id > 0;
            if (projectId > 0)
            {
                filter = filter.And(t => t.ProjectId == projectId);
            }
            projectUsers = _projectUserRepository.FindAll(filter);

            var userIds = projectUsers.Select(pu => pu.UserId).ToList();

            var users = _userRepository.FindAll();

            var newUsers = new List<UserModel>();


            var roles = _roleService.Filter(new RoleFilterModel()).Records;


            userIds.ToList().ForEach(u =>
            {
                if (users.FirstOrDefault(c => c.Id == u) != null)
                {
                    var user = users.FirstOrDefault(c => c.Id == u);
                    var projectUser = projectUsers.FirstOrDefault(c => c.ProjectId == projectId && c.UserId == u);

                    var userAdd = DomainMaps.Mapper.Map<UserModel>(user);
                    userAdd.ProjectRoles = projectUser.ProjectRoles;

                    var roleProject = new List<int>();
                    roleProject = projectUser.ProjectRoles.Split(new string[] { ";" }, StringSplitOptions.RemoveEmptyEntries).Select(c => Convert.ToInt32(c)).ToList();

                    if (roleProject.Count() > 0)
                    {
                        var roleIn = roles.Where(c => roleProject.Contains(c.Id));

                        if (roleIn.Min(c => c.DisplayOrder).HasValue)
                        {
                            userAdd.DisplayOrderInProject = roleIn.Min(c => c.DisplayOrder).Value;
                        }

                    }
                    newUsers.Add(userAdd);
                }

            });

            return newUsers;
        }

        public async Task<bool> AddMember(AddProjectMembersModel model)
        {
            var existUser = await _userRepository.FindByIdAsync(model.UserId);
            if (existUser == null) throw new ServiceException("Người dùng này không tồn tại");

            var existProject = await _projectRepository.FindByIdAsync(model.ProjectId);
            if (existProject == null) throw new ServiceException("Dự án này không tồn tại");

            var existUserOnProject = _projectUserRepository.Find(c => c.UserId == model.UserId && c.ProjectId == model.ProjectId);

            if (existUserOnProject == null)
            {
                return _projectUserRepository.Insert(new ProjectUser
                {
                    ProjectRoles = model.ProjectRoles,
                    ProjectId = model.ProjectId,
                    UserId = model.UserId,
                    DisplayOrder = 1
                });
            }

            if (existUserOnProject.ProjectRoles.Contains(model.ProjectRoles)) throw new ServiceException("Đã tồn tại người dùng với vai trò này trong dự án");
            else
            {
                if (existUserOnProject.ProjectRoles == ";")
                {
                    existUserOnProject.ProjectRoles = model.ProjectRoles;
                }
                else
                {
                    var roleExists = existUserOnProject.ProjectRoles.Split(new string[] { ";" }, StringSplitOptions.RemoveEmptyEntries).Select(c => Convert.ToInt32(c)).ToList();

                    var roleAdd = int.Parse(model.ProjectRoles.Replace(";", ""));
                    roleExists.Add(roleAdd);
                    existUserOnProject.ProjectRoles = ";" + string.Join(";", roleExists.Distinct()) + ";";
                }
                return _projectUserRepository.Update(existUserOnProject);
            }



            //var entity = model.CloneToModel<AddProjectMembersModel, ProjectUser>();
            //var addResult = await _projectUserRepository.InsertAsync(entity);

            //// get all project members
            //var userIds = new List<int>();
            //var users = _projectUserRepository.FindAll(c => c.ProjectId == entity.ProjectId);
            //foreach (var user in users)
            //{
            //    userIds.Add(user.UserId);
            //}

            //// notify
            //var newUser = _userRepository.FindById(entity.UserId);
            //var adderUser = _userRepository.FindById(model.CurrentUserId);
            //var notificationModel = new ProjectMemberEventModel()
            //{
            //    Type = EventType.AddProjectMember,
            //    ProjectHashId = _hashidService.Encode(entity.ProjectId),
            //    ProjectName = _projectRepository.FindById(entity.ProjectId).Name,
            //    MemberId = newUser.Id,
            //    MemberName = string.IsNullOrEmpty(newUser.FullName) ? newUser.Email : newUser.FullName,
            //    UserId = adderUser.Id,
            //    UserName = string.IsNullOrEmpty(adderUser.FullName) ? adderUser.Email : adderUser.FullName,
            //};
            //return _notificationService.AddNotification(userIds, notificationModel);
        }

        public bool RemoveProjectMembers(RemoveProjectMembersModel model)
        {
            var projectUser = _projectUserRepository.Find(c => c.ProjectId == model.ProjectId && c.UserId == model.UserId);
            if (projectUser == null) throw new ServiceException("Không tồn tại nhân sự này trong dự án");
            var existRoles = projectUser.ProjectRoles.Split(new string[] { ";" }, StringSplitOptions.RemoveEmptyEntries).Select(c => Convert.ToInt32(c)).ToArray();

            model.ProjectRoles = model.ProjectRoles.Replace(";", "");
            if (existRoles.Count() > 0)
            {
                var roleDelete = model.ProjectRoles.Split(new string[] { ";" }, StringSplitOptions.RemoveEmptyEntries).Select(c => Convert.ToInt32(c)).ToArray().FirstOrDefault();

                if (existRoles.Contains(roleDelete))
                {
                    if (existRoles.Count() == 1)
                    {
                        return _projectUserRepository.Delete(projectUser);
                    }
                    else
                    {
                        existRoles = existRoles.Where(c => c != roleDelete).ToArray();
                        projectUser.ProjectRoles = ";" + string.Join(";", existRoles) + ";";
                        return _projectUserRepository.Update(projectUser);
                    }
                }
            }
            else return false;




            //// notify
            //var member = _userRepository.FindById(entity.UserId);
            //var excutor = _userRepository.FindById(model.ExecutingUserId);
            //var notificationModel = new ProjectMemberEventModel()
            //{
            //    Type = EventType.RemoveProjectMember,
            //    ProjectHashId = _hashidService.Encode(entity.ProjectId),
            //    ProjectName = _projectRepository.FindById(entity.ProjectId).Name,
            //    MemberId = member.Id,
            //    MemberName = string.IsNullOrEmpty(member.FullName) ? member.Email : member.FullName,
            //    UserId = excutor.Id,
            //    UserName = string.IsNullOrEmpty(excutor.FullName) ? excutor.Email : excutor.FullName,
            //};

            //_notificationService.AddNotification(projectMemberIds, notificationModel);


            return true;
        }

        public async Task<bool> UpdatePersonnal(ProjectUserModel projectUserModel)
        {
            if (projectUserModel.Id <= 0)
                throw new ServiceException("Chưa chọn dự án để update nhân sự");
            if (projectUserModel == null)
                throw new ServiceException("Chưa chọn nhân sự để update");
            var entity = projectUserModel.CloneToModel<ProjectUserModel, ProjectUser>();
            var res = await _projectUserRepository.UpdateAsync(entity);
            return res;
        }

        public async Task<bool> UpdateMultiPersonnal(List<ProjectUserModel> projectUserModels)
        {
            if (projectUserModels == null)
                throw new ServiceException("Chưa chọn nhân sự để update");
            foreach (var projectUserModel in projectUserModels)
            {
                if (projectUserModel.ProjectId <= 0)
                    throw new ServiceException("Chưa chọn dự án");
                if (projectUserModel.DisplayOrder <= 0)
                    throw new ServiceException("Chưa chọn thứ tự nhân sự trong dự án");
            }
            var entity = projectUserModels.CloneToListModels<ProjectUserModel, ProjectUser>();
            var res = await _projectUserRepository.BulkUpdateAsync(entity);
            return res;
        }

        public (bool, string) AbilityApprovingProjectForEPC(int projectId)
        {
            string resultMessage = "";
            var existProjectPersonnal = _projectUserRepository.FindAll(c => c.ProjectId == projectId);
            if (existProjectPersonnal.Count() <= 0)
                resultMessage = resultMessage + "+ Chưa cài đặt nhân sự tham gia";
            return ((String.IsNullOrEmpty(resultMessage)) ? true : false, resultMessage);
        }

        public (bool, string) AbilityApprovingProjectForProjectManager(int projectId)
        {
            string resultMessage = "";
            var existInfoProject = _projectRepository.Find(c => c.Id == projectId);

            if (String.IsNullOrEmpty(existInfoProject.Location) || (existInfoProject.TotalContractValue == 0
                || existInfoProject.TotalContractValue == null))
            {
                if (existInfoProject.DateEnd == null || existInfoProject.DateEnd == null)
                    resultMessage = resultMessage + "+ Chưa cập nhật tổng tiến độ công trình";
                if (existInfoProject.TotalContractValue == 0 || existInfoProject.TotalContractValue == null)
                    resultMessage = resultMessage + "+ Chưa cập nhật tổng giá trị hợp đồng";
                if (String.IsNullOrEmpty(existInfoProject.Location))
                    resultMessage = resultMessage + "+ Chưa cập nhật địa chỉ";

            }
            return ((String.IsNullOrEmpty(resultMessage)) ? true : false, resultMessage);
        }

        public (bool, string) AbilityApprovingProjectForChiefOfSiteManagement(int projectId)
        {
            string resultMessage = "";
            var existWorkItem = _workitemRepository.FindAll(c => c.ProjectId == projectId);
            var existProjectPlan = _projectPlanRepository.FindAll(c => c.ProjectId == projectId);
            if (existWorkItem.Count() <= 0 || existProjectPlan.Count() <= 0)
            {
                if (existWorkItem.Count() <= 0)
                    resultMessage = resultMessage + "+ Chưa cập nhật khối lượng công trình";
                if (existProjectPlan.Count() <= 0)
                    resultMessage = resultMessage + "+ Chưa cập nhật kế hoạch chi tiết";
            }
            return ((String.IsNullOrEmpty(resultMessage)) ? true : false, resultMessage);
        }

        public (bool, string) CheckAbilityApprovingProject(int projectId)
        {
            if (projectId <= 0)
            {
                throw new ServiceException("Chưa chọn dự án");
            }

            var resultStatus = AbilityApprovingProjectForEPC(projectId).Item1
                && AbilityApprovingProjectForChiefOfSiteManagement(projectId).Item1;

            var resultMessage = AbilityApprovingProjectForEPC(projectId).Item2
                + AbilityApprovingProjectForProjectManager(projectId).Item2
                + AbilityApprovingProjectForChiefOfSiteManagement(projectId).Item2;

            return (resultStatus, resultMessage);
        }

        public bool ApproveProject(int projectId, int approveUser)
        {
            if (projectId <= 0) throw new ServiceException("Chưa chọn dự án");
            var project = _projectRepository.FindById(projectId);
            if (project.Status == ProjectStatus.Approved) throw new ServiceException("Dự án đã được phê duyệt");
            project.Status = ProjectStatus.Approved;
            if (project.CreatedBy != approveUser) throw new ServiceException("Bạn không phải là người tạo dự án này nên không có quyền duyệt");
            var workitems = _workitemRepository.FindAll(c => c.ProjectId == projectId);
            if (workitems.Count() <= 0) throw new ServiceException("Dự án chưa được cập nhật hạng mục");
            var plans = _projectPlanRepository.FindAll(c => c.ProjectId == projectId);
            if (plans.Count() <= 0) throw new ServiceException("Dự án chưa được cập nhật tiến độ");
            if (project.TotalContractValue == null) throw new ServiceException("Dự án chưa được cập nhật tổng giá trị hợp đồng");

            return _projectRepository.Update(project);
        }

        public async Task<bool> ChangeProjectStatus(long projectId, ProjectStatus projectStatus)
        {
            var project = await _projectRepository.FindByIdAsync(projectId);

            switch (projectStatus)
            {
                case ProjectStatus.Init:
                    {
                        //if (project != null && project.Status == ProjectStatus.Init)
                        {
                            project.Status = ProjectStatus.Approved;
                            await _projectRepository.UpdateAsync(project);
                        };
                    }; break;


                case ProjectStatus.Stop:
                    {
                        if (project != null)
                        {
                            await _projectRepository.UpdateAsync(project);
                        };
                    }; break;
                default: break;
            }
            return false;
        }

        public async Task<bool> UpdateOverallProject(ProjectOverallModel projectOverallModel, int userId)
        {
            var existUserCurrent = _projectUserRepository.Find(c => c.UserId == userId && c.ProjectId == projectOverallModel.Id);
            if (existUserCurrent != null)
            {
                var existProject = _projectRepository.FindById(projectOverallModel.Id);
                if (existProject != null)
                {
                    if (existProject.Status != ProjectStatus.Approved)
                    {
                        var check = AbilityApprovingProjectForEPC(projectOverallModel.Id);
                        var res = false;
                        if (check.Item1)
                        {
                            if (projectOverallModel == null)
                                throw new ServiceException("Yêu cầu chọn dự án và các trường liên quan đến tổng thể");
                            // Update project
                            var entity = _projectRepository.FindById(projectOverallModel.Id);
                            entity.DateStart = projectOverallModel.DateStart;
                            entity.DateEnd = projectOverallModel.DateEnd;
                            entity.TotalContractValue = projectOverallModel.TotalContractValue;
                            entity.Location = projectOverallModel.Location;
                            entity.Status = ProjectStatus.Pending;
                            res = await _projectRepository.UpdateAsync(entity);
                            if (res == false)
                                throw new ServiceException("Không cập nhật được dự án");
                        }
                        else
                            throw new ServiceException(check.Item2);
                        return res;
                    }
                    else throw new ServiceException("Dự án này đã được duyệt nên không thể cập nhật tổng thể");
                }
                else throw new ServiceException("Dự án này không tồn tại");
            }
            else throw new ServiceException("Bạn không thuộc dự án này nên không có quyền thao tác");
        }

        public void SendingAbilityApproveProjectToUserCreated(int projectId)
        {
            var project = _projectRepository.FindById(projectId);
            var approveProject = CheckAbilityApprovingProject(projectId);
            var userCreatedProject = project.CreatedBy.Value;
            var notificationModel = new AblityAproveProject()
            {
                Type = EventType.AbilityApproveProject,
                ProjectHashId = _hashidService.Encode(projectId),
                ProjectName = _projectRepository.FindById(projectId).Name,
            };
            if (approveProject.Item1) _notificationService.AddNotification(new List<int>() { userCreatedProject }, notificationModel);
        }
        public async Task<bool> NewPlans(int projectId, int userId, PlanParsingModel planParsingModel)
        {
            var isProjectMember = _projectUserRepository.Find(c => c.UserId == userId && c.ProjectId == projectId);
            if (isProjectMember == null) throw new ServiceException("Bạn không thuộc dự án này nên không có quyền thao tác");

            var project = _projectRepository.FindById(projectId);
            var affected = 0;
            if (project == null) throw new ServiceException("Không tồn tại dự án này !");
            if (project.DateEnd == null || project.DateStart == null) throw new ServiceException("Tiến độ theo hợp đồng chưa được xác định");
            //if (AbilityApprovingProjectForEPC(projectId).Item1
            //&& AbilityApprovingProjectForProjectManager(projectId).Item1)
            //{

            if (project.LastPlanNo == null)
                project.LastPlanNo = 1;
            else project.LastPlanNo++;

            var planStart = DateTime.Parse(planParsingModel.StartDate.GetDateTimeFormats('d')[0]);
            var planEnd = DateTime.Parse(planParsingModel.EndDate.GetDateTimeFormats('d')[0]);
            var projectStart = DateTime.Parse(project.DateStart.Value.GetDateTimeFormats('d')[0]);
            var projectEnd = DateTime.Parse(project.DateEnd.Value.GetDateTimeFormats('d')[0]);
            if (planStart < projectStart || planEnd > projectEnd)
            {
                throw new ServiceException("Thời gian trong file mpp không nằm trong kế hoạch tổng thể của dự án");
            }

            var plans = new List<ProjectPlan>();

            foreach (var task in planParsingModel.Tasks)
            {
                try
                {
                    if (task != null)
                    {



                        var projectPlan = new ProjectPlan()
                        {
                            Id = task.Id,
                            No = project.LastPlanNo.GetValueOrDefault(),
                            ProjectId = projectId,
                            ParentId = task.ParentId,
                            Title = task.Title,
                            Duration = Int32.Parse(task.Duration.Split('.').First()),
                            StartDate = DateTime.Parse(task.DateStart.ToString()),
                            EndDate = DateTime.Parse(task.DateEnd.ToString()),
                            Status = ProjectPlanStatus.Active,
                            IsLeaf = task.IsLeaf,
                            CompletedPercentage = 0,
                            DisplayOnMobile = true,
                            OrderNumber = task.OrderNumber
                        };
                        plans.Add(projectPlan);
                    }
                }
                catch (Exception)
                {

                    throw;
                }

            }
            //ok
            _projectRepository.Update(project);
            affected = await _projectPlanRepository.BulkInsertAsync(plans);

            SendingAbilityApproveProjectToUserCreated(project.Id);
            return affected > 0;
        }

        public int? CheckLastPlanNo(int projectId)
        {
            return _projectRepository.FindById(projectId).LastPlanNo;
        }

        public StatusPlan? CheckPlan(ProjectPlan projectPlan)
        {
            var result = new StatusPlan();

            var check = (int)(((DateTime.UtcNow - projectPlan.StartDate) / (projectPlan.EndDate - projectPlan.StartDate)) * 100);
            if (check < 100)
            {
                if (check < projectPlan.CompletedPercentage) result = StatusPlan.Fast;
                if (check == projectPlan.CompletedPercentage || projectPlan.StartDate > DateTime.UtcNow) result = StatusPlan.Normal;
                if (check > projectPlan.CompletedPercentage && check > 0) result = StatusPlan.Slowly;
            }

            else
            {
                if (projectPlan.CompletedPercentage < 100) result = StatusPlan.Slowly;
                if (projectPlan.CompletedPercentage == 100) result = StatusPlan.Normal;
            }
            return result;
        }

        public IEnumerable<ProjectPlanResulModel> GetProjectPlanWithGantt(int pid, int? no)
        {
            var project = _projectRepository.FindById(pid);
            var planNo = 0;
            if (no.HasValue)
                planNo = no.Value;
            else
                planNo = _projectRepository.FindById(pid).LastPlanNo.Value;
            var projectPlans = _projectPlanRepository.FindAll(c => c.ProjectId == pid && c.No == planNo);
            var plans = (from projectPlan in projectPlans
                         select new ProjectPlanResulModel
                         {
                             Id = projectPlan.Id,
                             ParentId = projectPlan.ParentId == Guid.Empty ? null : projectPlan.ParentId,
                             text = projectPlan.Title.TrimStart(' '),
                             duration = projectPlan.Duration,
                             start_date = projectPlan.StartDate,
                             end_date = projectPlan.EndDate,
                             IsLeaf = projectPlan.IsLeaf,
                             No = projectPlan.No,
                             StatusCurrent = (project.Status == ProjectStatus.Approved) ? CheckPlan(projectPlan) : null,
                             progress = projectPlan.CompletedPercentage,
                             DisplayOnMobile = projectPlan.DisplayOnMobile,
                             Order = projectPlan.OrderNumber.HasValue ? projectPlan.OrderNumber.Value : 0
                         }).OrderBy(c => c.Order);
            return plans;
        }
        public async Task<IEnumerable<ProjectPlanModel>> GetProjectPlan(int projectId, int? no)
        {
            var project = _projectRepository.FindById(projectId);
            var planNo = 0;
            if (no.HasValue)
                planNo = no.Value;
            else
                planNo = _projectRepository.FindById(projectId).LastPlanNo.Value;
            var projectPlans = await _projectPlanRepository.FindAllAsync(c => c.ProjectId == projectId && c.No == planNo);
            var plans = (from projectPlan in projectPlans
                         select new ProjectPlanModel
                         {
                             Id = projectPlan.Id,
                             ParentId = projectPlan.ParentId == Guid.Empty ? null : projectPlan.ParentId,
                             Title = projectPlan.Title.TrimStart(' '),
                             Duration = projectPlan.Duration,
                             StartDate = DateTime.SpecifyKind(projectPlan.StartDate, DateTimeKind.Utc),
                             EndDate = DateTime.SpecifyKind(projectPlan.EndDate, DateTimeKind.Utc),
                             IsLeaf = projectPlan.IsLeaf,
                             No = projectPlan.No,
                             StatusCurrent = (project.Status == ProjectStatus.Approved) ? CheckPlan(projectPlan) : null,
                             CompletedPercentage = projectPlan.CompletedPercentage,
                             DisplayOnMobile = projectPlan.DisplayOnMobile,
                             Order = projectPlan.OrderNumber.HasValue ? projectPlan.OrderNumber.Value : 0
                         }).OrderBy(c => c.Order);
            return plans;
        }

        public async Task<IEnumerable<ProjectPlanModel>> GetProjectPlanByParentId(Guid? parentId)
        {
            var listProjetPlanModels = new List<ProjectPlanModel>();
            if (parentId == null || parentId == Guid.Empty)
            {
                var projectPlans = await _projectPlanRepository.FindAllAsync(c => c.ParentId == Guid.Empty);
                var projectPlanModel = projectPlans.ToList().CloneToListModels<ProjectPlan, ProjectPlanModel>();
                listProjetPlanModels.AddRange(projectPlanModel);
            }
            else
            {
                var projectPlans = await _projectPlanRepository.FindAllAsync(c => c.ParentId == parentId);
                var projectPlanModel = projectPlans.ToList().CloneToListModels<ProjectPlan, ProjectPlanModel>();
                listProjetPlanModels.AddRange(projectPlanModel);
            }
            return listProjetPlanModels.OrderBy(c => c.Order);

        }

        public async Task<ProjectPlanModel> GetProjectPlanById(Guid? id)
        {
            if (id == null || id == Guid.Empty)
                throw new ServiceException("Chưa chọn mã plan");
            else
            {
                var plan = await _projectPlanRepository.FindByIdAsync(id);
                var planModel = DomainMaps.Mapper.Map<ProjectPlan, ProjectPlanModel>(plan);
                return planModel;
            }
        }

        public async Task<bool> AddProjectPlan(ProjectPlanModel projectPlanModel, int userId)
        {
            var existUserCurrent = _projectUserRepository.Find(c => c.UserId == userId && c.ProjectId == projectPlanModel.ProjectId);
            if (existUserCurrent == null)
            {
                var result = false;
                if (AbilityApprovingProjectForEPC(projectPlanModel.ProjectId).Item1
                    && AbilityApprovingProjectForProjectManager(projectPlanModel.ProjectId).Item1)
                {
                    var entity = projectPlanModel.CloneToModel<ProjectPlanModel, ProjectPlan>();
                    result = await _projectPlanRepository.InsertAsync(entity);
                }
                else throw new ServiceException(AbilityApprovingProjectForEPC(projectPlanModel.ProjectId).Item2
                    + AbilityApprovingProjectForProjectManager(projectPlanModel.ProjectId).Item2);
                return result;
            }
            else throw new ServiceException("Bạn không thuộc dự án này nên không có quyền thao tác");
        }

        public async Task<bool> DeleteProjectPlan(ProjectPlanModel projectPlanModel, int userId)
        {
            var existUserCurrent = _projectUserRepository.Find(c => c.UserId == userId && c.ProjectId == projectPlanModel.ProjectId);
            if (existUserCurrent == null)
            {
                var exist = await _projectPlanRepository.FindAllAsync(c => c.Id == projectPlanModel.Id);
                if (exist.Count() <= 0)
                    throw new ServiceException("Không tồn tại plan này");

                var entity = projectPlanModel.CloneToModel<ProjectPlanModel, ProjectPlan>();
                return await _projectPlanRepository.DeleteAsync(entity);
            }
            else throw new ServiceException("Bạn không thuộc dự án này nên không có quyền thao tác");
        }

        public (bool, Guid) CreateFolder(ProjectFolderModel projectFolderModel, int userId)
        {
            var existUserCurrent = _projectUserRepository.Find(c => c.UserId == userId && c.ProjectId == projectFolderModel.ProjectId);

            var existProject = _projectRepository.FindAll(
            c => c.Id == projectFolderModel.ProjectId);
            if (existProject == null)
                throw new ServiceException("Không tồn tại dự án này");

            if (String.IsNullOrEmpty(projectFolderModel.Name))
            {
                throw new Exception("Yêu cầu nhập tên cho thư mục");
            }
            var folder = new ProjectFolder
            {
                Id = Guid.NewGuid(),
                ProjectId = projectFolderModel.ProjectId,
                Name = projectFolderModel.Name,
                CreatedDate = DateTime.UtcNow
            };

            if (projectFolderModel.ParentId == Guid.Empty || projectFolderModel.ParentId == null)
            {
                folder.ParentId = Guid.Empty;
            }
            else
            {
                folder.ParentId = projectFolderModel.ParentId;
            }
            return (_projectFolderRepository.Insert(folder), folder.Id);
        }

        public async Task<IEnumerable<ProjectFolderModel>> GetFolders(int projectId, Guid? parentId)
        {

            if (projectId == 0) throw new ServiceException("Chưa chọn dự án");
            var existProject = await _projectRepository.FindAllAsync(c => c.Id == projectId);
            if (existProject.Count() <= 0) throw new ServiceException("Không tồn tại dự án này");
            IEnumerable<ProjectFolderModel> listFolderModel = null;
            if (parentId == Guid.Empty || parentId == null)
            {
                var folders = await _projectFolderRepository.FindAllAsync(c => c.ProjectId == projectId);


                listFolderModel = folders.ToList().CloneToListModels<ProjectFolder, ProjectFolderModel>();
            }
            else
            {
                var folders = await _projectFolderRepository.FindAllAsync(c => c.ProjectId == projectId
                    && c.ParentId == parentId);
                listFolderModel = folders.ToList().CloneToListModels<ProjectFolder, ProjectFolderModel>();
            }
            return listFolderModel.OrderBy(c => c.CreatedDate);
        }

        public bool AllowShowOnMobile(AllowShowOnMobileModel allowShowOnMobileModel, int userId)
        {
            var entityPlan = _projectPlanRepository.FindById(allowShowOnMobileModel.PlanId);
            if (entityPlan != null)
            {
                var existUserCurrent = _projectUserRepository.Find(c => c.UserId == userId && c.ProjectId == entityPlan.ProjectId);
                if (existUserCurrent != null)
                {
                    if (allowShowOnMobileModel.Status == true)
                    {
                        entityPlan.DisplayOnMobile = true;
                        return _projectPlanRepository.Update(entityPlan);
                    }
                    else
                    {
                        entityPlan.DisplayOnMobile = false;
                        return _projectPlanRepository.Update(entityPlan);
                    }
                }
                else throw new ServiceException("Bạn không thuộc dự án này nên không có quyền thao tác");
            }
            else throw new ServiceException("Không tìm thấy plan");
        }

        public bool AllowManyPlanShowOnMobile(List<AllowShowOnMobileModel> allowShowOnMobileModels, int userId)
        {

            var result = false;
            foreach (var allowShowOnMobileModel in allowShowOnMobileModels)
            {
                var entityPlan = _projectPlanRepository.FindById(allowShowOnMobileModel.PlanId);
                if (entityPlan != null)
                {
                    var existUserCurrent = _projectUserRepository.Find(c => c.UserId == userId && c.ProjectId == entityPlan.ProjectId);
                    if (existUserCurrent != null)
                    {
                        if (allowShowOnMobileModel.Status == true)
                        {
                            entityPlan.DisplayOnMobile = true;
                            result = _projectPlanRepository.Update(entityPlan);
                        }
                        else
                        {
                            entityPlan.DisplayOnMobile = false;
                            result = _projectPlanRepository.Update(entityPlan);
                        }
                    }
                    else throw new ServiceException("Bạn không thuộc dự án này nên không có quyền thao tác");
                }
                else throw new ServiceException("Không tìm thấy plan");
            }
            return result;
        }

        public bool DeleteProject(int projectId)
        {
            _projectRepository.Delete(p => p.Id == projectId);
            _projectUserRepository.Delete(p => p.ProjectId == projectId);
            _projectReportRepository.Delete(p => p.ProjectId == projectId);
            _projectPlanRepository.Delete(p => p.ProjectId == projectId);
            _projectPictureRepository.Delete(p => p.ProjectId == projectId);
            _projectFolderRepository.Delete(p => p.ProjectId == projectId);
            _projectCompanyRepository.Delete(c => c.ProjectId == projectId);
            _workitemRepository.Delete(c => c.ProjectId == projectId);
            _safelyReportImageRepository.Delete(c => c.ProjectId == projectId);
            _safelyReportRepository.Delete(c => c.ProjectId == projectId);
            _registerMaterialRepository.Delete(c => c.ProjectId == projectId);
            return true;
        }

        public Task<ProjectModel> GetByHashId(string id)
        {
            var projectId = _hashidService.Decode(id);
            return GetProjectById(projectId);
        }

        public IEnumerable<ProjectPlanModel> GetListPlanLevel1(int projectId, int? no)
        {
            var lastPlanNo = _projectRepository.FindById(projectId).LastPlanNo;
            if (no > lastPlanNo) throw new ServiceException("Số này lớn hơn số cập nhật cuối");
            no = no ?? (lastPlanNo = lastPlanNo ?? 0);
            var planTop = _projectPlanRepository.FindAll(c => c.ProjectId == projectId
                && c.ParentId == Guid.Empty
                && c.No == no).FirstOrDefault();
            var listPlan = _projectPlanRepository.FindAll(c => c.ParentId == planTop.Id);
            return listPlan.ToList().CloneToListModels<ProjectPlan, ProjectPlanModel>();
        }

        public bool ExistUserCurrentInProject(int pid, int userId)
        {
            var projectUser = _projectUserRepository.FindAll(c => c.ProjectId == pid && c.UserId == userId);
            if (projectUser.Count() > 0)
                return true;
            else return false;
        }

        public List<ProjectModel> GetRuningProjects()
        {
            throw new NotImplementedException();
        }

        public List<ProjectModel> GetYourProjects(int userId)
        {
            throw new NotImplementedException();
        }

        public bool FinishingTheProject(int projectId)
        {
            var success = false;
            var project = _projectRepository.FindById(projectId);
            if (project == null) throw new ServiceException("Dự án không tồn tại");
            if (project.Status == ProjectStatus.Stop) throw new ServiceException("Dự án đã bị tạm dừng không hoàn thành được");
            if (project.Status != ProjectStatus.Approved) throw new ServiceException("Dự án chưa được phê duyệt");

            project.Status = ProjectStatus.Finished;
            success = _projectRepository.Update(project);

            //var workitems = _workitemRepository.FindAll(c => c.ProjectId == project.Id);
            //var plans = _projectPlanRepository.FindAll(c => c.ProjectId == project.Id && c.ParentId == null);
            //var successWorkitem = false;
            //var successPlan = false;
            //if (plans.Count() > 0)
            //{
            //    var plan = plans.Where(p => p.No == plans.Max(c => c.No)).FirstOrDefault();
            //    if (plan.CompletedPercentage < 100) successPlan = false;
            //    else successPlan = true;
            //}

            //foreach (var w in workitems)
            //{
            //    if (w.CompletedNumberTotal != w.Quantity && w.Quantity != w.TotalPaidQuantity)
            //    {
            //        successWorkitem = false;
            //        break;
            //    }
            //    else successWorkitem = true;
            //};
            //if (successPlan && successWorkitem)
            //{
            //    project.Status = ProjectStatus.Stop;
            //    success = _projectRepository.Update(project);
            //}
            //else throw new ServiceException("Dự án chưa đủ điều kiện");
            return success;
        }

        public void CreateAllFolderWhenInitializeProject(int projectId, int userId)
        {
            var congvan = CreateFolderWithName(projectId, "01. CÔNG VĂN, THÔNG BÁO", null);
            if (congvan.Item1)
            {
                var di = CreateFolderWithName(projectId, "1. ĐI", congvan.Item2);
                if (di.Item1)
                {
                    var amb = CreateFolderWithName(projectId, "AMB", di.Item2);
                    if (amb.Item1)
                    {
                        var chuaduyet = CreateFolderWithName(projectId, "1. Chưa duyệt", amb.Item2);
                        var daduyet = CreateFolderWithName(projectId, "2. Đã duyệt", amb.Item2);
                        var giahan = CreateFolderWithName(projectId, "3. Gia hạn HĐ (nếu có)", amb.Item2);
                    }
                    var cacdvkhac = CreateFolderWithName(projectId, "Các đơn vị khác", di.Item2);
                }
                var den = CreateFolderWithName(projectId, "Các đơn vị khác", congvan.Item2);
                {
                    if (den.Item1)
                    {
                        var guiamb = CreateFolderWithName(projectId, "Gửi AMB, TVTK, T.Tải", congvan.Item2);
                    }
                }
            }

            var baocao = CreateFolderWithName(projectId, "02. BÁO CÁO", null);
            if (baocao.Item1)
            {
                var tiendohangky = CreateFolderWithName(projectId, "1. TIẾN ĐỘ HÀNG KỲ", baocao.Item2);
                if (tiendohangky.Item1)
                {
                    var nam2018 = CreateFolderWithName(projectId, "NĂM 2018", tiendohangky.Item2);
                    if (nam2018.Item1)
                    {
                        var bcngay = CreateFolderWithName(projectId, "1. BÁO CÁO NGÀY", nam2018.Item2);
                        var bctuan = CreateFolderWithName(projectId, "2. Báo cáo tuần", nam2018.Item2);
                        var bcthang = CreateFolderWithName(projectId, "3. Báo cáo tháng", nam2018.Item2);
                        var bcquy = CreateFolderWithName(projectId, "4. Báo cáo quý", nam2018.Item2);
                        var bcnam = CreateFolderWithName(projectId, "5. Báo cáo năm", nam2018.Item2);
                        var bccuochop = CreateFolderWithName(projectId, "6. Báo cáo cuộc họp kiểm điểm tiến độ", nam2018.Item2);
                        var dangkytiendo = CreateFolderWithName(projectId, "7. Đăng ký tiến độ thi công tháng", nam2018.Item2);
                        var bckhcv = CreateFolderWithName(projectId, "8. Báo cáo KHCV Tuần", nam2018.Item2);
                        var kehoachtuan = CreateFolderWithName(projectId, "9. Kế hoạch Tuần, Tháng, Quý gửi A", nam2018.Item2);
                    }

                }
                var anhchup = CreateFolderWithName(projectId, "2. ẢNH CHỤP", baocao.Item2);
                if (anhchup.Item1)
                {
                    var phanmong = CreateFolderWithName(projectId, "1. Phần móng", anhchup.Item2);
                    if (phanmong.Item1)
                    {
                        var dotiepdia = CreateFolderWithName(projectId, "Đo tiếp địa", phanmong.Item2);
                        var vt01 = CreateFolderWithName(projectId, "VT01", phanmong.Item2);
                        var vt02 = CreateFolderWithName(projectId, "VT02", phanmong.Item2);
                    }

                    var phanxayke = CreateFolderWithName(projectId, "2. Phần xây kè, bờ bao", anhchup.Item2);
                    if (phanxayke.Item1)
                    {
                        var vt01 = CreateFolderWithName(projectId, "VT01", phanxayke.Item2);
                        var vt02 = CreateFolderWithName(projectId, "VT02", phanxayke.Item2);
                    }
                    var phancot = CreateFolderWithName(projectId, "3. Phần cột", anhchup.Item2);

                    if (phancot.Item1)
                    {
                        var vt01 = CreateFolderWithName(projectId, "VT01", phancot.Item2);
                        var vt02 = CreateFolderWithName(projectId, "VT02", phancot.Item2);
                    }

                    var phanday = CreateFolderWithName(projectId, "4. Phần dây", anhchup.Item2);
                    if (phanday.Item1)
                    {
                        var vt01 = CreateFolderWithName(projectId, "VT01", phanday.Item2);
                        var vt02 = CreateFolderWithName(projectId, "VT02", phanday.Item2);
                    }
                    var capngam = CreateFolderWithName(projectId, "5. Cáp ngầm", anhchup.Item2);
                    var anhgiaocheo = CreateFolderWithName(projectId, "Ảnh giao chéo đường điện", anhchup.Item2);
                    var phanxlgc = CreateFolderWithName(projectId, "Phần xử lý giao chéo", anhchup.Item2);
                }

                var atvsld = CreateFolderWithName(projectId, "3. ATVSLĐ", baocao.Item2);
                var chatluongthammy = CreateFolderWithName(projectId, "4. CHẤT LƯỢNG, THẨM MỸ HÀNG KỲ", baocao.Item2);
                var bbhientruong = CreateFolderWithName(projectId, "5. BB HIỆN TRƯỜNG", baocao.Item2);

            }

            var totrinh = CreateFolderWithName(projectId, "03. TỜ TRÌNH, QUYẾT ĐỊNH", null);

            var tailieu = CreateFolderWithName(projectId, "04. TÀI LIỆU KT", null);
            if (tailieu.Item1)
            {
                var banve = CreateFolderWithName(projectId, "1. BẢN VẼ", tailieu.Item2);
                var thuyetminh = CreateFolderWithName(projectId, "2. THUYẾT MINH, HƯỚNG DẪN", tailieu.Item2);
                var quyetdinh = CreateFolderWithName(projectId, "3. QUYẾT ĐỊNH BAN HÀNH", tailieu.Item2);
                var hoancongtuyen = CreateFolderWithName(projectId, "4. HOÀN CÔNG TUYẾN", tailieu.Item2);
                var bienphap = CreateFolderWithName(projectId, "5. BIỆN PHÁP TCTC", tailieu.Item2);
                var hethongql = CreateFolderWithName(projectId, "6. HỆ THỐNG QUẢN LÝ CHẤT LƯỢNG", tailieu.Item2);
            }
            var tienluong = CreateFolderWithName(projectId, "05. TIÊN LƯỢNG, ĐỊNH MỨC", null);
            var vattu = CreateFolderWithName(projectId, "06. QUẢN LÝ VTTB, MÁY MÓC TBVP", null);
            if (vattu.Item1)
            {
                var vattucongtycap = CreateFolderWithName(projectId, "05. TIÊN LƯỢNG, ĐỊNH MỨC", vattu.Item2);
                var vattuacap = CreateFolderWithName(projectId, "05. TIÊN LƯỢNG, ĐỊNH MỨC", vattu.Item2);
                var maymoctb = CreateFolderWithName(projectId, "05. TIÊN LƯỢNG, ĐỊNH MỨC", vattu.Item2);
            }

            var trienkhai = CreateFolderWithName(projectId, "07. CHUẨN BỊ TRIỂN KHAI", null);
            var nghiemthu = CreateFolderWithName(projectId, "08. NGHIỆM THU, THANH TOÁN", null);
            if (nghiemthu.Item1)
            {
                var nghiemthuab = CreateFolderWithName(projectId, "1. Nghiệm thu A-B", nghiemthu.Item2);
                var nghiemthunb = CreateFolderWithName(projectId, "2. Nghiệm thu NB", nghiemthu.Item2);
            }
            var hopdong = CreateFolderWithName(projectId, "09. HỢP ĐỒNG GIAO KHOÁN", null);
            if (hopdong.Item1)
            {
                var hopdongvoia = CreateFolderWithName(projectId, "1. Hợp đồng với A", hopdong.Item2);
                var giaokhoannoibo = CreateFolderWithName(projectId, "2. Giao khoán nội bộ", hopdong.Item2);
            }
            var taichinh = CreateFolderWithName(projectId, "10. TÀI CHÍNH", null);
            if (taichinh.Item1)
            {
                var totrinhtamung = CreateFolderWithName(projectId, "1. TỜ TRÌNH, TẠM ỨNG", taichinh.Item2);
                var doichieu = CreateFolderWithName(projectId, "2. ĐỐI CHIẾU CÔNG NỢ", taichinh.Item2);
            }
            var denbu = CreateFolderWithName(projectId, "11. HỒ SƠ ĐỀN BÙ", null);
            if (denbu.Item1)
            {
                var denbua = CreateFolderWithName(projectId, "1. Đền bù A", denbu.Item2);
                if (denbua.Item1)
                {
                    var quyetdinh = CreateFolderWithName(projectId, "1. QUYẾT ĐỊNH", denbua.Item2);
                    var phuongan = CreateFolderWithName(projectId, "2. PHƯƠNG ÁN", denbua.Item2);
                    var tamung = CreateFolderWithName(projectId, "3. TẠM ỨNG, CHI TRẢ", denbua.Item2);
                }
                var denbuthicong = CreateFolderWithName(projectId, "2. Đền bù thi công", denbu.Item2);
            }
            var thongtin = CreateFolderWithName(projectId, "12. THÔNG TIN KHÁC", null);
            if (thongtin.Item1)
            {
                var congvanthongbao = CreateFolderWithName(projectId, "1. CÔNG VĂN, THÔNG BÁO", thongtin.Item2);
                var tailieutintuc = CreateFolderWithName(projectId, "2. TÀI LIỆU, TIN TỨC", thongtin.Item2);
            }
        }

        public (bool, Guid) CreateFolderWithName(int projectId, string name, Guid? parentId)
        {
            var folder = new ProjectFolder
            {
                Id = Guid.NewGuid(),
                ProjectId = projectId,
                ParentId = parentId,
                Name = name,
                CreatedDate = DateTime.UtcNow
            };
            System.Threading.Thread.Sleep(1);
            return (_projectFolderRepository.Insert(folder), folder.Id);
        }

        public InfoFolderModel GetInfoFoler(string id)
        {
            var idf = Guid.Parse(id);
            var folder = _projectFolderRepository.Find(c => c.Id == idf);
            if (folder == null) throw new ServiceException("Thư mục không tồn tại");
            var res = new InfoFolderModel
            {
                FolderName = folder.Name,
            };
            if (folder.ParentId.HasValue)
                res.ParentId = folder.ParentId.Value.ToString();
            else
                res.ParentId = null;
            return res;
        }

        public bool DeleteFolder(string folderId)
        {
            var result = false;
            var trans = _projectFolderRepository.BeginTransaction();
            if (trans.Connection.State != ConnectionState.Open) trans.Connection.Open();
            try
            {
                var id = Guid.Parse(folderId);
                var folder = _projectFolderRepository.Find(c => c.Id == id, trans);
                if (folder != null)
                {
                    result = DeleteFolderWithParent(folder.Id, trans);
                    _projectFolderRepository.Delete(folder, trans);

                    if (!result)
                    {
                        trans.Rollback();
                    }
                }
            }
            catch (Exception)
            {
                trans.Rollback();
                throw;
            }
            trans.Commit();
            return result;
        }

        public bool DeleteFolderWithParent(Guid parentId, IDbTransaction trans)
        {

            var result = true;
            var childrens = _projectFolderRepository.FindAll(c => c.ParentId == parentId, trans);
            if (childrens.Count() > 0)
            {

                foreach (var children in childrens)
                {
                    result = _projectFolderRepository.Delete(children, trans);
                    DeleteFolderWithParent(children.Id, trans);
                    if (!result) break;
                }

            }



            return result;
        }

        public IEnumerable<ProjectInfoMiniModel> ListProjects()
        {
            var projects = _projectRepository.FindAll().OrderByDescending(c => c.Id);
            return from p in projects
                   select new ProjectInfoMiniModel
                   {
                       HashId = _hashidService.Encode(p.Id),
                       ProjectName = p.Name
                   };
        }

        public bool ChangeStatusProjectToInit(ChangeProjectStatusModel statusModel)
        {
            var project = _projectRepository.FindById(statusModel.ProjectId);
            if (project == null) throw new ServiceException("Dự án không tồn tại");
            project.Status = statusModel.Status;
            return _projectRepository.Update(project);
        }

        public bool UpdatePlan(UpdatePlanModel model)
        {
            var result = false;
            var project = _projectRepository.FindById(model.ProjectId);
            if (project == null) throw new ServiceException("Dự án không tồn tại");

            var plan = _projectPlanRepository.FindById(model.Id);
            if (plan == null) throw new ServiceException("Kế hoạch này không tồn tại");


            if (model.StartDate.Date > model.EndDate.Date) throw new ServiceException("Yêu cầu chọn ngày bắt đầu nhỏ hơn ngày kết thúc");
            plan.Duration = (int)(DateTime.SpecifyKind(model.EndDate, DateTimeKind.Utc) - DateTime.SpecifyKind(model.StartDate, DateTimeKind.Utc)).TotalDays + 1;
            plan.StartDate = model.StartDate;
            plan.EndDate = model.EndDate;
            plan.Title = model.Title;
            result = _projectPlanRepository.Update(plan);

            var planInProjects = _projectPlanRepository.FindAll(c => c.ProjectId == project.Id).ToList();
            UpdatePlanParent(plan, planInProjects);
            return result;
        }

        protected void UpdatePlanParent(ProjectPlan plan, List<ProjectPlan> planInProjects)
        {

            var planParrent = _projectPlanRepository.FindById(plan.ParentId);
            if (planParrent != null)
            {
                var planSameLevels = planInProjects.Where(c => c.ParentId == planParrent.Id).ToList();
                var maxEndDate = planSameLevels.Max(c => c.EndDate);
                planParrent.EndDate = maxEndDate;
                planParrent.Duration = (int)(DateTime.SpecifyKind(planParrent.EndDate, DateTimeKind.Utc) - DateTime.SpecifyKind(planParrent.StartDate, DateTimeKind.Utc)).TotalDays + 1;

                _projectPlanRepository.Update(planParrent);
                UpdatePlanParent(planParrent, planInProjects);
            }
        }




        public bool CheckHasPermissionProject(string hashId, int userId, PermissionCode code)
        {
            var result = false;
            try
            {
                var projectId = _hashidService.Decode(hashId);
                var projectUser = _projectUserRepository.Find(c => c.ProjectId == projectId && c.UserId == userId);

                var user = _userRepository.FindById(userId);
                var roleBoss = _roleService.GetById(11);
                if (user.UserRoles.Contains(";11;"))
                {
                    result = true;

                }
                else
                {
                    if (projectUser != null)
                    {
                        var roleProjects = projectUser.ProjectRoles;

                        var permissionInProjects = new List<int>();
                        var role = _roleService.Filter(new RoleFilterModel());
                        var x = role.Records;
                        var permissions = string.Empty;
                        foreach (var item in roleProjects.Split(new string[] { ";" }, StringSplitOptions.RemoveEmptyEntries).Select(c => Convert.ToInt32(c)).ToArray())
                        {
                            var check = role.Records.FirstOrDefault(c => c.Id == item);
                            if (check != null)
                            {
                                permissionInProjects.AddRange(check.Permissions.Split(new string[] { ";" }, StringSplitOptions.RemoveEmptyEntries).Select(c => Convert.ToInt32(c)).ToArray());
                            }
                        }
                        permissions = ";" + string.Join(";", permissionInProjects.Distinct()) + ";";

                        result = permissions.Contains(";" + code.GetHashCode() + ";");
                    }
                }


            }
            catch (Exception ex)
            {
                throw ex;
            }
            return result;
        }

        public (string, string, bool, bool, int?) GetProjectRole(string hashId, int userId)
        {
            int? companyId;
            var permission = string.Empty;
            var roles = string.Empty;
            var access = false;
            var isBoss = false;
            var projectId = _hashidService.Decode(hashId);
            var user = _userRepository.FindById(userId);
            var roleBoss = _roleService.GetById(11);
            if (user.UserRoles.Contains(";11;"))
            {
                permission = roleBoss.Permissions;
                access = true;
                roles = ";11;";
                isBoss = true;
            }
            else
            {
                var projectUser = _projectUserRepository.Find(c => c.ProjectId == projectId && c.UserId == userId);
                var permissions = string.Empty;
                if (projectUser != null)
                {
                    var role = _roleService.Filter(new RoleFilterModel());
                    var x = role.Records;

                    try
                    {
                        var permissionInProjects = new List<int>();
                        var add = new List<string>();
                        foreach (var item in projectUser.ProjectRoles.Split(new string[] { ";" }, StringSplitOptions.RemoveEmptyEntries).Select(c => Convert.ToInt32(c)).ToArray())
                        {
                            var check = role.Records.FirstOrDefault(c => c.Id == item);
                            if (check != null)
                            {
                                permissionInProjects.AddRange(check.Permissions.Split(new string[] { ";" }, StringSplitOptions.RemoveEmptyEntries).Select(c => Convert.ToInt32(c)).ToArray());
                            }
                        }
                        permissions = ";" + string.Join(";", permissionInProjects.Distinct()) + ";";
                    }
                    catch (Exception ex)
                    {

                        throw ex;
                    }
                    permission = permissions;
                    roles = projectUser.ProjectRoles;
                    access = true;
                }
                else
                {
                    permission = permissions;
                    roles = string.Empty;
                    access = false;
                }
            }
            companyId = user.CompanyId;
            return (roles, permission, access, isBoss, companyId);

        }
    }
}

