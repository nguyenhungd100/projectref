﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using PCC1.PMS.Domain.Entity;
using PCC1.PMS.Domain.Enums;
using PCC1.PMS.Domain.Models;
using PCC1.PMS.Domain.Models.Companies;
using PCC1.PMS.Domain.Models.File;
using PCC1.PMS.Domain.Models.MediaStorage;
using PCC1.PMS.Domain.Models.Notification;
using PCC1.PMS.Domain.Models.Projects;
using PCC1.PMS.Domain.Models.ProjectUsers;
using PCC1.PMS.Domain.Models.Users;
using PCC1.PMS.Domain.Services.Roles;

namespace PCC1.PMS.Domain.Services.Projects
{
    public interface IProjectService
    {
        /// <summary>
        /// Search project by querry
        /// </summary>
        /// <param name="projectSearchModel"></param>
        /// <param name="userCurrent"></param>
        /// <returns></returns>
        SearchProjectResult Search(SearchProjectModel projectSearchModel, int userCurrent);


        /// <summary>
        /// Get info Project by Id
        /// </summary>
        /// <param name="projectId"></param>
        /// <returns></returns>
        Task<ProjectModel> GetProjectById(int projectId);

        /// <summary>
        /// Cập nhật thông tin dự án từ phía tổng công ty
        /// </summary>
        /// <param name="projectUpdateModel"></param>
        /// <returns></returns>
        Task<bool> UpdateProject(UpdateProjectModel projectUpdateModel);

        bool UpdateCompaniesInProject(UpdateCompanyInProject model);

        List<ProjectModel> GetRuningProjects();

        /// <summary>
        /// Khởi tạo thông tin cơ bản dựa án
        /// </summary>
        /// <param name="projectAddModel"></param>
        /// <returns></returns>
        ProjectModel InitInfoProject(ProjectAddModel projectAddModel);

        List<ProjectModel> GetYourProjects(int userId);

        List<UserModel> GetProjectUsers(int projectId);

        (bool, string) AbilityApprovingProjectForEPC(int projectId);

        (bool, string) AbilityApprovingProjectForProjectManager(int projectId);

        /// <summary>
        /// Thêm 1 nhân sự cho dự án
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        Task<bool> AddMember(AddProjectMembersModel model);

        Task<ProjectModel> GetByHashId(string hashId);

        /// <summary>
        /// Update 1 personnal for Project
        /// </summary>
        /// <param name="projectUserModel"></param>
        /// <returns></returns>
        Task<bool> UpdatePersonnal(ProjectUserModel projectUserModel);

        /// <summary>
        /// Update many personnal from Project
        /// </summary>
        /// <param name="projectUserModels"></param>
        /// <returns></returns>
        Task<bool> UpdateMultiPersonnal(List<ProjectUserModel> projectUserModels);

        /// <summary>
        /// Xóa nhiều nhân sự trong 1 dự án
        /// </summary>
        /// <param name="deleteProjectUserModel"></param>
        /// <returns></returns>
        bool RemoveProjectMembers(RemoveProjectMembersModel deleteProjectUserModel);


        (bool, string) CheckAbilityApprovingProject(int projectId);

        bool ApproveProject(int projectId, int approveUser);

        /// <summary>
        /// Cập nhật tổng thể cho dự án từ phía cán bộ quản lý dự án
        /// </summary>
        /// <param name="projectOverallModel"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        Task<bool> UpdateOverallProject(ProjectOverallModel projectOverallModel, int userId);
       
        void SendingAbilityApproveProjectToUserCreated(int projectId);

        Task<bool> NewPlans(int projectId, int userId, PlanParsingModel planParsingModel);

        int? CheckLastPlanNo(int projectId);

        Task<IEnumerable<ProjectPlanModel>> GetProjectPlan(int projectId, int? no);

        IEnumerable<ProjectPlanResulModel> GetProjectPlanWithGantt(int pid, int? no);

        Task<IEnumerable<ProjectPlanModel>> GetProjectPlanByParentId(Guid? parentId);

        Task<ProjectPlanModel> GetProjectPlanById(Guid? id);

        Task<IEnumerable<ProjectFolderModel>> GetFolders(int projectId, Guid? parentId);

        Task<bool> AddProjectPlan(ProjectPlanModel projectPlanModel, int userId);

        Task<bool> DeleteProjectPlan(ProjectPlanModel projectPlanModel, int userId);

        (bool, Guid) CreateFolder(ProjectFolderModel projectFolderModel, int userId);

        bool AllowShowOnMobile(AllowShowOnMobileModel allowShowOnMobileModel, int userId);

        bool AllowManyPlanShowOnMobile(List<AllowShowOnMobileModel> allowShowOnMobileModels, int userId);

        bool DeleteProject(int projectId);

        IEnumerable<ProjectPlanModel> GetListPlanLevel1(int projectId, int? no);

        bool ExistUserCurrentInProject(int pid, int userId);

        bool FinishingTheProject(int projectId);

        InfoFolderModel GetInfoFoler(string id);

        bool DeleteFolder(string folderId);

        IEnumerable<ProjectInfoMiniModel> ListProjects();

        bool ChangeStatusProjectToInit(ChangeProjectStatusModel statusModel);

        bool UpdatePlan(UpdatePlanModel model);

        (string, string, bool, bool, int?) GetProjectRole(string hashId, int userId);

        bool CheckHasPermissionProject(string hashId, int userId, PermissionCode code);

    }
}
