﻿using MicroOrm.Dapper.Repositories;
using PCC1.PMS.Api.Modules.Companies;
using PCC1.PMS.Domain.Entity;
using PCC1.PMS.Domain.Enums;
using PCC1.PMS.Domain.Models.Companies;
using PCC1.PMS.Domain.Repositories;
using PCC1.PMS.Domain.Services.Common;
using PCC1.PMS.Framework.Utils;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PCC1.PMS.Domain.Services.Companies.Implementation
{
    public class CompanyService : ICompanyService
    {
        private readonly ICompanyRepository _companyRepository;
        private readonly IDapperRepository<ProjectCompany> _companyProjectRepository;
        private readonly IDapperRepository<WorkitemCompany> _workItemCompanyRepository;
        private readonly IHashidService _hashidService;
        private readonly IProjectRepository _projectRepository;

        public CompanyService(ICompanyRepository companyRepository,
            IDapperRepository<ProjectCompany> companyProjectRepository,
            IDapperRepository<WorkitemCompany> workItemCompanyRepository,
            IHashidService hashidService,
            IProjectRepository projectRepository)
            
        {
            _companyRepository = companyRepository;
            _companyProjectRepository = companyProjectRepository;
            _workItemCompanyRepository = workItemCompanyRepository;
            _hashidService = hashidService;
            _projectRepository = projectRepository;
        }

        public async Task<IEnumerable<CompanyModel>> GetAllCommpany()
        {
            var companies = await _companyRepository.FindAllAsync();
            var result = DomainMaps.Mapper.Map<List<CompanyModel>>(companies);
            return result;
        }

        public CompanyModel GetInfoCompany(int companyId)
        {
            var existCompany = _companyRepository.FindById(companyId);
            if (existCompany == null) throw new ServiceException("Không tồn tại công ty này");
            return existCompany.CloneToModel<Company, CompanyModel>();
        }

        public SearchCompanyResult SearchCompanies(SearchCompanyModel search)
        {
           var result = new SearchCompanyResult();
            IEnumerable<Company> companies = new List<Company>();
            int totalRecords = 0;
            companies = _companyRepository.Search(search, out totalRecords).ToList();
            if (!string.IsNullOrEmpty(search.HashId))
            {
                search.ProjectId = _hashidService.Decode(search.HashId);
                var companyIds = companies.Select(c => c.Id);
                var companyInProjects = _companyProjectRepository.FindAll(c => companyIds.Contains(c.CompanyId) 
                    && c.ProjectId == search.ProjectId);
                if (companyInProjects.Count() > 0)
                {
                    var companyInProjectIds = companyInProjects.Select(c => c.CompanyId).Distinct();
                    companies = companies.Where(c => companyInProjectIds.Contains(c.Id));
                    totalRecords = companies.Count();
                }
            }
            companies = companies.OrderByDescending(c => c.Id).Skip(search.PageSize * (search.PageIndex - 1)).Take(search.PageSize);
            result.Records = companies.ToList().CloneToListModels<Company,CompanyModel>();
            search.TotalRecord = totalRecords;
            result.TotalRecord = search.TotalRecord;
            result.PageIndex = search.PageIndex;
            result.PageSize = search.PageSize;
            result.PageCount = result.TotalRecord / result.PageSize + (result.TotalRecord % result.PageSize > 0 ? 1 : 0);
            return result;
        }

        public IEnumerable<CompanyModel> ListCompanyAssigned(int workItemId)
        {
            var workItemCompany = _workItemCompanyRepository.FindAll(c => c.WorkItemId == workItemId);
            var company = _companyRepository.FindAll();
            return (from c in company
                    join w in workItemCompany on c.Id equals w.CompanyId
                    select c).ToList().CloneToListModels<Company, CompanyModel>();
        }

        public IEnumerable<CompanyModel> ListCompanyByProjectId(int pid)
        {
            var result = new List<CompanyModel>();
            var companyProject = _companyProjectRepository.FindAll(c => c.ProjectId == pid);
            foreach (var item in companyProject)
            {
                var entity = _companyRepository.FindById(item.CompanyId);
                result.Add(entity.CloneToModel<Company, CompanyModel>());
            }
            return result;
        }

        public bool SaveCompany(CompanyModel company)
        {
            var result = false;
            var trans = _companyRepository.BeginTransaction();
            if (trans.Connection.State != ConnectionState.Open) trans.Connection.Open();
            try
            {
                if (string.IsNullOrEmpty(company.Name) && string.IsNullOrEmpty(company.FullName)) throw new ServiceException("Tên công ty không được để trống");
                var Companies = _companyRepository.FindAll(trans);
                var CompanieIds = Companies.Select(c => c.Id).ToList();
                if (company == null) throw new Exception("Nhập thông tin công ty");
                if (company.Id.HasValue && CompanieIds.Contains(company.Id.Value))
                {
                    var item = company.CloneToModel<CompanyModel, Company>();
                    result = _companyRepository.Update(item, trans);
                }
                else
                {
                    if (Companies.Any(c => c.Name.Equals(company.Name))) throw new ServiceException("Tên công ty đã trùng");
                    var item = company.CloneToModel<CompanyModel, Company>();
                    result = _companyRepository.Insert(item, trans);
                }
            }
            catch(Exception ex)
            {
                trans.Rollback();
                throw ex;
            }

            trans.Commit();           
            return result;
        }

        public bool DeleteCompany(List<DeleteCompanyModel> companies)
        {
            var projects = _projectRepository.FindAll(c=>c.Status != ProjectStatus.Stop);
            var projectIds = projects.Select(c => c.Id);
            var result = false;
            var trans = _companyRepository.BeginTransaction();
            if (trans.Connection.State != ConnectionState.Open) trans.Connection.Open();
            try
            {
                var projectCompany = _companyProjectRepository.FindAll(trans);
                foreach (var company in companies)
                {
                    var projectRes = new List<Project>();
                    var existInProject = projectCompany.Where(c => c.CompanyId == company.id);
                    if (existInProject.Count()>0)
                    {
                        var checkProjectIds = existInProject.Select(c => c.ProjectId).Distinct();                       
                        foreach (var project in projects)
                        {
                            if (checkProjectIds.Contains(project.Id))
                                projectRes.Add(project);
                        }
                    }
                    
                    string listProjectNames = string.Join(", ", projectRes.Select(c => c.Name));
                    if (projectCompany.Any(c => c.CompanyId == company.id))
                    {
                        throw new ServiceException("Công ty này đang tham gia các dự án " + listProjectNames);
                    }
                    else
                    {
                        result = _companyRepository.Delete(new Company { Id = company.id}, trans);
                        if (!result)
                        {
                            trans.Rollback();
                            break;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                trans.Rollback();
                throw ex;
            }
            trans.Commit();
            return result;
        }
    }
}
