﻿using PCC1.PMS.Api.Modules.Companies;
using PCC1.PMS.Domain.Models.Companies;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace PCC1.PMS.Domain.Services.Companies
{
    public interface ICompanyService
    {
        Task<IEnumerable<CompanyModel>> GetAllCommpany();

        CompanyModel GetInfoCompany(int companyId);

        IEnumerable<CompanyModel> ListCompanyByProjectId(int pid);

        IEnumerable<CompanyModel> ListCompanyAssigned(int workItemId);
        bool SaveCompany(CompanyModel company);

        SearchCompanyResult SearchCompanies(SearchCompanyModel search);
        bool DeleteCompany(List<DeleteCompanyModel> companies);
    }
}
