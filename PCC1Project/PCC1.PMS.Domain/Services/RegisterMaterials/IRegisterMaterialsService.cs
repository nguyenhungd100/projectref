﻿using PCC1.PMS.Domain.Models.Materials;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace PCC1.PMS.Domain.Services.RegisterMaterials
{
    public interface IRegisterMaterialsService
    {
        SearchRegisterMaterialResult Search(SearchRegisterMaterialModel search);

        MaterialModel GetRegisterMaterialById(int id);

        bool AddRegisterMaterials(RegisterMaterialsModel register, int userId);

        bool UpdateRegisterMaterials(UpdateMaterialModel update, int userId);

        bool DeleteMultiRegisterMaterials(IEnumerable<DeleteMaterialModel> materialModels, int userId);

        bool ApproveRegisterMaterial(IEnumerable<ApproveRegisterMaterialModel> approveModels);

        bool RejectRegisterMaterial(IEnumerable<ApproveRegisterMaterialModel> approveModels);
    }
}
