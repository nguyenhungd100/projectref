﻿using MicroOrm.Dapper.Repositories;
using PCC1.PMS.Domain.Entity;
using PCC1.PMS.Domain.Enums;
using PCC1.PMS.Domain.Models.Materials;
using PCC1.PMS.Domain.Models.Notification;
using PCC1.PMS.Domain.Repositories;
using PCC1.PMS.Domain.Services.Common;
using PCC1.PMS.Framework.Utils;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PCC1.PMS.Domain.Services.RegisterMaterials.Implementation
{
    public class RegisterMaterialsService : IRegisterMaterialsService
    {
        private readonly IRegisterMaterialRepository _registerMaterialRepository;
        private readonly IDapperRepository<Project> _projectRepository;
        private readonly IDapperRepository<ProjectUser> _projectUserRepository;
        private readonly INotificationService _notificationService;
        private readonly IHashidService _hashidService;
        private readonly IDapperRepository<User> _userRepository;

        public RegisterMaterialsService(IRegisterMaterialRepository registerMaterialRepository,
            IDapperRepository<Project> projectRepository,
            IDapperRepository<ProjectUser> projectUserRepository,
            INotificationService notificationService,
            IHashidService hashidService,
            IDapperRepository<User> userRepository
        )
        {
            _registerMaterialRepository = registerMaterialRepository;
            _projectRepository = projectRepository;
            _projectUserRepository = projectUserRepository;
            _notificationService = notificationService;
            _hashidService = hashidService;
            _userRepository = userRepository;
        }

        public SearchRegisterMaterialResult Search(SearchRegisterMaterialModel search)
        {
            var result = new SearchRegisterMaterialResult();
            int totalRecords = 0;
            var registerMaterials = new List<RegisterMaterial>();

            registerMaterials = _registerMaterialRepository.Search(search, out totalRecords).ToList();

            //ok
            registerMaterials = registerMaterials.OrderByDescending(c => c.Id).Skip(search.PageSize * (search.PageIndex - 1)).Take(search.PageSize).ToList();

            //var projectIds = registerMaterials.Select(a => a.ProjectId);
            var projects = _projectRepository.FindAll();
            var petitionerIds = registerMaterials.Select(c => c.Petitioner);
            var users = _userRepository.FindAll(c => petitionerIds.Contains(c.Id));

            result.Records = DomainMaps.Mapper.Map<List<MaterialModel>>(registerMaterials);

            foreach (var item in result.Records)
            {
                item.HashId = _hashidService.Encode(item.ProjectId);
                item.PetitionerName = users.FirstOrDefault(c => c.Id == item.Petitioner).FullName
                    ?? users.FirstOrDefault(c => c.Id == item.Petitioner).Email;
                item.ProjectName = projects.FirstOrDefault(c => c.Id == item.ProjectId).Name;
                if (item.TimeRequired.HasValue)
                    item.TimeRequired = item.TimeRequired.Value.ToLocalTime();

            }
            result.TotalRecord = search.TotalRecord;
            result.PageIndex = search.PageIndex;
            result.PageSize = search.PageSize;
            result.PageCount = result.TotalRecord / result.PageSize + (result.TotalRecord % result.PageSize > 0 ? 1 : 0);

            return result;
        }

        public MaterialModel GetRegisterMaterialById(int id)
        {
            var entity = _registerMaterialRepository.FindById(id);
            if (entity != null)
            {
                var result = DomainMaps.Mapper.Map<RegisterMaterial, MaterialModel>(entity);
                if (result.TimeRequired.HasValue)
                    DateTime.SpecifyKind(result.TimeRequired.Value, DateTimeKind.Utc);
                return result;
            }
            else throw new ServiceException("Phiếu đăng ký vật tư không tồn tại");
        }

        public bool AddRegisterMaterials(RegisterMaterialsModel register, int userId)
        {
            var project = _projectRepository.FindById(register.ProjectId);
            if (project == null) throw new ServiceException("Dự án không tồn tại");

            var existUser = _projectUserRepository.Find(c => c.UserId == userId && c.ProjectId == register.ProjectId);
            if (existUser == null) throw new ServiceException("Bạn không thuộc dự án này nên không có quyền");

            if (project.Status != ProjectStatus.Approved) throw new ServiceException("Dự án này chưa được phê duyệt");
            if (register.Quantity <= 0) throw new ServiceException("Số lượng đăng ký không được nhỏ hơn 0");

            var add = new RegisterMaterial()
            {
                ProjectId = register.ProjectId,
                Name = register.Name,
                Quantity = register.Quantity,
                Note = register.Note,
                CreatedDate = DateTime.UtcNow,
                Petitioner = userId,
                Status = RegisterMaterialStatus.NotApproved
            };
            if (register.TimeRequired.HasValue) add.TimeRequired = register.TimeRequired.Value.ToLocalTime();

            var userRegister = _userRepository.FindById(userId);
            var purchasingChief = _userRepository.FindAll(c => c.UserRoles.Contains("10"));
            if (purchasingChief.Count() > 0)
            {
                var notificationModel = new RegisterMaterialEventModel()
                {
                    Type = EventType.RegisterMaterial,
                    ProjectHashId = _hashidService.Encode(project.Id),
                    ProjectName = _projectRepository.FindById(project.Id).Name,
                    ProjectId = project.Id,
                    UserRegisterId = userRegister.Id,
                    UserRegisterName = string.IsNullOrEmpty(userRegister.FullName) ? userRegister.Email : userRegister.FullName,

                };
                _notificationService.AddNotification(purchasingChief.Select(c => c.Id).ToList(), notificationModel);
            }
            return _registerMaterialRepository.Insert(add);
        }

        public bool UpdateRegisterMaterials(UpdateMaterialModel update, int userId)
        {
            var existRegisterMaterials = _registerMaterialRepository.FindById(update.Id);
            if (existRegisterMaterials == null) throw new ServiceException("Không tồn tại phiếu đăng ký vật tư này");
            if (update.Quantity <= 0) throw new ServiceException("Số lượng đăng ký không được nhỏ hơn 0");
            if (existRegisterMaterials.Status != RegisterMaterialStatus.Approved)
            {
                if (existRegisterMaterials.Petitioner == userId)
                {
                    existRegisterMaterials.Name = update.Name;
                    existRegisterMaterials.Quantity = update.Quantity;
                    existRegisterMaterials.Note = update.Note;
                    existRegisterMaterials.TimeRequired = update.TimeRequired;
                    return _registerMaterialRepository.Update(existRegisterMaterials);
                }
                else throw new ServiceException("Không cập nhật được vật tư do phiếu vật tư này không đúng người tạo");
            }
            else throw new ServiceException("Phiếu này đã được duyệt nên không được phép cập nhật");


        }

        public bool DeleteMultiRegisterMaterials(IEnumerable<DeleteMaterialModel> materialModels, int userId)
        {
            var success = false;
            var trans = _registerMaterialRepository.BeginTransaction();
            if (trans.Connection.State != ConnectionState.Open) trans.Connection.Open();
            try
            {
                foreach (var materialModel in materialModels)
                {
                    var existRegister = _registerMaterialRepository.FindById(materialModel.Id, trans);
                    if (existRegister == null) throw new ServiceException("Phiếu đăng ký vật tư này không tồn tại");

                    if (existRegister.Status == RegisterMaterialStatus.Approved) throw new ServiceException("Phiếu đăng ký vật tư này đã được duyệt");

                    if (existRegister.Petitioner != userId) throw new ServiceException("Bạn không phải người tạo phiếu đăng ký vật tư này nên không được phép xóa");

                    success = _registerMaterialRepository.Delete(existRegister, trans);
                    if (!success)
                    {
                        trans.Rollback();
                        break;
                    }
                }
            }
            catch (Exception ex)
            {
                trans.Rollback();
                throw ex;
            }
            trans.Commit();
            return success;
        }

        public bool ApproveRegisterMaterial(IEnumerable<ApproveRegisterMaterialModel> approveModels)
        {
            var result = false;
            var trans = _registerMaterialRepository.BeginTransaction();
            if (trans.Connection.State != ConnectionState.Open) trans.Connection.Open();
            try
            {
                foreach (var approve in approveModels)
                {
                    var entity = _registerMaterialRepository.FindById(approve.Id, trans);
                    if (entity == null) throw new ServiceException("Phiếu đăng ký vật tư này không tồn tại");
                    if (entity.Status == RegisterMaterialStatus.Approved) throw new ServiceException("Phiếu đăng ký vật tư này đã được duyệt");

                    entity.Status = RegisterMaterialStatus.Approved;
                    entity.ResponseMessage = string.Empty;
                    entity.Approver = approve.ApproveUser;

                    result = _registerMaterialRepository.Update(entity, trans);

                    if (!result)
                    {
                        trans.Rollback();
                        break;
                    }
                    else
                    {
                        var userRegister = _userRepository.FindById(entity.Petitioner, trans);

                        var approveUser = _userRepository.FindById(entity.Approver, trans);
                        var t = _projectRepository.FindById(entity.ProjectId, trans);
                        var notificationModel = new ApproveRegisterMaterialEventModel()
                        {
                            Type = EventType.ApproveMaterial,
                            ProjectHashId = _hashidService.Encode(entity.ProjectId),
                            ProjectName = _projectRepository.FindById(entity.ProjectId, trans).Name,
                            ProjectId = entity.ProjectId,
                            ApproveUserId = approve.ApproveUser,
                            ApproveUserName = string.IsNullOrEmpty(approveUser.FullName) ? approveUser.Email : approveUser.FullName,

                        };
                        var userIds = new List<int>();
                        userIds.Add(userRegister.Id);
                        _notificationService.AddNotification(userIds, notificationModel);

                    }
                }
            }
            catch (Exception ex)
            {
                trans.Rollback();
                throw ex;
            }
            trans.Commit();
            return result;
        }

        public bool RejectRegisterMaterial(IEnumerable<ApproveRegisterMaterialModel> approveModels)
        {
            var result = false;
            var trans = _registerMaterialRepository.BeginTransaction();
            if (trans.Connection.State != ConnectionState.Open) trans.Connection.Open();
            try
            {
                foreach (var approve in approveModels)
                {
                    var entity = _registerMaterialRepository.FindById(approve.Id, trans);
                    if (entity == null) throw new ServiceException("Phiếu đăng ký vật tư này không tồn tại");
                    if (entity.Status == RegisterMaterialStatus.Approved) throw new ServiceException("Phiếu đăng ký vật tư này đã được duyệt");

                    entity.Status = RegisterMaterialStatus.Reject;
                    entity.ResponseMessage = approve.ResponseMessage;
                    entity.Approver = approve.ApproveUser;
                    result = _registerMaterialRepository.Update(entity, trans);

                    if (!result)
                    {
                        trans.Rollback();
                        break;
                    }
                    else
                    {
                        var userRegister = _userRepository.FindById(entity.Petitioner, trans);

                        var rejectUser = _userRepository.FindById(entity.Approver, trans);
                        var notificationModel = new RejectRegisterMaterialModel()
                        {
                            Type = EventType.RejectMaterial,
                            ProjectHashId = _hashidService.Encode(entity.ProjectId),
                            ProjectName = _projectRepository.FindById(entity.ProjectId, trans).Name,
                            ProjectId = entity.ProjectId,
                            RejectUserId = approve.ApproveUser,
                            RejectUserName = string.IsNullOrEmpty(rejectUser.FullName) ? rejectUser.Email : rejectUser.FullName,

                        };
                        var userIds = new List<int>();
                        userIds.Add(userRegister.Id);
                        _notificationService.AddNotification(userIds, notificationModel);
                    }
                }
            }
            catch (Exception ex)
            {
                trans.Rollback();
                throw ex;
            }
            trans.Commit();
            return result;
            //t
        }
    }
}