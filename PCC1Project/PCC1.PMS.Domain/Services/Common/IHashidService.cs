﻿using HashidsNet;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PCC1.PMS.Domain.Services.Common
{
    public interface IHashidService
    {
        string Encode(int id);
        int Decode(string hashId);
    }
    public class HashidService : IHashidService
    {
        string salt = "!@#$%^&*7410963.zxcvbnmasdfasdfasgagqwemmcnhsdjhskoppoiuyy";
        Hashids hashids;
        public HashidService()
        {
            hashids = new Hashids(salt, 8);
        }
        public int Decode(string hashId)
        {
            return hashids.Decode(hashId).FirstOrDefault();
        }

        public string Encode(int id)
        {
            return hashids.Encode(id);
        }
    }
}
