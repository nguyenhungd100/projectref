﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PCC1.PMS.Domain.Services.Common
{
    public interface ISendMailService
    {
        bool SendOTPToEmail(string email, string otp);
    }
}
