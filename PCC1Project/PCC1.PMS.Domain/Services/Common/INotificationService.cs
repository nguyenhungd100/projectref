﻿using System;
using System.Collections.Generic;
using System.Text;
using PCC1.PMS.Domain.Models.Notification;
using PCC1.PMS.Domain.Models.ProjectUsers;

namespace PCC1.PMS.Domain.Services.Common
{
    public interface INotificationService
    {
        bool AddNotification(List<int> userIds, object data);
    }
}
