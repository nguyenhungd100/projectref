﻿using PCC1.PMS.Domain.Repositories;
using PCC1.PMS.Framework.Utils;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Mail;
using System.Text;

namespace PCC1.PMS.Domain.Services.Common
{
    public class SendMailService : ISendMailService
    {
        private readonly string _emailAdress;
        private readonly string _emailPass;

        public SendMailService(string emailAdress, string emailPass)
        {
            _emailAdress = emailAdress;
            _emailPass = emailPass;
        }


        public bool SendOTPToEmail(string email, string otp)
        {
            var pass = EncryptUtil.Decrypt(_emailPass, true);
            var result = false;
            try
            {
                var strBuilder = new StringBuilder();

                strBuilder.Append("Mã xác nhận tài khoản "+email+" của bạn là: " + otp + " ( có hiệu lực trong vòng 5 phút)");

                MailMessage msg = new MailMessage(_emailAdress, email)
                {
                    Subject = "Mã xác nhận phần mềm PCC1",
                    Body = strBuilder.ToString(),
                    IsBodyHtml = true,
                };


                SmtpClient smt = new SmtpClient("smtp.gmail.com", 587)
                {
                    UseDefaultCredentials = true,
                    Credentials = new NetworkCredential(_emailAdress, pass),
                    EnableSsl = true
                };

                smt.Send(msg);

                result = true;
            }
            catch (Exception ex)
            {

                throw ex;
            }

            return result;
        }
    }
}
