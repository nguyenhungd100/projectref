﻿
using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using PCC1.PMS.Domain.Models.Notification;
using PCC1.PMS.Domain.Models.ProjectUsers;
using PCC1.PMS.Domain.Shared.FireBase;

namespace PCC1.PMS.Domain.Services.Common.Impelementation
{
   
    public class NotificationService : INotificationService
    {
        public NotificationService(string firebaseUrl)
        {
            this.firebaseUrl = firebaseUrl;
            this.fireBaseDB = new FireBaseDB(firebaseUrl);
        }
        readonly FireBaseDB fireBaseDB;
        private readonly string firebaseUrl;

        public bool AddNotification(List<int> userIds, object data)
        {
            var result = false;
            foreach(var userId in userIds)
            {
                FireBaseDB fireBaseDBInfoProject = fireBaseDB.NodePath("notifications/u-" + userId);
                FirebaseResponse addProjectUserResponse = fireBaseDBInfoProject.Post(JsonConvert.SerializeObject(data, Formatting.None,
                    new JsonSerializerSettings
                    {
                        ContractResolver = new CamelCasePropertyNamesContractResolver()
                    }));
                result = addProjectUserResponse._success;
            }
            
            return result;
        }

        //public bool DeleteNotification(List<int> userIds, object data)
        //{
        //    FireBaseDB fireBase = fireBaseDB.NodePath("notification/u-" + 73);
        //    fireBase.Get();
            
        //    var result = false;
        //    foreach (var userId in userIds)
        //    {
        //        //FireBaseDB fireBase = fireBaseDB.NodePath("notification/u-" + userId);                
        //        FirebaseResponse deleteUserReponse = fireBase.Delete();
        //        result = deleteUserReponse._success;
        //        if (!result) break;
        //    }
        //    return result;
        //}
    }
}
