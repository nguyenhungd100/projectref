﻿using MicroOrm.Dapper.Repositories;
using PCC1.PMS.Domain.Entity;
using PCC1.PMS.Domain.Enums;
using PCC1.PMS.Domain.Models.Notification;
using PCC1.PMS.Domain.Models.TrackingMoneyAdvances;
using PCC1.PMS.Domain.Repositories;
using PCC1.PMS.Domain.Services.Common;
using PCC1.PMS.Domain.Services.TrackingMoneyAdvances;
using PCC1.PMS.Framework.Utils;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace PCC1.PMS.Domain.Services.TrackingMoneyAdvances.Implementation
{
    public class TrackingMoneyAdvanceService : ITrackingMoneyAdvanceService
    {
        private readonly IProjectRepository _projectRepository;
        private readonly ITrackingMoneyAdvanceRepository _trackingMoneyAdvanceRepository;
        private readonly ICompanyRepository _companyRepository;
        private readonly IDapperRepository<ProjectCompany> _projectCompanyRepository;
        private readonly IDapperRepository<SendNotifyTrackingMoneyAdvance> _sendNotifyTrackingMoneyAdvanceRepository;
        private readonly IDapperRepository<User> _userRepository;
        private readonly IDapperRepository<ProjectUser> _projectUserRepository;
        private readonly INotificationService _notificationService;
        private readonly IHashidService _hashidService;

        public TrackingMoneyAdvanceService(IProjectRepository projectRepository,
            ITrackingMoneyAdvanceRepository trackingMoneyAdvanceRepository,
            ICompanyRepository companyRepository,
            IDapperRepository<ProjectCompany> projectCompanyRepository,
            IDapperRepository<SendNotifyTrackingMoneyAdvance> sendNotifyTrackingMoneyAdvanceRepository,
            IDapperRepository<User> userRepository,
            IDapperRepository<ProjectUser> projectUserRepository,
            INotificationService notificationService,
            IHashidService hashidService)
        {
            _projectRepository = projectRepository;
            _trackingMoneyAdvanceRepository = trackingMoneyAdvanceRepository;
            _companyRepository = companyRepository;
            _projectCompanyRepository = projectCompanyRepository;
            _sendNotifyTrackingMoneyAdvanceRepository = sendNotifyTrackingMoneyAdvanceRepository;
            _userRepository = userRepository;
            _projectUserRepository = projectUserRepository;
            _notificationService = notificationService;
            _hashidService = hashidService;
        }

        public SearchTrackingMoneyAdvanceResult SearhTrackingMoneyAdvance(SearchTrackingMoneyAdvanceModel search)
        {
            var result = new SearchTrackingMoneyAdvanceResult();
            IEnumerable<TrackingMoneyAdvance> trackings = new List<TrackingMoneyAdvance>();
            int totalRecords = 0;
            if (!string.IsNullOrEmpty(search.HashId))
                search.ProjectId = _hashidService.Decode(search.HashId);

            trackings = _trackingMoneyAdvanceRepository.Search(search, out totalRecords).ToList();

            trackings = trackings.OrderByDescending(c => c.CreatedDate).Skip(search.PageSize * (search.PageIndex - 1)).Take(search.PageSize);
            result.Records = trackings.ToList().CloneToListModels<TrackingMoneyAdvance, TrackingMoneyAdvanceModel>();

            var companyExists = new List<Company>();
            var projectExists = new List<Project>();
            if (totalRecords > 0)
            {
                var projectCheckIds = result.Records.Select(c => c.ProjectId).Distinct();
                var companyCheckIds = result.Records.Select(c => c.CompanyId).Distinct();
                projectExists = _projectRepository.FindAll(c => projectCheckIds.Contains(c.Id)).ToList();
                companyExists = _companyRepository.FindAll(c => companyCheckIds.Contains(c.Id)).ToList();
            }

            foreach (var r in result.Records)
            {
                r.HashId = _hashidService.Encode(r.ProjectId);
                r.CompanyName = companyExists.FirstOrDefault(c => c.Id == r.CompanyId).Name;
                r.ProjectName = projectExists.FirstOrDefault(c => c.Id == r.ProjectId).Name;
            }
            search.TotalRecord = totalRecords;
            result.TotalRecord = search.TotalRecord;
            result.PageIndex = search.PageIndex;
            result.PageSize = search.PageSize;
            result.PageCount = result.TotalRecord / result.PageSize + (result.TotalRecord % result.PageSize > 0 ? 1 : 0);
            return result;

        }

        public bool InsertTrackingMoneyAdvance(SaveTrackingMoneyAdvanceModel saveModel)
        {
            var result = false;
            var trans = _trackingMoneyAdvanceRepository.BeginTransaction();
            if (trans.Connection.State != ConnectionState.Open) trans.Connection.Open();
            try
            {
                if (!saveModel.ProjectId.HasValue) throw new ServiceException("Yêu cầu chọn dự án để tạm ứng");
                var company = _companyRepository.Find(c => c.Id == saveModel.CompanyId, trans);
                if (company == null) throw new Exception("Công ty không tồn tại");
                var project = _projectRepository.Find(c => c.Id == saveModel.ProjectId, trans);
                if (project == null) throw new Exception("Dự án không tồn tại");
                var projectCompany = _projectCompanyRepository.Find(c => c.CompanyId == saveModel.CompanyId && c.ProjectId == saveModel.ProjectId, trans);
                if (projectCompany == null) throw new Exception("Công ty không thuộc dự án");
                if (saveModel.MoneyAdvance.Value < 0) throw new ServiceException("SỐ tiền cần tạm ứng nhập vào không được nhỏ hơn 0");

                var trackingAdvance = new TrackingMoneyAdvance();
                trackingAdvance = _trackingMoneyAdvanceRepository.Find(c => c.ProjectId == saveModel.ProjectId.Value && c.CompanyId == saveModel.CompanyId, trans);

                if (trackingAdvance == null)
                {
                    var modelAdd = new TrackingMoneyAdvance
                    {
                        CompanyId = saveModel.CompanyId.Value,
                        ProjectId = saveModel.ProjectId.Value,
                        ContractSigningDate = saveModel.ContractSigningDate.Value.Date,
                        Duration = saveModel.Duration.Value,
                        ExpirationDate = saveModel.ContractSigningDate.Value.AddDays(saveModel.Duration.Value).Date.AddDays(2).AddTicks(-1),
                        MoneyAdvance = saveModel.MoneyAdvance.Value,
                        IsPaid = saveModel.IsPaid,
                        CreatedBy = saveModel.CreatedBy,
                        CreatedDate = DateTime.UtcNow
                    };

                    result = _trackingMoneyAdvanceRepository.Insert(modelAdd, trans);
                }
                else throw new ServiceException("Dự án " + project.Name + " cho công ty " + company.Name + " đã tồn tại");
                if (!result) trans.Rollback();
            }
            catch (Exception ex)
            {
                trans.Rollback();
                throw ex;
            }
            trans.Commit();
            return result;
        }

        public bool UpdateTrackingMoneyAdvance(SaveTrackingMoneyAdvanceModel saveModel)
        {
            var result = false;
            var trans = _trackingMoneyAdvanceRepository.BeginTransaction();
            if (trans.Connection.State != ConnectionState.Open) trans.Connection.Open();
            try
            {
                if (!saveModel.ProjectId.HasValue) throw new ServiceException("Yêu cầu chọn dự án để tạm ứng");
                var company = _companyRepository.Find(c => c.Id == saveModel.CompanyId, trans);
                if (company == null) throw new Exception("Công ty không tồn tại");
                var project = _projectRepository.Find(c => c.Id == saveModel.ProjectId, trans);
                if (project == null) throw new Exception("Dự án không tồn tại");
                var projectCompany = _projectCompanyRepository.Find(c => c.CompanyId == saveModel.CompanyId && c.ProjectId == saveModel.ProjectId, trans);
                if (projectCompany == null) throw new Exception("Công ty không thuộc dự án");
                if (saveModel.MoneyAdvance.Value < 0) throw new ServiceException("SỐ tiền cần tạm ứng nhập vào không được nhỏ hơn 0");

                var trackingAdvance = new TrackingMoneyAdvance();

                trackingAdvance = _trackingMoneyAdvanceRepository.Find(c => c.ProjectId == saveModel.ProjectId.Value && c.CompanyId == saveModel.CompanyId, trans);
                if (trackingAdvance != null)
                {
                    if (trackingAdvance == null) throw new ServiceException("Lần tạm ứng này không tồn tại");

                    trackingAdvance.ContractSigningDate = saveModel.ContractSigningDate.Value.Date;
                    trackingAdvance.Duration = saveModel.Duration.Value;
                    trackingAdvance.ExpirationDate = saveModel.ContractSigningDate.Value.AddDays(saveModel.Duration.Value).Date.AddDays(2).AddTicks(-1);
                    trackingAdvance.MoneyAdvance = saveModel.MoneyAdvance.Value;
                    trackingAdvance.IsPaid = saveModel.IsPaid;

                    result = _trackingMoneyAdvanceRepository.Update(trackingAdvance, trans);
                }

                if (!result) trans.Rollback();
            }
            catch (Exception ex)
            {
                trans.Rollback();
                throw ex;
            }
            trans.Commit();
            return result;
        }

        public bool DeleteTrackingMoneyAdvance(List<DeleteTrackingMoneyAdvanceModel> deleteModels)
        {
            var result = false;
            var trans = _trackingMoneyAdvanceRepository.BeginTransaction();
            if (trans.Connection.State != ConnectionState.Open) trans.Connection.Open();
            try
            {
                if (deleteModels.Count() > 0)
                {
                    foreach (var delete in deleteModels)
                    {
                        var trackingExist = _trackingMoneyAdvanceRepository.Find(c => c.CompanyId == delete.CompanyId && c.ProjectId == delete.ProjectId, trans);
                        if (trackingExist == null) throw new ServiceException("Lần tạm ứng không tồn tại");
                        result = _trackingMoneyAdvanceRepository.Delete(trackingExist, trans);
                        if (!result) break;
                    }
                    if (!result) trans.Rollback();
                }
            }
            catch (Exception ex)
            {
                trans.Rollback();
                throw ex;
            }
            trans.Commit();
            return result;
        }

        public bool SendNotifyTrackingAvanceMoney()
        {
            var result = false;
            var timeCurrent = DateTime.Now.Date;


            var trackingExists = _trackingMoneyAdvanceRepository.FindAll(c => c.ContractSigningDate < timeCurrent);
            if (trackingExists.Count() > 0)
            {
                var trackingExistFilters = trackingExists
                   .Where(c => c.ExpirationDate.Value.AddDays(-7) < timeCurrent && timeCurrent <= c.ExpirationDate.Value && c.IsPaid == false);
               

                if (trackingExistFilters.Count() > 0)
                {
                    var projectCheckIds = trackingExistFilters.Select(c => c.ProjectId).Distinct();
                    var projects = _projectRepository.FindAll(c => projectCheckIds.Contains(c.Id));

                    var companyCheckIds = trackingExistFilters.Select(c => c.CompanyId).Distinct();
                    var companies = _companyRepository.FindAll(c => companyCheckIds.Contains(c.Id));


                    var projectCompanies = _projectCompanyRepository.FindAll(c => projectCheckIds.Contains(c.ProjectId)
                        && companyCheckIds.Contains(c.CompanyId));


                    var users = _userRepository.FindAll(c => c.UserRoles.Contains(";2;"));
                    var userIds = users.Select(c => c.Id);


                    var projectUsers = _projectUserRepository.FindAll(c => projectCheckIds.Contains(c.ProjectId) && userIds.Contains(c.UserId));

                    var existSendNotifies = _sendNotifyTrackingMoneyAdvanceRepository.FindAll(c => projectCheckIds.Contains(c.ProjectId)
                        && companyCheckIds.Contains(c.CompanyId) && userIds.Contains(c.UserId));

                    var existSendNotifiesFilterDate = existSendNotifies.Where(c => c.SendNotifyDate.Date == timeCurrent.Date);
                    foreach (var trackingExistFilter in trackingExistFilters)
                    {

                        var projectUserManagers = projectUsers.Where(c => c.ProjectId == trackingExistFilter.ProjectId);
                        if (projectUserManagers.Count() > 0)
                        {
                            foreach (var projectUserManager in projectUserManagers)
                            {
                                var existSendingNotify = existSendNotifiesFilterDate.FirstOrDefault(c => c.CompanyId == trackingExistFilter.CompanyId
                                    && c.ProjectId == trackingExistFilter.ProjectId && c.UserId == projectUserManager.UserId);

                                if (existSendingNotify == null)
                                {
                                    result = _sendNotifyTrackingMoneyAdvanceRepository.Insert(new SendNotifyTrackingMoneyAdvance
                                    {
                                        ProjectId = trackingExistFilter.ProjectId,
                                        CompanyId = trackingExistFilter.CompanyId,
                                        UserId = projectUserManager.UserId,
                                        SendNotifyDate = DateTime.UtcNow
                                    });
                                    if (!result)
                                    {
                                        break;
                                    }
                                    else
                                    {
                                        var project = projects.FirstOrDefault(c => c.Id == trackingExistFilter.ProjectId);
                                        var company = companies.FirstOrDefault(c => c.Id == trackingExistFilter.CompanyId);
                                        result = _notificationService.AddNotification(new List<int> { projectUserManager.UserId }, new TrackingMoneyAdvanceEventModel
                                        {
                                            ProjectId = project.Id,
                                            ProjectName = project.Name,
                                            CompanyId = company.Id,
                                            CompanyName = company.Name,
                                            ProjectHashId = _hashidService.Encode(project.Id),
                                            Type = EventType.TrackingMoneyAdvanceToManager
                                        });
                                    }
                                }
                                else
                                {
                                    result = true;
                                }
                            }

                            if (!result) break;

                        }
                    }
                }
            }
            return result;
        }
    }
}
