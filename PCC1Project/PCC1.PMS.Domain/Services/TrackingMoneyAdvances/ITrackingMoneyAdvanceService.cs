﻿using MicroOrm.Dapper.Repositories;
using PCC1.PMS.Domain.Models.TrackingMoneyAdvances;
using System;
using System.Collections.Generic;
using System.Text;

namespace PCC1.PMS.Domain.Services.TrackingMoneyAdvances
{
    public interface ITrackingMoneyAdvanceService
    {
        SearchTrackingMoneyAdvanceResult SearhTrackingMoneyAdvance(SearchTrackingMoneyAdvanceModel search);

        bool DeleteTrackingMoneyAdvance(List<DeleteTrackingMoneyAdvanceModel> deleteModels);

        bool SendNotifyTrackingAvanceMoney();

        bool InsertTrackingMoneyAdvance(SaveTrackingMoneyAdvanceModel saveModel);

        bool UpdateTrackingMoneyAdvance(SaveTrackingMoneyAdvanceModel saveModel);
    }
}
