﻿using MicroOrm.Dapper.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using PCC1.PMS.Domain.Entity;
using PCC1.PMS.Domain.Models;
using PCC1.PMS.Domain.Models.Users;
using PCC1.PMS.Domain.Repositories;
using PCC1.PMS.Framework.Caching;
using PCC1.PMS.Framework.Utils;
using PCC1.PMS.Framework.Extensions;
using System.IO;
using PCC1.PMS.Domain.Models.MediaStorage;
using PCC1.PMS.Domain.Enums;
using System.Text;
using System.Security.Cryptography;
using System.Net.Mail;
using System.Net;
using PCC1.PMS.Domain.Services.Common;
using Microsoft.Extensions.Caching.Memory;

namespace PCC1.PMS.Domain.Services.Users.Implementation
{
    public class UserService : IUserService
    {
        private readonly IMemoryCache _cache;
        private readonly IUserRepository _userRepository;
        private readonly IDapperRepository<DeviceToken> _deviceTokenRepository;
        private readonly ISendMailService _sendMailService;
        private readonly IRoleRepository _roleRepository;
        private readonly IHashidService _hashidService;
        private readonly IDapperRepository<Department> _departmentRepository;
        private readonly IDapperRepository<Company> _companyRepository;

        public UserService(IMemoryCache cache,
        IUserRepository userRepository,
            IDapperRepository<DeviceToken> deviceTokenRepository,
            ISendMailService sendMailService,
            IRoleRepository roleRepository,
            IHashidService hashidService,
            IDapperRepository<Department> departmentRepository,
            IDapperRepository<Company> companyRepository
            )
        {
            _cache = cache;
            _userRepository = userRepository;
            _deviceTokenRepository = deviceTokenRepository;
            _sendMailService = sendMailService;
            _roleRepository = roleRepository;
            _hashidService = hashidService;
            _departmentRepository = departmentRepository;
            _companyRepository = companyRepository;
        }
        public UserModel Add(UserModel user)
        {
            var entity = user.CloneToModel<UserModel, User>();
            var res = _userRepository.Insert(entity);
            var model = entity.CloneToModel<User, UserModel>();
            return model;
        }
        public bool Delete(long[] Ids)
        {
            return _userRepository.Delete(c => Ids.Contains(c.Id));
        }
        public UserModel GetById(long userId)
        {
            var result = new UserModel();
            var entity = _userRepository.FindById(userId);
            var roleAll = entity.UserRoles.Split(new string[] { ";" }, StringSplitOptions.RemoveEmptyEntries)
                    .Select(c => Convert.ToInt32(c)).ToList();
            var roleMin = 100000;
            if (roleAll.Count() > 0)
                roleMin = roleAll.Min();
            var role = _roleRepository.FindById(roleMin);

            result = entity.CloneToModel<User, UserModel>();
            if (role != null)
            {
                result.DisplayOrder = role.DisplayOrder ?? 100000;
            }

            return result;

        }
        public async Task<UserModel> GetByIdAsync(long userId)
        {
            var entity = await _userRepository.FindByIdAsync(userId);
            return entity.CloneToModel<User, UserModel>();
        }
        public async Task<UserModel> SingleAsync(Expression<Func<User, bool>> exp)
        {
            var entity = await _userRepository.FindAsync(exp);
            return entity.CloneToModel<User, UserModel>();
        }
        public UserModel Single(Expression<Func<User, bool>> exp)
        {
            var entity = _userRepository.Find(exp);
            return entity.CloneToModel<User, UserModel>();
        }
        public bool Update(UserModel user)
        {
            var entity = user.CloneToModel<UserModel, User>();
            return _userRepository.Update(entity);
        }
        public bool VerifyPassword(string userName, string encryptedPassword, ref UserModel userModel)
        {
            var user = _userRepository.Find(u => u.Email == userName || u.Mobile == userName);
            if (user.Password == encryptedPassword)
            {
                userModel = user.CloneToModel<User, UserModel>();
                return true;
            }
            userModel = null;
            return false;
        }
        public BaseSearchResult<UserModel> Search(UserSearchModel userSearchModel)
        {
            int totalRecord = 0;
            //Expression<Func<User, bool>> filter = t => t.Id > 0;
            //if (!string.IsNullOrEmpty(userSearchModel.UserName))
            //{
            //    filter = filter.And(t => t.Email.Contains(userSearchModel.UserName));


            //}
            var entity = _userRepository.FindUserName(userSearchModel.UserName);
            //var entries = _userRepository.FilterAll(filter, p=>p.Id, true, 100, 1, ref totalRecord);


            var departments = _departmentRepository.FindAll();



            var records = DomainMaps.Mapper.Map<List<UserModel>>(entity.ToList());


            if (userSearchModel.DepartmentId.HasValue)
            {
                records = records.Where(c => c.DepartmentId == userSearchModel.DepartmentId.Value).ToList();
            }

            if (!string.IsNullOrEmpty(userSearchModel.Role) || userSearchModel.Role == ";")
            {
                userSearchModel.Role = userSearchModel.Role.Replace(";", "");
                if (records.Count() > 0)
                {
                    var test = records.Select(c => c.UserRoles);

                    records = records.Where(c => !string.IsNullOrEmpty(c.UserRoles)).Where(a => a.UserRoles.Contains(";" + userSearchModel.Role + ";")).ToList();             
                }

            }
            var userCurrent = _userRepository.FindById(userSearchModel.UserId);
            if (userCurrent.CompanyId.HasValue)
            {
                records = records.Where(c=>c.CompanyId == userCurrent.CompanyId.Value).ToList();
            }
            else
            {
                if (userSearchModel.CompanyId.HasValue)
                {
                    records = records.Where(c => c.CompanyId == userSearchModel.CompanyId.Value).ToList();
                }               
            }

            var recordPaging = records.OrderByDescending(c => c.Id).Skip(userSearchModel.PageSize * (userSearchModel.PageIndex - 1)).Take(userSearchModel.PageSize);

            var companies = _companyRepository.FindAll();
            foreach (var item in recordPaging)
            {
                if (item.DepartmentId.HasValue)
                {
                    if (departments.Count() > 0)
                    {
                        if (departments.Select(c => c.Id).Contains(item.DepartmentId.Value))
                            item.DepartmentName = departments.FirstOrDefault(c => c.Id == item.DepartmentId.Value).Name;
                    }

                }

                if (item.CompanyId.HasValue)
                {
                    var company = companies.FirstOrDefault(c => c.Id == item.CompanyId.Value);
                    if (company!=null)
                    {
                        item.CompanyName = company.Name;
                    }                  
                }
            }
            return new BaseSearchResult<UserModel>()
            {
                TotalRecord = records.Count(),
                Records = recordPaging,
                PageIndex = userSearchModel.PageIndex,
                PageSize = userSearchModel.PageSize,
                PageCount = records.Count() / userSearchModel.PageSize + (records.Count() % userSearchModel.PageSize > 0 ? 1 : 0)
            };


        }
        public long CreateUser(CreateUserModel createUserModel)
        {
            if (_userRepository.Find(u => u.Mobile == createUserModel.Phone) != null)
            {
                throw new ServiceException("Số điện thoại đã tồn tại");
            }

            if (_userRepository.Find(u => u.Email == createUserModel.Email) != null)
            {
                throw new ServiceException("Email đã tồn tại");
            }

            var user = new User
            {
                FullName = createUserModel.FullName,
                Password = EncryptUtil.EncryptMD5(createUserModel.Password),
                Mobile = createUserModel.Phone,
                Email = createUserModel.Email,
                Status = Enums.UserStatus.Actived,
                UserRoles = createUserModel.Roles,
                Alias = createUserModel.Alias
            };

            if (createUserModel.CompanyId.HasValue)
            {
                if(createUserModel.CompanyId.Value !=0)
                    user.CompanyId = createUserModel.CompanyId.Value;
            }

            if (createUserModel.DepartmentId.HasValue) user.DepartmentId = createUserModel.DepartmentId.Value;
            if (!_userRepository.Insert(user)) throw new ServiceException("Cann't create account");
            //GenerateAndSendOTP(user.Id, user.Mobile);
            return user.Id;
        }

        private void GenerateAndSendOTP(long userId, string phone)
        {
            // generate activation code
            Random generator = new Random();
            String code = generator.Next(111111, 999999).ToString("D4");
        }
        public bool ActiveAccount(long userId, string code)
        {
            //if (code == "112233") { }
            //else
            //{
            // var cachedCode = _cacheManager.StringGet("otp:user-" + userId);
            //if (string.IsNullOrEmpty(cachedCode)) return false;
            //if (code != cachedCode) return false;
            //}

            var user = _userRepository.FindById(userId);
            user.Status = Enums.UserStatus.Actived;
            return _userRepository.Update(user);
        }
        public void ResendOTP(string phone)
        {
            var user = _userRepository.Find(u => u.Mobile == phone);
            if (user == null) throw new ServiceException("Phone number not existed");
            if (user.Status != Enums.UserStatus.NotActived) throw new ServiceException("Can't resend OTP to this account");

            GenerateAndSendOTP(user.Id, user.Mobile);

        }
        public bool UpdateLocation(long userId, LocationModel locationModel)
        {
            //var user = _userRepository.FindById(userId);

            //// update cache
            //_cacheManager.GeoAdd(CACHEKEYS.ALL_USER_LOCATIONS, locationModel.Lat, locationModel.Lng, userId);
            ////return _userRepository.Update(user);
            return true;
        }

        public bool UpdateToken(long userId, UpdateTokenModel tokenModel)
        {
            var deviceToken = _deviceTokenRepository.Find(t => t.UserId == userId);
            if (deviceToken == null)
            {
                return _deviceTokenRepository.Insert(new DeviceToken
                {
                    DeviceId = tokenModel.DeviceId,
                    IsExpoToken = tokenModel.IsExpo,
                    OS = tokenModel.OS,
                    Token = tokenModel.Token,
                    UserId = userId,
                    LastUpdateUtc = DateTime.UtcNow
                });
            }
            else
            {
                deviceToken.OS = tokenModel.OS;
                deviceToken.DeviceId = tokenModel.DeviceId;
                deviceToken.Token = tokenModel.Token;
                deviceToken.LastUpdateUtc = DateTime.UtcNow;
                return _deviceTokenRepository.Update(deviceToken);
            }
        }

        public List<UserModel> GetByIds(List<long> ids, int selectTop = 0)
        {
            if (ids == null || ids.Count == 0) return new List<UserModel>();
            var users = _userRepository.FindAll(u => ids.Contains(u.Id));
            return DomainMaps.Mapper.Map<List<UserModel>>(users);
        }

        public UserModel GetByUserName(string userName)
        {
            var user = _userRepository.Find(u => u.Email == userName);
            if (user == null) throw new ServiceException("User not exist");
            return DomainMaps.Mapper.Map<UserModel>(user);
        }

        public UserModel GetByPhone(string userName)
        {
            var user = _userRepository.Find(u => u.Mobile == userName);
            if (user == null) throw new ServiceException("User not exist");
            return DomainMaps.Mapper.Map<UserModel>(user);
        }

        public bool UpdateProfile(UpdateUserProfileModel userProfileModel)
        {
            var user = _userRepository.FindById(userProfileModel.UserId);
            user.FullName = userProfileModel.FullName;
            user.Email = userProfileModel.Email;
            user.Mobile = userProfileModel.Mobile;
            return _userRepository.Update(user);
        }

        public bool ChangePassword(ChangePasswordModel changePasswordModel)
        {
            var user = _userRepository.FindById(changePasswordModel.UserId);
            if (user == null) throw new ServiceException("User not exist");
            user.Password = EncryptUtil.EncryptMD5(changePasswordModel.NewPassword);
            return _userRepository.Update(user);
        }

        public async Task<bool> UpdateRole(int userId, string userRoles)
        {
            var result = false;
            if (userId <= 0)
            {
                throw new ServiceException("Hãy chọn user");
            }
            var user = await _userRepository.FindByIdAsync(userId);
            if (user == null)
            {
                throw new ServiceException("Không tồn tại user này, kiểm tra lại");
            }
            if (!String.IsNullOrEmpty(userRoles))
            {
                user.UserRoles = userRoles;
                result = await _userRepository.UpdateAsync(user);
            }
            return result;
        }

        public bool RemoveUser(UserRemoveModel userRemoveModel)
        {
            var user = _userRepository.FindById(userRemoveModel.Id);
            if (user != null)
            {
                user.Status = Enums.UserStatus.Disabled;
                return _userRepository.Update(user);
            }
            else throw new ServiceException("Người dùng không tồn tại");
        }

        public UserModel UpdateUser(UpdateUserModel updateUserModel)
        {
            var user = _userRepository.FindById(updateUserModel.UserId);
            if (user == null)
            {
                throw new ServiceException("Không tồn tại user này, kiểm tra lại");
            }
            user.FullName = updateUserModel.FullName;
            user.Email = updateUserModel.Email;
            user.Mobile = updateUserModel.Phone;

            if (!String.IsNullOrEmpty(updateUserModel.Roles))
            {
                user.UserRoles = updateUserModel.Roles;
            }

            if (updateUserModel.DepartmentId.HasValue)
            {
                user.DepartmentId = updateUserModel.DepartmentId.Value;
            }

            if (updateUserModel.DepartmentId.HasValue)
            {
                user.CompanyId = updateUserModel.CompanyId.Value;
            }

            if (!string.IsNullOrEmpty(updateUserModel.Alias))
            {
                user.Alias = updateUserModel.Alias;
            }

            user.Status = (updateUserModel.IsActived) ? UserStatus.Actived : UserStatus.NotActived;
            _userRepository.Update(user);
            return DomainMaps.Mapper.Map<UserModel>(user);
        }

        public string DecryptPassowrd(object obj)
        {
            string password = obj.ToString();

            System.Text.UTF8Encoding encoder = new System.Text.UTF8Encoding();

            System.Text.Decoder utf8Decode = encoder.GetDecoder();

            byte[] todecode_byte = Convert.FromBase64String(password.Replace("", "+"));

            int charCount = utf8Decode.GetCharCount(todecode_byte, 0, todecode_byte.Length);

            char[] decoded_char = new char[charCount];

            utf8Decode.GetChars(todecode_byte, 0, todecode_byte.Length, decoded_char, 0);

            string result = new String(decoded_char);

            return result;

        }

        public bool SendOtpToEmail(ForgotPasswordModel model)
        {
            var result = false;
            if (string.IsNullOrEmpty(model.Email)) throw new ServiceException("Yêu cầu nhập địa chỉ email để lấy lại mật khẩu");

            var user = _userRepository.Find(c => c.Email == model.Email || c.Mobile == model.Email);
            if (user == null) throw new ServiceException("Tài khoản không tồn tại");

            Random generator = new Random();
            String code = generator.Next(111111, 999999).ToString("D4");

            var statusSend = _sendMailService.SendOTPToEmail(user.Email, code);
            if (statusSend)
            {
                var cacheEntryOptions = new MemoryCacheEntryOptions()
                .SetSlidingExpiration(TimeSpan.FromMinutes(5));
                var otp = _cache.Set(code, user.Id.ToString(), cacheEntryOptions);
                result = true;
            }

            return result;
        }

        public bool VerifyOTPPassword(VerifyOTPPasswordModel verifyOTP)
        {
            var result = false;
            if (string.IsNullOrEmpty(verifyOTP.OTP) || string.IsNullOrEmpty(verifyOTP.Email) || string.IsNullOrEmpty(verifyOTP.NewPassword))
                throw new ServiceException("Yêu cầu nhập đầy đủ email, mã xác nhận kèm mật khẩu thay đổi");

            var user = _userRepository.Find(c => c.Email == verifyOTP.Email);
            if (user == null) throw new ServiceException("Tài khoản không tồn tại");

            var userId = _cache.Get(verifyOTP.OTP);
            if (userId == null || userId.ToString() != user.Id.ToString())
                throw new ServiceException("Mã xác nhận không tồn tại hoặc đã quá hạn 5 phút");
            else
            {
                user.Password = EncryptUtil.EncryptMD5(verifyOTP.NewPassword);
                result = _userRepository.Update(user);
            }
            return result;
        }

        public IEnumerable<DepartmentModel> GetDepartments()
        {
            var departments = _departmentRepository.FindAll();
            foreach (var department in departments)
            {
                yield return department.CloneToModel<Department, DepartmentModel>();
            }
        }
    }
}
