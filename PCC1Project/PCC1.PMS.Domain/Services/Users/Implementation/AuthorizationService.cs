﻿using System;
using System.Linq.Expressions;
using System.Threading.Tasks;
using PCC1.PMS.Domain.Entity;
using PCC1.PMS.Domain.Models.Users;
using PCC1.PMS.Domain.Repositories;
using PCC1.PMS.Framework.Utils;

namespace PCC1.PMS.Domain.Services.Users.Implementation
{
    public class AuthorizationService : IAuthorizationService
    {
        private readonly IUserRepository _userRepository;

        public AuthorizationService(
            IUserRepository userRepository)
        {
            _userRepository = userRepository;
        }
        public UserModel GetById(long userId)
        {
            var entity = _userRepository.FindById(userId);
            return entity.CloneToModel<User, UserModel>();
        }
        public async Task<UserModel> GetByIdAsync(long userId)
        {
            var entity = await _userRepository.FindByIdAsync(userId);
            return entity.CloneToModel<User, UserModel>();
        }
        public async Task<UserModel> SingleAsync(Expression<Func<User, bool>> exp)
        {
            var entity = await _userRepository.FindAsync(exp);
            return entity.CloneToModel<User, UserModel>();
        }
        public UserModel Single(Expression<Func<User, bool>> exp)
        {
            var entity = _userRepository.Find(exp);
            return entity.CloneToModel<User, UserModel>();
        }
        public bool VerifyPassword(string userName, string encryptedPassword, ref UserModel userModel)
        {
            var user = _userRepository.Find(u => u.Email == userName || u.Mobile == userName);
            if (user == null || user.Status != Enums.UserStatus.Actived) return false;

            if (user.Password == encryptedPassword)
            {
                userModel = user.CloneToModel<User, UserModel>();
                return true;
            }
            userModel = null;

            return false;
        }
    }
}
