﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;
using PCC1.PMS.Domain.Entity;
using PCC1.PMS.Domain.Models;
using PCC1.PMS.Domain.Models.Users;

namespace PCC1.PMS.Domain.Services.Users
{
    public interface IUserService
    {
        UserModel GetById(long userId);

        Task<UserModel> GetByIdAsync(long userId);

        Task<UserModel> SingleAsync(Expression<Func<User, bool>> exp);

        UserModel Single(Expression<Func<User, bool>> exp);

        UserModel Add(UserModel user);

        bool Update(UserModel user);

        bool Delete(long[] Ids);
        bool VerifyPassword(string userName, string encryptedPassword, ref UserModel userModel);
        BaseSearchResult<UserModel> Search(UserSearchModel userSearchModel);
        long CreateUser(CreateUserModel createUserModel);
        bool ActiveAccount(long userId, string code);
        bool UpdateLocation(long userId, LocationModel locationModel);
        bool UpdateToken(long userId, UpdateTokenModel tokenModel);
        List<UserModel> GetByIds(List<long> collectingUserIds, int selectTop = 0);
        UserModel GetByUserName(string userName);
        void ResendOTP(string phone);
        UserModel GetByPhone(string userName);
        bool ChangePassword(ChangePasswordModel changePasswordModel);
        bool UpdateProfile(UpdateUserProfileModel userProfileModel);
        Task<bool> UpdateRole(int userId, string userRoles);
        bool RemoveUser(UserRemoveModel userRemoveModel);
        UserModel UpdateUser(UpdateUserModel updateUserModel);

        bool SendOtpToEmail(ForgotPasswordModel model);


        bool VerifyOTPPassword(VerifyOTPPasswordModel verifyOTP);

        IEnumerable<DepartmentModel> GetDepartments();
    }
}
