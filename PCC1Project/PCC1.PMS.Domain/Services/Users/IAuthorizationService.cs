﻿using System;
using System.Linq.Expressions;
using System.Threading.Tasks;
using PCC1.PMS.Domain.Entity;
using PCC1.PMS.Domain.Models.Users;

namespace PCC1.PMS.Domain.Services.Users
{
    public interface IAuthorizationService
    {
        UserModel GetById(long userId);

        Task<UserModel> GetByIdAsync(long userId);

        Task<UserModel> SingleAsync(Expression<Func<User, bool>> exp);

        UserModel Single(Expression<Func<User, bool>> exp);

        bool VerifyPassword(string userName, string encryptedPassword, ref UserModel userModel);
    }
}
