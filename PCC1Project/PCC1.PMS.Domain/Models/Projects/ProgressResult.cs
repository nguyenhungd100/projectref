﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PCC1.PMS.Domain.Models.Projects
{
    public class ProgressResult
    {
        public int PercentageProgress { get; set; }

        public ProgressStatus? Status { get; set; }

        public int PercentageDiff { get; set; }
    }
}
