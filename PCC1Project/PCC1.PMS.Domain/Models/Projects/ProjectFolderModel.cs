﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace PCC1.PMS.Domain.Models.Projects
{
    public class ProjectFolderModel
    {
        public Guid Id { get; set; }
        [JsonIgnore]
        public int ProjectId { set; get; }
        public Guid? ParentId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }

        public DateTime CreatedDate { get; set; }
    }
}
