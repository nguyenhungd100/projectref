﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace PCC1.PMS.Domain.Models.Projects
{
    public class UpdateCompanyInProject
    {
        [JsonIgnore]
        public int ProjectId { get; set; }

        public string HashId { get; set; }

        public List<int?> CompanyIds { get; set; }
    }
}
