﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace PCC1.PMS.Domain.Models.Projects
{
    public class AllowShowOnMobileModel
    {
        [Required(ErrorMessage ="Cần nhập vào mã plan")]
        public Guid PlanId { get; set; }

        public bool Status { get; set; }
    }
}
