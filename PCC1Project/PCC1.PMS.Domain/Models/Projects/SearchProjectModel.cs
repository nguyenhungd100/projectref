﻿using PCC1.PMS.Domain.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace PCC1.PMS.Domain.Models.Projects
{
    public class SearchProjectModel : BaseSearchModel
    {
        public string Name { get; set; }

        public ContractType? Type { set; get; }

        public string Invester { set; get; }

        public string SupervisingConsultant { set; get; }

        public string DesignConsultant { set; get; }

        public string Subcontractor { set; get; }

        public ConstructionLevelProject? ConstructionLevel { set; get; }

        public string Scale { set; get; }

        public string Location { set; get; }

        public int? ProjectGroup { set; get; }

        public DateTime? DateStart { set; get; }

        public DateTime? DateEnd { set; get; }

        public ProjectStatus? Status { set; get; }

        public ProgressStatus? ProgressStatus { set; get; }

        public int? UserId { get; set; }

        public int TotalRecord { get; set; }
    }
}
