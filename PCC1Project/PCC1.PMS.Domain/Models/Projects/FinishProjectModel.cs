﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace PCC1.PMS.Domain.Models.Projects
{
    public class FinishProjectModel
    {
        [JsonIgnore]
        public int ProjectId { get; set; }

        [JsonIgnore]
        public int UserId { get; set; }
    }
}
