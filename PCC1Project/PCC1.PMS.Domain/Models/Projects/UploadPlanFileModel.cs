﻿using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;
using PCC1.PMS.Domain.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace PCC1.PMS.Domain.Models.Projects
{
    public class UploadPlanFileModel
    {
        [Required(ErrorMessage ="File mpp này không được để trống")]
        public IFormFile MPPFile { set; get; }
    }
}
