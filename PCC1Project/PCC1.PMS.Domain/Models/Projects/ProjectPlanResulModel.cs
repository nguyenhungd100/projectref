﻿using PCC1.PMS.Domain.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace PCC1.PMS.Domain.Models.Projects
{
    public class ProjectPlanResulModel
    {
        public Guid Id { get; set; }

        public int No { set; get; }

        public Guid? ParentId { set; get; }

        public string text { set; get; }

        public int duration { set; get; }

        public DateTime start_date { set; get; }

        public DateTime end_date { set; get; }

        public StatusPlan? StatusCurrent { set; get; }

        public bool? IsLeaf { set; get; }

        public ProjectPlanStatus Status { set; get; }

        public int? progress { get; set; }

        public bool? DisplayOnMobile { get; set; }

        public int Order { get; set; }
    }
}
