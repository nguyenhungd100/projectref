﻿using Newtonsoft.Json;
using PCC1.PMS.Domain.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace PCC1.PMS.Domain.Models.Projects
{
    public class SearchProjectReportsModel : BaseSearchModel
    {
        public ProjectReportType? ReportType { get; set; }
        public string ProjectHashId { get; set; }
        [JsonIgnore]
        public int? ProjectId { get; set; }
        public DateTime? FromDate { set; get; }
        public DateTime? ToDate { set; get; }
        public int? CreatedBy { set; get; }
        public string Email { set; get;}
        public ProjectReportStatus? Status { get; set; }
        public string Text { get; set; }
        [JsonIgnore]
        public int TotalRecord { get; set; }
    }
}
