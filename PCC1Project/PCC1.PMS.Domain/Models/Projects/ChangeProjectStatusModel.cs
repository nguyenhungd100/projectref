﻿using Newtonsoft.Json;
using PCC1.PMS.Domain.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace PCC1.PMS.Domain.Models.Projects
{
    public class ChangeProjectStatusModel
    {
        [JsonIgnore]
        public int ProjectId { get; set; }

        public string HashId { get; set; }

        public ProjectStatus Status { get; set; }
    }
}
