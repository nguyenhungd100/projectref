﻿using Newtonsoft.Json;
using PCC1.PMS.Domain.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace PCC1.PMS.Domain.Models.Projects
{
    public class ProjectPlanModel
    {
        [Required]
        public Guid Id { get; set; }

        [Required]
        public int ProjectId { set; get; }

        public int No { set; get; }

        public Guid? ParentId { set; get; }

        [Required]
        public string Title { set; get; }

        [Required]
        public int Duration { set; get; }

        [Required]
        public DateTime StartDate { set; get; }

        [Required]
        public DateTime EndDate { set; get; }

        public StatusPlan? StatusCurrent { set; get; }

        public bool? IsLeaf { set; get; }

        [Required]
        public ProjectPlanStatus Status { set; get; }

        public int? CompletedPercentage { get; set; }

        public bool? DisplayOnMobile { get; set; }

        public int Order { get; set; }

    }
}
