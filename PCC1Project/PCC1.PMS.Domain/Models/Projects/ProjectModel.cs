﻿using Newtonsoft.Json;
using PCC1.PMS.Domain.Enums;
using PCC1.PMS.Domain.Models.Companies;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace PCC1.PMS.Domain.Models.Projects
{
    public class ProjectModel
    {
        [JsonIgnore]
        public int Id { set; get; }

        [MinLength(10)]
        public string Name { get; set; }
        
        public ContractType Type { set; get; }

        public string Investor { set; get; }

        public string SupervisingConsultant { set; get; }

        public string DesignConsultant { set; get; }

        public string Subcontractor { set; get; }

        public ConstructionLevelProject? ConstructionLevel { set; get; }

        public int ConstructionType;

        public decimal? CapitalSource { set; get; }

        public int? ProjectGroup { set; get; }

        public string Scale { set; get; }

        public DateTime? DateStart { set; get; }

        public DateTime? DateEnd { set; get; }

        public decimal? TotalContractValue { set; get; }

        public string Location { set; get; }

        public ProjectStatus Status { set; get; }

        public int? LastPlanNo { set; get; }
        public string HashId { get; set; }

        public List<CompanyModel> Companies { get; set; }
      
    }

    public class ApproveProjectModel
    {
        [JsonIgnore]
        [Required(ErrorMessage ="Dự án cần phê duyệt không được để trống")]
        public int Id { set; get; }

        [JsonIgnore]
        public int ApproveUser { get; set; }
    }
}
