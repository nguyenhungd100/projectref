﻿using PCC1.PMS.Domain.Entity;
using PCC1.PMS.Domain.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace PCC1.PMS.Domain.Models.Projects
{
    public class ProjectReportModel : BaseEntity
    {
        public ProjectReportType ReportType { set; get; }

        public int ProjectId { set; get; }

        public int CreatedBy { set; get; }

        public DateTime CreatedOnUtc { set; get; }

        public ProjectReportStatus Status { set; get; }

        public string MorningWeather { get; set; }

        public string AfternoonWeather { get; set; }

        public string ManpowerInfo { get; set; }

        public string MachineInfo { get; set; }

        public string Note { set; get; }

        public IEnumerable<ProjectReportPlan> ProjectReportPlans { get; set; } = new List<ProjectReportPlan>();
        public IEnumerable<ProjectReportWorkitem> ProjectReportWorkitems { get; set; } = new List<ProjectReportWorkitem>();

    }
}
