﻿using Newtonsoft.Json;
using PCC1.PMS.Domain.Entity;
using PCC1.PMS.Domain.Enums;
using PCC1.PMS.Domain.Models.ProjectReports;
using PCC1.PMS.Domain.Models.ProjectReports.DefineMachineryManpower;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace PCC1.PMS.Domain.Models.Projects
{
    public class SaveReportModel
    {
        [JsonIgnore]
        public int UserId { get; set; }

        public int? ProjectReportId { get; set; }
        [Required]
        public ProjectReportType ReportType { set; get; }

        public string HashId { get; set; }
        [JsonIgnore]
        public int ProjectId { set; get; }

        public string MorningWeather { get; set; }

        public string AfternoonWeather { get; set; }

        public string MachineInfo { get; set; }

        public string ManpowerInfo { get; set; }

        public string Note { set; get; }

        public IEnumerable<SaveProjectReportPlanModel> ReportPlans { get; set; } = new List<SaveProjectReportPlanModel>();

        public IEnumerable<SaveWorkitemReportModel> ReportWorkitems { get; set; } = new List<SaveWorkitemReportModel>();

        public IEnumerable<MachineryManpowerDefinedModel> MachineryManpowerDefineds { get; set; } = new List<MachineryManpowerDefinedModel>();

    }

    public class UpdateReportModel
    {
        [Required]
        public int ProjectReportId { get; set; }
        public ProjectReportType ReportType { set; get; }
        public string Note { set; get; }
        public string Images { set; get; }
    }
    public class DeleteReportModel
    {
        [Required]
        public int ProjectReportId { get; set; }
    }
    public class ApproveReportModel
    {
        [Required]
        public int ProjectReportId { get; set; }
        [JsonIgnore]
        public int UserId { get; set; }
    }
    public class RejectReportModel
    {
        [Required]
        public int ProjectReportId { get; set; }
        [JsonIgnore]
        public int UserId { get; set; }
        [Required]
        public string Reason { get; set; }
    }

    public class SendToSiteManagerModel
    {
        [Required(ErrorMessage ="Mã báo cáo không được để trống")]
        public int ProjectReportId;
    }
}
