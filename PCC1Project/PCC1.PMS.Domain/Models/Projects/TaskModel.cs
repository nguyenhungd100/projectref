﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PCC1.PMS.Domain.Models.Projects
{
    public class PlanParsingModel
    {
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public List<TaskModel> Tasks { get; set; }
    }
    public class TaskModel
    {
        public Guid Id { get; set; }
        public Guid? ParentId { set; get; }
        public string Title { get; set; }
        public string Duration { get; set; }
        public DateTime DateStart { get; set; }
        public DateTime DateEnd { get; set; }
        public bool? IsLeaf { set; get; }
        public int OrderNumber { get; set; }
    }
}
