﻿using PCC1.PMS.Domain.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace PCC1.PMS.Domain.Models.Projects
{
    public class ProjectOverallModel
    {
        [Required(ErrorMessage = "Mã dự án không được để trống")]
        public int Id { set; get; }

        [Required(ErrorMessage = "Ngày bắt đầu dự án không được để trống")]
        public DateTime DateStart { set; get; }

        [Required(ErrorMessage = "Ngày kết thúc dự án không được để trống")]
        public DateTime DateEnd { set; get; }

        [Required(ErrorMessage = "Tổng giá trị hợp đồng không được để trống")]
        public decimal TotalContractValue { set; get; }

        [Required(ErrorMessage = "Vị trí không được để trống")]
        public string Location { set; get; }

    }
}
