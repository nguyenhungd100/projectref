﻿using Newtonsoft.Json;
using PCC1.PMS.Domain.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace PCC1.PMS.Domain.Models
{
    public class UpdateProjectModel
    {
        //[Required(ErrorMessage = "Tên dự án không được để trống")]
        [MinLength(10, ErrorMessage ="Tên dự án ít nhất 10 ký tự")]
        public string Name { get; set; }

        //[Required(ErrorMessage = "Loại dự án không được để trống")]
        //[EnumDataType(typeof(TypeProject), ErrorMessage = "Đầu vào loại dự án không thuộc các loại dự án đã định nghĩa")]
        public ContractType Type { set; get; }

        public int? Company { set; get; }

        [MaxLength(256, ErrorMessage = "Tên chủ đầu tư không được quá 256 ký tự")]
        public string Investor { set; get; }

        [MaxLength(256, ErrorMessage = "Tên đơn vị giám sát không được quá 256 ký tự")]
        public string SupervisingConsultant { set; get; }

        [MaxLength(256, ErrorMessage = "Tên đơn vị thiết kế không được quá 256 ký tự")]
        public string DesignConsultant { set; get; }

        [MaxLength(256, ErrorMessage = "Tên nhà thầu phụ không được quá 256 ký tự")]
        public string Subcontractor { set; get; }

        [EnumDataType(typeof(ConstructionLevelProject), ErrorMessage = "Đầu vào cấp công trình không thuộc cấp công trình đã định nghĩa")]
        public ConstructionLevelProject? ConstructionLevel { set; get; }

        public int? ConstructionType;

        public decimal? CapitalSource { set; get; }

        public int? ProjectGroup { set; get; }

        public string Scale { set; get; }
        public DateTime? DateStart { get; set; }
        public DateTime? DateEnd { get; set; }
        public string Location { get; set; }
        public decimal? TotalContractValue { get; set; }
        [JsonIgnore]
        public int ProjectId { get; set; }
        
    }
}
