﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PCC1.PMS.Domain.Models.Projects
{
    public class ProjectInfoMiniModel
    {
        public string HashId { get; set; }

        public string ProjectName { get; set; }
    }
}
