﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PCC1.PMS.Domain.Models.Projects
{
    public class NumberProjectProgressResult
    {
        public int BeyondProgress { get; set; }

        public int Slow { get; set; }

        public int OnSchedule { get; set; }

        public int NotPlan { set; get; }

        public int NotImplemented { get; set; }
    }
}
