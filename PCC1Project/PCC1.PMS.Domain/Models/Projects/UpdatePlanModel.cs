﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace PCC1.PMS.Domain.Models.Projects
{
    public class UpdatePlanModel
    {
        [Required(ErrorMessage ="Yêu cầu chọn tiến độ")]
        public Guid Id { get; set; }

        [JsonIgnore]
        public int ProjectId { set; get; }

        public string HashId { get; set; }
      

        [Required]
        public string Title { set; get; }

        [JsonIgnore]
        public int Duration { set; get; }

        [Required(ErrorMessage ="Yêu cầu chọn ngày bắt đầu")]
        public DateTime StartDate { set; get; }

        [Required(ErrorMessage = "Yêu cầu chọn ngày kết thúc")]
        public DateTime EndDate { set; get; }

    }
}
