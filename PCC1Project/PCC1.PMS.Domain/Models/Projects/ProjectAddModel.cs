﻿using PCC1.PMS.Domain.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace PCC1.PMS.Domain.Models.Projects
{
    public class ProjectAddModel
    {
        [Required(ErrorMessage ="Tên dự án không được để trống")]
        [MinLength(10, ErrorMessage ="Tên dự án ít nhất 10 ký tự")] 
        public string Name { get; set; }

        [Required(ErrorMessage = "Loại hợp đồng không được để trống")]
        [EnumDataType(typeof(ContractType), ErrorMessage = "Đầu vào loại hợp đồng không thuộc các loại hợp đồng đã định nghĩa")]
        public ContractType ContractType { set; get; }
  

        [MaxLength(256, ErrorMessage = "Tên chủ đầu tư không được quá 256 ký tự")]
        public string Investor { set; get; }

        [MaxLength(256, ErrorMessage = "Tên đơn vị giám sát không được quá 256 ký tự")]
        public string SupervisingConsultant { set; get; }

        [MaxLength(256, ErrorMessage = "Tên đơn vị thiết kế không được quá 256 ký tự")]
        public string DesignConsultant { set; get; }

        [MaxLength(256, ErrorMessage = "Tên nhà thầu phụ không được quá 256 ký tự")]
        public string Subcontractor { set; get; }

        [EnumDataType(typeof(ConstructionLevelProject), ErrorMessage = "Đầu vào cấp công trình không thuộc cấp công trình đã định nghĩa")]
        public ConstructionLevelProject? ConstructionLevel { set; get; }

        public int? ConstructionType;

        public decimal? CapitalSource { set; get; }

        public int? ProjectGroup { set; get; }

        public string Scale { set; get; }

        public int CreatedBy { get; set; }
    }
}
