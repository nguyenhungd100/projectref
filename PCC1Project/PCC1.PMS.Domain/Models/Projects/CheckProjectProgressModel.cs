﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace PCC1.PMS.Domain.Models.Projects
{
    public class CheckProjectProgressModel
    {
        public string hashId { get; set; }

        public int? PlanNo { get; set; }

    }
}
