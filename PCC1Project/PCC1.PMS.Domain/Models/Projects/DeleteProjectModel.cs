﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PCC1.PMS.Domain.Models.Projects
{
    public class DeleteProjectModel
    {
        public int ProjectId { get; set; }
    }
}
