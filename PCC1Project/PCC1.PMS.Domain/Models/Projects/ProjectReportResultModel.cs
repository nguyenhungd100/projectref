﻿using Newtonsoft.Json;
using PCC1.PMS.Domain.Entity;
using PCC1.PMS.Domain.Enums;
using PCC1.PMS.Domain.Models.ProjectReports;
using PCC1.PMS.Domain.Models.ProjectReports.DefineMachineryManpower;
using PCC1.PMS.Domain.Models.Workitems;
using System;
using System.Collections.Generic;
using System.Text;

namespace PCC1.PMS.Domain.Models.Projects
{
    public class ProjectReportResultModel : BaseEntity
    {
        public ProjectReportType ReportType { set; get; }

        public int ProjectId { set; get; }

        public string ProjectName { get; set; }

        public int CreatedBy { set; get; }

        public string Name { get; set; }

        public string Email { get; set; }

        public DateTime CreatedOnUtc { set; get; }

        public ProjectReportStatus Status { set; get; }

        public string MorningWeather { get; set; }

        public string AfternoonWeather { get; set; }

        public string ManpowerInfo { get; set; }

        public string MachineInfo { get; set; }

        public string Note { set; get; }
        //public string Images { set; get; }
        public string ProjectHashId { get; set; }

        public string Reason { get; set; }

        public string UnderPlaceCollectInReport { get; set; }

        public int UnderPlaceCountInReport { get; set; }

        public IEnumerable<ReportPlanModel> ReportPlans { get; set; } = new List<ReportPlanModel>();

        public IEnumerable<ReportWorkItemModel> ReportWorkitems { get; set; } = new List<ReportWorkItemModel>();

        public List<MachineryManpowerDefinedModel> ReportMachineryManpowers { get; set; } = new List<MachineryManpowerDefinedModel>(); 

        public MachineryManpowerCollectModel MachineryManpowerCollects { get; set; } = new MachineryManpowerCollectModel(); // tổng hợp tất cả nhân lực, máy móc

        public IEnumerable<CollectNumberInReportWorkitemModel> CollectNumbers { get; set; } = new List<CollectNumberInReportWorkitemModel>();

        public IEnumerable<CollectCategoryWorkitemInReportModel> CollectCategories { get; set; } = new List<CollectCategoryWorkitemInReportModel>();

        public IEnumerable<CollectWorkitemỈnReportWithCompany> CollectWorkitemWithCompanies { get; set; } = new List<CollectWorkitemỈnReportWithCompany>();

        public bool IsRead { get; set; }

        [JsonIgnore]
        public string MarkedRead { get; set; }

    }
}
