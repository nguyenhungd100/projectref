﻿using PCC1.PMS.Domain.Models.Workitems;
using System;
using System.Collections.Generic;
using System.Text;

namespace PCC1.PMS.Domain.Models.Workitems
{
    public class SettlementViewModel
    {
        public IEnumerable<SettlementWorkitemModel> SettlementWorkitems { get; set; }

        public decimal SettlementMoneyTotal { get; set; }

        public decimal SettlementMaxOrder { get; set; }
    }
}
