﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PCC1.PMS.Domain.Models.Workitems
{
    public class SettlementForQuarterCurrent
    {
        public decimal AccumulatedNumber { get; set; }

        public decimal Revenue { get; set; }
    }
}
