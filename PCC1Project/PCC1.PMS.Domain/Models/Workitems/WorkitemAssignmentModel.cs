﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PCC1.PMS.Domain.Models.Workitems
{
    public class WorkitemAssignmentModel
    {
        public int Id { get; set; }

        public string WorkitemName { get; set; }

        public string STT { get; set; }

        public int LevelHeading { get; set; }

        public string CalculationUnit { get; set; }

        public decimal? Quantity { get; set; }

        public decimal? UnitPrice { get; set; }

        public List<AssignmentCompanyModel> AssignmentCompanies { get; set; }

        public bool IsBold { get; set; } = false;

    }
}
