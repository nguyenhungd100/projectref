﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PCC1.PMS.Domain.Models.Workitems
{
    public class CollectNumberInReportWorkitemModel
    {
        public int WorkitemId { get; set; }

        public decimal CompletedNumberTotal { get; set; }

        public string CompletePlaceTotal { get; set; }

        public string UnderPlaceTotal { get; set; }

    }
}
