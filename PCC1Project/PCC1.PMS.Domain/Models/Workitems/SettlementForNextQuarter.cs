﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PCC1.PMS.Domain.Models.Workitems
{
    public class SettlementForNextQuarter
    {
        /// <summary>
        /// Ước lượng số hạng lượng cần thanh toán cho tháng tới
        /// </summary>
        public decimal EstimateNumber { get; set; }

        /// <summary>
        /// Ước lượng doanh thu tháng tới dựa vào số lượng hạng mục cần thanh toán
        /// </summary>
        public decimal EstimateRevenue { get; set; }
    }
}
