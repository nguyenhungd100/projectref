﻿using Newtonsoft.Json;
using PCC1.PMS.Domain.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace PCC1.PMS.Domain.Models.Workitems
{
    public class PerformingSettlementModel
    {
        [Required]
        public int WorkitemId { get; set; }

        public int? CompanyId { get; set; }

        [Required]
        public decimal Quantity { get; set; }

        [JsonIgnore]
        public SettlementType Type { get; set; }

        public int? Times { get; set; }

        [JsonIgnore]
        public int PerformingUser { get; set; }

        public int? Quarter { get; set; }

        public int? Year { get; set; }
    }
}
