﻿using PCC1.PMS.Domain.Enums;
using PCC1.PMS.Domain.Models.Workitems;
using System;
using System.Collections.Generic;
using System.Text;

namespace PCC1.PMS.Domain.Models.Workitems
{
    public class SettlementWorkitemModel
    {
        public int Id { set; get; }

        public string Name { set; get; }

        public int WorkitemGroupId { get; set; }

        public int ProjectId { set; get; }

        public decimal Quantity { set; get; }

        public string CalculationUnit { get; set; }

        public decimal Money { get; set; }

        public IEnumerable<OrderSettlementWorkitemModel> OrderSettlements { get; set; }

        public decimal TotalPaidQuantity { set; get; }

        public decimal Amount { set; get; }


    }
}
