﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace PCC1.PMS.Domain.Models.Workitems
{
    public class CollectWorkitemCategoryInCompany
    {
        public string STT { get; set; }

        public int WorkitemCategoryId { get; set; }

        public string WorkitemCategoryName { get; set; }

        public string CategoryCompletePlace { get; set; }

        public int CategoryCompletePlaceCount { get; set; } = 0;

        public string CategoryUnderPlace { get; set; }

        public int CategoryUnderPlaceCount { get; set; } = 0;

        [JsonIgnore]
        public List<string> AddCategoryUnderPlace { get; set; } = new List<string>();

        [JsonIgnore]
        public List<string> AddCategoryCompletePlace { get; set; } = new List<string>();

        public decimal CumulativeNumber { get; set; } = 0;

        public string PlaceType { get; set; }

        public string CalculationUnit { get; set; }

        public List<string> Note { get; set; } = new List<string>();

    }
}
