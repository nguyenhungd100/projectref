﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PCC1.PMS.Domain.Models.Workitems
{
    public class CompleteReportWithCompany
    {
        public int CompanyId { get; set; }

        public string CompanyName { get; set; }

        public decimal CompleteNumber { get; set; }
    }
}
