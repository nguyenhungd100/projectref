﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace PCC1.PMS.Domain.Models.Workitems
{
    public class WorkitemWithHeadingModel
    {
        public WorkitemInfoModel WorkitemCategory { get; set; }

        public List<WorkitemInfoModel> WorkItemSmallerModels { get; set; } = new List<WorkitemInfoModel>();

        public decimal TotalIntoMoney { get; set; }

        [JsonIgnore]
        public string PlaceType { get; set; }

        [JsonIgnore]
        public string CalculationUnit { get; set; }
    }
}
