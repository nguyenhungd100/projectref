﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PCC1.PMS.Domain.Models.Workitems
{
    public class WorkitemPerQuarterModel
    {
        public string Title { get; set; }

        public decimal WorkitemCompleted { get; set; }

        public decimal EstimateNumber { get; set; }
    }
}
