﻿using Newtonsoft.Json;
using PCC1.PMS.Domain.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace PCC1.PMS.Domain.Models.Workitems
{
    public class OrderSettlementWorkitemModel
    {
        public int Id { get; set; }

        [JsonIgnore]
        public int WorkitemId { get; set; }

        public int CompanyId { get; set; }

        public int Times { get; set; }

        public decimal SettlementNumber { get; set; }

        public DateTime SettlementDate { get; set; }

        public SettlementType Type { get; set; }
    }
}
