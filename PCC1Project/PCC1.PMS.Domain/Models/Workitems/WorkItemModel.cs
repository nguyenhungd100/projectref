﻿using Newtonsoft.Json;
using PCC1.PMS.Domain.Enums;
using PCC1.PMS.Domain.Models.Workitems;
using System;
using System.Collections.Generic;
using System.Text;

namespace PCC1.PMS.Domain.Models.Workitems
{
    public class WorkItemModel
    {
        public int Id { set; get; }

        public string STT { get; set; }

        public string Name { set; get; }

        public string CalculationUnit { get; set; }

        public decimal? UnitPrice { get; set; }

        public decimal? IntoMoney { get; set; }

        public decimal? Quantity { set; get; }

        public decimal TotalPaidQuantity { get; set; }

        public decimal? CompletedNumberTotal { get; set; }

        public int LevelHeading { get; set; }

        public string HashId { get; set; }

        [JsonIgnore]
        public int ProjectId { set; get; }                 
        
    }
    //public class WorkitemAssignmentModel : WorkItemModel
    //{
       //public IEnumerable<WorkitemCompanyModel> Assignments { set ; get; }
        
    //}
}
