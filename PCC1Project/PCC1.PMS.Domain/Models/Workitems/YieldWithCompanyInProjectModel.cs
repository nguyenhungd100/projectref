﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PCC1.PMS.Domain.Models.Workitems
{
    public class YieldWithCompanyInProjectModel
    {
        public int CompanyId { get; set; }

        public string CompanyName { get; set; }

        public decimal AccumulatePreviousByCompany { get; set; } = 0; // tổng sản lượng lũy kế quý trước

        public decimal YieldByCompany { get; set; } = 0; // tổng sản lượng quý này

        public decimal AccumulateCurrentByCompany { get; set; } = 0; // tổng sản lượng lũy kế quý này

    }
}
