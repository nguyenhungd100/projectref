﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PCC1.PMS.Domain.Models.Workitems
{
    public class WorkItemExcelModel
    {
        public string STT { set; get; }

        public int? LevelHeading { set; get; }

        public string Name { set; get; }


        public string CalculationUnit { get; set; }

        public decimal? Quantity { set; get; }

        public decimal? UnitPrice { get; set; }

        public decimal? IntoMoney { get; set; }

        public int ProjectId { set; get; }

        public int RowNumber { get; set; }
    }
}
