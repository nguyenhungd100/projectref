﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PCC1.PMS.Domain.Models.Workitems.AccumulateCompleteQuarter
{
    public class WorkitemCompletedReportQuarterModel
    {
        public int WorkitemId { get; set; }

        public decimal SumCompletedNumber { get; set; }

        public List<CompleteReportWithCompany> ReportWithCompanies { get; set; } = new List<CompleteReportWithCompany>();

    }
}
