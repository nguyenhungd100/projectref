﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PCC1.PMS.Domain.Models.Workitems.AccumulateCompleteQuarter
{
    public class WorkitemEstimationNextQuarter
    {
        /// <summary>
        /// Kế hoạch số hạng mục hoàn thành theo quý
        /// </summary>
        public decimal EstimationNumber { get; set; }

        /// <summary>
        /// Ước tính sản lượng hoàn thành theo quý
        /// </summary>
        public decimal EstimationYield { get; set; }
    }
}
