﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PCC1.PMS.Domain.Models.Workitems.AccumulateCompleteQuarter
{
    public class WorkitemCompletedCurrentQuarter
    {
        /// <summary>
        /// Số hạng mục hoàn thành theo quý hiện tại
        /// </summary>
        public decimal? CompletedNumber { get; set; }

        /// <summary>
        /// Sản lượng theo quý hiện tại = khối lượng hoàn thành * đơn giá
        /// </summary>
        public decimal? Yield { get; set; }
    }
}
