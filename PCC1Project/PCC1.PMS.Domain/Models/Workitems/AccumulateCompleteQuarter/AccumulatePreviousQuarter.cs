﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PCC1.PMS.Domain.Models.Workitems.AccumulateCompleteQuarter
{
    public class AccumulatePreviousQuarter
    {
        /// <summary>
        /// Số hạng mục hoàn thành lũy kế quý trước
        /// </summary>
        public decimal? AccumulatedNumber { get; set; }

        /// <summary>
        /// Sản lượng lũy kế đến quý trước = khối lượng hoàn thành * đơn giá
        /// </summary>
        public decimal? AccumulatedYield { get; set; }
    }
}
