﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PCC1.PMS.Domain.Models.Workitems.AccumulateCompleteQuarter
{
    public class StatisticQuarterCategoryModel
    {
        public int CategoryId { get; set; }

        public string CategoryName { get; set; }

        public string STT { get; set; }

        public decimal? UnitPrice { get; set; }

        public decimal? Quantity { get; set; }

        public string CalculationUnit { get; set; }

        public bool NotYetAssigned { get; set; } = true;

        public List<WorkitemInfoModel> ExceptionNotAssigneds { get; set; } = new List<WorkitemInfoModel>();

        public List<AccumulateQuarterCompleteWithCompany> StatisticWorkitemSmallerWithCompanies { get; set; } = new List<AccumulateQuarterCompleteWithCompany>(); // trường hợp hạng mục lớn chứa nhều hạng mục con

        public List<AccumulateQuarterCompleteWithCompany> StatisticWithCompanies { get; set; } = new List<AccumulateQuarterCompleteWithCompany>();// trường hợp chỉ chứa hạng mục lớn

        public WorkitemEstimationNextQuarter NextQuarter { get; set; } // Dự kiến của quý tiếp theo( có thể có của hạng mục lớn) 
    }
}
