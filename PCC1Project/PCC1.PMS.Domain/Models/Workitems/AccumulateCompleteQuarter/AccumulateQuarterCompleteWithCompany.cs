﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PCC1.PMS.Domain.Models.Workitems.AccumulateCompleteQuarter
{
    public class AccumulateQuarterCompleteWithCompany
    {
        public int CompanyId { get; set; }

        public string CompanyName { get; set; }

        public List<CompleteInQuarterWithCompanyDetail> CompleteWithCompanyDetails { get; set; } = new List<CompleteInQuarterWithCompanyDetail>();
    }
}
