﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PCC1.PMS.Domain.Models.Workitems.AccumulateCompleteQuarter
{
    public class CompleteInQuarterWithCompanyDetail
    {
        public string STT { get; set; }

        public int WorkitemId { get; set; }

        public string WorkitemName { get; set; }

        public decimal? UnitPrice { get; set; }

        public decimal? Quantity { get; set; }

        public decimal? IntoMoney { get; set; }

        public string CalculationUnit { get; set; }

        public int? LevelHeading { get; set; }

        public bool IsBold { get; set; } = false;

        public AccumulatePreviousQuarter PreviousQuarter { get; set; } // Lũy kế đến tháng trước

        public WorkitemCompletedCurrentQuarter CurrentQuarter { get; set; } // Hoàn thành của tháng này

        public AccumulateCompleteCurrentQuarter AccumulateCompleteCurrent { get; set; } // Lũy kế hoàn thành hiện tại

        public WorkitemEstimationNextQuarter NextQuarter { get; set; } // Dự kiến của tháng tiếp theo       
    }
}
