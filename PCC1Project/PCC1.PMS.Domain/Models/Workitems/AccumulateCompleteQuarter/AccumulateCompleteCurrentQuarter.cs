﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PCC1.PMS.Domain.Models.Workitems.AccumulateCompleteQuarter
{
    public class AccumulateCompleteCurrentQuarter
    {
        /// <summary>
        /// Số hạng mục hoàn thành lũy kế đến quý hiện tại
        /// </summary>
        public decimal? AccumulateCurrentNumber { get; set; }

        /// <summary>
        /// Sản lượng hoàn thành lũy kế đến quý hiện tại = khối lượng hoàn thành * đơn giá
        /// </summary>
        public decimal? AccumulateCurrentYield { get; set; }
    }
}
