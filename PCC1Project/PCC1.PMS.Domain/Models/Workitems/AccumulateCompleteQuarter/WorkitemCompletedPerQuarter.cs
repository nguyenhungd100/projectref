﻿using PCC1.PMS.Domain.Models.Workitems.AccumulateCompleteQuarter;
using System;
using System.Collections.Generic;
using System.Text;

namespace PCC1.PMS.Domain.Models.Workitems.AccumulateCompleteQuarter
{
    public class WorkitemCompletedPerQuarter
    {
        public int WorkitemId { get; set; }

        public string STT { get; set; }


        public string WorkitemName { get; set; }

        public decimal? Quantity { get; set; }

        public string CalculationUnit { get; set; }

        public decimal? UnitPrice { get; set; }

        public int LevelHeading { get; set; }

        public decimal? CompletedNumberTotal { get; set; }

        public decimal? CompletedYield { get; set; }

        public AccumulatePreviousQuarter PreviousQuarter { get; set; }

        public WorkitemCompletedCurrentQuarter CurrentQuarter { get; set; }

        public AccumulateCompleteCurrentQuarter AccumulateCompleteCurrent { get; set; }

        public WorkitemEstimationNextQuarter NextQuarter { get; set; }

        public bool IsBold { get; set; } = false;
    }
}
