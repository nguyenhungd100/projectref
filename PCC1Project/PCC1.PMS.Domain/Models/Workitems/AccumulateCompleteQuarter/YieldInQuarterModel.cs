﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PCC1.PMS.Domain.Models.Workitems.AccumulateCompleteQuarter
{
    public class YieldInQuarterModel
    {
        public List<WorkitemCompletedPerQuarter> CompletedPerQuarter { get; set; } = new List<WorkitemCompletedPerQuarter>();

        public decimal TotalAccumulatePrevious { get; set; } // tổng sản lượng lũy kế quý trước

        public decimal TotalYield { get; set; } = 0; // tổng sản lượng quý này

        public decimal TotalAccumulateCurrent { get; set; } // tổng sản lượng lũy kế quý này

        public decimal TotalNextQuarterYield { get; set; } = 0; // tổng sản lượng dự kiến quý tiếp theo

        public decimal TotalCompletedYield { get; set; } = 0; // tổng sản lượng không phụ thuộc thời gian

        public List<YieldWithCompanyInProjectModel> YieldWithCompany { get; set; } = new List<YieldWithCompanyInProjectModel>();

        public List<StatisticQuarterCategoryModel> StatisticCategories { get; set; } = new List<StatisticQuarterCategoryModel>();

    }
}
