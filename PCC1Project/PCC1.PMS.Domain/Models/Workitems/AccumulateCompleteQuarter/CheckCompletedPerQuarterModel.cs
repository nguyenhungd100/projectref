﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace PCC1.PMS.Domain.Models.Workitems.AccumulateCompleteQuarter
{
    public class CheckCompletedPerQuarterModel
    {
        [JsonIgnore]
        public int ProjectId { get; set; }

        public int Quarter { get; set; }

        public int Year { get; set; }
    }
}
