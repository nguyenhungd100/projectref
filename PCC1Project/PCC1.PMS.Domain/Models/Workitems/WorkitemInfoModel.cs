﻿using Newtonsoft.Json;
using PCC1.PMS.Domain.Models.Workitems.AccumulateCompleteQuarter;
using System;
using System.Collections.Generic;
using System.Text;

namespace PCC1.PMS.Domain.Models.Workitems
{
    public class WorkitemInfoModel
    {
        public int Id { set; get; }

        public string STT { get; set; }

        public string Name { set; get; }

        public string CalculationUnit { get; set; }

        public decimal? UnitPrice { get; set; }

        public decimal? IntoMoney { get; set; }

        public decimal? Quantity { set; get; }

        public decimal TotalPaidQuantity { get; set; }

        public decimal? CompletedNumberTotal { get; set; }

        public int LevelHeading { get; set; }

        public string HashId { get; set; }

        [JsonIgnore]
        public int ProjectId { set; get; }

        public string PlaceCollection { get; set; }

        public int PlaceCountCollection { get; set; }

        public bool IsBold { get; set; } = false;

        [JsonIgnore]
        public WorkitemEstimationNextMonth NextMonth { get; set; }

        [JsonIgnore]
        public WorkitemEstimationNextQuarter NextQuarter { get; set; }
    }
}
