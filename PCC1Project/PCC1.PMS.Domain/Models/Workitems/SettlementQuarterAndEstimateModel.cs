﻿using PCC1.PMS.Domain.Models.EstimationPlans;
using System;
using System.Collections.Generic;
using System.Text;

namespace PCC1.PMS.Domain.Models.Workitems
{
    public class SettlementQuarterAndEstimateModel
    {
        public IEnumerable<PerformingSettlementModel> SettlementQuarterModels { get; set; }

        public IEnumerable<EstimationModel> EstimationModels { get; set; }
    }
}
