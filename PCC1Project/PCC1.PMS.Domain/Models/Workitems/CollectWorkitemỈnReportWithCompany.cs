﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PCC1.PMS.Domain.Models.Workitems
{
    public class CollectWorkitemỈnReportWithCompany
    {
        public int CompanyId { get; set; }

        public string CompanyName { get; set; }

        public string CompletePlaceWithCompany { get; set; }

        public int CompletePlaceCountWithCompany { get; set; } = 0;

        public string UnderPlaceWithCompany { get; set; }

        public int UnderPlaceCountWithCompany { get; set; } = 0;

        //public string CompletePlaceWithCompanyInReport { get; set; }

        //public int CompletePlaceCountWithCompanyInReport { get; set; } = 0;

        //public string UnderPlaceWithCompanyInReport { get; set; }

        //public int UnderPlaceCountWithCompanyInReport { get; set; } = 0;

        public List<string> Note { get; set; }

        public List<CollectWorkitemCategoryInCompany> WorkitemCategoryInCompanies { get; set; } = new List<CollectWorkitemCategoryInCompany>();
    }
}
