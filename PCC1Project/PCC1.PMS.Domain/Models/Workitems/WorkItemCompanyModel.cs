﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace PCC1.PMS.Domain.Models.Workitems
{
    public class WorkitemCompanyModel
    {
        public int Id { get; set; }
        public int WorkitemId { get; set; }
        public int CompanyId { get; set; }

        public string CompanyName { set; get; }

        public decimal AssignedNumber { get; set; }

        public decimal? CompletedNumber { get; set; }
    }
}
