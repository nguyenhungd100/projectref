﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PCC1.PMS.Domain.Models.Workitems
{
    public class AssignmentErrorModel 
    {
        public int Id { get; set; }

        public int WorkitemId { get; set; }

        public string Content { get; set; }

        public int AssignmentUser { get; set; }

        public DateTime AssignmentDate { get; set; }
    }
}
