﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PCC1.PMS.Domain.Models.Workitems
{
    public class AccumulatedPreviousMonth
    {
        /// <summary>
        /// Số hạng mục hoàn thành lũy kế tháng trước
        /// </summary>
        public decimal? AccumulatedNumber { get; set; }

        /// <summary>
        /// Sản lượng lũy kế đến tháng trước = khối lượng hoàn thành * đơn giá
        /// </summary>
        public decimal? AccumulatedYield { get; set; }
    }
}
