﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PCC1.PMS.Domain.Models.Workitems
{
    public class WorkitemEstimationNextMonth
    {
        /// <summary>
        /// Kế hoạch số hạng mục hoàn thành theo tháng 
        /// </summary>
        public decimal EstimationNumber { get; set; }

        /// <summary>
        /// Ước tính sản lượng hoàn thành theo tháng
        /// </summary>
        public decimal EstimationYield { get; set; }

    }
}
