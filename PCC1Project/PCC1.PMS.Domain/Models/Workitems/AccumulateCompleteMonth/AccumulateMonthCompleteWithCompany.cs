﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PCC1.PMS.Domain.Models.Workitems.AccumulateCompleteMonth
{
    public class AccumulateMonthCompleteWithCompany
    {
        public int CompanyId { get; set; }

        public string CompanyName { get; set; }

        public List<CompleteInMonthWithCompanyDetail> CompleteWithCompanyDetails { get; set; } = new List<CompleteInMonthWithCompanyDetail>();
    }
}
