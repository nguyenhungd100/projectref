﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PCC1.PMS.Domain.Models.Workitems.AccumulateCompleteMonth
{
    public class AccumulateCompleteCurrentMonth
    {
        /// <summary>
        /// Số hạng mục hoàn thành lũy kế đến tháng hiện tại
        /// </summary>
        public decimal? AccumulateCurrentNumber { get; set; }

        /// <summary>
        /// Sản lượng hoàn thành lũy kế đến tháng hiện tại = khối lượng hoàn thành * đơn giá
        /// </summary>
        public decimal? AccumulateCurrentYield { get; set; }
    }
}
