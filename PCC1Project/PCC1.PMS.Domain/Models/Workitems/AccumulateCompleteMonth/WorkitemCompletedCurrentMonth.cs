﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PCC1.PMS.Domain.Models.Workitems
{
    public class WorkitemCompletedCurrentMonth
    {
        /// <summary>
        /// Số hạng mục hoàn thành theo tháng hiện tại
        /// </summary>
        public decimal? CompletedNumber { get; set; }

        /// <summary>
        /// Sản lượng theo tháng hiện tại = khối lượng hoàn thành * đơn giá
        /// </summary>
        public decimal? Yield { get; set; }
    }
}
