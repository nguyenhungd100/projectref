﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PCC1.PMS.Domain.Models.Workitems
{
    public class WorkitemCompletedReportMonthModel
    {
        public int WorkitemId { get; set; }

        public decimal SumCompletedNumber { get; set; }

        public List<CompleteReportWithCompany> ReportWithCompanies { get; set; } = new List<CompleteReportWithCompany>();
    }
}
