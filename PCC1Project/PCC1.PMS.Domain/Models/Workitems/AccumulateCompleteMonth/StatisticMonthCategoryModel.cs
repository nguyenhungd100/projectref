﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PCC1.PMS.Domain.Models.Workitems.AccumulateCompleteMonth
{
    public class StatisticMonthCategoryModel
    {
        public int CategoryId { get; set; }

        public string CategoryName { get; set; }

        public string STT { get; set; }

        public decimal? UnitPrice { get; set; }

        public decimal? Quantity { get; set; }

        public string CalculationUnit { get; set; }

        public bool NotYetAssigned { get; set; } = true;

        public List<WorkitemInfoModel> ExceptionNotAssigneds { get; set; } = new List<WorkitemInfoModel>();

        public List<AccumulateMonthCompleteWithCompany> StatisticWorkitemSmallerWithCompanies { get; set; } = new List<AccumulateMonthCompleteWithCompany>(); // trường hợp hạng mục lớn chứa nhều hạng mục con

        public List<AccumulateMonthCompleteWithCompany> StatisticWithCompanies { get; set; } = new List<AccumulateMonthCompleteWithCompany>();// trường hợp chỉ chứa hạng mục lớn

        public WorkitemEstimationNextMonth NextMonth { get; set; } // Dự kiến của tháng tiếp theo( có thể có của hạng mục lớn)       


    }
}
