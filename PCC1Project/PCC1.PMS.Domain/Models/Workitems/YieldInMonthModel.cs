﻿using PCC1.PMS.Domain.Models.Workitems.AccumulateCompleteMonth;
using System;
using System.Collections.Generic;
using System.Text;

namespace PCC1.PMS.Domain.Models.Workitems
{
    public class YieldInMonthModel
    {
        public List<WorkitemCompletedPerMonth> CompletedPerMonth { get; set; } = new List<WorkitemCompletedPerMonth>();

        public decimal TotalCompletedYield { get; set; } = 0; // Tổng sản lượng không phụ thuộc thời gian

        public decimal TotalAccumulatePrevious { get; set; } // Tổng sản lượng lũy kế đến tháng trước

        public decimal TotalYield { get; set; } = 0;// Tổng sản lượng tháng này

        public decimal TotalAccumulateCurrent { get; set; } // Tổng sản lượng lũy kế đến tháng hiện tại

        public decimal TotalNextMonthYield { get; set; } = 0; // Tổng sản lượng dự kiến tháng tiếp theo

        public List<YieldWithCompanyInProjectModel> YieldWithCompany { get; set; } = new List<YieldWithCompanyInProjectModel>();

        public List<StatisticMonthCategoryModel> StatisticCategories { get; set; } = new List<StatisticMonthCategoryModel>();
    }
}
