﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace PCC1.PMS.Domain.Models.Workitems
{
    public class AssignmentUpdateModel
    {
        [Required(ErrorMessage ="Công ty không được để trống")]
        public int CompanyId { get; set; }

        [Required(ErrorMessage = "Hạng mục không được để trống")]
        public int WorkItemId { get; set; }

        [Required(ErrorMessage = "Số lượng hạng mục không được để trống")]
        public decimal AssignmentNumber { get; set; }
    }
}
