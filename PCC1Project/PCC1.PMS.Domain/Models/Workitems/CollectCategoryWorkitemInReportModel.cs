﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PCC1.PMS.Domain.Models.Workitems
{
    public class CollectCategoryWorkitemInReportModel
    {
        public string STT { get; set; }

        public string WorkitemCategoryName { get; set; }

        public string UnderPlaceCollect { get; set; }

        public int UnderPlaceCountCollect { get; set; }

        public string CompletePlaceCollect { get; set; }

        public int CompletePlaceCountCollect { get; set; }

        public decimal CumulativeNumber { get; set; } = 0;

        public string PlaceType { get; set; }

        public string CalculationUnit { get; set; }
    }
}
