﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace PCC1.PMS.Domain.Models.Workitems
{
    public class DeleteWorkitemModel
    {
        [Required(ErrorMessage ="Yêu cầu nhập mã hạng mục để xóa")]
        public int Id { get; set; }
    }
}
