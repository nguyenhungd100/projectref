﻿using PCC1.PMS.Domain.Models.Companies;
using System;
using System.Collections.Generic;
using System.Text;

namespace PCC1.PMS.Domain.Models.Workitems
{
    public class WorkitemAssignmentWithHeading
    {
        public WorkitemAssignmentModel WorkitemAssignmentCategory { get; set; }

        public List<WorkitemAssignmentModel> WorkitemAssignmentSmallers { get; set; }

        public List<CompanyModel> Companies { get; set; } = new List<CompanyModel>();
    }
}
