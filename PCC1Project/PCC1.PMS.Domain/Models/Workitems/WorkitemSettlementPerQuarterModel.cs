﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PCC1.PMS.Domain.Models.Workitems
{
    public class WorkitemSettlementPerQuarterModel
    {
        public int WorkitemId { get; set; }

        public int WorkitemGroupId { get; set; }

        public string WorkitemName { get; set; }

        public decimal Quantity { get; set; }

        public decimal TotalPaidQuantity { get; set; }

        public string CalculationUnit { get; set; }

        public decimal Money { get; set; }

        public SettlementForQuarterCurrent QuarterCurrent { get; set; }

        public SettlementForNextQuarter NextQuarter { get; set; }
    }
}
