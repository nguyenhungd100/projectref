﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace PCC1.PMS.Domain.Models.Workitems
{
    public class AddWorkitemModel
    {
        public int? WorkitemGroupId { get; set; }

        [Required(ErrorMessage ="Tên hạng mục không được để trống")]
        public string Name { set; get; }

        public string HashId { get; set; }
        
        [JsonIgnore]
        public int ProjectId { set; get; }

        [Required(ErrorMessage ="Số lượng hạng mục không được để trống")]
        public decimal? Quantity { set; get; }

        public string CalculationUnit { get; set; }

        [Required(ErrorMessage = "Đơn giá hạng mục không được để trống")]
        public decimal Money { get; set; }


    }
}
