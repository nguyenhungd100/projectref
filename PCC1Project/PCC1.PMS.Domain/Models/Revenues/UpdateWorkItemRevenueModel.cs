﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace PCC1.PMS.Domain.Models.Revenues
{
    public class UpdateWorkItemRevenueModel
    {
        [JsonIgnore]
        public int ProjectId { get; set; }

        public string HashId { get; set; }

        public IEnumerable<UpdateRevenueInfo> UpdateRevenueInfos { get; set; }

    }
}
