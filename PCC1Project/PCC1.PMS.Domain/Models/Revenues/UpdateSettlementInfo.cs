﻿using PCC1.PMS.Domain.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace PCC1.PMS.Domain.Models.Revenues
{
    public class UpdateSettlementInfo
    {
        [Required(ErrorMessage ="Hạng mục không được để trống")]
        public int WorkItemId { get; set; }

        //public int CompanyId { get; set; }

        public int Times { get; set; }

        public decimal SettlementNumber { get; set; }

        //[DataType(DataType.DateTime, ErrorMessage ="Yêu cầu nhập đúng ngày tháng")]
        public DateTime SettlementDate { get; set; }
    }
}
