﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PCC1.PMS.Domain.Models.Revenues
{
    public class TimesRevenueInfo
    {
        public int Times { get; set; }

        public decimal SettlementNumber { get; set; }
    }
}
