﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PCC1.PMS.Domain.Models.Revenues
{
    public class QuarterRevenueInfo
    {
        public int Quarter { get; set; }

        public decimal Quantity { get; set; }
    }
}
