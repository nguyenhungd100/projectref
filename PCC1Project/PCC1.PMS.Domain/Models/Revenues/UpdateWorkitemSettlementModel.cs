﻿using System;
using System.Collections.Generic;
using System.Text;
using Newtonsoft.Json;

namespace PCC1.PMS.Domain.Models.Revenues
{
    public class UpdateWorkitemSettlementModel
    {
        [JsonIgnore]
        public int ProjectId { get; set; }

        public string Hashid { get; set; }

        public IEnumerable<UpdateSettlementInfo> UpdateSettlementInfos { get; set; }
    }
}
