﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PCC1.PMS.Domain.Models.Revenues
{
    public class FillEstimateToSettlementResult
    {
        public int WorkitemId { get; set; }

        public decimal? EstimateNumber { get; set; }
    }
}
