﻿using System.Collections.Generic;

namespace PCC1.PMS.Domain.Models.Revenues
{
    public class RevenueEstimateResultModel
    {
        public List<WorkitemRevenueInfoModel> WorkitemRevenueInfos { get; set; }

        public decimal TotalEstimateRevenueQuarter1 { get; set; }

        public decimal TotalEstimateRevenueQuarter2{ get; set; }


        public decimal TotalEstimateRevenueQuarter3 { get; set; }


        public decimal TotalEstimateRevenueQuarter4 { get; set; }


    }
}
