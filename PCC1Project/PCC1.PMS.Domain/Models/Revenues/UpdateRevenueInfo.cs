﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PCC1.PMS.Domain.Models.Revenues
{
    public class UpdateRevenueInfo
    {
        public int WorkItemId { get; set; }

        public decimal Quantity { get; set; }

        public int Quarter { get; set; }

        public int Year { get; set; }

    }
}
