﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PCC1.PMS.Domain.Models.Revenues
{
    public class SettlementRevenueResultModel
    {
        public List<SettlementWorkitemInfoModel> SettlementWorkitemInfos { get; set; }

        public List<TotalRevenueWithTimeModel> TotalRevenues { get; set; }

        public int TimeMax { get; set; }
    }
}
