﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PCC1.PMS.Domain.Models.Revenues { 
    public class SettlementWorkitemInfoModel
    {
        public int WorkitemId { get; set; }

        public string WorkitemName { get; set; }

        public int LevelHeading { get; set; }

        public string STT { get; set; }

        public decimal? Quantity { get; set; }

        public string CalculationUnit { get; set; }

        public decimal TotalPaidQuantity { get; set; }

        public decimal? UnitPrice { get; set; }

        public List<TimesRevenueInfo> TimesRevenues { get; set; } = new List<TimesRevenueInfo>();

        public bool IsBold { get; set; } = false;
    }
}
