﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PCC1.PMS.Domain.Models.Revenues
{
    public class TotalRevenueWithTimeModel
    {
        public int Times { get; set; }

        public decimal TotalValue { get; set; } = 0;
    }
}
