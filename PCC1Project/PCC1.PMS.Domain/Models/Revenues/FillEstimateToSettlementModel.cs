﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace PCC1.PMS.Domain.Models.Revenues
{
    public class FillEstimateToSettlementModel
    {

        public string HashId { get; set; }

        [JsonIgnore]
        public int ProjectId { get; set; }

        [DataType(DataType.DateTime, ErrorMessage ="Kiểu dữ liệu ngày tháng không đúng")]
        public DateTime DaysToFill { get; set; }


    }
}
