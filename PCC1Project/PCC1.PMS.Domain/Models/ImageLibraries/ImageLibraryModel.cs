﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Text;

namespace PCC1.PMS.Domain.Models.ImageLibraries
{
    public class ImageLibraryModel
    {
        public int Id { get; set; }

        public byte[]  Data { get; set; }

        public string Name { get; set; }

        public string Mime { get; set; }

        public DateTime CreatedDate { get; set; }
    }
}
