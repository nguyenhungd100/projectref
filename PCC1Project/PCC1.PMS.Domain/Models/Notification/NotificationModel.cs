﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PCC1.PMS.Domain.Models.Notification
{
    public class NotificationModel
    {
        public string ProjectHashId { set; get; }
        public string Title { get; set; }
        public EventType Type { get; set; }
        public bool Unread { get; set; } = true;
        public DateTime CreatedOnUtc { get; set; } = DateTime.UtcNow;
        public object Data { get; set; }
    }

    public enum EventType : int
    {
        System = 1,
        InfoProject = 10,
        AddWorkItem = 20,
        AddProjectMember = 21,
        RemoveProjectMember = 22,
        ApproveReport = 25,
        RejectReport = 30,
        SubmitedReport = 35,
        AbilityApproveProject = 40,
        UploadArchives = 45,
        TrackingMoneyAdvanceToManager = 50,
        RegisterMaterial = 55,
        ApproveMaterial = 60,
        RejectMaterial = 65

    }
}
