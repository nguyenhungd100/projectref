﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PCC1.PMS.Domain.Models.Notification
{
    public class ApproveRegisterMaterialEventModel : BaseEventModel
    {
        public int ProjectId { get; set; }

        public string ProjectHashId { get; set; }

        public string ProjectName { get; set; }

        public int ApproveUserId { get; set; }

        public string ApproveUserName { get; set; }
    }
}
