﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace PCC1.PMS.Domain.Models.Notification
{
    public class ApproveReportEventModel : BaseEventModel
    {       

        public int UserApproveId { get; set; }

        public string UserApproveName { get; set; }

        public string ProjectName { get; set; }

        public string ProjectHashId { get; set; }
    }
}
