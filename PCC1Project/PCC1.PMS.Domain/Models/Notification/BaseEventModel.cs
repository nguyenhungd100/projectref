﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PCC1.PMS.Domain.Models.Notification
{
    public class BaseEventModel
    {
        public DateTime CreatedOn { get; set; } = DateTime.Now;
        public EventType Type { get; set; } = EventType.System;
    }
}
