﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PCC1.PMS.Domain.Models.Notification
{
    public class AblityAproveProject : BaseEventModel
    {
        public string ProjectHashId { get; set; }

        public string ProjectName { get; set; }
    }
}
