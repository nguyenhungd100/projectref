﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PCC1.PMS.Domain.Models.Notification
{
    public class SubmitedReportEventModel : BaseEventModel
    {
        public string UserSend { get; set; }

        public string ProjectName { get; set; }

        public string ProjectHashId { get; set; }
    }
}
