﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PCC1.PMS.Domain.Models.Notification
{
    public class WorkItemAddNotificationModel
    {
        public int AddUserId { get; set; }

        public string AddUserName { set; get; }

        public string WorkitemName { get; set; }

        public int ProjectId { get; set; }

        public string ProjectName { get; set; }

        public List<int> ListCBQLDA { get; set; }
    }
}
