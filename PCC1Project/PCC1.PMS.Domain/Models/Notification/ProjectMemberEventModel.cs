﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PCC1.PMS.Domain.Models.Notification
{
    public class ProjectMemberEventModel : BaseEventModel
    {
        public string ProjectHashId { get; set; }
        public string ProjectName { set; get; }
        public int MemberId { get; set; }
        public string MemberName { set; get; }
        public int UserId { set; get; }
        public string UserName { set; get; }
    }
}
