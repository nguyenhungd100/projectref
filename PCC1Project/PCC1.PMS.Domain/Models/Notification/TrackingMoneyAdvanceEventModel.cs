﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PCC1.PMS.Domain.Models.Notification
{
    public class TrackingMoneyAdvanceEventModel : BaseEventModel
    {
        public string ProjectHashId { get; set; }

        public int ProjectId { get; set; }

        public string ProjectName { get; set; }

        public int CompanyId { get; set; }

        public string CompanyName { get; set; }
    }
}
