﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PCC1.PMS.Domain.Models.Notification
{
    public class RejectReportEventModel : BaseEventModel
    {
        public int UserRejectId { get; set; }

        public string UserRejectName { get; set; }

        public string ProjectName { get; set; }

        public string ProjectHashId { get; set; }
    }
}
