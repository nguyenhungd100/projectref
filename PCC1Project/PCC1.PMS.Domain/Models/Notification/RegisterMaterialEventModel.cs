﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PCC1.PMS.Domain.Models.Notification
{
    public class RegisterMaterialEventModel : BaseEventModel
    {
        public int ProjectId { get; set; }

        public string ProjectHashId { get; set; }

        public string ProjectName { get; set; }

        public int UserRegisterId { get; set; }

        public string UserRegisterName { get; set; }
    }
}
