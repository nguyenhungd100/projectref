﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PCC1.PMS.Domain.Models.Notification
{
    public class UploadArchivesEventModel : BaseEventModel
    {
        public int ProjectId { get; set; }

        public string ProjectHashId { get; set; }

        public string ProjectName { get; set; }

        public int CreatedBy { get; set; }

        public string CreatedUserName { get; set; }

        public string ArchivesName { get; set; }
    }
}
