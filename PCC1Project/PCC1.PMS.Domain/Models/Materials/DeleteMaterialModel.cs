﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace PCC1.PMS.Domain.Models.Materials
{
    public class DeleteMaterialModel
    {
        [Required(ErrorMessage ="Nhập phiếu vật tư cần xóa")]
        public int Id { get; set; }
    }
}
