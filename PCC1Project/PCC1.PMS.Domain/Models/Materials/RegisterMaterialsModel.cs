﻿using Newtonsoft.Json;
using PCC1.PMS.Domain.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace PCC1.PMS.Domain.Models.Materials
{  
    public class RegisterMaterialsModel
    {
        [JsonIgnore]
        public int ProjectId { get; set; }

        [StringLength(500,ErrorMessage ="Tên không được quá 500 ký tự")]
        [Required(ErrorMessage = "Yêu cầu nhập tên vật tư")]
        public string Name { get; set; }

        [Required(ErrorMessage = "Yêu cầu nhập vào số lượng cần đăng ký")]
        public decimal Quantity { get; set; }

        [StringLength(500, ErrorMessage = "Chú thích không quá 500 ký tự")]
        public string Note { get; set; }

        public DateTime? TimeRequired { get; set; }

    }
}
