﻿using Newtonsoft.Json;
using PCC1.PMS.Domain.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace PCC1.PMS.Domain.Models.Materials
{
    public class SearchRegisterMaterialModel : BaseSearchModel
    {
        public RegisterMaterialStatus? Status { get; set; }

        public int? UserId { get; set; }

        public DateTime? CreateDate { get; set; }

        [JsonIgnore]
        public int? ProjectId { get; set; }

        public string HashId { get; set; }

        public string Text { get; set; }

        [JsonIgnore]
        public int TotalRecord { get; set; }
    }
}
