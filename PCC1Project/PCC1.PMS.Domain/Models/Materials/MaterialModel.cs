﻿using Newtonsoft.Json;
using PCC1.PMS.Domain.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace PCC1.PMS.Domain.Models.Materials
{
    public class MaterialModel
    {
        public int Id { get; set; }

        [JsonIgnore]
        public int ProjectId { get; set; }

        public string HashId { get; set; }

        public string ProjectName { get; set; }

        public string Name { set; get; }

        public decimal Quantity { get; set; }      

        public DateTime CreatedDate { get; set; }

        public DateTime? ApprovedDate { get; set; }

        public int Petitioner { get; set; }

        public string PetitionerName { get; set; }

        public int? Approver { get; set; }

        public DateTime? TimeRequired { get; set; }

        public string Note { get; set; }

        public string ResponseMessage { get; set; }

        public RegisterMaterialStatus Status { get; set; }

       
    }
}
