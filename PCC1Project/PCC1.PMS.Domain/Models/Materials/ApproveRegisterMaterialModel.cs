﻿using Newtonsoft.Json;
using PCC1.PMS.Domain.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace PCC1.PMS.Domain.Models.Materials
{
    public class ApproveRegisterMaterialModel
    {
        public int Id { get; set; }

        [JsonIgnore]
        public int ApproveUser { get; set; }

        public string ResponseMessage { get; set; }

        [JsonIgnore]
        public RegisterMaterialStatus Status { get; set; }
    }
}
