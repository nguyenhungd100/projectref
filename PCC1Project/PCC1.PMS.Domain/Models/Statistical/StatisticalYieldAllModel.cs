﻿using PCC1.PMS.Domain.Models.Projects;
using System;
using System.Collections.Generic;
using System.Text;

namespace PCC1.PMS.Domain.Models.Statistical
{
    public class StatisticalYieldAllModel
    {
        public List<StatisticalAllProjectYieldModel> StatisticalAllProjectYields { get; set; } = new List<StatisticalAllProjectYieldModel>();

        public decimal TotalYieldAllWithTime { get; set; } = 0;
        
        public decimal TotalAccumulatedAllProject { get; set; } = 0;

        public decimal? TotalContractValueAllProject { get; set; } = 0;
    }
}
