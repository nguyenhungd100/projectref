﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PCC1.PMS.Domain.Models.Statistical
{
    public class StatisticsActualOfConstructionInfo
    {
        public int CompanyId { get; set; }

        public string CompanyName { get; set; }

        public decimal AssignmentNumber { get; set; } = 0;

        public decimal StatisticsCompletedNumber { get; set; } = 0;

        public string StatisticsCompletePlace { get; set; }

        public int CompletePlaceCount { get; set; } = 0;

        public string StatisticsUnderPlace { get; set; }

        public int UnderPlaceCount { get; set; } = 0;

    }
}
