﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace PCC1.PMS.Domain.Models.Statistical.StatisticalActuals
{
    public class CheckMonthlyReport
    {
        public string HashId { get; set; }

        [JsonIgnore]
        public int ProjectId { get; set; }

        public int? Month { get; set; }

        public int? Year { get; set; }
    }
}
