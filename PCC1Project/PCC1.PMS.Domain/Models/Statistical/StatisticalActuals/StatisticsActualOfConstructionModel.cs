﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PCC1.PMS.Domain.Models.Statistical
{
    public class StatisticsActualOfConstructionModel
    {
        public List<StatisticsActualOfConstructionInfo> StatisticsActuals { get; set; }

        public decimal TotalAssignment { get; set; } = 0;

        public decimal TotalStatisticsCompleted { get; set; } = 0;

        public string TotalStatisticsCompletePlace { get; set; }

        public int TotalCompletePlaceCount { get; set; } = 0;

        public string TotalStatisticsUnderPlace { get; set; }

        public int TotalUnderPlaceCount { get; set; } = 0;
    }
}
