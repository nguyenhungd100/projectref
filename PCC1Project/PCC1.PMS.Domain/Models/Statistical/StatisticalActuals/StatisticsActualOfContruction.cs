﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PCC1.PMS.Domain.Models.Statistical.StatisticalActuals
{
    public class StatisticsActualOfContruction
    {
        public string STT { get; set; }

        public int WorkitemCategoryId { get; set; }

        public string WorkitemCategoryName { get; set; }

        public List<StatisticsActualOfContructionDetails> ActualContruction { get; set; } = new List<StatisticsActualOfContructionDetails>();



        //public decimal TotalAssignment { get; set; } = 0;

        //public decimal TotalStatisticsCompleted { get; set; } = 0;

        //public string TotalStatisticsCompletePlace { get; set; }

        //public int TotalCompletePlaceCount { get; set; } = 0;

        //public string TotalStatisticsUnderPlace { get; set; }

        //public int TotalUnderPlaceCount { get; set; } = 0;
    }
}
