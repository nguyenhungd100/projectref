﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PCC1.PMS.Domain.Models.Statistical
{
    public class StatisticYieldWithCompanyModel
    {
        public List<StatisticCompanyYieldInProject> StatisticCompanyYieldInProjects { get; set; } = new List<StatisticCompanyYieldInProject>();

        public decimal TotalCompleteNumberInCompany { get; set; } = 0;
    }
}
