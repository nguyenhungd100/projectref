﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PCC1.PMS.Domain.Models.Statistical.StatisticalSafelyAllProjects
{
    public class RatingCompanyViolation
    {
        public int CompanyId { get; set; }

        public string CompanyName { get; set; }

        public string Rating { get; set; }
    }
}
