﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PCC1.PMS.Domain.Models.Statistical.StatisticalSafelyAllProjects
{
    public class StatisticCompanyWithProject
    {
        public int ProjectId { get; set; }

        public string ProjectName { get; set; }

        public string HashId { get; set; }

        public List<RatingCompanyViolation> RatingCompanyViolations { get; set; } = new List<RatingCompanyViolation>();
    }
}
