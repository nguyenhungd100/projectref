﻿using PCC1.PMS.Domain.Models.Statistical.StatisticalSafelyAllProjects;
using System;
using System.Collections.Generic;
using System.Text;

namespace PCC1.PMS.Domain.Models.Statistical
{
    public class StatisticalViolationAllProjectModel
    {
        public List<StatisticCompanyWithProject> StatisticCompanyWithProjects { get; set; }

        public List<RatingCompanyViolation> RatingTotals { get; set; } = new List<RatingCompanyViolation>();
    }
}
