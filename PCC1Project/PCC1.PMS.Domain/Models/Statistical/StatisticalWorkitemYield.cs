﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PCC1.PMS.Domain.Models.Statistical
{
    public class StatisticalWorkitemYield
    {
        public string STT { get; set; }

        public int WorkitemId { get; set; }

        public string WorkitemName { get; set; }

        public int LevelHeading { get; set; }

        public string CalculationUnit { get; set; }

        public decimal? UnitPrice { get; set; }

        public decimal? Quantity { get; set; }

        public decimal? CompletedNumber { get; set; }

        public decimal? WorkitemYield { get; set; }

        public bool IsBold { get; set; } = false;
    }
}
