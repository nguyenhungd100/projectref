﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace PCC1.PMS.Domain.Models.Projects
{
    public class StatisticalAllProjectYieldModel
    {
        public string HashId { get; set; }

        public string ProjectName { get; set; }

        public decimal Accumulated { get; set; }

        public decimal? TotalContractValue { get; set; }

        public decimal ProjectYieldWithTime { get; set; }

    }
}
