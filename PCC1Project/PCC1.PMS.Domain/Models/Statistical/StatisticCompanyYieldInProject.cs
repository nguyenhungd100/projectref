﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PCC1.PMS.Domain.Models.Statistical
{
    public class StatisticCompanyYieldInProject
    {
        public string CompanyName { get; set; }

        public int CompanyId { get; set; }

        public decimal CompleteNumberInCompany { get; set; } = 0;

        public decimal YieldNumberInCompany { get; set; } = 0;
    }
}
