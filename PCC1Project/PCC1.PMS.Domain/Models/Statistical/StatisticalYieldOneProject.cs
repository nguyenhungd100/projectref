﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PCC1.PMS.Domain.Models.Statistical
{
    public class StatisticalYieldOneProject
    {
        public List<StatisticalWorkitemYield> StatisticalWorkitemYields { get; set; } = new List<StatisticalWorkitemYield>();

        public decimal ProjectYield { get; set; } = 0;
    }
}
