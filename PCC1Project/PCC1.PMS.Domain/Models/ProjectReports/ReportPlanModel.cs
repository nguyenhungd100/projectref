﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PCC1.PMS.Domain.Models.ProjectReports
{
    public class ReportPlanModel
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public int ProjectReportId { get; set; }
        public Guid PlanId { get; set; }
        public int CompletedPercentage { get; set; }
        public int? PercentageCurrent { get; set; }

    }
}
