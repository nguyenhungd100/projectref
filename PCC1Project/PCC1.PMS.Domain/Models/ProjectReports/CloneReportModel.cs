﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace PCC1.PMS.Domain.Models.ProjectReports
{
    public class CloneReportModel
    {
        public int ReportId { get; set; }

        [JsonIgnore]
        public int UserId { get; set; }
    }
}
