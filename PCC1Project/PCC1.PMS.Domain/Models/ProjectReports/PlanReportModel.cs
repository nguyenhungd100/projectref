﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PCC1.PMS.Domain.Models.ProjectReports
{
    public class PlanReportModel
    {
        public int Id { get; set; }
        public int ProjectReportId { get; set; }
        public Guid PlanId { get; set; }
        public int CompletedPercentage { get; set; }
    }
}
