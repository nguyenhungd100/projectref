﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Text;

namespace PCC1.PMS.Domain.Models.ProjectReports
{
    public class AddReportImageModel
    {
        public int ProjectReportId { get; set; }

        public int ReportWorkitemId { get; set; }

        public IFormFileCollection Files { get; set; }
    }
}
