﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PCC1.PMS.Domain.Models.ProjectReports
{
    public class ShowReportOnDardboardModel
    {
        public int CreatedBy { get; set; }

        public string CreateName { get; set; }

        public int ReportId { get; set; }

        public DateTime CreatedDate { get; set; }

        public int ProjectId { get; set; }

        public string HashId { get; set; }

        public string ProjectName { get; set; }

    }
}
