﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace PCC1.PMS.Domain.Models.Projects
{
    public class WorkitemReportModel
    {
        public int Id { get; set; }
        public int ProjectReportId { get; set; }
        public int WorkitemId { get; set; }

        public int CompanyId { get; set; }
        public decimal CompletedNumber { get; set; }

        public string Content { get; set; }
    }
}
