﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PCC1.PMS.Domain.Models.ProjectReports.DefineMachineryManpower
{
    public class MachineryManpowerCollectModel
    {
        public List<ManpowerDefined> ManpowerCollects { get; set; } = new List<ManpowerDefined>();

        public List<AddManpowerDefined> AddManpowerCollects { get; set; } = new List<AddManpowerDefined>();

        public List<MachineryDefined> MachineryCollects { get; set; } = new List<MachineryDefined>();

        public List<AddMachineryDefined> AddMachineryCollects { get; set; } = new List<AddMachineryDefined>();
    }
}
