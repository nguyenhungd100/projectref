﻿using PCC1.PMS.Domain.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace PCC1.PMS.Domain.Models.ProjectReports.DefineMachineryManpower
{
    public class MachineryManpowerCommon
    {
        public MachineryManpowerType Code { get; set; }

        public string Title { get; set; }

        public int Quantity { get; set; }
    }
}
