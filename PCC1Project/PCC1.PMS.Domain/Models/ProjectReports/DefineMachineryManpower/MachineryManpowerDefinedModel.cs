﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PCC1.PMS.Domain.Models.ProjectReports.DefineMachineryManpower
{
    public class MachineryManpowerDefinedModel
    {
        public int? CompanyId { get; set; }

        public string CompanyName { get; set; }

        public List<ManpowerDefined> ManpowerDefineds { get; set; } = new List<ManpowerDefined>();

        public List<AddManpowerDefined> AddManpowerDefineds { get; set; } = new List<AddManpowerDefined>();

        public List<MachineryDefined> MachineryDefineds { get; set; } = new List<MachineryDefined>();

        public List<AddMachineryDefined> AddMachineryDefineds { get; set; } = new List<AddMachineryDefined>();

    }
}
