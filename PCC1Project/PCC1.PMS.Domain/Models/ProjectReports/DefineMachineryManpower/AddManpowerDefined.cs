﻿using PCC1.PMS.Domain.Models.ProjectReports.StatisticsHumanResource;
using System;
using System.Collections.Generic;
using System.Text;

namespace PCC1.PMS.Domain.Models.ProjectReports.DefineMachineryManpower
{
    public class AddManpowerDefined : MachineryManpowerCommon
    {
        public List<MachineryManpowerWithCompany> MachineryManpowerWithCompanies { get; set; } = new List<MachineryManpowerWithCompany>();
    }
}
