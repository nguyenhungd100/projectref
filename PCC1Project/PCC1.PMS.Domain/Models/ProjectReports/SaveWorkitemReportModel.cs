﻿using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace PCC1.PMS.Domain.Models.ProjectReports
{
    public class SaveWorkitemReportModel
    {
        public int? Id { get; set; }

        [JsonIgnore]
        //[Required(ErrorMessage = "Mã báo cáo không được để trống")]
        public int ProjectReportId { get; set; }

        [Required(ErrorMessage = "Mã hạng mục không được để trống")]
        public int WorkitemId { get; set; }

        [Required(ErrorMessage = "Công ty không được để trống")]
        public int CompanyId { get; set; }

        public decimal? CompletedNumber { get; set; }

        public string UnderPlace { get; set; }

        public string CompletePlace { get; set; }

        public string Content { get; set; }
       
        public List<WorkitemImageInfo> Images { get; set; }

        public string PlaceType { get; set; }
    }
}
