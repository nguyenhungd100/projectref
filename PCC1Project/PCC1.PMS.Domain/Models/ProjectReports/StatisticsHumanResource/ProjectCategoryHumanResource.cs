﻿using PCC1.PMS.Domain.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace PCC1.PMS.Domain.Models.ProjectReports.StatisticsHumanResource
{
    public class ProjectCategoryHumanResource
    {
        public string Title { get; set; }

        public MachineryManpowerType Code { get; set; }

        public List<HumanResourceByCompanyModel> HumanResourceByCompanies { get; set; } = new List<HumanResourceByCompanyModel>();
    }
}
