﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PCC1.PMS.Domain.Models.ProjectReports.StatisticsHumanResource
{
    public class MachineryManpowerWithCompany
    {
        public int CompanyId { get; set; }

        public string CompanyName { get; set; }

        public int Quantity { get; set; } = 0;
    }
}
