﻿using PCC1.PMS.Domain.Models.Companies;
using System;
using System.Collections.Generic;
using System.Text;

namespace PCC1.PMS.Domain.Models.ProjectReports.StatisticsHumanResource
{
    public class StatisticsHumanResourceModel
    {
        public List<StatisticsHumanByProjectModel> StatisticsHumanByProjects { get; set; } = new List<StatisticsHumanByProjectModel>();

        public List<CompanyModel> Companies { get; set; } = new List<CompanyModel>();
    }
}
