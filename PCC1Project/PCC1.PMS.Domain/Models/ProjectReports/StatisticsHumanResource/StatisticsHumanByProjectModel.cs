﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace PCC1.PMS.Domain.Models.ProjectReports.StatisticsHumanResource
{
    public class StatisticsHumanByProjectModel
    {
        [JsonIgnore]
        public int ProjectId { get; set; }

        public string HashId { get; set; }

        public string ProjectName { get; set; }

        public List<ProjectCategoryHumanResource> ProjectCategoryHumanResources { get; set; } = new List<ProjectCategoryHumanResource>();
    }
}
