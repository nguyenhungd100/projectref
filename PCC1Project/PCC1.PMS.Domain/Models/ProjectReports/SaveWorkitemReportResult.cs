﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PCC1.PMS.Domain.Models.ProjectReports
{
    public class SaveWorkitemReportResult
    {
        public IEnumerable<int> WorkitemReportId { get; set; }

        public bool Status { get; set; }

        public IEnumerable<int> ImageId { get; set; }
    }
}
