﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Text;

namespace PCC1.PMS.Domain.Models.ProjectReports
{
    public class TestImageModel
    {
        public string JsonContent { get; set; }

        public IFormFileCollection Files { get; set; }
    }
}
