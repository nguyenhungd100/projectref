﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace PCC1.PMS.Domain.Models.ProjectReports
{
    public class DeleteWorkitemReportModel
    {
        [Required(ErrorMessage ="Hạng mục không được để trống")]
        public int Id { get; set; }
    }
}
