﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PCC1.PMS.Domain.Models.ProjectReports
{
    public class ImageCoordinates
    {
        public string Lat { get; set; }

        public string Lng { get; set; }
    }
}
