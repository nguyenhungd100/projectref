﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PCC1.PMS.Domain.Models.ProjectReports.AutoFillReportWithCompany
{
    public class FillContentReportByCompany
    {
        public int CompanyId { get; set; }

        public int WorkitemId { get; set; }

        public string WorkitemName { get; set; }

        public string CalculationUnit { get; set; }

        public decimal AssignmentNumber { get; set; }

        public decimal NumberAccumulatedWithCompany { get; set; }

        public string CompletePlace { get; set; }

        public string UnderPlace { get; set; }
       
        public string Content { get; set; }

        public string PlaceType { get; set; }
    }
}
