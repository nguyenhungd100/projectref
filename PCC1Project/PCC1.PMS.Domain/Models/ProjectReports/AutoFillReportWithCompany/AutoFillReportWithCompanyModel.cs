﻿using PCC1.PMS.Domain.Models.ProjectReports.AutoFillReportWithCompany;
using System;
using System.Collections.Generic;
using System.Text;

namespace PCC1.PMS.Domain.Models.ProjectReports
{
    public class AutoFillReportWithCompanyModel
    {
        public int CompanyId { get; set; }

        public List<FillContentReportByCompany> Workitems { get; set; } = new List<FillContentReportByCompany>();
        
    }
}
