﻿namespace PCC1.PMS.Domain.Models.ProjectReports
{
    public class ReportImageModel
    {
        public int Id { get; set; }

        public FileContentResult Data { get; set; }
    }
}
