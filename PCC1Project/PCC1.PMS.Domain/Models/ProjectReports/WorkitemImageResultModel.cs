﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PCC1.PMS.Domain.Models.ProjectReports
{
    public class WorkitemImageResultModel : ImageCoordinates
    {
        public Guid Image { get; set; }

        public string Message { get; set; }
    }
}
