﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PCC1.PMS.Domain.Models.ProjectReports
{
    public class WorkitemImageInfo : ImageCoordinates
    {
        public string Image { get; set; }

        public string Message { get; set; }
    }
}
