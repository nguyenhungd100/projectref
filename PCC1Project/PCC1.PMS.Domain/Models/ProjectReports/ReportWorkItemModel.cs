﻿using System;
using System.Collections.Generic;
using System.Text;
using PCC1.PMS.Domain.Entity;

namespace PCC1.PMS.Domain.Models.ProjectReports
{
    public class ReportWorkItemModel
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public int ProjectReportId { get; set; }

        public int WorkitemId { get; set; }
     
        public int CompanyId { get; set; }

        public decimal CompletedNumber { get; set; }

        public decimal NumberAccumulatedWithCompany { get; set; }

        public decimal? CompletedNumberCurrent { get; set; }

        public string UnderPlace { get; set; }

        public int UnderPlaceCount { get; set; } = 0;

        public string CompletePlace { get; set; }

        public int CompletePlaceCount { get; set; } = 0;

        public string CompanyName { get; set; }

        public string CalculationUnit { get; set; }

        public string Content { get; set; }

        public decimal Quantity { get; set; }

        public IEnumerable<WorkitemImageResultModel> Images { get; set; }

        public decimal AssignedNumber { get; set; }

        public decimal? ApprovedCompletedNumber { get; set; }

        public string PlaceType { get; set; }

    }
}
