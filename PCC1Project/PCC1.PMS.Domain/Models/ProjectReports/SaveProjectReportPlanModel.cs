﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace PCC1.PMS.Domain.Models.ProjectReports
{
    public class SaveProjectReportPlanModel
    {
        public int? Id { get; set; }

        [JsonIgnore]
        //[Required(ErrorMessage ="Mã báo cáo không được để trống")]
        public int ProjectReportId { get; set; }

        [Required(ErrorMessage ="Mã kế hoạch không được để trống")]
        public Guid PlanId { get; set; }

        [Required(ErrorMessage ="Phần trăm hoàn thành không được để trống")]
        public int CompletedPercentage { get; set; }
    }
}
