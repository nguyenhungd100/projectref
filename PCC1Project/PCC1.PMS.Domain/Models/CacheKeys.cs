﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PCC1.PMS.Domain.Models
{
    public static class CACHEKEYS
    {
        public const string ALL_AGENT_LOCATIONS = "ALL_AGENT_LOCATIONS";
        public const string ALL_USER_LOCATIONS = "ALL_USER_LOCATIONS";

        public static string OTP_RESET_PASSWORD = "OTP_RESET_PASS";
        public static string USER_ID_RESET_PASS = "USER_ID_RESET_PASS";
    }
}
