﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PCC1.PMS.Domain.Models
{
    public class BaseSearchModel
    {
        public string SortBy { get; set; }
        public bool SortDesc { get; set; }
        public int PageIndex { get; set; } = 1;
        public int PageSize { get; set; } = 20;
    }
}
