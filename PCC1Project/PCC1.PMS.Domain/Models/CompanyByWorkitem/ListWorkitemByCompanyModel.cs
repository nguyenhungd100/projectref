﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PCC1.PMS.Domain.Models.WorkitemByCompany
{
    public class ListWorkitemByCompanyModel
    {
        public string CompanyName { get; set; }

        public List<WorkitemByCompanyModel> Workitems { get; set; } = new List<WorkitemByCompanyModel>();
    }
}
