﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PCC1.PMS.Domain.Models.WorkitemByCompany
{
    public class WorkitemByCompanyModel
    {        
        public int WorkitemId { get; set; }

        public int CompanyId { get; set; }

        public string WorkitemName { get; set; }

        public decimal Quantity { get; set; }

        public string CalculationUnit { get; set; }

        public decimal? CompletedNumber { get; set; }

        public decimal AssignmentNumber { get; set; }

        public decimal CompletedNumberTotal { get; set; }
    }
}
