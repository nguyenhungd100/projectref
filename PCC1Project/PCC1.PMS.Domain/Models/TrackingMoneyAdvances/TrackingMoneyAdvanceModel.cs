﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace PCC1.PMS.Domain.Models.TrackingMoneyAdvances
{
    public class TrackingMoneyAdvanceModel
    {
        public int? Id { get; set; }

        public string HashId { get; set; }

        [JsonIgnore]
        public int ProjectId { get; set; }

        public string ProjectName { get; set; }

        public int CompanyId { get; set; }

        public string CompanyName { get; set; }

        public DateTime ContractSigningDate { get; set; }

        public int Duration { get; set; }

        [JsonIgnore]
        public DateTime? ExpirationDate { get; set; }

        public decimal MoneyAdvance { get; set; }

        public bool? IsPaid { get; set; }

        [JsonIgnore]
        public int CreatedBy { get; set; }
        [JsonIgnore]
        public DateTime CreatedDate { get; set; }
    }
}
