﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace PCC1.PMS.Domain.Models.TrackingMoneyAdvances
{
    public class SaveTrackingMoneyAdvanceModel
    {
        [JsonIgnore]
        public int? ProjectId { get; set; }

        public string HashId { get; set; }

        [Required(ErrorMessage = "Yêu cầu chọn công ty để tạm ứng")]
        public int? CompanyId { get; set; }

        [Required(ErrorMessage = "Yêu cầu nhập ngày kí hợp đồng")]
        public DateTime? ContractSigningDate { get; set; }

        [Required(ErrorMessage = "Yêu cầu nhập khoảng thời gian tạm ứng")]
        public int? Duration { get; set; }

        [Required(ErrorMessage ="Số tiền cần tạm ứng không được để trống")]
        public decimal? MoneyAdvance { get; set; }

        public bool? IsPaid { get; set; }

        [JsonIgnore]
        public int CreatedBy { get; set; }
    }
}
