﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace PCC1.PMS.Domain.Models.TrackingMoneyAdvances
{
    public class DeleteTrackingMoneyAdvanceModel
    {
        public string HashId { get; set; }

        [JsonIgnore]
        public int ProjectId { get; set; }

        public int CompanyId { get; set; }

        public int Times { get; set; }
    }
}
