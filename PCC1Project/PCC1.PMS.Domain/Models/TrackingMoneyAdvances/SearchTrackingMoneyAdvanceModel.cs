﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace PCC1.PMS.Domain.Models.TrackingMoneyAdvances
{
    public class SearchTrackingMoneyAdvanceModel : BaseSearchModel
    {
        [JsonIgnore]
        public int? ProjectId { get; set; }

        public string HashId { get; set; }

        public int? CompanyId { get; set; }

        public int TotalRecord { get; set; }
    }
}
