﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace PCC1.PMS.Domain.Models.ProjectUsers
{
    public class RemoveProjectMembersModel
    {
        [JsonIgnore]
        public int ProjectId { get; set; }
        /// <summary>
        /// Mảng user ids xóa khỏi project
        /// </summary>
        [Required]
        public int UserId { get; set; }
        [JsonIgnore]
        public int ExecutingUserId { get; set; }

        public string ProjectRoles { get; set; }
    }
}
