﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace PCC1.PMS.Domain.Models.ProjectUsers
{
    public class ProjectUserModel
    {
        [Required]
        public int Id { set; get; }

        [Required]
        public int UserId { set; get; }

        [Required]
        public int ProjectId { set; get; }

        [Required]
        public int DisplayOrder { set; get; }
    }
}
