﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace PCC1.PMS.Domain.Models.ProjectUsers
{
    public class AddProjectMembersModel
    {
        [JsonIgnore]
        public int ProjectId { get; set; }
        [JsonIgnore]
        public int CurrentUserId { get; set; }

        [Required(ErrorMessage = "User không được để trống")]
        public int UserId { set; get; }

        [Required(ErrorMessage ="Thứ tự hiển thị không được để trống")]        
        public int DisplayOrder { get; set; }

        public string ProjectRoles { get; set; }
    }
}
