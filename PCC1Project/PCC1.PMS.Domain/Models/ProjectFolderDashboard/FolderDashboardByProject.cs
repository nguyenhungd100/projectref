﻿using PCC1.PMS.Domain.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace PCC1.PMS.Domain.Models.ProjectFolderDashboard
{
    public class FolderDashboardByProject
    {
        public object Id { get; set; }

        public string Name { get; set; }

        public object ParentId { get; set; }

        public TypeFolder TypeFolders { get; set; }

        public int ViewLevel { get; set; }

        public string HashId { get; set; }

        public List<FolderDashboardByProject> Childrens { get; set; } = new List<FolderDashboardByProject>();
    }
}
