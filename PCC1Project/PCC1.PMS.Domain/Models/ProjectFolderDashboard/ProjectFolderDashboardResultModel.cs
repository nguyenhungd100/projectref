﻿using PCC1.PMS.Domain.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace PCC1.PMS.Domain.Models.ProjectFolderDashboard
{
    public class ProjectFolderDashboardResultModel
    {
        public List<FolderDashboardByProject> ProjectFolderDashboard { get; set; } = new List<FolderDashboardByProject>();

    }
}
