﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace PCC1.PMS.Domain.Models.Users
{
    public class UserRemoveModel
    {
        [Required(ErrorMessage ="Mã user không được để trống")]
        public int Id { get; set; }
    }
}
