﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PCC1.PMS.Domain.Models.Users
{
    public class VerifyOTPPasswordModel
    {
        public string OTP { get; set; }

        public string Email { get; set; }

        public string NewPassword { get; set; }
    }
}
