﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PCC1.PMS.Domain.Models.Users
{
    public class DepartmentModel
    {
        public int Id { get; set; }

        public string Name { get; set; }
    }
}
