﻿using System;
using System.Collections.Generic;
using System.Text;
using PCC1.PMS.Domain.Entity;
using PCC1.PMS.Domain.Enums;

namespace PCC1.PMS.Domain.Models.Users
{
    public class UserModel
    {
        public int Id { set; get; }
        public string FullName { set; get; }
        public string Email { set; get; }
        public UserStatus Status { set; get; }
        public string Mobile { set; get; }
        public Guid Avatar { set; get; }
        public int? Gender { set; get; }
        public string UserRoles { set; get; }
        public string Permissions { get; set; }

        public int? DisplayOrder { get; set; }

        public int? DepartmentId { get; set; }

        public string DepartmentName { get; set; }

        public string ProjectRoles { get; set; }

        public int? DisplayOrderInProject { get; set; }

        public int? CompanyId { get; set; }

        public string CompanyName { get; set; }

        public string Alias { get; set; }

    }
}
