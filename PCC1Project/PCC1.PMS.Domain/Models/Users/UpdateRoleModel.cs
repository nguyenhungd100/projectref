﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PCC1.PMS.Domain.Models.Users
{
    public class UpdateRoleModel
    {
        public int Id { set; get; }   
        public string UserRoles { set; get; }
    }
}
