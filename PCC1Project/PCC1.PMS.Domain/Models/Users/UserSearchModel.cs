﻿using System;
using System.Collections.Generic;
using System.Text;
using Newtonsoft.Json;
using PCC1.PMS.Domain.Models;

namespace PCC1.PMS.Domain.Models
{
    public class UserSearchModel : BaseSearchModel
    {
        public String UserName { get; set; }


        public int? DepartmentId { get; set; }

        public string Role { get; set; }

        public int? CompanyId { get; set; }

        [JsonIgnore]
        public int UserId { get; set; }

    }
}
