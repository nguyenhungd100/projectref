﻿using System;
using System.Collections.Generic;
using System.Text;
using PCC1.PMS.Domain.Enums;

namespace PCC1.PMS.Domain.Models.Users
{
    public class UpdateTokenModel
    {
        public Platform OS { get; set; }
        public string DeviceId { get; set; }
        public string Token { get; set; }
        public bool IsExpo { get; set; } = true;
    }
}
