﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace PCC1.PMS.Domain.Models.Users
{
    public class CreateUserModel
    {
        [Required]
        public string FullName { get; set; }
        [Required(ErrorMessage = "Password is required")]
        [StringLength(255, ErrorMessage = "Password must be between 8 and 255 characters", MinimumLength = 8)]
        [DataType(DataType.Password)]
        public string Password { get; set; }
        [Required]
        public string Email { get; set; }
        [Required]
        public string Phone { get; set; }
        public string Roles { get; set; }

        public int? DepartmentId { get; set; }

        public int? CompanyId { get; set; }

        public string Alias { get; set; }
    }

    public class UpdateUserModel
    {
        [Required]
        public int UserId { get; set; }
        [Required]
        public string FullName { get; set; }
        [Required]
        public string Email { get; set; }
        [Required]
        public string Phone { get; set; }
        public string Roles { get; set; }
        public bool IsActived { set; get; }

        public int? DepartmentId { get; set; }

        public int? CompanyId { get; set; }

        public string Alias { get; set; }
    }
}
