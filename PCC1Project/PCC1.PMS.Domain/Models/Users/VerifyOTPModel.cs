﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace PCC1.PMS.Domain.Models.Users
{
    public class VerifyOTPModel
    {
        [Required]
        public long UserId { get; set; }
        [Required]
        public string OTP { get; set; }
    }
}
