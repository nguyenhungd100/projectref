﻿using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;
using PCC1.PMS.Domain.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace PCC1.PMS.Domain.Models.Users
{
    public class MediaUserAvartarModel
    {
        public IFormFile Avatar { set; get; }

        public FileStatus Status { set; get; }
        [JsonIgnore]
        public long UserId { get; set; }
    }
}
