﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PCC1.PMS.Domain.Models.Users
{
    public class LocationModel
    {
        public double Lat { get; set; }
        public double Lng { get; set; }
    }
}
