﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace PCC1.PMS.Domain.Models.File
{
    public class RemoveDocumentModel
    {
        public int DocumentId { get; set; }

        [JsonIgnore]
        public int UserId { get; set; }
    }
}
