﻿using PCC1.PMS.Domain.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace PCC1.PMS.Domain.Models.File
{
    public class FileModel
   {
        public Guid Id { set; get; }

        public ProviderFileType ProviderType { set; get; }

        public long StorageId { set; get; }

        public string Mime { set; get; }

        public string Title { set; get; }

        public DateTime CreatedUtcTime { set; get; }

        public int Size { set; get; }

        public FileStatus Status { set; get; }
    }
}
