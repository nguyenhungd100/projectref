﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PCC1.PMS.Domain.Models.File
{
    public class InfoFolderModel
    {
        public string ParentId { get; set; }

        public string FolderName { get; set; }
    }
}
