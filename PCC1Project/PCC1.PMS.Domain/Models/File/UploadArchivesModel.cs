﻿using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace PCC1.PMS.Domain.Models.File
{
    public class UploadArchivesModel
    {
        public string HashId { get; set; }

        public Guid Folder { get; set; }

        public List<IFormFile> Data { set; get; }

    }
}
