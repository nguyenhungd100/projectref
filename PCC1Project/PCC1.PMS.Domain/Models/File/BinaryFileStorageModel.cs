﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PCC1.PMS.Domain.Models.File
{
    public class BinaryFileStorageModel
    {
        public long Id { set; get; }

        public byte[] Data { set; get; }
    }
}
