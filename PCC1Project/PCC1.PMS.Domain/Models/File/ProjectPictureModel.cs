﻿using PCC1.PMS.Domain.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace PCC1.PMS.Domain.Models.MediaStorage
{
    public class ProjectPictureModel
    {
        public int Id { set; get; }

        public int ProjectId { set; get; }     

        public int DisplayOrder { set; get; }

        public int FileId { set; get; }

        public ProjectPictureType PictureType { get; set; }
    }
}
