﻿using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;
using PCC1.PMS.Domain.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace PCC1.PMS.Domain.Models.MediaStorage
{
    public class MediaModel
    {
        [JsonIgnore]
        public int ProjectId { set; get; }
      
        public List<IFormFile> Data { set; get; }

        public FileStatus Status { set; get; }

        public DateTime CreatedUtcTime { set; get; }

        public string Title { set; get; }

        public ProjectPictureType PictureType { get; set; } = 0;

        /// <summary>
        /// Thuộc tính dưới chỉ dành cho thêm document
        /// </summary>
        public int LastModifyBy { set; get; }

        public Guid Folder { set; get; }

        
    }
}
