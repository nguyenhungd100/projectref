﻿using PCC1.PMS.Domain.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace PCC1.PMS.Domain.Models.MediaStorage
{
    public class ProjectDocumentModel
    {
        public int Id { set; get; }

        public long? ProjectId { set; get; }

        public string DocumentName { set; get; }

        public DateTime LastModified { set; get; }

        public int LastModifyBy { set; get; }

        public int FileId { set; get; }

        public Guid Folder { set; get; }

    }
}
