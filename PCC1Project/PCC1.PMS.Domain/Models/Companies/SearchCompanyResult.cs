﻿using PCC1.PMS.Domain.Models;
using PCC1.PMS.Domain.Models.Companies;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PCC1.PMS.Api.Modules.Companies
{
    public class SearchCompanyResult : BaseSearchResult<CompanyModel>
    {
    }
}
