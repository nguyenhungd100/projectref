﻿using PCC1.PMS.Domain.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace PCC1.PMS.Domain.Models.Companies
{

    public class CompanyModel
    {
        public int? Id { set; get; }

        public string Name { set; get; }
        
        public string FullName { get; set; }

        [Required(ErrorMessage ="Loại công ty không được để trống")]
        [EnumDataType(typeof(CompanyType), ErrorMessage = "Đầu vào loại công ty không thuộc loại công ty đã định nghĩa")]
        public CompanyType Type { get; set; }

        public string Address { get; set; }

        public string Mobile { get; set; }
    }
}
