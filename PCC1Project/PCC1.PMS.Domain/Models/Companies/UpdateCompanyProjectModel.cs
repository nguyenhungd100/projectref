﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace PCC1.PMS.Domain.Models.Companies
{
    public class UpdateCompanyProjectModel
    {
        [JsonIgnore]
        public int ProjectId { get; set; }

        [Required(ErrorMessage ="Yêu cầu nhập danh sách mã công ty")]
        public List<int> CompanyIds { get; set; }
    }
}
