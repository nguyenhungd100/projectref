﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace PCC1.PMS.Domain.Models.Companies
{
    public class DeleteCompanyModel
    {
        [Required(ErrorMessage ="Yêu cầu chọn công ty để xóa")]
        public int id { get; set; }
    }
}
