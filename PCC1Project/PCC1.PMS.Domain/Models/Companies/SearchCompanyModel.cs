﻿using Newtonsoft.Json;
using PCC1.PMS.Domain.Enums;
using PCC1.PMS.Domain.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PCC1.PMS.Api.Modules.Companies
{

    public class SearchCompanyModel : BaseSearchModel
    {
        public string HashId { get; set; }

        [JsonIgnore]
        public int? ProjectId { get; set; }

        public CompanyType? Type { get; set; }

        public string Name { get; set; }

        public int TotalRecord { get; set; }
    }
}
