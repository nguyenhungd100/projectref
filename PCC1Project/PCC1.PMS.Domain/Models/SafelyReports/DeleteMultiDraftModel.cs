﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace PCC1.PMS.Domain.Models.SafelyReports
{
    public class DeleteMultiDraftModel
    {
        [JsonIgnore]
        public int UserId { get; set; }

        public int[] SafelyDraftId { get; set; }
    }
}
