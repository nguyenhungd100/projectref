﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace PCC1.PMS.Domain.Models.ProjectReports
{
    public class SearchSafelyReportModel : BaseSearchModel
    {
        public int? CreatedBy { get; set; }

        public string ProjectHashId { get; set; }

        public DateTime? FromDate { get; set; }

        public DateTime? ToDate { get; set; }

        [JsonIgnore]
        public int? ProjectId { get; set; }

        public int? CompanyId { get; set; }

        [JsonIgnore]
        public int TotalRecord { get; set; }
    }
}
