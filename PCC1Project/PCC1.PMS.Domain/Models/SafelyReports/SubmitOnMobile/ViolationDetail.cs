﻿using PCC1.PMS.Domain.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace PCC1.PMS.Domain.Models.SafelyReports.SubmitOnMobile
{
    public class ViolationDetail
    {
        public ViolationType Type { get; set; }

        public int Quantity { get; set; }
    }
}
