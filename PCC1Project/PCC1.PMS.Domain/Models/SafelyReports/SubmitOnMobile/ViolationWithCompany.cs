﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PCC1.PMS.Domain.Models.SafelyReports.SubmitOnMobile
{
    public class ViolationWithCompany
    {
        public int CompanyId { get; set; }

        public List<ViolationDetail> ViolationDetails { get; set; } = new List<ViolationDetail>();
    }
}
