﻿using PCC1.PMS.Domain.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace PCC1.PMS.Domain.Models.SafelyReports
{
    public class SafelyViolationTypeStatistical
    {
        public ViolationType Type { get; set; }
        
        public string ViolationTypeName { get; set; }

        public List<StatisticViolationCompany> Companies { get; set; } = new List<StatisticViolationCompany>();
    }
}
