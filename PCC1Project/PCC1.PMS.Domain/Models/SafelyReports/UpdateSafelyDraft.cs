﻿using Newtonsoft.Json;
using PCC1.PMS.Domain.Models.ProjectReports;
using System;
using System.Collections.Generic;
using System.Text;

namespace PCC1.PMS.Domain.Models.SafelyReports
{
    public class UpdateSafelyDraft
    {
        public int? Id { get; set; }

        [JsonIgnore]
        public int ProjectId { get; set; }

        public string ProjectHashId { get; set; }

        public SafelyReportImageModel ImageInfo { get; set; }
      
        [JsonIgnore]
        public int CreatedBy { get; set; }

        [JsonIgnore]
        public DateTime CreatedDate { set; get; }
    }
}
