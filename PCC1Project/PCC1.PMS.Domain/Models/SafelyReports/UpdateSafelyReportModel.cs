﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace PCC1.PMS.Domain.Models.ProjectReports
{
    public class UpdateSafelyReportModel
    {
        public int? Id { get; set; }

        [JsonIgnore]
        public int ProjectId { get; set; }

        public List<SafelyReportImageModel> ImageInfo { get; set; } = new List<SafelyReportImageModel>();

        public string DisadvantagesFixingRequirement { get; set; }

        public string Requests { get; set; }

        [JsonIgnore]
        public int CreatedBy { get; set; }

        [JsonIgnore]
        public DateTime CreatedDate { set; get; }

    }
}
