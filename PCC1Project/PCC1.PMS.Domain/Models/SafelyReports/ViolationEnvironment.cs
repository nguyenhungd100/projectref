﻿using PCC1.PMS.Domain.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace PCC1.PMS.Domain.Models.SafelyReports
{
    public class ViolationEnvironment
    {
        public ViolationType Code { get; set; }

        public string Title { get; set; }

        public int ViolationNumber { get; set; }

    }
}
