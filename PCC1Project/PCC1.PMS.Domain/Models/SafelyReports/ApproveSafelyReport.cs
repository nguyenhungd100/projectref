﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace PCC1.PMS.Domain.Models.SafelyReports
{
    public class ApproveSafelyReport
    {
        [Required(ErrorMessage ="Yêu cầu chọn báo cáo")]
        public int? ReportId { get; set; }

        [JsonIgnore]
        public int ApproveUser { get; set; }
    }

    public class RejectSafelyReport
    {
        [Required(ErrorMessage = "Yêu cầu chọn báo cáo")]
        public int? ReportId { get; set; }

        public string Reason { get; set; }

        [JsonIgnore]
        public int RejectUser { get; set; }
    }

    public class SubmitSafelyReport
    {
        [Required(ErrorMessage = "Yêu cầu chọn báo cáo")]
        public int? ReportId { get; set; }


        [JsonIgnore]
        public int SubmitUser { get; set; }
    }
}
