﻿using Newtonsoft.Json;
using PCC1.PMS.Domain.Enums;
using PCC1.PMS.Domain.Models.ProjectReports;
using System;
using System.Collections.Generic;
using System.Text;

namespace PCC1.PMS.Domain.Models.SafelyReports
{
    public class SafelyReportInfoModel
    {
        public int Id { get; set; }

        public string UnitBuilds { get; set; }

        [JsonIgnore]
        public int ProjectId { get; set; }

        public string ProjectHashId { get; set; }

        public string ProjectName { get; set; }

        public string DisadvantagesFixingRequirement { get; set; }

        public string Requests { get; set; }

        public DateTime CreatedDate { get; set; }

        [JsonIgnore]
        public int CreatedBy { get; set; }

        public string CreatedUserName { get; set; }

        public List<SafelyReportImageModel> ImageInfo { get; set; } = new List<SafelyReportImageModel>();

        public string Reason { get; set; }

        public SafelyReportStatus? Status { get; set; }
    }
}
