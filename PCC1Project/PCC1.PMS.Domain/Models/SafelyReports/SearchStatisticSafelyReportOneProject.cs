﻿using Newtonsoft.Json;
using PCC1.PMS.Domain.Models.Statistical;
using System;
using System.Collections.Generic;
using System.Text;

namespace PCC1.PMS.Domain.Models.SafelyReports
{
    public class SearchStatisticSafelyReportOneProject : CheckStatisticalWithTime
    {
        public string HashId { get; set; }

        [JsonIgnore]
        public int ProjectId { get; set; }
      
    }
}
