﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace PCC1.PMS.Domain.Models.SafelyReports
{
    public class ViolationDefinedModel
    {
        public List<ViolationHuman> ViolationHumans { get; set; } = new List<ViolationHuman>();

        public List<ViolationEnvironment> ViolationEnvironments { get; set; } = new List<ViolationEnvironment>();
    }
}
