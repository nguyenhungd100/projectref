﻿using Newtonsoft.Json;
using PCC1.PMS.Domain.Models.ProjectReports;
using System;
using System.Collections.Generic;
using System.Text;

namespace PCC1.PMS.Domain.Models.SafelyReports
{
    public class SafelyDraftModel
    {
        public int Id { get; set; }

        [JsonIgnore]
        public int ProjectId { get; set; }

        public string ProjectHashId { get; set; }

        public string ProjectName { get; set; }

        public string Note { get; set; }

        public string ErrorMessage { get; set; }

        [JsonIgnore]
        public int CreatedBy { get; set; }

        public string CreatedUserName { get; set; }

        public SafelyReportImageModel ImageInfo { get; set; } = new SafelyReportImageModel();

    }
}
