﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PCC1.PMS.Domain.Models.SafelyReports
{
    public class StatisticViolationCompany
    {
        public int CompanyId { get; set; }

        public string CompanyName { get; set; }

        public int ViolationNumber { get; set; } = 0;
    }
}
