﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PCC1.PMS.Domain.Models.ProjectReports
{
    public class SafelyReportImageResult : ImageCoordinates
    {
        public string FileId { get; set; }

        public string Note { get; set; }

    }
}
