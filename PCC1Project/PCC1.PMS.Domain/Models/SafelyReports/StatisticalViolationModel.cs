﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PCC1.PMS.Domain.Models.SafelyReports
{
    public class StatisticalViolationModel
    {
        public List<SafelyViolationTypeStatistical> SafelyViolations { get; set; } = new List<SafelyViolationTypeStatistical>();

        public List<CompanyViolationTotalInfo> CompanyViolationTotals { get; set; } = new List<CompanyViolationTotalInfo>();
    }
}
