﻿using Newtonsoft.Json;
using PCC1.PMS.Domain.Models.SafelyReports;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace PCC1.PMS.Domain.Models.ProjectReports
{
    public class SafelyReportImageModel
    {
        public int? ImageInfoId { get; set; }

        public List<string> Images { get; set; }

        [Required(ErrorMessage = "Mã công ty không được để trống")]
        public int CompanyId { get; set; }

        public string CompanyName { get; set; }

        public ImageCoordinates Coordinates { get; set; }

        public ViolationDefinedModel ViolationDefined { get; set; }

        public string Note { get; set; }

        public DateTime DayPhotographed { get; set; }

        

    }
}
