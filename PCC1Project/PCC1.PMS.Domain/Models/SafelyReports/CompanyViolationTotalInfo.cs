﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PCC1.PMS.Domain.Models.SafelyReports
{
    public class CompanyViolationTotalInfo
    {
        public int CompanyId { get; set; }

        public int ViolationTotal { get; set; } = 0;

        public string Rating { get; set; }
    }
}
