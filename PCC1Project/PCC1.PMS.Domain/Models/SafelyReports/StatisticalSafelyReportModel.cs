﻿using Newtonsoft.Json;
using PCC1.PMS.Domain.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace PCC1.PMS.Domain.Models.ProjectReports
{
    public class StatisticalSafelyReportModel
    {
        public int CompanyId { get; set; }

        public string ProjectHashId { get; set; }

        [JsonIgnore]
        public int? ProjectId { get; set; }

        public DateTime? FromDate { get; set; }

        public DateTime? ToDate { get; set; }

        public int? Month { get; set; }

        public int? Quarter { get; set; }

        public int? Year { get; set; }

    }
}
