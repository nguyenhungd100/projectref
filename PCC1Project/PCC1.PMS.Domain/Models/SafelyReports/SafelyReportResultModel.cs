﻿using Newtonsoft.Json;
using PCC1.PMS.Domain.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace PCC1.PMS.Domain.Models.ProjectReports
{
    public class SafelyReportResultModel
    {
        public int Id { get; set; }

        [JsonIgnore]
        public int ProjectId { get; set; }

        public string ProjectHashId { get; set; }

        public string ProjectName { get; set; }

        public string DisadvantagesFixingRequirement { get; set; }

        public string Requests { get; set; }

        public int CreatedBy { get; set; }

        public string CreatedUserName { get; set; }

        public DateTime CreatedDate { get; set; }

        public string Reason { get; set; }

        public SafelyReportStatus? Status { get; set; }
    }
}
