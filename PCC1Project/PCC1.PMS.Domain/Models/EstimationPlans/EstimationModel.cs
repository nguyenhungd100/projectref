﻿using Newtonsoft.Json;
using PCC1.PMS.Domain.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace PCC1.PMS.Domain.Models.EstimationPlans
{
    public class EstimationModel
    {
        public string Name { get; set; }

        [Required]
        public int WorkitemId { get; set; }

        [Required]
        public decimal Quantity { get; set; }

        public int? Month { get; set; }

        public int? Quarter { get; set; }
       
        [Required]
        public int Year { get; set; }

        [JsonIgnore]
        public EstimationType Type { get; set; }
    }
}
