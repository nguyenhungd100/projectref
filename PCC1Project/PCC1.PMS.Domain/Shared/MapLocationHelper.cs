﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PCC1.PMS.Domain.Shared
{
    public static class MapLocationHelper
    {
        /// <summary>
        /// Validate map format data
        /// </summary>
        /// <param name="mapLocation"></param>
        /// <returns></returns>
        public static bool IsValidMap(string mapLocation)
        {
            dynamic json = JsonConvert.DeserializeObject(mapLocation);
            if (json.lat != null && json.lng != null)
                return true;
            return false;
        }
    }
}
