﻿using OfficeOpenXml;
using OfficeOpenXml.Style;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text;

namespace PCC1.PMS.Domain.Shared
{
    public static class TitleCompanyExcel
    {
        public static void FillTitleTechnology(ExcelWorksheet worksheet, int? maxColumn = null,int ? leftAppend = null, int? rightDepend = null)
        {
            worksheet.Cells[1, 1, 1, leftAppend.Value].Merge = true;
            worksheet.Row(1).Height = 80;
            worksheet.Row(2).Height = 80;
            worksheet.Row(3).Height = 45;

            var row11 = worksheet.Cells[1, 1].RichText.Add("CÔNG TY CỔ PHẦN XÂY LẮP ĐIỆN I" + "\r\n");
            row11.Bold = false;
            row11.Size = 12;

            var row110 = worksheet.Cells[1, 1].RichText.Add("   ");

            var row12 = worksheet.Cells[1, 1].RichText.Add("PHÒNG KỸ THUẬT CÔNG NGHỆ");
            worksheet.Cells[1, 1].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            worksheet.Cells[1, 1].Style.VerticalAlignment = ExcelVerticalAlignment.Center;

            row12.Bold = true;
            row12.Size = 11;
            row12.UnderLine = true;

            worksheet.Cells[1, 1].AutoFitColumns();

            if (maxColumn.HasValue)
            {
                worksheet.Cells[2, 1, 2, maxColumn.Value].Merge = true;

                worksheet.Cells[1, maxColumn.Value - rightDepend.Value, 1, maxColumn.Value].Merge = true;
                var row13 = worksheet.Cells[1, maxColumn.Value - rightDepend.Value, 1, maxColumn.Value].RichText.Add("CỘNG HÒA XÃ HỘI CHỦ NGHĨA VIỆT NAM" + "\r\n");
                row13.Bold = true;
                row13.Size = 13;

                var row14 = worksheet.Cells[1, maxColumn.Value - rightDepend.Value, 1, maxColumn.Value].RichText.Add("Độc lập - Tự do - Hạnh phúc");
                row14.Bold = true;
                row14.Size = 12;

                worksheet.Cells[1, maxColumn.Value - rightDepend.Value, 1, maxColumn.Value].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                worksheet.Cells[1, maxColumn.Value - rightDepend.Value, 1, maxColumn.Value].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                worksheet.Cells[1, maxColumn.Value - rightDepend.Value].AutoFitColumns(worksheet.Column(maxColumn.Value - rightDepend.Value + 1).Width);


                worksheet.Cells[2, 1, 2, maxColumn.Value].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                worksheet.Cells[1, 1, 2, maxColumn.Value].Style.Fill.PatternType = ExcelFillStyle.Solid;
                worksheet.Cells[1, 1, 2, maxColumn.Value].Style.Fill.PatternColor.SetColor(Color.White);
                worksheet.Cells[1, 1, 2, maxColumn.Value].Style.Fill.BackgroundColor.SetColor(Color.White);
            }
            
        }

        public static void FillTitleCommanderWorks(ExcelWorksheet worksheet, string projectName, int? maxColumn = null, int? leftAppend = null, int? rightDepend = null)
        {
            worksheet.Cells[1, 1, 1, leftAppend.Value].Merge = true;

            worksheet.Row(1).Height = 80;
            worksheet.Row(2).Height = 80;
            worksheet.Row(3).Height = 45;

            var row11 = worksheet.Cells[1, 1].RichText.Add("CÔNG TY CỔ PHẦN XÂY LẮP ĐIỆN I" + "\r\n");
            row11.Bold = false;
            row11.Size = 12;

            var row12 = worksheet.Cells[1, 1].RichText.Add("Ban chỉ huy công trình: " + projectName);


            row12.Bold = true;
            row12.Size = 11;
            row12.UnderLine = true;

            worksheet.Cells[1, 1].AutoFitColumns();
            worksheet.Cells[1, 1].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            worksheet.Cells[1, 1].Style.VerticalAlignment = ExcelVerticalAlignment.Center;

            if (maxColumn.HasValue)
            {
                worksheet.Cells[2, 1, 2, maxColumn.Value].Merge = true;


                worksheet.Cells[1, maxColumn.Value - rightDepend.Value, 1, maxColumn.Value].Merge = true;
                var row13 = worksheet.Cells[1, maxColumn.Value - rightDepend.Value, 1, maxColumn.Value].RichText.Add("CỘNG HÒA XÃ HỘI CHỦ NGHĨA VIỆT NAM" + "\r\n");
                row13.Bold = true;
                row13.Size = 13;

                var row14 = worksheet.Cells[1, maxColumn.Value - rightDepend.Value, 1, maxColumn.Value].RichText.Add("Độc lập - Tự do - Hạnh phúc");
                row14.Bold = true;
                row14.Size = 12;

                worksheet.Cells[1, maxColumn.Value - rightDepend.Value, 1, maxColumn.Value].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                worksheet.Cells[1, maxColumn.Value - rightDepend.Value, 1, maxColumn.Value].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                worksheet.Cells[1, maxColumn.Value - rightDepend.Value].AutoFitColumns(worksheet.Column(maxColumn.Value - rightDepend.Value + 1).Width);


                worksheet.Cells[2, 1, 2, maxColumn.Value].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                worksheet.Cells[1, 1, 2, maxColumn.Value].Style.Fill.PatternType = ExcelFillStyle.Solid;
                worksheet.Cells[1, 1, 2, maxColumn.Value].Style.Fill.PatternColor.SetColor(Color.White);
                worksheet.Cells[1, 1, 2, maxColumn.Value].Style.Fill.BackgroundColor.SetColor(Color.White);
            }
        }
    }
}
