﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PCC1.PMS.Domain.Shared
{
    public static class StringResources
    {
        internal static readonly string REQUEST_SENT_TITLE = "Request sent";
        internal static readonly string REQUEST_SENT_CONTENT = "Your request has been sent to the agents";
        internal static readonly string COLLECTING_TRANSACTION_REQUEST_TITLE = "Collecting transaction request";
        internal static readonly string COLLECTING_TRANSACTION_REQUEST_CONTENT = "You have received a collecting transaction request";
        internal static readonly string PAYOUT_TRANSACTION_REQUEST_TITLE = "Payout transaction request";
        internal static readonly string PAYOUT_TRANSACTION_REQUEST_CONTENT = "You have received a payout transaction request";
        internal static readonly string CASH_COLLECTED_TITLE = "Cash has collected";
        internal static readonly string CASH_COLLECTED_CONTENT = "Collecting agent has collected cash";
        internal static readonly string INCOMING_CASH_TITLE = "Incoming cash";
        internal static readonly string INCOMING_CASH_CONTENT = "One person is sending cash to you";
    }
}
