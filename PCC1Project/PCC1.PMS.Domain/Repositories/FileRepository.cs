﻿using MicroOrm.Dapper.Repositories;
using MicroOrm.Dapper.Repositories.SqlGenerator;
using PCC1.PMS.Domain.Entity;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;

namespace PCC1.PMS.Domain.Repositories
{
    public interface IFileRepository : IDapperRepository<File>
    {
    }
    public class FileRepository : DapperRepository<File>, IFileRepository
    {
        private readonly IDbConnection _connection;

        public FileRepository(IDbConnection connection, ISqlGenerator<File> sqlGenerator)
            : base(connection, sqlGenerator)
        {
            _connection = connection;
        }
    }
}
