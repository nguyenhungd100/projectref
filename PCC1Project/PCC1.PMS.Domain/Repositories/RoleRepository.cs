﻿using Dapper;
using MicroOrm.Dapper.Repositories;
using MicroOrm.Dapper.Repositories.SqlGenerator;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using PCC1.PMS.Domain.Entity;

namespace PCC1.PMS.Domain.Repositories
{
    public interface IRoleRepository : IDapperRepository<Role>
    {
        IEnumerable<Role> FindIn(int[] ids);
    }
    public class RoleRepository : DapperRepository<Role>, IRoleRepository
    {
        private readonly IDbConnection _connection;

        public RoleRepository(IDbConnection connection, ISqlGenerator<Role> sqlGenerator)
            : base(connection, sqlGenerator)
        {
            _connection = connection;
        }

        public IEnumerable<Role> FindIn(int[] ids)
        {
            var sql = string.Format("SELECT * FROM {0} WHERE Id in ({1})", SqlGenerator.TableName, string.Join(", ", ids));
            return _connection.Query<Role>(sql);
        }
    }
}
