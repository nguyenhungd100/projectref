﻿using MicroOrm.Dapper.Repositories;
using MicroOrm.Dapper.Repositories.SqlGenerator;
using PCC1.PMS.Domain.Entity;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;

namespace PCC1.PMS.Domain.Repositories
{
    public interface IWorkitemRepository : IDapperRepository<Workitem>
    {

    }
    public class WorkitemRepository : DapperRepository<Workitem>, IWorkitemRepository
    {
        private readonly IDbConnection _connection;

        public WorkitemRepository(IDbConnection connection, ISqlGenerator<Workitem> sqlGenerator)
            : base(connection, sqlGenerator)
        {
            _connection = connection;
        }
    }
}
