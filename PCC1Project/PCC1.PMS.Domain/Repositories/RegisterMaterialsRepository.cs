﻿using Dapper;
using MicroOrm.Dapper.Repositories;
using MicroOrm.Dapper.Repositories.SqlGenerator;
using PCC1.PMS.Domain.Entity;
using PCC1.PMS.Domain.Models.Materials;
using PCC1.PMS.Framework.Extensions;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;

namespace PCC1.PMS.Domain.Repositories
{
    public interface IRegisterMaterialRepository : IDapperRepository<RegisterMaterial>
    {
        IEnumerable<RegisterMaterial> Search(SearchRegisterMaterialModel search, out int totalRecords);
    }
    public class RegisterMaterialsRepository : DapperRepository<RegisterMaterial>, IRegisterMaterialRepository
    {
        private readonly IDbConnection _connection; 
        public RegisterMaterialsRepository(IDbConnection connection, ISqlGenerator<RegisterMaterial> sqlGenerator)
            :base(connection, sqlGenerator)
        {
            _connection = connection;
        }

        public IEnumerable<RegisterMaterial> Search(SearchRegisterMaterialModel search, out int totalRecords)
        {
            var r = new RegisterMaterial();
            var compiler = new SqlKata.Compilers.SqlServerCompiler();
            var query = new SqlKata.Query(SqlGenerator.TableName);
            
            if(search.UserId.HasValue)
            {
                query = query.Where(r.GetMemberName(c => c.Petitioner), search.UserId);
            }
            if (!string.IsNullOrEmpty(search.HashId))
            {
                query = query.Where(r.GetMemberName(c => c.ProjectId), search.ProjectId);
            }
            if (search.CreateDate.HasValue)
            {
                query = query.Where(r.GetMemberName(c => c.CreatedDate), search.CreateDate);
            }

            if (!string.IsNullOrEmpty(search.Text))
            {
                query = query.WhereContains(r.GetMemberName(c => c.Name), search.Text);
            }
          

            if (search.Status.HasValue)
            {
                query = query.Where(r.GetMemberName(c => c.Status), search.Status);
            }

            var queryCount = query.Clone();
            var sqlCount = compiler.Compile(queryCount.Select(r.GetMemberName(t => t.Id)).AsCount());
            var sql = compiler.Compile(query);
            var results = Connection.QueryMultiple(sqlCount.ToString() + ";" + sql.ToString());

            search.TotalRecord = results.ReadFirst<int>();
            totalRecords = search.TotalRecord;
            return results.Read<RegisterMaterial>();
        }
    }
}
