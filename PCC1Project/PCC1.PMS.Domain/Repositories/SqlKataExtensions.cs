﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using PCC1.PMS.Framework.Extensions;

namespace PCC1.PMS.Domain.Repositories
{
    public static class SqlKataExtensions
    {
        public static SqlKata.Query Where<T>(this SqlKata.Query query, Expression<Func<T, object>> expression, object value) where T : new()
        {
            var t = new T();
            return query.Where(t.GetMemberName(expression), value);
        }
    }
}
