﻿using Dapper;
using MicroOrm.Dapper.Repositories;
using MicroOrm.Dapper.Repositories.SqlGenerator;
using PCC1.PMS.Domain.Entity;
using PCC1.PMS.Domain.Models.Projects;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq.Expressions;
using PCC1.PMS.Framework.Extensions;

namespace PCC1.PMS.Domain.Repositories
{
    public interface IProjectReportRepository : IDapperRepository<ProjectReport>
    {
        IEnumerable<ProjectReport> Search(SearchProjectReportsModel searchModel);
    }
    public class ProjectReportRepository : DapperRepository<ProjectReport>, IProjectReportRepository
    {
        private readonly IDbConnection _dbConnection;
        public ProjectReportRepository(IDbConnection dbConnection, ISqlGenerator<ProjectReport>sqlGenerator)
            :base(dbConnection, sqlGenerator)
        {
            _dbConnection = dbConnection;
        }

        public IEnumerable<ProjectReport> Search(SearchProjectReportsModel searchModel)
        {
            var e = new ProjectReport();
            var compiler = new SqlKata.Compilers.SqlServerCompiler();

            var query = new SqlKata.Query(SqlGenerator.TableName);

            if (searchModel.ReportType.HasValue)
            {
                query = query.Where(e.GetMemberName(t => t.ReportType), searchModel.ReportType.Value);
            }
            if (searchModel.Status.HasValue)
            {
                query = query.Where(e.GetMemberName(t => t.Status), searchModel.Status.Value);
            }
            if (searchModel.ProjectId.HasValue)
            {
                query = query.Where(e.GetMemberName(t => t.ProjectId), searchModel.ProjectId.Value);
            }
            if (searchModel.CreatedBy.HasValue)
            {
                query = query.Where(e.GetMemberName(t => t.CreatedBy), searchModel.CreatedBy.Value);
            }
            if (searchModel.FromDate.HasValue)
            {
                query = query.WhereBetween(e.GetMemberName(t => t.CreatedOnUtc), searchModel.FromDate.Value, DateTime.MaxValue);
            }
            if (searchModel.ToDate.HasValue)
            {
                query = query.WhereBetween(e.GetMemberName(t => t.CreatedOnUtc), new DateTime(2000, 1,1), searchModel.ToDate.Value);
            }
            if (!string.IsNullOrEmpty(searchModel.Text))
            {
                query = query.WhereLike(e.GetMemberName(t => t.Note), searchModel.Text);
            }

            var queryCount = query.Clone();
            var sqlCount = compiler.Compile(queryCount.Select(e.GetMemberName(t => t.Id)).AsCount());

            //query = query.OrderByDesc(e.GetMemberName(t => t.Id)).Limit(searchModel.PageSize).Offset((searchModel.PageIndex - 1) * searchModel.PageSize);

            var sql = compiler.Compile(query);
            var results = Connection.QueryMultiple(sqlCount.ToString() + ";" + sql.ToString());

            searchModel.TotalRecord = results.ReadFirst<int>();
            return results.Read<ProjectReport>();
        }
    }
}
