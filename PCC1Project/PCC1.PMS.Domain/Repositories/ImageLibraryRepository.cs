﻿using MicroOrm.Dapper.Repositories;
using MicroOrm.Dapper.Repositories.SqlGenerator;
using PCC1.PMS.Domain.Entity;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;

namespace PCC1.PMS.Domain.Repositories
{
    public interface IImageLibraryRepository : IDapperRepository<ImageLibrary>
    {

    }
    public class ImageLibraryRepository : DapperRepository<ImageLibrary>, IImageLibraryRepository
    {
        private readonly IDbConnection _dbConnection;
        public ImageLibraryRepository(IDbConnection dbConnection, ISqlGenerator<ImageLibrary> sqlGenerator)
            : base(dbConnection, sqlGenerator)
        {
            _dbConnection = dbConnection;
        }
    }
}
