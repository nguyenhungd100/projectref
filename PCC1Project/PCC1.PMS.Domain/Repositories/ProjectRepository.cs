﻿using Dapper;
using MicroOrm.Dapper.Repositories;
using MicroOrm.Dapper.Repositories.SqlGenerator;
using PCC1.PMS.Domain.Entity;
using PCC1.PMS.Domain.Enums;
using PCC1.PMS.Domain.Models.Projects;
using PCC1.PMS.Framework.Extensions;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Threading.Tasks;

namespace PCC1.PMS.Domain.Repositories
{

    public interface IProjectRepository : IDapperRepository<Project>
    {
        IEnumerable<Project> Search(SearchProjectModel model,out int totalRecords);
        IEnumerable<Project> GetByIds(List<int> projectIds);
    }
    public class ProjectRepository : DapperRepository<Project>, IProjectRepository
    {
        private readonly IDbConnection _connection;

        public ProjectRepository(IDbConnection connection, ISqlGenerator<Project> sqlGenerator)
            : base(connection, sqlGenerator)
        {
            _connection = connection;
        }

        public IEnumerable<Project> GetByIds(List<int> projectIds)
        {
            var e = new Project();
            var compiler = new SqlKata.Compilers.SqlServerCompiler();

            var query = new SqlKata.Query(SqlGenerator.TableName);
            query = query.WhereIn(e.GetMemberName(t => t.Id), projectIds);

            query = query.OrderByDesc(e.GetMemberName(t => t.Id));

            var sql = compiler.Compile(query);
            return Connection.Query<Project>(sql.ToString());
        }

        public IEnumerable<Project> Search(SearchProjectModel model, out int totalRecords)
        {
            var e = new Project();
            var compiler = new SqlKata.Compilers.SqlServerCompiler();

            var query = new SqlKata.Query(SqlGenerator.TableName);

            if (!String.IsNullOrEmpty(model.Name))
            {              
                query = query.OrWhereContains(e.GetMemberName(t => t.Name), "'+N'" + model.Name + "'+'", true);
            }

            if (model.Type != null && model.Type != 0)
            {
                query = query.Where(e.GetMemberName(t => t.ContractType), model.Type);
            }

            if (!string.IsNullOrEmpty(model.Invester))
            {
                query = query.Where(e.GetMemberName(t => t.Investor), model.Invester);
            }

            if (!string.IsNullOrEmpty(model.SupervisingConsultant))
            {
                query = query.Where(e.GetMemberName(t => t.SupervisingConsultant), model.SupervisingConsultant);
            }

            if (!string.IsNullOrEmpty(model.DesignConsultant))
            {
                query = query.Where(e.GetMemberName(t => t.DesignConsultant), model.DesignConsultant);
            }

            //if (!string.IsNullOrEmpty(model.Subcontractor))
            //{
            //    filter = filter.And(t => t.Subcontractor == model.Subcontractor);
            //}

            //if (model.ConstructionLevel.HasValue)
            //{
            //    filter = filter.And(t => t.ConstructionLevel == model.ConstructionLevel);
            //}

            //if (!string.IsNullOrEmpty(model.Location))
            //{
            //    filter = filter.And(t => t.Location == model.Location);
            //}

            //if (!string.IsNullOrEmpty(model.Scale))
            //{
            //    filter = filter.And(t => t.Scale == model.Scale);
            //}

            //if (model.ProjectGroup != null && model.ProjectGroup != 0)
            //{
            //    filter = filter.And(t => t.ProjectGroup == model.ProjectGroup);
            //}

            //if (model.DateStart != null || model.DateEnd != null)
            //{
            //    if (model.DateStart != null && model.DateEnd != null)
            //    {
            //        filter = filter.And(t => (t.DateStart >= model.DateStart
            //        && t.DateEnd <= model.DateEnd));
            //    }

            //    if (model.DateStart != null && model.DateEnd == null)
            //    {
            //        filter = filter.And(t => t.DateStart >= model.DateStart);
            //    }

            //    if (model.DateStart == null && model.DateEnd != null)
            //    {
            //        filter = filter.And(t => t.DateEnd <= model.DateEnd);
            //    }

            //}

            if (model.Status != 0 && model.Status !=null)
            {
                query = query.Where(e.GetMemberName(t => t.Status), model.Status);
            }
            if (model.ProgressStatus != 0 && model.ProgressStatus != null)
            {
                query = query.Where(e.GetMemberName(t => t.ProgressStatus), model.ProgressStatus);
                query = query.Where(e.GetMemberName(a => a.Status), ProjectStatus.Approved);

            }
            query = query.WhereNotLike(e.GetMemberName(t => t.Status),((int)ProjectStatus.Stop).ToString(), false);
            var queryCount = query.Clone();
            var sqlCount = compiler.Compile(queryCount.Select(e.GetMemberName(t => t.Id)).AsCount());

            //query = query.OrderByDesc(e.GetMemberName(t => t.Id)).Limit(model.PageSize).Offset((model.PageIndex - 1) * model.PageSize);

            var sql = compiler.Compile(query);
            var results = Connection.QueryMultiple(sqlCount.ToString() + ";" + sql.ToString());

            model.TotalRecord = results.ReadFirst<int>();
            totalRecords = model.TotalRecord;
            return results.Read<Project>();
        }
    }
}
