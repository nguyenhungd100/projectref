﻿using MicroOrm.Dapper.Repositories;
using MicroOrm.Dapper.Repositories.SqlGenerator;
using PCC1.PMS.Domain.Entity;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;

namespace PCC1.PMS.Domain.Repositories
{
    public interface IProjectUserRepository : IDapperRepository<ProjectUser>
    {

    }
    public class ProjectUserRepository : DapperRepository<ProjectUser>, IProjectUserRepository
    {
        private readonly IDbConnection _connection;

        public ProjectUserRepository(IDbConnection connection, ISqlGenerator<ProjectUser> sqlGenerator)
            :base (connection, sqlGenerator)
        {
            _connection = connection;
        }

    }
}
