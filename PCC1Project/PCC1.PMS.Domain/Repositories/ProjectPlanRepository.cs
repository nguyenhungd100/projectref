﻿using MicroOrm.Dapper.Repositories;
using MicroOrm.Dapper.Repositories.SqlGenerator;
using PCC1.PMS.Domain.Entity;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;

namespace PCC1.PMS.Domain.Repositories
{
    public interface IProjectPlanRepository : IDapperRepository<ProjectPlan>
    {
    }
    public class ProjectPlanRepository : DapperRepository<ProjectPlan>, IProjectPlanRepository
    {
        private readonly IDbConnection _dbConnection;
        public ProjectPlanRepository(IDbConnection dbConnection, ISqlGenerator<ProjectPlan> sqlGenerator)
            :base(dbConnection, sqlGenerator)
        {
            _dbConnection = dbConnection;
        }
    }
}
