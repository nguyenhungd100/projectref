﻿using Dapper;
using MicroOrm.Dapper.Repositories;
using MicroOrm.Dapper.Repositories.SqlGenerator;
using PCC1.PMS.Domain.Entity;
using PCC1.PMS.Domain.Models.ProjectReports;
using PCC1.PMS.Framework.Extensions;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;

namespace PCC1.PMS.Domain.Repositories
{
    public interface ISafelyReportRepository : IDapperRepository<SafelyReport>
    {
        IEnumerable<SafelyReport> Search(SearchSafelyReportModel search);
    }
    public class SafelyReportRepository : DapperRepository<SafelyReport>, ISafelyReportRepository
    {
        private readonly IDbConnection _connection;

        public SafelyReportRepository(IDbConnection connection, ISqlGenerator<SafelyReport> sqlGenerator)
            : base(connection, sqlGenerator)
        {
            _connection = connection;
        }

        public IEnumerable<SafelyReport> Search(SearchSafelyReportModel search)
        {
            var s = new SafelyReport();
            var compiler = new SqlKata.Compilers.SqlServerCompiler();
            var query = new SqlKata.Query(SqlGenerator.TableName);

            if (search.ProjectId.HasValue)
            {
                query = query.Where(s.GetMemberName(t => t.ProjectId), search.ProjectId.Value);
            }

            if (search.CompanyId.HasValue)
            {
                query = query.Where(s.GetMemberName(t => t.ProjectId), search.ProjectId.Value);
            }

                if (search.CreatedBy.HasValue)
            {
                query = query.Where(s.GetMemberName(t => t.CreatedBy), search.CreatedBy.Value);
            }

            if (search.FromDate.HasValue)
            {
                query = query.WhereBetween(s.GetMemberName(t => t.CreatedDate), search.FromDate.Value, DateTime.MaxValue);
            }

            if (search.ToDate.HasValue)
            {
                query = query.WhereBetween(s.GetMemberName(t => t.CreatedDate), new DateTime(2000, 1, 1), search.ToDate.Value);
            }

            var queryCount = query.Clone();
            var sqlCount = compiler.Compile(queryCount.Select(s.GetMemberName(c => c.Id)).AsCount());

            query = query.OrderByDesc(s.GetMemberName(t => t.Id)).Limit(search.PageSize).Offset((search.PageIndex - 1) * search.PageSize);

            var sql = compiler.Compile(query);
            var results = Connection.QueryMultiple(sqlCount.ToString() + ";" + sql.ToString());

            search.TotalRecord = results.ReadFirst<int>();
            return results.Read<SafelyReport>();
        }
    }
}
