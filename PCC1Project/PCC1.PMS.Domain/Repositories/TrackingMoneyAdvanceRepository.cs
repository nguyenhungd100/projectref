﻿using Dapper;
using MicroOrm.Dapper.Repositories;
using MicroOrm.Dapper.Repositories.SqlGenerator;
using PCC1.PMS.Domain.Entity;
using PCC1.PMS.Domain.Models.TrackingMoneyAdvances;
using PCC1.PMS.Framework.Extensions;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;

namespace PCC1.PMS.Domain.Repositories
{
    public interface ITrackingMoneyAdvanceRepository : IDapperRepository<TrackingMoneyAdvance>
    {
        List<TrackingMoneyAdvance> Search(SearchTrackingMoneyAdvanceModel search, out int totalRecords);
    }

    public class TrackingMoneyAdvanceRepository : DapperRepository<TrackingMoneyAdvance>, ITrackingMoneyAdvanceRepository
    {
        private readonly IDbConnection _dbConnection;
        public TrackingMoneyAdvanceRepository(IDbConnection dbConnection, ISqlGenerator<TrackingMoneyAdvance> sqlGenerator)
            : base(dbConnection, sqlGenerator)
        {
            _dbConnection = dbConnection;
        }

        public List<TrackingMoneyAdvance> Search(SearchTrackingMoneyAdvanceModel search, out int totalRecords)
        {
            var e = new TrackingMoneyAdvance();
            var compiler = new SqlKata.Compilers.SqlServerCompiler();

            var query = new SqlKata.Query(SqlGenerator.TableName);

            if (search.ProjectId.HasValue)
            {
                query = query.Where(e.GetMemberName(t=>t.ProjectId), search.ProjectId);
            }
            if (search.CompanyId.HasValue)
            {
                query = query.Where(e.GetMemberName(t => t.CompanyId), search.CompanyId);
            }

            var queryCount = query.Clone();
            var sqlCount = compiler.Compile(queryCount.Select(e.GetMemberName(t => t.Id)).AsCount());


            var sql = compiler.Compile(query);
            var results = Connection.QueryMultiple(sqlCount.ToString() + ";" + sql.ToString());

            search.TotalRecord = results.ReadFirst<int>();
            totalRecords = search.TotalRecord;
            return results.Read<TrackingMoneyAdvance>().AsList();
        }
    }
}
