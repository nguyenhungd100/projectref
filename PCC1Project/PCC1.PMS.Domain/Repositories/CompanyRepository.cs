﻿using Dapper;
using MicroOrm.Dapper.Repositories;
using MicroOrm.Dapper.Repositories.SqlGenerator;
using PCC1.PMS.Api.Modules.Companies;
using PCC1.PMS.Domain.Entity;
using PCC1.PMS.Framework.Extensions;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;

namespace PCC1.PMS.Domain.Repositories
{
    public interface ICompanyRepository : IDapperRepository<Company>
    {
        List<Company> Search(SearchCompanyModel search, out int totalRecords);
    }
    public class CompanyRepository : DapperRepository<Company>, ICompanyRepository
    {
        private readonly IDbConnection _dbConnection;
        public CompanyRepository(IDbConnection dbConnection, ISqlGenerator<Company> sqlGenerator)
            : base(dbConnection, sqlGenerator)
        {
            _dbConnection = dbConnection;
        }

        public List<Company> Search(SearchCompanyModel search, out int totalRecords)
        {
            var e = new Company();
            var compiler = new SqlKata.Compilers.SqlServerCompiler();

            var query = new SqlKata.Query(SqlGenerator.TableName);

            if (!string.IsNullOrEmpty(search.Name))
            {
                query = query.OrWhereContains(e.GetMemberName(t => t.Name), "'+N'"+ search.Name + "'+'", true).OrWhereContains(e.GetMemberName(t=>t.FullName), "'+N'" + search.Name + "'+'", true);
            }
        

            if (search.Type.HasValue)
            {
                query = query.Where(e.GetMemberName(t => t.Type), search.Type);
            }
                 
            var queryCount = query.Clone();
            var sqlCount = compiler.Compile(queryCount.Select(e.GetMemberName(t => t.Id)).AsCount());


            var sql = compiler.Compile(query);
            var results = Connection.QueryMultiple(sqlCount.ToString() + ";" + sql.ToString());

            search.TotalRecord = results.ReadFirst<int>();
            totalRecords = search.TotalRecord;
            return results.Read<Company>().AsList();
        }
    }
}
