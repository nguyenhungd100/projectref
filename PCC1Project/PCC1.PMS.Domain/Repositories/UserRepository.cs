﻿using Dapper;
using MicroOrm.Dapper.Repositories;
using MicroOrm.Dapper.Repositories.SqlGenerator;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using PCC1.PMS.Domain.Entity;
using PCC1.PMS.Domain.Models;
using PCC1.PMS.Domain.Models.Users;
using PCC1.PMS.Domain.Services.Users;

namespace PCC1.PMS.Domain.Repositories
{
    public interface IUserRepository : IDapperRepository<User>
    {
        BaseSearchResult<User> Search(UserSearchModel userSearchModel);
        IEnumerable<UserModel> FindInArrayWithMinBalance(long[] ids, decimal amount, int selectTop);

        IEnumerable<UserModel> FindUserName(string userName);
    }
    public class UserRepository : DapperRepository<User>, IUserRepository
    {
        private readonly IDbConnection _connection;

        public UserRepository(IDbConnection connection, ISqlGenerator<User> sqlGenerator)
            : base(connection, sqlGenerator)
        {
            _connection = connection;
        }

        public IEnumerable<UserModel> FindInArrayWithMinBalance(long[] ids, decimal minAmount, int selectTop)
        {
            var sql = string.Format("select top {3} * from {0} where totalbalance >= {1} and id in ({2})", SqlGenerator.TableName, minAmount, string.Join(',', ids), selectTop);
            return _connection.Query<UserModel>(sql);
        }

        public IEnumerable<UserModel> FindUserName(string userName)
        {
            var sql = string.Format("Select * from {0} where Status != 3 and (Email LIKE '%'+'{1}'+'%' Or FullName LIKE '%'+'{1}'+'%' Or Mobile LIKE '%'+'{1}'+'%')", SqlGenerator.TableName, userName);
            return _connection.Query<UserModel>(sql);
        }

        public BaseSearchResult<User> Search(UserSearchModel userSearchModel)
        {
            
            return new BaseSearchResult<User>
            {
                //Records = entries,
                TotalRecord = 0
            };
        }
    }
}
