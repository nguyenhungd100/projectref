﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace PCC1.PMS.Domain.Entity
{
    [Table("T_SafelyImageDetails")]
    public class SafelyImageDetail : BaseEntity
    {
        public int SafelyReportId { get; set; }

        public int SafelyImageId { get; set; }

        public string Files { get; set; }
    }
}
