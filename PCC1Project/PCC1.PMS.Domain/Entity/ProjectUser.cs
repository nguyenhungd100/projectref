﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace PCC1.PMS.Domain.Entity
{
    [Table("T_ProjectUsers")]
    public class ProjectUser : BaseEntity
    {
        public int UserId { set; get; }

        public int ProjectId { set; get; }

        public int DisplayOrder { set; get; }

        public string ProjectRoles { get; set; }
    }
}
