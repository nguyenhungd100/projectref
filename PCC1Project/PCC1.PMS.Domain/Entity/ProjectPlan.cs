﻿using PCC1.PMS.Domain.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace PCC1.PMS.Domain.Entity
{
    [Table("T_ProjectPlans")]
    public class ProjectPlan 
    {
        [Key]
        public Guid Id { get; set; }
        public int ProjectId { set; get; }

        public int No { set; get; }

        public Guid? ParentId { set; get; }

        public string Title { set; get; }

        public int Duration { set; get; }

        public DateTime StartDate { set; get; }

        public DateTime EndDate { set; get; }

        public bool? IsLeaf { set; get; }

        public ProjectPlanStatus Status { set; get; }

        public int? CompletedPercentage { get; set; }

        public bool? DisplayOnMobile { get; set; }

        public int? OrderNumber { get; set; }
    }
}
