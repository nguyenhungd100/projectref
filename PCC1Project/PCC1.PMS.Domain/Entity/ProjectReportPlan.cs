﻿using PCC1.PMS.Domain.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace PCC1.PMS.Domain.Entity
{
    [Table("T_ProjectReportPlan")]
    public class ProjectReportPlan : BaseEntity
    {
        public int ProjectReportId { get; set; }
        public Guid PlanId { get; set; }
        public int CompletedPercentage { get; set; }
    }
}
