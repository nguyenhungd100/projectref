﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace PCC1.PMS.Domain.Entity
{
    [Table("T_TrackingMoneyAdvances")]
    public class TrackingMoneyAdvance : BaseEntity
    {
        public int ProjectId { get; set; }

        public int CompanyId { get; set; }


        public DateTime ContractSigningDate { get; set; }

        public int Duration { get; set; }

        public DateTime? ExpirationDate { get; set; }

        public decimal MoneyAdvance { get; set; }

        public bool? IsPaid { get; set; }

        public int CreatedBy { get; set; }

        public DateTime CreatedDate { get; set; }
    }
}
