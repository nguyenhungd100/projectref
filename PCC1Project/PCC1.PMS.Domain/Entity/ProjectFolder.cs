﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace PCC1.PMS.Domain.Entity
{
    [Table("T_ProjectFolder")]
    public class ProjectFolder
    {
        [Key]
        public Guid Id { get; set; }
        public int ProjectId { set; get; }
        public Guid? ParentId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }

        public DateTime CreatedDate { get; set; }

    }
}
