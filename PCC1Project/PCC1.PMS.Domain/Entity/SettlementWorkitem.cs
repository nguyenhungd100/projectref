﻿using PCC1.PMS.Domain.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace PCC1.PMS.Domain.Entity
{
    [Table("T_SettlementWorkitems")]
    public class SettlementWorkitem : BaseEntity
    {
        public int WorkitemId { get; set; }

        public int? CompanyId { get; set; }

        public int Times { get; set; }

        public decimal SettlementNumber{ get; set; }

        public DateTime SettlementDate { get; set; }

        public int? Quarter { get; set; }

        public int? Year { get; set; }

        public SettlementType Type { get; set; }

    }
}
