﻿using MicroOrm.Dapper.Repositories.Attributes;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace PCC1.PMS.Domain.Entity
{
    [Table("T_BinaryFileStorage")]
    public class BinaryFileStorage : BaseEntity
    {
        public byte[] Data { set; get; }
    }
}
