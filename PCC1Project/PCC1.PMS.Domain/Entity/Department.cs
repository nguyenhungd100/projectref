﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace PCC1.PMS.Domain.Entity
{
    [Table("T_Departments")]
    public class Department : BaseEntity
    {
        public string Name { get; set; }
    }
}
