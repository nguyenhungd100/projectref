﻿using PCC1.PMS.Domain.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace PCC1.PMS.Domain.Entity
{
    [Table("T_SafelyReports")]
    public class SafelyReport : BaseEntity
    {
        public int ProjectId { get; set; }

        public string DisadvantagesFixingRequirement { get; set; }

        public string Requests { get; set; }

        public int CreatedBy { get; set; }

        public DateTime CreatedDate { get; set; }

        public string Reason { get; set; }

        public SafelyReportStatus? Status { get; set; }
    }
}
