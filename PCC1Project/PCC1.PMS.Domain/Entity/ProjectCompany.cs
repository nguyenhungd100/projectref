﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace PCC1.PMS.Domain.Entity
{
    [Table("T_ProjectCompanies")]
    public class ProjectCompany : BaseEntity
    {
        public int ProjectId { get; set; }
        public int CompanyId { get; set; }
    }
}
