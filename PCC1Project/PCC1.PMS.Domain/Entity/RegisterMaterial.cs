﻿using PCC1.PMS.Domain.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace PCC1.PMS.Domain.Entity
{
    [Table("T_RegisterMaterials")]
    public class RegisterMaterial :BaseEntity
    {

        public int ProjectId { get; set; }

        public string Name { set; get; }

        public decimal Quantity { get; set; }

        public DateTime CreatedDate { get; set; }

        public DateTime? ApprovedDate { get; set; }

        public int Petitioner { get; set; }

        public int? Approver { get; set; }

        public DateTime? TimeRequired { get; set; }

        public string Note { get; set; }

        public string ResponseMessage { get; set; }

        public RegisterMaterialStatus Status { get; set; }


    }
}
