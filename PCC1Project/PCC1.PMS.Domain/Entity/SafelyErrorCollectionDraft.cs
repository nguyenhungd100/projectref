﻿using PCC1.PMS.Domain.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace PCC1.PMS.Domain.Entity
{
    [Table("T_SafelyErrorCollectionDrafts")]
    public class SafelyErrorCollectionDraft : BaseEntity
    {
        public int SafelyImageDraftId { get; set; }

        public int? ViolationQuantity { get; set; }

        public ViolationType Type { get; set; }
    }
}
