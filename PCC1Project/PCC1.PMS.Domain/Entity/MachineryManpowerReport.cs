﻿using PCC1.PMS.Domain.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace PCC1.PMS.Domain.Entity
{
    [Table("T_MachineryManpowerReports")]
    public class MachineryManpowerReport : BaseEntity
    {
        public int ReportId { get; set; }

        public int ProjectId { get; set; }

        public int CompanyId { get; set; }

        public MachineryManpowerType Type { get; set; }

        public int? Quantity { get; set; }

        public string Title { get; set; }
    }
}
