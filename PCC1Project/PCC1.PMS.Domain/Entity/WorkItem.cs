﻿using PCC1.PMS.Domain.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace PCC1.PMS.Domain.Entity
{
    [Table("T_WorkItems")]
    public class Workitem : BaseEntity
    {
        public string STT { get; set; }

        public string Name { set; get; }

        public string CalculationUnit { get; set; }

        public decimal? Quantity { set; get; }

        public decimal? UnitPrice { get; set; }

        public decimal? IntoMoney { get; set; }

        public int ProjectId { set; get; }              

        public decimal? TotalPaidQuantity { set; get; }

        public decimal? CompletedNumberTotal { get; set; }

        public int? LevelHeading { get; set; }

        public int? RowNumber { get; set; }
    }
}
