﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace PCC1.PMS.Domain.Entity
{
    [Table("T_LocalFileStorage")]
    public class LocalFileStorage
    {
        public int Id { set; get; }

        public string FolderPath { set; get; }

        public string FileName { set; get; }
    }
}
