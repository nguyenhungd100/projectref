﻿using MicroOrm.Dapper.Repositories.Attributes;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using PCC1.PMS.Domain.Enums;
using PCC1.PMS.Domain.Services.Users;

namespace PCC1.PMS.Domain.Entity
{
    [Table("T_Users")]
    public class User : BaseEntity
    {
        public string FullName { set; get; }
        public string Email { set; get; }
        public string Password { set; get; }
        public UserStatus Status { set; get; }
        public string Mobile { set; get; }
        public Guid Avatar { set; get; }
        public int? Gender { set; get; }
        public string UserRoles { set; get; }

        public int? CompanyId { get; set; }

        public int? DepartmentId { get; set; }

        public string Alias { get; set; }

    }
}
