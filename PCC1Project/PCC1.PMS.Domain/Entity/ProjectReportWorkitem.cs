﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace PCC1.PMS.Domain.Entity
{
    [Table("T_ProjectReportWorkitem")]
    public class ProjectReportWorkitem : BaseEntity
    {
        public int ProjectReportId { get; set; }

        public int WorkitemId { get; set; }

        public int CompanyId { get; set; }

        public decimal CompletedNumber { get; set; }

        public string CompletePlace { get; set; }

        public string UnderPlace { get; set; }

        public string Content { get; set; }

        public string PlaceType { get; set; }
    }
}
