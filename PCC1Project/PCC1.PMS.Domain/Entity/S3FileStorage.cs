﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace PCC1.PMS.Domain.Entity
{
    [Table("T_S3FileStorage")]
    public class S3FileStorage
    {
        public int Id { set; get; }

        public string Bucket { set; get; }

        public string FolderPath { set; get; }

        public string FileName { set; get; }
    }
}
