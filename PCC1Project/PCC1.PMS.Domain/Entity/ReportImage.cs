﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace PCC1.PMS.Domain.Entity
{
    [Table("T_ReportImages")]
    public class ReportImage
    {
        public int ProjectReportId { get; set; }

        public int ReportWorkitemId { get; set; }
        [Key]
        public Guid FileId { get; set; }

        public string Lat { get; set; }

        public string Lng { get; set; }

        public string Message { get; set; }

    }
}
