﻿using MicroOrm.Dapper.Repositories.Attributes;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace PCC1.PMS.Domain.Entity
{
    public class BaseEntity
    {
        [Key, Identity]
        public int Id { get; set; }
    }
}
