﻿using PCC1.PMS.Domain.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace PCC1.PMS.Domain.Entity
{
    [Table("T_ProjectReports")]
    public class ProjectReport: BaseEntity
    {
        public ProjectReportType ReportType { set; get; }

        public int ProjectId { set; get; }

        public int CreatedBy { set; get; }

        public DateTime CreatedOnUtc { set; get; }

        public ProjectReportStatus Status { set; get; }

        public string MorningWeather { get; set; }

        public string AfternoonWeather { get; set; }
        

        public string Note { set; get; }

        public string Reason { get;  set; }

        public string MarkedRead { get; set; }
    }
}
