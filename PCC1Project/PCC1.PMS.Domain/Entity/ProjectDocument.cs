﻿using PCC1.PMS.Domain.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace PCC1.PMS.Domain.Entity
{
    [Table("T_ProjectDocuments")]
    public class ProjectDocument : BaseEntity
    {
        public int? ProjectId { set; get; }

        public string DocumentName { set; get; }

        public DateTime LastModified { set; get; }

        public int LastModifyBy { set; get; }

        public Guid FileId { set; get; }

        public Guid Folder { set; get; }

    }
}
