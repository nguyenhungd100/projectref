﻿using MicroOrm.Dapper.Repositories.Attributes;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace PCC1.PMS.Domain.Entity
{
    [Table("T_Roles")]
    public class Role 
    {
        [Key, Identity]
        public int Id { get; set; }
        public string RoleName { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public string Permissions { get; set; }
        public int? DisplayOrder { get; set; }
    }
}
