﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace PCC1.PMS.Domain.Entity
{
    [Table("T_SafelyDraft")]
    public class SafelyDraft : BaseEntity
    {
        public int ProjectId { get; set; }

        public int CompanyId { get; set; }

        public Guid? FileId { get; set; }

        public string Lat { get; set; }

        public string Lng { get; set; }

        public string Note { get; set; }

        public string ErrorMessage { get; set; }

        public DateTime DayPhotographed { get; set; }

        public int CreatedBy { get; set; }
    }
}
