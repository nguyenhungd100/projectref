﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations.Schema;

namespace PCC1.PMS.Domain.Entity
{
    [Table("T_ImageLibrary")]
    public class ImageLibrary: BaseEntity
    {
        public byte[] Data { get; set; }

        public string  Name { get; set; }

        public string  Mime { get; set; }

        public DateTime CreatedDate { get; set; }
    }
}
