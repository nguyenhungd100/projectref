﻿using PCC1.PMS.Domain.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace PCC1.PMS.Domain.Entity
{
    [Table("T_EstimationPlans")]
    public class EstimationPlan : BaseEntity
    {
        public string Name { get; set; }

        public int WorkitemId { get; set; }

        public decimal Quantity { get; set; }

        public int? Month { get; set; }

        public int? Quarter { get; set; }

        public int Year { get; set; }

        public EstimationType Type { get; set; }
    }
}
