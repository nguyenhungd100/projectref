﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace PCC1.PMS.Domain.Entity
{
    [Table("T_AssignmentErrors")]
    public class AssignmentError : BaseEntity
    {
        public int WorkitemId { get; set; }

        public string Content { get; set; }

        public int AssignmentUser { get; set; }

        public DateTime AssignmentDate { get; set; }
    }
}
