﻿using MicroOrm.Dapper.Repositories.Attributes;
using PCC1.PMS.Domain.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace PCC1.PMS.Domain.Entity
{
    [Table("T_Files")]
    public class File 
    { 
        [Key]
        public Guid Id { set; get; }

        public ProviderFileType ProviderType { set; get; }

        public int StorageId { set; get; }

        public string Mime { set; get; }

        public string Title { set; get; }

        public DateTime CreatedUtcTime { set; get; }

        public int Size { set; get; }

        public FileStatus Status { set; get; }
    }
}
