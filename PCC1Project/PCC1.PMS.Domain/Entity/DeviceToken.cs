﻿using MicroOrm.Dapper.Repositories.Attributes;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using PCC1.PMS.Domain.Enums;

namespace PCC1.PMS.Domain.Entity
{
    [Table("T_DeviceTokens")]
    public class DeviceToken : BaseEntity
    {
        public long UserId { get; set; }
        public string Token { get; set; }
        public Platform OS { get; set; }
        public string DeviceId { get; set; }
        public DateTime LastUpdateUtc { get; set; }
        public bool IsExpoToken { get; set; }
    }
}
