﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace PCC1.PMS.Domain.Entity
{
    [Table("T_SafelyReportImages")]
    public class SafelyReportImage : BaseEntity
    {

        public int SafelyReportId { get; set; }

        public int ProjectId { get; set; }

        public int CompanyId { get; set; }

        public Guid? FileId { get; set; }

        public string Lat { get; set; }

        public string Lng { get; set; }

        public string Note { get; set; }

        public DateTime DayPhotographed { get; set; }

    }
}
