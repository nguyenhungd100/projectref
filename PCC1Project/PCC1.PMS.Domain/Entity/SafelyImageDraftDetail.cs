﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace PCC1.PMS.Domain.Entity
{
    [Table("T_SafelyImageDraftDetails")]
    public class SafelyImageDraftDetail : BaseEntity
    {
        public int SafelyDraftId { get; set; }

        public string Files { get; set; }
    }
}
