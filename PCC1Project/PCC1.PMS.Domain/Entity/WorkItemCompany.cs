﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace PCC1.PMS.Domain.Entity
{
    [Table("T_WorkitemCompanies")]
    public class WorkitemCompany : BaseEntity
    {

        public int CompanyId { get; set; }

        public int WorkItemId { get; set; }

        public decimal AssignedNumber { get; set; }

        public decimal? CompletedNumber { get; set; }

        public string CompletePlace { get; set; }

        public string UnderPlace { get; set; }

        public string LastUpdateContentReport { get; set; }

        public decimal? LastUpdateCompletedNumber { get; set; }
    }
}
