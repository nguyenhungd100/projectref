﻿using PCC1.PMS.Domain.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace PCC1.PMS.Domain.Entity
{
    [Table("T_Companies")]
    public class Company : BaseEntity
    {
        public string Name { set; get; }

        public string FullName { get; set; }

        public CompanyType Type { get; set; }


        public string Address { get; set; }

        public string Mobile { get; set; }



    }
}
