﻿using PCC1.PMS.Domain.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace PCC1.PMS.Domain.Entity
{
    [Table("T_ProjectPictures")]
    public class ProjectPicture :BaseEntity
    {
        public long ProjectId {set;get;}

        public int DisplayOrder { set; get; }

        public Guid FileId { set; get; }

        public ProjectPictureType PictureType { get; set; }
    }
}
