﻿using PCC1.PMS.Domain.Enums;
using PCC1.PMS.Domain.Models.Projects;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace PCC1.PMS.Domain.Entity
{
    [Table("T_Projects")]
    public class Project : BaseEntity
    {
        public string Name { get; set; }

        public ContractType ContractType { set; get; }

        public string Investor { set; get; }

        public string SupervisingConsultant { set; get; }

        public string DesignConsultant { set; get; }

        public string Subcontractor { set; get; }

        public ConstructionLevelProject? ConstructionLevel { set; get; }

        public int? ConstructionType { set; get; }

        public decimal? CapitalSource { set; get; }

        public int? ProjectGroup { set; get; }

        public string Scale { set; get; }

        public DateTime? DateStart { set; get; }

        public DateTime? DateEnd { set; get; }

        public decimal? TotalContractValue { set; get; }

        public string Location { set; get; }

        public ProjectStatus? Status { set; get; }

        public ProgressStatus? ProgressStatus { set; get; }

        public int? LastPlanNo { set; get; }

        public int? CreatedBy { get; set; }
    }
}
