﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PCC1.PMS.Domain.Enums
{
    public enum ProjectStatus
    {
        /// <summary>
        /// Initinalize project
        /// </summary>
        Init = 1,

        Pending = 2,

        Approved = 5,

        Stop = 15,

        Finished = 20
    }

    //public enum ProjectProgressStatus
    //{
    //    /// <summary>
    //    /// Chậm tiến độ
    //    /// </summary>
    //    Late = 1,

    //    /// <summary>
    //    /// Đạt
    //    /// </summary>
    //    Reach = 2,

    //    /// <summary>
    //    /// Vượt tiến độ
    //    /// </summary>
    //    Pass = 3
    //}
}
