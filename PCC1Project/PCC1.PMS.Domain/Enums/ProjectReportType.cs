﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PCC1.PMS.Domain.Enums
{
    public enum ProjectReportType
    {
        /// <summary>
        /// Báo cáo tiến độ
        /// </summary>
        ProgressReport = 1,
        /// <summary>
        /// Báo cáo ATVS lao động
        /// </summary>
        SafelyReport = 2
    }
}
