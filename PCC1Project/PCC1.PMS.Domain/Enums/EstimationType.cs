﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PCC1.PMS.Domain.Enums
{
    public enum EstimationType
    {
        /// <summary>
        /// Ước lượng danh thu dự kiến theo quý
        /// </summary>
        QuarterRevenueEstimation = 5,

        /// <summary>
        /// Ước lượng hoàn thành hạng mục theo tháng
        /// </summary>
        EstimationMonthComplete = 10,

        /// <summary>
        /// Ước lượng hoàn thành hạng mục theo quý
        /// </summary>
        EstimationQuarterComplete = 15
    }
}
