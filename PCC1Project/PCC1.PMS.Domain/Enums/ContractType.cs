﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PCC1.PMS.Domain.Enums
{
    public enum ContractType
    {
        /// <summary>
        /// Hợp đồng công ty
        /// </summary>
        HopDongCongTy = 1,

        /// <summary>
        /// HỢp đồng ủy quyền
        /// </summary>
        HopDongUyQuyen = 2,

        /// <summary>
        /// Hợp đồng công ty thành viên
        /// </summary>
        HợpDongCongTyThanhVien = 3

    }
}
