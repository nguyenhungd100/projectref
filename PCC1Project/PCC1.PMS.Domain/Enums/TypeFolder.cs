﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PCC1.PMS.Domain.Enums
{
    public enum TypeFolder
    {
        /// <summary>
        /// Dự án
        /// </summary>
        Project = 1,

        /// <summary>
        /// Folder
        /// </summary>
        Folder = 5,

        /// <summary>
        /// Tài liệu
        /// </summary>
        Document=10
    }
}
