﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PCC1.PMS.Domain.Enums
{
    public enum StatusPlan
    {
        /// <summary>
        /// Nhanh hơn tiến độ
        /// </summary>
         Fast = 1,

         /// <summary>
         /// Đúng tiến độ
         /// </summary>
         Normal=2,
         
         /// <summary>
         /// Chậm tiến độ
         /// </summary>
        Slowly= 3,

    }
}
