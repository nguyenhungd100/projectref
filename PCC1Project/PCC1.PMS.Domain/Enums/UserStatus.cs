﻿using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PCC1.PMS.Domain.Enums
{
    /// <summary>
    /// User status
    /// </summary>
    public enum UserStatus
    {
        /// <summary>
        /// Account is not active
        /// </summary>
        NotActived = 1,

        /// <summary>
        /// Account is actived
        /// </summary>
        Actived = 2,

        /// <summary>
        /// Account is disabled by admin
        /// </summary>
        Disabled = 3
    }
}
