﻿using PCC1.PMS.Domain.Services.Projects;
using System;
using System.Collections.Generic;
using System.Text;

namespace PCC1.PMS.Domain.Enums
{
    public enum MachineryManpowerGroup
    {
        Manpower = 1,
        Machine = 5,
        MorePartManpower = 10,
        MorePartMachine = 15,
    }

    public enum MachineryManpowerType
    {
        [MachineryManpowerDetailAttribute(MachineryManpowerGroup.Manpower, "Nhân lực thi công phần điện")]
        HumanForConstructionOfElectricity = 1,

        [MachineryManpowerDetailAttribute(MachineryManpowerGroup.Manpower, "Nhân lực thi công phần xây dựng")]
        ManpowerForContruction = 5,

        [MachineryManpowerDetailAttribute(MachineryManpowerGroup.Manpower, "Tổ trưởng")]
        TeamLead = 10,

        [MachineryManpowerDetailAttribute(MachineryManpowerGroup.Manpower, "Kỹ thuật")]
        Technicians = 15,

        [MachineryManpowerDetailAttribute(MachineryManpowerGroup.MorePartManpower, "Thêm nhân sự")]
        AddMorePartManpower = 20,

        [MachineryManpowerDetailAttribute(MachineryManpowerGroup.Machine, "Máy đào đất")]
        Excavator = 105,

        [MachineryManpowerDetailAttribute(MachineryManpowerGroup.Machine, "Máy bơm nước")]
        ForcePump = 110,

        [MachineryManpowerDetailAttribute(MachineryManpowerGroup.Machine, "Máy đầm dùi")]
        VibratorCylinder = 115,

        [MachineryManpowerDetailAttribute(MachineryManpowerGroup.MorePartMachine, "Thêm máy móc")]
        AddMorePartMachine = 120

    }
}
