﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PCC1.PMS.Domain.Enums
{
    public enum CompanyType
    {
        /// <summary>
        /// Công ty thành viên
        /// </summary>
        MemberCompany = 1,

        /// <summary>
        /// Đơn vị ngoài
        /// </summary>
        ExternalCompany = 5,
    }
}
