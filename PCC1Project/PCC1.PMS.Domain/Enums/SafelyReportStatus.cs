﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PCC1.PMS.Domain.Enums
{
    public enum SafelyReportStatus
    {
        Draft = 1,
        Submit = 5,
        Approve = 10,
        Reject = 15
    }
}
