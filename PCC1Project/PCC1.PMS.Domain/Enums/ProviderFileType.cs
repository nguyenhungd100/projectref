﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PCC1.PMS.Domain.Enums
{
    public enum ProviderFileType
    {
        LocalStorage =1,

        BinaryStorage = 2,

        S3Storage = 3
    }
}
