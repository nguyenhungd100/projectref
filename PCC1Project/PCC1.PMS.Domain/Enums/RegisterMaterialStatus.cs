﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PCC1.PMS.Domain.Enums
{
    public enum RegisterMaterialStatus
    {
        /// <summary>
        /// Chưa được phê duyệt
        /// </summary>
        NotApproved = 1,

        /// <summary>
        /// Đã được phê duyệt
        /// </summary>
        Approved = 5,

        /// <summary>
        /// Từ chối
        /// </summary>
        Reject = 10
    }
}
