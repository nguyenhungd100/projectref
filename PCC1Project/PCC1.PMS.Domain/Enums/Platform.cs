﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PCC1.PMS.Domain.Enums
{
    public enum Platform : short
    {
        Android = 1,
        iOS = 2
    }
}
