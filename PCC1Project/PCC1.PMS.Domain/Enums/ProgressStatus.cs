﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PCC1.PMS.Domain.Models.Projects
{
    public enum ProgressStatus
    {
        /// <summary>
        /// Chưa có kế hoạch
        /// </summary>
        NotPlan = -1,
        
        /// <summary>
        /// Chưa được triển khai
        /// </summary>
        NotImplemented = 0,

        /// <summary>
        /// Chậm tiến độ
        /// </summary>
        SlowProgress = 1,

        /// <summary>
        /// Đúng tiến độ
        /// </summary>
        OnSchedule = 2,

        /// <summary>
        /// Nhanh hơn so với kế hoạch
        /// </summary>
        BeyondProgress = 3


    }
}
