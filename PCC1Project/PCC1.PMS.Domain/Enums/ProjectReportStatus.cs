﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PCC1.PMS.Domain.Enums
{
    public enum ProjectReportStatus
    {
        Draft = 1,

        Submited = 5, 

        Approved = 10,

        Rejected = 20,

        CloneReport = 25
    }
}
