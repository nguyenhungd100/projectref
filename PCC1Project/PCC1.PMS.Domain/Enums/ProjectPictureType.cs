﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PCC1.PMS.Domain.Enums
{
    public enum ProjectPictureType
    {
        /// <summary>
        /// Hình ảnh công trình từ phía cán bộ quản lý dự án
        /// </summary>
        HinhAnhCongTrinhTuCBQLDA = 1,

        /// <summary>
        /// Hình ảnh báo cáo từ phía người gửi báo cáo
        /// </summary>
        HinhAnhBaoCao = 5,

        /// <summary>
        /// Hình ảnh báo cáo an toàn vệ sinh
        /// </summary>
        HinhAnhBaoCaoAnToanVeSinh = 10
    }
}
