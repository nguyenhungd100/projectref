﻿using PCC1.PMS.Domain.Services.SafelyReports;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace PCC1.PMS.Domain.Enums
{
    public enum ViolationGroupCode
    {
        ViolationHuman = 1,
        ViolationEnvironment = 2,
       
    }
    public enum ViolationType
    {
        [ViolationDetailAttribute(ViolationGroupCode.ViolationHuman, "Số lao động HĐ>=3 tháng chưa huấn luyện AT/tổng số LĐ HĐ >=3 tháng")]
        Than3Month = 5,

        [ViolationDetailAttribute(ViolationGroupCode.ViolationHuman, "Số lao động HĐ<3 tháng chưa huấn luyện AT/tổng số LĐ HĐ <3 tháng")]
        LessThan3Month = 10,

        [ViolationDetailAttribute(ViolationGroupCode.ViolationHuman, "Số người không mặc quần áo BHLĐ theo quy định")]
        Undressed = 20,

        [ViolationDetailAttribute(ViolationGroupCode.ViolationHuman, "Số người không đội mũ BHLĐ theo quy định")]
        NotWearingAHat = 25,

        [ViolationDetailAttribute(ViolationGroupCode.ViolationHuman, "Số người không đi giày BHLĐ theo quy định")]
        NotWearShoes = 30,

        [ViolationDetailAttribute(ViolationGroupCode.ViolationHuman, "Số người không đeo dây da AT theo quy định khi làm việc ở độ cao >= 2m")]
        NotWearingLeatherStraps = 35,

        [ViolationDetailAttribute(ViolationGroupCode.ViolationHuman, "Số người có nồng độ cồn trong khi làm việc")]
        HaveAlcoholic = 40,

        [ViolationDetailAttribute(ViolationGroupCode.ViolationEnvironment, "Số chỗ làm việc chưa được phân vùng cảnh báo an toàn theo BP TCTC")]
        NotWorkplacePartition = 45,

        [ViolationDetailAttribute(ViolationGroupCode.ViolationEnvironment, "Số lần chưa thi công theo đúng biện pháp TCTC")]
        NotConstructionStandardCompliance = 50,

        [ViolationDetailAttribute(ViolationGroupCode.ViolationEnvironment, "Số lượng dụng cụ an toàn, DCTC chưa đầy đủ và chưa được kiểm tra đảm bảo an toàn, chưa dùng đúng mục đích, tải trọng trước khi thi công")]
        NotFullMedicalEquipment = 55,

        [ViolationDetailAttribute(ViolationGroupCode.ViolationEnvironment, "Số lần các Tổ/nhóm sản xuất chưa phổ biến công việc, biện pháp AT, chưa phân công đúng người, đúng việc trước khi làm việc vào đầu giờ hàng ngày")]
        NotRolesOfPersionCompliance = 60,

        [ViolationDetailAttribute(ViolationGroupCode.ViolationEnvironment, "Không đăng ký cho Ban CHCT theo BM QT6.4.AT/BM03a và khi có sự cố thay đổi về nhân lực đăng ký lại bằng văn bản hoặc email")]
        NotRegisterForCommanderWorks = 65,

        [ViolationDetailAttribute(ViolationGroupCode.ViolationEnvironment, "Khi có các biện pháp TCTC, biện pháp AT CBKT đơn vị chưa phổ biến cho các tổ/nhóm và từng người lao động trước khi thi công")]
        PopularBeforeConstruction = 70,

        [ViolationDetailAttribute(ViolationGroupCode.ViolationEnvironment, "Số địa điểm/nơi làm việc không có ATV")]
        NotHaveATV = 75,
    }
}
