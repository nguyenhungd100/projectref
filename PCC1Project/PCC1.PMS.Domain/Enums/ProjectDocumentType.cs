﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PCC1.PMS.Domain.Enums
{
    public enum ProjectDocumentType
    {
        /// <summary>
        /// File hợp đồng chủ đầu tư
        /// </summary>
        HDCDT = 1,

        /// <summary>
        /// File hợp đồng bên B
        /// </summary>
        HDB = 2,

    }
}
