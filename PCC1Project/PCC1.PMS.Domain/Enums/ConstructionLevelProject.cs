﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PCC1.PMS.Domain.Enums
{
    public enum ConstructionLevelProject
    {
        /// <summary>
        /// Construction Level 1
        /// </summary>
        CongTrinhCap1 = 1,

        /// <summary>
        /// Construction Level 2
        /// </summary>
        CongTrinhCap2 = 2
    }
}
