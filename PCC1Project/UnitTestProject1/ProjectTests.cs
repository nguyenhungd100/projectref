﻿using MicroOrm.Dapper.Repositories;
using MicroOrm.Dapper.Repositories.SqlGenerator;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using PCC1.PMS.Domain.Entity;
using PCC1.PMS.Domain.Enums;
using PCC1.PMS.Domain.Models.Projects;
using PCC1.PMS.Domain.Repositories;
using PCC1.PMS.Domain.Services.Projects;
using System;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Threading.Tasks;
using static Microsoft.EntityFrameworkCore.DbLoggerCategory;

namespace UnitTestProject1
{
    [TestClass]
    public class ProjectTests
    {
        private static IDbConnection CreateConnection()
        {
            var connection = new SqlConnection("Server=35.229.137.189;Database=PCC1_PMS_DB;user=pcc1_dbo;" +
                "password=2pSrBfFutans2wBM;MultipleActiveResultSets=true");
            return connection;
        }     
        private IProjectRepository _projectRepository;
        //private readonly IDapperRepository<ProjectUser> _projectUserRepository;
        private readonly IDbConnection connection = CreateConnection();
        private readonly ISqlGenerator<Project> sqlGenerator = new SqlGenerator<Project>();

       

        public ProjectTests()
        {                
            _projectRepository = new ProjectRepository(connection, sqlGenerator);
            //_projectUserRepository = new DapperRepository<ProjectUser>(connection);
        }
        [TestMethod]
        public async Task TestMethod1()
        {
            ProjectService _projectService = new ProjectService(_projectRepository);
            ProjectModel projectModel = new ProjectModel();
            projectModel.Name = "s";
            projectModel.Type = TypeProject.DuAnCongTy;
            projectModel.Status = ProjectStatus.Running;
            var pr = await _projectService.InitProject(projectModel);

            var a = pr;
            Assert.IsFalse(a == false,"lỗi");

            //var project = _projectRepository.FindById(projectModel.Id);
            //Assert.Equals(projectModel.Status, ProjectStatus.Init);
        }
    }
}
