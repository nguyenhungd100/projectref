﻿using Autofac;
using MicroOrm.Dapper.Repositories;
using MicroOrm.Dapper.Repositories.SqlGenerator;
using Microsoft.AspNetCore.Http;
using PCC1.PMS.Domain.Entity;
using PCC1.PMS.Domain.Enums;
using PCC1.PMS.Domain.Models.Projects;
using PCC1.PMS.Domain.Models.Workitems;
using PCC1.PMS.Domain.Repositories;
using PCC1.PMS.Domain.Services;
using PCC1.PMS.Domain.Services.ProjectReports;
using PCC1.PMS.Domain.Services.Projects;
using PCC1.PMS.Domain.Services.WorkItems;
using PCC1.PMS.Framework.Utils;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace DomainTests
{
    public class ProjectTests
    {
        //  private readonly ProjectRepository _projectRepository;
        //private readonly IDapperRepository<ProjectUser> _projectUserRepository;


        //1. Khởi tạo dự án kèm thông tin tổng thể của dự án
        // ------Thêm thông thông tin tổng thể của dự án, xác định rõ loại dự án( da cty, dự án ủy quyền, cty thành viên)(Tổng cty)

        // ------Add nhân sự dự án, thêm vai trò cho nhân sự từ phía tổng công ty.
        //      + Thông tin tài khoản.
        //      + Vai trò cho nhân sự .
        // ------Cập nhật thông tin dự án( tiến độ tổng thể, tổng giá trị hơp đồng, xđ rõ hđ A B....) từ CBQLDA.
        // ------Cập nhật thông tin khối lượng công trình, tiến độ chi tiết, khối lượng thanh toán 
        //       do TBCHCT 
        // ------Duyệt dự án từ phía công ty.

        //2. Báo cáo tiến độ, báo cáo ATVS, Giám sát.
        // -----Báo cáo ATVS, tiến độ chi tiết từ bộ phận từ bộ phận ATVS, Giám sát(Đồng thời cập nhật
        // chi tiết tiến độ hàng ngày,tháng, quý). Gửi báo cáo cho phía trưởng ban chỉ huy công trình
        // -----Đối với bộ phận trưởng ban chỉ huy công trình. Bô phận này tổng hợp báo cáo từ bộ phân ATVS, 
        // giám sát để gửi cho tổng công ty. 



























        //private static IProjectRepository _projectRepository;

        //[Fact]
        //public async Task InitProjectTestAsync()
        //{
        //    ProjectService _projectService = new ProjectService(_projectRepository);
        //    var projectModel = new ProjectModel()
        //    {
        //        Name = "s",
        //        Type = TypeProject.DuAnCongTy,
        //        Status = ProjectStatus.Running
        //    };

        //    var pr = await _projectService.InitProject(projectModel);

        //    var a = pr;
        //    //Assert.False(a == false,"lỗi");

        //    //var project = _projectRepository.FindById(projectModel.Id);
        //    //Assert.Equal(projectModel.Status, ProjectStatus.Init);
        //}

        //private readonly IProjectRepository _projectRepository ;


        //private readonly IProjectService projectService = new ProjectService(_projectRepository);

        //public IProjectService _projectService;




        //private static IDbConnection CreateConnection()
        //{
        //    var connection = new SqlConnection("Server=35.229.137.189;Database=PCC1_PMS_DB;user=pcc1_dbo;" +
        //        "password=2pSrBfFutans2wBM;MultipleActiveResultSets=true");
        //    return connection;
        //}

        ////private readonly IDapperRepository<ProjectUser> _projectUserRepository;
        //private readonly IDbConnection connection = CreateConnection();
        //private readonly ISqlGenerator<Project> sqlGenerator1 = new SqlGenerator<Project>();
        //private readonly ISqlGenerator<ProjectUser> sqlGenerator2 = new SqlGenerator<ProjectUser>();
        //private readonly ISqlGenerator<ProjectPlan> sqlGenerator3 = new SqlGenerator<ProjectPlan>();
        //private readonly ISqlGenerator<ProjectReport> sqlGenerator4 = new SqlGenerator<ProjectReport>();

        //public ProjectTests()
        //{
        //    _projectRepository = new ProjectRepository(connection, sqlGenerator1);
        //    _projectUserRepository = new ProjectUserRepository(connection, sqlGenerator2);
        //    _projectPlanRepository = new ProjectPlanRepository(connection, sqlGenerator3);
        //    _projectReportRepository = new ProjectReportRepository(connection, sqlGenerator4);
        //}
        //private IProjectRepository _projectRepository;
        //private IDapperRepository<ProjectUser> _projectUserRepository;
        //private IDapperRepository<ProjectPlan> _projectPlanRepository;
        //private IDapperRepository<ProjectReport> _projectReportRepository;

        //private IUserRepository _userRepository;
        //private IDapperRepository<ProjectFolder> _projectFolderRepository;
        //private IDapperRepository<ProjectPicture> _projectPictureRepository;
        //private IDapperRepository<WorkItem> _workitemRepository;









        private static IContainer container;
        public ProjectTests()
        {
            var builder = new ContainerBuilder();
            var cnn = "Server=35.229.137.189;Database=PCC1_PMS_DB;user=pcc1_dbo;password=2pSrBfFutans2wBM;MultipleActiveResultSets=true";
            builder.RegisterModule(new DomainModule(cnn));
            builder.RegisterModule(new FrameworkModule("127.0.0.1:6379", 1));
            container = builder.Build();
            DomainMaps.Config();
        }

        /// <summary>
        /// Tạo mới info project như bình thường
        /// </summary>
        /// <param name="a"></param>
        public ProjectModel Test0KhoiTaoDuAn(ProjectAddModel projectAddModel)
        {
            using (var scope = container.BeginLifetimeScope())
            {
                var _projectService = scope.Resolve<IProjectService>();
                //return _projectService.InitInfoProject(projectAddModel);
                return null;
            }
        }


        /// <summary>
        ///Test case: Khởi tạo dự án không để status # init
        /// </summary>
        /// <returns></returns>
        [Fact]
        public void Test1KhoiTaoDuAnCoStatusInit()
        {
            using (var scope = container.BeginLifetimeScope())
            {
                var _projectService = scope.Resolve<IProjectService>();
                var projectAddModel = new ProjectAddModel
                {
                    Name = "test thử dự án",
                    ContractType = ContractType.HopDongCongTy,
                    Investor = "hùng",
                    SupervisingConsultant = "hùng",
                    DesignConsultant = "hùng",
                    Subcontractor = "hùng",
                    ConstructionLevel = ConstructionLevelProject.CongTrinhCap1,
                    ConstructionType = 1,
                    CapitalSource = 500,
                    ProjectGroup = 1,
                    Scale = "cấp quốc gia"
                };
                var projectModel = Test0KhoiTaoDuAn(projectAddModel);
                Assert.False(projectModel.Status != ProjectStatus.Init);
            }
        }

        ///// <summary>
        ///// Test cập nhật thông tin tổng thể cho dự án từ cán bộ quản lý dự án( Chưa add nhân sự
        ///// cho dự án không được cập nhật)
        ///// </summary>
        [Fact]
        public void Test2CapNhatTongTheDuAn()
        {
            using (var scope = container.BeginLifetimeScope())
            {
                var _projectService = scope.Resolve<IProjectService>();
                var projectAddModel = new ProjectAddModel
                {
                    Name = "test thử dự án",
                    ContractType = ContractType.HopDongCongTy,
                    Investor = "hùng",
                    SupervisingConsultant = "hùng",
                    DesignConsultant = "hùng",
                    Subcontractor = "hùng",
                    ConstructionLevel = ConstructionLevelProject.CongTrinhCap1,
                    ConstructionType = 1,
                    CapitalSource = 500,
                    ProjectGroup = 1,
                    Scale = "cấp quốc gia"
                };
                var projectModel = Test0KhoiTaoDuAn(projectAddModel);
                var checkAbilityOnEPC = _projectService.AbilityApprovingProjectForEPC(projectModel.Id);

                var updateModel = new ProjectOverallModel
                {
                    Id = projectModel.Id,
                    DateStart = new DateTime(2018, 8, 8),
                    DateEnd = new DateTime(2019, 9, 9),
                    TotalContractValue = 7,
                    Location = "hà nội"
                };

                var resultUpdate = false;
                try
                {
                    //resultUpdate = await _projectService.UpdateOverallProject(updateModel);
                }
                catch (Exception ex) { ex.Message.ToString(); };

                Assert.False(checkAbilityOnEPC.Item1 != resultUpdate);
            }
        }

        /// <summary>
        /// Cập nhật kế hoạch cho dự án
        /// </summary>
        [Fact]
        public void Test3CapNhatKeHoachChoDuAn()
        {
            using (var scope = container.BeginLifetimeScope())
            {
                var _projectService = scope.Resolve<IProjectService>();

                var planModels = new List<TaskModel>
                {
                    new TaskModel{
                        Id = Guid.NewGuid(),
                        ParentId = Guid.NewGuid(),
                        Title ="test thử 25/9",
                        Duration ="1",
                        DateStart =new DateTime(2018, 8, 8),
                        DateEnd =new DateTime(2018, 8, 9),
                        IsLeaf = true
                    }
                };
                var resultUpdatePlan = false;
                try
                {
                    //resultUpdatePlan = await _projectService.NewPlans(1046, planModels);
                }
                catch (Exception ex) { ex.Message.ToString(); }

                Assert.False(resultUpdatePlan == true);
            }
        }

        /// <summary>
        /// Test case 4: Kiểm tra điều kiện khi thêm mới hạng mục từ trưởng ban chỉ huy công trình
        /// (dự án có nhân sự, đã được cập nhât tổng thể từ cán bộ quản lý dự án)
        /// </summary>
        [Fact]
        public void Test4ThemMoiHangMuc()
        {
            using (var scope = container.BeginLifetimeScope())
            {
                var _workItemService = scope.Resolve<IWorkItemService>();
                var workItemModel = new AddWorkitemModel
                {
                    Name = "test thử thêm hạng mục ngày 25/9",
                    ProjectId = 1046,
                    Quantity = 10,
                    CalculationUnit = "cột",
                    Money = 5
                };
                var resultAdd = false;
                try
                {
                    //resultAdd = await _workItemService.AddWorkItem(workItemModel);
                }
                catch (Exception ex)
                {
                    ex.Message.ToString();
                }
                Assert.False(resultAdd == true);
            }
        }

      
        /// <summary>
        /// Test case 5: Duyệt dự án từ phía tổng công ty
        /// </summary>
        [Fact]
        public async Task Test5DuyetDuAnAsync()
        {
            using (var scope = container.BeginLifetimeScope())
            {
                var _projectService = scope.Resolve<IProjectService>();
                var projectAddModel = new ProjectAddModel
                {
                    Name = "test thử dự án",
                    ContractType = ContractType.HopDongCongTy,
                    Investor = "hùng",
                    SupervisingConsultant = "hùng",
                    DesignConsultant = "hùng",
                    Subcontractor = "hùng",
                    ConstructionLevel = ConstructionLevelProject.CongTrinhCap1,
                    ConstructionType = 1,
                    CapitalSource = 500,
                    ProjectGroup = 1,
                    Scale = "cấp quốc gia"
                };
                var projectModel = Test0KhoiTaoDuAn(projectAddModel);
                var resultApprove = false;
                var appoveModel = new ApproveProjectModel
                {
                    Id = projectModel.Id
                };
                try
                {
                   // resultApprove = await _projectService.ApproveProject(appoveModel.Id);
                }
                catch (Exception ex) { ex.Message.ToString(); }

                Assert.False(resultApprove == true);
            }
        }













    }
}
