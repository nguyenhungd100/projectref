using Autofac;
using MicroOrm.Dapper.Repositories;
using System;
using Xunit;
using PCC1.PMS.Domain.Entity;
using PCC1.PMS.Domain.Repositories;
using PCC1.PMS.Domain.Services;
using PCC1.PMS.Domain.Services.Users;
using PCC1.PMS.Domain.Services.Users.Implementation;

namespace DomainTests
{
    public class UserTests
    {
        private static IContainer Container { get; set; }

        public UserTests()
        {
            var builder = new ContainerBuilder();
            var cnn = "Server=125.253.113.43;Database=ZB_MASTER;user=demo;password=112233445566;MultipleActiveResultSets=true;";
            builder.RegisterModule(new DomainModule(cnn));
            builder.RegisterModule(new FrameworkModule("127.0.0.1:6379", 1));

            Container = builder.Build();

            DomainMaps.Config();
        }

        [Fact]
        public void Test1()
        {
            //IUserService userService = new UserService();
            //userService.Add(new UserModel() {
            //    Address = "HN",
            //    Email = "locle@gmail.com",
            //    Gender = 1,
            //    Mobile = "09123123",
            //    Password = "123123",
            //    Status = UserStatus.Actived,
            //    FullName = "abc"
            //});
            using (var scope = Container.BeginLifetimeScope())
            {
                var userService = scope.Resolve<IUserService>();
                //var users = userService.GetById(1);
                var result = userService.Search(new PCC1.PMS.Domain.Models.UserSearchModel
                {
                    UserName = "khanhdien@gmail.com"
                });

                //var userRepo = scope.Resolve<IDapperRepository<User>>();

            }

        }

        [Fact]
        public void RegisterTest()
        {
            using (var scope = Container.BeginLifetimeScope())
            {
                var userService = scope.Resolve<IUserService>();
                try
                {
                    var result = userService.CreateUser(new PCC1.PMS.Domain.Models.Users.CreateUserModel
                    {
                        //Email = "diennk@vnsolution.com.vn",
                        //UserName = "diennk",
                        Phone = "0916154234",
                        Password = "123456a@"
                    });
                }
                catch 
                {
                    
                }
                
            }
        }
        [Fact]
        public void AddNotificationTest()
        {
            using (var scope = Container.BeginLifetimeScope())
            {
                //var notificationService = scope.Resolve<INotificationStorageService>();
                //try
                //{
                //    var result = notificationService.Add(1, new PCC1.PMS.Domain.Models.Notification.NotificationModel
                //    {
                //        CreatedOnUtc = DateTime.UtcNow,
                //        Type = PCC1.PMS.Domain.Models.Notification.NotificationType.System,
                //        Unread = true,
                //        Content = "He thong gui cho ban mot tin nhan"
                //    });
                //}
                //catch (Exception ex)
                //{
                //}
            }
        }
        [Fact]
        public void ListNotificationTest()
        {
            using (var scope = Container.BeginLifetimeScope())
            {
                //var notificationService = scope.Resolve<INotificationStorageService>();
                //try
                //{
                //    var result = notificationService.List(1, DateTime.UtcNow.AddDays(-1), DateTime.UtcNow, 100, 0);
                //}
                //catch (Exception ex)
                //{
                //}
            }
        }
        [Fact]
        public void PushNofiticationServiceTestAsync()
        {
            //using (var scope = Container.BeginLifetimeScope())
            //{
            //    var pusher = scope.Resolve<IPushNofiticationService>();
            //    try
            //    {
            //        var result = await pusher.PushAsync(1, "hioii", "dfdfdf");
            //    }
            //    catch (Exception ex)
            //    {
            //    }
            //}
        }
    }
}
