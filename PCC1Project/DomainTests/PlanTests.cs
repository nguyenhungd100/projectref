using Autofac;
using MicroOrm.Dapper.Repositories;
using System;
using Xunit;
using PCC1.PMS.Domain.Entity;
using PCC1.PMS.Domain.Repositories;
using PCC1.PMS.Domain.Services;
using PCC1.PMS.Domain.Services.Users;
using PCC1.PMS.Domain.Services.Users.Implementation;
using PCC1.PMS.Domain.Services.ProjectReports;
using System.Linq;

namespace DomainTests
{
    public class PlanTests
    {
        private static IContainer Container { get; set; }

        public PlanTests()
        {
            var builder = new ContainerBuilder();
            var cnn = "Server=35.229.137.189;Database=PCC1_PMS_DB;user=pcc1_dbo;password=2pSrBfFutans2wBM;MultipleActiveResultSets=true";
            builder.RegisterModule(new DomainModule(cnn));
            builder.RegisterModule(new FrameworkModule("127.0.0.1:6379", 1));

            Container = builder.Build();

            DomainMaps.Config();
        }

        [Fact]
        public void RegisterTest()
        {
            using (var scope = Container.BeginLifetimeScope())
            {
                var projectRepository = scope.Resolve<IProjectRepository>();
                //var users = userRepository.FindAll(u => u.FullName.Contains("d")); // ok
                //var ids = new int[] { 1, 10, 15, 18 };
                //var users = userRepository.FindAll(u => ids.Contains(u.Id)); // ok
                var users = projectRepository.FindAll(u => u.DateEnd > DateTime.Now);
            }
        }
    }
}
