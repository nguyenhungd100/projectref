﻿using Autofac;
using System.Data;
using System.Data.SqlClient;
using PCC1.PMS.Domain.Repositories;
using PCC1.PMS.Domain.Services.Users.Implementation;
using PCC1.PMS.Framework.Caching;
using MicroOrm.Dapper.Repositories.SqlGenerator;
using MicroOrm.Dapper.Repositories;

namespace FrameworkTests
{
    public class DomainModule : Module
    {
        private readonly string _strCnn;

        public DomainModule(string strCnn)
        {
            this._strCnn = strCnn;
        }
        protected override void Load(ContainerBuilder builder)
        {
            base.Load(builder);

            builder.Register(c => new SqlConnection(_strCnn)).As<IDbConnection>().InstancePerLifetimeScope();

            // Register your own services within Autofac
            builder.RegisterGeneric(typeof(SqlGenerator<>)).AsImplementedInterfaces().InstancePerLifetimeScope();
            builder.RegisterGeneric(typeof(DapperRepository<>)).AsImplementedInterfaces().InstancePerLifetimeScope();
            builder.RegisterType<UserRepository>().AsImplementedInterfaces().InstancePerLifetimeScope(); ;
        }
    }

    public class FrameworkModule : Module
    {
        private readonly string _strCnn;
        private readonly int db;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="strCnn"></param>
        /// <param name="db"></param>
        public FrameworkModule(string strCnn, int db)
        {
            _strCnn = strCnn;
            this.db = db;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="builder"></param>
        protected override void Load(ContainerBuilder builder)
        {
            base.Load(builder);

            builder.Register(c => new RedisCacheManager(_strCnn, db)).As<IRedisCacheManager>().InstancePerLifetimeScope();
        }
    }
}
