using System;
using Xunit;
using PCC1.PMS.Framework.ExpoServer;
using Autofac;
using PCC1.PMS.Domain.Repositories;
using System.Linq;

namespace FrameworkTests
{
    public class DapperRepositoryTest
    {
        private static IContainer Container { get; set; }

        public DapperRepositoryTest()
        {
            var builder = new ContainerBuilder();
            var cnn = "Server=35.229.137.189;Database=PCC1_PMS_DB;user=pcc1_dbo;password=2pSrBfFutans2wBM;MultipleActiveResultSets=true";
            builder.RegisterModule(new DomainModule(cnn));
            builder.RegisterModule(new FrameworkModule("127.0.0.1:6379", 1));

            Container = builder.Build();
        }
        [Fact]
        public void TestContainsOperator()
        {
            using (var scope = Container.BeginLifetimeScope())
            {
                var userRepository = scope.Resolve<IUserRepository>();
                var ids = new long[] { 1, 3, 5, 7 };
                var result = userRepository.FindAll(u=> ids.Contains(u.Id));

            }
        }
    }
}
