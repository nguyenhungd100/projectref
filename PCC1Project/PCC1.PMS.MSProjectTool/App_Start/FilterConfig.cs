﻿using System.Web;
using System.Web.Mvc;

namespace PCC1.PMS.MSProjectTool
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }
}
