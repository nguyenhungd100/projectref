﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PCC1.PMS.MSProjectTool.Models
{
    public class PlanParsingModel
    {
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public List<TaskModel> Tasks { get; set; }
    }
}