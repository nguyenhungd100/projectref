﻿using java.lang;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PCC1.PMS.MSProjectTool.Models
{
    public class TaskModel
    {
        [JsonIgnore]
        public int UniqueId { get; set; }
        [JsonIgnore]
        public int? ParentUniqueId { set; get; }
        public string Title { get; set; }
        public string Duration { get; set; }
        public DateTime DateStart { get; set; }
        public DateTime DateEnd { get; set; }
        public Guid Id { get; set; }
        public Guid? ParentId { get; set; }
        public System.Boolean IsLeaf { set; get; }
        public int? OutLineLevel { set; get; }
        public int OrderNumber { get; set; }
    }
}