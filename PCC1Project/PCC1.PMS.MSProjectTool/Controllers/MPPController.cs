﻿using java.lang;
using java.util;
using net.sf.mpxj;
using net.sf.mpxj.reader;
using PCC1.PMS.MSProjectTool.Models;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace PCC1.PMS.MSProjectTool.Controllers
{

    public class MPPController : ApiController
    {
        [Route("api/mpp/test")]
        [HttpGet]
        public IHttpActionResult Test()
        {
            File.WriteAllText(@"C:\PCC1-Temp\logOutputDateTime.txt", "Test Diennk");
            return Json("");
        }
        public static DateTime ConvertJavaDateToDateTime(Date date)
        {

            var strDate = date != null ? date.ToString() : string.Empty;
            //try
            //{
            //    
            //    File.AppendAllLines(@"C:\PCC1-Temp\logOutputDateTime.txt", new string[] { strDate });
            //}
            //catch (System.Exception)
            //{

            //}
            var partern = strDate.Contains("GMT") ? "ddd MMM dd HH:mm:ss 'GMT' yyyy" : "ddd MMM dd HH:mm:ss 'ICT' yyyy";

            DateTime datetime = DateTime.ParseExact(strDate, partern, CultureInfo.CurrentUICulture.DateTimeFormat);
            return datetime;
        }
        [Route("api/mpp/getmpp")]
        [HttpGet]
        public IHttpActionResult Get(Guid fileId, string fileName)
        {
            try
            {
                var projectPlan = new PlanParsingModel();
                string storage = @"C:\PCC1-Temp\";
                string inputFile = string.Concat(storage, fileName.TrimStart());
                ProjectReader reader = ProjectReaderUtility.getProjectReader(inputFile);
                ProjectFile projectFile = reader.read(inputFile);
                var _taks = projectFile.Tasks.Cast<net.sf.mpxj.Task>();

                projectPlan.StartDate = ConvertJavaDateToDateTime(projectFile.StartDate);
                projectPlan.EndDate = ConvertJavaDateToDateTime(projectFile.FinishDate);

                var tasks = (from t in _taks
                             where t.UniqueID.intValue() > 0
                             select new TaskModel
                             {
                                 UniqueId = int.Parse(t.UniqueID.toString()),
                                 Id = Guid.NewGuid(),
                                 ParentUniqueId = t.ParentTask != null ? int.Parse(t.ParentTask.UniqueID.ToString()) : 0,
                                 Title = t.Name.ToString(),
                                 Duration = t.Duration.Duration.ToString(),
                                 DateStart = ConvertJavaDateToDateTime(t.Start),
                                 DateEnd = ConvertJavaDateToDateTime(t.Finish),
                                 IsLeaf = true ? t.ChildTasks.toArray().Length == 0 : false,
                                 OutLineLevel = int.Parse(t.OutlineLevel.toString()),
                                 OrderNumber = int.Parse(t.ID.toString())
                             }).ToList();

                foreach (var p in tasks)
                {
                    if (p.ParentUniqueId != 0)
                    {
                        var parent = tasks.SingleOrDefault(t => t.UniqueId == p.ParentUniqueId);
                        p.ParentId = parent.Id;
                    }
                    else
                    {
                        p.ParentId = Guid.Empty;
                    }
                }
                projectPlan.Tasks = tasks;
                return Json(projectPlan);
            }
            catch (System.Exception ex)
            {

                throw ex;
            }
        }
    }
}
