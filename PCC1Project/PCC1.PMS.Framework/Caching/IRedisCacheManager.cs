﻿using System;
using System.Collections.Generic;
using StackExchange.Redis;

namespace PCC1.PMS.Framework.Caching
{
    public interface IRedisCacheManager
    {
        bool Delete(string key);
        bool GeoAdd(string key, double lat, double lng, long id);
        GeoRadiusResult[] GeoRadius(string key, double lat, double lng, double radius, int count = -1);
        GeoRadiusResult[] GeoRadiusByMember(string key, long member, int radius, int count = -1);
        T Get<T>(string key);
        string StringGet(string key);
        bool StringSet(string key, string value, TimeSpan expireIn = default(TimeSpan));
        bool SortedSetAdd(string key, double score, string value);
        string[] SortedSetQuery(string key, double scoreFrom, double scoreTo, long take, long skip);
    }
}