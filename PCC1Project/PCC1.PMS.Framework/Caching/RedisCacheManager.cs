﻿using Newtonsoft.Json;
using StackExchange.Redis;
using System;
using System.Collections.Generic;

namespace PCC1.PMS.Framework.Caching
{
    public class RedisCacheManager : IRedisCacheManager
    {
        private static ConnectionMultiplexer connectionMultiplexer;
        private static IDatabase database;
        public RedisCacheManager(string connectionString, int redisDbNumber)
        {
            connectionMultiplexer = ConnectionMultiplexer.Connect(connectionString);
            database = connectionMultiplexer.GetDatabase(redisDbNumber);
        }

        public bool Delete(string key)
        {
            return database.KeyDelete(key);
        }
        public bool GeoAdd(string key, double lat, double lng, long id)
        {
            return database.GeoAdd(key, lng, lat, id);
        }
        public GeoRadiusResult[] GeoRadius(string key, double lat, double lng, double radius, int count = -1)
        {
            return database.GeoRadius(key, lng, lat, radius, unit: GeoUnit.Kilometers, count : count);
        }

        public GeoRadiusResult[] GeoRadiusByMember(string key, long member, int radius, int count = -1)
        {
            return database.GeoRadius(key, member, radius, unit: GeoUnit.Kilometers, count: count, order: Order.Ascending);

        }

        public string StringGet(string key)
        {
            return database.StringGet(key);
        }

        public bool StringSet(string key, string value, TimeSpan expireIn = default(TimeSpan))
        {
            return database.StringSet(key, value, expireIn);
        }
        public T Get<T>(string key) 
        {
            var str = database.StringGet(key);
            if (string.IsNullOrEmpty(str)) return default(T);
            return JsonConvert.DeserializeObject<T>(str);
        }

        public bool SortedSetAdd(string key, double score, string value)
        {
            return database.SortedSetAdd(key, value, score);
        }

        public string[] SortedSetQuery(string key, double scoreFrom, double scoreTo, long take, long skip)
        {
            var result = database.SortedSetRangeByScore(key, scoreFrom, scoreTo, skip: skip, take: take);
            return result.ToStringArray();
        }
    }
}
