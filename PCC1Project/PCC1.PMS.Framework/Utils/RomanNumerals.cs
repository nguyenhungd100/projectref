﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PCC1.PMS.Framework.Utils
{
    public static class RomanNumerals
    {
        public static (bool, int) IsRomanNumeralAndConvertToInt(string input)
        {
            var result = false;
            int num = 0;
            switch (input)
            {
                case "I": { result = true; num = 1; } break;
                case "II": { result = true; num = 2; } break;
                case "III": { result = true; num = 3; } break;
                case "IV": { result = true; num = 4; } break;
                case "V": { result = true; num = 5; } break;
                case "VI": { result = true; num = 6; } break;
                case "VII": { result = true; num = 7; } break;
                case "VIII": { result = true; num = 8; } break;
                case "IX": { result = true; num = 9; } break;
                case "X": { result = true; num = 10; } break;
                case "XI": { result = true; num = 11; } break;
                case "XII": { result = true; num = 12; } break;
                case "XIII": { result = true; num = 13; } break;
                case "XIV": { result = true; num = 14; } break;
                case "XV": { result = true; num = 15; } break;
                case "XVI": { result = true; num = 16; } break;
                case "XVII": { result = true; num = 17; } break;
                case "XVIII": { result = true; num = 18; } break;
                case "XIX": { result = true; num = 19; } break;
                case "XX": { result = true; num = 20; } break;

                case "XXI": { result = true; num = 21; } break;
                case "XXII": { result = true; num = 22; } break;
                case "XXIII": { result = true; num = 23; } break;
                case "XXIV": { result = true; num = 24; } break;
                case "XXV": { result = true; num = 25; } break;
                case "XXVI": { result = true; num = 26; } break;

                case "XXVII": { result = true; num = 27; } break;
                case "XXVIII": { result = true; num = 28; } break;
                case "XXIX": { result = true; num = 29; } break;
                case "XXX": { result = true; num = 30; } break;
                case "XXXI": { result = true; num = 31; } break;
                case "XXXII": { result = true; num = 32; } break;
                case "XXXIII": { result = true; num = 33; } break;

                case "XXXIV": { result = true; num = 34; } break;
                case "XXXV": { result = true; num = 35; } break;
                case "XXXVI": { result = true; num = 36; } break;
                case "XXXVII": { result = true; num = 37; } break;
                case "XXXVIII": { result = true; num = 38; } break;
                case "XXXIX": { result = true; num = 39; } break;

                case "XL": { result = true; num = 40; } break;
                case "XLI": { result = true; num = 41; } break;
                case "XLII": { result = true; num = 42; } break;
                case "XLIII": { result = true; num = 43; } break;
                case "XLIV": { result = true; num = 44; } break;
                case "XLV": { result = true; num = 45; } break;
                case "XLVI": { result = true; num = 46; } break;
                case "XLVII": { result = true; num = 47; } break;
                case "XLVIII": { result = true; num = 48; } break;
                case "XLIX": { result = true; num = 49; } break;
                case "L": { result = true; num = 50; } break;

            }
            return (result, num);
        }

        public static string ConvertRomanNumeralToInt(int input)
        {
            var result = string.Empty;
            switch (input)
            {
                case 1: { result = "I"; } break;
                case 2: { result = "II"; } break;
                case 3: { result = "III"; } break;
                case 4: { result = "III"; } break;
                case 5: { result = "V"; } break;
                case 6: { result = "VI";  } break;
                case 7: { result = "VII";  } break;
                case 8: { result = "VIII";  } break;
                case 9: { result = "IX"; } break;
                case 10: { result = "X";  } break;
            }
            return result;
        }
    }
}
