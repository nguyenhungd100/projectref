﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PCC1.PMS.Framework.Utils
{
    public static class HeadingExcels
    {
        public static bool IsHeading1(string input) // I
        {
            var result = false;
            if (!string.IsNullOrEmpty(input))
            {
                result = RomanNumerals.IsRomanNumeralAndConvertToInt(input).Item1;
            }

            return result;                       
        }
      
        public static bool IsHeading2(string input) // I.1
        {
            var result = false;
            if (!string.IsNullOrEmpty(input))
            {
                var stringSplit = input.Split('.');
                int numberOut = 0;
                if (stringSplit.Length == 2)
                {
                    result = int.TryParse(stringSplit[1], out numberOut);
                }
            }                       

            return result;
        }

        public static bool IsHeading3(string input) // I.1.1
        {
            var result = false;
           
            int numberLastOut = 0;
            int numberMidOut = 0;
            if (!string.IsNullOrEmpty(input))
            {
                var stringSplit = input.Split('.');
                if (stringSplit.Length == 3)
                {                  
                    var lastStringIsInt = int.TryParse(stringSplit[2], out numberLastOut);
                    var midStringIsInt = int.TryParse(stringSplit[1], out numberMidOut);
                    result = lastStringIsInt && midStringIsInt;
                }
            }        
            return result;
        }

        public static bool IsHeading4(string input) //1
        {
            var result = false;
            var number = 0;
            if (!string.IsNullOrEmpty(input))
            {
                result= int.TryParse(input, out number);
            }         
            return result;
        }

        public static bool IsHeading5(string input)// null
        {
            return string.IsNullOrEmpty(input) ? true : false;
        }

        public static int DeteminedLevelHeading(string input)
        {
            int level = 0;
            if (IsHeading1(input)) level = 1;
            if (IsHeading2(input)) level = 2;
            if (IsHeading3(input)) level = 3;
            if (IsHeading4(input)) level = 4;
            if (IsHeading5(input)) level = 5;
            return level;
        }
    }
}
