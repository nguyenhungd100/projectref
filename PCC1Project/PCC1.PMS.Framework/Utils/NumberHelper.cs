﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PCC1.PMS.Framework.Utils
{
    public static class NumberHelper
    {
        public static decimal RoundDown(decimal i, double decimalPlaces)
        {
            var power = Convert.ToDecimal(Math.Pow(10, decimalPlaces));
            return Math.Floor(i * power) / power;
        }
    }
}
