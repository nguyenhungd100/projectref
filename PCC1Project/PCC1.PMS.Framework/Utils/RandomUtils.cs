﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PCC1.PMS.Framework.Utils
{
    public static class RandomUtils
    {
        public static double GetRandomNumber(double minimum, double maximum)
        {
            Random random = new Random();
            return random.NextDouble() * (maximum - minimum) + minimum;
        }
    }
}
