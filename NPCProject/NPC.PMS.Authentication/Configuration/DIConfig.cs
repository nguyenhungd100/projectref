﻿using Microsoft.Extensions.DependencyInjection;
using NPC.PMS.Domain.Data;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using NPC.PMS.Domain.Services;
using NPC.PMS.Domain.Services.Roles;
using NPC.PMS.Domain.Services.Users;

namespace NPC.PMS.Authentication.Configuration
{
    /// <summary>
    /// 
    /// </summary>
    public static class DIConfigurator
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="services"></param>
        /// <param name="configuration"></param>
        public static void ConfigDI(this IServiceCollection services, IConfiguration configuration)
        {
            services
                .AddEntityFrameworkSqlServer()
                .AddDbContext<DomainContext>((serviceProvider, options) =>
                             options.UseSqlServer(configuration.GetConnectionString("DefaultConnection"))
                             .UseInternalServiceProvider(serviceProvider));

            services.AddScoped(typeof(IRepository<>), typeof(Repository<>));
            services.AddScoped<IUserService, UserService>();
            services.AddScoped<IRolesService, RolesService>();
        }
    }
}
