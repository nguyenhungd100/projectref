﻿using IdentityServer4.Models;
using IdentityServer4.Validation;
using System;
using System.Threading.Tasks;
using NPC.PMS.Framework.Utils;
using NPC.PMS.Domain.Services;
using NPC.PMS.Domain.Services.Users;
using NPC.PMS.Domain.Services.Roles;
using NPC.PMS.Domain.Models;

namespace NPC.PMS.Authentication.IdentityServer
{
    /// <summary>
    /// 
    /// </summary>
    public class ResourceOwnerPasswordValidator : IResourceOwnerPasswordValidator
    {
        //repository to get user from db
        private readonly IAuthorizationService _authorizationService;
        private readonly IRolesService _rolesService;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="authorizationService"></param>
        /// <param name="rolesService"></param>
        public ResourceOwnerPasswordValidator(IAuthorizationService authorizationService, IRolesService rolesService)
        {
            _authorizationService = authorizationService; //DI
            _rolesService = rolesService;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="context"></param>
        /// <returns></returns>
        //this is used to validate your user account with provided grant at /connect/token
        public async Task ValidateAsync(ResourceOwnerPasswordValidationContext context)
        {
            try
            {
                //get your user model from db (by username - in my case its email)
                var result = await _authorizationService.VerifyPasswordAsync(context.UserName, EncryptUtil.EncryptMD5(context.Password));

                //check if password match - remember to hash password if stored as hash in db
                if (result)
                {
                    var userModel = _authorizationService.GetUser(context.UserName);
                    context.Result = new GrantValidationResult(
                       subject: userModel.Id.ToString(),
                       authenticationMethod: "custom",
                       claims: Config.GetUserClaims(userModel, _rolesService));
                }
                else
                {
                    context.Result = new GrantValidationResult(TokenRequestErrors.InvalidGrant);
                }
            }
            catch (Exception ex)
            {
                context.Result = new GrantValidationResult(TokenRequestErrors.InvalidRequest, ex.Message);
            }
        }
    }
}
