﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace NPC.PMS.Framework.ExpoServer
{
    public static class NotificationService
    {
        public static async Task PushAsync(string[] tokens, string title, string body, JObject data = null)
        {
            var pageSize = 100;
            var pageIndex = 0;
            if (data == null) data = new JObject();
            data["title"] = title;
            data["body"] = body;
            do
            {
                var messages = from t in tokens.Skip(pageSize * pageIndex).Take(pageSize)
                               select new
                               {
                                   to = t,
                                   title,
                                   body,
                                   data
                               };
                if (messages.Count() == 0) break;

                pageIndex++;

                await CallExpoApiAsync(messages);
            }
            while (true);
        }

        private static async Task CallExpoApiAsync(IEnumerable<object> messages)
        {
            using (WebClient client = new WebClient())
            {
                client.Headers.Add("accept", "application/json");
                client.Headers.Add("accept-encoding", "gzip, deflate");
                client.Headers.Add("Content-Type", "application/json");
                try
                {
                    var response = await client.UploadStringTaskAsync("https://exp.host/--/api/v2/push/send", Newtonsoft.Json.JsonConvert.SerializeObject(messages));
                }
                catch (Exception)
                {
                }
            }
        }
    }
}
