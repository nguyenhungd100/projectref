﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace NPC.PMS.Framework.Push
{
    public interface IPushService
    {
        Task PushAsync(string token, string title, string body, Dictionary<string, string> data = default(Dictionary<string, string>));
    }
}