﻿using FcmSharp;
using FcmSharp.Requests;
using FcmSharp.Settings;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace NPC.PMS.Framework.Push
{
    public class PushService : IPushService
    {
        private readonly string settingFile;

        public PushService(string settingFile)
        {
            this.settingFile = settingFile;
        }

        public async Task PushAsync(string token, string title, string body, Dictionary<string, string> data = default(Dictionary<string, string>))
        {
            // Read the Credentials from a File, which is not under Version Control:
            var settings = FileBasedFcmClientSettings.CreateFromFile(settingFile);

            // Construct the Client:
            using (var client = new FcmClient(settings))
            {
                var notification = new Notification
                {
                    Title = title,
                    Body = body
                };

                // The Message should be sent to the News Topic:
                var message = new FcmMessage()
                {
                    ValidateOnly = false,
                    Message = new Message
                    {
                        Token = token,
                        Notification = notification,
                        Data = data,
                        AndroidConfig = new AndroidConfig
                        {
                            Notification = new AndroidNotification
                            {
                                Sound = "default"
                            }
                        }
                    },
                };

                // Finally send the Message and wait for the Result:
                CancellationTokenSource cts = new CancellationTokenSource();

                // Send the Message and wait synchronously:
                var result = await client.SendAsync(message, cts.Token);
            }
        }
    }
}
