﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NPC.PMS.Framework.Utils
{
    public static class DatetimeHelper
    {
        public static bool IsALeapYear(int year)
        {
            if (year % 4 != 0) return false;
            if (year % 100 != 0) return true;
            if (year % 400 != 0) return false;
            return true;
        }

        public static int DayInYears(int year)
        {
            if (IsALeapYear(year)) return 366;
            return (365);
        }

        public static int TotalDayBeforeYears(int year)
        {
            int totalBeforeDays = 0;
            for (int i = 1; i < year; i += 1)
                totalBeforeDays += DayInYears(i);
            return totalBeforeDays;
        }

        public static int DayInMonths(int year, int month)
        {
            switch (month)
            {
                case 4:
                case 6:
                case 9:
                case 11: return 30;
                case 2:
                    {
                        if (IsALeapYear(year)) return 29;
                        return 28;
                    }
                default: return 31;
            }
        }

        public static int SoNgayTruocThang(int year, int month)
        {
            var SoNgay = 0;
            for (int i = 1; i < month; i += 1)
                SoNgay += DayInMonths(year, i);
            return SoNgay;
        }

        public static int TotalDays(int year, int month, int day)
        {
            return TotalDayBeforeYears(year) + SoNgayTruocThang(year, month) + day;
        }

        public static string DayInWeeks(int year, int month, int day)
        {
            switch (TotalDays(year, month, day) % 7)
            {
                case 0: return " chủ nhật ";
                case 1: return " thứ hai ";
                case 2: return " thứ ba";
                case 3: return " thứ tư ";
                case 4: return " thứ năm ";
                case 5: return " thứ sáu ";
                default: return " thứ bảy ";
            }
        }
    }
}
