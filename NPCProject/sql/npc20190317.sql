SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[SettlementPayments](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[SettlementId] [int] NULL,
	[PaymentValue] [decimal](18, 0) NULL,
	[PaymentDate] [datetime] NULL,
	[Note] [nvarchar](450) NULL,
 CONSTRAINT [PK_Payments] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO


Alter table SettlementChecks add DocumentStatus int
Alter table SettlementChecks add P5Reply nvarchar(450)
Alter table SettlementChecks add P5Comment nvarchar(450)
Alter table SettlementChecks add P2Reply nvarchar(450)
Alter table SettlementChecks add Attachments nvarchar(450)


Alter table Settlements add ValueByBill decimal(18,0)
Alter table Settlements add Vat decimal(4,2)
Alter table Settlements add VatValue decimal(18,2)
Alter table Settlements add BillValue decimal(18,2)
Alter table Contract add  OpeningBalance decimal(18,0)
Insert into SettlementDocuments (DocumentName, Type)  values (N'Đề nghị tạm ứng',1)



