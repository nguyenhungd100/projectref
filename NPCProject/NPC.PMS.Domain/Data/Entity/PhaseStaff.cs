﻿using NPC.PMS.Domain.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace NPC.PMS.Domain.Data.Entity
{
    public class PhaseStaff : BaseEntity
    {
        public int PhaseId { get; set; }
        public int StaffId { get; set; }
        public PhaseStaffPosition? PositionInPhase { get; set; }
        public PCMAssignPotition Position { get; set; }
        public DateTime StartTime { get; set; }
        public DateTime EndTime { get; set; }

        public int? ReplaceUserId { get; set; }

        public DateTime? ReplaceDate { get; set; }

        public PhaseStaffStatus? Status { get; set; }

        public bool? IsOriginal { get; set; }
    }
}
