﻿using NPC.PMS.Domain.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace NPC.PMS.Domain.Data.Entity
{
    public class ContractUser : BaseEntity
    {
        public int ContractId { get; set; }
        public ContractKind Kind { get; set; }
        public int UserId { get; set; }
        public UserFunction Function { get; set; }
        public Department DepartmentId { get; set; }
        public ProcessType? ProcessType { get; set; }
    }
}
