﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace NPC.PMS.Domain.Data.Entity
{
    //Bảng hồ sơ thanh toán
    public class Settlement
    {
        public int Id { get; set; }
        public Guid ContractUID { get; set; }
        public int No { get; set; }
        public decimal Value { get; set; }
        public DateTime CreatedOn { get; set; }
        public int CreatedBy { get; set; }
        public SettlemenStatus Status { get; set; }
        public string Note { get; set; }
        public int? ReceiverId { get; set; }
        public DateTime? ReceivedTime { get; set; }
        public DateTime? BillDate { get; set; }
        public string BillCode { get; set; }
        /// <summary>
        /// ngày thanh quyết toán.
        /// </summary>
        public DateTime? PaymentDate { get; set; }
        /// <summary>
        /// Số thanh quyết toán.
        /// </summary>
        public string PaymentNumber { get; set; }

        public decimal? ValueByBill { get; set; }
        public decimal? Vat { get; set; }
        public decimal? VatValue { get; set; }
        public decimal? BillValue { get; set; }

    }
    public enum SettlementType : int
    {
        ContractAGeneral = 10,
        ContractA = 5,
        Contract = 1
    }
    public enum SettlemenStatus : int
    {
        [Description("Bản nháp")]
        Draft = 1,
        [Description("Chờ duyệt")]
        Submited = 2,
        [Description("Trả lại")]
        Rejected = 3,
        [Description("Đã duyệt")]
        Approved = 5,
        [Description("Đã chuyển P5")]
        Passed = 10,
        [Description("P5 Đã nhận")]

        Received = 20,
        [Description("P5 trả lại P2")]
        P5Reject = 22,
        [Description("P5 Hoàn tất")]
        Completed = 25,
    }
}
