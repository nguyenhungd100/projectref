﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NPC.PMS.Domain.Data.Entity
{
    public class PhaseWorkitem : BaseEntity
    {
        public int PhaseId { get; set; }
        public int DepartmentId { get; set; }
        public int WorkitemId { get; set; }
        public int Quantity { get; set; }
        public int? CompletedQuantity { get; set; }
    }
}
