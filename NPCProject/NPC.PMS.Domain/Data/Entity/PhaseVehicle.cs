﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NPC.PMS.Domain.Data.Entity
{
    public class PhaseVehicle : BaseEntity
    {
        public int PhaseId { get; set; }
        public int VehicleId { get; set; }
    }
}
