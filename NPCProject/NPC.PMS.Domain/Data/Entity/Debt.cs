﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NPC.PMS.Domain.Data.Entity
{
    //Bảng quyết toán nợ công của kế toán
    public class Debt
    {
        public int Id { get; set; }
        public string Code { get; set; }
        public int CustomerId { get; set; }
        //Bắt đâu kì hạn
        public decimal Beginning { get; set; }
        //Tổng giá trị đơn hàng trong kì hạn
        public decimal Bill { get; set; }
        //Tổng thanh toán trong kì hạn
        public decimal Paid { get; set; }
        //Cuối kì còn
        public decimal TermEnd { get; set; }
        //Số lượng kì hạn.
        public int Term { get; set; }
        //Thời gian/kì
        public double Time { get; set; }
        public string Description { get; set; }
        //Người quản lý
        public int UserId { get; set; }
    }
}
