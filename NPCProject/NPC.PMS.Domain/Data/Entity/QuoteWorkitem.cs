﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NPC.PMS.Domain.Data.Entity
{
    public class QuoteWorkitem : BaseEntity
    {
        public int QuoteId { get; set; }

        public int? Index { get; set; }

        public string No { get; set; }

        public string Title { get; set; }

        public decimal? Quantity { get; set; }

        public string Unit { get; set; }

        public decimal? Price { get; set; }

        public decimal? Money { get; set; }

        public DateTime? StartDate { get; set; }

        public DateTime? EndDate { get; set; }

        public decimal? AcceptancedQuantity { get; set; }

    }
}
