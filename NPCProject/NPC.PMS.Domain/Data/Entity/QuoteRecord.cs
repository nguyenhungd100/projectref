﻿using NPC.PMS.Domain.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace NPC.PMS.Domain.Data.Entity
{
    public class QuoteRecord : BaseEntity
    {
        public int QuoteId { get; set; }

        public int DepartmentId { get; set; }

        public string Files { get; set; }

        public DateTime CreatedOn { get; set; }

        public int CreatedBy { get; set; }

        public RecordStatus Status { get; set; }

        public string Content { get; set; }

        public string Note { get; set; }
    }
}
