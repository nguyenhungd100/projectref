﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NPC.PMS.Domain.Data.Entity
{
    public class QuoteStaff : BaseEntity
    {
        public int QuoteId { get; set; }

        public int StaffId { get; set; }

        public bool IsLeader { get; set; }


    }
}
