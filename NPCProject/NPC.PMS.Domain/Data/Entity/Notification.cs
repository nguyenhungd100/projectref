﻿/// <summary>
/// Thông báo hệ thống
/// </summary>

namespace NPC.PMS.Domain.Data.Entity
{
    using NPC.PMS.Domain.Enums;
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations.Schema;

    public partial class Notification
    {
        public Guid Id { get; set; }
        public NotificationType Type { get; set; } = NotificationType.System;
        public int UserId { get; set; }
        public DateTime Time { get; set; } = DateTime.Now;
        public string Message { get; set; }
        public bool Opened { get; set; } = false;
        public int ObjectId { get; set; } = 0;

        [NotMapped]
        public string Data { get; set; }
    }
}
