﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NPC.PMS.Domain.Data.Entity
{
    //Bảng tài liệu
    public class SettlementDocument
    {
        public int Id { get; set; }
        public string DocumentName { get; set; }
        public SettlementDocumentType Type { get; set; }
    }
    public enum SettlementDocumentType : int
    {
        Contract = 1,
        Contracta = 10,
    }
}
