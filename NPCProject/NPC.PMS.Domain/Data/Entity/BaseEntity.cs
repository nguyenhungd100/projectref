﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NPC.PMS.Domain.Data.Entity
{
    public class BaseEntity
    {
        public int Id { get; set; }
    }
}
