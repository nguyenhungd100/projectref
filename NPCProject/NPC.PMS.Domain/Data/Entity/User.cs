﻿
using NPC.PMS.Domain.Enums;
using System;
/// <summary>
/// Tài khoản của nhân viên trong hệ thống
/// </summary>
namespace NPC.PMS.Domain.Data.Entity
{
    public partial class User : BaseEntity
    {
        public string FullName { get; set; }
        public string Password { get; set; }
        public string Email { get; set; }
        public bool? Gender { get; set; }
        public string Mobile { get; set; }
        public UserStatus Status { get; set; }
        public string Avatar { get; set; }
        public string UserRoles { get; set; }
        public int? DepartmentId { get; set; }
        public StaffPosition? Position { get; set; }
        public int? SafeLevel { get; set; }
        public WorkLevel? WorkLevel { get; set; }
    }
}
