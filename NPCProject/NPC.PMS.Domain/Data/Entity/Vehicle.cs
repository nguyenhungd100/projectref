﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NPC.PMS.Domain.Data.Entity
{
    //Bảng danh mục phương tiện
    public class Vehicle
    {
        public int Id { get; set; }
        public string Number { get; set; }
        public string DriverName { get; set; }
        public int Seat { get; set; }

        public bool IsCarRental { get; set; }
    }
}
