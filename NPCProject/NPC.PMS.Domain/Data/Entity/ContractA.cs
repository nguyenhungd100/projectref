﻿using NPC.PMS.Domain.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace NPC.PMS.Domain.Data.Entity
{
    public class Contracta
    {
        public int Id { get; set; }
        public Guid UID { get; set; }
        public string Code { get; set; }
        public decimal? Price { get; set; }
        public string Supplier { get; set; }
        public string Contents { get; set; }
        public string Description { get; set; }
        public DateTime DateCreated { get; set; }
        public DateTime? DateSign { get; set; }
        public ContractaStatus Status { get; set; }
        public int CreatedBy { get; set; }
        public string Files { get; set; }
        public string Logs { get; set; }
        public int? RefContractId { get; set; }
        public int? GroupId { get; set; }

        public FormalitySelectContractA? FormalityCode { get; set; }


        public string Attachments { get; set; }

    }
    public enum ContractaStatus : int
    {
        [Description("Bản nháp")]
        Draft = 1,

        /// <summary>
        /// Chuyên viên gửi trưởng phòng P2"
        /// </summary>
        [Description("Đã gửi trưởng phòng P2")]
        SumitToP2Director = 10,

        /// <summary>
        /// Từ chối hợp đồng A từ trưởng phòng P2
        /// </summary>
        [Description("Từ chối")]
        RejectFromP2Director = 15,

        /// <summary>
        /// Chuyên viên gửi trưởng phòng P5
        /// </summary>
        [Description("Đã gửi trưởng phòng P5")]       
        SumitToP5Director = 20,

        /// <summary>
        /// Từ chối hợp đồng A từ trưởng phòng P5
        /// </summary>
        [Description("Từ chối")]
        RejectFromP5Director = 25,

        /// <summary>
        /// P2 send P5
        /// </summary>
        [Description("Đã chuyển cho phòng P5")]
        P2SendP5 = 30,

        /// <summary>
        /// P5 send P2
        /// </summary>
        [Description("Đã chuyển cho phòng P2")]
        P5SendP2 = 35,

        /// <summary>
        /// Đã hoàn thiện
        /// </summary>
        [Description("Đã hoàn thiện")]
        Completed = 40

    }
}
