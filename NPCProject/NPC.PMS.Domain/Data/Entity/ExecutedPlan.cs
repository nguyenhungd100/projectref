﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NPC.PMS.Domain.Data.Entity
{
    public class ExecutedPlan
    {
        public int Id { get; set; }
        public string Categories { get; set; }
        public int Quatity { get; set; }
        public DateTime StartedDate { get; set; }
        public DateTime EndDate { get; set; }
        public int DepartmentId { get; set; }
    }
}
