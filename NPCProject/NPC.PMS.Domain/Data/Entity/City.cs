﻿using System;

namespace NPC.PMS.Domain.Data.Entity
{
    public partial class City : BaseEntity
    {
        public int? RegionId { get; set; }
        public DateTime AddedDate { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public int? DisplayOrder { get; set; }
        public bool IsActive { get; set; }
        public string PermanLink { get; set; }
    }
}
