﻿using NPC.PMS.Domain.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace NPC.PMS.Domain.Data.Entity
{
    public class Phase : BaseEntity
    {
        public int ContractId { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public PhaseStatus Status { get; set; } = PhaseStatus.Draft;
        public string Departments { get; set; }
        public string DoneStaffsDepartments { get; set; }

        public string Title { get; set; }
        public string Note { get; set; }
        public int? No { get; set; }
        public string Centres { get; set; }
        public string RejectReason { get; set; }

        public bool? IsModify { get; set; }
        public ProcessType? ProcessType { get; set; }
    }
}
