﻿/// <summary>
/// Cài đặt cấu hình hệ thống
/// </summary>

namespace NPC.PMS.Domain.Data.Entity
{
    using System;
    using System.Collections.Generic;
    
    public partial class Configuration : BaseEntity
    {
        public string SmtpServer { get; set; }
        public int SmtpPort { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public Nullable<bool> UseSsl { get; set; }
        public string AuthenticationMethod { get; set; }
        public string EmailTest { get; set; }
        public string SmsProviderAlias { get; set; }
        public string SmsAuthUser { get; set; }
        public string SmsPassUser { get; set; }
    }
}
