﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NPC.PMS.Domain.Data.Entity
{
    public class Centre : BaseEntity
    {
        public string Name { get; set; }
        public string Code { get; set; }
    }
}
