﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NPC.PMS.Domain.Data.Entity
{
    public class ContractUnit : BaseEntity
    {
        public int ContractId { get; set; }
        public int UnitId { get; set; }
        public UnitType Type { get; set; }
    }
    public enum UnitType : int
    {
        Division = 1,
        Company = 2
    }
}
