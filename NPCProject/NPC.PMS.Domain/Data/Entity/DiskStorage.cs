﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace NPC.PMS.Domain.Data.Entity
{
    public class DiskStorage : BaseEntity
    {
        public string FolderPath { set; get; }

        public string FileName { set; get; }
    }
}
