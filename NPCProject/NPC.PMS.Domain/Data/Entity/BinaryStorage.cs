﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace NPC.PMS.Domain.Data.Entity
{
    public class BinaryStorage 
    {
        public long Id { get; set; }
        public byte[] Data { set; get; }
    }
}
