﻿using NPC.PMS.Domain.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace NPC.PMS.Domain.Data.Entity
{
    public class Delegacy : BaseEntity
    {
        public int DelegacyUserId { get; set; }

        public int DelegatedUserId { get; set; }

        public string RoleDelegated { get; set; }

        public string OldRole { get; set; }

        public StaffPosition PotitionDelegated { get; set; }

        public StaffPosition? OldPotition { get; set; }

    }
}
