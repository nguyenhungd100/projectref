﻿using NPC.PMS.Domain.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace NPC.PMS.Domain.Data.Entity
{
    public class PhaseEquipment : BaseEntity
    {
        public int PhaseId { get; set; }
        public int DepartmentId { get; set; }
        public int EquipmentId { get; set; }
    }
}
