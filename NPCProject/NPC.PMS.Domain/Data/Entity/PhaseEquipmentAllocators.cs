﻿using NPC.PMS.Domain.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace NPC.PMS.Domain.Data.Entity
{
    public class PhaseEquipmentAllocators : BaseEntity
    {
        public int PhaseId { get; set; }

        public int UserId { get; set; }

        public Department DepartmentId { get; set; }
    }
}
