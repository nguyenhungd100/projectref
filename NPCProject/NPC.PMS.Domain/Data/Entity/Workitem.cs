﻿using NPC.PMS.Domain.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace NPC.PMS.Domain.Data.Entity
{
    public class Workitem : BaseEntity
    {
        public string No { get; set; }
        public string Title { get; set; }
        public string Unit { get; set; }
        public decimal? Quantity { get; set; }
        public decimal? AcceptancedQuantity { get; set; }
        public decimal? Price { get; set; }
        public decimal? Money { get; set; }
        public int ContractId { get; set; }
        public int Index { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }

        public bool? IsStem { get; set; }
        public ProcessType? ProcessType { get; set; }
    }
}
