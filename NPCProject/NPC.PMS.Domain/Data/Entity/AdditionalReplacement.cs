﻿using NPC.PMS.Domain.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace NPC.PMS.Domain.Data.Entity
{
    public class AdditionalReplacement : BaseEntity
    {
        public int PhaseId { get; set; }

        public int StaffId { get; set; }

        public int Substitute { get; set; }

        public ReplacementStaffStatus Status { get; set; }
    }
}
