﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NPC.PMS.Domain.Data.Entity
{
    public class ContractGroup : BaseEntity
    {
        public string Name { get; set; }
    }
}
