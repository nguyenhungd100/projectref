﻿using NPC.PMS.Domain.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace NPC.PMS.Domain.Data.Entity
{
    public class ProfileListContractA : BaseEntity
    {
        public Guid ContractAUID { get; set; }

        public FormalitySelectContractA FormalityCode { get; set; }

        public string Name { get; set; }

        public DateTime? PromisedDate { get; set; }

        public string Feedback { get; set; }

        public string Reply { get; set; }

        public ProfileListContractAStatus Status { get; set; }

        public P5FeedbackContractAStatus? FeedbackStatus { get; set; }
    }
}
