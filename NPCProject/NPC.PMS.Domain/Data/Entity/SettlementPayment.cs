﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NPC.PMS.Domain.Data.Entity
{
   public class SettlementPayment:BaseEntity
    {
        public int? SettlementId { get; set; }
        public decimal? PaymentValue { get; set; }
        public DateTime? PaymentDate { get; set; }
        public string Note { get; set; }
    }
}
