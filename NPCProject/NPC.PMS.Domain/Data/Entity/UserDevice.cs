﻿using System;
using NPC.PMS.Domain.Data.Entity;
using NPC.PMS.Domain.Enums;

namespace NPC.PMS.Domain.Entity
{
    public class UserDevice : BaseEntity
    {
        public int UserId { get; set; }
        public string DeviceToken { get; set; }
        public Platform OS { get; set; }
        public string DeviceId { get; set; }
    }
}
