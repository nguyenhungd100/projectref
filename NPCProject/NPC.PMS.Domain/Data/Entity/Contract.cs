﻿using NPC.PMS.Domain.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace NPC.PMS.Domain.Data.Entity
{
    public class Contract : BaseEntity
    {
        public Guid UID { get; set; } = Guid.NewGuid();
        public string Code { get; set; }
        public string CustomerContractCode { get; set; }
        public DocumentType DocType { get; set; }
        public ContractType Type { get; set; }
        public int? CompanyId { get; set; }
        public ContractStatus Status { get; set; }
        public int? GroupId { get; set; }
        public DateTime? SigningDate { get; set; }
        public int? CustomerId { get; set; }
        public decimal? ContractValue { get; set; }
        public string Branch { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public string Content { get; set; }
        public string Email { get; set; }
        public string ContactName { get; set; }
        public string ContactAddress { get; set; }
        public DateTime CreatedOn { get; set; }
        public int CreatedBy { get; set; }
        public ContractPriority? Priority { get; set; }
        public string Attachments { get; set; }
        public string Note { get; set; }
        /// <summary>
        /// Dư nợ đầu kỳ
        /// </summary>
        public decimal? OpeningBalance { get; set; }
    }
}
