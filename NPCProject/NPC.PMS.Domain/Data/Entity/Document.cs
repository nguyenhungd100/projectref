﻿using NPC.PMS.Domain.Enums;
using System;

namespace NPC.PMS.Domain.Data.Entity
{
    public class Document
    {
        public Guid Id { get; set; } = Guid.NewGuid();
        public StorageProvider Provider { set; get; }

        public long StorageId { set; get; }

        public string Mime { set; get; }

        public string Title { set; get; }

        public DateTime CreatedUtcTime { set; get; }

        public long Size { set; get; }

        public FileStatus Status { set; get; }
    }
}
