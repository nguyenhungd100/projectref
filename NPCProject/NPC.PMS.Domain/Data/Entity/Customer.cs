﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NPC.PMS.Domain.Data.Entity
{
    public class Customer : BaseEntity
    {
        public string Code { get; set; }
        public string Name { get; set; }
        public string Phone { get; set; }
        public string Address { get; set; }
        public string Email { get; set; }

        public bool? IsExternalEVN { get; set; }

        public bool? IsExternalEVNNPC { get; set; }
    }
}
