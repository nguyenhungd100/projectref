﻿using NPC.PMS.Domain.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace NPC.PMS.Domain.Data.Entity
{
    public class Quote : BaseEntity
    {
        public Guid UID { get; set; } = Guid.NewGuid();
        public string Code { get; set; }

        public DocumentType DocType { get; set; } = DocumentType.Contract;
        public int? CompanyId { get; set; }
        public string Departments { get; set; }
        public QuoteStatus Status { get; set; }
        public DateTime? SigningDate { get; set; }
        public int? CustomerId { get; set; }
        public decimal? Value { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public string Content { get; set; }
        public DateTime CreatedOn { get; set; }
        public int CreatedBy { get; set; }
        public string Attachments { get; set; }
        public string Note { get; set; }
    }
}
