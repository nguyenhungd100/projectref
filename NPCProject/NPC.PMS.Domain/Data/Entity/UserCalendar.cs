﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace NPC.PMS.Domain.Data.Entity
{
    public class UserCalendar : BaseEntity
    {
        public int UserId { get; set; }
        public DateTime DateFrom { get; set; }
        public DateTime DateTo { get; set; }
        public UserCalendarType Type { get; set; }
        public string Note { get; set; }
    }
    public enum UserCalendarType : int
    {
        [Description("Ngày đi làm")]
        NormalDate =0,
        [Description("Nghỉ phép")]
        PaidLeave = 1,
        [Description("Nghỉ bù")]
        Compensate = 2,
        [Description("Công tác")]
        Mission = 3
    }
}
