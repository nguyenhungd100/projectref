﻿/// <summary>
/// Vai trò của hệ thống
/// </summary>

namespace NPC.PMS.Domain.Data.Entity
{
    using System;
    using System.Collections.Generic;
    
    public partial class Role : BaseEntity
    {
        public string RoleName { get; set; }
        public string Description { get; set; }
        public string Title { get; set; }
        public string Permissions { get; set; }
        public int? DisplayOrder { get; set; }
    }
}
