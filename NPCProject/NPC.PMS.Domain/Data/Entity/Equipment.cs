﻿using NPC.PMS.Domain.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace NPC.PMS.Domain.Data.Entity
{

    public class Equipment
    {
        public int Id { get; set; }

        public string CardNumberTSCD { get; set; } // Số thẻ TSCĐ

        public Department DepartmentId { get; set; }// Phòng chuyên môn

        public string NameForPCM { get; set; }// tên thiết bị theo phòng chuyên môn

        public string CodeByPCM { get; set; } // mã thiết bị theo phòng 

        public string Code { get; set; } // mã thiết bị

        public string Imei { get; set; }// số chế tạo

        public EquipmentStatus Status { get; set; } // Tình trạng thiết bị

        public DateTime? ExpiriteDate { get; set; } // Hạn kiểm định

        public string NameForCompany { get; set; }// Tên thiết bị theo công ty

        public string ClassifyTS { get; set; } // Phân loại TS

        public string CalculationUnit { get; set; }

        public int? Quantity { get; set; }

        public DateTime? DatePutIntoUse { get; set; } // Ngày đưa vào sử dụng

        public string IndividualIsHolding { get; set; } // Cá nhân đang cầm
    }
}
