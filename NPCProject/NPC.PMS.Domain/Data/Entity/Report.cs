﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace NPC.PMS.Domain.Data.Entity
{
    public class Report
    {
        public int Id { get; set; }
        public int UserId { get; set; }
        public DateTime Time { get; set; }
        //Đợt của dự án
        public int PhaseId { get; set; }
        public int? UnitId { get; set; }
        public ReportStatus Status { get; set; }
        //Tiến độ hoàn thành
        public string Content { get; set; }
        public string Logs { get; set; }
    }
    public enum ReportStatus : int
    {
        [Description("Chờ duyệt")]
        Submited = 1,
        [Description("Đã duyệt")]
        Approved = 5,
        [Description("Từ chối")]
        Rejected = 10
    }
    public class Reportitem
    {
        public int Id { get; set; }
        public int ReportId { get; set; }
        //Hạng mục
        public int WorkitemId { get; set; }
        //Số lượng báo cáo
        public int Quantity { get; set; }
    }
}
