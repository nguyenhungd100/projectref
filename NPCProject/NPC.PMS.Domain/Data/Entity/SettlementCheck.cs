﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NPC.PMS.Domain.Data.Entity
{
    //Bảng kiểm tra hồ sơ thanh toán
    public class SettlementCheck
    {
        public int Id { get; set; }
        public int SettlementId { get; set; }
        public int SettlementDocumentId { get; set; }
        public bool Checked { get; set; } = false;
        public DocumentStatus? DocumentStatus { get; set; }
        public string P5Reply { get; set; }
        public string P5Comment { get; set; }
        public string P2Reply { get; set; }
        public string Attachments { get; set; }
    }
    public enum DocumentStatus
    {
        UnComplete = 1,
        Complete=2,

    }
}
