﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NPC.PMS.Domain.Data.Entity
{
    public class MasterPlan : BaseEntity
    {
        public int ContractId { get; set; }
        public int WorkitemId { get; set; }
        public int DepartmentId { get; set; }
    }
}
