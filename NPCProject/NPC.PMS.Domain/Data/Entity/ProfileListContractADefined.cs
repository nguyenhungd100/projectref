﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NPC.PMS.Domain.Data.Entity
{
    public class ProfileListContractADefined : BaseEntity
    {
        public int FormalityCode { get; set; }

        public string Name { get; set; }
    }
}
