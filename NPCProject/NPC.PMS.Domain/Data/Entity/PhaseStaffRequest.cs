﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NPC.PMS.Domain.Data.Entity
{
    public class PhaseStaffRequest : BaseEntity
    {
        public int PhaseId { get; set; }
        public int DepartmentId { get; set; }
        public int Offered { get; set; }
        public int Assigned { get; set; }
    }
}
