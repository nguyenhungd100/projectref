﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NPC.PMS.Domain.Data
{
    /// <summary>
    /// Order with ascending or descending
    /// </summary>
    public enum SortDirection
    {
        /// <summary>
        /// Ascending
        /// </summary>
        Ascending,

        /// <summary>
        /// Descending
        /// </summary>
        Descending
    }
}
