using Microsoft.EntityFrameworkCore;
using NPC.PMS.Domain.Data.Entity;
using NPC.PMS.Domain.Entity;

namespace NPC.PMS.Domain.Data
{
    public partial class DomainContext : DbContext
    {
        public DomainContext(DbContextOptions<DomainContext> options) : base(options)
        {
        }

        public static string ConnectionString { get; private set; }

        public virtual DbSet<Configuration> Configurations { get; set; }
        public virtual DbSet<Notification> Notifications { get; set; }
        public virtual DbSet<Role> Roless { get; set; }
        public virtual DbSet<User> Users { get; set; }
        public virtual DbSet<City> Cities { get; set; }
        public virtual DbSet<Contract> Contracts { get; set; }
        public virtual DbSet<ContractGroup> ContractGroups { get; set; }
        public virtual DbSet<Document> Documents { get; set; }
        public virtual DbSet<BinaryStorage> BinaryStorages { get; set; }
        public virtual DbSet<DiskStorage> DiskStorages { get; set; }
        public virtual DbSet<Debt> Debts { get; set; }
        public virtual DbSet<Customer> Customers { get; set; }
        public virtual DbSet<Equipment> Equipments { get; set; }
        public virtual DbSet<Vehicle> Vehicles { get; set; }
        public virtual DbSet<Workitem> Workitems { get; set; }
        public virtual DbSet<ContractUnit> ContractUnits { get; set; }
        public virtual DbSet<MasterPlan> MasterPlans { get; set; }
        public virtual DbSet<Phase> Phases { get; set; }
        public virtual DbSet<PhaseWorkitem> PhaseWorkitems { get; set; }
        public virtual DbSet<PhaseStaff> PhaseStaffs { get; set; }
        public virtual DbSet<PhaseEquipment> PhaseEquipments { get; set; }
        public virtual DbSet<PhaseVehicle> PhaseVehicles { get; set; }

        public virtual DbSet<PhaseEquipmentAllocators> PhaseEquipmentAllocators { get; set; }
        public virtual DbSet<Company> Companies { get; set; }
        public virtual DbSet<Settlement> Settlements { get; set; }
        public virtual DbSet<SettlementPayment> SettlementPayments { get; set; }
        public virtual DbSet<SettlementDocument> SettlementDocuments { get; set; }
        public virtual DbSet<SettlementCheck> SettlementChecks { get; set; }
        public virtual DbSet<PhaseStaffRequest> PhaseStaffRequests { get; set; }
        public virtual DbSet<Contracta> Contractas { get; set; }
        public virtual DbSet<ContractAGroup> ContractAGroups { get; set; }
        public virtual DbSet<Report> Reports { get; set; }
        public virtual DbSet<Reportitem> Reportitems { get; set; }
        public virtual DbSet<ExperimentRecord> ExperimentRecords { get; set; }
        public virtual DbSet<Centre> Centres { get; set; }
        public virtual DbSet<UserDevice> UserDevices { get; set; }
        public virtual DbSet<UserCalendar> UserCalendars { get; set; }
        public virtual DbSet<ContractUser> ContractUsers { get; set; }

        public virtual DbSet<ContractAUser> ContractAUsers { get; set; }
        public virtual DbSet<Quote> Quotes { get; set; }
        public virtual DbSet<QuoteStaff> QuoteStaffs { get; set; }
        public virtual DbSet<QuoteWorkitem> QuoteWorkitems { get; set; }
        public virtual DbSet<QuoteRecord> QuoteRecords { get; set; }
        public virtual DbSet<ProfileListContractA> ProfileListContractAs { get; set; }
        public virtual DbSet<ProfileListContractADefined> ProfileListContractADefineds { get; set; }

        public virtual DbSet<Delegacy> Delegacies { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Configuration>().ToTable("Configuration");
            modelBuilder.Entity<Notification>().ToTable("Notification");
            modelBuilder.Entity<Role>().ToTable("Roles");
            modelBuilder.Entity<User>().ToTable("Users");
            modelBuilder.Entity<City>().ToTable("City");
            modelBuilder.Entity<Contract>().ToTable("Contracts");
            modelBuilder.Entity<ContractGroup>().ToTable("ContractGroups");
            modelBuilder.Entity<Document>().ToTable("F_Documents");
            modelBuilder.Entity<BinaryStorage>().ToTable("F_BinaryStorage");
            modelBuilder.Entity<DiskStorage>().ToTable("F_DiskStorage");
            modelBuilder.Entity<Debt>().ToTable("Debts");
            modelBuilder.Entity<Customer>().ToTable("Customers");
            modelBuilder.Entity<Equipment>().ToTable("Equipments");
            modelBuilder.Entity<Vehicle>().ToTable("Vehicles");
            modelBuilder.Entity<Workitem>().ToTable("Workitems");
            modelBuilder.Entity<ContractUnit>().ToTable("ContractUnits");
            modelBuilder.Entity<MasterPlan>().ToTable("MasterPlans");
            modelBuilder.Entity<PhaseWorkitem>().ToTable("PhaseWorkitems");
            modelBuilder.Entity<PhaseStaff>().ToTable("PhaseStaffs");
            modelBuilder.Entity<PhaseEquipment>().ToTable("PhaseEquipments");
            modelBuilder.Entity<PhaseVehicle>().ToTable("PhaseVehicles");
            modelBuilder.Entity<PhaseEquipmentAllocators>().ToTable("PhaseEquipmentAllocators");
            modelBuilder.Entity<Company>().ToTable("Companies");
            modelBuilder.Entity<Settlement>().ToTable("Settlements");
            modelBuilder.Entity<SettlementPayment>().ToTable("SettlementPayments");
            modelBuilder.Entity<SettlementDocument>().ToTable("SettlementDocuments");
            modelBuilder.Entity<SettlementCheck>().ToTable("SettlementChecks");
            modelBuilder.Entity<PhaseStaffRequest>().ToTable("PhaseStaffRequests");
            modelBuilder.Entity<Contracta>().ToTable("Contractas");
            modelBuilder.Entity<ContractAGroup>().ToTable("ContractAGroups");
            modelBuilder.Entity<Report>().ToTable("Reports");
            modelBuilder.Entity<Reportitem>().ToTable("Reportitems");
            modelBuilder.Entity<ExperimentRecord>().ToTable("ExperimentRecords");
            modelBuilder.Entity<Centre>().ToTable("Centres");
            modelBuilder.Entity<UserDevice>().ToTable("UserDevices");
            modelBuilder.Entity<UserCalendar>().ToTable("UserCalendars");
            modelBuilder.Entity<ContractUser>().ToTable("ContractUsers");
            modelBuilder.Entity<ContractAUser>().ToTable("ContractAUsers");
            modelBuilder.Entity<Quote>().ToTable("Quotes");
            modelBuilder.Entity<QuoteStaff>().ToTable("QuoteStaffs");
            modelBuilder.Entity<QuoteWorkitem>().ToTable("QuoteWorkitems");
            modelBuilder.Entity<QuoteRecord>().ToTable("QuoteRecords");
            modelBuilder.Entity<ProfileListContractA>().ToTable("ProfileListContractAs");
            modelBuilder.Entity<ProfileListContractADefined>().ToTable("ProfileListContractADefineds");
            modelBuilder.Entity<Delegacy>().ToTable("Delegacies");
        }
    }
}
