﻿using AutoMapper;
using AutoMapper.EquivalencyExpression;

using NPC.PMS.Domain.Data.Entity;
using NPC.PMS.Domain.Models;
using NPC.PMS.Domain.Models.ContractA;
using NPC.PMS.Domain.Models.ContractB;
using NPC.PMS.Domain.Models.Quotes;
using NPC.PMS.Domain.Models.Reports;
using NPC.PMS.Domain.Models.Settlements;
using NPC.PMS.Domain.Models.Users;
using NPC.PMS.Domain.Models.Vehicles;
using NPC.PMS.Domain.Services.Common.Devices;
using System.Collections.Generic;

namespace NPC.PMS.Domain.Services
{
    public static class DomainMaps
    {
        public static IMapper Mapper { get; set; }

        public static void Config()
        {
            var config = new MapperConfiguration(cfg =>
            {
                cfg.AddCollectionMappers();
                cfg.CreateMap<User, UserModel>();
                cfg.CreateMap<Role, RoleModel>();
                cfg.CreateMap<Contract, ContractModel>();
                cfg.CreateMap<ContractUser, ContractUserModel>();
                cfg.CreateMap<Workitem, WorkitemModel>();
                cfg.CreateMap<MasterPlan, MasterPlanModel>();
                cfg.CreateMap<Phase, PhaseModel>();
                cfg.CreateMap<PhaseStaff, PhaseStaffModel>();
                cfg.CreateMap<Equipment, EquipmentModel>();
                cfg.CreateMap<PhaseEquipment, PhaseEquipmentModel>();
                cfg.CreateMap<Vehicle, VehicleModel>();
                cfg.CreateMap<Reportitem, ReportitemModel>();
                cfg.CreateMap<Report, ReportModel>();
                cfg.CreateMap<Report, ReportInfoModel>();
                cfg.CreateMap<ExperimentRecord, RecordModel>();
                cfg.CreateMap<Contracta, ContractaModel>();
                cfg.CreateMap<ContractaSaveModel, Contracta>();
                cfg.CreateMap<SettlementCheck, SettlementCheckModel>();
                cfg.CreateMap<Settlement, SettlementModel>();
                cfg.CreateMap<ContractModel, SyntheticReportModel>();
                cfg.CreateMap<Quote, QuoteModel>();
                cfg.CreateMap<QuoteWorkitem, QuoteWorkitemModel>();
                cfg.CreateMap<QuoteRecord, QuoteRecordModel>();
                cfg.CreateMap<PhaseEquipmentAllocators, PhaseEquipmentAllocatorModel>();
            });
            Mapper = config.CreateMapper();
        }

        public static TTo Map<TTo>(this object entity)
        {
            return Mapper.Map<TTo>(entity);
        }
    }
}
