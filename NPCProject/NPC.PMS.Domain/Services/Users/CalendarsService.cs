﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NPC.PMS.Domain.Data;
using NPC.PMS.Domain.Data.Entity;
using NPC.PMS.Domain.Enums;
using NPC.PMS.Domain.Models.Calendars;
using NPC.PMS.Domain.Models.ContractB;
using NPC.PMS.Framework.Extensions.Enums;
using NPC.PMS.Framework.Utils;

namespace NPC.PMS.Domain.Services.Users
{
    public class CalendarsService : ICalendarsService
    {
        private readonly IRepository<UserCalendar> userCalendarsRepository;
        private readonly IRepository<PhaseStaff> phaseStaffRepository;
        private readonly IRepository<User> userRepository;

        public CalendarsService(IRepository<UserCalendar> userCalendarsRepository,
            IRepository<PhaseStaff> phaseStaffRepository,
            IRepository<User> userRepository)
        {
            this.userCalendarsRepository = userCalendarsRepository;
            this.phaseStaffRepository = phaseStaffRepository;
            this.userRepository = userRepository;
        }
        public CalendarsFilterResult List(UserCalendarsFilter filter)
        {
            var result = new CalendarsFilterResult();
            using (var db = userCalendarsRepository.GetDbContext())
            {
                var q = db.UserCalendars.AsQueryable();
                var entities = q.ToList();


                var userCurrent = userRepository.GetById(filter.UserCurrentId);
                var userInDepartments = userRepository.Fetch(c => c.DepartmentId == userCurrent.DepartmentId);

                if (userInDepartments.Count() > 0)
                {
                    var userIds = userInDepartments.Select(t => t.Id).Distinct();

                    var models = new List<UserCalendarsListModel>();

                    var users = userRepository.Fetch(t => userIds.Contains(t.Id));

                    //Ngày đi công trình
                    var phaseStaffs = phaseStaffRepository.Fetch(t => userIds.Contains(t.StaffId));
                    foreach (var userId in userIds)
                    {
                        var calendars = entities.FindAll(t => t.UserId == userId);
                        var model = new UserCalendarsListModel();
                        model.UserId = userId;
                        var user = users.FirstOrDefault(t => t.Id == userId);
                        model.UserName = user.FullName;
                        var departmentId = (Department)user.DepartmentId;
                        model.DepartmentName = departmentId.ToDescription();
                        model.UserCalendars = calendars.CloneToListModels<UserCalendar, UserCalendarModel>();
                        foreach (var calendar in model.UserCalendars)
                        {
                            calendar.TypeName = calendar.Type.ToDescription();
                        }
                        //Ngày đi công trình
                        var phaseStaff = phaseStaffs.FindAll(t => t.StaffId == userId);
                        model.phaseCalendars = phaseStaff.CloneToListModels<PhaseStaff, PhaseCalendarModel>();

                        models.Add(model);
                    }
                    var total = models.Count();
                    var res = models.OrderByDescending(c => c.UserId).Skip((filter.PageIndex - 1) * filter.PageSize).Take(filter.PageSize).ToList();
                    //Check department
                    if (filter.DepartmentId.HasValue)
                    {
                        userIds = users.FindAll(t => t.DepartmentId == filter.DepartmentId).Select(t => t.Id);
                        res = res.Where(t => userIds.Contains(t.UserId)).ToList();
                    }

                    //Lọc theo tháng năm
                    if (filter.Mouth.HasValue && filter.Year.HasValue)
                    {
                        foreach (var item in res)
                        {
                            item.UserCalendars = item.UserCalendars.Where(t => t.DateFrom.Month == filter.Mouth &&
                                    t.DateFrom.Year == filter.Year &&
                                    t.DateTo.Month == filter.Mouth &&
                                    t.DateTo.Year == filter.Year).ToList();
                            item.phaseCalendars = item.phaseCalendars.Where(t => t.StartTime.Month == filter.Mouth &&
                                    t.StartTime.Year == filter.Year &&
                                    t.EndTime.Month == filter.Mouth &&
                                    t.EndTime.Year == filter.Year).ToList();
                        }
                    }
                    else
                    {
                        foreach (var item in res)
                        {
                            item.UserCalendars = item.UserCalendars.Where(t => t.DateFrom.Month == DateTime.Now.Month &&
                                    t.DateFrom.Year == DateTime.Now.Year &&
                                    t.DateTo.Month == DateTime.Now.Month &&
                                    t.DateTo.Year == DateTime.Now.Year).ToList();
                            item.phaseCalendars = item.phaseCalendars.Where(t => t.StartTime.Month == DateTime.Now.Month &&
                                    t.StartTime.Year == DateTime.Now.Year &&
                                    t.EndTime.Month == DateTime.Now.Month &&
                                    t.EndTime.Year == DateTime.Now.Year).ToList();
                        }
                    }
                    result.Filter = filter;
                    result.Records = res;
                    result.TotalRecord = total;

                }
            }
            return result;
        }


        public CalendarsFilterResult GetReportCalendar(UserCalendarsFilter filter)
        {
            var result = new CalendarsFilterResult();
            using (var db = userCalendarsRepository.GetDbContext())
            {
                var q = db.UserCalendars.AsQueryable();
                var entities = q.ToList();

                //chi lay user thuoc cac phong chuyen mon
                var users = db.Users.Where(
                    c => c.DepartmentId >= 10
                    && c.DepartmentId <= 20
                    );
                if (filter.DepartmentId != null)
                    users = users.Where(c => c.DepartmentId == filter.DepartmentId);
                var total = users.Count();

                var models = users.OrderBy(c => c.FullName)
                        .Select(c => new
                        {
                            UserId = c.Id,
                            DepartmentId = c.DepartmentId,
                            c.FullName,

                        }).Skip((filter.PageIndex - 1) * filter.PageSize).Take(filter.PageSize).ToList();

                var res = models.Select(c => new UserCalendarsListModel()
                {
                    DepartmentName = ((Department)c.DepartmentId).ToDescription(),
                    UserId = c.UserId,
                    UserName = c.FullName,

                }).ToList();

                var userIds = res.Select(c => c.UserId).ToList();
                var reportMonth = DateTime.Now.AddMonths(-1);
                var month = reportMonth.Month;
                var year = reportMonth.Year;

                var usersCalendar = db.UserCalendars.Where(c =>

                userIds.Contains(c.UserId)
                && ((c.DateFrom.Month == month
                    && c.DateFrom.Year == year)
                    || (c.DateTo.Month == month && c.DateTo.Year == year)
                )

                ).ToList();

                var phaseCalendars = db.PhaseStaffs.Where(c =>
                        userIds.Contains(c.StaffId)
                        && ((c.StartTime.Year == year && c.StartTime.Month == month)
                        || (c.EndTime.Year == year && c.EndTime.Month == month)
                        )
                    ).ToList();
                foreach (var item in res)
                {
                    item.phaseCalendars = phaseCalendars.Where(c => c.StaffId == item.UserId).ToList().CloneToListModels<PhaseStaff, PhaseCalendarModel>();
                    item.UserCalendars = usersCalendar.Where(c => c.UserId == item.UserId).ToList().CloneToListModels<UserCalendar, UserCalendarModel>();
                }


                result.Filter = filter;
                result.Records = res;
                result.TotalRecord = total;

            }

          
            return result;
        }


        public UserCalendarsListModel Save(UserCalendarsListModel model)
        {
            throw new NotImplementedException();
        }
    }
}
