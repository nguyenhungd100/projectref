﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using NPC.PMS.Domain.Entity;
using NPC.PMS.Domain.Models;
using NPC.PMS.Domain.Models.Users;
using NPC.PMS.Framework.Utils;
using NPC.PMS.Domain.Enums;
using NPC.PMS.Domain.Data;
using NPC.PMS.Domain.Data.Entity;
using NPC.PMS.Framework.Extensions.Enums;
using NPC.PMS.Domain.Models.Calendars;

namespace NPC.PMS.Domain.Services.Users
{
    public class UserService : IUserService
    {
        private readonly IRepository<User> userRepository;
        private readonly IRepository<Role> roleRepository;
        private readonly IRepository<UserDevice> userDeviceRepository;
        private readonly IRepository<UserCalendar> userCalendarRepository;
        private readonly IRepository<Delegacy> delegacyRepository;

        public UserService(
            IRepository<User> userRepository,
            IRepository<Role> roleRepository,
            IRepository<UserDevice> userDeviceRepository,
            IRepository<UserCalendar> userCalendarRepository,
            IRepository<Delegacy> delegacyRepository)
        {
            this.userRepository = userRepository;
            this.roleRepository = roleRepository;
            this.userDeviceRepository = userDeviceRepository;
            this.userCalendarRepository = userCalendarRepository;
            this.delegacyRepository = delegacyRepository;
        }
        public UserModel Add(UserModel user)
        {
            var entity = user.Clone<UserModel, User>();
            var res = userRepository.Insert(entity);
            var model = entity.Clone<User, UserModel>();
            return model;
        }
        public bool Delete(int[] Ids)
        {
            return userRepository.DeleteMany(c => Ids.Contains(c.Id)) > 0;
        }
        public UserModel GetById(int userId)
        {
            var entity = userRepository.GetById(userId);
            return entity.Clone<User, UserModel>();
        }
        public bool Update(UserModel user)
        {
            var entity = user.Clone<UserModel, User>();
            return userRepository.Update(entity);
        }
        public bool VerifyPassword(string userName, string encryptedPassword, ref UserModel userModel)
        {
            var user = userRepository.FirstOrDefault(u => u.Email == userName || u.Mobile == userName);
            if (user.Password == encryptedPassword)
            {
                userModel = user.Clone<User, UserModel>();
                return true;
            }
            userModel = null;
            return false;
        }
        public BaseFilterResult<UserModel> Search(UserFilterModel userFilterModel)
        {
            int totalRecord = 0;

            var q = userRepository.GetDbContext().Users.AsQueryable();

            if (userFilterModel.DepartmentId.HasValue)
            {
                q = q.Where(u => u.DepartmentId == (int)userFilterModel.DepartmentId.Value);
            }
            if (!string.IsNullOrEmpty(userFilterModel.Text))
            {
                q = q.Where(u => u.FullName.Contains(userFilterModel.Text) || u.Email.Contains(userFilterModel.Text));
            }
            if (userFilterModel.UserStatus.HasValue)
            {
                q = q.Where(u => u.Status == userFilterModel.UserStatus.Value);
            }
            totalRecord = q.Count();
            var entities = q.OrderByDescending(c => c.Id).Skip((userFilterModel.PageIndex - 1) * userFilterModel.PageSize).Take(userFilterModel.PageSize).ToList();
            var records = DomainMaps.Mapper.Map<List<UserModel>>(entities);
            var roles = roleRepository.Fetch();
            foreach (var record in records)
            {
                record.StatusName = record.Status.ToDescription();
                if (string.IsNullOrEmpty(record.UserRoles)) continue;
                var userRoles = record.UserRoles.Split(';');
                var roleEntities = roles.Where(r => userRoles.Contains(r.Id.ToString())).ToList();
                var roleNames = string.Empty;
                roleEntities.ForEach(r => roleNames += r.RoleName + " ");
                record.RoleNames = roleNames;

            }
            return new BaseFilterResult<UserModel>()
            {
                Filter = userFilterModel,
                Records = records,
                TotalRecord = totalRecord
            };
        }
        public int CreateUser(CreateUserModel createUserModel)
        {
            var test = userRepository.Fetch(u => u.Mobile == createUserModel.Phone);
            if (userRepository.Fetch(u => u.Mobile == createUserModel.Phone).Count() != 0)
            {
                throw new ServiceException("Số điện thoại đã tồn tại");
            }

            if (userRepository.Fetch(u => u.Email == createUserModel.Email).Count() != 0)
            {
                throw new ServiceException("Email đã tồn tại");
            }

            var user = new User
            {
                FullName = createUserModel.FullName,
                Password = EncryptUtil.EncryptMD5(createUserModel.Password),
                Mobile = createUserModel.Phone,
                Email = createUserModel.Email,
                Status = Enums.UserStatus.Actived,
                UserRoles = createUserModel.Roles,
                DepartmentId = createUserModel.DepartmentId,
                Position = createUserModel.Position
            };

            if (!userRepository.Insert(user)) throw new ServiceException("Cann't create account");
            //GenerateAndSendOTP(user.Id, user.Mobile);
            return user.Id;
        }

        private void GenerateAndSendOTP(int userId, string phone)
        {
            // generate activation code
            Random generator = new Random();
            String code = generator.Next(111111, 999999).ToString("D4");
        }
        public bool ActiveAccount(int userId, string code)
        {
            //if (code == "112233") { }
            //else
            //{
            // var cachedCode = _cacheManager.StringGet("otp:user-" + userId);
            //if (string.IsNullOrEmpty(cachedCode)) return false;
            //if (code != cachedCode) return false;
            //}

            var user = userRepository.GetById(userId);
            user.Status = Enums.UserStatus.Actived;
            return userRepository.Update(user);
        }
        public void ResendOTP(string phone)
        {
            var user = userRepository.FirstOrDefault(u => u.Mobile == phone);
            if (user == null) throw new ServiceException("Phone number not existed");
            if (user.Status != Enums.UserStatus.NotActived) throw new ServiceException("Can't resend OTP to this account");

            GenerateAndSendOTP(user.Id, user.Mobile);

        }
        public bool UpdateToken(int userId, UpdateTokenModel tokenModel)
        {
            var deviceToken = userDeviceRepository.FirstOrDefault(t => t.UserId == userId);
            if (deviceToken == null)
            {
                return userDeviceRepository.Insert(new UserDevice
                {
                    DeviceId = tokenModel.DeviceId,
                    OS = tokenModel.OS,
                    DeviceToken = tokenModel.DeviceToken,
                    UserId = userId,
                });
            }
            else
            {
                deviceToken.OS = tokenModel.OS;
                deviceToken.DeviceId = tokenModel.DeviceId;
                deviceToken.DeviceToken = tokenModel.DeviceToken;
                return userDeviceRepository.Update(deviceToken);
            }
        }

        public List<UserModel> GetByIds(List<int> ids, int selectTop = 0)
        {
            if (ids == null || ids.Count == 0) return new List<UserModel>();
            var users = userRepository.Fetch(u => ids.Contains(u.Id));
            return DomainMaps.Mapper.Map<List<UserModel>>(users);
        }

        public UserModel GetByUserName(string userName)
        {
            var user = userRepository.FirstOrDefault(u => u.Email == userName);
            if (user == null) throw new ServiceException("User not exist");
            return DomainMaps.Mapper.Map<UserModel>(user);
        }

        public UserModel GetByPhone(string userName)
        {
            var user = userRepository.FirstOrDefault(u => u.Mobile == userName);
            if (user == null) throw new ServiceException("User not exist");
            return DomainMaps.Mapper.Map<UserModel>(user);
        }

        public bool UpdateProfile(UpdateUserProfileModel userProfileModel)
        {
            var user = userRepository.FirstOrDefault(t => t.Id == userProfileModel.UserId);
            user.FullName = userProfileModel.FullName;
            user.Email = userProfileModel.Email;
            user.Mobile = userProfileModel.Mobile;
            return userRepository.Update(user);
        }

        public bool ChangePassword(ChangePasswordModel changePasswordModel)
        {
            var user = userRepository.GetById(changePasswordModel.UserId);
            if (user == null) throw new ServiceException("User not exist");

            user.Password = EncryptUtil.EncryptMD5(changePasswordModel.NewPassword);
            return userRepository.Update(user);
        }

        public async Task<bool> UpdateRole(int userId, string userRoles)
        {
            var result = false;
            if (userId <= 0)
            {
                throw new ServiceException("Hãy chọn user");
            }
            var user = userRepository.GetById(userId);
            if (user == null)
            {
                throw new ServiceException("Không tồn tại user này, kiểm tra lại");
            }
            if (!String.IsNullOrEmpty(userRoles))
            {
                user.UserRoles = userRoles;
                result = await userRepository.UpdateAsync(user);
            }
            return result;
        }

        public bool RemoveUser(int id)
        {
            var user = userRepository.GetById(id);
            if (user != null)
            {
                return userRepository.Delete(user);
            }
            else throw new ServiceException("Người dùng không tồn tại");
        }

        public UserModel UpdateUser(UpdateUserModel updateUserModel)
        {
            var user = userRepository.GetById(updateUserModel.UserId);
            if (user == null)
            {
                throw new ServiceException("Không tồn tại user này, kiểm tra lại");
            }
            user.FullName = updateUserModel.FullName;
            user.Email = updateUserModel.Email;
            user.Mobile = updateUserModel.Phone;
            user.Gender = updateUserModel.Gender;
            user.Avatar = updateUserModel.Avatar;
            user.DepartmentId = updateUserModel.DepartmentId;
            user.Position = updateUserModel.Position;
            user.SafeLevel = updateUserModel.SafeLevel;
            if (!String.IsNullOrEmpty(updateUserModel.Roles))
            {
                user.UserRoles = updateUserModel.Roles;
            }
            //user.Status = (updateUserModel.IsActived) ? UserStatus.Actived : UserStatus.NotActived;
            userRepository.Update(user);
            return DomainMaps.Mapper.Map<UserModel>(user);
        }

        #region User Calendars
        public UserCalendarModel CreateCalendar(UserCalendarModel model)
        {
            model.DateTo = new DateTime(model.DateTo.Year, model.DateTo.Month, model.DateTo.Day, 23, 59, 59);
            var calendars = userCalendarRepository.Fetch(t => t.UserId == model.UserId);
            foreach (var item in calendars)
            {
                int from = DateTime.Compare(item.DateFrom.Date, model.DateFrom.Date);
                int to = DateTime.Compare(item.DateTo.Date, model.DateTo.Date);
                if (from >= 0 && to <= 0)
                {
                    throw new ServiceException("Đã tồn tại lịch nghỉ trong thời gian này");
                }
            }
            var entity = model.Clone<UserCalendarModel, UserCalendar>();

            if (model.Id != 0) userCalendarRepository.Update(entity);
            else userCalendarRepository.Insert(entity);
            return entity.Clone<UserCalendar, UserCalendarModel>();
        }

        public UserCalendarModel GetCalendar(int id, int userId)
        {
            var entity = userCalendarRepository.GetById(id);
            var model = entity.Clone<UserCalendar, UserCalendarModel>();
            model.TypeName = model.Type.ToDescription();
            return model;
        }

        public bool DeleteCalendar(int id, int userId)
        {
            var entity = userCalendarRepository.GetById(id);
            return userCalendarRepository.Delete(entity);
        }

        public List<UserCalendarModel> GetUserCalendar(int userId)
        {
            var entities = userCalendarRepository.Fetch(t => t.UserId == userId);
            var models = entities.CloneToListModels<UserCalendar, UserCalendarModel>();
            foreach (var item in models)
            {
                item.TypeName = item.Type.ToDescription();
            }
            return models;
        }

        public BaseFilterResult<UserCalendarModel> GetListCalendar(FilterUserCalendarModel filter)
        {
            using (var db = userCalendarRepository.GetDbContext())
            {
                var total = 0;
                var q = db.UserCalendars.AsQueryable();
                if (filter.From.HasValue && !filter.To.HasValue) filter.To = DateTime.Now;
                if (filter.From.HasValue && filter.To.HasValue)
                {
                    q = q.Where(t => DateTime.Compare(t.DateFrom, filter.From.Value) >= 0 && DateTime.Compare(t.DateTo, filter.To.Value) <= 0);
                }
                if (!filter.From.HasValue && filter.To.HasValue)
                {
                    q = q.Where(t => DateTime.Compare(t.DateTo, filter.To.Value) <= 0);
                }
                if (filter.Type.HasValue)
                {
                    q = q.Where(t => t.Type == filter.Type);
                }
                total = q.Count();
                var entities = q.OrderByDescending(c => c.Id).Skip((filter.PageIndex - 1) * filter.PageSize).Take(filter.PageSize).ToList();
                var models = entities.CloneToListModels<UserCalendar, UserCalendarModel>();
                foreach (var item in models)
                {
                    item.TypeName = item.Type.ToDescription();
                }
                return new BaseFilterResult<UserCalendarModel>()
                {
                    Filter = filter,
                    Records = models,
                    TotalRecord = total
                };
            }
        }

        public UserModel GetDirector(Department department)
        {
            var user = userRepository.FirstOrDefault(u => u.DepartmentId == (int)department && u.Position == StaffPosition.Director);
            if (user == null) return null;
            return user.Map<UserModel>();
        }

        public IEnumerable<int> UserInSpecializedDivisions(Department department)
        {
            var users = userRepository.Fetch(c => c.DepartmentId == (int)department);
            foreach (var user in users)
            {
                yield return user.Id;
            }
        }

        public List<UserDelegacyModel> GetUserDelegacy(int userCurrentId)
        {
            var delegacyUser = userRepository.GetById(userCurrentId);
            if (delegacyUser.Position == StaffPosition.Director)
            {
                var delegacies = delegacyRepository.Fetch(c => c.DelegacyUserId == userCurrentId);
                if (delegacies.Count() > 0)
                {
                    var userCheckIds = delegacies.Select(c => c.DelegatedUserId);
                    var users = userRepository.Fetch(c => userCheckIds.Contains(c.Id));
                    return (from c in delegacies
                            select new UserDelegacyModel
                            {
                                DelegatedUserId = c.DelegatedUserId,
                                DelegatedUserName = users.FirstOrDefault(a => a.Id == c.DelegatedUserId).FullName
                            }).ToList();

                }
                else return null;
            }
            else return null;
        }

        public bool UserDelegacy(UserDelegacyModel model)
        {
            var result = false;

            var delegatedUser = userRepository.GetById(model.DelegatedUserId);
            if (delegatedUser == null) throw new ServiceException("Tài khoản được ủy quyền không tồn tại");

            var delegacyUser = userRepository.GetById(model.DelegacyUserId);
            if (delegacyUser.Position != StaffPosition.Director) throw new ServiceException("Chức năng này chỉ dành cho trưởng phòng");

            if (model.DelegacyUserId == model.DelegatedUserId)
                throw new ServiceException("Bạn đang tự ủy quyền cho chính mình");

            if (delegacyUser.DepartmentId != delegatedUser.DepartmentId) throw new ServiceException("Chức năng chỉ ủy quyền được cho nhân viên cùng phòng");


            var delegacyExist = delegacyRepository.FirstOrDefault(c => c.DelegacyUserId == model.DelegacyUserId
                && c.DelegatedUserId == model.DelegatedUserId);

            if (delegacyExist != null) throw new ServiceException("Bạn đã được ủy quyền cho " + delegatedUser.FullName + " từ trước");
            else
            {
                result = delegacyRepository.Insert(new Delegacy
                {
                    DelegacyUserId = model.DelegacyUserId,
                    DelegatedUserId = model.DelegatedUserId,
                    RoleDelegated = delegacyUser.UserRoles,
                    OldRole = delegatedUser.UserRoles,
                    PotitionDelegated = delegacyUser.Position.Value,
                    OldPotition = delegatedUser.Position.Value
                });

                if (result)
                {
                    delegatedUser.UserRoles = delegacyUser.UserRoles;
                    delegatedUser.Position = delegacyUser.Position.Value;
                    result = userRepository.Update(delegatedUser);
                }
            }
            return result;
        }

        public bool EvictDelegacy(UserDelegacyModel model)
        {
            var result = false;

            using (var db = delegacyRepository.GetDbContext())
            {
                using (var trans = db.Database.BeginTransaction())
                {
                    try
                    {
                        var delegatedUser = userRepository.GetById(model.DelegatedUserId);
                        if (delegatedUser == null) throw new ServiceException("Tài khoản được ủy quyền không tồn tại");

                        var delegacyUser = userRepository.GetById(model.DelegacyUserId);
                        if (delegacyUser.Position != StaffPosition.Director) throw new ServiceException("Chức năng này chỉ dành cho trưởng phòng");

                        var existDelegacy = delegacyRepository.FirstOrDefault(c => c.DelegacyUserId == model.DelegacyUserId
                            && c.DelegatedUserId == model.DelegatedUserId);

                        if (existDelegacy != null)
                        {
                            delegatedUser.UserRoles = existDelegacy.OldRole;
                            delegatedUser.Position = existDelegacy.OldPotition;
                            result = userRepository.Update(delegatedUser);

                            if (result)
                                result = delegacyRepository.Delete(existDelegacy);
                        }
                        else throw new ServiceException("Bạn chưa ủy quyền cho " + delegatedUser.FullName);
                        if (!result) trans.Rollback();
                    }
                    catch (Exception ex)
                    {
                        trans.Rollback();
                        throw ex;
                    }
                    trans.Commit();
                }
            }


            return result;
        }


        #endregion
    }
}
