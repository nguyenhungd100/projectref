﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;
using NPC.PMS.Domain.Data.Entity;
using NPC.PMS.Domain.Entity;
using NPC.PMS.Domain.Enums;
using NPC.PMS.Domain.Models;
using NPC.PMS.Domain.Models.Calendars;
using NPC.PMS.Domain.Models.Users;

namespace NPC.PMS.Domain.Services.Users
{
    public interface IUserService
    {
        UserModel GetById(int userId);
        UserModel Add(UserModel user);

        bool Update(UserModel user);

        bool Delete(int[] Ids);
        bool VerifyPassword(string userName, string encryptedPassword, ref UserModel userModel);
        BaseFilterResult<UserModel> Search(UserFilterModel userSearchModel);
        int CreateUser(CreateUserModel createUserModel);
        bool ActiveAccount(int userId, string code);
        bool UpdateToken(int userId, UpdateTokenModel tokenModel);
        List<UserModel> GetByIds(List<int> collectingUserIds, int selectTop = 0);
        UserModel GetByUserName(string userName);
        void ResendOTP(string phone);
        UserModel GetByPhone(string userName);
        bool ChangePassword(ChangePasswordModel changePasswordModel);
        bool UpdateProfile(UpdateUserProfileModel userProfileModel);
        Task<bool> UpdateRole(int userId, string userRoles);
        bool RemoveUser(int id);
        UserModel UpdateUser(UpdateUserModel updateUserModel);

        UserCalendarModel CreateCalendar(UserCalendarModel model);
        UserCalendarModel GetCalendar(int id, int userId);
        List<UserCalendarModel> GetUserCalendar(int userId);
        BaseFilterResult<UserCalendarModel> GetListCalendar(FilterUserCalendarModel filter);
        bool DeleteCalendar(int id, int userId);
        UserModel GetDirector(Department department);

        IEnumerable<int> UserInSpecializedDivisions(Department department);

        List<UserDelegacyModel> GetUserDelegacy(int userCurrentId);

        bool UserDelegacy(UserDelegacyModel model);
        
        bool EvictDelegacy(UserDelegacyModel model);
      
    }
}
