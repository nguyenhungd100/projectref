﻿using System;
using System.Collections.Generic;
using System.Text;
using NPC.PMS.Domain.Models.Calendars;

namespace NPC.PMS.Domain.Services.Users
{
    public interface ICalendarsService
    {
        UserCalendarsListModel Save(UserCalendarsListModel model);
        CalendarsFilterResult List(UserCalendarsFilter filter);
        CalendarsFilterResult GetReportCalendar(UserCalendarsFilter filter);
    }
}
