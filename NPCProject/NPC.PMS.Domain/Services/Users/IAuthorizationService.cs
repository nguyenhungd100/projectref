﻿using System.Threading.Tasks;
using NPC.PMS.Domain.Models;

namespace NPC.PMS.Domain.Services.Users
{
    public interface IAuthorizationService
    {
        Task<UserModel> GetByIdAsync(int userId);
        Task<bool> VerifyPasswordAsync(string userName, string encryptedPassword);
        UserModel GetUser(string userName);
    }
}