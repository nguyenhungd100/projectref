﻿using System;
using System.Linq.Expressions;
using System.Threading.Tasks;
using NPC.PMS.Domain.Data;
using NPC.PMS.Domain.Data.Entity;
using NPC.PMS.Domain.Models;
using NPC.PMS.Framework.Utils;

namespace NPC.PMS.Domain.Services.Users
{
    public class AuthorizationService : IAuthorizationService
    {
        private readonly IRepository<User> _userRepository;

        public AuthorizationService(
            IRepository<User> userRepository)
        {
            _userRepository = userRepository;
        }
        public async Task<UserModel> GetByIdAsync(int userId)
        {
            var entity = await _userRepository.GetByIdAsync(userId);
            return entity.Clone<User, UserModel>();
        }

        public UserModel GetUser(string userName)
        {
            var entity = _userRepository.FirstOrDefault(u=>u.Email == userName);
            return entity.Clone<User, UserModel>();
        }

        public async Task<bool> VerifyPasswordAsync(string userName, string encryptedPassword)
        {
            var user = _userRepository.FirstOrDefault(u => u.Email == userName || u.Mobile == userName);
            if (user == null) return false;

            if (user.Password == encryptedPassword)
                return true;
            else
                return false;

        }
    }
}
