﻿using NPC.PMS.Domain.Data;
using NPC.PMS.Domain.Data.Entity;
using NPC.PMS.Domain.Models.Settlements;
using NPC.PMS.Framework.Utils;
using System;
using System.Collections.Generic;
using System.Text;

namespace NPC.PMS.Domain.Services.Settlements
{
    public class SaveSettlementCheck
    {
        IRepository<Settlement> _SettlementRepository;
        IRepository<SettlementDocument> _SettlementDocumentRepository;
        IRepository<SettlementCheck> _SettlementCheckRepository;
        public SaveSettlementCheck(IRepository<Settlement> SettlementRepository,
            IRepository<SettlementDocument> SettlementDocumentRepository,
            IRepository<SettlementCheck> SettlementCheckRepository)
        {
            _SettlementDocumentRepository = SettlementDocumentRepository;
            _SettlementRepository = SettlementRepository;
            _SettlementCheckRepository = SettlementCheckRepository;
        }
        public SaveSettlementCheck()
        {

        }
        public bool SaveSettlement(SettlementSaveModel settlement,int userId)
        {
            var res = false;
            var entity = settlement.Clone<SettlementSaveModel, Settlement>();
            entity.CreatedBy = userId;
            entity.CreatedOn = DateTime.Now;
            entity.Status = SettlemenStatus.Draft;
            entity.No = 1;

            if (settlement.Id != 0) res = _SettlementRepository.Update(entity);
            else
            {
                res = _SettlementRepository.Insert(entity);
                foreach (var item in settlement.Documents)
                {
                    var check = new SettlementCheck();
                    if (item.Selected)
                    {
                        check.SettlementId = settlement.Id;
                        check.SettlementDocumentId = item.DocumentId;
                        check.Checked = true;
                        _SettlementCheckRepository.Insert(check);
                    }
                }
            }
            foreach (var item in settlement.Documents)
            {
                var check = new SettlementCheck();
                if (item.Selected)
                {
                    check.SettlementId = settlement.Id;
                    check.SettlementDocumentId = item.DocumentId;
                    check.Checked = true;
                    _SettlementCheckRepository.Insert(check);
                }
                else
                {
                    check = _SettlementCheckRepository.FirstOrDefault(t => t.SettlementId == settlement.Id && t.SettlementDocumentId == item.DocumentId);
                    if(check != null)
                    _SettlementCheckRepository.Delete(check);
                }
            }
            return res;
        }
    }
}
