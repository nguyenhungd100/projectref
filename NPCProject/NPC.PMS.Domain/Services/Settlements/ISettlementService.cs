﻿using System;
using System.Collections.Generic;
using System.Text;
using NPC.PMS.Domain.Data.Entity;
using NPC.PMS.Domain.Models;
using NPC.PMS.Domain.Models.Settlements;

namespace NPC.PMS.Domain.Services.Settlements
{
    public interface ISettlementService
    {
        bool Save(SettlementSaveModel model,int userId);
        SettlementFilterResult Filter(SettlementFilterModel filter);
        bool Delete(int id);
        SettlementDocumentModel SaveDocument(SettlementDocumentModel model);
        bool DeleteCheck(int id);
        SettlementCheckModel SaveCheck(SettlementCheckModel model);
        bool DeleteDocument(int id);
        List<SettlementCheckModel> ListCheck(BaseFilterModel model);
        SettlementModel Get(int id);
        bool Submit(int id, int userId);
        bool Reject(int id, int userId);
        bool Approve(int id, int userId);
        bool Pass(int id, int userId);
        bool Receive(int id, int userId);
        List<SettlementDocumentModel> GetSettlementDocuments();
        bool Complete(int id, SettlementModel model);
        bool P5Reject(int id, SettlementModel model);

        bool P2Reply(int id, SettlementModel model);

        bool AddPayment(SettlementPaymentModel model);
        bool DeletePayment(int id);
        List<SettlementPaymentModel> GetPayments(int settlementId);
    }
}
