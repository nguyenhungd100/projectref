﻿using Newtonsoft.Json;
using NPC.PMS.Domain.Data;
using NPC.PMS.Domain.Data.Entity;
using NPC.PMS.Domain.Enums;
using NPC.PMS.Domain.Models;
using NPC.PMS.Domain.Models.ContractB;
using NPC.PMS.Domain.Models.Settlements;
using NPC.PMS.Domain.Services.Common.Documents;
using NPC.PMS.Domain.Services.Common.Notifications;
using NPC.PMS.Domain.Services.Users;
using NPC.PMS.Framework.Push;
using NPC.PMS.Framework.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NPC.PMS.Domain.Services.Settlements
{
    public class SettlementService : ISettlementService
    {
        IRepository<Settlement> settlementRepository;
        IRepository<SettlementDocument> settlementDocumentRepository;
        IRepository<SettlementCheck> settlementCheckRepository;
        private readonly IRepository<User> userRepository;
        private readonly INotificationService notificationService;
        private readonly IUserService userService;
        private readonly IDocumentService documentService;
        private readonly IContractBService contractService;
        private readonly IRepository<Contract> contractRepository;
        private readonly IRepository<Contracta> contractaRepository;
        private readonly IRepository<Quote> quoteRepository;

        public SettlementService(IRepository<Settlement> settlementRepository,
            IRepository<SettlementDocument> settlementDocumentRepository,
            IRepository<SettlementCheck> settlementCheckRepository,
            IRepository<User> userRepository,
            INotificationService notificationService,
            IUserService userService,
            IContractBService contractService,
            IRepository<Contract> contractRepository,
            IRepository<Contracta> contractaRepository,
            IDocumentService documentService,
            IRepository<Quote> quoteRepository
            )
        {
            this.settlementDocumentRepository = settlementDocumentRepository;
            this.settlementRepository = settlementRepository;
            this.settlementCheckRepository = settlementCheckRepository;
            this.userRepository = userRepository;
            this.notificationService = notificationService;
            this.userService = userService;
            this.contractService = contractService;
            this.contractRepository = contractRepository;
            this.contractaRepository = contractaRepository;
            this.documentService = documentService;
            this.quoteRepository = quoteRepository;
        }

        public bool Delete(int id)
        {
            var Settlement = settlementRepository.GetById(id);
            var res = settlementRepository.Delete(Settlement);

            //Xóa settlment check
            var checks = settlementCheckRepository.Fetch(t => t.SettlementId == id);
            if (checks.Count > 0)
                foreach (var item in checks)
                {
                    settlementCheckRepository.Delete(item);
                }
            return res;
        }

        public SettlementFilterResult Filter(SettlementFilterModel filter)
        {
            var currentUser = userRepository.GetById(filter.UserId);
            var result = new SettlementFilterResult { Filter = filter };
            var db = settlementRepository.GetDbContext();

            var q = db.Settlements.AsQueryable();
            if (filter.ContractUID.HasValue) q = q.Where(s => s.ContractUID == filter.ContractUID.Value);

            if (currentUser.DepartmentId == (int)Department.TCKT) q = q.Where(s => (int)s.Status >= (int)SettlemenStatus.Passed);

            var settlementes = q.ToList();

            var models = settlementes.Map<List<SettlementModel>>();

            var userids = models.Select(m => m.CreatedBy);
            var receiverIds = models.Select(m => m.ReceiverId);
            var users = userRepository.Fetch(u => userids.Contains(u.Id) || receiverIds.Contains(u.Id));

            var ids = models.Select(s => s.Id);
            var documents = settlementCheckRepository.Fetch(s => ids.Contains(s.SettlementId));

            var contractUIDs = models.Select(s => s.ContractUID);
            var contracts = contractRepository.Fetch(c => contractUIDs.Contains(c.UID));
            var contractas = contractaRepository.Fetch(c => contractUIDs.Contains(c.UID));
            var quotes = quoteRepository.Fetch(c => contractUIDs.Contains(c.UID));
            foreach (var model in models)
            {
                var user = users.FirstOrDefault(u => u.Id == model.CreatedBy);
                if (user != null) model.CreatedName = user.FullName;

                var receiver = users.FirstOrDefault(u => u.Id == model.ReceiverId);
                if (receiver != null) model.ReceiverName = receiver.FullName;

                model.SettlementChecks = documents.Where(d => d.SettlementId == model.Id).ToList().Map<List<SettlementCheckModel>>();
                var payments = db.SettlementPayments.Where(c => c.SettlementId == model.Id).OrderBy(c => c.Id).ToList();
                if (payments.Count > 0)
                {
                    model.ReceivedValue = payments.Sum(c => c.PaymentValue);
                    model.PaymentDate = payments[0].PaymentDate;
                }

                if (contracts.Any(c => c.UID == model.ContractUID))
                {
                    var contract = contracts.FirstOrDefault(c => c.UID == model.ContractUID);
                    model.ContractCode = contract.Code;
                    model.DocumentType = SettlementDocumentType.Contract;
                }
                if (contractas.Any(c => c.UID == model.ContractUID))
                {
                    var contract = contractas.FirstOrDefault(c => c.UID == model.ContractUID);
                    model.ContractCode = contract.Code;
                    model.IsContractA = true;
                    model.DocumentType = SettlementDocumentType.Contracta;

                }

                if (quotes.Any(c => c.UID == model.ContractUID))
                {
                    var contract = quotes.FirstOrDefault(c => c.UID == model.ContractUID);
                    model.ContractCode = contract.Code;
                    model.IsContractA = false;
                    model.IsQuote = true;
                    model.DocumentType = SettlementDocumentType.Contract;

                }
                //get attachment
                foreach (var settlementDocument in model.SettlementChecks)
                {
                    if (!string.IsNullOrWhiteSpace(settlementDocument.Attachments))
                    {
                        var documentIds = JsonConvert.DeserializeObject<List<Guid>>(settlementDocument.Attachments);
                        settlementDocument.AttachmentDocuments = db.Documents.Where(c => documentIds.Contains(c.Id)).ToList();
                    }
                }
            }

            result.Records = models;
            return result;
        }

        public bool Save(SettlementSaveModel model, int userId)
        {
            var res = false;
            if (model.Id > 0) res = UpdateSettlement(model, userId);
            else
            {
                var contract = contractRepository.FirstOrDefault(t => t.UID == model.ContractUID);
                if (contract != null)
                {
                    var status = contract.Status;
                    if (status == ContractStatus.Draft) throw new ServiceException("Hợp đồng nháp không thể tạo lần thanh toán");
                }
                var contracta = contractaRepository.FirstOrDefault(t => t.UID == model.ContractUID);
                if (contracta != null)
                {
                    var status = contracta.Status;
                    if (status == ContractaStatus.Draft) throw new ServiceException("Hợp đồng nháp không thể tạo lần thanh toán");
                }
                res = CreateSettlement(model, userId);
            }
            return res;
        }

        public bool Save2Db(SettlementSaveModel model, int userId)
        {
            var res = false;
            if (model.Id > 0) res = UpdateSettlement(model, userId);
            else
            {
                var contract = contractRepository.FirstOrDefault(t => t.UID == model.ContractUID);
                if (contract != null)
                {
                    var status = contract.Status;
                    if (status == ContractStatus.Draft) throw new ServiceException("Hợp đồng nháp không thể tạo lần thanh toán");
                }
                var contracta = contractaRepository.FirstOrDefault(t => t.UID == model.ContractUID);
                if (contracta != null)
                {
                    var status = contracta.Status;
                    if (status == ContractaStatus.Draft) throw new ServiceException("Hợp đồng nháp không thể tạo lần thanh toán");
                }
                res = CreateSettlement(model, userId);
            }
            return res;
        }
        private bool UpdateSettlement(SettlementSaveModel model, int userId)
        {
            var res = false;
            var entity = settlementRepository.GetById(model.Id);
            entity.Note = model.Note;
            entity.Value = model.Value;
            entity.PaymentDate = model.PaymentDate;
            entity.PaymentNumber = model.PaymentNumber;
            res = settlementRepository.Update(entity);
            if (model.Documents == null)
                model.Documents = new List<DocumentCheckModel>();
            if (model.SettlementDocumentChecks == null)
                model.SettlementDocumentChecks = new List<SettlementCheckModel>();
            foreach (var item in model.Documents)
            {
                var check = new SettlementCheck();
                if (item.Selected)
                {
                    check.SettlementId = entity.Id;
                    check.SettlementDocumentId = item.DocumentId;
                    settlementCheckRepository.Insert(check);
                }
                else
                {
                    check = settlementCheckRepository.FirstOrDefault(t => t.SettlementId == model.Id && t.SettlementDocumentId == item.DocumentId);
                    if (check != null)
                        settlementCheckRepository.Delete(check);
                }
            }

            foreach (SettlementCheckModel item in model.SettlementDocumentChecks)
            {
                item.SettlementId = entity.Id;
                if (item.Checked)
                {
                    if (item.Id == 0)
                    {
                        settlementCheckRepository.Insert(item.Map<SettlementCheck>());
                    }
                    else
                    {
                        settlementCheckRepository.Update(item.Map<SettlementCheck>());
                    }


                }
                else
                {
                    var check = settlementCheckRepository.FirstOrDefault(t => t.SettlementId == model.Id && t.SettlementDocumentId == item.SettlementDocumentId);
                    if (check != null)
                        settlementCheckRepository.Delete(check);
                }

            }
            return res;
        }
        private bool CreateSettlement(SettlementSaveModel model, int userId)
        {
            var res = false;
            var entity = new Settlement();
            entity.Note = model.Note;
            entity.Value = model.Value;

            // kiem tra xem co lan thanh toan truoc chua hoan thanh
            //if (settlementRepository.Any(s => s.ContractUID == model.ContractUID && s.Status != SettlemenStatus.Received))
            //    throw new ServiceException("Bạn không thể thêm lần thanh toán mới khi đang có lần thanh toán chưa được duyệt hoặc chưa được P5 nhận.");

            var count = settlementRepository.Count(s => s.ContractUID == model.ContractUID);
            entity.No = count + 1;
            entity.Status = SettlemenStatus.Draft;
            entity.ContractUID = model.ContractUID;
            entity.CreatedBy = userId;
            entity.CreatedOn = DateTime.Now;
            entity.PaymentDate = model.PaymentDate;
            entity.PaymentNumber = model.PaymentNumber;
            res = settlementRepository.Insert(entity);
            if (model.Documents == null)
                model.Documents = new List<DocumentCheckModel>();
            if (model.SettlementDocumentChecks == null)
                model.SettlementDocumentChecks = new List<SettlementCheckModel>();
            foreach (var item in model.Documents)
            {
                var check = new SettlementCheck();
                if (item.Selected)
                {
                    check.SettlementId = entity.Id;
                    check.SettlementDocumentId = item.DocumentId;
                    settlementCheckRepository.Insert(check);
                }
                else
                {
                    check = settlementCheckRepository.FirstOrDefault(t => t.SettlementId == model.Id && t.SettlementDocumentId == item.DocumentId);
                    if (check != null)
                        settlementCheckRepository.Delete(check);
                }
            }

            foreach (SettlementCheckModel item in model.SettlementDocumentChecks)
            {
                item.SettlementId = entity.Id;
                if (item.Checked)
                {
                    if (item.Id == 0)
                    {
                        settlementCheckRepository.Insert(item.Map<SettlementCheck>());
                    }
                    else
                    {
                        settlementCheckRepository.Update(item.Map<SettlementCheck>());
                    }
                }
                else
                {
                    var check = settlementCheckRepository.FirstOrDefault(t => t.SettlementId == model.Id && t.SettlementDocumentId == item.SettlementDocumentId);
                    if (check != null)
                        settlementCheckRepository.Delete(check);
                }

            }
            return res;
        }

        public SettlementDocumentModel SaveDocument(SettlementDocumentModel model)
        {
            if (model.DocumentName == null) throw new ServiceException("Chưa nhập tên!");
            var settlementDocument = model.Map<SettlementDocument>();
            if (model.Id != 0) settlementDocumentRepository.Update(settlementDocument);
            else settlementDocumentRepository.Insert(settlementDocument);
            return settlementDocument.Map<SettlementDocumentModel>();
        }

        public bool DeleteDocument(int id)
        {
            var SettlementDocument = settlementDocumentRepository.GetById(id);
            var res = settlementDocumentRepository.Delete(SettlementDocument);
            return res;
        }

        public SettlementCheckModel SaveCheck(SettlementCheckModel model)
        {
            if (model.SettlementId == 0) throw new ServiceException("Chưa nhập hồ sơ!");
            if (model.SettlementDocumentId == 0) throw new ServiceException("Chưa nhập tài liệu!");
            var settlementCheck = model.Map<SettlementCheck>();
            if (model.Id != 0) settlementCheckRepository.Update(settlementCheck);
            else settlementCheckRepository.Insert(settlementCheck);
            return settlementCheck.Map<SettlementCheckModel>();
        }

        public bool DeleteCheck(int id)
        {
            var SettlementCheck = settlementCheckRepository.GetById(id);
            var res = settlementCheckRepository.Delete(SettlementCheck);
            return res;
        }

        public List<SettlementCheckModel> ListCheck(BaseFilterModel filter)
        {
            using (var db = settlementCheckRepository.GetDbContext())
            {
                var q = db.SettlementChecks.AsQueryable();
                var entities = q.OrderByDescending(c => c.Id).Skip((filter.PageIndex - 1) * filter.PageSize).Take(filter.PageSize).ToList();
                var models = entities.Map<List<SettlementCheckModel>>();
                //var models = new List<SettlementCheckModel>();
                //foreach (var item in entities)
                //{
                //    var settlements = db.Settlements.AsQueryable().Where(t => t.Id == item.ContractSettlementId).FirstOrDefault();
                //    var settlementDocument = db.SettlementDocuments.AsQueryable().Where(t => t.Id == item.SettlementDocumentId).FirstOrDefault();
                //}
                return models;
            }
        }

        public SettlementModel Get(int id)
        {
            var entity = settlementRepository.GetById(id);
            return entity.Map<SettlementModel>();
        }

        public List<SettlementDocumentModel> GetSettlementDocuments()
        {
            var settlementDocuments = settlementDocumentRepository.Fetch();
            var model = settlementDocuments.Map<List<SettlementDocumentModel>>();
            return model;
        }

        public bool Submit(int id, int userId)
        {
            var entity = settlementRepository.GetById(id);
            if (entity.Status != SettlemenStatus.Draft && entity.Status != SettlemenStatus.Rejected)
                throw new ServiceException("Không thể trình khi lần thanh toán đang ở trạng thái bản nháp hoặc trả lại");
            entity.Status = SettlemenStatus.Submited;
            settlementRepository.Update(entity);

            var contractCode = string.Empty;
            var message = string.Empty;
            var director = userService.GetDirector(Department.KHDT);
            var submitor = userService.GetById(userId);

            var contract = contractRepository.FirstOrDefault(c => c.UID == entity.ContractUID);
            if (contract != null)
            {
                contractCode = contract.Code;
                if (contract.DocType == DocumentType.Contract)
                {
                    message = string.Format("{0} trình duyệt hồ sơ thanh toán lần {1} Hợp đồng B {2}", submitor.FullName, entity.No, contractCode);
                }
                if (contract.DocType == DocumentType.Quotation)
                {
                    message = string.Format("{0} trình duyệt hồ sơ thanh toán lần {1} Báo giá Hợp đồng B {2}", submitor.FullName, entity.No, contractCode);
                }
            }

            var quote = quoteRepository.FirstOrDefault(c => c.UID == entity.ContractUID);
            if (quote != null)
            {
                contractCode = quote.Code;
                if (quote.DocType == DocumentType.Contract)
                {
                    message = string.Format("{0} trình duyệt hồ sơ thanh toán lần {1} Hợp đồng thiết bị lẻ {2}", submitor.FullName, entity.No, contractCode);
                }
                if (quote.DocType == DocumentType.Quotation)
                {
                    message = string.Format("{0} trình duyệt hồ sơ thanh toán lần {1} Báo giá thiết bị lẻ {2}", submitor.FullName, entity.No, contractCode);
                }
                if (quote.DocType == DocumentType.Receipt)
                {
                    message = string.Format("{0} trình duyệt hồ sơ thanh toán lần {1} Biên nhận và thanh toán {2}", submitor.FullName, entity.No, contractCode);
                }
            }

            var contracta = contractaRepository.FirstOrDefault(c => c.UID == entity.ContractUID);
            if (contracta != null)
            {
                contractCode = contracta.Code;
                message = string.Format("{0} trình duyệt hồ sơ thanh toán lần {1} Hợp đồng A {2}", submitor.FullName, entity.No, contractCode);

            }
            notificationService.Notify(director.Id, message, NotificationType.NewSettlement, entity.Id);
            return true;

        }
        public bool Reject(int id, int userId)
        {
            var entity = settlementRepository.GetById(id);
            if (entity.Status != SettlemenStatus.Submited)
                throw new ServiceException("Không thể trả lại");
            entity.Status = SettlemenStatus.Rejected;
            settlementRepository.Update(entity);

            var contractCode = string.Empty;
            var message = string.Empty;
            var contract = contractRepository.FirstOrDefault(c => c.UID == entity.ContractUID);
            if (contract != null)
            {
                contractCode = contract.Code;
                if (contract.DocType == DocumentType.Contract)
                {
                    message = string.Format("Hồ sơ thanh toán lần {0} Hợp đồng B {1} bị trả lại", entity.No, contractCode);
                }
                if (contract.DocType == DocumentType.Quotation)
                {
                    message = string.Format("Hồ sơ thanh toán lần {0} Báo giá Hợp đồng B {1} bị trả lại", entity.No, contractCode);
                }
            }

            var quote = quoteRepository.FirstOrDefault(c => c.UID == entity.ContractUID);
            if (quote != null)
            {
                contractCode = quote.Code;
                if (quote.DocType == DocumentType.Contract)
                {
                    message = string.Format("Hồ sơ thanh toán lần {0} Hợp đồng thiết bị lẻ {1} bị trả lại", entity.No, contractCode);
                }
                if (quote.DocType == DocumentType.Quotation)
                {
                    message = string.Format("Hồ sơ thanh toán lần {0} Báo giá thiết bị lẻ {1} bị trả lại", entity.No, contractCode);
                }
                if (quote.DocType == DocumentType.Receipt)
                {
                    message = string.Format("Hồ sơ thanh toán lần {0} Biên nhận và thanh toán {1} bị trả lại", entity.No, contractCode);
                }
            }

            var contracta = contractaRepository.FirstOrDefault(c => c.UID == entity.ContractUID);
            if (contracta != null)
            {
                contractCode = contracta.Code;
                message = string.Format("Hồ sơ thanh toán lần {0} Hợp đồng A {1} bị trả lại", entity.No, contractCode);
            }

            notificationService.Notify(entity.CreatedBy, message, NotificationType.RejectSettlement, entity.Id);

            return true;
        }
        public bool Approve(int id, int userId)
        {
            var result = false;
            var entity = settlementRepository.GetById(id);
            if (entity.Status != SettlemenStatus.Submited)
                throw new ServiceException("Không thể phê duyệt");
            entity.Status = SettlemenStatus.Approved;
            result = settlementRepository.Update(entity);

            var contractCode = string.Empty;
            var message = string.Empty;
            var contract = contractRepository.FirstOrDefault(c => c.UID == entity.ContractUID);
            if (contract != null)
            {
                contractCode = contract.Code;
                if (contract.DocType == DocumentType.Contract)
                {
                    message = string.Format("Hồ sơ thanh toán lần {0} Hợp đồng B {1} đã được duyệt", entity.No, contractCode);
                }
                if (contract.DocType == DocumentType.Quotation)
                {
                    message = string.Format("Hồ sơ thanh toán lần {0} Báo giá Hợp đồng B {1} đã được duyệt", entity.No, contractCode);
                }
            }

            var quote = quoteRepository.FirstOrDefault(c => c.UID == entity.ContractUID);
            if (quote != null)
            {
                contractCode = quote.Code;
                if (quote.DocType == DocumentType.Contract)
                {
                    message = string.Format("Hồ sơ thanh toán lần {0} Hợp đồng thiết bị lẻ {1} đã được duyệt", entity.No, contractCode);
                }
                if (quote.DocType == DocumentType.Quotation)
                {
                    message = string.Format("Hồ sơ thanh toán lần {0} Báo giá thiết bị lẻ {1} đã được duyệt", entity.No, contractCode);
                }
                if (quote.DocType == DocumentType.Receipt)
                {
                    message = string.Format("Hồ sơ thanh toán lần {0} Biên nhận và thanh toán {1} đã được duyệt", entity.No, contractCode);
                }
            }

            var contracta = contractaRepository.FirstOrDefault(c => c.UID == entity.ContractUID);
            if (contracta != null)
            {
                contractCode = contracta.Code;
                message = string.Format("Hồ sơ thanh toán lần {0} Hợp đồng A {1} đã được duyệt", entity.No, contractCode);
            }

            notificationService.Notify(entity.CreatedBy, message, NotificationType.ApproveSettlement, entity.Id);

            if (result)
            {
                result = Pass(id, userId);
            }

            return result;
        }
        public bool Pass(int id, int userId)
        {
            var entity = settlementRepository.GetById(id);
            if (entity.Status != SettlemenStatus.Approved)
                throw new ServiceException("Không thể chuyển khi lần thanh toán chưa được duyệt");
            entity.Status = SettlemenStatus.Passed;
            settlementRepository.Update(entity);

            var contractCode = string.Empty;
            var message = string.Empty;
            var passer = userService.GetById(userId);

            var contract = contractRepository.FirstOrDefault(c => c.UID == entity.ContractUID);
            if (contract != null)
            {
                contractCode = contract.Code;

                if (contract.DocType == DocumentType.Contract)
                {
                    message = string.Format("{0} đã chuyển hồ sơ thanh toán lần {1} Hợp đồng B {2} sang P5", passer.FullName, entity.No, contractCode);
                }
                if (contract.DocType == DocumentType.Quotation)
                {
                    message = string.Format("{0} đã chuyển hồ sơ thanh toán lần {1} Báo giá Hợp đồng B {2} sang P5", passer.FullName, entity.No, contractCode);
                }
            }

            var quote = quoteRepository.FirstOrDefault(c => c.UID == entity.ContractUID);
            if (quote != null)
            {
                contractCode = quote.Code;
                if (quote.DocType == DocumentType.Contract)
                {
                    message = string.Format("{0} đã chuyển hồ sơ thanh toán lần {1} Hợp đồng thiết bị lẻ {2} sang P5", passer.FullName, entity.No, contractCode);
                }
                if (quote.DocType == DocumentType.Quotation)
                {
                    message = string.Format("{0} đã chuyển hồ sơ thanh toán lần {1} Báo giá thiết bị lẻ {2} sang P5", passer.FullName, entity.No, contractCode);
                }
                if (quote.DocType == DocumentType.Receipt)
                {
                    message = string.Format("{0} đã chuyển hồ sơ thanh toán lần {1} Biên nhận và thanh toán {2} sang P5", passer.FullName, entity.No, contractCode);
                }
            }

            var contracta = contractaRepository.FirstOrDefault(c => c.UID == entity.ContractUID);
            if (contracta != null)
            {
                contractCode = contracta.Code;
                message = string.Format("{0} đã chuyển hồ sơ thanh toán lần {1} Hợp đồng A {2} sang P5", passer.FullName, entity.No, contractCode);
            }

            var receivers = new List<int>();

            var director = userService.GetDirector(Department.KHDT);

            if (director != null)
            {
                receivers.Add(director.Id);
            }
            var p5Director = userService.GetDirector(Department.TCKT);
            if (p5Director != null)
            {
                receivers.Add(p5Director.Id);
            }

            notificationService.Notify(receivers, message, NotificationType.PassSettlement, entity.Id);

            return true;
        }

        public bool Receive(int id, int userId)
        {
            var entity = settlementRepository.GetById(id);
            if (entity.Status != SettlemenStatus.Passed)
                throw new ServiceException("Không thể nhận hồ sơ thanh toán");
            entity.Status = SettlemenStatus.Received;
            entity.ReceiverId = userId;
            entity.ReceivedTime = DateTime.Now;
            settlementRepository.Update(entity);

            var contractCode = string.Empty;
            var message = string.Empty;
            var receiver = userService.GetById(userId);

            var contract = contractRepository.FirstOrDefault(c => c.UID == entity.ContractUID);
            if (contract != null)
            {
                contractCode = contract.Code;

                if (contract.DocType == DocumentType.Contract)
                {
                    message = string.Format("{0} đã nhận hồ sơ thanh toán lần {1} Hợp đồng B {2}", receiver.FullName, entity.No, contractCode);
                }
                if (contract.DocType == DocumentType.Quotation)
                {
                    message = string.Format("{0} đã nhận hồ sơ thanh toán lần {1} Báo giá Hợp đồng B {2}", receiver.FullName, entity.No, contractCode);
                }
            }

            var quote = quoteRepository.FirstOrDefault(c => c.UID == entity.ContractUID);
            if (quote != null)
            {
                contractCode = quote.Code;
                if (quote.DocType == DocumentType.Contract)
                {
                    message = string.Format("{0} đã nhận hồ sơ thanh toán lần {1} Hợp đồng thiết bị lẻ {2}", receiver.FullName, entity.No, contractCode);
                }
                if (quote.DocType == DocumentType.Quotation)
                {
                    message = string.Format("{0} đã nhận hồ sơ thanh toán lần {1} Báo giá thiết bị lẻ {2}", receiver.FullName, entity.No, contractCode);
                }
                if (quote.DocType == DocumentType.Receipt)
                {
                    message = string.Format("{0} đã nhận hồ sơ thanh toán lần {1} Biên nhận và thanh toán {2}", receiver.FullName, entity.No, contractCode);
                }
            }

            var contracta = contractaRepository.FirstOrDefault(c => c.UID == entity.ContractUID);
            if (contracta != null)
            {
                contractCode = contracta.Code;
                message = string.Format("{0} đã nhận hồ sơ thanh toán lần {1} Hợp đồng A {2}", receiver.FullName, entity.No, contractCode);
            }
            
            var director = userService.GetDirector(Department.KHDT);
            var p5Director = userService.GetDirector(Department.TCKT);

            notificationService.Notify(new List<int> { director.Id, p5Director.Id, receiver.Id }, message, NotificationType.PassSettlement, entity.Id);

            return true;
        }

        public bool Complete(int id, SettlementModel model)
        {
            var entity = settlementRepository.GetById(id);
            if (entity.Status != SettlemenStatus.Received)
                throw new ServiceException("Không thể thanh toán!");
            //Update Settlement
            if (!model.BillDate.HasValue || model.BillCode == null)
            {
                throw new ServiceException("Ngày xuất hóa đơn, mã hóa đơn phải có giá trị!");
            }
            //Update Settlement
            if (!model.BillValue.HasValue || !model.VatValue.HasValue)
            {
                throw new ServiceException("Tiền ghi trên hóa đơn, Tiền thuế phải có giá trị!");
            }

            if (model.SettlementDocumentChecks.Any(c => c.Checked && c.DocumentStatus == DocumentStatus.UnComplete))
            {
                throw new ServiceException("Tất cả hồ sơ phải được xác nhận đã hoàn thiện!");
            }

            entity.BillDate = model.BillDate;
            entity.BillCode = model.BillCode;
            entity.PaymentDate = model.PaymentDate;
            entity.PaymentNumber = model.PaymentNumber;
            entity.Status = SettlemenStatus.Completed;
            entity.BillValue = model.BillValue;
            entity.Vat = model.Vat;
            entity.ValueByBill = model.ValueByBill;
            entity.VatValue = model.VatValue;
            settlementRepository.Update(entity);
            foreach (SettlementCheckModel item in model.SettlementDocumentChecks)
            {
                item.SettlementId = entity.Id;
                if (item.Checked)
                {
                    if (item.Id == 0)
                    {
                        settlementCheckRepository.Insert(item.Map<SettlementCheck>());
                    }
                    else
                    {
                        settlementCheckRepository.Update(item.Map<SettlementCheck>());
                    }
                }

            }


            //Push notification
            var contractCode = string.Empty;
            var message = string.Empty;
            var receiver = userService.GetById(model.CreatedBy);

            var contract = contractRepository.FirstOrDefault(c => c.UID == entity.ContractUID);
            if (contract != null)
            {
                contractCode = contract.Code;

                if (contract.DocType == DocumentType.Contract)
                {
                    message = string.Format("{0} đã hoàn thành hồ sơ thanh toán lần {1} Hợp đồng B {2}", receiver.FullName, entity.No, contractCode);
                }
                if (contract.DocType == DocumentType.Quotation)
                {
                    message = string.Format("{0} đã hoàn thành hồ sơ thanh toán lần {1} Báo giá Hợp đồng B {2}", receiver.FullName, entity.No, contractCode);
                }
            }

            var quote = quoteRepository.FirstOrDefault(c => c.UID == entity.ContractUID);
            if (quote != null)
            {
                contractCode = quote.Code;
                if (quote.DocType == DocumentType.Contract)
                {
                    message = string.Format("{0} đã hoàn thành hồ sơ thanh toán lần {1} Hợp đồng thiết bị lẻ {2}", receiver.FullName, entity.No, contractCode);
                }
                if (quote.DocType == DocumentType.Quotation)
                {
                    message = string.Format("{0} đã hoàn thành hồ sơ thanh toán lần {1} Báo giá thiết bị lẻ {2}", receiver.FullName, entity.No, contractCode);
                }
                if (quote.DocType == DocumentType.Receipt)
                {
                    message = string.Format("{0} đã hoàn thành hồ sơ thanh toán lần {1} Biên nhận và thanh toán {2}", receiver.FullName, entity.No, contractCode);
                }
            }

            var contracta = contractaRepository.FirstOrDefault(c => c.UID == entity.ContractUID);
            if (contracta != null)
            {
                contractCode = contracta.Code;
                message = string.Format("{0} đã hoàn thành hồ sơ thanh toán lần {1} Hợp đồng A  {2}", receiver.FullName, entity.No, contractCode);
            }
            var director = userService.GetDirector(Department.KHDT);
            var p5Director = userService.GetDirector(Department.TCKT);
            
            notificationService.Notify(new List<int> { director.Id, p5Director.Id, receiver.Id }, message, NotificationType.PassSettlement, entity.Id);
            return true;
        }


        public bool P5Reject(int id, SettlementModel model)
        {
            var entity = settlementRepository.GetById(id);
            if (entity.Status != SettlemenStatus.Received)
                throw new ServiceException("Không thể thanh toán!");


            if (model.SettlementDocumentChecks.Any(c => c.Checked == true && (c.DocumentStatus == DocumentStatus.UnComplete && string.IsNullOrWhiteSpace(c.P5Reply))))
            {
                throw new ServiceException("Nội dung p5 phản hồi là bắt buộc!");
            }
            else if (!model.SettlementDocumentChecks.Any(c => c.Checked == true && (c.DocumentStatus == DocumentStatus.UnComplete)))
            {
                throw new ServiceException("Phải có ít nhất một tài liệu chưa hoàn thành!");
            }

            entity.Status = SettlemenStatus.P5Reject;
            settlementRepository.Update(entity);

            foreach (SettlementCheckModel item in model.SettlementDocumentChecks)
            {
                item.SettlementId = entity.Id;
                if (item.Checked)
                {
                    if (item.Id == 0)
                    {
                        settlementCheckRepository.Insert(item.Map<SettlementCheck>());
                    }
                    else
                    {
                        settlementCheckRepository.Update(item.Map<SettlementCheck>());
                    }


                }
                else
                {
                    var check = settlementCheckRepository.FirstOrDefault(t => t.SettlementId == model.Id && t.SettlementDocumentId == item.SettlementDocumentId);
                    if (check != null)
                        settlementCheckRepository.Delete(check);
                }

            }


            //Push notification
            var contractCode = string.Empty;
            var reason = model.SettlementDocumentChecks.Where(c => c.Checked == true && (c.DocumentStatus == DocumentStatus.UnComplete)).Select(c => c.P5Reply).ToList();
            var message = string.Empty;
            var receiver = userService.GetById(model.CreatedBy);

            var contract = contractRepository.FirstOrDefault(c => c.UID == entity.ContractUID);
            if (contract != null)
            {
                contractCode = contract.Code;
                if (contract.DocType == DocumentType.Contract)
                {
                    message = string.Format("Hồ sơ thanh toán lần {1} Hợp đồng B {2} đã được trả lại P2 với lý do [{0}]", string.Join(". ", reason), entity.No, contractCode);
                }
                if (contract.DocType == DocumentType.Quotation)
                {
                    message = string.Format("Hồ sơ thanh toán lần {1} Báo giá Hợp đồng B {2} đã được trả lại P2 với lý do [{0}]", string.Join(". ", reason), entity.No, contractCode);
                }
            }

            var quote = quoteRepository.FirstOrDefault(c => c.UID == entity.ContractUID);
            if (quote != null)
            {
                contractCode = quote.Code;
                if (quote.DocType == DocumentType.Contract)
                {
                    message = string.Format("Hồ sơ thanh toán lần {1} Hợp đồng thiết bị lẻ  {2} đã được trả lại P2 với lý do [{0}]", string.Join(". ", reason), entity.No, contractCode);
                }
                if (quote.DocType == DocumentType.Quotation)
                {
                    message = string.Format("Hồ sơ thanh toán lần {1} Báo giá thiết bị lẻ {2} đã được trả lại P2 với lý do [{0}]", string.Join(". ", reason), entity.No, contractCode);
                }
                if (quote.DocType == DocumentType.Receipt)
                {
                    message = string.Format("Hồ sơ thanh toán lần {1} Biên nhận và thanh toán {2} đã được trả lại P2 với lý do [{0}]", string.Join(". ", reason), entity.No, contractCode);
                }
            }

            var contracta = contractaRepository.FirstOrDefault(c => c.UID == entity.ContractUID);
            if (contracta != null)
            {
                contractCode = contracta.Code;
                message = string.Format("Hồ sơ thanh toán lần {1} Hợp đồng A {2} đã được trả lại P2 với lý do [{0}]", string.Join(". ", reason), entity.No, contractCode);
            }
            var director = userService.GetDirector(Department.KHDT);
            var p5Director = userService.GetDirector(Department.TCKT);           
          
            notificationService.Notify(new List<int> { director.Id, p5Director.Id, receiver.Id }, message, NotificationType.PassSettlement, entity.Id);

            return true;
        }


        public bool P2Reply(int id, SettlementModel model)
        {
            var entity = settlementRepository.GetById(id);
            if (entity.Status != SettlemenStatus.P5Reject)
                throw new ServiceException("Không thể thanh toán!");


            if (model.SettlementDocumentChecks.Any(c => c.Checked == true && (c.DocumentStatus == DocumentStatus.UnComplete && string.IsNullOrWhiteSpace(c.P2Reply))))
            {
                throw new ServiceException("Nội dung P2 phúc đáp là bắt buộc!");
            }


            entity.Status = SettlemenStatus.Received;
            settlementRepository.Update(entity);

            foreach (SettlementCheckModel item in model.SettlementDocumentChecks)
            {
                item.SettlementId = entity.Id;
                if (item.Checked)
                {
                    if (item.Id == 0)
                    {
                        settlementCheckRepository.Insert(item.Map<SettlementCheck>());
                    }
                    else
                    {
                        settlementCheckRepository.Update(item.Map<SettlementCheck>());
                    }


                }
                else
                {
                    var check = settlementCheckRepository.FirstOrDefault(t => t.SettlementId == model.Id && t.SettlementDocumentId == item.SettlementDocumentId);
                    if (check != null)
                        settlementCheckRepository.Delete(check);
                }

            }


            //Push notification
            var contractCode = string.Empty;
            var message = string.Empty;
            var reason = model.SettlementDocumentChecks.Where(c => c.Checked == true && (c.DocumentStatus == DocumentStatus.UnComplete)).Select(c => c.P5Reply).ToList();
            var receiver = userService.GetById(model.CreatedBy);

            var contract = contractRepository.FirstOrDefault(c => c.UID == entity.ContractUID);
            if (contract != null)
            {
                contractCode = contract.Code;
                if (contract.DocType == DocumentType.Contract)
                {
                    message = string.Format("Hồ sơ thanh toán lần {1} Hợp đồng B {2} P2 phúc đáp:{0}", string.Join(". ", reason), entity.No, contractCode);
                }
                if (contract.DocType == DocumentType.Quotation)
                {
                    message = string.Format("Hồ sơ thanh toán lần {1} Báo giá Hợp đồng B {2} P2 phúc đáp:{0}", string.Join(". ", reason), entity.No, contractCode);
                }
            }

            var quote = quoteRepository.FirstOrDefault(c => c.UID == entity.ContractUID);
            if (quote != null)
            {
                contractCode = quote.Code;
                if (quote.DocType == DocumentType.Contract)
                {
                    message = string.Format("Hồ sơ thanh toán lần {1} Hợp đồng thiết bị lẻ {2} P2 phúc đáp:{0}", string.Join(". ", reason), entity.No, contractCode);
                }
                if (quote.DocType == DocumentType.Quotation)
                {
                    message = string.Format("Hồ sơ thanh toán lần {1} Báo giá thiết bị lẻ {2} P2 phúc đáp:{0}", string.Join(". ", reason), entity.No, contractCode);
                }
                if (quote.DocType == DocumentType.Receipt)
                {
                    message = string.Format("Hồ sơ thanh toán lần {1} Biên nhận và thanh toán {2} P2 phúc đáp:{0}", string.Join(". ", reason), entity.No, contractCode);
                }
            }

            var contracta = contractaRepository.FirstOrDefault(c => c.UID == entity.ContractUID);
            if (contracta != null)
            {
                contractCode = contracta.Code;
                message = string.Format("Hồ sơ thanh toán lần {1} Hợp đồng A {2} P2 phúc đáp:{0}", string.Join(". ", reason), entity.No, contractCode);
            }


            var director = userService.GetDirector(Department.KHDT);
            var p5Director = userService.GetDirector(Department.TCKT);

            notificationService.Notify(new List<int> { director.Id, p5Director.Id, receiver.Id }, message, NotificationType.PassSettlement, entity.Id);

            return true;
        }

        public bool AddPayment(SettlementPaymentModel model)
        {
            if (model.PaymentValue == null)
                throw new ServiceException("Số tiền trả phải có giá trị!");
            if (model.PaymentDate == null)
                throw new ServiceException("Ngày trả phải có giá trị!");
            var context = settlementRepository.GetDbContext();
            var entity = model.Map<SettlementPayment>();
            context.SettlementPayments.Add(entity);
            context.SaveChanges();
            return true;
        }

        public bool DeletePayment(int id)
        {

            var context = settlementRepository.GetDbContext();
            var payment = context.SettlementPayments.FirstOrDefault(c => c.Id == id);
            if (payment == null)
            {
                throw new ServiceException("Không thể xóa!");

            }
            context.SettlementPayments.Remove(payment);
            context.SaveChanges();
            return true;
        }

        public List<SettlementPaymentModel> GetPayments(int settlementId)
        {

            var context = settlementRepository.GetDbContext();
            var payments = context.SettlementPayments.Where(c => c.SettlementId == settlementId).ToList();

            var models = payments.Map<List<SettlementPaymentModel>>();
            return models;
        }

    }
}
