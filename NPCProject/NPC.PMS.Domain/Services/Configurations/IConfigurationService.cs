﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NPC.PMS.Domain.Services
{
    public interface IConfigurationService
    {
        Task<bool> Save(ConfigurationModel config);

        Task<ConfigurationModel> GetConfig();
    }
}
