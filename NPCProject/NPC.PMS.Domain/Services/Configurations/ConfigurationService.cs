﻿using System.Threading.Tasks;
using NPC.PMS.Domain.Data;
using NPC.PMS.Domain.Data.Entity;

namespace NPC.PMS.Domain.Services
{
    public class ConfigurationService : IConfigurationService
    {
        IRepository<Configuration> _configRepository;

        public ConfigurationService(IRepository<Configuration> configRepository)
        {
            _configRepository = configRepository;
        }


        public async Task<ConfigurationModel> GetConfig()
        {
            var entity = await _configRepository.SingleAsync(c => true == true);
            return entity.Map<ConfigurationModel>();
        }

        public async Task<bool> Save(ConfigurationModel config)
        {
            var entity = config.Map<Configuration>();
            var exist = await _configRepository.SingleAsync(c => true == true);

            if (exist == null)
            {
                var res = await _configRepository.AddAsync(entity);

                return res != null;
            }
            else
            {
                entity.Id = exist.Id;
                return await _configRepository.UpdateAsync(entity);
            }
        }
    }
}
