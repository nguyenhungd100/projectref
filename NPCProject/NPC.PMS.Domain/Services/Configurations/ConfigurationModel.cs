
using NPC.PMS.Domain.Models;
using System;
namespace NPC.PMS.Domain.Services
{
    public class ConfigurationModel : BaseModel
    {
        public String SmtpServer { set; get; }
        public Int32 SmtpPort { set; get; }
        public String Username { set; get; }
        public String Password { set; get; }
        public Nullable<Boolean> UseSsl { set; get; }
        public String AuthenticationMethod { set; get; }
        public String EmailTest { set; get; }
        public string SmsProviderAlias { get; set; }
        public string SmsAuthUser { get; set; }
        public string SmsPassUser { get; set; }
    }
}



