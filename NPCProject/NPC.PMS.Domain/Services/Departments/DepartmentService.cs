
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using NPC.PMS.Domain.Data;
using NPC.PMS.Domain.Data.Entity;
using System.Linq.Expressions;
using NPC.PMS.Domain.Enums;
using NPC.PMS.Framework.Extensions.Enums;

namespace NPC.PMS.Domain.Services
{
    public class DepartmentService : IDepartmentService
    {
        private readonly IRepository<Centre> centreRepository;
        private readonly IRepository<UserCalendar> userCalendarRepository;

        public DepartmentService(IRepository<Centre> centreRepository, IRepository<UserCalendar> userCalendarRepository)
        {
            this.centreRepository = centreRepository;
            this.userCalendarRepository = userCalendarRepository;
        }
        public List<Centre> GetCentres()
        {
            return centreRepository.Fetch();
        }

        public List<DepartmentModel> GetDepartments()
        {
            var values = Enum.GetValues(typeof(Department)).Cast<Department>();
            var result = new List<DepartmentModel>();
            foreach (var value in values)
            {
                result.Add(new DepartmentModel
                {
                    Id = (int)value,
                    Name = value.ToString(),
                    Description = value.ToDescription()
                });
            }
            return result;
        }

        public List<DepartmentModel> GetTechDepartments()
        {
            var values = Enum.GetValues(typeof(Department)).Cast<Department>();
            var result = new List<DepartmentModel>();
            foreach (var value in values)
            {
                if ((int)value < 10 || (int)value > 20) continue;
                result.Add(new DepartmentModel
                {
                    Id = (int)value,
                    Name = value.ToString(),
                    Description = value.ToDescription()
                });
            }
            return result;
        }
    }
}

