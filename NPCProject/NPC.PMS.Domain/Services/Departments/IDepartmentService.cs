
using NPC.PMS.Domain.Data.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace NPC.PMS.Domain.Services
{
    public interface IDepartmentService
    {
        List<DepartmentModel> GetTechDepartments();
        List<DepartmentModel> GetDepartments();
        List<Centre> GetCentres();
    }
}


