
using NPC.PMS.Domain.Models;

namespace NPC.PMS.Domain.Services
{
    public class DepartmentFilterModel : BaseFilterModel
	{
		public string Search { set; get; }
	}
}


