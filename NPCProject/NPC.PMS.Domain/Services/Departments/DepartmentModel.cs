
using NPC.PMS.Domain.Models;
using System;
namespace NPC.PMS.Domain.Services
{
    public class DepartmentModel : BaseModel
    {
        public String Name { set; get; }
        public String Description { set; get; }
    }
}



