﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NPC.PMS.Domain.Services
{
    public class ActionResults
    {
        public bool Result { set; get; }

        public string Error { set; get; }

        public object Data { set; get; }

        public static implicit operator decimal(ActionResults v)
        {
            throw new NotImplementedException();
        }
    }

    public class ActionResultType<T> : ActionResults
    {
        public new T Data { set; get; }
    }
}
