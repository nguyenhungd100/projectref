﻿using NPC.PMS.Domain.Data.Entity;

namespace NPC.PMS.Domain.Services.FileStorages
{
    public interface IFileStorageService
    {
        Document GetFile(int id);
        BinaryStorage GetBinaryStorage(int storageId);
    }
}
