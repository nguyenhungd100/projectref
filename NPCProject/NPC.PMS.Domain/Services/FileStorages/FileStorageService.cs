﻿using NPC.PMS.Domain.Data;
using NPC.PMS.Domain.Data.Entity;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace NPC.PMS.Domain.Services.FileStorages
{
    public class FileStorageService : IFileStorageService
    {
        private readonly IRepository<Data.Entity.Document> fileRepository;
        private readonly IRepository<BinaryStorage> binaryStorageRepository;

        public FileStorageService(
            IRepository<Data.Entity.Document> fileRepository,
            IRepository<Data.Entity.BinaryStorage> binaryStorageRepository
            )
        {
            this.fileRepository = fileRepository;
            this.binaryStorageRepository = binaryStorageRepository;
        }

        public Data.Entity.Document GetFile(int id)
        {
            return fileRepository.GetById(id);
        }

        public BinaryStorage GetBinaryStorage(int storageId)
        {
            return binaryStorageRepository.GetById(storageId);
        }
    }
}
