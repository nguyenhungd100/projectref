﻿using System;
using System.Collections.Generic;
using System.Text;
using NPC.PMS.Domain.Data.Entity;
using NPC.PMS.Domain.Enums;
using NPC.PMS.Domain.Models;
using NPC.PMS.Domain.Models.ContractA;
using NPC.PMS.Domain.Models.ContractB;
using NPC.PMS.Domain.Models.Documents;
using NPC.PMS.Domain.Models.Users;
using NPC.PMS.Domain.Models.Vehicles;
using NPC.PMS.Domain.Services.Common.Devices;

namespace NPC.PMS.Domain.Services
{
    public interface IContractBService
    {
        ContractFilterResult Filter(ContractFilterModel model);
        List<ContractModel> GetActives();
        ContractModel Save(ContractFormModel model);
        bool Assign(AssignModel assignModel);
        bool Unassign(int id);

        bool Delete(int id);
        ContractModel Get(int id);
        ContractModel Get(Guid uid);

        int Clone(CloneContractModel model);
        bool Finish(FinishContractModel model);
        List<WorkitemModel> ParseWorkitemFile(int id, byte[] bytes, bool? isStem, ProcessType processType = ProcessType.QT01);

        bool AddWorkitemStem(SaveWorkitemStemModel stemModel);

        List<ContractGroupModel> GetGroups();
        string NextCode(DateTime signingDate, bool isContract);
        Dictionary<int, string> GetTypes();
        List<WorkitemModel> GetWorkitems(int contractId, ProcessType processType = ProcessType.QT01);
        List<WorkitemModel> GetWorkitems(Guid uid, ProcessType processType = ProcessType.QT01);
        bool SaveMasterPlan(List<MasterPlanModel> model);
        List<MasterPlan> GetMasterPlan(int id);
        bool UpdateWorkitemDates(List<WorkitemModel> model);
        bool DeleteContractGroup(int id);
        ContractGroupModel SaveContractGroup(ContractGroupModel model);
        List<PhaseModel> GetPhases(int id, ProcessType processType = ProcessType.QT01);
        List<PhaseModel> GetPhases(Guid uid, ProcessType processType = ProcessType.QT01);
        List<PhaseModel> GetPhases(int userId, bool hasViewAll);

        PhaseModel UpdatePhase(PhaseModel model);
        PhaseModel GetPhase(int id);
        List<WorkitemModel> GetUnassignedWorkitems(int phaseId);
        bool AddWorkitemsToPhase(PhaseWorkitemAddModel model);
        List<WorkitemModel> GetWorkitemsByPhase(int phaseId);
        List<PhaseWorkitemModel> GetPhaseWorkitems(int phaseId);
        bool UpdatePhaseWorkitems(List<PhaseWorkitem> model);
        bool DeletePhaseWorkitem(int phaseId, int workitemId);
        List<PhaseStaffRequest> GetRequestedStaffs(int phaseId);
        List<PhaseStaffModel> GetPhaseStaffs(int phaseId);
        bool SaveRequestedStaffs(List<PhaseStaffRequest> requests);
        List<StaffStateModel> GetUnassignedStaffs(DepartmentStaffFilterModel filter);
        bool AddStaffsToPhase(int phaseId, List<int> staffIds);
        bool SetPhaseStaffPositionFromPKT(int phaseStaffId, PhaseStaffPosition position);
        bool SetPhaseStaffPositionFromSpecialDepartments(int phaseStaffId, PCMAssignPotition position, int userId);
        bool RemovePhaseStaff(int phaseStaffId);
        bool FinishPhaseStaff(int phaseStaffId);

        List<PhaseEquipmentModel> GetPhaseEquipments(int phaseId);
        bool AddEquipmentsToPhase(int phaseId, int departmentId, List<int> equipmentIds);

        bool DeleteEquipmentsFromPhase(int phaseId, int departmentId, List<int> equipmentIds, int userId);


        bool PublishPhase(int id);
        bool Next(int id, string action);
        bool CompletePhase(int phaseId);
        List<Document> AttachDocuments(int id, List<FileInfoModel> files);
        bool AddVehicle(int phaseId, int vehicleId);
        List<VehicleModel> GetVehicles(int phaseId);
        bool RemoveVehicle(int phaseId, int vehicleId);
        List<int> GetPhasesCount(int id,ProcessType processType = ProcessType.QT01);
        bool SaveRecord(RecordSaveModel model);
        RecordFilterResult FilterRecords(RecordFilterModel model);
        bool Reject(RejectContractModel model);
        bool SubmitPhase(int phaseId, int userId);
        bool SubmitPhaseToP3Manager(int phaseId, int userId);
        bool RejectPhase(int phaseId, string reason);
        bool ApprovePhase(int phaseId);
        bool ApprovePhaseByP3Manager(int phaseId);
        bool PassPhase(int phaseId);
        List<WorkitemModel> GetOutputWorkitems(Guid uid, ProcessType processType = ProcessType.QT01);
        RecordModel GetRecord(int recordId);
        bool SubmitRecord(int recordId);
        bool RejectRecord(int recordId);
        bool ApproveRecord(int recordId);
        bool UpdateAcceptancedQuantity(List<WorkitemModel> workitems);
        bool DonePhaseStaffs(int phaseId, int departmentId);
        bool PassPhaseToP3(int phaseId);
        bool ClearWorkitems(int id, ProcessType processType = ProcessType.QT01);
        bool RemoveCenter(int contractId, int centerId);
        bool SupplementStaffsForPhase(List<ReplacementStaffModel> models, int userId);
        bool AddVehicleRental(VehicleModel vehicle, int phaseId);
        bool UpdateWorkitem(UpdateWorkitemModel model, Guid uid);
        List<PhaseEquipmentAllocatorModel> GetEquipmentAllocator(int phaseId);
        bool UpdateEquipmentAllocator(int phaseId, int userId, int departmentId);
    }
}
