﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Newtonsoft.Json;
using NPC.PMS.Domain.Data;
using NPC.PMS.Domain.Data.Entity;
using NPC.PMS.Domain.Enums;
using NPC.PMS.Domain.Models;
using NPC.PMS.Domain.Models.ContractA;
using NPC.PMS.Domain.Models.ContractB;
using NPC.PMS.Domain.Models.Documents;
using NPC.PMS.Domain.Models.Users;
using NPC.PMS.Domain.Models.Vehicles;
using NPC.PMS.Domain.Services.Common.Devices;
using NPC.PMS.Domain.Services.Common.Documents;
using NPC.PMS.Domain.Services.Common.Notifications;
using NPC.PMS.Domain.Services.Users;
using NPC.PMS.Framework.Extensions.Enums;
using NPC.PMS.Framework.MimeDetective;
using NPC.PMS.Framework.Utils;
using OfficeOpenXml;

namespace NPC.PMS.Domain.Services
{
    public class ContractBService : IContractBService
    {
        #region Constructor
        private readonly IRepository<Contract> contractRepository;
        private readonly IRepository<User> userRepository;
        private readonly IRepository<ContractGroup> contractGroupRepository;
        private readonly IRepository<Customer> customerRepository;
        private readonly IRepository<Workitem> workitemRepository;
        private readonly IRepository<ContractUnit> contractUnitRepository;
        private readonly IRepository<MasterPlan> masterPlanRepository;
        private readonly IRepository<Phase> phaseRepository;
        private readonly IRepository<PhaseWorkitem> phaseWorkitemRepository;
        private readonly IRepository<PhaseStaffRequest> staffRequestRepository;
        private readonly IRepository<PhaseStaff> phaseStaffRepository;
        private readonly IRepository<PhaseEquipment> phaseEquipmentRepository;
        private readonly IRepository<PhaseVehicle> phaseVehileRepository;
        private readonly IDepartmentService departmentService;
        private readonly IRepository<Equipment> equipmentRepository;
        private readonly IRepository<Contracta> contractARepository;
        private readonly IRepository<Document> documentRepository;
        private readonly IRepository<Vehicle> vehicleRepository;
        private readonly IRepository<ExperimentRecord> recordRepository;
        private readonly IRepository<Centre> centreRepository;
        private readonly IRepository<UserCalendar> userCalendarRepository;
        private readonly IRepository<ContractUser> contractUserRepository;
        private readonly IDocumentService documentService;
        private readonly IUserService userService;
        private readonly IRepository<Report> reportRepository;
        private readonly IRepository<Reportitem> reportItemRepository;
        private readonly INotificationService notificationService;
        private readonly IRepository<PhaseEquipmentAllocators> phaseEquipmentAllocatorRepository;
        private readonly IRepository<Quote> quoteRepository;
        const string SUBMITED_MESSAGE = "Hợp đồng {0} đã được trình lên TP.KH&ĐT";
        const string APPROVED_MESSAGE = "Hợp đồng {0} đã được phê duyệt";
        const string PASSED_MESSAGE = "Hợp đồng {0} đã được duyệt và chuyển sang PKT";
        const string ASSIGN_CONTRACT_USER = "{0} {1} được chỉ định vào hợp đồng {2}";

        public ContractBService(
            IRepository<Contract> contractRepository,
            IRepository<User> userRepository,
            IRepository<ContractGroup> contractGroupRepository,
            IRepository<Customer> customerRepository,
            IRepository<Workitem> workitemRepository,
            IRepository<ContractUnit> contractUnitRepository,
            IRepository<MasterPlan> masterPlanRepository,
            IRepository<Phase> phaseRepository,
            IRepository<PhaseWorkitem> phaseWorkitemRepository,
            IRepository<PhaseStaffRequest> staffRequestRepository,
            IRepository<PhaseStaff> phaseStaffRepository,
            IRepository<PhaseEquipment> phaseEquipmentRepository,
            IRepository<PhaseVehicle> phaseVehileRepository,
            IDepartmentService departmentService,
            IRepository<Equipment> equipmentRepository,
            IRepository<Contracta> contractARepository,
            IRepository<Document> documentRepository,
            IRepository<Vehicle> vehicleRepository,
            IRepository<ExperimentRecord> experimentRecordRepository,
            IRepository<Centre> centreRepository,
            IRepository<UserCalendar> userCalendarRepository,
            IRepository<ContractUser> contractUserRepository,
            IDocumentService documentService,
            IUserService userService,
            IRepository<Report> reportRepository,
            IRepository<Reportitem> reportItemRepository,
            INotificationService notificationService,
            IRepository<PhaseEquipmentAllocators> phaseEquipmentAllocatorRepository,
           IRepository<Quote> quoteRepository
            )
        {
            this.contractRepository = contractRepository;
            this.userRepository = userRepository;
            this.contractGroupRepository = contractGroupRepository;
            this.customerRepository = customerRepository;
            this.workitemRepository = workitemRepository;
            this.contractUnitRepository = contractUnitRepository;
            this.masterPlanRepository = masterPlanRepository;
            this.phaseRepository = phaseRepository;
            this.phaseWorkitemRepository = phaseWorkitemRepository;
            this.staffRequestRepository = staffRequestRepository;
            this.phaseStaffRepository = phaseStaffRepository;
            this.phaseEquipmentRepository = phaseEquipmentRepository;
            this.phaseVehileRepository = phaseVehileRepository;
            this.departmentService = departmentService;
            this.equipmentRepository = equipmentRepository;
            this.contractARepository = contractARepository;
            this.documentRepository = documentRepository;
            this.vehicleRepository = vehicleRepository;
            this.recordRepository = experimentRecordRepository;
            this.centreRepository = centreRepository;
            this.userCalendarRepository = userCalendarRepository;
            this.contractUserRepository = contractUserRepository;
            this.documentService = documentService;
            this.userService = userService;
            this.reportRepository = reportRepository;
            this.reportItemRepository = reportItemRepository;
            this.notificationService = notificationService;
            this.phaseEquipmentAllocatorRepository = phaseEquipmentAllocatorRepository;
            this.quoteRepository = quoteRepository;
        }

        #endregion

        #region Contract
        public ContractFilterResult Filter(ContractFilterModel model)
        {
            var result = new ContractFilterResult { Filter = model };
            var user = userRepository.GetById(model.UserId);
            var contractGroups = contractGroupRepository.Fetch();
            var db = contractRepository.GetDbContext();
            {
                var q = db.Contracts.AsQueryable();
                if (model.DocType != null)
                    q = q.Where(c => c.DocType == model.DocType);

                if ((Department)user.DepartmentId == Department.KHDT || (Department)user.DepartmentId == Department.TCKT)
                {
                    if (user.Position != StaffPosition.Director)
                    {
                        var contractUsers = contractUserRepository.Fetch(cu => cu.UserId == user.Id && cu.Kind == ContractKind.B);
                        var contractIds = contractUsers.Select(cu => cu.ContractId);
                        q = q.Where(c => contractIds.Contains(c.Id));
                    }


                    var except = q.Where(c => c.CreatedBy != model.UserId && c.Status == ContractStatus.Draft);
                    if (except.Count() > 0)
                    {
                        q = q.Except(except);
                    }

                }
                // neu user thuoc phong ky thuat thi chi loc nhung hop dong da duoc chuyen
                // sang phong ky thuat
                else if ((Department)user.DepartmentId == Department.KT)
                {
                    q = q.Where(c => (int)c.Status >= (int)ContractStatus.Passed);
                }
                else // return empty
                {
                    q = q.Where(c => c.Id < 0);
                }

                if (!string.IsNullOrEmpty(model.ContractCode))
                {
                    q = q.Where(c => c.Code.Contains(model.ContractCode));
                }
                if (!string.IsNullOrEmpty(model.Content))
                {
                    q = q.Where(c => c.Content.Contains(model.Content));
                }
                if (model.ContractType.HasValue)
                {
                    q = q.Where(c => c.Type == model.ContractType.Value);
                }
                if (model.Status.HasValue)
                {
                    q = q.Where(c => c.Status == model.Status.Value);
                }
                if (model.CustomerId.HasValue)
                {
                    q = q.Where(c => c.CustomerId == model.CustomerId.Value);
                }
                if (model.GroupId.HasValue)
                {
                    q = q.Where(c => c.GroupId == model.GroupId.Value);
                }

                result.TotalRecord = q.Count();


                var orderTypes = new List<ContractStatus>()
                {
                    ContractStatus.Submited,
                    ContractStatus.Draft,
                    ContractStatus.Rejected,
                    ContractStatus.Approved,
                    ContractStatus.Passed,
                    ContractStatus.Closed,
                };
                var entities = q.OrderBy(x => orderTypes.IndexOf(x.Status)).ToList().Skip((model.PageIndex - 1) * model.PageSize).Take(model.PageSize).ToList();
                result.Records = entities.Map<List<ContractModel>>();
                var customers = (from r in result.Records
                                join c in db.Customers on r.CustomerId equals c.Id
                                select c).ToList();
                var userIds = result.Records.Select(c => c.CreatedBy.Value);
                var users = userRepository.Fetch(u => userIds.Contains(u.Id)).ToList();


                var userCurrent = userRepository.GetById(model.UserId);
                foreach (var record in result.Records)
                {
                    record.TypeCode = record.Type.ToString();
                    var customer = customers.FirstOrDefault(c => c.Id == record.CustomerId);
                    if (customer != null)
                    {
                        record.CustomerName = customer.Name;
                        record.CustomerCode = customer.Code;
                    }
                    var group = contractGroups.FirstOrDefault(g => g.Id == record.GroupId);
                    if (group != null) record.GroupName = group.Name;

                    var creator = users.FirstOrDefault(u => u.Id == record.CreatedBy.Value);
                    if (creator != null)
                    {
                        record.CreateByName = creator.FullName;
                    }
                    record.StatusName = record.Status.ToDescription();
                    if ((Department)userCurrent.DepartmentId == Department.KT && record.Status == ContractStatus.Passed)
                        record.StatusName = "Đã tiếp nhận";
                }



                //tinh toan dư tiền đã thanh toán.
                var uids = result.Records.Select(c => c.UID).ToList();
                var settlements = db.Settlements.Where(c => uids.Contains(c.ContractUID)).Select(c => new { c.Id, c.ContractUID, c.BillValue }).ToList();
                var settlementids = settlements.Select(c => c.Id).ToList();
                List<int?> settIds = new List<int?>();
                foreach (var id in settlementids)
                {
                    settIds.Add(id);

                }

                var payments = db.SettlementPayments.Where(c => settIds.Contains(c.SettlementId)).Select(c => new { c.Id, c.SettlementId, c.PaymentValue }).ToList();
                foreach (var record in result.Records)
                {
                    record.BillAmount = (settlements.Where(c => c.ContractUID == record.UID).Sum(c => c.BillValue) ?? 0) + (record.OpeningBalance ?? 0);
                    var paymentAmount = (from a in settlements.Where(c => c.ContractUID == record.UID)
                                         join b in payments on a.Id equals b.SettlementId
                                         select b.PaymentValue
                                        ).Sum();
                    record.PaymentAmount = paymentAmount;
                }
                return result;
            }
        }
        public List<ContractModel> GetActives()
        {
            var entities = contractRepository.Fetch();
            return entities.Map<List<ContractModel>>();
        }
        public ContractModel Save(ContractFormModel model)
        {
            var creator = userRepository.GetById(model.CreateBy);

            if (model.ContractValue < 0) throw new ServiceException("Không thể nhập số âm cho hợp đồng");
            if (!model.StartDate.HasValue) throw new ServiceException("Vui lòng chọn ngày bắt đầu thực hiện hợp đồng.");
            if (!model.EndDate.HasValue) throw new ServiceException("Vui lòng chọn ngày kết thúc hợp đồng.");
            if (model.EndDate.Value.Date < model.StartDate.Value.Date) throw new ServiceException("Ngày kết thúc phải lớn hơn hoặc bằng ngày bắt đầu.");

            var entity = new Contract();
            if (model.Id.HasValue) entity = contractRepository.GetById(model.Id.Value);
            else
            {
                entity.CreatedBy = model.CreateBy;
                entity.CreatedOn = DateTime.Now;
                entity.Status = ContractStatus.Draft;
                entity.Code = model.Code;
            }
            //if (entity.Status == ContractStatus.Passed || entity.Status == ContractStatus.Closed) throw new ServiceException("Không thể sửa hợp đồng");
            entity.CustomerContractCode = model.CustomerContractCode;
            entity.Type = model.Type;
            entity.DocType = model.DocType;
            entity.CompanyId = model.CompanyId;
            entity.GroupId = model.GroupId;
            entity.SigningDate = model.SigningDate;
            entity.CustomerId = model.CustomerId;
            entity.ContractValue = model.ContractValue;
            entity.Branch = model.Branch;
            entity.StartDate = model.StartDate;
            entity.EndDate = model.EndDate;
            entity.Content = model.Content;
            entity.Email = model.Email;
            entity.ContactName = model.ContactName;
            entity.ContactAddress = model.ContactAddress;
            entity.Priority = model.Priority;
            entity.Email = model.Email;

            if (model.Id.HasValue) // update
            {
                if (model.Code != entity.Code)
                {
                    var exsited = contractRepository.Any(c => c.Code == model.Code);
                    if (exsited) throw new ServiceException("Số hợp đồng đã tồn tại");
                    entity.Code = model.Code;
                }
                contractRepository.Update(entity);
            }
            else // add
            {
                var exsited = contractRepository.Any(c => c.Code == model.Code);
                if (exsited) throw new ServiceException("Số hợp đồng đã tồn tại");

                contractRepository.Insert(entity);
                if (creator.Position.Value != StaffPosition.Director)
                {
                    contractUserRepository.Insert(new ContractUser
                    {
                        ContractId = entity.Id,
                        Kind = ContractKind.B,
                        DepartmentId = (Department)creator.DepartmentId.Value,
                        UserId = creator.Id,
                        Function = UserFunction.Handler,
                        ProcessType = ProcessType.QT01
                    });
                }
            }

            return entity.Map<ContractModel>();
        }
        public bool Assign(AssignModel assignModel)
        {
            var entities = new List<ContractUser>();
            assignModel.UserIds.ForEach(id => entities.Add(new ContractUser
            {
                UserId = id,
                ContractId = assignModel.ContractId,
                DepartmentId = assignModel.DepartmentId,
                Kind = assignModel.Kind,
                Function = assignModel.Function,
                ProcessType = assignModel.ProcessType
            }));
            contractUserRepository.InsertMany(entities);

            // notify assignee
            if (assignModel.ProcessType == ProcessType.QT01)
            {
                var contract = contractRepository.GetById(assignModel.ContractId);
                var users = userRepository.Fetch(u => assignModel.UserIds.Contains(u.Id));
                foreach (var user in users)
                {
                    var message = string.Format(ASSIGN_CONTRACT_USER, user.FullName, assignModel.DepartmentId.ToDescription(), contract.Code);
                    notificationService.Notify(user.Id, message, NotificationType.AssignContractUser, contract.Id);
                }
            }
            else
            {
                var contract = quoteRepository.GetById(assignModel.ContractId);
                var users = userRepository.Fetch(u => assignModel.UserIds.Contains(u.Id));
                foreach (var user in users)
                {
                    var message = string.Format(ASSIGN_CONTRACT_USER, user.FullName, assignModel.DepartmentId.ToDescription(), contract.Code);
                    notificationService.Notify(user.Id, message, NotificationType.AssignContractUser, contract.Id);
                }
            }


            return true;
        }
        public bool Unassign(int id)
        {
            var entity = contractUserRepository.GetById(id);
            return contractUserRepository.Delete(entity);
        }
        public ContractModel Get(int id)
        {
            var entity = contractRepository.GetById(id);

            return BuildModel(entity);
        }
        public ContractModel Get(Guid uid)
        {
            var entity = contractRepository.FirstOrDefault(c => c.UID == uid);

            return BuildModel(entity);
        }
        public ContractModel BuildModel(Contract entity)
        {
            var model = entity.Map<ContractModel>();
            model.TypeCode = model.Type.ToDescription();
            model.PriorityName = model.Priority.ToDescription();
            var customer = customerRepository.FirstOrDefault(c => c.Id == model.CustomerId);
            if (customer != null) model.CustomerName = customer.Name;

            var group = contractGroupRepository.FirstOrDefault(c => c.Id == model.GroupId);
            if (group != null) model.GroupName = group.Name;

            model.Phases = phaseRepository.Fetch(p => p.ContractId == entity.Id && p.ProcessType == ProcessType.QT01);
            if (!string.IsNullOrEmpty(model.Attachments))
            {
                var attachments = JsonConvert.DeserializeObject<List<Guid>>(model.Attachments);
                model.Documents = documentRepository.Fetch(d => attachments.Contains(d.Id));
            }

            var contractUsers = contractUserRepository.Fetch(cu => cu.ContractId == entity.Id && cu.ProcessType == ProcessType.QT01).OrderBy(cu => cu.Function);
            var userIds = contractUsers.Select(u => u.UserId);
            var users1 = userRepository.Fetch(u => userIds.Contains(u.Id));
            var contractUserModels = contractUsers.Map<List<ContractUserModel>>();

            foreach (var contractUser in contractUserModels)
            {
                var user = users1.FirstOrDefault(u => u.Id == contractUser.UserId);
                if (user != null) contractUser.UserName = user.FullName;
            }
            model.ContractUsers = contractUserModels;
            //tinh công nợ
            //tinh toan dư tiền đã thanh toán.
            var db = contractARepository.GetDbContext();
            var settlements = db.Settlements.Where(c => c.ContractUID == model.UID).Select(c => new { c.Id, c.ContractUID, c.BillValue }).ToList();
            var settlementids = settlements.Select(c => c.Id).ToList();
            List<int?> settIds = new List<int?>();
            foreach (var id in settlementids)
            {
                settIds.Add(id);

            }

            var payments = db.SettlementPayments.Where(c => settIds.Contains(c.SettlementId)).Select(c => new { c.Id, c.SettlementId, c.PaymentValue }).ToList();
            model.BillAmount = settlements.Sum(c => c.BillValue) ?? 0 + model.OpeningBalance ?? 0;
            model.PaymentAmount = payments.Sum(c => c.PaymentValue);

            return model;
        }
        public bool Reject(RejectContractModel model)
        {
            var entity = contractRepository.GetById(model.ContractId);
            if (entity.Status != ContractStatus.Submited) return false;

            entity.Status = ContractStatus.Rejected;
            entity.Note = model.Reason;
            return contractRepository.Update(entity);
        }

        public int Clone(CloneContractModel model)
        {
            throw new NotImplementedException();
        }

        public bool Finish(FinishContractModel model)
        {
            throw new NotImplementedException();
        }
        public bool Next(int id, string action)
        {
            var notificationMessage = string.Empty;
            var receivers = new List<int>();

            var entity = contractRepository.GetById(id);
            if (action == "submit") // handle submit action
            {
                var isUploadWorkitems = workitemRepository.Any(w => w.ContractId == entity.Id);
                if (!isUploadWorkitems) throw new ServiceException("Vui lòng upload bảng khối lượng trước khi trình duyệt");

                entity.Status = ContractStatus.Submited;
                notificationMessage = string.Format(SUBMITED_MESSAGE, entity.Code);

                var p2Director = userService.GetDirector(Department.KHDT);
                receivers.Add(p2Director.Id);
                var contractUsers = GetContractUsers(entity.Id, ContractKind.B);
                receivers.AddRange(contractUsers);
            }
            //else if (action == "approve") // handle approve action
            //{
            //    entity.Status = ContractStatus.Approved;
            //    entity.Note = null;
            //    notificationMessage = string.Format(APPROVED_MESSAGE, entity.Code);

            //    var p2Director = userService.GetDirector(Department.KHDT);
            //    receivers.Add(p2Director.Id);
            //    var contractUsers = GetContractUsers(entity.Id, ContractKind.B);
            //    receivers.AddRange(contractUsers);
            //}
            else if (action == "approve" || action == "pass") // handle pass to technical division action
            {
                entity.Status = ContractStatus.Passed;
                notificationMessage = string.Format(PASSED_MESSAGE, entity.Code);

                var p2Director = userService.GetDirector(Department.KHDT);
                receivers.Add(p2Director.Id);
                var contractUsers = GetContractUsers(entity.Id, ContractKind.B);
                receivers.AddRange(contractUsers);

                var p4Director = userService.GetDirector(Department.KT);
                if (p4Director != null) receivers.Add(p4Director.Id);

                var p5Director = userService.GetDirector(Department.TCKT);
                if (p5Director != null) receivers.Add(p5Director.Id);
            }
            var result = contractRepository.Update(entity);
            if (result && !string.IsNullOrEmpty(notificationMessage)) notificationService.Notify(receivers, notificationMessage, NotificationType.PassContract, entity.Id);

            return true;
        }

        private List<int> GetContractUsers(int contractId, ContractKind kind)
        {
            return contractUserRepository.Fetch(cu => cu.ContractId == contractId && cu.Kind == kind).Select(cu => cu.UserId).ToList();
        }

        public List<Document> AttachDocuments(int id, List<FileInfoModel> files)
        {
            var documents = new List<Document>();
            foreach (var file in files)
            {
                var document = documentService.AddDocument(file);
                documents.Add(document);
            }
            if (documents.Count > 0)
            {
                var entity = contractRepository.GetById(id);
                var docs = new List<Guid>();
                if (!string.IsNullOrEmpty(entity.Attachments))
                {
                    docs = JsonConvert.DeserializeObject<List<Guid>>(entity.Attachments);
                }
                docs.AddRange(documents.Select(d => d.Id));
                entity.Attachments = JsonConvert.SerializeObject(docs);
                contractRepository.Update(entity);
            }
            return documents;
        }

        public List<ContractGroupModel> GetGroups()
        {
            var list = contractGroupRepository.Fetch().ToList();
            return list.Map<List<ContractGroupModel>>();
        }

        public string NextCode(DateTime signingDate, bool isContract)
        {
            using (var db = contractRepository.GetDbContext())
            {
                var last = db.Contracts.LastOrDefault(c => c.SigningDate.Value.Year == signingDate.Year && (isContract ? c.DocType == DocumentType.Contract : c.DocType == DocumentType.Quotation));
                var thisYearCount = 0;
                if (last != null)
                {
                    try
                    {
                        var no = last.Code.Split('/').First();
                        if (no.StartsWith("0"))
                            no = no.TrimStart(new char[] { '0' });
                        thisYearCount = int.Parse(no);
                    }
                    catch { }
                }

                var lastTBL = db.Quotes.LastOrDefault(c => c.SigningDate.Value.Year == signingDate.Year && (isContract ? c.DocType == DocumentType.Contract : c.DocType == DocumentType.Quotation));
                var thisYearCountTBL = 0;
                if (lastTBL != null)
                {
                    try
                    {
                        var no = lastTBL.Code.Split('/').First();
                        if (no.StartsWith("0"))
                            no = no.TrimStart(new char[] { '0' });
                        thisYearCountTBL = int.Parse(no);
                    }
                    catch { }
                }

                if (thisYearCount > thisYearCountTBL)
                    return string.Format("{0}/{1}" + (isContract == true ? "" : "/BG"), thisYearCount + 1, signingDate.Year);
                else
                    return string.Format("{0}/{1}" + (isContract == true ? "" : "/BG"), thisYearCountTBL + 1, signingDate.Year);
            }
        }

        public Dictionary<int, string> GetTypes()
        {
            var values = Enum.GetValues(typeof(ContractType)).Cast<ContractType>();
            var result = new Dictionary<int, string>();
            foreach (var value in values)
            {
                result.Add((int)value, value.ToDescription());
            }
            return result;
        }

        public bool Delete(int id)
        {
            var entry = contractRepository.GetById(id);
            return contractRepository.Delete(entry);
        }
        #endregion

        #region Workitems
        public List<WorkitemModel> ParseWorkitemFile(int id, byte[] bytes, bool? isStem, ProcessType processType = ProcessType.QT01)
        {
            var fileType = bytes.GetFileType();
            if (fileType.Mime != "application/vnd.ms-excel" && fileType.Mime != "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")
                throw new ServiceException("File không được chấp nhận");

            MemoryStream stream = new MemoryStream();
            stream.Write(bytes, 0, bytes.Length);

            var package = new ExcelPackage(stream);
            ExcelWorksheet worksheet = package.Workbook.Worksheets[0];
            int totalRows = worksheet.Dimension.Rows;

            var cells = worksheet.Cells;
            var sttCell = cells.FirstOrDefault(c => c.Text.Replace(" ", string.Empty).ToLower() == "stt");
            if (sttCell == null) throw new ServiceException("Không tìm thấy tiêu đề cột: STT");
            var titleCell = cells.FirstOrDefault(c => c.Text.Replace(" ", string.Empty).ToLower() == "nộidung");
            var unitCell = cells.FirstOrDefault(c => c.Text.Replace(" ", string.Empty).ToLower() == "đơnvị");
            var quantityCell = cells.FirstOrDefault(c => c.Text.Replace(" ", string.Empty).ToLower() == "sl");
            var priceCell = cells.FirstOrDefault(c => c.Text.Replace(" ", string.Empty).ToLower() == "đơngiá");
            var moneyCell = cells.FirstOrDefault(c => c.Text.Replace(" ", string.Empty).ToLower() == "thànhtiền");


            var endCell = cells.FirstOrDefault(c => c.Text.Replace(" ", string.Empty).ToLower() == "giátrịtrướcthuế");
            if (endCell == null) throw new ServiceException("Không tìm thấy: Giá trị trước thuế");

            var entities = new List<Workitem>();
            var index = 1;
            for (int i = sttCell.Start.Row + 1; i < endCell.Start.Row; i++)
            {
                var quantity = cells[i, quantityCell.Start.Column].Text;
                var price = cells[i, priceCell.Start.Column].Text;
                var money = cells[i, moneyCell.Start.Column].Text;

                var entity = new Workitem
                {
                    ContractId = id,
                    Index = index,
                    No = cells[i, sttCell.Start.Column].Text,
                    Title = cells[i, titleCell.Start.Column].Text,
                    Unit = cells[i, unitCell.Start.Column].Text,
                    Quantity = !string.IsNullOrEmpty(quantity) ? decimal.Parse(quantity) : 0,
                    Price = !string.IsNullOrEmpty(price) ? decimal.Parse(price) : 0,
                    Money = !string.IsNullOrEmpty(money) ? decimal.Parse(money) : 0,
                    ProcessType = processType
                };
                if (isStem.HasValue)
                {
                    if (isStem.Value)
                        entity.IsStem = true;
                }
                entities.Add(entity);
                index++;
            }
            if (workitemRepository.InsertMany(entities))
            {
                return entities.Map<List<WorkitemModel>>();
            }
            return new List<WorkitemModel>();
        }

        public bool AddWorkitemStem(SaveWorkitemStemModel stemModel)
        {
            int contractId = 0;
            if (stemModel.ProcessType == ProcessType.QT01)
            {
               
                var contract = contractRepository.FirstOrDefault(c => c.UID == stemModel.UID);
                if (contract == null)
                    throw new ServiceException("Hợp đồng không tồn tại");
                contractId = contract.Id;
            }
            else
            {
                var contract = quoteRepository.FirstOrDefault(c => c.UID == stemModel.UID);
                if (contract == null)
                    throw new ServiceException("Hợp đồng không tồn tại");
                contractId = contract.Id;
            }




            if (stemModel.Id.HasValue)
            {
                var workitem = workitemRepository.GetById(stemModel.Id.Value);
                if (workitem == null) throw new ServiceException("Hạng mục không tồn tại");
                workitem.Title = stemModel.Title;
                workitem.Unit = stemModel.Unit;
                workitem.Quantity = stemModel.Quantity;
                workitem.Price = stemModel.Price;
                workitem.ProcessType = stemModel.ProcessType;
                if (workitem.Quantity.HasValue && workitem.Price.HasValue)
                    workitem.Money = workitem.Quantity.Value * workitem.Price.Value;
                return workitemRepository.Update(workitem);
            }
            else
            {
                var add = new Workitem
                {
                    Title = stemModel.Title,
                    Unit = stemModel.Unit,
                    Quantity = stemModel.Quantity,
                    Price = stemModel.Price,
                    IsStem = true,
                    ContractId = contractId,
                    ProcessType = stemModel.ProcessType
                };
                if (stemModel.Quantity.HasValue && stemModel.Price.HasValue)
                    add.Money = stemModel.Quantity.Value * stemModel.Price.Value;
                var k = workitemRepository.Insert(add);
                return k;
            }
        }


        public List<WorkitemModel> GetWorkitems(int id, ProcessType processType = ProcessType.QT01)
        {
            var list = workitemRepository.Fetch(w => w.ContractId == id).ToList().OrderBy(w => w.Index);
            return list.Map<List<WorkitemModel>>();
        }
        public List<WorkitemModel> GetWorkitems(Guid uid, ProcessType processType = ProcessType.QT01)
        {

            //var contract = contractRepository.FirstOrDefault(c => c.UID == uid);
            var contractId = 0;
            if (processType == ProcessType.QT01)
            {
                contractId = contractRepository.FirstOrDefault(c => c.UID == uid).Id;
            }
            else
            {
                contractId = quoteRepository.FirstOrDefault(c => c.UID == uid).Id;
            }
            var result = new List<WorkitemModel>();
            var listNotStems = workitemRepository.Fetch(w => w.ContractId == contractId && w.ProcessType == processType && (!w.IsStem.HasValue || w.IsStem.Value == false)).ToList().OrderBy(w => w.Index).Map<List<WorkitemModel>>();
            result.AddRange(listNotStems);

            var listStems = workitemRepository.Fetch(w => w.ContractId == contractId && w.IsStem.Value == true && w.ProcessType == processType).ToList().OrderBy(w => w.Index).Map<List<WorkitemModel>>();
            if (listStems.Count() > 0)
            {
                result.Add(new WorkitemModel { Title = "KHỐI LƯỢNG PHÁT SINH" });
                result.AddRange(listStems);
            }

            var outputs = GetOutputWorkitems(uid, processType);
            foreach (var r in result)
            {
                var output = outputs.FirstOrDefault(c => c.Id == r.Id);
                if (output != null)
                {
                    r.Completed = output.Completed;
                    r.CompletedMoney = output.CompletedMoney;
                }
            }

            return result;
        }
        public List<WorkitemModel> GetOutputWorkitems(Guid uid, ProcessType processType = ProcessType.QT01)
        {
            var phaseIds = new List<int>();
            List<WorkitemModel> models = new List<WorkitemModel>();
            if (processType == ProcessType.QT01)
            {
                var contract = contractRepository.FirstOrDefault(c => c.UID == uid);
                var list = workitemRepository.Fetch(w => w.ContractId == contract.Id && w.ProcessType == processType).ToList().OrderBy(w => w.Index);
                models = list.Map<List<WorkitemModel>>();
                var phases = phaseRepository.Fetch(p => p.ContractId == contract.Id && p.ProcessType == processType);
                phaseIds = phases.Select(p => p.Id).ToList(); ;
            }
            else
            {
                var contract = quoteRepository.FirstOrDefault(c => c.UID == uid);
                var list = workitemRepository.Fetch(w => w.ContractId == contract.Id && w.ProcessType == processType).ToList().OrderBy(w => w.Index);
                models = list.Map<List<WorkitemModel>>();
                var phases = phaseRepository.Fetch(p => p.ContractId == contract.Id && p.ProcessType == processType);
                phaseIds = phases.Select(p => p.Id).ToList(); ;
            }

            var phaseWorkitems = phaseWorkitemRepository.Fetch(p => phaseIds.Contains(p.PhaseId));
            foreach (var model in models)
            {
                model.Completed = phaseWorkitems.Where(p => p.WorkitemId == model.Id).Sum(p => p.CompletedQuantity);
                model.CompletedMoney = model.Completed * model.Price;
                model.AcceptancedMoney = model.AcceptancedQuantity * model.Price;
            }
            return models.Where(m => m.Completed > 0).ToList();
        }
        public bool ClearWorkitems(int ContractId, ProcessType processType = ProcessType.QT01)
        {
            return workitemRepository.DeleteMany(w => w.ContractId == ContractId && w.ProcessType == processType) > 0;
        }
        public bool UpdateAcceptancedQuantity(List<WorkitemModel> workitems)
        {
            foreach (var workitem in workitems)
            {
                var entity = workitemRepository.GetById(workitem.Id);
                if (entity != null) entity.AcceptancedQuantity = workitem.AcceptancedQuantity;
                workitemRepository.Update(entity);
            }
            return true;
        }
        public bool UpdateWorkitemDates(List<WorkitemModel> workitemModels)
        {
            var ids = workitemModels.Select(m => m.Id);
            var entities = workitemRepository.Fetch(p => ids.Contains(p.Id));
            foreach (var entry in entities)
            {
                var model = workitemModels.FirstOrDefault(m => m.Id == entry.Id);
                if (model == null) continue;
                if (model.StartDate.HasValue)
                {
                    entry.StartDate = model.StartDate;
                }
                if (model.EndDate.HasValue)
                {
                    entry.EndDate = model.EndDate;
                }
            }
            workitemRepository.UpdateMany(entities);
            return true;
        }
        #endregion

        #region Plan
        public bool SaveMasterPlan(List<MasterPlanModel> model)
        {
            var removes = model.Where(p => p.Checked == false);
            foreach (var remove in removes)
            {
                var entity = masterPlanRepository.FirstOrDefault(m => m.ContractId == remove.ContractId && m.WorkitemId == remove.WorkitemId && m.DepartmentId == remove.DepartmentId);
                if (entity == null) continue;
                masterPlanRepository.Delete(entity);
            }
            var adds = model.Where(p => p.Checked == true);
            if (adds != null)
                return masterPlanRepository.InsertMany(adds.Map<List<MasterPlan>>());
            return true;
        }

        public List<MasterPlan> GetMasterPlan(int id)
        {
            var entities = masterPlanRepository.Fetch(c => c.ContractId == id);
            return entities;
        }
        #endregion

        #region Phase
        public List<int> GetPhasesCount(int id, ProcessType processType = ProcessType.QT01)
        {
            return phaseRepository.Fetch(p => p.ContractId == id && p.ProcessType == processType).Select(p => p.Id).ToList();
        }
        public List<PhaseModel> GetPhases(int id, ProcessType processType = ProcessType.QT01)
        {
            var entities = phaseRepository.Fetch(p => p.ContractId == id && p.ProcessType == processType).OrderByDescending(p => p.Id).ToList();
            return BuildPhaseList(entities);
        }
        public List<PhaseModel> GetPhases(Guid uid, ProcessType processType = ProcessType.QT01)
        {


            if (processType == ProcessType.QT01)
            {
                var contract = contractRepository.FirstOrDefault(c => c.UID == uid);
                var entities = phaseRepository.Fetch(p => p.ContractId == contract.Id && p.ProcessType == processType).OrderByDescending(p => p.Id).ToList();
                var models = BuildPhaseList(entities);
                models.ForEach(m => { m.ContractUID = uid; m.ContractNumber = contract.Code; });
                return models;
            }
            else
            {
                var contract = quoteRepository.FirstOrDefault(c => c.UID == uid);
                var entities = phaseRepository.Fetch(p => p.ContractId == contract.Id && p.ProcessType == processType).OrderByDescending(p => p.Id).ToList();
                var models = BuildPhaseList(entities);
                models.ForEach(m => { m.ContractUID = uid; m.ContractNumber = contract.Code; });
                return models;
            }


        }
        public List<PhaseModel> GetPhases(int userId, bool hasViewAll)
        {
            var result = new List<PhaseModel>();
            var entities = phaseRepository.Fetch(p => (int)p.Status >= (int)PhaseStatus.Passed);
            var contractIds = entities.Where(c=>c.ProcessType==ProcessType.QT01).Select(c => c.ContractId);
            var contracts = contractRepository.Fetch(c => contractIds.Contains(c.Id));


            var quoteIds = entities.Where(c => c.ProcessType == ProcessType.QT02).Select(c => c.ContractId);
            var quotes = quoteRepository.Fetch(c => quoteIds.Contains(c.Id));

            var models = BuildPhaseList(entities);


            models.ForEach(m =>
            {
                if (m.ProcessType == ProcessType.QT01)
                {
                    var contract = contracts.FirstOrDefault(c => c.Id == m.ContractId);
                    if (contract != null)
                    {
                        m.ContractUID = contract.UID;
                        m.ContractNumber = contract.Code;
                    }
                }
                else
                {
                    var contract = quotes.FirstOrDefault(c => c.Id == m.ContractId);
                    if (contract != null)
                    {
                        m.ContractUID = contract.UID;
                        m.ContractNumber = contract.Code;
                    }
                }
               
            });
            models = models.OrderByDescending(p => p.Id).ToList();

            var userCurrent = userRepository.GetById(userId);

            var phaseIds = models.Select(c => c.Id);
            var phaseStaffs = new List<PhaseStaff>();
            if (phaseIds.Count() > 0)
                phaseStaffs = phaseStaffRepository.Fetch(c => phaseIds.Contains(c.PhaseId));


            foreach (var item in models)
            {
                if (!string.IsNullOrEmpty(item.Departments))
                {
                    var departmentIns = JsonConvert.DeserializeObject<List<int>>(item.Departments);
                    var phaseStaffIns = phaseStaffs.Where(c => c.PhaseId == item.Id);
                    var checkStaffIds = new List<int>();
                    if (phaseStaffIns.Count() > 0)
                        checkStaffIds = phaseStaffIns.Select(c => c.StaffId).ToList();

                    if ((departmentIns.Contains(userCurrent.DepartmentId.Value) && userCurrent.Position == StaffPosition.Director)
                        || checkStaffIds.Contains(userCurrent.Id))
                    {
                        result.Add(item);
                    }
                }


            }
            return result;
        }
        private List<PhaseModel> BuildPhaseList(List<Phase> entities)
        {
            var departments = departmentService.GetTechDepartments();
            var models = entities.Map<List<PhaseModel>>();

            var phaseWorkitemExists = phaseWorkitemRepository.Fetch();
            var staffRequestExists = staffRequestRepository.Fetch();
            var phaseEquipmentExists = phaseEquipmentRepository.Fetch();
            var centreExists = centreRepository.Fetch();


            foreach (var model in models)
            {
                if (!string.IsNullOrEmpty(model.Departments))
                {
                    var list = JsonConvert.DeserializeObject<List<int>>(model.Departments);
                    departments.Where(d => list.Contains(d.Id)).ToList().ForEach(d => model.DepartmentNames += d.Name + " ");
                }
                var entity = entities.FirstOrDefault(p => p.Id == model.Id);
                if (!string.IsNullOrEmpty(entity.Centres))
                {
                    var centers = JsonConvert.DeserializeObject<List<int>>(entity.Centres);
                    var centre = centreExists.FirstOrDefault(c => c.Id == centers.FirstOrDefault());
                    model.DepartmentNames += " " + centre.Code;
                }
                model.WorkitemCount = phaseWorkitemExists.Where(p => p.PhaseId == model.Id).Sum(t => t.Quantity);
                model.OfferedStaffCount = staffRequestExists.Where(p => p.PhaseId == model.Id).Sum(t => t.Offered);
                model.AssignedStaffCount = phaseStaffRepository.Count(p => p.PhaseId == model.Id);
                var totalEquipment = phaseEquipmentExists.Where(c => c.PhaseId == model.Id);
                model.EquipmentCount = totalEquipment.Count();
            }
            return models;
        }

        public PhaseModel UpdatePhase(PhaseModel model)
        {
            var count = phaseRepository.Fetch(p => p.ContractId == model.ContractId && p.ProcessType == model.ProcessType).Count;

            var entity = new Phase();
            if (model.Id.HasValue) entity = phaseRepository.GetById(model.Id.Value);
            else
            {
                entity.ContractId = model.ContractId;
                entity.No = count + 1;
                entity.ProcessType = model.ProcessType;
            }
            if (model.StartDate.HasValue)
                entity.StartDate = model.StartDate.Value;
            if (model.EndDate.HasValue)
                entity.EndDate = model.EndDate.Value;
            if (model.DepartmentList != null && model.DepartmentList.Count > 0)
            {
                entity.Departments = JsonConvert.SerializeObject(model.DepartmentList);

                // delete phase data relevan to removed departments
                var removedDepartments = departmentService.GetDepartments().Where(d => !model.DepartmentList.Contains(d.Id)).ToList();
                var departmentIds = removedDepartments.Select(d => d.Id).ToList();

                phaseWorkitemRepository.DeleteMany(pw => pw.PhaseId == model.Id && departmentIds.Contains(pw.DepartmentId));

                var users = userRepository.Fetch(u => u.DepartmentId.HasValue && departmentIds.Contains(u.DepartmentId.Value));
                var userIds = users.Select(u => u.Id).ToList();
                phaseStaffRepository.DeleteMany(ps => ps.PhaseId == model.Id && userIds.Contains(ps.StaffId));
                staffRequestRepository.DeleteMany(pr => pr.PhaseId == model.Id && departmentIds.Contains(pr.DepartmentId));


                phaseEquipmentRepository.DeleteMany(pe => pe.PhaseId == model.Id && departmentIds.Contains(pe.DepartmentId));
            }

            if (model.DoneStaffsDepartmentList != null)
            {
                entity.DoneStaffsDepartments = JsonConvert.SerializeObject(model.DoneStaffsDepartmentList);
            }

            if (model.CentreId.HasValue)
            {
                entity.Centres = JsonConvert.SerializeObject(new int[] { model.CentreId.Value });
            }

            entity.Title = model.Title;
            entity.Note = model.Note;
            //entity.Status = PhaseStatus.Draft;
            if (model.Id.HasValue) phaseRepository.Update(entity);
            else
            {
                phaseRepository.Insert(entity);
            }

            return GetPhase(entity.Id);
        }
        public PhaseModel GetPhase(int id)
        {
            var entry = phaseRepository.GetById(id);
            var model = entry.Map<PhaseModel>();
            if (!string.IsNullOrEmpty(model.Departments))
            {
                model.DepartmentList = JsonConvert.DeserializeObject<List<int>>(model.Departments);
            }
            if (!string.IsNullOrEmpty(model.DoneStaffsDepartments))
            {
                model.DoneStaffsDepartmentList = JsonConvert.DeserializeObject<List<int>>(model.DoneStaffsDepartments);
            }
            if (!string.IsNullOrEmpty(entry.Centres))
            {
                model.CentreId = JsonConvert.DeserializeObject<List<int>>(entry.Centres).FirstOrDefault();
            }

            var departmentList = new List<int>();
            if (!string.IsNullOrEmpty(entry.Departments))
            {
                departmentList = JsonConvert.DeserializeObject<List<int>>(entry.Departments);
            }
            var doneStaffsDepartmentList = new List<int>();
            if (!string.IsNullOrEmpty(entry.DoneStaffsDepartments))
            {
                doneStaffsDepartmentList = JsonConvert.DeserializeObject<List<int>>(entry.DoneStaffsDepartments);
            }
            bool allDone = false;
            foreach (var d in departmentList)
            {
                if (!doneStaffsDepartmentList.Contains(d)) { allDone = false; break; }
                allDone = true;
            }
            model.PhaseStaffAllDone = allDone;



            var phaseCheckId = id;

            var phaseStaffs = phaseStaffRepository.Fetch(c => c.PhaseId == id);
            if (model.Status == PhaseStatus.Passed || model.Status == PhaseStatus.ApprovedbyP3Manager)
            {
                var staffInPhases = phaseStaffs.Where(c => c.PhaseId == model.Id && c.Status == PhaseStaffStatus.StillWork && c.ReplaceUserId.HasValue);

                var staffInPhaseWithReplaces = staffInPhases.Where(c => c.ReplaceUserId.HasValue);
                if (staffInPhaseWithReplaces.Count() > 0)
                {
                    var staffInPhaseIds = staffInPhases.Select(c => c.StaffId).ToList();
                    if (!staffInPhaseWithReplaces.Any(c => staffInPhaseIds.Contains(c.ReplaceUserId.Value)))
                        model.IsModification = true;
                }

                var staffAdds = phaseStaffs.Where(c => !c.ReplaceUserId.HasValue && c.ReplaceDate.HasValue);
                if (staffAdds.Count() > 0)
                {
                    model.IsModification = true;
                }
            }

            return model;
        }
        public List<WorkitemModel> GetUnassignedWorkitems(int phaseId)
        {
            var phase = phaseRepository.GetById(phaseId);
            var was = phaseWorkitemRepository.Fetch(p => p.PhaseId == phaseId);
            var workitemIds = was.Select(w => w.WorkitemId);

            var unassignedWorkitems = workitemRepository.Fetch(w => w.ContractId == phase.ContractId && !workitemIds.Contains(w.Id)).OrderBy(w => w.Index);
            return unassignedWorkitems.Map<List<WorkitemModel>>();
        }
        public List<WorkitemModel> GetWorkitemsByPhase(int phaseId)
        {
            var was = phaseWorkitemRepository.Fetch(p => p.PhaseId == phaseId);
            var workitemIds = was.Select(w => w.WorkitemId);

            var workitems = workitemRepository.Fetch(w => workitemIds.Contains(w.Id));
            return workitems.Map<List<WorkitemModel>>();
        }
        public bool AddWorkitemsToPhase(PhaseWorkitemAddModel model)
        {
            var phase = GetPhase(model.PhaseId);
            if (phase.DepartmentList.Count == 0) throw new ServiceException("Chưa chọn phòng chuyên môn tham gia đợt");
            var departmentIds = phase.DepartmentList;
            if (phase.CentreId.HasValue) departmentIds.Add(phase.CentreId.Value);

            var masterPlans = masterPlanRepository.Fetch(m => m.ContractId == phase.ContractId);
            var workitems = workitemRepository.Fetch(w => w.ContractId == phase.ContractId);

            var entities = new List<PhaseWorkitem>();
            foreach (var workitemId in model.WorkitemIds)
            {
                foreach (var departmentId in departmentIds)
                {
                    var masterPlan = masterPlans.FirstOrDefault(m => m.WorkitemId == workitemId && m.DepartmentId == departmentId);
                    var initialQuantity = 0;
                    if (masterPlan != null)
                    {
                        var workitem = workitems.FirstOrDefault(w => w.Id == masterPlan.WorkitemId);
                        if (workitem != null) initialQuantity = Convert.ToInt32(workitem.Quantity.Value);
                    }
                    var entity = new PhaseWorkitem
                    {
                        PhaseId = model.PhaseId,
                        WorkitemId = workitemId,
                        DepartmentId = departmentId,
                        Quantity = initialQuantity
                    };
                    entities.Add(entity);
                }
            }
            return phaseWorkitemRepository.InsertMany(entities);
        }
        public List<PhaseWorkitemModel> GetPhaseWorkitems(int phaseId)
        {
            var entities = phaseWorkitemRepository.Fetch(w => w.PhaseId == phaseId);
            var models = entities.Map<List<PhaseWorkitemModel>>();
            return models;
        }
        public bool UpdatePhaseWorkitems(List<PhaseWorkitem> phaseWorkitems)
        {
            foreach (var phaseWorkitem in phaseWorkitems)
            {
                var entity = phaseWorkitemRepository.FirstOrDefault(w => w.PhaseId == phaseWorkitem.PhaseId && w.WorkitemId == phaseWorkitem.WorkitemId && w.DepartmentId == phaseWorkitem.DepartmentId);
                if (entity != null)
                {
                    entity.Quantity = phaseWorkitem.Quantity;
                    phaseWorkitemRepository.Update(entity);
                }
                else
                {
                    phaseWorkitemRepository.Insert(phaseWorkitem);
                }
            }
            return true;
        }

        public bool DeletePhaseWorkitem(int phaseId, int workitemId)
        {
            return phaseWorkitemRepository.DeleteMany(t => t.PhaseId == phaseId && t.WorkitemId == workitemId) > 0;
        }

        public List<PhaseStaffRequest> GetRequestedStaffs(int phaseId)
        {
            return staffRequestRepository.Fetch(s => s.PhaseId == phaseId).ToList();
        }

        public List<PhaseStaffModel> GetPhaseStaffs(int phaseId)
        {
            var entities = phaseStaffRepository.Fetch(p => p.PhaseId == phaseId && p.Status == PhaseStaffStatus.StillWork);
            var userIds = entities.Select(e => e.StaffId);
            var users = userRepository.Fetch(u => userIds.Contains(u.Id));
            var userAlls = userRepository.Fetch();
            var models = entities.Map<List<PhaseStaffModel>>();
            foreach (var model in models)
            {
                if (model.ReplaceUserId.HasValue)
                {
                    var replaceUser = userAlls.FirstOrDefault(u => u.Id == model.ReplaceUserId);
                    model.ReplaceUserName = replaceUser.FullName;
                }

                var user = users.FirstOrDefault(u => u.Id == model.StaffId);
                if (user != null)
                {
                    model.DepartmentId = user.DepartmentId ?? 0;
                    model.StaffName = user.FullName;
                    model.SafeLevel = user.SafeLevel ?? 0;
                    model.WorkLevel = user.WorkLevel ?? 0;
                }
            }
            return models.OrderByDescending(e => e.PositionInPhase).ToList();
        }

        public bool SaveRequestedStaffs(List<PhaseStaffRequest> requests)
        {
            foreach (var request in requests)
            {
                var entity = staffRequestRepository.FirstOrDefault(w => w.PhaseId == request.PhaseId && w.DepartmentId == request.DepartmentId);
                if (entity != null)
                {
                    entity.Offered = request.Offered;
                    staffRequestRepository.Update(entity);
                }
                else
                {
                    staffRequestRepository.Insert(request);
                }
            }
            return true;
        }
        public bool DonePhaseStaffs(int phaseId, int departmentId)
        {
            var phaseModel = GetPhase(phaseId);

            var department = (Department)departmentId;
            if (phaseModel.DoneStaffsDepartmentList.Contains(departmentId)) return true;
            phaseModel.DoneStaffsDepartmentList.Add(departmentId);

            UpdatePhase(phaseModel);

            phaseModel = GetPhase(phaseId);

            if (phaseModel.PhaseStaffAllDone)
            {
                if (phaseModel.ProcessType==ProcessType.QT01)
                {
                    var contract = contractRepository.GetById(phaseModel.ContractId);
                    string messege = string.Format("Tất cả PCM đã xong việc thêm nhân sự cho Đợt {0} HĐ {1}", phaseModel.No, contract.Code);

                    var receivers = new List<int>();

                    //Người phụ trách hđ
                    var p2ContractUsers = contractUserRepository.Fetch(cu => cu.DepartmentId == Department.KHDT && cu.ContractId == contract.Id && cu.Kind == ContractKind.Quote);
                    receivers.AddRange(p2ContractUsers.Select(cu => cu.UserId));

                    // p2 director
                    var p2Director = userService.GetDirector(Department.KHDT);
                    if (p2Director != null) receivers.Add(p2Director.Id);

                    var p4Director = userService.GetDirector(Department.KT);
                    if (p4Director != null) receivers.Add(p4Director.Id);

                    notificationService.Notify(receivers, messege, NotificationType.PhaseStaffAllDone, phaseId);
                }
                else
                {
                    var context = contractRepository.GetDbContext();
                    var phase =  context.Phases.FirstOrDefault(c => c.Id == phaseId);
                    phase.Status = PhaseStatus.Published;
                    context.SaveChanges();

                    var contract = quoteRepository.GetById(phaseModel.ContractId);
                    string messege = string.Format("Tất cả PCM đã xong việc thêm nhân sự cho Đợt {0} HĐ {1}", phaseModel.No, contract.Code);

                    var receivers = new List<int>();

                    //Người phụ trách hđ
                    var p2ContractUsers = contractUserRepository.Fetch(cu => cu.DepartmentId == Department.KHDT && cu.ContractId==contract.Id && cu.Kind==ContractKind.Quote);
                    receivers.AddRange(p2ContractUsers.Select(cu => cu.UserId));

                    // p2 director
                    var p2Director = userService.GetDirector(Department.KHDT);
                    if (p2Director != null) receivers.Add(p2Director.Id);

                    //var p4Director = userService.GetDirector(Department.KT);
                    //if (p4Director != null) receivers.Add(p4Director.Id);

                    notificationService.Notify(receivers, messege, NotificationType.TBLPhaseStaffAllDone, phaseId);
                }

            }
            return true;
        }

        public List<StaffStateModel> GetUnassignedStaffs(DepartmentStaffFilterModel filter)
        {
            var phase = phaseRepository.GetById(filter.PhaseId);

            var phaseStaffs = phaseStaffRepository.Fetch(ps =>
            (phase.StartDate <= ps.StartTime && ps.StartTime <= phase.EndDate) ||
            (ps.StartTime <= phase.StartDate && phase.EndDate <= ps.EndTime) ||
            (ps.EndTime <= phase.StartDate && ps.EndTime <= phase.EndDate)
            );
            var phaseIds = phaseStaffs.Select(p => p.PhaseId);
            var phases = phaseRepository.Fetch(p => phaseIds.Contains(p.Id));
            var contractIds = phases.Where(c=>c.ProcessType==ProcessType.QT01).Select(p => p.ContractId);
            var contracts = contractRepository.Fetch(c => contractIds.Contains(c.Id));

            var quoteIds = phases.Where(c => c.ProcessType == ProcessType.QT02).Select(p => p.ContractId);
            var quotes = quoteRepository.Fetch(c => quoteIds.Contains(c.Id));

            var staffs = userRepository.Fetch(w => w.DepartmentId == filter.DepartmentId);
            var userIds = staffs.Select(u => u.Id);

            var userCalendars = userCalendarRepository.Fetch(uc =>
            userIds.Contains(uc.UserId) &&
            (phase.StartDate <= uc.DateFrom && uc.DateFrom <= phase.EndDate) ||
            (uc.DateFrom <= phase.StartDate && phase.EndDate <= uc.DateTo) ||
            (uc.DateTo <= phase.StartDate && uc.DateTo <= phase.EndDate)
            );

            var models = new List<StaffStateModel>();
            foreach (var staff in staffs)
            {
                var model = new StaffStateModel { StaffId = staff.Id, FullName = staff.FullName, DepartmentId = staff.DepartmentId };
                if (phaseStaffs.Any(ps => ps.StaffId == staff.Id)) // dang ban
                {
                    model.Busy = true;
                    var busyPhaseIds = phaseStaffs.Where(ps => ps.StaffId == staff.Id).Select(ps => ps.PhaseId);
                    var busyPhases = phases.Where(p => busyPhaseIds.Contains(p.Id));
                    foreach (var busyPhase in busyPhases)
                    {
                        var contractCode = string.Empty;
                        if (busyPhase.ProcessType == ProcessType.QT01)
                        {
                            var contract = contracts.FirstOrDefault(c => c.Id == busyPhase.ContractId);
                            if (contract!=null)
                            {
                                contractCode = contract.Code;
                            }
                        }
                        else
                        {
                            var contract = quotes.FirstOrDefault(c => c.Id == busyPhase.ContractId);
                            if (contract != null)
                            {
                                contractCode = contract.Code;
                            }
                        }
                        
                        
                        string temp = string.Format("Đợt {0} HĐ {1} Từ {2} Đến {3}", busyPhase.No, contractCode, busyPhase.StartDate.ToString("dd/MM/yyyy HH:mm"), busyPhase.EndDate.ToString("dd/MM/yyyy HH:mm"));
                        model.BusyPhases.Add(temp);
                    }

                }

                if (userCalendars.Any(uc => uc.UserId == staff.Id && (int)uc.Type == 1))
                {
                    model.Break = true;
                }

                if (userCalendars.Any(uc => uc.UserId == staff.Id && (int)uc.Type == 2))
                {
                    model.Offline = true;
                }

                models.Add(model);
            }

            if (!filter.Busy) models = models.Where(m => !m.Busy).ToList();
            if (!filter.Offline) models = models.Where(m => !m.Offline).ToList();
            if (!filter.Break) models = models.Where(m => !m.Break).ToList();

            return models;
        }

        public bool AddStaffsToPhase(int phaseId, List<int> staffIds)
        {
            var entities = new List<PhaseStaff>();
            var phase = phaseRepository.GetById(phaseId);
            //if (phase.Status == PhaseStatus.Published) throw new ServiceException("Kế hoạch đợt đã phát hành QĐ, không thể thêm nhân sự");

            foreach (var staffId in staffIds)
            {
                var entity = new PhaseStaff
                {
                    PhaseId = phaseId,
                    StaffId = staffId,
                    Position = PCMAssignPotition.Member,
                    StartTime = phase.StartDate,
                    EndTime = phase.EndDate,
                    Status = PhaseStaffStatus.StillWork,
                    IsOriginal = true,
                };
                entities.Add(entity);
            }

            if (phase.Status == PhaseStatus.Published)// notify p3
            {
                var contract = contractRepository.GetById(phase.ContractId);
                var conractUsers = contractUserRepository.Fetch(u => u.DepartmentId == Department.TCNS && u.ContractId == phase.ContractId);
                var message = string.Format("Kế hoạch ĐỢT {0} HĐ {1} có sự thay đổi nhân sự", phase.No, contract.Code);
                notificationService.Notify(conractUsers.Select(u => u.UserId).ToList(), message, NotificationType.PhaseChangeStaffs, phase.Id);
            }
            return phaseStaffRepository.InsertMany(entities);
        }

        public bool SetPhaseStaffPositionFromPKT(int phaseStaffId, PhaseStaffPosition position)
        {
            var entity = phaseStaffRepository.GetById(phaseStaffId);

            if (position == PhaseStaffPosition.Captain && phaseStaffRepository.Any(ps => ps.PhaseId == entity.PhaseId && ps.PositionInPhase == position && ps.Status == PhaseStaffStatus.StillWork))
                throw new ServiceException("Chỉ cho phép chọn 1 đội trưởng");

            entity.PositionInPhase = position;
            return phaseStaffRepository.Update(entity);
        }

        public bool SetPhaseStaffPositionFromSpecialDepartments(int phaseStaffId, PCMAssignPotition position, int userId)
        {
            var entity = phaseStaffRepository.GetById(phaseStaffId);

            var phaseStaffs = phaseStaffRepository.Fetch(c => c.PhaseId == entity.PhaseId);

            var staffIds = phaseStaffs.Select(c => c.StaffId);

            var users = userRepository.Fetch(c => staffIds.Contains(c.Id));

            var userCurrent = userRepository.GetById(userId);
            var userFilterDepartmenIds = users.Where(c => c.DepartmentId == userCurrent.DepartmentId).Select(c => c.Id);
            phaseStaffs = phaseStaffs.Where(c => userFilterDepartmenIds.Contains(c.StaffId)).ToList();

            if (phaseStaffs.Any(ps => ps.Position == PCMAssignPotition.Leader))
                throw new ServiceException("Chỉ cho phép chọn 1 trưởng nhóm");

            entity.Position = position;
            return phaseStaffRepository.Update(entity);
        }

        public bool RemovePhaseStaff(int phaseStaffId)
        {
            var entity = phaseStaffRepository.GetById(phaseStaffId);
            var phase = phaseRepository.GetById(entity.PhaseId);
            if (phase.Status == PhaseStatus.Published)// notify p3
            {
                var contract = contractRepository.GetById(phase.ContractId);
                var conractUsers = contractUserRepository.Fetch(u => u.DepartmentId == Department.TCNS && u.ContractId == phase.ContractId);
                var message = string.Format("Kế hoạch ĐỢT {0} HĐ {1} có sự thay đổi nhân sự", phase.No, contract.Code);
                notificationService.Notify(conractUsers.Select(u => u.UserId).ToList(), message, NotificationType.PhaseChangeStaffs, phase.Id);
            }

            return phaseStaffRepository.Delete(entity);
        }
        public bool FinishPhaseStaff(int phaseStaffId)
        {

            var entity = phaseStaffRepository.GetById(phaseStaffId);
            if (entity.PositionInPhase == PhaseStaffPosition.Captain)
                throw new ServiceException("Không thể rút đội trưởng");
            if (entity.PositionInPhase == PhaseStaffPosition.Safety)
                throw new ServiceException("Không thể rút an toàn viên");

            var phaseStaffs = phaseStaffRepository.Fetch(c => c.PhaseId == entity.PhaseId);
            var staffIds = phaseStaffs.Select(c => c.StaffId);
            var userFinish = userRepository.GetById(entity.StaffId);
            var userIds = userRepository.Fetch(c => staffIds.Contains(c.Id) && c.DepartmentId == userFinish.DepartmentId).Select(c => c.Id);
            var phaseStaffByDepartments = phaseStaffs.Where(c => userIds.Contains(c.StaffId));
            if (phaseStaffByDepartments.Count(c => c.Position == PCMAssignPotition.Leader) == 1 && entity.Position == PCMAssignPotition.Leader)
                throw new ServiceException("Yêu cầu chọn trưởng nhóm khác khi rút về");

            var phaseId = entity.PhaseId;
            var phase = phaseRepository.GetById(phaseId);
            //if (phase.Status != PhaseStatus.Published) throw new ServiceException("Kế hoạch đợt chưa phát hành QĐ, không thể rút nhân sự");
            entity.EndTime = DateTime.Now;
            entity.Status = PhaseStaffStatus.Desist;
            return phaseStaffRepository.Update(entity);
        }
        public List<PhaseEquipmentModel> GetPhaseEquipments(int phaseId)
        {
            var entities = phaseEquipmentRepository.Fetch(p => p.PhaseId == phaseId);
            var phaseEquipments = entities.Map<List<PhaseEquipmentModel>>();
            var equipmentIds = phaseEquipments.Select(e => e.EquipmentId);
            var equipments = equipmentRepository.Fetch(e => equipmentIds.Contains(e.Id));

            foreach (var phaseEquipment in phaseEquipments)
            {
                var equipment = equipments.FirstOrDefault(e => e.Id == phaseEquipment.EquipmentId);
                if (equipment != null)
                {
                    phaseEquipment.NameForPCM = equipment.NameForPCM;
                    phaseEquipment.NameForCompany = equipment.NameForCompany;
                    phaseEquipment.Imei = equipment.Imei;
                    phaseEquipment.Code = equipment.Code;
                }
            }
            return phaseEquipments;
        }

        public bool AddEquipmentsToPhase(int phaseId, int departmentId, List<int> equipmentIds)
        {
            var entities = new List<PhaseEquipment>();

            var exists = phaseEquipmentRepository.Fetch(c => c.PhaseId == phaseId && c.DepartmentId == departmentId && equipmentIds.Contains(c.EquipmentId));

            foreach (var equipmentId in equipmentIds)
            {
                if (exists.FirstOrDefault(c => c.EquipmentId == equipmentId) != null)
                    throw new ServiceException("Thiết bị đã tham gia đợt này");
                var entity = new PhaseEquipment
                {
                    PhaseId = phaseId,
                    DepartmentId = departmentId,
                    EquipmentId = equipmentId,
                };
                entities.Add(entity);
            }
            return phaseEquipmentRepository.InsertMany(entities);
        }

        public bool DeleteEquipmentsFromPhase(int phaseId, int departmentId, List<int> equipmentIds, int userId)
        {
            var result = false;
            if (equipmentIds.Count() == 0) throw new ServiceException("Yêu cầu chọn thiết bị để xóa khỏi đợt");
            using (var db = phaseEquipmentRepository.GetDbContext())
            {
                using (var trans = db.Database.BeginTransaction())
                {
                    try
                    {
                        var phaseEquipments = phaseEquipmentRepository.Fetch(c => c.DepartmentId == departmentId && equipmentIds.Contains(c.EquipmentId) && c.PhaseId == phaseId);
                        foreach (var item in phaseEquipments)
                        {
                            result = phaseEquipmentRepository.Delete(item);
                            if (!result)
                                break;
                        }
                        if (!result) trans.Rollback();
                    }
                    catch (Exception ex)
                    {
                        trans.Rollback();
                        throw ex;
                    }
                    trans.Commit();
                }
            }
            return result;
        }


        public bool SubmitPhase(int phaseId, int userId)
        {
            var entity = phaseRepository.GetById(phaseId);
            entity.Status = PhaseStatus.Submited;
            phaseRepository.Update(entity);

            var user = userRepository.GetById(userId);
            var contractCode = string.Empty;
           
            if (entity.ProcessType==ProcessType.QT01)
            {
                var contract = contractRepository.GetById(entity.ContractId);
                contractCode = contract.Code;
                var notificationMessage = string.Format("{0} đã khởi tạo và trình duyệt kế hoạch đợt {1} trong dự án {2}", user.FullName, entity.No, contractCode);

                // p4
                var receivers = new List<int>();
                var techDirector = userRepository.FirstOrDefault(u => u.DepartmentId == (int)Department.KT);
                if (techDirector != null) receivers.Add(techDirector.Id);

                notificationService.Notify(receivers, notificationMessage, NotificationType.SubmitPhase, entity.Id);
            }
            else
            {
                var contract = quoteRepository.GetById(entity.ContractId);
                contractCode = contract.Code;

                var notificationMessage = string.Format("{0} đã khởi tạo và trình duyệt kế hoạch đợt {1} trong dự án {2}", user.FullName, entity.No, contractCode);

                // p2
                var receivers = new List<int>();
                var techDirector = userRepository.FirstOrDefault(u => u.DepartmentId == (int)Department.KHDT);
                if (techDirector != null) receivers.Add(techDirector.Id);
                notificationService.Notify(receivers, notificationMessage, NotificationType.TBLSubmitPhase, entity.Id);
            }
            
            return true;
        }

        public bool SubmitPhaseToP3Manager(int phaseId, int userId)
        {
            var entity = phaseRepository.GetById(phaseId);
            entity.Status = PhaseStatus.SubmitedToP3Manager;

            var phaseStaffs = phaseStaffRepository.Fetch(c => c.PhaseId == phaseId);
            if (!phaseStaffs.Where(c => c.Status == PhaseStaffStatus.StillWork).Any(c => c.PositionInPhase == PhaseStaffPosition.Captain))
                throw new ServiceException("Chưa chọn đội trưởng đội công tác");
            if (!phaseStaffs.Where(c => c.Status == PhaseStaffStatus.StillWork).Any(c => c.PositionInPhase == PhaseStaffPosition.Safety))
                throw new ServiceException("Chưa chọn an toàn viên");

            phaseRepository.Update(entity);

            var user = userRepository.GetById(userId);
            var contract = contractRepository.GetById(entity.ContractId);
            var notificationMessage = string.Format("{0} đã trình quyết định thành lập đội của đợt {1} trong dự án {2}", user.FullName, entity.No, contract.Code);

            // p4
            var receivers = new List<int>();
            var techDirector = userRepository.FirstOrDefault(u => u.DepartmentId == (int)Department.TCNS);
            if (techDirector != null) receivers.Add(techDirector.Id);

            notificationService.Notify(receivers, notificationMessage, NotificationType.SubmitedToP3Manager, entity.Id);
            return true;
        }

        public bool RejectPhase(int phaseId, string reason)
        {
            var entity = phaseRepository.GetById(phaseId);
            entity.Status = PhaseStatus.Rejected;
            entity.RejectReason = reason;
            phaseRepository.Update(entity);

            if (entity.ProcessType==ProcessType.QT01)
            {
                var contract = contractRepository.GetById(entity.ContractId);
                var notificationMessage = string.Format("Kế hoạch đợt {0} trong dự án {1} bị trả lại", entity.No, contract.Code);

                // p4
                var receivers = new List<int>();
                var techDirector = userRepository.FirstOrDefault(u => u.DepartmentId == (int)Department.KT);
                if (techDirector != null) receivers.Add(techDirector.Id);

                notificationService.Notify(receivers, notificationMessage, NotificationType.RejectPhase, entity.Id);
            }
            else
            {
                //var contract = quoteRepository.GetById(entity.ContractId);
                //var notificationMessage = string.Format("Kế hoạch đợt {0} trong dự án {1} bị trả lại", entity.No, contract.Code);

                //// p4
                //var receivers = new List<int>();
                //var techDirector = userRepository.FirstOrDefault(u => u.DepartmentId == (int)Department.KT);
                //if (techDirector != null) receivers.Add(techDirector.Id);

                //notificationService.Notify(receivers, notificationMessage, NotificationType.RejectPhase, entity.Id);
            }
            return true;

        }
        public bool ApprovePhase(int phaseId)
        {
            var entity = phaseRepository.GetById(phaseId);
           

           
            var notificationMessage = string.Empty;
            if (entity.ProcessType==ProcessType.QT01)
            {
                entity.Status = PhaseStatus.Approved;
                phaseRepository.Update(entity);
                var contract = contractRepository.GetById(entity.ContractId);
                notificationMessage = string.Format("Kế hoạch đợt {0} trong dự án {1} đã được duyệt", entity.No, contract.Code);
                // p4
                var receivers = new List<int>();
                var techDirector = userRepository.FirstOrDefault(u => u.DepartmentId == (int)Department.KT);
                if (techDirector != null) receivers.Add(techDirector.Id);
                notificationService.Notify(receivers, notificationMessage, NotificationType.ApprovePhase, entity.Id);
            }
            else
            {
                //p2 phê duyệt thì chuyển phòng chuyên môn.
                entity.Status = PhaseStatus.Passed;
                phaseRepository.Update(entity);
                var contract = quoteRepository.GetById(entity.ContractId);
                notificationMessage = string.Format("Kế hoạch đợt {0} trong dự án {1} đã được duyệt và chuyển đến các phòng chuyên môn.", entity.No, contract.Code);
                // pcm
                var phaseModel = GetPhase(entity.Id);
                var receivers = new List<int>();
                if (phaseModel.DepartmentList.Count > 0)
                {
                    var departmentDirectors = userRepository.Fetch(u => u.Position == StaffPosition.Director && phaseModel.DepartmentList.Contains(u.DepartmentId.Value));
                    departmentDirectors.ForEach(d => receivers.Add(d.Id));
                }
                notificationService.Notify(receivers, notificationMessage, NotificationType.TBLPassPhase, entity.Id);
            }
            

          

            return true;
        }


        public bool ApprovePhaseByP3Manager(int phaseId)
        {
            var entity = phaseRepository.GetById(phaseId);
            entity.Status = PhaseStatus.ApprovedbyP3Manager;
            phaseRepository.Update(entity);

            var contract = contractRepository.GetById(entity.ContractId);
            var notificationMessage = string.Format("Quyết định thành lập đội {0} trong dự án {1} đã được duyệt", entity.No, contract.Code);

            // p4
            var receivers = new List<int>();
            var techDirector = userRepository.FirstOrDefault(u => u.DepartmentId == (int)Department.TCNS);
            if (techDirector != null) receivers.Add(techDirector.Id);

            notificationService.Notify(receivers, notificationMessage, NotificationType.ApprovedbyP3Manager, entity.Id);

            return true;
        }

        public bool PassPhase(int phaseId)
        {
            var entity = phaseRepository.GetById(phaseId);
            entity.Status = PhaseStatus.Passed;
            phaseRepository.Update(entity);
            // notify truong phong chuyen mon

            var contract = contractRepository.GetById(entity.ContractId);
            var notificationMessage = string.Format("Kế hoạch đợt {0} trong dự án {1} đã chuyển đến các phòng chuyên môn", entity.No, contract.Code);

            // p4
            var receivers = new List<int>();
            var techDirector = userRepository.FirstOrDefault(u => u.DepartmentId == (int)Department.KT);
            if (techDirector != null) receivers.Add(techDirector.Id);

            // pcm
            var phaseModel = GetPhase(entity.Id);
            if (phaseModel.DepartmentList.Count > 0)
            {
                var departmentDirectors = userRepository.Fetch(u => u.Position == StaffPosition.Director && phaseModel.DepartmentList.Contains(u.DepartmentId.Value));
                departmentDirectors.ForEach(d => receivers.Add(d.Id));
            }
            notificationService.Notify(receivers, notificationMessage, NotificationType.PassPhase, entity.Id);

            return true;
        }
        public bool PassPhaseToP3(int phaseId)
        {
            var entity = phaseRepository.GetById(phaseId);
            entity.Status = PhaseStatus.PassedToP3;
            phaseRepository.Update(entity);

            var contract = contractRepository.GetById(entity.ContractId);
            var notificationMessage = string.Format("Kế hoạch đợt {0} trong dự án {1} đã chuyển đến P3", entity.No, contract.Code);

            // p2
            var receivers = new List<int>();
            var p2Director = userService.GetDirector(Department.KHDT);
            if (p2Director != null) receivers.Add(p2Director.Id);

            var p3Director = userService.GetDirector(Department.TCNS);
            if (p3Director != null) receivers.Add(p3Director.Id);


            var contractUsers = contractUserRepository.Fetch(c => c.ContractId == entity.ContractId);
            if (contractUsers.Count() > 0)
            {
                var userP3s = contractUsers.Where(c => c.DepartmentId == Department.TCNS).ToList();
                if (userP3s.Count() > 0)
                {
                    var userP3Ids = userP3s.Select(c => c.UserId).ToList();
                    receivers.AddRange(userP3Ids);
                }

            }
            receivers = receivers.Distinct().ToList();
            notificationService.Notify(receivers, notificationMessage, NotificationType.PassPhaseToP3, entity.Id);

            return true;
        }
        public bool PublishPhase(int id)
        {
            var result = false;
            var entity = phaseRepository.GetById(id);

            // check phase staffs
            var phaseStaffs = phaseStaffRepository.Fetch(c => c.PhaseId == id);
            if (!phaseStaffs.Any(p => p.PositionInPhase == PhaseStaffPosition.Captain)) throw new ServiceException("Chưa chọn đội trưởng");

            if (!phaseStaffs.Any(p => p.PositionInPhase == PhaseStaffPosition.Safety)) throw new ServiceException("Chưa chọn an toàn viên");

            var phase = GetPhase(id);
            var contract = contractRepository.GetById(phase.ContractId);
            if (phase.IsModification)
                entity.IsModify = true;

            foreach (var staff in phaseStaffs)
            {
                if (staff.ReplaceUserId.HasValue)
                {
                    if (staff.Status == PhaseStaffStatus.StillWork)
                    {
                        phaseStaffRepository.Insert(new PhaseStaff
                        {
                            PhaseId = staff.PhaseId,
                            StaffId = staff.ReplaceUserId.Value,
                            Position = staff.Position,
                            PositionInPhase = staff.PositionInPhase,
                            StartTime = DateTime.UtcNow,
                            EndTime = entity.EndDate,
                            ReplaceDate = DateTime.UtcNow,
                            Status = PhaseStaffStatus.StillWork,
                            IsOriginal = false
                        });
                        staff.Status = PhaseStaffStatus.HasBeenReplaced;
                        phaseStaffRepository.Update(staff);
                    }
                }
            }




            entity.Status = PhaseStatus.Published;

            result = phaseRepository.Update(entity);
            if (result)
            {
                string messenger = string.Format("Đã phát hành quyết định cho ĐỢT {0} HĐ {1}", phase.No, contract.Code);

                var receivers = new List<int>() { };

                var departments = phase.DepartmentList;
                foreach (var department in departments)
                {
                    // Trưởng phòng CM
                    var director = userService.GetDirector((Department)department);
                    receivers.Add(director.Id);
                }
                receivers.AddRange(phaseStaffs.Select(c => c.StaffId));
                receivers = receivers.Distinct().ToList();
                notificationService.Notify(receivers, messenger, NotificationType.PublishPhase, phase.Id.Value);
            }


            return result;
        }

        public bool CompletePhase(int phaseId)
        {
            var entity = phaseRepository.GetById(phaseId);
            entity.Status = PhaseStatus.Completed;
            return phaseRepository.Update(entity);
        }

        public bool AddVehicle(int phaseId, int vehicleId)
        {
            var entity = new PhaseVehicle
            {
                PhaseId = phaseId,
                VehicleId = vehicleId
            };
            return phaseVehileRepository.Insert(entity);
        }

        public List<VehicleModel> GetVehicles(int phaseId)
        {
            var phaseVehicles = phaseVehileRepository.Fetch(f => f.PhaseId == phaseId);
            var vehicleIds = phaseVehicles.Select(p => p.VehicleId).ToList();
            var vehicles = vehicleRepository.Fetch(v => vehicleIds.Contains(v.Id));
            return vehicles.Map<List<VehicleModel>>();
        }

        public bool RemoveVehicle(int phaseId, int vehicleId)
        {
            var entity = phaseVehileRepository.FirstOrDefault(pv => pv.PhaseId == phaseId && pv.VehicleId == vehicleId);
            return phaseVehileRepository.Delete(entity);
        }

        public bool SupplementStaffsForPhase(List<ReplacementStaffModel> models, int userId)
        {
            var result = false;

            var phaseChecks = models.Select(c => c.PhaseId);
            var phaseExists = phaseRepository.Fetch(c => phaseChecks.Contains(c.Id));

            if (phaseExists.Count() == 0) throw new ServiceException("Không tồn tại kế hoạch đợt");

            var phaseExistIds = phaseExists.Select(c => c.Id);

            var phaseStaffExists = phaseStaffRepository.Fetch(c => phaseExistIds.Contains(c.PhaseId));

            var staffCheckIds = phaseStaffExists.Select(c => c.StaffId);

            var staffExists = userRepository.Fetch(c => staffCheckIds.Contains(c.Id));

            var users = userRepository.Fetch();

            using (var context = phaseRepository.GetDbContext())
            {
                using (var trans = context.Database.BeginTransaction())
                {
                    try
                    {
                        foreach (var model in models)
                        {
                            var staff = users.FirstOrDefault(c => c.Id == model.StaffId);

                            var userCurrent = users.FirstOrDefault(c => c.Id == userId);

                            var department = staff.DepartmentId;
                            var director = userService.GetDirector((Department)department);
                            if (department != userCurrent.DepartmentId) throw new ServiceException("Bạn không có quyền thực hiện tính năng này");

                            var phaseExist = phaseExists.FirstOrDefault(c => c.Id == model.PhaseId);
                            if (phaseExist == null) throw new ServiceException("Không tồn tại kế hoạch đợt");
                            var staffExist = phaseStaffExists.FirstOrDefault(c => c.StaffId == model.StaffId);

                            if (model.SubstituteId.HasValue)
                            {
                                if (model.SubstituteId.Value == model.StaffId) throw new ServiceException("Nhân sự thay thế trùng nhân sự đã có trong đợt");
                                if (staffExist == null) throw new ServiceException("Nhân sự chưa tham gia kế hoạch đợt này");

                                staffExist.ReplaceDate = DateTime.UtcNow;
                                staffExist.ReplaceUserId = model.SubstituteId;
                                result = phaseStaffRepository.Update(staffExist);
                            }
                            else
                            {

                                if (staffExist != null) throw new ServiceException("Nhân sự đã tham gia kế hoạch đợt này");
                                result = phaseStaffRepository.Insert(new PhaseStaff
                                {
                                    PhaseId = model.PhaseId,
                                    StaffId = model.StaffId,
                                    Position = PCMAssignPotition.Member,
                                    StartTime = DateTime.UtcNow,
                                    EndTime = phaseExists.FirstOrDefault(c => c.Id == model.PhaseId).EndDate,
                                    ReplaceDate = DateTime.UtcNow,
                                    Status = PhaseStaffStatus.StillWork,
                                    IsOriginal = false
                                });
                            }

                            if (!result)
                            {
                                trans.Rollback();
                                break;
                            }
                            else
                            {
                                var doneDepartmentLists = JsonConvert.DeserializeObject<List<int>>(phaseExist.DoneStaffsDepartments);
                                var doneDepartmentListUpdates = doneDepartmentLists.Where(c => c != department);
                                phaseExist.DoneStaffsDepartments = JsonConvert.SerializeObject(doneDepartmentListUpdates);
                                phaseExist.Status = PhaseStatus.Passed;
                                result = phaseRepository.Update(phaseExist);
                            }
                            if (!result)
                            {
                                trans.Rollback();
                                break;
                            }

                        }
                    }
                    catch (Exception ex)
                    {
                        trans.Rollback();
                        throw ex;
                    }
                    trans.Commit();
                }
            }
            return result;

        }



        #endregion

        #region Records

        public bool SaveRecord(RecordSaveModel model)
        {
            if (model.PhaseId == 0) throw new ServiceException("Vui lòng chọn đợt");
            if (model.FileNames == null || model.FileNames.Count == 0) throw new ServiceException("Vui lòng chọn file biên bản đính kèm");
            var entity = new ExperimentRecord();
            if (model.Id.HasValue && model.Id.Value > 0)
            {
                entity = recordRepository.GetById(model.Id.Value);
            }
            entity.PhaseId = model.PhaseId;
            if (model.FileNames != null && model.FileNames.Count > 0)
                entity.Files = JsonConvert.SerializeObject(model.FileNames);
            entity.Content = model.Content;
            entity.Note = model.Note;

            if (model.Id > 0) return recordRepository.Update(entity);
            else
            {
                var user = userRepository.GetById(model.UserId);
                entity.DepartmentId = user.DepartmentId.Value;
                entity.Status = RecordStatus.Draft;
                entity.CreatedOn = DateTime.Now;
                entity.CreatedBy = model.UserId;
                return recordRepository.Insert(entity);
            }
        }

        public RecordFilterResult FilterRecords(RecordFilterModel model)
        {
            var result = new RecordFilterResult();
            var phases = phaseRepository.Fetch(p => p.ContractId == model.ContractId);
            var phaseIds = phases.Select(p => p.Id);
            var q = recordRepository.Fetch(e => phaseIds.Contains(e.PhaseId));

            result.TotalRecord = q.Count();
            var entities = q.OrderByDescending(c => c.Id).Skip((model.PageIndex - 1) * model.PageSize).Take(model.PageSize).ToList();

            var userIds = entities.Select(u => u.CreatedBy);

            var users = userRepository.Fetch(u => userIds.Contains(u.Id));
            result.Records = entities.Map<List<RecordModel>>();

            foreach (var record in result.Records)
            {
                var user = users.FirstOrDefault(c => c.Id == record.CreatedBy);
                if (user != null)
                {
                    record.UserName = user.FullName;
                    record.DepartmentName = ((Department)user.DepartmentId.Value).ToDescription();
                }
                if (!string.IsNullOrEmpty(record.Files))
                {
                    record.FileList = JsonConvert.DeserializeObject<List<string>>(record.Files);
                }

                var phase = phases.FirstOrDefault(p => p.Id == record.PhaseId);
                if (phase != null) record.PhaseNo = phase.No;
            }
            return result;
        }

        public RecordModel GetRecord(int recordId)
        {
            var entity = recordRepository.GetById(recordId);

            var model = entity.Map<RecordModel>();
            if (!string.IsNullOrEmpty(model.Files))
            {
                model.FileList = JsonConvert.DeserializeObject<List<string>>(model.Files);
            }
            return model;
        }

        public bool SubmitRecord(int recordId)
        {
            var entity = recordRepository.GetById(recordId);
            entity.Status = RecordStatus.Submited;

            var user = userRepository.GetById(entity.CreatedBy);
            var contractId = phaseRepository.GetById(entity.PhaseId).ContractId;
            var contract = contractRepository.GetById(contractId);

            if (recordRepository.Update(entity))
            {
                var noPhase = phaseRepository.GetById(entity.PhaseId).No;
                string messenger = string.Format("{0} trình duyệt biên bản ĐỢT {1} HĐ {2} chờ duyệt", user.FullName, noPhase, contract.Code);

                var receivers = new List<int>() { entity.CreatedBy };
                // Trưởng phòng CM
                var director = userService.GetDirector((Department)user.DepartmentId.Value);
                receivers.Add(director.Id);

                notificationService.Notify(receivers, messenger, NotificationType.SubmitRecord, recordId);
            }
            return true;
        }

        public bool RejectRecord(int recordId)
        {
            var entity = recordRepository.GetById(recordId);
            if (entity.Status == RecordStatus.Draft) throw new ServiceException("Biên bản nháp không thể trả lại");
            if (entity.Status == RecordStatus.Approved) throw new ServiceException("Biên bản đã được duyệt không thể từ chối");
            entity.Status = RecordStatus.Rejected;

            var user = userRepository.GetById(entity.CreatedBy);
            var contractId = phaseRepository.GetById(entity.PhaseId).ContractId;
            var contract = contractRepository.GetById(contractId);

            if (recordRepository.Update(entity))
            {
                var noPhase = phaseRepository.GetById(entity.PhaseId).No;
                string messenger = string.Format("Biên bản ĐỢT {0} HĐ {1} bị trả lại", noPhase, contract.Code);

                var receivers = new List<int>() { entity.CreatedBy };
                // Trưởng phòng CM
                var director = userService.GetDirector((Department)user.DepartmentId.Value);
                receivers.Add(director.Id);

                notificationService.Notify(receivers, messenger, NotificationType.RejectRecord, recordId);
            }
            return true;
        }

        public bool ApproveRecord(int recordId)
        {
            var entity = recordRepository.GetById(recordId);
            if (entity.Status == RecordStatus.Rejected) throw new ServiceException("Biên bản chưa được duyệt");
            if (entity.Status == RecordStatus.Draft) throw new ServiceException("Biên bản nháp không thể duyệt");
            entity.Status = RecordStatus.Approved;

            var user = userRepository.GetById(entity.CreatedBy);
            var contractId = phaseRepository.GetById(entity.PhaseId).ContractId;
            var contract = contractRepository.GetById(contractId);

            if (recordRepository.Update(entity))
            {
                var noPhase = phaseRepository.GetById(entity.PhaseId).No;
                string messenger = string.Format("Biên bản ĐỢT {0} HĐ {1} đã được duyệt", noPhase, contract.Code);

                var receivers = new List<int>() { entity.CreatedBy };
                // Trưởng phòng CM
                var director = userService.GetDirector((Department)user.DepartmentId.Value);
                receivers.Add(director.Id);

                //Trưởng phòng kt
                var techDirector = userService.GetDirector(Department.KT);
                receivers.Add(techDirector.Id);

                notificationService.Notify(receivers, messenger, NotificationType.ApproveRecord, recordId);
            }
            return true;
        }
        #endregion

        #region Misc
        public bool DeleteContractGroup(int id)
        {
            var model = contractGroupRepository.GetById(id);
            var group = model.Map<ContractGroup>();
            var res = contractGroupRepository.Delete(group);
            return res;
        }

        public ContractGroupModel SaveContractGroup(ContractGroupModel model)
        {
            if (model.Name == null) throw new ServiceException("Chưa nhập tên!");
            var contractGroup = model.Map<ContractGroup>();
            if (model.Id != 0) contractGroupRepository.Update(contractGroup);
            else contractGroupRepository.Insert(contractGroup);
            return contractGroup.Map<ContractGroupModel>();
        }

        public bool RemoveCenter(int contractId, int centerId)
        {
            return masterPlanRepository.DeleteMany(m => m.ContractId == contractId && m.DepartmentId == centerId) > 0;
        }

        public bool AddVehicleRental(VehicleModel vehicle, int phaseId)
        {
            var result = true;
            using (var db = vehicleRepository.GetDbContext())
            {
                using (var trans = db.Database.BeginTransaction())
                {
                    try
                    {
                        var phase = phaseRepository.GetById(phaseId);
                        if (phase == null) throw new ServiceException("Kế hoạch theo đợt không tồn tại");
                        var add = new Vehicle
                        {
                            Number = vehicle.Number,
                            Seat = vehicle.Seat,
                            DriverName = vehicle.DriverName,
                            IsCarRental = true
                        };
                        result = vehicleRepository.Insert(add);
                        if (result)
                        {
                            phaseVehileRepository.Insert(new PhaseVehicle
                            {
                                PhaseId = phaseId,
                                VehicleId = add.Id
                            });
                        }
                        if (!result) trans.Rollback();
                    }
                    catch (Exception ex)
                    {
                        trans.Rollback();
                        throw ex;
                    }
                    trans.Commit();
                }
            }
            return result;
        }

        public bool UpdateWorkitem(UpdateWorkitemModel model, Guid uid)
        {
            var result = false;
            var workitem = workitemRepository.GetById(model.Id.Value);
            if (workitem != null) throw new ServiceException("Hạng mục không tồn tại");

            if (!string.IsNullOrEmpty(model.Title))
                workitem.Title = model.Title;
            if (!string.IsNullOrEmpty(model.Unit))
                workitem.Title = model.Unit;
            if (model.Quantity.HasValue)
            {
                var reportitems = reportItemRepository.Fetch(c => c.WorkitemId == model.Id);
                if (reportitems.Count() > 0)
                {
                    var totalCompleted = reportitems.Sum(c => c.Quantity);
                    if (totalCompleted >= model.Quantity)
                        throw new ServiceException("Số lượng cập nhật không được nhỏ hơn số lượng hoàn thành trong báo cáo");
                }
                workitem.Quantity = model.Quantity;
            }
            if (model.Price.HasValue)
            {
                workitem.Price = model.Price;
            }

            if (workitem.Quantity.HasValue && workitem.Price.HasValue)
                workitem.Money = workitem.Quantity.Value * workitem.Price.Value;

            result = workitemRepository.Update(workitem);
            return false;
        }

        public List<PhaseEquipmentAllocatorModel> GetEquipmentAllocator(int phaseId)
        {
            var result = new List<PhaseEquipmentAllocatorModel>();
            var entities = phaseEquipmentAllocatorRepository.Fetch(c => c.PhaseId == phaseId);
            if (entities.Count() > 0)
            {
                var userCheckIds = entities.Select(c => c.UserId);
                var users = userRepository.Fetch(c => userCheckIds.Contains(c.Id));
                result = entities.Map<List<PhaseEquipmentAllocatorModel>>();

                foreach (var r in result)
                {
                    r.UserName = users.FirstOrDefault(c => c.Id == r.UserId).FullName;
                }
            }
            return result;
        }

        public bool UpdateEquipmentAllocator(int phaseId, int userId, int departmentId)
        {
            var result = false;
            var exist = phaseEquipmentAllocatorRepository.FirstOrDefault(c => c.PhaseId == phaseId && c.DepartmentId == ((Department)departmentId));
            if (exist != null)
            {
                if (exist.UserId == userId)
                    throw new ServiceException("Nhân sự này đã tham gia phân thiết bị cho phòng chuyên môn");
                else
                {
                    exist.UserId = userId;
                    result = phaseEquipmentAllocatorRepository.Update(exist);
                }
            }
            else result = phaseEquipmentAllocatorRepository.Insert(new PhaseEquipmentAllocators
            {
                PhaseId = phaseId,
                UserId = userId,
                DepartmentId = (Department)departmentId
            });
            return result;
        }




        #endregion

    }
}
