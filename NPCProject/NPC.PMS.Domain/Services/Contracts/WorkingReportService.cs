﻿using System;
using System.Collections.Generic;
using System.Linq;
using NPC.PMS.Domain.Data;
using NPC.PMS.Domain.Data.Entity;
using NPC.PMS.Domain.Enums;
using NPC.PMS.Domain.Models.ContractB;
using NPC.PMS.Domain.Models.Reports;
using NPC.PMS.Domain.Services.Users;
using NPC.PMS.Domain.Models.Reports.Mobile;
using Newtonsoft.Json;
using NPC.PMS.Framework.Extensions.Enums;
using NPC.PMS.Domain.Services.Common.Notifications;
using NPC.PMS.Domain.Services.Contracts;

namespace NPC.PMS.Domain.Services.Reports
{
    public class WorkingReportService : IWorkingReportService
    {
        private readonly IRepository<Contract> contractRepository;
        IRepository<Report> reportRepository;
        private readonly IRepository<Reportitem> reportitemRepository;
        private readonly IRepository<Phase> phaseRepository;
        private readonly IRepository<PhaseStaff> phaseStaffRepository;
        private readonly IUserService userService;
        private readonly IContractBService contractService;
        private readonly IRepository<Quote> quoteRepository;
        private readonly INotificationService notificationService;
        private readonly IRepository<PhaseWorkitem> phaseWorkitemRepository;
        private readonly IRepository<Workitem> workitemRepository;
        private readonly IRepository<User> userRepository;

        public WorkingReportService(IRepository<Contract> contractRepository,
            IRepository<Report> phaseReportRepository,
            IRepository<Reportitem> phaseReportItemRepository,
            IRepository<Phase> phaseRepository,
            IRepository<PhaseStaff> phaseStaffRepository,
            IUserService userService,
            IContractBService contractService,
            INotificationService notificationService,
            IRepository<PhaseWorkitem> phaseWorkitemRepository,
            IRepository<Workitem> WorkitemRepository,
             IRepository<Quote> quoteRepository,
            IRepository<User> UserRepository)
        {
            this.contractRepository = contractRepository;
            this.reportRepository = phaseReportRepository;
            this.reportitemRepository = phaseReportItemRepository;
            this.phaseRepository = phaseRepository;
            this.phaseStaffRepository = phaseStaffRepository;
            this.userService = userService;
            this.contractService = contractService;
            this.notificationService = notificationService;
            this.phaseWorkitemRepository = phaseWorkitemRepository;
            workitemRepository = WorkitemRepository;
            userRepository = UserRepository;
            this.quoteRepository = quoteRepository;
        }

        public ReportModel Save(ReportModel model)
        {
            var report = new Report();
           
            if (model.Id != 0)
            {
                report = reportRepository.GetById(model.Id);
                if (report.Status == ReportStatus.Approved) throw new ServiceException("Không thể sửa báo cáo đã được hoàn thành!");
            }
            else
            {
                var user = userRepository.GetById(model.UserId);
                report.Time = DateTime.Now;
                report.UserId = model.UserId;
                report.PhaseId = model.PhaseId;
                report.UnitId = user.DepartmentId;
            }

            report.Status = ReportStatus.Submited;
            report.Content = model.Content;

            if(model.Id > 0)
            {
                reportRepository.Update(report);
                foreach(var reportitemModel in model.Reportitems)
                {
                    var reportitem = reportitemRepository.FirstOrDefault(ri => ri.ReportId == report.Id && ri.WorkitemId == reportitemModel.WorkitemId);
                    if(reportitem != null)
                    {
                        reportitem.Quantity = reportitemModel.Quantity;
                        reportitemRepository.Update(reportitem);
                    }
                    else
                    {
                        reportitem = new Reportitem
                        {
                            ReportId = report.Id,
                            WorkitemId = reportitemModel.WorkitemId,
                            Quantity = reportitemModel.Quantity
                        };
                        reportitemRepository.Insert(reportitem);
                    }
                }
            }
            else
            {
                reportRepository.Insert(report);
                var reportItems = model.Reportitems.Map<List<Reportitem>>();
                reportItems.ForEach(c =>
                {
                    c.ReportId = report.Id;
                });

                reportitemRepository.InsertMany(reportItems);
            }


            // notification
            var receivers = new List<int>();
            // tp pcm
            var phaseModel = contractService.GetPhase(report.PhaseId);
            if (phaseModel.DepartmentList.Count > 0)
            {
                var departmentDirectors = userRepository.Fetch(u => u.Position == StaffPosition.Director && phaseModel.DepartmentList.Contains(u.DepartmentId.Value));
                departmentDirectors.ForEach(d => receivers.Add(d.Id));
            }
            // tp p4
            //var techDirector = userRepository.FirstOrDefault(u => u.DepartmentId == (int)Department.KT);
            //if (techDirector != null) receivers.Add(techDirector.Id);
            var contractCode = string.Empty;
            if (phaseModel.ProcessType == ProcessType.QT01)
            {
                var contract = contractRepository.GetById(phaseModel.ContractId);
                if (contract!=null)
                {
                    contractCode = contract.Code;
                }
                var reporter = userRepository.GetById(model.UserId);
                var department = (Department)reporter.DepartmentId;
                var notificationMessage = string.Format("{0} gửi báo cáo P.{1} ĐỢT.{2} HĐ.{3}", reporter.FullName, department.ToDescription(), phaseModel.No, contractCode);
                notificationService.Notify(receivers, notificationMessage, NotificationType.NewReport, report.Id);
            }
            else
            {
                var contract = quoteRepository.GetById(phaseModel.ContractId);
                if (contract != null)
                {
                    contractCode = contract.Code;
                }
                var reporter = userRepository.GetById(model.UserId);
                var department = (Department)reporter.DepartmentId;
                var notificationMessage = string.Format("{0} gửi báo cáo P.{1} ĐỢT.{2} HĐ.{3}", reporter.FullName, department.ToDescription(), phaseModel.No, contractCode);
                notificationService.Notify(receivers, notificationMessage, NotificationType.TBLNewReport, report.Id);
            }
           

          

            return report.Map<ReportModel>();
        }

        public ReportFilterResult Filter(ReportFilterModel filter)
        {
            using (var db = reportRepository.GetDbContext())
            {
                var q = db.Reports.AsQueryable();
                if (filter.PhaseId > 0)
                {
                    q = q.Where(t => t.PhaseId == filter.PhaseId);
                }
                if (filter.ContractId > 0)
                {
                    var phaseIds = phaseRepository.Fetch(t => t.ContractId == filter.ContractId).Select(c => c.Id);
                    q = q.Where(t => phaseIds.Contains(t.PhaseId));
                }
                if (filter.Status > 0)
                {
                    q = q.Where(t => t.Status == filter.Status);
                }
                if (filter.DepartmentId > 0)
                {
                    var uids = userRepository.Fetch(t => t.DepartmentId == filter.DepartmentId).Select(c => c.Id);
                    q = q.Where(t => uids.Contains(t.UserId));
                }
                if (filter.Started != null || filter.End != null)
                {
                    q = q.Where(t => DateTime.Compare(filter.Started.Value, t.Time) < 0 && DateTime.Compare(t.Time, filter.End.Value) < 0);
                }
                var total = q.Count();
                var entities = q.OrderByDescending(c => c.Time).Skip((filter.PageIndex - 1) * filter.PageSize).Take(filter.PageSize).ToList();
                var models = entities.Map<List<ReportInfoModel>>();

                var userIds = models.Select(m => m.UserId);
                var users = userRepository.Fetch(u => userIds.Contains(u.Id));

                var phaseids = models.Select(t => t.PhaseId);

                var phases = phaseRepository.Fetch(p => phaseids.Contains(p.Id));

                var contractIds = phases.Select(p => p.ContractId);

                var contracts = contractRepository.Fetch(c => contractIds.Contains(c.Id));

                var phaseworkitemss = phaseWorkitemRepository.Fetch(t => phaseids.Contains(t.PhaseId));
                var reportIds = models.Where(t=>phaseids.Contains(t.PhaseId)).Select(t => t.Id);
                var reportItemss = reportitemRepository.Fetch(t => reportIds.Contains(t.ReportId));
                foreach (var model in models)
                {
                    var phase = phases.FirstOrDefault(u => u.Id == model.PhaseId);
                    if (phase != null)
                    {
                        model.PhaseNo = phase.No;
                        var contract = contracts.FirstOrDefault(u => u.Id == phase.ContractId);
                        model.ContractCode = contract != null ? contract.Code : string.Empty;
                    }                   

                    var user = users.FirstOrDefault(u=>u.Id == model.UserId);
                    model.UserName = user != null ? user.FullName : string.Empty;

                    //var departmentId = (Department)users.FirstOrDefault(t => t.Id == model.UserId).DepartmentId;
                    var departmentId = (Department)user.DepartmentId;
                    model.Department = departmentId.ToDescription();

                    var reportItems = reportItemss.FindAll(t=>t.ReportId == model.Id);
                    model.ReportQuantity = reportItems.Sum(c => c.Quantity);

                    model.StatusName = model.Status.ToDescription();

                    var phaseworkitems = phaseworkitemss.FindAll(t => t.PhaseId == model.PhaseId && t.DepartmentId == user.DepartmentId);
                    var totalPhaseQuantity = phaseworkitems.Sum(t => t.Quantity);
                    model.TotalAssignedQuantity = totalPhaseQuantity;
                    if (totalPhaseQuantity != 0)
                        model.Percent = Convert.ToInt32(((model.ReportQuantity / totalPhaseQuantity) * 100)).ToString();
                }
                //models = models.Where(t => t.ReportQuantity != 0).ToList();
                return new ReportFilterResult
                {
                    Filter = filter,
                    TotalRecord = total,
                    Records = models
                };
            }
        }

        public WorkingModel GetWorkingInfo(int staffId, int phaseId)
        {
            var user = userService.GetById(staffId);
            var phase = phaseRepository.GetById(phaseId);

            var phaseStaffs = phaseStaffRepository.Fetch(p => p.PhaseId == phaseId && p.StaffId == staffId);
            if (!phaseStaffs.Any()) throw new ServiceException("Bạn không tham gia đợt này");

            phaseStaffs = phaseStaffs.Where(p => p.Position == PCMAssignPotition.Leader || p.PositionInPhase == PhaseStaffPosition.Captain).ToList();
            if(!phaseStaffs.Any()) throw new ServiceException("Chỉ cho phép trưởng nhóm lập báo cáo");

            var phaseWorkitems = phaseWorkitemRepository.Fetch(t => t.PhaseId == phaseId && t.DepartmentId == (int)user.DepartmentId);
            var workModel = new WorkingModel();

            var workItemIds = phaseWorkitems.Select(t => t.WorkitemId).ToList();
            var workItems = workitemRepository.Fetch(t => workItemIds.Contains(t.Id)).ToList();

            workModel.Items = new List<WorkingItem>();
            foreach(var phaseWorkitem in phaseWorkitems)
            {
                var workitem = workItems.FirstOrDefault(w => w.Id == phaseWorkitem.WorkitemId);
                if (workitem == null) continue;
                workModel.Items.Add(new WorkingItem
                {
                    WorkitemId = phaseWorkitem.WorkitemId,
                    Title = workitem.Title,
                    Quantity = phaseWorkitem.Quantity,
                    Completed = phaseWorkitem.CompletedQuantity.HasValue ? phaseWorkitem.CompletedQuantity.Value : 0
                });
            }
            
            workModel.PhaseId = phaseId;
            var contractId = phase.ContractId;
            workModel.ContractId = contractId;
            if (phase.ProcessType==ProcessType.QT01)
            {
                workModel.ContractNumber = contractRepository.GetById(contractId).Code;
            }
            else
            {
                workModel.ContractNumber = quoteRepository.GetById(contractId).Code;
            }
          
            workModel.PhaseLabel = phase.No.ToString();
            workModel.Items = workModel.Items.Where(t => t.Quantity != 0).ToList();
            return workModel;
        }
        public List<PhaseModel> GetUserPhases(int userId)
        {
            var phaseStaff = phaseStaffRepository.Fetch(ps => ps.StaffId == userId);
            var phaseIds = phaseStaff.Select(ps => ps.PhaseId);
            var phases = phaseRepository.Fetch(p => phaseIds.Contains(p.Id));

            var contractIds = phases.Select(p => p.ContractId);
            var contracts = contractRepository.Fetch(c => contractIds.Contains(c.Id));

            var phaseModels = phases.Map<List<PhaseModel>>();
            foreach (var phaseModel in phaseModels)
            {
                var contract = contracts.FirstOrDefault(c => c.Id == phaseModel.ContractId);
                if (contract != null)
                {
                    phaseModel.ContractNumber = contract.Code;
                }
            }

            return phaseModels;
        }

        public ReportModel Get(int id)
        {
            var report = reportRepository.GetById(id);
            var user = userRepository.GetById(report.UserId);
            var reportItems = reportitemRepository.Fetch(t => t.ReportId == id);
            var model = new ReportModel()
            {
                Id = report.Id,
                UserId = report.UserId,
                PhaseId = report.PhaseId,
                phaseName = phaseRepository.GetById(report.PhaseId).Title,
                Time = report.Time,
                Status = report.Status,
                StatusName = report.Status.ToDescription(),
                Content = report.Content,
                Logs = report.Logs,
                StaffName = user != null ? user.FullName : string.Empty,
                DepartmentId = report.UnitId

            };
            
            model.Reportitems = reportItems.Map<List<ReportitemModel>>();
            var workitemIds = model.Reportitems.Select(pr => pr.WorkitemId);
            var workitems = workitemRepository.Fetch(w => workitemIds.Contains(w.Id));
            //var departmentId = userRepository.GetById(userId).DepartmentId;
            var phaseWorkitems = phaseWorkitemRepository.Fetch(t => t.PhaseId == model.PhaseId && t.DepartmentId == user.DepartmentId);
            foreach(var item in model.Reportitems)
            {
                var workitem = workitems.FirstOrDefault(w => w.Id == item.WorkitemId);
                if (workitem != null) item.WorkitemTitle = workitem.Title;
                var phaseWorkitem = phaseWorkitems.FirstOrDefault(pw => pw.WorkitemId == item.WorkitemId);
                item.AssignedQuantity = phaseWorkitem != null ? phaseWorkitem.Quantity : 0;
            }

            return model;
        }

        public bool Approve(int id)
        {
            var report = reportRepository.GetById(id);
            var user = userRepository.GetById(report.UserId);
            if (report.Status == ReportStatus.Rejected) throw new ServiceException("Trạng thái từ chối không thể duyệt!");
            if (report.Status == ReportStatus.Approved) return true;
            report.Status = ReportStatus.Approved;
            reportRepository.Update(report);

            var reportItems = reportitemRepository.Fetch(r => r.ReportId == id);
            foreach (var item in reportItems)
            {
                var phaseWorkitem = phaseWorkitemRepository.FirstOrDefault(p => p.DepartmentId == user.DepartmentId && p.PhaseId == report.PhaseId && p.WorkitemId == item.WorkitemId);
                if (phaseWorkitem == null) continue;
                phaseWorkitem.CompletedQuantity = (phaseWorkitem.CompletedQuantity.HasValue ? phaseWorkitem.CompletedQuantity.Value : 0) + item.Quantity;
                //if (item.Quantity > phaseWorkitem.Quantity) phaseWorkitem.Quantity = 0;
                //else phaseWorkitem.Quantity = phaseWorkitem.Quantity - item.Quantity;
                phaseWorkitemRepository.Update(phaseWorkitem);
            }
            //Push notify
            var reportMess = string.Format("Báo cáo ngày {0} đã được duyệt bởi {1}", report.Time, user.FullName);
            var context = reportRepository.GetDbContext();
            var phase = (from p in context.Phases
                         join r in context.Reports on p.Id equals r.PhaseId
                         where r.Id == id
                         select p
                    ).FirstOrDefault();
            if (phase!=null && phase.ProcessType==ProcessType.QT01)
            {
                notificationService.Notify(user.Id, reportMess,NotificationType.ApproveReport,id);
            }
            else if (phase != null)
            {
                notificationService.Notify(user.Id, reportMess, NotificationType.TBLApproveReport, id);
            }
           
          
            return true;
        }
        public bool Reject(int id, string log)
        {
            var report = reportRepository.GetById(id);
            var user = userRepository.GetById(report.UserId);
            if (report.Status == ReportStatus.Rejected) return true;
            report.Status = ReportStatus.Rejected;
            var logs = new List<string>();
            if (!string.IsNullOrEmpty(report.Logs))
            {
                var oldLogs = JsonConvert.DeserializeObject<List<string>>(report.Logs);
                logs.AddRange(oldLogs);
            }
            logs.Add(log);
            report.Logs = JsonConvert.SerializeObject(logs);
            var res = reportRepository.Update(report);
            if (res)
            {
                var context = reportRepository.GetDbContext();
                var phase = (from p in context.Phases
                             join r in context.Reports on p.Id equals r.PhaseId
                             where r.Id == id
                             select p
                        ).FirstOrDefault();
                var reportMess = string.Format("Báo cáo ngày {0} đã bị từ chối bởi {1} với lí do: {2}", report.Time, user.FullName, log);
                if (phase != null && phase.ProcessType == ProcessType.QT01)
                {
                    notificationService.Notify(user.Id, reportMess, NotificationType.RejectReport, id);
                }
                else if(phase!=null)
                {
                    notificationService.Notify(user.Id, reportMess, NotificationType.TBLRejectReport, id);
                }


                
              //  notificationService.Notify(user.Id, reportMess);
            }
            return res;
        }

        public List<ReportModel> MobileFilter(int userId, ReportFilterMobileModel filter)
        {
            using (var db = reportitemRepository.GetDbContext())
            {
                var q = db.Reports.AsQueryable().Where(t => t.UserId == userId);
                if (filter.Status > 0) q = q.Where(t => (int)t.Status == filter.Status);
                if (filter.Started != null || filter.End != null)
                {
                    q = q.Where(t => DateTime.Compare(filter.Started.Value, t.Time) < 0 && DateTime.Compare(t.Time, filter.End.Value) < 0);
                }

                if (filter.PhaseId.HasValue)
                {
                    q = q.Where(r => r.PhaseId == filter.PhaseId.Value);
                }
                var entities = q.OrderByDescending(c => c.Time).Skip((filter.PageIndex - 1) * filter.PageSize).Take(filter.PageSize).ToList();

                var models = entities.Map<List<ReportModel>>();

                var reportIds = models.Select(w => w.Id);
                var items = db.Reportitems.Where(p => reportIds.Contains(p.ReportId));
                var itemModels = items.Map<List<ReportitemModel>>();

                var workitemIds = items.Select(i => i.WorkitemId);
                var workitems = db.Workitems.Where(w => workitemIds.Contains(w.Id));
                var workitemModels = workitems.Map<List<WorkitemModel>>();

                foreach(var itemModel in itemModels)
                {
                    var workitem = workitemModels.FirstOrDefault(w => w.Id == itemModel.WorkitemId);
                    if (workitem == null) continue;
                    itemModel.WorkitemTitle = workitem.Title;
                }

                foreach(var model in models)
                {
                    model.Reportitems = itemModels.Where(i => i.ReportId == model.Id).ToList();
                }

                return models;
            }
        }
    }
}
