﻿using Microsoft.EntityFrameworkCore.Internal;
using Newtonsoft.Json;
using NPC.PMS.Domain.Data;
using NPC.PMS.Domain.Data.Entity;
using NPC.PMS.Domain.Enums;
using NPC.PMS.Domain.Models.ContractA;
using NPC.PMS.Domain.Models.ContractB;
using NPC.PMS.Domain.Models.Documents;
using NPC.PMS.Domain.Models.Settlements;
using NPC.PMS.Domain.Services.Common.Documents;
using NPC.PMS.Domain.Services.Common.Notifications;
using NPC.PMS.Domain.Services.Users;
using NPC.PMS.Framework.Extensions.Enums;
using NPC.PMS.Framework.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NPC.PMS.Domain.Services.Contracts
{
    public class ContractAService : IContractAService
    {
        private readonly IRepository<Contracta> contractARepository;
        private readonly IRepository<Contract> contractRepository;
        private readonly IRepository<SettlementCheck> settlementCheckRepository;
        private readonly IRepository<Settlement> settlementRepository;
        private readonly IRepository<SettlementDocument> settlementDocumentRepository;
        private readonly IRepository<User> userRepository;
        private readonly IUserService userService;
        private readonly INotificationService notificationService;
        private readonly IRepository<ContractAGroup> contractAGroupRepository;
        private readonly IRepository<ProfileListContractADefined> profileListContractADefinedRepository;
        private readonly IRepository<ProfileListContractA> profileListContractARepository;
        private readonly IRepository<ContractAUser> contractAUserRepository;
        private readonly IRepository<Document> documentRepository;
        private readonly IDocumentService documentService;
        const string ASSIGN_CONTRACTA_USER = "{0} {1} được chỉ định vào hợp đồng {2}";


        public ContractAService(IRepository<Contracta> contractARepository,
            IRepository<Contract> contractRepository,
            IRepository<SettlementCheck> settlementCheckRepository,
            IRepository<Settlement> settlementRepository,
            IRepository<SettlementDocument> settlementDocumentRepository,
            IRepository<User> userRepository,
            IUserService userService,
            INotificationService notificationService,
            IRepository<ContractAGroup> contractAGroupRepository,
            IRepository<ProfileListContractADefined> profileListContractADefinedRepository,
            IRepository<ProfileListContractA> profileListContractARepository,
            IRepository<ContractAUser> contractAUserRepository,
            IRepository<Document> documentRepository,
            IDocumentService documentService
        )
        {
            this.contractARepository = contractARepository;
            this.contractRepository = contractRepository;
            this.settlementCheckRepository = settlementCheckRepository;
            this.settlementRepository = settlementRepository;
            this.settlementDocumentRepository = settlementDocumentRepository;
            this.userRepository = userRepository;
            this.userService = userService;
            this.notificationService = notificationService;
            this.contractAGroupRepository = contractAGroupRepository;
            this.profileListContractADefinedRepository = profileListContractADefinedRepository;
            this.profileListContractARepository = profileListContractARepository;
            this.contractAUserRepository = contractAUserRepository;
            this.documentRepository = documentRepository;
            this.documentService = documentService;
        }
        public List<ContractaModel> Filter(ContractaFilterModel filter)
        {
            using (var db = contractARepository.GetDbContext())
            {
                var q = db.Contractas.AsQueryable();
                if (!string.IsNullOrEmpty(filter.ContractCode)) q = q.Where(t => t.Code.Contains(filter.ContractCode));
                if (!string.IsNullOrEmpty(filter.Content)) q = q.Where(t => t.Contents.Contains(filter.Content));
                if (!string.IsNullOrEmpty(filter.Supplier)) q = q.Where(t => t.Supplier.Contains(filter.Supplier));
                if (filter.Status.HasValue) q = q.Where(t => t.Status == filter.Status);
                if (filter.RefContractId.HasValue)
                {
                    q = q.Where(t => t.RefContractId == filter.RefContractId.Value);
                }

                var except = q.Where(c => c.CreatedBy != filter.UserId && c.Status == ContractaStatus.Draft);
                if (except.Count()>0)
                {
                    q = q.Except(except);
                }
               


                //else q = q.Where(t => !t.RefContractId.HasValue);
                var entities = q.OrderByDescending(c => c.Id).ToList();

                var userIds = entities.Select(c => c.CreatedBy);
                var users = userRepository.Fetch(u => userIds.Contains(u.Id));

                var models = entities.CloneToListModels<Contracta, ContractaModel>();
                foreach (var model in models)
                {
                    var creator = users.FirstOrDefault(u => u.Id == model.CreatedBy);
                    if (creator != null)
                    {
                        model.CreateByName = creator.FullName;
                    }
                    if (model.FormalityCode.HasValue)
                        model.FormalitySelectName = model.FormalityCode.ToDescription();
                    model.StatusName = model.Status.ToDescription();

                    if (model.DateSign.HasValue)
                    {
                        model.DateSign = DateTime.SpecifyKind(model.DateSign.Value, DateTimeKind.Utc);
                    }
                }


                var userCurrent = userRepository.GetById(filter.UserId);
                var p2Director = userService.GetDirector(Department.KHDT);
                var p5Director = userService.GetDirector(Department.TCKT);
                var result = new List<ContractaModel>();

                var userHandlers = new List<ContractAUser>();
                var userCreatedContracts = new List<int>();
                var contractIds = models.Select(c => c.Id);
                if (contractIds.Count() > 0)
                {
                    userHandlers = contractAUserRepository.Fetch(c => contractIds.Contains(c.ContractId));
                    userCreatedContracts.AddRange(models.Select(c => c.CreatedBy));
                }


                foreach (var item in models)
                {

                    if (userCurrent.Id == p2Director.Id && item.Status != ContractaStatus.SumitToP5Director
                        && item.Status != ContractaStatus.RejectFromP5Director)
                    {
                        if (item.Status == ContractaStatus.P5SendP2)
                        {
                            item.StatusName = "Đã tiếp nhận";
                        }
                        result.Add(item);
                    }
                    if (userCurrent.Id == p5Director.Id && item.Status != ContractaStatus.Draft && item.Status != ContractaStatus.SumitToP2Director
                        && item.Status != ContractaStatus.RejectFromP2Director)
                    {
                        if (item.Status == ContractaStatus.P2SendP5)
                        {
                            item.StatusName = "Đã tiếp nhận";
                        }

                        result.Add(item);
                    }
                    var userCreate = userCreatedContracts.FirstOrDefault(c => c == item.CreatedBy);
                    if (userHandlers.Count() + userCreatedContracts.Count() > 0)
                    {
                        var handlerInContractAs = userHandlers.Where(c => c.ContractId == item.Id);

                        if (handlerInContractAs.Count() + userCreate > 0)
                        {

                            var check = handlerInContractAs.Select(c => c.UserId).ToList();
                            check.Add(userCreate);
                            if (check.Contains(userCurrent.Id))
                            {
                                result.Add(item);
                            }
                        }

                    }


                }


                result = result.GroupBy(c => c.Id).Select(y => y.First()).ToList();

                return result;
            }
        }

        public ContractAFilterResult FilterContractA(ContractaFilterModel filter)
        {
            using (var db = contractARepository.GetDbContext())
            {
                var result = new ContractAFilterResult() { Filter = filter };

                var q = db.Contractas.AsQueryable();
                q = q.Where(c => c.RefContractId == null);
                if (!string.IsNullOrEmpty(filter.ContractCode)) q = q.Where(t => t.Code.Contains(filter.ContractCode));
                if (!string.IsNullOrEmpty(filter.Content)) q = q.Where(t => t.Contents.Contains(filter.Content));
                if (!string.IsNullOrEmpty(filter.Supplier)) q = q.Where(t => t.Supplier.Contains(filter.Supplier));
                if (filter.Status.HasValue) q = q.Where(t => t.Status == filter.Status);
                if (filter.RefContractId.HasValue)
                {
                    q = q.Where(t => t.RefContractId == filter.RefContractId.Value);
                }

                var except = q.Where(c => c.CreatedBy != filter.UserId && c.Status == ContractaStatus.Draft);
                if (except.Count() > 0)
                {
                    q = q.Except(except);
                }



                //else q = q.Where(t => !t.RefContractId.HasValue);
                result.TotalRecord = q.Count();
                
                var entities = q.OrderByDescending(c => c.Id).Skip((filter.PageIndex - 1) * filter.PageSize).Take(filter.PageSize).ToList();

                var userIds = entities.Select(c => c.CreatedBy);
                var users = userRepository.Fetch(u => userIds.Contains(u.Id));

                var models = entities.CloneToListModels<Contracta, ContractaModel>();
                //get contract groups
                var contractIds = models.Select(c => c.Id).ToList();
                var groups = (from c in db.Contractas
                              join g in db.ContractAGroups on c.GroupId equals g.Id
                              where contractIds.Contains(c.Id)
                              select new
                              {
                                  c.Id,
                                  GroupName = g.Name
                              }).ToList();

                foreach (var model in models)
                {
                    var creator = users.FirstOrDefault(u => u.Id == model.CreatedBy);
                    if (creator != null)
                    {
                        model.CreateByName = creator.FullName;
                    }
                    if (model.FormalityCode.HasValue)
                        model.FormalitySelectName = model.FormalityCode.ToDescription();
                    model.StatusName = model.Status.ToDescription();

                    if (model.DateSign.HasValue)
                    {
                        model.DateSign = DateTime.SpecifyKind(model.DateSign.Value, DateTimeKind.Utc);
                    }

                    var group = groups.FirstOrDefault(c => c.Id == model.Id);
                    if (group!=null)
                    {
                        model.GroupName = group.GroupName;
                    }
                }


                var userCurrent = userRepository.GetById(filter.UserId);
                var p2Director = userService.GetDirector(Department.KHDT);
                var p5Director = userService.GetDirector(Department.TCKT);
                var records  = new List<ContractaModel>();

                var userHandlers = new List<ContractAUser>();
                var userCreatedContracts = new List<int>();
               
                if (contractIds.Count() > 0)
                {
                    userHandlers = contractAUserRepository.Fetch(c => contractIds.Contains(c.ContractId));
                    userCreatedContracts.AddRange(models.Select(c => c.CreatedBy));
                }


                foreach (var item in models)
                {

                    if (userCurrent.Id == p2Director.Id && item.Status != ContractaStatus.SumitToP5Director
                        && item.Status != ContractaStatus.RejectFromP5Director)
                    {
                        if (item.Status == ContractaStatus.P5SendP2)
                        {
                            item.StatusName = "Đã tiếp nhận";
                        }
                        records.Add(item);
                    }
                    if (userCurrent.Id == p5Director.Id && item.Status != ContractaStatus.Draft && item.Status != ContractaStatus.SumitToP2Director
                        && item.Status != ContractaStatus.RejectFromP2Director)
                    {
                        if (item.Status == ContractaStatus.P2SendP5)
                        {
                            item.StatusName = "Đã tiếp nhận";
                        }

                        records.Add(item);
                    }
                    var userCreate = userCreatedContracts.FirstOrDefault(c => c == item.CreatedBy);
                    if (userHandlers.Count() + userCreatedContracts.Count() > 0)
                    {
                        var handlerInContractAs = userHandlers.Where(c => c.ContractId == item.Id);

                        if (handlerInContractAs.Count() + userCreate > 0)
                        {

                            var check = handlerInContractAs.Select(c => c.UserId).ToList();
                            check.Add(userCreate);
                            if (check.Contains(userCurrent.Id))
                            {
                                records.Add(item);
                            }
                        }

                    }
                    
                }


                records = records.GroupBy(c => c.Id).Select(y => y.First()).ToList();
                result.Records = records;
                //tinh toan dư tiền đã thanh toán.
                var uids = result.Records.Select(c => c.UID).ToList();
                var settlements = db.Settlements.Where(c => uids.Contains(c.ContractUID)).Select(c => new { c.Id, c.ContractUID, c.BillValue }).ToList();
                var settlementids = settlements.Select(c => c.Id).ToList();
                List<int?> settIds = new List<int?>();
                foreach (var id in settlementids)
                {
                    settIds.Add(id);

                }

                var payments = db.SettlementPayments.Where(c => settIds.Contains(c.SettlementId)).Select(c => new { c.Id, c.SettlementId, c.PaymentValue }).ToList();
                foreach (var record in result.Records)
                {
                    record.BillAmount = (settlements.Where(c => c.ContractUID == record.UID).Sum(c => c.BillValue) ?? 0) + (record.OpeningBalance ?? 0);
                    var paymentAmount = (from a in settlements.Where(c => c.ContractUID == record.UID)
                                         join b in payments on a.Id equals b.SettlementId
                                         select b.PaymentValue
                                        ).Sum();
                    record.PaymentAmount = paymentAmount;
                }

                return result;
            }
        }

        public ContractAFilterResult FilterContractAB(ContractaFilterModel filter)
        {
            using (var db = contractARepository.GetDbContext())
            {
                var result = new ContractAFilterResult() { Filter = filter };

                var q = (from c in db.Contractas
                         join cb in db.Contracts on c.RefContractId equals cb.Id
                         select c).AsQueryable();
                if (!string.IsNullOrEmpty(filter.ContractCode)) q = q.Where(t => t.Code.Contains(filter.ContractCode));
                if (!string.IsNullOrEmpty(filter.Content)) q = q.Where(t => t.Contents.Contains(filter.Content));
                if (!string.IsNullOrEmpty(filter.Supplier)) q = q.Where(t => t.Supplier.Contains(filter.Supplier));
                if (filter.Status.HasValue) q = q.Where(t => t.Status == filter.Status);
                if (filter.RefContractId.HasValue)
                {
                    q = q.Where(t => t.RefContractId == filter.RefContractId.Value);
                }

                var except = q.Where(c => c.CreatedBy != filter.UserId && c.Status == ContractaStatus.Draft);
                if (except.Count() > 0)
                {
                    q = q.Except(except);
                }



                //else q = q.Where(t => !t.RefContractId.HasValue);
                result.TotalRecord = q.Count();
                var entities = q.OrderByDescending(c => c.Id).Skip((filter.PageIndex - 1) * filter.PageSize).Take(filter.PageSize).ToList();

                var userIds = entities.Select(c => c.CreatedBy);
                var users = userRepository.Fetch(u => userIds.Contains(u.Id));

                var models = entities.CloneToListModels<Contracta, ContractaModel>();
                //get contract groups
                var contractIds = models.Select(c => c.Id).ToList();
                var contractBIds = models.Select(c => c.RefContractId).ToList();
                var groups = (from c in db.Contracts
                              join g in db.ContractGroups on c.GroupId equals g.Id
                              where contractBIds.Contains(c.Id)
                              select new
                              {
                                  c.Id,
                                  GroupName = g.Name,
                                  ContractCode = c.CustomerContractCode,
                                  ContractUID = c.UID
                              }).ToList();

                foreach (var model in models)
                {
                    var creator = users.FirstOrDefault(u => u.Id == model.CreatedBy);
                    if (creator != null)
                    {
                        model.CreateByName = creator.FullName;
                    }
                    if (model.FormalityCode.HasValue)
                        model.FormalitySelectName = model.FormalityCode.ToDescription();
                    model.StatusName = model.Status.ToDescription();

                    if (model.DateSign.HasValue)
                    {
                        model.DateSign = DateTime.SpecifyKind(model.DateSign.Value, DateTimeKind.Utc);
                    }

                    var group = groups.FirstOrDefault(c => c.Id == model.RefContractId);
                    if (group != null)
                    {
                        model.GroupName = group.GroupName;
                        model.RefContractCode = group.ContractCode;
                        model.RefContractUID = group.ContractUID;
                    }
                }


                var userCurrent = userRepository.GetById(filter.UserId);
                var p2Director = userService.GetDirector(Department.KHDT);
                var p5Director = userService.GetDirector(Department.TCKT);
                var records = new List<ContractaModel>();

                var userHandlers = new List<ContractAUser>();
                var userCreatedContracts = new List<int>();

                if (contractIds.Count() > 0)
                {
                    userHandlers = contractAUserRepository.Fetch(c => contractIds.Contains(c.ContractId));
                    userCreatedContracts.AddRange(models.Select(c => c.CreatedBy));
                }


                foreach (var item in models)
                {

                    if (userCurrent.Id == p2Director.Id && item.Status != ContractaStatus.SumitToP5Director
                        && item.Status != ContractaStatus.RejectFromP5Director)
                    {
                        if (item.Status == ContractaStatus.P5SendP2)
                        {
                            item.StatusName = "Đã tiếp nhận";
                        }
                        records.Add(item);
                    }
                    if (userCurrent.Id == p5Director.Id && item.Status != ContractaStatus.Draft && item.Status != ContractaStatus.SumitToP2Director
                        && item.Status != ContractaStatus.RejectFromP2Director)
                    {
                        if (item.Status == ContractaStatus.P2SendP5)
                        {
                            item.StatusName = "Đã tiếp nhận";
                        }

                        records.Add(item);
                    }
                    var userCreate = userCreatedContracts.FirstOrDefault(c => c == item.CreatedBy);
                    if (userHandlers.Count() + userCreatedContracts.Count() > 0)
                    {
                        var handlerInContractAs = userHandlers.Where(c => c.ContractId == item.Id);

                        if (handlerInContractAs.Count() + userCreate > 0)
                        {

                            var check = handlerInContractAs.Select(c => c.UserId).ToList();
                            check.Add(userCreate);
                            if (check.Contains(userCurrent.Id))
                            {
                                records.Add(item);
                            }
                        }

                    }


                }


                records = records.GroupBy(c => c.Id).Select(y => y.First()).ToList();
                result.Records = records;
                //tinh toan dư tiền đã thanh toán.
                var uids = result.Records.Select(c => c.UID).ToList();
                var settlements = db.Settlements.Where(c => uids.Contains(c.ContractUID)).Select(c => new { c.Id, c.ContractUID, c.BillValue }).ToList();
                var settlementids = settlements.Select(c => c.Id).ToList();
                List<int?> settIds = new List<int?>();
                foreach (var id in settlementids)
                {
                    settIds.Add(id);

                }

                var payments = db.SettlementPayments.Where(c => settIds.Contains(c.SettlementId)).Select(c => new { c.Id, c.SettlementId, c.PaymentValue }).ToList();
                foreach (var record in result.Records)
                {
                    record.BillAmount = (settlements.Where(c => c.ContractUID == record.UID).Sum(c => c.BillValue) ?? 0) + (record.OpeningBalance ?? 0);
                    var paymentAmount = (from a in settlements.Where(c => c.ContractUID == record.UID)
                                         join b in payments on a.Id equals b.SettlementId
                                         select b.PaymentValue
                                        ).Sum();
                    record.PaymentAmount = paymentAmount;
                }

                return result;
            }
        }
        public ContractaModel Save(ContractaSaveModel model)
        {
            var entity = new Contracta
            {
                DateCreated = DateTime.Now,
                CreatedBy = model.CreatedBy,
                Status = ContractaStatus.Draft,
                UID = Guid.NewGuid(),
                RefContractId = model.RefContractId
            };
            if (model.Id > 0 /*&& model.Status != ContractaStatus.Approved*/) entity = contractARepository.GetById(model.Id);
            entity.Price = model.Price;
            entity.Description = model.Description;
            entity.DateSign = model.DateSign;
            entity.Code = model.Code;
            entity.Supplier = model.Supplier;
            entity.Contents = model.Contents;
            entity.FormalityCode = model.FormalityCode;
            var groupId = contractAGroupRepository.GetById(model.GroupId);
            if (groupId == null) throw new Exception("Nhóm hợp đồng A không tồn tại");
            entity.GroupId = model.GroupId;
            if (model.FileList != null && model.FileList.Count > 0) entity.Files = JsonConvert.SerializeObject(model.FileList);

            if (model.Id > 0) contractARepository.Update(entity);
            else
            {
                //Tạo mới hợp đồng
                var code = contractARepository.FirstOrDefault(t => t.Code == model.Code);
                if (code != null) throw new ServiceException("Code đã tồn tại!");
                contractARepository.Insert(entity);
            }
            return entity.Clone<Contracta, ContractaModel>();
        }
        public bool Delete(Guid uid)
        {
            var entity = contractARepository.FirstOrDefault(c => c.UID == uid);
            var settlements = settlementRepository.Fetch(t => t.ContractUID == uid);
            var settlementIds = settlements.Select(t => t.Id);
            var settlementDocumentIds = settlementCheckRepository.Fetch(t => settlementIds.Contains(t.SettlementId)).Select(t => t.SettlementDocumentId);
            var settlementDocuments = settlementDocumentRepository.Fetch(t => settlementDocumentIds.Contains(t.Id));
            var profileLists = profileListContractARepository.Fetch(c => c.ContractAUID == entity.UID);
            foreach (var item in settlements)
            {
                settlementRepository.Delete(item);
            }
            foreach (var item in settlementDocuments)
            {
                settlementDocumentRepository.Delete(item);
            }
            foreach (var item in profileLists)
            {
                profileListContractARepository.Delete(item);
            }
            return contractARepository.Delete(entity);
        }

        public IEnumerable<ProfileListContractAModel> GetProfileListContractA(Guid contractaUid)
        {
            var contracta = contractARepository.FirstOrDefault(c => c.UID == contractaUid);
            if (contracta == null) throw new Exception("Hợp đồng này không tồn tại");
            var profileListContractAs = profileListContractARepository.Fetch(c => c.ContractAUID == contracta.UID);
            foreach (var profileListContractA in profileListContractAs)
            {
                var res = profileListContractA.Clone<ProfileListContractA, ProfileListContractAModel>();
                res.StatusName = res.Status.ToDescription();
                yield return res;
            }
        }

        public ContractaModel Get(Guid uid, int userId)
        {

            var entity = contractARepository.FirstOrDefault(u => u.UID == uid);
            var model = entity.Clone<Contracta, ContractaModel>();

            if (!string.IsNullOrEmpty(entity.Files))
            {
                model.FileList = JsonConvert.DeserializeObject<List<string>>(entity.Files);
            }

            if (entity.RefContractId.HasValue)
            {
                var contract = contractRepository.GetById(entity.RefContractId.Value);
                model.RefContractCode = contract.Code;
                model.RefContractUID = contract.UID;
            }
            model.ProfileListContractAs = GetProfileListContractA(entity.UID);

            var contractAUsers = new List<ContractAUser>();
            contractAUsers = contractAUserRepository.Fetch(cu => cu.ContractId == entity.Id).OrderBy(cu => cu.Function).ToList();


            var createdBy = userRepository.GetById(entity.CreatedBy);
            if (createdBy.Position != StaffPosition.Director)
            {
                var contractAUserNew = new ContractAUser
                {
                    ContractId = entity.Id,
                    UserId = createdBy.Id,
                    Function = UserFunction.Handler,
                    DepartmentId = Department.KHDT
                };
                if (entity.RefContractId.HasValue) contractAUserNew.Kind = ContractKind.AB; else contractAUserNew.Kind = ContractKind.A;
                contractAUsers.Add(contractAUserNew);
            }
            var userIds = contractAUsers.Select(u => u.UserId);
            var users1 = userRepository.Fetch(u => userIds.Contains(u.Id));

            var contractAUserModels = contractAUsers.ToList().CloneToListModels<ContractAUser, ContractUserModel>();

            foreach (var contractAUser in contractAUserModels)
            {
                var user = users1.FirstOrDefault(u => u.Id == contractAUser.UserId);
                if (user != null) contractAUser.UserName = user.FullName ?? user.Email;
            }
            model.ContractAUsers = contractAUserModels;
            if (entity.FormalityCode.HasValue)
            {
                model.FormalitySelectName = entity.FormalityCode.ToDescription();
            }
            var createUserName = userRepository.FirstOrDefault(u => u.Id == model.CreatedBy);
            model.CreateByName = createUserName.FullName ?? createUserName.Email;
            model.PlaceOn = DeterminedContractPlace(uid);
            model.StatusName = model.Status.ToDescription();
            model.DateSign = DateTime.SpecifyKind(model.DateSign.Value, DateTimeKind.Utc);


            if (!string.IsNullOrEmpty(model.Attachments))
            {
                var attachments = JsonConvert.DeserializeObject<List<Guid>>(model.Attachments);
                model.Documents = documentRepository.Fetch(d => attachments.Contains(d.Id));
            }



            var userCurrent = userRepository.GetById(userId);
            var p2Director = userService.GetDirector(Department.KHDT);
            var p5Director = userService.GetDirector(Department.TCKT);

            if (userCurrent.Id == p2Director.Id && model.Status != ContractaStatus.SumitToP5Director
                        && model.Status != ContractaStatus.RejectFromP5Director)
            {
                if (model.Status == ContractaStatus.P5SendP2)
                {
                    model.StatusName = "Đã tiếp nhận";
                }
            }
            if (userCurrent.Id == p5Director.Id && model.Status != ContractaStatus.Draft && model.Status != ContractaStatus.SumitToP2Director
                && model.Status != ContractaStatus.RejectFromP2Director)
            {
                if (model.Status == ContractaStatus.P2SendP5)
                {
                    model.StatusName = "Đã tiếp nhận";
                }
            }

            return model;
        }

        public string NextCode(DateTime signingDate)
        {
            var max = 1;
            var thisYearCount = contractARepository.Count(c => c.DateCreated.Year == signingDate.Year);

            var contractas = contractARepository.Fetch(c => c.Code.Contains("/" + signingDate.Year + "/HĐA/EVNETC"));

            if (contractas.Count() > 0)
            {
                var codes = contractas.Select(c => c.Code);

                var compares = new List<int>();
                foreach (var code in codes)
                {

                    var no = code.Split('/').First();
                    
                    int testCode = int.Parse(no);
                    compares.Add(testCode);
                }

                if (compares.Count() > 0)
                {
                    var maxCompare = compares.Max();
                    max = compares.Max();
                }
            }

            return string.Format("{0}/{1}/HĐA/EVNETC", max + 1, signingDate.Year);
        }

        #region ContractAGroup
        public IEnumerable<ContractAGroupModel> GetGroup()
        {
            return contractAGroupRepository.Fetch().ToList().CloneToListModels<ContractAGroup, ContractAGroupModel>();
        }

        public ContractAGroupModel SaveContractAGroup(ContractAGroupModel groupModel)
        {
            if (string.IsNullOrEmpty(groupModel.Name))
                throw new ServiceException("Chưa chọn tên nhóm hợp đồng A");
            var entity = groupModel.Map<ContractAGroup>();
            if (groupModel.Id != 0) contractAGroupRepository.Update(entity);
            else contractAGroupRepository.Insert(entity);
            return entity.Map<ContractAGroupModel>();
        }

        public bool DeleteContractAGroup(int groupId)
        {
            var entity = contractAGroupRepository.GetById(groupId);
            if (entity == null) throw new ServiceException("Nhóm hợp đồng A không tồn tại");
            return contractAGroupRepository.Delete(entity);
        }

        public IEnumerable<FormalitySelectDefined> GetFormalitySelectDefined()
        {

            foreach (var code in Enum.GetValues(typeof(FormalitySelectContractA)).Cast<FormalitySelectContractA>())
            {
                yield return new FormalitySelectDefined()
                {
                    FormalityCode = (int)code,
                    Title = code.ToDescription()
                };
            }
        }

        public IEnumerable<ProfileListContractADefinedModel> GetProfileListContractADefined(int formalityCode)
        {
            var defineds = profileListContractADefinedRepository.Fetch(c => c.FormalityCode == formalityCode);
            foreach (var define in defineds)
            {
                yield return define.Clone<ProfileListContractADefined, ProfileListContractADefinedModel>();
            }
        }

        public bool DeleteProfileListContractA(DeleteProfileContractA deleteProfile)
        {
            var result = false;
            if (deleteProfile.ProfileContractId.Count() > 0)
            {
                using (var context = profileListContractARepository.GetDbContext())
                {
                    using (var trans = context.Database.BeginTransaction())
                    {
                        try
                        {
                            foreach (var item in deleteProfile.ProfileContractId)
                            {
                                var existProfile = profileListContractARepository.GetById(item);
                                if (existProfile == null) throw new ServiceException("Danh mục hồ sơ này không tồn tại");
                                if (existProfile.Status == ProfileListContractAStatus.SentP5) throw new ServiceException("Danh mục hồ sơ này đã gửi cho P5 nên không được phép xóa");
                                result = profileListContractARepository.Delete(existProfile);
                                if (!result) break;
                            }
                            if (!result) trans.Rollback();
                        }
                        catch (Exception ex)
                        {
                            trans.Rollback();
                            throw ex;
                        }
                        if (result) { trans.Commit(); }
                    }
                }
            }
            return result;
        }

        public bool SaveProfileList(IEnumerable<ProfileListContractAModel> profileListContractAs, Guid uid)
        {
            var successUpdateProfile = true;
            var contractA = contractARepository.FirstOrDefault(c => c.UID == uid);
            if (contractA == null) throw new ServiceException("Hợp đồng không tồn tại");
            if (profileListContractAs.Count() > 0)
            {
                using (var context = profileListContractARepository.GetDbContext())
                {
                    using (var trans = context.Database.BeginTransaction())
                    {
                        try
                        {
                            //Cập nhật danh mục hồ sơ hợp đồng
                            var profileExists = profileListContractARepository.Fetch(c => c.ContractAUID == uid);

                            foreach (var item in profileListContractAs)
                            {
                                if (profileExists.Select(c => c.Id).Contains(item.Id))
                                {
                                    var profile = profileExists.FirstOrDefault(c => c.Id == item.Id);
                                    profile.Status = item.Status;
                                    if (item.Status == ProfileListContractAStatus.InCompleted)
                                    {
                                        if (!item.PromisedDate.HasValue) throw new ServiceException("Yêu cầu chọn ngày hoàn thiện cho danh mục hồ sơ " + item.Name);
                                        profile.PromisedDate = item.PromisedDate;
                                        profile.Feedback = item.Feedback;
                                    }
                                    if (item.Status == ProfileListContractAStatus.Failed)
                                    {
                                        if (string.IsNullOrEmpty(item.Feedback)) throw new ServiceException("Yêu cầu cho ý kiến phản hồi danh mục hồ sơ " + item.Name);
                                        profile.Feedback = item.Feedback;
                                    }
                                    profile.Reply = item.Reply;
                                    if (item.FeedbackStatus.HasValue)
                                    {
                                        profile.Feedback = item.Feedback;
                                        profile.FeedbackStatus = item.FeedbackStatus;
                                    }

                                    successUpdateProfile = profileListContractARepository.Update(profile);
                                }
                                else
                                {
                                    var addModel = new ProfileListContractA()
                                    {
                                        ContractAUID = item.ContractAUID,
                                        FormalityCode = item.FormalityCode,
                                        Name = item.Name,
                                        Status = item.Status
                                    };
                                    if (item.Status == ProfileListContractAStatus.InCompleted)
                                    {
                                        if (!item.PromisedDate.HasValue) throw new ServiceException("Yêu cầu chọn ngày hoàn thiện cho danh mục hồ sơ " + item.Name);
                                        addModel.PromisedDate = item.PromisedDate;
                                    }
                                    if (item.Status == ProfileListContractAStatus.Failed)
                                    {
                                        if (string.IsNullOrEmpty(item.Feedback)) throw new ServiceException("Yêu cầu cho ý kiến phản hồi danh mục hồ sơ " + item.Name);
                                        addModel.Feedback = item.Feedback;
                                    }
                                    addModel.Reply = item.Reply;

                                    successUpdateProfile = profileListContractARepository.Insert(addModel);
                                }
                                if (!successUpdateProfile) break;
                            }
                            if (!successUpdateProfile)
                                trans.Rollback();
                        }
                        catch (Exception)
                        {
                            trans.Rollback();
                            throw;
                        }
                        trans.Commit();
                    }
                }

            }
            return successUpdateProfile;
        }

        public bool SumitProfileListToP2Manager(Guid uid)
        {
            var result = false;
            var contracta = contractARepository.FirstOrDefault(c => c.UID == uid);
            if (contracta == null) throw new ServiceException("Hợp đồng không tồn tại");

            var profileLists = profileListContractARepository.Fetch(c => c.ContractAUID == uid);
            if (profileLists.Count() == 0) throw new ServiceException("Chưa có danh mục hồ sơ nào trong hợp đồng");
            using (var context = profileListContractARepository.GetDbContext())
            {
                using (var trans = context.Database.BeginTransaction())
                {
                    try
                    {
                        foreach (var profile in profileLists)
                        {
                            //profile.Status = 
                            //result = profileListContractARepository.Update(profile);
                        }
                    }
                    catch (Exception ex)
                    {
                        trans.Rollback();
                        throw ex;
                    }
                    trans.Commit();
                }
            }
            return false;
        }

        public bool Assign(AssignModel assignModel)
        {
            var entities = new List<ContractAUser>();
            assignModel.UserIds.ForEach(id => entities.Add(new ContractAUser
            {
                UserId = id,
                ContractId = assignModel.ContractId,
                DepartmentId = assignModel.DepartmentId,
                Kind = assignModel.Kind,
                Function = assignModel.Function,
            }));
            contractAUserRepository.InsertMany(entities);

            // notify assignee

            var contract = contractARepository.GetById(assignModel.ContractId);
            var users = userRepository.Fetch(u => assignModel.UserIds.Contains(u.Id));
            foreach (var user in users)
            {
                var message = string.Format(ASSIGN_CONTRACTA_USER, user.FullName, assignModel.DepartmentId.ToDescription(), contract.Code);
                notificationService.Notify(user.Id, message, NotificationType.AssignContractAUser, contract.Id);
            }
            return true;
        }

        public bool Unassign(int id)
        {
            if (id == 0) throw new ServiceException("Chuyên viên khởi tạo hợp đồng mặc định định là chuyên viên phụ trách nên không được phép xóa");
            var entity = contractAUserRepository.GetById(id);
            
            
            return contractAUserRepository.Delete(entity);
        }

        public ContractaModel SubmitToP2Director(Guid uid, int userId)
        {
            var contractA = contractARepository.FirstOrDefault(c => c.UID == uid);
            if (contractA == null) throw new ServiceException("Hợp đồng này không tồn tại");
            if (DeterminedContractPlace(contractA.UID) == 5) throw new ServiceException("Hợp đồng này đã được chuyển cho phòng tài chính kế toán");

            var p2Handlers = contractAUserRepository.Fetch(c => c.DepartmentId == Department.KHDT && c.Function == UserFunction.Handler && c.ContractId == contractA.Id);

            if (contractA.CreatedBy != userId && !p2Handlers.Select(c => c.UserId).Contains(userId))
                throw new ServiceException("Bạn không phải chuyên viên phụ trách hợp đồng này( phòng kế hoạch đầu tư)");


            //var 
            // if (userId != contractA.CreatedBy) throw new ServiceException("Bạn không phải chuyên viên phụ trách hợp đồng này( phòng kế hoạch đầu tư)");

            var p2Director = userService.GetDirector(Department.KHDT);
            var receiver = userService.GetById(contractA.CreatedBy);

            contractA.Status = ContractaStatus.SumitToP2Director;
            contractARepository.Update(contractA);

            var message = string.Format("{0} đã trình hợp đồng A, số HĐ {1}", receiver.FullName, contractA.Code);
            notificationService.Notify(new List<int> { p2Director.Id, receiver.Id }, message, NotificationType.SubmitToP2Director, contractA.Id);
            return contractA.Clone<Contracta, ContractaModel>();
        }

        public ContractaModel RejectContractAFromP2Director(Guid uid, int userId)
        {
            var p2Director = userService.GetDirector(Department.KHDT);
            var contractA = contractARepository.FirstOrDefault(c => c.UID == uid);
            if (contractA == null) throw new ServiceException("Hợp đồng này không tồn tại");
            if (p2Director.Id != userId) throw new ServiceException("Bạn không có quyền thực hiện tính năng này");
            if (DeterminedContractPlace(contractA.UID) == 5) throw new ServiceException("Hợp đồng này đã được chuyển cho phòng tài chính kế toán");

            var user = userService.GetById(contractA.CreatedBy);

            contractA.Status = ContractaStatus.RejectFromP2Director;
            contractARepository.Update(contractA);

            var message = string.Format("Yêu cầu thực hiện lại do {0} đã từ chối hợp đồng A, số HĐ {1}", p2Director.FullName, contractA.Code);
            notificationService.Notify(new List<int> { p2Director.Id, user.Id }, message, NotificationType.RejectFromP2Director, contractA.Id);
            return contractA.Clone<Contracta, ContractaModel>();
        }

        public ContractaModel P2SendContractP5(Guid uid, int userId)
        {
            var p2Director = userService.GetDirector(Department.KHDT);
            var p5Director = userService.GetDirector(Department.TCKT);

            var contractA = contractARepository.FirstOrDefault(c => c.UID == uid);
            if (contractA == null) throw new ServiceException("Hợp đồng này không tồn tại");
            if (DeterminedContractPlace(contractA.UID) == 5) throw new ServiceException("Hợp đồng này đã được chuyển cho phòng tài chính kế toán từ trước đó");
            if (p2Director.Id != userId) throw new ServiceException("Bạn không có quyền thực hiện tính năng này");

            if (contractA.Status != ContractaStatus.SumitToP2Director) throw new ServiceException("Hồ sơ hợp đồng chưa được trình lên");
            contractA.Status = ContractaStatus.P2SendP5;
            contractARepository.Update(contractA);


            var message = string.Format("Hồ sơ hợp đồng đã được chuyển sang phòng tài chính kế toán, số HĐ {0}", contractA.Code);
            if (p2Director.Id != userId) throw new ServiceException("Bạn không có quyền thực hiện tính năng này");

            var recivers = new List<int>();
            var contractAUsers = contractAUserRepository.Fetch(c => c.ContractId == contractA.Id);
            recivers.Add(p2Director.Id);
            recivers.Add(p5Director.Id);
            recivers.Add(contractA.CreatedBy);
            if (contractAUsers.Count() > 0)
            {
                var contractAUserIds = contractAUsers.Select(c => c.UserId);
                recivers.AddRange(contractAUserIds);
            }
            recivers = recivers.Distinct().ToList();


            notificationService.Notify(recivers, message, NotificationType.P2SendP5, contractA.Id);
            return contractA.Clone<Contracta, ContractaModel>();
        }


        public ContractaModel SubmitToP5Director(Guid uid, int userId)
        {
            var contractA = contractARepository.FirstOrDefault(c => c.UID == uid);
            if (contractA == null) throw new ServiceException("Hợp đồng này không tồn tại");
            if (DeterminedContractPlace(contractA.UID) == 1) throw new ServiceException("Hợp đồng này đã được chuyển cho phòng kế hoạch đầu tư trước đó");

            var contractUsers = contractAUserRepository.Fetch(c => c.ContractId == contractA.Id);
            var handlers = contractUsers.Where(c => c.Function == UserFunction.Handler);
            if (handlers.Count() == 0) throw new ServiceException("Hợp đồng chưa có chuyên viên phụ trách, số HĐ " + contractA.Code);
            if (!handlers.Select(c => c.UserId).Contains(userId)) throw new ServiceException("Bạn không phải chuyên viên phụ trách hợp đồng này( phòng tài chính kế toán)");



            contractA.Status = ContractaStatus.SumitToP5Director;
            contractARepository.Update(contractA);

            var p5Director = userService.GetDirector(Department.TCKT);
            var user = userService.GetById(userId);
            var message = string.Format("{0} đã trình hợp đồng A, số HĐ {1}", user.FullName, contractA.Code);
            notificationService.Notify(new List<int> { p5Director.Id, user.Id }, message, NotificationType.SubmitToP5Director, contractA.Id);

            return contractA.Clone<Contracta, ContractaModel>();
        }

        public ContractaModel RejectFromP5Director(Guid uid, int userId)
        {
            var contractA = contractARepository.FirstOrDefault(c => c.UID == uid);
            if (contractA == null) throw new ServiceException("Hợp đồng này không tồn tại");
            var p5Director = userService.GetDirector(Department.TCKT);
            if (p5Director.Id != userId) throw new ServiceException("Bạn không có quyền thực hiện tính năng này");
            if (DeterminedContractPlace(contractA.UID) == 1) throw new ServiceException("Hợp đồng này đã được chuyển cho phòng kế hoạch đầu tư");

            var contractUsers = contractAUserRepository.Fetch(c => c.ContractId == contractA.Id);

            var message = string.Format("Yêu cầu thực hiện lại do {0} đã từ chối hợp đồng A, số HĐ {1}", p5Director.FullName, contractA.Code);

            contractA.Status = ContractaStatus.RejectFromP5Director;
            contractARepository.Update(contractA);

            var listReceives = new List<int>();
            listReceives.Add(p5Director.Id);
            listReceives.AddRange(contractUsers.Select(c => c.Id).ToList());
            notificationService.Notify(listReceives, message, NotificationType.RejectFromP5Director, contractA.Id);

            return contractA.Clone<Contracta, ContractaModel>();
        }

        public (ContractaModel, string) P5SendContractP2(Guid uid, int userId)
        {
            var p2Director = userService.GetDirector(Department.KHDT);
            var p5Director = userService.GetDirector(Department.TCKT);

            var contractA = contractARepository.FirstOrDefault(c => c.UID == uid);
            if (contractA == null) throw new ServiceException("Hợp đồng này không tồn tại");
            if (p5Director.Id != userId) throw new ServiceException("Bạn không có quyền thực hiện tính năng này");
            if (DeterminedContractPlace(contractA.UID) == 1) throw new ServiceException("Hợp đồng này đã được chuyển cho phòng kế hoạch đầu tư từ trước đó");

            if (contractA.Status != ContractaStatus.SumitToP5Director) throw new ServiceException("Hồ sơ hợp đồng chưa được trình lên");


            var profileLists = profileListContractARepository.Fetch(c => c.ContractAUID == contractA.UID);
            var message = string.Empty;
            if (profileLists.Count() > 0)
            {

                //if (profileLists.Any(c => c.FeedbackStatus == P5FeedbackContractAStatus.NotAchieved))
                //{
                //    contractA.Status = ContractaStatus.P5SendP2;
                //    message = string.Format("Hồ sơ hợp đồng đã được chuyển sang phòng kế hoạch đầu tư, số HĐ {0}", contractA.Code);
                //}
                //else
                //{
                    contractA.Status = ContractaStatus.P5SendP2;
                    message = string.Format("Danh mục hồ sơ trong hợp đồng A đã hoàn thiện, số HĐ {0}", contractA.Code);
                //}
                contractARepository.Update(contractA);
            }

            var contractAUsers = contractAUserRepository.Fetch(c => c.ContractId == contractA.Id);

            var recivers = new List<int>();
            recivers.Add(p2Director.Id);
            recivers.Add(p5Director.Id);
            recivers.Add(contractA.CreatedBy);
            if (contractAUsers.Count() > 0)
            {
                var contractAUserIds = contractAUsers.Select(c => c.UserId);
                recivers.AddRange(contractAUserIds);
            }
            recivers = recivers.Distinct().ToList();



            notificationService.Notify(recivers, message, NotificationType.P5SendP2, contractA.Id);
            return (contractA.Clone<Contracta, ContractaModel>(), message);
        }

        public ContractaModel P2DoneCompleted(Guid uid, int userId)
        {
            var p2Director = userService.GetDirector(Department.KHDT);
            var p5Director = userService.GetDirector(Department.TCKT);
            var contractA = contractARepository.FirstOrDefault(c => c.UID == uid);
            if (contractA == null) throw new ServiceException("Hợp đồng này không tồn tại");
            var profileLists = profileListContractARepository.Fetch(c => c.ContractAUID == contractA.UID);

            if (userId != p2Director.Id && userId != p5Director.Id)
                throw new ServiceException("Bạn không có quyền thực hiện tính năng này");
            contractA.Status = ContractaStatus.Completed;
            contractARepository.Update(contractA);

            return contractA.Clone<Contracta, ContractaModel>();
        }


        /// <summary>
        /// If result = 1 contract A place in P2, if result = 5 contract A place in P5, if result = 10 contract A completed
        /// </summary>
        /// <param name="uid"></param>
        /// <returns></returns>
        public int DeterminedContractPlace(Guid uid)
        {
            var result = 0;
            var contractA = contractARepository.FirstOrDefault(c => c.UID == uid);
            if (contractA == null) throw new ServiceException("Hợp đồng này không tồn tại");
            switch (contractA.Status)
            {
                case ContractaStatus.Draft:
                    result = 1;
                    break;
                case ContractaStatus.SumitToP2Director:
                    result = 1;
                    break;
                case ContractaStatus.RejectFromP2Director:
                    result = 1;
                    break;
                case ContractaStatus.SumitToP5Director:
                    result = 5;
                    break;
                case ContractaStatus.RejectFromP5Director:
                    result = 5;
                    break;
                case ContractaStatus.P2SendP5:
                    result = 5;
                    break;
                case ContractaStatus.P5SendP2:
                    result = 1;
                    break;
                case ContractaStatus.Completed:
                    result = 10;
                    break;
                default:
                    break;
            }

            return result;
        }

        public List<Document> AttachDocuments(int id, List<FileInfoModel> files)
        {
            var documents = new List<Document>();
            foreach (var file in files)
            {
                var document = documentService.AddDocument(file);
                documents.Add(document);
            }
            if (documents.Count > 0)
            {
                var entity = contractARepository.GetById(id);
                var docs = new List<Guid>();
                if (!string.IsNullOrEmpty(entity.Attachments))
                {
                    docs = JsonConvert.DeserializeObject<List<Guid>>(entity.Attachments);
                }
                docs.AddRange(documents.Select(d => d.Id));
                entity.Attachments = JsonConvert.SerializeObject(docs);
                contractARepository.Update(entity);
            }
            return documents;
        }


        #endregion

    }
}
