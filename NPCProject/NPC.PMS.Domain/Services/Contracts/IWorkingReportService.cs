using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using NPC.PMS.Domain.Models.ContractB;
using NPC.PMS.Domain.Models.Reports;
using NPC.PMS.Domain.Models.Reports.Mobile;

namespace NPC.PMS.Domain.Services.Reports
{
    public interface IWorkingReportService
    {
        WorkingModel GetWorkingInfo(int staffId, int phaseId);
        ReportModel Save(ReportModel model);
        ReportFilterResult Filter(ReportFilterModel filter);
        List<ReportModel> MobileFilter(int userId, ReportFilterMobileModel filter);
        List<PhaseModel> GetUserPhases(int userId);
        ReportModel Get(int id);
        bool Approve(int id);
        bool Reject(int id,string reason);
    }
}
