﻿using Newtonsoft.Json;
using NPC.PMS.Domain.Data;
using NPC.PMS.Domain.Data.Entity;
using NPC.PMS.Domain.Enums;
using NPC.PMS.Domain.Models;
using NPC.PMS.Domain.Models.ContractA;
using NPC.PMS.Domain.Models.ContractB;
using NPC.PMS.Domain.Models.Documents;
using NPC.PMS.Domain.Models.Quotes;
using NPC.PMS.Domain.Services.Common.Documents;
using NPC.PMS.Domain.Services.Common.Notifications;
using NPC.PMS.Domain.Services.Users;
using NPC.PMS.Framework.Extensions.Enums;
using NPC.PMS.Framework.MimeDetective;
using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace NPC.PMS.Domain.Services.Contracts
{
    public class QuoteService : IQuoteService
    {
        private readonly IRepository<Quote> quoteRepository;
        private readonly IRepository<User> userRepository;
        private readonly IUserService userService;
        private readonly IRepository<ContractUser> contractUserRepository;
        private readonly IRepository<QuoteStaff> quoteStaffRepository;
        private readonly IRepository<QuoteWorkitem> quoteWorkitemRepository;
        private readonly IRepository<Customer> customerRepository;
        private readonly IRepository<QuoteRecord> quoteRecordRepository;
        private readonly INotificationService notificationService;
        private readonly IDepartmentService departmentService;
        private readonly IDocumentService documentService;
        private readonly IRepository<Document> documentRepository;
        private readonly IRepository<Phase> phaseRepository;
        const string SUBMITED_MESSAGE = "Hợp đồng {0} đã được trình lên TP.KH&ĐT";
        const string APPROVE_MESSAGE = "Hợp đồng {0} đã được phê duyệt";
        const string PASSED_MESSAGE = "Phiếu giao nhiệm vụ {0} đã được chuyển cho phòng chuyên môn {1}";

        public QuoteService(IRepository<Quote> quoteRepository,
            IRepository<User> userRepository,
            IUserService userService,
            IRepository<ContractUser> contractUserRepository,
            IRepository<QuoteStaff> quoteStaffRepository,
            IRepository<QuoteWorkitem> quoteWorkitemRepository,
            IRepository<Customer> customerRepository,
            IRepository<QuoteRecord> quoteRecordRepository,
            INotificationService notificationService,
            IDepartmentService departmentService,
            IDocumentService documentService,
            IRepository<Document> documentRepository,
             IRepository<Phase> phaseRepository
        )
        {
            this.quoteRepository = quoteRepository;
            this.userRepository = userRepository;
            this.userService = userService;
            this.contractUserRepository = contractUserRepository;
            this.quoteStaffRepository = quoteStaffRepository;
            this.quoteWorkitemRepository = quoteWorkitemRepository;
            this.customerRepository = customerRepository;
            this.quoteRecordRepository = quoteRecordRepository;
            this.notificationService = notificationService;
            this.departmentService = departmentService;
            this.documentService = documentService;
            this.documentRepository = documentRepository;
            this.phaseRepository = phaseRepository;
        }

        public QuoteFilterResult Filter(QuoteFilterModel model)
        {
            var result = new QuoteFilterResult { Filter = model };
            var user = userService.GetById(model.UserId);
            var db = quoteRepository.GetDbContext();
            {
                var q = db.Quotes.AsQueryable();
                if (model.DocType!=null)
                {
                    q = q.Where(c => c.DocType == model.DocType);
                }
              

                if (user.DepartmentId == Department.KHDT || user.DepartmentId == Department.TCKT)
                {

                }


                if (!string.IsNullOrEmpty(model.Code))
                {
                    q = q.Where(c => c.Code.Contains(model.Code));
                }
                if (!string.IsNullOrEmpty(model.Content))
                {
                    q = q.Where(c => c.Content.Contains(model.Content));
                }

                if (model.Status.HasValue)
                {
                    q = q.Where(c => c.Status == model.Status.Value);
                }
                if (model.CustomerId.HasValue)
                {
                    q = q.Where(c => c.CustomerId == model.CustomerId.Value);
                }

                var except = q.Where(c => c.CreatedBy != model.UserId && c.Status == QuoteStatus.Draft);
                if (except.Count() > 0)
                {
                    q = q.Except(except);
                }


                var userCurrent = userRepository.FirstOrDefault(c => c.Id == model.UserId);


                var isDirectorP2 = (userCurrent.Position == StaffPosition.Director && userCurrent.DepartmentId == (int)Department.KHDT);

                var isDirectorP5 = (userCurrent.Position == StaffPosition.Director && userCurrent.DepartmentId == (int)Department.TCKT);

                var isStaffP2 = ((userCurrent.Position == StaffPosition.Employee || userCurrent.Position == StaffPosition.Deputy) && userCurrent.DepartmentId == (int)Department.KHDT);

                var quoteResults = new List<Quote>();

                //q.ToList();
                if (q.Count() > 0)
                {
                    var quoteSelectIds = q.GroupBy(c=>c.Id).Select(c=>c.Key).ToList();
                    var quoteStaffAlls = quoteStaffRepository.Fetch(c => quoteSelectIds.Contains(c.QuoteId)); // tất cả nhân viên trong tất cả báo giá


                    var t = quoteStaffAlls.Select(c => c.StaffId);

                    foreach (var item in q)
                    {
                        var departmentList = new List<int>();
                        if (!string.IsNullOrEmpty(item.Departments))
                        {
                            departmentList = JsonConvert.DeserializeObject<List<int>>(item.Departments);
                        }

                        var isDirectorPCMIn = (userCurrent.Position == StaffPosition.Director && departmentList.Contains(userCurrent.DepartmentId.Value));

                        var quoteStaffIns = quoteStaffAlls.Where(c => c.QuoteId == item.Id).ToList();

                        //if(isStaffP2 && item.CreatedBy == model.UserId && item.Status == QuoteStatus.Draft) //nếu là nhân viên P2 và khởi tạo hợp đồng này ở trạng thái bản nháp
                        //{
                        //    quoteResults.Add(item);
                        //    continue;
                        //}


                        // trưởng phòng chuyên môn của báo giá, trưởng phòng P2, trưởng phòng P5, nhân sự tham gia báo giá
                        if (isDirectorP2 || isDirectorP5 || isDirectorPCMIn || (item.CreatedBy == model.UserId /*&& item.Status != QuoteStatus.Draft*/) || (quoteStaffIns.Any(c => c.StaffId == model.UserId) && item.Status == QuoteStatus.Passed))
                        {
                            quoteResults.Add(item);
                        }

                    }
                }


                result.TotalRecord = quoteResults.Count();

                var entities = quoteResults.OrderBy(c => c.Status).Skip((model.PageIndex - 1) * model.PageSize).Take(model.PageSize).ToList();
                result.Records = entities.Map<List<QuoteModel>>();
                var customers = (from r in result.Records
                                join c in db.Customers on r.CustomerId equals c.Id
                                select c).ToList();
                var userIds = result.Records.Select(c => c.CreatedBy.Value);
                var users = userRepository.Fetch(u => userIds.Contains(u.Id));


                var departments = departmentService.GetTechDepartments();
                foreach (var record in result.Records)
                {
                    var customer = customers.FirstOrDefault(c => c.Id == record.CustomerId);
                    if (customer != null)
                    {
                        record.CustomerName = customer.Name;
                        record.CustomerCode = customer.Code;
                    }

                    var creator = users.FirstOrDefault(u => u.Id == record.CreatedBy.Value);
                    if (creator != null)
                    {
                        record.CreateByName = creator.FullName;
                    }


                    if (!string.IsNullOrEmpty(record.Departments))
                    {
                        record.DepartmentList = JsonConvert.DeserializeObject<List<int>>(record.Departments);
                        var str = new List<string>();
                        foreach (var item in record.DepartmentList)
                        {
                            str.Add(((Department)item).ToDescription());
                        }

                        record.DepartmentNames = string.Join(",", str);
                    }
                }

                //tinh toan dư tiền đã thanh toán.
                var uids = result.Records.Select(c => c.UID).ToList();
                var settlements = db.Settlements.Where(c => uids.Contains(c.ContractUID)).Select(c => new { c.Id, c.ContractUID, c.BillValue }).ToList();
                var settlementids = settlements.Select(c => c.Id).ToList();
                List<int?> settIds = new List<int?>();
                foreach (var id in settlementids)
                {
                    settIds.Add(id);

                }

                var payments = db.SettlementPayments.Where(c => settIds.Contains(c.SettlementId)).Select(c => new { c.Id, c.SettlementId, c.PaymentValue }).ToList();
                foreach (var record in result.Records)
                {
                    record.BillAmount = (settlements.Where(c => c.ContractUID == record.UID).Sum(c => c.BillValue) ?? 0) + (record.OpeningBalance ?? 0);
                    var paymentAmount = (from a in settlements.Where(c => c.ContractUID == record.UID)
                                         join b in payments on a.Id equals b.SettlementId
                                         select b.PaymentValue
                                        ).Sum();
                    record.PaymentAmount = paymentAmount;
                }

                return result;
            }
        }

        public QuoteModel Save(QuoteFormModel model)
        {
            var creator = userRepository.GetById(model.CreateBy);

            //if (!model.StartDate.HasValue) throw new ServiceException("Vui lòng chọn ngày bắt đầu thực hiện hợp đồng.");
            //if (!model.EndDate.HasValue) throw new ServiceException("Vui lòng chọn ngày kết thúc hợp đồng.");
            //if (model.EndDate.Value.Date < model.StartDate.Value.Date) throw new ServiceException("Ngày kết thúc phải lớn hơn hoặc bằng ngày bắt đầu.");

            var entity = new Quote();
            if (model.Id.HasValue) entity = quoteRepository.GetById(model.Id.Value);
            else
            {
                entity.CreatedBy = model.CreateBy;
                entity.CreatedOn = DateTime.Now;
                entity.Status = QuoteStatus.Draft;
                entity.Code = model.Code;
            }
            if (entity.Status == QuoteStatus.Passed || entity.Status == QuoteStatus.Closed) throw new ServiceException("Không thể sửa hợp đồng");
            entity.SigningDate = model.SigningDate;
            entity.DocType = model.DocType;
            entity.CustomerId = model.CustomerId;
            entity.Value = model.Value;
            entity.StartDate = model.StartDate;
            entity.EndDate = model.EndDate;
            entity.Content = model.Content;

            if (model.DepartmentList != null && model.DepartmentList.Count > 0)
            {
                entity.Departments = JsonConvert.SerializeObject(model.DepartmentList);
            }

            if (model.Id.HasValue) // update
            {
                if (model.Code != entity.Code)
                {
                    var exsited = quoteRepository.Any(c => c.Code == model.Code);
                    if (exsited) throw new ServiceException("Số hợp đồng đã tồn tại");
                    entity.Code = model.Code;
                }
                quoteRepository.Update(entity);
            }
            else // add
            {
                var test = quoteRepository.Fetch();
                var exsited = quoteRepository.Any(c => c.Code == model.Code && c.DocType == DocumentType.Quotation);
                if (exsited) throw new ServiceException("Số báo giá đã tồn tại");

                quoteRepository.Insert(entity);
                if (creator.Position.Value != StaffPosition.Director)
                {
                    contractUserRepository.Insert(new ContractUser
                    {
                        ContractId = entity.Id,
                        Kind = ContractKind.Quote,
                        DepartmentId = (Department)creator.DepartmentId.Value,
                        UserId = creator.Id,
                        Function = UserFunction.Handler,
                        ProcessType = ProcessType.QT02
                    });
                }
            }
            var result = entity.Map<QuoteModel>();
            if (!string.IsNullOrEmpty(result.Departments))
            {
                result.DepartmentList = JsonConvert.DeserializeObject<List<int>>(result.Departments);

                var str = new List<string>();
                foreach (var item in result.DepartmentList)
                {
                    str.Add(((Department)item).ToDescription());
                }
               
                result.DepartmentNames = string.Join(",", str);
            }

            return result;
        }

        public bool DeleteQuote(int id)
        {
            var entity = quoteRepository.GetById(id);
            return quoteRepository.Delete(entity);
        }

        public bool AddQuoteStaff(AddQuoteStaffModel model, int userCurrentId)
        {

            var staff = userRepository.GetById(model.StaffId.Value);
            staff = staff ?? throw new ServiceException("Cán bộ không có trong danh sách");

            var userCurrent = userRepository.GetById(userCurrentId);
            var director = userService.GetDirector((Department)staff.DepartmentId);
            if (userCurrent.DepartmentId != staff.DepartmentId || userCurrent.Id != director.Id)
                throw new ServiceException("Bạn không phải là trưởng phòng chuyên môn");

            var quote = quoteRepository.GetById(model.QuoteId.Value);
            quote = quote ?? throw new ServiceException("Phiếu giao nhiệm vụ không tồn tại");

            var quoteStaff = quoteStaffRepository.FirstOrDefault(c => c.StaffId == model.StaffId.Value
                && c.QuoteId == model.QuoteId.Value);
            if (quoteStaff != null) throw new ServiceException("Cán bộ đã được giao nhiệm vụ");           
       

            var result = quoteStaffRepository.Insert(new QuoteStaff
            {
                StaffId = model.StaffId.Value,
                QuoteId = model.QuoteId.Value,
                IsLeader = model.IsLeader
            });
            var notificationMessage = string.Empty;
            if (quote.DocType == DocumentType.Contract)
            {
                notificationMessage = string.Format("Bạn đã được chỉ định vào hợp đồng TBL: {0}", quote.Code);
                if (result) notificationService.Notify(model.StaffId.Value, notificationMessage, NotificationType.AssignUserHDTBL, quote.Id);
            }
            if (quote.DocType == DocumentType.Quotation)
            {
                notificationMessage = string.Format("Bạn đã được chỉ định vào báo giá TBL : {0}", quote.Code);
                if (result) notificationService.Notify(model.StaffId.Value, notificationMessage, NotificationType.AssignUserHDTBL, quote.Id);
            }
            
            return result;
        }

        public bool SetLeaderQuote(AddQuoteStaffModel model)
        {
            var staff = userRepository.GetById(model.StaffId.Value);
            staff = staff ?? throw new ServiceException("Cán bộ không có trong danh sách");

            var quote = quoteRepository.GetById(model.QuoteId.Value);
            quote = quote ?? throw new ServiceException("Phiếu giao nhiệm vụ không tồn tại");

            var quoteStaff = quoteStaffRepository.FirstOrDefault(c => c.StaffId == model.StaffId.Value
                && c.QuoteId == model.QuoteId.Value);


            var quoteStaffs = quoteStaffRepository.Fetch(c => c.QuoteId == model.QuoteId.Value);        

            if (quoteStaff == null) throw new ServiceException("Cán bộ chưa được giao nhiệm vụ");
            quoteStaff.IsLeader = model.IsLeader;
            return quoteStaffRepository.Update(quoteStaff);
        }

        public bool DeleteQuoteStaff(DeleteQuoteStaffModel model)
        {
            var staff = userRepository.GetById(model.StaffId.Value);
            staff = staff ?? throw new ServiceException("Cán bộ không có trong danh sách");

            var quote = quoteRepository.GetById(model.QuoteId.Value);
            quote = quote ?? throw new ServiceException("Phiếu giao nhiệm vụ không tồn tại");

            var quoteStaff = quoteStaffRepository.FirstOrDefault(c => c.StaffId == model.StaffId && c.QuoteId == model.QuoteId);
            quoteStaff = quoteStaff ?? throw new ServiceException("Cán bộ " + staff.Email + " chưa được giao nhiệm vụ này");

            return quoteStaffRepository.Delete(quoteStaff);
        }

        public IEnumerable<QuoteStaffModel> GetStaffByQuoteId(int quoteId)
        {
            var quoteStaffs = quoteStaffRepository.Fetch(c => c.QuoteId == quoteId);
            if (quoteStaffs.Count() > 0)
            {
                var staffIds = quoteStaffs.Select(c => c.StaffId);
                var users = userRepository.Fetch(c => staffIds.Contains(c.Id));
                foreach (var quoteStaff in quoteStaffs)
                {
                    var user = users.FirstOrDefault(c => c.Id == quoteStaff.StaffId);
                    yield return new QuoteStaffModel
                    {
                        QuoteId = quoteStaff.QuoteId,
                        StaffId = quoteStaff.StaffId,
                        StaffName = user.FullName ?? user.Email,
                        IsLeader = quoteStaff.IsLeader,
                        SafeLevel = user.SafeLevel.HasValue ? user.SafeLevel.Value : 0,
                        DepartmentId = user.DepartmentId
                    };
                }
            }
        }

        public List<QuoteWorkitemModel> ParseQuoteWorkitemFile(int id, byte[] bytes)
        {
            var fileType = bytes.GetFileType();
            if (fileType.Mime != "application/vnd.ms-excel" && fileType.Mime != "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")
                throw new ServiceException("File không được chấp nhận");

            MemoryStream stream = new MemoryStream();
            stream.Write(bytes, 0, bytes.Length);

            var package = new ExcelPackage(stream);
            ExcelWorksheet worksheet = package.Workbook.Worksheets[0];
            int totalRows = worksheet.Dimension.Rows;

            var cells = worksheet.Cells;
            var sttCell = cells.FirstOrDefault(c => c.Text == "STT");
            if (sttCell == null) throw new ServiceException("Không tìm thấy tiêu đề cột: STT");
            var titleCell = cells.FirstOrDefault(c => c.Text == "Nội dung");
            var unitCell = cells.FirstOrDefault(c => c.Text == "Đơn vị");
            var quantityCell = cells.FirstOrDefault(c => c.Text == "SL");
            var priceCell = cells.FirstOrDefault(c => c.Text == "Đơn giá");
            var moneyCell = cells.FirstOrDefault(c => c.Text == "Thành tiền");


            var endCell = cells.FirstOrDefault(c => c.Text == "Giá trị trước thuế");
            if (endCell == null) throw new ServiceException("Không tìm thấy: Giá trị trước thuế");

            var entities = new List<QuoteWorkitem>();
            var index = 1;
            for (int i = sttCell.Start.Row + 1; i < endCell.Start.Row; i++)
            {
                var quantity = cells[i, quantityCell.Start.Column].Text;
                var price = cells[i, priceCell.Start.Column].Text;
                var money = cells[i, moneyCell.Start.Column].Text;

                var entity = new QuoteWorkitem
                {
                    QuoteId = id,
                    Index = index,
                    No = cells[i, sttCell.Start.Column].Text,
                    Title = cells[i, titleCell.Start.Column].Text,
                    Unit = cells[i, unitCell.Start.Column].Text,
                    Quantity = !string.IsNullOrEmpty(quantity) ? decimal.Parse(quantity) : 0,
                    Price = !string.IsNullOrEmpty(price) ? decimal.Parse(price) : 0,
                    Money = !string.IsNullOrEmpty(money) ? decimal.Parse(money) : 0
                };
                entities.Add(entity);
                index++;
            }
            if (quoteWorkitemRepository.InsertMany(entities))
            {
                return entities.Map<List<QuoteWorkitemModel>>();
            }
            return new List<QuoteWorkitemModel>();
        }

        public List<QuoteWorkitemModel> GetQuoteWorkitems(Guid uid)
        {
            var quote = quoteRepository.FirstOrDefault(c => c.UID == uid);
            var list = quoteWorkitemRepository.Fetch(w => w.QuoteId == quote.Id).ToList().OrderBy(w => w.Index);
            return list.Map<List<QuoteWorkitemModel>>();
        }

        public bool ClearQuoteWorkitem(int id)
        {
            return quoteWorkitemRepository.DeleteMany(w => w.QuoteId == id) > 0;
        }

        public QuoteModel GetQuote(int quoteId)
        {
            var result = new QuoteModel();
            var entity = quoteRepository.GetById(quoteId);

            result = entity.Map<QuoteModel>();

            var customer = customerRepository.FirstOrDefault(c => c.Id == result.CustomerId);
            if (customer != null)
            {
                result.CustomerName = customer.Name;
                result.CustomerCode = customer.Code;
            }
            if (!string.IsNullOrEmpty(result.Departments))
            {
                result.DepartmentList = JsonConvert.DeserializeObject<List<int>>(result.Departments);
                var str = new List<string>();
                foreach (var item in result.DepartmentList)
                {
                    str.Add(((Department)item).ToDescription());
                }

                result.DepartmentNames = string.Join(",", str);
            }
            return result;
        }

        public QuoteModel GetQuoteByUid(Guid uid)
        {
            var result = new QuoteModel();
            var entity = quoteRepository.FirstOrDefault(c => c.UID == uid);

            result = entity.Map<QuoteModel>();

            var customer = customerRepository.FirstOrDefault(c => c.Id == result.CustomerId);
            if (customer != null)
            {
                result.CustomerName = customer.Name;
                result.CustomerCode = customer.Code;
            }

            if (!string.IsNullOrEmpty(result.Departments))
            {
                result.DepartmentList = JsonConvert.DeserializeObject<List<int>>(result.Departments);
                var str = new List<string>();
                foreach (var item in result.DepartmentList)
                {
                    str.Add(((Department)item).ToDescription());
                }

                result.DepartmentNames = string.Join(",", str);
            }

            //if (!string.IsNullOrEmpty(result.Attachments))
            //{
            //    var attachments = JsonConvert.DeserializeObject<List<Guid>>(result.Attachments);
            //    result.Documents = documentRepository.Fetch(d => attachments.Contains(d.Id));
            //}


            result.Phases = phaseRepository.Fetch(p => p.ContractId == entity.Id && p.ProcessType == ProcessType.QT01);
            if (!string.IsNullOrEmpty(result.Attachments))
            {
                var attachments = JsonConvert.DeserializeObject<List<Guid>>(result.Attachments);
                result.Documents = documentRepository.Fetch(d => attachments.Contains(d.Id));
            }

            var contractUsers = contractUserRepository.Fetch(cu => cu.ContractId == entity.Id && cu.ProcessType == ProcessType.QT02).OrderBy(cu => cu.Function);
            var userIds = contractUsers.Select(u => u.UserId);
            var users1 = userRepository.Fetch(u => userIds.Contains(u.Id));
            var contractUserModels = contractUsers.Map<List<ContractUserModel>>();

            foreach (var contractUser in contractUserModels)
            {
                var user = users1.FirstOrDefault(u => u.Id == contractUser.UserId);
                if (user != null) contractUser.UserName = user.FullName;
            }
            result.ContractUsers = contractUserModels;

            return result;
        }

        public string NextCode(DateTime signingDate, bool isContract)
        {
            using (var db = quoteRepository.GetDbContext())
            {
                var last = db.Contracts.LastOrDefault(c => c.SigningDate.Value.Year == signingDate.Year && (isContract ? c.DocType == DocumentType.Contract : c.DocType == DocumentType.Quotation));
                var thisYearCount = 0;
                if (last != null)
                {
                    try
                    {
                        var no = last.Code.Split('/').First();
                        if (no.StartsWith("0"))
                            no = no.TrimStart(new char[] { '0' });
                        thisYearCount = int.Parse(no);
                    }
                    catch { }
                }

                var lastTBL = db.Quotes.LastOrDefault(c => c.SigningDate.Value.Year == signingDate.Year && (isContract ? c.DocType == DocumentType.Contract : c.DocType == DocumentType.Quotation));
                var thisYearCountTBL = 0;
                if (lastTBL != null)
                {
                    try
                    {
                        var no = lastTBL.Code.Split('/').First();
                        if (no.StartsWith("0"))
                            no = no.TrimStart(new char[] { '0' });
                        thisYearCountTBL = int.Parse(no);
                    }
                    catch { }
                }

                if (thisYearCount > thisYearCountTBL)
                    return string.Format("{0}/{1}" + (isContract == true ? "" : "/BG"), thisYearCount + 1, signingDate.Year);
                else
                    return string.Format("{0}/{1}" + (isContract == true ? "" : "/BG"), thisYearCountTBL + 1, signingDate.Year);

                //return string.Format("{0}/{1}/BG", thisYearCount + 1, signingDate.Year);
            }
        }

        public bool Next(int id, string action)
        {
            var notificationMessage = string.Empty;
            var receivers = new List<int>();

            var entity = quoteRepository.GetById(id);
            if (action == "pass") // handle pass to specialized division action
            {
                var departments = entity.Departments;

                var departmentLists = JsonConvert.DeserializeObject<List<int>>(departments);
                if (departmentLists.Count() == 0) throw new ServiceException("Chưa chọn phòng ban để thực hiện");

                var names = new List<string>();
                departmentLists.ForEach(c =>
                {
                    names.Add(((Department)c).ToDescription());
                });
                var departmentNames = string.Join(", ", names);

                notificationMessage = string.Format(PASSED_MESSAGE, entity.Code, departmentNames);
                foreach (var department in departmentLists)
                {
                    entity.Status = QuoteStatus.Passed;
                    var departmentName = EnumExtensions.ToDescription((Department)department);
                    
                    var headOfDepartment = userService.GetDirector((Department)department);
                    receivers.Add(headOfDepartment.Id);
                    
                }


            }
            else if (action == "submit")
            {
                //var isUploadWorkitems = quoteWorkitemRepository.Any(w => w.QuoteId == entity.Id);
                //if (!isUploadWorkitems) throw new ServiceException("Vui lòng upload bảng khối lượng trước khi trình duyệt");
                entity.Status = QuoteStatus.Submited;

                notificationMessage = string.Format(SUBMITED_MESSAGE, entity.Code);
                var headOfDepartment = userService.GetDirector(Department.KHDT);
                receivers.Add(headOfDepartment.Id);
            }
            else if (action == "approve") // handle approve
            {
                entity.Status = QuoteStatus.Approved;
                notificationMessage = string.Format(APPROVE_MESSAGE, entity.Code);

                var director = userService.GetDirector(Department.BGD);
                receivers.Add(director.Id);
                var quoteUsers = userService.UserInSpecializedDivisions(Department.BGD);
                receivers.AddRange(quoteUsers);


                var chuyenviens = contractUserRepository.Fetch(c => c.ContractId == entity.Id && c.ProcessType == ProcessType.QT02).Select(c=>c.UserId).ToList();
                receivers.AddRange(chuyenviens);
                //var directorate = userService.GetDirector(Department.KT);
                //if (directorate != null) receivers.Add(directorate.Id);
                if (!string.IsNullOrWhiteSpace(entity.Departments))
                {
                    var departments = JsonConvert.DeserializeObject<List<int>>(entity.Departments);
                    if (departments.Count() > 0)
                    {
                        var userRecivers = new List<int>();
                        foreach (var department in departments)
                        {
                            userRecivers.Add(userService.GetDirector((Department)department).Id);
                        }
                        if (entity.DocType == DocumentType.Contract)
                            notificationService.Notify(userRecivers, "Phòng chuyên môn được chỉ định tham gia HĐ: " + entity.Code, NotificationType.AssignPCMBGTBL, entity.Id);
                        else
                            notificationService.Notify(userRecivers, "Phòng chuyên môn được chỉ định tham gia BG: " + entity.Code, NotificationType.AssignPCMBGTBL, entity.Id);
                    }

                }
                
            }
            var result = quoteRepository.Update(entity);
            if (result && !string.IsNullOrEmpty(notificationMessage)) notificationService.Notify(receivers, notificationMessage, NotificationType.PassQuote, entity.Id);

            return result;
        }

        public bool Reject(RejectQuoteModel model)
        {
            var entity = quoteRepository.GetById(model.QuoteId);
            if (entity.Status != QuoteStatus.Submited) return false;

            entity.Status = QuoteStatus.Rejected;
            entity.Note = model.Reason;
            return quoteRepository.Update(entity);
        }

        public bool SaveQuoteRecord(QuoteRecordSaveModel model)
        {
            if (model.FileNames == null || model.FileNames.Count == 0) throw new ServiceException("Vui lòng chọn file biên bản đính kèm");
            var entity = new QuoteRecord();
            var context = quoteRecordRepository.GetDbContext();
           var quoteStaffs = (from q in context.Quotes
                           join p in context.Phases on q.Id equals p.ContractId
                           join ps in context.PhaseStaffs on p.Id equals ps.PhaseId
                           where p.ProcessType == ProcessType.QT02
                              && q.Id == model.QuoteId
                           select ps
            ).ToList();


            //var quoteStaffs = quoteStaffRepository.Fetch(c => c.QuoteId == model.QuoteId.Value);
            if (quoteStaffs.Count() == 0) throw new ServiceException("Phiếu giao nhiệm vụ chưa có cán bộ nào tham gia");
            var quoteStaff = quoteStaffs.FirstOrDefault(c => c.StaffId == model.UserId);
            if (quoteStaff == null) throw new ServiceException("Bạn chưa tham gia phiếu giao nhiệm vụ này");
            if(!quoteStaffs.Any(c=>c.StaffId==model.UserId && c.Position==PCMAssignPotition.Leader))
                throw new ServiceException("Chỉ nhóm trưởng mới được phép lập biên bản");
            //  if (quoteStaff.PositionInPhase!= PhaseStaffPosition.Captain)  throw new ServiceException("Chỉ nhóm trưởng mới được phép lập biên bản");

            if (model.Id.HasValue && model.Id.Value > 0)
            {
                entity = quoteRecordRepository.GetById(model.Id.Value);
            }
            entity.QuoteId = model.QuoteId.Value;
            if (model.FileNames != null && model.FileNames.Count > 0)
                entity.Files = JsonConvert.SerializeObject(model.FileNames);
            entity.Content = model.Content;
            entity.Note = model.Note;

            if (model.Id > 0) return quoteRecordRepository.Update(entity);
            else
            {
                var user = userRepository.GetById(model.UserId);
                entity.DepartmentId = user.DepartmentId.Value;
                entity.Status = RecordStatus.Draft;
                entity.CreatedOn = DateTime.Now;
                entity.CreatedBy = model.UserId;
                return quoteRecordRepository.Insert(entity);
            }
        }

        public QuoteRecordFilterResult FilterQuoteRecords(QuoteRecordFilterModel model)
        {
            var result = new QuoteRecordFilterResult();
            var q = quoteRecordRepository.Fetch(e => e.QuoteId == model.QuoteId);

            result.TotalRecord = q.Count();
            var entities = q.OrderByDescending(c => c.Id).Skip((model.PageIndex - 1) * model.PageSize).Take(model.PageSize).ToList();

            var userIds = entities.Select(u => u.CreatedBy);

            var users = userRepository.Fetch(u => userIds.Contains(u.Id));
            result.Records = entities.Map<List<QuoteRecordModel>>();

            foreach (var record in result.Records)
            {
                var user = users.FirstOrDefault(c => c.Id == record.CreatedBy);
                if (user != null)
                {
                    record.UserName = user.FullName;
                    record.DepartmentName = ((Department)user.DepartmentId.Value).ToDescription();
                }
                if (!string.IsNullOrEmpty(record.Files))
                {
                    record.FileList = JsonConvert.DeserializeObject<List<string>>(record.Files);
                }
            }
            return result;
        }

        public QuoteRecordModel GetQuoteRecords(int recordId)
        {
            var entity = quoteRecordRepository.GetById(recordId);

            var model = entity.Map<QuoteRecordModel>();
            if (!string.IsNullOrEmpty(model.Files))
            {
                model.FileList = JsonConvert.DeserializeObject<List<string>>(model.Files);
            }
            return model;
        }

        public bool SubmitQuoteRecord(int recordId)
        {
            var entity = quoteRecordRepository.GetById(recordId);
            entity.Status = RecordStatus.Submited;

            var user = userRepository.GetById(entity.CreatedBy);
            var quote = quoteRepository.GetById(entity.QuoteId);


            if (quoteRecordRepository.Update(entity))
            {
                string messenger = string.Format("{0} trình duyệt biên bản phiếu giao NV {1} chờ duyệt", user.FullName, quote.Code);

                var receivers = new List<int>() { entity.CreatedBy };
                // Trưởng phòng CM
                var director = userService.GetDirector((Department)user.DepartmentId.Value);
                receivers.Add(director.Id);

                notificationService.Notify(receivers, messenger, NotificationType.SubmitQuoteRecord, recordId);
            }
            return true;
        }

        public bool RejectQuoteRecord(int recordId)
        {
            var entity = quoteRecordRepository.GetById(recordId);
            if (entity.Status == RecordStatus.Draft) throw new ServiceException("Biên bản nháp không thể trả lại");
            if (entity.Status == RecordStatus.Approved) throw new ServiceException("Biên bản đã được duyệt không thể từ chối");
            entity.Status = RecordStatus.Rejected;

            var user = userRepository.GetById(entity.CreatedBy);

            var quote = quoteRepository.GetById(entity.QuoteId);

            if (quoteRecordRepository.Update(entity))
            {
                string messenger = string.Format("Biên bản phiếu giao NV {0} bị trả lại", quote.Code);

                var receivers = new List<int>() { entity.CreatedBy };
                // Trưởng phòng CM
                var director = userService.GetDirector((Department)user.DepartmentId.Value);
                receivers.Add(director.Id);

                notificationService.Notify(receivers, messenger, NotificationType.RejectQuoteRecord, recordId);
            }
            return true;
        }

        public bool ApproveQuoteRecord(int recordId)
        {
            var entity = quoteRecordRepository.GetById(recordId);
            if (entity.Status == RecordStatus.Rejected) throw new ServiceException("Biên bản chưa được duyệt");
            if (entity.Status == RecordStatus.Draft) throw new ServiceException("Biên bản nháp không thể duyệt");
            entity.Status = RecordStatus.Approved;

            var user = userRepository.GetById(entity.CreatedBy);

            var quote = quoteRepository.GetById(entity.QuoteId);

            if (quoteRecordRepository.Update(entity))
            {
                string messenger = string.Format("Biên bản phiếu giao NV {0} đã được duyệt", quote.Code);

                var receivers = new List<int>() { entity.CreatedBy };
                // Trưởng phòng CM
                var director = userService.GetDirector((Department)user.DepartmentId.Value);
                receivers.Add(director.Id);

                //Trưởng phòng kt
                var techDirector = userService.GetDirector(Department.KT);
                receivers.Add(techDirector.Id);

                notificationService.Notify(receivers, messenger, NotificationType.ApproveQuoteRecord, recordId);
            }
            return true;
        }

        public bool DeleteRecord(int recordId, int userId)
        {
            var record = quoteRecordRepository.GetById(recordId);
            if (record.CreatedBy != userId) throw new ServiceException("Bạn không phải là người tạo biên bản thí nghiệm này");
            if (record.Status == RecordStatus.Approved) throw new ServiceException("Biên bản đã được duyệt");
            if (record.Status == RecordStatus.Submited) throw new ServiceException("Biên bản đã trình lên");

            return quoteRecordRepository.Delete(record);
        }

        public bool UpdateNewBgToBGTBL()
        {
            var listContainBgs = quoteRepository.Fetch(c => c.DocType == DocumentType.Contract && c.Code.Contains("BG"));

            listContainBgs.ForEach(c =>
            {
                c.DocType = DocumentType.Quotation;
                var update = quoteRepository.Update(c);
            });

            return true;
        }

        public bool ConvertDepartment()
        {
            var quotes = quoteRepository.Fetch();
            foreach (var quote in quotes)
            {
                if (!string.IsNullOrEmpty(quote.Departments))
                {
                    int x = 0;
                    try
                    {
                        int.TryParse(quote.Departments.ToString(), out x);
                        if (x != 0)
                        {
                            var addLists = new List<int>();
                            addLists.Add(x);
                            quote.Departments = JsonConvert.SerializeObject(addLists);
                            quoteRepository.Update(quote);
                        }
                    }
                    catch (Exception ex)
                    {

                        throw ex;
                    }

                    //var departments = JsonConvert.DeserializeObject<List<int>>(quote.Departments);

                }
            }
            return true;
        }

        public List<Document> AttachDocuments(int id, List<FileInfoModel> files)
        {
            var documents = new List<Document>();
            foreach (var file in files)
            {
                var document = documentService.AddDocument(file);
                documents.Add(document);
            }
            if (documents.Count > 0)
            {
                var entity = quoteRepository.GetById(id);
                var docs = new List<Guid>();
                if (!string.IsNullOrEmpty(entity.Attachments))
                {
                    docs = JsonConvert.DeserializeObject<List<Guid>>(entity.Attachments);
                }
                docs.AddRange(documents.Select(d => d.Id));
                entity.Attachments = JsonConvert.SerializeObject(docs);
                quoteRepository.Update(entity);
            }
            return documents;
        }
    }
}
