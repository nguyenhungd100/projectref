﻿using NPC.PMS.Domain.Data.Entity;
using NPC.PMS.Domain.Models;
using NPC.PMS.Domain.Models.Documents;
using NPC.PMS.Domain.Models.Quotes;
using System;
using System.Collections.Generic;

namespace NPC.PMS.Domain.Services.Contracts
{
    public interface IQuoteService
    {
        QuoteFilterResult Filter(QuoteFilterModel filterModel);

        QuoteModel Save(QuoteFormModel model);

        bool DeleteQuote(int id);

        bool AddQuoteStaff(AddQuoteStaffModel model, int userCurrentId);

        bool SetLeaderQuote(AddQuoteStaffModel model);

        bool DeleteQuoteStaff(DeleteQuoteStaffModel model);

        IEnumerable<QuoteStaffModel> GetStaffByQuoteId(int quoteId);

        List<QuoteWorkitemModel> ParseQuoteWorkitemFile(int id, byte[] bytes);

        List<QuoteWorkitemModel> GetQuoteWorkitems(Guid uid);

        bool ClearQuoteWorkitem(int id);

        QuoteModel GetQuote(int quoteId);

        string NextCode(DateTime signingDate, bool isContract);

        QuoteModel GetQuoteByUid(Guid uid);

        bool Next(int id, string action);

        bool Reject(RejectQuoteModel model);

        bool SaveQuoteRecord(QuoteRecordSaveModel model);

        QuoteRecordFilterResult FilterQuoteRecords(QuoteRecordFilterModel model);

        QuoteRecordModel GetQuoteRecords(int recordId);

        bool SubmitQuoteRecord(int recordId);

        bool RejectQuoteRecord(int recordId);

        bool ApproveQuoteRecord(int recordId);

        bool DeleteRecord(int recordId, int userId);
        bool UpdateNewBgToBGTBL();
        bool ConvertDepartment();

        List<Document> AttachDocuments(int id, List<FileInfoModel> files);
    }
}