﻿using System;
using System.Collections.Generic;
using System.Text;
using NPC.PMS.Domain.Data.Entity;
using NPC.PMS.Domain.Models.ContractA;
using NPC.PMS.Domain.Models.ContractB;
using NPC.PMS.Domain.Models.Documents;

namespace NPC.PMS.Domain.Services.Contracts
{
    public interface IContractAService
    {
        List<ContractaModel> Filter(ContractaFilterModel filter);
        ContractAFilterResult FilterContractA(ContractaFilterModel filter);
        ContractAFilterResult FilterContractAB(ContractaFilterModel filter);
        ContractaModel Save(ContractaSaveModel model);

        ContractaModel Get(Guid uid, int userId);
        bool Delete(Guid id);
        string NextCode(DateTime dateTime);

        IEnumerable<ContractAGroupModel> GetGroup();
        ContractAGroupModel SaveContractAGroup(ContractAGroupModel groupModel);
        bool DeleteContractAGroup(int groupId);
        IEnumerable<FormalitySelectDefined> GetFormalitySelectDefined();
        IEnumerable<ProfileListContractADefinedModel> GetProfileListContractADefined(int formalityCode);
        bool DeleteProfileListContractA(DeleteProfileContractA deleteProfile);
        bool SaveProfileList(IEnumerable<ProfileListContractAModel> profileListContractAs, Guid uid);
        bool SumitProfileListToP2Manager(Guid uid);
        bool Assign(AssignModel assignModel);
        bool Unassign(int id);



        #region P2, P5 and contract
        ContractaModel SubmitToP2Director(Guid uid, int userId);

        ContractaModel RejectContractAFromP2Director(Guid uid, int userId);

        ContractaModel P2SendContractP5(Guid uid, int userId);

        ContractaModel SubmitToP5Director(Guid uid, int userId);

        ContractaModel RejectFromP5Director(Guid uid, int userId);

        (ContractaModel, string) P5SendContractP2(Guid uid, int userId);
        ContractaModel P2DoneCompleted(Guid uid, int userId);
        List<Document> AttachDocuments(int id, List<FileInfoModel> files);
        #endregion

    }
}
