﻿using NPC.PMS.Domain.Models.Reports;
using NPC.PMS.Domain.Models.Reports.ReportDebt;
using NPC.PMS.Domain.Models.Reports.ReportExistSettlement;
using NPC.PMS.Domain.Models.Reports.ReportRevenue;
using NPC.PMS.Domain.Models.Reports.Settlement;
using System;
using System.Collections.Generic;
using System.IO;

namespace NPC.PMS.Domain.Services.ReportServices
{
    public interface IContractReportService
    {
        List<SyntheticReportModel> GetSyntheticReport(SyntheticReportFilter filter);
        List<DepartmentOutputReportModel> GetDepartmentOutput(DepartmentOutputFilter departmentOutputFilter);

        MemoryStream ExportReportSyntheticToExcel();

        StatisticReportOneDayModel StatisticDepartmentReportOneDay(YieldDepartmentFilter filter);

        ReportExistSettlementModel StatisticExistSettlementTest(DateTime check);

        ReportExistSettlementModel StatisticExistSettlement(DateTime check);




        List<RevenueReportModel> StatisticRevenue(CheckTimeModel filter);

        //ReportDebtModel StatisticDebt(CheckTimeModel check);
        List<ReportDebtModel> StatisticDebt(CheckTimeModel filter);
        List<SettlementP2ReportModel> GetSettlementP2Report(SettlementP2ReportFilter filter);

    }
}