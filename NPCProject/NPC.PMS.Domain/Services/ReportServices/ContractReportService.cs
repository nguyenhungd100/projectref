﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using NPC.PMS.Domain.Data;
using NPC.PMS.Domain.Data.Entity;
using NPC.PMS.Domain.Enums;
using NPC.PMS.Domain.Models.Reports;
using NPC.PMS.Domain.Models.Reports.ReportDebt;
using NPC.PMS.Domain.Models.Reports.ReportExistSettlement;
using NPC.PMS.Domain.Models.Reports.ReportRevenue;
using NPC.PMS.Framework.Extensions.Enums;
using OfficeOpenXml;
using NPC.PMS.Domain.Models.Reports.Settlement;

namespace NPC.PMS.Domain.Services.ReportServices
{
    public class ContractReportService : IContractReportService
    {
        private readonly IContractBService contractService;
        private readonly IRepository<Phase> phaseRepository;
        private readonly IRepository<PhaseWorkitem> phaseWorkitemRepository;
        private readonly IRepository<Workitem> workitemRepository;
        private readonly IRepository<ExperimentRecord> experimentRecordRepository;
        private readonly IRepository<Settlement> settlementRepository;
        private readonly IRepository<Report> reportRepository;
        private readonly IRepository<Reportitem> reportitemRepository;
        private readonly IRepository<User> userRepository;
        private readonly IRepository<Contract> contractRepository;
        private readonly IRepository<Customer> custormerRepository;
        private readonly IRepository<Contract> contractBRepository;
        private readonly IRepository<Quote> quoteRepository;
        private readonly IRepository<SettlementCheck> settlementCheckRepository;
        private readonly IRepository<SettlementDocument> settlementDocumentRepository;

        public ContractReportService(IContractBService contractService,
            IRepository<Phase> phaseRepository,
            IRepository<PhaseWorkitem> phaseWorkitemRepository,
            IRepository<Workitem> workitemRepository,
            IRepository<ExperimentRecord> experimentRecordRepository,
            IRepository<Settlement> settlementRepository,
            IRepository<Report> reportRepository,
            IRepository<Reportitem> reportitemRepository,
            IRepository<User> userRepository,
            IRepository<Contract> contractRepository,
            IRepository<Customer> custormerRepository,
            IRepository<Contract> contractBRepository,
            IRepository<Quote> quoteRepository,
            IRepository<SettlementCheck> settlementCheckRepository,
            IRepository<SettlementDocument> settlementDocumentRepository)
        {
            this.contractService = contractService;
            this.phaseRepository = phaseRepository;
            this.phaseWorkitemRepository = phaseWorkitemRepository;
            this.workitemRepository = workitemRepository;
            this.experimentRecordRepository = experimentRecordRepository;
            this.settlementRepository = settlementRepository;
            this.reportRepository = reportRepository;
            this.reportitemRepository = reportitemRepository;
            this.userRepository = userRepository;
            this.contractRepository = contractRepository;
            this.custormerRepository = custormerRepository;
            this.contractBRepository = contractBRepository;
            this.quoteRepository = quoteRepository;
            this.settlementCheckRepository = settlementCheckRepository;
            this.settlementDocumentRepository = settlementDocumentRepository;
        }
        public List<SyntheticReportModel> GetSyntheticReport(SyntheticReportFilter filter)
        {
            var contracts = contractService.Filter(new Models.ContractA.ContractFilterModel() { PageSize = 1000, UserId = filter.UserId, HasViewAll = true }).Records;
            var models = contracts.Map<List<SyntheticReportModel>>();
            var contractIds = models.Select(m => m.Id);
            var contractUIDs = models.Select(m => m.UID);
            var phases = phaseRepository.Fetch(p => contractIds.Contains(p.ContractId));
            var phaseIds = phases.Select(p => p.Id);
            var phaseWorkitems = phaseWorkitemRepository.Fetch(p => phaseIds.Contains(p.PhaseId));
            var experimentRecords = experimentRecordRepository.Fetch(e => phaseIds.Contains(e.PhaseId) && e.Status == Enums.RecordStatus.Approved);

            var settlements = settlementRepository.Fetch(s => contractUIDs.Contains(s.ContractUID) && s.Status == SettlemenStatus.Completed);

            foreach (var model in models)
            {
                var contractSettlements = settlements.Where(s => s.ContractUID == model.UID).ToList();
                model.LastYearSettlementValue = 0;
                var lastYearSettlements = contractSettlements.Where(cs => cs.ReceivedTime.HasValue && cs.ReceivedTime.Value.Year - DateTime.Now.Year >= 1);
                if (lastYearSettlements.Any())
                {
                    model.LastYearSettlementValue = lastYearSettlements.Sum(s => s.Value);
                }

                model.MonthlySettlementValues = new List<decimal> { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
                var thisYearSettlements = contractSettlements.Where(cs => cs.ReceivedTime.HasValue && cs.ReceivedTime.Value.Year - DateTime.Now.Year == 0);
                if (thisYearSettlements.Any())
                {
                    for (int i = 0; i < model.MonthlySettlementValues.Count; i++)
                    {
                        model.MonthlySettlementValues[i] = thisYearSettlements.Where(s => s.ReceivedTime.Value.Month == (i + 1)).Sum(s => s.Value);
                    }
                }

                model.ThisYearSettlementValue = model.LastYearSettlementValue + model.MonthlySettlementValues.Sum(p => p);

                model.NextYearSettlementValue = model.ContractValue - model.ThisYearSettlementValue;

                var contractPhases = phases.Where(p => p.ContractId == model.Id);
                var contractPhaseIds = contractPhases.Select(p => p.Id);
                var contractPhaseWorkitems = phaseWorkitems.Where(p => contractPhaseIds.Contains(p.PhaseId));

                var workitemQuantity = workitemRepository.Fetch(w => w.ContractId == model.Id).Sum(p => p.Quantity);
                var completed = contractPhaseWorkitems.Sum(cpw => cpw.CompletedQuantity);
                if (workitemQuantity > 0) model.Percentage = completed / workitemQuantity;

                model.PaperCount = experimentRecords.Count(e => contractPhaseIds.Contains(e.PhaseId));
            }
            return models;
        }
        public List<DepartmentOutputReportModel> GetDepartmentOutput(DepartmentOutputFilter filter)
        {
            if (filter == null) filter = new DepartmentOutputFilter();
            var result = new List<DepartmentOutputReportModel>();


            if (!filter.StartDate.HasValue || !filter.EndDate.HasValue)
            {
                filter.StartDate = new DateTime(DateTime.Today.Year, DateTime.Today.Month, 1);
                filter.EndDate = new DateTime(DateTime.Today.Year, DateTime.Today.Month, DateTime.DaysInMonth(DateTime.Today.Year, DateTime.Today.Month));
            }


            using (var db = reportRepository.GetDbContext())
            {
                //var q = db.Reports.AsQueryable();

                //filter.StartDate = filter.StartDate.Value.Date;
                //filter.EndDate = filter.EndDate.Value.Date.AddDays(1);
                //q = q.Where(r => filter.StartDate <= r.Time && r.Time <= filter.EndDate && r.Status == ReportStatus.Approved);

                //if (filter.DepartmentId.HasValue)
                //{
                //    q = q.Where(r => r.UnitId == filter.DepartmentId.Value);
                //}

                //var reports = q.ToList();
                //var reportIds = reports.Select(r => r.Id);

                //var items = db.Reportitems.Where(ri => reportIds.Contains(ri.ReportId)).ToList();
                //var workitemIds = items.Select(ri => ri.WorkitemId);
                //var workitems = db.Workitems.Where(w => workitemIds.Contains(w.Id));

                ////var userIds = reports.Select(r=>r.UserId);
                ////var users = db.Users.Where(u => userIds.Contains(u.Id));

                //foreach (var item in items)
                //{
                //    var report = reports.FirstOrDefault(r => r.Id == item.ReportId);
                //    //var user = users.FirstOrDefault(r => r.Id == report.UserId);
                //    var workitem = workitems.FirstOrDefault(r => r.Id == item.WorkitemId);
                //    var price = workitem.Price ?? 0;
                //    result.Add(new DepartmentOutputReportModel
                //    {
                //        DepartmentId = (Department)report.UnitId,
                //        ReportId = report.Id,
                //        ReportTime = report.Time,
                //        WorkitemId = item.WorkitemId,
                //        WorkitemTitle = workitem.Title,
                //        Price = price,
                //        Quantity = item.Quantity,
                //        Money = item.Quantity * price
                //    });
                //}

                //var resultNews = result.GroupBy(c => new { c.DepartmentId, c.ReportTime.Date, c.WorkitemId })
                //                        .Select(sl => new DepartmentOutputReportModel
                //                        {
                //                            DepartmentId = sl.FirstOrDefault().DepartmentId,
                //                            WorkitemId = sl.FirstOrDefault().WorkitemId,
                //                            WorkitemTitle = sl.FirstOrDefault().WorkitemTitle,
                //                            Price = sl.FirstOrDefault().Price,
                //                            Quantity = sl.Sum(c => c.Quantity),
                //                            Money = sl.Sum(c => c.Quantity) * sl.Select(c => c.Price).FirstOrDefault()

                //                        }).ToList()/*.ToList<DepartmentOutputReportModel>();*/;



                var res = (from r in db.Reports.Where(c => c.Time >= filter.StartDate && c.Time <= filter.EndDate && c.Status == ReportStatus.Approved)
                           join rt in db.Reportitems on r.Id equals rt.ReportId
                           join w in db.Workitems on rt.WorkitemId equals w.Id
                           join c in db.Contracts on w.ContractId equals c.Id
                           join g in db.ContractGroups on c.GroupId equals g.Id
                           where r.UnitId == filter.DepartmentId || filter.DepartmentId == null
                           select new
                           {
                               DepartmentId = r.UnitId,
                               ReportId = r.Id,
                               WorkItemId = w.Id,
                               WorkItemTitle = w.Title,
                               Price = w.Price,
                               Quantity = rt.Quantity,
                               ContractId = c.Id,
                               ReportTime = r.Time,
                               ContractCode = c.CustomerContractCode,
                               Group = g.Name,
                               ContractContent = c.Content
                           }).ToList();

                var re = res.GroupBy(c => new
                {
                    c.Group,
                    c.ContractCode,
                    c.ContractId,
                    c.DepartmentId,
                    c.WorkItemId,
                    c.WorkItemTitle,
                    c.ContractContent,
                    c.Price,
                    ReportTime = c.ReportTime.Date
                }).Select(sl => new DepartmentOutputReportModel
                {
                    Group = sl.Key.Group,
                    ContractCode = sl.Key.ContractCode,
                    DepartmentId = (Department)sl.Key.DepartmentId,
                    WorkitemId = sl.Key.WorkItemId,
                    WorkitemTitle = sl.Key.WorkItemTitle,
                    ReportTime = sl.Key.ReportTime,
                    ContractContent = sl.Key.ContractContent,
                    Price = sl.Key.Price ?? 0,
                    Quantity = sl.Sum(c => c.Quantity),

                    Money = sl.Sum(c => c.Quantity) * (sl.Key.Price ?? 0)

                }).ToList();
                return re.OrderBy(c => c.Group).ThenBy(c => c.ContractCode).ThenBy(c => c.DepartmentId).ToList();
            }



            //return result.OrderBy(r => r.DepartmentId).ToList();
        }

        public MemoryStream ExportReportSyntheticToExcel()
        {
            throw new NotImplementedException();
        }



        public StatisticReportOneDayModel StatisticDepartmentReportOneDay(YieldDepartmentFilter filter)
        {
            var result = new StatisticReportOneDayModel();
            var dateCheck = filter.Date.Value;

            var contract = contractRepository.FirstOrDefault(c => c.UID == filter.UID);
            if (contract == null) throw new ServiceException("Hợp đồng không tồn tại");
            var workitems = workitemRepository.Fetch(c => c.ContractId == contract.Id);
            var workitemCheckConditions = new List<Workitem>();

            if (workitems.Count() > 0)
            {
                var workitemIds = workitems.Select(c => c.Id);

                var phaseWorkitems = phaseWorkitemRepository.Fetch(c => workitemIds.Contains(c.WorkitemId));

                ///var listDepartments = Enum.GetValues(typeof(Department)).Cast<Department>().Selectc);

                var departmentAllPhases = new List<int> { 11, 12, 13, 14, 15, 16, 17 };





                var reportitems = reportitemRepository.Fetch(c => workitemIds.Contains(c.WorkitemId));

                if (reportitems.Count() > 0)
                {
                    var reportCheckIds = reportitems.Select(c => c.ReportId).Distinct();
                    var reports = reportRepository.Fetch(c => reportCheckIds.Contains(c.Id));

                    if (reports.Count() > 0)
                    {
                        var reportInDays = reports.Where(c => c.Time.Day == dateCheck.Day && c.Time.Month == dateCheck.Month
                            && c.Time.Year == dateCheck.Year);

                        if (reportInDays.Count() > 0)
                        {

                            var userIds = reportInDays.Select(c => c.UserId).Distinct();
                            var users = userRepository.Fetch(c => userIds.Contains(c.Id));

                            var reportInDayIds = reportInDays.Select(c => c.Id);


                            var reportitemInDays = reportitems.Where(c => reportInDayIds.Contains(c.ReportId));
                            if (reportitemInDays.Count() > 0)
                            {
                                var workitemCheckIds = reportitemInDays.Select(c => c.WorkitemId).ToList();
                                workitemCheckConditions = workitems.Where(c => workitemCheckIds.Contains(c.Id)).ToList();

                                foreach (var workitem in workitemCheckConditions)
                                {
                                    var workitemAdd = new YieldDepartmentOneDayModel
                                    {
                                        WorkitemId = workitem.Id,
                                        WorkitemTitle = workitem.Title
                                    };
                                    if (workitem.Quantity.HasValue && workitem.Price.HasValue)
                                        workitemAdd.CompletedNumber = 0;
                                    foreach (var department in departmentAllPhases)
                                    {
                                        var yieldDepartment = new YielDepartmentOneDayDetail
                                        {
                                            DepartmentId = (Department)department,
                                            DepartmentName = ((Department)department).ToDescription()
                                        };
                                        workitemAdd.YielDepartmentOneDayDetails.Add(yieldDepartment);
                                    }
                                    result.YieldDepartmentOneDays.Add(workitemAdd);
                                }

                                var workitemChecks = workitems.Where(c => workitemCheckIds.Contains(c.Id));
                                foreach (var workitem in workitemChecks)
                                {
                                    if (workitemCheckIds.Contains(workitem.Id))
                                    {
                                        var modify = result.YieldDepartmentOneDays.FirstOrDefault(c => c.WorkitemId == workitem.Id);
                                        modify.CompletedNumber = reportitemInDays.Where(c => c.WorkitemId == workitem.Id).Sum(c => c.Quantity);

                                        var reportitemForWorkitems = reportitemInDays.Where(c => c.WorkitemId == workitem.Id);

                                        var reportForWorkitems = new List<Report>();
                                        if (reportitemForWorkitems.Count() > 0)
                                        {
                                            var reportitemForWorkitemIds = reportitemForWorkitems.Select(c => c.ReportId).Distinct();
                                            reportForWorkitems = reportInDays.Where(c => reportitemForWorkitemIds.Contains(c.Id)).ToList();
                                        }


                                        foreach (var department in departmentAllPhases)
                                        {

                                            var workitemResult = result.YieldDepartmentOneDays.FirstOrDefault(c => c.WorkitemId == workitem.Id);
                                            var YieldDepartmentUpdates = workitemResult.YielDepartmentOneDayDetails.FirstOrDefault(c => c.DepartmentId == (Department)department);


                                            if (reportForWorkitems.Count() > 0)
                                            {
                                                var reportUserIds = reportForWorkitems.Select(c => c.UserId);

                                                if (reportUserIds.Count() > 0)
                                                {
                                                    var reportUserFilterDepartments = users.Where(c => c.DepartmentId == (int)department && reportUserIds.Contains(c.Id));
                                                    if (reportUserFilterDepartments.Count() > 0)
                                                    {
                                                        var reportUserDepartmentIds = reportUserFilterDepartments.Select(c => c.Id);
                                                        var reportFilterDepartments = reportForWorkitems.Where(c => reportUserDepartmentIds.Contains(c.UserId));

                                                        if (reportFilterDepartments.Count() > 0)
                                                        {
                                                            var reportFilterDepartmentIds = reportFilterDepartments.Select(c => c.Id);
                                                            var reportitemFilterDepartments = reportitemForWorkitems.Where(c => reportFilterDepartmentIds.Contains(c.ReportId));
                                                            YieldDepartmentUpdates.CompletedNumber = reportitemFilterDepartments.Sum(c => c.Quantity);

                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                            foreach (var department in departmentAllPhases)
                            {
                                StringBuilder str = new StringBuilder();
                                str.Append("- " + ((Department)department).ToDescription() + ": ");
                                foreach (var report in reportInDays)
                                {
                                    var reportUserId = report.UserId;
                                    var user = users.FirstOrDefault(c => c.Id == reportUserId);

                                    if (user.DepartmentId == department)
                                        str.Append(report.Content + "( " + user.FullName + ").");

                                }
                                result.SyntheticNotes.Add(str.ToString());
                            }
                        }
                    }
                }
            }

            //result.YieldDepartmentOneDays = result.YieldDepartmentOneDays.Where(c => workitemCheckConditions.Contains(c.WorkitemId)).ToList();
            //if (workitemCheckConditions.Count() > 0)
            //{
            return result;
            //}
            //else
            //    return null;

        }

        public ReportExistSettlementModel StatisticExistSettlementTest(DateTime check)
        {
            var result = new ReportExistSettlementModel();

            //var custormers = custormerRepository.Fetch();

            //var externalEVNs = new List<ExternalEVNSettlement>();
            //var missingexternal = new List<string>();
            //missingexternal.Add("Hồ sơ ngoài EVN 1");
            //missingexternal.Add("Hồ sơ ngoài EVN 2");
            //missingexternal.Add("Hồ sơ ngoài EVN 3");
            //externalEVNs.Add(new ExternalEVNSettlement
            //{
            //    ContractId = 11,
            //    UID = Guid.NewGuid(),
            //    SigningDate = DateTime.Now,
            //    MissingRecords = missingexternal,
            //    MissingRecordCollects = string.Join(", ", missingexternal),
            //    Note = "ngoài EVN"

            //});
            //result.ExternalEVNs.AddRange(externalEVNs);




            //var internalEVNs = new InternalEVNSettlement();
            //var internalEVNNPCs = new List<InternalEVNNPCSettlement>();
            //internalEVNNPCs.Add(new InternalEVNNPCSettlement
            //{
            //    ContractId = 1,
            //    UID = Guid.NewGuid(),
            //    SigningDate = DateTime.Now,
            //    MissingRecords = new List<string> { "hồ sơ 11", "hồ sơ 12" },
            //    MissingRecordCollects = string.Join(", ", new List<string> { "hồ sơ 11", "hồ sơ 12" }),
            //    Note = "trong EVN NPC 1"
            //});

            //internalEVNNPCs.Add(new InternalEVNNPCSettlement
            //{
            //    ContractId = 2,
            //    UID = Guid.NewGuid(),
            //    SigningDate = DateTime.Now,
            //    MissingRecords = new List<string> { "hồ sơ 22", "hồ sơ 23", "hồ sơ 24", "hồ sơ 25" },
            //    MissingRecordCollects = string.Join(", ", new List<string> { "hồ sơ 22", "hồ sơ 23", "hồ sơ 24", "hồ sơ 25" }),
            //    Note = "trong EVN NPC 2"
            //});

            //internalEVNNPCs.Add(new InternalEVNNPCSettlement
            //{
            //    ContractId = 3,
            //    UID = Guid.NewGuid(),
            //    SigningDate = DateTime.Now,
            //    MissingRecords = new List<string> { "hồ sơ 33", "hồ sơ 34" },
            //    MissingRecordCollects = string.Join(", ", new List<string> { "hồ sơ 33", "hồ sơ 34" }),
            //    Note = "trong EVN NPC 3"
            //});

            //var externalEVNNPCs = new List<ExternalEVNNPCSettlement>();
            //externalEVNNPCs.Add(new ExternalEVNNPCSettlement
            //{
            //    ContractId = 4,
            //    UID = Guid.NewGuid(),
            //    SigningDate = DateTime.Now,
            //    MissingRecords = new List<string> { "hồ sơ 44", "hồ sơ 45" },
            //    MissingRecordCollects = string.Join(", ", new List<string> { "hồ sơ 44", "hồ sơ 45" }),
            //    Note = "trong EVN NPC 1"
            //});

            //externalEVNNPCs.Add(new ExternalEVNNPCSettlement
            //{
            //    ContractId = 5,
            //    UID = Guid.NewGuid(),
            //    SigningDate = DateTime.Now,
            //    MissingRecords = new List<string> { "hồ sơ 55", "hồ sơ 56", "hồ sơ 57" },
            //    MissingRecordCollects = string.Join(", ", new List<string> { "hồ sơ 55", "hồ sơ 56", "hồ sơ 57" }),
            //    Note = "ngoài EVN NPC 2"
            //});

            //internalEVNs.InternalEVNNPCs.AddRange(internalEVNNPCs);
            //internalEVNs.ExternalEVNNPCs.AddRange(externalEVNNPCs);
            //result.InternalEVNs = internalEVNs;


            return result;

        }


        public ReportExistSettlementModel StatisticExistSettlement(DateTime check)
        {
            var result = new ReportExistSettlementModel();

            var custormers = custormerRepository.Fetch(c => c.IsExternalEVN.HasValue);

            var externalEVNs = custormers.Where(c => c.IsExternalEVN == true);
            var externalEVNIds = externalEVNs.Select(c => c.Id);

            var externalEVNNPCs = custormers.Where(c => c.IsExternalEVN == false && c.IsExternalEVNNPC == true);
            var externalEVNNPCIds = externalEVNNPCs.Select(c => c.Id);

            var internalEVNNPCs = custormers.Where(c => c.IsExternalEVN == false && c.IsExternalEVNNPC == false);
            var internalEVNNPCIds = internalEVNNPCs.Select(c => c.Id);

            var contracCheckUIDs = new List<Guid>();
            var contractBs = contractBRepository.Fetch(c => c.CustomerId.HasValue && (externalEVNIds.Contains(c.CustomerId.Value) || externalEVNNPCIds.Contains(c.CustomerId.Value)
                || internalEVNNPCIds.Contains(c.CustomerId.Value)));
            if (contractBs.Count() > 0)
            {
                var contractBUIDs = contractBs.Select(c => c.UID);
                contracCheckUIDs.AddRange(contractBUIDs);
            }

            var quotes = quoteRepository.Fetch(c => c.CustomerId.HasValue && (externalEVNIds.Contains(c.CustomerId.Value) || externalEVNNPCIds.Contains(c.CustomerId.Value)
                || internalEVNNPCIds.Contains(c.CustomerId.Value)));
            if (quotes.Count() > 0)
            {
                var quoteUIDs = quotes.Select(c => c.UID);
                contracCheckUIDs.AddRange(quoteUIDs);
            }

            var settlements = settlementRepository.Fetch(c => contracCheckUIDs.Contains(c.ContractUID));
            var settlementChecks = new List<SettlementCheck>();
            var settlementDocuments = new List<SettlementDocument>();
            if (settlements.Count() > 0)
            {
                var settlementIds = settlements.Select(c => c.Id);
                settlementChecks = settlementCheckRepository.Fetch(c => settlementIds.Contains(c.Id));
                var checkDocumentIds = settlementChecks.Select(c => c.SettlementDocumentId);
                settlementDocuments = settlementDocumentRepository.Fetch(c => checkDocumentIds.Contains(c.Id));
            }



            void FillData(Customer customer, int type)
            {
                var addExternalEVN = new ExternalEVNSettlement
                {
                    CustomerId = customer.Id,
                    CustomerName = customer.Name
                };

                var addExternalEVNNPC = new ExternalEVNNPCSettlement
                {
                    CustomerId = customer.Id,
                    CustomerName = customer.Name
                };

                var addInternalEVNNPC = new InternalEVNNPCSettlement
                {
                    CustomerId = customer.Id,
                    CustomerName = customer.Name
                };


                var contractBIns = contractBs.Where(c => c.CustomerId == customer.Id);
                var contractBInUIDs = contractBIns.Select(c => c.UID);

                var quoteIns = quotes.Where(c => c.CustomerId == customer.Id);
                var quoteInUIDs = quoteIns.Select(c => c.UID);

                var settlementByCustormers = settlements.Where(c => contractBInUIDs.Contains(c.ContractUID) || quoteInUIDs.Contains(c.ContractUID));

                if (settlementByCustormers.Count() > 0)
                {
                    var settlementByCustormerIds = settlementByCustormers.Select(c => c.Id);
                    var settlementCheckByCustomers = settlementChecks.Where(c => settlementByCustormerIds.Contains(c.SettlementId));

                    if (settlementCheckByCustomers.Any(c => c.Checked == false))
                    {
                        foreach (var item in contractBIns)
                        {
                            var content = new ContentContractSettlementModel
                            {
                                CustomerId = customer.Id,
                                CustomerName = customer.Name,
                                ContractId = item.Id,
                                UID = item.UID,
                                ContractCode = item.Code,
                                ContractContent = item.Content
                            };

                            var settlementIns = settlementByCustormers.Where(c => c.ContractUID == item.UID);

                            if (settlementIns.Count() > 0)
                            {
                                var settlementInIds = settlementIns.Select(c => c.Id);
                                var settlementCheckIns = settlementCheckByCustomers.Where(c => settlementInIds.Contains(c.SettlementId));
                                if (settlementCheckIns.Count() > 0)
                                {
                                    var documentIds = settlementCheckIns.Select(c => c.SettlementDocumentId);
                                    var settlementDocumentIns = settlementDocuments.Where(c => documentIds.Contains(c.Id));

                                    foreach (var settlementCheckIn in settlementCheckIns)
                                    {
                                        if (!settlementCheckIn.Checked)
                                        {
                                            var settlement = settlementIns.FirstOrDefault(c => c.Id == settlementCheckIn.SettlementId);
                                            var documentIn = settlementDocumentIns.FirstOrDefault(c => c.Id == settlementCheckIn.SettlementDocumentId);
                                            content.MissingRecords.Add(documentIn.DocumentName + " Lần TT " + settlement.No);
                                        }
                                    }
                                    if (type == 1)
                                    {
                                        addExternalEVN.Contents.Add(content);
                                    }
                                    if (type == 2)
                                    {
                                        addExternalEVNNPC.Contents.Add(content);
                                    }
                                    if (type == 3)
                                    {
                                        addInternalEVNNPC.Contents.Add(content);
                                    }

                                }
                            }
                        }
                        if (type == 1)
                        {
                            result.ExternalEVNs.Add(addExternalEVN);
                        }
                        if (type == 2)
                        {
                            result.InternalEVNs.ExternalEVNNPCs.Add(addExternalEVNNPC);
                        }
                        if (type == 3)
                        {
                            result.InternalEVNs.InternalEVNNPCs.Add(addInternalEVNNPC);
                        }
                    }
                }
            }

            foreach (var customer in externalEVNs)
            {
                FillData(customer, 1);
            }

            foreach (var customer in externalEVNNPCs)
            {
                FillData(customer, 2);
            }


            foreach (var customer in internalEVNNPCs)
            {
                FillData(customer, 3);
            }

            return result;
        }


        public List<RevenueReportModel> StatisticRevenue(CheckTimeModel filter)
        {
            var result = new List<RevenueReportModel>();
            using (var context = custormerRepository.GetDbContext())
            {
                //get cong ty co hop dong
                var res = (from s in context.Settlements
                           join c in context.Contracts on s.ContractUID equals c.UID
                           join cu in context.Customers on c.CustomerId equals cu.Id
                           where s.BillDate >= filter.StartDate && s.BillDate <= filter.EndDate

                           select new RevenueReportModel
                           {
                               Content = c.Content,
                               ContractCode = c.Code,
                               ContractDate = c.SigningDate,
                               CustomerName = cu.Name,
                               PaymentDate = s.PaymentDate,
                               PaymentNumber = s.PaymentNumber,
                               InvoiceCode = s.BillCode,
                               InvoiceDate = s.BillDate,
                               Note = s.Note,
                               Revenue = s.BillValue,
                               Vat = s.VatValue,
                               Value = s.Value,
                               CompanyType = cu.IsExternalEVN == true ? CompanyType.ExternalEVN : (cu.IsExternalEVNNPC == true ? CompanyType.ExternalEVNPC : CompanyType.InternalEVNPC)


                           }
                         ).Union(

                    from s in context.Settlements
                    join c in context.Quotes on s.ContractUID equals c.UID
                    join cu in context.Customers on c.CustomerId equals cu.Id
                    where s.BillDate >= filter.StartDate && s.BillDate <= filter.EndDate

                    select new RevenueReportModel
                    {
                        Content = c.Content,
                        ContractCode = c.Code,
                        ContractDate = c.SigningDate,
                        CustomerName = cu.Name,
                        PaymentDate = s.PaymentDate,
                        PaymentNumber = s.PaymentNumber,
                        InvoiceCode = s.BillCode,
                        InvoiceDate = s.BillDate,
                        Note = s.Note,
                        Revenue = s.BillValue,
                        Vat = s.VatValue,
                        Value = s.Value,
                        CompanyType = cu.IsExternalEVN == true ? CompanyType.ExternalEVN : (cu.IsExternalEVNNPC == true ? CompanyType.ExternalEVNPC : CompanyType.InternalEVNPC)


                    }

                    ).ToList().OrderBy(c => c.CompanyType).ThenBy(c => c.CustomerName).ThenBy(c => c.ContractCode).ThenBy(c => c.PaymentDate).ToList();


                return res;
            }

        }

        public List<ReportDebtModel> StatisticDebt(CheckTimeModel filter)
        {
            var result = new List<ReportDebtModel>();
            using (var context = custormerRepository.GetDbContext())
            {
                //get cong ty co hop dong
                var customers = (from c in context.Customers
                                 join k in context.Contracts on c.Id equals k.CustomerId
                                 where k.Status >= ContractStatus.Passed
                                 select new ReportDebtModel()
                                 {
                                     CustomerId = c.Id,
                                     CustomerName = c.Name,
                                     ContractCode = k.CustomerContractCode,
                                     ContractDate = k.SigningDate,
                                     CompanyType = c.IsExternalEVN == true ? CompanyType.ExternalEVN : (c.IsExternalEVNNPC == true ? CompanyType.ExternalEVNPC : CompanyType.InternalEVNPC)
                                    ,
                                     UID = k.UID
                                 }
                                ).Union(
                    from c in context.Customers
                    join k in context.Quotes on c.Id equals k.CustomerId
                    where k.Status >= QuoteStatus.Approved
                    select new ReportDebtModel()
                    {
                        CustomerId = c.Id,
                        CustomerName = c.Name,
                        ContractCode = k.Code,
                        ContractDate = k.SigningDate,
                        CompanyType = c.IsExternalEVN == true ? CompanyType.ExternalEVN : (c.IsExternalEVNNPC == true ? CompanyType.ExternalEVNPC : CompanyType.InternalEVNPC)
                        ,
                        UID = k.UID
                    }
                    );

                List<Guid> uids = customers.Select(c => c.UID).ToList();
                //tinh du no dau ky.

                var items = context.Settlements.Where(c => uids.Contains(c.ContractUID)).ToList();
                var dunodaukys = (

                                from s in context.Settlements.Where(
                                    c => uids.Contains(c.ContractUID)
                                    && c.BillDate < filter.StartDate
                                    && c.Status == SettlemenStatus.Completed
                                )
                                from sp in context.SettlementPayments.Where(
                                       c => c.PaymentDate < filter.StartDate
                                       && c.SettlementId == s.Id
                                    ).DefaultIfEmpty()

                                group new { s, sp } by new
                                {
                                    UID = s.ContractUID,
                                    s.Id,
                                    s.BillCode,
                                    s.BillDate,
                                    s.Value,
                                    s.Note
                                }
                                into g
                                select new
                                {
                                    g.Key.UID,
                                    g.Key.Id,
                                    g.Key.BillDate,
                                    g.Key.BillCode,
                                    g.Key.Note,
                                    g.Key.Value,
                                    PaymentValue = g.Sum(c => c.sp == null ? 0 : c.sp.PaymentValue)
                                }).ToList();

                //phat sinh tron ky:bao gom thanh toan moi và tra cho các thanh toan trước.
                var phatsinhtrongkyThanhtoanmoi = (
                                //thanh toan mới
                                from s in context.Settlements.Where(
                                   c => uids.Contains(c.ContractUID)
                                   && c.Status == SettlemenStatus.Completed
                                   && c.BillDate >= filter.StartDate
                                     && c.BillDate <= filter.EndDate
                               )
                                from sp in context.SettlementPayments.Where(
                                       c => c.PaymentDate >= filter.StartDate
                                        && c.PaymentDate <= filter.EndDate
                                         && c.SettlementId == s.Id
                                    ).DefaultIfEmpty()
                                group new { s, sp } by new
                                {
                                    UID = s.ContractUID,
                                    s.Id,
                                    s.BillCode,
                                    s.BillDate,
                                    s.Value,
                                    s.Note
                                }
                                into g
                                select new
                                {
                                    g.Key.UID,
                                    g.Key.Id,
                                    g.Key.BillDate,
                                    g.Key.BillCode,
                                    g.Key.Note,
                                    g.Key.Value,
                                    PaymentValue = g.Sum(c => c.sp == null ? 0 : c.sp.PaymentValue)
                                }).ToList();

                var phatsinhtrongkyThanhtoacu = (
                                //thanh toan mới
                                from s in context.Settlements.Where(
                                   c => uids.Contains(c.ContractUID)
                                   && c.Status == SettlemenStatus.Completed
                                   && c.BillDate < filter.StartDate
                               )
                                from sp in context.SettlementPayments.Where(
                                       c => c.PaymentDate >= filter.StartDate
                                        && c.PaymentDate <= filter.EndDate
                                         && c.SettlementId == s.Id
                                    )

                                group new { s, sp } by new
                                {
                                    UID = s.ContractUID,
                                    s.Id,
                                    s.BillCode,
                                    s.BillDate,
                                    s.Value,
                                    s.Note
                                }
                                into g
                                select new
                                {
                                    g.Key.UID,
                                    g.Key.Id,
                                    g.Key.BillDate,
                                    g.Key.BillCode,
                                    g.Key.Note,
                                    g.Key.Value,

                                    PaymentValue = g.Sum(c => c.sp == null ? 0 : c.sp.PaymentValue)
                                }).ToList();


                //tinh toan du no
                result = dunodaukys.Select(c => new ReportDebtModel
                {
                    InvoiceCode = c.BillCode,
                    InvoiceDate = c.BillDate,
                    SettlementId = c.Id,
                    Value = c.Value,
                    OpeningStock = (c.Value) - (c.PaymentValue ?? 0),
                    OpeningStockHave = 0,
                    UID = c.UID

                }).ToList();

                phatsinhtrongkyThanhtoanmoi.ForEach(c =>
                {
                    ReportDebtModel m = new ReportDebtModel()
                    {
                        InvoiceCode = c.BillCode,
                        SettlementId = c.Id,
                        InvoiceDate = c.BillDate,
                        Value = c.Value,
                        OpeningStock = 0,
                        OpeningStockHave = 0,
                        Inperiod = c.Value - (c.PaymentValue ?? 0),
                        InperiodHave = c.PaymentValue,
                        UID = c.UID
                    };

                    result.Add(m);
                });
                phatsinhtrongkyThanhtoacu.ForEach(c =>
                {
                    var k = result.FirstOrDefault(n => c.Id == n.SettlementId);
                    if (k != null)
                    {
                        k.InperiodHave = c.PaymentValue;
                        k.Inperiod = 0;
                    }
                });

                var res = (from c in customers
                           join k in result on c.UID equals k.UID
                           orderby c.CompanyType ascending, c.CustomerName ascending, c.ContractDate descending, k.InvoiceDate ascending

                           select new ReportDebtModel()
                           {
                               ClosingStock = k.ClosingStock,
                               ClosingStockHave = k.ClosingStockHave,
                               Inperiod = k.Inperiod,
                               InperiodHave = k.InperiodHave,
                               Value = k.Value,
                               OpeningStock = k.OpeningStock,
                               OpeningStockHave = k.OpeningStockHave,
                               InvoiceCode = k.InvoiceCode,
                               InvoiceDate = k.InvoiceDate,
                               ContractCode = c.ContractCode,
                               ContractDate = c.ContractDate,
                               Content = c.Content,
                               CustomerName = c.CustomerName,
                               CompanyType = c.CompanyType
                           }).ToList();

                res.ForEach(c =>
                {
                    c.ClosingStock = Math.Max((c.OpeningStock ?? 0) + (c.Inperiod ?? 0) - (c.OpeningStockHave ?? 0) - (c.InperiodHave ?? 0), 0);
                    c.ClosingStockHave = Math.Max((c.OpeningStockHave ?? 0) + (c.InperiodHave ?? 0) - (c.Inperiod ?? 0) - (c.ClosingStock ?? 0), 0);
                });

                return res;
            }
        }

        public List<SettlementP2ReportModel> GetSettlementP2Report(SettlementP2ReportFilter filter)
        {

            using (var context = reportitemRepository.GetDbContext())
            {
                var startDate = new DateTime(filter.Year, 1, 1);
                var endDate = new DateTime(filter.Year + 1, 1, 1);
                var giatrihdNamtruoc = context.Contracts.Where(c => startDate > c.SigningDate && c.Status >= ContractStatus.Approved).Sum(c => c.ContractValue) +
                    context.Quotes.Where(c => startDate > c.SigningDate && c.Status >= QuoteStatus.Approved).Sum(c => c.Value);

                ;
                var giatriTTNamtruoc = (from ct in context.Contracts.Where(c => startDate > c.SigningDate && c.Status >= ContractStatus.Approved)
                                        join s in context.Settlements on ct.UID equals s.ContractUID
                                        join sp in context.SettlementPayments on s.Id equals sp.SettlementId
                                        where sp.PaymentDate < startDate
                                        select sp.PaymentValue).Sum(c => c)
                                        +
                                      (from ct in context.Quotes.Where(c => startDate > c.SigningDate && c.Status >= QuoteStatus.Approved)
                                       join s in context.Settlements on ct.UID equals s.ContractUID
                                       join sp in context.SettlementPayments on s.Id equals sp.SettlementId
                                       where sp.PaymentDate < startDate
                                       select sp.PaymentValue).Sum(c => c);
                var doanhthuTungthang = (from ct in context.Contracts
                                         join s in context.Settlements on ct.UID equals s.ContractUID
                                         where s.Status >= SettlemenStatus.Approved
                                            && s.BillDate > startDate && s.BillDate <= endDate
                                         group s by new
                                         {
                                             s.BillDate.Value.Month
                                         } into g
                                         select new
                                         {
                                             Month = g.Key.Month,
                                             Value = g.Sum(c => c.BillValue)
                                         }).Union(
                    from ct in context.Quotes
                    join s in context.Settlements on ct.UID equals s.ContractUID
                    where s.Status >= SettlemenStatus.Approved
                       && s.BillDate > startDate && s.BillDate <= endDate
                    group s by new
                    {
                        s.BillDate.Value.Month
                    } into g
                    select new
                    {
                        Month = g.Key.Month,
                        Value = g.Sum(c => c.BillValue)
                    }
                    ).ToList()
                    .GroupBy(
                    c => c.Month,
                    (key, g) => new { Month = key, Value = g.Sum(k => k.Value) }

                    ).ToList();

                var thanhtoanTungthang = (from ct in context.Contracts
                                          join s in context.Settlements on ct.UID equals s.ContractUID
                                          join sp in context.SettlementPayments on s.Id equals sp.SettlementId
                                          where s.Status >= SettlemenStatus.Approved
                                             && s.BillDate > startDate && s.BillDate <= endDate
                                          group s by new
                                          {
                                              s.BillDate.Value.Month
                                          } into g
                                          select new
                                          {
                                              Month = g.Key.Month,
                                              Value = g.Sum(c => c.BillValue)
                                          }).Union(
                    from ct in context.Quotes
                    join s in context.Settlements on ct.UID equals s.ContractUID
                    join sp in context.SettlementPayments on s.Id equals sp.SettlementId
                    where s.Status >= SettlemenStatus.Approved
                       && s.BillDate > startDate && s.BillDate <= endDate
                    group sp by new
                    {
                        sp.PaymentDate.Value.Month
                    } into g
                    select new
                    {
                        Month = g.Key.Month,
                        Value = g.Sum(c => c.PaymentValue)
                    }).ToList()
                    .GroupBy(
                    c => c.Month,
                    (key, g) => new { Month = key, Value = g.Sum(k => k.Value) }

                    ).ToList();


                List<SettlementP2ReportModel> dataByMonth = new List<SettlementP2ReportModel>();
                dataByMonth.Add(new SettlementP2ReportModel()
                {
                    Content = "Giá trị năm trước",
                    ContractValue = giatrihdNamtruoc,
                    PreviousPaymentValue = giatriTTNamtruoc

                });
                for (int i = 1; i <= 12; i++)
                {
                    var monthItem = new SettlementP2ReportModel()
                    {
                        Content = "Tổng cộng tháng " + i.ToString(),
                        PaymentValue = thanhtoanTungthang.Where(c => c.Month == i).Sum(c => c.Value),
                        BillValue = doanhthuTungthang.Where(c => c.Month == i).Sum(c => c.Value),
                    };
                    dataByMonth.Add(monthItem);
                }
                dataByMonth.Add(new SettlementP2ReportModel()
                {
                    Content = "Tổng lũy kế năm",
                    PaymentValue = thanhtoanTungthang.Sum(c => c.Value),
                    BillValue = doanhthuTungthang.Sum(c => c.Value),
                    PreviousPaymentValue = giatriTTNamtruoc
                });


                //gia trị theo nhom hop dong

                var doanhthuTheoHopdong = (from s in context.Settlements.Where(
                                            c => c.BillDate >= startDate
                                            && c.BillDate <= endDate
                                            )
                                           join c in context.Contracts on s.ContractUID equals c.UID
                                           select new
                                           {
                                               ContractId = c.Id,
                                               UID = c.UID,
                                               Value = s.BillValue,

                                           })
                                          .Union(
                    from s in context.Settlements.Where(
                                            c => c.BillDate >= startDate
                                            && c.BillDate <= endDate
                                            )
                    join c in context.Quotes on s.ContractUID equals c.UID
                    select new
                    {
                        ContractId = c.Id,
                        UID = c.UID,
                        Value = s.BillValue,

                    }
                    )
                                          .ToList();
                var thanhtoanTheoHopdong = (from s in context.Settlements
                                            join sp in context.SettlementPayments on s.Id equals sp.SettlementId
                                            join c in context.Contracts on s.ContractUID equals c.UID
                                            where sp.PaymentDate >= startDate
                                                && sp.PaymentDate <= endDate

                                            group new { s, sp, c }
                                            by new
                                            {
                                                c.Id,
                                                c.UID
                                            }
                                            into g

                                            select new
                                            {
                                                ContractId = g.Key.Id,
                                                UID = g.Key.UID,
                                                Value = g.Sum(c => c.sp.PaymentValue),

                                            }).Union(

                                            from s in context.Settlements
                                            join sp in context.SettlementPayments on s.Id equals sp.SettlementId
                                            join c in context.Quotes on s.ContractUID equals c.UID
                                            where sp.PaymentDate >= startDate
                                                && sp.PaymentDate <= endDate

                                            group new { s, sp, c }
                                            by new
                                            {
                                                c.Id,
                                                c.UID
                                            }
                                            into g


                                            select new
                                            {
                                                ContractId = g.Key.Id,
                                                UID = g.Key.UID,
                                                Value = g.Sum(c => c.sp.PaymentValue),

                                            }
                    ).ToList();

                var uids = new List<Guid>();
                var uidDoanhthu = doanhthuTheoHopdong.Select(c => c.UID).ToList();
                if (uidDoanhthu.Count > 0)
                    uids.AddRange(uidDoanhthu);


                var uidThanhtoan = thanhtoanTheoHopdong.Select(c => c.UID).ToList();
                if (uidThanhtoan.Count > 0)
                    uids.AddRange(uidThanhtoan);



                var giatrihopdong = (from ct in context.Contracts.Where(c =>

                                    (c.StartDate >= startDate && c.EndDate <= endDate && c.Status >= ContractStatus.Approved)
                                    || uids.Contains(c.UID)
                                    )
                                     join g in context.ContractGroups on ct.GroupId equals g.Id
                                     select new
                                     {
                                         ct.Content,
                                         ContractId = ct.Id,
                                         UID = ct.UID,
                                         Value = ct.ContractValue,
                                         GroupName = g.Name,
                                         GroupId = g.Id
                                     }).Union(
                                    from ct in context.Quotes.Where(c =>

                                    (c.StartDate >= startDate && c.EndDate <= endDate && c.Status >= QuoteStatus.Approved)
                                    || uids.Contains(c.UID)
                                    )

                                    select new
                                    {
                                        ct.Content,
                                        ContractId = ct.Id,
                                        UID = ct.UID,
                                        Value = ct.Value,
                                        GroupName = "Báo giá TBL",
                                        GroupId = -1
                                    }
                    ).Distinct().ToList();


                var groups = giatrihopdong.Select(c => c.GroupName).ToList().OrderBy(c => c).Distinct();

                var uidHds = giatrihopdong.Select(c => c.UID).ToList();
                var thanhtoanKytruocTheohopdong = (from s in context.Settlements.Where(c => uidHds.Contains(c.ContractUID))
                                                   join sp in context.SettlementPayments on s.Id equals sp.SettlementId
                                                   group new { s, sp }
                                                   by new { s.ContractUID }
                                                 into g
                                                   select new
                                                   {
                                                       UID = g.Key,
                                                       Value = g.Sum(c => c.sp.PaymentValue)
                                                   }).ToList();



                foreach (var item in groups)
                {
                    var itemByGroup = giatrihopdong.Where(c => c.GroupName == item).ToList();
                    var reportData = new SettlementP2ReportModel()
                    {
                        Content = item,
                        Group = item,
                        ContractValue = itemByGroup.Sum(c => c.Value),
                        IsSummaryRow = true
                    };
                    reportData.PaymentValue = thanhtoanTheoHopdong.Join(itemByGroup,
                        t => t.UID,
                        g => g.UID,
                        (t, g) => new { t.Value }).Sum(c => c.Value);

                    reportData.BillValue = doanhthuTheoHopdong.Join(itemByGroup,
                        t => t.UID,
                        g => g.UID,
                        (t, g) => new { t.Value }).Sum(c => c.Value);

                    dataByMonth.Add(reportData);

                    foreach (var hd in itemByGroup)
                    {
                        var r = new SettlementP2ReportModel();
                        r.Content = hd.Content;
                        r.Group = item;
                        r.ContractValue = hd.Value;
                        r.BillValue = doanhthuTheoHopdong.Where(c => c.UID == hd.UID).Sum(c => c.Value);
                        r.PaymentValue = thanhtoanTheoHopdong.Where(c => c.UID == hd.UID).Sum(c => c.Value);
                        r.PreviousPaymentValue = thanhtoanKytruocTheohopdong.Where(c => c.UID.GetHashCode() == hd.UID.GetHashCode()).Sum(c => c.Value);
                        dataByMonth.Add(r);
                    }



                }

                return dataByMonth;



            }
        }



    }
}
