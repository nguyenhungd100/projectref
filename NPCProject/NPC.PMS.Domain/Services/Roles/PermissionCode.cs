﻿namespace NPC.PMS.Domain.Services.Roles
{
    public enum PermissionGroupCode
    {
        SYSTEM = 1,
        CONTRACT = 2,
        REPORT = 3,
        ACCOUNTING = 4,
    }
    public enum PermissionCode
    {
        // System
        [PermissionDetails(PermissionGroupCode.SYSTEM, "Quản lý người dùng")]
        MANAGE_USERS = 1,
        [PermissionDetails(PermissionGroupCode.SYSTEM, "Quản lý vai trò")]
        MANAGE_ROLES = 5,

        // Contract
        [PermissionDetails(PermissionGroupCode.CONTRACT, "Xem tất cả hợp đồng")]
        CONTRACT_VIEWALL = 99,
        [PermissionDetails(PermissionGroupCode.CONTRACT, "Khởi tạo hợp đồng")]
        CONTRACT_INIT = 100,
        [PermissionDetails(PermissionGroupCode.CONTRACT, "Cập nhật hợp đồng")]
        CONTRACT_UPDATE = 101,
        [PermissionDetails(PermissionGroupCode.CONTRACT, "Phê duyệt hợp đồng")]
        CONTRACT_APPROVE = 102,
        [PermissionDetails(PermissionGroupCode.CONTRACT, "Đóng hợp đồng")]
        CONTRACT_CLOSE = 103,
        [PermissionDetails(PermissionGroupCode.CONTRACT, "Cập nhật kế hoạch tổng thể")]
        UPDATE_MASTER_PLAN = 104,
        [PermissionDetails(PermissionGroupCode.CONTRACT, "Lập kế hoạch đợt")]
        INIT_PHASE = 105,
        [PermissionDetails(PermissionGroupCode.CONTRACT, "Cập nhật kế hoạch đợt")]
        UPDATE_PHASE = 106,
        [PermissionDetails(PermissionGroupCode.CONTRACT, "Phê duyệt đợt")]
        APPROVE_PHASE = 107,
        [PermissionDetails(PermissionGroupCode.CONTRACT, "Phát hành quyết định đợt")]
        PUBLISH_PHASE = 109,
        [PermissionDetails(PermissionGroupCode.CONTRACT, "Xóa kế hoạch đợt")]
        DELETE_PHASE = 110,
        [PermissionDetails(PermissionGroupCode.CONTRACT, "Hoàn thành hoạch đợt")]
        CONTRACT_FINISH_PHASE = 111,
        [PermissionDetails(PermissionGroupCode.CONTRACT, "Phân giao khối lượng")]
        ASSIGN_WORKITEMS = 112,
        [PermissionDetails(PermissionGroupCode.CONTRACT, "Đề xuất nhân sự")]
        REQUEST_STAFFS = 113,
        [PermissionDetails(PermissionGroupCode.CONTRACT, "Phân công nhân sự")]
        ASSIGN_STAFFS = 116,
        [PermissionDetails(PermissionGroupCode.CONTRACT, "Duyệt thiết bị")]
        SETUP_EQUIPMENTS = 115,
        [PermissionDetails(PermissionGroupCode.CONTRACT, "Phân xe")]
        SETUP_VERHICLES = 114,
        [PermissionDetails(PermissionGroupCode.CONTRACT, "Chỉ định CV phụ trách HĐ")]
        ASSIGN_HANDLERS = 120,
        [PermissionDetails(PermissionGroupCode.CONTRACT, "Chỉ định đội trưởng")]
        ASSIGN_CAPTAIN = 125,
        // Workout
        [PermissionDetails(PermissionGroupCode.REPORT, "Lập báo cáo")]
        REPORT_CREATE = 205,
        [PermissionDetails(PermissionGroupCode.REPORT, "Duyệt báo cáo")]
        REPORT_APPROVE = 220,
        [PermissionDetails(PermissionGroupCode.REPORT, "Lập biên bản")]
        RECORD_CREATE = 225,
        [PermissionDetails(PermissionGroupCode.REPORT, "Duyệt biên bản")]
        RECORD_APPROVE = 230,
        // settlements
        [PermissionDetails(PermissionGroupCode.ACCOUNTING, "Lập hồ sơ thanh toán")]
        CREATE_SETTLEMENT = 301,

       
    }
}
