
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using NPC.PMS.Domain.Data;
using NPC.PMS.Domain.Data.Entity;
using NPC.PMS.Domain.Models.Users;
using NPC.PMS.Framework.Utils;

namespace NPC.PMS.Domain.Services.Roles
{
    public class RolesService : IRolesService
    {
        private readonly IRepository<Role> _roleRepository;

        public RolesService()
        {

        }
        public RolesService(IRepository<Role> roleRepository)
        {
            _roleRepository = roleRepository;
        }

        public RoleFilterResult Filter(RoleFilterModel roleFilterModel)
        {
            var result = new RoleFilterResult();
            var roles = _roleRepository.Fetch( r=>r.Id > 0, orderBy: r =>  r.DisplayOrder).ToList();
            result.Records = DomainMaps.Mapper.Map<List<RoleModel>>(roles);
            result.TotalRecord = roles.Count();
            return result;
        }

        public RoleModel GetById(int id)
        {
            var role = _roleRepository.GetById(id);
            return DomainMaps.Mapper.Map<RoleModel>(role);
        }

        public List<RoleModel> List(int[] ids)
        {
            var res = _roleRepository.Fetch(r => ids.Contains(r.Id));
            return res.ToList().CloneToListModels<Role, RoleModel>();
        }

        public List<PermissionGroup> ListPermissions()
        {
            return PermissionHelper.GetPermissionGroups();
        }

        public async Task<bool> UpdateAsync(RoleModel role)
        {
            //update to storage
            var entity = role.Clone<RoleModel, Role>();
            return await _roleRepository.UpdateAsync(entity);
        }
    }
}

