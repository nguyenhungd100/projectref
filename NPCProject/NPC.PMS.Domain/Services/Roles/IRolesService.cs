using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using NPC.PMS.Domain.Entity;
using NPC.PMS.Domain.Models.Users;

namespace NPC.PMS.Domain.Services.Roles
{
	public interface IRolesService
	{
        List<PermissionGroup> ListPermissions();

        List<RoleModel> List(int[] ids);
        RoleFilterResult Filter(RoleFilterModel roleFilterModel);
        RoleModel GetById(int id);
        Task<bool> UpdateAsync(RoleModel role);
    }
}


