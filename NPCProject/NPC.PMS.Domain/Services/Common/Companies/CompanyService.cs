﻿using NPC.PMS.Domain.Data;
using NPC.PMS.Domain.Data.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NPC.PMS.Domain.Services.Common.Companies
{
    public class CompanyService : ICompanyService
    {
        IRepository<Company> _companyBranchRepository;
        public CompanyService(IRepository<Company> CompanyRepository)
        {
            _companyBranchRepository = CompanyRepository;
        }

        public bool Delete(int id)
        {
            var companyBranch = _companyBranchRepository.GetById(id);
            var res = _companyBranchRepository.Delete(companyBranch);
            return res;
        }

        public List<CompanyModel> GetList()
        {
            using(var db = _companyBranchRepository.GetDbContext())
            {
                var q = db.Companies.AsQueryable();
                var companyBranches = q.ToList();
                var model = companyBranches.Map<List<CompanyModel>>();
                return model;
            }
        }

        public CompanyModel Save(CompanyModel model)
        {
            if (model.Name == null) throw new ServiceException("Chưa nhập tên!");
            if (model.Address == null) throw new ServiceException("Chưa nhập địa chỉ!");
            if (model.Email == null) throw new ServiceException("Chưa nhập Email!");
            if (model.Phone == null) throw new ServiceException("Chưa nhập sđt!");
            var companyBranch = model.Map<Company>();
            bool res = false;
            if (model.Id != 0) res = _companyBranchRepository.Update(companyBranch);
            else
            {
                var code = _companyBranchRepository.Fetch(t => t.Code == model.Code);
                if (code.Count != 0) throw new ServiceException("Code đã tồn tại");
                res = _companyBranchRepository.Insert(companyBranch);
            }
            return companyBranch.Map<CompanyModel>();
        }

        public CompanyModel Get(int id)
        {
            var entity = _companyBranchRepository.GetById(id);
            return entity.Map<CompanyModel>();
        }
    }
}
