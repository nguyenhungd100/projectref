﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NPC.PMS.Domain.Services.Common.Companies
{
    public interface ICompanyService
    {
        List<CompanyModel> GetList();
        CompanyModel Save(CompanyModel model);
        bool Delete(int id);
        CompanyModel Get(int id);
    }
}
