﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.AspNetCore.Http;
using NPC.PMS.Domain.Data;
using NPC.PMS.Domain.Data.Entity;

namespace NPC.PMS.Domain.Services.Common.Devices
{
    public interface IEquipmentService
    {
        EquipmentFilterResult GetList(EquipmentFilterModel filter);
        EquipmentModel SaveModel(EquipmentModel model);
        bool UpdateModel(EquipmentModel model);
        bool Delete(int[] ids);
        List<EquipmentModel> GetAvailable();
        EquipmentModel Get(int id);
        List<EquipmentModel> ImportExcelEquipment(byte[] bytes, int userId);
    }
}
