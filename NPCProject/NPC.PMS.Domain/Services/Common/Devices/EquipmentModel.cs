﻿using NPC.PMS.Domain.Enums;
using NPC.PMS.Framework.Extensions.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace NPC.PMS.Domain.Services.Common.Devices
{
    public class EquipmentModel
    {
        public int Id { get; set; }
        public string Code { get; set; } // mã thiết bị

        public string Imei { get; set; }// số chế tạo
        public string NameForPCM { get; set; }// tên thiết bị theo phòng chuyên môn
        public string NameForCompany { get; set; }// Tên thiết bị theo công ty

        public Department DepartmentId { get; set; }

        public DateTime ExpiriteDate { get; set; }

        public EquipmentStatus Status { get; set; }

        public string StatusName { get { return Status.ToDescription(); } }

        public string DepartmentName { get { return DepartmentId.ToDescription(); } }

        public string PhaseDetail { get; set; }

        public List<string> BusyPhases { get; set; } = new List<string>();
    }
}
