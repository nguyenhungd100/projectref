﻿using NPC.PMS.Domain.Data;
using NPC.PMS.Domain.Data.Entity;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using Microsoft.AspNetCore.Http;
using NPC.PMS.Framework.MimeDetective;
using System.IO;
using OfficeOpenXml;
using NPC.PMS.Domain.Enums;

namespace NPC.PMS.Domain.Services.Common.Devices
{
    public class EquipmentService : IEquipmentService
    {
        IRepository<Equipment> equipmentRepository;
        private readonly IRepository<Phase> phaseRepository;
        private readonly IRepository<PhaseEquipment> phaseEquipmentRepository;
        private readonly IRepository<Contract> contractRepository;

        public EquipmentService(IRepository<Equipment> equipmentRepository,
            IRepository<Phase> phaseRepository,
            IRepository<PhaseEquipment> phaseEquipmentRepository,
            IRepository<Contract> contractRepository)
        {
            this.equipmentRepository = equipmentRepository;
            this.phaseRepository = phaseRepository;
            this.phaseEquipmentRepository = phaseEquipmentRepository;
            this.contractRepository = contractRepository;
        }

        public bool Delete(int[] ids)
        {
            var del = equipmentRepository.DeleteMany(d => ids.Contains(d.Id));
            return del > 0;
        }

        public List<EquipmentModel> GetAvailable()
        {
            var runingPhases = phaseRepository.Fetch(p => p.Status != Enums.PhaseStatus.Completed);
            var phaseIds = runingPhases.Select(p => p.Id);
            var phaseEquipments = phaseEquipmentRepository.Fetch(e => phaseIds.Contains(e.PhaseId));
            var equipmentIds = phaseEquipments.Select(p => p.EquipmentId);

            var equipments = equipmentRepository.Fetch();
            var result = new List<EquipmentModel>();
            foreach (var equipment in equipments)
            {
                var model = equipment.Map<EquipmentModel>();
                if (equipmentIds.Contains(equipment.Id))
                {
                    //var usingQuantity = phaseEquipments.Where(p => p.EquipmentId == equipment.Id).Sum(p => p.Quantity);
                    ///model.InuseQuantity = usingQuantity;
                }
                result.Add(model);
            }
            return result;
        }

        public EquipmentFilterResult GetList(EquipmentFilterModel filter)
        {
            var result = new EquipmentFilterResult();

            var devices = new List<Equipment>();
            using (var db = equipmentRepository.GetDbContext())
            {
                var q = db.Equipments.AsQueryable();

                if (filter.Status.HasValue)
                {
                    q = q.Where(c => c.Status == filter.Status.Value);
                }

                if (filter.DepartmentId.HasValue)
                {
                    q = q.Where(c => c.DepartmentId == filter.DepartmentId.Value);
                }

                if (!string.IsNullOrEmpty(filter.Code))
                {
                    q = q.Where(c => c.Code.Contains(filter.Code));
                }

                if (!string.IsNullOrEmpty(filter.Imei))
                {
                    q = q.Where(c => c.Code.Contains(filter.Imei));
                }

                if (!string.IsNullOrEmpty(filter.NameForCompany))
                {
                    q = q.Where(c => c.Code.Contains(filter.NameForCompany));
                }

                if (!string.IsNullOrEmpty(filter.NameForPCM))
                {
                    q = q.Where(c => c.Code.Contains(filter.NameForPCM));
                }

                var entities = q.OrderBy(c => c.Status).Skip((filter.PageIndex - 1) * filter.PageSize).Take(filter.PageSize).ToList();
                result.Records = entities.Map<List<EquipmentModel>>();
                result.TotalRecord = result.Records.Count();



                var phaseEquipments = new List<PhaseEquipment>();
                var contracts = new List<Contract>();
                if (result.Records.Count() > 0)
                {
                    var checkIds = result.Records.Select(c => c.Id).ToList();
                    phaseEquipments = phaseEquipmentRepository.Fetch(c => checkIds.Contains(c.EquipmentId)).ToList();

                }

                if (phaseEquipments.Count() > 0)
                {
                    var phaseIds = phaseEquipments.Select(c => c.PhaseId).Distinct();
                    var phases = phaseRepository.Fetch(c => phaseIds.Contains(c.Id));
                    var contractIds = phases.Select(c => c.ContractId);
                    contracts = contractRepository.Fetch(c => contractIds.Contains(c.Id));

                    foreach (var device in result.Records)
                    {
                        var busyPhaseIds = phaseEquipments.Where(ps => ps.EquipmentId == device.Id).Select(ps => ps.PhaseId);
                        var busyPhases = phases.Where(p => busyPhaseIds.Contains(p.Id));

                        foreach (var busyPhase in busyPhases)
                        {
                            var contract = contracts.FirstOrDefault(c => c.Id == busyPhase.ContractId);
                            string temp = string.Format("Đợt {0} {1} Từ {2} Đến {3}", busyPhase.No, contract.Content, busyPhase.StartDate.ToString("dd/MM/yyyy HH:mm"), busyPhase.EndDate.ToString("dd/MM/yyyy HH:mm"));
                            device.BusyPhases.Add(temp);
                        }
                    }
                }
            }




            return result;
        }

        public EquipmentModel SaveModel(EquipmentModel model)
        {
            var entity = new Equipment();
            if (model.Id > 0)
            {
                entity = equipmentRepository.GetById(model.Id);
                if (entity != null) throw new ServiceException("Thiết bị không tồn tại");

                entity.DepartmentId = model.DepartmentId;
                entity.NameForCompany = model.NameForCompany;
                entity.NameForPCM = model.NameForPCM;
                entity.ExpiriteDate = model.ExpiriteDate;
                entity.Imei = model.Imei;
                entity.Status = model.Status;
                entity.Code = model.Code;
                equipmentRepository.Update(entity);
            }
            else
            {

                entity.DepartmentId = model.DepartmentId;
                entity.NameForCompany = model.NameForCompany;
                entity.NameForPCM = model.NameForPCM;
                entity.ExpiriteDate = model.ExpiriteDate;
                entity.Imei = model.Imei;
                entity.Status = model.Status;
                entity.Code = model.Code;
                equipmentRepository.Insert(entity);

            };


            return entity.Map<EquipmentModel>();
        }

        public bool UpdateModel(EquipmentModel model)
        {
            var Device = model.Map<Equipment>();
            return equipmentRepository.Update(Device);
        }

        public EquipmentModel Get(int id)
        {
            var entity = equipmentRepository.GetById(id);
            return entity.Map<EquipmentModel>();
        }




        public List<EquipmentModel> ImportExcelEquipment(byte[] bytes, int userId)
        {
            //var fileType = bytes.GetFileType();
            //if (fileType.Mime != "application/vnd.ms-excel" && fileType.Mime != "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")
            //    throw new ServiceException("File không được chấp nhận");

            //MemoryStream stream = new MemoryStream();
            //stream.Write(bytes, 0, bytes.Length);

            //var package = new ExcelPackage(stream);
            //ExcelWorksheet worksheet = package.Workbook.Worksheets[0];

            //var cells = worksheet.Cells;

            //var sothetscdCell = cells.FirstOrDefault(c => c.Text.Replace(" ", string.Empty).ToLower() == "sốthẻtscđ");
            //var phongCMCell = cells.FirstOrDefault(c => c.Text.Replace(" ", string.Empty).ToLower() == "phòngchuyênmôn");
            //var tentbPCMCell = cells.FirstOrDefault(c => c.Text.Replace(" ", string.Empty).ToLower() == "tênthiếtbịtheopcm");
            //var matbtheoPCM = cells.FirstOrDefault(c => c.Text.Replace(" ", string.Empty).ToLower() == "mãtbtheophòng");

            //var matbCell = cells.FirstOrDefault(c => c.Text.Replace(" ", string.Empty).ToLower() == "mãtb(kiểumáy)" || c.Text.Replace(" ", string.Empty).ToLower() == "mãmodel");
            //var soctCell = cells.FirstOrDefault(c => c.Text.Replace(" ", string.Empty).ToLower() == "sốchếtạo");
            //var tinhtrangCell = cells.FirstOrDefault(c => c.Text.Replace(" ", string.Empty).ToLower() == "tìnhtrạngtb");

            //var hankdCell = cells.FirstOrDefault(c => c.Text.Replace(" ", string.Empty).ToLower() == "hạnkiểmđịnh");
            //var tentbCtyCell = cells.FirstOrDefault(c => c.Text.Replace(" ", string.Empty).ToLower() == "tênthiếtbịtheocôngty");

            //var phanloaitsCell = cells.FirstOrDefault(c => c.Text.Replace(" ", string.Empty).ToLower() == "phânloạits");
            //var donvitinhCell = cells.FirstOrDefault(c => c.Text.Replace(" ", string.Empty).ToLower() == "đơnvịtính");

            //var soluongCell = cells.FirstOrDefault(c => c.Text.Replace(" ", string.Empty).ToLower() == "sốlượng");
            //var ngayduavaosudungCell = cells.FirstOrDefault(c => c.Text.Replace(" ", string.Empty).ToLower() == "ngàyđưavàosửdụng");
            ////var canhancamCell = cells.FirstOrDefault(c => c.Text.Replace(" ", string.Empty).ToLower() == "cánhânđangcầm");

            //var entities = new List<Equipment>();


            //int totalRows = worksheet.Dimension.End.Row;
            ////var index = 0;
            ////for (int i = 1; i <= totalRows; i++)
            ////{
            ////    var spaceRow = worksheet.Cells[i, worksheet.Cells.First(c => c.Text.ToString().Contains("stt"))
            ////                  .Start.Column].Text.ToString();
            ////    index = i;
            ////    break;
            ////}

            //for (int i = 3; i <= totalRows; i++)
            //{
            //    var entity = new Equipment
            //    {
            //        CardNumberTSCD = cells[i, sothetscdCell.Start.Column].Text,
            //        NameForPCM = cells[i, tentbPCMCell.Start.Column].Text,
            //        CodeByPCM = cells[i, matbtheoPCM.Start.Column].Text,
            //        Code = cells[i, matbCell.Start.Column].Text,
            //        Imei = cells[i, soctCell.Start.Column].Text,
            //        NameForCompany = cells[i, tentbCtyCell.Start.Column].Text,
            //        CalculationUnit = cells[i, donvitinhCell.Start.Column].Text,
            //        ClassifyTS = cells[i, phanloaitsCell.Start.Column].Text,
            //        //IndividualIsHolding = cells[i, canhancamCell.Start.Column].Text
            //    };

            //    if (cells[i, soluongCell.Start.Column].Text.Length > 0)
            //    {
            //        entity.Quantity = int.Parse(cells[i, soluongCell.Start.Column].Text);
            //    }
            //    var checkPCM = cells[i, phongCMCell.Start.Column].Text.Replace(" ", string.Empty).ToLower();

            //    if (checkPCM == "phòngtnrơle")
            //        entity.DepartmentId = Department.RL;
            //    if (checkPCM == "p.caoáp")
            //        entity.DepartmentId = Department.CA;
            //    if (checkPCM == "phòngđolường")
            //        entity.DepartmentId = Department.DL;
            //    if (checkPCM == "phònghoáchất")
            //        entity.DepartmentId = Department.H;
            //    if (checkPCM == "tncnnl")
            //        entity.DepartmentId = Department.CNNL;
            //    if (checkPCM == "phânxưởngcơđiện")
            //        entity.DepartmentId = Department.CD;
            //    if (checkPCM == "phòngtựđộnghóa")
            //        entity.DepartmentId = Department.TDH;

            //    var checkTinhtrang = cells[i, tinhtrangCell.Start.Column].Text.Replace(" ", string.Empty).ToLower();
            //    if (checkTinhtrang == "tốt" || checkTinhtrang == "bìnhthường")
            //        entity.Status = EquipmentStatus.AllRight;
            //    if (checkTinhtrang == "hỏng")
            //        entity.Status = EquipmentStatus.Awry;
            //    if (checkTinhtrang == "đangsửa")
            //        entity.Status = EquipmentStatus.Fixing;

            //    //entity.ExpiriteDate = DateTime.ParseExact(cells[i, hankdCell.Start.Column].Text, "dd/MM/yyyy", null);
            //    if (!string.IsNullOrEmpty(cells[i, ngayduavaosudungCell.Start.Column].Text))
            //    {
            //        var te = cells[i, ngayduavaosudungCell.Start.Column].Value.ToString();
            //        entity.DatePutIntoUse = DateTime.Parse(te);
            //    }

            //    if (!string.IsNullOrEmpty(cells[i, hankdCell.Start.Column].Text))
            //    {
            //        var te = cells[i, hankdCell.Start.Column].Value.ToString();
            //        entity.ExpiriteDate = DateTime.Parse(te);
            //    }

            //    entities.Add(entity);
            //}

            //equipmentRepository.InsertMany(entities);


            return null;
        }
    }
}
