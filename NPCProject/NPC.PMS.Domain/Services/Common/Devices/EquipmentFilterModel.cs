﻿using NPC.PMS.Domain.Enums;
using NPC.PMS.Domain.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace NPC.PMS.Domain.Services.Common.Devices
{
    public class EquipmentFilterModel : BaseFilterModel
    {
        public Department? DepartmentId { get; set; }

        public string Code { get; set; } // mã thiết bị

        public string Imei { get; set; }// số chế tạo
        public string NameForPCM { get; set; }// tên thiết bị theo phòng chuyên môn
        public string NameForCompany { get; set; }// Tên thiết bị theo công ty

        public EquipmentStatus? Status { get; set; }
    }
}
