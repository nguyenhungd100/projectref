﻿using System;
using System.Collections.Generic;
using System.Text;
using NPC.PMS.Domain.Data;
using NPC.PMS.Domain.Data.Entity;
using System.Linq;
using NPC.PMS.Domain.Services.Common.Customers;

namespace NPC.PMS.Domain.Services.Common.Debts
{
    public class DebtService : IDebtService
    {
        IRepository<Debt> _debtRepository;
        IRepository<Customer> _customerRepository;
        public DebtService(IRepository<Debt> DebtRepositoty,
        IRepository<Customer> CustomerRepository)
        {
            _debtRepository = DebtRepositoty;
            _customerRepository = CustomerRepository;
        }

        public bool DeleteModel(int[] ids)
        {
            foreach (var id in ids)
            {
                var debt = _debtRepository.GetById(id);
                var del = _debtRepository.Delete(debt);
                if (!del) throw new ServiceException("Lỗi xóa id = " + id);
            }
            return true;
        }

        public List<DebtModel> GetList(PagingInfo filter)
        {
            using(var db = _debtRepository.GetDbContext())
            {
                var q = from d in db.Debts
                        select d;
                var debts = q.OrderByDescending(c => c.Id).Skip(filter.StartRowIndex).Take(filter.PageSize).ToList();
                filter.RowsCount = q.Count();
                var debtModel = debts.Map<List<DebtModel>>();
                foreach (var debt in debtModel)
                {
                    var customer = _customerRepository.GetById(debt.CustomerId);
                    debt.Customers = customer.Map<CustomerModel>();
                }
                return debtModel;
            }
        }

        public DebtModel SaveModel(DebtModel model)
        {
            if (model.CustomerId == 0) throw new ServiceException("Chưa nhập thông tin khách hàng!");
            var debt = model.Map<Debt>();
            bool res = false;
            if (model.Id != 0) res = _debtRepository.Update(debt);
            else res = _debtRepository.Insert(debt);
            return debt.Map<DebtModel>();
        }

        public DebtModel Get(int id)
        {
            var entity = _debtRepository.GetById(id);
            return entity.Map<DebtModel>();
        }
    }
}
