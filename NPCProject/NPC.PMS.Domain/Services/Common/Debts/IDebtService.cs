﻿using System;
using System.Collections.Generic;
using System.Text;
using NPC.PMS.Domain.Data;
using NPC.PMS.Domain.Data.Entity;

namespace NPC.PMS.Domain.Services.Common.Debts
{
    public interface IDebtService
    {
        List<DebtModel> GetList(PagingInfo filter);
        DebtModel SaveModel(DebtModel model);
        bool DeleteModel(int[] ids);
        DebtModel Get(int id);
    }
}
