﻿using NPC.PMS.Domain.Services.Common.Customers;
using System;
using System.Collections.Generic;
using System.Text;

namespace NPC.PMS.Domain.Services.Common.Debts
{
    public class DebtModel
    {
        public int Id { get; set; }
        public string Code { get; set; }
        public int CustomerId { get; set; }
        public decimal Beginning { get; set; }
        public decimal Bill { get; set; }
        public decimal Paid { get; set; }
        public decimal TermEnd { get; set; }
        public int Term { get; set; }
        public double Time { get; set; }
        public string Description { get; set; }
        public int UserId { get; set; }
        public CustomerModel Customers { get; set; }
    }
}
