﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Newtonsoft.Json;
using NPC.PMS.Domain.Data;
using NPC.PMS.Domain.Data.Entity;
using NPC.PMS.Domain.Entity;
using NPC.PMS.Domain.Enums;
using NPC.PMS.Domain.Models.Notifications;
using NPC.PMS.Framework.Extensions.Enums;
using NPC.PMS.Framework.Push;

namespace NPC.PMS.Domain.Services.Common.Notifications
{
    public class NotificationService : INotificationService
    {
        private readonly IRepository<Notification> notificationRepository;
        private readonly IPushService pushService;
        private readonly IRepository<UserDevice> userDeviceRepository;

        public NotificationService(IRepository<Notification> notificationRepository, IPushService pushService, IRepository<UserDevice> userDeviceRepository)
        {
            this.notificationRepository = notificationRepository;
            this.pushService = pushService;
            this.userDeviceRepository = userDeviceRepository;
        }
        public Notification Add(Notification model)
        {
            model.Time = DateTime.Now;
            notificationRepository.Insert(model);
            return model;
        }

        public bool Delete(List<Guid> ids)
        {
            notificationRepository.DeleteMany(n => ids.Contains(n.Id));
            return true;
        }

        public List<Notification> Filter(NotificationFilterModel model)
        {
            using (var db = notificationRepository.GetDbContext())
            {
                var q = db.Notifications.Where(n => n.UserId == model.UserId);
                var entities = q.OrderByDescending(c => c.Time).Skip((model.PageIndex - 1) * model.PageSize).Take(model.PageSize).ToList();

                //prepare json data for notify
                foreach (var item in entities)
                {
                    var json = string.Empty;

                    switch (item.Type)
                    {
                        case NotificationType.System:
                            {
                                break;
                            };

                        case NotificationType.NewContract:
                            {
                                break;
                            };

                        case NotificationType.PassContract:
                            {
                                dynamic data = db.Contracts.Find(item.ObjectId);
                                json = JsonConvert.SerializeObject(data);
                                break;
                            };

                        case NotificationType.AssignContractUser:
                            {
                                var data = db.Contracts.Find(item.ObjectId);
                                json = JsonConvert.SerializeObject(data);
                                break;
                            };


                        case NotificationType.AssignContractAUser:
                            {
                                var data = db.Contractas.Find(item.ObjectId);
                                json = JsonConvert.SerializeObject(data);
                                break;
                            };


                        case NotificationType.SubmitPhase:
                            {
                                var data = db.Phases.Find(item.ObjectId);
                                var contract = db.Contracts.Find(data.ContractId);
                                json = JsonConvert.SerializeObject(new { data, contract });
                                break;
                            };


                        case NotificationType.RejectPhase:
                            {
                                var data = db.Phases.Find(item.ObjectId);
                                var contract = db.Contracts.Find(data.ContractId);
                                json = JsonConvert.SerializeObject(new { data, contract });
                                break;
                            };


                        case NotificationType.ApprovePhase:
                            {
                                var data = db.Phases.Find(item.ObjectId);
                                var contract = db.Contracts.Find(data.ContractId);
                                json = JsonConvert.SerializeObject(new { data, contract });
                                break;
                            };


                        case NotificationType.PassPhase:
                            {
                                var data = db.Phases.Find(item.ObjectId);
                                var contract = db.Contracts.Find(data.ContractId);
                                json = JsonConvert.SerializeObject(new { data, contract });
                                break;
                            };


                        case NotificationType.PassPhaseToP3:
                            {
                                var data = db.Phases.Find(item.ObjectId);
                                var contract = db.Contracts.Find(data.ContractId);
                                json = JsonConvert.SerializeObject(new { data, contract });
                                break;
                            };


                        case NotificationType.PhaseStaffAllDone:
                            {
                                var data = db.Phases.Find(item.ObjectId);
                                var contract = db.Contracts.Find(data.ContractId);
                                json = JsonConvert.SerializeObject(new { data, contract });
                                break;
                            };


                        case NotificationType.PhaseChangeStaffs:
                            {
                                var data = db.Phases.Find(item.ObjectId);
                                var contract = db.Contracts.Find(data.ContractId);
                                json = JsonConvert.SerializeObject(new { data, contract });
                                break;
                            };


                        case NotificationType.SubmitedToP3Manager:
                            {
                                var data = db.Phases.Find(item.ObjectId);
                                var contract = db.Contracts.Find(data.ContractId);
                                json = JsonConvert.SerializeObject(new { data, contract });
                                break;
                            };


                        case NotificationType.ApprovedbyP3Manager:
                            {
                                var data = db.Phases.Find(item.ObjectId);
                                var contract = db.Contracts.Find(data.ContractId);
                                json = JsonConvert.SerializeObject(new { data, contract });
                                break;
                            };


                        //case NotificationType.NewReport:
                        //    {
                        //        var data = db.Reports.Find(item.ObjectId);
                        //        var phase = db.Phases.Find(data.PhaseId);
                        //        var contract = db.Contracts.Find(phase.ContractId);
                        //        json = JsonConvert.SerializeObject(new { data, contract });
                        //        break;
                        //    };


                        case NotificationType.NewSettlement:
                            {

                                var data = db.Settlements.Find(item.ObjectId);
                                if (db.Contracts.FirstOrDefault(c => c.UID == data.ContractUID) != null)
                                {
                                    var contract = db.Contracts.FirstOrDefault(c => c.UID == data.ContractUID);
                                    if (contract != null)
                                        json = JsonConvert.SerializeObject(new { data, contract });
                                }
                                if (db.Quotes.FirstOrDefault(c => c.UID == data.ContractUID) != null)
                                {
                                    var quote = db.Quotes.FirstOrDefault(c => c.UID == data.ContractUID);
                                    if (quote != null)
                                        json = JsonConvert.SerializeObject(new { data, quote });
                                }
                                if (db.Contractas.FirstOrDefault(c => c.UID == data.ContractUID) != null)
                                {
                                    var contracta = db.Contractas.FirstOrDefault(c => c.UID == data.ContractUID);
                                    if (contracta != null)
                                        json = JsonConvert.SerializeObject(new { data, contracta });
                                }


                                break;
                            };


                        case NotificationType.RejectSettlement:
                            {
                                var data = db.Settlements.Find(item.ObjectId);
                                if (db.Contracts.FirstOrDefault(c => c.UID == data.ContractUID) != null)
                                {
                                    var contract = db.Contracts.FirstOrDefault(c => c.UID == data.ContractUID);
                                    if (contract != null)
                                        json = JsonConvert.SerializeObject(new { data, contract });
                                }
                                if (db.Quotes.FirstOrDefault(c => c.UID == data.ContractUID) != null)
                                {
                                    var quote = db.Quotes.FirstOrDefault(c => c.UID == data.ContractUID);
                                    if (quote != null)
                                        json = JsonConvert.SerializeObject(new { data, quote });
                                }
                                if (db.Contractas.FirstOrDefault(c => c.UID == data.ContractUID) != null)
                                {
                                    var contracta = db.Contractas.FirstOrDefault(c => c.UID == data.ContractUID);
                                    if (contracta != null)
                                        json = JsonConvert.SerializeObject(new { data, contracta });
                                }


                                break;
                            };


                        case NotificationType.ApproveSettlement:
                            {
                                var data = db.Settlements.Find(item.ObjectId);
                                if (db.Contracts.FirstOrDefault(c => c.UID == data.ContractUID) != null)
                                {
                                    var contract = db.Contracts.FirstOrDefault(c => c.UID == data.ContractUID);
                                    if (contract != null)
                                        json = JsonConvert.SerializeObject(new { data, contract });
                                }
                                if (db.Quotes.FirstOrDefault(c => c.UID == data.ContractUID) != null)
                                {
                                    var quote = db.Quotes.FirstOrDefault(c => c.UID == data.ContractUID);
                                    if (quote != null)
                                        json = JsonConvert.SerializeObject(new { data, quote });
                                }
                                if (db.Contractas.FirstOrDefault(c => c.UID == data.ContractUID) != null)
                                {
                                    var contracta = db.Contractas.FirstOrDefault(c => c.UID == data.ContractUID);
                                    if (contracta != null)
                                        json = JsonConvert.SerializeObject(new { data, contracta });
                                }


                                break;
                            };


                        case NotificationType.PassSettlement:
                            {
                                var data = db.Settlements.Find(item.ObjectId);
                                if (db.Contracts.FirstOrDefault(c => c.UID == data.ContractUID) != null)
                                {
                                    var contract = db.Contracts.FirstOrDefault(c => c.UID == data.ContractUID);
                                    if (contract != null)
                                        json = JsonConvert.SerializeObject(new { data, contract });
                                }
                                if (db.Quotes.FirstOrDefault(c => c.UID == data.ContractUID) != null)
                                {
                                    var quote = db.Quotes.FirstOrDefault(c => c.UID == data.ContractUID);
                                    if (quote != null)
                                        json = JsonConvert.SerializeObject(new { data, quote });
                                }
                                if (db.Contractas.FirstOrDefault(c => c.UID == data.ContractUID) != null)
                                {
                                    var contracta = db.Contractas.FirstOrDefault(c => c.UID == data.ContractUID);
                                    if (contracta != null)
                                        json = JsonConvert.SerializeObject(new { data, contracta });
                                }


                                break;
                            };


                        case NotificationType.SubmitRecord:
                            {
                                var data = db.ExperimentRecords.Find(item.ObjectId);
                                var phase = db.Phases.Find(data.PhaseId);
                                var contract = db.Contracts.Find(phase.ContractId);
                                json = JsonConvert.SerializeObject(new { data, contract });
                                break;
                            };


                        case NotificationType.RejectRecord:
                            {
                                var data = db.ExperimentRecords.Find(item.ObjectId);
                                var phase = db.Phases.Find(data.PhaseId);
                                var contract = db.Contracts.Find(phase.ContractId);
                                json = JsonConvert.SerializeObject(new { data, contract });
                                break;
                            };


                        case NotificationType.ApproveRecord:
                            {
                                var data = db.ExperimentRecords.Find(item.ObjectId);
                                var phase = db.Phases.Find(data.PhaseId);
                                var contract = db.Contracts.Find(phase.ContractId);
                                json = JsonConvert.SerializeObject(new { data, contract });
                                break;
                            };


                        case NotificationType.PassQuote:
                            {
                                var data = db.Quotes.Find(item.ObjectId);
                                json = JsonConvert.SerializeObject(data);
                                break;
                            };


                        case NotificationType.SubmitQuoteRecord:
                            {
                                var data = db.QuoteRecords.Find(item.ObjectId);
                                var quote = db.Quotes.Find(data.QuoteId);
                                json = JsonConvert.SerializeObject(new { data, quote });
                                break;
                            };


                        case NotificationType.RejectQuoteRecord:
                            {
                                var data = db.QuoteRecords.Find(item.ObjectId);
                                var quote = db.Quotes.Find(data.QuoteId);
                                json = JsonConvert.SerializeObject(new { data, quote });
                                break;
                            };


                        case NotificationType.ApproveQuoteRecord:
                            {
                                var data = db.QuoteRecords.Find(item.ObjectId);
                                var quote = db.Quotes.Find(data.QuoteId);
                                json = JsonConvert.SerializeObject(new { data, quote });
                                break;
                            };


                        case NotificationType.SubmitToP2Director:
                            {
                                var data = db.Contractas.Find(item.ObjectId);
                                json = JsonConvert.SerializeObject(data);
                                break;
                            };


                        case NotificationType.RejectFromP2Director:
                            {
                                var data = db.Contractas.Find(item.ObjectId);
                                json = JsonConvert.SerializeObject(data);
                                break;
                            };


                        case NotificationType.SubmitToP5Director:
                            {
                                var data = db.Contractas.Find(item.ObjectId);
                                json = JsonConvert.SerializeObject(data);
                                break;
                            };

                        case NotificationType.RejectFromP5Director:
                            {
                                var data = db.Contractas.Find(item.ObjectId);
                                json = JsonConvert.SerializeObject(data);
                                break;
                            };

                        case NotificationType.P2SendP5:
                            {
                                var data = db.Contractas.Find(item.ObjectId);
                                json = JsonConvert.SerializeObject(data);
                                break;
                            };


                        case NotificationType.P5SendP2:
                            {
                                var data = db.Contractas.Find(item.ObjectId);
                                json = JsonConvert.SerializeObject(data);
                                break;
                            };

                        case NotificationType.AssignPCMBGTBL:
                            {
                                var data = db.Quotes.Find(item.ObjectId);
                                json = JsonConvert.SerializeObject(data);
                                break;
                            };
                        case NotificationType.AssignUserHDTBL:
                            {
                                var data = db.Quotes.Find(item.ObjectId);
                                json = JsonConvert.SerializeObject(data);
                                break;
                            };

                        case NotificationType.PublishPhase:
                            {
                                var data = db.Phases.Find(item.ObjectId);
                                json = JsonConvert.SerializeObject(data);
                                break;
                            };

                        case NotificationType.TBLSubmitPhase:
                        case NotificationType.TBLPassPhase:
                        case NotificationType.TBLPhaseStaffAllDone:
                       
                            {
                                var data = db.Phases.Find(item.ObjectId);
                                var quote = db.Quotes.Find(data.ContractId);
                                json = JsonConvert.SerializeObject(new {data,quote});
                                break;
                            };
                        case NotificationType.TBLNewReport:
                        case NotificationType.TBLApproveReport:
                        case NotificationType.TBLRejectReport:
                            {
                                var data = db.Reports.Find(item.ObjectId);
                                var quote = (from r in db.Reports
                                             join p in db.Phases on r.PhaseId equals p.Id
                                             join qu in db.Quotes on p.ContractId equals qu.Id
                                             where p.ProcessType == ProcessType.QT02
                                             select qu).FirstOrDefault();

                                json = JsonConvert.SerializeObject(new { data, quote });
                                break;
                            };

                        case NotificationType.NewReport:
                        case NotificationType.ApproveReport:
                        case NotificationType.RejectReport:
                            {
                                var data = db.Reports.Find(item.ObjectId);
                                var contract = (from r in db.Reports
                                             join p in db.Phases on r.PhaseId equals p.Id
                                             join qu in db.Contracts on p.ContractId equals qu.Id
                                             where p.ProcessType == ProcessType.QT01
                                             select qu).FirstOrDefault();

                                json = JsonConvert.SerializeObject(new { data, contract });
                                break;
                            };
                    }//switch

                    item.Data = json;
                }

                return entities;
            }
        }

        public async void Notify(int userId, string message, NotificationType notificationType = NotificationType.System, int objectId = 0)
        {
            if (userId == 0) return;
            Add(new Notification
            {
                Type = notificationType,
                UserId = userId,
                Message = message,
                ObjectId = objectId
            });

            var device = userDeviceRepository.FirstOrDefault(ud => ud.UserId == userId);
            if (device != null && !string.IsNullOrEmpty(device.DeviceToken))
            {
                //pushService.PushAsync(device.DeviceToken, notificationType.ToDescription(), message);//remove firebase
                await NPC.PMS.Framework.ExpoServer.NotificationService.PushAsync(new string[] { device.DeviceToken }, notificationType.ToDescription(), message);
            }
        }

        public void Notify(List<int> userIds, string message, NotificationType notificationType = NotificationType.System, int objectId = 0)
        {
            foreach (var userId in userIds)
            {
                Notify(userId, message, notificationType, objectId);
            }
        }

        public bool SetOpened(List<Guid> ids)
        {
            var entities = notificationRepository.Fetch(n => ids.Contains(n.Id));
            entities.ForEach(e => e.Opened = true);
            return notificationRepository.UpdateMany(entities);
        }
    }
}
