﻿using NPC.PMS.Domain.Data.Entity;
using NPC.PMS.Domain.Enums;
using NPC.PMS.Domain.Models.Notifications;
using System;
using System.Collections.Generic;

namespace NPC.PMS.Domain.Services.Common.Notifications
{
    public interface INotificationService
    {
        List<Notification> Filter(NotificationFilterModel model);
        bool SetOpened(List<Guid> ids);
        bool Delete(List<Guid> ids);
        Notification Add(Notification model);
        void Notify(int userId, string message, NotificationType notificationType = NotificationType.System, int objectId = 0);
        void Notify(List<int> userIds, string message, NotificationType notificationType = NotificationType.System, int objectId = 0);

    }
}