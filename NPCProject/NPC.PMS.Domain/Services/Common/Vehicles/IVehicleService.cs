﻿using NPC.PMS.Domain.Data;
using NPC.PMS.Domain.Models.Vehicles;
using System;
using System.Collections.Generic;
using System.Text;

namespace NPC.PMS.Domain.Services.Common.Vehicles
{
    public interface IVehicleService
    {
        List<VehicleModel> Filter(VehicleFilterModel filter);
        VehicleModel SaveModel(VehicleModel smodel);
        bool DeleteModel(int[] ids);
        VehicleModel Get(int id);
    }
}
