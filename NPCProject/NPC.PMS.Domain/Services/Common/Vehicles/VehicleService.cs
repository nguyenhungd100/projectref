﻿using NPC.PMS.Domain.Data;
using NPC.PMS.Domain.Data.Entity;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using NPC.PMS.Domain.Models.Vehicles;

namespace NPC.PMS.Domain.Services.Common.Vehicles
{
    public class VehicleService : IVehicleService
    {
        IRepository<Vehicle> vehicleRepository;
        public VehicleService(IRepository<Vehicle> vehicleRepository)
        {
            this.vehicleRepository = vehicleRepository;
        }

        public bool DeleteModel(int[] ids)
        {
            foreach (var id in ids)
            {
                var Transport = vehicleRepository.GetById(id);
                var del = vehicleRepository.Delete(Transport);
                if(!del) throw new ServiceException("Lỗi xóa id = " +id );
            }
            return true;
        }

        public VehicleModel Get(int id)
        {
            var entity = vehicleRepository.GetById(id);
            return entity.Map<VehicleModel>();
        }

        public List<VehicleModel> Filter(VehicleFilterModel filter)
        {
            using (var db = vehicleRepository.GetDbContext())
            {
                var q = from d in db.Vehicles select d;

                if(filter.AvailableFromTime.HasValue && filter.AvailableToTime.HasValue)
                {
                    var phases = db.Phases.Where(p => p.StartDate >= filter.AvailableFromTime.Value && p.EndDate <= filter.AvailableToTime.Value);
                    var phaseIds = phases.Select(p => p.Id).ToList();
                    var phaseVehicles = db.PhaseVehicles.Where(pv => phaseIds.Contains(pv.PhaseId));

                    var inuseVehicleIds = phaseVehicles.Select(pv => pv.VehicleId).ToList();
                    q = q.Where(v => !inuseVehicleIds.Contains(v.Id));
                }
                return q.ToList().Map<List<VehicleModel>>();
            }
        }

        public VehicleModel SaveModel(VehicleModel model)
        {
            if (model.Id > 0)
            {
                var entity = vehicleRepository.GetById(model.Id);
                entity.Number = model.Number;
                entity.DriverName = model.DriverName;
                entity.Seat = model.Seat;
                vehicleRepository.Update(entity);
                return entity.Map<VehicleModel>();
            }
            else
            {
                var entity = model.Map<Vehicle>();
                vehicleRepository.Insert(entity);
                return entity.Map<VehicleModel>();
            }
        }
    }
}
