﻿using System;
using System.Collections.Generic;
using System.Text;
using NPC.PMS.Domain.Data;
using NPC.PMS.Domain.Models.Customers;

namespace NPC.PMS.Domain.Services.Common.Customers
{
    public interface ICustomerService
    {
        CustomerFilterResult GetList(CustomerFilterModel filter);
        CustomerModel SaveModel(CustomerModel model);
        bool DeleteModel(int[] ids);
        CustomerModel Get(int id);
        decimal GetDebt(int customerId);
    }
}
