﻿using NPC.PMS.Domain.Data;
using NPC.PMS.Domain.Data.Entity;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using NPC.PMS.Domain.Models.Customers;

namespace NPC.PMS.Domain.Services.Common.Customers
{
    public class CustomerService :ICustomerService
    {
        IRepository<Customer> _customerRepository;
        public CustomerService(IRepository<Customer> CustomerRepositoty)
        {
            _customerRepository = CustomerRepositoty;
        }

        public bool DeleteModel(int[] ids)
        {
            foreach (var id in ids)
            {
                var customer = _customerRepository.GetById(id);
                var del = _customerRepository.Delete(customer);
                if (!del) throw new ServiceException("Lỗi xóa id = "+id);
            }
            return true;
        }

        public CustomerModel Get(int id)
        {
            var entity = _customerRepository.GetById(id);
            return entity.Map<CustomerModel>();
        }

        public CustomerFilterResult GetList(CustomerFilterModel filter)
        {
            var result = new CustomerFilterResult();
            using (var db = _customerRepository.GetDbContext())
            {
                var q = db.Customers.AsQueryable();

                if (!string.IsNullOrEmpty(filter.Name))
                {
                    q = q.Where(c => c.Name.Contains(filter.Name));
                }

                if (filter.IsExternalEVN.HasValue)
                {
                    q = q.Where(c => c.IsExternalEVN == filter.IsExternalEVN.Value);
                }

                if (filter.IsExternalEVNNPC.HasValue)
                {
                    q = q.Where(c => c.IsExternalEVN == filter.IsExternalEVNNPC.Value);
                }

                result.TotalRecord = q.Count();
                var entities = q.OrderByDescending(c => c.Id).Skip((filter.PageIndex - 1) * filter.PageSize).Take(filter.PageSize).ToList();
                result.Records = entities.Map<List<CustomerModel>>();
                result.Filter = filter;
                return result;
            }
        }

        public CustomerModel SaveModel(CustomerModel model)
        {
            if (model.Name == null) throw new ServiceException("Chưa nhập tên!");
            var customer = model.Map<Customer>();
            bool res = false;
            if (model.Id != 0) res = _customerRepository.Update(customer);
            else
            {
                var existName = _customerRepository.FirstOrDefault(c => c.Name == customer.Name);
                if (existName != null) throw new ServiceException("Tên khách hàng đã bị trùng");
                res = _customerRepository.Insert(customer);
            } 
            return customer.Map<CustomerModel>();
        }

        public decimal GetDebt(int customerId)
        {
            using (var context = _customerRepository.GetDbContext())
            {
                var billValueContactB = (from c in context.Customers.Where(c => c.Id == customerId)
                                 join ct in context.Contracts on c.Id equals ct.CustomerId
                                 join s in context.Settlements on ct.UID equals s.ContractUID
                                 select s.BillValue
                   ).Sum();

                var billValueQuote = (from c in context.Customers.Where(c => c.Id == customerId)
                                         join ct in context.Quotes on c.Id equals ct.CustomerId
                                         join s in context.Settlements on ct.UID equals s.ContractUID
                                         select s.BillValue
                  ).Sum();

                var billValue = billValueContactB + billValueQuote;
                var paymentContactB = (from c in context.Customers.Where(c => c.Id == customerId)
                                         join ct in context.Contracts on c.Id equals ct.CustomerId
                                         join s in context.Settlements on ct.UID equals s.ContractUID
                                         join sp in context.SettlementPayments on s.Id equals sp.SettlementId
                                         select sp.PaymentValue
                 ).Sum();

                var paymentQuote = (from c in context.Customers.Where(c => c.Id == customerId)
                                       join ct in context.Quotes on c.Id equals ct.CustomerId
                                       join s in context.Settlements on ct.UID equals s.ContractUID
                                       join sp in context.SettlementPayments on s.Id equals sp.SettlementId
                                       select sp.PaymentValue
                 ).Sum();

                decimal? result = billValue - paymentQuote - paymentContactB;
                if (result == null)
                    return 0;
                return result.Value;


            }
           
        }
    }
}
