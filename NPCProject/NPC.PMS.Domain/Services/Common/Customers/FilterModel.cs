﻿using NPC.PMS.Domain.Data;
using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;

namespace NPC.PMS.Domain.Services.Categories.Customers
{
    public class FilterModel
    {
        public string Name { get; set; }
        public PagingInfo Paging { get; set; }
        public string ConvertUTF8toText(string name) {
            string stFormD = name.Normalize(NormalizationForm.FormD);
            StringBuilder sb = new StringBuilder();
            for (int ich = 0; ich < stFormD.Length; ich++)
            {
                System.Globalization.UnicodeCategory uc = System.Globalization.CharUnicodeInfo.GetUnicodeCategory(stFormD[ich]);
                if (uc != System.Globalization.UnicodeCategory.NonSpacingMark)
                {
                    sb.Append(stFormD[ich]);
                }
            }
            string[] As = { "ă", "ắ", "ằ", "ẳ", "ặ", "ẵ", "â", "ấ", "ầ", "ẩ", "ẫ", "ậ", "á", "à", "ã", "ả", "ạ" },
                Es = { "ê", "ế", "ề", "ể", "ễ", "ệ", "é", "ẻ", "è", "ẽ", "ẹ" },
                Os = { "ó", "ò", "ỏ", "õ", "ọ", "ô", "ố", "ồ", "ổ", "ỗ", "ộ", "ơ", "ớ", "ờ", "ở", "ỡ", "ợ" },
                Is = { "í", "ì", "ỉ", "ĩ", "ị" },
                Ys = {"y","ý","ỳ","ỷ","ỹ","ỵ"},
                Us = { "ú", "ù", "ủ", "ũ", "ụ", "ư", "ừ", "ử", "ữ", "ự", "ứ" };
            sb = sb.Replace("Đ", "D");
            foreach(var a in As) sb = sb.Replace(a, "a");
            foreach (var e in Es) sb = sb.Replace(e, "e");
            foreach (var i in Is) sb = sb.Replace(i, "i");
            foreach (var o in Os) sb = sb.Replace(o, "o");
            foreach (var y in Ys) sb = sb.Replace(y, "y");
            foreach (var u in Us) sb = sb.Replace(u, "u");

            foreach (var a in As) sb = sb.Replace(a.ToUpper(), "a");
            foreach (var e in Es) sb = sb.Replace(e.ToUpper(), "e");
            foreach (var i in Is) sb = sb.Replace(i.ToUpper(), "i");
            foreach (var o in Os) sb = sb.Replace(o.ToUpper(), "o");
            foreach (var y in Ys) sb = sb.Replace(y.ToUpper(), "y");
            foreach (var u in Us) sb = sb.Replace(u.ToUpper(), "u");

            sb = sb.Replace('Đ', 'D');
            sb = sb.Replace('đ', 'd');
            return (sb.ToString().Normalize(NormalizationForm.FormD));
        }
    }
}
