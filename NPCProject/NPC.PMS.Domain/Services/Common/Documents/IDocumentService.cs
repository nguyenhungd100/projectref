﻿using NPC.PMS.Domain.Data.Entity;
using NPC.PMS.Domain.Enums;
using NPC.PMS.Domain.Models.Documents;
using System;
using System.Collections.Generic;
using System.Text;

namespace NPC.PMS.Domain.Services.Common.Documents
{
    public interface IDocumentService
    {
        Document AddDocument(FileInfoModel fileInfoModel, StorageProvider storageProvider = StorageProvider.Binary);
        Document GetDocument(Guid id);
        byte[] GetBinaryData(long storageId);
        bool DeleteDocument(Guid id);
        List<Document> GetDocuments(List<Guid> ids);
    }
}
