﻿using System;
using System.Collections.Generic;
using System.Text;
using NPC.PMS.Domain.Data;
using NPC.PMS.Domain.Data.Entity;
using NPC.PMS.Domain.Enums;
using NPC.PMS.Domain.Models.Documents;
using System.Linq;

namespace NPC.PMS.Domain.Services.Common.Documents
{
    public class DocumentService : IDocumentService
    {
        private readonly IRepository<BinaryStorage> binaryRepository;
        private readonly IRepository<Document> documentRepository;

        public DocumentService(IRepository<BinaryStorage> binaryRepository, IRepository<Document> documentRepository)
        {
            this.binaryRepository = binaryRepository;
            this.documentRepository = documentRepository;
        }
        public Document AddDocument(FileInfoModel file, StorageProvider storageProvider = StorageProvider.Binary)
        {
            var storage = new BinaryStorage
            {
                Data = file.Bytes
            };
            binaryRepository.Insert(storage);

            var document = new Document
            {
                CreatedUtcTime = DateTime.Now,
                Mime = file.Mime,
                Provider = storageProvider,
                Size = file.Length,
                Title = file.Title,
                Status = FileStatus.New,
                StorageId = storage.Id
            };

            documentRepository.Insert(document);
            return document;
        }

        public bool DeleteDocument(Guid id)
        {
            var entity = documentRepository.GetById(id);
            if (entity.Provider == StorageProvider.Binary)
            {
                var binary = binaryRepository.GetById(entity.StorageId);
                binaryRepository.Delete(binary);
            }
            return documentRepository.Delete(entity);
        }

        public byte[] GetBinaryData(long storageId)
        {
            return binaryRepository.GetById(storageId).Data;
        }

        public Document GetDocument(Guid id)
        {
            return documentRepository.GetById(id);
        }

        public List<Document> GetDocuments(List<Guid> ids)
        {
            using (var context = documentRepository.GetDbContext())
            {
                var res = context.Documents.Where(c => ids.Contains(c.Id)).ToList();
                return res;

            }
        }
    }
}
