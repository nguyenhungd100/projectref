﻿using System;
using System.Collections.Generic;
using System.Text;
using NPC.PMS.Domain.Data;

namespace NPC.PMS.Domain.Services.ExecutedPlans
{
    public interface IExecutedPlanService
    {
        List<ExecutedPlanModel> GetListPlan(PagingInfo filter);
        ActionResults SavePlan(ExecutedPlanModel model);
    }
}
