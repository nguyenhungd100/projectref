﻿using System;
using System.Collections.Generic;
using System.Text;
using NPC.PMS.Domain.Data;
using NPC.PMS.Domain.Data.Entity;
using System.Linq;

namespace NPC.PMS.Domain.Services.ExecutedPlans
{
    public class ExecutedPlanService : IExecutedPlanService
    {
        IRepository<ExecutedPlan> _executedPlanReponsitory;
        public ExecutedPlanService(IRepository<ExecutedPlan> ExecutedPlanReponsitory)
        {
            _executedPlanReponsitory = ExecutedPlanReponsitory;
        }
        public List<ExecutedPlanModel> GetListPlan(PagingInfo filter)
        {
            using(var db = _executedPlanReponsitory.GetDbContext())
            {
                var q = from e in db.ExecutedPlans
                        select e;
                var plans = q.ToList();
                return plans.Map<List<ExecutedPlanModel>>();
            }
        }

        public ActionResults SavePlan(ExecutedPlanModel model)
        {
            if(model.Categories == null)
            {
                return new ActionResults() { Result = false, Error = "Chưa nhập hạng mục" };
            }
            if (model.Quatity == 0)
            {
                return new ActionResults() { Result = false, Error = "Chưa nhập số lượng" };
            }
            if (model.StartedDate == null)
            {
                return new ActionResults() { Result = false, Error = "Chưa nhập ngày bắt đầu" };
            }
            if (model.EndDate == null)
            {
                return new ActionResults() { Result = false, Error = "Chưa nhập ngày kết thúc" };
            }
            if (model.DepartmentId == 0)
            {
                return new ActionResults() { Result = false, Error = "Chưa nhập bộ phận chuyên môn" };
            }
            if(model.Id != 0)
            {
                var plan = model.Map<ExecutedPlan>();
                var res = _executedPlanReponsitory.Update(plan);
                return new ActionResults() { Result = res, Error = "Cập nhật thành công!" };
            }
            var pl = model.Map<ExecutedPlan>();
            var result = _executedPlanReponsitory.Insert(pl);
            return new ActionResults() { Result = result, Error = "Lưu thành công!" };
        }
    }
}
