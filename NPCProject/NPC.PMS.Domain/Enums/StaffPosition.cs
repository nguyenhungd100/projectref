﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace NPC.PMS.Domain.Enums
{
    public enum StaffPosition : int
    {
        [Description("Nhân viên")]
        Employee = 0,
        [Description("Phó phòng")]
        Deputy = 8,
        [Description("Trưởng phòng")]
        Director = 10
    }
}
