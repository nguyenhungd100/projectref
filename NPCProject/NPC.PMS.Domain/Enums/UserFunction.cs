﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace NPC.PMS.Domain.Enums
{
    public enum UserFunction : int
    {
        [Description("Phụ trách")]
        Handler = 1,
        [Description("Phối hợp")]
        Supporter = 2,        
    }
}
