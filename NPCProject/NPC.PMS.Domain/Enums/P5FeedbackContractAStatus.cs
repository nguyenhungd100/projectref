﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace NPC.PMS.Domain.Enums
{
    public enum P5FeedbackContractAStatus
    {
        [Description("Đạt")]
        Reached = 1,

        [Description("Không đạt")]
        NotAchieved = 5
    }
}
