﻿using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NPC.PMS.Domain.Enums
{
    /// <summary>
    /// Trạng thái tài khoản
    /// </summary>
    public enum UserStatus
    {
        /// <summary>
        /// Chưa kích hoạt
        /// </summary>
        [Description("Chưa kích hoạt")]
        NotActived = 1,

        /// <summary>
        /// Kích hoạt
        /// </summary>
        [Description("Kích hoạt")]
        Actived = 2,

        /// <summary>
        /// Bị vô hiệu
        /// </summary>
        [Description("Bị vô hiệu hóa")]
        Disabled = 3
    }
    public enum WorkLevel : int
    {
        [Description("Công nhân")]
        Worker = 5,
        [Description("Kỹ sư")]
        Engineer = 10
    }
}
