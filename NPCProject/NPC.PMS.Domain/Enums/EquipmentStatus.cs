﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace NPC.PMS.Domain.Enums
{
    public enum EquipmentStatus
    {
        [Description("Tốt")]
        AllRight = 1,

        [Description("Hỏng")]
        Awry = 5,

        [Description("Quá hạn")]
        OutOfDate = 10,

        [Description("Đang sửa")]
        Fixing = 15
    }
}
