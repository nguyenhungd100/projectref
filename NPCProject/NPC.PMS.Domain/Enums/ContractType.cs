﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace NPC.PMS.Domain.Enums
{
    public enum ContractType : int
    {
        [Description("Hợp đồng công ty")]
        CT = 1,
        [Description("Hợp đồng chi nhánh")]
        CN = 2
    }
}
