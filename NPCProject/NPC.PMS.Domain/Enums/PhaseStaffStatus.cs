﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NPC.PMS.Domain.Enums
{
    public enum PhaseStaffStatus
    {
        StillWork = 1,

        HasBeenReplaced = 5,

        Desist = 10
    }
}
