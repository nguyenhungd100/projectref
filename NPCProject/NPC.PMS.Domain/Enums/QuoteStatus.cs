﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace NPC.PMS.Domain.Enums
{
    public enum QuoteStatus : int
    {
        [Description("Bản nháp")]
        Draft = 1,
        [Description("Chờ duyệt")]
        Submited = 2,
        [Description("Trả lại")]
        Rejected = 3,
        [Description("Đã duyệt")]
        Approved = 5,
        [Description("Đã chuyển PCM")]
        Passed = 10,
        [Description("Đã đóng")]
        Closed = 20,
    }
}
