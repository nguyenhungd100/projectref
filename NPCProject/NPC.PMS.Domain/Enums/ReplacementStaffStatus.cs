﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NPC.PMS.Domain.Enums
{
    public enum ReplacementStaffStatus
    {
        /// <summary>
        /// Yêu cầu thay thế nhân sự
        /// </summary>
        RequestReplacement = 1,

        /// <summary>
        /// Bổ sung nhân sự
        /// </summary>
        RequestAddStaff = 5,


    }
}
