﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace NPC.PMS.Domain.Enums
{
    public enum PhaseStaffPosition : int
    {       
        [Description("An toàn viên")]
        Safety = 2,        
        [Description("Đội trưởng")]
        Captain = 4
    }
}
