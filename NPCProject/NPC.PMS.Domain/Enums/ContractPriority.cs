﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace NPC.PMS.Domain.Enums
{
    public enum ContractPriority : int
    {
        [Description("Trung bình")]
        NA = 0,
        [Description("Trung bình")]
        Normal = 1,
        [Description("Ưu tiên cao")]
        High = 2,
        [Description("Ưu tiên thấp")]
        Low = 3
    }
}
