﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace NPC.PMS.Domain.Enums
{
    public enum Department : int
    {
        [Description("Ban giám đốc")]
        BGD = 0,
        [Description("Văn phòng")]
        VP = 1,
        [Description("Kế hoạch đầu tư")]
        KHDT = 2,
        [Description("Tổ chức & Nhân sự")]
        TCNS = 3,
        [Description("Kỹ thuật")]
        KT = 4,
        [Description("Tài chính Kế toán")]
        TCKT = 5,
        [Description("Rơle")]
        RL = 11,
        [Description("Cao áp")]
        CA = 12,
        [Description("Đo lường")]
        DL = 13,
        [Description("Hóa")]
        H = 14,
        [Description("Công nghệ năng lượng")]
        CNNL = 15,
        [Description("Cơ điện")]
        CD = 16,
        [Description("Tự động hóa")]
        TDH = 17,
        [Description("An toàn")]
        AT = 21,
        [Description("Tủ bảng điện")]
        TBD = 22
    }
}
