﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace NPC.PMS.Domain.Enums
{
    public enum FormalitySelectContractA
    {
        [Description("Mua sắm sửa chữa")]
        ShoppingAndRepair = 1,

        [Description("Thuê dịch vụ")]
        HideServices = 5,

        [Description("Mua sắm nhỏ lẻ")]
        RetailShopping = 10,

        [Description("Mua sắm thường xuyên")]
        RegularShopping = 15,

        [Description("Khác")]
        AnotherChoice = 20
    }
}
