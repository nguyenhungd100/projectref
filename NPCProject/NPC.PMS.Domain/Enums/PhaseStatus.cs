﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace NPC.PMS.Domain.Enums
{
    public enum PhaseStatus : int
    {
        [Description("Bản nháp")]
        Draft = 1,
        [Description("Đã trình")]
        Submited = 5,
        [Description("Bị trả lại")]
        Rejected = 7,
        [Description("Đã duyệt")]
        Approved = 10,
        [Description("Đã chuyển PCM")]
        Passed = 15,
        [Description("Đã chuyển P3")]
        PassedToP3 = 17,
        [Description("Đã trình TP P3")]
        SubmitedToP3Manager = 18,
        [Description("Đã duyệt bởi TP P3")]
        ApprovedbyP3Manager = 19,
        [Description("Đã phát hành QĐ")]
        Published = 20,

        [Description("Hoàn thành")]
        Completed = 25
    }
}
