﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace NPC.PMS.Domain.Enums
{
    public enum ContractKind : int
    {
        [Description("Hợp đồng B")]
        B = 1,
        [Description("Hợp đồng A")]
        A = 2,
        [Description("Hợp đồng A của B")]
        AB = 3,
        [Description("Báo giá")]
        Quote = 4,
    }
}
