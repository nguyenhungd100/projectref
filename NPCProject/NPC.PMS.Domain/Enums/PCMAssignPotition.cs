﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace NPC.PMS.Domain.Enums
{
    public enum PCMAssignPotition
    {
        [Description("Thành viên")]
        Member = 1,

        [Description("Trưởng nhóm")]
        Leader = 3,       

    }
}
