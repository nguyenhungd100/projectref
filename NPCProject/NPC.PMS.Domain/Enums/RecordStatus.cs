﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace NPC.PMS.Domain.Enums
{
    public enum RecordStatus : int
    {
        [Description("Bản nháp")]
        Draft = 0,
        [Description("Chưa duyệt")]
        Submited =1,
        [Description("Trả lại")]
        Rejected = 2,
        [Description("Đã duyệt")]
        Approved = 3,
    }
}
