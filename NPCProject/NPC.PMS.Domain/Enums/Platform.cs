﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NPC.PMS.Domain.Enums
{
    public enum Platform : int
    {
        Android = 1,
        iOS = 2
    }
}
