﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NPC.PMS.Domain.Enums
{
    public enum StorageProvider
    {
        Disk = 1,
        Binary = 2
    }
}
