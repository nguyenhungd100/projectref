﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace NPC.PMS.Domain.Enums
{
    public enum ProcessType
    {
        [Description("Quy trình 01")]
        QT01 = 1,
        [Description("Quy trình 02")]
        QT02 = 2
    }
}
