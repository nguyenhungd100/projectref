﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace NPC.PMS.Domain.Enums
{
    public enum NotificationType : int
    {
        [Description("Tin nhắn từ hệ thống")]
        System = 1,
        [Description("Hợp đồng mới")]
        NewContract = 2,
        [Description("Hợp đồng chuyển sang P4")]
        PassContract = 5,
        [Description("Chỉ định chuyên viên")]
        AssignContractUser = 6,
        [Description("Chỉ định chuyên viên hợp đồng A")]
        AssignContractAUser = 7,
        SubmitPhase = 10,
        [Description("Kế hoạch đợt bị từ chôi")]
        RejectPhase = 11,
        [Description("Kế hoạch đợt được duyệt")]
        ApprovePhase = 12,
        [Description("Kế hoạch đợt đã chuyển sang PCM")]
        PassPhase = 13,
        [Description("Kế hoạch đợt đã chuyển sang P3")]
        PassPhaseToP3 = 15,
        [Description("Tất cả PCM đã xong thêm nhân sự cho đợt")]
        PhaseStaffAllDone = 14,
        [Description("Kế hoạch đợt có sự thay đổi nhân sự")]
        PhaseChangeStaffs = 16,
        [Description("Đã trình TP P3")]
        SubmitedToP3Manager = 18,
        [Description("Đã duyệt bởi TP P3")]
        ApprovedbyP3Manager = 19,
        [Description("Báo cáo mới")]
        NewReport = 20,
        [Description("Phê duyệt báo cáo")]
        ApproveReport = 21,
        [Description("Từ chối báo cáo")]
        RejectReport = 22,
        [Description("Hồ sơ thanh toán mới")]
        NewSettlement = 30,
        [Description("Hồ sơ thanh toán bị trả lại")]
        RejectSettlement = 31,
        [Description("Hồ sơ thanh toán được duyệt")]
        ApproveSettlement = 32,
        [Description("Hồ sơ thanh toán đã chuyển P5")]
        PassSettlement = 33,
        [Description("Trình duyệt biên bản thí nghiệm")]
        SubmitRecord = 40,
        [Description("Trả lại biên bản thí nghiệm")]
        RejectRecord = 41,
        [Description("Duyệt biên bản thí nghiệm")]
        ApproveRecord = 42,        
        [Description("Phiếu giao nhiệm vụ chuyển sang phòng chuyên môn")]
        PassQuote = 50,
        [Description("Trình duyệt biên bản phiếu giao nhiệm vụ")]
        SubmitQuoteRecord = 51,
        [Description("Trả lại biên bản phiếu giao nhiệm vụ")]
        RejectQuoteRecord = 52,
        [Description("Duyệt biên bản phiếu giao nhiệm vụ")]
        ApproveQuoteRecord = 53,
        [Description("Chỉ định nhân sự tham gia hợp đồng thiết bị lẻ")]
        AssignUserHDTBL = 54,
        [Description("Chỉ định phòng chuyên môn tham gia hợp đồng thiết bị lẻ")]
        AssignPCMBGTBL = 56,

        [Description("Chuyên viên phụ trách hợp đồng A gửi trưởng phòng P2")]
        SubmitToP2Director = 60,

        [Description("Trưởng phòng P2 từ chối hợp đồng A")]
        RejectFromP2Director = 65,

        [Description("Chuyên viên phụ trách P5 gửi hợp đồng A cho trưởng phòng P2")]
        SubmitToP5Director = 70,

        [Description("Chuyên viên phụ trách P5 gửi hợp đồng A cho trưởng phòng P2")]
        RejectFromP5Director = 75,

        [Description("Phòng kế hoạch đầu tư chuyển hồ sơ hợp đồng A sang phòng tài chính kế toán")]
        P2SendP5 = 80,

        [Description("Phòng tài chính kế toán chuyển hồ sơ hợp đồng A sang phòng kế hoạch đầu tư")]
        P5SendP2 = 85,

        [Description("Phát hành quyết định thành lập đội nhóm công tác")]
        PublishPhase = 90,

        [Description("Submit phase thiết bị lẻ")]
        TBLSubmitPhase = 100,
        [Description("Kế hoạch đợt đã chuyển sang PCM")]
        TBLPassPhase = 101,
        [Description("Tất cả PCM đã xong thêm nhân sự cho đợt")]
        TBLPhaseStaffAllDone = 102,
        [Description("Báo cáo mới")]
        TBLNewReport = 103,
        [Description("Phê duyệt báo cáo")]
        TBLApproveReport = 104,
        [Description("Từ chối báo cáo")]
        TBLRejectReport = 105,

    }
}
