﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace NPC.PMS.Domain.Enums
{
    public enum DocumentType : int
    {
        [Description("Hợp đồng")]
        Contract = 1,

        [Description("Báo giá")]
        Quotation = 2,

        [Description("Biên nhận và thanh toán")]
        Receipt = 3,
    }
}
