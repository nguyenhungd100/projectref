﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace NPC.PMS.Domain.Enums
{
    public enum ProfileListContractAStatus : int
    { 
       
        [Description("Đã hoàn thiện")]
        Completed = 1,

        [Description("Chưa hoàn thiện")]
        InCompleted = 5,

        [Description("Đã gửi P5")]
        SentP5 = 10,

        [Description("Đã được P5 đồng ý")]
        Passed = 15,

        [Description("Chưa được P5 đồng ý")]
        Failed = 20
    }
}
