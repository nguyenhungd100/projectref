﻿using System;
using System.Collections.Generic;
using System.Text;
using NPC.PMS.Domain.Enums;

namespace NPC.PMS.Domain.Models.Users
{
    public class UpdateTokenModel
    {
        public Platform OS { get; set; }
        public string DeviceId { get; set; }
        public string DeviceToken { get; set; }
    }
}
