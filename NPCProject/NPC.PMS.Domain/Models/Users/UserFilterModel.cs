using NPC.PMS.Domain.Enums;

namespace NPC.PMS.Domain.Models
{
    public class UserFilterModel : BaseFilterModel
    {
        public string Text { set; get; }

        public UserStatus? UserStatus { set; get; }

        public Department? DepartmentId { get; set; }

        public StaffPosition? Position { get; set; }
    }
}


