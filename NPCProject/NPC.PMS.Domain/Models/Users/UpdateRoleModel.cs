﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NPC.PMS.Domain.Models.Users
{
    public class UpdateRoleModel
    {
        public int Id { set; get; }   
        public string UserRoles { set; get; }
    }
}
