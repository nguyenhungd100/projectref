﻿using NPC.PMS.Domain.Data.Entity;
using System;
using System.Collections.Generic;
using System.Text;

namespace NPC.PMS.Domain.Models.Users
{
    public class FilterUserCalendarModel:BaseFilterModel
    {
        public DateTime? From { get; set; }
        public DateTime? To { get; set; }
        public UserCalendarType? Type { get; set; }
    }
}
