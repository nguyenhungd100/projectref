﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NPC.PMS.Domain.Models.Users
{
    public class StaffStateModel
    {
        public bool Busy { get; set; }
        public string FullName { get; set; }
        public int? DepartmentId { get; set; }
        public int StaffId { get; set; }
        public List<string> BusyPhases { get; set; } = new List<string>();
        public bool Offline { get; set; }
        public bool Break { get; set; }
    }
}
