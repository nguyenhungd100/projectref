﻿using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace NPC.PMS.Domain.Models.Users
{
    public class UpdateUserImageModel
    {
        public IFormFile Avartar { set; get; }

        [JsonIgnore]
        public long UserId { get; set; }
    }
}
