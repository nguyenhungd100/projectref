﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace NPC.PMS.Domain.Models.Users
{
    public class UserDelegacyModel
    {
        [JsonIgnore]
        public int DelegacyUserId { get; set; }

        public int DelegatedUserId { get; set; }

        public string DelegatedUserName { get; set; }
    }
}
