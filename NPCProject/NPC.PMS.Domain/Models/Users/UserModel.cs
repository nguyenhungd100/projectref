﻿
using Newtonsoft.Json;
using System;
using System.ComponentModel.DataAnnotations;
using NPC.PMS.Framework.Formatter;
using NPC.PMS.Domain.Enums;
using NPC.PMS.Framework.Extensions.Enums;

namespace NPC.PMS.Domain.Models
{
    public class UserModel : BaseModel
    {
        public string FullName { get; set; }
        public string Email { get; set; }
        public bool? Gender { get; set; }
        public string Mobile { get; set; }
        public UserStatus Status { get; set; }
        public string Avatar { get; set; }
        public string UserRoles { get; set; }
        public Department DepartmentId { get; set; }
        public string DepartmentName { get { return DepartmentId.ToDescription(); } }
        public StaffPosition Position { get; set; }
        public string PositionName { get { return Position.ToDescription(); } }
        public string Permissions { get; set; }
        public string RoleNames { get; set; }
        public int SafeLevel { get; set; }
        public WorkLevel? WorkLevel { get; set; }
        public string StatusName { get; set; }
    }
}



