﻿using NPC.PMS.Domain.Data.Entity;
using System;
using System.Collections.Generic;
using System.Text;

namespace NPC.PMS.Domain.Models.Notifications
{
    public class NotificationFilterModel : BaseFilterModel
    {
        public int UserId { get; set; }
    }

    public class NotificationFilterResult : BaseFilterResult<Notification>
    {
    }
}
