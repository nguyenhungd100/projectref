﻿using NPC.PMS.Domain.Data.Entity;
using System;
using System.Collections.Generic;
using System.Text;

namespace NPC.PMS.Domain.Models.Calendars
{
    public class UserCalendarModel:BaseModel
    {
        public int UserId { get; set; }
        public DateTime DateFrom { get; set; }
        public DateTime DateTo { get; set; }
        public UserCalendarType Type { get; set; }
        public string TypeName { get; set; }
        public string Note { get; set; }
    }
}
