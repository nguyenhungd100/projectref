﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NPC.PMS.Domain.Models.Calendars
{
    public class UserCalendarsListModel
    {
        public int UserId { get; set; }
        public string UserName { get; set; }
        public string DepartmentName { get; set; }
        public List<PhaseCalendarModel> phaseCalendars { get; set; }
        public List<UserCalendarModel> UserCalendars { get; set; }
    }
    public class PhaseCalendarModel
    {
        public DateTime StartTime { get; set; }
        public DateTime EndTime { get; set; }
    }
}
