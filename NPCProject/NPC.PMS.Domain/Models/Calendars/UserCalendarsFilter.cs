﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace NPC.PMS.Domain.Models.Calendars
{
    public class UserCalendarsFilter:BaseFilterModel
    {
        public int? UserId { get; set; }
        public int? DepartmentId { get; set; }
        public int? Mouth { get; set; }
        public int? Year { get; set; }

        [JsonIgnore]
        public int UserCurrentId { get; set; }
    }
}
