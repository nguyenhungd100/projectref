﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NPC.PMS.Domain.Models.Reports
{
  public  class SettlementP2ReportModel
    {
        public string Group { get; set; }
        public string Content { get; set; }
        public decimal? ContractValue { get; set; }
        /// <summary>
        /// giá trị thanh toàn trước năm report
        /// </summary>
        public decimal? PreviousPaymentValue { get; set; }
        /// <summary>
        /// Giá trị thanh toán trong năm.
        /// </summary>
        public decimal? PaymentValue { get; set; }
        /// <summary>
        /// giá trị quyết toán
        /// </summary>
        public decimal? FinalPaymentValue { get; set; }

        /// <summary>
        /// doanh thu
        /// </summary>
        public decimal? BillValue { get; set; }
        /// <summary>
        /// dự kiến quyết toán
        /// </summary>
        public  decimal? ExpectedFinalValue { get; set; }

        /// <summary>
        /// doanh thu dự kiên
        /// </summary>
        public decimal? BillValuePlan { get; set; }

        /// <summary>
        /// Tổng doanh thu
        /// </summary>
        public decimal? TotalBillValue { get; set; }
        public bool IsSummaryRow { get; set; }

    }
}
