﻿using Newtonsoft.Json;
using NPC.PMS.Domain.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace NPC.PMS.Domain.Models.Reports
{
    public class YieldDepartmentOneDayModel
    {     
        public int WorkitemId { get; set; }

        public string WorkitemTitle { get; set; }

        public decimal? CompletedNumber { get; set; }

        public List<YielDepartmentOneDayDetail> YielDepartmentOneDayDetails { get; set; } = new List<YielDepartmentOneDayDetail>();
    }
}
