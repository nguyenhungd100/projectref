﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NPC.PMS.Domain.Models.Reports
{
    public enum TypeContractTotalEnum
    {
        /// <summary>
        /// Hợp đồng B
        /// </summary>
        ContractB = 1,

        /// <summary>
        /// Báo giá hợp đồng B
        /// </summary>
        QuotationB = 5,

        /// <summary>
        /// Hợp đồng thiết bị lẻ
        /// </summary>
        ContractRetailEquipment =10,

        /// <summary>
        /// Báo giá thiêt bị lẻ
        /// </summary>
        QuoteRetailEquipment = 15,

        /// <summary>
        /// Biên nhận và thanh toán
        /// </summary>
        Receipt =20
    }
}
