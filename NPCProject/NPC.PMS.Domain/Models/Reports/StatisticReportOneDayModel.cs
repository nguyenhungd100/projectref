﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NPC.PMS.Domain.Models.Reports
{
    public class StatisticReportOneDayModel
    {
        public List<YieldDepartmentOneDayModel> YieldDepartmentOneDays { get; set; } = new List<YieldDepartmentOneDayModel>();

        public List<string> SyntheticNotes { get; set; } = new List<string>();
    }
}
