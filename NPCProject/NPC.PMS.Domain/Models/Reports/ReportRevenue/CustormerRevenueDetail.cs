﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NPC.PMS.Domain.Models.Reports.ReportRevenue
{
    public class CustormerRevenueDetail
    {
        public string CustomerName { get; set; }

        public int CustormerId { get; set; }

        public IEnumerable<ContentContractRevenueModel> Contents { get; set; } = new List<ContentContractRevenueModel>();

    }
}
