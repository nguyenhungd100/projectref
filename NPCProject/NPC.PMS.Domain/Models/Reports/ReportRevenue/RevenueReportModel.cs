﻿using NPC.PMS.Domain.Models.Reports.ReportDebt;
using System;
using System.Collections.Generic;
using System.Text;

namespace NPC.PMS.Domain.Models.Reports.ReportRevenue
{
    public class RevenueReportModel
    {
        public CompanyType CompanyType { get; set; }
        public string CustomerName { get; set; }
        public int CustomerId { get; set; }
        public string Content { get; set; }
        public string ContractCode { get; set; }
        public DateTime? ContractDate { get; set; }
        public string InvoiceCode { get; set; }
        public DateTime? InvoiceDate { get; set; }
        /// <summary>
        /// danh thu
        /// </summary>
        public decimal  ? Revenue { get; set; }
        /// <summary>
        /// thuế
        /// </summary>
        public decimal? Vat { get; set; }
        /// <summary>
        /// thành tiền
        /// </summary>
        public decimal? Value { get; set; }
        public string Note { get; set; }
        /// <summary>
        /// số quyết toán.
        /// </summary>
        public string PaymentNumber { get; set; }
        /// <summary>
        /// ngày quyết toán.
        /// </summary>
        public DateTime? PaymentDate { get; set; }
        public int? SettlementId { get; set; }
        public Guid UID { get; set; }
        public string STT { get; set; }
    }
}
