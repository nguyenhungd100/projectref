﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NPC.PMS.Domain.Models.Reports.ReportRevenue
{
    public class ContentContractRevenueModel
    {
        public int ContractId { get; set; }

        public Guid UID { get; set; }

        public DateTime SigningDate { get; set; }


    }
}
