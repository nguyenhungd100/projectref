﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NPC.PMS.Domain.Models.Reports.ReportRevenue
{
    public class InternalEVNRevenue
    {
        public List<InternalEVNNPCRevenue> InternalEVNNPCs { get; set; } = new List<InternalEVNNPCRevenue>();

        public List<ExternalEVNNPCRevenue> ExternalEVNNPCs { get; set; } = new List<ExternalEVNNPCRevenue>();
    }
}
