﻿using NPC.PMS.Domain.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace NPC.PMS.Domain.Models.Reports
{
    public class DepartmentOutputFilter
    {
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public int? DepartmentId { get; set; }

    }
}
