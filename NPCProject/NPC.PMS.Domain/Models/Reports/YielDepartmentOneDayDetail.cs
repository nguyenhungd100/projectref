﻿using NPC.PMS.Domain.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace NPC.PMS.Domain.Models.Reports
{
    public class YielDepartmentOneDayDetail
    {
        public Department DepartmentId { get; set; }

        public string DepartmentName { get; set; }

        public decimal CompletedNumber { get; set; } = 0;
    }
}
