﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NPC.PMS.Domain.Models.Reports.ReportExistSettlement
{
    public class CustomerSettlementInfo
    {
        public int CustomerId { get; set; }

        public string CustomerName { get; set; }
    }
}
