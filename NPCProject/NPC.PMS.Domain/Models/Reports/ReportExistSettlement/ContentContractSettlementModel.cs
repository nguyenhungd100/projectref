﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NPC.PMS.Domain.Models.Reports.ReportRevenue
{
    public class ContentContractSettlementModel
    {
        public int CustomerId { get; set; }

        public string CustomerName { get; set; }
        
        public int ContractId { get; set; }

        public string ContractCode { get; set; }

        public string ContractContent { get; set; }

        public TypeContractTotalEnum Type { get; set; }
        
        public Guid UID { get; set; }

        public DateTime SigningDate { get; set; }

        public List<string> MissingRecords { get; set; } = new List<string>();

        public string MissingRecordCollects { get; set; }

        public string Note { get; set; }
    }
}
