﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NPC.PMS.Domain.Models.Reports.ReportExistSettlement
{
    public class InternalEVNSettlement
    {
        public List<InternalEVNNPCSettlement> InternalEVNNPCs { get; set; } = new List<InternalEVNNPCSettlement>();

        public List<ExternalEVNNPCSettlement> ExternalEVNNPCs { get; set; } = new List<ExternalEVNNPCSettlement>();       
    }
}
