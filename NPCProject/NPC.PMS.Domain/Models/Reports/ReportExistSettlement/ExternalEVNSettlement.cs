﻿using NPC.PMS.Domain.Models.Reports.ReportRevenue;
using System;
using System.Collections.Generic;
using System.Text;

namespace NPC.PMS.Domain.Models.Reports.ReportExistSettlement
{
    public class ExternalEVNSettlement : CustomerSettlementInfo
    {
        public List<ContentContractSettlementModel> Contents { get; set; } = new List<ContentContractSettlementModel>();
    }
}
