﻿using NPC.PMS.Domain.Models.Reports.ReportRevenue;
using System;
using System.Collections.Generic;
using System.Text;

namespace NPC.PMS.Domain.Models.Reports.ReportExistSettlement
{
    public class ExistSettlementDetail
    {
        public int CustormerId { get; set; }

        public string CustormerName { get; set; }

        public IEnumerable<ContentContractSettlementModel> ContentContracts { get; set; } = new List<ContentContractSettlementModel>();
    }
}
