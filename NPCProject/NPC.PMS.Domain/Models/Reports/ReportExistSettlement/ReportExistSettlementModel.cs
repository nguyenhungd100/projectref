﻿using NPC.PMS.Domain.Models.Reports.ReportRevenue;
using System;
using System.Collections.Generic;
using System.Text;

namespace NPC.PMS.Domain.Models.Reports.ReportExistSettlement
{
    public class ReportExistSettlementModel
    {
        public InternalEVNSettlement InternalEVNs { get; set; } = new InternalEVNSettlement();

        public List<ExternalEVNSettlement> ExternalEVNs { get; set; } = new List<ExternalEVNSettlement>();
    }
}
