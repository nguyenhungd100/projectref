﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NPC.PMS.Domain.Models.Reports
{
    public class YieldDepartmentFilter
    {
        public DateTime? Date { get; set; }

        public Guid UID { get; set; }
    }
}
