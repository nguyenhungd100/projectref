﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NPC.PMS.Domain.Models.Reports.ReportDebt
{
    public class InternalEVNDebt 
    {
        public List<InternalEVNNPCDebt> InternalEVNNPCs { get; set; } = new List<InternalEVNNPCDebt>();

        public List<ExternalEVNNPCDebt> ExternalEVNNPCs { get; set; } = new List<ExternalEVNNPCDebt>();
    }
}
