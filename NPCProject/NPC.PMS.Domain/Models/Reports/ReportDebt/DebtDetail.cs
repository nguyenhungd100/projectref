﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NPC.PMS.Domain.Models.Reports.ReportDebt
{
    public class DebtDetail
    {
        public int CustormerId { get; set; }

        public string CustormerName { get; set; }

        public IEnumerable<ContentContractDebtModel> ContentContracts { get; set; } = new List<ContentContractDebtModel>();

    }
}
