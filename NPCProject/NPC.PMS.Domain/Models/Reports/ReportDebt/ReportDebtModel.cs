﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NPC.PMS.Domain.Models.Reports.ReportDebt
{
    public class ReportDebtModel
    {
        //public InternalEVNDebt InternalEVNs { get; set; } = new InternalEVNDebt();

        //public List<ExternalEVNDebt> ExternalEVNs { get; set; } = new List<ExternalEVNDebt>();


        public CompanyType CompanyType { get; set; }
        public string CustomerName { get; set; }
        public int CustomerId { get; set; }
        public string Content { get; set; }
        public string ContractCode { get; set; }
        public DateTime? ContractDate { get; set; }
        public string InvoiceCode { get; set; }
        public DateTime? InvoiceDate { get; set; }
        public decimal? OpeningStock { get; set; }
        public decimal? OpeningStockHave { get; set; }
        public decimal? ClosingStock { get; set; }
        public decimal? ClosingStockHave { get; set; }
        public decimal? Inperiod { get; set; }
        public decimal? InperiodHave { get; set; }
        public decimal? Value { get; set; }
        public int? SettlementId { get; set; }
        public Guid UID { get; set; }
        public string STT { get; set; }
    }

    public enum CompanyType {
        InternalEVN = 1,
        InternalEVNPC=2,
        ExternalEVNPC = 3,
        ExternalEVN = 4
    }
}
