﻿using NPC.PMS.Domain.Enums;
using NPC.PMS.Framework.Extensions.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace NPC.PMS.Domain.Models.Reports
{
    public class DepartmentOutputReportModel
    {
        public Department DepartmentId { get; set; }
        public string DepartmentName { get { return DepartmentId.ToDescription(); } }
        public DateTime ReportTime { get; set; }
        public int ReportId { get; set; }
        public int WorkitemId { get; set; }
        public int Quantity { get; set; }
        public decimal Price { get; set; }
        public decimal Money { get; set; }
        public string WorkitemTitle { get; set; }
        public string Group { get; set; }
        public string ContractCode { get; set; }
        public string ContractContent { get; set; }
        public int STT { get; set; }
    }
}
