﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NPC.PMS.Domain.Models.Reports
{
    public class SyntheticReportModel : ContractModel
    {
        public List<decimal> MonthlySettlementValues { get; set; }
        public decimal ThisYearSettlementValue { get; set; }
        public int PaperCount { get; internal set; }
        public decimal? Percentage { get; set; }
        public decimal LastYearSettlementValue { get; set; }
        public decimal? NextYearSettlementValue { get; set; }
    }

    public class SyntheticReportFilter
    {
        public int UserId { get; set; }
    }
}
