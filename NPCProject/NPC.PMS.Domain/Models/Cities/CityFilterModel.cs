using Newtonsoft.Json;
using System.Collections.Generic;

namespace NPC.PMS.Domain.Models
{
    public class CityFilterModel : BaseFilterModel
    {
        public string Search { set; get; }

        public int? RegionId { set; get; }

        [JsonIgnore]
        public List<int> CityIds { set; get; }
    }
}


