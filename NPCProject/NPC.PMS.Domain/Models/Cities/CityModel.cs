
using System;
namespace NPC.PMS.Domain.Models
{
    public class CityModel : BaseModel
    {
        public int? RegionId { get; set; }
        public DateTime AddedDate { set; get; }
        public String Title { set; get; }
        public String Description { set; get; }
        public Nullable<Int32> DisplayOrder { set; get; }
        public Boolean IsActive { set; get; }
        public String PermanLink { set; get; }

    }
}



