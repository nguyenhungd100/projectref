﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace NPC.PMS.Domain.Models.ContractB
{
    public class DeleteContractModel
    {
        public List<int> Ids { get; set; }
        [Required]
        public string Reason { get; set; }
    }
}
