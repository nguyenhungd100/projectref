﻿using NPC.PMS.Domain.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace NPC.PMS.Domain.Models.ContractB
{
    public class AssignModel
    {
        public int ContractId { get; set; }
        public ContractKind Kind { get; set; }
        public Department DepartmentId { get; set; }
        public UserFunction Function { get; set; }
        public ProcessType? ProcessType { get; set; }
        public List<int> UserIds { get; set; }
    }
}
