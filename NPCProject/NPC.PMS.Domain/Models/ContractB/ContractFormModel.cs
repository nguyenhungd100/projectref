﻿using Newtonsoft.Json;
using NPC.PMS.Domain.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace NPC.PMS.Domain.Models.ContractB
{
    public class ContractFormModel
    {
        public int? Id { get; set; }
        //[Required(ErrorMessage = "Mã hợp đồng là trường bắt buộc")]
        public string Code { get; set; }
        public string CustomerContractCode { get; set; }
        public ContractType Type { get; set; }
        public int? GroupId { get; set; }
        [Required(ErrorMessage = "Ngày ký là trường bắt buộc")]
        public DateTime SigningDate { get; set; }
        public int? CustomerId { get; set; }
        public decimal? ContractValue { get; set; }
        public string Branch { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        [StringLength(2000, ErrorMessage = "Không nhập quá 2000 kí tự")]
        public string Content { get; set; }
        public string Email { get; set; }
        public string ContactName { get; set; }
        public string ContactAddress { get; set; }
        [JsonIgnore]
        public int CreateBy { get; set; }
        public int? CompanyId { get; set; }
        public ContractPriority? Priority { get; set; }
        public DocumentType DocType { get; set; }
    }
}
