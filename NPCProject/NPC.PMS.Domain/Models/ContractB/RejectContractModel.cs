﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NPC.PMS.Domain.Models.ContractB
{
    public class RejectContractModel
    {
        public int ContractId { get; set; }
        public string Reason { get; set; }
    }
}
