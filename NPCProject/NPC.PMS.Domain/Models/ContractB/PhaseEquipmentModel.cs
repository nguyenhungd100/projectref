﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NPC.PMS.Domain.Models.ContractB
{
    public class PhaseEquipmentModel
    {
        public int Id { get; set; }
        public int PhaseId { get; set; }
        public int DepartmentId { get; set; }        
        public int EquipmentId { get; set; }
        public string Code { get; set; } // mã thiết bị

        public string Imei { get; set; }// số chế tạo
        public string NameForPCM { get; set; }// tên thiết bị theo phòng chuyên môn
        public string NameForCompany { get; set; }// Tên thiết bị theo công ty
    }
}
