﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NPC.PMS.Domain.Models.ContractB
{
    public class PhaseWorkitemModel : BaseModel
    {
        public int PhaseId { get; set; }
        public int DepartmentId { get; set; }
        public int WorkitemId { get; set; }
        public int Quantity { get; set; }
        public int CompletedQuantity { get; set; }
    }
}
