﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NPC.PMS.Domain.Models.ContractB
{
    public class WorkitemModel : BaseModel
    {
        public int Index { get; set; }
        public string No { get; set; }
        public string Title { get; set; }
        public string Unit { get; set; }
        public decimal Quantity { get; set; }
        public decimal Price { get; set; }
        public decimal Money { get; set; }
        public int ContractId { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public int? Completed { get; set; }
        public decimal? CompletedMoney { get; set; }
        public decimal? AcceptancedQuantity { get; set; }
        public decimal? AcceptancedMoney { get; set; }
        public bool? IsStem { get; set; }
        public decimal? RemainQuantity {
            get { if (Quantity == 0) {
                    return null;
                    }
                var res = Quantity - (Completed ?? 0);
                if (res < 0)
                    return 0;
                else
                    return res;

            }
        }
    }
}
