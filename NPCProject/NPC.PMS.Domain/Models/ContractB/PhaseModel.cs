﻿using Newtonsoft.Json;
using NPC.PMS.Domain.Enums;
using NPC.PMS.Framework.Extensions.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace NPC.PMS.Domain.Models.ContractB
{
    public class PhaseModel
    {
        public int? Id { get; set; }
        public int ContractId { get; set; }
        public DateTime? StartDate { get; set; }
        public int StartHour
        {
            get
            {
                if (!StartDate.HasValue) return 0;
                return StartDate.Value.Hour;
            }
        }
        public int StartMinute
        {
            get
            {
                if (!StartDate.HasValue) return 0;
                return StartDate.Value.Minute;
            }
        }
        public int EndHour
        {
            get
            {
                if (!EndDate.HasValue) return 0;
                return EndDate.Value.Hour;
            }
        }
        public int EndMinute
        {
            get
            {
                if (!EndDate.HasValue) return 0;
                return EndDate.Value.Minute;
            }
        }
        public DateTime? EndDate { get; set; }
        public PhaseStatus Status { get; set; }
        public string StatusName
        {
            get { return Status.ToDescription(); }
        }

        public string DepartmentNames { get; set; }
        public int WorkitemCount { get; internal set; }
        public int OfferedStaffCount { get; set; }
        public int AssignedStaffCount { get; set; }
        public string ContractNumber { get; set; }
        public string Departments { get; set; }
        public string DoneStaffsDepartments { get; set; }

        public List<int> DepartmentList { get; set; } = new List<int>();
        public string Title { get; set; }
        public string Note { get; set; }
        public int? No { get; set; }
        public int? CentreId { get; set; }
        public string RejectReason { get; set; }
        public Guid ContractUID { get; set; }
        public List<int> DoneStaffsDepartmentList { get; set; } = new List<int>();

        public bool IsModification { get; set; } = false;

        public bool PhaseStaffAllDone { get; set; }

        public int EquipmentCount { get; set; } = 0;
        public ProcessType ProcessType { get; set; } = ProcessType.QT01;
    }
}
