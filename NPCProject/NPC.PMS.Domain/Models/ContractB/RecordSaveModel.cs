﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace NPC.PMS.Domain.Models.ContractB
{
    public class RecordSaveModel
    {
        public int? Id { get; set; }
        [Required]
        public int PhaseId { get; set; }
        public List<string> FileNames { get; set; }
        public string Content { get; set; }
        public string Note { get; set; }

        [JsonIgnore]
        public int UserId { get; set; }
    }
}
