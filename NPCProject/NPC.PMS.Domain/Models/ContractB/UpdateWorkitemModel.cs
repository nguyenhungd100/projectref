﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace NPC.PMS.Domain.Models.ContractB
{
    public class UpdateWorkitemModel
    {
        [Description("Yêu chọn khối lượng để sửa")]
        public int? Id { get; set; }

        public string Title { get; set; }

        public string Unit { get; set; }

        public decimal? Quantity { get; set; }
        public decimal? Price { get; set; }
    }
}
