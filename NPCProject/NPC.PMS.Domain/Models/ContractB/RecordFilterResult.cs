﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NPC.PMS.Domain.Models.ContractB
{
    public class RecordFilterModel : BaseFilterModel
    {
        public int ContractId { get; set; }
    }
    public class RecordFilterResult : BaseFilterResult<RecordModel>
    {
    }
}
