﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NPC.PMS.Domain.Models.ContractB
{
    public class ContractGroupModel : BaseModel
    {
        public string Name { get; set; }
    }
}
