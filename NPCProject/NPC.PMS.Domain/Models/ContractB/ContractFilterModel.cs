﻿using Newtonsoft.Json;
using NPC.PMS.Domain.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace NPC.PMS.Domain.Models.ContractA
{
    public class ContractFilterModel : BaseFilterModel
    {
        public ContractStatus? Status { get; set; }
        public string Code { get; set; }
        [JsonIgnore]
        public int UserId { get; set; }
        public ContractType? ContractType { get; set; }
        public bool? HasViewAll { get; set; }
        public int? CustomerId { get; set; }
        public string ContractCode { get; set; }
        public string Content { get; set; }
        public int? GroupId { get; set; }

        /// <summary>
        /// Lọc theo hợp đồng hay báo giá
        /// </summary>
        public DocumentType? DocType { set; get; } = DocumentType.Contract;
    }
}
