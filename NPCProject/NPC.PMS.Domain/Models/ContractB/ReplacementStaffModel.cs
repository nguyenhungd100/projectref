﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace NPC.PMS.Domain.Models.ContractB
{
    public class ReplacementStaffModel
    {
        [Required(ErrorMessage = "Yêu cầu chọn kế hoạch đợt")]
        public int PhaseId { get; set; }

        [Required(ErrorMessage = "Yêu cầu chọn nhân sự")]
        public int StaffId { get; set; }

        public int? SubstituteId { get; set; }

    }
}
