﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace NPC.PMS.Domain.Models.ContractB
{
    public class CloneContractModel
    {
        [Required]
        public int Id { get; set; }
        [JsonIgnore]
        public int UserId { get; set; }
    }
}
