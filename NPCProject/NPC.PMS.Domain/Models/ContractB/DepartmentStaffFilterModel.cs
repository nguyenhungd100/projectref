﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NPC.PMS.Domain.Models.ContractB
{
    public class DepartmentStaffFilterModel
    {
        public int PhaseId { get; set; }
        public int DepartmentId { get; set; }
        public bool Offline { get; set; } = false;
        public bool Break { get; set; } = false;
        public bool Busy { get; set; } = false;
    }
}
