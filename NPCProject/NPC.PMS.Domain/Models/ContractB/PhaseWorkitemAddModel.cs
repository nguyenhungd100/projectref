﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NPC.PMS.Domain.Models.ContractB
{
    public class PhaseWorkitemAddModel
    {
        public int PhaseId { get; set; }
        public List<int> WorkitemIds { get; set; }
    }
}
