﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NPC.PMS.Domain.Models.ContractB
{
    public class MasterPlanModel : BaseModel
    {
        public int ContractId { get; set; }
        public int WorkitemId { get; set; }
        public int DepartmentId { get; set; }
        public bool Checked { get; set; }
    }
}
