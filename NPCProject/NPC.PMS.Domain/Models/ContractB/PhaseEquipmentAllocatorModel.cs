﻿using NPC.PMS.Domain.Enums;
using NPC.PMS.Framework.Extensions.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace NPC.PMS.Domain.Models.ContractB
{
    public class PhaseEquipmentAllocatorModel
    {
        public int Id { get; set; }

        public int PhaseId { get; set; }
        public int UserId { get; set; }

        public string UserName { get; set; }

        public Department DepartmentId { get; set; }
        public string DepartmentName { get { return DepartmentId.ToDescription(); } }
    }
}
