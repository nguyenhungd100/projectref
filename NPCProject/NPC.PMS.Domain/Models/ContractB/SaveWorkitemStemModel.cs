﻿using Newtonsoft.Json;
using NPC.PMS.Domain.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace NPC.PMS.Domain.Models.ContractB
{
    public class SaveWorkitemStemModel
    {
        public int? Id { get; set; }
        public string Title { get; set; }
        public decimal? Quantity { get; set; }
        public string Unit { get; set; }
        public decimal? Price { get; set; }

        public Guid UID { get; set; }
        public ProcessType? ProcessType { get; set; }
    }
}
