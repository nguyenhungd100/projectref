﻿using Newtonsoft.Json;
using NPC.PMS.Domain.Enums;
using NPC.PMS.Framework.Extensions.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace NPC.PMS.Domain.Models.ContractB
{
    public class PhaseStaffModel : BaseModel
    {
        public int PhaseId { get; set; }
        public int StaffId { get; set; }
        public string StaffName { get; set; }

        public PhaseStaffPosition? PositionInPhase { get; set; }       

        public PCMAssignPotition Position { get; set; }
        public string PositionName { get { return Position.ToDescription(); } }
        public string PotitionInPhaseName { get { return PositionInPhase.HasValue ? PositionInPhase.ToDescription() : string.Empty; } }
        public int DepartmentId { get; set; }
        public int SafeLevel { get; set; }
        public WorkLevel WorkLevel { get; set; }

        public DateTime? ReplaceDate { get; set; }

        public int? ReplaceUserId { get; set; }

        public string ReplaceUserName { get; set; }

        [JsonIgnore]
        public PhaseStaffStatus? Status { get; set; }

        [JsonIgnore]
        public bool? IsOriginal { get; set; }
    }
}
