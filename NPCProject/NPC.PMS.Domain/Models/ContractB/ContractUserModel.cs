﻿using NPC.PMS.Domain.Data.Entity;
using NPC.PMS.Framework.Extensions.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace NPC.PMS.Domain.Models.ContractB
{
    public class ContractUserModel : ContractUser
    {
        public string UserName { get; set; }
        public string DepartmentName { get { return DepartmentId.ToDescription(); } }
        public string FunctionName { get { return Function.ToDescription(); } }
    }
}
