﻿using System.Collections.Generic;
using NPC.PMS.Domain.Data.Entity;
using NPC.PMS.Domain.Enums;
using NPC.PMS.Framework.Extensions.Enums;

namespace NPC.PMS.Domain.Models.ContractB
{
    public class RecordModel : ExperimentRecord
    {
        public string UserName { get; set; }
        public string DepartmentName { get; set; }
        public List<string> FileList { get; set; }
        public int? PhaseNo { get; set; }
        public string StatusName { get { return Status.ToDescription(); } }
    }
}