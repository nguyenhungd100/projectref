﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NPC.PMS.Domain.Models.Documents
{
    public class FileInfoModel
    {
        public string Mime { set; get; }
        public string Title { set; get; }
        public byte[] Bytes { get; set; }
        public long Length { get; set; }
    }
}
