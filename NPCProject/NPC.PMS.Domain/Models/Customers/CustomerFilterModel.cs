﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NPC.PMS.Domain.Models.Customers
{
    public class CustomerFilterModel : BaseFilterModel
    {
        public string Name { get; set; }

        public bool? IsExternalEVN { get; set; }

        public bool? IsExternalEVNNPC { get; set; }
    }
}
