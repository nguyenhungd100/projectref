﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NPC.PMS.Domain.Models.Vehicles
{
    public class VehicleFilterModel
    {
        public DateTime? AvailableFromTime { get; set; }
        public DateTime? AvailableToTime { get; set; }
        public int? Seat { get; set; }
    }
}
