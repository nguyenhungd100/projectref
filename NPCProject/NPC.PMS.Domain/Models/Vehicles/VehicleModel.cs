﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace NPC.PMS.Domain.Models.Vehicles
{
    public class VehicleModel
    {
        public int Id { get; set; }
        [Required(ErrorMessage = "Chưa nhập số chỗ")]
        public int Seat { get; set; }
        [Required(ErrorMessage = "Chưa nhập biển xe")]
        public string Number { get; set; }
        [Required(ErrorMessage = "Chưa nhập lái xe")]
        public string DriverName { get; set; }

        public bool IsCarRental { get; set; }
    }
}
