﻿using NPC.PMS.Domain.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace NPC.PMS.Domain.Models.ContractA
{
    public class ProfileListContractADefinedModel : BaseModel
    {
        public FormalitySelectContractA FormalityCode { get; set; }

        public string Name { get; set; }
    }
}
