﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NPC.PMS.Domain.Models.ContractA
{
    public class FormalitySelectDefined
    {
        public int FormalityCode { get; set; }

        public string Title { get; set; }
    }
}
