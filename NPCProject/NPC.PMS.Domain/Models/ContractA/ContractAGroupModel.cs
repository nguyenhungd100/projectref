﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NPC.PMS.Domain.Models.ContractA
{
    public class ContractAGroupModel : BaseModel
    {
        public string Name { get; set; }
    }
}
