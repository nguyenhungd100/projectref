﻿using NPC.PMS.Domain.Enums;
using NPC.PMS.Framework.Extensions.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace NPC.PMS.Domain.Models.ContractA
{
    public class ProfileListContractAModel : BaseModel
    {
        public Guid ContractAUID { get; set; }

        public FormalitySelectContractA FormalityCode { get; set; }

        public string Name { get; set; }

        public DateTime? PromisedDate { get; set; }

        public string Reply { get; set; }

        public string Feedback { get; set; }

        public ProfileListContractAStatus Status { get; set; }

        public P5FeedbackContractAStatus? FeedbackStatus { get; set; }

        public string FeedbackStatusName { get { return FeedbackStatus.HasValue? FeedbackStatus.ToDescription(): string.Empty; } } 

        public string StatusName { get; set; }
    }
}
