﻿using NPC.PMS.Domain.Data.Entity;
using NPC.PMS.Domain.Enums;
using NPC.PMS.Domain.Models.ContractB;
using NPC.PMS.Domain.Models.Settlements;
using NPC.PMS.Framework.Extensions.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace NPC.PMS.Domain.Models.ContractA
{
    public class ContractaModel
    {
        public int Id { get; set; }
        public Guid UID { get; set; }
        [StringLength(50, MinimumLength = 9)]
        public string Code { get; set; }
        [DataType(DataType.Currency)]
        public decimal Price { get; set; }
        [Required]
        public string Supplier { get; set; }
        [Required]
        public string Contents { get; set; }
        public string Description { get; set; }
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime DateCreated { get; set; }
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime? DateSign { get; set; }
        public ContractaStatus Status { get; set; }
        public string StatusName { get; set; }
        [Required]
        public int CreatedBy { get; set; }
        public string Files { get; set; }
        public string Logs { get; set; }
        //Readonly
        public List<SettlementDocumentModel> SettlementDocuments { get; set; }
        public List<SettlementModel> Settlements { get; set; }
        public List<string> FileList { get; set; }
        public int? RefContractId { get; set; }
        public string RefContractCode { get; set; }
        public Guid RefContractUID { get; set; }
        public string CreateByName { get; set; }

        public int? GroupId { get; set; }
        
        public FormalitySelectContractA? FormalityCode { get; set; }

        public string FormalitySelectName { get; set; }

        public IEnumerable<ProfileListContractAModel> ProfileListContractAs { get; set; }
        public List<ContractUserModel> ContractAUsers { get; set; }

        public int PlaceOn { get; set; }


        public List<Document> Documents { get; set; }

        public string Attachments { get; set; }
        public string GroupName { get; set; }
        /// <summary>
        /// Dư nợ đầu kỳ
        /// </summary>
        public decimal? OpeningBalance { get; set; }
        /// <summary>
        /// số tiền lũy kế thanh toán
        /// </summary>
        public decimal? BillAmount { get; set; }
        /// <summary>
        /// số tiền đã thanh toán
        /// </summary>
        public decimal? PaymentAmount { get; set; }
        /// <summary>
        /// Công nợ
        /// </summary>
        public decimal? Debt
        {
            get
            {
                if (BillAmount == null)
                    BillAmount = 0;
                if (PaymentAmount == null)
                    PaymentAmount = 0;
                return BillAmount - PaymentAmount;
            }
        }

    }
}
