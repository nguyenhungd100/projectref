﻿using NPC.PMS.Domain.Data.Entity;
using NPC.PMS.Domain.Enums;
using NPC.PMS.Domain.Models.Settlements;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace NPC.PMS.Domain.Models.ContractA
{
    public class ContractaSaveModel : BaseModel
    {
        public Guid UID { get; set; }

        public string Code { get; set; }
        [DataType(DataType.Currency)]
        public decimal? Price { get; set; }
        public string Supplier { get; set; }
        public string Contents { get; set; }
        public string Description { get; set; }
        public DateTime DateCreated { get; set; } = DateTime.Now;
        public DateTime? DateSign { get; set; }
        public ContractaStatus Status { get; set; }
        public int CreatedBy { get; set; }
        //public string Files { get; set; }
        public List<string> FileList { get; set; } = new List<string>();
        public string Logs { get; set; }
        public List<SettlementDocumentModel> SettlementDocuments { get; set; } = new List<SettlementDocumentModel>();

        public int? RefContractId { get; set; }
        public int? GroupId { get; set; }

        public FormalitySelectContractA? FormalityCode { get; set; }
    }
}
