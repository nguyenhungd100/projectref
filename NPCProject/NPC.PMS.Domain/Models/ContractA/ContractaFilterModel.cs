﻿using Newtonsoft.Json;
using NPC.PMS.Domain.Data.Entity;
using System;
using System.Collections.Generic;
using System.Text;

namespace NPC.PMS.Domain.Models.ContractA
{
    public class ContractaFilterModel: BaseFilterModel
    {
        public string ContractCode { get; set; }
        public string Content { get; set; }
        public string Supplier { get; set; }
        public ContractaStatus? Status { get; set; }
        public int? RefContractId { get; set; } // id hop dong thi cong
        [JsonIgnore]
        public int UserId { get; set; }
    }
}
