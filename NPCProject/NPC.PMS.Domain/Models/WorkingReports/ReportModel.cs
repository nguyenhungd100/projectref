﻿using NPC.PMS.Domain.Data.Entity;
using NPC.PMS.Domain.Models.ContractB;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace NPC.PMS.Domain.Models.Reports
{
    public class ReportModel
    {
        public int Id { get; set; }
        [Required]
        public int UserId { get; set; }
        [Required]
        public int PhaseId { get; set; }
        public string phaseName { get; set; }
        public DateTime Time { get; set; }
        public ReportStatus Status { get; set; }
        public string Content { get; set; }
        public string Logs { get; set; }
        public string StatusName { get; set; }
        public List<ReportitemModel> Reportitems { get; set; }
        public string StaffName { get; set; }
        public int? DepartmentId { get; set; }
    }
    public class ReportitemModel
    {
        public int Id { get; set; }
        [Required]
        public int ReportId { get; set; }
        [Required]
        public int WorkitemId { get; set; }
        [Required]
        // Số lượng báo cáo
        public int Quantity { get; set; }
        //Số lượng phan giao
        public int AssignedQuantity { get; set; }
        public string WorkitemTitle { get; set; }
    }
}
