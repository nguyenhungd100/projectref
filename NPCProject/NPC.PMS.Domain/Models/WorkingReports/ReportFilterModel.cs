﻿using NPC.PMS.Domain.Data.Entity;
using System;
using System.Collections.Generic;
using System.Text;

namespace NPC.PMS.Domain.Models.Reports
{
    public class ReportFilterModel:BaseFilterModel
    {
        public int PhaseId { get; set; }
        public ReportStatus Status { get; set; }
        public int ContractId { get; set; }
        public int DepartmentId { get; set; }
        public DateTime? Started { get; set; }
        public DateTime? End { get; set; }
    }
}
