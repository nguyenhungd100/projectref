﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NPC.PMS.Domain.Models.Reports
{
    public class RejectReportModel
    {
        public int ReportId { get; set; }
        public string Reason { get; set; }
    }
}
