﻿using NPC.PMS.Domain.Data.Entity;
using NPC.PMS.Domain.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace NPC.PMS.Domain.Models.Reports
{
    public class ReportInfoModel
    {
        public int Id { get; set; }
        public int UserId { get; set; }
        public int PhaseId { get; set; }
        public DateTime Time { get; set; }
        public ReportStatus Status { get; set; }
        public string StatusName { get; set; }
        public string Content { get; set; }

        public string UserName { get; set; }
        public string Department { get; set; }
        //public List<Workitem> Workitems { get; set; }
        public decimal ReportQuantity { get; set; }
        public decimal TotalAssignedQuantity { get; set; }
        public string Percent { get; set; }
        public int? PhaseNo { get; set; }
        public string ContractCode { get; set; }
    }
}
