﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NPC.PMS.Domain.Models.Reports.Mobile
{
    public class ReportFilterMobileModel : BaseFilterModel
    {
        public int? PhaseId { get; set; }
        public int? Status { get; set; }
        public DateTime? Started { get; set; }
        public DateTime? End { get; set; }
    }
}
