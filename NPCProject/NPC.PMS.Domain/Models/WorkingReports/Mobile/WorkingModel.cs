﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NPC.PMS.Domain.Models.Reports
{
    public class WorkingModel
    {
        public int ContractId { get; set; }
        public string ContractNumber { get; set; }
        public int PhaseId { get; set; }
        public string PhaseLabel { get; set; }
        public List<WorkingItem> Items { get; set; }
    }
    public class WorkingItem
    {
        public int WorkitemId { get; set; }
        public string Title { get; set; }
        public int Quantity { get; set; }
        public int Completed { get; set; }
    }
}
