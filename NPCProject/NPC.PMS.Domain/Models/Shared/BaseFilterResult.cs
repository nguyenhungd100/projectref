﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NPC.PMS.Domain.Models
{
    public class BaseFilterResult<T> where T : class
    {
        public BaseFilterModel Filter { get; set; }
        public IEnumerable<T> Records { get; set; }
        public int TotalRecord { get; set; }
        public int PageCount
        {
            get
            {
                if (Filter == null) return 0;
                var count = TotalRecord / Filter.PageSize + (TotalRecord % Filter.PageSize > 0? 1 : 0);
                return count;

            }
        }
        public int DisplayFrom
        {
            get
            {
                if (Filter == null) return 0;
                var from = (Filter.PageIndex - 1) * Filter.PageSize;
                return from;
            }
        }
        public int DisplayTo
        {
            get
            {
                if (Filter == null) return 0;
                if (Records.Count() < Filter.PageSize) return DisplayFrom + Records.Count();
                return DisplayFrom + Filter.PageSize;
            }
        }
    }
}
