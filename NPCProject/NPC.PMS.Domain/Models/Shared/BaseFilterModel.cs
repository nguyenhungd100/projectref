﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace NPC.PMS.Domain.Models
{
    public class BaseFilterModel
    {
        public string SortBy { get; set; }
        public bool SortDesc { get; set; }
        [Range(1, double.MaxValue)]
        public int PageIndex { get; set; } = 1;
        [Range(1, double.MaxValue)]
        public int PageSize { get; set; } = 20;
    }
}
