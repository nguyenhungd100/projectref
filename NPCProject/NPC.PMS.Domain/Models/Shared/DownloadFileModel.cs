﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NPC.PMS.Domain.Models.Shared
{
    public class DownloadFileModel
    {
        public string FilePath { get; set; }
        public string ContentType { get; set; }
        public string FileName { get; set; }

        public DownloadFileModel()
        {
            ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
        }
    }
}
