﻿using NPC.PMS.Domain.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace NPC.PMS.Domain.Models.Settlements
{
    public class SettlementFilterModel : BaseFilterModel
    {
        public Guid? ContractUID { get; set; }
        public int UserId { get; set; }
    }
}
