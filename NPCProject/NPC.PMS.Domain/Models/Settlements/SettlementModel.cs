﻿using NPC.PMS.Domain.Data.Entity;
using NPC.PMS.Framework.Extensions.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace NPC.PMS.Domain.Models.Settlements
{
    public class SettlementModel
    {
        public int Id { get; set; }
        public Guid? ContractUID { get; set; }
        public int No { get; set; }
        public decimal Value { get; set; }
        public DateTime CreatedOn { get; set; }
        public int CreatedBy { get; set; }
        public SettlemenStatus Status { get; set; }
        public string StatusName { get { return Status.ToDescription(); } }
        public string Note { get; set; }
        public List<SettlementCheckModel> SettlementChecks { get; set; }
        public string CreatedName { get; set; }
        public int? ReceiverId { get; set; }
        public string ReceiverName { get; set; }
        public DateTime? ReceivedTime { get; set; }
        public DateTime? BillDate { get; set; }
        public string BillCode { get; set; }
        public DateTime? PaymentDate { get; set; }
        public string PaymentNumber { get; set; }
        public string ContractCode { get; set; }
        public bool IsContractA { get; set; }
        public SettlementDocumentType DocumentType { get; set; }
        public decimal? ValueByBill { get; set; }
        public decimal? Vat { get; set; }
        public decimal? VatValue { get; set; }
        public decimal? BillValue { get; set; }
        /// <summary>
        /// tổng số tiền đã thanh toán
        /// </summary>
        public decimal? ReceivedValue { get; set; }
       
        public bool IsQuote { get; set; }
        public List<SettlementCheckModel> SettlementDocumentChecks { get; set; }
    }
    public class SettlementDocumentModel
    {
        public int Id { get; set; }
        public string DocumentName { get; set; }
        public SettlementDocumentType Type { get; set; }
    }
    public class SettlementCheckModel
    {
        public int Id { get; set; }
        public int SettlementId { get; set; }
        public int SettlementDocumentId { get; set; }
        public bool Checked { get; set; }
        public DocumentStatus? DocumentStatus { get; set; }
        public string P5Reply { get; set; }
        public string P5Comment { get; set; }
        public string P2Reply { get; set; }
        public string Attachments { get; set; }
        public List<Document> AttachmentDocuments { get; set; }

    }
}
