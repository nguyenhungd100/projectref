﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NPC.PMS.Domain.Models.Settlements
{
  public  class SettlementPaymentModel
    {
        public int Id { get; set; }
        public int? SettlementId { get; set; }
        public decimal? PaymentValue { get; set; }
        public DateTime? PaymentDate { get; set; }
        public string Note { get; set; }
    }
}
