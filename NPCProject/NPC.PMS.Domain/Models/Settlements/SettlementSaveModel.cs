﻿using NPC.PMS.Domain.Data.Entity;
using System;
using System.Collections.Generic;
using System.Text;

namespace NPC.PMS.Domain.Models.Settlements
{
    public class SettlementSaveModel
    {
        public int Id { get; set; }
        public Guid ContractUID { get; set; }
        public decimal Value { get; set; }
        public string Note { get; set; }
        public DateTime? BillDate { get; set; }
        public string BillCode { get; set; }
        public DateTime? PaymentDate { get; set; }
        public string PaymentNumber { get; set; }
        public List<DocumentCheckModel> Documents { get; set; }
        public List<SettlementCheckModel> SettlementDocumentChecks { get; set; }
    }
}
