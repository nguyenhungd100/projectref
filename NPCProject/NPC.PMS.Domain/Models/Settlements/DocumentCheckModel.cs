﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NPC.PMS.Domain.Models.Settlements
{
    public class DocumentCheckModel
    {
        public int DocumentId { get; set; }
        public bool Selected { get; set; }
    }
}
