﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NPC.PMS.Domain.Models.Quotes
{
    public class QuoteStaffModel
    {
        public int QuoteId { get; set; }

        public int StaffId { get; set; }

        public string StaffName { get; set; }

        public bool IsLeader { get; set; }

        public int SafeLevel { get; set; }

        public int? DepartmentId { get; set; }
    }
}
