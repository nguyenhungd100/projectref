﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace NPC.PMS.Domain.Models.Quotes
{
    public class QuoteRecordSaveModel
    {
        public int? Id { get; set; }
        [Required(ErrorMessage ="Yêu cầu chọn phiếu giao nhiệm vụ")]
        public int? QuoteId { get; set; }
        public List<string> FileNames { get; set; }
        public string Content { get; set; }
        public string Note { get; set; }

        [JsonIgnore]
        public int UserId { get; set; }
    }
}
