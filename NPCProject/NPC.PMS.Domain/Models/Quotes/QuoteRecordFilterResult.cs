﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NPC.PMS.Domain.Models.Quotes
{
    public class QuoteRecordFilterModel : BaseFilterModel
    {
        public int QuoteId { get; set; }
    }
    public class QuoteRecordFilterResult : BaseFilterResult<QuoteRecordModel>
    {
    }
}
