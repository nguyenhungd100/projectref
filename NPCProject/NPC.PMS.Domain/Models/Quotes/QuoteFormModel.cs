﻿using Newtonsoft.Json;
using NPC.PMS.Domain.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace NPC.PMS.Domain.Models.Quotes
{
    public class QuoteFormModel
    {
        public int? Id { get; set; }
        [Required(ErrorMessage = "Mã báo giá là trường bắt buộc")]
        public string Code { get; set; }
        
        [Required(ErrorMessage = "Ngày ký là trường bắt buộc")]
        public DateTime SigningDate { get; set; }
        public int? CustomerId { get; set; }
        public decimal? Value { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        [StringLength(2000, ErrorMessage = "Không nhập quá 2000 kí tự")]
        public string Content { get; set; }

        [JsonIgnore]
        public int CreateBy { get; set; }
        public DocumentType DocType { get; set; }
        public List<int> DepartmentList { get; set; } = new List<int>();
    }
}
