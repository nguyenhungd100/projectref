﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace NPC.PMS.Domain.Models.Quotes
{
    public class DeleteQuoteStaffModel
    {
        [Required(ErrorMessage = "Yêu cầu nhập mã báo giá")]
        public int? QuoteId { get; set; }

        [Required(ErrorMessage = "Yêu cầu nhập mã cán bộ")]
        public int? StaffId { get; set; }
    }
}
