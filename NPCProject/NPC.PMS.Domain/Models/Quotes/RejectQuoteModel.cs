﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NPC.PMS.Domain.Models.Quotes
{
    public class RejectQuoteModel
    {
        public int QuoteId { get; set; }

        public string Reason { get; set; }       
    }
}
