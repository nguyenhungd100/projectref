﻿using Newtonsoft.Json;
using NPC.PMS.Domain.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace NPC.PMS.Domain.Models.Quotes
{
    public class QuoteFilterModel : BaseFilterModel
    {
        public QuoteStatus? Status { get; set; }
        public string Code { get; set; }
        [JsonIgnore]
        public int UserId { get; set; }
        public bool? HasViewAll { get; set; }
        public int? CustomerId { get; set; }
        public string Content { get; set; }

        public DocumentType? DocType { set; get; } = DocumentType.Contract;
    }
}
