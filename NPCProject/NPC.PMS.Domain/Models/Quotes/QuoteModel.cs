﻿using Newtonsoft.Json;
using NPC.PMS.Domain.Data.Entity;
using NPC.PMS.Domain.Enums;
using NPC.PMS.Domain.Models.ContractB;
using NPC.PMS.Framework.Extensions.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace NPC.PMS.Domain.Models
{
    public class QuoteModel : BaseModel
    {
        public Guid UID { get; set; }
        public string Code { get; set; }

        public List<int> DepartmentList { get; set; } = new List<int>();

        public string DepartmentNames { get; set; }

        [JsonIgnore]
        public string Departments { get; set; }

        // public string DepartmentName { get { return DepartmentId.HasValue ? ((Department)DepartmentId).ToDescription() : string.Empty; } }

        public DocumentType DocType { get; set; } = DocumentType.Contract;
        public QuoteStatus Status { get; set; }
        public string StatusName { get { return Status.ToDescription(); } }

        public DateTime? SigningDate { get; set; }
        public int? CustomerId { get; set; }
        public decimal? Value { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public string Content { get; set; }
       
        public int? CreatedBy { get; set; }
        public DateTime CreatedOn { get; set; }
        public string TypeCode { get; set; }
        public string CustomerName { get; set; }
        public string Attachments { get; set; }
        public string Note { get; set; }
        public List<Document> Documents { get; set; }
        public string CustomerCode { get; set; }
        public string CreateByName { get; set; }

        public List<ContractUserModel> ContractUsers { get; set; }
        public List<Phase> Phases { get; set; }


        /// <summary>
        /// Dư nợ đầu kỳ
        /// </summary>
        public decimal? OpeningBalance { get; set; }
        /// <summary>
        /// số tiền lũy kế thanh toán
        /// </summary>
        public decimal? BillAmount { get; set; }
        /// <summary>
        /// số tiền đã thanh toán
        /// </summary>
        public decimal? PaymentAmount { get; set; }
        /// <summary>
        /// Công nợ
        /// </summary>
        public decimal? Debt
        {
            get
            {
                if (BillAmount == null)
                    BillAmount = 0;
                if (PaymentAmount == null)
                    PaymentAmount = 0;
                return BillAmount - PaymentAmount;
            }
        }
    }
}
