﻿using NPC.PMS.Domain.Data.Entity;
using NPC.PMS.Framework.Extensions.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace NPC.PMS.Domain.Models.Quotes
{
    public class QuoteRecordModel : QuoteRecord
    {
        public string UserName { get; set; }
        public string DepartmentName { get; set; }
        public List<string> FileList { get; set; }
        public string StatusName { get { return Status.ToDescription(); } }
    }
}
