﻿using Microsoft.Extensions.DependencyInjection;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using NPC.PMS.Domain.Data;
using NPC.PMS.Domain.Services;
using NPC.PMS.Domain.Services.Common.Debts;
using NPC.PMS.Domain.Services.Common.Customers;
using NPC.PMS.Domain.Services.Common.Devices;
using NPC.PMS.Domain.Services.Common.Vehicles;
using NPC.PMS.Domain.Services.Roles;
using NPC.PMS.Domain.Services.Users;
using NPC.PMS.Domain.Services.Common.Companies;
using NPC.PMS.Domain.Services.Settlements;
using NPC.PMS.Domain.Services.Common.Documents;
using NPC.PMS.Domain.Services.Reports;
using NPC.PMS.Domain.Services.Common.Notifications;
using NPC.PMS.Framework.Push;
using NPC.PMS.Domain.Services.Contracts;
using NPC.PMS.Domain.Services.ReportServices;

namespace NPC.PMS.Api.Configuration
{
    public static class DIConfigurator
    {
        public static void ConfigDI(this IServiceCollection services, IConfiguration configuration, AppSettings appSettings)
        {
            services
                .AddEntityFrameworkSqlServer()
                .AddDbContext<DomainContext>((serviceProvider, options) =>
                             options.UseSqlServer(configuration.GetConnectionString("DefaultConnection"))
                             .UseInternalServiceProvider(serviceProvider));
            services.AddTransient<IPushService>(s => new PushService(appSettings.FcmSettingFile));

            services.AddScoped(typeof(IRepository<>), typeof(Repository<>));
            services.AddScoped<IUserService, UserService>();
            services.AddScoped<IRolesService, RolesService>();
            services.AddScoped<IDepartmentService, DepartmentService>();
            services.AddScoped<IContractBService, ContractBService>();
            services.AddScoped<IDebtService, DebtService>();
            services.AddScoped<ICustomerService, CustomerService>();
            services.AddScoped<IEquipmentService, EquipmentService>();
            services.AddScoped<IVehicleService, VehicleService>();
            services.AddScoped<ICompanyService, CompanyService>();
            services.AddScoped<ISettlementService, SettlementService>();
            services.AddScoped<IDocumentService, DocumentService>();
            services.AddScoped<IWorkingReportService, WorkingReportService>();
            services.AddScoped<INotificationService, NotificationService>();
            services.AddScoped<IContractAService, ContractAService>();
            services.AddScoped<IContractReportService, ContractReportService>();
            services.AddScoped<ICalendarsService, CalendarsService>();
            services.AddScoped<IQuoteService, QuoteService>();

        }
    }
}
