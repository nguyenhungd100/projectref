﻿using System;
using System.Collections.Generic;
using System.IO;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Serilog;
using Swashbuckle.AspNetCore.Swagger;
using NPC.PMS.Api.Authorization;
using NPC.PMS.Api.Configuration;
using NPC.PMS.Api.Helpers;
using NPC.PMS.Domain.Services;
using NPC.PMS.Domain.Services.Roles;
using Swashbuckle.AspNetCore.SwaggerUI;
using Microsoft.AspNetCore.Mvc.Cors.Internal;
using Swashbuckle.AspNetCore.SwaggerGen;

namespace NPC.PMS.Api
{
    public class Startup
    {
        public Startup(IConfiguration configuration, IHostingEnvironment env)
        {
            Configuration = configuration;
            DomainMaps.Config();

            var builder = new ConfigurationBuilder()
                    .SetBasePath(Directory.GetCurrentDirectory())
                    .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true)
                    .AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: true)
                    .AddEnvironmentVariables();

            Configuration = builder.Build();

            Log.Logger = new LoggerConfiguration()
                        .ReadFrom.Configuration(Configuration)
                        //.WriteTo.Sentry("https://f65f908fde9940f0b2b16c2045643ea9@sentry.io/1260343")
                        .Enrich.FromLogContext()
                        .CreateLogger();
            //Log.Information("env.EnvironmentName:" + env.EnvironmentName);
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddCors(options =>
            {
                options.AddPolicy("AllowAll",
                    builder =>
                    {
                        builder
                        .AllowAnyOrigin()
                        .AllowAnyMethod()
                        .AllowAnyHeader()
                        .AllowCredentials();
                    });
            });

            services.Configure<MvcOptions>(options => {
                options.Filters.Add(new CorsAuthorizationFilterFactory("AllowAll"));
            });

            services
                .AddMvc(config =>
                {
                    config.Filters.Add(typeof(GlobalExceptionFilter));
                })
                .SetCompatibilityVersion(CompatibilityVersion.Version_2_1);
            services.AddRouting(options => options.LowercaseUrls = true);

            services.AddSwaggerGen(config =>
            {
                config.SwaggerDoc("v1", new Info { Title = "EVN NPC PMS API v1", Version = "V1" });
                config.IncludeXmlComments(string.Format("{0}\\NPC.PMS.Api.xml", AppDomain.CurrentDomain.BaseDirectory));
                config.IncludeXmlComments(string.Format("{0}\\NPC.PMS.Domain.xml", AppDomain.CurrentDomain.BaseDirectory));

                config.DescribeAllEnumsAsStrings();
                //config.CustomSchemaIds(x => x.FullName);
                config.DescribeAllEnumsAsStrings();
                config.AddSecurityDefinition("Bearer", new ApiKeyScheme()
                {
                    Description = "Authorization header using the Bearer scheme",
                    Name = "Authorization",

                    In = "header"
                });
                config.DocumentFilter<SwaggerSecurityRequirementsDocumentFilter>();
            });

            

            // Add our Config object so it can be injected
            var appSettingSection = Configuration.GetSection("AppSettings");
            services.Configure<AppSettings>(appSettingSection);
            var appSettings = new AppSettings();
            appSettingSection.Bind(appSettings);

            services.AddAuthentication("Bearer")
                   .AddIdentityServerAuthentication(options =>
                   {
                       options.Authority = appSettings.AuthenticationServer;
                       options.RequireHttpsMetadata = false;
                       options.ApiName = "npc_api";
                   });

            //Authorize user access
            services.AddAuthorization(options =>
            {
                var groups = PermissionHelper.GetPermissionGroups();
                var all = new List<Permission>();
                groups.ForEach(k => all.AddRange(k.Permissions));
                foreach (var permission in all)
                {
                    var policyName = permission.Code.ToString();
                    options.AddPolicy(policyName,
                        policy => policy.Requirements.Add(new PermissionRequirement(permission.Code)));
                }
            });

            //Inject storage user service
            services.ConfigDI(Configuration, appSettings);

            // Create an Autofac Container and push the framework services
            //var containerBuilder = new ContainerBuilder();
            //containerBuilder.Populate(services);
            //containerBuilder.RegisterModule(new DomainModule());
            //containerBuilder.RegisterModule(new FrameworkModule(
            //    Configuration.GetConnectionString("RedisConnection"), 
            //    appSettings.RedisDbNumber));

            // Build the container and return an IServiceProvider from Autofac
            //var container = containerBuilder.Build();
            //return container.Resolve<IServiceProvider>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            //app.UseCors(x => x
            //        .AllowAnyOrigin()
            //        .AllowAnyMethod()
            //        .AllowAnyHeader());

            app.AddSentryContext();

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseHsts();
            }

            app.UseAuthentication();
            //app.UseHttpsRedirection();

            app.UseCors("AllowAll");

            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=Index}/{id?}");
            });

            //implement docs for api
            app.UseSwagger();
            app.UseSwaggerUI(config =>
            {
                config.SwaggerEndpoint("v1/swagger.json", "NPC PMS Api V1");
                config.DocExpansion(DocExpansion.None);
                config.ShowExtensions();
            });
        }
    }
    public class SwaggerSecurityRequirementsDocumentFilter : IDocumentFilter
    {
        public void Apply(SwaggerDocument document, DocumentFilterContext context)
        {
            document.Security = new List<IDictionary<string, IEnumerable<string>>>()
            {
                new Dictionary<string, IEnumerable<string>>()
                {
                    { "Bearer", new string[]{ } },
                    { "Basic", new string[]{ } },
                }
            };
        }
    }
}
