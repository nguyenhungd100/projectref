﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using NPC.PMS.Api.Helpers;
using NPC.PMS.Domain.Models.Customers;
using NPC.PMS.Domain.Services.Common.Customers;
using Swashbuckle.AspNetCore.Annotations;

namespace NPC.PMS.Api.Modules.Categories.Customers
{
    public class CustomerController : Controller
    {
        ICustomerService _customerService;
        public CustomerController(ICustomerService CustomerService)
        {
            _customerService = CustomerService;
        }
        /// <summary>
        /// Lấy danh sách
        /// </summary>
        /// <param name="filter"></param>
        /// <returns></returns>
        [Authorize]
        [HttpPost("/api/customer/list")]
        [SwaggerResponse((int)System.Net.HttpStatusCode.OK, Type = typeof(ResultBase<CustomerFilterResult>))]
        public IActionResult List([FromBody]CustomerFilterModel filter)
        {
            var res = _customerService.GetList(filter);
            return Json(new { success = res != null, data = res });
        }
        /// <summary>
        /// Save
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost("/api/customer/save")]
        [Authorize]
        [SwaggerResponse((int)System.Net.HttpStatusCode.OK, Type = typeof(ResultBase<CustomerModel>))]
        public IActionResult Save([FromBody]CustomerModel model)
        {
            var res = _customerService.SaveModel(model);
            return Json(new { success = true, data = res });
        }

        /// <summary>
        /// Xóa
        /// </summary>
        /// <param name="ids"></param>
        /// <returns></returns>
        [HttpPost("/api/customer/delete")]
        [SwaggerResponse((int)System.Net.HttpStatusCode.OK, Type = typeof(ResultBase<CustomerModel>))]
        public IActionResult Delete([FromBody]int[] ids)
        {
            var res = _customerService.DeleteModel(ids);
            return Json(new { success = res });
        }

        /// <summary>
        /// Get customer
        /// </summary>
        /// <returns></returns>
        [HttpPost("/api/customer/{id}")]
        [SwaggerResponse((int)System.Net.HttpStatusCode.OK, Type = typeof(ResultBase<CustomerModel>))]
        public IActionResult Get(int id)
        {
            var res = _customerService.Get(id);
            return Json(new { success = true, data = res });
        }

        /// <summary>
        /// Lấy công nợ của KH
        /// </summary>
        /// <returns></returns>
        [HttpPost("/api/customer/GetDebt/{id}")]
        [SwaggerResponse((int)System.Net.HttpStatusCode.OK, Type = typeof(ResultBase<decimal>))]
        public IActionResult GetDebt(int id)
        {
            var res = _customerService.GetDebt(id);
            return Json(new { success = true, data = res });

        }
    }
}