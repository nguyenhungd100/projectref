﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using NPC.PMS.Api.Helpers;
using NPC.PMS.Domain.Data;
using NPC.PMS.Domain.Data.Entity;
using NPC.PMS.Domain.Services.Common.Devices;
using Swashbuckle.AspNetCore.Annotations;

namespace NPC.PMS.Api.Modules.Catalogs
{
    public class EquipmentController : Controller
    {
        IEquipmentService equipmentService;
        public EquipmentController(IEquipmentService equipmentService)
        {
            this.equipmentService = equipmentService;
        }
        /// <summary>
        /// Lấy danh sách
        /// </summary>
        /// <param name="filter"></param>
        /// <returns></returns>
        [HttpPost("/api/equipment/list")]
        [SwaggerResponse((int)System.Net.HttpStatusCode.OK, Type = typeof(ResultBase<EquipmentFilterResult>))]
        public IActionResult List([FromBody]EquipmentFilterModel filter)
        {
            var res = equipmentService.GetList(filter);
            return Json(new { success = res != null ? true : false, data = res });
        }
        /// <summary>
        /// Save
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost("/api/equipment/save")]
        [Authorize]
        [SwaggerResponse((int)System.Net.HttpStatusCode.OK, Type = typeof(ResultBase<EquipmentModel>))]
        public IActionResult Save([FromBody]EquipmentModel model)
        {
            var res = equipmentService.SaveModel(model);
            return Json(new { success = true, data = res });
        }
        /// <summary>
        /// Xóa
        /// </summary>
        /// <param name="ids"></param>
        /// <returns></returns>
        [HttpPost("/api/equipment/delete")]
        [SwaggerResponse((int)System.Net.HttpStatusCode.OK, Type = typeof(bool))]
        public IActionResult Delete([FromBody]int[] ids)
        {
            var res = equipmentService.Delete(ids);
            return Json(new { success = true });
        }

        /// <summary>
        /// Get avalable equipments
        /// </summary>
        /// <returns></returns>
        [Authorize]
        [HttpGet("/api/equipment/available")]
        [SwaggerResponse((int)System.Net.HttpStatusCode.OK, Type = typeof(ResultBase<List<Equipment>>))]
        public IActionResult GetAvailableEquipments()
        {
            var result = equipmentService.GetAvailable();
            return Json(new { success = true, data = result });
        }

        /// <summary>
        /// Get equipment
        /// </summary>
        /// <returns></returns>
        [Authorize]
        [HttpGet("/api/equipment/{id}")]
        [SwaggerResponse((int)System.Net.HttpStatusCode.OK, Type = typeof(ResultBase<EquipmentModel>))]
        public IActionResult Get(int id)
        {
            var result = equipmentService.Get(id);
            return Json(new { success = true, data = result });
        }

        [Authorize]
        [HttpPost("/api/equipment/import_excel")]
        [SwaggerResponse((int)System.Net.HttpStatusCode.OK, Type = typeof(ResultBase<bool>))]
        public IActionResult ImportExcelEquipment(IFormFile file)
        {
            var userId = HttpContext.User.Identity.GetUserId();
            var ms = new MemoryStream();
            file.CopyTo(ms);
            var bytes = ms.ToArray();
            var result = equipmentService.ImportExcelEquipment(bytes, userId);
            return Json(new { success = true, data = result });
        }

    }
}