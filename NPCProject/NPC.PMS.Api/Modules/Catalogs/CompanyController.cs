﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using NPC.PMS.Domain.Services.Common.Companies;
using Swashbuckle.AspNetCore.Annotations;

namespace NPC.PMS.Api.Modules.Catalogs
{
    public class CompanyController : Controller
    {
        ICompanyService _CompanyService;
        public CompanyController(ICompanyService CompanyService)
        {
            _CompanyService = CompanyService;
        }
        /// <summary>
        /// Get list
        /// </summary>
        /// <returns></returns>
        [HttpGet("/api/company/list")]
        [SwaggerResponse((int)System.Net.HttpStatusCode.OK, Type = typeof(List<CompanyModel>))]
        public IActionResult List()
        {
            var res = _CompanyService.GetList();
            return Json(new { success = res != null ? true : false, data = res });
        }
        /// <summary>
        /// Save
        /// </summary>
        /// <returns></returns>
        [HttpPost("/api/company/save")]
        [SwaggerResponse((int)System.Net.HttpStatusCode.OK, Type = typeof(CompanyModel))]
        public IActionResult Save([FromBody]CompanyModel model)
        {
            var res = _CompanyService.Save(model);
            return Json(new { success = true, data = res});
        }
        /// <summary>
        /// Delete
        /// </summary>
        /// <returns></returns>
        [HttpGet("/api/company/delete")]
        [SwaggerResponse((int)System.Net.HttpStatusCode.OK, Type = typeof(bool))]
        public IActionResult Delete(int id)
        {
            var res = _CompanyService.Delete(id);
            return Json(new { success = res });
        }
        /// <summary>
        /// Get By Id
        /// </summary>
        /// <returns></returns>
        [HttpGet("/api/company/get")]
        [SwaggerResponse((int)System.Net.HttpStatusCode.OK, Type = typeof(CompanyModel))]
        public IActionResult GetById(int id)
        {
            var res = _CompanyService.Get(id);
            return Json(new { success = res != null ? true : false, data = res });
        }
    }
}