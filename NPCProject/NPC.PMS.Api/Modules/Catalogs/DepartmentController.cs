using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using NPC.PMS.Domain.Services;
using Swashbuckle.AspNetCore.Annotations;
using NPC.PMS.Domain.Data.Entity;

namespace NPC.PMS.Api.Modules.Catalogs
{
    public class DepartmentController : Controller
    {
        IDepartmentService _departmentService;
        public DepartmentController(IDepartmentService DepartmentService)
        {
            _departmentService = DepartmentService;
        }
        /// <summary>
        /// Get divisions
        /// </summary>
        /// <returns></returns>
        [HttpGet("/api/department/list")]
        [SwaggerResponse((int)System.Net.HttpStatusCode.OK, Type = typeof(List<DepartmentModel>))]
        public IActionResult GetDivisions()
        {
            var result = _departmentService.GetDepartments();

            return Json(result);
        }

        /// <summary>
        /// Get technique divisions
        /// </summary>
        /// <returns></returns>
        [HttpGet("/api/department/techs")]
        [SwaggerResponse((int)System.Net.HttpStatusCode.OK, Type = typeof(List<DepartmentModel>))]
        public IActionResult GetTechDepartments()
        {
            var result = _departmentService.GetTechDepartments();

            return Json(result);
        }

        /// <summary>
        /// Get companies
        /// </summary>
        /// <returns></returns>
        [HttpGet("/api/department/companies")]
        [SwaggerResponse((int)System.Net.HttpStatusCode.OK, Type = typeof(Dictionary<int, string>))]
        public IActionResult GetCompanies()
        {
            var result = new Dictionary<int, string>();
            return Json(result);
        }

        /// <summary>
        /// Get companies
        /// </summary>
        /// <returns></returns>
        [HttpGet("/api/department/centres")]
        [SwaggerResponse((int)System.Net.HttpStatusCode.OK, Type = typeof(List<Centre>))]
        public IActionResult GetCentres()
        {
            var result = _departmentService.GetCentres();
            return Json(result);
        }
    }
}