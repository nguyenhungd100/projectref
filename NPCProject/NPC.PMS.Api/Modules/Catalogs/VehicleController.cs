﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using NPC.PMS.Domain.Data;
using NPC.PMS.Domain.Models.Vehicles;
using NPC.PMS.Domain.Services.Common.Vehicles;
using Swashbuckle.AspNetCore.Annotations;

namespace NPC.PMS.Api.Modules.Catalogs
{
    public class VehicleController : Controller
    {
        IVehicleService vehicleService;
        public VehicleController(IVehicleService vehicleService)
        {
            this.vehicleService = vehicleService;
        }
        /// <summary>
        /// Lấy danh sách
        /// </summary>
        /// <param name="filter"></param>
        /// <returns></returns>
        [HttpPost("/api/verhicle/list")]
        [SwaggerResponse((int)System.Net.HttpStatusCode.OK, Type = typeof(ResultBase<List<VehicleModel>>))]
        public IActionResult List([FromBody]VehicleFilterModel filter)
        {
            var res = vehicleService.Filter(filter);
            return Json(new ResultBase<List<VehicleModel>> { data = res });
        }
        /// <summary>
        /// Save
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost("/api/verhicle/save")]
        [Authorize]
        [SwaggerResponse((int)System.Net.HttpStatusCode.OK, Type = typeof(ResultBase<VehicleModel>))]
        public IActionResult Save([FromBody]VehicleModel model)
        {
            var res = vehicleService.SaveModel(model);
            return Json(new { success = true, data = res });
        }
        /// <summary>
        /// Xóa
        /// </summary>
        /// <param name="ids"></param>
        /// <returns></returns>
        [HttpPost("/api/verhicle/delete")]
        [SwaggerResponse((int)System.Net.HttpStatusCode.OK, Type = typeof(bool))]
        public IActionResult Delte(int[] ids)
        {
            var res = vehicleService.DeleteModel(ids);
            return Json(new  { success = res });
        }

        /// <summary>
        /// Get verhicle
        /// </summary>
        /// <returns></returns>
        [HttpGet("/api/verhicle/{id}")]
        [SwaggerResponse((int)System.Net.HttpStatusCode.OK, Type = typeof(ResultBase<VehicleModel>))]
        public IActionResult Delte(int id)
        {
            var res = vehicleService.Get(id);
            return Json(new { success = true, data = res });
        }
    }
}