﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using NPC.PMS.Api.Helpers;
using NPC.PMS.Domain.Data;
using NPC.PMS.Domain.Data.Entity;
using NPC.PMS.Domain.Services;
using NPC.PMS.Domain.Services.Common.Debts;
using Swashbuckle.AspNetCore.Annotations;

namespace NPC.PMS.Api.Modules.Catalogs
{
    public class DebtController : Controller
    {
        IDebtService _debtService;
        public DebtController(IDebtService DebtService)
        {
            _debtService = DebtService;
        }
        /// <summary>
        /// Lấy danh sách nợ công
        /// </summary>
        /// <param name="filter"></param>
        /// <returns></returns>
        [HttpPost("/api/debt/list")]
        [SwaggerResponse((int)System.Net.HttpStatusCode.OK, Type = typeof(List<DebtModel>))]
        public IActionResult List([FromBody]PagingInfo filter)
        {
            var res = _debtService.GetList(filter);
            return Json(new ResultBase<List<DebtModel>> { success = res != null ? true : false,data = res });
        }
        /// <summary>
        /// Save
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost("/api/debt/save")]
        [Authorize]
        [SwaggerResponse((int)System.Net.HttpStatusCode.OK, Type = typeof(DebtModel))]
        public IActionResult Save([FromBody]DebtModel model)
        {
            var userId = HttpContext.User.Identity.GetUserId();
            model.UserId = userId;
            var res = _debtService.SaveModel(model);
            return Json(new {success = true, data =res});
        }
        /// <summary>
        /// Xóa
        /// </summary>
        /// <param name="ids"></param>
        /// <returns></returns>
        [HttpPost("/api/debt/delete")]
        [SwaggerResponse((int)System.Net.HttpStatusCode.OK, Type = typeof(bool))]
        public IActionResult Delete(int[] ids)
        {
            var res = _debtService.DeleteModel(ids);
            return Json(new { success = res });
        }
        /// <summary>
        /// Get
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost("/api/debt/get")]
        [SwaggerResponse((int)System.Net.HttpStatusCode.OK, Type = typeof(DebtModel))]
        public IActionResult Get(int id)
        {
            var res = _debtService.Get(id);
            return Json(new { success = res !=null?true:false,data=res });
        }
    }
}