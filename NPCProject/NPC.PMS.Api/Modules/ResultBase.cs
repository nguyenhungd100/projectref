﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NPC.PMS.Api.Modules
{
    public class ResultBase<T>
    {
        public bool success { set; get; } = true;
        public T data { set; get; }
        public string message { set; get; }
    }
}