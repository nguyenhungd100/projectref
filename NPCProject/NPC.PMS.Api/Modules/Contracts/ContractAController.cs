using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using NPC.PMS.Api.Helpers;
using NPC.PMS.Domain.Models.ContractA;
using NPC.PMS.Domain.Models.ContractB;
using NPC.PMS.Domain.Models.Documents;
using NPC.PMS.Domain.Services;
using NPC.PMS.Domain.Services.Contracts;
using Swashbuckle.AspNetCore.Annotations;

namespace NPC.PMS.Api.Modules.Contracts
{
    public class ContractAController : Controller
    {
        private readonly IContractAService contractService;

        public ContractAController(IContractAService contractService)
        {
            this.contractService = contractService;
        }

        [HttpPost("/api/contracta/list")]
        [SwaggerResponse((int)System.Net.HttpStatusCode.OK, Type = typeof(List<ContractaModel>))]
        public IActionResult Filter([FromBody]ContractaFilterModel filter)
        {
            var userId = User.Identity.GetUserId();
            filter.UserId = userId;
            var result = contractService.Filter(filter);
            return Json(new { success = true, data = result });
        }

        [HttpPost("/api/contracta/listA")]
        [SwaggerResponse((int)System.Net.HttpStatusCode.OK, Type = typeof(ContractAFilterResult))]
        public IActionResult FilterContractA([FromBody]ContractaFilterModel filter)
        {
            var userId = User.Identity.GetUserId();
            filter.UserId = userId;
            var result = contractService.FilterContractA(filter);
            return Json(new { success = true, data = result });
        }

        [HttpPost("/api/contracta/listAB")]
        [SwaggerResponse((int)System.Net.HttpStatusCode.OK, Type = typeof(ContractAFilterResult))]
        public IActionResult FilterContractAB([FromBody]ContractaFilterModel filter)
        {
            var userId = User.Identity.GetUserId();
            filter.UserId = userId;
            var result = contractService.FilterContractAB(filter);
            return Json(new { success = true, data = result });
        }


        /// <summary>
        /// Get next contract code
        /// </summary>
        /// <returns></returns>
        [Authorize]
        [HttpGet("/api/contracta/nextcode/{date}")]
        [SwaggerResponse((int)System.Net.HttpStatusCode.OK, Type = typeof(ResultBase<string>))]
        public IActionResult NextCode(string date)
        {
            var result = contractService.NextCode(DateTime.Parse(date));
            return Json(new { success = true, data = result });
        }
        /// <summary>
        /// Save Contract A
        /// </summary>
        /// <returns></returns>
        [HttpPost("/api/contracta/save")]
        [Authorize]
        [SwaggerResponse((int)System.Net.HttpStatusCode.OK, Type = typeof(ContractaSaveModel))]
        public IActionResult Save([FromBody]ContractaSaveModel model)
        {
            var userId = HttpContext.User.Identity.GetUserId();
            model.CreatedBy = userId;
            if (!ModelState.IsValid) throw new Exception(ModelState.GetErrorsMessage());
            var result = contractService.Save(model);
            return Json(new { success = true, data = result });
        }
        /// <summary>
        /// Delete Contract A
        /// </summary> 
        /// <returns></returns>
        [HttpPost("/api/contracta/delete/{uid}")]
        [SwaggerResponse((int)System.Net.HttpStatusCode.OK, Type = typeof(bool))]
        public IActionResult Delete(Guid uid)
        {
            var result = contractService.Delete(uid);
            return Json(new { success = result });
        }
        /// <summary>
        /// Get contract A by id
        /// </summary>
        /// <param name="uid"></param>
        /// <returns></returns>
        [Authorize]
        [HttpGet("/api/contracta/get/{uid}")]
        [SwaggerResponse((int)System.Net.HttpStatusCode.OK, Type = typeof(ContractaModel))]
        public IActionResult Get(Guid uid)
        {
            var userId = HttpContext.User.Identity.GetUserId();
            var result = contractService.Get(uid, userId);
            return Json(new { success = true, data = result });
        }

        /// <summary>
        /// Upload contract documents
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [Authorize]
        [HttpPost("/api/contracta/{id}/attachments/upload")]
        [SwaggerResponse((int)System.Net.HttpStatusCode.OK, Type = typeof(ResultBase<List<Domain.Data.Entity.Document>>))]
        public IActionResult UploadFiles(int id)
        {
            var files = Request.Form.Files;
            var fileModels = new List<FileInfoModel>();
            foreach (var file in files)
            {
                var ms = new MemoryStream();
                file.CopyTo(ms);
                var bytes = ms.ToArray();
                fileModels.Add(new FileInfoModel
                {
                    Bytes = bytes,
                    Mime = file.ContentType,
                    Title = file.FileName,
                    Length = file.Length
                });
            }

            var result = contractService.AttachDocuments(id, fileModels);
            return Json(new { success = true, data = result });
        }

        /// <summary>
        /// Submit to P2 director (v2-p2)
        /// </summary>
        /// <param name="uid"></param>
        /// <returns></returns>
        [HttpPost("/api/contracta/submit_to_p2_director/{uid}")]
        [SwaggerResponse((int)System.Net.HttpStatusCode.OK, Type = typeof(ContractaModel))]
        public IActionResult SubmitToP2Director([FromRoute]Guid uid)
        {
            var userId = User.Identity.GetUserId();
            var result = contractService.SubmitToP2Director(uid, userId);
            return Json(new { Success = true, Data = result });
        }

        /// <summary>
        /// Reject contract from p2 director (v2-p2)
        /// </summary>
        /// <param name="uid"></param>
        /// <returns></returns>
        [HttpPost("/api/contracta/reject_contract_from_p2_director/{uid}")]
        [Authorize]
        [SwaggerResponse((int)System.Net.HttpStatusCode.OK, Type = typeof(ContractaModel))]
        public IActionResult RejectContractAFromP2Director([FromRoute]Guid uid)
        {
            var userId = User.Identity.GetUserId();
            var result = contractService.RejectContractAFromP2Director(uid, userId);
            return Json(new { Success = true, Data = result });
        }

        /// <summary>
        /// P2 send contract P5 (v2-p2)
        /// </summary>
        /// <param name="uid"></param>
        /// <returns></returns>
        [HttpPost("/api/contracta/p2_send_contract_p5/{uid}")]
        [Authorize]
        [SwaggerResponse((int)System.Net.HttpStatusCode.OK, Type = typeof(ContractaModel))]
        public IActionResult P2SendContractP5([FromRoute]Guid uid)
        {
            var userId = User.Identity.GetUserId();
            var result = contractService.P2SendContractP5(uid, userId);
            return Json(new { Success = true, Data = result });
        }

        /// <summary>
        /// Submit to P5 director (v2-p5)
        /// </summary>
        /// <param name="uid"></param>
        /// <returns></returns>
        [HttpPost("/api/contracta/submit_to_p5_director/{uid}")]
        [Authorize]
        [SwaggerResponse((int)System.Net.HttpStatusCode.OK, Type = typeof(ContractaModel))]
        public IActionResult SubmitToP5Director([FromRoute]Guid uid)
        {
            var userId = User.Identity.GetUserId();
            var result = contractService.SubmitToP5Director(uid, userId);
            return Json(new { Success = true, Data = result });
        }

        /// <summary>
        /// Reject from P5 director (v2-p5)
        /// </summary>
        /// <param name="uid"></param>
        /// <returns></returns>
        [HttpPost("/api/contracta/reject_from_p5_director/{uid}")]
        [Authorize]
        [SwaggerResponse((int)System.Net.HttpStatusCode.OK, Type = typeof(ContractaModel))]
        public IActionResult RejectFromP5Director([FromRoute]Guid uid)
        {
            var userId = User.Identity.GetUserId();
            var result = contractService.RejectFromP5Director(uid, userId);
            return Json(new { Success = true, Data = result });
        }

        /// <summary>
        /// P5 send contract P2 (v2-p5)
        /// </summary>
        /// <param name="uid"></param>
        /// <returns></returns>
        [HttpPost("/api/contracta/p5_send_contract_p2/{uid}")]
        [Authorize]
        [SwaggerResponse((int)System.Net.HttpStatusCode.OK, Type = typeof(ContractaModel))]
        public IActionResult P5SendContractP2([FromRoute]Guid uid)
        {
            var userId = User.Identity.GetUserId();
            var result = contractService.P5SendContractP2(uid, userId);
            return Json(new { Success = true, Data = result.Item1, message =  result.Item2});
        }
       
        /// <summary>
        /// P2 done contract
        /// </summary>
        /// <param name="uid"></param>
        /// <returns></returns>
        [HttpPost("/api/contracta/p2_done_completed/{uid}")]
        [Authorize]
        [SwaggerResponse((int)System.Net.HttpStatusCode.OK, Type = typeof(ContractaModel))]
        public IActionResult P2DoneCompleted([FromRoute]Guid uid)
        {
            var userId = User.Identity.GetUserId();
            var result = contractService.P2DoneCompleted(uid, userId);
            return Json(new { Success = true, Data = result });
        }



        /// <summary>
        /// Get contract A groups
        /// </summary>
        /// <returns></returns>
        [HttpGet("/api/contracta/groups")]
        [Authorize]
        [SwaggerResponse((int)System.Net.HttpStatusCode.OK, Type = typeof(IEnumerable<ContractAGroupModel>))]
        public IActionResult GetGroups()
        {
            var result = contractService.GetGroup();
            return Json(new { success = true, data = result });
        }

        /// <summary>
        /// Save contract A group
        /// </summary>
        /// <returns></returns>
        [HttpPost("/api/contracta/save_contracta_group")]
        [Authorize]
        [SwaggerResponse((int)System.Net.HttpStatusCode.OK, Type = typeof(IEnumerable<ContractAGroupModel>))]
        public IActionResult SaveContractAGroup([FromBody] ContractAGroupModel groupModel)
        {
            var result = contractService.SaveContractAGroup(groupModel);
            return Json(new { success = true, data = result });
        }

        /// <summary>
        /// Delete contract A group
        /// </summary>
        /// <param name="groupId"></param>
        /// <returns></returns>
        [HttpDelete("/api/contracta/delete_contracta-group/{groupId}")]
        [Authorize]
        [SwaggerResponse((int)System.Net.HttpStatusCode.OK, Type = typeof(bool))]
        public IActionResult DeleteContractAGroup([FromRoute]int groupId)
        {
            var result = contractService.DeleteContractAGroup(groupId);
            return Json(new { success = result });
        }

        /// <summary>
        ///  Get formality select defined
        /// </summary>
        /// <returns></returns>
        [HttpGet("/api/contracta/formality_select_defineds")]
        [Authorize]
        [SwaggerResponse((int)System.Net.HttpStatusCode.OK, Type = typeof(IEnumerable<FormalitySelectDefined>))]
        public IActionResult GetFormalitySelectDefined()
        {
            var result = contractService.GetFormalitySelectDefined();
            return Json(new { success = true, data = result });
        }

        /// <summary>
        /// Get profile list contracta defined 
        /// </summary>
        /// <returns></returns>
        [HttpGet("/api/contracta/profile_list_contracta_defineds/{formalityCode}")]
        [Authorize]
        [SwaggerResponse((int)System.Net.HttpStatusCode.OK, Type = typeof(IEnumerable<ProfileListContractADefinedModel>))]
        public IActionResult GetProfileListContractADefined(int formalityCode)
        {
            var result = contractService.GetProfileListContractADefined(formalityCode);
            return Json(new { success = true, data = result });
        }

        /// <summary>
        /// Delete profile list contract A
        /// </summary>
        /// <param name="deleteProfile"></param>
        /// <returns></returns>
        [HttpDelete("/api/contracta/delete_profile_list_contracta")]
        [Authorize]
        [SwaggerResponse((int)System.Net.HttpStatusCode.OK, Type = typeof(IEnumerable<bool>))]
        public IActionResult DeleteProfileListContractA([FromBody]DeleteProfileContractA deleteProfile)
        {
            var result = contractService.DeleteProfileListContractA(deleteProfile);
            return Json(new { success = result });
        }




        /// <summary>
        /// Save profile list contract A
        /// </summary>
        /// <param name="profileListContractAs"></param>
        /// <param name="uid"></param>p5
        /// <returns></returns>
        [HttpPost("/api/contracta/save_profile_list/{uid}")]
        [Authorize]
        [SwaggerResponse((int)System.Net.HttpStatusCode.OK, Type = typeof(IEnumerable<bool>))]
        public IActionResult SaveProfileList([FromBody]IEnumerable<ProfileListContractAModel> profileListContractAs, Guid uid)
        {
            var result = contractService.SaveProfileList(profileListContractAs, uid);
            return Json(new { success = result });
        }

        /// <summary>
        /// Submit profile list to p2 manager
        /// </summary>
        /// <param name="profileListContractAs"></param>
        /// <param name="uid"></param>
        /// <returns></returns>
        [HttpPost("/api/contracta/submit_profile_list_to_p2_manager/{uid}")]
        [Authorize]
        [SwaggerResponse((int)System.Net.HttpStatusCode.OK, Type = typeof(IEnumerable<bool>))]
        public IActionResult SumitProfileListToP2Manager([FromBody]IEnumerable<ProfileListContractAModel> profileListContractAs, Guid uid)
        {
            var result = contractService.SumitProfileListToP2Manager(uid);
            return Json(new { success = result });
        }




        /// <summary>
        /// Add users to contract A
        /// </summary>
        /// <returns></returns>
        [HttpPost("/api/contracta/assign")]
        [Authorize]
        [SwaggerResponse((int)System.Net.HttpStatusCode.OK, Type = typeof(IEnumerable<bool>))]
        public IActionResult Assign([FromBody] AssignModel assignModel)
        {
            var userId = User.Identity.GetUserId();
            var result = contractService.Assign(assignModel);
            return Json(new { success = true });
        }
        /// <summary>
        /// Remove user from contract A
        /// </summary>
        /// <returns></returns>
        [HttpPost("/api/contracta/unassign/{id}")]
        [Authorize]
        [SwaggerResponse((int)System.Net.HttpStatusCode.OK, Type = typeof(IEnumerable<bool>))]
        public IActionResult Unassign(int id)
        {
            var userId = User.Identity.GetUserId();
            var result = contractService.Unassign(id);
            return Json(new { success = true });
        }


    }
}