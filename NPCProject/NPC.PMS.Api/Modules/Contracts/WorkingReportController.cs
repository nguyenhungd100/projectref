﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using NPC.PMS.Api.Helpers;
using NPC.PMS.Domain.Models.ContractB;
using NPC.PMS.Domain.Models.Reports;
using NPC.PMS.Domain.Models.Reports.Mobile;
using NPC.PMS.Domain.Services;
using NPC.PMS.Domain.Services.Reports;
using Swashbuckle.AspNetCore.Annotations;

namespace NPC.PMS.Api.Modules.Reports
{
    public class WorkingReportController : Controller
    {
        private readonly IWorkingReportService reportingService;

        public WorkingReportController(IWorkingReportService reportingService)
        {
            this.reportingService = reportingService;
        }

        #region WEB
        /// <summary>
        /// Get phases of current user
        /// </summary>
        /// <returns></returns>
        [HttpGet("/api/report/phases")]
        [SwaggerResponse((int)System.Net.HttpStatusCode.OK, Type = typeof(List<PhaseModel>))]
        public IActionResult GetUserPhases()
        {
            var userId = User.Identity.GetUserId();
            var res = reportingService.GetUserPhases(userId);
            return Json(new { success = true, data = res });
        }

        /// <summary>
        /// Save báo cáo ngày
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost("/api/report/save")]
        [Authorize]
        [SwaggerResponse((int)System.Net.HttpStatusCode.OK, Type = typeof(ReportModel))]
        public IActionResult Save([FromBody]ReportModel model)
        {
            var userId = User.Identity.GetUserId();
            model.UserId = userId;
            if (!ModelState.IsValid) throw new Exception(ModelState.GetErrorsMessage());
            var res = reportingService.Save(model);
            return Json(new { success = true, data = res });
        }
        /// <summary>
        /// Get report by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("/api/report/{id}")]
        [Authorize]
        [SwaggerResponse((int)System.Net.HttpStatusCode.OK, Type = typeof(ReportModel))]
        public IActionResult Get(int id)
        {
            var userId = User.Identity.GetUserId();
            var res = reportingService.Get(id);
            return Json(new { success = true, data = res });
        }
        /// <summary>
        /// Filter report
        /// </summary>
        /// <param name="filter"></param>
        /// <returns></returns>
        [HttpPost("/api/report/get_list")]
        [SwaggerResponse((int)System.Net.HttpStatusCode.OK, Type = typeof(List<ReportInfoModel>))]
        public IActionResult GetList([FromBody]ReportFilterModel filter)
        {
            var res = reportingService.Filter(filter);
            return Json(new { success = true, data = res });
        }


        /// <summary>
        /// Phase report success
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost("/api/report/{id}/approve")]
        [SwaggerResponse((int)System.Net.HttpStatusCode.OK, Type = typeof(ResultBase<bool>))]
        public IActionResult ReportSuccess(int id)
        {
            var success = reportingService.Approve(id);
            return Json(new { success });
        }

        /// <summary>
        /// Phase report redirect
        /// </summary>
        /// <param name="id"></param>
        /// <param name="reason"></param>
        /// <returns></returns>
        [HttpPost("/api/report/{id}/reject")]
        [SwaggerResponse((int)System.Net.HttpStatusCode.OK, Type = typeof(ResultBase<bool>))]
        public IActionResult Reject(int id,[FromBody]RejectReportModel reason)
        {
            var success = reportingService.Reject(id, reason.Reason);
            return Json(new { success });
        }
        #endregion

        #region Mobile
        /// <summary>
        /// (Mobile)Get user phase
        /// </summary>
        /// <returns></returns>
        [HttpGet("/api/report/mobile/phases")]
        [Authorize]
        [SwaggerResponse((int)System.Net.HttpStatusCode.OK, Type = typeof(List<PhaseModel>))]
        public IActionResult GetUserPhase()
        {
            var userId = User.Identity.GetUserId();
            var res = reportingService.GetUserPhases(userId);
            return Json(new { success = true, data = res });
        }

        /// <summary>
        /// (Mobile)Get working info
        /// </summary>
        /// <param name="phaseId"></param>
        /// <returns></returns>
        [HttpGet("/api/report/mobile/assignments/{phaseId}")]
        [Authorize]
        [SwaggerResponse((int)System.Net.HttpStatusCode.OK, Type = typeof(List<WorkingModel>))]
        public IActionResult GetWorkingInfo(int phaseId)
        {
            var userId = User.Identity.GetUserId();
            var res = reportingService.GetWorkingInfo(userId, phaseId);
            return Json(new { success = true, data = res });
        }

        /// <summary>
        /// (Mobile)Get list report
        /// </summary>
        /// <returns></returns>
        [HttpPost("/api/report/mobile/filter")]
        [Authorize]
        [SwaggerResponse((int)System.Net.HttpStatusCode.OK, Type = typeof(List<ReportModel>))]
        public IActionResult MobileFilter([FromBody] ReportFilterMobileModel filter)
        {
            var userId = User.Identity.GetUserId();
            var res = reportingService.MobileFilter(userId,filter);
            return Json(new { success = true, data = res });
        }
        
        #endregion
    }
}