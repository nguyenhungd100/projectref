﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using NPC.PMS.Api.Helpers;
using NPC.PMS.Domain.Data;
using NPC.PMS.Domain.Data.Entity;
using NPC.PMS.Domain.Enums;
using NPC.PMS.Domain.Models;
using NPC.PMS.Domain.Models.ContractA;
using NPC.PMS.Domain.Models.ContractB;
using NPC.PMS.Domain.Models.Documents;
using NPC.PMS.Domain.Models.Vehicles;
using NPC.PMS.Domain.Services;
using NPC.PMS.Framework.Extensions.Enums;
using NPC.PMS.Framework.Utils;
using NPOI.OpenXml4Net.OPC;
using NPOI.XWPF.UserModel;
using Swashbuckle.AspNetCore.Annotations;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace NPC.PMS.Api.Modules.Contracts
{
    [Route("api/[controller]")]
    public class ContractBController : Controller
    {
        #region Ctor
        private readonly IHostingEnvironment env;
        private readonly IContractBService contractService;
        private readonly IRepository<Phase> phaseRepository;
        private readonly IRepository<PhaseStaff> phaseStaffRepository;
        private readonly IRepository<User> userRepository;

        public ContractBController(
            IHostingEnvironment env,
            IContractBService contractService,
            IRepository<Phase> phaseRepository,
            IRepository<PhaseStaff> phaseStaffRepository,
            IRepository<User> userRepository)
        {
            this.env = env;
            this.contractService = contractService;
            this.phaseRepository = phaseRepository;
            this.phaseStaffRepository = phaseStaffRepository;
            this.userRepository = userRepository;
        }
        #endregion

        #region Contract
        /// <summary>
        /// Get active contracts
        /// </summary>
        /// <returns></returns>
        [Authorize]
        [HttpGet("/api/contract/actives")]
        [SwaggerResponse((int)System.Net.HttpStatusCode.OK, Type = typeof(List<ContractModel>))]
        public IActionResult GetActives()
        {
            var result = contractService.GetActives();
            return Json(new { success = true, data = result });
        }
        /// <summary>
        /// Filter contracts
        /// </summary>
        /// <param name="filterModel"></param>
        /// <returns></returns>
        [Authorize]
        [HttpPost("/api/contract/filter")]
        [SwaggerResponse((int)System.Net.HttpStatusCode.OK, Type = typeof(ResultBase<ContractFilterResult>))]
        public IActionResult Filter([FromBody]ContractFilterModel filterModel)
        {
            var userId = HttpContext.User.Identity.GetUserId();
            filterModel.UserId = userId;
            filterModel.HasViewAll = HttpContext.User.HasPermission(Domain.Services.Roles.PermissionCode.CONTRACT_VIEWALL);
            var result = contractService.Filter(filterModel);
            return Json(new { success = true, data = result });
        }
        /// <summary>
        /// Get next contract code
        /// </summary>
        /// <returns></returns>
        [Authorize]
        [HttpGet("/api/contract/nextcode/{date}/{isContract}")]
        [SwaggerResponse((int)System.Net.HttpStatusCode.OK, Type = typeof(ResultBase<string>))]
        public IActionResult NextCode(string date, bool isContract = true)
        {
            var result = contractService.NextCode(DateTime.Parse(date), isContract);
            return Json(new { success = true, data = result });
        }
        /// <summary>
        /// Get types
        /// </summary>
        /// <returns></returns>
        [Authorize]
        [HttpGet("/api/contract/types")]
        [SwaggerResponse((int)System.Net.HttpStatusCode.OK, Type = typeof(ResultBase<Dictionary<int, string>>))]
        public IActionResult GetTypes()
        {
            var result = contractService.GetTypes();
            return Json(new { success = result, data = result });
        }
        /// <summary>
        /// Save contract
        /// </summary>
        /// <param name="model">
        /// id = 0 : Create
        /// id > 0 : Update
        /// </param>
        /// <returns></returns>
        [Authorize]
        [HttpPost("/api/contract/save")]
        [SwaggerResponse((int)System.Net.HttpStatusCode.OK, Type = typeof(ResultBase<ContractModel>))]
        public IActionResult Save([FromBody]ContractFormModel model)
        {
            if (!ModelState.IsValid) throw new Exception(ModelState.GetErrorsMessage());

            var userId = HttpContext.User.Identity.GetUserId();
            model.CreateBy = userId;
            var result = contractService.Save(model);
            return Json(new { success = true, data = result });
        }

        /// <summary>
        /// Add users to contract
        /// </summary>
        /// <returns></returns>
        [HttpPost("/api/contract/assign")]
        public IActionResult Assign([FromBody] AssignModel assignModel)
        {
            var userId = User.Identity.GetUserId();
            if (assignModel.ProcessType==null)
            {
                assignModel.ProcessType = ProcessType.QT01;
            }
            var result = contractService.Assign(assignModel);
            return Json(new { success = true });
        }
        /// <summary>
        /// Remove user from contract
        /// </summary>
        /// <returns></returns>
        [HttpPost("/api/contract/unassign/{id}")]
        public IActionResult Unassign(int id)
        {
            var userId = User.Identity.GetUserId();
            var result = contractService.Unassign(id);
            return Json(new { success = true });
        }

        /// <summary>
        /// Next step by process
        /// </summary>
        /// <returns></returns>
        [Authorize]
        [HttpPost("/api/contract/{id}/next")]
        [SwaggerResponse((int)System.Net.HttpStatusCode.OK, Type = typeof(ResultBase<bool>))]
        public IActionResult Next(int id, [FromBody] string action)
        {
            var message = string.Empty;
            var success = contractService.Next(id, action);
            return Json(new { success, message });
        }
        /// <summary>
        /// Reject
        /// </summary>
        /// <returns></returns>
        [Authorize]
        [HttpPost("/api/contract/{id}/reject")]
        [SwaggerResponse((int)System.Net.HttpStatusCode.OK, Type = typeof(ResultBase<bool>))]
        public IActionResult Reject([FromBody] RejectContractModel model)
        {
            var success = contractService.Reject(model);
            return Json(new { success });
        }
        /// <summary>
        /// Delete contracts
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [Authorize]
        [HttpDelete("/api/contract/{id}/delete")]
        [SwaggerResponse((int)System.Net.HttpStatusCode.OK, Type = typeof(ResultBase<bool>))]
        public IActionResult Delete(int id)
        {
            bool result = contractService.Delete(id);
            return Json(new { success = result });
        }
        /// <summary>
        /// Get contract
        /// </summary>
        /// <param name="uid">Contract id</param>
        /// <returns></returns>
        [Authorize]
        [HttpGet("/api/contract/{uid}")]
        [SwaggerResponse((int)System.Net.HttpStatusCode.OK, Type = typeof(ResultBase<ContractModel>))]
        public IActionResult Get(Guid uid)
        {
            var result = contractService.Get(uid);
            return Json(new { success = true, data = result });
        }
        /// <summary>
        /// Cone contract
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [Authorize]
        [HttpPost("/api/contract/{id}")]
        [SwaggerResponse((int)System.Net.HttpStatusCode.OK, Type = typeof(ResultBase<int>))]
        public IActionResult Clone(CloneContractModel model)
        {
            var userId = HttpContext.User.Identity.GetUserId();
            model.UserId = userId;
            var result = contractService.Clone(model);
            return Json(new { success = true, data = result });
        }
        /// <summary>
        /// Finish contract
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [Authorize]
        [HttpPost("/api/contract/{id}/finish")]
        [SwaggerResponse((int)System.Net.HttpStatusCode.OK, Type = typeof(ResultBase<bool>))]
        public IActionResult Finish(FinishContractModel model)
        {
            var userId = HttpContext.User.Identity.GetUserId();
            model.UserId = userId;
            var result = contractService.Finish(model);
            return Json(new { success = result });
        }
        /// <summary>
        /// Upload contract documents
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [Authorize]
        [HttpPost("/api/contract/{id}/attachments/upload")]
        [SwaggerResponse((int)System.Net.HttpStatusCode.OK, Type = typeof(ResultBase<List<Domain.Data.Entity.Document>>))]
        public IActionResult UploadFiles(int id)
        {
            var files = Request.Form.Files;
            var fileModels = new List<FileInfoModel>();
            foreach (var file in files)
            {
                var ms = new MemoryStream();
                file.CopyTo(ms);
                var bytes = ms.ToArray();
                fileModels.Add(new FileInfoModel
                {
                    Bytes = bytes,
                    Mime = file.ContentType,
                    Title = file.FileName,
                    Length = file.Length
                });
            }

            var result = contractService.AttachDocuments(id, fileModels);
            return Json(new { success = true, data = result });
        }
        #endregion


        #region Groups
        /// <summary>
        /// Get contract groups
        /// </summary>
        /// <returns></returns>
        [Authorize]
        [HttpGet("/api/contract/groups")]
        [SwaggerResponse((int)System.Net.HttpStatusCode.OK, Type = typeof(List<ContractGroupModel>))]
        public IActionResult GetGroups()
        {
            var result = contractService.GetGroups();
            return Json(new { success = true, data = result });
        }

        /// <summary>
        /// Save contract groups
        /// </summary>
        /// <returns></returns>
        [Authorize]
        [HttpGet("/api/contract/save_contract_group")]
        [SwaggerResponse((int)System.Net.HttpStatusCode.OK, Type = typeof(ContractGroupModel))]
        public IActionResult SaveGroup(ContractGroupModel model)
        {
            var result = contractService.SaveContractGroup(model);
            return Json(new { success = true, data = result });
        }

        /// <summary>
        /// Delete contract groups
        /// </summary>
        /// <returns></returns>
        [Authorize]
        [HttpGet("/api/contract/delete_contract_group")]
        [SwaggerResponse((int)System.Net.HttpStatusCode.OK, Type = typeof(bool))]
        public IActionResult DeleteGroup(int id)
        {
            var result = contractService.DeleteContractGroup(id);
            return Json(new { success = result });
        }
        #endregion

        #region Workitems
        /// <summary>
        /// Upload and parse workitems file
        /// </summary>
        /// <param name="id"></param>
        /// <param name="file"></param>
        /// /// <param name="isStem"></param>
        /// <returns></returns>
        [Authorize]
        [HttpPost("/api/contract/{id}/workitems/upload")]
        [HttpPost("/api/contract/{id}/workitems/upload/{isStem}")]
        [SwaggerResponse((int)System.Net.HttpStatusCode.OK, Type = typeof(ResultBase<bool>))]
        public IActionResult UploadWorkitem(int id, IFormFile file, bool? isStem = false, ProcessType processType=ProcessType.QT01)
        {
            //var userId = HttpContext.User.Identity.GetUserId();
            var ms = new MemoryStream();
            file.CopyTo(ms);
            var bytes = ms.ToArray();
            var result = contractService.ParseWorkitemFile(id, bytes, isStem,processType);
            return Json(new { success = true, data = result });
        }

        /// <summary>
        /// Add Stem workitem
        /// </summary>
        /// <param name="stemModel"></param>
        /// <returns></returns>
        [HttpPost("/api/contract/workitems/add-stem")]
        [Authorize]
        [SwaggerResponse((int)System.Net.HttpStatusCode.OK, Type = typeof(ResultBase<bool>))]
        public IActionResult AddWorkitemStem([FromBody] SaveWorkitemStemModel stemModel)
        {
            var result = contractService.AddWorkitemStem(stemModel);
            return Json(new { success = result });
        }

        /// <summary>
        /// Update workitem
        /// </summary>
        /// <param name="model"></param>
        /// <param name="uid"></param>
        /// <returns></returns>
        [HttpPost("/api/contract/{uid}/workitems/update")]
        [Authorize]
        [SwaggerResponse((int)System.Net.HttpStatusCode.OK, Type = typeof(ResultBase<bool>))]
        public IActionResult UpdateWorkitem([FromBody] UpdateWorkitemModel model, [FromRoute] Guid uid)
        {
            var result = contractService.UpdateWorkitem(model, uid);
            return Json(new { success = result });
        }



        /// <summary>
        /// Clear workitems
        /// </summary>
        /// <param name="id"></param>
        /// <param name="file"></param>
        /// <returns></returns>
        [Authorize]
        [HttpPost("/api/contract/{id}/workitems/clear")]
        [SwaggerResponse((int)System.Net.HttpStatusCode.OK, Type = typeof(ResultBase<bool>))]
        public IActionResult ClearWorkitem(int id, IFormFile file,ProcessType processType = ProcessType.QT01)
        {
            var success = contractService.ClearWorkitems(id,processType);
            return Json(new { success });
        }
        /// <summary>
        /// Get workitems
        /// </summary>
        /// <param name="uid"></param>
        /// <returns></returns>
        [Authorize]
        [HttpGet("/api/contract/{uid}/workitems")]
        [SwaggerResponse((int)System.Net.HttpStatusCode.OK, Type = typeof(ResultBase<List< WorkitemModel>>))]
        public IActionResult GetWorkitems(Guid uid, ProcessType processType = ProcessType.QT01)
        {
            var result = contractService.GetWorkitems(uid,processType);
            return Json(new { success = true, data = result });
        }

        /// <summary>
        /// Get workitems
        /// </summary>
        /// <param name="uid"></param>
        /// <returns></returns>
        [Authorize]
        [HttpGet("/api/contract/{uid}/workitems/outputs")]
        [SwaggerResponse((int)System.Net.HttpStatusCode.OK, Type = typeof(ResultBase<List<WorkitemModel>>))]
        public IActionResult GetOutputWorkitems(Guid uid, ProcessType processType = ProcessType.QT01)
        {
            var result = contractService.GetOutputWorkitems(uid,processType);
            return Json(new { success = true, data = result });
        }

        /// <summary>
        /// Update acceptanced quantity
        /// </summary>
        /// <param name="workitems"></param>
        /// <returns></returns>
        [Authorize]
        [HttpPost("/api/contract/workitems/acceptanced")]
        [SwaggerResponse((int)System.Net.HttpStatusCode.OK, Type = typeof(ResultBase<List<WorkitemModel>>))]
        public IActionResult UpdateAcceptancedQuantity([FromBody] List<WorkitemModel> workitems)
        {
            var result = contractService.UpdateAcceptancedQuantity(workitems);
            return Json(new { success = true, data = result });
        }

        #endregion

        #region Plans
        /// <summary>
        /// Save department master plans
        /// </summary>
        /// <returns></returns>
        [Authorize]
        [HttpPost("/api/contract/{id}/masterplan/save")]
        [SwaggerResponse((int)System.Net.HttpStatusCode.OK, Type = typeof(ResultBase<bool>))]
        public IActionResult SaveMasterPlan(int id, [FromBody] List<MasterPlanModel> model)
        {
            model.ForEach(m => m.ContractId = id);
            var result = contractService.SaveMasterPlan(model);
            return Json(new { success = result });
        }
        /// <summary>
        /// Save master plan dates
        /// </summary>
        /// <returns></returns>
        [Authorize]
        [HttpPost("/api/contract/{id}/workitems/update/dates")]
        [SwaggerResponse((int)System.Net.HttpStatusCode.OK, Type = typeof(ResultBase<bool>))]
        public IActionResult UpdateWorkitemDates(int id, [FromBody] List<WorkitemModel> model)
        {
            model.ForEach(m => m.ContractId = id);
            var result = contractService.UpdateWorkitemDates(model);
            return Json(new { success = result });
        }
        /// <summary>
        /// Get master plan
        /// </summary>
        /// <returns></returns>
        [Authorize]
        [HttpGet("/api/contract/{id}/masterplan")]
        [SwaggerResponse((int)System.Net.HttpStatusCode.OK, Type = typeof(ResultBase<List<MasterPlan>>))]
        public IActionResult GetMasterPlan(int id)
        {
            var result = contractService.GetMasterPlan(id);
            return Json(new { success = true, data = result });
        }
        /// <summary>
        /// Remove center from master plan
        /// </summary>
        /// <returns></returns>
        [Authorize]
        [HttpPost("/api/contract/{contractId}/masterplan/remove/center/{centerId}")]
        [SwaggerResponse((int)System.Net.HttpStatusCode.OK, Type = typeof(ResultBase<bool>))]
        public IActionResult RemoveCenter(int contractId, int centerId)
        {
            var success = contractService.RemoveCenter(contractId, centerId);
            return Json(new { success });
        }
        #endregion

        #region Phases
        /// <summary>
        /// Get phases by contract
        /// </summary>
        /// <param name="uid">Contract id</param>
        /// <returns></returns>
        [Authorize]
        [HttpGet("/api/contract/{uid}/phases")]
        [SwaggerResponse((int)System.Net.HttpStatusCode.OK, Type = typeof(ResultBase<List<PhaseModel>>))]
        public IActionResult GetPhases(Guid uid,ProcessType processType=ProcessType.QT01)
        {
            var result = contractService.GetPhases(uid,processType);
            return Json(new { success = true, data = result });
        }
        /// <summary>
        /// Get phases
        /// </summary>
        /// <returns></returns>
        [Authorize]
        [HttpGet("/api/contract/phases")]
        [SwaggerResponse((int)System.Net.HttpStatusCode.OK, Type = typeof(ResultBase<List<PhaseModel>>))]
        public IActionResult GetPhases()
        {
            var userId = HttpContext.User.Identity.GetUserId();
            var result = contractService.GetPhases(userId, true);
            return Json(new { success = true, data = result });
        }
        /// <summary>
        /// Get phases by contract
        /// </summary>
        /// <param name="id">Contract id</param>
        /// <returns></returns>
        [Authorize]
        [HttpGet("/api/contract/{id}/phases/count")]
        [SwaggerResponse((int)System.Net.HttpStatusCode.OK, Type = typeof(ResultBase<List<int>>))]
        public IActionResult GetPhasesCount(int id,ProcessType processType=ProcessType.QT01)
        {
            var result = contractService.GetPhasesCount(id, processType);
            return Json(new { success = true, data = result });
        }

        /// <summary>
        /// Save contract
        /// </summary>
        /// <param name="model">
        /// id = 0 : Create
        /// id > 0 : Update
        /// </param>
        /// <returns></returns>
        [Authorize]
        [HttpPost("/api/contract/phases/update")]
        [SwaggerResponse((int)System.Net.HttpStatusCode.OK, Type = typeof(ResultBase<bool>))]
        public IActionResult UpdatePhase([FromBody]PhaseModel model)
        {
            if (!ModelState.IsValid) throw new Exception(ModelState.GetErrorsMessage());
            var data = contractService.UpdatePhase(model);
            return Json(new { success = true, data });
        }
        /// <summary>
        /// Get phase by id
        /// </summary>
        /// <param name="id">Phase id</param>
        /// <returns></returns>
        [Authorize]
        [HttpGet("/api/contract/phases/{id}")]
        [SwaggerResponse((int)System.Net.HttpStatusCode.OK, Type = typeof(ResultBase<List<PhaseModel>>))]
        public IActionResult GetPhase(int id)
        {
            var result = contractService.GetPhase(id);
            return Json(new { success = true, data = result });
        }
        /// <summary>
        /// Get unassigned workitems
        /// </summary>
        /// <param name="phaseId">Phase id</param>
        /// <returns></returns>
        [Authorize]
        [HttpGet("/api/contract/phases/{phaseId}/workitems/unassigned")]
        [SwaggerResponse((int)System.Net.HttpStatusCode.OK, Type = typeof(ResultBase<List<WorkitemModel>>))]
        public IActionResult GetUnassignedWorkitems(int phaseId)
        {
            var result = contractService.GetUnassignedWorkitems(phaseId);
            return Json(new { success = true, data = result });
        }

        /// <summary>
        /// Get assigned workitems
        /// </summary>
        /// <param name="phaseId">Phase id</param>
        /// <returns></returns>
        [Authorize]
        [HttpGet("/api/contract/phases/{phaseId}/workitems/assigned")]
        [SwaggerResponse((int)System.Net.HttpStatusCode.OK, Type = typeof(ResultBase<List<WorkitemModel>>))]
        public IActionResult GetAssignedWorkitems(int phaseId)
        {
            var result = contractService.GetWorkitemsByPhase(phaseId);
            return Json(new { success = true, data = result });
        }
        /// <summary>
        /// Add workitems to phase
        /// </summary>
        /// <returns></returns>
        [Authorize]
        [HttpPost("/api/contract/phases/workitems/add")]
        [SwaggerResponse((int)System.Net.HttpStatusCode.OK, Type = typeof(ResultBase<bool>))]
        public IActionResult AddWorkitemsToPhase([FromBody] PhaseWorkitemAddModel model)
        {
            var result = contractService.AddWorkitemsToPhase(model);
            return Json(new { success = result });
        }
        /// <summary>
        /// Get assignments
        /// </summary>
        /// <param name="phaseId">Phase id</param>
        /// <returns></returns>
        [Authorize]
        [HttpGet("/api/contract/phases/{phaseId}/assignments")]
        [SwaggerResponse((int)System.Net.HttpStatusCode.OK, Type = typeof(ResultBase<List<PhaseWorkitem>>))]
        public IActionResult GetAssignments(int phaseId)
        {
            var result = contractService.GetPhaseWorkitems(phaseId);
            return Json(new { success = true, data = result });
        }
        /// <summary>
        /// Update assignments
        /// </summary>
        /// <param name="phaseId">Phase id</param>
        /// <param name="model"></param>
        /// <returns></returns>
        [Authorize]
        [HttpPost("/api/contract/phases/{phaseId}/assignments/update")]
        [SwaggerResponse((int)System.Net.HttpStatusCode.OK, Type = typeof(ResultBase<List<PhaseWorkitem>>))]
        public IActionResult UpdateAssignments(int phaseId, [FromBody] List<PhaseWorkitem> model)
        {
            model.ForEach(m => m.PhaseId = phaseId);
            var result = contractService.UpdatePhaseWorkitems(model);
            return Json(new { success = true, data = result });
        }

        /// <summary>
        /// Update assignments
        /// </summary>
        /// <param name="phaseId">Phase id</param>
        /// <param name="workitemId"></param>
        /// <returns></returns>
        [Authorize]
        [HttpPost("/api/contract/phases/{phaseId}/assignments/delete/{workitemId}")]
        [SwaggerResponse((int)System.Net.HttpStatusCode.OK, Type = typeof(ResultBase<bool>))]
        public IActionResult DeleteAssignments(int phaseId, int workitemId)
        {
            var result = contractService.DeletePhaseWorkitem(phaseId, workitemId);
            return Json(new { success = result });
        }
        /// <summary>
        /// Get requested staffs
        /// </summary>
        /// <param name="phaseId">Phase id</param>
        /// <returns></returns>
        [Authorize]
        [HttpGet("/api/contract/phases/{phaseId}/staffs/requested")]
        [SwaggerResponse((int)System.Net.HttpStatusCode.OK, Type = typeof(ResultBase<List<PhaseStaffRequest>>))]
        public IActionResult GetRequestedStaffs(int phaseId)
        {
            var result = contractService.GetRequestedStaffs(phaseId);
            return Json(new { success = true, data = result });
        }
        /// <summary>
        /// Save requested staffs
        /// </summary>
        /// <param name="phaseId">Phase id</param>
        /// <param name="requests"></param>
        /// <returns></returns>
        [Authorize]
        [HttpPost("/api/contract/phases/{phaseId}/staffs/requested/save")]
        [SwaggerResponse((int)System.Net.HttpStatusCode.OK, Type = typeof(ResultBase<bool>))]
        public IActionResult SaveRequestedStaffs(int phaseId, [FromBody] List<PhaseStaffRequest> requests)
        {
            requests.ForEach(r => r.PhaseId = phaseId);
            var success = contractService.SaveRequestedStaffs(requests);
            return Json(new { success });
        }
        /// <summary>
        /// Get assigned staffs
        /// </summary>
        /// <param name="phaseId">Phase id</param>
        /// <returns></returns>
        [Authorize]
        [HttpGet("/api/contract/phases/{phaseId}/staffs")]
        [SwaggerResponse((int)System.Net.HttpStatusCode.OK, Type = typeof(ResultBase<List<PhaseStaffModel>>))]
        public IActionResult GetPhaseStaffs(int phaseId)
        {
            var result = contractService.GetPhaseStaffs(phaseId);
            return Json(new { success = true, data = result });
        }

        /// <summary>
        /// Get unassigned staffs
        /// </summary>
        /// <returns></returns>
        [Authorize]
        [HttpPost("/api/contract/phases/department/unassigned")]
        [SwaggerResponse((int)System.Net.HttpStatusCode.OK, Type = typeof(ResultBase<List<UserModel>>))]
        public IActionResult GetUnassignedStaffs([FromBody] DepartmentStaffFilterModel filter)
        {
            var result = contractService.GetUnassignedStaffs(filter);
            return Json(new { success = true, data = result });
        }
        /// <summary>
        /// Finish staff assignment
        /// </summary>
        /// <returns></returns>
        [Authorize]
        [HttpPost("/api/contract/phases/{phaseId}/{departmentId}/staffs/done")]
        [SwaggerResponse((int)System.Net.HttpStatusCode.OK, Type = typeof(ResultBase<List<UserModel>>))]
        public IActionResult FinishStaffAssignment(int phaseId, int departmentId)
        {
            var result = contractService.DonePhaseStaffs(phaseId, departmentId); ;
            return Json(new { success = true, data = result });
        }
        /// <summary>
        /// Add staffs to phase
        /// </summary>
        /// <param name="phaseId">Phase id</param>
        /// <param name="staffIds"></param>
        /// <returns></returns>
        [Authorize]
        [HttpPost("/api/contract/phases/{phaseId}/staffs/add")]
        [SwaggerResponse((int)System.Net.HttpStatusCode.OK, Type = typeof(ResultBase<bool>))]
        public IActionResult AddStaffsToPhase(int phaseId, [FromBody] List<int> staffIds)
        {
            var result = contractService.AddStaffsToPhase(phaseId, staffIds);
            return Json(new { success = result });
        }

        /// <summary>
        /// Set phase staff position from P4
        /// </summary>
        /// <returns></returns>
        [Authorize]
        [HttpPost("/api/contract/phases/staffs/{phaseStaffId}/{position}")]
        [SwaggerResponse((int)System.Net.HttpStatusCode.OK, Type = typeof(ResultBase<bool>))]
        public IActionResult SetPhaseStaffPositionFromPKT(int phaseStaffId, PhaseStaffPosition position)
        {
            var result = contractService.SetPhaseStaffPositionFromPKT(phaseStaffId, position);
            return Json(new { success = result });
        }

        /// <summary>
        /// Set phase staff position from specialized departments
        /// </summary>
        /// <returns></returns>
        [Authorize]
        [HttpPost("/api/contract/phases/staffs/{phaseStaffId}/{position}/from_specialized_departments")]
        [SwaggerResponse((int)System.Net.HttpStatusCode.OK, Type = typeof(ResultBase<bool>))]
        public IActionResult SetPhaseStaffPositionFromSpecialDepartments(int phaseStaffId, PCMAssignPotition position)
        {
            var userId = HttpContext.User.Identity.GetUserId();
            var result = contractService.SetPhaseStaffPositionFromSpecialDepartments(phaseStaffId, position, userId);
            return Json(new { success = result });
        }

        /// <summary>
        /// Remove phase staff
        /// </summary>
        /// <returns></returns>
        [Authorize]
        [HttpPost("/api/contract/phases/staffs/{phaseStaffId}/remove")]
        [SwaggerResponse((int)System.Net.HttpStatusCode.OK, Type = typeof(ResultBase<bool>))]
        public IActionResult RemovePhaseStaff(int phaseStaffId)
        {
            var result = contractService.RemovePhaseStaff(phaseStaffId);
            return Json(new { success = result });
        }

        /// <summary>
        /// Finish phase staff
        /// </summary>
        /// <returns></returns>
        [Authorize]
        [HttpPost("/api/contract/phases/staffs/{phaseStaffId}/finish")]
        [SwaggerResponse((int)System.Net.HttpStatusCode.OK, Type = typeof(ResultBase<bool>))]
        public IActionResult FinishPhaseStaff(int phaseStaffId)
        {
            var result = contractService.FinishPhaseStaff(phaseStaffId);
            return Json(new { success = result });
        }

        /// <summary>
        /// Submit phase
        /// </summary>
        /// <returns></returns>
        [Authorize]
        [HttpPost("/api/contract/phases/{phaseId}/submit")]
        [SwaggerResponse((int)System.Net.HttpStatusCode.OK, Type = typeof(ResultBase<bool>))]
        public IActionResult SubmitPhase(int phaseId)
        {
            var userId = HttpContext.User.Identity.GetUserId();
            var success = contractService.SubmitPhase(phaseId, userId);
            return Json(new { success });
        }

        /// <summary>
        /// Submit deciding to form a team to P3 manager
        /// </summary>
        /// <returns></returns>
        [Authorize]
        [HttpPost("/api/contract/phases/{phaseId}/submit_to_p3manager")]
        [SwaggerResponse((int)System.Net.HttpStatusCode.OK, Type = typeof(ResultBase<bool>))]
        public IActionResult SubmitPhaseToP3Manager(int phaseId)
        {
            var userId = HttpContext.User.Identity.GetUserId();
            var success = contractService.SubmitPhaseToP3Manager(phaseId, userId);
            return Json(new { success });
        }

        /// <summary>
        /// Approve deciding to form a team by P3 manager
        /// </summary>
        /// <returns></returns>
        [Authorize]
        [HttpPost("/api/contract/phases/{phaseId}/approve_by_p3manager")]
        [SwaggerResponse((int)System.Net.HttpStatusCode.OK, Type = typeof(ResultBase<bool>))]
        public IActionResult ApprovePhaseByP3Manager(int phaseId)
        {
            var success = contractService.ApprovePhaseByP3Manager(phaseId);
            return Json(new { success });
        }

        /// <summary>
        /// Reject phase
        /// </summary>
        /// <returns></returns>
        [Authorize]
        [HttpPost("/api/contract/phases/{phaseId}/reject")]
        [SwaggerResponse((int)System.Net.HttpStatusCode.OK, Type = typeof(ResultBase<bool>))]
        public IActionResult RejectPhase(int phaseId, [FromBody] string reason)
        {
            var success = contractService.RejectPhase(phaseId, reason);
            return Json(new { success });
        }

        /// <summary>
        /// Approve phase
        /// </summary>
        /// <returns></returns>
        [Authorize]
        [HttpPost("/api/contract/phases/{phaseId}/approve")]
        [SwaggerResponse((int)System.Net.HttpStatusCode.OK, Type = typeof(ResultBase<bool>))]
        public IActionResult ApprovePhase(int phaseId)
        {
            var success = contractService.ApprovePhase(phaseId);
            return Json(new { success });
        }


        /// <summary>
        /// Pass phase
        /// </summary>
        /// <returns></returns>
        [Authorize]
        [HttpPost("/api/contract/phases/{phaseId}/pass")]
        [SwaggerResponse((int)System.Net.HttpStatusCode.OK, Type = typeof(ResultBase<bool>))]
        public IActionResult PassPhase(int phaseId)
        {
            var success = contractService.PassPhase(phaseId);
            return Json(new { success });
        }
        /// <summary>
        /// Pass phase
        /// </summary>
        /// <returns></returns>
        [Authorize]
        [HttpPost("/api/contract/phases/{phaseId}/passtop3")]
        [SwaggerResponse((int)System.Net.HttpStatusCode.OK, Type = typeof(ResultBase<bool>))]
        public IActionResult PassPhaseToP3(int phaseId)
        {
            var success = contractService.PassPhaseToP3(phaseId);
            return Json(new { success });
        }
        /// <summary>
        /// Publish phase
        /// </summary>
        /// <returns></returns>
        [Authorize]
        [HttpPost("/api/contract/phases/{phaseId}/publish")]
        [SwaggerResponse((int)System.Net.HttpStatusCode.OK, Type = typeof(ResultBase<bool>))]
        public IActionResult PublishPhase(int phaseId)
        {
            var result = contractService.PublishPhase(phaseId);
            return Json(new { success = result });
        }

        /// <summary>
        /// Generate published phase paper
        /// </summary>
        /// <returns></returns>
        //[Authorize]
        [HttpGet("/api/contract/phases/{phaseId}/paper")]
        [SwaggerResponse((int)System.Net.HttpStatusCode.OK, Type = typeof(ResultBase<bool>))]
        public IActionResult GeneratePhasePaper(int phaseId)


        {
            var phase = contractService.GetPhase(phaseId);
            var contract = contractService.Get(phase.ContractId);
            var phaseStaffs = contractService.GetPhaseStaffs(phaseId);
            var captainStaff = phaseStaffs.FirstOrDefault(ps => ps.PositionInPhase == PhaseStaffPosition.Captain);

            if (captainStaff == null)
                throw new Exception("Chưa xác định đội trưởng đội công tác.");

            var phaseStaffGroups = phaseStaffs.GroupBy(ps => ps.DepartmentId);
            foreach (var group in phaseStaffGroups)
            {
                if (group.Count(s => s.Position == PCMAssignPotition.Leader) == 0)
                {
                    var department = ((Department)group.Key).ToDescription();
                    throw new Exception("Chưa xác định tổ trưởng cho PCM : " + department);
                }
            }

            var safetyStaff = phaseStaffs.FirstOrDefault(ps => ps.PositionInPhase == PhaseStaffPosition.Safety);
            if (safetyStaff == null)
                throw new Exception("Chưa xác định cán bộ an toàn.");

            var webRoot = env.ContentRootPath;

            var checkCondition = false;
            var phaseExist = phaseRepository.GetById(phaseId);
            if (!phaseStaffs.Any(c => c.IsOriginal == false))
            {
                checkCondition = true;
            }
            if (checkCondition)
            {
                var template = Path.Combine(webRoot, "Templates", "NPCETC.KTQT.01BM.04.docx");
                var file = Path.Combine(webRoot, "caches", "NPCETC.KTQT.01BM.04.docx");
                if (System.IO.File.Exists(file)) System.IO.File.Delete(file);
                System.IO.File.Copy(template, file);
                var doc = new XWPFDocument(OPCPackage.Open(file));

                var textNotBold = new List<string>();

                var phaseStaffOriginals = phaseStaffs.Where(c => c.IsOriginal == true);

                foreach (var p in doc.Paragraphs)
                {
                    foreach (var r in p.Runs)
                    {



                        var text = r.GetText(0);
                        if (text == null) continue;
                        if (text.Contains("{CONTRACT_CODE}"))
                        {
                            text = text.Replace("{CONTRACT_CODE}", contract.Code);
                            r.SetText(text);
                        }
                        if (text.Contains("{PHASE_TITLE}"))
                        {
                            text = text.Replace("{PHASE_TITLE}", phase.Title);
                            r.SetText(text);
                        }
                        if (text.Contains("{PHASE_NOTE}"))
                        {
                            text = text.Replace("{PHASE_NOTE}", phase.Note);
                            r.SetText(text);
                        }
                        if (text.Contains("{START_TIME}"))
                        {
                            var startTime = phase.StartDate.Value;
                            text = text.Replace("{START_TIME}", string.Format("{0}h{1} phút,{2}ngày {3} tháng {4} năm {5}", startTime.Hour, startTime.Minute,DatetimeHelper.DayInWeeks(startTime.Year, startTime.Month, startTime.Day), startTime.Day, startTime.Month, startTime.Year));
                            r.SetText(text);

                        }
                        if (text.Contains("ENDTIME"))
                        {
                            var endTime = phase.EndDate.Value;
                            text = text.Replace("ENDTIME", string.Format("{0}h{1} phút, ngày {2}/{3}/{4}", endTime.Hour, endTime.Minute, endTime.Day, endTime.Month, endTime.Year));
                            r.SetText(text);
                        }
                        if (text.Contains("{CAPTAIN}"))
                        {
                            text = text.Replace("{CAPTAIN}", captainStaff.StaffName);
                            r.SetText(text);
                        }
                        var textSafety = string.Empty;
                        var listSafety = new List<string>();

                        var staffSafety = phaseStaffOriginals.Where(ps => ps.PositionInPhase == PhaseStaffPosition.Safety);
                        foreach (var staff in staffSafety)
                        {
                            listSafety.Add(staff.StaffName);

                            textNotBold.Add(((Department)staff.DepartmentId).ToDescription());

                        }
                        textSafety = string.Join(", ", listSafety);
                        if (text.Contains("{SAFETY}"))
                        {

                            text = text.Replace("{SAFETY}", textSafety);
                            r.SetText(text);
                            r.IsBold = true;
                            //r.AppendText()
                        }



                        if (text.Contains("{PHASE_NOTE}"))
                        {
                            text = text.Replace("{PHASE_NOTE}", phase.Note);
                            r.SetText(text);
                        }
                    }
                }




                phaseStaffOriginals = phaseStaffOriginals.OrderBy(ps => ps.DepartmentId).ThenByDescending(ps => ps.PositionInPhase).ToList();

                // staffs table
                var table = doc.Tables[doc.Tables.Count - 2];
                for (int i = 0; i < phaseStaffOriginals.Count(); i++)
                {
                    var phaseStaff = phaseStaffOriginals.ToList()[i];
                    //if (phaseStaff.Position == PhaseStaffPosition.Captain) phaseStaff.Position = PhaseStaffPosition.Leader;
                    //if (phaseStaff.Position == PhaseStaffPosition.Safety) phaseStaff.Position = PhaseStaffPosition.Member;

                    var row = table.CreateRow();
                    row.GetCell(0).SetText((i + 1).ToString());
                    row.GetCell(0).Paragraphs.First().Alignment = ParagraphAlignment.CENTER;
                    row.GetCell(1).SetText(phaseStaff.StaffName);
                    row.GetCell(2).SetText(phaseStaff.WorkLevel.ToDescription());
                    row.GetCell(2).Paragraphs.First().Alignment = ParagraphAlignment.CENTER;
                    row.GetCell(3).SetText(phaseStaff.SafeLevel.ToString());
                    row.GetCell(3).Paragraphs.First().Alignment = ParagraphAlignment.CENTER;
                    row.GetCell(4).SetText("Phòng TN " + ((Department)phaseStaff.DepartmentId).ToDescription());
                    row.GetCell(5).SetText(phaseStaff.PositionName);
                }

                foreach (var row in table.Rows)
                {
                    foreach (var cell in row.GetTableCells())
                    {
                        cell.SetBorderTop(XWPFTable.XWPFBorderType.SINGLE, 1, 0, "000000");
                        cell.SetBorderRight(XWPFTable.XWPFBorderType.SINGLE, 1, 0, "000000");
                        cell.SetBorderBottom(XWPFTable.XWPFBorderType.SINGLE, 1, 0, "000000");
                        cell.SetBorderLeft(XWPFTable.XWPFBorderType.SINGLE, 1, 0, "000000");
                    }
                }
                table.SetCellMargins(100, 100, 100, 100);

                // vehicles table
                var phaseVehicles = contractService.GetVehicles(phaseId);
                var vehicleTable = doc.Tables[doc.Tables.Count - 1];
                for (int i = 0; i < phaseVehicles.Count; i++)
                {
                    var phaseVehicle = phaseVehicles[i];
                    var row = vehicleTable.CreateRow();
                    row.GetCell(0).SetText((i + 1).ToString());
                    row.GetCell(0).Paragraphs.First().Alignment = ParagraphAlignment.CENTER;
                    row.GetCell(1).SetText(phaseVehicle.Number);
                    row.GetCell(2).SetText(phaseVehicle.Seat.ToString());
                    row.GetCell(3).SetText(phaseVehicle.DriverName);
                }
                foreach (var row in vehicleTable.Rows)
                {
                    foreach (var cell in row.GetTableCells())
                    {
                        cell.SetBorderTop(XWPFTable.XWPFBorderType.SINGLE, 1, 0, "000000");
                        cell.SetBorderRight(XWPFTable.XWPFBorderType.SINGLE, 1, 0, "000000");
                        cell.SetBorderBottom(XWPFTable.XWPFBorderType.SINGLE, 1, 0, "000000");
                        cell.SetBorderLeft(XWPFTable.XWPFBorderType.SINGLE, 1, 0, "000000");
                    }
                }
                vehicleTable.SetCellMargins(100, 100, 100, 100);

                var fileName = "NPCETC.KTQT.01BM.04-" + contract.Code.Replace("/", ".") + "-" + phase.Id + ".docx";
                var target = Path.Combine(webRoot, "caches", fileName);
                if (System.IO.File.Exists(target)) System.IO.File.Delete(target);
                using (var fs = System.IO.File.Create(target))
                {
                    doc.Write(fs);
                    fs.Close();
                }
                doc.Close();
            }
            else
            {
                var phaseStaffAlls = phaseStaffRepository.Fetch(c => c.PhaseId == phaseId);
                var phaseStaffAllIds = phaseStaffAlls.Select(c => c.StaffId);
                var userAlls = userRepository.Fetch(c => phaseStaffAllIds.Contains(c.Id));
                var staffModifies = phaseStaffAlls.Where(c => c.ReplaceDate.HasValue);

                var departmentAlls = userAlls.Select(c => c.DepartmentId).Distinct();

                var staffAdd = staffModifies.Where(c => !c.ReplaceUserId.HasValue);
                var staffReplaces = staffModifies.Where(c => c.ReplaceUserId.HasValue);

                var staffReplaceDetermine = phaseStaffAlls.Where(c => c.IsOriginal == false);

                var template = Path.Combine(webRoot, "Templates", "BSTT.NHANLUC.DCT.docx");
                var file = Path.Combine(webRoot, "caches", "BSTT.NHANLUC.DCT.docx");
                if (System.IO.File.Exists(file)) System.IO.File.Delete(file);
                System.IO.File.Copy(template, file);
                var doc = new XWPFDocument(OPCPackage.Open(file));

                

                // staffs table
                var table = doc.Tables[1];
                
                var oldReplaces = phaseStaffAlls.Where(c => c.Status == PhaseStaffStatus.HasBeenReplaced);

                var extendModifies = new List<PhaseStaff>();
                if (oldReplaces.Count() > 0)
                {
                    var checkIds = oldReplaces.Select(c => c.ReplaceUserId);
                    extendModifies = staffModifies.Where(c => !checkIds.Contains(c.StaffId)).ToList();
                }
                else
                    extendModifies = staffModifies.ToList();

                var departmentModifies = new List<int>();
                var departmentS = new List<string>();
                var departmentModifyNames = string.Empty;
                var dateModifies = new List<DateTime>();

                for (int i = 0; i < extendModifies.Count(); i++)
                {
                    try
                    {
                        var staffModify = extendModifies.ToArray()[i];
                        //if (phaseStaff.Position == PhaseStaffPosition.Captain) phaseStaff.Position = PhaseStaffPosition.Leader;
                        //if (phaseStaff.Position == PhaseStaffPosition.Safety) phaseStaff.Position = PhaseStaffPosition.Member;

                        var row = table.CreateRow();
                        row.GetCell(0).SetText((i + 1).ToString());
                        row.GetCell(0).Paragraphs.First().Alignment = ParagraphAlignment.CENTER;
                                             
                        row.GetCell(2).Paragraphs.First().Alignment = ParagraphAlignment.CENTER;



                        var changeText = string.Empty;
                        if (staffModify.ReplaceUserId.HasValue)
                        {
                            changeText = userAlls.FirstOrDefault(c => c.Id == staffModify.ReplaceUserId).FullName + "(" + userAlls.FirstOrDefault(c => c.Id == staffModify.StaffId).FullName + ")";
                        }
                        else changeText = userAlls.FirstOrDefault(c => c.Id == staffModify.StaffId).FullName;


                        var oldReplaceUserIds = oldReplaces.Select(c => c.ReplaceUserId);
                        if (oldReplaceUserIds.Contains(staffModify.ReplaceUserId))
                        {
                            row.GetCell(1).SetText(userAlls.FirstOrDefault(c => c.Id == staffModify.ReplaceUserId).FullName);
                            var slv = userAlls.FirstOrDefault(c => c.Id == staffModify.ReplaceUserId).SafeLevel.HasValue ?
                                userAlls.FirstOrDefault(c => c.Id == staffModify.ReplaceUserId).SafeLevel.Value : 0;
                            row.GetCell(2).SetText(slv + "/5");
                            row.GetCell(3).SetText("PTN " + ((Department)userAlls.FirstOrDefault(c => c.Id == staffModify.ReplaceUserId).DepartmentId).ToDescription());
                            row.GetCell(4).SetText("Thay thế cho "+ userAlls.FirstOrDefault(c => c.Id == staffModify.StaffId).FullName);
                        }
                        else
                        {
                            row.GetCell(1).SetText(userAlls.FirstOrDefault(c => c.Id == staffModify.StaffId).FullName);
                            var slv = userAlls.FirstOrDefault(c => c.Id == staffModify.StaffId).SafeLevel.HasValue ?
                                userAlls.FirstOrDefault(c => c.Id == staffModify.StaffId).SafeLevel.Value : 0;
                            row.GetCell(2).SetText(slv + "/5");
                            row.GetCell(3).SetText("PTN " + ((Department)userAlls.FirstOrDefault(c => c.Id == staffModify.StaffId).DepartmentId).ToDescription());
                            row.GetCell(4).SetText("Bổ sung");
                        }
                        dateModifies.Add(staffModify.ReplaceDate.Value);
                        departmentModifies.Add(userAlls.FirstOrDefault(c => c.Id == staffModify.StaffId).DepartmentId.Value);
                    }
                    catch (Exception)
                    {

                        throw;
                    }
                    
                }
                departmentModifies = departmentModifies.Distinct().ToList();
                foreach (var item in departmentModifies)
                {
                    departmentS.Add(((Department)item).ToDescription());
                }
                departmentModifyNames = string.Join(", ", departmentS);
                // thử phátt

                foreach (var p in doc.Paragraphs)
                {
                    foreach (var r in p.Runs)
                    {
                        var text = r.GetText(0);
                        if (text == null) continue;

                        if (text.Contains("{CONTRACT_CODE}"))
                        {
                            text = text.Replace("{CONTRACT_CODE}", contract.Code);
                            r.SetText(text);
                        }
                        if (text.Contains("{PHASE_TITLE}"))
                        {
                            text = text.Replace("{PHASE_TITLE}", phase.Title);
                            r.SetText(text);
                        }

                        var replaceDirectors = string.Empty;

                        var dateChange = DateTime.SpecifyKind(dateModifies.Max(), DateTimeKind.Utc);
                        if (text.Contains("DATE_R"))
                        {
                            text = text.Replace("DATE_R",String.Format("{0:dd/MM/yyyy}", dateChange));
                            r.SetText(text);
                        }

                        if (text.Contains("{DATE_C}"))
                        {
                            text = text.Replace("{DATE_C}", dateChange.Day +" tháng "+dateChange.Month);
                            r.SetText(text);
                        }

                        replaceDirectors = string.Join(", ", departmentModifyNames);
                        if (text.Contains("{REPLACE_DIRECTOR}"))
                        {
                            text = text.Replace("{REPLACE_DIRECTOR}", replaceDirectors);
                            r.SetText(text);
                        }

                        

                        //var isModification = (staffReplaces.Where(c => c.PositionInPhase == PhaseStaffPosition.Captain || c.PositionInPhase == PhaseStaffPosition.Safety).Count() > 0);
                        if (text.Contains("{THONGTIN}"))
                        {
                            if (!checkCondition)
                                text = text.Replace("{THONGTIN}", "Điều 2. ");
                            else text = text.Replace("{THONGTIN}", string.Empty);
                            r.SetText(text);
                            r.IsBold = true;
                        }
                        if (text.Contains("{DIEU22}"))
                        {
                            string dieu22Change = string.Empty;
                            if (!checkCondition)
                            {
                                dieu22Change = "Cử đồng chí ";
                                var listModify = new List<string>();

                                var listSafety = staffReplaceDetermine.Where(c => c.PositionInPhase == PhaseStaffPosition.Safety);
                                var captain = staffReplaceDetermine.FirstOrDefault(c => c.PositionInPhase == PhaseStaffPosition.Captain);

                                foreach (var item in staffReplaceDetermine)
                                {
                                    if (item.PositionInPhase == PhaseStaffPosition.Captain)
                                        listModify.Add(userAlls.FirstOrDefault(c => c.Id == item.StaffId).FullName + " làm đội trưởng đội công tác");

                                    if (item.PositionInPhase == PhaseStaffPosition.Safety)
                                        listModify.Add(userAlls.FirstOrDefault(c => c.Id == item.StaffId).FullName + " làm an toàn viên");
                                }
                                dieu22Change = dieu22Change + string.Join(", ", listModify);
                            }
                            text = text.Replace("{DIEU22}", dieu22Change);
                            r.SetText(text);
                        }
                    }
                }


                foreach (var row in table.Rows)
                {
                    foreach (var cell in row.GetTableCells())
                    {
                        cell.SetBorderTop(XWPFTable.XWPFBorderType.SINGLE, 1, 0, "000000");
                        cell.SetBorderRight(XWPFTable.XWPFBorderType.SINGLE, 1, 0, "000000");
                        cell.SetBorderBottom(XWPFTable.XWPFBorderType.SINGLE, 1, 0, "000000");
                        cell.SetBorderLeft(XWPFTable.XWPFBorderType.SINGLE, 1, 0, "000000");
                    }
                }
                table.SetCellMargins(100, 100, 100, 100);


                var fileName = "BSTT.NHANLUC.DCT-" + contract.Code.Replace("/", ".") + "-" + phase.Id + ".docX";
                var target = Path.Combine(webRoot, "caches", fileName);
                if (System.IO.File.Exists(target)) System.IO.File.Delete(target);
                using (var fs = System.IO.File.Create(target))
                {
                    doc.Write(fs);
                    fs.Close();
                }
                doc.Close();
            }


            return Json(new { success = true });
        }


        /// <summary>
        /// Generate published phase paper
        /// </summary>
        /// <returns></returns>
        //[Authorize]
        [HttpGet("/api/contract/phases/{phaseId}/paper/download")]
        public IActionResult DownloadPhasePaper(int phaseId)
        {
            var phase = contractService.GetPhase(phaseId);
            var contract = contractService.Get(phase.ContractId);
            var webRoot = env.ContentRootPath;

            var phaseStaffs = phaseStaffRepository.Fetch(c => c.PhaseId == phaseId);
            var checkCondition = false;
            var phaseExist = phaseRepository.GetById(phaseId);
            if (!phaseStaffs.Any(c => c.IsOriginal == false))
            {
                checkCondition = true;
            }
            var fileName = string.Empty;
            if (checkCondition)
            {
                fileName = "NPCETC.KTQT.01BM.04-" + contract.Code.Replace("/", ".") + "-" + phase.Id + ".docx";
            }
            else
            {
                fileName = "BSTT.NHANLUC.DCT-" + contract.Code.Replace("/", ".") + "-" + phase.Id + ".docx";
            }
            var file = Path.Combine(webRoot, "caches", fileName);
            return PhysicalFile(file, "application/vnd.openxmlformats-officedocument.wordprocessingml.document", fileName);
        }

        /// <summary>
        /// Complete phase
        /// </summary>
        /// <returns></returns>
        [Authorize]
        [HttpPost("/api/contract/phases/{phaseId}/complete")]
        [SwaggerResponse((int)System.Net.HttpStatusCode.OK, Type = typeof(ResultBase<bool>))]
        public IActionResult CompletePhase(int phaseId)
        {
            var result = contractService.CompletePhase(phaseId);
            return Json(new { success = result });
        }
        #endregion

        #region Equipments
        /// <summary>
        /// Get phase equipments
        /// </summary>
        /// <returns></returns>
        [Authorize]
        [HttpGet("/api/contract/phases/{phaseId}/equipments")]
        [SwaggerResponse((int)System.Net.HttpStatusCode.OK, Type = typeof(ResultBase<List<PhaseEquipmentModel>>))]
        public IActionResult GetPhaseEquipments(int phaseId)
        {
            var result = contractService.GetPhaseEquipments(phaseId);
            return Json(new { success = true, data = result });
        }

        /// <summary>
        /// Add equipments to phase
        /// </summary>
        /// <param name="phaseId">Phase id</param>
        /// <param name="departmentId"></param>
        /// <param name="equipmentIds"></param>
        /// <returns></returns>
        [Authorize]
        [HttpPost("/api/contract/phases/{phaseId}/department/{departmentId}/equipments/add")]
        [SwaggerResponse((int)System.Net.HttpStatusCode.OK, Type = typeof(ResultBase<bool>))]
        public IActionResult AddEquipmentsToPhase(int phaseId, int departmentId, [FromBody] List<int> equipmentIds)
        {
            var result = contractService.AddEquipmentsToPhase(phaseId, departmentId, equipmentIds);
            return Json(new { success = result });
        }

        [Authorize]
        [HttpPost("/api/contract/phases/{phaseId}/department/{departmentId}/equipments/delete")]
        [SwaggerResponse((int)System.Net.HttpStatusCode.OK, Type = typeof(ResultBase<bool>))]
        public IActionResult DeleteEquipmentsFromPhase(int phaseId, int departmentId, [FromBody] List<int> equipmentIds)
        {
            var userId = HttpContext.User.Identity.GetUserId();
            var result = contractService.DeleteEquipmentsFromPhase(phaseId, departmentId, equipmentIds, userId);
            return Json(new { success = result });
        }


        #endregion

        #region Vehicles
        /// <summary>
        /// Add vehicle
        /// </summary>
        /// <param name="phaseId"></param>
        /// <param name="vehicleId"></param>
        /// <returns></returns>
        [Authorize]
        [HttpPost("/api/contract/phases/{phaseId}/vehicles/add/{vehicleId}")]
        [SwaggerResponse((int)System.Net.HttpStatusCode.OK, Type = typeof(ResultBase<List<VehicleModel>>))]
        public IActionResult AddVehicle(int phaseId, int vehicleId)
        {
            var success = contractService.AddVehicle(phaseId, vehicleId);
            return Json(new { success });
        }

        /// <summary>
        /// Add vehicle rental
        /// </summary>
        /// <param name="vehicle"></param>
        /// <param name="phaseId"></param>
        /// <returns></returns>
        [Authorize]
        [HttpPost("/api/contract/{phaseId}/add_vehicle_rental")]
        [SwaggerResponse((int)System.Net.HttpStatusCode.OK, Type = typeof(ResultBase<bool>))]
        public IActionResult AddVehicleRental([FromBody]VehicleModel vehicle, [FromRoute] int phaseId)
        {
            if (!ModelState.IsValid) throw new Exception(ModelState.GetErrorsMessage());
            var success = contractService.AddVehicleRental(vehicle, phaseId);
            return Json(new { success });
        }

        /// <summary>
        /// Remove vehicle
        /// </summary>
        /// <param name="phaseId"></param>
        /// <param name="vehicleId"></param>
        /// <returns></returns>
        [Authorize]
        [HttpPost("/api/contract/phases/{phaseId}/vehicles/remove/{vehicleId}")]
        [SwaggerResponse((int)System.Net.HttpStatusCode.OK, Type = typeof(ResultBase<bool>))]
        public IActionResult RemoveVehicle(int phaseId, int vehicleId)
        {
            var success = contractService.RemoveVehicle(phaseId, vehicleId);
            return Json(new { success });
        }
        /// <summary>
        /// Add vehicle
        /// </summary>
        /// <param name="phaseId"></param>
        /// <returns></returns>
        [Authorize]
        [HttpGet("/api/contract/phases/{phaseId}/vehicles")]
        [SwaggerResponse((int)System.Net.HttpStatusCode.OK, Type = typeof(ResultBase<bool>))]
        public IActionResult GetVehicles(int phaseId)
        {
            var data = contractService.GetVehicles(phaseId);
            return Json(new { success = true, data });
        }

        /// <summary>
        /// Additional Replacements Staff
        /// </summary>
        /// <param name="models"></param>
        /// <returns></returns>
        [Authorize]
        [HttpPost("/api/contract/phases/staff/additional_replacements")]
        [SwaggerResponse((int)System.Net.HttpStatusCode.OK, Type = typeof(ResultBase<bool>))]
        public IActionResult SupplementStaffsForPhase([FromBody]List<ReplacementStaffModel> models)
        {
            var userId = HttpContext.User.Identity.GetUserId();
            if (!ModelState.IsValid) throw new Exception(ModelState.GetErrorsMessage());
            var result = contractService.SupplementStaffsForPhase(models, userId);
            return Json(new { success = result });
        }

        /// <summary>
        /// Get equipment allocator 
        /// </summary>
        /// <param name="phaseId"></param>
        /// <returns></returns>
        [Authorize]
        [HttpGet("/api/contract/{phaseId}/equipment_allocator")]
        [SwaggerResponse((int)System.Net.HttpStatusCode.OK, Type = typeof(ResultBase<List<PhaseEquipmentAllocatorModel>>))]
        public IActionResult GetEquipmentAllocator( int phaseId)
        {
            var userId = HttpContext.User.Identity.GetUserId();
            var result = contractService.GetEquipmentAllocator( phaseId);
            return Json(new { success = true, data = result });
        }

        /// <summary>
        /// Update equipment allocator
        /// </summary>
        /// <param name="phaseId"></param>
        /// <param name="userId"></param>
        /// <param name="departmentId"></param>
        /// <returns></returns>
        [Authorize]
        [HttpPost("/api/contract/{phaseId}/update_equipment_allocator")]
        [SwaggerResponse((int)System.Net.HttpStatusCode.OK, Type = typeof(ResultBase<PhaseEquipmentAllocatorModel>))]
        public IActionResult UpdateEquipmentAllocator(int phaseId, int userId, int departmentId)
        {
            var result = contractService.UpdateEquipmentAllocator(phaseId, userId, departmentId);
            return Json(new { success = result });
        }



        #endregion

        #region Records

        /// <summary>
        /// Save experiment record
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [Authorize]
        [HttpPost("/api/contract/records/save")]
        [SwaggerResponse((int)System.Net.HttpStatusCode.OK, Type = typeof(ResultBase<bool>))]
        public IActionResult SaveExperimentRecord([FromBody]RecordSaveModel model)
        {
            var userId = HttpContext.User.Identity.GetUserId();
            model.UserId = userId;
            var success = contractService.SaveRecord(model);
            return Json(new { success = true });
        }

        /// <summary>
        /// List experiment records
        /// </summary>
        /// <returns></returns>
        [Authorize]
        [HttpPost("/api/contract/{id}/records")]
        [SwaggerResponse((int)System.Net.HttpStatusCode.OK, Type = typeof(ResultBase<RecordFilterResult>))]
        public IActionResult FilterExperimentRecords(int id, RecordFilterModel model)
        {
            model.ContractId = id;
            var data = contractService.FilterRecords(model);
            return Json(new { success = true, data });
        }

        /// <summary>
        /// Get experiment record
        /// </summary>
        /// <returns></returns>
        [Authorize]
        [HttpGet("/api/records/{recordId}")]
        [SwaggerResponse((int)System.Net.HttpStatusCode.OK, Type = typeof(ResultBase<RecordModel>))]
        public IActionResult FilterExperimentRecords(int recordId)
        {
            var data = contractService.GetRecord(recordId);
            return Json(new { success = true, data });
        }

        /// <summary>
        /// Submit record
        /// </summary>
        /// <returns></returns>
        [Authorize]
        [HttpPost("/api/record/{recordId}/submit")]
        [SwaggerResponse((int)System.Net.HttpStatusCode.OK, Type = typeof(ResultBase<bool>))]
        public IActionResult SubmitRecord(int recordId)
        {
            var success = contractService.SubmitRecord(recordId);
            return Json(new { success = true });
        }

        /// <summary>
        /// Reject record
        /// </summary>
        /// <returns></returns>
        [Authorize]
        [HttpPost("/api/record/{recordId}/reject")]
        [SwaggerResponse((int)System.Net.HttpStatusCode.OK, Type = typeof(ResultBase<bool>))]
        public IActionResult RejectRecord(int recordId)
        {
            var success = contractService.RejectRecord(recordId);
            return Json(new { success = true });
        }

        /// <summary>
        /// Approve record
        /// </summary>
        /// <returns></returns>
        [Authorize]
        [HttpPost("/api/record/{recordId}/approve")]
        [SwaggerResponse((int)System.Net.HttpStatusCode.OK, Type = typeof(ResultBase<bool>))]
        public IActionResult ApproveRecord(int recordId)
        {
            var success = contractService.ApproveRecord(recordId);
            return Json(new { success = true });
        }

        


        #endregion
    }
}
