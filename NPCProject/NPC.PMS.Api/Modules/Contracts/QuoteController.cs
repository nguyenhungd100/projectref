﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Hosting;
using NPC.PMS.Api.Helpers;
using NPC.PMS.Domain.Data;
using NPC.PMS.Domain.Data.Entity;
using NPC.PMS.Domain.Enums;
using NPC.PMS.Domain.Models;
using NPC.PMS.Domain.Models.Documents;
using NPC.PMS.Domain.Models.Quotes;
using NPC.PMS.Domain.Services;
using NPC.PMS.Domain.Services.Contracts;
using NPC.PMS.Framework.Extensions.Enums;
using NPOI.OpenXml4Net.OPC;
using NPOI.SS.UserModel;
using NPOI.XWPF.UserModel;
using Swashbuckle.AspNetCore.Annotations;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace NPC.PMS.Api.Modules.Contracts
{
    [Route("api/[controller]")]
    public class QuoteController : Controller
    {
        private readonly IQuoteService quoteService;
        private readonly IContractBService contractService;
        private readonly IDepartmentService departmentService;
        private readonly IHostingEnvironment env;
        private readonly IRepository<User> userRepository;

        public QuoteController(IQuoteService quoteService,
            IContractBService contractService,
        IDepartmentService departmentService,
            IHostingEnvironment env,
            IRepository<User> userRepository)
        {
            this.quoteService = quoteService;
            this.departmentService = departmentService;
            this.env = env;
            this.userRepository = userRepository;
            this.contractService = contractService;
        }

        /// <summary>
        /// Filter quotes
        /// </summary>
        /// <param name="filterModel"></param>
        /// <returns></returns>
        [Authorize]
        [HttpPost("/api/quote/filter")]
        [SwaggerResponse((int)System.Net.HttpStatusCode.OK, Type = typeof(ResultBase<QuoteFilterResult>))]
        public IActionResult Filter([FromBody]QuoteFilterModel filterModel)
        {
            var userId = HttpContext.User.Identity.GetUserId();
            filterModel.UserId = userId;
            filterModel.HasViewAll = HttpContext.User.HasPermission(Domain.Services.Roles.PermissionCode.CONTRACT_VIEWALL);
            var result = quoteService.Filter(filterModel);
            return Json(new { success = true, data = result });
        }

        /// <summary>
        /// Save quote
        /// </summary>
        /// <param name="model">
        /// id = 0 : Create
        /// id > 0 : Update
        /// </param>
        /// <returns></returns>
        [Authorize]
        [HttpPost("/api/quote/save")]
        [SwaggerResponse((int)System.Net.HttpStatusCode.OK, Type = typeof(ResultBase<QuoteModel>))]
        public IActionResult Save([FromBody]QuoteFormModel model)
        {
            if (!ModelState.IsValid) throw new Exception(ModelState.GetErrorsMessage());

            var userId = HttpContext.User.Identity.GetUserId();
            model.CreateBy = userId;
            var result = quoteService.Save(model);
            return Json(new { success = true, data = result });
        }

        /// <summary>
        /// Quote Info
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [Authorize]
        [HttpGet("/api/quote/{id}/info")]
        [SwaggerResponse((int)System.Net.HttpStatusCode.OK, Type = typeof(ResultBase<QuoteModel>))]
        public IActionResult QuoteInfo(int id)
        {
            var result = quoteService.GetQuote(id);
            return Json(new { success = true, data = result });
        }

        /// <summary>
        /// Get Quote
        /// </summary>
        /// <param name="uid"></param>
        /// <returns></returns>
        [Authorize]
        [HttpGet("/api/quote/{uid}")]
        [SwaggerResponse((int)System.Net.HttpStatusCode.OK, Type = typeof(ResultBase<QuoteModel>))]
        public IActionResult GetQuote(Guid uid)
        {
            var result = quoteService.GetQuoteByUid(uid);
            return Json(new { success = true, data = result });
        }

        /// <summary>
        /// Delete quote
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [Authorize]
        [HttpDelete("/api/quote/{id}/delete")]
        [SwaggerResponse((int)System.Net.HttpStatusCode.OK, Type = typeof(ResultBase<bool>))]
        public IActionResult DeleteQuote(int id)
        {
            var success = quoteService.DeleteQuote(id);
            return Json(new { success = true });
        }

        /// <summary>
        /// Staffs by quote
        /// </summary>
        /// <param name="quoteId"></param>
        /// <returns></returns>
        [HttpGet("/api/quote/{quoteId}/staffs")]
        [Authorize]
        [SwaggerResponse((int)System.Net.HttpStatusCode.OK, Type = typeof(ResultBase<QuoteModel>))]
        public IActionResult GetStaffByQuoteId(int quoteId)
        {
            var result = quoteService.GetStaffByQuoteId(quoteId);
            return Json(new { success = true, data = result });
        }

        /// <summary>
        /// Add staff
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost("/api/quote/staffs/add")]
        [Authorize]
        [SwaggerResponse((int)System.Net.HttpStatusCode.OK, Type = typeof(ResultBase<bool>))]
        public IActionResult AddQuoteStaff([FromBody] AddQuoteStaffModel model)
        {

            if (!ModelState.IsValid) throw new Exception(ModelState.GetErrorsMessage());
            var userCurrentId = HttpContext.User.Identity.GetUserId();
            var result = quoteService.AddQuoteStaff(model, userCurrentId);
            return Json(new { success = result });
        }

        /// <summary>
        /// Set leader quote
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost("/api/quote/staffs/set-leader")]
        [Authorize]
        [SwaggerResponse((int)System.Net.HttpStatusCode.OK, Type = typeof(ResultBase<bool>))]
        public IActionResult SetLeaderQuote([FromBody] AddQuoteStaffModel model)
        {
            if (!ModelState.IsValid) throw new Exception(ModelState.GetErrorsMessage());
            var result = quoteService.SetLeaderQuote(model);
            return Json(new { success = result });
        }

        /// <summary>
        /// Remove staff
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [Authorize]
        [HttpPost("/api/quote/staffs/remove")]
        [SwaggerResponse((int)System.Net.HttpStatusCode.OK, Type = typeof(ResultBase<bool>))]
        public IActionResult DeleteQuoteStaff([FromBody] DeleteQuoteStaffModel model)
        {
            if (!ModelState.IsValid) throw new Exception(ModelState.GetErrorsMessage());
            var result = quoteService.DeleteQuoteStaff(model);
            return Json(new { success = result });
        }

        /// <summary>
        /// Upload and parse quouteWorkitems file
        /// </summary>
        /// <param name="id"></param>
        /// <param name="file"></param>
        /// <returns></returns>
        [Authorize]
        [HttpPost("/api/quote/{id}/workitems/upload")]
        [SwaggerResponse((int)System.Net.HttpStatusCode.OK, Type = typeof(ResultBase<bool>))]
        public IActionResult UploadQuoteWorkitems(int id, IFormFile file)
        {
            //var userId = HttpContext.User.Identity.GetUserId();
            var ms = new MemoryStream();
            file.CopyTo(ms);
            var bytes = ms.ToArray();
            var result = quoteService.ParseQuoteWorkitemFile(id, bytes);
            return Json(new { success = true, data = result });
        }

        /// <summary>
        /// Get quote workitems
        /// </summary>
        /// <param name="uid"></param>
        /// <returns></returns>
        [Authorize]
        [HttpGet("/api/quote/{uid}/workitems")]
        [SwaggerResponse((int)System.Net.HttpStatusCode.OK, Type = typeof(ResultBase<bool>))]
        public IActionResult GetQuoteWorkitems(Guid uid)
        {
            var result = quoteService.GetQuoteWorkitems(uid);
            return Json(new { success = true, data = result });
        }

        /// <summary>
        /// Clear quote workitems
        /// </summary>
        /// <param name="id"></param>
        /// <param name="file"></param>
        /// <returns></returns>       
        [Authorize]
        [HttpPost("/api/quote/{id}/workitems/clear")]
        [SwaggerResponse((int)System.Net.HttpStatusCode.OK, Type = typeof(ResultBase<bool>))]
        public IActionResult ClearQuoteWorkitem(int id, IFormFile file)
        {
            var success = quoteService.ClearQuoteWorkitem(id);
            return Json(new { success });
        }

        /// <summary>
        /// Get next quote code
        /// </summary>
        /// <returns></returns>
        [Authorize]
        [HttpGet("/api/quote/nextcode/{date}/{isContract}")]
        [SwaggerResponse((int)System.Net.HttpStatusCode.OK, Type = typeof(ResultBase<string>))]
        public IActionResult NextCode(string date, bool isContract = true)
        {
            var result = quoteService.NextCode(DateTime.Parse(date), isContract);
            return Json(new { success = true, data = result });
        }

        /// <summary>
        /// Generate published quote paper
        /// </summary>
        /// <returns></returns>
        [HttpGet("/api/quote/{phaseId}/paper")]
        [SwaggerResponse((int)System.Net.HttpStatusCode.OK, Type = typeof(ResultBase<bool>))]
        public IActionResult GenerateQuotePaper(int phaseId)

        {
            var phase =   contractService.GetPhase(phaseId);

            var quote = quoteService.GetQuote(phase.ContractId);

            var userCurrent = userRepository.GetById(HttpContext.User.Identity.GetUserId());
            if ((Department)userCurrent.DepartmentId != Department.KHDT)
                throw new ServiceException("Bạn không thuộc phòng Kế hoạch đầu tư nên không được tải Phiếu GNV");


            //viết lại
            var phaseStaffs = contractService.GetPhaseStaffs(phaseId);
            var quoteStaffs = new List<QuoteStaffModel> ();
            foreach (var item in phaseStaffs)
            {
                quoteStaffs.Add(new QuoteStaffModel() {
                    DepartmentId = item.DepartmentId,
                    IsLeader = item.Position == PCMAssignPotition.Leader?true : false,
                    QuoteId = quote.Id,
                    SafeLevel = item.SafeLevel,
                    StaffName = item.StaffName,
                    StaffId = item.StaffId
                });
            }


           // var quoteStaffs = quoteService.GetStaffByQuoteId(quote.Id);
            if (quoteStaffs.Count() == 0) throw new ServiceException("Phiếu giao nhiệm vụ chưa có cán bộ nào tham gia");

            var departments = quoteStaffs.Select(c => c.DepartmentId.Value).Distinct();
            var departmentName = string.Empty;
            var departmentListNames = new List<string>();
            foreach (var item in departments)
            {
                var quoteStaffByDepartments = quoteStaffs.Where(c => c.DepartmentId == item);
                if (quoteStaffByDepartments.Count() == 0)
                    throw new ServiceException("Phòng chuyên môn " + ((Department)item).ToDescription() + " chưa có cán bộ nào tham gia");

                if (quoteStaffByDepartments.Count(c => c.IsLeader) == 0)
                    throw new ServiceException("Phòng chuyên môn " + ((Department)item).ToDescription() + " chưa có Trưởng nhóm");
                departmentListNames.Add(((Department)item).ToDescription());
            }
            departmentName = string.Join(", ", departmentListNames);


            //var webRoot = env.ContentRootPath;
            //var template = Path.Combine(webRoot, "Templates", "NPCETC.KTQT.01BM.04.docx");
            //var file = Path.Combine(webRoot, "caches", "NPCETC.KTQT.01BM.04.docx");
            //if (System.IO.File.Exists(file)) System.IO.File.Delete(file);
            //System.IO.File.Copy(template, file);
            //var doc = new XWPFDocument(OPCPackage.Open(file));

            var webRoot = env.ContentRootPath;
            var template = Path.Combine(webRoot, "Templates", "NPCETC.KTQT.01BM.05.docx");
            var file = Path.Combine(webRoot, "caches", "NPCETC.KTQT.01BM.05.docx");
            if (System.IO.File.Exists(file)) System.IO.File.Delete(file);
            System.IO.File.Copy(template, file);
            var doc = new XWPFDocument(OPCPackage.Open(file));

            foreach (var p in doc.Paragraphs)
            {
                foreach (var r in p.Runs)
                {
                    var text = r.GetText(0);
                    if (text == null) continue;
                    if (text.Contains("{QUOTE_CODE}"))
                    {
                        text = text.Replace("{QUOTE_CODE}", quote.Code);
                        r.SetText(text);
                    }


                    if (text.Contains("{DEPARTMENT_NAME}"))
                    {
                        text = text.Replace("{DEPARTMENT_NAME}", departmentName);
                        r.SetText(text);
                    }

                    if (text.Contains("{START_TIME}"))
                    {
                        var startTime = quote.StartDate.Value;
                        text = text.Replace("{START_TIME}", string.Format("ngày {0}/{1}/{2}", startTime.Day, startTime.Month, startTime.Year));
                        r.SetText(text);
                    }

                    if (text.Contains("ENDTIME"))
                    {
                        var endTime = quote.EndDate.Value;
                        text = text.Replace("ENDTIME", string.Format("ngày {0}/{1}/{2}", endTime.Day, endTime.Month, endTime.Year));
                        r.SetText(text);
                    }

                    if (text.Contains("{CONTENT}"))
                    {
                        text = text.Replace("{CONTENT}", quote.Content);
                        r.SetText(text);
                    }


                }
            }

            var fileName = "NPCETC.KTQT.01BM.05-" + quote.Code.Replace("/", ".") + ".docx";
            var target = Path.Combine(webRoot, "caches", fileName);
            if (System.IO.File.Exists(target)) System.IO.File.Delete(target);
            using (var fs = System.IO.File.Create(target))
            {
                var pathLogo = Path.Combine(webRoot, "Templates", "LogoIso.png");
                using (FileStream picFile = new FileStream(pathLogo, FileMode.Open, FileAccess.Read))
                {
                    var titleTable = doc.Tables[1];

                    XWPFTableCell cell = titleTable.GetRow(0).GetCell(0);

                    XWPFParagraph para = cell.AddParagraph();

                    XWPFRun run = para.CreateRun();
                    para.IndentationLeft = 60;
                    var pic = run.AddPicture(picFile, (int)NPOI.SS.UserModel.PictureType.PNG, "image", 100 * 12857, 65 * 12857);

                    titleTable.SetCellMargins(100, 100, 100, 100);
                }

                var staffTable = doc.Tables[2];



                foreach (var item in departments)
                {

                    var quoteStaffByDepartments = quoteStaffs.Where(c => c.DepartmentId == item);

                    var rowTitle = staffTable.Rows[0];
                    rowTitle = staffTable.CreateRow();
                    rowTitle.GetCell(0).SetText("Phòng " + ((Department)item).ToDescription());


                    for (int i = 0; i < quoteStaffByDepartments.Count(); i++)
                    {
                        XWPFTableRow row = null;

                        row = staffTable.CreateRow();
                        var quoteIndex = quoteStaffByDepartments.ElementAt(i);
                        var lead = string.Format("Nhóm trưởng : " + quoteIndex.StaffName);

                        row.GetCell(0).SetText(i + 1 + ". " + (quoteIndex.IsLeader ? lead : "" + quoteIndex.StaffName));
                        row.GetCell(0).Paragraphs.First().IndentationLeft = 350;
                        row.GetCell(1).SetText("Bậc AT : " + quoteIndex.SafeLevel + "/5");
                        row.GetCell(1).Paragraphs.First().Alignment = ParagraphAlignment.LEFT;
                    }
                    rowTitle = staffTable.CreateRow();
                }

                //var quoteStaffIds = quoteStaffs.Select(c => c.StaffId);






                staffTable.CreateRow();


                doc.Write(fs);
                fs.Close();
            }
            doc.Close();
            return Json(new { success = true });
        }


        /// <summary>
        /// Download quote paper
        /// </summary>
        /// <returns></returns>
        [HttpGet("/api/quote/{quoteId}/paper/download")]
        public IActionResult DownloadQuotePaper(int quoteId)
        {
            var quote = quoteService.GetQuote(quoteId);
            var webRoot = env.ContentRootPath;
            var fileName = "NPCETC.KTQT.01BM.05-" + quote.Code.Replace("/", ".") + ".docx";
            var file = Path.Combine(webRoot, "caches", fileName);
            return PhysicalFile(file, "application/vnd.openxmlformats-officedocument.wordprocessingml.document", fileName);
        }

        /// <summary>
        /// Next step by quote process 
        /// </summary>
        /// <returns></returns>
        [Authorize]
        [HttpPost("/api/quote/{id}/next")]
        [SwaggerResponse((int)System.Net.HttpStatusCode.OK, Type = typeof(ResultBase<bool>))]
        public IActionResult Next(int id, [FromBody] string action)
        {
            var message = string.Empty;
            var success = quoteService.Next(id, action);
            return Json(new { success, message });
        }

        /// <summary>
        /// Reject
        /// </summary>
        /// <returns></returns>
        [Authorize]
        [HttpPost("/api/quote/{id}/reject")]
        [SwaggerResponse((int)System.Net.HttpStatusCode.OK, Type = typeof(ResultBase<bool>))]
        public IActionResult Reject([FromBody] RejectQuoteModel model)
        {
            var success = quoteService.Reject(model);
            return Json(new { success });
        }

        /// <summary>
        /// Save quote record
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [Authorize]
        [HttpPost("/api/quote/records/save")]
        [SwaggerResponse((int)System.Net.HttpStatusCode.OK, Type = typeof(ResultBase<bool>))]
        public IActionResult SaveQuoteRecord([FromBody]QuoteRecordSaveModel model)
        {
            var userId = HttpContext.User.Identity.GetUserId();
            model.UserId = userId;
            var success = quoteService.SaveQuoteRecord(model);
            return Json(new { success = true });
        }

        /// <summary>
        /// List quote records
        /// </summary>
        /// <returns></returns>
        [Authorize]
        [HttpGet("/api/quote/{id}/records")]
        [SwaggerResponse((int)System.Net.HttpStatusCode.OK, Type = typeof(ResultBase<QuoteRecordFilterResult>))]
        public IActionResult FilterQuoteRecords(int id, QuoteRecordFilterModel model)
        {
            model.QuoteId = id;
            var data = quoteService.FilterQuoteRecords(model);
            return Json(new { success = true, data });
        }

        /// <summary>
        /// Get quote record
        /// </summary>
        /// <returns></returns>
        [Authorize]
        [HttpGet("/api/quote/record/{recordId}")]
        [SwaggerResponse((int)System.Net.HttpStatusCode.OK, Type = typeof(ResultBase<QuoteRecordModel>))]
        public IActionResult GetQuoteRecords(int recordId)
        {
            var data = quoteService.GetQuoteRecords(recordId);
            return Json(new { success = true, data });
        }

        /// <summary>
        /// Submit quote record
        /// </summary>
        /// <returns></returns>
        [Authorize]
        [HttpPost("/api/quote/record/{recordId}/submit")]
        [SwaggerResponse((int)System.Net.HttpStatusCode.OK, Type = typeof(ResultBase<bool>))]
        public IActionResult SubmitQuoteRecord(int recordId)
        {
            var success = quoteService.SubmitQuoteRecord(recordId);
            return Json(new { success = true });
        }

        /// <summary>
        /// Reject quote record
        /// </summary>
        /// <returns></returns>
        [Authorize]
        [HttpPost("/api/quote/record/{recordId}/reject")]
        [SwaggerResponse((int)System.Net.HttpStatusCode.OK, Type = typeof(ResultBase<bool>))]
        public IActionResult RejectQuoteRecord(int recordId)
        {
            var success = quoteService.RejectQuoteRecord(recordId);
            return Json(new { success = true });
        }

        /// <summary>
        /// Approve quote record
        /// </summary>
        /// <returns></returns>
        [Authorize]
        [HttpPost("/api/quote/record/{recordId}/approve")]
        [SwaggerResponse((int)System.Net.HttpStatusCode.OK, Type = typeof(ResultBase<bool>))]
        public IActionResult ApproveQuoteRecord(int recordId)
        {
            var success = quoteService.ApproveQuoteRecord(recordId);
            return Json(new { success = true });
        }

        /// <summary>
        /// Delete quote record
        /// </summary>
        /// <param name="recordId"></param>
        /// <returns></returns>
        [Authorize]
        [HttpDelete("/api/quote/record/{recordId}/delete")]
        [SwaggerResponse((int)System.Net.HttpStatusCode.OK, Type = typeof(ResultBase<bool>))]
        public IActionResult DeleteRecord(int recordId)
        {
            var userId = HttpContext.User.Identity.GetUserId();
            var success = quoteService.DeleteRecord(recordId, userId);
            return Json(new { success = true });
        }

        /// <summary>
        /// Support 1
        /// </summary>
        /// <returns></returns>
        [Authorize]
        [HttpPost("/api/quote/update-new-bg")]
        [SwaggerResponse((int)System.Net.HttpStatusCode.OK, Type = typeof(ResultBase<bool>))]
        public IActionResult UpdateNewBgToBGTBL()
        {
            var result = quoteService.UpdateNewBgToBGTBL();
            return Json(new { success = result });
        }

        [Authorize]
        [HttpPost("/api/quote/convert-departments")]
        [SwaggerResponse((int)System.Net.HttpStatusCode.OK, Type = typeof(ResultBase<bool>))]
        public IActionResult ConvertDepartment()
        {
            var result = quoteService.ConvertDepartment();
            return Json(new { success = result });
        }

        /// <summary>
        /// Upload quote documents
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [Authorize]
        [HttpPost("/api/quote/{id}/attachments/upload")]
        [SwaggerResponse((int)System.Net.HttpStatusCode.OK, Type = typeof(ResultBase<List<Domain.Data.Entity.Document>>))]
        public IActionResult UploadFiles(int id)
        {
            var files = Request.Form.Files;
            var fileModels = new List<FileInfoModel>();
            foreach (var file in files)
            {
                var ms = new MemoryStream();
                file.CopyTo(ms);
                var bytes = ms.ToArray();
                fileModels.Add(new FileInfoModel
                {
                    Bytes = bytes,
                    Mime = file.ContentType,
                    Title = file.FileName,
                    Length = file.Length
                });
            }

            var result = quoteService.AttachDocuments(id, fileModels);
            return Json(new { success = true, data = result });
        }
    }
}
