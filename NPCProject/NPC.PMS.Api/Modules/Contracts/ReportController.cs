﻿using System;
using System.Collections.Generic;
using System.IO;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using NPC.PMS.Api.Helpers;
using NPC.PMS.Domain.Models.Reports;
using NPC.PMS.Domain.Models.Reports.ReportDebt;
using NPC.PMS.Domain.Models.Reports.ReportExistSettlement;
using NPC.PMS.Domain.Models.Reports.ReportRevenue;
using NPC.PMS.Domain.Services.ReportServices;
using Swashbuckle.AspNetCore.Annotations;
using Microsoft.AspNetCore.Hosting;
using OfficeOpenXml;
using System.Linq;
using OfficeOpenXml.Style;
using NPC.PMS.Domain.Models.Shared;
using System.Threading.Tasks;
using NPC.PMS.Domain.Models.Reports.Settlement;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace NPC.PMS.Api.Modules.Contracts
{
    [Route("api/[controller]")]
    public class ReportController : Controller
    {
        private readonly IContractReportService contractReportService;
        private IHostingEnvironment _hostingEnvironment;
        public ReportController(IContractReportService contractReportService, IHostingEnvironment hostingEnvironment)
        {
            this.contractReportService = contractReportService;
            this._hostingEnvironment = hostingEnvironment;
        }
        // GET: api/<controller>
        [HttpGet("/api/report/synthetic")]
        public IEnumerable<SyntheticReportModel> GetSyntheticReport([FromBody]SyntheticReportFilter filter)
        {
            filter = new SyntheticReportFilter();
            var userId = HttpContext.User.Identity.GetUserId();
            filter.UserId = userId;
            var reports = contractReportService.GetSyntheticReport(filter);
            return reports;
        }
        // GET: api/<controller>
        [HttpPost("/api/report/departmentoutput")]
        public IEnumerable<DepartmentOutputReportModel> DepartmentOutput([FromBody]DepartmentOutputFilter filter)
        {
            var userId = HttpContext.User.Identity.GetUserId();
            var reports = contractReportService.GetDepartmentOutput(filter);
            return reports;
        }


        // GET: api/<controller>
        [HttpPost("/api/report/export_departmentoutput")]
        public IActionResult ExportDepartmentOutput([FromBody]DepartmentOutputFilter filter)
        {
            string webRootPath = _hostingEnvironment.ContentRootPath;
            string templatePath = Path.Combine(webRootPath, "Templates", "BAO_CAO_SAN_LUONG.xlsx");
            using (var package = new ExcelPackage(new FileInfo(templatePath)))
            {
                var workbook = package.Workbook;
                var worksheet = workbook.Worksheets.First();
                var res = contractReportService.GetDepartmentOutput(filter);

                //xu ly data
                var index = 0;
                res.ForEach(c =>
                {
                    index++;
                    c.STT = index;
                });
                

                var exportDatas = res.Select(c =>

                    new
                    {
                        c.STT,
                        c.Group,
                        c.ContractCode,
                        c.ContractContent,
                        c.DepartmentName,
                        c.ReportTime,
                        c.WorkitemTitle,
                        c.Quantity,
                        c.Money
                    }
                ).ToList();

                worksheet.Cells["A5"].Value = string.Format("Từ ngày {0} đến ngày {1}", filter.StartDate.Value.ToString("dd/MM/yyyy"), filter.EndDate.Value.ToString("dd/MM/yyyy"));

                worksheet.Cells["A8"].LoadFromCollection(exportDatas);
                var range = worksheet.Cells["A8:I" + (7 + exportDatas.Count()).ToString()];
                range.Style.Border.Top.Style = ExcelBorderStyle.Thin;
                range.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                range.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                range.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                range.Style.WrapText = true;
                range.Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                worksheet.Cells["H8:I" + (7 + exportDatas.Count())].Style.Numberformat.Format = "_ #,##0;-#,##0;_-\"-\"??_-;_-@_-";
                worksheet.Cells["F8:F" + (7 + exportDatas.Count())].Style.Numberformat.Format = "dd/mm/yyyy";
              
                var folder = Path.Combine(webRootPath, "caches");
                if (!Directory.Exists(folder))
                {
                    Directory.CreateDirectory(folder);
                }

                worksheet.Cells[10 + exportDatas.Count, 2].Value = "Ngày     tháng       năm";
                worksheet.Cells[11 + exportDatas.Count, 2].Value = "NGƯỜI BÁO CÁO";

          


                var exportPath = Path.Combine(webRootPath, "caches", Guid.NewGuid().ToString() + ".xlsx");
                package.SaveAs(new FileInfo(exportPath));
                return Json(new ResultBase<object>()
                {
                    success = true,
                    data = new DownloadFileModel()
                    {
                        FileName = "BAO_CAO_SAN_LUONG.xlsx",
                        FilePath = exportPath
                    },

                });
            }
        }






        [HttpGet("/api/report/synthetic/export_to_excel")]
        public FileStreamResult ExportReportSyntheticToExcel()
        {
            var stream = contractReportService.ExportReportSyntheticToExcel();
            var contentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
            stream.Position = 0;
            return File(stream, contentType, "Báo cáo tổng hợp.xlsx");
        }

        /// <summary>
        /// Statistics Department Report One Day
        /// </summary>
        /// <param name="filter"></param>
        /// <returns></returns>
        [HttpPost("/api/report/statistics_report_one_day")]
        [Authorize]
        [SwaggerResponse((int)System.Net.HttpStatusCode.OK, Type = typeof(StatisticReportOneDayModel))]
        public IActionResult StatisticDepartmentReportOneDay([FromBody]YieldDepartmentFilter filter)
        {
            //if (ModelState.IsValid) throw new Exception(ModelState.GetErrorsMessage());

            if (!filter.Date.HasValue) throw new Exception("Yêu cầu chọn ngày để tổng hợp");
            var result = contractReportService.StatisticDepartmentReportOneDay(filter);
            return Json(new { success = true, data = result });
        }

        [HttpPost("/api/report/statistic_exist_settlement_test")]
        //[Authorize]
        [SwaggerResponse((int)System.Net.HttpStatusCode.OK, Type = typeof(ResultBase<ReportExistSettlementModel>))]
        public IActionResult StatisticExistSettlementTest(/*[FromBody] DateTime check*/)
        {
            var check = DateTime.Now;
            if (!ModelState.IsValid) throw new Exception(ModelState.GetErrorsMessage());
            var result = contractReportService.StatisticExistSettlementTest(check);
            return Json(new { success = true, data = result });
        }



        [HttpPost("/api/report/statistic_exist_settlement")]
        //[Authorize]
        [SwaggerResponse((int)System.Net.HttpStatusCode.OK, Type = typeof(ResultBase<ReportExistSettlementModel>))]
        public IActionResult StatisticExistSettlement(/*[FromBody] DateTime check*/)
        {
            var check = DateTime.Now;
            if (!ModelState.IsValid) throw new Exception(ModelState.GetErrorsMessage());
            var result = contractReportService.StatisticExistSettlement(check);
            return Json(new { success = true, data = result });
        }



        [HttpPost("/api/report/statistic_revenue")]
        //[Authorize]
        [SwaggerResponse((int)System.Net.HttpStatusCode.OK, Type = typeof(ResultBase<RevenueReportModel>))]
        public IActionResult StatisticRevenue([FromBody] CheckTimeModel check)
        {
            if (!ModelState.IsValid) throw new Exception(ModelState.GetErrorsMessage());
            var result = contractReportService.StatisticRevenue(check);
            return Json(new { success = true, data = result });
        }

        [HttpPost("/api/report/export_statistic_revenue")]
        public IActionResult ExportStatisticRevenue([FromBody] CheckTimeModel filter)
        {
            string webRootPath = _hostingEnvironment.ContentRootPath;
            string templatePath = Path.Combine(webRootPath, "Templates", "BAO_CAO_THUC_HIEN_DOANH_THU.xlsx");
            using (var package = new ExcelPackage(new FileInfo(templatePath)))
            {
                var workbook = package.Workbook;
                var worksheet = workbook.Worksheets.First();
                var res = contractReportService.StatisticRevenue(filter);

                //xu ly data
                var list = new List<RevenueReportModel>();
                list.Add(new RevenueReportModel()
                {
                    STT = "A",
                    CustomerName = "Các đơn vị trong EVN"
                });
                list.Add(new RevenueReportModel()
                {
                    STT = "  I",
                    CustomerName = "Các đơn vị ngoài EVNNPC"
                });
                var externalEVNNPCs = res.Where(c => c.CompanyType == CompanyType.ExternalEVNPC).ToList();
                var index = 0;
                externalEVNNPCs.ForEach(c =>
                {
                    index++;
                    c.STT = index.ToString();
                });
                list.AddRange(externalEVNNPCs);

                list.Add(new RevenueReportModel()
                {
                    STT = "  II",
                    CustomerName = "Các đơn vị trong EVNNPC"
                });

                var internalEVNNPCs = res.Where(c => c.CompanyType == CompanyType.InternalEVNPC).ToList();
                index = 0;
                internalEVNNPCs.ForEach(c =>
                {
                    index++;
                    c.STT = "    " + index.ToString();
                });
                list.AddRange(internalEVNNPCs);


                list.Add(new RevenueReportModel()
                {
                    STT = "B",
                    CustomerName = "Các đơn vị ngoài EVNNPC"
                });

                var externalEVNs = res.Where(c => c.CompanyType == CompanyType.ExternalEVN).ToList();
                index = 0;
                externalEVNs.ForEach(c =>
                {
                    index++;
                    c.STT = "    " + index.ToString();
                });
                list.AddRange(externalEVNs);
                //add cột tổng.
                list.Add(new RevenueReportModel()
                {
                    CustomerName = "Cộng",
                    Revenue = list.Sum(c => c.Revenue),
                    Vat = list.Sum(c => c.Vat),
                    Value = list.Sum(c => c.Value),

                });

                var exportDatas = list.Select(c =>

                    new
                    {
                        c.STT,
                        c.CustomerName,
                        c.Content,
                        c.ContractCode,
                        c.ContractDate,
                        c.PaymentNumber,
                        c.PaymentDate,
                        c.InvoiceCode,
                        c.InvoiceDate,
                        c.Revenue,
                        c.Vat,
                        c.Value,
                        c.Note
                    }
                ).ToList();

                worksheet.Cells["A5"].Value = string.Format("Từ ngày {0} đến ngày {1}", filter.StartDate.Value.ToString("dd/MM/yyyy"), filter.EndDate.Value.ToString("dd/MM/yyyy"));

                worksheet.Cells["A9"].LoadFromCollection(exportDatas);
                var range = worksheet.Cells["A9:M" + (8 + exportDatas.Count()).ToString()];
                range.Style.Border.Top.Style = ExcelBorderStyle.Thin;
                range.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                range.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                range.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                range.Style.WrapText = true;

                worksheet.Cells["J9:L" + (8 + exportDatas.Count())].Style.Numberformat.Format = "_ #,##0;-#,##0;_-\"-\"??_-;_-@_-";
                worksheet.Cells["E9:E" + (8 + exportDatas.Count())].Style.Numberformat.Format = "dd/mm/yyyy";
                worksheet.Cells["G9:G" + (8 + exportDatas.Count())].Style.Numberformat.Format = "dd/mm/yyyy";
                worksheet.Cells["I9:I" + (8 + exportDatas.Count())].Style.Numberformat.Format = "dd/mm/yyyy";
                var folder = Path.Combine(webRootPath, "caches");
                if (!Directory.Exists(folder))
                {
                    Directory.CreateDirectory(folder);
                }

                worksheet.Cells[10 + exportDatas.Count, 2].Value = "Ngày     tháng       năm";
                worksheet.Cells[11 + exportDatas.Count, 2].Value = "NGƯỜI BÁO CÁO";

                worksheet.Cells[11 + exportDatas.Count, 10].Value = "KẾ TOÁN TRƯỞNG";


                var exportPath = Path.Combine(webRootPath, "caches", Guid.NewGuid().ToString() + ".xlsx");
                package.SaveAs(new FileInfo(exportPath));
                return Json(new ResultBase<object>()
                {
                    success = true,
                    data = new DownloadFileModel()
                    {
                        FileName = "BAO_CAO_DOANH_THU.xlsx",
                        FilePath = exportPath
                    },

                });
            }
        }



        [HttpPost("/api/report/statistic_debt")]
        //[Authorize]
        [SwaggerResponse((int)System.Net.HttpStatusCode.OK, Type = typeof(ResultBase<ReportDebtModel>))]
        public IActionResult StatisticDebt([FromBody] CheckTimeModel check)
        {
            if (!ModelState.IsValid) throw new Exception(ModelState.GetErrorsMessage());
            var result = contractReportService.StatisticDebt(check);
            return Json(new { success = true, data = result });
        }

        [HttpPost("/api/report/export_statistic_debt")]
        public IActionResult ExportStatisticDebt([FromBody] CheckTimeModel filter)
        {
            string webRootPath = _hostingEnvironment.ContentRootPath;
            string templatePath = Path.Combine(webRootPath, "Templates", "BAO_CAO_CONG_NO_PHAI_THU.xlsx");
            using (var package = new ExcelPackage(new FileInfo(templatePath)))
            {
                var workbook = package.Workbook;
                var worksheet = workbook.Worksheets.First();
                var res = contractReportService.StatisticDebt(filter);

                //xu ly data
                var list = new List<ReportDebtModel>();
                list.Add(new ReportDebtModel()
                {
                    STT = "A",
                    CustomerName = "Các đơn vị trong EVN"
                });
                list.Add(new ReportDebtModel()
                {
                    STT = "  I",
                    CustomerName = "Các đơn vị ngoài EVNNPC"
                });
                var externalEVNNPCs = res.Where(c => c.CompanyType == CompanyType.ExternalEVNPC).ToList();
                var index = 0;
                externalEVNNPCs.ForEach(c =>
                {
                    index++;
                    c.STT = index.ToString();
                });
                list.AddRange(externalEVNNPCs);

                list.Add(new ReportDebtModel()
                {
                    STT = "  II",
                    CustomerName = "Các đơn vị trong EVNNPC"
                });

                var internalEVNNPCs = res.Where(c => c.CompanyType == CompanyType.InternalEVNPC).ToList();
                index = 0;
                internalEVNNPCs.ForEach(c =>
                {
                    index++;
                    c.STT = "    " + index.ToString();
                });
                list.AddRange(internalEVNNPCs);


                list.Add(new ReportDebtModel()
                {
                    STT = "B",
                    CustomerName = "Các đơn vị ngoài EVNNPC"
                });

                var externalEVNs = res.Where(c => c.CompanyType == CompanyType.ExternalEVN).ToList();
                index = 0;
                externalEVNs.ForEach(c =>
                {
                    index++;
                    c.STT = "    " + index.ToString();
                });
                list.AddRange(externalEVNs);
                //add cột tổng.
                list.Add(new ReportDebtModel()
                {
                    CustomerName = "Cộng",
                    OpeningStock = list.Sum(c => c.OpeningStock),
                    OpeningStockHave = list.Sum(c => c.OpeningStockHave),
                    Inperiod = list.Sum(c => c.Inperiod),
                    InperiodHave = list.Sum(c => c.InperiodHave),
                    ClosingStock = list.Sum(c => c.ClosingStock),
                    ClosingStockHave = list.Sum(c => c.ClosingStockHave)
                });

                var exportDatas = list.Select(c =>

                    new
                    {
                        c.STT,
                        c.CustomerName,
                        c.Content,
                        c.ContractCode,
                        c.ContractDate,
                        c.InvoiceCode,
                        c.InvoiceDate,
                        c.OpeningStock,
                        c.OpeningStockHave,
                        c.Inperiod,
                        c.InperiodHave,
                        c.ClosingStock,
                        c.ClosingStockHave
                    }
                ).ToList();

                worksheet.Cells["A5"].Value = string.Format("Từ ngày {0} đến ngày {1}", filter.StartDate.Value.ToString("dd/MM/yyyy"), filter.EndDate.Value.ToString("dd/MM/yyyy"));

                worksheet.Cells["A9"].LoadFromCollection(exportDatas);
                var range = worksheet.Cells["A9:M" + (8 + exportDatas.Count()).ToString()];
                range.Style.Border.Top.Style = ExcelBorderStyle.Thin;
                range.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                range.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                range.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                range.Style.WrapText = true;

                worksheet.Cells["H9:M" + (8 + exportDatas.Count())].Style.Numberformat.Format = "_ #,##0;-#,##0;_-\"-\"??_-;_-@_-";
                worksheet.Cells["E9:E" + (8 + exportDatas.Count())].Style.Numberformat.Format = "dd/mm/yyyy";
                worksheet.Cells["G9:G" + (8 + exportDatas.Count())].Style.Numberformat.Format = "dd/mm/yyyy";
                var folder = Path.Combine(webRootPath, "caches");
                if (!Directory.Exists(folder))
                {
                    Directory.CreateDirectory(folder);
                }

                worksheet.Cells[10 + exportDatas.Count, 2].Value = "Ngày     tháng       năm";
                worksheet.Cells[11 + exportDatas.Count, 2].Value = "NGƯỜI BÁO CÁO";

                worksheet.Cells[11 + exportDatas.Count, 10].Value = "KẾ TOÁN TRƯỞNG";


                var exportPath = Path.Combine(webRootPath, "caches", Guid.NewGuid().ToString() + ".xlsx");
                package.SaveAs(new FileInfo(exportPath));
                return Json(new ResultBase<object>()
                {
                    success = true,
                    data = new DownloadFileModel()
                    {
                        FileName = "BAO_CAO_CONG_NO_PHAI_THU.xlsx",
                        FilePath = exportPath
                    },

                });
            }
        }


        [HttpGet]
        [Route("/api/report/download")]
        public async Task<IActionResult> DownloadExport(string filePath, string fileName, string contentType)
        {

            var res = System.IO.File.Exists(filePath);
            if (res == true)
            {
                var memory = new MemoryStream();
                using (var stream = new FileStream(filePath, FileMode.Open))
                {
                    await stream.CopyToAsync(memory);
                }
                memory.Position = 0;
                System.IO.File.Delete(filePath);
                return File(memory, contentType, fileName);
            }
            else
            {
                return Content("filename not present");
            }
        }

        [HttpPost]
        [Route("/api/report/GetSettlementP2Report")]
        public IActionResult GetSettlementP2Report(SettlementP2ReportFilter filter)
        {
            var list = contractReportService.GetSettlementP2Report(filter);
            return Json(new { success = true, data = list });
        }

        [HttpPost]
        [Route("/api/report/GetReportCalendar")]
        public IActionResult GetReportCalendar(SettlementP2ReportFilter filter)
        {
            var list = contractReportService.GetSettlementP2Report(filter);
            return Json(new { success = true, data = list });
        }
    }
}
