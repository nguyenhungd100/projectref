﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Swashbuckle.AspNetCore.Annotations;
using System;
using System.Net;
using System.Linq;
using System.Threading.Tasks;
using NPC.PMS.Domain.Services;
using NPC.PMS.Domain.Services.Roles;
using NPC.PMS.Domain.Models;
using NPC.PMS.Api.Helpers;
using NPC.PMS.Domain.Models.Users;
using NPC.PMS.Domain.Services.Users;
using NPC.PMS.Domain.Data.Entity;
using NPC.PMS.Domain.Models.Calendars;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace NPC.PMS.Api.Modules.Users
{
    /// <summary>
    /// User api to access user's profile and manage user's account
    /// </summary>
    [Route("/api/[controller]")]
    public class UserController : Controller
    {
        private readonly IUserService _userService;
        private readonly IRolesService _rolesService;
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="userService"></param>
        /// <param name="rolesService"></param>
        public UserController(IUserService userService, IRolesService rolesService)
        {
            _userService = userService;
            _rolesService = rolesService;

        }
        /// <summary>
        /// Get user's profile detail with authenticated user
        /// </summary>
        /// <returns></returns>
        [HttpGet("/api/user/get")]
        public IEnumerable<string> Get()
        {

            return new string[] { "value1", "value2" };
        }

        /// <summary>
        /// Get user current loged user info
        /// </summary>
        /// <returns></returns>
        [HttpGet("/api/user/profile")]
        [Authorize]
        //[Authorize]
        [SwaggerResponse((int)HttpStatusCode.OK, Type = typeof(ResultBase<UserModel>))]
        public ActionResult GetMyProfile()
        {
            var userId = HttpContext.User.Identity.GetUserId();
            var userModel = _userService.GetById(userId);
            var permissions = HttpContext.User.GetPermissions();
            userModel.Permissions = permissions;

            return Json(new { success = true, data = userModel });
        }

        /// <summary>
        /// Trả về role user đăng nhập
        /// </summary>
        /// <returns></returns>
        [HttpGet("/api/user/get-my-roles")]
        [Authorize]
        [SwaggerResponse((int)HttpStatusCode.OK, Type = typeof(ResultBase<RoleModel>))]
        public ActionResult GetMyRoles()
        {
            var userId = HttpContext.User.Identity.GetUserId();
            var user = _userService.GetById(userId);

            var roleIds = new List<int>();
            var roles = new List<RoleModel>();

            if (!String.IsNullOrEmpty(user.UserRoles))
            {
                roleIds = user.UserRoles.Split(new string[] { ";" }, StringSplitOptions.RemoveEmptyEntries)
                    .Select(c => Convert.ToInt32(c)).ToList();
                roles = _rolesService.List(roleIds.ToArray());
            }

            return Json(new { success = true, data = roles });
        }

        /// <summary>
        /// Get user info by id
        /// </summary>
        /// <returns></returns>
        [HttpGet("/api/user/get-user-info")]
        [Authorize]
        [SwaggerResponse((int)HttpStatusCode.OK, Type = typeof(ResultBase<UserModel>))]
        public ActionResult GetUserInfo(int userId)
        {
            var userModel = _userService.GetById(userId);
            var permissions = HttpContext.User.GetPermissions();
            userModel.Permissions = permissions;

            return Json(new { success = true, data = userModel });
        }

        /// <summary>
        /// Search users
        /// </summary>
        /// <param name="userSearchModel"></param>
        /// <returns></returns>
        [HttpPost("/api/user/search")]
        [Authorize]
        [SwaggerResponse((int)HttpStatusCode.OK, Type = typeof(ResultBase<BaseFilterResult<UserModel>>))]
        public ActionResult Search([FromBody]UserFilterModel userSearchModel)
        {
            var result = _userService.Search(userSearchModel);
            return Json(new { success = true, data = result });
        }

        /// <summary>
        /// Add user( Admin)
        /// </summary>
        /// <param name="createUserModel"></param>
        /// <returns></returns>
        [HttpPost("/api/user/add-user")]
        [Authorize(Policy = nameof(PermissionCode.MANAGE_USERS))]
        [SwaggerResponse((int)HttpStatusCode.OK, Type = typeof(ResultBase<int>))]
        public ActionResult CreateUser([FromBody]CreateUserModel createUserModel)
        {
            if (!ModelState.IsValid) throw new Exception(ModelState.GetErrorsMessage());

            var result = _userService.CreateUser(createUserModel);
            return Json(new { success = true, data = result });
        }

        /// <summary>
        /// Add user( Admin)
        /// </summary>
        /// <param name="updateUserModel"></param>
        /// <returns></returns>
        [HttpPost("/api/user/update-user")]
        [Authorize(Policy = nameof(PermissionCode.MANAGE_USERS))]
        [SwaggerResponse((int)HttpStatusCode.OK, Type = typeof(ResultBase<UserModel>))]
        public ActionResult UpdateUser([FromBody]UpdateUserModel updateUserModel)
        {
            if (!ModelState.IsValid) throw new Exception(ModelState.GetErrorsMessage());
            var result = _userService.UpdateUser(updateUserModel);
            return Json(new { success = true, data = result });
        }

        /// <summary>
        /// Change password
        /// </summary>
        /// <param name="changePasswordModel"></param>
        /// <returns></returns>
        [HttpPost("/api/user/change-password")]
        [SwaggerResponse((int)HttpStatusCode.OK, Type = typeof(ResultBase<bool>))]
        public ActionResult ChangePassword([FromBody]ChangePasswordModel changePasswordModel)
        {
            if (!ModelState.IsValid) throw new Exception(ModelState.GetErrorsMessage());

            var userId = HttpContext.User.Identity.GetUserId();
            changePasswordModel.UserId = userId;

            var result = _userService.ChangePassword(changePasswordModel);
            return Json(new { success = true, data = result });
        }
        /// <summary>
        /// Update user token
        /// </summary>
        /// <param name="tokenModel"></param>
        /// <returns></returns>
        [Authorize]
        [HttpPost("/api/user/update-token")]
        [SwaggerResponse((int)HttpStatusCode.OK, Type = typeof(ResultBase<>))]
        public ActionResult UpdateToken([FromBody]UpdateTokenModel tokenModel)
        {
            var userId = HttpContext.User.Identity.GetUserId();
            var result = _userService.UpdateToken(userId, tokenModel);
            return Json(new { success = result });
        }

        /// <summary>
        /// Update user profile
        /// </summary>
        /// <param name="userProfileModel"></param>
        /// <returns></returns>
        [Authorize]
        [HttpPost("/api/user/update-profile")]
        [SwaggerResponse((int)System.Net.HttpStatusCode.OK, Type = typeof(ResultBase<>))]
        public ActionResult UpdateProfile([FromBody]UpdateUserProfileModel userProfileModel)
        {
            var userId = HttpContext.User.Identity.GetUserId();
            userProfileModel.UserId = userId;
            var result = _userService.UpdateProfile(userProfileModel);
            return Json(new { success = result });
        }

        [Authorize]
        [HttpPost("/api/user/update-role")]
        [SwaggerResponse((int)System.Net.HttpStatusCode.OK, Type = typeof(ResultBase<bool>))]
        public async Task<ActionResult> UpdateRole([FromBody] UpdateRoleModel model)
        {
            var result = await _userService.UpdateRole(model.Id, model.UserRoles);
            return Json(new { success = result });
        }

        [HttpPost("/api/user/{id}/remove")]
        [SwaggerResponse((int)System.Net.HttpStatusCode.OK, Type = typeof(ResultBase<bool>))]
        [Authorize(Policy = nameof(PermissionCode.MANAGE_USERS))]
        public IActionResult RemoveUser(int id)
        {
            if (!ModelState.IsValid) throw new Exception(ModelState.GetErrorsMessage());

            var currentUser = HttpContext.User.Identity.GetUserId();
            if(currentUser == id) return Json(new { success = false, message = "Bạn không thể xóa tài khoản này!" });
            var result = _userService.RemoveUser(id);
            return Json(new { success = result });
        }

        /// <summary>
        /// Get list delegacy
        /// </summary>
        /// <returns></returns>
        [HttpGet("/api/user/list-delegacy")]
        [SwaggerResponse((int)System.Net.HttpStatusCode.OK, Type = typeof(ResultBase<List<UserDelegacyModel>>))]
        [Authorize]
        public IActionResult GetUserDelegacy()
        {         
            var userCurrentId = HttpContext.User.Identity.GetUserId();
            var result = _userService.GetUserDelegacy(userCurrentId);
            return Json(new { success = true, data = result });
        }


        /// <summary>
        /// Delegacy
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost("/api/user/delegacy")]
        [SwaggerResponse((int)System.Net.HttpStatusCode.OK, Type = typeof(ResultBase<bool>))]
        [Authorize]
        public IActionResult UserDelegacy([FromBody]UserDelegacyModel  model)
        {
            if (!ModelState.IsValid) throw new Exception(ModelState.GetErrorsMessage());

            model.DelegacyUserId = HttpContext.User.Identity.GetUserId();
            var result = _userService.UserDelegacy(model);
            return Json(new { success = result });
        }

        /// <summary>
        /// Evict delegacy
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost("/api/user/evict-delegacy")]
        [SwaggerResponse((int)System.Net.HttpStatusCode.OK, Type = typeof(ResultBase<bool>))]
        [Authorize]
        public IActionResult EvictDelegacy([FromBody]UserDelegacyModel model)
        {
            if (!ModelState.IsValid) throw new Exception(ModelState.GetErrorsMessage());

            model.DelegacyUserId = HttpContext.User.Identity.GetUserId();
            var result = _userService.EvictDelegacy(model);
            return Json(new { success = result });
        }

        

        #region Users Calendars
        /// <summary>
        /// Create user calendars
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost("/api/user/calendar/save")]
        [SwaggerResponse((int)System.Net.HttpStatusCode.OK, Type = typeof(ResultBase<List<UserCalendarModel>>))]
        [Authorize]
        public IActionResult SaveCalendar([FromBody]UserCalendarModel model)
        {
            var result = _userService.CreateCalendar(model);
            return Json(new { success = true, data=result });
        }

        /// <summary>
        /// Delete user calendars
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost("/api/user/calendar/delete/{id}")]
        [SwaggerResponse((int)HttpStatusCode.OK, Type = typeof(ResultBase<bool>))]
        [Authorize]
        public IActionResult DeleteCalendar(int id)
        {
            var userId = User.Identity.GetUserId();
            var result = _userService.DeleteCalendar(id,userId);
            return Json(new { success = result });
        }

        /// <summary>
        /// Get calendars by user id
        /// </summary>
        /// <returns></returns>
        [HttpGet("/api/user/{userId}/calendars")]
        [SwaggerResponse((int)HttpStatusCode.OK, Type = typeof(ResultBase<List<UserCalendarModel>>))]
        [Authorize]
        public IActionResult GetUserCalendars(int userId)
        {
            var result = _userService.GetUserCalendar(userId);
            return Json(new { success = true, data = result });
        }

        /// <summary>
        /// Filter user calendars
        /// </summary>
        /// <returns></returns>
        [HttpPost("/api/user/calendar/get_list")]
        [SwaggerResponse((int)System.Net.HttpStatusCode.OK, Type = typeof(BaseFilterResult<UserCalendarModel>))]
        [Authorize]
        public IActionResult GetListCalendar([FromBody]FilterUserCalendarModel filter)
        {
            var userId = User.Identity.GetUserId();
            var result = _userService.GetListCalendar(filter);
            return Json(new { success = true, data = result });
        }

        /// <summary>
        /// Get user calendars
        /// </summary>
        /// <returns></returns>
        [HttpGet("/api/user/calendar/get")]
        [SwaggerResponse((int)System.Net.HttpStatusCode.OK, Type = typeof(ResultBase<UserCalendarModel>))]
        [Authorize]
        public IActionResult GetCalendar(int id)
        {
            var userId = User.Identity.GetUserId();
            var result = _userService.GetCalendar(id,userId);
            return Json(new { success = true, data = result });
        }
        #endregion
    }
}
