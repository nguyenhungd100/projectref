﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using NPC.PMS.Api.Helpers;
using NPC.PMS.Domain.Models;
using NPC.PMS.Domain.Models.Calendars;
using NPC.PMS.Domain.Models.Users;
using NPC.PMS.Domain.Services.Users;
using Swashbuckle.AspNetCore.Annotations;

namespace NPC.PMS.Api.Modules.Users
{
    public class CalendarsController : Controller
    {
        private readonly ICalendarsService calendarsService;
        public CalendarsController(ICalendarsService calendarsService)
        {
            this.calendarsService = calendarsService;
        }
        /// <summary>
        /// Filter Calendars
        /// </summary>
        /// <param name="filter"></param>
        /// <returns></returns>
        [HttpPost("/api/calendar/filter")]
        [SwaggerResponse((int)System.Net.HttpStatusCode.OK, Type = typeof(CalendarsFilterResult))]
        public IActionResult List([FromBody]UserCalendarsFilter filter)
        {
            filter.UserCurrentId = User.Identity.GetUserId();
            var res = calendarsService.List(filter);
            return Json(new { result = true, data = res });
        }
    }
}