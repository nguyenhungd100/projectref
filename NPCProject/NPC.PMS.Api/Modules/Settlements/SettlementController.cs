﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using NPC.PMS.Api.Helpers;
using NPC.PMS.Domain.Data.Entity;
using NPC.PMS.Domain.Models;
using NPC.PMS.Domain.Models.ContractB;
using NPC.PMS.Domain.Models.Settlements;
using NPC.PMS.Domain.Services.Settlements;
using Swashbuckle.AspNetCore.Annotations;

namespace NPC.PMS.Api.Modules.Settlements
{
    public class SettlementController : Controller
    {
        ISettlementService settlementService;
        public SettlementController(ISettlementService SettlementService)
        {
            settlementService = SettlementService;
        }

        #region Settlement
        /// <summary>
        /// Get list
        /// </summary>
        /// <returns></returns>
        [HttpPost("/api/settlements")]
        [SwaggerResponse((int)System.Net.HttpStatusCode.OK, Type = typeof(ResultBase<SettlementFilterResult>))]
        public IActionResult Filter([FromBody]SettlementFilterModel filter)
        {
            var userId = HttpContext.User.Identity.GetUserId();
            filter.UserId = userId;
            var data = settlementService.Filter(filter);
            return Json(new { success =true , data });
        }
        /// <summary>
        /// Save
        /// </summary>
        /// <returns></returns>
        [HttpPost("/api/settlement/save")]
        [SwaggerResponse((int)System.Net.HttpStatusCode.OK, Type = typeof(bool))]
        [Authorize]
        public IActionResult Save([FromBody]SettlementSaveModel model)
        {
            var userId = HttpContext.User.Identity.GetUserId();
            var res = settlementService.Save(model,userId);
            return Json(new { success = res });
        }

        /// <summary>
        /// Save
        /// </summary>
        /// <returns></returns>
        [HttpPost("/api/settlement/save2Db")]
        [SwaggerResponse((int)System.Net.HttpStatusCode.OK, Type = typeof(bool))]
        [Authorize]
        public IActionResult Save2Db([FromBody]SettlementSaveModel model)
        {
            var userId = HttpContext.User.Identity.GetUserId();
            var res = settlementService.Save(model, userId);
            return Json(new { success = res });
        }

        /// <summary>
        /// Submit
        /// </summary>
        /// <returns></returns>
        [HttpPost("/api/settlement/{id}/submit")]
        [SwaggerResponse((int)System.Net.HttpStatusCode.OK, Type = typeof(bool))]
        public IActionResult Submit(int id)
        {
            var userId = HttpContext.User.Identity.GetUserId();
            var success = settlementService.Submit(id, userId);
            return Json(new { success });
        }
        /// <summary>
        /// Reject
        /// </summary>
        /// <returns></returns>
        [HttpPost("/api/settlement/{id}/reject")]
        [SwaggerResponse((int)System.Net.HttpStatusCode.OK, Type = typeof(bool))]
        public IActionResult Reject(int id)
        {
            var userId = HttpContext.User.Identity.GetUserId();
            var success = settlementService.Reject(id, userId);
            return Json(new { success });
        }
        /// <summary>
        /// Approve
        /// </summary>
        /// <returns></returns>
        [HttpPost("/api/settlement/{id}/approve")]
        [SwaggerResponse((int)System.Net.HttpStatusCode.OK, Type = typeof(bool))]
        public IActionResult Approve(int id)
        {
            var userId = HttpContext.User.Identity.GetUserId();
            var success = settlementService.Approve(id, userId);
            return Json(new { success });
        }
        /// <summary>
        /// Pass
        /// </summary>
        /// <returns></returns>
        [HttpPost("/api/settlement/{id}/pass")]
        [SwaggerResponse((int)System.Net.HttpStatusCode.OK, Type = typeof(bool))]
        public IActionResult Pass(int id)
        {
            var userId = HttpContext.User.Identity.GetUserId();
            var success = settlementService.Pass(id, userId);
            return Json(new { success });
        }
        /// <summary>
        /// Delete
        /// </summary>
        /// <returns></returns>
        [HttpPost("/api/settlement/{id}/delete")]
        [SwaggerResponse((int)System.Net.HttpStatusCode.OK, Type = typeof(bool))]
        public IActionResult Delete(int id)
        {
            var res = settlementService.Delete(id);
            return Json(new { success = res });
        }
        /// <summary>
        /// Receive
        /// </summary>
        /// <returns></returns>
        [HttpPost("/api/settlement/{id}/receive")]
        [SwaggerResponse((int)System.Net.HttpStatusCode.OK, Type = typeof(bool))]
        public IActionResult Receive(int id)
        {
            var userId = HttpContext.User.Identity.GetUserId();
            var res = settlementService.Receive(id, userId);
            return Json(new { success = res });
        }

        /// <summary>
        /// Complete
        /// </summary>
        /// <returns></returns>
        [HttpPost("/api/settlement/{id}/complete")]
        [SwaggerResponse((int)System.Net.HttpStatusCode.OK, Type = typeof(bool))]
        public IActionResult Complete(int id, [FromBody] SettlementModel model)
        {
            var userId = HttpContext.User.Identity.GetUserId();
            model.CreatedBy = userId;
            var res = settlementService.Complete(id, model);
            return Json(new { success = res });
        }
        /// <summary>
        /// p5 reject
        /// </summary>
        /// <returns></returns>
        [HttpPost("/api/settlement/{id}/P5Reject")]
        [SwaggerResponse((int)System.Net.HttpStatusCode.OK, Type = typeof(bool))]
        public IActionResult P5Reject(int id, [FromBody] SettlementModel model)
        {
            var userId = HttpContext.User.Identity.GetUserId();
            model.CreatedBy = userId;
            var res = settlementService.P5Reject(id, model);
            return Json(new { success = res });
        }

        /// <summary>
        /// p2 phúc đáp p5
        /// </summary>
        /// <returns></returns>
        [HttpPost("/api/settlement/{id}/p2reply")]
        [SwaggerResponse((int)System.Net.HttpStatusCode.OK, Type = typeof(bool))]
        public IActionResult P2Reply(int id, [FromBody] SettlementModel model)
        {
            var userId = HttpContext.User.Identity.GetUserId();
            model.CreatedBy = userId;
            var res = settlementService.P2Reply(id, model);
            return Json(new { success = res });
        }

        /// <summary>
        /// Get settlement
        /// </summary>
        /// <returns></returns>
        [HttpGet("/api/settlement/get")]
        [SwaggerResponse((int)System.Net.HttpStatusCode.OK, Type = typeof(List<SettlementModel>))]
        public IActionResult Get(int id)
        {
            var res = settlementService.Get(id);
            return Json(new { success = res != null ? true : false, data = res });
        }
        #endregion

        #region Settlement Document

        /// <summary>
        /// Get list settlement document by type
        /// </summary>
        /// <returns></returns>
        [HttpGet("/api/settlement/documents")]
        [SwaggerResponse((int)System.Net.HttpStatusCode.OK, Type = typeof(List<SettlementModel>))]
        public IActionResult ListDocument(Guid contractUID)
        {
            var res = settlementService.GetSettlementDocuments();
            return Json(new { success = res != null ? true : false, data = res });
        }
        /// <summary>
        /// Save Settlement document
        /// </summary>
        /// <returns></returns>
        [HttpPost("/api/settlement_document/save")]
        [SwaggerResponse((int)System.Net.HttpStatusCode.OK, Type = typeof(SettlementDocumentModel))]
        //[Authorize]
        public IActionResult SaveDocument([FromBody]SettlementDocumentModel model)
        {
            var res = settlementService.SaveDocument(model);
            return Json(new { success = true, data = res });
        }
        /// <summary>
        /// Delete
        /// </summary>
        /// <returns></returns>
        [HttpGet("/api/settlement_document/delete")]
        [SwaggerResponse((int)System.Net.HttpStatusCode.OK, Type = typeof(bool))]
        public IActionResult DeleteDocument(int id)
        {
            var res = settlementService.DeleteCheck(id);
            return Json(new { success = res });
        }
        #endregion

        #region Settlement Check
        /// <summary>
        /// Save Settlement check
        /// </summary>
        /// <returns></returns>
        [HttpPost("/api/settlement_check/list")]
        [SwaggerResponse((int)System.Net.HttpStatusCode.OK, Type = typeof(SettlementCheckModel))]
        //[Authorize]
        public IActionResult ListCheck([FromBody]BaseFilterModel model)
        {
            var res = settlementService.ListCheck(model);
            return Json(new { success = true, data = res });
        }
        /// <summary>
        /// Save Settlement check
        /// </summary>
        /// <returns></returns>
        [HttpPost("/api/settlement_check/save")]
        [SwaggerResponse((int)System.Net.HttpStatusCode.OK, Type = typeof(SettlementCheckModel))]
        //[Authorize]
        public IActionResult SaveCheck([FromBody]SettlementCheckModel model)
        {
            var res = settlementService.SaveCheck(model);
            return Json(new { success = true, data = res });
        }
        /// <summary>
        /// Delete
        /// </summary>
        /// <returns></returns>
        [HttpGet("/api/settlement_check/delete")]
        [SwaggerResponse((int)System.Net.HttpStatusCode.OK, Type = typeof(bool))]
        public IActionResult DeleteCheck(int id)
        {
            var res = settlementService.DeleteCheck(id);
            return Json(new { success = res });
        }

        /// <summary>
        /// add payment
        /// </summary>
        /// <returns></returns>
        [HttpPost("/api/settlement/addpayment")]
        [SwaggerResponse((int)System.Net.HttpStatusCode.OK, Type = typeof(bool))]
        public IActionResult AddPayment([FromBody] SettlementPaymentModel model)
        {
            var res = settlementService.AddPayment(model);
            return Json(new { success = res != null ? true : false, data = res });
        }

        /// <summary>
        /// add payment
        /// </summary>
        /// <returns></returns>
        [HttpPost("/api/settlement/deletePayment")]
        [SwaggerResponse((int)System.Net.HttpStatusCode.OK, Type = typeof(bool))]
        public IActionResult DeletePayment(int id)
        {
            var res = settlementService.DeletePayment(id);
            return Json(new { success = res != null ? true : false, data = res });
        }

        /// <summary>
        /// add payment
        /// </summary>
        /// <returns></returns>
        [HttpGet("/api/settlement/getPayments")]
        [SwaggerResponse((int)System.Net.HttpStatusCode.OK, Type = typeof(List<SettlementPaymentModel>))]
        public IActionResult GetPayments(int settlementId)
        {
            var res = settlementService.GetPayments(settlementId);
            return Json(new { success = true, data = res });
        }
        #endregion
    }
}