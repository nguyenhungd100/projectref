﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using NPC.PMS.Domain.Services.Common.Documents;
using Swashbuckle.AspNetCore.Annotations;
using Microsoft.AspNetCore.Authorization;
using NPC.PMS.Domain.Models.Documents;
using System.IO;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace NPC.PMS.Api.Modules.Documents
{
    [Route("api/[controller]")]
    public class DocumentController : Controller
    {
        private readonly IHostingEnvironment env;
        private readonly IDocumentService documentService;

        public DocumentController(IHostingEnvironment env, IDocumentService documentService)
        {
            this.env = env;
            this.documentService = documentService;
        }
        /// <summary>
        /// Get document
        /// </summary>
        /// <returns></returns>
        [HttpGet("/api/document/{id}")]
        public ActionResult GetDocument(Guid id)
        {
            var document = documentService.GetDocument(id);
            if (document == null) return new EmptyResult();

            var webRoot = env.ContentRootPath;
            var file = System.IO.Path.Combine(webRoot, "caches", document.Id + document.Title);
            if (!System.IO.File.Exists(file))
            {
                if (!System.IO.Directory.Exists(System.IO.Path.Combine(webRoot, "caches"))) System.IO.Directory.CreateDirectory(System.IO.Path.Combine(webRoot, "caches"));
                if (document.Provider == Domain.Enums.StorageProvider.Binary)
                {
                    byte[] bytes = documentService.GetBinaryData(document.StorageId);
                    System.IO.File.WriteAllBytes(file, bytes);
                }
            }
            return PhysicalFile(System.IO.Path.Combine(env.ContentRootPath, "caches", document.Id + document.Title), document.Mime, document.Title);
        }

        /// <summary>
        /// Delete document
        /// </summary>
        /// <returns></returns>
        [HttpDelete("/api/document/{id}/delete")]
        [SwaggerResponse((int)System.Net.HttpStatusCode.OK, Type = typeof(ResultBase<bool>))]
        public ActionResult DeleteDocument(Guid id)
        {
            var success = documentService.DeleteDocument(id);
            return Json(new { success });
        }

        /// <summary>
        /// Upload contract documents
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [Authorize]
        [HttpPost("/api/document/upload")]
        [SwaggerResponse((int)System.Net.HttpStatusCode.OK, Type = typeof(ResultBase<List<Domain.Data.Entity.Document>>))]
        public IActionResult UploadFiles(int id)
        {
            var files = Request.Form.Files;
            var fileModels = new List<FileInfoModel>();
            foreach (var file in files)
            {
                var ms = new MemoryStream();
                file.CopyTo(ms);
                var bytes = ms.ToArray();
                fileModels.Add(new FileInfoModel
                {
                    Bytes = bytes,
                    Mime = file.ContentType,
                    Title = file.FileName,
                    Length = file.Length
                });
            }

            var documents = new List<NPC.PMS.Domain.Data.Entity.Document>();
            foreach (var file in fileModels)
            {
                var document = documentService.AddDocument(file);
                documents.Add(document);
            }

            return Json(new { success = true, data = documents });
        }
    }
}
