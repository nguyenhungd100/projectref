﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using NPC.PMS.Api.Helpers;
using NPC.PMS.Domain.Data.Entity;
using NPC.PMS.Domain.Models.Notifications;
using NPC.PMS.Domain.Services.Common.Documents;
using NPC.PMS.Domain.Services.Common.Notifications;
using Swashbuckle.AspNetCore.Annotations;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace NPC.PMS.Api.Modules.Documents
{
    [Route("api/[controller]")]
    public class NotificationController : Controller
    {
        private readonly INotificationService notificationService;

        public NotificationController(INotificationService notificationService)
        {
            this.notificationService = notificationService;
        }
        /// <summary>
        /// Get notifications
        /// </summary>
        /// <returns></returns>
        [HttpPost("/api/notifications")]
        [Authorize]
        [SwaggerResponse((int)System.Net.HttpStatusCode.OK, Type = typeof(NotificationFilterResult))]
        public ActionResult GetNotifications([FromBody] NotificationFilterModel model)
        {
            var userId = HttpContext.User.Identity.GetUserId();
            model.UserId = userId;
            var result = notificationService.Filter(model);

            return Json(new { success = true, data = result });
        }
        /// <summary>
        /// Set opened
        /// </summary>
        /// <returns></returns>
        [Authorize]
        [HttpPost("/api/notifications/open/{id}")]
        [SwaggerResponse((int)System.Net.HttpStatusCode.OK, Type = typeof(NotificationFilterResult))]
        public ActionResult SetOpened(Guid id)
        {
            var success = notificationService.SetOpened(new List<Guid> { id });

            return Json(new { success});
        }
    }
}
