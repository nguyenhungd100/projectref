﻿using Microsoft.AspNetCore.Mvc;
using NPC.PMS.Domain.Services;

namespace NPC.PMS.Api.Controllers
{
    /// <summary>
    /// 
    /// </summary>
    [Route("/")]
    [ApiController]
    [ApiExplorerSettings(IgnoreApi = true)]
    public class HomeController : ControllerBase
    {
        [HttpGet]
        [HttpGet("/api")]
        public ActionResult<string> Get()
        {
            return "EVN NPC API SERVER";
        }
    }
}
