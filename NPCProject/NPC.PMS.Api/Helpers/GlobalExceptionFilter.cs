﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.Filters;
using Newtonsoft.Json;
using System.Net;

namespace NPC.PMS.Api.Helpers
{
    public class GlobalExceptionFilter : IExceptionFilter
    {
        public void OnException(ExceptionContext context)
        {
            //var exceptionType = context.Exception.GetType();
            //if (exceptionType == typeof(ServiceException))
            //{
            //    string message = context.Exception.Message;
            //    var status = HttpStatusCode.OK;

            //    context.ExceptionHandled = true;
            //    HttpResponse response = context.HttpContext.Response;
            //    response.StatusCode = (int)status;
            //    response.ContentType = "application/json";
            //    var err = new { success = false, message };
            //    response.WriteAsync(JsonConvert.SerializeObject(err));
            //}
            //else
            {
                var status = HttpStatusCode.Accepted;
                context.ExceptionHandled = true;
                HttpResponse response = context.HttpContext.Response;
                response.StatusCode = (int)status;
                response.ContentType = "application/json";
                var err = new { success = false, message = context.Exception.Message, exception = context.Exception.ToString() };
                response.WriteAsync(JsonConvert.SerializeObject(err));
            }
        }
    }
}
