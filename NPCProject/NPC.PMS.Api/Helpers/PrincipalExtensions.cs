﻿using NPC.PMS.Domain.Services;
using NPC.PMS.Domain.Services.Roles;
using System;
using System.Linq;
using System.Security.Claims;

namespace NPC.PMS.Api.Helpers
{
    public static class PrincipalExtensions
    {
        public static bool HasPermission(this ClaimsPrincipal claimsPrincipal, PermissionCode permissionCode)
        {
            var permissionsClaims = claimsPrincipal.Claims.FirstOrDefault(c => c.Type == "permissions");
            if (permissionsClaims == null) return false;

            return permissionsClaims.Value.Contains(";" + permissionCode.GetHashCode() + ";");
        }

        public static string GetPermissions(this ClaimsPrincipal claimsPrincipal)
        {
            var permissionsClaims = claimsPrincipal.Claims.FirstOrDefault(c => c.Type == "permissions");
            if (permissionsClaims == null) return string.Empty;

            return permissionsClaims.Value;
        }
    }
}
