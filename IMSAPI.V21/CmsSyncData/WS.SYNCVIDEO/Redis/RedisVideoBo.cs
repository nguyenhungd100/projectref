﻿using System;
using System.Collections.Generic;
using System.Linq;
using StackExchange.Redis;
using Newtonsoft.Json;
using WS.SYNCVIDEO.Model;
using WS.SYNCVIDEO.Common;

namespace WS.SYNCVIDEO.Redis
{
    public class RedisVideoBo
    {
        private static readonly object _synRoot = new object();

        //private static string strConn = "10.3.11.128:2015,password=fdsf34@4563gd53SFKKJgjs,defaultDatabase=8";
        //private static string nameSpace = "AFamily";
        private static ConnectionMultiplexer _conn = null;        

        private static ConfigurationOptions GetRedisConfiguration(string connectionString)
        {
            var options = ConfigurationOptions.Parse(connectionString);
            options.AbortOnConnectFail = false;
            //options.ClientName = "DESKTOP-K3RU0DD";
            //options.EndPoints.Add("192.168.38.97", 6379);
            //options.Ssl = true;
            //options.Password = "";
            //options.AllowAdmin = true;
            //options.KeepAlive = 30;
            options.ConnectRetry = 3;
            options.ConnectTimeout = 7000;
            options.SyncTimeout = 7000;
            return options;
        }

        protected static ConnectionMultiplexer GetConnection(string strConn)
        {
            try
            {
                lock (_synRoot)
                {
                    if (_conn == null || (null != _conn && !_conn.IsConnected))
                    {
                        _conn = ConnectionMultiplexer.Connect(GetRedisConfiguration(strConn));
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return _conn;
        }

        protected static string NameOf(string nameSpace, params string[] values)
        {
            var name = "";
            if (null != values)
            {
                var strValue = string.Join(":", values);
                if (!string.IsNullOrEmpty(strValue))
                {
                    if (string.IsNullOrEmpty(nameSpace))
                        name = strValue.ToLower();
                    else
                        name = (nameSpace + ":" + strValue).ToLower();
                }
            }
            return name;
        }

        public static bool AddVideo(string nameSpace, string connectRedis, VideoEntity video)
        {
            try
            {                
                var videoCached = new VideoCachedEntity() { VideoInfo = video };                
                
                return AddInHash(nameSpace, connectRedis, video.Id.ToString(), videoCached);
            }
            catch (Exception ex)
            {
                Logger.Log(TypeLog.Error, "RedisVideoBo: AddVideo => " + ex.Message);
                return false;
            }
        }

        private static bool AddInHash<T>(string nameSpace, string connectRedis, string key, T document) where T : class, new()
        {
            var hashId = typeof(T).Name;
            return AddInHash(nameSpace, connectRedis, hashId, key, document);
        }

        private static bool AddInHash<T>(string nameSpace, string connectRedis, string hashId, string key, T document) where T : class, new()
        {
            try
            {
                var valueJson = Common.NewtonJson.Serialize(document);

                bool returnValue;
                using (var conn = GetConnection(connectRedis))
                {
                    var db = conn.GetDatabase();
                    returnValue = db.HashSet(NameOf(nameSpace, hashId), key, valueJson);
                }
                return returnValue;
            }
            catch (Exception ex)
            {
                Logger.Log(TypeLog.Error, "RedisVideoBo: AddInHash => " + ex.Message);
                return false;
            }
        }

        public static bool AddInHash<T>(string nameSpace, string connectRedis, string hashId, IEnumerable<KeyValuePair<string, T>> data) where T : class, new()
        {
            try
            {
                if (data == null) return false;

                var meta = new List<HashEntry>();
                meta.AddRange(data.Select(s => new HashEntry(s.Key, JsonConvert.SerializeObject(s.Value))).ToList());
                using (var conn = GetConnection(connectRedis))
                {
                    var db = conn.GetDatabase();
                    db.HashSet(NameOf(nameSpace, hashId), meta.ToArray());
                }
                return true;
            }
            catch (Exception ex)
            {                
                return false;
            }
        }

        private static bool AddInHash<T>(string nameSpace, string connectRedis, IEnumerable<KeyValuePair<string, T>> data) where T : class, new()
        {
            var hashId = typeof(T).Name;
            return AddInHash(nameSpace, connectRedis, hashId, data);
        }

        private static bool AddInSortedSet<T>(string nameSpace, string connectRedis, string value, double score) where T : class, new()
        {
            var setId = typeof(T).Name;
            return AddInSortedSet(nameSpace, connectRedis, setId, value, score);
        }

        public static bool AddInSortedSet(string nameSpace, string connectRedis, string key, string value, double score)
        {
            try
            {
                var setId = NameOf(nameSpace, key);

                bool returnValue;
                using (var conn = GetConnection(connectRedis))
                {
                    var db = conn.GetDatabase();
                    returnValue = db.SortedSetAdd(setId, value, score);
                }
                return returnValue;
            }
            catch (Exception ex)
            {                
                return false;
            }
        }
    }
}
