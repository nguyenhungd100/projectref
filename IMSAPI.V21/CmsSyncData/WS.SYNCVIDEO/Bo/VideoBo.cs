﻿using WS.SYNCVIDEO.Common;
using WS.SYNCVIDEO.Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using System.Configuration;
using System.Text;
using WS.SYNCVIDEO.ES;
using WS.SYNCVIDEO.Redis;

namespace WS.SYNCVIDEO.Bo
{
    public class VideoBo
    {
        public static SqlConnection GetConnection(string conn="")
        {
            if (string.IsNullOrEmpty(conn))
            {
                conn = "Data Source=172.16.41.125;Initial Catalog=IMS2_AFAMILY_DEV;Persist Security Info=True;User ID=ims_test;Password=ImsTest123$%^789";
            }            
            return new SqlConnection(conn);            
        }

        #region AFamily

        private static ApiClient GetApiClient(string username)
        {
            var rootApi = ConfigurationManager.AppSettings["CmsApi.ApiUrl"];
            var seccretKey = ConfigurationManager.AppSettings["CmsApi.Passcode"];
            var nameSpace = ConfigurationManager.AppSettings["NameSpace"];

            return new ApiClient
            {
                EndPoint = rootApi,
                Method = ApiClient.HttpMethod.POST,
                ContentType = "application/x-www-form-urlencoded; charset=utf-8",
                NameSpace = nameSpace,
                Token = TokenRequest.GetToken(username, seccretKey, nameSpace, rootApi),
                PostData = "",
                ActionName = ""
            };
        }

        #region video
        public static List<VideoEntity> GetDataVideo(string connectStr, string startDate, string currentDate)
        {
            var con = GetConnection(connectStr);
            try
            {
                string query = @"SELECT * FROM Video WITH(NOLOCK) where LastModifiedDate>=@DateFrom and LastModifiedDate<=@DateTo order by LastModifiedDate desc";

                using (var cmd = new SqlCommand(query, con))
                {                    
                    cmd.Parameters.AddWithValue("@DateFrom", startDate);
                    cmd.Parameters.AddWithValue("@DateTo", currentDate);

                    con.Open();
                    var reader = cmd.ExecuteReader();

                    var data = EntityMapper.FillCollection<VideoEntity>(reader);
                    
                    con.Close();                    

                    return data;
                }
            }
            catch (Exception ex)
            {
                Logger.Log(TypeLog.Error, "GetDataVideo => " + ex.Message);
                return new List<VideoEntity>();
            }
            finally
            {
                con.Close();
                con.Dispose();
            }
        }

        public static void SyncDataVideo(string connectStr1, string connectStr2, List<VideoEntity> data, string startDate, string currentDate)
        {
            try
            {
                var count = 0;
                data.Reverse();
                foreach (var item in data)
                {
                    try
                    {
                        if (item != null)
                        {                            
                            //video
                            var resVideo = SyncVideo(connectStr2, item);
                            
                            //VideoInZone
                            SyncVideoInZone(connectStr1, connectStr2, item.Id);

                            //VideoExtension
                            //SyncVideoExtension(connectStr1, connectStr2, item.Id);                            

                            //VideoInTags
                            SyncVideoInTag(connectStr1, connectStr2, item.Id);

                            //VideoTags
                            SyncVideoTags(connectStr1, connectStr2, item.Id);

                            //ThreadVideo
                            //SyncThreadVideo(connectStr1, connectStr2, item.Id);                                               

                            if (resVideo)
                            {                                
                                //sync es, redis
                                SyncVideoEsAndReDist(connectStr2, item);

                                count++;
                                //log ket qua
                                if (count == data.Count)
                                    Logger.WriteFile(startDate + " => " + currentDate + " => Totall: " + data.Count + " => Success: " + count, "ketquasync");
                            }
                        }
                        else
                        {
                            Logger.Log(TypeLog.Bug, "SyncDataVideo => item is null");
                        }
                    }
                    catch (Exception ex)
                    {
                        Logger.Log(TypeLog.Error, "SyncDataVideo => " + ex.Message);
                    }
                }                
            }
            catch (Exception ex)
            {
                Logger.Log(TypeLog.Error, "SyncDataVideo for=> " + ex.Message);
            }            
        }

        #region sync table

        private static bool SyncVideo(string connectStr, VideoEntity item)
        {
            var con = GetConnection(connectStr);
            try {
                string query = @"SET IDENTITY_INSERT [Video] ON;
                                    DELETE [Video] WHERE Id=@Id;
                                    INSERT INTO [Video]([Id]
                                                      ,[ZoneId]
                                                      ,[Name]
                                                      ,[UnsignName]
                                                      ,[Description]
                                                      ,[HtmlCode]
                                                      ,[Avatar]
                                                      ,[KeyVideo]
                                                      ,[Pname]
                                                      ,[Status]
                                                      ,[NewsId]
                                                      ,[Views]
                                                      ,[Mode]
                                                      ,[Tags]
                                                      ,[DistributionDate]
                                                      ,[CreatedBy]
                                                      ,[CreatedDate]
                                                      ,[LastModifiedBy]
                                                      ,[LastModifiedDate]
                                                      ,[PublishDate]
                                                      ,[PublishBy]
                                                      ,[EditedDate]
                                                      ,[EditedBy]
                                                      ,[Url]
                                                      ,[Source]
                                                      ,[VideoRelation]
                                                      ,[FileName]
                                                      ,[Duration]
                                                      ,[Size]
                                                      ,[Capacity]
                                                      ,[AllowAd]
                                                      ,[IsRemoveLogo]
                                                      ,[OriginalUrl]
                                                      ,[Type]
                                                      ,[IsConverted]
                                                      ,[AvatarShareFacebook]
                                                      ,[Author]
                                                      ,[OriginalId]
                                                ) VALUES(@Id
                                                        ,@ZoneId
                                                        ,@Name
                                                        ,@UnsignName
                                                        ,@Description
                                                        ,@HtmlCode
                                                        ,@Avatar
                                                        ,@KeyVideo
                                                        ,@Pname
                                                        ,@Status
                                                        ,@NewsId
                                                        ,@Views
                                                        ,@Mode
                                                        ,@Tags
                                                        ,@DistributionDate
                                                        ,@CreatedBy
                                                        ,@CreatedDate
                                                        ,@LastModifiedBy
                                                        ,@LastModifiedDate
                                                        ,@PublishDate
                                                        ,@PublishBy
                                                        ,@EditedDate
                                                        ,@EditedBy
                                                        ,@Url
                                                        ,@Source
                                                        ,@VideoRelation
                                                        ,@FileName
                                                        ,@Duration
                                                        ,@Size
                                                        ,@Capacity
                                                        ,@AllowAd
                                                        ,@IsRemoveLogo
                                                        ,@OriginalUrl
                                                        ,@Type
                                                        ,@IsConverted
                                                        ,@AvatarShareFacebook
                                                        ,@Author
                                                        ,@OriginalId);
                                                SET IDENTITY_INSERT [Video] OFF;";

                using (var cmd = new SqlCommand(query, con))
                {
                    cmd.CommandType = CommandType.Text;

                    cmd.Parameters.AddWithValue("@Id", item.Id);
                    cmd.Parameters.AddWithValue("@ZoneId", item.ZoneId);
                    cmd.Parameters.AddWithValue("@Name", item.Name ?? (object)DBNull.Value);
                    cmd.Parameters.AddWithValue("@UnsignName", item.UnsignName ?? (object)DBNull.Value);
                    cmd.Parameters.AddWithValue("@Description", item.Description ?? (object)DBNull.Value);
                    cmd.Parameters.AddWithValue("@HtmlCode", item.HtmlCode ?? (object)DBNull.Value);
                    cmd.Parameters.AddWithValue("@Avatar", item.Avatar ?? (object)DBNull.Value);
                    cmd.Parameters.AddWithValue("@KeyVideo", item.KeyVideo ?? (object)DBNull.Value);
                    cmd.Parameters.AddWithValue("@Pname", item.Pname ?? (object)DBNull.Value);
                    cmd.Parameters.AddWithValue("@Status", item.Status);
                    cmd.Parameters.AddWithValue("@NewsId", item.NewsId);
                    cmd.Parameters.AddWithValue("@Views", item.Views);
                    cmd.Parameters.AddWithValue("@Mode", item.Mode);
                    cmd.Parameters.AddWithValue("@Tags", item.Tags ?? (object)DBNull.Value);

                    if (item.DistributionDate <= new DateTime(1980, 1, 1))
                        cmd.Parameters.AddWithValue("@DistributionDate", DBNull.Value);
                    else
                        cmd.Parameters.AddWithValue("@DistributionDate", item.DistributionDate);

                    cmd.Parameters.AddWithValue("@CreatedBy", item.CreatedBy ?? (object)DBNull.Value);

                    if (item.CreatedDate <= new DateTime(1980, 1, 1))
                        cmd.Parameters.AddWithValue("@CreatedDate", DBNull.Value);
                    else
                        cmd.Parameters.AddWithValue("@CreatedDate", item.CreatedDate);

                    cmd.Parameters.AddWithValue("@LastModifiedBy", item.LastModifiedBy ?? (object)DBNull.Value);

                    if (item.LastModifiedDate <= new DateTime(1980, 1, 1))
                        cmd.Parameters.AddWithValue("@LastModifiedDate", DBNull.Value);
                    else
                        cmd.Parameters.AddWithValue("@LastModifiedDate", item.LastModifiedDate);

                    if (item.PublishDate <= new DateTime(1980, 1, 1))
                        cmd.Parameters.AddWithValue("@PublishDate", DBNull.Value);
                    else
                        cmd.Parameters.AddWithValue("@PublishDate", item.PublishDate);

                    cmd.Parameters.AddWithValue("@PublishBy", item.PublishBy ?? (object)DBNull.Value);

                    if (item.EditedDate <= new DateTime(1980, 1, 1))
                        cmd.Parameters.AddWithValue("@EditedDate", DBNull.Value);
                    else
                        cmd.Parameters.AddWithValue("@EditedDate", item.EditedDate);

                    cmd.Parameters.AddWithValue("@EditedBy", item.EditedBy ?? (object)DBNull.Value);
                    cmd.Parameters.AddWithValue("@Url", item.Url ?? (object)DBNull.Value);
                    cmd.Parameters.AddWithValue("@Source", item.Source ?? (object)DBNull.Value);
                    cmd.Parameters.AddWithValue("@VideoRelation", item.VideoRelation ?? (object)DBNull.Value);
                    cmd.Parameters.AddWithValue("@FileName", item.FileName ?? (object)DBNull.Value);
                    cmd.Parameters.AddWithValue("@Duration", item.Duration ?? (object)DBNull.Value);
                    cmd.Parameters.AddWithValue("@Size", item.Size ?? (object)DBNull.Value);
                    cmd.Parameters.AddWithValue("@Capacity", item.Capacity);
                    cmd.Parameters.AddWithValue("@AllowAd", item.AllowAd);
                    cmd.Parameters.AddWithValue("@IsRemoveLogo", item.IsRemoveLogo);
                    cmd.Parameters.AddWithValue("@OriginalUrl", item.OriginalUrl);
                    cmd.Parameters.AddWithValue("@Type", item.Type);
                    cmd.Parameters.AddWithValue("@IsConverted", item.IsConverted);
                    cmd.Parameters.AddWithValue("@AvatarShareFacebook", item.AvatarShareFacebook ?? (object)DBNull.Value);
                    cmd.Parameters.AddWithValue("@Author", item.Author ?? (object)DBNull.Value);
                    cmd.Parameters.AddWithValue("@OriginalId", item.OriginalId);

                    con.Open();
                    var reader = cmd.ExecuteNonQuery();

                    con.Close();

                    return reader > 0;
                }
            }
            catch (Exception ex)
            {                
                Logger.Log(TypeLog.Error, "SyncVideo => " + ex.Message);
                return false;
            }
            finally{                
                con.Close();
                con.Dispose();
            }
        }        

        #region ES
        public static List<ZoneVideoEntity> GetZoneVideoByVideoId(string conn, long id)
        {
            var con = GetConnection(conn);
            try
            {                
                string query = @"SELECT Z.*, NZ.IsPrimary
	                            FROM ZoneVideo AS Z INNER JOIN
                                    VideoInZone AS NZ ON Z.Id = NZ.ZoneId
	                            WHERE NZ.VideoId = @VideoId";
                                
                var cmd = new SqlCommand(query, con);                
                cmd.Parameters.AddWithValue("@VideoId", id);

                con.Open();
                var reader = cmd.ExecuteReader();
                var data = EntityMapper.FillCollection<ZoneVideoEntity>(reader);
                con.Close();

                return data;
            }
            catch (Exception ex)
            {
                con.Close();
                con.Dispose();
                Logger.Log(TypeLog.Error, "GetZoneVideoByVideoId=> " + ex.Message);
                return new List<ZoneVideoEntity>();
            }
            finally
            {
                con.Close();
                con.Dispose();
            }
        }

        private static void SyncVideoEsAndReDist(string connectStr, VideoEntity s)
        {
            var nameSpace = ConfigurationManager.AppSettings["NameSpace"];
            var connectEs= ConfigurationManager.AppSettings["ConnectString.ES"];
            var connectRedis = ConfigurationManager.AppSettings["ConnectString.REDIS"];

            var listZone = GetZoneVideoByVideoId(connectStr, s.Id);
            var zoneIds = new string[] { };
            if (listZone!=null && listZone.Count > 0)
            {
                zoneIds = listZone.Select(z => z.Id.ToString()).ToArray();
                s.ZoneName = listZone.Where(w=>w.IsPrimary==true).Select(z => z.Name).FirstOrDefault();
            }            
                
            var obj = new VideoSearchEntity
            {
                Id = s.Id,
                ZoneId = s.ZoneId,
                Name = s.Name,
                ZoneIds = GetListZoneRelation(connectStr, s.Id).Select(z => z.Id.ToString()).ToArray(),
                PlaylistIds = GetListByVideoId(connectStr, s.Id).Select(z => z.Id.ToString()).ToArray(),
                PlaylistName = GetListByVideoId(connectStr, s.Id).Select(z => z.Name.ToString()).ToArray(),
                ChannelIds = GetVideoChannelByVideoId(connectStr, s.Id).Select(z => z.Id.ToString()).ToArray(),
                ChannelName = GetVideoChannelByVideoId(connectStr, s.Id).Select(z => z.Name.ToString()).ToArray(),
                CreatedBy = s.CreatedBy,
                EditedBy = s.EditedBy,
                PublishBy = s.PublishBy,
                PublishDate = s.PublishDate,
                LastModifiedBy = s.LastModifiedBy,
                LastModifiedDate = s.LastModifiedDate,
                CreatedDate = s.CreatedDate,
                DistributionDate = s.DistributionDate,
                EditedDate = s.EditedDate,
                Mode = s.Mode,
                Status = s.Status,
                Views = s.Views,
                ZoneName = s.ZoneName,
                Avatar = s.Avatar,
                HtmlCode = s.HtmlCode,
                KeyVideo = s.KeyVideo,
                ParentId = s.ParentId < 0 ? 0 : s.ParentId,
                Type = s.Type < 0 ? 0 : s.Type,
                Duration = s.Duration,
                ListType = GetListVideoByParentId(connectStr, s.Id).Select(z => z.Type.ToString()).ToArray(),
                Description = s.Description,
                Url = s.Url,
                FileName = s.FileName
            };
            
            EsVideoBo.Create(nameSpace, connectEs, obj);

            RedisVideoBo.AddVideo(nameSpace, connectRedis, s);
        }

        #region get
        public static List<ZoneVideoEntity> GetListZoneRelation(string conn, int id)
        {
            try
            {
                string query = @"VideoCms_ZoneVideo_GetListRelationVideo";

                var con = GetConnection(conn);
                var cmd = new SqlCommand(query, con);
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.AddWithValue("@VideoId", id);

                con.Open();
                var reader = cmd.ExecuteReader();
                var data = EntityMapper.FillCollection<ZoneVideoEntity>(reader);
                con.Close();

                return data;
            }
            catch (Exception ex)
            {
                return new List<ZoneVideoEntity>();
            }
        }
        public static List<PlaylistEntity> GetListByVideoId(string conn, int id)
        {
            try
            {
                string query = @"VideoCms_Playlist_GetListByVideoId";

                var con = GetConnection(conn);
                var cmd = new SqlCommand(query, con);
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.AddWithValue("@VideoId", id);

                con.Open();
                var reader = cmd.ExecuteReader();
                var data = EntityMapper.FillCollection<PlaylistEntity>(reader);
                con.Close();

                return data;
            }
            catch (Exception ex)
            {
                return new List<PlaylistEntity>();
            }
        }
        public static List<VideoChannelEntity> GetVideoChannelByVideoId(string conn, int id)
        {
            try
            {
                string query = @"CMS_VideoChannel_GetVideoChannelByVideoId";

                var con = GetConnection(conn);
                var cmd = new SqlCommand(query, con);
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.AddWithValue("@VideoId", id);

                con.Open();
                var reader = cmd.ExecuteReader();
                var data = EntityMapper.FillCollection<VideoChannelEntity>(reader);
                con.Close();

                return data;
            }
            catch (Exception ex)
            {
                return new List<VideoChannelEntity>();
            }
        }
        public static List<VideoEntity> GetListVideoByParentId(string conn, int id)
        {
            try
            {
                string query = @"VideoCms_Video_GetListVideoByParentId";

                var con = GetConnection(conn);
                var cmd = new SqlCommand(query, con);
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.AddWithValue("@VideoId", id);

                con.Open();
                var reader = cmd.ExecuteReader();
                var data = EntityMapper.FillCollection<VideoEntity>(reader);
                con.Close();

                return data;
            }
            catch (Exception ex)
            {
                return new List<VideoEntity>();
            }
        }
        #endregion

        #endregion

        #region VideoInZone
        private static void SyncVideoInZone(string connectStr1, string connectStr2, long videoId)
        {
            var con = GetConnection(connectStr1);
            try
            {                
                string query = @"SELECT n.* FROM VideoInZone n                                
                                WHERE n.VideoId=@VideoId";

                using (var cmd = new SqlCommand(query, con))
                {

                    cmd.Parameters.AddWithValue("@VideoId", videoId);

                    con.Open();
                    var reader = cmd.ExecuteReader();

                    var data = EntityMapper.FillCollection<VideoInZoneEntity>(reader);

                    con.Close();

                    InitVideoInZoneToDb(connectStr2, data);
                }              
            }
            catch (Exception ex)
            {                
                Logger.Log(TypeLog.Error, "SyncVideoInZone => " + ex.Message);
            }
            finally
            {
                con.Close();
                con.Dispose();
            }
        }
        private static void InitVideoInZoneToDb(string connectStr, List<VideoInZoneEntity> data)
        {            
            var con = GetConnection(connectStr);
            con.Open();

            try
            {                
                foreach (var item in data)
                {
                    string query = @"DELETE [VideoInZone] WHERE VideoId=@VideoId and ZoneId=@ZoneId;
                                    INSERT INTO [VideoInZone]([VideoId]
                                                            ,[ZoneId]
                                                            ,[IsPrimary]
                                                            ,[LastModifiedDate]
                                                            ,[RootZoneId]
                                                ) VALUES(@VideoId
                                                          ,@ZoneId
                                                          ,@IsPrimary
                                                          ,@LastModifiedDate
                                                          ,@RootZoneId);
                                                ";

                    using (var cmd = new SqlCommand(query, con))
                    {                        
                        cmd.CommandType = CommandType.Text;

                        cmd.Parameters.AddWithValue("@VideoId", item.VideoId);
                        cmd.Parameters.AddWithValue("@ZoneId", item.ZoneId);
                        cmd.Parameters.AddWithValue("@IsPrimary", item.IsPrimary);

                        if (item.LastModifiedDate <= new DateTime(1980, 1, 1))
                            cmd.Parameters.AddWithValue("@LastModifiedDate", DBNull.Value);
                        else
                            cmd.Parameters.AddWithValue("@LastModifiedDate", item.LastModifiedDate);

                        cmd.Parameters.AddWithValue("@RootZoneId", item.RootZoneId);

                        var reader = cmd.ExecuteNonQuery();
                    }                
                }

                con.Close();
            }
            catch (Exception ex)
            {                
                Logger.Log(TypeLog.Error, "InitVideoInZoneToDb => " + ex.Message);
            }
            finally
            {
                con.Close();
                con.Dispose();
            }
        }
        #endregion

        //#region VideoExtension
        //private static void SyncVideoExtension(string connectStr1, string connectStr2, long videoId)
        //{
        //    var con = GetConnection(connectStr1);
        //    try
        //    {
        //        string query = @"SELECT n.* FROM VideoExtension n                                
        //                        WHERE n.VideoId=@VideoId";

        //        using (var cmd = new SqlCommand(query, con))
        //        {

        //            cmd.Parameters.AddWithValue("@VideoId", videoId);

        //            con.Open();
        //            var reader = cmd.ExecuteReader();

        //            var data = EntityMapper.FillCollection<VideoExtensionEntity>(reader);

        //            con.Close();

        //            InitVideoExtensionToDb(connectStr2, data);
        //        }
        //    }
        //    catch (Exception ex)
        //    {                
        //        Logger.Log(TypeLog.Error, "SyncVideoExtension => " + ex.Message);
        //    }
        //    finally
        //    {
        //        con.Close();
        //    }
        //}
        //private static void InitVideoExtensionToDb(string connectStr, List<VideoExtensionEntity> data)
        //{
        //    var con = GetConnection(connectStr);
        //    con.Open();

        //    try
        //    {
        //        foreach (var item in data)
        //        {
        //            string query = @"DELETE [VideoExtension] WHERE VideoId=@VideoId and [Type]=@Type
        //                            INSERT INTO [VideoExtension]([VideoId]
        //                                                        ,[Type]
        //                                                        ,[Value]
        //                                        ) VALUES(@VideoId
        //                                                ,@Type
        //                                                ,@Value)";

        //            using (var cmd = new SqlCommand(query, con))
        //            {
        //                cmd.CommandType = CommandType.Text;

        //                cmd.Parameters.AddWithValue("@VideoId", item.VideoId);
        //                cmd.Parameters.AddWithValue("@Type", item.Type);
        //                cmd.Parameters.AddWithValue("@Value", item.Value ?? (object)DBNull.Value);

        //                var reader = cmd.ExecuteNonQuery();
        //            }
        //        }

        //        con.Close();
        //    }
        //    catch (Exception ex)
        //    {                
        //        Logger.Log(TypeLog.Error, "InitVideoExtensionToDb => " + ex.Message);
        //    }
        //    finally
        //    {
        //        con.Close();
        //        con.Dispose();
        //    }
        //}
        //#endregion

        #region VideoTags
        private static void SyncVideoTags(string connectStr1, string connectStr2, long videoId)
        {
            var con = GetConnection(connectStr1);
            try
            {
                string query = @"SELECT n.* FROM VideoTags n                                
                                WHERE n.VideoId=@VideoId";

                using (var cmd = new SqlCommand(query, con))
                {

                    cmd.Parameters.AddWithValue("@VideoId", videoId);

                    con.Open();
                    var reader = cmd.ExecuteReader();

                    var data = EntityMapper.FillCollection<VideoTagsEntity>(reader);

                    con.Close();

                    InitVideoTagsToDb(connectStr2, data);
                }
            }
            catch (Exception ex)
            {                
                Logger.Log(TypeLog.Error, "SyncVideoTags => " + ex.Message);
            }
            finally
            {
                con.Close();
                con.Dispose();
            }
        }
        private static void InitVideoTagsToDb(string connectStr, List<VideoTagsEntity> data)
        {
            var con = GetConnection(connectStr);
            con.Open();

            try
            {
                foreach (var item in data)
                {
                    string query = @"DELETE [VideoTags] WHERE VideoId=@VideoId and TagId=@TagId
                                    INSERT INTO [VideoTags]([TagId]
                                                              ,[VideoId]
                                                              ,[Type]
                                                              ,[Priority]                                                              
                                                ) VALUES(@TagId
                                                        ,@VideoId
                                                        ,@Type                                                        
                                                        ,@Priority)";

                    using (var cmd = new SqlCommand(query, con))
                    {
                        cmd.CommandType = CommandType.Text;

                        cmd.Parameters.AddWithValue("@TagId", item.TagId);
                        cmd.Parameters.AddWithValue("@VideoId", item.VideoId);                        
                        cmd.Parameters.AddWithValue("@Type", item.Type);
                        cmd.Parameters.AddWithValue("@Priority", item.Priority);

                        var reader = cmd.ExecuteNonQuery();
                    }
                }

                con.Close();
            }
            catch (Exception ex)
            {                
                Logger.Log(TypeLog.Error, "InitVideoTagsToDb => " + ex.Message);
            }
            finally
            {
                con.Close();
                con.Dispose();
            }
        }
        #endregion

        #region VideoInTag
        private static void SyncVideoInTag(string connectStr1, string connectStr2, long videoId)
        {
            var con = GetConnection(connectStr1);
            try
            {
                string query = @"SELECT n.* FROM VideoInTag n                                
                                WHERE n.VideoId=@VideoId";

                using (var cmd = new SqlCommand(query, con))
                {

                    cmd.Parameters.AddWithValue("@VideoId", videoId);

                    con.Open();
                    var reader = cmd.ExecuteReader();

                    var data = EntityMapper.FillCollection<VideoInTagEntity>(reader);

                    con.Close();

                    InitVideoInTagToDb(connectStr2, data);
                }
            }
            catch (Exception ex)
            {
                Logger.Log(TypeLog.Error, "SyncVideoInTag => " + ex.Message);
            }
            finally
            {
                con.Close();
                con.Dispose();
            }
        }
        private static void InitVideoInTagToDb(string connectStr, List<VideoInTagEntity> data)
        {
            var con = GetConnection(connectStr);
            con.Open();

            try
            {
                foreach (var item in data)
                {
                    string query = @"DELETE [VideoInTag] WHERE VideoId=@VideoId and VideoTagId=@VideoTagId
                                    INSERT INTO [VideoTags]([VideoTagId]
                                                              ,[VideoId]
                                                              ,[TagMode]
                                                              ,[Priority]                                                              
                                                ) VALUES(@VideoTagId
                                                        ,@VideoId
                                                        ,@TagMode                                                        
                                                        ,@Priority)";

                    using (var cmd = new SqlCommand(query, con))
                    {
                        cmd.CommandType = CommandType.Text;

                        cmd.Parameters.AddWithValue("@VideoTagId", item.VideoTagId);
                        cmd.Parameters.AddWithValue("@VideoId", item.VideoId);
                        cmd.Parameters.AddWithValue("@TagMode", item.TagMode);
                        cmd.Parameters.AddWithValue("@Priority", item.Priority);

                        var reader = cmd.ExecuteNonQuery();
                    }
                }

                con.Close();
            }
            catch (Exception ex)
            {
                Logger.Log(TypeLog.Error, "InitVideoInTagToDb => " + ex.Message);
            }
            finally
            {
                con.Close();
                con.Dispose();
            }
        }
        #endregion

        //#region ThreadVideo
        //private static void SyncThreadVideo(string connectStr1, string connectStr2, long videoId)
        //{
        //    var con = GetConnection(connectStr1);
        //    try
        //    {
        //        string query = @"SELECT n.* FROM ThreadVideo n                                
        //                        WHERE n.VideoId=@VideoId";

        //        using (var cmd = new SqlCommand(query, con))
        //        {

        //            cmd.Parameters.AddWithValue("@VideoId", videoId);

        //            con.Open();
        //            var reader = cmd.ExecuteReader();

        //            var data = EntityMapper.FillCollection<ThreadVideoEntity>(reader);

        //            con.Close();

        //            InitThreadVideoToDb(connectStr2, data);
        //        }
        //    }
        //    catch (Exception ex)
        //    {                
        //        Logger.Log(TypeLog.Error, "SyncThreadVideo => " + ex.Message);
        //    }
        //    finally
        //    {
        //        con.Close();
        //        con.Dispose();
        //    }
        //}
        //private static void InitThreadVideoToDb(string connectStr, List<ThreadVideoEntity> data)
        //{
        //    var con = GetConnection(connectStr);
        //    con.Open();

        //    try
        //    {
        //        foreach (var item in data)
        //        {
        //            string query = @"DELETE [ThreadVideo] WHERE VideoId=@VideoId and ThreadId=@ThreadId
        //                            INSERT INTO [ThreadVideo]([ThreadId]
        //                                                      ,[VideoId]
        //                                                      ,[Ordinary]                                                             
        //                                                      ,[LastModifiedDate]
        //                                        ) VALUES(@ThreadId
        //                                                ,@VideoId                                                        
        //                                                ,@Ordinary
        //                                                ,@LastModifiedDate)";

        //            using (var cmd = new SqlCommand(query, con))
        //            {
        //                cmd.CommandType = CommandType.Text;

        //                cmd.Parameters.AddWithValue("@ThreadId", item.ThreadId);
        //                cmd.Parameters.AddWithValue("@VideoId", item.VideoId);
        //                cmd.Parameters.AddWithValue("@Ordinary", item.Ordinary);

        //                if (item.LastModifiedDate <= new DateTime(1980, 1, 1))
        //                    cmd.Parameters.AddWithValue("@LastModifiedDate", DBNull.Value);
        //                else
        //                    cmd.Parameters.AddWithValue("@LastModifiedDate", item.LastModifiedDate);

        //                var reader = cmd.ExecuteNonQuery();
        //            }
        //        }

        //        con.Close();
        //    }
        //    catch (Exception ex)
        //    {                
        //        Logger.Log(TypeLog.Error, "InitThreadVideoToDb => " + ex.Message);
        //    }
        //    finally
        //    {
        //        con.Close();
        //        con.Dispose();
        //    }
        //}
        //#endregion                       

        #endregion

        #endregion

        #endregion
    }
}
