﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Runtime.Serialization;
using System.Text;
using WS.SYNCVIDEO.Common;
using WS.SYNCVIDEO.Model;

namespace WS.SYNCVIDEO.Bo
{
    public class ApiClient
    {
        public enum HttpMethod
        {
            GET,
            POST,
            PUT,
            DELETE
        }
        public string EndPoint { get; set; }
        public HttpMethod Method { get; set; }
        public string ContentType { get; set; }
        public string PostData { get; set; }
        public string ActionName { get; set; }
        public string NameSpace { get; set; }
        public string Token { get; set; }
        public ApiClient()
        {
            EndPoint = "";
            Method = HttpMethod.POST;
            ContentType = "application/x-www-form-urlencoded; charset=utf-8";
            NameSpace = "";
            Token = "";
            PostData = "";
            ActionName = "";
        }
        public string MakeRequest()
        {
            try
            {
                var request = (HttpWebRequest)WebRequest.Create(EndPoint + ActionName);

                request.Method = Method.ToString();
                request.Headers.Add("Authorization", Token);
                request.ContentLength = 0;
                request.ContentType = ContentType;

                if (!string.IsNullOrEmpty(PostData) && (Method == HttpMethod.POST || Method == HttpMethod.PUT))
                {
                    var bytes = Encoding.UTF8.GetBytes(PostData);
                    request.ContentLength = bytes.Length;

                    using (var writeStream = request.GetRequestStream())
                    {
                        writeStream.Write(bytes, 0, bytes.Length);
                    }
                }

                using (var response = (HttpWebResponse)request.GetResponse())
                {
                    var responseValue = string.Empty;

                    if (response.StatusCode != HttpStatusCode.OK)
                    {
                        var message = string.Format("Request failed. Received HTTP {0}", response.StatusCode);
                        Logger.Log(TypeLog.Error, message);
                    }

                    using (var responseStream = response.GetResponseStream())
                    {
                        if (responseStream != null)
                            using (var reader = new StreamReader(responseStream))
                            {
                                responseValue = reader.ReadToEnd();
                            }
                    }
                    //Logger.WriteLog(Logger.LogType.Warning, ActionName+":" + DateTime.Now.ToString("hh:mm:ss:fff"));
                    return responseValue;
                }
            }
            catch (Exception ex)
            {
                Logger.Log(TypeLog.Error, "MakeRequest:" + ActionName + " =>" + ex.Message);
                return "";
            }
        }        

        public static WcfActionResponse ConvertWcfResponse(ApiActionResponse response)
        {
            if (response == null)
                response = new ApiActionResponse();
            return new WcfActionResponse
            {
                Data = response.Data,
                ErrorCode = response.Code,
                Message = response.Message,
                Success = response.Success
            };
        }
        public static WcfActionResponse ConvertWcfResponseByObj(NodejsActionResponse response)
        {
            if (response == null)
                response = new NodejsActionResponse();
            return new WcfActionResponse
            {
                Data = NewtonJson.Serialize(response.Data),
                ErrorCode = response.Code,
                Message = response.Message,
                Success = response.Success
            };
        }
        public class ApiActionResponse
        {
            [DataMember]
            public bool Success { get; set; }
            [DataMember]
            public int Code { get; set; }
            [DataMember]
            public string Message { get; set; }
            [DataMember]
            public string Data { get; set; }
            public ApiActionResponse()
            {
                Success = false;
                Code = 0;
                Message = "";
                Data = "";
            }
        }
        public class NodejsActionResponse
        {
            [DataMember]
            public bool Success { get; set; }
            [DataMember]
            public int Code { get; set; }
            [DataMember]
            public string Message { get; set; }
            [DataMember]
            public List<string> Data { get; set; }
            public NodejsActionResponse()
            {
                Success = false;
                Code = 0;
                Message = "";
                Data = null;
            }
        }
    }

    public class TokenRequest
    {        
        public static string GetToken(string username, string password, string nameSpace, string endPoint)
        {
            try
            {
                var request = (HttpWebRequest)WebRequest.Create(endPoint+ "oauth2/token");

                request.Method = "POST";                
                request.ContentLength = 0;
                request.ContentType = "application/x-www-form-urlencoded; charset=utf-8";
                
                var sb = new StringBuilder();
                sb.AppendFormat("username={0}", username);
                sb.AppendFormat("&password={0}", password);
                sb.AppendFormat("&grant_type={0}", "password");
                sb.AppendFormat("&namespace={0}", nameSpace);                

                var PostData = sb.ToString();

                if (!string.IsNullOrEmpty(PostData))
                {
                    var bytes = Encoding.UTF8.GetBytes(PostData);
                    request.ContentLength = bytes.Length;

                    using (var writeStream = request.GetRequestStream())
                    {
                        writeStream.Write(bytes, 0, bytes.Length);
                    }
                }

                using (var response = (HttpWebResponse)request.GetResponse())
                {
                    var responseValue = string.Empty;

                    if (response.StatusCode != HttpStatusCode.OK)
                    {
                        var message = string.Format("Request failed. Received HTTP {0}", response.StatusCode);
                        Logger.Log(TypeLog.Error, message);
                    }

                    using (var responseStream = response.GetResponseStream())
                    {
                        if (responseStream != null)
                            using (var reader = new StreamReader(responseStream))
                            {
                                responseValue = reader.ReadToEnd();
                            }
                    }

                    if (!string.IsNullOrEmpty(responseValue))
                    {
                        var obj = NewtonJson.Deserialize<dynamic>(responseValue);
                        if(obj!=null)
                            return obj.access_token;
                    }

                    return responseValue;
                }
            }
            catch (Exception ex)
            {
                Logger.Log(TypeLog.Error, "GetToken: =>" + ex.Message);
                return "";
            }
        }
    }
}
