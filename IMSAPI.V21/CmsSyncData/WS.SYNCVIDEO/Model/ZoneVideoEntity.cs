﻿using WS.SYNCVIDEO.Common;
using System;
using System.Runtime.Serialization;

namespace WS.SYNCVIDEO.Model
{
    [DataContract]
    public class ZoneVideoEntity : EntityBase
    {
        [DataMember]
        public int Id { get; set; }

        [DataMember]
        public int ParentId { get; set; }

        [DataMember]
        public string Name { get; set; }

        [DataMember]
        public string Url { get; set; }

        [DataMember]
        public int Order { get; set; }

        [DataMember]
        public int Status { get; set; }

        [DataMember]
        public DateTime CreatedDate { get; set; }

        [DataMember]
        public DateTime ModifiedDate { get; set; }

        [DataMember]
        public int CatId { get; set; }

        [DataMember]
        public int NumberOfChild { get; set; }

        [DataMember]
        public int DisplayStyle { get; set; }

        [DataMember]
        public bool ShowOnHome { get; set; }

        [DataMember]
        public bool Invisibled { get; set; }

        [DataMember]
        public string ListNewsZoneId { get; set; }

        [DataMember]
        public string ParentName { get; set; }

        [DataMember]
        public string Avatar { get; set; }

        [DataMember]
        public string AvatarCover { get; set; }

        [DataMember]
        public string ZoneRelation { get; set; }

        [DataMember]
        public string Logo { get; set; }

        [DataMember]
        public string MetaAvatar { get; set; }

        [DataMember]
        public string Keyword { get; set; }

        [DataMember]
        public bool IsPrimary { get; set; }
    }
}
