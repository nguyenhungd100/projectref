﻿using Elasticsearch.Net;
using Nest;
using System;
using System.Collections.Generic;
using WS.SYNCVIDEO.Common;

namespace WS.SYNCVIDEO.ES
{
    public class EsVideoBo
    {
        const int REQUEST_TIMEOUT = 5; //default => 5 seconds
        private static StaticConnectionPool _pool;
        private static ElasticClient _client;
        //private static string strConn = "http://192.168.60.95:9200/";
        //private static string nameSpace = "AFamily";

        protected static string NameOf(string nameSpace, params string[] values)
        {
            var name = "";
            if (null != values)
            {
                var strValue = string.Join("_", values);
                if (!string.IsNullOrEmpty(strValue))
                {
                    if (string.IsNullOrEmpty(nameSpace))
                        name = strValue.ToLower();
                    else
                        name = (nameSpace + "_" + strValue).ToLower();
                }
            }
            return name;
        }

        protected static ElasticClient GetConnection(string strConn)
        {
            try
            {
                if (_client == null)
                {
                    var nodes = new Uri[]
                    {
                        new Uri(strConn)
                    };
                    _pool = new StaticConnectionPool(nodes);
                    var settings = new ConnectionSettings(_pool).RequestTimeout(TimeSpan.FromSeconds(REQUEST_TIMEOUT));

                    _client = new ElasticClient(settings);
                }
            }
            catch(Exception ex)
            {
                Logger.Log(TypeLog.Error, "EsNewsBo: GetConnection => " + ex.Message);
                _client = null;
            }
            return _client;
        }

        public static bool Create<T>(string nameSpace, string connectEs, T document) where T : class
        {
            try
            {
                GetConnection(connectEs);

                string stringId = NameOf(nameSpace, typeof(T).Name);
                var index = _client.Index(document, i => i.Index(stringId).Refresh(Refresh.True));

                return index.IsValid;
            }
            catch(Exception ex)
            {
                Logger.Log(TypeLog.Error, "EsNewsBo: Create => " + ex.Message);
                return false;
            }
        }

        public static bool CreateMany<T>(string nameSpace, string connectEs, List<T> documents) where T : class
        {
            try
            {
                GetConnection(connectEs);

                string stringId = NameOf(nameSpace, typeof(T).Name);
                var bulkIndexer = new BulkDescriptor();
                foreach (var document in documents)
                {
                    bulkIndexer.Index<T>(i => i
                        .Document(document)
                        .Index(stringId)
                        );
                }

                var index = _client.Bulk(bulkIndexer.Refresh(Refresh.True));
                return index.IsValid;
            }
            catch
            {
                return false;
            }
        }

        public static bool CreateIndexSettings<T>(string nameSpace, string connectEs, string fieldNameAnalyzer, List<string> filterAnalyzer = null) where T : class
        {
            try
            {
                GetConnection(connectEs);

                if (filterAnalyzer == null)
                {
                    filterAnalyzer = new List<string>()
                    {
                        "lowercase", "asciifolding"
                    };
                }
                CustomAnalyzer customAnlyzer = new CustomAnalyzer
                {
                    Tokenizer = "standard",
                    Filter = filterAnalyzer
                };

                string stringId = NameOf(nameSpace, typeof(T).Name);
                var index = _client.CreateIndex(stringId, i => i                    
                    .Settings(s => s
                        .Analysis(a => a
                            .Analyzers(al => al
                                .UserDefined("analyzer_userdefind", customAnlyzer)
                            )
                        )
                        .Setting("index.max_result_window", 999999999)
                    )
                    .Mappings(ms => ms
                        .Map<T>(m => m
                            .AutoMap()
                            .Properties(p => p
                                .Text(s => s
                                    .Name(fieldNameAnalyzer)
                                    .Fields(f => f
                                        .Text(ss => ss
                                            .Name("folded")
                                            .Analyzer("analyzer_userdefind")
                                        )
                                    )
                                )
                            )
                        )
                    )
                );

                return index.IsValid;
            }
            catch
            {
                return false;
            }
        }
    }
}
