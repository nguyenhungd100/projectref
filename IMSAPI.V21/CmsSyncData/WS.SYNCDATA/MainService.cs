﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.ServiceProcess;
using System.Threading.Tasks;
using System.Timers;
using WS.SYNCDATA.Bo;
using WS.SYNCDATA.Common;

namespace WS.SYNCDATA
{
    public partial class MainService : ServiceBase
    {
        private Timer time;
        private int count = 1;

        public MainService()
        {
            InitializeComponent();
        }

        protected override void OnStart(string[] args)
        {
            Logger.Log(TypeLog.Info, "In OnStart.");
            
            var timeInterval= Utility.ConvertToDouble(ConfigurationManager.AppSettings["TimeInterval"]);
            if (timeInterval <= 0)
            {
                timeInterval = 60000;
            }
            time = new Timer { Interval = timeInterval };
            time.Elapsed += OnTimer;
            time.Start();
        }

        public void OnTimer(object sender, ElapsedEventArgs args)
        {            
            try
            {
                TaskSyncData();
            }
            catch(Exception ex)
            {
                Logger.Log(TypeLog.Error, ex.Message);
            }
        }

        private void TaskSyncData()
        {            
            var processCount = Task.Run(() =>
            {
                try
                {        
                    //Lấy thời điểm bắt đâu: A            
                    var startDate= Logger.ReadFile();
                    //lưu lại thời điểm lấy: A
                    var currentDate = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
                    Logger.WriteFile(currentDate);

                    //lấy số bản ghi thay đổi từ thời điểm hiện A của IMS1: LastModifilyDate
                    var connectStr1 = ConfigurationManager.AppSettings["ConnectString.IMS1"];
                    var data = NewsBo.GetDataNews(connectStr1, startDate, currentDate);
                    
                    //Đồng bộ QUEU vào db IMS2 -> qua api IMS2
                    if(data!=null && data.Count > 0)
                    {
                        var connectStr2 = ConfigurationManager.AppSettings["ConnectString.IMS2"];
                        NewsBo.SyncDataNews(connectStr1, connectStr2, data, startDate, currentDate);
                    }
                }
                catch(Exception ex) {
                    Logger.Log(TypeLog.Error, "processCount.catch =>" + ex.Message);
                }
            });

            try
            {
                processCount.Wait(TimeSpan.FromMilliseconds(5000));
            }
            catch(Exception ex) {
                Logger.Log(TypeLog.Error, "processCount.Wait =>"+ex.Message);
            }
        }

        protected override void OnStop()
        {
            Logger.Log(TypeLog.Info, "In OnStop.");
            time.Stop();
        }
    }
}
