﻿using System;
using System.Runtime.Serialization;

namespace WS.SYNCDATA.Model
{
    [DataContract]
    public class WcfActionResponse<TPayload>
    {
        [DataMember]
        public bool Success { get; set; }
        [DataMember]
        public int ErrorCode { get; set; }
        [DataMember]
        public string Message { get; set; }
        [DataMember]
        public TPayload Data { get; set; }

        public WcfActionResponse()
        {
            Success = false;
            ErrorCode = 0;
            Message = "";
            Data = default(TPayload);
        }

        #region create response methods

        public static WcfActionResponse<TPayload> CreateErrorResponse(int errorCode, string message)
        {
            return new WcfActionResponse<TPayload>
            {
                Success = false,
                Message = (string.IsNullOrEmpty(message) ? "Error!" : message),
                ErrorCode = errorCode,
                Data = default(TPayload)
            };
        }

        public static WcfActionResponse<TPayload> CreateErrorResponse(Exception ex)
        {
            return CreateErrorResponse(9998, ex.Message);
        }

        public static WcfActionResponse<TPayload> CreateErrorResponse(string message)
        {
            return CreateErrorResponse(9997, message);
        }

        public static WcfActionResponse<TPayload> CreateErrorResponseForInvalidRequest()
        {
            return CreateErrorResponse(9996, "Invalid request!");
        }

        public static WcfActionResponse<TPayload> CreateSuccessResponse(TPayload data, string message)
        {
            return new WcfActionResponse<TPayload>
            {
                Success = true,
                Message = string.IsNullOrEmpty(message) ? "Success!" : message,
                ErrorCode = 0,
                Data = data
            };
        }

        public static WcfActionResponse<TPayload> CreateSuccessResponse(string message)
        {
            return new WcfActionResponse<TPayload>
            {
                Success = true,
                Message = string.IsNullOrEmpty(message) ? "Success!" : message,
                ErrorCode = 0
            };
        }

        public static WcfActionResponse<TPayload> CreateSuccessResponse()
        {
            return CreateSuccessResponse("Success!");
        }

        #endregion
    }

    [DataContract]
    public class WcfActionResponse : WcfActionResponse<string>
    {
        public WcfActionResponse() : base()
        {
            Data = "";
        }

        #region create response methods

        new public static WcfActionResponse CreateErrorResponse(int errorCode, string message)
        {
            return new WcfActionResponse
            {
                Success = false,
                Message = (string.IsNullOrEmpty(message) ? "Error!" : message),
                ErrorCode = errorCode,
                Data = ""
            };
        }

        new public static WcfActionResponse CreateErrorResponse(Exception ex)
        {
            return CreateErrorResponse(9998, ex.Message);
        }

        new public static WcfActionResponse CreateErrorResponse(string message)
        {
            return CreateErrorResponse(9997, message);
        }

        new public static WcfActionResponse CreateErrorResponseForInvalidRequest()
        {
            return CreateErrorResponse(9996, "Invalid request!");
        }

        new public static WcfActionResponse CreateSuccessResponse(string data, string message)
        {
            return new WcfActionResponse
            {
                Success = true,
                Message = string.IsNullOrEmpty(message) ? "Success!" : message,
                ErrorCode = 0,
                Data = data
            };
        }

        new public static WcfActionResponse CreateSuccessResponse(string message)
        {
            return new WcfActionResponse
            {
                Success = true,
                Message = string.IsNullOrEmpty(message) ? "Success!" : message,
                ErrorCode = 0
            };
        }

        new public static WcfActionResponse CreateSuccessResponse()
        {
            return CreateSuccessResponse("Success!");
        }

        #endregion
    }
}
