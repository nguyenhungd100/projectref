﻿using System;
using Nest;

namespace WS.SYNCDATA.Model
{
    [ElasticsearchType]
    public class NewsSearchEntity
    {
        [Number(NumberType.Long, Name = "id", Index = true)]
        public long Id { get; set; }

        [Text(Name = "encrypt_id")]
        public string EncryptId { get; set; }

        [Text(Name = "title", Index = true, Analyzer = "standard")]
        public string Title { get; set; }

        [Text(Name = "zone_ids", Index = true, Analyzer = "standard")]
        public string[] ZoneIds { get; set; }

        [Number(NumberType.Integer, Name = "type", Index = true)]
        public int Type { get; set; }

        [Keyword(Name = "created_by", Index = true)]
        public string CreatedBy { get; set; }

        [Keyword(Name = "last_modified_by")]
        public string LastModifiedBy { get; set; }

        [Keyword(Name = "edited_by", Index = true)]
        public string EditedBy { get; set; }

        [Keyword(Name = "published_by")]
        public string PublishedBy { get; set; }

        [Keyword(Name = "last_receiver")]
        public string LastReceiver { get; set; }

        [Keyword(Name = "approved_by", Index = true)]
        public string ApprovedBy { get; set; }

        [Keyword(Name = "returned_by")]
        public string ReturnedBy { get; set; }

        [Date(Name = "created_date", Index = true)]
        public DateTime CreatedDate { get; set; }

        [Date(Name = "distribution_date", Index = true)]
        public DateTime DistributionDate { get; set; }

        [Date(Name = "last_modified_date")]
        public DateTime LastModifiedDate { get; set; }

        [Date(Name = "approved_date")]
        public DateTime ApprovedDate { get; set; }

        [Number(NumberType.Integer, Name = "view_count")]
        public int ViewCount { get; set; }

        [Number(NumberType.Integer, Name = "status", Index = true)]
        public int Status { get; set; }

        [Boolean(Name = "is_on_home")]
        public bool IsOnHome { get; set; }

        [Text(Name = "avatar")]
        public string Avatar { get; set; }

        [Number(NumberType.Integer, Name = "view_royalties")]
        public int ViewRoyalties { get; set; }

        [Text(Name = "zone_name")]
        public string ZoneName { get; set; }

        [Text(Name = "note")]
        public string Note { get; set; }

        [Text(Name = "author", Index = true, Analyzer = "standard")]
        public string Author { get; set; }

        [Text(Name = "error_checked_by")]
        public string ErrorCheckedBy { get; set; }

        [Date(Name = "error_checked_date")]
        public DateTime ErrorCheckedDate { get; set; }

        [Keyword(Name = "sensitive_checked_by")]
        public string SensitiveCheckedBy { get; set; }

        [Date(Name = "sensitive_checked_date")]
        public DateTime SensitiveCheckedDate { get; set; }

        [Number(NumberType.Integer, Name = "word_count")]
        public int WordCount { get; set; }

        [Text(Name = "sapo")]
        public string Sapo { get; set; }

        [Text(Name = "url")]
        public string Url { get; set; }

        [Number(NumberType.Integer, Name = "display_posotion", Index = true)]
        public int DisplayPosition { get; set; }

        [Number(NumberType.Integer, Name = "priority", Index = true)]
        public int Priority { get; set; }
    }
}
