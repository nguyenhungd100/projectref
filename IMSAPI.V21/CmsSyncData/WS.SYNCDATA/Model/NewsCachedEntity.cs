﻿using WS.SYNCDATA.Common;
using System.Runtime.Serialization;

namespace WS.SYNCDATA.Model
{
    [DataContract]
    public class NewsCachedEntity : EntityBase
    {
        [DataMember]
        public NewsEntity NewsInfo { get; set; }

        //[DataMember]
        //public List<ZoneWithSimpleFieldEntity> AllZone { get; set; }
        //[DataMember]
        //public List<NewsInZoneEntity> NewsInZone { get; set; }
        //[DataMember]
        //public List<TagNewsWithTagInfoEntity> TagInNews { get; set; }
        //[DataMember]
        //public List<NewsPublishForNewsRelationEntity> NewsRelation { get; set; }
        //[DataMember]
        //public List<NewsPublishForNewsRelationEntity> NewsRelationSpecial { get; set; }
        //[DataMember]
        //public List<NewsByAuthorEntity> NewsByAuthor { get; set; }
        //[DataMember]
        //public List<NewsBySourceEntity> NewsBySource { get; set; }
        //[DataMember]
        //public List<NewsVersionWithSimpleFieldsEntity> ListVersion { get; set; }
        //[DataMember]
        //public List<NewsChildEntity> ListNewsChild { get; set; }
        //[DataMember]
        //public List<NewsExtensionEntity> NewsExtensions { get; set; }
        //[DataMember]
        //public string NewsContentWithTemplate { get; set; }
        //[DataMember]
        //public int ExpertId { get; set; }
        //[DataMember]
        //public string ExpertQuote { get; set; }
        //[DataMember]
        //public VoteEntity VoteInfo { get; set; }
        //[DataMember]
        //public int TopicId { get; set; }
        //[DataMember]
        //public List<BrandContentEntity> BrandContent { get; set; }
        //[DataMember]
        //public List<ThreadEntity> ThreadNewsInfo { get; set; }
        //[DataMember]
        //public NewsPrEntity NewsPr { get; set; }
    }
}
