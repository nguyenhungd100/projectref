﻿using WS.SYNCDATA.Common;
using WS.SYNCDATA.Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using System.Configuration;
using System.Text;
using WS.SYNCDATA.ES;
using WS.SYNCDATA.Redis;

namespace WS.SYNCDATA.Bo
{
    public class NewsBo
    {
        public static SqlConnection GetConnection(string conn="")
        {
            if (string.IsNullOrEmpty(conn))
            {
                conn = "Data Source=172.16.41.125;Initial Catalog=IMS2_AFAMILY_DEV;Persist Security Info=True;User ID=ims_test;Password=ImsTest123$%^789";
            }            
            return new SqlConnection(conn);            
        }

        #region AFamily

        private static ApiClient GetApiClient(string username)
        {
            var rootApi = ConfigurationManager.AppSettings["CmsApi.ApiUrl"];
            var seccretKey = ConfigurationManager.AppSettings["CmsApi.Passcode"];
            var nameSpace = ConfigurationManager.AppSettings["NameSpace"];

            return new ApiClient
            {
                EndPoint = rootApi,
                Method = ApiClient.HttpMethod.POST,
                ContentType = "application/x-www-form-urlencoded; charset=utf-8",
                NameSpace = nameSpace,
                Token = TokenRequest.GetToken(username, seccretKey, nameSpace, rootApi),
                PostData = "",
                ActionName = ""
            };
        }

        #region news
        public static List<NewsEntity> GetDataNews(string connectStr, string startDate, string currentDate)
        {
            var con = GetConnection(connectStr);
            try
            {
                string query = @"SELECT * FROM News WITH(NOLOCK) where LastModifiedDate>=@DateFrom and LastModifiedDate<=@DateTo order by LastModifiedDate desc";

                using (var cmd = new SqlCommand(query, con))
                {                    
                    cmd.Parameters.AddWithValue("@DateFrom", startDate);
                    cmd.Parameters.AddWithValue("@DateTo", currentDate);

                    con.Open();
                    var reader = cmd.ExecuteReader();

                    var data = EntityMapper.FillCollection<NewsEntity>(reader);
                    
                    con.Close();                    

                    return data;
                }
            }
            catch (Exception ex)
            {
                Logger.Log(TypeLog.Error, "GetDataNews => " + ex.Message);
                return new List<NewsEntity>();
            }
            finally
            {
                con.Close();
                con.Dispose();
            }
        }

        public static void SyncDataNews(string connectStr1, string connectStr2, List<NewsEntity> data, string startDate, string currentDate)
        {
            try
            {
                var count = 0;
                data.Reverse();    
                foreach (var item in data)
                {
                    try
                    {
                        if (item != null)
                        {
                            //Sử dụng api ims2
                            //var apiClient = GetApiClient(item.CreatedBy);
                            //PostDataApi(item, apiClient);

                            //news
                            var resNews = SyncNews(connectStr2, item);
                            
                            //NewsInZone
                            SyncNewsInZone(connectStr1, connectStr2, item.Id);

                            //NewsExtension
                            SyncNewsExtension(connectStr1, connectStr2, item.Id);

                            //TagNews
                            SyncTagNews(connectStr1, connectStr2, item.Id);

                            //ThreadNews
                            SyncThreadNews(connectStr1, connectStr2, item.Id);

                            //NewsRelation
                            SyncNewsRelation(connectStr1, connectStr2, item.Id);

                            //NewsPublish
                            SyncNewsPublish(connectStr1, connectStr2, item);

                            //NewsContent
                            SyncNewsContent(connectStr1, connectStr2, item);

                            if (resNews)
                            {
                                //Update lai chuan avatar theo namespace
                                UpdateDbChuanHoaNoteByHuu(connectStr2, item.Id);

                                //sync es, redis
                                SyncNewsEsAndReDist(connectStr2, item);

                                count++;
                                //log ket qua
                                if (count == data.Count)
                                    Logger.WriteFile(startDate + " => " + currentDate + " => Totall: " + data.Count + " => Success: " + count, "ketquasync");
                            }
                        }
                        else
                        {
                            Logger.Log(TypeLog.Bug, "SyncDataNews => item is null");
                        }
                    }
                    catch (Exception ex)
                    {
                        Logger.Log(TypeLog.Error, "SyncDataNews => " + ex.Message);
                    }
                }                
            }
            catch (Exception ex)
            {
                Logger.Log(TypeLog.Error, "SyncDataNews for=> " + ex.Message);
            }            
        }

        #region sync table

        private static bool SyncNews(string connectStr, NewsEntity item)
        {
            var con = GetConnection(connectStr);
            try {
                string query = @"DELETE [News] WHERE Id=@Id
                                    INSERT INTO [News]([Id]
                                                      ,[Title]
                                                      ,[SubTitle]
                                                      ,[Sapo]
                                                      ,[Body]
                                                      ,[Avatar]
                                                      ,[AvatarDesc]
                                                      ,[Avatar2]
                                                      ,[Avatar3]
                                                      ,[Avatar4]
                                                      ,[Avatar5]
                                                      ,[Author]
                                                      ,[NewsRelation]
                                                      ,[Status]
                                                      ,[Source]
                                                      ,[IsFocus]
                                                      ,[Type]
                                                      ,[ThreadId]
                                                      ,[CreatedDate]
                                                      ,[LastModifiedDate]
                                                      ,[DistributionDate]
                                                      ,[CreatedBy]
                                                      ,[LastModifiedBy]
                                                      ,[PublishedBy]
                                                      ,[EditedBy]
                                                      ,[LastReceiver]
                                                      ,[ApprovedBy]
                                                      ,[WordCount]
                                                      ,[ViewCount]
                                                      ,[Priority]
                                                      ,[Tag]
                                                      ,[Note]
                                                      ,[TagPrimary]
                                                      ,[Price]
                                                      ,[DisplayStyle]
                                                      ,[DisplayPosition]
                                                      ,[DisplayInSlide]
                                                      ,[AvatarCustom]
                                                      ,[OriginalId]
                                                      ,[NewsType]
                                                      ,[IsOnHome]
                                                      ,[Url]
                                                      ,[NoteRoyalties]
                                                      ,[TagItem]
                                                      ,[NewsCategory]
                                                      ,[InitSapo]
                                                      ,[TemplateName]
                                                      ,[TemplateConfig]
                                                      ,[InterviewId]
                                                      ,[IsBreakingNews]
                                                      ,[OriginalUrl]
                                                      ,[IsPr]
                                                      ,[AdStore]
                                                      ,[AdStoreUrl]
                                                      ,[PrBookingNumber]
                                                      ,[PegaBreakingNews]
                                                      ,[RollingNewsId]
                                                      ,[IsOnMobile]
                                                      ,[TagSubTitleId]
                                                      ,[PrPosition]
                                                      ,[LocationType]
                                                      ,[ExpiredDate]
                                                      ,[SourceUrl]
                                                      ,[BonusPrice]
                                                      ,[ViewCountMobile]
                                                      ,[PhotoCount]
                                                      ,[VideoCount]
                                                      ,[ShortTitle]
                                                      ,[ParentNewsId]
                                                      ,[WarningLevel]
                                                ) VALUES(@Id
                                                      ,@Title
                                                      ,@SubTitle
                                                      ,@Sapo
                                                      ,@Body
                                                      ,@Avatar
                                                      ,@AvatarDesc
                                                      ,@Avatar2
                                                      ,@Avatar3
                                                      ,@Avatar4
                                                      ,@Avatar5
                                                      ,@Author
                                                      ,@NewsRelation
                                                      ,@Status
                                                      ,@Source
                                                      ,@IsFocus
                                                      ,@Type
                                                      ,@ThreadId
                                                      ,@CreatedDate
                                                      ,@LastModifiedDate
                                                      ,@DistributionDate
                                                      ,@CreatedBy
                                                      ,@LastModifiedBy
                                                      ,@PublishedBy
                                                      ,@EditedBy
                                                      ,@LastReceiver
                                                      ,@ApprovedBy
                                                      ,@WordCount
                                                      ,@ViewCount
                                                      ,@Priority
                                                      ,@Tag
                                                      ,@Note
                                                      ,@TagPrimary
                                                      ,@Price
                                                      ,@DisplayStyle
                                                      ,@DisplayPosition
                                                      ,@DisplayInSlide
                                                      ,@AvatarCustom
                                                      ,@OriginalId
                                                      ,@NewsType
                                                      ,@IsOnHome
                                                      ,@Url
                                                      ,@NoteRoyalties
                                                      ,@TagItem
                                                      ,@NewsCategory
                                                      ,@InitSapo
                                                      ,@TemplateName
                                                      ,@TemplateConfig
                                                      ,@InterviewId
                                                      ,@IsBreakingNews
                                                      ,@OriginalUrl
                                                      ,@IsPr
                                                      ,@AdStore
                                                      ,@AdStoreUrl
                                                      ,@PrBookingNumber
                                                      ,@PegaBreakingNews
                                                      ,@RollingNewsId
                                                      ,@IsOnMobile
                                                      ,@TagSubTitleId
                                                      ,@PrPosition
                                                      ,@LocationType
                                                      ,@ExpiredDate
                                                      ,@SourceUrl
                                                      ,@BonusPrice
                                                      ,@ViewCountMobile
                                                      ,@PhotoCount
                                                      ,@VideoCount
                                                      ,@ShortTitle
                                                      ,@ParentNewsId
                                                      ,@WarningLevel)";
                                                                
                var cmd = new SqlCommand(query, con);
                cmd.CommandType = CommandType.Text;

                cmd.Parameters.AddWithValue("@Id", item.Id);
                cmd.Parameters.AddWithValue("@Title", item.Title ?? (object)DBNull.Value);
                cmd.Parameters.AddWithValue("@SubTitle", item.SubTitle ?? (object)DBNull.Value);
                cmd.Parameters.AddWithValue("@Sapo", item.Sapo ?? (object)DBNull.Value);
                cmd.Parameters.AddWithValue("@Body", item.Body ?? (object)DBNull.Value);
                cmd.Parameters.AddWithValue("@Avatar", item.Avatar ?? (object)DBNull.Value);
                cmd.Parameters.AddWithValue("@AvatarDesc", item.AvatarDesc ?? (object)DBNull.Value);
                cmd.Parameters.AddWithValue("@Avatar2", item.Avatar2 ?? (object)DBNull.Value);
                cmd.Parameters.AddWithValue("@Avatar3", item.Avatar3 ?? (object)DBNull.Value);
                cmd.Parameters.AddWithValue("@Avatar4", item.Avatar4 ?? (object)DBNull.Value);
                cmd.Parameters.AddWithValue("@Avatar5", item.Avatar5 ?? (object)DBNull.Value);
                cmd.Parameters.AddWithValue("@Author", item.Author ?? (object)DBNull.Value);
                cmd.Parameters.AddWithValue("@NewsRelation", item.NewsRelation ?? (object)DBNull.Value);
                cmd.Parameters.AddWithValue("@Status", item.Status);
                cmd.Parameters.AddWithValue("@Source", item.Source ?? (object)DBNull.Value);
                cmd.Parameters.AddWithValue("@IsFocus", item.IsFocus);

                if (item.Type <= 0)
                    cmd.Parameters.AddWithValue("@Type", 0);
                else
                    cmd.Parameters.AddWithValue("@Type", item.Type);

                cmd.Parameters.AddWithValue("@ThreadId", item.ThreadId);

                if (item.CreatedDate <= new DateTime(1980, 1, 1))
                    cmd.Parameters.AddWithValue("@CreatedDate", DBNull.Value);
                else
                    cmd.Parameters.AddWithValue("@CreatedDate", item.CreatedDate);

                if (item.LastModifiedDate <= new DateTime(1980, 1, 1))
                    cmd.Parameters.AddWithValue("@LastModifiedDate", DBNull.Value);
                else
                    cmd.Parameters.AddWithValue("@LastModifiedDate", item.LastModifiedDate);

                if (item.DistributionDate <= new DateTime(1980, 1, 1))
                    cmd.Parameters.AddWithValue("@DistributionDate", DBNull.Value);
                else
                    cmd.Parameters.AddWithValue("@DistributionDate", item.DistributionDate);

                cmd.Parameters.AddWithValue("@CreatedBy", item.CreatedBy ?? (object)DBNull.Value);
                cmd.Parameters.AddWithValue("@LastModifiedBy", item.LastModifiedBy ?? (object)DBNull.Value);
                cmd.Parameters.AddWithValue("@PublishedBy", item.PublishedBy ?? (object)DBNull.Value);
                cmd.Parameters.AddWithValue("@EditedBy", item.EditedBy ?? (object)DBNull.Value);
                cmd.Parameters.AddWithValue("@LastReceiver", item.LastReceiver ?? (object)DBNull.Value);

                if (item.Status == 6)                
                    cmd.Parameters.AddWithValue("@ApprovedBy", item.PublishedBy ?? (object)DBNull.Value);
                else
                    cmd.Parameters.AddWithValue("@ApprovedBy", item.ApprovedBy ?? (object)DBNull.Value);


                if (item.WordCount <= 0)
                    cmd.Parameters.AddWithValue("@WordCount", 0);
                else
                    cmd.Parameters.AddWithValue("@WordCount", item.WordCount);

                cmd.Parameters.AddWithValue("@ViewCount", item.ViewCount);

                if (item.Priority <= 0)
                    cmd.Parameters.AddWithValue("@Priority", 0);
                else
                    cmd.Parameters.AddWithValue("@Priority", item.Priority);

                cmd.Parameters.AddWithValue("@Tag", item.Tag ?? (object)DBNull.Value);
                cmd.Parameters.AddWithValue("@Note", item.Note ?? (object)DBNull.Value);
                cmd.Parameters.AddWithValue("@TagPrimary", item.TagPrimary);

                if (item.Price < 0)
                    cmd.Parameters.AddWithValue("@Price", DBNull.Value);
                else
                    cmd.Parameters.AddWithValue("@Price", item.Price);

                cmd.Parameters.AddWithValue("@DisplayStyle", item.DisplayStyle);
                cmd.Parameters.AddWithValue("@DisplayPosition", item.DisplayPosition);
                cmd.Parameters.AddWithValue("@DisplayInSlide", item.DisplayInSlide);
                cmd.Parameters.AddWithValue("@AvatarCustom", item.AvatarCustom ?? (object)DBNull.Value);
                cmd.Parameters.AddWithValue("@OriginalId", item.OriginalId);

                if (item.NewsType <= 0)
                    cmd.Parameters.AddWithValue("@NewsType", 0);
                else
                    cmd.Parameters.AddWithValue("@NewsType", item.NewsType);

                cmd.Parameters.AddWithValue("@IsOnHome", item.IsOnHome);
                cmd.Parameters.AddWithValue("@Url", item.Url ?? (object)DBNull.Value);
                cmd.Parameters.AddWithValue("@NoteRoyalties", item.NoteRoyalties ?? (object)DBNull.Value);
                cmd.Parameters.AddWithValue("@TagItem", item.TagItem ?? (object)DBNull.Value);
                cmd.Parameters.AddWithValue("@NewsCategory", item.NewsCategory);
                cmd.Parameters.AddWithValue("@InitSapo", item.InitSapo ?? (object)DBNull.Value);
                cmd.Parameters.AddWithValue("@TemplateName", item.TemplateName ?? (object)DBNull.Value);
                cmd.Parameters.AddWithValue("@TemplateConfig", item.TemplateConfig ?? (object)DBNull.Value);
                cmd.Parameters.AddWithValue("@InterviewId", item.InterviewId);
                cmd.Parameters.AddWithValue("@IsBreakingNews", item.IsBreakingNews);
                cmd.Parameters.AddWithValue("@OriginalUrl", item.OriginalUrl ?? (object)DBNull.Value);
                cmd.Parameters.AddWithValue("@IsPr", item.IsPr);
                cmd.Parameters.AddWithValue("@AdStore", item.AdStore);
                cmd.Parameters.AddWithValue("@AdStoreUrl", item.AdStoreUrl ?? (object)DBNull.Value);
                cmd.Parameters.AddWithValue("@PrBookingNumber", item.PrBookingNumber ?? (object)DBNull.Value);
                cmd.Parameters.AddWithValue("@PegaBreakingNews", item.PegaBreakingNews);
                cmd.Parameters.AddWithValue("@RollingNewsId", item.RollingNewsId);
                cmd.Parameters.AddWithValue("@IsOnMobile", item.IsOnMobile);
                cmd.Parameters.AddWithValue("@TagSubTitleId", item.TagSubTitleId);
                cmd.Parameters.AddWithValue("@PrPosition", item.PrPosition);
                cmd.Parameters.AddWithValue("@LocationType", item.LocationType);

                if (item.ExpiredDate <= new DateTime(1980, 1, 1))
                    cmd.Parameters.AddWithValue("@ExpiredDate", DBNull.Value);
                else
                    cmd.Parameters.AddWithValue("@ExpiredDate", item.ExpiredDate);

                cmd.Parameters.AddWithValue("@SourceUrl", item.SourceUrl ?? (object)DBNull.Value);
                cmd.Parameters.AddWithValue("@BonusPrice", item.BonusPrice);
                cmd.Parameters.AddWithValue("@ViewCountMobile", item.ViewCountMobile);
                cmd.Parameters.AddWithValue("@PhotoCount", item.PhotoCount);
                cmd.Parameters.AddWithValue("@VideoCount", item.VideoCount);
                cmd.Parameters.AddWithValue("@ShortTitle", item.ShortTitle ?? (object)DBNull.Value);
                cmd.Parameters.AddWithValue("@ParentNewsId", item.ParentNewsId);
                cmd.Parameters.AddWithValue("@WarningLevel", item.WarningLevel);

                con.Open();
                var reader = cmd.ExecuteNonQuery();

                con.Close();

                return reader > 0;
            }
            catch (Exception ex)
            {                
                Logger.Log(TypeLog.Error, "SyncNews => " + ex.Message);
                return false;
            }
            finally{                
                con.Close();
                con.Dispose();
            }
        }        

        #region ES
        public static List<ZoneEntity> GetZoneByNewsId(string conn, long id)
        {
            var con = GetConnection(conn);
            try
            {                
                string query = @"SELECT Z.Id, Z.Name, Z.Description, Z.ModifiedDate, Z.CreatedDate, Z.ShortURL, Z.SortOrder, 
		                            Z.ParentId, Z.Invisibled, Z.Status, NZ.IsPrimary
	                            FROM Zone AS Z INNER JOIN
                                    NewsInZone AS NZ ON Z.Id = NZ.ZoneId
	                            WHERE NZ.NewsId = @NewsId";
                                
                var cmd = new SqlCommand(query, con);                
                cmd.Parameters.AddWithValue("@NewsId", id);

                con.Open();
                var reader = cmd.ExecuteReader();
                var data = EntityMapper.FillCollection<ZoneEntity>(reader);
                con.Close();

                return data;
            }
            catch (Exception ex)
            {
                con.Close();
                con.Dispose();
                Logger.Log(TypeLog.Error, "GetZoneByNewsId=> " + ex.Message);
                return new List<ZoneEntity>();
            }
            finally
            {
                con.Close();
                con.Dispose();
            }
        }

        private static void SyncNewsEsAndReDist(string connectStr, NewsEntity s)
        {
            var nameSpace = ConfigurationManager.AppSettings["NameSpace"];
            var connectEs= ConfigurationManager.AppSettings["ConnectString.ES"];
            var connectRedis = ConfigurationManager.AppSettings["ConnectString.REDIS"];

            var listZone = GetZoneByNewsId(connectStr, s.Id);
            var zoneIds = new string[] { };
            if (listZone!=null && listZone.Count > 0)
            {
                zoneIds = listZone.Select(z => z.Id.ToString()).ToArray();
                s.ZoneName = listZone.Where(w=>w.IsPrimary==true).Select(z => z.Name).FirstOrDefault();
            }            
                
            var obj = new NewsSearchEntity
            {
                Id = s.Id,
                EncryptId = s.Id.ToString(),
                Title = s.Title,
                ZoneIds = zoneIds,
                Type = s.Type,
                CreatedBy = s.CreatedBy,
                LastModifiedBy = s.LastModifiedBy,
                EditedBy = s.EditedBy,
                PublishedBy = s.PublishedBy,
                LastReceiver = s.LastReceiver,
                ApprovedBy = s.ApprovedBy,
                ReturnedBy = s.ReturnedBy,
                CreatedDate = s.CreatedDate,
                DistributionDate = s.DistributionDate,
                LastModifiedDate = s.LastModifiedDate,
                ApprovedDate = s.ApprovedDate,
                ViewCount = s.ViewCount,
                Status = s.Status,
                IsOnHome = s.IsOnHome,
                Avatar = s.Avatar,
                ViewRoyalties = 0,
                ZoneName = s.ZoneName,
                Note = s.Note,
                Author = s.Author,
                ErrorCheckedBy = s.ErrorCheckedBy,
                ErrorCheckedDate = s.ErrorCheckedDate,
                SensitiveCheckedBy = s.SensitiveCheckedBy,
                SensitiveCheckedDate = s.SensitiveCheckedDate,
                WordCount = s.WordCount,
                Sapo = s.Sapo,
                Url = s.Url,
                DisplayPosition = s.DisplayPosition,
                Priority=s.Priority
            };

            if (s.Status == 6)
            {
                obj.ApprovedBy = s.PublishedBy;
            }

            EsNewsBo.Create(nameSpace, connectEs, obj);

            RedisNewsBo.AddNews(nameSpace, connectRedis, s);
        }

        #endregion

        #region NewsInZone
        private static void SyncNewsInZone(string connectStr1, string connectStr2, long newsId)
        {
            var con = GetConnection(connectStr1);
            try
            {                
                string query = @"SELECT n.* FROM NewsInZone n                                
                                WHERE n.NewsId=@NewsId";

                using (var cmd = new SqlCommand(query, con))
                {

                    cmd.Parameters.AddWithValue("@NewsId", newsId);

                    con.Open();
                    var reader = cmd.ExecuteReader();

                    var data = EntityMapper.FillCollection<NewsInZoneEntity>(reader);

                    con.Close();

                    InitNewsInZoneToDb(connectStr2, data);
                }              
            }
            catch (Exception ex)
            {                
                Logger.Log(TypeLog.Error, "SyncNewsInZone => " + ex.Message);
            }
            finally
            {
                con.Close();
                con.Dispose();
            }
        }
        private static void InitNewsInZoneToDb(string connectStr, List<NewsInZoneEntity> data)
        {            
            var con = GetConnection(connectStr);
            con.Open();

            try
            {                
                foreach (var item in data)
                {
                    string query = @"DELETE [NewsInZone] WHERE NewsId=@NewsId and ZoneId=@ZoneId
                                    INSERT INTO [NewsInZone]([NewsId]
                                                            ,[ZoneId]
                                                            ,[IsPrimary]
                                                            ,[DistributionID]
                                                            ,[RootZoneId]
                                                ) VALUES(@NewsId
                                                        ,@ZoneId
                                                        ,@IsPrimary
                                                        ,@DistributionID
                                                        ,@RootZoneId)";

                    using (var cmd = new SqlCommand(query, con))
                    {
                        cmd.CommandType = CommandType.Text;

                        cmd.Parameters.AddWithValue("@NewsId", item.NewsId);
                        cmd.Parameters.AddWithValue("@ZoneId", item.ZoneId);
                        cmd.Parameters.AddWithValue("@IsPrimary", item.IsPrimary);

                        if (item.DistributionID < 0)
                            cmd.Parameters.AddWithValue("@DistributionID", DBNull.Value);
                        else
                            cmd.Parameters.AddWithValue("@DistributionID", item.DistributionID);

                        cmd.Parameters.AddWithValue("@RootZoneId", item.RootZoneId);

                        var reader = cmd.ExecuteNonQuery();
                    }                
                }

                con.Close();
            }
            catch (Exception ex)
            {                
                Logger.Log(TypeLog.Error, "InitNewsInZoneToDb => " + ex.Message);
            }
            finally
            {
                con.Close();
                con.Dispose();
            }
        }
        #endregion

        #region NewsExtension
        private static void SyncNewsExtension(string connectStr1, string connectStr2, long newsId)
        {
            var con = GetConnection(connectStr1);
            try
            {
                string query = @"SELECT n.* FROM NewsExtension n                                
                                WHERE n.NewsId=@NewsId";

                using (var cmd = new SqlCommand(query, con))
                {

                    cmd.Parameters.AddWithValue("@NewsId", newsId);

                    con.Open();
                    var reader = cmd.ExecuteReader();

                    var data = EntityMapper.FillCollection<NewsExtensionEntity>(reader);

                    con.Close();

                    InitNewsExtensionToDb(connectStr2, data);
                }
            }
            catch (Exception ex)
            {                
                Logger.Log(TypeLog.Error, "SyncNewsExtension => " + ex.Message);
            }
            finally
            {
                con.Close();
            }
        }
        private static void InitNewsExtensionToDb(string connectStr, List<NewsExtensionEntity> data)
        {
            var con = GetConnection(connectStr);
            con.Open();

            try
            {
                foreach (var item in data)
                {
                    string query = @"DELETE [NewsExtension] WHERE NewsId=@NewsId and [Type]=@Type
                                    INSERT INTO [NewsExtension]([NewsId]
                                                                ,[Type]
                                                                ,[Value]
                                                ) VALUES(@NewsId
                                                        ,@Type
                                                        ,@Value)";

                    using (var cmd = new SqlCommand(query, con))
                    {
                        cmd.CommandType = CommandType.Text;

                        cmd.Parameters.AddWithValue("@NewsId", item.NewsId);
                        cmd.Parameters.AddWithValue("@Type", item.Type);
                        cmd.Parameters.AddWithValue("@Value", item.Value ?? (object)DBNull.Value);

                        var reader = cmd.ExecuteNonQuery();
                    }
                }

                con.Close();
            }
            catch (Exception ex)
            {                
                Logger.Log(TypeLog.Error, "InitNewsExtensionToDb => " + ex.Message);
            }
            finally
            {
                con.Close();
                con.Dispose();
            }
        }
        #endregion

        #region TagNews
        private static void SyncTagNews(string connectStr1, string connectStr2, long newsId)
        {
            var con = GetConnection(connectStr1);
            try
            {
                string query = @"SELECT n.* FROM TagNews n                                
                                WHERE n.NewsId=@NewsId";

                using (var cmd = new SqlCommand(query, con))
                {

                    cmd.Parameters.AddWithValue("@NewsId", newsId);

                    con.Open();
                    var reader = cmd.ExecuteReader();

                    var data = EntityMapper.FillCollection<TagNewsEntity>(reader);

                    con.Close();

                    InitTagNewsToDb(connectStr2, data);
                }
            }
            catch (Exception ex)
            {                
                Logger.Log(TypeLog.Error, "SyncTagNews => " + ex.Message);
            }
            finally
            {
                con.Close();
                con.Dispose();
            }
        }
        private static void InitTagNewsToDb(string connectStr, List<TagNewsEntity> data)
        {
            var con = GetConnection(connectStr);
            con.Open();

            try
            {
                foreach (var item in data)
                {
                    string query = @"DELETE [TagNews] WHERE NewsId=@NewsId and TagID=@TagID
                                    INSERT INTO [TagNews]([TagID]
                                                              ,[NewsID]
                                                              ,[TagMode]
                                                              ,[TagProperty]
                                                              ,[Priority]
                                                ) VALUES(@TagID
                                                        ,@NewsId
                                                        ,@TagMode
                                                        ,@TagProperty
                                                        ,@Priority)";

                    using (var cmd = new SqlCommand(query, con))
                    {
                        cmd.CommandType = CommandType.Text;

                        cmd.Parameters.AddWithValue("@TagID", item.TagID);
                        cmd.Parameters.AddWithValue("@NewsId", item.NewsId);

                        if (item.TagMode <= 0)
                            cmd.Parameters.AddWithValue("@TagMode", 0);
                        else
                            cmd.Parameters.AddWithValue("@TagMode", item.TagMode);

                        cmd.Parameters.AddWithValue("@TagProperty", item.TagProperty ?? (object)DBNull.Value);
                        cmd.Parameters.AddWithValue("@Priority", item.Priority);

                        var reader = cmd.ExecuteNonQuery();
                    }
                }

                con.Close();
            }
            catch (Exception ex)
            {                
                Logger.Log(TypeLog.Error, "InitTagNewsToDb => " + ex.Message);
            }
            finally
            {
                con.Close();
                con.Dispose();
            }
        }
        #endregion

        #region ThreadNews
        private static void SyncThreadNews(string connectStr1, string connectStr2, long newsId)
        {
            var con = GetConnection(connectStr1);
            try
            {
                string query = @"SELECT n.* FROM ThreadNews n                                
                                WHERE n.NewsId=@NewsId";

                using (var cmd = new SqlCommand(query, con))
                {

                    cmd.Parameters.AddWithValue("@NewsId", newsId);

                    con.Open();
                    var reader = cmd.ExecuteReader();

                    var data = EntityMapper.FillCollection<ThreadNewsEntity>(reader);

                    con.Close();

                    InitThreadNewsToDb(connectStr2, data);
                }
            }
            catch (Exception ex)
            {                
                Logger.Log(TypeLog.Error, "SyncThreadNews => " + ex.Message);
            }
            finally
            {
                con.Close();
                con.Dispose();
            }
        }
        private static void InitThreadNewsToDb(string connectStr, List<ThreadNewsEntity> data)
        {
            var con = GetConnection(connectStr);
            con.Open();

            try
            {
                foreach (var item in data)
                {
                    string query = @"DELETE [ThreadNews] WHERE NewsId=@NewsId and ThreadId=@ThreadId
                                    INSERT INTO [ThreadNews]([ThreadId]
                                                              ,[NewsId]
                                                              ,[Ordinary]                                                             
                                                              ,[LastModifiedDate]
                                                ) VALUES(@ThreadId
                                                        ,@NewsId                                                        
                                                        ,@Ordinary
                                                        ,@LastModifiedDate)";

                    using (var cmd = new SqlCommand(query, con))
                    {
                        cmd.CommandType = CommandType.Text;

                        cmd.Parameters.AddWithValue("@ThreadId", item.ThreadId);
                        cmd.Parameters.AddWithValue("@NewsId", item.NewsId);
                        cmd.Parameters.AddWithValue("@Ordinary", item.Ordinary);

                        if (item.LastModifiedDate <= new DateTime(1980, 1, 1))
                            cmd.Parameters.AddWithValue("@LastModifiedDate", DBNull.Value);
                        else
                            cmd.Parameters.AddWithValue("@LastModifiedDate", item.LastModifiedDate);

                        var reader = cmd.ExecuteNonQuery();
                    }
                }

                con.Close();
            }
            catch (Exception ex)
            {                
                Logger.Log(TypeLog.Error, "InitThreadNewsToDb => " + ex.Message);
            }
            finally
            {
                con.Close();
                con.Dispose();
            }
        }
        #endregion

        #region NewsRelation
        private static void SyncNewsRelation(string connectStr1, string connectStr2, long newsId)
        {
            var con = GetConnection(connectStr1);
            try
            {
                string query = @"SELECT n.* FROM NewsRelation n                                
                                WHERE n.NewsId=@NewsId";

                using (var cmd = new SqlCommand(query, con))
                {

                    cmd.Parameters.AddWithValue("@NewsId", newsId);

                    con.Open();
                    var reader = cmd.ExecuteReader();

                    var data = EntityMapper.FillCollection<NewsRelationEntity>(reader);

                    con.Close();

                    InitNewsRelationToDb(connectStr2, data);
                }
            }
            catch (Exception ex)
            {
                con.Close();
                Logger.Log(TypeLog.Error, "SyncNewsRelation => " + ex.Message);
            }
            finally
            {
                con.Close();
                con.Dispose();
            }
        }
        private static void InitNewsRelationToDb(string connectStr, List<NewsRelationEntity> data)
        {
            var con = GetConnection(connectStr);
            con.Open();

            try
            {
                foreach (var item in data)
                {
                    string query = @"DELETE [NewsRelation] WHERE NewsId=@NewsId and NewsRelationId=@NewsRelationId and ZoneId=@ZoneId
                                    INSERT INTO [NewsRelation]([NewsId]
                                                              ,[NewsRelationId]
                                                              ,[ZoneId]
                                                              ,[IsChange]
                                                              ,[Priority]                                                             
                                                              ,[Type]
                                                ) VALUES(@NewsId
                                                        ,@NewsRelationId                                                        
                                                        ,@ZoneId
                                                        ,@IsChange
                                                        ,@Priority
                                                        ,@Type)";

                    using (var cmd = new SqlCommand(query, con))
                    {
                        cmd.CommandType = CommandType.Text;

                        cmd.Parameters.AddWithValue("@NewsID", item.NewsId);
                        cmd.Parameters.AddWithValue("@NewsRelationId", item.NewsRelationId);
                        cmd.Parameters.AddWithValue("@ZoneId", item.ZoneId);
                        cmd.Parameters.AddWithValue("@IsChange", item.IsChange);
                        cmd.Parameters.AddWithValue("@Priority", item.Priority);
                        cmd.Parameters.AddWithValue("@Type", item.Type);

                        var reader = cmd.ExecuteNonQuery();
                    }
                }

                con.Close();
            }
            catch (Exception ex)
            {                
                Logger.Log(TypeLog.Error, "InitNewsRelationToDb => " + ex.Message);
            }
            finally
            {
                con.Close();
                con.Dispose();
            }
        }
        #endregion

        #region NewsPublish
        private static void SyncNewsPublish(string connectStr1, string connectStr2, NewsEntity item)
        {
            if (item.Status == 8)
            {
                var con = GetConnection(connectStr1);
                try
                {
                    string query = @"SELECT n.* FROM NewsPublish n                                
                                WHERE n.NewsId=@NewsId";

                    using (var cmd = new SqlCommand(query, con))
                    {

                        cmd.Parameters.AddWithValue("@NewsId", item.Id);

                        con.Open();
                        var reader = cmd.ExecuteReader();

                        var data = EntityMapper.FillCollection<NewsPublishEntity>(reader);

                        con.Close();

                        InitNewsPublishToDb(connectStr2, data);
                    }
                }
                catch (Exception ex)
                {                    
                    Logger.Log(TypeLog.Error, "SyncNewsPublish => " + ex.Message);
                }
                finally
                {                    
                    con.Close();                    
                    con.Dispose();
                }
            }
        }
        private static void InitNewsPublishToDb(string connectStr, List<NewsPublishEntity> data)
        {
            var con = GetConnection(connectStr);
            con.Open();

            try
            {
                foreach (var item in data)
                {
                    string query = @"DELETE [NewsPublish] WHERE NewsId=@NewsId and ZoneId=@ZoneId 
                                    INSERT INTO [NewsPublish]([NewsId]
                                                              ,[ZoneId]
                                                              ,[Title]
                                                              ,[SubTitle]
                                                              ,[Sapo]
                                                              ,[Avatar]
                                                              ,[AvatarDesc]
                                                              ,[Avatar1]
                                                              ,[Avatar2]
                                                              ,[Avatar3]
                                                              ,[Avatar4]
                                                              ,[Avatar5]
                                                              ,[Author]
                                                              ,[NewsRelation]
                                                              ,[Source]
                                                              ,[IsFocus]
                                                              ,[Type]
                                                              ,[ThreadId]
                                                              ,[DistributionDate]
                                                              ,[Url]
                                                              ,[DisplayStyle]
                                                              ,[DisplayPosition]
                                                              ,[DisplayInSlide]
                                                              ,[AvatarCustom]
                                                              ,[OriginalId]
                                                              ,[PublishedDate]
                                                              ,[IsOnHome]
                                                              ,[TagItem]
                                                              ,[IsPrimary]
                                                              ,[InitSapo]
                                                              ,[NewsType]
                                                              ,[InterviewId]
                                                              ,[IsBreakingNews]
                                                              ,[OriginalUrl]
                                                              ,[IsPr]
                                                              ,[AdStore]
                                                              ,[AdStoreUrl]
                                                              ,[LastModifiedDate]
                                                              ,[RollingNewsId]
                                                              ,[IsOnMobile]
                                                              ,[TagSubTitleId]
                                                              ,[Position]
                                                              ,[PrPosition]
                                                              ,[Priority]
                                                              ,[LastModifiedDateStamp]
                                                              ,[PublishedDateStamp]
                                                              ,[LocationType]
                                                              ,[ExpiredDate]
                                                              ,[DistributionID]
                                                              ,[PrimaryZoneId]
                                                              ,[TitleDetail]
                                                              ,[ParentNewsId]
                                                ) VALUES(@NewsId
                                                        ,@ZoneId
                                                        ,@Title
                                                        ,@SubTitle
                                                        ,@Sapo
                                                        ,@Avatar
                                                        ,@AvatarDesc
                                                        ,@Avatar1
                                                        ,@Avatar2
                                                        ,@Avatar3
                                                        ,@Avatar4
                                                        ,@Avatar5
                                                        ,@Author
                                                        ,@NewsRelation
                                                        ,@Source
                                                        ,@IsFocus
                                                        ,@Type
                                                        ,@ThreadId
                                                        ,@DistributionDate
                                                        ,@Url
                                                        ,@DisplayStyle
                                                        ,@DisplayPosition
                                                        ,@DisplayInSlide
                                                        ,@AvatarCustom
                                                        ,@OriginalId
                                                        ,@PublishedDate
                                                        ,@IsOnHome
                                                        ,@TagItem
                                                        ,@IsPrimary
                                                        ,@InitSapo
                                                        ,@NewsType
                                                        ,@InterviewId
                                                        ,@IsBreakingNews
                                                        ,@OriginalUrl
                                                        ,@IsPr
                                                        ,@AdStore
                                                        ,@AdStoreUrl
                                                        ,@LastModifiedDate
                                                        ,@RollingNewsId
                                                        ,@IsOnMobile
                                                        ,@TagSubTitleId
                                                        ,@Position
                                                        ,@PrPosition
                                                        ,@Priority
                                                        ,@LastModifiedDateStamp
                                                        ,@PublishedDateStamp
                                                        ,@LocationType
                                                        ,@ExpiredDate
                                                        ,@DistributionID
                                                        ,@PrimaryZoneId
                                                        ,@TitleDetail
                                                        ,@ParentNewsId)";

                    using (var cmd = new SqlCommand(query, con))
                    {
                        cmd.CommandType = CommandType.Text;

                        cmd.Parameters.AddWithValue("@NewsId", item.NewsId);
                        cmd.Parameters.AddWithValue("@ZoneId", item.ZoneId);
                        cmd.Parameters.AddWithValue("@Title", item.Title ?? (object)DBNull.Value);
                        cmd.Parameters.AddWithValue("@SubTitle", item.SubTitle ?? (object)DBNull.Value);
                        cmd.Parameters.AddWithValue("@Sapo", item.Sapo ?? (object)DBNull.Value);
                        cmd.Parameters.AddWithValue("@Avatar", item.Avatar ?? (object)DBNull.Value);
                        cmd.Parameters.AddWithValue("@AvatarDesc", item.AvatarDesc ?? (object)DBNull.Value);
                        cmd.Parameters.AddWithValue("@Avatar1", item.Avatar1 ?? (object)DBNull.Value);
                        cmd.Parameters.AddWithValue("@Avatar2", item.Avatar2 ?? (object)DBNull.Value);
                        cmd.Parameters.AddWithValue("@Avatar3", item.Avatar3 ?? (object)DBNull.Value);
                        cmd.Parameters.AddWithValue("@Avatar4", item.Avatar4 ?? (object)DBNull.Value);
                        cmd.Parameters.AddWithValue("@Avatar5", item.Avatar5 ?? (object)DBNull.Value);
                        cmd.Parameters.AddWithValue("@Author", item.Author ?? (object)DBNull.Value);
                        cmd.Parameters.AddWithValue("@NewsRelation", item.NewsRelation ?? (object)DBNull.Value);
                        cmd.Parameters.AddWithValue("@Source", item.Source ?? (object)DBNull.Value);
                        cmd.Parameters.AddWithValue("@IsFocus", item.IsFocus);

                        if (item.Type <= 0)
                            cmd.Parameters.AddWithValue("@Type", 0);
                        else
                            cmd.Parameters.AddWithValue("@Type", item.Type);

                        cmd.Parameters.AddWithValue("@ThreadId", item.ThreadId);

                        if (item.DistributionDate <= new DateTime(1980, 1, 1))
                            cmd.Parameters.AddWithValue("@DistributionDate", DBNull.Value);
                        else
                            cmd.Parameters.AddWithValue("@DistributionDate", item.DistributionDate);

                        cmd.Parameters.AddWithValue("@Url", item.Url ?? (object)DBNull.Value);
                        cmd.Parameters.AddWithValue("@DisplayStyle", item.DisplayStyle);
                        cmd.Parameters.AddWithValue("@DisplayPosition", item.DisplayPosition);
                        cmd.Parameters.AddWithValue("@DisplayInSlide", item.DisplayInSlide);
                        cmd.Parameters.AddWithValue("@AvatarCustom", item.AvatarCustom ?? (object)DBNull.Value);
                        cmd.Parameters.AddWithValue("@OriginalId", item.OriginalId);
                        cmd.Parameters.AddWithValue("@PublishedDate", item.PublishedDate);
                        cmd.Parameters.AddWithValue("@IsOnHome", item.IsOnHome);
                        cmd.Parameters.AddWithValue("@TagItem", item.TagItem ?? (object)DBNull.Value);
                        cmd.Parameters.AddWithValue("@IsPrimary", item.IsPrimary);
                        cmd.Parameters.AddWithValue("@InitSapo", item.InitSapo ?? (object)DBNull.Value);

                        if (item.NewsType <= 0)
                            cmd.Parameters.AddWithValue("@NewsType", 0);
                        else
                            cmd.Parameters.AddWithValue("@NewsType", item.NewsType);

                        cmd.Parameters.AddWithValue("@InterviewId", item.InterviewId);
                        cmd.Parameters.AddWithValue("@IsBreakingNews", item.IsBreakingNews);
                        cmd.Parameters.AddWithValue("@OriginalUrl", item.OriginalUrl ?? (object)DBNull.Value);
                        cmd.Parameters.AddWithValue("@IsPr", item.IsPr);
                        cmd.Parameters.AddWithValue("@AdStore", item.AdStore);
                        cmd.Parameters.AddWithValue("@AdStoreUrl", item.AdStoreUrl ?? (object)DBNull.Value);

                        if (item.LastModifiedDate <= new DateTime(1980, 1, 1))
                            cmd.Parameters.AddWithValue("@LastModifiedDate", DBNull.Value);
                        else
                            cmd.Parameters.AddWithValue("@LastModifiedDate", item.LastModifiedDate);

                        cmd.Parameters.AddWithValue("@RollingNewsId", item.RollingNewsId);
                        cmd.Parameters.AddWithValue("@IsOnMobile", item.IsOnMobile);
                        cmd.Parameters.AddWithValue("@TagSubTitleId", item.TagSubTitleId);
                        cmd.Parameters.AddWithValue("@Position", item.Position);
                        cmd.Parameters.AddWithValue("@PrPosition", item.PrPosition);
                        cmd.Parameters.AddWithValue("@Priority", item.Priority);
                        cmd.Parameters.AddWithValue("@LastModifiedDateStamp", item.LastModifiedDateStamp);
                        cmd.Parameters.AddWithValue("@PublishedDateStamp", item.PublishedDateStamp);
                        cmd.Parameters.AddWithValue("@LocationType", item.LocationType);

                        if (item.ExpiredDate <= new DateTime(1980, 1, 1))
                            cmd.Parameters.AddWithValue("@ExpiredDate", DBNull.Value);
                        else
                            cmd.Parameters.AddWithValue("@ExpiredDate", item.ExpiredDate);

                        cmd.Parameters.AddWithValue("@DistributionID", item.DistributionID);
                        cmd.Parameters.AddWithValue("@PrimaryZoneId", item.PrimaryZoneId);
                        cmd.Parameters.AddWithValue("@TitleDetail", item.TitleDetail ?? (object)DBNull.Value);
                        cmd.Parameters.AddWithValue("@ParentNewsId", item.ParentNewsId);

                        var reader = cmd.ExecuteNonQuery();
                    }
                }

                con.Close();
            }
            catch (Exception ex)
            {                
                Logger.Log(TypeLog.Error, "InitNewsPublishToDb => " + ex.Message);
            }
            finally
            {
                con.Close();
                con.Dispose();
            }
        }
        #endregion

        #region NewsContent
        private static void SyncNewsContent(string connectStr1, string connectStr2, NewsEntity item)
        {
            if (item.Status == 8)
            {
                var con = GetConnection(connectStr1);
                try
                {
                    string query = @"SELECT n.* FROM NewsContent n                                
                                WHERE n.NewsId=@NewsId";

                    using (var cmd = new SqlCommand(query, con))
                    {

                        cmd.Parameters.AddWithValue("@NewsId", item.Id);

                        con.Open();
                        var reader = cmd.ExecuteReader();

                        var data = EntityMapper.FillCollection<NewsContentEntity>(reader);

                        con.Close();

                        InitNewsContentToDb(connectStr2, data);
                    }
                }
                catch (Exception ex)
                {
                    Logger.Log(TypeLog.Error, "SyncNewsContent => " + ex.Message);
                }
                finally
                {
                    con.Close();
                    con.Dispose();
                }
            }
        }
        private static void InitNewsContentToDb(string connectStr, List<NewsContentEntity> data)
        {
            var con = GetConnection(connectStr);
            con.Open();

            try
            {
                foreach (var item in data)
                {
                    string query = @"DELETE [NewsContent] WHERE NewsId=@NewsId;
                                    INSERT INTO [NewsContent]([NewsId]
                                                              ,[Title]
                                                              ,[Sapo]
                                                              ,[Avatar]
                                                              ,[Avatar2]
                                                              ,[Avatar3]
                                                              ,[Avatar4]
                                                              ,[Avatar5]
                                                              ,[Body]
                                                              ,[PublishedDate]
                                                              ,[Source]
                                                              ,[NewsRelation]
                                                              ,[Tags]
                                                              ,[Author]
                                                              ,[TagPrimary]
                                                              ,[Url]
                                                              ,[ZoneId]
                                                              ,[OriginalId]
                                                              ,[TagItem]
                                                              ,[SubTitle]
                                                              ,[InitSapo]
                                                              ,[InterviewId]
                                                              ,[OriginalUrl]
                                                              ,[Type]
                                                              ,[AvatarDesc]
                                                              ,[NewsType]
                                                              ,[RollingNewsId]
                                                              ,[AdStore]
                                                              ,[AdStoreUrl]
                                                              ,[TagSubTitleId]
                                                              ,[ThreadId]
                                                              ,[Position]
                                                              ,[PrPosition]
                                                              ,[IsOnMobile]
                                                              ,[UseTemplate]
                                                              ,[LocationType]
                                                              ,[ExpiredDate]
                                                              ,[SourceUrl]
                                                              ,[ParentNewsId]
                                                ) VALUES(@NewsId
                                                          ,@Title
                                                          ,@Sapo
                                                          ,@Avatar
                                                          ,@Avatar2
                                                          ,@Avatar3
                                                          ,@Avatar4
                                                          ,@Avatar5
                                                          ,@Body
                                                          ,@PublishedDate
                                                          ,@Source
                                                          ,@NewsRelation
                                                          ,@Tags
                                                          ,@Author
                                                          ,@TagPrimary
                                                          ,@Url
                                                          ,@ZoneId
                                                          ,@OriginalId
                                                          ,@TagItem
                                                          ,@SubTitle
                                                          ,@InitSapo
                                                          ,@InterviewId
                                                          ,@OriginalUrl
                                                          ,@Type
                                                          ,@AvatarDesc
                                                          ,@NewsType
                                                          ,@RollingNewsId
                                                          ,@AdStore
                                                          ,@AdStoreUrl
                                                          ,@TagSubTitleId
                                                          ,@ThreadId
                                                          ,@Position
                                                          ,@PrPosition
                                                          ,@IsOnMobile
                                                          ,@UseTemplate
                                                          ,@LocationType
                                                          ,@ExpiredDate
                                                          ,@SourceUrl
                                                          ,@ParentNewsId);
                                                ";

                    using (var cmd = new SqlCommand(query, con))
                    {
                        cmd.CommandType = CommandType.Text;

                        cmd.Parameters.AddWithValue("@NewsId", item.NewsId);
                        cmd.Parameters.AddWithValue("@Title", item.Title ?? (object)DBNull.Value);
                        cmd.Parameters.AddWithValue("@Sapo", item.Sapo ?? (object)DBNull.Value);
                        cmd.Parameters.AddWithValue("@Avatar", item.Avatar ?? (object)DBNull.Value);
                        cmd.Parameters.AddWithValue("@Avatar2", item.Avatar2 ?? (object)DBNull.Value);
                        cmd.Parameters.AddWithValue("@Avatar3", item.Avatar3 ?? (object)DBNull.Value);
                        cmd.Parameters.AddWithValue("@Avatar4", item.Avatar4 ?? (object)DBNull.Value);
                        cmd.Parameters.AddWithValue("@Avatar5", item.Avatar5 ?? (object)DBNull.Value);
                        cmd.Parameters.AddWithValue("@Body", item.Body ?? (object)DBNull.Value);

                        if (item.PublishedDate <= new DateTime(1980, 1, 1))
                            cmd.Parameters.AddWithValue("@PublishedDate", DBNull.Value);
                        else
                            cmd.Parameters.AddWithValue("@PublishedDate", item.PublishedDate);

                        cmd.Parameters.AddWithValue("@Source", item.Source ?? (object)DBNull.Value);
                        cmd.Parameters.AddWithValue("@NewsRelation", item.NewsRelation ?? (object)DBNull.Value);
                        cmd.Parameters.AddWithValue("@Tags", item.Tags ?? (object)DBNull.Value);
                        cmd.Parameters.AddWithValue("@Author", item.Author ?? (object)DBNull.Value);
                        cmd.Parameters.AddWithValue("@TagPrimary", item.TagPrimary ?? (object)DBNull.Value);
                        cmd.Parameters.AddWithValue("@Url", item.Url ?? (object)DBNull.Value);
                        cmd.Parameters.AddWithValue("@ZoneId", item.ZoneId);
                        cmd.Parameters.AddWithValue("@OriginalId", item.OriginalId);
                        cmd.Parameters.AddWithValue("@TagItem", item.TagItem ?? (object)DBNull.Value);
                        cmd.Parameters.AddWithValue("@SubTitle", item.SubTitle ?? (object)DBNull.Value);
                        cmd.Parameters.AddWithValue("@InitSapo", item.InitSapo ?? (object)DBNull.Value);
                        cmd.Parameters.AddWithValue("@InterviewId", item.InterviewId);
                        cmd.Parameters.AddWithValue("@OriginalUrl", item.OriginalUrl ?? (object)DBNull.Value);

                        if (item.Type <= 0)
                            cmd.Parameters.AddWithValue("@Type", 0);
                        else
                            cmd.Parameters.AddWithValue("@Type", item.Type);

                        cmd.Parameters.AddWithValue("@AvatarDesc", item.AvatarDesc ?? (object)DBNull.Value);

                        if (item.NewsType <= 0)
                            cmd.Parameters.AddWithValue("@NewsType", 0);
                        else
                            cmd.Parameters.AddWithValue("@NewsType", item.NewsType);

                        cmd.Parameters.AddWithValue("@RollingNewsId", item.RollingNewsId);
                        cmd.Parameters.AddWithValue("@AdStore", item.AdStore);
                        cmd.Parameters.AddWithValue("@AdStoreUrl", item.AdStoreUrl ?? (object)DBNull.Value);
                        cmd.Parameters.AddWithValue("@TagSubTitleId", item.TagSubTitleId);
                        cmd.Parameters.AddWithValue("@ThreadId", item.ThreadId);
                        cmd.Parameters.AddWithValue("@Position", item.Position);
                        cmd.Parameters.AddWithValue("@PrPosition", item.PrPosition);
                        cmd.Parameters.AddWithValue("@IsOnMobile", item.IsOnMobile);
                        cmd.Parameters.AddWithValue("@UseTemplate", item.UseTemplate);
                        cmd.Parameters.AddWithValue("@LocationType", item.LocationType);

                        if (item.ExpiredDate <= new DateTime(1980, 1, 1))
                            cmd.Parameters.AddWithValue("@ExpiredDate", DBNull.Value);
                        else
                            cmd.Parameters.AddWithValue("@ExpiredDate", item.ExpiredDate);

                        cmd.Parameters.AddWithValue("@SourceUrl", item.SourceUrl ?? (object)DBNull.Value);
                        cmd.Parameters.AddWithValue("@ParentNewsId", item.ParentNewsId);

                        var reader = cmd.ExecuteNonQuery();
                    }
                }

                con.Close();
            }
            catch (Exception ex)
            {
                Logger.Log(TypeLog.Error, "InitNewsContentToDb => " + ex.Message);
            }
            finally
            {
                con.Close();
                con.Dispose();
            }
        }
        #endregion

        #endregion

        public static void PostDataApi(NewsEntity item, ApiClient client)
        {
            var processCount = Task.Run(() =>
            {
                try
                {
                    var sb = new StringBuilder();
                    sb.AppendFormat("Id={0}", item.Id);
                    sb.AppendFormat("&EncryptId={0}", item.EncryptId);
                    sb.AppendFormat("&StrId={0}", item.StrId);
                    sb.AppendFormat("&Title={0}", item.Title);
                    sb.AppendFormat("&SubTitle={0}", item.SubTitle);
                    sb.AppendFormat("&Sapo={0}", item.Sapo);
                    sb.AppendFormat("&Body={0}", item.Body);
                    sb.AppendFormat("&Avatar={0}", item.Avatar);
                    sb.AppendFormat("&AvatarDesc={0}", item.AvatarDesc);
                    sb.AppendFormat("&Avatar2={0}", item.Avatar2);
                    sb.AppendFormat("&Avatar3={0}", item.Avatar3);
                    sb.AppendFormat("&Avatar4={0}", item.Avatar4);
                    sb.AppendFormat("&Avatar5={0}", item.Avatar5);
                    sb.AppendFormat("&Author={0}", item.Author);
                    sb.AppendFormat("&NewsRelation={0}", item.NewsRelation);
                    sb.AppendFormat("&Status={0}", item.Status);
                    sb.AppendFormat("&Source={0}", item.Source);
                    sb.AppendFormat("&IsFocus={0}", item.IsFocus);
                    sb.AppendFormat("&Type={0}", item.Type);
                    sb.AppendFormat("&ThreadId={0}", item.ThreadId);
                    sb.AppendFormat("&CreatedDate={0}", item.CreatedDate);//23/05/2018 15:36
                    sb.AppendFormat("&LastModifiedDate={0}", item.LastModifiedDate);//23/05/2018 15:36
                    sb.AppendFormat("&DistributionDate={0}", item.DistributionDate);//23/05/2018 15:36
                    sb.AppendFormat("&CreatedBy={0}", item.CreatedBy);
                    sb.AppendFormat("&LastModifiedBy={0}", item.LastModifiedBy);
                    sb.AppendFormat("&PublishedBy={0}", item.PublishedBy);
                    sb.AppendFormat("&EditedBy={0}", item.EditedBy);
                    sb.AppendFormat("&LastReceiver={0}", item.LastReceiver);
                    sb.AppendFormat("&WordCount={0}", item.WordCount);
                    sb.AppendFormat("&ViewCount={0}", item.ViewCount);
                    sb.AppendFormat("&Priority={0}", item.Priority);

                    sb.AppendFormat("&ListZoneId={0}", item.ListZoneId);//107;64;155

                    sb.AppendFormat("&Tag={0}", item.Tag);
                    sb.AppendFormat("&Note={0}", item.Note);
                    sb.AppendFormat("&TagPrimary={0}", item.TagPrimary);
                    sb.AppendFormat("&Price={0}", item.Price);
                    sb.AppendFormat("&DisplayStyle={0}", item.DisplayStyle);
                    sb.AppendFormat("&DisplayPosition={0}", item.DisplayPosition);
                    sb.AppendFormat("&DisplayInSlide={0}", item.DisplayInSlide);
                    sb.AppendFormat("&AvatarCustom={0}", item.AvatarCustom);
                    sb.AppendFormat("&OriginalId={0}", item.OriginalId);
                    sb.AppendFormat("&NewsType={0}", item.NewsType);
                    sb.AppendFormat("&IsOnHome={0}", item.IsOnHome);
                    sb.AppendFormat("&Url={0}", item.Url);
                    sb.AppendFormat("&NoteRoyalties={0}", item.NoteRoyalties);                                        
                    sb.AppendFormat("&TagItem={0}", item.TagItem);
                    sb.AppendFormat("&NewsCategory={0}", item.NewsCategory);
                    sb.AppendFormat("&InitSapo={0}", item.InitSapo);
                    sb.AppendFormat("&TemplateName={0}", item.TemplateName);
                    sb.AppendFormat("&TemplateConfig={0}", item.TemplateConfig);//[]
                    sb.AppendFormat("&InterviewId={0}", item.InterviewId);
                    sb.AppendFormat("&IsBreakingNews={0}", item.IsBreakingNews);
                    sb.AppendFormat("&OriginalUrl={0}", item.OriginalUrl);
                    sb.AppendFormat("&IsPr={0}", item.IsPr);                    
                    sb.AppendFormat("&AdStore={0}", item.AdStore);
                    sb.AppendFormat("&AdStoreUrl={0}", item.AdStoreUrl);
                    sb.AppendFormat("&PrBookingNumber={0}", item.PrBookingNumber);
                    sb.AppendFormat("&PegaBreakingNews={0}", item.PegaBreakingNews);
                    sb.AppendFormat("&RollingNewsId={0}", item.RollingNewsId);
                    sb.AppendFormat("&IsOnMobile={0}", item.IsOnMobile);
                    sb.AppendFormat("&TagSubTitleId={0}", item.TagSubTitleId);
                    sb.AppendFormat("&PrPosition={0}", item.PrPosition);
                    sb.AppendFormat("&LocationType={0}", item.LocationType);
                    sb.AppendFormat("&ExpiredDate={0}", item.ExpiredDate);//23/05/2018 15:36
                    sb.AppendFormat("&SourceURL={0}", item.SourceURL);
                    sb.AppendFormat("&BonusPrice={0}", item.BonusPrice);

                    sb.AppendFormat("&ShortTitle={0}", item.ShortTitle);
                    sb.AppendFormat("&ParentNewsId={0}", item.ParentNewsId);
                    sb.AppendFormat("&WarningLevel={0}", item.WarningLevel);

                    sb.AppendFormat("&ZoneId={0}", item.ZoneId);//31
                    sb.AppendFormat("&ZoneName={0}", item.ZoneName);//Tình yêu - Hôn nhân
                    sb.AppendFormat("&QuickNewsVietId={0}", item.QuickNewsVietId);
                    sb.AppendFormat("&CmsAccountVietId={0}", item.CmsAccountVietId);
                    sb.AppendFormat("&PenName={0}", item.PenName);
                    sb.AppendFormat("&IsActivePenName={0}", item.IsActivePenName);
                    sb.AppendFormat("&IsShowPenNameCTV={0}", item.IsShowPenNameCTV);
                    sb.AppendFormat("&ContentFooterType={0}", item.ContentFooterType);                                                                               

                    var topicId = 1;
                    sb.AppendFormat("&TopicId={0}", topicId);

                    var magazine_video_cover = "";
                    sb.AppendFormat("&magazine_video_cover={0}", magazine_video_cover);

                    var magazine_video_text_layer = "";
                    sb.AppendFormat("&magazine_video_text_layer={0}", magazine_video_text_layer);

                    var is_show_avatar = true;
                    sb.AppendFormat("&is_show_avatar={0}", is_show_avatar);

                    var sensitivenews = 0;
                    sb.AppendFormat("&sensitivenews={0}", sensitivenews);

                    var _ParentNewsTitle = "Khóc dở mếu dở khi đọc tâm thư của những ông chồng phải gồng mình lên để chiều vợ \"sinh lý cao\"";
                    sb.AppendFormat("&_ParentNewsTitle={0}", _ParentNewsTitle);

                    var MetaKeywordFocus = "công lương";
                    sb.AppendFormat("&MetaKeywordFocus={0}", MetaKeywordFocus);

                    var MetaDescription = "mô tả seo";
                    sb.AppendFormat("&MetaDescription={0}", MetaDescription);

                    var MetaTitle = "tiêu đề seo";
                    sb.AppendFormat("&MetaTitle={0}", MetaTitle);

                    var TagList = "47927";
                    sb.AppendFormat("&TagList={0}", TagList);

                    var NewsRelationIdList = "20180521152206976;20180521151911742;20180519111207414";
                    sb.AppendFormat("&NewsRelationIdList={0}", NewsRelationIdList);

                    var ExtensionInfo = "[{\"NewsId\":\"0\",\"Type\":19,\"Value\":\"1\"}]";
                    sb.AppendFormat("&ExtensionInfo={0}", ExtensionInfo);

                    var AccountName = "";
                    sb.AppendFormat("&AccountName={0}", AccountName);

                    sb.AppendFormat("&action={0}", "notnewid");

                    client.PostData = sb.ToString();
                    client.ActionName = "api/base/news/insert_news";
                    var strData = client.MakeRequest();

                    var ParseObject = NewtonJson.Deserialize<WcfActionResponse>(strData);
                    if (ParseObject == null) ParseObject = new WcfActionResponse();
                    if (!ParseObject.Success)
                        Logger.Log(TypeLog.Data, "PostDataApi => NewId: " + item.Id + " => Message: " + ParseObject.Message);
                }
                catch { }
            });

            try
            {
                processCount.Wait(TimeSpan.FromSeconds(3));
            }
            catch { }
        }
        #endregion

        #region Chuẩn hóa theo note của Hữu
        private static bool UpdateDbChuanHoaNoteByHuu(string connectStr2, long newsId)
        {
            var nameSpace = ConfigurationManager.AppSettings["NameSpace"];
            var flg = false;
            switch (nameSpace)
            {
                case "AFamily":
                    flg = Afamily_ChuanHoa(connectStr2, newsId);
                    break;
            }
            return flg;
        }

        private static bool Afamily_ChuanHoa(string connectStr2, long newsId)
        {
            var con = GetConnection(connectStr2);
            try
            {
                string query = @"UPDATE NewsPublish set NewsType=0, [Type]=8 WHERE NewsType=4 and NewsId=@NewsId;
                                
                                UPDATE News set NewsType=0, [Type]=8 WHERE NewsType=4 and Id=@NewsId;
                                
                                UPDATE NewsPublish set NewsType=0, [Type]=18 WHERE NewsType=5 and NewsId=@NewsId;
                                
                                UPDATE News set NewsType=0, [Type]=18 WHERE NewsType=5 and Id=@NewsId;
                                
                                UPDATE NewsExtension set [Type]=3005 WHERE [Type]=8008 and NewsId=@NewsId;
                                
                                UPDATE NewsExtension set [Type]=3006 WHERE [Type]=8009 and NewsId=@NewsId;
                                
                                UPDATE NewsExtension set [Type]=3007 WHERE [Type]=8010 and NewsId=@NewsId;
                                
                                UPDATE News set Avatar5=Avatar3,Avatar3=Avatar5 WHERE [Type]<>27 and Avatar5 is not null and avatar5<>'' and Id=@NewsId;
                                
                                UPDATE News set Avatar5=Avatar3 WHERE [Type]<>27 and (Avatar5 is null or avatar5 ='') and Id=@NewsId;
                                
                                UPDATE News set Avatar3=Avatar4 WHERE [Type]=27 and Id=@NewsId;
                                
                                UPDATE News set Avatar4=NewsExtension.Value FROM News 
                                INNER JOIN NewsExtension ON News.Id=NewsExtension.NewsId 
                                WHERE NewsExtension.[Type]=3004 and News.[Type]=27 and News.Id=@NewsId;";

                using (var cmd = new SqlCommand(query, con))
                {

                    cmd.Parameters.AddWithValue("@NewsId", newsId);

                    con.Open();
                    var reader = cmd.ExecuteNonQuery();
                    con.Close();

                    return reader > 0;
                }
            }
            catch (Exception ex)
            {
                Logger.Log(TypeLog.Error, "UpdateDbChuanHoaNoteByHuu=> " + ex.Message);
                return false;
            }
            finally
            {
                con.Close();
                con.Dispose();
            }

        }
        #endregion

        #endregion
    }
}
