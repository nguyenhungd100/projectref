﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System;

namespace WS.SYNCDATA.Common
{
    public class NewtonJson
    {        
        public static string Serialize(object @object, string dateTimeFormat)
        {
            try
            {
                return JsonConvert.SerializeObject(@object, new IsoDateTimeConverter { DateTimeFormat = dateTimeFormat });                
            }
            catch (Exception ex)
            {
                Logger.Log(TypeLog.Error, ex.Message);
                return string.Empty;
            }
        }

        public static string Serialize(object @object)
        {
            try
            {
                return JsonConvert.SerializeObject(@object, MicrosoftDateFormatSettings);                
            }
            catch (Exception ex)
            {
                Logger.Log(TypeLog.Error, ex.Message);
                return string.Empty;
            }
        }
        
        public static T Deserialize<T>(string jsonString)
        {
            try
            {
                return JsonConvert.DeserializeObject<T>(jsonString, MicrosoftDateFormatSettings);                
            }
            catch (Exception ex)
            {
                Logger.Log(TypeLog.Error, ex.Message);
                return default(T);
            }
        }
        public static T Deserialize<T>(string jsonString, string dateTimeFormat)
        {
            try
            {
                return JsonConvert.DeserializeObject<T>(jsonString, new IsoDateTimeConverter { DateTimeFormat = dateTimeFormat });                
            }
            catch (Exception ex)
            {
                Logger.Log(TypeLog.Error, ex.Message);
                return default(T);
            }
        }

        // Privates Format Date
        private static readonly JsonSerializerSettings MicrosoftDateFormatSettings = new JsonSerializerSettings
        {
            DateFormatHandling =
                                                                                 DateFormatHandling.MicrosoftDateFormat
        };
    }
}
