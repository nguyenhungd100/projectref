﻿using IMS2.SYNCDATA.SQL;
using System;
using System.Configuration;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using Newtonsoft.Json;
using IMS2.SYNCDATA.Entity;
using System.Collections.Generic;
using IMS2.SYNCDATA.Common;
using System.Globalization;
using System.Linq;

namespace IMS2.SYNCDATA
{
    public partial class Main : Form
    {
        private CancellationTokenSource tokenSourceEs;
        private CancellationTokenSource tokenSourceRedis;

        private int ss1 = 0;
        private int mm1 = 0;
        private int hh1 = 0;

        private int ss2 = 0;
        private int mm2 = 0;
        private int hh2 = 0;
        private NameSpaceEntity nameSpace = new NameSpaceEntity();

        public Main()
        {
            InitializeComponent();
        }

        private void Main_Load(object sender, EventArgs e)
        {
            //ES
            progressBarEs.Minimum = 0;
            progressBarEs.Maximum = 100;
            buttonCancelEs.Enabled = false;

            //REDIS
            progressBarRedis.Minimum = 0;
            progressBarRedis.Maximum = 100;
            buttonCancelRedis.Enabled = false;

            //Get Connect to config
            GetAllConnectString();

            //Get namespace
            GetAllNameSpaces();

            //Get table
            GetAllTables();

        }

        #region Init
        private void GetAllConnectString()
        {
            treeViewDB.Nodes.Clear();
            var valueSql = ConfigurationManager.AppSettings["ListConnectString.SQL"];            
            if (!string.IsNullOrEmpty(valueSql))
            {
                var listSql = JsonConvert.DeserializeObject<List<ConnectEntity>>(valueSql)
                    .Where(t=>string.IsNullOrEmpty(nameSpace.Name) 
                    || t.Name.ToUpper().Contains(nameSpace.Name.ToUpper())
                    || (!string.IsNullOrEmpty(nameSpace.ListSubName) && nameSpace.ListSubName.Split(',').FirstOrDefault(n=>t.Name.ToUpper().Contains(n.ToUpper())) != null)).ToList();
                var root = new TreeNode("SQL DB");                
                if(listSql!=null && listSql.Count > 0)
                {
                    foreach(var item in listSql)
                    {
                        var node = new TreeNode(item.Name);
                        node.ToolTipText = item.ConnectString;
                        root.Nodes.Add(node);
                    }
                }
                treeViewDB.Nodes.Add(root);                
            }

            var valueEs = ConfigurationManager.AppSettings["ListConnectString.ES"];            
            if (!string.IsNullOrEmpty(valueEs))
            {
                var listEs = JsonConvert.DeserializeObject<List<ConnectEntity>>(valueEs)
                    .Where(t => string.IsNullOrEmpty(nameSpace.Name) 
                    || t.Name.ToUpper().Contains(nameSpace.Name.ToUpper())
                    //|| t.ConnectString.ToUpper().Contains(nameSpace.Name.ToUpper())
                    || (!string.IsNullOrEmpty(nameSpace.ListSubName) && nameSpace.ListSubName.Split(',').FirstOrDefault(n => t.Name.ToUpper().Contains(n.ToUpper())) != null)).ToList();
                var rootEs = new TreeNode("ES DB");
                if (listEs != null && listEs.Count > 0)
                {
                    foreach (var item in listEs)
                    {
                        var node = new TreeNode(item.Name);
                        node.ToolTipText = item.ConnectString;
                        rootEs.Nodes.Add(node);
                    }
                }
                treeViewDB.Nodes.Add(rootEs);
            }

            var valueRedis = ConfigurationManager.AppSettings["ListConnectString.REDIS"];
            if (!string.IsNullOrEmpty(valueRedis))
            {
                var listRedis = JsonConvert.DeserializeObject<List<ConnectEntity>>(valueRedis)
                    .Where(t => string.IsNullOrEmpty(nameSpace.Name)
                    || t.Name.ToUpper().Contains(nameSpace.Name.ToUpper())
                    //|| t.ConnectString.ToUpper().Contains(nameSpace.Name.ToUpper())
                    || (!string.IsNullOrEmpty(nameSpace.ListSubName) && nameSpace.ListSubName.Split(',').FirstOrDefault(n => t.Name.ToUpper().Contains(n.ToUpper())) != null)).ToList();
                var rootRedis = new TreeNode("REDIS DB");
                if (listRedis != null && listRedis.Count > 0)
                {
                    foreach (var item in listRedis)
                    {
                        var node = new TreeNode(item.Name);
                        node.ToolTipText = item.ConnectString;
                        rootRedis.Nodes.Add(node);
                    }
                }
                treeViewDB.Nodes.Add(rootRedis);                
            }

            treeViewDB.ExpandAll();

            maskedTextBoxFormDate.Text = DateTime.Now.ToString("MM-dd-yyyy HH:mm");
            maskedTextBoxToDate.Text = DateTime.Now.ToString("MM-dd-yyyy HH:mm");
        }

        private void GetAllTables()
        {
            var tables = new List<TableEntity> {
                new TableEntity {Name="News",Value="news" },
                new TableEntity {Name="Zone",Value="zone" },
                new TableEntity {Name="User",Value="user" },
                new TableEntity {Name="Video",Value="video" },
                new TableEntity {Name="ZoneVideo",Value="zonevideo" },
                new TableEntity {Name="Tag",Value="tag" },
                new TableEntity {Name="Thread",Value="thread" },
                new TableEntity {Name="Topic",Value="topic" },
                new TableEntity {Name="UpdateBody",Value="updatebody" }
            };
            comboBoxTable.DataSource = tables;
            comboBoxTable.DisplayMember = "Name";
            comboBoxTable.ValueMember = "Value";
        }

        private void GetAllNameSpaces()
        {
            var valueSql = ConfigurationManager.AppSettings["ListNameSpace"];
            var lstNameSpace = JsonConvert.DeserializeObject<List<NameSpaceEntity>>(valueSql);
            comboBoxNameSpace.DataSource = lstNameSpace;
            comboBoxNameSpace.DisplayMember = "Name";
            comboBoxNameSpace.ValueMember = "Name";
        }
        #endregion

        #region ES
        private async void buttonStartEs_Click(object sender, EventArgs e)
        {
            var connectStr = labelConnectStringSQL.Text;
            if (connectStr == "Not ConnectStringSQL" || string.IsNullOrEmpty(connectStr))
            {
                MessageBox.Show("Bạn chưa chọn connect tới SQL");
                return;
            }
            var connect = labelConnectStringES.Text;            
            if (connect == "Not ConnectStringES" || string.IsNullOrEmpty(connect))
            {
                MessageBox.Show("Bạn chưa chọn connect tới ES");
                return;
            }
            var connectEs = connect.Split('|')[1];
            var nameSpace = connect.Split('|')[0];
            if (string.IsNullOrEmpty(connectEs) || connectEs == "Not Decrypt Key ConnectString" || string.IsNullOrEmpty(nameSpace))
            {
                MessageBox.Show("Kiểm tra lại connect tới ES và NameSpace");
                return;
            }

            var pageIndex = 1;
            if (!string.IsNullOrEmpty(textBoxPageIndex.Text))
            {
                pageIndex = Convert.ToInt32(textBoxPageIndex.Text);
            }
            var pageSize = 1000;
            if (!string.IsNullOrEmpty(textBoxPageSize.Text))
            {
                pageSize = Convert.ToInt32(textBoxPageSize.Text);
            }

            DateTime rs;

            var from = maskedTextBoxFormDate.Text;
            if (!maskedTextBoxFormDate.MaskFull || !DateTime.TryParseExact(maskedTextBoxFormDate.Text, "MM-dd-yyyy HH:mm", new CultureInfo("en-US"), DateTimeStyles.None, out rs))
            {
                //MessageBox.Show("Bạn chưa nhập đúng định dạng FormDate.");
                //return;
                from = "";
            }
            
            var to = maskedTextBoxToDate.Text;
            if (!maskedTextBoxToDate.MaskFull || !DateTime.TryParseExact(maskedTextBoxToDate.Text, "MM-dd-yyyy HH:mm", new CultureInfo("en-US"), DateTimeStyles.None, out rs))
            {
                //MessageBox.Show("Bạn chưa nhập đúng định dạng ToDate.");
                //return;
                to = "";
            }
            var isDelete = false;
            if (checkBoxIsDelete.Checked)
            {
                isDelete = true;
            }

            var table = comboBoxTable.SelectedValue.ToString();

            var progressIndicator = new Progress<InitDataDb.ProcessEntity>(UpdateProgressEs);

            buttonStartEs.Enabled = false;
            buttonCancelEs.Enabled = true;
            labelPercentEs.Text = @"Page: 0";
            progressBarEs.Value = 0;
            tokenSourceEs = new CancellationTokenSource();

            labelClockEs.Text = "00:00:00";
            ss1 = 0; mm1 = 0; hh1 = 0;
            timerClockEs.Interval = 1000;
            timerClockEs.Enabled = true;
            timerClockEs.Tick += new EventHandler(TimerClockEs_Tick);

            try
            {
                var allFiles = await TaskSyncAllDataEs(nameSpace, connectStr, connectEs, table, pageIndex, pageSize, from, to, progressIndicator, tokenSourceEs.Token, isDelete);
            }
            catch (OperationCanceledException)
            {
                //lblPercent.Text = @"Stop: ";
            }

            buttonStartEs.Enabled = true;
            buttonCancelEs.Enabled = false;
            timerClockEs.Enabled = false;

            labelPercentEs.Text = labelPercentEs.Text + " -> DONE";
        }

        private void TimerClockEs_Tick(object Sender, EventArgs e)
        {
            ss1++;
            if (ss1 == 60)
            {
                ss1 = 0;
                mm1 = mm1 + 1;
            };
            if (mm1 == 60)
            {
                mm1 = 0;
                hh1 = hh1 + 1;
            }

            var ssTemp = ss1.ToString();
            if (ss1 < 10)
            {
                ssTemp = "0" + ss1;
            }
            var mmTemp = mm1.ToString();
            if (mm1 < 10)
            {
                mmTemp = "0" + mm1;
            }
            var hhTemp = hh1.ToString();
            if (hh1 < 10)
            {
                hhTemp = "0" + hh1;
            }

            labelClockEs.Text = hhTemp + ":" + mmTemp + ":" + ssTemp;
        }

        private void buttonCancel_Click(object sender, EventArgs e)
        {
            buttonCancelEs.Enabled = false;
            buttonStartEs.Enabled = false;
            timerClockEs.Enabled = false;

            tokenSourceEs.Cancel();
        }

        private async Task<int> TaskSyncAllDataEs(string nameSpace, string connectString, string connectEs, string table, int pageIndex, int pageSize, string from, string to, IProgress<InitDataDb.ProcessEntity> progress, CancellationToken token, bool isDelete)
        {
            var countAll= GetCountAllEs(table, connectString, from, to);
            if (countAll == 0)
            {
                MessageBox.Show("Table không được hỗ trợ sync OR Không tìm thấy bản ghi nào của Table: " + table);
                return 0;
            }

            var totalCount = (int)Math.Ceiling((double)countAll / pageSize);
            progressBarEs.Maximum = totalCount;
            labelTotalPageES.Text = @"Total page: "+ totalCount;
            
            var processCount = await Task.Run(() =>
            {
                var processEntity = new InitDataDb.ProcessEntity();
                for (var page = pageIndex; page <= totalCount; page++)
                {
                    processEntity.page = page;
                    processEntity.value = SyncDataToPageEs(nameSpace, connectString, connectEs, page, pageSize, table, from, to, isDelete);
                                        
                    token.ThrowIfCancellationRequested();

                    if (progress != null)
                    {                        
                        progress.Report(processEntity);
                    }
                }

                return processEntity.page;
            }, token);

            return processCount;
        }

        private int GetCountAllEs(string table, string connectString, string from, string to)
        {
            int count = 0;
            switch (table)
            {
                case "news": count = NewsBo.GetCountNewsES(connectString, from, to); break;
                case "zone": count = 0; break;
                case "user": count = UserBo.GetCountUser(connectString); break;
                case "video": count = VideoBo.GetCountVideo(connectString); break;
                case "zonevideo": count = 0; break;
                case "tag": count = TagBo.GetCountTag(connectString); break;
                case "thread": count = ThreadBo.GetCountThread(connectString); break;
                case "topic": count = TopicBo.GetCountTopic(connectString); break;
            }

            return count;
        }

        private int SyncDataToPageEs(string nameSpace, string connectString, string connectEs, int page, int pageSize, string table, string from, string to, bool isDelete)
        {
            var count = 0;
            switch (table)
            {
                case "news": count = NewsBo.SyncNewsToEs(nameSpace, connectString, connectEs, page, pageSize, from, to, isDelete); break;
                case "zone": count = 0; break;
                case "user": count = UserBo.SyncUserToEs(nameSpace, connectString, connectEs, page, pageSize); break;
                case "video": count = VideoBo.SyncVideoToEs(nameSpace, connectString, connectEs, page, pageSize); break;
                case "zonevideo": count = 0; break;
                case "tag": count = TagBo.SyncTagToEs(nameSpace, connectString, connectEs, page, pageSize); break;
                case "thread": count = ThreadBo.SyncThreadToEs(nameSpace, connectString, connectEs, page, pageSize); break;
                case "topic": count = TopicBo.SyncTopicToEs(nameSpace, connectString, connectEs, page, pageSize); break;
            }

            return count;            
        }        

        private void UpdateProgressEs(InitDataDb.ProcessEntity obj)
        {
            progressBarEs.Value = obj.page;
            labelPercentEs.Text = "Đang sync page: " + obj.page.ToString();// + " ------> Running...: " + obj.value.ToString();
        }
        #endregion

        #region Redis
        private async void buttonStartRedis_Click(object sender, EventArgs e)
        {
            var connectStr = labelConnectStringSQL.Text;
            if (connectStr == "Not ConnectStringSQL" || string.IsNullOrEmpty(connectStr))
            {
                MessageBox.Show("Bạn chưa chọn connect tới SQL");
                return;
            }
            var connect = labelConnectStringREDIS.Text;
            if (connect == "Not ConnectStringREDIS" || string.IsNullOrEmpty(connect))
            {
                MessageBox.Show("Bạn chưa chọn connect tới REDIS");
                return;
            }
            var connectRedis = connect.Split('|')[1];
            var nameSpace = connect.Split('|')[0];
            if (string.IsNullOrEmpty(connectRedis) || connectRedis == "Not Decrypt Key ConnectString" || string.IsNullOrEmpty(nameSpace))
            {
                MessageBox.Show("kiểm tra lại connect tới REDIS và NameSpace");
                return;
            }

            var pageIndex = 1;
            if (!string.IsNullOrEmpty(textBoxPageIndex.Text))
            {
                pageIndex = Convert.ToInt32(textBoxPageIndex.Text);
            }
            var pageSize = 1000;
            if (!string.IsNullOrEmpty(textBoxPageSize.Text))
            {
                pageSize = Convert.ToInt32(textBoxPageSize.Text);
            }            

            var table = comboBoxTable.SelectedValue.ToString();

            var progressIndicator = new Progress<InitDataDb.ProcessEntity>(UpdateProgressRedis);

            buttonStartRedis.Enabled = false;
            buttonCancelRedis.Enabled = true;
            labelPercentRedis.Text = @"Page: 0";
            progressBarRedis.Value = 0;
            tokenSourceRedis = new CancellationTokenSource();

            labelClockRedis.Text = "00:00:00";
            ss2 = 0; mm2 = 0; hh2 = 0;
            timerClockRedis.Interval = 1000;
            timerClockRedis.Enabled = true;
            timerClockRedis.Tick += new EventHandler(TimerClockRedis_Tick);

            try
            {
                var allFiles = await TaskSyncAllDataRedis(nameSpace, connectStr, connectRedis, table, pageIndex, pageSize, progressIndicator, tokenSourceRedis.Token);
            }
            catch (OperationCanceledException)
            {
                //lblPercent.Text = @"Stop: ";
            }

            buttonStartRedis.Enabled = true;
            buttonCancelRedis.Enabled = false;
            timerClockRedis.Enabled = false;

            labelPercentRedis.Text = labelPercentRedis.Text + " -> DONE";
        }

        private void TimerClockRedis_Tick(object Sender, EventArgs e)
        {
            ss2++;
            if (ss2 == 60)
            {
                ss2 = 0;
                mm2 = mm2 + 1;
            };
            if (mm2 == 60)
            {
                mm2 = 0;
                hh2 = hh2 + 1;
            }

            var ssTemp = ss2.ToString();
            if (ss2 < 10)
            {
                ssTemp = "0" + ss2;
            }
            var mmTemp = mm2.ToString();
            if (mm2 < 10)
            {
                mmTemp = "0" + mm2;
            }
            var hhTemp = hh2.ToString();
            if (hh2 < 10)
            {
                hhTemp = "0" + hh2;
            }

            labelClockRedis.Text = hhTemp + ":" + mmTemp + ":" + ssTemp;
        }

        private void buttonCancelRedis_Click(object sender, EventArgs e)
        {
            buttonCancelRedis.Enabled = false;
            buttonStartRedis.Enabled = false;
            timerClockRedis.Enabled = false;

            tokenSourceRedis.Cancel();
        }

        private async Task<int> TaskSyncAllDataRedis(string nameSpace, string connectString, string connectRedis, string table, int pageIndex, int pageSize, IProgress<InitDataDb.ProcessEntity> progress, CancellationToken token)
        {
            var countAll = GetCountAllRedis(table, connectString);
            if (countAll == 0)
            {
                MessageBox.Show("Table không được hỗ trợ sync OR Không tìm thấy bản ghi nào của Table: "+table);
                return 0;
            }

            var totalCount = (int)Math.Ceiling((double)countAll / pageSize);
            progressBarRedis.Maximum = totalCount;
            labelTotalPageRedis.Text = @"Total page: " + totalCount;

            var processCount = await Task.Run(() =>
            {
                var processEntity = new InitDataDb.ProcessEntity();
                for (var page = pageIndex; page <= totalCount; page++)
                {
                    processEntity.page = page;
                    processEntity.value = SyncDataToPageRedis(nameSpace, connectString, connectRedis, page, pageSize, table);
                   
                    token.ThrowIfCancellationRequested();

                    if (progress != null)
                    {                        
                        progress.Report(processEntity);
                    }
                }

                return processEntity.page;
            }, token);

            return processCount;
        }

        private int GetCountAllRedis(string table, string connectString)
        {
            int count = 0;
            switch (table)
            {
                case "news": count = NewsBo.GetCountNewsRedis(connectString); break;
                case "zone": count = ZoneBo.GetCountZone(connectString); break;
                case "user": count = UserBo.GetCountUser(connectString); break;
                case "video": count = VideoBo.GetCountVideo(connectString); break;
                case "zonevideo": count = ZoneVideoBo.GetCountZoneVideo(connectString); break;
                case "tag": count = TagBo.GetCountTag(connectString); break;
                case "thread": count = ThreadBo.GetCountThread(connectString); break;
                case "topic": count = TopicBo.GetCountTopic(connectString); break;
                case "updatebody": count = NewsBo.GetCountNewsRedisUpdateBody(connectString); break;                
            }

            return count;            
        }

        private int SyncDataToPageRedis(string nameSpace, string connectString, string connectRedis, int page, int pageSize, string table)
        {
            int count = 0;
            switch (table)
            {
                case "news": count = NewsBo.SyncNewsToRedis(nameSpace, connectString, connectRedis, page, pageSize); break;
                case "zone": count = ZoneBo.SyncZoneToRedis(nameSpace, connectString, connectRedis, page, pageSize); break;
                case "user": count = UserBo.SyncUserToRedis(nameSpace, connectString, connectRedis, page, pageSize); break;
                case "video": count = VideoBo.SyncVideoToRedis(nameSpace, connectString, connectRedis, page, pageSize); break;
                case "zonevideo": count = ZoneVideoBo.SyncZoneVideoToRedis(nameSpace, connectString, connectRedis, page, pageSize); break;
                case "tag": count = TagBo.SyncTagToRedis(nameSpace, connectString, connectRedis, page, pageSize); break;
                case "thread": count = ThreadBo.SyncThreadToRedis(nameSpace, connectString, connectRedis, page, pageSize); break;
                case "topic": count = TopicBo.SyncTopicToRedis(nameSpace, connectString, connectRedis, page, pageSize); break;
                case "updatebody": count = NewsBo.SyncNewsToRedisUpdateBody(nameSpace, connectString, connectRedis, page, pageSize); break;               
            }

            return 0;          
        }

        private void UpdateProgressRedis(InitDataDb.ProcessEntity obj)
        {
            progressBarRedis.Value = obj.page;
            labelPercentRedis.Text = "Đang sync page: " + obj.page.ToString();// + " ------> Running...: " + obj.value.ToString();
        }

        private async Task<int> TaskSyncAllDataExcel(string connectString, string domain, string from, string to, int pageIndex, int pageSize, IProgress<InitDataDb.ProcessEntity> progress, CancellationToken token)
        {
            var countAll = ExportExcelBo.GetCountNewsExcel(connectString, from, to);
            if (countAll == 0)
            {
                MessageBox.Show("Table không được hỗ trợ sync OR Không tìm thấy bản ghi nào của Table");
                return 0;
            }

            var totalCount = (int)Math.Ceiling((double)countAll / pageSize);
            progressBarRedis.Maximum = totalCount;
            labelTotalPageRedis.Text = @"Total page: " + totalCount;

            var processCount = await Task.Run(() =>
            {
                var processEntity = new InitDataDb.ProcessEntity();
                for (var page = pageIndex; page <= totalCount; page++)
                {
                    processEntity.page = page;
                    processEntity.value = ExportExcelBo.SyncNewsToExcel(connectString, from,to, page, pageSize, totalCount, domain);

                    token.ThrowIfCancellationRequested();

                    if (progress != null)
                    {
                        progress.Report(processEntity);
                    }
                }

                return processEntity.page;
            }, token);

            return processCount;
        }
        #endregion                

        private void treeViewDB_NodeMouseDoubleClick(object sender, TreeNodeMouseClickEventArgs e)
        {
            var node = treeViewDB.SelectedNode;
            if (node != null && node.Level > 0)
            {
                var decryptKey = ConfigurationManager.AppSettings["ConnectionDecryptKey"];

                if (node.Parent.Text == "SQL DB")
                {
                    labelConnectStringSQL.Text = Crypton.DecryptByKey(node.ToolTipText, decryptKey) == string.Empty ? "Not Decrypt Key ConnectString" : Crypton.DecryptByKey(node.ToolTipText, decryptKey);
                }
                if (node.Parent.Text == "ES DB")
                {
                    var ar = node.ToolTipText.Split('|');
                    labelConnectStringES.Text = ar[0] + "|" + (Crypton.DecryptByKey(ar[1], decryptKey) == string.Empty ? "Not Decrypt Key ConnectString" : Crypton.DecryptByKey(ar[1], decryptKey));
                }
                if (node.Parent.Text == "REDIS DB")
                {
                    var ar = node.ToolTipText.Split('|');
                    labelConnectStringREDIS.Text = ar[0] + "|" + (Crypton.DecryptByKey(ar[1], decryptKey) == string.Empty ? "Not Decrypt Key ConnectString" : Crypton.DecryptByKey(ar[1], decryptKey));
                }
            }
        }

        private void treeViewDB_NodeMouseClick(object sender, TreeNodeMouseClickEventArgs e)
        {
            var node = e.Node;
            if (node != null && node.Level>0)
            {
                var decryptKey = ConfigurationManager.AppSettings["ConnectionDecryptKey"];

                if (node.Parent.Text == "SQL DB")
                {
                    labelConnectStringSQL.Text = Crypton.DecryptByKey(node.ToolTipText, decryptKey) == string.Empty ? "Not Decrypt Key ConnectString" : Crypton.DecryptByKey(node.ToolTipText, decryptKey);
                }
                if (node.Parent.Text == "ES DB")
                {
                    var ar = node.ToolTipText.Split('|');
                    labelConnectStringES.Text = ar[0]+"|"+ (Crypton.DecryptByKey(ar[1], decryptKey) == string.Empty ? "Not Decrypt Key ConnectString" : Crypton.DecryptByKey(ar[1], decryptKey));
                }
                if (node.Parent.Text == "REDIS DB")
                {
                    var ar = node.ToolTipText.Split('|');
                    labelConnectStringREDIS.Text = ar[0] + "|" + (Crypton.DecryptByKey(ar[1], decryptKey) == string.Empty ? "Not Decrypt Key ConnectString" : Crypton.DecryptByKey(ar[1], decryptKey));
                }                                
            }            
        }

        private void textBoxPageIndex_TextChanged(object sender, EventArgs e)
        {
            if (System.Text.RegularExpressions.Regex.IsMatch(textBoxPageIndex.Text, "[^0-9]"))
            {
                textBoxPageIndex.Text = textBoxPageIndex.Text.Remove(textBoxPageIndex.Text.Length - 1);
                MessageBox.Show("Please enter only numbers.");                
            }
        }

        private void textBoxPageSize_TextChanged(object sender, EventArgs e)
        {
            if (System.Text.RegularExpressions.Regex.IsMatch(textBoxPageSize.Text, "[^0-9]"))
            {
                textBoxPageSize.Text = textBoxPageSize.Text.Remove(textBoxPageSize.Text.Length - 1);
                MessageBox.Show("Please enter only numbers.");
            }
        }

        private void genConnectStringToolStripMenuItem_Click(object sender, EventArgs e)
        {            
            var frm = new GenConnectString();
            frm.ShowDialog(this);
            frm.Dispose();
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void comboBoxTable_SelectedIndexChanged(object sender, EventArgs e)
        {            
            var table = comboBoxTable.SelectedValue.ToString();
            switch (table)
            {                
                case "zone":
                case "user":               
                case "zonevideo": textBoxPageIndex.Enabled = false; textBoxPageSize.Enabled = false; break;
                default: textBoxPageIndex.Enabled = true; textBoxPageSize.Enabled = true; break;
            }
        }

        private void iNITIMS1TOIMS2ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var frm = new InitDataDb();
            frm.Show(this);
            //frm.Dispose();
        }

        private async void button1_Click(object sender, EventArgs e)
        {
            var connectStr = labelConnectStringSQL.Text;
            if (connectStr == "Not ConnectStringSQL" || string.IsNullOrEmpty(connectStr))
            {
                MessageBox.Show("Bạn chưa chọn connect tới SQL");
                return;
            }
            var connect = labelConnectStringREDIS.Text;
            if (connect == "Not ConnectStringREDIS" || string.IsNullOrEmpty(connect))
            {
                MessageBox.Show("Bạn chưa chọn connect tới REDIS");
                return;
            }
            var connectRedis = connect.Split('|')[1];
            var nameSpace = connect.Split('|')[0];
            if (string.IsNullOrEmpty(connectRedis) || connectRedis == "Not Decrypt Key ConnectString" || string.IsNullOrEmpty(nameSpace))
            {
                MessageBox.Show("kiểm tra lại connect tới REDIS và NameSpace");
                return;
            }

            var pageIndex = 1;
            if (!string.IsNullOrEmpty(textBoxPageIndex.Text))
            {
                pageIndex = Convert.ToInt32(textBoxPageIndex.Text);
            }
            var pageSize = 1000;
            if (!string.IsNullOrEmpty(textBoxPageSize.Text))
            {
                pageSize = Convert.ToInt32(textBoxPageSize.Text);
            }

            var table = comboBoxTable.SelectedValue.ToString();
            if (!table.Contains("updatebody"))
            {
                MessageBox.Show("Selected Tables => action: updatebody");
                return;
            }

            var progressIndicator = new Progress<InitDataDb.ProcessEntity>(UpdateProgressRedis);

            buttonStartRedis.Enabled = false;
            buttonCancelRedis.Enabled = true;
            labelPercentRedis.Text = @"Page: 0";
            progressBarRedis.Value = 0;
            tokenSourceRedis = new CancellationTokenSource();

            labelClockRedis.Text = "00:00:00";
            ss2 = 0; mm2 = 0; hh2 = 0;
            timerClockRedis.Interval = 1000;
            timerClockRedis.Enabled = true;
            timerClockRedis.Tick += new EventHandler(TimerClockRedis_Tick);

            try
            {
                var allFiles = await TaskSyncAllDataRedis(nameSpace, connectStr, connectRedis, table, pageIndex, pageSize, progressIndicator, tokenSourceRedis.Token);
            }
            catch (OperationCanceledException)
            {
                //lblPercent.Text = @"Stop: ";
            }

            buttonStartRedis.Enabled = true;
            buttonCancelRedis.Enabled = false;
            timerClockRedis.Enabled = false;

            labelPercentRedis.Text = labelPercentRedis.Text + " -> DONE";
        }

        private async void buttonExportExcel_Click(object sender, EventArgs e)
        {
            var connectStr = labelConnectStringSQL.Text;
            if (connectStr == "Not ConnectStringSQL" || string.IsNullOrEmpty(connectStr))
            {
                MessageBox.Show("Bạn chưa chọn connect tới SQL");
                return;
            }            

            DateTime rs;

            var from = maskedTextBoxFormDate.Text;
            if (!maskedTextBoxFormDate.MaskFull || !DateTime.TryParseExact(maskedTextBoxFormDate.Text, "MM-dd-yyyy HH:mm", new CultureInfo("en-US"), DateTimeStyles.None, out rs))
            {
                //MessageBox.Show("Bạn chưa nhập đúng định dạng FormDate.");
                //return;
                from = "";
            }

            var to = maskedTextBoxToDate.Text;
            if (!maskedTextBoxToDate.MaskFull || !DateTime.TryParseExact(maskedTextBoxToDate.Text, "MM-dd-yyyy HH:mm", new CultureInfo("en-US"), DateTimeStyles.None, out rs))
            {
                //MessageBox.Show("Bạn chưa nhập đúng định dạng ToDate.");
                //return;
                to = "";
            }

            var pageIndex = 1;
            if (!string.IsNullOrEmpty(textBoxPageIndex.Text))
            {
                pageIndex = Convert.ToInt32(textBoxPageIndex.Text);
            }
            var pageSize = 50;
            //if (!string.IsNullOrEmpty(textBoxPageSize.Text))
            //{
            //    pageSize = Convert.ToInt32(textBoxPageSize.Text);
            //}     
            var domain = "afamily.vn";
            var connect = labelConnectStringREDIS.Text;
            if (connect == "Not ConnectStringREDIS" || string.IsNullOrEmpty(connect))
            {
                MessageBox.Show("Bạn chưa chọn connect tới REDIS");
                return;
            }
            var connectRedis = connect.Split('|')[1];
            var nameSpace = connect.Split('|')[0];
            if (string.IsNullOrEmpty(connectRedis) || connectRedis == "Not Decrypt Key ConnectString" || string.IsNullOrEmpty(nameSpace))
            {
                MessageBox.Show("kiểm tra lại connect tới REDIS và NameSpace");
                return;
            }
            switch (nameSpace)
            {
                case "AFamily":
                    domain = "afamily.vn";
                    break;
                case "Sport5":
                    domain = "sport5.vn";
                    break;
            }

            var progressIndicator = new Progress<InitDataDb.ProcessEntity>(UpdateProgressRedis);

            buttonStartRedis.Enabled = false;
            buttonCancelRedis.Enabled = true;
            labelPercentRedis.Text = @"Page: 0";
            progressBarRedis.Value = 0;
            tokenSourceRedis = new CancellationTokenSource();

            labelClockRedis.Text = "00:00:00";
            ss2 = 0; mm2 = 0; hh2 = 0;
            timerClockRedis.Interval = 1000;
            timerClockRedis.Enabled = true;
            timerClockRedis.Tick += new EventHandler(TimerClockRedis_Tick);

            try
            {
                var allFiles = await TaskSyncAllDataExcel(connectStr, domain, from, to, pageIndex, pageSize, progressIndicator, tokenSourceRedis.Token);
            }
            catch (OperationCanceledException)
            {
                //lblPercent.Text = @"Stop: ";
            }

            buttonStartRedis.Enabled = true;
            buttonCancelRedis.Enabled = false;
            timerClockRedis.Enabled = false;

            labelPercentRedis.Text = labelPercentRedis.Text + " -> DONE";
        }

        private void comboBoxNameSpace_SelectedIndexChanged(object sender, EventArgs e)
        {
            nameSpace = (NameSpaceEntity)(comboBoxNameSpace.Items[comboBoxNameSpace.SelectedIndex]);
            labelConnectStringSQL.Text = "Not ConnectStringSQL";
            labelConnectStringES.Text = "Not ConnectStringES";
            labelConnectStringREDIS.Text = "Not ConnectStringREDIS";
            GetAllConnectString();
        }

        private void iNITESIMS1ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var frm = new InitDataIMS1();
            frm.Show(this);
        }
    }
}
