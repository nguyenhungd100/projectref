﻿namespace IMS2.SYNCDATA
{
    partial class InitDataDb
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(InitDataDb));
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.treeViewDB = new System.Windows.Forms.TreeView();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.labelConnectStringSQL1 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.labelConnectStringSQL2 = new System.Windows.Forms.Label();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.groupBox9 = new System.Windows.Forms.GroupBox();
            this.buttonUpdateCount = new System.Windows.Forms.Button();
            this.label7 = new System.Windows.Forms.Label();
            this.maskedTextBoxToDate = new System.Windows.Forms.MaskedTextBox();
            this.maskedTextBoxFormDate = new System.Windows.Forms.MaskedTextBox();
            this.labelTotalIMS2 = new System.Windows.Forms.Label();
            this.labelTotalIMS1 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.labelKetQuaSyncByDate = new System.Windows.Forms.Label();
            this.buttonSyncByDate = new System.Windows.Forms.Button();
            this.label8 = new System.Windows.Forms.Label();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.labelKetQuaSyncTagIds = new System.Windows.Forms.Label();
            this.buttonSyncTagIds = new System.Windows.Forms.Button();
            this.textBoxTagIds = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.labelKetQuaSyncByNewsId = new System.Windows.Forms.Label();
            this.buttonSyncNowByNewsId = new System.Windows.Forms.Button();
            this.textBoxNewsId = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.textBoxPageSize = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.labelClock = new System.Windows.Forms.Label();
            this.textBoxPageIndex = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.labelTotalPage = new System.Windows.Forms.Label();
            this.comboBoxTable = new System.Windows.Forms.ComboBox();
            this.progressBar = new System.Windows.Forms.ProgressBar();
            this.labelPercent = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.buttonStart = new System.Windows.Forms.Button();
            this.buttonCancel = new System.Windows.Forms.Button();
            this.timerClock = new System.Windows.Forms.Timer(this.components);
            this.groupBox7 = new System.Windows.Forms.GroupBox();
            this.labelConnectStringREDIS = new System.Windows.Forms.Label();
            this.groupBox8 = new System.Windows.Forms.GroupBox();
            this.labelConnectStringES = new System.Windows.Forms.Label();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.groupBox3.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.groupBox9.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.groupBox6.SuspendLayout();
            this.groupBox7.SuspendLayout();
            this.groupBox8.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.treeViewDB);
            this.groupBox2.Location = new System.Drawing.Point(12, 12);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(339, 737);
            this.groupBox2.TabIndex = 10;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Connection DB";
            // 
            // treeViewDB
            // 
            this.treeViewDB.FullRowSelect = true;
            this.treeViewDB.Location = new System.Drawing.Point(20, 19);
            this.treeViewDB.Name = "treeViewDB";
            this.treeViewDB.ShowNodeToolTips = true;
            this.treeViewDB.Size = new System.Drawing.Size(293, 697);
            this.treeViewDB.TabIndex = 0;
            this.treeViewDB.NodeMouseClick += new System.Windows.Forms.TreeNodeMouseClickEventHandler(this.treeViewDB_NodeMouseClick);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(686, 62);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(45, 29);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 14;
            this.pictureBox1.TabStop = false;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.labelConnectStringSQL1);
            this.groupBox3.Location = new System.Drawing.Point(369, 12);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(883, 44);
            this.groupBox3.TabIndex = 13;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "IMS 1";
            // 
            // labelConnectStringSQL1
            // 
            this.labelConnectStringSQL1.AutoSize = true;
            this.labelConnectStringSQL1.Location = new System.Drawing.Point(25, 19);
            this.labelConnectStringSQL1.Name = "labelConnectStringSQL1";
            this.labelConnectStringSQL1.Size = new System.Drawing.Size(115, 13);
            this.labelConnectStringSQL1.TabIndex = 0;
            this.labelConnectStringSQL1.Text = "Not ConnectStringSQL";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.labelConnectStringSQL2);
            this.groupBox1.Location = new System.Drawing.Point(369, 97);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(883, 51);
            this.groupBox1.TabIndex = 15;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "IMS 2";
            // 
            // labelConnectStringSQL2
            // 
            this.labelConnectStringSQL2.AutoSize = true;
            this.labelConnectStringSQL2.Location = new System.Drawing.Point(25, 20);
            this.labelConnectStringSQL2.Name = "labelConnectStringSQL2";
            this.labelConnectStringSQL2.Size = new System.Drawing.Size(115, 13);
            this.labelConnectStringSQL2.TabIndex = 0;
            this.labelConnectStringSQL2.Text = "Not ConnectStringSQL";
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.groupBox9);
            this.groupBox4.Controls.Add(this.groupBox5);
            this.groupBox4.Controls.Add(this.groupBox6);
            this.groupBox4.Location = new System.Drawing.Point(369, 331);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(883, 418);
            this.groupBox4.TabIndex = 16;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Init Data";
            // 
            // groupBox9
            // 
            this.groupBox9.Controls.Add(this.buttonUpdateCount);
            this.groupBox9.Controls.Add(this.label7);
            this.groupBox9.Controls.Add(this.maskedTextBoxToDate);
            this.groupBox9.Controls.Add(this.maskedTextBoxFormDate);
            this.groupBox9.Controls.Add(this.labelTotalIMS2);
            this.groupBox9.Controls.Add(this.labelTotalIMS1);
            this.groupBox9.Controls.Add(this.label6);
            this.groupBox9.Controls.Add(this.labelKetQuaSyncByDate);
            this.groupBox9.Controls.Add(this.buttonSyncByDate);
            this.groupBox9.Controls.Add(this.label8);
            this.groupBox9.Location = new System.Drawing.Point(16, 299);
            this.groupBox9.Name = "groupBox9";
            this.groupBox9.Size = new System.Drawing.Size(861, 119);
            this.groupBox9.TabIndex = 13;
            this.groupBox9.TabStop = false;
            this.groupBox9.Text = "Init by thời gian";
            // 
            // buttonUpdateCount
            // 
            this.buttonUpdateCount.Location = new System.Drawing.Point(17, 48);
            this.buttonUpdateCount.Name = "buttonUpdateCount";
            this.buttonUpdateCount.Size = new System.Drawing.Size(102, 23);
            this.buttonUpdateCount.TabIndex = 13;
            this.buttonUpdateCount.Text = "View Count";
            this.buttonUpdateCount.UseVisualStyleBackColor = true;
            this.buttonUpdateCount.Click += new System.EventHandler(this.buttonUpdateCount_Click);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(413, 12);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(110, 13);
            this.label7.TabIndex = 12;
            this.label7.Text = "( MM-dd-yyyy HH:mm)";
            // 
            // maskedTextBoxToDate
            // 
            this.maskedTextBoxToDate.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.maskedTextBoxToDate.Location = new System.Drawing.Point(415, 64);
            this.maskedTextBoxToDate.Mask = "00-00-0000 90:00";
            this.maskedTextBoxToDate.Name = "maskedTextBoxToDate";
            this.maskedTextBoxToDate.Size = new System.Drawing.Size(225, 20);
            this.maskedTextBoxToDate.TabIndex = 11;
            // 
            // maskedTextBoxFormDate
            // 
            this.maskedTextBoxFormDate.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.maskedTextBoxFormDate.Location = new System.Drawing.Point(415, 30);
            this.maskedTextBoxFormDate.Mask = "00-00-0000 90:00";
            this.maskedTextBoxFormDate.Name = "maskedTextBoxFormDate";
            this.maskedTextBoxFormDate.Size = new System.Drawing.Size(225, 20);
            this.maskedTextBoxFormDate.TabIndex = 10;
            // 
            // labelTotalIMS2
            // 
            this.labelTotalIMS2.AutoSize = true;
            this.labelTotalIMS2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelTotalIMS2.Location = new System.Drawing.Point(129, 73);
            this.labelTotalIMS2.Name = "labelTotalIMS2";
            this.labelTotalIMS2.Size = new System.Drawing.Size(119, 13);
            this.labelTotalIMS2.TabIndex = 9;
            this.labelTotalIMS2.Text = "Tổng số tin IMS2: 0";
            // 
            // labelTotalIMS1
            // 
            this.labelTotalIMS1.AutoSize = true;
            this.labelTotalIMS1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelTotalIMS1.Location = new System.Drawing.Point(129, 33);
            this.labelTotalIMS1.Name = "labelTotalIMS1";
            this.labelTotalIMS1.Size = new System.Drawing.Size(119, 13);
            this.labelTotalIMS1.TabIndex = 8;
            this.labelTotalIMS1.Text = "Tổng số tin IMS1: 0";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(347, 67);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(26, 13);
            this.label6.TabIndex = 7;
            this.label6.Text = "To: ";
            // 
            // labelKetQuaSyncByDate
            // 
            this.labelKetQuaSyncByDate.AutoSize = true;
            this.labelKetQuaSyncByDate.ForeColor = System.Drawing.SystemColors.Highlight;
            this.labelKetQuaSyncByDate.Location = new System.Drawing.Point(418, 92);
            this.labelKetQuaSyncByDate.Name = "labelKetQuaSyncByDate";
            this.labelKetQuaSyncByDate.Size = new System.Drawing.Size(0, 13);
            this.labelKetQuaSyncByDate.TabIndex = 5;
            // 
            // buttonSyncByDate
            // 
            this.buttonSyncByDate.Location = new System.Drawing.Point(660, 48);
            this.buttonSyncByDate.Name = "buttonSyncByDate";
            this.buttonSyncByDate.Size = new System.Drawing.Size(144, 23);
            this.buttonSyncByDate.TabIndex = 4;
            this.buttonSyncByDate.Text = "Sync Now";
            this.buttonSyncByDate.UseVisualStyleBackColor = true;
            this.buttonSyncByDate.Click += new System.EventHandler(this.buttonSyncByDate_Click);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(347, 32);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(36, 13);
            this.label8.TabIndex = 2;
            this.label8.Text = "From: ";
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.labelKetQuaSyncTagIds);
            this.groupBox5.Controls.Add(this.buttonSyncTagIds);
            this.groupBox5.Controls.Add(this.textBoxTagIds);
            this.groupBox5.Controls.Add(this.label9);
            this.groupBox5.Controls.Add(this.label1);
            this.groupBox5.Controls.Add(this.labelKetQuaSyncByNewsId);
            this.groupBox5.Controls.Add(this.buttonSyncNowByNewsId);
            this.groupBox5.Controls.Add(this.textBoxNewsId);
            this.groupBox5.Controls.Add(this.label5);
            this.groupBox5.Location = new System.Drawing.Point(570, 30);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(307, 253);
            this.groupBox5.TabIndex = 12;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Init simple by NewsId";
            // 
            // labelKetQuaSyncTagIds
            // 
            this.labelKetQuaSyncTagIds.AutoSize = true;
            this.labelKetQuaSyncTagIds.Location = new System.Drawing.Point(71, 206);
            this.labelKetQuaSyncTagIds.Name = "labelKetQuaSyncTagIds";
            this.labelKetQuaSyncTagIds.Size = new System.Drawing.Size(40, 13);
            this.labelKetQuaSyncTagIds.TabIndex = 10;
            this.labelKetQuaSyncTagIds.Text = "Sync...";
            // 
            // buttonSyncTagIds
            // 
            this.buttonSyncTagIds.Location = new System.Drawing.Point(106, 223);
            this.buttonSyncTagIds.Name = "buttonSyncTagIds";
            this.buttonSyncTagIds.Size = new System.Drawing.Size(144, 23);
            this.buttonSyncTagIds.TabIndex = 9;
            this.buttonSyncTagIds.Text = "Sync Now";
            this.buttonSyncTagIds.UseVisualStyleBackColor = true;
            this.buttonSyncTagIds.Click += new System.EventHandler(this.buttonSyncTagIds_Click);
            // 
            // textBoxTagIds
            // 
            this.textBoxTagIds.Location = new System.Drawing.Point(68, 143);
            this.textBoxTagIds.Multiline = true;
            this.textBoxTagIds.Name = "textBoxTagIds";
            this.textBoxTagIds.Size = new System.Drawing.Size(225, 59);
            this.textBoxTagIds.TabIndex = 8;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(13, 144);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(46, 13);
            this.label9.TabIndex = 7;
            this.label9.Text = "TagIds: ";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(142, 10);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(148, 13);
            this.label1.TabIndex = 6;
            this.label1.Text = "nhập newsid cách nhau dấu ,";
            // 
            // labelKetQuaSyncByNewsId
            // 
            this.labelKetQuaSyncByNewsId.AutoSize = true;
            this.labelKetQuaSyncByNewsId.Location = new System.Drawing.Point(71, 90);
            this.labelKetQuaSyncByNewsId.Name = "labelKetQuaSyncByNewsId";
            this.labelKetQuaSyncByNewsId.Size = new System.Drawing.Size(40, 13);
            this.labelKetQuaSyncByNewsId.TabIndex = 5;
            this.labelKetQuaSyncByNewsId.Text = "Sync...";
            // 
            // buttonSyncNowByNewsId
            // 
            this.buttonSyncNowByNewsId.Location = new System.Drawing.Point(106, 109);
            this.buttonSyncNowByNewsId.Name = "buttonSyncNowByNewsId";
            this.buttonSyncNowByNewsId.Size = new System.Drawing.Size(144, 23);
            this.buttonSyncNowByNewsId.TabIndex = 4;
            this.buttonSyncNowByNewsId.Text = "Sync Now";
            this.buttonSyncNowByNewsId.UseVisualStyleBackColor = true;
            this.buttonSyncNowByNewsId.Click += new System.EventHandler(this.buttonSyncNowByNewsId_Click);
            // 
            // textBoxNewsId
            // 
            this.textBoxNewsId.Location = new System.Drawing.Point(68, 28);
            this.textBoxNewsId.Multiline = true;
            this.textBoxNewsId.Name = "textBoxNewsId";
            this.textBoxNewsId.Size = new System.Drawing.Size(225, 59);
            this.textBoxNewsId.TabIndex = 3;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(13, 29);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(54, 13);
            this.label5.TabIndex = 2;
            this.label5.Text = "NewsIds: ";
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this.textBoxPageSize);
            this.groupBox6.Controls.Add(this.label3);
            this.groupBox6.Controls.Add(this.labelClock);
            this.groupBox6.Controls.Add(this.textBoxPageIndex);
            this.groupBox6.Controls.Add(this.label2);
            this.groupBox6.Controls.Add(this.labelTotalPage);
            this.groupBox6.Controls.Add(this.comboBoxTable);
            this.groupBox6.Controls.Add(this.progressBar);
            this.groupBox6.Controls.Add(this.labelPercent);
            this.groupBox6.Controls.Add(this.label4);
            this.groupBox6.Controls.Add(this.buttonStart);
            this.groupBox6.Controls.Add(this.buttonCancel);
            this.groupBox6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox6.Location = new System.Drawing.Point(16, 30);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(539, 253);
            this.groupBox6.TabIndex = 10;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "Init IMS1 to IMS2 by table";
            // 
            // textBoxPageSize
            // 
            this.textBoxPageSize.Location = new System.Drawing.Point(133, 109);
            this.textBoxPageSize.Name = "textBoxPageSize";
            this.textBoxPageSize.Size = new System.Drawing.Size(149, 20);
            this.textBoxPageSize.TabIndex = 5;
            this.textBoxPageSize.Text = "1000";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(72, 112);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(59, 13);
            this.label3.TabIndex = 4;
            this.label3.Text = "Page size: ";
            // 
            // labelClock
            // 
            this.labelClock.AutoSize = true;
            this.labelClock.Location = new System.Drawing.Point(477, 189);
            this.labelClock.Name = "labelClock";
            this.labelClock.Size = new System.Drawing.Size(49, 13);
            this.labelClock.TabIndex = 11;
            this.labelClock.Text = "00:00:00";
            // 
            // textBoxPageIndex
            // 
            this.textBoxPageIndex.Location = new System.Drawing.Point(133, 71);
            this.textBoxPageIndex.Name = "textBoxPageIndex";
            this.textBoxPageIndex.Size = new System.Drawing.Size(149, 20);
            this.textBoxPageIndex.TabIndex = 3;
            this.textBoxPageIndex.Text = "1";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(65, 74);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(66, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Page index: ";
            // 
            // labelTotalPage
            // 
            this.labelTotalPage.AutoSize = true;
            this.labelTotalPage.Location = new System.Drawing.Point(5, 164);
            this.labelTotalPage.Name = "labelTotalPage";
            this.labelTotalPage.Size = new System.Drawing.Size(70, 13);
            this.labelTotalPage.TabIndex = 8;
            this.labelTotalPage.Text = "Total page: 0";
            // 
            // comboBoxTable
            // 
            this.comboBoxTable.FormattingEnabled = true;
            this.comboBoxTable.Location = new System.Drawing.Point(132, 30);
            this.comboBoxTable.Name = "comboBoxTable";
            this.comboBoxTable.Size = new System.Drawing.Size(150, 21);
            this.comboBoxTable.TabIndex = 1;
            // 
            // progressBar
            // 
            this.progressBar.Location = new System.Drawing.Point(98, 159);
            this.progressBar.Name = "progressBar";
            this.progressBar.Size = new System.Drawing.Size(428, 23);
            this.progressBar.TabIndex = 0;
            // 
            // labelPercent
            // 
            this.labelPercent.AutoSize = true;
            this.labelPercent.Location = new System.Drawing.Point(98, 189);
            this.labelPercent.Name = "labelPercent";
            this.labelPercent.Size = new System.Drawing.Size(13, 13);
            this.labelPercent.TabIndex = 1;
            this.labelPercent.Text = "0";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(84, 33);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(45, 13);
            this.label4.TabIndex = 0;
            this.label4.Text = "Tables: ";
            // 
            // buttonStart
            // 
            this.buttonStart.Location = new System.Drawing.Point(186, 210);
            this.buttonStart.Name = "buttonStart";
            this.buttonStart.Size = new System.Drawing.Size(105, 23);
            this.buttonStart.TabIndex = 2;
            this.buttonStart.Text = "Start Init";
            this.buttonStart.UseVisualStyleBackColor = true;
            this.buttonStart.Click += new System.EventHandler(this.buttonStart_Click);
            // 
            // buttonCancel
            // 
            this.buttonCancel.Location = new System.Drawing.Point(307, 210);
            this.buttonCancel.Name = "buttonCancel";
            this.buttonCancel.Size = new System.Drawing.Size(97, 23);
            this.buttonCancel.TabIndex = 3;
            this.buttonCancel.Text = "Stop";
            this.buttonCancel.UseVisualStyleBackColor = true;
            this.buttonCancel.Click += new System.EventHandler(this.buttonCancel_Click);
            // 
            // groupBox7
            // 
            this.groupBox7.Controls.Add(this.labelConnectStringREDIS);
            this.groupBox7.Location = new System.Drawing.Point(369, 250);
            this.groupBox7.Name = "groupBox7";
            this.groupBox7.Size = new System.Drawing.Size(883, 53);
            this.groupBox7.TabIndex = 17;
            this.groupBox7.TabStop = false;
            this.groupBox7.Text = "Đang kết nối tới REDIS";
            // 
            // labelConnectStringREDIS
            // 
            this.labelConnectStringREDIS.AutoSize = true;
            this.labelConnectStringREDIS.Location = new System.Drawing.Point(25, 24);
            this.labelConnectStringREDIS.Name = "labelConnectStringREDIS";
            this.labelConnectStringREDIS.Size = new System.Drawing.Size(127, 13);
            this.labelConnectStringREDIS.TabIndex = 0;
            this.labelConnectStringREDIS.Text = "Not ConnectStringREDIS";
            // 
            // groupBox8
            // 
            this.groupBox8.Controls.Add(this.labelConnectStringES);
            this.groupBox8.Location = new System.Drawing.Point(369, 181);
            this.groupBox8.Name = "groupBox8";
            this.groupBox8.Size = new System.Drawing.Size(883, 50);
            this.groupBox8.TabIndex = 18;
            this.groupBox8.TabStop = false;
            this.groupBox8.Text = "Đang kết nối tới ES";
            // 
            // labelConnectStringES
            // 
            this.labelConnectStringES.AutoSize = true;
            this.labelConnectStringES.Location = new System.Drawing.Point(25, 24);
            this.labelConnectStringES.Name = "labelConnectStringES";
            this.labelConnectStringES.Size = new System.Drawing.Size(108, 13);
            this.labelConnectStringES.TabIndex = 0;
            this.labelConnectStringES.Text = "Not ConnectStringES";
            // 
            // pictureBox2
            // 
            this.pictureBox2.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox2.Image")));
            this.pictureBox2.Location = new System.Drawing.Point(686, 152);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(45, 29);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox2.TabIndex = 19;
            this.pictureBox2.TabStop = false;
            // 
            // InitDataDb
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1264, 761);
            this.Controls.Add(this.pictureBox2);
            this.Controls.Add(this.groupBox7);
            this.Controls.Add(this.groupBox8);
            this.Controls.Add(this.groupBox4);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(1280, 800);
            this.MinimumSize = new System.Drawing.Size(1280, 800);
            this.Name = "InitDataDb";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "InitDataDb";
            this.Load += new System.EventHandler(this.InitDataDb_Load);
            this.groupBox2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox9.ResumeLayout(false);
            this.groupBox9.PerformLayout();
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            this.groupBox6.ResumeLayout(false);
            this.groupBox6.PerformLayout();
            this.groupBox7.ResumeLayout(false);
            this.groupBox7.PerformLayout();
            this.groupBox8.ResumeLayout(false);
            this.groupBox8.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.TreeView treeViewDB;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Label labelConnectStringSQL1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label labelConnectStringSQL2;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.TextBox textBoxPageSize;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox textBoxPageIndex;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox comboBoxTable;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label labelTotalPage;
        private System.Windows.Forms.ProgressBar progressBar;
        private System.Windows.Forms.Label labelPercent;
        private System.Windows.Forms.Button buttonStart;
        private System.Windows.Forms.Button buttonCancel;
        private System.Windows.Forms.Label labelClock;
        private System.Windows.Forms.Timer timerClock;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.TextBox textBoxNewsId;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button buttonSyncNowByNewsId;
        private System.Windows.Forms.Label labelKetQuaSyncByNewsId;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox groupBox7;
        private System.Windows.Forms.Label labelConnectStringREDIS;
        private System.Windows.Forms.GroupBox groupBox8;
        private System.Windows.Forms.Label labelConnectStringES;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.GroupBox groupBox9;
        private System.Windows.Forms.Label labelKetQuaSyncByDate;
        private System.Windows.Forms.Button buttonSyncByDate;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label labelTotalIMS2;
        private System.Windows.Forms.Label labelTotalIMS1;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.MaskedTextBox maskedTextBoxFormDate;
        private System.Windows.Forms.MaskedTextBox maskedTextBoxToDate;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Button buttonUpdateCount;
        private System.Windows.Forms.Button buttonSyncTagIds;
        private System.Windows.Forms.TextBox textBoxTagIds;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label labelKetQuaSyncTagIds;
    }
}