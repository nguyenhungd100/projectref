﻿using IMS2.SYNCDATA.Common;
using IMS2.SYNCDATA.Entity;
using IMS2.SYNCDATA.SQL;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace IMS2.SYNCDATA
{
    public partial class InitDataDb : Form
    {
        private CancellationTokenSource tokenSource;
        private int ss = 0;
        private int mm = 0;
        private int hh = 0;

        public InitDataDb()
        {
            InitializeComponent();
        }

        private void InitDataDb_Load(object sender, EventArgs e)
        {            
            progressBar.Minimum = 0;
            progressBar.Maximum = 100;
            buttonCancel.Enabled = false;

            //Get Connect to config
            GetAllConnectString();

            //Get table
            GetAllTables();

            maskedTextBoxFormDate.Text = DateTime.Now.AddMinutes(-1).ToString("MM-dd-yyyy HH:mm");
            maskedTextBoxToDate.Text = DateTime.Now.ToString("MM-dd-yyyy HH:mm");
        }

        #region Init
        private void GetAllConnectString()
        {
            var valueSql1 = ConfigurationManager.AppSettings["ListConnectString.SQL.IMS1"];
            if (!string.IsNullOrEmpty(valueSql1))
            {
                var listSql = JsonConvert.DeserializeObject<List<ConnectEntity>>(valueSql1);
                var root = new TreeNode("SQL DB IMS 1");
                if (listSql != null && listSql.Count > 0)
                {
                    foreach (var item in listSql)
                    {
                        var node = new TreeNode(item.Name);
                        node.ToolTipText = item.ConnectString;
                        root.Nodes.Add(node);
                    }
                }
                treeViewDB.Nodes.Add(root);
            }

            var valueSql2 = ConfigurationManager.AppSettings["ListConnectString.SQL"];
            if (!string.IsNullOrEmpty(valueSql2))
            {
                var listSql = JsonConvert.DeserializeObject<List<ConnectEntity>>(valueSql2);
                var root = new TreeNode("SQL DB IMS 2");
                if (listSql != null && listSql.Count > 0)
                {
                    foreach (var item in listSql)
                    {
                        var node = new TreeNode(item.Name);
                        node.ToolTipText = item.ConnectString;
                        root.Nodes.Add(node);
                    }
                }
                treeViewDB.Nodes.Add(root);
            }

            var valueEs = ConfigurationManager.AppSettings["ListConnectString.ES"];
            if (!string.IsNullOrEmpty(valueEs))
            {
                var listEs = JsonConvert.DeserializeObject<List<ConnectEntity>>(valueEs);
                var rootEs = new TreeNode("ES DB");
                if (listEs != null && listEs.Count > 0)
                {
                    foreach (var item in listEs)
                    {
                        var node = new TreeNode(item.Name);
                        node.ToolTipText = item.ConnectString;
                        rootEs.Nodes.Add(node);
                    }
                }
                treeViewDB.Nodes.Add(rootEs);
            }

            var valueRedis = ConfigurationManager.AppSettings["ListConnectString.REDIS"];
            if (!string.IsNullOrEmpty(valueRedis))
            {
                var listRedis = JsonConvert.DeserializeObject<List<ConnectEntity>>(valueRedis);
                var rootRedis = new TreeNode("REDIS DB");
                if (listRedis != null && listRedis.Count > 0)
                {
                    foreach (var item in listRedis)
                    {
                        var node = new TreeNode(item.Name);
                        node.ToolTipText = item.ConnectString;
                        rootRedis.Nodes.Add(node);
                    }
                }
                treeViewDB.Nodes.Add(rootRedis);
            }

            treeViewDB.ExpandAll();
        }

        private void GetAllTables()
        {
            var tables = new List<TableEntity> {
                new TableEntity {Name="News",Value="news" },
                new TableEntity {Name="NewsPublish",Value="newspublish" },
                new TableEntity {Name="NewsRelation",Value="newsrelation" },
                new TableEntity {Name="NewsContent",Value="newscontent" },
                new TableEntity {Name="NewsInZone",Value="newsinzone" },
                new TableEntity {Name="NewsExtension",Value="newsextension" },
                new TableEntity {Name="NewsPosition",Value="newsposition" },
                new TableEntity {Name="NewsPr",Value="newspr" },
                new TableEntity {Name="Zone",Value="zone" },
                new TableEntity {Name="User",Value="user" },
                new TableEntity {Name="UserProfile",Value="userprofile" },
                new TableEntity {Name="UserPermission",Value="userpermission" },
                new TableEntity {Name="GroupPermission",Value="grouppermission" },
                new TableEntity {Name="Permission",Value="permission" },
                new TableEntity {Name="Tag",Value="tag" },
                new TableEntity {Name="TagNews",Value="tagnews" },
                new TableEntity {Name="TagZone",Value="tagzone" },
                new TableEntity {Name="Thread",Value="thread" },                
                new TableEntity {Name="ThreadInZone",Value="threadinzone" },
                new TableEntity {Name="ThreadNews",Value="threadnews" },
                new TableEntity {Name="ThreadRelation",Value="threadrelation" },
                new TableEntity {Name="Topic",Value="topic" },
                new TableEntity {Name="TopicInZone",Value="topicinzone" },
                new TableEntity {Name="NewsInTopic",Value="newsintopic" },
                new TableEntity {Name="Video",Value="video" },
                new TableEntity {Name="VideoInZone",Value="videoinzone" },
                new TableEntity {Name="ZoneVideo",Value="zonevideo" },
                new TableEntity {Name="VideoTag",Value="videotag" },
                new TableEntity {Name="VideoInTag",Value="videointag" },
                new TableEntity {Name="VideoTags",Value="videotags" },
                new TableEntity {Name="MenuExtension",Value="menuextension" },
                new TableEntity {Name="SEOMetaNews",Value="seometanews" },
                new TableEntity {Name="SEOMetaVideo",Value="seometavideo" },
                new TableEntity {Name="SEOTagZone",Value="seotagzone" }
                
            };
            comboBoxTable.DataSource = tables;
            comboBoxTable.DisplayMember = "Name";
            comboBoxTable.ValueMember = "Value";
        }

        private void CountNewsIMS1IMS2()
        {
            var connectStr1 = labelConnectStringSQL1.Text;
            if (connectStr1 == "Not ConnectStringSQL" || string.IsNullOrEmpty(connectStr1))
            {
                MessageBox.Show("Bạn chưa chọn connect tới SQL IMS 1");
                return;
            }
            var connectStr2 = labelConnectStringSQL2.Text;
            if (connectStr2 == "Not ConnectStringSQL" || string.IsNullOrEmpty(connectStr2))
            {
                MessageBox.Show("Bạn chưa chọn connect tới SQL IMS 2");
                return;
            }

            var count1 = NewsBo.GetCountNews(connectStr1);
            var count2 = NewsBo.GetCountNews(connectStr2);
            labelTotalIMS1.Text = "Tổng số tin IMS1: "+ count1;
            labelTotalIMS2.Text = "Tổng số tin IMS2: "+ count2;
        }
        #endregion

        #region event
        private async void buttonStart_Click(object sender, EventArgs e)
        {
            var connectStr1 = labelConnectStringSQL1.Text;
            if (connectStr1 == "Not ConnectStringSQL" || string.IsNullOrEmpty(connectStr1))
            {
                MessageBox.Show("Bạn chưa chọn connect tới SQL IMS 1");
                return;
            }
            var connectStr2 = labelConnectStringSQL2.Text;
            if (connectStr2 == "Not ConnectStringSQL" || string.IsNullOrEmpty(connectStr2))
            {
                MessageBox.Show("Bạn chưa chọn connect tới SQL IMS 2");
                return;
            }

            var pageIndex = 1;
            if (!string.IsNullOrEmpty(textBoxPageIndex.Text))
            {
                pageIndex = Convert.ToInt32(textBoxPageIndex.Text);
            }
            var pageSize = 1000;
            if (!string.IsNullOrEmpty(textBoxPageSize.Text))
            {
                pageSize = Convert.ToInt32(textBoxPageSize.Text);
            }

            var table = comboBoxTable.SelectedValue.ToString();

            var progressIndicator = new Progress<ProcessEntity>(UpdateProgress);

            buttonStart.Enabled = false;
            buttonCancel.Enabled = true;
            labelPercent.Text = @"Page: 0";
            progressBar.Value = 0;
            tokenSource = new CancellationTokenSource();

            var nameSpace = "";

            labelClock.Text = "00:00:00";
            ss = 0; mm = 0; hh = 0;
            timerClock.Interval = 1000;
            timerClock.Enabled = true;
            timerClock.Tick += new EventHandler(TimerClock_Tick);

            try
            {
                var allFiles = await TaskSyncAllData(nameSpace, connectStr1, connectStr2, table, pageIndex, pageSize, progressIndicator, tokenSource.Token);
            }
            catch (OperationCanceledException)
            {
                //lblPercent.Text = @"Stop: ";
            }

            buttonStart.Enabled = true;
            buttonCancel.Enabled = false;
            timerClock.Enabled = false;

            labelPercent.Text = labelPercent.Text + " -> DONE";
        }
        
        private void TimerClock_Tick(object Sender, EventArgs e)
        {
            ss++;
            if (ss == 60) {
                ss = 0;
                mm = mm + 1;
            };
            if (mm == 60)
            {
                mm = 0;
                hh = hh + 1;
            }
                        
            var ssTemp = ss.ToString();
            if (ss < 10)
            {
                ssTemp = "0" + ss;
            }
            var mmTemp = mm.ToString();
            if (mm < 10)
            {
                mmTemp = "0" + mm;
            }
            var hhTemp = hh.ToString();
            if (hh < 10)
            {
                hhTemp = "0" + hh;
            }            
            
            labelClock.Text = hhTemp + ":" + mmTemp + ":" + ssTemp;
        }

        private async Task<int> TaskSyncAllData(string nameSpace, string connectString1, string connectString2, string table, int pageIndex, int pageSize, IProgress<ProcessEntity> progress, CancellationToken token)
        {
            var countAll = GetCountAll(table, connectString1);
            if (countAll == 0)
            {
                MessageBox.Show("Table không được hỗ trợ sync OR Không tìm thấy bản ghi nào của Table: " + table);
                return 0;
            }
            if (countAll == 1)
            {
                MessageBox.Show("Không mở được connect tới sql server");
                return 0;
            }

            var totalCount = (int)Math.Ceiling((double)countAll / pageSize);
            progressBar.Maximum = totalCount;
            labelTotalPage.Text = @"Total page: " + totalCount;

            var processCount = await Task.Run(() =>
            {
                var processEntity = new ProcessEntity();
                for (var page = pageIndex; page <= totalCount; page++)
                {
                    processEntity.page = page;
                    processEntity.value = SyncDataToPage(nameSpace, connectString1, connectString2, page, pageSize, table, progress, processEntity);                    

                    token.ThrowIfCancellationRequested();

                    //if (progress != null)
                    //{
                    //    //progress.Report(tempCount * 100 / totalCount);
                    //    progress.Report(processEntity);
                    //}
                }

                return processEntity.page;

            }, token);

            return processCount;
        }

        private int GetCountAll(string table, string connectString)
        {
            int count = 0;
            switch (table)
            {
                case "news": count = NewsBo.GetCountNews(connectString); break;
                case "newspublish": count = NewsBo.GetCountNewsPublish(connectString); break;

                case "newsrelation": count = NewsBo.GetCountNewsRelation(connectString); break;
                case "newscontent": count = NewsBo.GetCountNewsContent(connectString); break;
                case "newspr": count = NewsBo.GetCountNewsPr(connectString); break;

                case "newsinzone": count = NewsBo.GetCountNewsInZone(connectString); break;
                case "newsextension": count = NewsBo.GetCountNewsExtension(connectString); break;
                case "newsposition": count = NewsBo.GetCountNewsPosition(connectString); break;
                case "zone": count = ZoneBo.GetCountZone(connectString); break;
                case "user": count = UserBo.GetCountUser(connectString); break;
                case "userprofile": count = UserBo.GetCountUserProfile(connectString); break;
                case "userpermission": count = UserBo.GetCountUserPermission(connectString); break;
                case "grouppermission": count = UserBo.GetCountGroupPermission(connectString); break;
                case "permission": count = UserBo.GetCountPermission(connectString); break;
                case "tag": count = TagBo.GetCountTag(connectString); break;
                case "tagnews": count = TagBo.GetCountTagNews(connectString); break;
                case "tagzone": count = TagBo.GetCountTagZone(connectString); break;
                case "thread": count = ThreadBo.GetCountThread(connectString); break;
                case "threadinzone": count = ThreadBo.GetCountThreadInZone(connectString); break;                
                case "threadnews": count = ThreadBo.GetCountThreadNews(connectString); break;
                case "threadrelation": count = ThreadBo.GetCountThreadRelation(connectString); break;
                case "video": count = VideoBo.GetCountVideo(connectString); break;

                case "topic": count = TopicBo.GetCountTopic(connectString); break;
                case "topicinzone": count = TopicBo.GetCountTopicInZone(connectString); break;
                case "newsintopic": count = TopicBo.GetCountNewsInTopic(connectString); break;

                case "videoinzone": count = VideoBo.GetCountVideoInZone(connectString); break;
                case "zonevideo": count = VideoBo.GetCountZoneVideo(connectString); break;
                case "videotag": count = VideoBo.GetCountVideoTag(connectString); break;
                case "videointag": count = VideoBo.GetCountVideoInTag(connectString); break;
                case "videotags": count = VideoBo.GetCountVideoTags(connectString); break;
                case "menuextension": count = NewsBo.GetCountMenuExtension(connectString); break;

                case "seometanews": count = NewsBo.GetCountSEOMetaNews(connectString); break;
                case "seometavideo": count = NewsBo.GetCountSEOMetaVideo(connectString); break;
                case "seotagzone": count = NewsBo.GetCountSEOTagZone(connectString); break;
            }

            return count;
        }

        private int SyncDataToPage(string nameSpace, string connectString1, string connectString2, int page, int pageSize, string table, IProgress<ProcessEntity> progress, ProcessEntity processEntity)
        {
            var count = 0;
            switch (table)
            {
                case "news": count = NewsBo.SyncNewsToDb(nameSpace, connectString1, connectString2, page, pageSize, progress, processEntity); break;
                case "newspublish": count = NewsBo.SyncNewsPublishToDb(nameSpace, connectString1, connectString2, page, pageSize, progress, processEntity); break;

                case "newsrelation": count = NewsBo.SyncNewsRelationToDb(nameSpace, connectString1, connectString2, page, pageSize, progress, processEntity); break;
                case "newscontent": count = NewsBo.SyncNewsContentToDb(nameSpace, connectString1, connectString2, page, pageSize, progress, processEntity); break;
                case "newspr": count = NewsBo.SyncNewsPrToDb(nameSpace, connectString1, connectString2, page, pageSize, progress, processEntity); break;

                case "newsinzone": count = NewsBo.SyncNewsInZoneToDb(nameSpace, connectString1, connectString2, page, pageSize, progress, processEntity); break;
                case "newsextension": count = NewsBo.SyncNewsExtensionToDb(nameSpace, connectString1, connectString2, page, pageSize, progress, processEntity); break;
                case "newsposition": NewsBo.SyncNewsPositionToDb(nameSpace, connectString1, connectString2, page, pageSize, progress, processEntity); break;
                case "zone": count = ZoneBo.SyncZoneToDb(nameSpace, connectString1, connectString2, page, pageSize, progress, processEntity); break;
                case "user": UserBo.SyncUserToDb(nameSpace, connectString1, connectString2, page, pageSize, progress, processEntity); break;
                case "userprofile": UserBo.SyncUserProfileToDb(nameSpace, connectString1, connectString2, page, pageSize, progress, processEntity); break;
                case "userpermission": UserBo.SyncUserPermissionToDb(nameSpace, connectString1, connectString2, page, pageSize, progress, processEntity); break;
                case "grouppermission": UserBo.SyncGroupPermissionToDb(nameSpace, connectString1, connectString2, page, pageSize, progress, processEntity); break;
                case "permission": UserBo.SyncPermissionToDb(nameSpace, connectString1, connectString2, page, pageSize, progress, processEntity); break;
                case "tag": TagBo.SyncTagToDb(nameSpace, connectString1, connectString2, page, pageSize, progress, processEntity); break;
                case "tagnews": TagBo.SyncTagNewsToDb(nameSpace, connectString1, connectString2, page, pageSize, progress, processEntity); break;
                case "tagzone": TagBo.SyncTagZoneToDb(nameSpace, connectString1, connectString2, page, pageSize, progress, processEntity); break;
                case "thread": ThreadBo.SyncThreadToDb(nameSpace, connectString1, connectString2, page, pageSize, progress, processEntity); break;
                case "threadinzone": ThreadBo.SyncThreadInZoneToDb(nameSpace, connectString1, connectString2, page, pageSize, progress, processEntity); break;                
                case "threadnews": ThreadBo.SyncThreadNewsToDb(nameSpace, connectString1, connectString2, page, pageSize, progress, processEntity); break;
                case "threadrelation": ThreadBo.SyncThreadRelationToDb(nameSpace, connectString1, connectString2, page, pageSize, progress, processEntity); break;

                case "topic": TopicBo.SyncTopicToDb(nameSpace, connectString1, connectString2, page, pageSize, progress, processEntity); break;
                case "topicinzone": TopicBo.SyncTopicInZoneToDb(nameSpace, connectString1, connectString2, page, pageSize, progress, processEntity); break;
                case "newsintopic": TopicBo.SyncNewsInTopicToDb(nameSpace, connectString1, connectString2, page, pageSize, progress, processEntity); break;

                case "video": VideoBo.SyncVideoToDb(nameSpace, connectString1, connectString2, page, pageSize, progress, processEntity); break;
                case "videoinzone": VideoBo.SyncVideoInZoneToDb(nameSpace, connectString1, connectString2, page, pageSize, progress, processEntity); break;
                case "zonevideo": VideoBo.SyncZoneVideoToDb(nameSpace, connectString1, connectString2, page, pageSize, progress, processEntity); break;
                case "videotag": VideoBo.SyncVideoTagToDb(nameSpace, connectString1, connectString2, page, pageSize, progress, processEntity); break;
                case "videointag": VideoBo.SyncVideoInTagToDb(nameSpace, connectString1, connectString2, page, pageSize, progress, processEntity); break;
                case "videotags": VideoBo.SyncVideoTagsToDb(nameSpace, connectString1, connectString2, page, pageSize, progress, processEntity); break;

                case "menuextension": count = NewsBo.SyncMenuExtensionToDb(nameSpace, connectString1, connectString2, page, pageSize, progress, processEntity); break;

                case "seometanews": count = NewsBo.SyncSEOMetaNewsToDb(nameSpace, connectString1, connectString2, page, pageSize, progress, processEntity); break;
                case "seometavideo": count = NewsBo.SyncSEOMetaVideoToDb(nameSpace, connectString1, connectString2, page, pageSize, progress, processEntity); break;
                case "seotagzone": count = NewsBo.SyncSEOTagZoneToDb(nameSpace, connectString1, connectString2, page, pageSize, progress, processEntity); break;
            }
            return count;
        }

        private void UpdateProgress(ProcessEntity obj)
        {
            progressBar.Value = obj.page;
            labelPercent.Text = "Đang sync page: " + obj.page.ToString() +" ------> Running...: "+ obj.value.ToString();
        }

        private void treeViewDB_NodeMouseClick(object sender, TreeNodeMouseClickEventArgs e)
        {
            var node = e.Node;
            if (node != null && node.Level > 0)
            {
                var decryptKey = ConfigurationManager.AppSettings["ConnectionDecryptKey"];

                if (node.Parent.Text == "SQL DB IMS 1")
                {
                    labelConnectStringSQL1.Text = Crypton.DecryptByKey(node.ToolTipText, decryptKey) == string.Empty ? "Not Decrypt Key ConnectString" : Crypton.DecryptByKey(node.ToolTipText, decryptKey);
                }
                if (node.Parent.Text == "SQL DB IMS 2")
                {
                    labelConnectStringSQL2.Text = Crypton.DecryptByKey(node.ToolTipText, decryptKey) == string.Empty ? "Not Decrypt Key ConnectString" : Crypton.DecryptByKey(node.ToolTipText, decryptKey);
                }
                if (node.Parent.Text == "ES DB")
                {
                    var ar = node.ToolTipText.Split('|');
                    labelConnectStringES.Text = ar[0] + "|" + (Crypton.DecryptByKey(ar[1], decryptKey) == string.Empty ? "Not Decrypt Key ConnectString" : Crypton.DecryptByKey(ar[1], decryptKey));
                }
                if (node.Parent.Text == "REDIS DB")
                {
                    var ar = node.ToolTipText.Split('|');
                    labelConnectStringREDIS.Text = ar[0] + "|" + (Crypton.DecryptByKey(ar[1], decryptKey) == string.Empty ? "Not Decrypt Key ConnectString" : Crypton.DecryptByKey(ar[1], decryptKey));
                }
            }
        }

        private void buttonCancel_Click(object sender, EventArgs e)
        {
            buttonCancel.Enabled = false;
            buttonStart.Enabled = false;
            timerClock.Enabled = false;

            tokenSource.Cancel();
        }
        #endregion

        #region object
        public class ProcessEntity
        {
            public int page { get; set; }
            public int value { get; set; }
        }
        #endregion

        #region NewsId
        private async void buttonSyncNowByNewsId_Click(object sender, EventArgs e)
        {
            var connectStr1 = labelConnectStringSQL1.Text;
            if (connectStr1 == "Not ConnectStringSQL" || string.IsNullOrEmpty(connectStr1))
            {
                MessageBox.Show("Bạn chưa chọn connect tới SQL IMS 1");
                return;
            }
            var connectStr2 = labelConnectStringSQL2.Text;
            if (connectStr2 == "Not ConnectStringSQL" || string.IsNullOrEmpty(connectStr2))
            {
                MessageBox.Show("Bạn chưa chọn connect tới SQL IMS 2");
                return;
            }
            var connectE = labelConnectStringES.Text;
            if (connectE == "Not ConnectStringES" || string.IsNullOrEmpty(connectE))
            {
                MessageBox.Show("Bạn chưa chọn connect tới ES");
                return;
            }
            var connectEs = connectE.Split('|')[1];
            var nameSpaceEs = connectE.Split('|')[0];
            if (string.IsNullOrEmpty(connectEs) || connectEs == "Not Decrypt Key ConnectString" || string.IsNullOrEmpty(nameSpaceEs))
            {
                MessageBox.Show("Kiểm tra lại connect tới ES và NameSpace");
                return;
            }
            var connectR = labelConnectStringREDIS.Text;
            if (connectR == "Not ConnectStringREDIS" || string.IsNullOrEmpty(connectR))
            {
                MessageBox.Show("Bạn chưa chọn connect tới REDIS");
                return;
            }
            var connectRedis = connectR.Split('|')[1];
            var nameSpaceRedis = connectR.Split('|')[0];
            if (string.IsNullOrEmpty(connectRedis) || connectRedis == "Not Decrypt Key ConnectString" || string.IsNullOrEmpty(nameSpaceRedis))
            {
                MessageBox.Show("kiểm tra lại connect tới REDIS và NameSpace");
                return;
            }

            if (string.IsNullOrEmpty(textBoxNewsId.Text))
            {
                MessageBox.Show("Bạn chưa nhập NewsId cần sync.");
                return;                
            }
            var newsIds = textBoxNewsId.Text;

            labelKetQuaSyncByNewsId.Text = "Đang Sync...";
            var progressIndicator = new Progress<ProcessEntity>(UpdateProgressByNewsId);
            try
            {
                await TaskSyncByNewsId(connectStr1, connectStr2, newsIds, nameSpaceEs, connectEs, connectRedis, progressIndicator);
            }
            catch (OperationCanceledException)
            {
                labelKetQuaSyncByNewsId.Text = " Sync-> Error!";
            }

            labelKetQuaSyncByNewsId.Text = " Sync-> DONE";
        }

        private async Task<int> TaskSyncByNewsId(string connectString1, string connectString2, string newsIds, string nameSpaceEs, string connectEs, string connectRedis, IProgress<ProcessEntity> progressIndicator)
        {
            int count = 0;
            var processCount = await Task.Run(() =>
            {               
                var data = NewsBo.GetDataNews(connectString1, newsIds);
                
                if (data != null && data.Count > 0)
                {
                    count = NewsBo.SyncDataNews(connectString1, connectString2, data, nameSpaceEs, connectEs, connectRedis, progressIndicator);
                }

                return count;
            });

            return processCount;
        }

        private void UpdateProgressByNewsId(ProcessEntity obj)
        {
            labelKetQuaSyncByNewsId.Text = "Đang sync...: " + obj.value.ToString() + "/" + obj.page.ToString();
        }
        #endregion

        #region Date
        private async void buttonSyncByDate_Click(object sender, EventArgs e)
        {
            var connectStr1 = labelConnectStringSQL1.Text;
            if (connectStr1 == "Not ConnectStringSQL" || string.IsNullOrEmpty(connectStr1))
            {
                MessageBox.Show("Bạn chưa chọn connect tới SQL IMS 1");
                return;
            }
            var connectStr2 = labelConnectStringSQL2.Text;
            if (connectStr2 == "Not ConnectStringSQL" || string.IsNullOrEmpty(connectStr2))
            {
                MessageBox.Show("Bạn chưa chọn connect tới SQL IMS 2");
                return;
            }
            var connectE = labelConnectStringES.Text;
            if (connectE == "Not ConnectStringES" || string.IsNullOrEmpty(connectE))
            {
                MessageBox.Show("Bạn chưa chọn connect tới ES");
                return;
            }
            var connectEs = connectE.Split('|')[1];
            var nameSpaceEs = connectE.Split('|')[0];
            if (string.IsNullOrEmpty(connectEs) || connectEs == "Not Decrypt Key ConnectString" || string.IsNullOrEmpty(nameSpaceEs))
            {
                MessageBox.Show("Kiểm tra lại connect tới ES và NameSpace");
                return;
            }
            var connectR = labelConnectStringREDIS.Text;
            if (connectR == "Not ConnectStringREDIS" || string.IsNullOrEmpty(connectR))
            {
                MessageBox.Show("Bạn chưa chọn connect tới REDIS");
                return;
            }
            var connectRedis = connectR.Split('|')[1];
            var nameSpaceRedis = connectR.Split('|')[0];
            if (string.IsNullOrEmpty(connectRedis) || connectRedis == "Not Decrypt Key ConnectString" || string.IsNullOrEmpty(nameSpaceRedis))
            {
                MessageBox.Show("kiểm tra lại connect tới REDIS và NameSpace");
                return;
            }

            DateTime rs;
            if (!maskedTextBoxFormDate.MaskFull || !DateTime.TryParseExact(maskedTextBoxFormDate.Text, "MM-dd-yyyy HH:mm", new CultureInfo("en-US"), DateTimeStyles.None, out rs))
            {
                MessageBox.Show("Bạn chưa nhập đúng định dạng FormDate.");
                return;
            }
            var from = maskedTextBoxFormDate.Text;
            
            if (!maskedTextBoxToDate.MaskFull || !DateTime.TryParseExact(maskedTextBoxToDate.Text, "MM-dd-yyyy HH:mm", new CultureInfo("en-US"), DateTimeStyles.None, out rs))
            {
                MessageBox.Show("Bạn chưa nhập đúng định dạng ToDate.");
                return;
            }
            var to = maskedTextBoxToDate.Text;

            labelKetQuaSyncByDate.Text = "Đang Sync...";

            var progressIndicator = new Progress<ProcessEntity>(UpdateProgressByDate);

            try
            {
                await TaskSyncByDate(connectStr1, connectStr2, from, to, nameSpaceEs, connectEs, connectRedis, progressIndicator);
            }
            catch (OperationCanceledException)
            {
                labelKetQuaSyncByDate.Text = " Sync-> Error!";
            }
            
            labelKetQuaSyncByDate.Text = " Sync-> DONE";
        }

        private async Task<int> TaskSyncByDate(string connectString1, string connectString2, string from, string to, string nameSpaceEs, string connectEs, string connectRedis, IProgress<ProcessEntity> progressIndicator)
        {
            int count = 0;
            var processCount = await Task.Run(() =>
            {
                var data = NewsBo.GetDataNewsByDate(connectString1, from, to);

                if (data != null && data.Count > 0)
                {
                    count = NewsBo.SyncDataNews(connectString1, connectString2, data, nameSpaceEs, connectEs, connectRedis, progressIndicator);
                }

                return count;
            });

            return processCount;
        }

        private void UpdateProgressByDate(ProcessEntity obj)
        {            
            labelKetQuaSyncByDate.Text = "Đang sync...: " + obj.value.ToString() +"/"+ obj.page.ToString();
        }       

        private void buttonUpdateCount_Click(object sender, EventArgs e)
        {
            //Count News IMS1 - IMS2
            CountNewsIMS1IMS2();
        }
        #endregion

        #region TagId
        private async void buttonSyncTagIds_Click(object sender, EventArgs e)
        {
            var connectStr1 = labelConnectStringSQL1.Text;
            if (connectStr1 == "Not ConnectStringSQL" || string.IsNullOrEmpty(connectStr1))
            {
                MessageBox.Show("Bạn chưa chọn connect tới SQL IMS 1");
                return;
            }
            var connectStr2 = labelConnectStringSQL2.Text;
            if (connectStr2 == "Not ConnectStringSQL" || string.IsNullOrEmpty(connectStr2))
            {
                MessageBox.Show("Bạn chưa chọn connect tới SQL IMS 2");
                return;
            }
            var connectE = labelConnectStringES.Text;
            if (connectE == "Not ConnectStringES" || string.IsNullOrEmpty(connectE))
            {
                MessageBox.Show("Bạn chưa chọn connect tới ES");
                return;
            }
            var connectEs = connectE.Split('|')[1];
            var nameSpaceEs = connectE.Split('|')[0];
            if (string.IsNullOrEmpty(connectEs) || connectEs == "Not Decrypt Key ConnectString" || string.IsNullOrEmpty(nameSpaceEs))
            {
                MessageBox.Show("Kiểm tra lại connect tới ES và NameSpace");
                return;
            }
            var connectR = labelConnectStringREDIS.Text;
            if (connectR == "Not ConnectStringREDIS" || string.IsNullOrEmpty(connectR))
            {
                MessageBox.Show("Bạn chưa chọn connect tới REDIS");
                return;
            }
            var connectRedis = connectR.Split('|')[1];
            var nameSpaceRedis = connectR.Split('|')[0];
            if (string.IsNullOrEmpty(connectRedis) || connectRedis == "Not Decrypt Key ConnectString" || string.IsNullOrEmpty(nameSpaceRedis))
            {
                MessageBox.Show("kiểm tra lại connect tới REDIS và NameSpace");
                return;
            }

            if (string.IsNullOrEmpty(textBoxTagIds.Text))
            {
                MessageBox.Show("Bạn chưa nhập TagIds cần sync.");
                return;
            }
            var tagIds = textBoxTagIds.Text;

            labelKetQuaSyncTagIds.Text = "Đang Sync...";
            var progressIndicator = new Progress<ProcessEntity>(UpdateProgressByTagId);
            try
            {
                await TaskSyncByTagId(connectStr1, connectStr2, tagIds, nameSpaceEs, connectEs, connectRedis, progressIndicator);
            }
            catch (OperationCanceledException)
            {
                labelKetQuaSyncTagIds.Text = " Sync-> Error!";
            }

            labelKetQuaSyncTagIds.Text = " Sync-> DONE";
        }

        private async Task<int> TaskSyncByTagId(string connectString1, string connectString2, string tagIds, string nameSpaceEs, string connectEs, string connectRedis, IProgress<ProcessEntity> progressIndicator)
        {
            int count = 0;
            var processCount = await Task.Run(() =>
            {
                var data = TagBo.GetDataTag(connectString1, tagIds);

                if (data != null && data.Count > 0)
                {
                    count = TagBo.SyncDataTag(connectString1, connectString2, data, nameSpaceEs, connectEs, connectRedis, progressIndicator);
                }

                return count;
            });

            return processCount;
        }

        private void UpdateProgressByTagId(ProcessEntity obj)
        {
            labelKetQuaSyncTagIds.Text = "Đang sync...: " + obj.value.ToString() + "/" + obj.page.ToString();
        }
        #endregion
    }
}
