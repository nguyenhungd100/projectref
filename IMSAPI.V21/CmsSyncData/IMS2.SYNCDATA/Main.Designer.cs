﻿namespace IMS2.SYNCDATA
{
    partial class Main
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Main));
            this.progressBarEs = new System.Windows.Forms.ProgressBar();
            this.labelPercentEs = new System.Windows.Forms.Label();
            this.buttonStartEs = new System.Windows.Forms.Button();
            this.buttonCancelEs = new System.Windows.Forms.Button();
            this.buttonCancelRedis = new System.Windows.Forms.Button();
            this.buttonStartRedis = new System.Windows.Forms.Button();
            this.labelPercentRedis = new System.Windows.Forms.Label();
            this.progressBarRedis = new System.Windows.Forms.ProgressBar();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.buttonExportExcel = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.labelClockRedis = new System.Windows.Forms.Label();
            this.labelClockEs = new System.Windows.Forms.Label();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.checkBoxIsDelete = new System.Windows.Forms.CheckBox();
            this.label7 = new System.Windows.Forms.Label();
            this.maskedTextBoxToDate = new System.Windows.Forms.MaskedTextBox();
            this.maskedTextBoxFormDate = new System.Windows.Forms.MaskedTextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.textBoxPageSize = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.textBoxPageIndex = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.comboBoxTable = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.labelTotalPageRedis = new System.Windows.Forms.Label();
            this.labelTotalPageES = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.treeViewDB = new System.Windows.Forms.TreeView();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.labelConnectStringSQL = new System.Windows.Forms.Label();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.labelConnectStringES = new System.Windows.Forms.Label();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.labelConnectStringREDIS = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.menuToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.genConnectStringToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.iNITIMS1TOIMS2ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.timerClockEs = new System.Windows.Forms.Timer(this.components);
            this.timerClockRedis = new System.Windows.Forms.Timer(this.components);
            this.comboBoxNameSpace = new System.Windows.Forms.ComboBox();
            this.iNITESIMS1ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.groupBox1.SuspendLayout();
            this.groupBox6.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.groupBox5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // progressBarEs
            // 
            this.progressBarEs.Location = new System.Drawing.Point(135, 212);
            this.progressBarEs.Name = "progressBarEs";
            this.progressBarEs.Size = new System.Drawing.Size(554, 23);
            this.progressBarEs.TabIndex = 0;
            // 
            // labelPercentEs
            // 
            this.labelPercentEs.AutoSize = true;
            this.labelPercentEs.Location = new System.Drawing.Point(135, 242);
            this.labelPercentEs.Name = "labelPercentEs";
            this.labelPercentEs.Size = new System.Drawing.Size(13, 13);
            this.labelPercentEs.TabIndex = 1;
            this.labelPercentEs.Text = "0";
            // 
            // buttonStartEs
            // 
            this.buttonStartEs.Location = new System.Drawing.Point(329, 271);
            this.buttonStartEs.Name = "buttonStartEs";
            this.buttonStartEs.Size = new System.Drawing.Size(105, 23);
            this.buttonStartEs.TabIndex = 2;
            this.buttonStartEs.Text = "Start Sync ES";
            this.buttonStartEs.UseVisualStyleBackColor = true;
            this.buttonStartEs.Click += new System.EventHandler(this.buttonStartEs_Click);
            // 
            // buttonCancelEs
            // 
            this.buttonCancelEs.Location = new System.Drawing.Point(450, 271);
            this.buttonCancelEs.Name = "buttonCancelEs";
            this.buttonCancelEs.Size = new System.Drawing.Size(97, 23);
            this.buttonCancelEs.TabIndex = 3;
            this.buttonCancelEs.Text = "Stop";
            this.buttonCancelEs.UseVisualStyleBackColor = true;
            this.buttonCancelEs.Click += new System.EventHandler(this.buttonCancel_Click);
            // 
            // buttonCancelRedis
            // 
            this.buttonCancelRedis.Location = new System.Drawing.Point(450, 375);
            this.buttonCancelRedis.Name = "buttonCancelRedis";
            this.buttonCancelRedis.Size = new System.Drawing.Size(97, 23);
            this.buttonCancelRedis.TabIndex = 7;
            this.buttonCancelRedis.Text = "Stop";
            this.buttonCancelRedis.UseVisualStyleBackColor = true;
            this.buttonCancelRedis.Click += new System.EventHandler(this.buttonCancelRedis_Click);
            // 
            // buttonStartRedis
            // 
            this.buttonStartRedis.Location = new System.Drawing.Point(329, 375);
            this.buttonStartRedis.Name = "buttonStartRedis";
            this.buttonStartRedis.Size = new System.Drawing.Size(105, 23);
            this.buttonStartRedis.TabIndex = 6;
            this.buttonStartRedis.Text = "Start Sync REDIS";
            this.buttonStartRedis.UseVisualStyleBackColor = true;
            this.buttonStartRedis.Click += new System.EventHandler(this.buttonStartRedis_Click);
            // 
            // labelPercentRedis
            // 
            this.labelPercentRedis.AutoSize = true;
            this.labelPercentRedis.Location = new System.Drawing.Point(135, 353);
            this.labelPercentRedis.Name = "labelPercentRedis";
            this.labelPercentRedis.Size = new System.Drawing.Size(13, 13);
            this.labelPercentRedis.TabIndex = 5;
            this.labelPercentRedis.Text = "0";
            // 
            // progressBarRedis
            // 
            this.progressBarRedis.Location = new System.Drawing.Point(135, 323);
            this.progressBarRedis.Name = "progressBarRedis";
            this.progressBarRedis.Size = new System.Drawing.Size(554, 23);
            this.progressBarRedis.TabIndex = 4;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.buttonExportExcel);
            this.groupBox1.Controls.Add(this.button1);
            this.groupBox1.Controls.Add(this.labelClockRedis);
            this.groupBox1.Controls.Add(this.labelClockEs);
            this.groupBox1.Controls.Add(this.groupBox6);
            this.groupBox1.Controls.Add(this.labelTotalPageRedis);
            this.groupBox1.Controls.Add(this.labelTotalPageES);
            this.groupBox1.Controls.Add(this.progressBarEs);
            this.groupBox1.Controls.Add(this.buttonCancelRedis);
            this.groupBox1.Controls.Add(this.labelPercentEs);
            this.groupBox1.Controls.Add(this.buttonStartRedis);
            this.groupBox1.Controls.Add(this.buttonStartEs);
            this.groupBox1.Controls.Add(this.labelPercentRedis);
            this.groupBox1.Controls.Add(this.buttonCancelEs);
            this.groupBox1.Controls.Add(this.progressBarRedis);
            this.groupBox1.Location = new System.Drawing.Point(403, 328);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(769, 424);
            this.groupBox1.TabIndex = 8;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Sync Data";
            // 
            // buttonExportExcel
            // 
            this.buttonExportExcel.Location = new System.Drawing.Point(563, 375);
            this.buttonExportExcel.Name = "buttonExportExcel";
            this.buttonExportExcel.Size = new System.Drawing.Size(97, 23);
            this.buttonExportExcel.TabIndex = 14;
            this.buttonExportExcel.Text = "Export Excel";
            this.buttonExportExcel.UseVisualStyleBackColor = true;
            this.buttonExportExcel.Click += new System.EventHandler(this.buttonExportExcel_Click);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(217, 375);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(97, 23);
            this.button1.TabIndex = 13;
            this.button1.Text = "Update Body";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // labelClockRedis
            // 
            this.labelClockRedis.AutoSize = true;
            this.labelClockRedis.Location = new System.Drawing.Point(640, 353);
            this.labelClockRedis.Name = "labelClockRedis";
            this.labelClockRedis.Size = new System.Drawing.Size(49, 13);
            this.labelClockRedis.TabIndex = 12;
            this.labelClockRedis.Text = "00:00:00";
            // 
            // labelClockEs
            // 
            this.labelClockEs.AutoSize = true;
            this.labelClockEs.Location = new System.Drawing.Point(640, 242);
            this.labelClockEs.Name = "labelClockEs";
            this.labelClockEs.Size = new System.Drawing.Size(49, 13);
            this.labelClockEs.TabIndex = 11;
            this.labelClockEs.Text = "00:00:00";
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this.checkBoxIsDelete);
            this.groupBox6.Controls.Add(this.label7);
            this.groupBox6.Controls.Add(this.maskedTextBoxToDate);
            this.groupBox6.Controls.Add(this.maskedTextBoxFormDate);
            this.groupBox6.Controls.Add(this.label6);
            this.groupBox6.Controls.Add(this.label8);
            this.groupBox6.Controls.Add(this.textBoxPageSize);
            this.groupBox6.Controls.Add(this.label3);
            this.groupBox6.Controls.Add(this.textBoxPageIndex);
            this.groupBox6.Controls.Add(this.label2);
            this.groupBox6.Controls.Add(this.comboBoxTable);
            this.groupBox6.Controls.Add(this.label1);
            this.groupBox6.Location = new System.Drawing.Point(32, 30);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(711, 149);
            this.groupBox6.TabIndex = 10;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "Input";
            // 
            // checkBoxIsDelete
            // 
            this.checkBoxIsDelete.AutoSize = true;
            this.checkBoxIsDelete.Location = new System.Drawing.Point(470, 107);
            this.checkBoxIsDelete.Name = "checkBoxIsDelete";
            this.checkBoxIsDelete.Size = new System.Drawing.Size(97, 17);
            this.checkBoxIsDelete.TabIndex = 18;
            this.checkBoxIsDelete.Text = "Is Delete Index";
            this.checkBoxIsDelete.UseVisualStyleBackColor = true;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(467, 16);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(110, 13);
            this.label7.TabIndex = 17;
            this.label7.Text = "( MM-dd-yyyy HH:mm)";
            // 
            // maskedTextBoxToDate
            // 
            this.maskedTextBoxToDate.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.maskedTextBoxToDate.Location = new System.Drawing.Point(469, 68);
            this.maskedTextBoxToDate.Mask = "00-00-0000 90:00";
            this.maskedTextBoxToDate.Name = "maskedTextBoxToDate";
            this.maskedTextBoxToDate.Size = new System.Drawing.Size(225, 20);
            this.maskedTextBoxToDate.TabIndex = 16;
            // 
            // maskedTextBoxFormDate
            // 
            this.maskedTextBoxFormDate.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.maskedTextBoxFormDate.Location = new System.Drawing.Point(469, 34);
            this.maskedTextBoxFormDate.Mask = "00-00-0000 90:00";
            this.maskedTextBoxFormDate.Name = "maskedTextBoxFormDate";
            this.maskedTextBoxFormDate.Size = new System.Drawing.Size(225, 20);
            this.maskedTextBoxFormDate.TabIndex = 15;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(401, 71);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(26, 13);
            this.label6.TabIndex = 14;
            this.label6.Text = "To: ";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(401, 36);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(36, 13);
            this.label8.TabIndex = 13;
            this.label8.Text = "From: ";
            // 
            // textBoxPageSize
            // 
            this.textBoxPageSize.Location = new System.Drawing.Point(133, 109);
            this.textBoxPageSize.Name = "textBoxPageSize";
            this.textBoxPageSize.Size = new System.Drawing.Size(149, 20);
            this.textBoxPageSize.TabIndex = 5;
            this.textBoxPageSize.Text = "1000";
            this.textBoxPageSize.TextChanged += new System.EventHandler(this.textBoxPageSize_TextChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(72, 112);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(59, 13);
            this.label3.TabIndex = 4;
            this.label3.Text = "Page size: ";
            // 
            // textBoxPageIndex
            // 
            this.textBoxPageIndex.Location = new System.Drawing.Point(133, 71);
            this.textBoxPageIndex.Name = "textBoxPageIndex";
            this.textBoxPageIndex.Size = new System.Drawing.Size(149, 20);
            this.textBoxPageIndex.TabIndex = 3;
            this.textBoxPageIndex.Text = "1";
            this.textBoxPageIndex.TextChanged += new System.EventHandler(this.textBoxPageIndex_TextChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(65, 74);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(66, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Page index: ";
            // 
            // comboBoxTable
            // 
            this.comboBoxTable.FormattingEnabled = true;
            this.comboBoxTable.Location = new System.Drawing.Point(132, 30);
            this.comboBoxTable.Name = "comboBoxTable";
            this.comboBoxTable.Size = new System.Drawing.Size(150, 21);
            this.comboBoxTable.TabIndex = 1;
            this.comboBoxTable.SelectedIndexChanged += new System.EventHandler(this.comboBoxTable_SelectedIndexChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(84, 33);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(45, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Tables: ";
            // 
            // labelTotalPageRedis
            // 
            this.labelTotalPageRedis.AutoSize = true;
            this.labelTotalPageRedis.Location = new System.Drawing.Point(29, 329);
            this.labelTotalPageRedis.Name = "labelTotalPageRedis";
            this.labelTotalPageRedis.Size = new System.Drawing.Size(70, 13);
            this.labelTotalPageRedis.TabIndex = 9;
            this.labelTotalPageRedis.Text = "Total page: 0";
            // 
            // labelTotalPageES
            // 
            this.labelTotalPageES.AutoSize = true;
            this.labelTotalPageES.Location = new System.Drawing.Point(29, 217);
            this.labelTotalPageES.Name = "labelTotalPageES";
            this.labelTotalPageES.Size = new System.Drawing.Size(70, 13);
            this.labelTotalPageES.TabIndex = 8;
            this.labelTotalPageES.Text = "Total page: 0";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.treeViewDB);
            this.groupBox2.Location = new System.Drawing.Point(12, 64);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(339, 688);
            this.groupBox2.TabIndex = 9;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Connection DB";
            // 
            // treeViewDB
            // 
            this.treeViewDB.FullRowSelect = true;
            this.treeViewDB.Location = new System.Drawing.Point(20, 19);
            this.treeViewDB.Name = "treeViewDB";
            this.treeViewDB.ShowNodeToolTips = true;
            this.treeViewDB.Size = new System.Drawing.Size(293, 658);
            this.treeViewDB.TabIndex = 0;
            this.treeViewDB.NodeMouseClick += new System.Windows.Forms.TreeNodeMouseClickEventHandler(this.treeViewDB_NodeMouseClick);
            this.treeViewDB.NodeMouseDoubleClick += new System.Windows.Forms.TreeNodeMouseClickEventHandler(this.treeViewDB_NodeMouseDoubleClick);
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.labelConnectStringSQL);
            this.groupBox3.Location = new System.Drawing.Point(403, 64);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(769, 49);
            this.groupBox3.TabIndex = 10;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Đang kết nối tới SQL";
            // 
            // labelConnectStringSQL
            // 
            this.labelConnectStringSQL.AutoSize = true;
            this.labelConnectStringSQL.Location = new System.Drawing.Point(25, 23);
            this.labelConnectStringSQL.Name = "labelConnectStringSQL";
            this.labelConnectStringSQL.Size = new System.Drawing.Size(115, 13);
            this.labelConnectStringSQL.TabIndex = 0;
            this.labelConnectStringSQL.Text = "Not ConnectStringSQL";
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.labelConnectStringES);
            this.groupBox4.Location = new System.Drawing.Point(403, 153);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(769, 50);
            this.groupBox4.TabIndex = 11;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Đang kết nối tới ES";
            // 
            // labelConnectStringES
            // 
            this.labelConnectStringES.AutoSize = true;
            this.labelConnectStringES.Location = new System.Drawing.Point(25, 24);
            this.labelConnectStringES.Name = "labelConnectStringES";
            this.labelConnectStringES.Size = new System.Drawing.Size(108, 13);
            this.labelConnectStringES.TabIndex = 0;
            this.labelConnectStringES.Text = "Not ConnectStringES";
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.labelConnectStringREDIS);
            this.groupBox5.Location = new System.Drawing.Point(403, 228);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(769, 53);
            this.groupBox5.TabIndex = 11;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Đang kết nối tới REDIS";
            // 
            // labelConnectStringREDIS
            // 
            this.labelConnectStringREDIS.AutoSize = true;
            this.labelConnectStringREDIS.Location = new System.Drawing.Point(25, 24);
            this.labelConnectStringREDIS.Name = "labelConnectStringREDIS";
            this.labelConnectStringREDIS.Size = new System.Drawing.Size(127, 13);
            this.labelConnectStringREDIS.TabIndex = 0;
            this.labelConnectStringREDIS.Text = "Not ConnectStringREDIS";
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(792, 120);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(45, 29);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 12;
            this.pictureBox1.TabStop = false;
            // 
            // pictureBox2
            // 
            this.pictureBox2.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox2.Image")));
            this.pictureBox2.Location = new System.Drawing.Point(792, 294);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(45, 29);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox2.TabIndex = 13;
            this.pictureBox2.TabStop = false;
            // 
            // menuStrip1
            // 
            this.menuStrip1.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.menuStrip1.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menuToolStripMenuItem,
            this.iNITIMS1TOIMS2ToolStripMenuItem,
            this.iNITESIMS1ToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(1184, 28);
            this.menuStrip1.TabIndex = 14;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // menuToolStripMenuItem
            // 
            this.menuToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.genConnectStringToolStripMenuItem,
            this.exitToolStripMenuItem});
            this.menuToolStripMenuItem.Name = "menuToolStripMenuItem";
            this.menuToolStripMenuItem.Size = new System.Drawing.Size(58, 24);
            this.menuToolStripMenuItem.Text = "Menu";
            // 
            // genConnectStringToolStripMenuItem
            // 
            this.genConnectStringToolStripMenuItem.Name = "genConnectStringToolStripMenuItem";
            this.genConnectStringToolStripMenuItem.Size = new System.Drawing.Size(201, 24);
            this.genConnectStringToolStripMenuItem.Text = "Gen connect string";
            this.genConnectStringToolStripMenuItem.Click += new System.EventHandler(this.genConnectStringToolStripMenuItem_Click);
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(201, 24);
            this.exitToolStripMenuItem.Text = "Exit";
            this.exitToolStripMenuItem.Click += new System.EventHandler(this.exitToolStripMenuItem_Click);
            // 
            // iNITIMS1TOIMS2ToolStripMenuItem
            // 
            this.iNITIMS1TOIMS2ToolStripMenuItem.Name = "iNITIMS1TOIMS2ToolStripMenuItem";
            this.iNITIMS1TOIMS2ToolStripMenuItem.Size = new System.Drawing.Size(109, 24);
            this.iNITIMS1TOIMS2ToolStripMenuItem.Text = "INIT DB IMS2";
            this.iNITIMS1TOIMS2ToolStripMenuItem.Click += new System.EventHandler(this.iNITIMS1TOIMS2ToolStripMenuItem_Click);
            // 
            // comboBoxNameSpace
            // 
            this.comboBoxNameSpace.FormattingEnabled = true;
            this.comboBoxNameSpace.Location = new System.Drawing.Point(976, 37);
            this.comboBoxNameSpace.Name = "comboBoxNameSpace";
            this.comboBoxNameSpace.Size = new System.Drawing.Size(196, 21);
            this.comboBoxNameSpace.TabIndex = 15;
            this.comboBoxNameSpace.SelectedIndexChanged += new System.EventHandler(this.comboBoxNameSpace_SelectedIndexChanged);
            // 
            // iNITESIMS1ToolStripMenuItem
            // 
            this.iNITESIMS1ToolStripMenuItem.Name = "iNITESIMS1ToolStripMenuItem";
            this.iNITESIMS1ToolStripMenuItem.Size = new System.Drawing.Size(105, 24);
            this.iNITESIMS1ToolStripMenuItem.Text = "INIT ES IMS1";
            this.iNITESIMS1ToolStripMenuItem.Click += new System.EventHandler(this.iNITESIMS1ToolStripMenuItem_Click);
            // 
            // Main
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1184, 765);
            this.Controls.Add(this.comboBoxNameSpace);
            this.Controls.Add(this.pictureBox2);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.groupBox5);
            this.Controls.Add(this.groupBox4);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.MaximizeBox = false;
            this.Name = "Main";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "IMS2 SYNC DATA BY CHINHNB";
            this.Load += new System.EventHandler(this.Main_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox6.ResumeLayout(false);
            this.groupBox6.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ProgressBar progressBarEs;
        private System.Windows.Forms.Label labelPercentEs;
        private System.Windows.Forms.Button buttonStartEs;
        private System.Windows.Forms.Button buttonCancelEs;
        private System.Windows.Forms.Button buttonCancelRedis;
        private System.Windows.Forms.Button buttonStartRedis;
        private System.Windows.Forms.Label labelPercentRedis;
        private System.Windows.Forms.ProgressBar progressBarRedis;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.TreeView treeViewDB;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Label labelConnectStringSQL;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.Label labelConnectStringES;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.Label labelConnectStringREDIS;
        private System.Windows.Forms.Label labelTotalPageES;
        private System.Windows.Forms.Label labelTotalPageRedis;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox comboBoxTable;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox textBoxPageSize;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox textBoxPageIndex;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem menuToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem genConnectStringToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem iNITIMS1TOIMS2ToolStripMenuItem;
        private System.Windows.Forms.Timer timerClockEs;
        private System.Windows.Forms.Timer timerClockRedis;
        private System.Windows.Forms.Label labelClockEs;
        private System.Windows.Forms.Label labelClockRedis;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.MaskedTextBox maskedTextBoxToDate;
        private System.Windows.Forms.MaskedTextBox maskedTextBoxFormDate;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.CheckBox checkBoxIsDelete;
        private System.Windows.Forms.Button buttonExportExcel;
        private System.Windows.Forms.ComboBox comboBoxNameSpace;
        private System.Windows.Forms.ToolStripMenuItem iNITESIMS1ToolStripMenuItem;
    }
}

