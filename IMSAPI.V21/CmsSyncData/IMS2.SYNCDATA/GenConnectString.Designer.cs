﻿namespace IMS2.SYNCDATA
{
    partial class GenConnectString
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.textBoxConnectString = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.buttonEncrypt = new System.Windows.Forms.Button();
            this.buttonDecrypt = new System.Windows.Forms.Button();
            this.textBoxResult = new System.Windows.Forms.TextBox();
            this.buttonMD5 = new System.Windows.Forms.Button();
            this.buttonCreatedIndexEs = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.textBoxSecretKey = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // textBoxConnectString
            // 
            this.textBoxConnectString.Location = new System.Drawing.Point(112, 13);
            this.textBoxConnectString.Name = "textBoxConnectString";
            this.textBoxConnectString.Size = new System.Drawing.Size(750, 20);
            this.textBoxConnectString.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(26, 16);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(80, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "ConnectString: ";
            // 
            // buttonEncrypt
            // 
            this.buttonEncrypt.Location = new System.Drawing.Point(272, 77);
            this.buttonEncrypt.Name = "buttonEncrypt";
            this.buttonEncrypt.Size = new System.Drawing.Size(146, 23);
            this.buttonEncrypt.TabIndex = 2;
            this.buttonEncrypt.Text = "Encrypt";
            this.buttonEncrypt.UseVisualStyleBackColor = true;
            this.buttonEncrypt.Click += new System.EventHandler(this.buttonEncrypt_Click);
            // 
            // buttonDecrypt
            // 
            this.buttonDecrypt.Location = new System.Drawing.Point(443, 77);
            this.buttonDecrypt.Name = "buttonDecrypt";
            this.buttonDecrypt.Size = new System.Drawing.Size(146, 23);
            this.buttonDecrypt.TabIndex = 3;
            this.buttonDecrypt.Text = "Decrypt";
            this.buttonDecrypt.UseVisualStyleBackColor = true;
            this.buttonDecrypt.Click += new System.EventHandler(this.buttonDecrypt_Click);
            // 
            // textBoxResult
            // 
            this.textBoxResult.Location = new System.Drawing.Point(112, 109);
            this.textBoxResult.Multiline = true;
            this.textBoxResult.Name = "textBoxResult";
            this.textBoxResult.Size = new System.Drawing.Size(750, 240);
            this.textBoxResult.TabIndex = 4;
            // 
            // buttonMD5
            // 
            this.buttonMD5.Location = new System.Drawing.Point(716, 44);
            this.buttonMD5.Name = "buttonMD5";
            this.buttonMD5.Size = new System.Drawing.Size(146, 23);
            this.buttonMD5.TabIndex = 5;
            this.buttonMD5.Text = "MD5";
            this.buttonMD5.UseVisualStyleBackColor = true;
            this.buttonMD5.Click += new System.EventHandler(this.buttonMD5_Click);
            // 
            // buttonCreatedIndexEs
            // 
            this.buttonCreatedIndexEs.Location = new System.Drawing.Point(716, 77);
            this.buttonCreatedIndexEs.Name = "buttonCreatedIndexEs";
            this.buttonCreatedIndexEs.Size = new System.Drawing.Size(146, 23);
            this.buttonCreatedIndexEs.TabIndex = 6;
            this.buttonCreatedIndexEs.Text = "Tạo Index ES";
            this.buttonCreatedIndexEs.UseVisualStyleBackColor = true;
            this.buttonCreatedIndexEs.Click += new System.EventHandler(this.buttonCreatedIndexEs_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(26, 47);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(56, 13);
            this.label2.TabIndex = 8;
            this.label2.Text = "SecretKey";
            // 
            // textBoxSecretKey
            // 
            this.textBoxSecretKey.Location = new System.Drawing.Point(112, 44);
            this.textBoxSecretKey.Name = "textBoxSecretKey";
            this.textBoxSecretKey.Size = new System.Drawing.Size(586, 20);
            this.textBoxSecretKey.TabIndex = 7;
            // 
            // GenConnectString
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(874, 361);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.textBoxSecretKey);
            this.Controls.Add(this.buttonCreatedIndexEs);
            this.Controls.Add(this.buttonMD5);
            this.Controls.Add(this.textBoxResult);
            this.Controls.Add(this.buttonDecrypt);
            this.Controls.Add(this.buttonEncrypt);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.textBoxConnectString);
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(890, 400);
            this.MinimumSize = new System.Drawing.Size(890, 400);
            this.Name = "GenConnectString";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Gen Connect String";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox textBoxConnectString;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button buttonEncrypt;
        private System.Windows.Forms.Button buttonDecrypt;
        private System.Windows.Forms.TextBox textBoxResult;
        private System.Windows.Forms.Button buttonMD5;
        private System.Windows.Forms.Button buttonCreatedIndexEs;
        private System.Windows.Forms.TextBox textBoxSecretKey;
        private System.Windows.Forms.Label label2;
    }
}