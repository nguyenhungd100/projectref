﻿using IMS2.SYNCDATA.Common;
using System;
using System.Runtime.Serialization;

namespace IMS2.SYNCDATA.Entity
{
    [DataContract]
    public class ThreadInZoneEntity : EntityBase
    {
        [DataMember]
        public int ZoneId { get; set; }
        [DataMember]
        public int ThreadId { get; set; }        
        [DataMember]
        public DateTime CreatedDate { get; set; }
        [DataMember]
        public bool IsPrimary { get; set; }
        [DataMember]
        public int Ordinary { get; set; }
    }

    [DataContract]
    public class ThreadNewsEntity : EntityBase
    {
        [DataMember]
        public int ThreadId { get; set; }
        [DataMember]
        public long NewsId { get; set; }                
        [DataMember]
        public int Ordinary { get; set; }
        [DataMember]
        public DateTime LastModifiedDate { get; set; }
    }

    [DataContract]
    public class ThreadRelationEntity : EntityBase
    {
        [DataMember]
        public int ThreadId { get; set; }
        [DataMember]
        public int ThreadRelationId { get; set; }
        [DataMember]
        public int Ordinary { get; set; }        
    }
}
