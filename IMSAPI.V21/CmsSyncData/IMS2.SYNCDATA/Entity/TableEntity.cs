﻿using System.Runtime.Serialization;

namespace IMS2.SYNCDATA.Entity
{
    [DataContract]
    public class TableEntity
    {
        [DataMember]
        public string Name { get; set; }
        [DataMember]
        public string Value { get; set; }
    }
}
