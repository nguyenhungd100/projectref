﻿using System.Runtime.Serialization;

namespace IMS2.SYNCDATA.Entity
{
    [DataContract]
    public class CountNewsEntity
    {
        [DataMember]
        public int Count { get; set; }
    }
}
