﻿using IMS2.SYNCDATA.Common;
using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace IMS2.SYNCDATA.Entity
{
    [DataContract]
    public class VideoEntity : EntityBase
    {
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public int ZoneId { get; set; }
        [DataMember]
        public string Name { get; set; }
        [DataMember]
        public string UnsignName { get; set; }
        [DataMember]
        public string Description { get; set; }
        [DataMember]
        public string HtmlCode { get; set; }
        [DataMember]
        public string Avatar { get; set; }
        [DataMember]
        public string KeyVideo { get; set; }
        [DataMember]
        public string Pname { get; set; }
        [DataMember]
        public int Status { get; set; }
        [DataMember]
        public long NewsId { get; set; }
        [DataMember]
        public int Views { get; set; }
        [DataMember]
        public int Mode { get; set; }
        [DataMember]
        public string Tags { get; set; }
        [DataMember]
        public DateTime DistributionDate { get; set; }
        [DataMember]
        public string CreatedBy { get; set; }
        [DataMember]
        public DateTime CreatedDate { get; set; }
        [DataMember]
        public string LastModifiedBy { get; set; }
        [DataMember]
        public DateTime LastModifiedDate { get; set; }
        [DataMember]
        public string PublishBy { get; set; }
        [DataMember]
        public DateTime PublishDate { get; set; }
        [DataMember]
        public string EditedBy { get; set; }
        [DataMember]
        public DateTime EditedDate { get; set; }
        [DataMember]
        public string Url { get; set; }
        [DataMember]
        public string VideoRelation { get; set; }
        // VideoFileInfo
        [DataMember]
        public string FileName { get; set; }
        [DataMember]
        public string Duration { get; set; }
        [DataMember]
        public string Size { get; set; }
        [DataMember]
        public int Capacity { get; set; }
        // For advertisement
        [DataMember]
        public bool AllowAd { get; set; }
        [DataMember]
        public bool IsRemoveLogo { get; set; }
        [DataMember]
        public string OriginalUrl { get; set; }
        [DataMember]
        public int Type { get; set; }
        [DataMember]
        public bool IsConverted { get; set; }
        [DataMember]
        public string AvatarShareFacebook { get; set; }
        [DataMember]
        public int OriginalId { get; set; }
        [DataMember]
        public string Author { get; set; }
        [DataMember]
        public bool IsNewProcessed { get; set; }
        [DataMember]
        public int ParentId { get; set; }
        [DataMember]
        public string HashId { get; set; }
        [DataMember]
        public string TrailerUrl { get; set; }
        [DataMember]
        public string MetaAvatar { get; set; }
        [DataMember]
        public int Priority { get; set; }
        [DataMember]
        public string Location { get; set; }
        [DataMember]
        public string PolicyContentId { get; set; }

        //ext
        [DataMember]
        public int VideoFolderId { get; set; }
        [DataMember]
        public DateTime PlayOnTime { get; set; }
        [DataMember]
        public string ZoneName { get; set; }
        [DataMember]
        public string Source { get; set; }
        [DataMember]
        public List<VideoExtensionEntity> VideoExtension { get; set; }
        [DataMember]
        public string GroupName { get; set; }
    }

    [DataContract]
    public class VideoExtensionEntity : EntityBase
    {
        [DataMember]
        public long VideoId { get; set; }
        [DataMember]
        public int Type { get; set; }
        [DataMember]
        public string Value { get; set; }
        [DataMember]
        public int NumericValue { get; set; }
    }

    [DataContract]
    public class VideoInZoneEntity : EntityBase
    {
        [DataMember]
        public int VideoId { get; set; }
        [DataMember]
        public int ZoneId { get; set; }
        [DataMember]
        public bool IsPrimary { get; set; }
        [DataMember]
        public DateTime LastModifiedDate { get; set; }
        [DataMember]
        public int RootZoneId { get; set; }
    }

    [DataContract]
    public class VideoTagEntity : EntityBase
    {
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public int ParentId { get; set; }
        [DataMember]
        public string Name { get; set; }
        [DataMember]
        public string UnsignName { get; set; }
        [DataMember]
        public string Avatar { get; set; }
        [DataMember]
        public string Description { get; set; }
        [DataMember]
        public string Url { get; set; }
        [DataMember]
        public bool IsHotTag { get; set; }
        [DataMember]
        public DateTime CreatedDate { get; set; }
        [DataMember]
        public string CreatedBy { get; set; }
        [DataMember]
        public int Status { get; set; }
    }

    [DataContract]
    public class VideoInTagEntity : EntityBase
    {
        [DataMember]
        public int VideoTagId { get; set; }
        [DataMember]
        public int VideoId { get; set; }
        [DataMember]
        public int TagMode { get; set; }
        [DataMember]
        public int Priority { get; set; }
    }

    [DataContract]
    public class VideoTagsEntity : EntityBase
    {
        [DataMember]
        public long TagId { get; set; }
        [DataMember]
        public int VideoId { get; set; }
        [DataMember]
        public int Type { get; set; }
        [DataMember]
        public int Priority { get; set; }
    }

}
