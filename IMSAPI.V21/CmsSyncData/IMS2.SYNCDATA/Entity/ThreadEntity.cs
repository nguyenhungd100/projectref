﻿using IMS2.SYNCDATA.Common;
using System;
using System.Runtime.Serialization;

namespace IMS2.SYNCDATA.Entity
{
    [DataContract]
    public class ThreadEntity : EntityBase
    {
        [DataMember]
        public long Id { get; set; }
        [DataMember]
        public string Name { get; set; }
        [DataMember]
        public string UnsignName { get; set; }
        [DataMember]
        public string Title { get; set; }
        [DataMember]
        public string Description { get; set; }
        [DataMember]
        public string Url { get; set; }
        [DataMember]
        public string Avatar { get; set; }
        [DataMember]
        public string HomeAvatar { get; set; }
        [DataMember]
        public string SpecialAvatar { get; set; }
        [DataMember]
        public bool IsHot { get; set; }
        [DataMember]
        public bool IsOnHome { get; set; }
        [DataMember]
        public DateTime CreatedDate { get; set; }
        [DataMember]
        public DateTime ModifiedDate { get; set; }
        [DataMember]
        public string CreatedBy { get; set; }
        [DataMember]
        public string EditedBy { get; set; }
        [DataMember]
        public string MetaKeyword { get; set; }
        [DataMember]
        public string MetaContent { get; set; }
        [DataMember]
        public int TemplateId { get; set; }
        [DataMember]
        public bool Invisibled { get; set; }

        [DataMember]
        public int NewsCount { get; set; }
        [DataMember]
        public long NewsCoverId { get; set; }
        [DataMember]
        public string RelationThread { get; set; }
        [DataMember]
        public string RelationZone { get; set; }
        [DataMember]
        public int Ordinary { get; set; }
        [DataMember]
        public bool IsHightLight { get; set; }
        [DataMember]
        public bool IsHightLightOnMobile { get; set; }
        [DataMember]
        public int ViewCount { get; set; }
    }    
}
