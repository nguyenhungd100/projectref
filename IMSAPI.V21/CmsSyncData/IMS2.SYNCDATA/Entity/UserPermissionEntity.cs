﻿using IMS2.SYNCDATA.Common;
using System;
using System.Runtime.Serialization;

namespace IMS2.SYNCDATA.Entity
{
    [DataContract]
    public class UserPermissionEntity : EntityBase
    {
        [DataMember]
        public int UserId { get; set; }
        [DataMember]
        public int PermissionId { get; set; }
        [DataMember]
        public int ZoneId { get; set; }
    }
}
