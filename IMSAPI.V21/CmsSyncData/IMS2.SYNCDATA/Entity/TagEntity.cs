﻿using IMS2.SYNCDATA.Common;
using System;
using System.Runtime.Serialization;

namespace IMS2.SYNCDATA.Entity
{
    [DataContract]
    public class TagEntity : EntityBase
    {
        [DataMember]
        public long Id { get; set; }
        [DataMember]
        public long ParentId { get; set; }
        [DataMember]
        public string Name { get; set; }
        [DataMember]
        public string SubTitle { get; set; }
        [DataMember]
        public string Description { get; set; }
        [DataMember]
        public string Url { get; set; }
        [DataMember]
        public bool Invisibled { get; set; }
        [DataMember]
        public bool IsHotTag { get; set; }
        [DataMember]
        public int Type { get; set; }
        [DataMember]
        public DateTime CreatedDate { get; set; }
        [DataMember]
        public DateTime ModifiedDate { get; set; }
        [DataMember]
        public string CreatedBy { get; set; }
        [DataMember]
        public string EditedBy { get; set; }
        [DataMember]
        public string UnsignName { get; set; }
        [DataMember]
        public bool IsThread { get; set; }
        [DataMember]
        public string Avatar { get; set; }
        [DataMember]
        public long Priority { get; set; }
        [DataMember]
        public string TagContent { get; set; }
        [DataMember]
        public string TagTitle { get; set; }
        [DataMember]
        public string TagInit { get; set; }
        [DataMember]
        public string TagMetaKeyword { get; set; }
        [DataMember]
        public string TagMetaContent { get; set; }
        [DataMember]
        public int TemplateId { get; set; }
        [DataMember]
        public int NewsCount { get; set; }
        [DataMember]
        public int ZoneId { get; set; }
        [DataMember]
        public string ZoneName { get; set; }
        [DataMember]
        public long NewsCoverId { get; set; }
        [DataMember]
        public int Status { get; set; }
        [DataMember]
        public string SubTile { get; set; }
        [DataMember]
        public int ViewCount { get; set; }
        [DataMember]
        public int CountNewsInTag { get; set; }
    }

    [DataContract]
    public class TagNewsEntity : EntityBase
    {
        [DataMember]
        public int TagID { get; set; }
        [DataMember]
        public long NewsID { get; set; }
        [DataMember]
        public int TagMode { get; set; }
        [DataMember]
        public string TagProperty { get; set; }
        [DataMember]
        public int Priority { get; set; }       
    }

    [DataContract]
    public class TagZoneEntity : EntityBase
    {
        [DataMember]
        public long TagId { get; set; }
        [DataMember]
        public int ZoneId { get; set; }
        [DataMember]
        public bool IsPrimary { get; set; }        
    }
}
