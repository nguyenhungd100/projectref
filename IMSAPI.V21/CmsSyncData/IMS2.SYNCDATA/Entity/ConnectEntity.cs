﻿using System.Runtime.Serialization;

namespace IMS2.SYNCDATA.Entity
{
    [DataContract]
    public class ConnectEntity
    {
        [DataMember]
        public string Name { get; set; }
        [DataMember]
        public string ConnectString { get; set; }
    }
}
