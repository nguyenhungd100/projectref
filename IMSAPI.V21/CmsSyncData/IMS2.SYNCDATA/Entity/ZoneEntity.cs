﻿using IMS2.SYNCDATA.Common;
using System;
using System.Runtime.Serialization;

namespace IMS2.SYNCDATA.Entity
{
    [DataContract]
    public class ZoneEntity : EntityBase
    {
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public string Name { get; set; }
        [DataMember]
        public string Description { get; set; }
        [DataMember]
        public DateTime ModifiedDate { get; set; }
        [DataMember]
        public DateTime CreatedDate { get; set; }
        [DataMember]
        public string ShortUrl { get; set; }
        [DataMember]
        public int SortOrder { get; set; }
        [DataMember]
        public int ParentId { get; set; }
        [DataMember]
        public bool Invisibled { get; set; }
        [DataMember]
        public int Status { get; set; }
        [DataMember]
        public bool AllowComment { get; set; }
        [DataMember]
        public string Domain { get; set; }
        [DataMember]
        public bool UseForFunnyNews { get; set; }
        [DataMember]
        public bool IsPrimary { get; set; }
    }
}
