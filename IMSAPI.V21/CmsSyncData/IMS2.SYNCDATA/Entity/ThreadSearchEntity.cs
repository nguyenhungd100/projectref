﻿using System;
using Nest;

namespace IMS2.SYNCDATA.Entity
{
    [ElasticsearchType]
    public class ThreadSearchEntity
    {
        [Number(NumberType.Integer, Name = "id")]
        public long Id { get; set; }

        [Text(Name = "name", Index = true, Analyzer = "standard")]
        public string Name { get; set; }

        [Boolean(Name = "is_hot")]
        public bool IsHot { get; set; }

        [Text(Name = "zone_ids", Index = true)]
        public string[] ZoneIds { get; set; }

        [Date(Name = "created_date")]
        public DateTime CreatedDate { get; set; }
    }
}
