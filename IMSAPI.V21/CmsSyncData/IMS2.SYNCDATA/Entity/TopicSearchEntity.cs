﻿using System;
using Nest;

namespace IMS2.SYNCDATA.Entity
{
    [ElasticsearchType]
    public class TopicSearchEntity
    {
        [Number(NumberType.Integer, Name = "id")]
        public long Id { get; set; }

        [Text(Name = "topic_name", Index = true, Analyzer = "standard")]
        public string TopicName { get; set; }

        [Boolean(Name = "is_active")]
        public bool IsActive { get; set; }

        [Text(Name = "zone_ids", Index = true)]
        public string[] ZoneIds { get; set; }

        [Boolean(Name = "is_top_toolbar")]
        public bool isTopToolbar { get; set; }

        [Text(Name = "logo")]
        public string Logo { get; set; }        
    }
}
