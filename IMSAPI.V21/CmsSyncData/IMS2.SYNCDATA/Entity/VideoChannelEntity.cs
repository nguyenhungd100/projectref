﻿using IMS2.SYNCDATA.Common;
using System;
using System.Runtime.Serialization;

namespace IMS2.SYNCDATA.Entity
{
    [DataContract]
    public class VideoChannelEntity : EntityBase
    {
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public int PublisherId { get; set; }
        [DataMember]
        public string Name { get; set; }
        [DataMember]
        public string Avatar { get; set; }
        [DataMember]
        public string Description { get; set; }
        [DataMember]
        public int Priority { get; set; }
        [DataMember]
        public string CreatedBy { get; set; }
        [DataMember]
        public DateTime CreatedDate { get; set; }
        [DataMember]
        public int FollowCount { get; set; }
        [DataMember]
        public int VideoCount { get; set; }
        [DataMember]
        public int Rank { get; set; }
        [DataMember]
        public string IntroClip { get; set; }
        [DataMember]
        public string ChannelRelation { get; set; }
        [DataMember]
        public int Status { get; set; }
        [DataMember]
        public string LastModifiedBy { get; set; }
        [DataMember]
        public DateTime LastModifiedDate { get; set; }
        [DataMember]
        public string Url { get; set; }

        //relasionship
        [DataMember]
        public int ZoneId { get; set; }
        [DataMember]
        public int LabelId { get; set; }
        [DataMember]
        public string ZoneName { get; set; }

        [DataMember]
        public string Cover { get; set; }
    }
}
