﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IMS2.SYNCDATA.Entity
{
    public class NewsExcelEntity
    {
        public long Id { get; set; }
        public string Title { get; set; }
        public string CreatedBy { get; set; }
        public string EditedBy { get; set; }
        public string PublishedBy { get; set; }
        public DateTime DistributionDate { get; set; }
        public int ViewCount { get; set; }
        public int ViewPcCount { get; set; }
        public int ViewMobCount { get; set; }
        public string ZoneName { get; set; }
        public string Url { get; set; }
        public string TagItem { get; set; }
        public int CountLike { get; set; }
        public int CountShare { get; set; }
        public string Domain { get; set; }
    }
}
