﻿using IMS2.SYNCDATA.Common;
using System;
using System.Runtime.Serialization;

namespace IMS2.SYNCDATA.Entity
{
    [DataContract]
    public class UserEntity : EntityBase
    {
        [DataMember]
        public int Id { get; set; }        
        [DataMember]
        public string EncryptId { get; set; }
        [DataMember]
        public string Password { get; set; }
        [DataMember]
        public string UserName { get; set; }
        [DataMember]
        public string FullName { get; set; }
        [DataMember]
        public string Avatar { get; set; }
        [DataMember]
        public string Email { get; set; }
        [DataMember]
        public string Mobile { get; set; }
        [DataMember]
        public bool IsFullPermission { get; set; }
        [DataMember]
        public bool IsFullZone { get; set; }
        [DataMember]
        public int Status { get; set; }
        [DataMember]
        public string Address { get; set; }
        [DataMember]
        public DateTime Birthday { get; set; }
        [DataMember]
        public string Description { get; set; }
        [DataMember]
        public DateTime CreatedDate { get; set; }
        [DataMember]
        public DateTime ModifiedDate { get; set; }
        [DataMember]
        public DateTime LastLogined { get; set; }
        [DataMember]
        public DateTime LastChangePass { get; set; }
        [DataMember]
        public int PermissionCount { get; set; }
        [DataMember]
        public bool IsSystem { get; set; }
        [DataMember]
        public int UserType { get; set; }
        [DataMember]
        public string Domain { get; set; }
        [DataMember]
        public int IsRole { get; set; }
        [DataMember]
        public bool IsSendOver { get; set; }
        [DataMember]
        public string OtpSecretKey { get; set; }
    }

    [DataContract]
    public class UserProfileEntity : EntityBase
    {
        [DataMember]
        public int UserId { get; set; }
        [DataMember]
        public string Address { get; set; }
        [DataMember]
        public DateTime Birthday { get; set; }
        [DataMember]
        public string Description { get; set; }       
    }

    [DataContract]
    public class GroupPermissionEntity : EntityBase
    {
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public string Name { get; set; }        
    }

    [DataContract]
    public class PermissionEntity : EntityBase
    {
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public int GroupId { get; set; }
        [DataMember]
        public string Name { get; set; }
        [DataMember]
        public bool IsGrantByCategory { get; set; }
    }
}
