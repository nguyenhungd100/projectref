﻿using IMS2.SYNCDATA.Common;
using System;
using System.Runtime.Serialization;

namespace IMS2.SYNCDATA.Entity
{
    [DataContract]
    public class TopicEntity : EntityBase
    {
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public string TopicName { get; set; }
        [DataMember]
        public string Logo { get; set; }
        [DataMember]
        public string LogoFancyClose { get; set; }
        [DataMember]
        public string LogoTopicName { get; set; }
        [DataMember]
        public string LogoSubMenu { get; set; }
        [DataMember]
        public string Cover { get; set; }
        [DataMember]
        public bool IsActive { get; set; }
        [DataMember]
        public bool IsIconActive { get; set; }
        [DataMember]
        public int NewsCount { get; set; }
        [DataMember]
        public string Description { get; set; }
        [DataMember]
        public string DisplayUrl { get; set; }
        //[DataMember]
        //public List<TagWithSimpleFieldEntity> ListTag { get; set; }
        [DataMember]
        public string TagInString { get; set; }
        [DataMember]
        public int DefaultViewMode { get; set; }
        [DataMember]
        public string GuideToSendMail { get; set; }
        [DataMember]
        public string TopicEmail { get; set; }
        [DataMember]
        public bool IsTopToolbar { get; set; }
        [DataMember]
        public int ParentId { get; set; }
        [DataMember]
        public string ParentName { get; set; }
        [DataMember]
        public string RelationTopic { get; set; }
        [DataMember]
        public bool IsPrimary { get; set; }
        [DataMember]
        public string DisplayName { get; set; }
        [DataMember]
        public int Priority { get; set; }
        [DataMember]
        public int ZoneId { get; set; }
        [DataMember]
        public string ZoneIdList { get; set; }
    }

    [DataContract]
    public class TopicInZoneEntity : EntityBase
    {
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public int TopicId { get; set; }
        [DataMember]
        public int ZoneId { get; set; }        
    }

    [DataContract]
    public class NewsInTopicEntity : EntityBase
    {
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public long NewsId { get; set; }
        [DataMember]
        public int TopicId { get; set; }        
    }
}
