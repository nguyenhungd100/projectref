﻿using IMS2.SYNCDATA.Common;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace IMS2.SYNCDATA.Entity
{
    [DataContract]
    public class UserCachedEntity : EntityBase
    {
        [DataMember]
        public UserEntity User { get; set; }

        [DataMember]
        public List<UserPermissionEntity> PermissionList { get; set; }

        public UserCachedEntity()
        {
            this.User = new UserEntity();
            this.PermissionList = new List<UserPermissionEntity>();
        }
    }
}
