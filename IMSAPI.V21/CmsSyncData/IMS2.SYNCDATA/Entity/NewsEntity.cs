﻿using IMS2.SYNCDATA.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;

namespace IMS2.SYNCDATA.Entity
{
    [DataContract]
    public class NewsEntity : EntityBase
    {
        public NewsEntity()
        {
            Price = 0;
            CreatedDate = DateTime.Now;
            LastModifiedDate = DateTime.Now;
            Status = 0;
        }

        [DataMember]
        public long Id { get; set; }
        [DataMember]
        public string EncryptId { get { return Id.ToString(); } set { } }
        [DataMember]
        public string StrId
        {
            get
            {
                return Id.ToString();
            }
            set { }
        }
        [DataMember]
        public string Title { get; set; }
        [DataMember]
        public string SubTitle { get; set; }
        [DataMember]
        public string Sapo { get; set; }
        [DataMember]
        public string Body { get; set; }
        [DataMember]
        public string Avatar { get; set; }
        [DataMember]
        public string AvatarDesc { get; set; }
        [DataMember]
        public string Avatar2 { get; set; }
        [DataMember]
        public string Avatar3 { get; set; }
        [DataMember]
        public string Avatar4 { get; set; }
        [DataMember]
        public string Avatar5 { get; set; }
        [DataMember]
        public string Author { get; set; }
        [DataMember]
        public string NewsRelation { get; set; }
        [DataMember]
        public int Status { get; set; }
        [DataMember]
        public string Source { get; set; }
        [DataMember]
        public bool IsFocus { get; set; }
        [DataMember]
        public int Type { get; set; }
        [DataMember]
        public int ThreadId { get; set; }
        [DataMember]
        public DateTime CreatedDate { get; set; }
        [DataMember]
        public DateTime LastModifiedDate { get; set; }
        [DataMember]
        public DateTime DistributionDate { get; set; }
        [DataMember]
        public string CreatedBy { get; set; }
        [DataMember]
        public string LastModifiedBy { get; set; }
        [DataMember]
        public string PublishedBy { get; set; }
        [DataMember]
        public string EditedBy { get; set; }
        [DataMember]
        public string LastReceiver { get; set; }
        [DataMember]
        public int WordCount { get; set; }
        [DataMember]
        public int ViewCount { get; set; }
        [DataMember]
        public int Priority { get; set; }
        [DataMember]
        public string ListZoneId { get; set; }
        [DataMember]
        public string Tag { get; set; }
        [DataMember]
        public string Note { get; set; }
        [DataMember]
        public string TagPrimary { get; set; }
        [DataMember]
        public decimal Price { get; set; }

        [DataMember]
        public int DisplayStyle { get; set; }
        [DataMember]
        public int DisplayPosition { get; set; }
        [DataMember]
        public int DisplayInSlide { get; set; }
        [DataMember]
        public string AvatarCustom { get; set; }

        [DataMember]
        public int OriginalId { get; set; }
        [DataMember]
        public int NewsType { get; set; }
        [DataMember]
        public bool IsOnHome { get; set; }

        [DataMember]
        public string Url { get; set; }
        [DataMember]
        public string OriginalUrl { get; set; }
        [DataMember]
        public string NoteRoyalties { get; set; }
        [DataMember]
        public string TagItem { get; set; }

        [DataMember]
        public int NewsCategory { get; set; }
        [DataMember]
        public string InitSapo { get; set; }

        [DataMember]
        public string TemplateName { get; set; }
        [DataMember]
        public string TemplateConfig { get; set; }

        [DataMember]
        public bool IsPr { get; set; }
        [DataMember]
        public int PrPosition { get; set; }
        [DataMember]
        public bool AdStore { get; set; }
        [DataMember]
        public string AdStoreUrl { get; set; }
        [DataMember]
        public string PrBookingNumber { get; set; }

        [DataMember]
        public bool IsBreakingNews { get; set; }

        [DataMember]
        public bool IsOnMobile { get; set; }

        [DataMember]
        public int PegaBreakingNews { get; set; }

        [DataMember]
        public int InterviewId { get; set; }
        [DataMember]
        public int RollingNewsId { get; set; }
        [DataMember]
        public int TagSubTitleId { get; set; }
        [DataMember]
        public int ZoneId { get; set; }
        [DataMember]
        public string ZoneName { get; set; }

        [DataMember]
        public long QuickNewsVietId { get; set; }

        [DataMember]
        public long CmsAccountVietId { get; set; }

        [DataMember]
        public string PenName { get; set; }

        [DataMember]
        public bool IsActivePenName { get; set; }

        [DataMember]
        public bool IsShowPenNameCTV { get; set; }

        [DataMember]
        public int ContentFooterType { get; set; }

        [DataMember]
        public int LocationType { get; set; }

        [DataMember]
        public DateTime ExpiredDate { get; set; }
        [DataMember]
        public string SourceURL { get; set; }

        [DataMember]
        public decimal BonusPrice { get; set; }

        [DataMember]
        public string ShortTitle { get; set; }
        [DataMember]
        public long ParentNewsId { get; set; }
        //chinhnb add
        [DataMember]
        public string ApprovedBy { get; set; }
        [DataMember]
        public DateTime ApprovedDate { get; set; }
        [DataMember]
        public string ReturnedBy { get; set; }
        [DataMember]
        public string SentBy { get; set; }
        [DataMember]
        public string ErrorCheckedBy { get; set; }
        [DataMember]
        public DateTime ErrorCheckedDate { get; set; }
        [DataMember]
        public string SensitiveCheckedBy { get; set; }
        [DataMember]
        public DateTime SensitiveCheckedDate { get; set; }

        [DataMember]
        public string SourceUrl { get; set; }
        [DataMember]
        public int ViewCountMobile { get; set; }
        [DataMember]
        public int PhotoCount { get; set; }
        [DataMember]
        public int VideoCount { get; set; }
        [DataMember]
        public int WarningLevel { get; set; }
        [DataMember]
        public bool IsProd { get; set; }
    }

    [DataContract]
    public class NewsInZoneEntity : EntityBase
    {
        [DataMember]
        public long NewsId { get; set; }
        [DataMember]
        public int ZoneId { get; set; }
        [DataMember]
        public bool IsPrimary { get; set; }
        [DataMember]
        public int DistributionID { get; set; }
        [DataMember]
        public int RootZoneId { get; set; }
    }

    [DataContract]
    public class MenuExtensionEntity : EntityBase
    {
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public int ParentMenuId{ get; set; }
        [DataMember]
        public int GroupMenuId { get; set; }
        [DataMember]
        public int ZoneId{ get; set; }
        [DataMember]
        public int ParentZoneId{ get; set; }
        [DataMember]
        public long TagId{ get; set; }
        [DataMember]
        public string Name{ get; set; }
        [DataMember]
        public string Avatar{ get; set; }
        [DataMember]
        public string Url{ get; set; }
        [DataMember]
        public int Priority{ get; set; }
        [DataMember]
        public int Status{ get; set; }
        [DataMember]
        public int OldId{ get; set; }
        [DataMember]
        public int OldParentId{ get; set; }
        [DataMember]
        public int OldType{ get; set; }
        public int OldIsTopMenu{ get; set; }
        [DataMember]
        public int OldCatId{ get; set; }
    }

    [DataContract]
    public class NewsExtensionEntity : EntityBase
    {
        [DataMember]
        public long NewsId { get; set; }
        [DataMember]
        public int Type { get; set; }
        [DataMember]
        public string Value { get; set; }
    }

    [DataContract]
    public class NewsPositionEntity : EntityBase
    {
        [DataMember]
        public long Id { get; set; }
        [DataMember]
        public int ZoneId { get; set; }
        [DataMember]
        public int TypeId { get; set; }
        [DataMember]
        public int Order { get; set; }
        [DataMember]
        public bool Lockable { get; set; }
        [DataMember]
        public bool Locked { get; set; }
        [DataMember]
        public DateTime ExpiredLock { get; set; }
        [DataMember]
        public long NewsIdForBomd { get; set; }
        [DataMember]
        public long NewsId { get; set; }
        [DataMember]
        public string Title { get; set; }
        [DataMember]
        public string SubTitle { get; set; }
        [DataMember]
        public string Sapo { get; set; }
        [DataMember]
        public string Avatar { get; set; }
        [DataMember]
        public string AvatarDesc { get; set; }
        [DataMember]
        public string Avatar1 { get; set; }
        [DataMember]
        public string Avatar2 { get; set; }
        [DataMember]
        public string Avatar3 { get; set; }
        [DataMember]
        public string Avatar4 { get; set; }
        [DataMember]
        public string Avatar5 { get; set; }
        [DataMember]
        public string Author { get; set; }
        [DataMember]
        public string NewsRelation { get; set; }
        [DataMember]
        public string Source { get; set; }
        [DataMember]
        public bool IsFocus { get; set; }
        [DataMember]
        public int NewsType { get; set; }
        [DataMember]
        public int ThreadId { get; set; }
        [DataMember]
        public DateTime DistributionDate { get; set; }
        [DataMember]
        public string Url { get; set; }
        [DataMember]
        public bool AllowAutoUpdate { get; set; }
        [DataMember]
        public int DisplayStyle { get; set; }
        [DataMember]
        public int DisplayPosition { get; set; }
        [DataMember]
        public int DisplayInSlide { get; set; }
        [DataMember]
        public string AvatarCustom { get; set; }
        [DataMember]
        public int OriginalId { get; set; }
        [DataMember]
        public string InitSapo { get; set; }
        [DataMember]
        public int InterviewId { get; set; }
        [DataMember]
        public bool IsBreakingNews { get; set; }
        [DataMember]
        public string OriginalUrl { get; set; }
        [DataMember]
        public bool IsPr { get; set; }
        [DataMember]
        public bool AdStore { get; set; }
        [DataMember]
        public string AdStoreUrl { get; set; }
        [DataMember]
        public int RollingNewsId { get; set; }
        [DataMember]
        public DateTime LastModifiedDate { get; set; }
        [DataMember]
        public int TagSubTitleId { get; set; }
        [DataMember]
        public int ZoneIdForNews { get; set; }
        [DataMember]
        public int Position { get; set; }
        [DataMember]
        public int PrPosition { get; set; }
        [DataMember]
        public int Type { get; set; }
        [DataMember]
        public int Priority { get; set; }
        [DataMember]
        public long LastModifiedDateStamp { get; set; }
        [DataMember]
        public long PublishedDateStamp { get; set; }
        [DataMember]
        public int LocationType { get; set; }
        [DataMember]
        public DateTime ExpiredDate { get; set; }
        [DataMember]
        public DateTime ScheduleDate { get; set; }
        [DataMember]
        public bool IsOnHome { get; set; }
        [DataMember]
        public long ParentNewsId { get; set; }
    }

    [DataContract]
    public class NewsPublishEntity : EntityBase
    {
        [DataMember]
        public int ID { get; set; }
        [DataMember]
        public long NewsId { get; set; }
        [DataMember]
        public int ZoneId { get; set; }
        [DataMember]
        public string Title { get; set; }
        [DataMember]
        public string SubTitle { get; set; }
        [DataMember]
        public string Sapo { get; set; }
        [DataMember]
        public string Avatar { get; set; }
        [DataMember]
        public string AvatarDesc { get; set; }
        [DataMember]
        public string Avatar1 { get; set; }
        [DataMember]
        public string Avatar2 { get; set; }
        [DataMember]
        public string Avatar3 { get; set; }
        [DataMember]
        public string Avatar4 { get; set; }
        [DataMember]
        public string Avatar5 { get; set; }
        [DataMember]
        public string Author { get; set; }
        [DataMember]
        public string NewsRelation { get; set; }
        [DataMember]
        public string Source { get; set; }
        [DataMember]
        public bool IsFocus { get; set; }
        [DataMember]
        public int Type { get; set; }
        [DataMember]
        public int ThreadId { get; set; }
        [DataMember]
        public DateTime DistributionDate { get; set; }
        [DataMember]
        public string Url { get; set; }
        [DataMember]
        public int DisplayStyle { get; set; }
        [DataMember]
        public int DisplayPosition { get; set; }
        [DataMember]
        public int DisplayInSlide { get; set; }
        [DataMember]
        public string AvatarCustom { get; set; }
        [DataMember]
        public int OriginalId { get; set; }
        [DataMember]
        public long PublishedDate { get; set; }
        [DataMember]
        public bool IsOnHome { get; set; }
        [DataMember]
        public DateTime LastModifiedDate { get; set; }
        [DataMember]
        public int Priority { get; set; }
        [DataMember]
        public string OriginalUrl { get; set; }
        public string TagItem { get; set; }
        [DataMember]
        public bool IsPrimary { get; set; }
        [DataMember]
        public string InitSapo { get; set; }
        [DataMember]
        public int NewsType { get; set; }
        [DataMember]
        public bool IsPr { get; set; }
        [DataMember]
        public int Position { get; set; }
        [DataMember]
        public int PrPosition { get; set; }
        [DataMember]
        public bool AdStore { get; set; }
        [DataMember]
        public string AdStoreUrl { get; set; }
        [DataMember]
        public bool IsOnMobile { get; set; }
        [DataMember]
        public bool IsBreakingNews { get; set; }
        [DataMember]
        public int InterviewId { get; set; }
        [DataMember]
        public int RollingNewsId { get; set; }
        [DataMember]
        public int TagSubTitleId { get; set; }

        [DataMember]
        public long LastModifiedDateStamp { get; set; }

        [DataMember]
        public long PublishedDateStamp { get; set; }

        [DataMember]
        public int LocationType { get; set; }

        [DataMember]
        public DateTime ExpiredDate { get; set; }
        [DataMember]
        public int DistributionID { get; set; }

        [DataMember]
        public int PrimaryZoneId { get; set; }

        [DataMember]
        public string TitleDetail { get; set; }
        [DataMember]
        public long ParentNewsId { get; set; }
    }

    [DataContract]
    public class NewsContentEntity : EntityBase
    {        
        [DataMember]
        public long NewsId { get; set; }        
        [DataMember]
        public string Title { get; set; }
        [DataMember]
        public string SubTitle { get; set; }
        [DataMember]
        public string Sapo { get; set; }
        [DataMember]
        public string Avatar { get; set; }
        [DataMember]
        public string AvatarDesc { get; set; }
        [DataMember]
        public string Body { get; set; }
        [DataMember]
        public string Avatar2 { get; set; }
        [DataMember]
        public string Avatar3 { get; set; }
        [DataMember]
        public string Avatar4 { get; set; }
        [DataMember]
        public string Avatar5 { get; set; }
        [DataMember]
        public string Tags { get; set; }
        [DataMember]
        public string Author { get; set; }
        [DataMember]
        public string NewsRelation { get; set; }
        [DataMember]
        public string Source { get; set; }
        [DataMember]
        public bool IsFocus { get; set; }
        [DataMember]
        public int Type { get; set; }
        [DataMember]
        public int ThreadId { get; set; }
        [DataMember]
        public DateTime PublishedDate { get; set; }
        [DataMember]
        public string TagPrimary { get; set; }
        [DataMember]
        public string Url { get; set; }        
        [DataMember]
        public int ZoneId { get; set; }
        [DataMember]
        public long OriginalId { get; set; }        
        [DataMember]
        public bool IsOnHome { get; set; }        
        [DataMember]
        public string OriginalUrl { get; set; }
        public string TagItem { get; set; }
        [DataMember]
        public bool IsPrimary { get; set; }
        [DataMember]
        public string InitSapo { get; set; }
        [DataMember]
        public int NewsType { get; set; }
        [DataMember]
        public bool IsPr { get; set; }
        [DataMember]
        public int Position { get; set; }
        [DataMember]
        public int PrPosition { get; set; }
        [DataMember]
        public bool AdStore { get; set; }
        [DataMember]
        public string AdStoreUrl { get; set; }
        [DataMember]
        public bool IsOnMobile { get; set; }
        [DataMember]
        public bool IsBreakingNews { get; set; }
        [DataMember]
        public int InterviewId { get; set; }
        [DataMember]
        public int RollingNewsId { get; set; }
        [DataMember]
        public int TagSubTitleId { get; set; }
        [DataMember]
        public bool UseTemplate { get; set; }
        [DataMember]
        public int LocationType { get; set; }
        [DataMember]
        public DateTime ExpiredDate { get; set; }
        [DataMember]
        public string SourceUrl { get; set; }
        [DataMember]
        public long ParentNewsId { get; set; }
    }

    [DataContract]
    public class SEOMetaNewsEntity : EntityBase
    {
        [DataMember]
        public long NewsId { get; set; }
        [DataMember]
        public string MetaTitle { get; set; }
        [DataMember]
        public string MetaKeyword { get; set; }
        [DataMember]
        public string MetaDescription { get; set; }
        [DataMember]
        public string MetaNewsKeyword { get; set; }
        [DataMember]
        public string KeywordFocus { get; set; }
        [DataMember]
        public string CreatedBy { get; set; }
        [DataMember]
        public string SocialTitle { get; set; }        
    }

    [DataContract]
    public class NewsRelationEntity : EntityBase
    {
        [DataMember]
        public long NewsId { get; set; }
        [DataMember]
        public long NewsRelationId { get; set; }
        [DataMember]
        public int ZoneId { get; set; }
        [DataMember]
        public bool IsChange { get; set; }
        [DataMember]
        public int Priority { get; set; }
        [DataMember]
        public int Type { get; set; }
    }

    [DataContract]
    public class SEOMetaVideoEntity : EntityBase
    {
        [DataMember]
        public int VideoId { get; set; }
        [DataMember]
        public string MetaTitle { get; set; }
        [DataMember]
        public string MetaKeyword { get; set; }
        [DataMember]
        public string MetaDescription { get; set; }
        [DataMember]
        public string MetaVideoKeyword { get; set; }
        [DataMember]
        public string KeywordFocus { get; set; }        
    }

    [DataContract]
    public class SEOTagZoneEntity : EntityBase
    {
        [DataMember]
        public long TagId { get; set; }
        [DataMember]
        public int ZoneId { get; set; }
        [DataMember]
        public bool IsPrimary { get; set; }        
    }

    [DataContract]
    public class NewsPrEntity : EntityBase
    {
        [DataMember]
        public long Id { get; set; }
        [DataMember]
        public long ContentId { get; set; }
        [DataMember]
        public long DistributionId { get; set; }
        [DataMember]
        public long NewsId { get; set; }
        [DataMember]
        public int Mode { get; set; }
        [DataMember]
        public bool IsFocus { get; set; }
        [DataMember]
        public DateTime DistributionDate { get; set; }
        [DataMember]
        public DateTime PublishDate { get; set; }
        [DataMember]
        public DateTime ExpireDate { get; set; }
        [DataMember]
        public DateTime CreatedDate { get; set; }
        [DataMember]
        public int Duration { get; set; }
        [DataMember]
        public DateTime LastModifiedDate { get; set; }
        [DataMember]
        public long BookingId { get; set; }
        [DataMember]
        public string ContractNo { get; set; }
        [DataMember]
        public int ZoneId { get; set; }
        [DataMember]
        public decimal Price { get; set; }
        [DataMember]
        public int ViewPlusBannerId { get; set; }
    }
}