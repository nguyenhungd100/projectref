﻿using System;
using System.IO;
using System.Linq;

namespace IMS2.SYNCDATA.Common
{
    public class Logger
    {
        public static void Log(TypeLog type, string message)
        {
            try {
                message = DateTime.Now.ToString("dd-MM-yyyy HH:mm:ss") + "=> " + message;
                var typeName = "error";
                switch (type)
                {
                    case TypeLog.Bug: typeName = "bug"; break;
                    case TypeLog.Error: typeName = "error"; break;
                    case TypeLog.Info: typeName = "info"; break;
                    case TypeLog.Data: typeName = "data"; break;
                }
                var fileName = "log." + typeName + ".txt";

                var location = System.Reflection.Assembly.GetEntryAssembly().Location;
                var directoryPath = Path.GetDirectoryName(location);

                var folderLog = directoryPath + @"\_Logs\" + DateTime.Now.ToString("yyyyMMdd");

                if (!Directory.Exists(folderLog))
                {
                    Directory.CreateDirectory(folderLog);
                }

                var path = folderLog + @"\" + fileName;

                var fs = new FileStream(path, FileMode.OpenOrCreate, FileAccess.Write);
                var sw = new StreamWriter(fs);
                sw.BaseStream.Seek(0, SeekOrigin.End);
                sw.WriteLine(message);
                sw.Flush();
                sw.Close();
            }
            catch { }
        }

        public static void WriteFile(string message,string name = "datesync")
        {
            try {
                var fileName = name+".running.txt";

                var location = System.Reflection.Assembly.GetEntryAssembly().Location;
                var directoryPath = Path.GetDirectoryName(location);

                var folderTime = directoryPath + @"\_Time\" + DateTime.Now.ToString("yyyyMMdd");

                if (!Directory.Exists(folderTime))
                {
                    Directory.CreateDirectory(folderTime);
                }
                
                var path = folderTime + @"\" + fileName;

                var fs = new FileStream(path, FileMode.OpenOrCreate, FileAccess.Write);
                var sw = new StreamWriter(fs);
                sw.BaseStream.Seek(0, SeekOrigin.End);
                sw.WriteLine(message);
                sw.Flush();
                sw.Close();
            }
            catch { }
        }

        public static string ReadFile(string name = "datesync")
        {
            try {
                var fileName = name+".running.txt";

                var location = System.Reflection.Assembly.GetEntryAssembly().Location;
                var directoryPath = Path.GetDirectoryName(location);
                var folderTime = directoryPath + @"\_Time\" + DateTime.Now.ToString("yyyyMMdd");
                var path = folderTime + @"\" + fileName;
                if (File.Exists(path))
                {                    
                    string lastLine = File.ReadLines(path).Last();

                    return lastLine;
                }
                return "";
            }
            catch { return ""; }           
        }
    }    

    public enum TypeLog
    {
        Bug=1,
        Error=2,
        Info=3,
        Data=4
    }
}
