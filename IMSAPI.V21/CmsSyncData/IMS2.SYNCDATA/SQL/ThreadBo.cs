﻿using IMS2.SYNCDATA.Common;
using IMS2.SYNCDATA.Entity;
using IMS2.SYNCDATA.ES;
using IMS2.SYNCDATA.Redis;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Windows.Forms;

namespace IMS2.SYNCDATA.SQL
{
    public class ThreadBo
    {
        public static SqlConnection GetConnection(string conn="")
        {
            if (string.IsNullOrEmpty(conn))
            {
                conn = "Data Source=172.16.41.125;Initial Catalog=IMS2_AFAMILY_DEV;Persist Security Info=True;User ID=ims_test;Password=ImsTest123$%^789";
            }            
            return new SqlConnection(conn);            
        }

        private static List<ThreadInZoneEntity> GetThreadInZoneByThreadId(string conn, long id)
        {
            try
            {
                string query = @"CMS_ThreadInZone_GetByThreadId";

                var con = GetConnection(conn);
                var cmd = new SqlCommand(query, con);
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.AddWithValue("@ThreadId", id);

                con.Open();
                var reader = cmd.ExecuteReader();
                var data = EntityMapper.FillCollection<ThreadInZoneEntity>(reader);
                con.Close();

                return data;
            }
            catch (Exception ex)
            {
                return new List<ThreadInZoneEntity>();
            }
        }

        public static int SyncThreadToEs(string nameSpace, string connectStr, string connectEs, int pageIndex, int pageSize)
        {
            try
            {                
                string query = @"CMS_Thread_InitESAllThread";

                var con = GetConnection(connectStr);                                
                var cmd = new SqlCommand(query, con);
                cmd.CommandType = CommandType.StoredProcedure;               
                var dateFrom = DBNull.Value;
                var dateTo = DBNull.Value;
                var totalRows = 0;

                var totalRow = cmd.CreateParameter();
                totalRow.ParameterName = "@TotalRow";
                totalRow.Value = totalRows;
                totalRow.Direction = ParameterDirection.Output;

                cmd.Parameters.Add(totalRow);
                cmd.Parameters.AddWithValue("@PageIndex", pageIndex);
                cmd.Parameters.AddWithValue("@PageSize", pageSize);
                cmd.Parameters.AddWithValue("@DateFrom", dateFrom);
                cmd.Parameters.AddWithValue("@DateTo", dateTo);
                
                con.Open();
                var reader = cmd.ExecuteReader();                

                var data = EntityMapper.FillCollection<ThreadEntity>(reader);

                con.Close();

                var list = data.Select(s => new ThreadSearchEntity
                {
                    Id = s.Id,
                    Name = s.Name,
                    ZoneIds = GetThreadInZoneByThreadId(connectStr, s.Id).Select(se => se.ZoneId.ToString()).ToArray(),
                    CreatedDate = s.CreatedDate,
                    IsHot = s.IsHot
                }).ToList();

                if (pageIndex == 1)
                {
                    EsNewsBo.CreateIndexSettings<ThreadSearchEntity>(nameSpace, connectEs, "name");
                }
                var abc= EsNewsBo.CreateMany(nameSpace, connectEs, list);

                return 1;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return 0;                
            }
        }

        public static int GetCountThread(string connectStr)
        {
            try
            {
                string query = @"SELECT COUNT(*) as [Count] FROM [Thread]";

                var con = GetConnection(connectStr);
                var cmd = new SqlCommand(query, con);                               

                con.Open();
                var reader = cmd.ExecuteReader();

                var data = EntityMapper.FillCollection<CountNewsEntity>(reader);

                con.Close();

                var count = 0;
                if(data!=null && data.Count > 0)
                {
                    count = data.FirstOrDefault().Count;
                }

                return count;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return 0;                
            }
        }

        public static int SyncThreadToRedis(string nameSpace, string connectStr, string connectRedis, int pageIndex, int pageSize)
        {           
            try
            {
                string query = @"CMS_Thread_InitESAllThread";

                var con = GetConnection(connectStr);
                var cmd = new SqlCommand(query, con);
                cmd.CommandType = CommandType.StoredProcedure;
                var dateFrom = DBNull.Value;
                var dateTo = DBNull.Value;
                var totalRows = 0;

                var totalRow = cmd.CreateParameter();
                totalRow.ParameterName = "@TotalRow";
                totalRow.Value = totalRows;
                totalRow.Direction = ParameterDirection.Output;

                cmd.Parameters.Add(totalRow);
                cmd.Parameters.AddWithValue("@PageIndex", pageIndex);
                cmd.Parameters.AddWithValue("@PageSize", pageSize);
                cmd.Parameters.AddWithValue("@DateFrom", dateFrom);
                cmd.Parameters.AddWithValue("@DateTo", dateTo);

                con.Open();
                var reader = cmd.ExecuteReader();

                var listNews = EntityMapper.FillCollection<ThreadEntity>(reader);

                con.Close();

                var data = new Dictionary<string, ThreadEntity>();
                foreach (var entry in listNews)
                {
                    data[entry.Id.ToString()] = entry;
                }
                var abc = RedisNewsBo.AddInHash(nameSpace, connectRedis, data);

                return 1;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return 0;
            }
        }

        #region Afamily

        #region Thread
        public static int SyncThreadToDb(string nameSpace, string connectStr1, string connectStr2, int pageIndex, int pageSize, IProgress<InitDataDb.ProcessEntity> progress, InitDataDb.ProcessEntity processEntity)
        {
            try
            {
                string query = @"DECLARE @UpperBand int, @LowerBand int
                                SET @LowerBand  = (@pageIndex - 1) * @PageSize
                                SET @UpperBand  = (@pageIndex * @PageSize)
                                SELECT * FROM (
                                SELECT n.*,ROW_NUMBER() OVER(ORDER BY n.Id DESC) AS RowNumber 
                                FROM [Thread] n
                                ) AS temp
                                WHERE RowNumber > @LowerBand AND RowNumber <= @UpperBand";

                //test Id=20161214035128196
                //query = @"SELECT * FROM News WHERE Id=20161214035128196";

                var con = GetConnection(connectStr1);
                var cmd = new SqlCommand(query, con);
                //cmd.CommandType = CommandType.StoredProcedure;               
                var dateFrom = DBNull.Value;
                var dateTo = DBNull.Value;
                cmd.Parameters.AddWithValue("@PageIndex", pageIndex);
                cmd.Parameters.AddWithValue("@PageSize", pageSize);
                cmd.Parameters.AddWithValue("@DateFrom", dateFrom);
                cmd.Parameters.AddWithValue("@DateTo", dateTo);

                con.Open();
                var reader = cmd.ExecuteReader();

                var data = EntityMapper.FillCollection<ThreadEntity>(reader);

                con.Close();

                var abc = InitThreadToDb(nameSpace, connectStr2, data, progress, processEntity);

                return abc;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return 0;
            }
        }
        private static int InitThreadToDb(string nameSpace, string connectStr2, List<ThreadEntity> data, IProgress<InitDataDb.ProcessEntity> progress, InitDataDb.ProcessEntity processEntity)
        {
            long id = 0;
            var count = 0;
            try
            {
                var con = GetConnection(connectStr2);
                con.Open();

                foreach (var item in data)
                {
                    string query = @"SET IDENTITY_INSERT [Thread] ON;
                                    DELETE [Thread] WHERE Id=@Id;
                                    INSERT INTO [Thread]([Id]
                                                      ,[Name]
                                                      ,[UnsignName]
                                                      ,[Title]
                                                      ,[Description]
                                                      ,[Url]
                                                      ,[Avatar]
                                                      ,[HomeAvatar]
                                                      ,[SpecialAvatar]
                                                      ,[IsHot]
                                                      ,[IsOnHome]
                                                      ,[CreatedDate]
                                                      ,[ModifiedDate]
                                                      ,[CreatedBy]
                                                      ,[EditedBy]
                                                      ,[MetaKeyword]
                                                      ,[MetaContent]
                                                      ,[TemplateId]
                                                      ,[RelationThread]
                                                      ,[RelationZone]
                                                      ,[Ordinary]
                                                      ,[Invisibled]
                                                      ,[IsHightLight]
                                                      ,[IsHightLightOnMobile]
                                                      ,[ViewCount]
                                                      ,[NewsCoverId]
                                                ) VALUES(@Id
                                                        ,@Name
                                                        ,@UnsignName
                                                        ,@Title
                                                        ,@Description
                                                        ,@Url
                                                        ,@Avatar
                                                        ,@HomeAvatar
                                                        ,@SpecialAvatar
                                                        ,@IsHot
                                                        ,@IsOnHome
                                                        ,@CreatedDate
                                                        ,@ModifiedDate
                                                        ,@CreatedBy
                                                        ,@EditedBy
                                                        ,@MetaKeyword
                                                        ,@MetaContent
                                                        ,@TemplateId
                                                        ,@RelationThread
                                                        ,@RelationZone
                                                        ,@Ordinary
                                                        ,@Invisibled
                                                        ,@IsHightLight
                                                        ,@IsHightLightOnMobile
                                                        ,@ViewCount
                                                        ,@NewsCoverId);
                                                SET IDENTITY_INSERT [Thread] OFF;";

                    var cmd = new SqlCommand(query, con);
                    //cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandType = CommandType.Text;
                    cmd.Parameters.AddWithValue("@Id", item.Id);
                    cmd.Parameters.AddWithValue("@Name", item.Name ?? (object)DBNull.Value);
                    cmd.Parameters.AddWithValue("@UnsignName", item.UnsignName ?? (object)DBNull.Value);
                    cmd.Parameters.AddWithValue("@Title", item.Title ?? (object)DBNull.Value);
                    cmd.Parameters.AddWithValue("@Description", item.Description ?? (object)DBNull.Value);
                    cmd.Parameters.AddWithValue("@Url", item.Url ?? (object)DBNull.Value);
                    cmd.Parameters.AddWithValue("@Avatar", item.Avatar ?? (object)DBNull.Value);
                    cmd.Parameters.AddWithValue("@HomeAvatar", item.HomeAvatar ?? (object)DBNull.Value);
                    cmd.Parameters.AddWithValue("@SpecialAvatar", item.SpecialAvatar ?? (object)DBNull.Value);
                    cmd.Parameters.AddWithValue("@IsHot", item.IsHot);
                    cmd.Parameters.AddWithValue("@IsOnHome", item.IsOnHome);

                    if (item.CreatedDate <= new DateTime(1980, 1, 1))
                        cmd.Parameters.AddWithValue("@CreatedDate", DBNull.Value);
                    else
                        cmd.Parameters.AddWithValue("@CreatedDate", item.CreatedDate);

                    if (item.ModifiedDate <= new DateTime(1980, 1, 1))
                        cmd.Parameters.AddWithValue("@ModifiedDate", DBNull.Value);
                    else
                        cmd.Parameters.AddWithValue("@ModifiedDate", item.ModifiedDate);
            
                    cmd.Parameters.AddWithValue("@CreatedBy", item.CreatedBy ?? (object)DBNull.Value);
                    cmd.Parameters.AddWithValue("@EditedBy", item.EditedBy ?? (object)DBNull.Value);
                    cmd.Parameters.AddWithValue("@MetaKeyword", item.MetaKeyword ?? (object)DBNull.Value);
                    cmd.Parameters.AddWithValue("@MetaContent", item.MetaContent ?? (object)DBNull.Value);
                    cmd.Parameters.AddWithValue("@TemplateId", item.TemplateId);
                    cmd.Parameters.AddWithValue("@RelationThread", item.RelationThread ?? (object)DBNull.Value);
                    cmd.Parameters.AddWithValue("@RelationZone", item.RelationZone ?? (object)DBNull.Value);
                    cmd.Parameters.AddWithValue("@Ordinary", item.Ordinary);
                    cmd.Parameters.AddWithValue("@Invisibled", item.Invisibled);
                    cmd.Parameters.AddWithValue("@IsHightLight", item.IsHightLight);
                    cmd.Parameters.AddWithValue("@IsHightLightOnMobile", item.IsHightLightOnMobile);
                    cmd.Parameters.AddWithValue("@ViewCount", item.ViewCount);
                    cmd.Parameters.AddWithValue("@NewsCoverId", item.NewsCoverId);

                    id = item.Id;
                    var reader = cmd.ExecuteNonQuery();
                    count++;

                    if (progress != null)
                    {
                        processEntity.value = count;
                        progress.Report(processEntity);
                    }
                }

                con.Close();

                return count;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message + " ID: " + id.ToString());
                return count;
            }
        }
        #endregion

        #region ThreadInZone
        public static int SyncThreadInZoneToDb(string nameSpace, string connectStr1, string connectStr2, int pageIndex, int pageSize, IProgress<InitDataDb.ProcessEntity> progress, InitDataDb.ProcessEntity processEntity)
        {
            try
            {
                string query = @"DECLARE @UpperBand int, @LowerBand int
                                SET @LowerBand  = (@pageIndex - 1) * @PageSize
                                SET @UpperBand  = (@pageIndex * @PageSize)
                                SELECT * FROM (
                                SELECT n.*,ROW_NUMBER() OVER(ORDER BY n.ZoneId DESC) AS RowNumber 
                                FROM [ThreadInZone] n
                                ) AS temp
                                WHERE RowNumber > @LowerBand AND RowNumber <= @UpperBand";

                //test Id=20161214035128196
                //query = @"SELECT * FROM News WHERE Id=20161214035128196";

                var con = GetConnection(connectStr1);
                var cmd = new SqlCommand(query, con);
                //cmd.CommandType = CommandType.StoredProcedure;               
                var dateFrom = DBNull.Value;
                var dateTo = DBNull.Value;
                cmd.Parameters.AddWithValue("@PageIndex", pageIndex);
                cmd.Parameters.AddWithValue("@PageSize", pageSize);
                cmd.Parameters.AddWithValue("@DateFrom", dateFrom);
                cmd.Parameters.AddWithValue("@DateTo", dateTo);

                con.Open();
                var reader = cmd.ExecuteReader();

                var data = EntityMapper.FillCollection<ThreadInZoneEntity>(reader);

                con.Close();

                var abc = InitThreadInZoneToDb(nameSpace, connectStr2, data, progress, processEntity);

                return abc;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return 0;
            }
        }
        private static int InitThreadInZoneToDb(string nameSpace, string connectStr2, List<ThreadInZoneEntity> data, IProgress<InitDataDb.ProcessEntity> progress, InitDataDb.ProcessEntity processEntity)
        {
            long id = 0;
            var count = 0;
            try
            {
                var con = GetConnection(connectStr2);
                con.Open();

                foreach (var item in data)
                {
                    string query = @"DELETE [ThreadInZone] WHERE ZoneId=@ZoneId and ThreadId=@ThreadId;
                                    INSERT INTO [ThreadInZone]([ZoneId]
                                                        ,[ThreadId]
                                                        ,[CreatedDate]
                                                        ,[IsPrimary]
                                                        ,[Ordinary]
                                                ) VALUES(@ZoneId
                                                        ,@ThreadId
                                                        ,@CreatedDate
                                                        ,@IsPrimary
                                                        ,@Ordinary);";

                    var cmd = new SqlCommand(query, con);
                    //cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandType = CommandType.Text;
                    cmd.Parameters.AddWithValue("@ZoneId", item.ZoneId);
                    cmd.Parameters.AddWithValue("@ThreadId", item.ThreadId);
                    
                    if (item.CreatedDate <= new DateTime(1980, 1, 1))
                        cmd.Parameters.AddWithValue("@CreatedDate", DBNull.Value);
                    else
                        cmd.Parameters.AddWithValue("@CreatedDate", item.CreatedDate);
                    
                    cmd.Parameters.AddWithValue("@IsPrimary", item.IsPrimary);
                    cmd.Parameters.AddWithValue("@Ordinary", item.Ordinary);                    

                    id = item.ZoneId;
                    var reader = cmd.ExecuteNonQuery();
                    count++;

                    if (progress != null)
                    {
                        processEntity.value = count;
                        progress.Report(processEntity);
                    }
                }

                con.Close();

                return count;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message + " ID: " + id.ToString());
                return count;
            }
        }
        public static int GetCountThreadInZone(string connectStr)
        {
            try
            {
                string query = @"SELECT COUNT(*) as [Count] FROM [ThreadInZone]";

                var con = GetConnection(connectStr);
                var cmd = new SqlCommand(query, con);

                con.Open();
                var reader = cmd.ExecuteReader();

                var data = EntityMapper.FillCollection<CountNewsEntity>(reader);

                con.Close();

                var count = 0;
                if (data != null && data.Count > 0)
                {
                    count = data.FirstOrDefault().Count;
                }

                return count;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return 0;                
            }
        }
        #endregion

        #region ThreadNews
        public static int SyncThreadNewsToDb(string nameSpace, string connectStr1, string connectStr2, int pageIndex, int pageSize, IProgress<InitDataDb.ProcessEntity> progress, InitDataDb.ProcessEntity processEntity)
        {
            try
            {
                string query = @"DECLARE @UpperBand int, @LowerBand int
                                SET @LowerBand  = (@pageIndex - 1) * @PageSize
                                SET @UpperBand  = (@pageIndex * @PageSize)
                                SELECT * FROM (
                                SELECT n.*,ROW_NUMBER() OVER(ORDER BY n.ThreadId DESC) AS RowNumber 
                                FROM [ThreadNews] n
                                ) AS temp
                                WHERE RowNumber > @LowerBand AND RowNumber <= @UpperBand";

                //test Id=20161214035128196
                //query = @"SELECT * FROM News WHERE Id=20161214035128196";

                var con = GetConnection(connectStr1);
                var cmd = new SqlCommand(query, con);
                //cmd.CommandType = CommandType.StoredProcedure;               
                var dateFrom = DBNull.Value;
                var dateTo = DBNull.Value;
                cmd.Parameters.AddWithValue("@PageIndex", pageIndex);
                cmd.Parameters.AddWithValue("@PageSize", pageSize);
                cmd.Parameters.AddWithValue("@DateFrom", dateFrom);
                cmd.Parameters.AddWithValue("@DateTo", dateTo);

                con.Open();
                var reader = cmd.ExecuteReader();

                var data = EntityMapper.FillCollection<ThreadNewsEntity>(reader);

                con.Close();

                var abc = InitThreadNewsToDb(nameSpace, connectStr2, data, progress, processEntity);

                return abc;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return 0;
            }
        }
        private static int InitThreadNewsToDb(string nameSpace, string connectStr2, List<ThreadNewsEntity> data, IProgress<InitDataDb.ProcessEntity> progress, InitDataDb.ProcessEntity processEntity)
        {
            long id = 0;
            var count = 0;
            try
            {
                var con = GetConnection(connectStr2);
                con.Open();

                foreach (var item in data)
                {
                    string query = @"DELETE [ThreadNews] WHERE NewsId=@NewsId and ThreadId=@ThreadId;
                                    INSERT INTO [ThreadNews]([ThreadId]
                                                          ,[NewsId]
                                                          ,[Ordinary]
                                                          ,[LastModifiedDate]
                                                ) VALUES(@ThreadId
                                                        ,@NewsId
                                                        ,@Ordinary
                                                        ,@LastModifiedDate);";

                    var cmd = new SqlCommand(query, con);
                    //cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandType = CommandType.Text;                    
                    cmd.Parameters.AddWithValue("@ThreadId", item.ThreadId);
                    cmd.Parameters.AddWithValue("@NewsId", item.NewsId);
                    cmd.Parameters.AddWithValue("@Ordinary", item.Ordinary);

                    if (item.LastModifiedDate <= new DateTime(1980, 1, 1))
                        cmd.Parameters.AddWithValue("@LastModifiedDate", DBNull.Value);
                    else
                        cmd.Parameters.AddWithValue("@LastModifiedDate", item.LastModifiedDate);

                    id = item.ThreadId;
                    var reader = cmd.ExecuteNonQuery();
                    count++;

                    if (progress != null)
                    {
                        processEntity.value = count;
                        progress.Report(processEntity);
                    }
                }

                con.Close();

                return count;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message + " ID: " + id.ToString());
                return count;
            }
        }
        public static int GetCountThreadNews(string connectStr)
        {
            try
            {
                string query = @"SELECT COUNT(*) as [Count] FROM [ThreadNews]";

                var con = GetConnection(connectStr);
                var cmd = new SqlCommand(query, con);

                con.Open();
                var reader = cmd.ExecuteReader();

                var data = EntityMapper.FillCollection<CountNewsEntity>(reader);

                con.Close();

                var count = 0;
                if (data != null && data.Count > 0)
                {
                    count = data.FirstOrDefault().Count;
                }

                return count;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return 0;                
            }
        }
        #endregion

        #region ThreadRelation
        public static int SyncThreadRelationToDb(string nameSpace, string connectStr1, string connectStr2, int pageIndex, int pageSize, IProgress<InitDataDb.ProcessEntity> progress, InitDataDb.ProcessEntity processEntity)
        {
            try
            {
                string query = @"DECLARE @UpperBand int, @LowerBand int
                                SET @LowerBand  = (@pageIndex - 1) * @PageSize
                                SET @UpperBand  = (@pageIndex * @PageSize)
                                SELECT * FROM (
                                SELECT n.*,ROW_NUMBER() OVER(ORDER BY n.ThreadId DESC) AS RowNumber 
                                FROM [ThreadRelation] n
                                ) AS temp
                                WHERE RowNumber > @LowerBand AND RowNumber <= @UpperBand";

                //test Id=20161214035128196
                //query = @"SELECT * FROM News WHERE Id=20161214035128196";

                var con = GetConnection(connectStr1);
                var cmd = new SqlCommand(query, con);
                //cmd.CommandType = CommandType.StoredProcedure;               
                var dateFrom = DBNull.Value;
                var dateTo = DBNull.Value;
                cmd.Parameters.AddWithValue("@PageIndex", pageIndex);
                cmd.Parameters.AddWithValue("@PageSize", pageSize);
                cmd.Parameters.AddWithValue("@DateFrom", dateFrom);
                cmd.Parameters.AddWithValue("@DateTo", dateTo);

                con.Open();
                var reader = cmd.ExecuteReader();

                var data = EntityMapper.FillCollection<ThreadRelationEntity>(reader);

                con.Close();

                var abc = InitThreadRelationToDb(nameSpace, connectStr2, data, progress, processEntity);

                return abc;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return 0;
            }
        }
        private static int InitThreadRelationToDb(string nameSpace, string connectStr2, List<ThreadRelationEntity> data, IProgress<InitDataDb.ProcessEntity> progress, InitDataDb.ProcessEntity processEntity)
        {
            long id = 0;
            var count = 0;
            try
            {
                var con = GetConnection(connectStr2);
                con.Open();

                foreach (var item in data)
                {
                    string query = @"DELETE [ThreadRelation] WHERE ThreadRelationId=@ThreadRelationId and ThreadId=@ThreadId;
                                    INSERT INTO [ThreadRelation]([ThreadId]
                                                          ,[ThreadRelationId]
                                                          ,[Ordinary]                                                          
                                                ) VALUES(@ThreadId
                                                        ,@ThreadRelationId
                                                        ,@Ordinary);";

                    var cmd = new SqlCommand(query, con);
                    //cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandType = CommandType.Text;
                    cmd.Parameters.AddWithValue("@ThreadId", item.ThreadId);
                    cmd.Parameters.AddWithValue("@ThreadRelationId", item.ThreadRelationId);
                    cmd.Parameters.AddWithValue("@Ordinary", item.Ordinary);
                    
                    id = item.ThreadId;
                    var reader = cmd.ExecuteNonQuery();
                    count++;

                    if (progress != null)
                    {
                        processEntity.value = count;
                        progress.Report(processEntity);
                    }
                }

                con.Close();

                return count;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message + " ID: " + id.ToString());
                return count;
            }
        }
        public static int GetCountThreadRelation(string connectStr)
        {
            try
            {
                string query = @"SELECT COUNT(*) as [Count] FROM [ThreadRelation]";

                var con = GetConnection(connectStr);
                var cmd = new SqlCommand(query, con);

                con.Open();
                var reader = cmd.ExecuteReader();

                var data = EntityMapper.FillCollection<CountNewsEntity>(reader);

                con.Close();

                var count = 0;
                if (data != null && data.Count > 0)
                {
                    count = data.FirstOrDefault().Count;
                }

                return count;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return 0;
            }
        }
        #endregion

        #endregion
    }
}
