﻿using IMS2.SYNCDATA.Common;
using IMS2.SYNCDATA.Entity;
using IMS2.SYNCDATA.ES;
using IMS2.SYNCDATA.Redis;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Windows.Forms;

namespace IMS2.SYNCDATA.SQL
{
    public class TagBo
    {
        public static SqlConnection GetConnection(string conn="")
        {
            if (string.IsNullOrEmpty(conn))
            {
                conn = "Data Source=172.16.41.125;Initial Catalog=IMS2_AFAMILY_DEV;Persist Security Info=True;User ID=ims_test;Password=ImsTest123$%^789";
            }            
            return new SqlConnection(conn);            
        }

        public static int GetCountNewsInTag(string connectStr, long TagId)
        {
            try
            {
                string query = @"SELECT COUNT(*) as [Count] FROM [TagNews] Where TagID="+ TagId;

                var con = GetConnection(connectStr);
                var cmd = new SqlCommand(query, con);

                con.Open();
                var reader = cmd.ExecuteReader();

                var data = EntityMapper.FillCollection<CountNewsEntity>(reader);

                con.Close();

                var count = 0;
                if (data != null && data.Count > 0)
                {
                    count = data.FirstOrDefault().Count;
                }

                return count;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return 0;                
            }
        }

        public static int SyncTagToEs(string nameSpace, string connectStr, string connectEs, int pageIndex, int pageSize)
        {
            try
            {                
                //string query = @"CMS_Tag_InitESAllTag";
                string query = @"DECLARE @UpperBand int, @LowerBand int

                                SELECT @totalRow = COUNT(*) FROM Tag T where (@DateFrom is null and (1=1)) or (@DateFrom is not null and (CONVERT(datetime,T.CreatedDate) >= CONVERT(datetime,@DateFrom) AND CONVERT(datetime,T.CreatedDate)<=CONVERT(datetime,@DateTo)))

                                SET @LowerBand  = (@pageIndex - 1) * @PageSize
                                SET @UpperBand  = (@pageIndex * @PageSize)
                                SELECT * FROM (
                                SELECT T.Id, T.ParentId, T.Name, T.Description, T.Url, T.Invisibled, T.IsHotTag,
	                                T.Type, T.CreatedDate, T.ModifiedDate, T.CreatedBy, T.EditedBy, T.UnsignName,
	                                T.IsThread, T.Avatar, T.Priority, T.TemplateId, TZ.ZoneId,
	                                Z.Name AS ZoneName, count(TN.TagId) as NewsCount,
	                                ROW_NUMBER() OVER(ORDER BY T.Id DESC) AS RowNumber 
                                FROM Tag T 
                                left join TagZone TZ ON T.Id = TZ.TagId
                                left join Zone Z ON Z.Id = TZ.ZoneId
                                left join TagNews TN on TN.TagId=T.Id
                                where (@DateFrom is null and (1=1)) or (@DateFrom is not null and (CONVERT(datetime,T.CreatedDate) >= CONVERT(datetime,@DateFrom) AND CONVERT(datetime,T.CreatedDate)<=CONVERT(datetime,@DateTo)))
                                group by T.Id, T.ParentId, T.Name, T.Description, T.Url, T.Invisibled, T.IsHotTag,
	                                T.Type, T.CreatedDate, T.ModifiedDate, T.CreatedBy, T.EditedBy, T.UnsignName,
	                                T.IsThread, T.Avatar, T.Priority, T.TemplateId, TZ.ZoneId,Z.Name
                                ) AS temp
                                WHERE RowNumber > @LowerBand AND RowNumber <= @UpperBand";

                var con = GetConnection(connectStr);                                
                var cmd = new SqlCommand(query, con);
                //cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandType = CommandType.Text;
                var dateFrom = DBNull.Value;
                var dateTo = DBNull.Value;
                var totalRows = 0;

                var totalRow = cmd.CreateParameter();
                totalRow.ParameterName = "@TotalRow";
                totalRow.Value = totalRows;
                totalRow.Direction = ParameterDirection.Output;

                cmd.Parameters.Add(totalRow);
                cmd.Parameters.AddWithValue("@PageIndex", pageIndex);
                cmd.Parameters.AddWithValue("@PageSize", pageSize);
                cmd.Parameters.AddWithValue("@DateFrom", dateFrom);
                cmd.Parameters.AddWithValue("@DateTo", dateTo);
                
                con.Open();
                var reader = cmd.ExecuteReader();                

                var data = EntityMapper.FillCollection<TagEntity>(reader);

                con.Close();

                var list = data.Select(s => new TagSearchEntity
                {
                    Id = s.Id,
                    Name = s.Name,
                    ZoneId = s.ZoneId,
                    CreatedDate = s.CreatedDate,
                    IsHotTag = s.IsHotTag,
                    IsThread = s.IsThread,
                    ParentId = s.ParentId,
                    Avatar = s.Avatar,
                    CreatedBy = s.CreatedBy,
                    Invisibled = s.Invisibled,
                    NewsCount = s.NewsCount,//GetCountNewsInTag(connectStr, s.Id),
                    Status = s.Status,
                    Url = s.Url,
                    ZoneName=s.ZoneName
                }).ToList();

                if (pageIndex == 1)
                {
                    EsNewsBo.CreateIndexSettings<TagSearchEntity>(nameSpace, connectEs, "name");
                }
                var abc= EsNewsBo.CreateMany(nameSpace, connectEs, list);

                return 1;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return 0;                
            }
        }

        public static int GetCountTag(string connectStr)
        {
            try
            {
                string query = @"SELECT COUNT(*) as [Count] FROM [Tag]";

                var con = GetConnection(connectStr);
                var cmd = new SqlCommand(query, con);                               

                con.Open();
                var reader = cmd.ExecuteReader();

                var data = EntityMapper.FillCollection<CountNewsEntity>(reader);

                con.Close();

                var count = 0;
                if(data!=null && data.Count > 0)
                {
                    count = data.FirstOrDefault().Count;
                }

                return count;
            }
            catch (Exception ex)
            {
                return 0;
                //MessageBox.Show(ex.Message);
            }
        }

        public static int SyncTagToRedis(string nameSpace, string connectStr, string connectRedis, int pageIndex, int pageSize)
        {           
            try
            {
                //string query = @"CMS_Tag_InitESAllTag";
                string query = @"DECLARE @UpperBand int, @LowerBand int

                                SELECT @totalRow = COUNT(*) FROM Tag T where (@DateFrom is null and (1=1)) or (@DateFrom is not null and (CONVERT(datetime,T.CreatedDate) >= CONVERT(datetime,@DateFrom) AND CONVERT(datetime,T.CreatedDate)<=CONVERT(datetime,@DateTo)))

                                SET @LowerBand  = (@pageIndex - 1) * @PageSize
                                SET @UpperBand  = (@pageIndex * @PageSize)
                                SELECT * FROM (
                                SELECT T.Id, T.ParentId, T.Name, T.Description, T.Url, T.Invisibled, T.IsHotTag,
	                                T.Type, T.CreatedDate, T.ModifiedDate, T.CreatedBy, T.EditedBy, T.UnsignName,
	                                T.IsThread, T.Avatar, T.Priority, T.TemplateId, TZ.ZoneId,
	                                Z.Name AS ZoneName, count(TN.TagId) as NewsCount,
	                                ROW_NUMBER() OVER(ORDER BY T.Id DESC) AS RowNumber 
                                FROM Tag T 
                                left join TagZone TZ ON T.Id = TZ.TagId
                                left join Zone Z ON Z.Id = TZ.ZoneId
                                left join TagNews TN on TN.TagId=T.Id
                                where (@DateFrom is null and (1=1)) or (@DateFrom is not null and (CONVERT(datetime,T.CreatedDate) >= CONVERT(datetime,@DateFrom) AND CONVERT(datetime,T.CreatedDate)<=CONVERT(datetime,@DateTo)))
                                group by T.Id, T.ParentId, T.Name, T.Description, T.Url, T.Invisibled, T.IsHotTag,
	                                T.Type, T.CreatedDate, T.ModifiedDate, T.CreatedBy, T.EditedBy, T.UnsignName,
	                                T.IsThread, T.Avatar, T.Priority, T.TemplateId, TZ.ZoneId,Z.Name
                                ) AS temp
                                WHERE RowNumber > @LowerBand AND RowNumber <= @UpperBand";

                var con = GetConnection(connectStr);
                var cmd = new SqlCommand(query, con);
                //cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandType = CommandType.Text;
                var dateFrom = DBNull.Value;
                var dateTo = DBNull.Value;
                var totalRows = 0;

                var totalRow = cmd.CreateParameter();
                totalRow.ParameterName = "@TotalRow";
                totalRow.Value = totalRows;
                totalRow.Direction = ParameterDirection.Output;

                cmd.Parameters.Add(totalRow);
                cmd.Parameters.AddWithValue("@PageIndex", pageIndex);
                cmd.Parameters.AddWithValue("@PageSize", pageSize);
                cmd.Parameters.AddWithValue("@DateFrom", dateFrom);
                cmd.Parameters.AddWithValue("@DateTo", dateTo);

                con.Open();
                var reader = cmd.ExecuteReader();

                var listNews = EntityMapper.FillCollection<TagEntity>(reader);

                con.Close();

                var data = new Dictionary<string, TagEntity>();
                foreach (var entry in listNews)
                {
                    data[entry.Id.ToString()] = entry;
                }
                var abc = RedisNewsBo.AddInHash(nameSpace, connectRedis, data);

                return 1;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return 0;
            }
        }

        #region Afamily

        #region Tag
        public static int SyncTagToDb(string nameSpace, string connectStr1, string connectStr2, int pageIndex, int pageSize, IProgress<InitDataDb.ProcessEntity> progress, InitDataDb.ProcessEntity processEntity)
        {
            try
            {
                string query = @"DECLARE @UpperBand int, @LowerBand int
                                SET @LowerBand  = (@pageIndex - 1) * @PageSize
                                SET @UpperBand  = (@pageIndex * @PageSize)
                                SELECT * FROM (
                                SELECT n.*,ROW_NUMBER() OVER(ORDER BY n.Id DESC) AS RowNumber 
                                FROM [Tag] n
                                ) AS temp
                                WHERE RowNumber > @LowerBand AND RowNumber <= @UpperBand";

                //test Id=20161214035128196
                //query = @"SELECT * FROM News WHERE Id=20161214035128196";

                var con = GetConnection(connectStr1);
                var cmd = new SqlCommand(query, con);
                //cmd.CommandType = CommandType.StoredProcedure;               
                var dateFrom = DBNull.Value;
                var dateTo = DBNull.Value;
                cmd.Parameters.AddWithValue("@PageIndex", pageIndex);
                cmd.Parameters.AddWithValue("@PageSize", pageSize);
                cmd.Parameters.AddWithValue("@DateFrom", dateFrom);
                cmd.Parameters.AddWithValue("@DateTo", dateTo);

                con.Open();
                var reader = cmd.ExecuteReader();

                var data = EntityMapper.FillCollection<TagEntity>(reader);

                con.Close();

                var abc = InitTagToDb(nameSpace, connectStr2, data, progress, processEntity);

                return abc;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return 0;
            }
        }
        private static int InitTagToDb(string nameSpace, string connectStr2, List<TagEntity> data, IProgress<InitDataDb.ProcessEntity> progress, InitDataDb.ProcessEntity processEntity)
        {
            long id = 0;
            var count = 0;
            try
            {
                var con = GetConnection(connectStr2);
                con.Open();

                foreach (var item in data)
                {
                    string query = @"SET IDENTITY_INSERT [Tag] ON;
                                    DELETE [Tag] WHERE Id=@Id;
                                    INSERT INTO [Tag]([Id]
                                                      ,[ParentId]
                                                      ,[Name]
                                                      ,[Description]
                                                      ,[Url]
                                                      ,[Invisibled]
                                                      ,[IsHotTag]
                                                      ,[Type]
                                                      ,[CreatedDate]
                                                      ,[ModifiedDate]
                                                      ,[CreatedBy]
                                                      ,[EditedBy]
                                                      ,[UnsignName]
                                                      ,[IsThread]
                                                      ,[Avatar]
                                                      ,[Priority]
                                                      ,[TagContent]
                                                      ,[TagTitle]
                                                      ,[TagInit]
                                                      ,[TagMetaKeyword]
                                                      ,[TagMetaContent]
                                                      ,[TemplateId]
                                                      ,[ViewCount]
                                                      ,[SubTitle]
                                                      ,[SubTile]
                                                      ,[NewsCoverId]
                                                      ,[Status]
                                                      ,[CountNewsInTag]
                                                ) VALUES(@Id
                                                        ,@ParentId
                                                        ,@Name
                                                        ,@Description
                                                        ,@Url
                                                        ,@Invisibled
                                                        ,@IsHotTag
                                                        ,@Type
                                                        ,@CreatedDate
                                                        ,@ModifiedDate
                                                        ,@CreatedBy
                                                        ,@EditedBy
                                                        ,@UnsignName
                                                        ,@IsThread
                                                        ,@Avatar
                                                        ,@Priority
                                                        ,@TagContent
                                                        ,@TagTitle
                                                        ,@TagInit
                                                        ,@TagMetaKeyword
                                                        ,@TagMetaContent
                                                        ,@TemplateId
                                                        ,@ViewCount
                                                        ,@SubTitle
                                                        ,@SubTile
                                                        ,@NewsCoverId
                                                        ,@Status
                                                        ,@CountNewsInTag);
                                                SET IDENTITY_INSERT [Tag] OFF;";

                    var cmd = new SqlCommand(query, con);
                    //cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandType = CommandType.Text;
                    cmd.Parameters.AddWithValue("@Id", item.Id);
                    cmd.Parameters.AddWithValue("@ParentId", item.ParentId);
                    cmd.Parameters.AddWithValue("@Name", item.Name ?? (object)DBNull.Value);
                    cmd.Parameters.AddWithValue("@Description", item.Description ?? (object)DBNull.Value);
                    cmd.Parameters.AddWithValue("@Url", item.Url ?? (object)DBNull.Value);
                    cmd.Parameters.AddWithValue("@Invisibled", item.Invisibled);
                    cmd.Parameters.AddWithValue("@IsHotTag", item.IsHotTag);
                    cmd.Parameters.AddWithValue("@Type", item.Type);

                    if (item.CreatedDate <= new DateTime(1980, 1, 1))
                        cmd.Parameters.AddWithValue("@CreatedDate", DBNull.Value);
                    else
                        cmd.Parameters.AddWithValue("@CreatedDate", item.CreatedDate);

                    if (item.ModifiedDate <= new DateTime(1980, 1, 1))
                        cmd.Parameters.AddWithValue("@ModifiedDate", DBNull.Value);
                    else
                        cmd.Parameters.AddWithValue("@ModifiedDate", item.ModifiedDate);
                    
                    cmd.Parameters.AddWithValue("@CreatedBy", item.CreatedBy ?? (object)DBNull.Value);
                    cmd.Parameters.AddWithValue("@EditedBy", item.EditedBy ?? (object)DBNull.Value);
                    cmd.Parameters.AddWithValue("@UnsignName", item.UnsignName ?? (object)DBNull.Value);
                    cmd.Parameters.AddWithValue("@IsThread", item.IsThread);
                    cmd.Parameters.AddWithValue("@Avatar", item.Avatar ?? (object)DBNull.Value);
                    cmd.Parameters.AddWithValue("@Priority", item.Priority);
                    cmd.Parameters.AddWithValue("@TagContent", item.TagContent ?? (object)DBNull.Value);
                    cmd.Parameters.AddWithValue("@TagTitle", item.TagTitle ?? (object)DBNull.Value);
                    cmd.Parameters.AddWithValue("@TagInit", item.TagInit ?? (object)DBNull.Value);
                    cmd.Parameters.AddWithValue("@TagMetaKeyword", item.TagMetaKeyword ?? (object)DBNull.Value);
                    cmd.Parameters.AddWithValue("@TagMetaContent", item.TagMetaContent ?? (object)DBNull.Value);

                    if (item.TemplateId <= 0)
                        cmd.Parameters.AddWithValue("@TemplateId", 0);
                    else
                        cmd.Parameters.AddWithValue("@TemplateId", item.TemplateId);
                    
                    cmd.Parameters.AddWithValue("@ViewCount", item.ViewCount);
                    cmd.Parameters.AddWithValue("@SubTitle", item.SubTitle ?? (object)DBNull.Value);
                    cmd.Parameters.AddWithValue("@SubTile", item.SubTile ?? (object)DBNull.Value);
                    cmd.Parameters.AddWithValue("@NewsCoverId", item.NewsCoverId);
                    cmd.Parameters.AddWithValue("@Status", item.Status);
                    cmd.Parameters.AddWithValue("@CountNewsInTag", item.CountNewsInTag);

                    id = item.Id;
                    var reader = cmd.ExecuteNonQuery();
                    count++;

                    if (progress != null)
                    {
                        processEntity.value = count;
                        progress.Report(processEntity);
                    }
                }

                con.Close();

                return count;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message + " ID: " + id.ToString());
                return count;
            }
        }
        #endregion

        #region TagNews
        public static int SyncTagNewsToDb(string nameSpace, string connectStr1, string connectStr2, int pageIndex, int pageSize, IProgress<InitDataDb.ProcessEntity> progress, InitDataDb.ProcessEntity processEntity)
        {
            try
            {
                string query = @"DECLARE @UpperBand int, @LowerBand int
                                SET @LowerBand  = (@pageIndex - 1) * @PageSize
                                SET @UpperBand  = (@pageIndex * @PageSize)
                                SELECT * FROM (
                                SELECT n.*,ROW_NUMBER() OVER(ORDER BY n.TagID DESC) AS RowNumber 
                                FROM [TagNews] n
                                ) AS temp
                                WHERE RowNumber > @LowerBand AND RowNumber <= @UpperBand";

                //test Id=20161214035128196
                //query = @"SELECT * FROM News WHERE Id=20161214035128196";

                var con = GetConnection(connectStr1);
                var cmd = new SqlCommand(query, con);
                //cmd.CommandType = CommandType.StoredProcedure;               
                var dateFrom = DBNull.Value;
                var dateTo = DBNull.Value;
                cmd.Parameters.AddWithValue("@PageIndex", pageIndex);
                cmd.Parameters.AddWithValue("@PageSize", pageSize);
                cmd.Parameters.AddWithValue("@DateFrom", dateFrom);
                cmd.Parameters.AddWithValue("@DateTo", dateTo);

                con.Open();
                var reader = cmd.ExecuteReader();

                var data = EntityMapper.FillCollection<TagNewsEntity>(reader);

                con.Close();

                var abc = InitTagNewsToDb(nameSpace, connectStr2, data, progress, processEntity);

                return abc;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return 0;
            }
        }
        private static int InitTagNewsToDb(string nameSpace, string connectStr2, List<TagNewsEntity> data, IProgress<InitDataDb.ProcessEntity> progress, InitDataDb.ProcessEntity processEntity)
        {
            long id = 0;
            var count = 0;
            try
            {
                var con = GetConnection(connectStr2);
                con.Open();

                foreach (var item in data)
                {
                    string query = @"DELETE [TagNews] WHERE TagID=@TagID and NewsID=@NewsID and TagMode=@TagMode;
                                    INSERT INTO [TagNews]([TagID]
                                                      ,[NewsID]
                                                      ,[TagMode]
                                                      ,[TagProperty]
                                                      ,[Priority]
                                                ) VALUES(@TagID
                                                        ,@NewsID
                                                        ,@TagMode
                                                        ,@TagProperty
                                                        ,@Priority);
                                               ";

                    var cmd = new SqlCommand(query, con);
                    //cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandType = CommandType.Text;
                    cmd.Parameters.AddWithValue("@TagID", item.TagID);
                    cmd.Parameters.AddWithValue("@NewsID", item.NewsID);
                    if (item.TagMode < 0)
                        item.TagMode = 0;
                    cmd.Parameters.AddWithValue("@TagMode", item.TagMode);
                    cmd.Parameters.AddWithValue("@TagProperty", item.TagProperty);
                    cmd.Parameters.AddWithValue("@Priority", item.Priority);

                    id = item.TagID;
                    var reader = cmd.ExecuteNonQuery();
                    count++;

                    if (progress != null)
                    {
                        processEntity.value = count;
                        progress.Report(processEntity);
                    }
                }

                con.Close();

                return count;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message + " ID: " + id.ToString());
                return count;
            }
        }
        public static int GetCountTagNews(string connectStr)
        {
            try
            {
                string query = @"SELECT COUNT(*) as [Count] FROM [TagNews]";

                var con = GetConnection(connectStr);
                var cmd = new SqlCommand(query, con);

                con.Open();
                var reader = cmd.ExecuteReader();

                var data = EntityMapper.FillCollection<CountNewsEntity>(reader);

                con.Close();

                var count = 0;
                if (data != null && data.Count > 0)
                {
                    count = data.FirstOrDefault().Count;
                }

                return count;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return 0;                
            }
        }
        #endregion

        #region TagZone
        public static int SyncTagZoneToDb(string nameSpace, string connectStr1, string connectStr2, int pageIndex, int pageSize, IProgress<InitDataDb.ProcessEntity> progress, InitDataDb.ProcessEntity processEntity)
        {
            try
            {
                string query = @"DECLARE @UpperBand int, @LowerBand int
                                SET @LowerBand  = (@pageIndex - 1) * @PageSize
                                SET @UpperBand  = (@pageIndex * @PageSize)
                                SELECT * FROM (
                                SELECT n.*,ROW_NUMBER() OVER(ORDER BY n.TagId DESC) AS RowNumber 
                                FROM [TagZone] n
                                ) AS temp
                                WHERE RowNumber > @LowerBand AND RowNumber <= @UpperBand";

                //test Id=20161214035128196
                //query = @"SELECT * FROM News WHERE Id=20161214035128196";

                var con = GetConnection(connectStr1);
                var cmd = new SqlCommand(query, con);
                //cmd.CommandType = CommandType.StoredProcedure;               
                var dateFrom = DBNull.Value;
                var dateTo = DBNull.Value;
                cmd.Parameters.AddWithValue("@PageIndex", pageIndex);
                cmd.Parameters.AddWithValue("@PageSize", pageSize);
                cmd.Parameters.AddWithValue("@DateFrom", dateFrom);
                cmd.Parameters.AddWithValue("@DateTo", dateTo);

                con.Open();
                var reader = cmd.ExecuteReader();

                var data = EntityMapper.FillCollection<TagZoneEntity>(reader);

                con.Close();

                var abc = InitTagZoneToDb(nameSpace, connectStr2, data, progress, processEntity);

                return abc;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return 0;
            }
        }
        private static int InitTagZoneToDb(string nameSpace, string connectStr2, List<TagZoneEntity> data, IProgress<InitDataDb.ProcessEntity> progress, InitDataDb.ProcessEntity processEntity)
        {
            long id = 0;
            var count = 0;
            try
            {
                var con = GetConnection(connectStr2);
                con.Open();

                foreach (var item in data)
                {
                    string query = @"DELETE [TagZone] WHERE TagId=@TagId and ZoneId=@ZoneId;
                                    INSERT INTO [TagZone]([TagId]
                                                        ,[ZoneId]
                                                        ,[IsPrimary]
                                                ) VALUES(@TagId
                                                        ,@ZoneId
                                                        ,@IsPrimary);
                                               ";

                    var cmd = new SqlCommand(query, con);
                    //cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandType = CommandType.Text;
                    cmd.Parameters.AddWithValue("@TagId", item.TagId);
                    cmd.Parameters.AddWithValue("@ZoneId", item.ZoneId);                    
                    cmd.Parameters.AddWithValue("@IsPrimary", item.IsPrimary);
                    
                    id = item.TagId;
                    var reader = cmd.ExecuteNonQuery();
                    count++;

                    if (progress != null)
                    {
                        processEntity.value = count;
                        progress.Report(processEntity);
                    }
                }

                con.Close();

                return count;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message + " ID: " + id.ToString());
                return count;
            }
        }
        public static int GetCountTagZone(string connectStr)
        {
            try
            {
                string query = @"SELECT COUNT(*) as [Count] FROM [TagZone]";

                var con = GetConnection(connectStr);
                var cmd = new SqlCommand(query, con);

                con.Open();
                var reader = cmd.ExecuteReader();

                var data = EntityMapper.FillCollection<CountNewsEntity>(reader);

                con.Close();

                var count = 0;
                if (data != null && data.Count > 0)
                {
                    count = data.FirstOrDefault().Count;
                }

                return count;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return 0;
            }
        }
        #endregion

        #endregion

        #region TagId

        public static List<TagEntity> GetDataTag(string connectStr, string tagIds)
        {
            var con = GetConnection(connectStr);
            try
            {
                var array = tagIds.Split(',');
                string query = @"SELECT * FROM Tag where Id in(" + tagIds + ")";

                using (var cmd = new SqlCommand(query, con))
                {                    
                    con.Open();
                    var reader = cmd.ExecuteReader();

                    var data = EntityMapper.FillCollection<TagEntity>(reader);

                    con.Close();

                    return data;
                }
            }
            catch (Exception ex)
            {
                Logger.Log(TypeLog.Error, "GetDataTag => " + ex.Message);
                return new List<TagEntity>();
            }
            finally
            {
                con.Close();
                con.Dispose();
            }
        }

        public static int SyncDataTag(string connectStr1, string connectStr2, List<TagEntity> data, string nameSpaceEs, string connectEs, string connectRedis, IProgress<InitDataDb.ProcessEntity> progress)
        {
            try
            {
                var processEntity = new InitDataDb.ProcessEntity();
                processEntity.page = data.Count;
                var count = 0;
                foreach (var item in data)
                {
                    try
                    {
                        if (item != null)
                        {
                            //Tag
                            var resTag = SyncTag(connectStr2, item);                          

                            if (resTag)
                            {                                
                                //sync es, redis
                                SyncTagEsAndReDist(connectStr2, item, nameSpaceEs, connectEs, connectRedis);

                                count++;
                                //log ket qua
                                if (count == data.Count)
                                {
                                    Logger.WriteFile(" => Totall: " + data.Count + " => Success: " + count, "ketquasync.tagid");
                                }

                                if (progress != null)
                                {
                                    processEntity.value = count;
                                    progress.Report(processEntity);
                                }
                            }
                        }
                        else
                        {
                            Logger.Log(TypeLog.Bug, "SyncDataTag => item is null");
                        }
                    }
                    catch (Exception ex)
                    {
                        Logger.Log(TypeLog.Error, "SyncDataTag => " + ex.Message);
                    }
                }
                return count;
            }
            catch (Exception ex)
            {
                Logger.Log(TypeLog.Error, "SyncDataTag for=> " + ex.Message);
                return 0;
            }
        }
        private static bool SyncTag(string connectStr, TagEntity item)
        {
            var con = GetConnection(connectStr);
            try
            {
                string query = @"SET IDENTITY_INSERT [Tag] ON;
                                    DELETE [Tag] WHERE Id=@Id;
                                    INSERT INTO [Tag]([Id]
                                                      ,[ParentId]
                                                      ,[Name]
                                                      ,[Description]
                                                      ,[Url]
                                                      ,[Invisibled]
                                                      ,[IsHotTag]
                                                      ,[Type]
                                                      ,[CreatedDate]
                                                      ,[ModifiedDate]
                                                      ,[CreatedBy]
                                                      ,[EditedBy]
                                                      ,[UnsignName]
                                                      ,[IsThread]
                                                      ,[Avatar]
                                                      ,[Priority]
                                                      ,[TagContent]
                                                      ,[TagTitle]
                                                      ,[TagInit]
                                                      ,[TagMetaKeyword]
                                                      ,[TagMetaContent]
                                                      ,[TemplateId]
                                                      ,[ViewCount]
                                                      ,[SubTitle]
                                                      ,[SubTile]
                                                      ,[NewsCoverId]
                                                      ,[Status]
                                                      ,[CountNewsInTag]
                                                ) VALUES(@Id
                                                        ,@ParentId
                                                        ,@Name
                                                        ,@Description
                                                        ,@Url
                                                        ,@Invisibled
                                                        ,@IsHotTag
                                                        ,@Type
                                                        ,@CreatedDate
                                                        ,@ModifiedDate
                                                        ,@CreatedBy
                                                        ,@EditedBy
                                                        ,@UnsignName
                                                        ,@IsThread
                                                        ,@Avatar
                                                        ,@Priority
                                                        ,@TagContent
                                                        ,@TagTitle
                                                        ,@TagInit
                                                        ,@TagMetaKeyword
                                                        ,@TagMetaContent
                                                        ,@TemplateId
                                                        ,@ViewCount
                                                        ,@SubTitle
                                                        ,@SubTile
                                                        ,@NewsCoverId
                                                        ,@Status
                                                        ,@CountNewsInTag);
                                                SET IDENTITY_INSERT [Tag] OFF;";

                var cmd = new SqlCommand(query, con);               
                cmd.CommandType = CommandType.Text;

                cmd.Parameters.AddWithValue("@Id", item.Id);
                cmd.Parameters.AddWithValue("@ParentId", item.ParentId);
                cmd.Parameters.AddWithValue("@Name", item.Name ?? (object)DBNull.Value);
                cmd.Parameters.AddWithValue("@Description", item.Description ?? (object)DBNull.Value);
                cmd.Parameters.AddWithValue("@Url", item.Url ?? (object)DBNull.Value);
                cmd.Parameters.AddWithValue("@Invisibled", item.Invisibled);
                cmd.Parameters.AddWithValue("@IsHotTag", item.IsHotTag);
                cmd.Parameters.AddWithValue("@Type", item.Type);

                if (item.CreatedDate <= new DateTime(1980, 1, 1))
                    cmd.Parameters.AddWithValue("@CreatedDate", DBNull.Value);
                else
                    cmd.Parameters.AddWithValue("@CreatedDate", item.CreatedDate);

                if (item.ModifiedDate <= new DateTime(1980, 1, 1))
                    cmd.Parameters.AddWithValue("@ModifiedDate", DBNull.Value);
                else
                    cmd.Parameters.AddWithValue("@ModifiedDate", item.ModifiedDate);

                cmd.Parameters.AddWithValue("@CreatedBy", item.CreatedBy ?? (object)DBNull.Value);
                cmd.Parameters.AddWithValue("@EditedBy", item.EditedBy ?? (object)DBNull.Value);
                cmd.Parameters.AddWithValue("@UnsignName", item.UnsignName ?? (object)DBNull.Value);
                cmd.Parameters.AddWithValue("@IsThread", item.IsThread);
                cmd.Parameters.AddWithValue("@Avatar", item.Avatar ?? (object)DBNull.Value);
                cmd.Parameters.AddWithValue("@Priority", item.Priority);
                cmd.Parameters.AddWithValue("@TagContent", item.TagContent ?? (object)DBNull.Value);
                cmd.Parameters.AddWithValue("@TagTitle", item.TagTitle ?? (object)DBNull.Value);
                cmd.Parameters.AddWithValue("@TagInit", item.TagInit ?? (object)DBNull.Value);
                cmd.Parameters.AddWithValue("@TagMetaKeyword", item.TagMetaKeyword ?? (object)DBNull.Value);
                cmd.Parameters.AddWithValue("@TagMetaContent", item.TagMetaContent ?? (object)DBNull.Value);

                if (item.TemplateId <= 0)
                    cmd.Parameters.AddWithValue("@TemplateId", 0);
                else
                    cmd.Parameters.AddWithValue("@TemplateId", item.TemplateId);

                cmd.Parameters.AddWithValue("@ViewCount", item.ViewCount);
                cmd.Parameters.AddWithValue("@SubTitle", item.SubTitle ?? (object)DBNull.Value);
                cmd.Parameters.AddWithValue("@SubTile", item.SubTile ?? (object)DBNull.Value);
                cmd.Parameters.AddWithValue("@NewsCoverId", item.NewsCoverId);
                cmd.Parameters.AddWithValue("@Status", item.Status);
                cmd.Parameters.AddWithValue("@CountNewsInTag", item.CountNewsInTag);

                con.Open();
                var reader = cmd.ExecuteNonQuery();

                con.Close();

                return reader > 0;
            }
            catch (Exception ex)
            {
                Logger.Log(TypeLog.Error, "SyncTag => " + ex.Message);
                return false;
            }
            finally
            {
                con.Close();
                con.Dispose();
            }
        }

        private static void SyncTagEsAndReDist(string connectStr, TagEntity s, string nameSpace, string connectEs, string connectRedis)
        {            
            var obj = new TagSearchEntity
            {
                Id = s.Id,
                Name = s.Name,
                ZoneId = s.ZoneId,
                CreatedDate = s.CreatedDate,
                IsHotTag = s.IsHotTag,
                IsThread = s.IsThread,
                ParentId = s.ParentId,
                Avatar = s.Avatar,
                CreatedBy = s.CreatedBy,
                Invisibled = s.Invisibled,
                NewsCount = GetCountNewsInTag(connectStr, s.Id),
                Status = s.Status,
                Url = s.Url,
                ZoneName = s.ZoneName
            };
                        
            EsNewsBo.Create(nameSpace, connectEs, obj);

            var data = new Dictionary<string, TagEntity>();            
            data[s.Id.ToString()] = s;            
            RedisNewsBo.AddInHash(nameSpace, connectRedis, data);
        }
        #endregion
    }
}
