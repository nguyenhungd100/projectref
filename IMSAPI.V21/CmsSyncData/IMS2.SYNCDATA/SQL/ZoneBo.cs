﻿using IMS2.SYNCDATA.Common;
using IMS2.SYNCDATA.Entity;
using IMS2.SYNCDATA.ES;
using IMS2.SYNCDATA.Redis;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Windows.Forms;

namespace IMS2.SYNCDATA.SQL
{
    public class ZoneBo
    {
        public static SqlConnection GetConnection(string conn="")
        {
            if (string.IsNullOrEmpty(conn))
            {
                conn = "Data Source=172.16.41.125;Initial Catalog=IMS2_AFAMILY_DEV;Persist Security Info=True;User ID=ims_test;Password=ImsTest123$%^789";
            }            
            return new SqlConnection(conn);            
        }
               
        public static bool SyncZoneToEs(string nameSpace, string connectStr, string connectEs, int pageIndex, int pageSize)
        {
            try
            {                
                string query = @"CMS_User_InitAllUser";

                var con = GetConnection(connectStr);                                
                var cmd = new SqlCommand(query, con);
                cmd.CommandType = CommandType.StoredProcedure;                
                
                con.Open();
                var reader = cmd.ExecuteReader();                

                var data = EntityMapper.FillCollection<UserEntity>(reader);

                con.Close();

                var list = data.Select(s => new UserSearchEntity
                {
                    Id = s.Id,
                    FullName = s.FullName,
                    UserName = s.UserName,
                    Status = s.Status,
                    CreatedDate = s.CreatedDate
                }).ToList();

                if (pageIndex == 1)
                {
                    EsNewsBo.CreateIndexSettings<UserSearchEntity>(nameSpace, connectEs, "fullname");
                }
                var abc= EsNewsBo.CreateMany(nameSpace, connectEs, list);
                return abc;
            }
            catch (Exception ex)
            {
                return false;
                //MessageBox.Show(ex.Message);
            }
        }

        public static int GetCountZone(string connectStr)
        {
            try
            {
                string query = @"SELECT COUNT(*) as [Count] FROM Zone";

                var con = GetConnection(connectStr);
                var cmd = new SqlCommand(query, con);                               

                con.Open();
                var reader = cmd.ExecuteReader();

                var data = EntityMapper.FillCollection<CountNewsEntity>(reader);

                con.Close();

                var count = 0;
                if(data!=null && data.Count > 0)
                {
                    count = data.FirstOrDefault().Count;
                }

                return count;
            }
            catch (Exception ex)
            {
                return 0;
                //MessageBox.Show(ex.Message);
            }
        }

        public static int SyncZoneToRedis(string nameSpace, string connectStr, string connectRedis, int pageIndex, int pageSize)
        {           
            try
            {
                string query = @"CMS_Zone_GetByParentId";

                var con = GetConnection(connectStr);
                var cmd = new SqlCommand(query, con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@ParentId", -1);

                con.Open();
                var reader = cmd.ExecuteReader();

                var listNews = EntityMapper.FillCollection<ZoneEntity>(reader);

                con.Close();

                var data = new Dictionary<string, ZoneEntity>();
                foreach (var entry in listNews)
                {
                    data[entry.Id.ToString()] = entry;                    
                }
                var abc= RedisNewsBo.AddInHash(nameSpace, connectRedis, data);

                return 1;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return 0;
            }
        }

        #region Afamily zone
        public static int SyncZoneToDb(string nameSpace, string connectStr1, string connectStr2, int pageIndex, int pageSize, IProgress<InitDataDb.ProcessEntity> progress, InitDataDb.ProcessEntity processEntity)
        {
            try
            {
                string query = @"DECLARE @UpperBand int, @LowerBand int
                                SET @LowerBand  = (@pageIndex - 1) * @PageSize
                                SET @UpperBand  = (@pageIndex * @PageSize)
                                SELECT * FROM (
                                SELECT n.*,ROW_NUMBER() OVER(ORDER BY n.Id DESC) AS RowNumber 
                                FROM Zone n
                                ) AS temp
                                WHERE RowNumber > @LowerBand AND RowNumber <= @UpperBand";

                //test Id=20161214035128196
                //query = @"SELECT * FROM News WHERE Id=20161214035128196";

                var con = GetConnection(connectStr1);
                var cmd = new SqlCommand(query, con);
                //cmd.CommandType = CommandType.StoredProcedure;               
                var dateFrom = DBNull.Value;
                var dateTo = DBNull.Value;
                cmd.Parameters.AddWithValue("@PageIndex", pageIndex);
                cmd.Parameters.AddWithValue("@PageSize", pageSize);
                cmd.Parameters.AddWithValue("@DateFrom", dateFrom);
                cmd.Parameters.AddWithValue("@DateTo", dateTo);

                con.Open();
                var reader = cmd.ExecuteReader();

                var data = EntityMapper.FillCollection<ZoneEntity>(reader);

                con.Close();

                var abc = InitZoneToDb(nameSpace, connectStr2, data, progress, processEntity);

                return abc;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return 0;                
            }
        }
        private static int InitZoneToDb(string nameSpace, string connectStr2, List<ZoneEntity> data, IProgress<InitDataDb.ProcessEntity> progress, InitDataDb.ProcessEntity processEntity)
        {
            long id = 0;
            var count = 0;
            try
            {
                var con = GetConnection(connectStr2);
                con.Open();

                foreach (var item in data)
                {
                    string query = @"DELETE [Zone] WHERE Id=@Id;
                                    INSERT INTO [Zone]([Id]
                                                      ,[Name]
                                                      ,[Description]
                                                      ,[ModifiedDate]
                                                      ,[CreatedDate]
                                                      ,[ShortURL]
                                                      ,[SortOrder]
                                                      ,[ParentId]
                                                      ,[Invisibled]
                                                      ,[Status]
                                                      ,[AllowComment]
                                                      ,[Domain]
                                                      ,[UseForFunnyNews]
                                                ) VALUES(@Id
                                                        ,@Name
                                                        ,@Description
                                                        ,@ModifiedDate
                                                        ,@CreatedDate
                                                        ,@ShortURL
                                                        ,@SortOrder
                                                        ,@ParentId
                                                        ,@Invisibled
                                                        ,@Status
                                                        ,@AllowComment
                                                        ,@Domain
                                                        ,@UseForFunnyNews);";

                    var cmd = new SqlCommand(query, con);
                    //cmd.CommandType = CommandType.StoredProcedure;  
                    cmd.CommandType = CommandType.Text;
                    cmd.Parameters.AddWithValue("@Id", item.Id);
                    cmd.Parameters.AddWithValue("@Name", item.Name ?? (object)DBNull.Value);
                    cmd.Parameters.AddWithValue("@Description", item.Description ?? (object)DBNull.Value);

                    if (item.ModifiedDate <= new DateTime(1980, 1, 1))
                        cmd.Parameters.AddWithValue("@ModifiedDate", DBNull.Value);
                    else
                        cmd.Parameters.AddWithValue("@ModifiedDate", item.ModifiedDate);

                    if (item.CreatedDate <= new DateTime(1980, 1, 1))
                        cmd.Parameters.AddWithValue("@CreatedDate", DBNull.Value);
                    else
                        cmd.Parameters.AddWithValue("@CreatedDate", item.CreatedDate);                                       

                    cmd.Parameters.AddWithValue("@ShortURL", item.ShortUrl ?? (object)DBNull.Value);
                    cmd.Parameters.AddWithValue("@SortOrder", item.SortOrder);
                    cmd.Parameters.AddWithValue("@ParentId", item.ParentId);
                    cmd.Parameters.AddWithValue("@Invisibled", item.Invisibled);
                    cmd.Parameters.AddWithValue("@Status", item.Status);                   
                    cmd.Parameters.AddWithValue("@AllowComment", item.AllowComment);
                    cmd.Parameters.AddWithValue("@Domain", item.Domain ?? (object)DBNull.Value);
                    cmd.Parameters.AddWithValue("@UseForFunnyNews", item.UseForFunnyNews);                    

                    id = item.Id;
                    var reader = cmd.ExecuteNonQuery();
                    count++;

                    if (progress != null)
                    {
                        processEntity.value = count;
                        progress.Report(processEntity);
                    }
                }

                con.Close();

                return count;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message + " ID: " + id.ToString());
                return count;
            }
        }        
        #endregion

    }
}
