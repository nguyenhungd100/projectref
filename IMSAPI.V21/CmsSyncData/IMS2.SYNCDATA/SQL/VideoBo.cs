﻿using IMS2.SYNCDATA.Common;
using IMS2.SYNCDATA.Entity;
using IMS2.SYNCDATA.ES;
using IMS2.SYNCDATA.Redis;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Windows.Forms;

namespace IMS2.SYNCDATA.SQL
{
    public class VideoBo
    {
        public static SqlConnection GetConnection(string conn="")
        {
            if (string.IsNullOrEmpty(conn))
            {
                conn = "Data Source=172.16.41.125;Initial Catalog=IMS2_AFAMILY_DEV;Persist Security Info=True;User ID=ims_test;Password=ImsTest123$%^789";
            }            
            return new SqlConnection(conn);            
        }

        #region get
        public static List<ZoneVideoEntity> GetListZoneRelation(string conn, int id)
        {
            try {               
                string query = @"VideoCms_ZoneVideo_GetListRelationVideo";

                var con = GetConnection(conn);
                var cmd = new SqlCommand(query, con);
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.AddWithValue("@VideoId", id);

                con.Open();
                var reader = cmd.ExecuteReader();
                var data= EntityMapper.FillCollection<ZoneVideoEntity>(reader);
                con.Close();

                return data;
            }
            catch (Exception ex)
            {
                return new List<ZoneVideoEntity>();
            }
        }
        public static List<PlaylistEntity> GetListByVideoId(string conn, int id)
        {
            try
            {
                string query = @"VideoCms_Playlist_GetListByVideoId";

                var con = GetConnection(conn);
                var cmd = new SqlCommand(query, con);
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.AddWithValue("@VideoId", id);

                con.Open();
                var reader = cmd.ExecuteReader();
                var data = EntityMapper.FillCollection<PlaylistEntity>(reader);
                con.Close();

                return data;
            }
            catch (Exception ex)
            {
                return new List<PlaylistEntity>();
            }
        }
        public static List<VideoChannelEntity> GetVideoChannelByVideoId(string conn, int id)
        {
            try
            {
                string query = @"CMS_VideoChannel_GetVideoChannelByVideoId";

                var con = GetConnection(conn);
                var cmd = new SqlCommand(query, con);
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.AddWithValue("@VideoId", id);

                con.Open();
                var reader = cmd.ExecuteReader();
                var data = EntityMapper.FillCollection<VideoChannelEntity>(reader);
                con.Close();

                return data;
            }
            catch (Exception ex)
            {
                return new List<VideoChannelEntity>();
            }
        }
        public static List<VideoEntity> GetListVideoByParentId(string conn, int id)
        {
            try
            {
                string query = @"VideoCms_Video_GetListVideoByParentId";

                var con = GetConnection(conn);
                var cmd = new SqlCommand(query, con);
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.AddWithValue("@VideoId", id);

                con.Open();
                var reader = cmd.ExecuteReader();
                var data = EntityMapper.FillCollection<VideoEntity>(reader);
                con.Close();

                return data;
            }
            catch (Exception ex)
            {
                return new List<VideoEntity>();
            }
        }        
        #endregion

        public static int SyncVideoToEs(string nameSpace, string connectStr, string connectEs, int pageIndex, int pageSize)
        {
            try
            {                
                string query = @"VideoCms_Video_InitESAllVideo";

                var con = GetConnection(connectStr);                                
                var cmd = new SqlCommand(query, con);
                cmd.CommandType = CommandType.StoredProcedure;               
                var dateFrom = DBNull.Value;
                var dateTo = DBNull.Value;
                var totalRows = 0;

                var totalRow = cmd.CreateParameter();
                totalRow.ParameterName = "@TotalRow";
                totalRow.Value = totalRows;
                totalRow.Direction = ParameterDirection.Output;

                cmd.Parameters.Add(totalRow);
                cmd.Parameters.AddWithValue("@PageIndex", pageIndex);
                cmd.Parameters.AddWithValue("@PageSize", pageSize);
                cmd.Parameters.AddWithValue("@DateFrom", dateFrom);
                cmd.Parameters.AddWithValue("@DateTo", dateTo);
                
                con.Open();
                var reader = cmd.ExecuteReader();                

                var data = EntityMapper.FillCollection<VideoEntity>(reader);

                con.Close();

                var list = data.Select(s => new VideoSearchEntity
                {
                    Id = s.Id,
                    ZoneId = s.ZoneId,
                    Name = s.Name,
                    ZoneIds = GetListZoneRelation(connectStr, s.Id).Select(z => z.Id.ToString()).ToArray(),
                    PlaylistIds = GetListByVideoId(connectStr, s.Id).Select(z => z.Id.ToString()).ToArray(),
                    PlaylistName = GetListByVideoId(connectStr, s.Id).Select(z => z.Name.ToString()).ToArray(),
                    ChannelIds = GetVideoChannelByVideoId(connectStr, s.Id).Select(z => z.Id.ToString()).ToArray(),
                    ChannelName = GetVideoChannelByVideoId(connectStr, s.Id).Select(z => z.Name.ToString()).ToArray(),
                    CreatedBy = s.CreatedBy,
                    EditedBy = s.EditedBy,
                    PublishBy = s.PublishBy,
                    PublishDate = s.PublishDate,
                    LastModifiedBy = s.LastModifiedBy,
                    LastModifiedDate = s.LastModifiedDate,
                    CreatedDate = s.CreatedDate,
                    DistributionDate = s.DistributionDate,
                    EditedDate = s.EditedDate,
                    Mode = s.Mode,
                    Status = s.Status,
                    Views = s.Views,
                    ZoneName = s.ZoneName,
                    Avatar = s.Avatar,
                    HtmlCode = s.HtmlCode,
                    KeyVideo = s.KeyVideo,
                    ParentId = s.ParentId<0?0:s.ParentId,
                    Type = s.Type < 0 ? 0 : s.Type,
                    Duration = s.Duration,
                    ListType = GetListVideoByParentId(connectStr, s.Id).Select(z => z.Type.ToString()).ToArray(),
                    Description = s.Description,
                    Url = s.Url,
                    FileName=s.FileName
                }).ToList();

                if (pageIndex == 1)
                {
                    EsNewsBo.CreateIndexSettings<VideoSearchEntity>(nameSpace, connectEs, "name");
                }
                var abc= EsNewsBo.CreateMany(nameSpace, connectEs, list);

                return 1;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return 0;                
            }
        }

        public static int GetCountVideo(string connectStr)
        {
            try
            {
                string query = @"SELECT COUNT(*) as [Count] FROM Video";

                var con = GetConnection(connectStr);
                var cmd = new SqlCommand(query, con);                               

                con.Open();
                var reader = cmd.ExecuteReader();

                var data = EntityMapper.FillCollection<CountNewsEntity>(reader);

                con.Close();

                var count = 0;
                if(data!=null && data.Count > 0)
                {
                    count = data.FirstOrDefault().Count;
                }

                return count;
            }
            catch (Exception ex)
            {
                return 0;
                //MessageBox.Show(ex.Message);
            }
        }

        public static int SyncVideoToRedis(string nameSpace, string connectStr, string connectRedis, int pageIndex, int pageSize)
        {           
            try
            {
                string query = @"VideoCms_Video_InitESAllVideo";

                var con = GetConnection(connectStr);
                var cmd = new SqlCommand(query, con);
                cmd.CommandType = CommandType.StoredProcedure;
                var dateFrom = DBNull.Value;
                var dateTo = DBNull.Value;
                var totalRows = 0;

                var totalRow = cmd.CreateParameter();
                totalRow.ParameterName = "@TotalRow";
                totalRow.Value = totalRows;
                totalRow.Direction = ParameterDirection.Output;

                cmd.Parameters.Add(totalRow);
                cmd.Parameters.AddWithValue("@PageIndex", pageIndex);
                cmd.Parameters.AddWithValue("@PageSize", pageSize);
                cmd.Parameters.AddWithValue("@DateFrom", dateFrom);
                cmd.Parameters.AddWithValue("@DateTo", dateTo);

                con.Open();
                var reader = cmd.ExecuteReader();

                var listNews = EntityMapper.FillCollection<VideoEntity>(reader);

                con.Close();

                var data = new Dictionary<string, VideoCachedEntity>();
                foreach (var entry in listNews)
                {
                    data[entry.Id.ToString()] = new VideoCachedEntity() { VideoInfo = entry };
                }
                var abc= RedisNewsBo.AddInHash(nameSpace, connectRedis, data);

                return 1;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return 0;
            }
        }

        #region Afamily

        #region Video
        public static int SyncVideoToDb(string nameSpace, string connectStr1, string connectStr2, int pageIndex, int pageSize, IProgress<InitDataDb.ProcessEntity> progress, InitDataDb.ProcessEntity processEntity)
        {
            try
            {
                string query = @"DECLARE @UpperBand int, @LowerBand int
                                SET @LowerBand  = (@pageIndex - 1) * @PageSize
                                SET @UpperBand  = (@pageIndex * @PageSize)
                                SELECT * FROM (
                                SELECT n.*,ROW_NUMBER() OVER(ORDER BY n.Id DESC) AS RowNumber 
                                FROM [Video] n
                                ) AS temp
                                WHERE RowNumber > @LowerBand AND RowNumber <= @UpperBand";

                //test Id=20161214035128196
                //query = @"SELECT * FROM News WHERE Id=20161214035128196";

                var con = GetConnection(connectStr1);
                var cmd = new SqlCommand(query, con);
                //cmd.CommandType = CommandType.StoredProcedure;               
                var dateFrom = DBNull.Value;
                var dateTo = DBNull.Value;
                cmd.Parameters.AddWithValue("@PageIndex", pageIndex);
                cmd.Parameters.AddWithValue("@PageSize", pageSize);
                cmd.Parameters.AddWithValue("@DateFrom", dateFrom);
                cmd.Parameters.AddWithValue("@DateTo", dateTo);

                con.Open();
                var reader = cmd.ExecuteReader();

                var data = EntityMapper.FillCollection<VideoEntity>(reader);

                con.Close();

                var abc = InitVideoToDb(nameSpace, connectStr2, data, progress, processEntity);

                return abc;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return 0;
            }
        }
        private static int InitVideoToDb(string nameSpace, string connectStr2, List<VideoEntity> data, IProgress<InitDataDb.ProcessEntity> progress, InitDataDb.ProcessEntity processEntity)
        {
            long id = 0;
            var count = 0;
            try
            {
                var con = GetConnection(connectStr2);
                con.Open();

                foreach (var item in data)
                {
                    string query = @"SET IDENTITY_INSERT [Video] ON;
                                    DELETE [Video] WHERE Id=@Id;
                                    INSERT INTO [Video]([Id]
                                                      ,[ZoneId]
                                                      ,[Name]
                                                      ,[UnsignName]
                                                      ,[Description]
                                                      ,[HtmlCode]
                                                      ,[Avatar]
                                                      ,[KeyVideo]
                                                      ,[Pname]
                                                      ,[Status]
                                                      ,[NewsId]
                                                      ,[Views]
                                                      ,[Mode]
                                                      ,[Tags]
                                                      ,[DistributionDate]
                                                      ,[CreatedBy]
                                                      ,[CreatedDate]
                                                      ,[LastModifiedBy]
                                                      ,[LastModifiedDate]
                                                      ,[PublishDate]
                                                      ,[PublishBy]
                                                      ,[EditedDate]
                                                      ,[EditedBy]
                                                      ,[Url]
                                                      ,[Source]
                                                      ,[VideoRelation]
                                                      ,[FileName]
                                                      ,[Duration]
                                                      ,[Size]
                                                      ,[Capacity]
                                                      ,[AllowAd]
                                                      ,[IsRemoveLogo]
                                                      ,[OriginalUrl]
                                                      ,[Type]
                                                      ,[IsConverted]
                                                      ,[AvatarShareFacebook]
                                                      ,[Author]
                                                      ,[OriginalId]
                                                ) VALUES(@Id
                                                        ,@ZoneId
                                                        ,@Name
                                                        ,@UnsignName
                                                        ,@Description
                                                        ,@HtmlCode
                                                        ,@Avatar
                                                        ,@KeyVideo
                                                        ,@Pname
                                                        ,@Status
                                                        ,@NewsId
                                                        ,@Views
                                                        ,@Mode
                                                        ,@Tags
                                                        ,@DistributionDate
                                                        ,@CreatedBy
                                                        ,@CreatedDate
                                                        ,@LastModifiedBy
                                                        ,@LastModifiedDate
                                                        ,@PublishDate
                                                        ,@PublishBy
                                                        ,@EditedDate
                                                        ,@EditedBy
                                                        ,@Url
                                                        ,@Source
                                                        ,@VideoRelation
                                                        ,@FileName
                                                        ,@Duration
                                                        ,@Size
                                                        ,@Capacity
                                                        ,@AllowAd
                                                        ,@IsRemoveLogo
                                                        ,@OriginalUrl
                                                        ,@Type
                                                        ,@IsConverted
                                                        ,@AvatarShareFacebook
                                                        ,@Author
                                                        ,@OriginalId);
                                                SET IDENTITY_INSERT [Video] OFF;";

                    var cmd = new SqlCommand(query, con);
                    //cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandType = CommandType.Text;
                    cmd.Parameters.AddWithValue("@Id", item.Id);                                                            
                    cmd.Parameters.AddWithValue("@ZoneId", item.ZoneId);
                    cmd.Parameters.AddWithValue("@Name", item.Name ?? (object)DBNull.Value);
                    cmd.Parameters.AddWithValue("@UnsignName", item.UnsignName ?? (object)DBNull.Value);
                    cmd.Parameters.AddWithValue("@Description", item.Description ?? (object)DBNull.Value);
                    cmd.Parameters.AddWithValue("@HtmlCode", item.HtmlCode ?? (object)DBNull.Value);
                    cmd.Parameters.AddWithValue("@Avatar", item.Avatar ?? (object)DBNull.Value);
                    cmd.Parameters.AddWithValue("@KeyVideo", item.KeyVideo ?? (object)DBNull.Value);
                    cmd.Parameters.AddWithValue("@Pname", item.Pname ?? (object)DBNull.Value);
                    cmd.Parameters.AddWithValue("@Status", item.Status);
                    cmd.Parameters.AddWithValue("@NewsId", item.NewsId);
                    cmd.Parameters.AddWithValue("@Views", item.Views);
                    cmd.Parameters.AddWithValue("@Mode", item.Mode);
                    cmd.Parameters.AddWithValue("@Tags", item.Tags ?? (object)DBNull.Value);

                    if (item.DistributionDate <= new DateTime(1980, 1, 1))
                        cmd.Parameters.AddWithValue("@DistributionDate", DBNull.Value);
                    else
                        cmd.Parameters.AddWithValue("@DistributionDate", item.DistributionDate);
                    
                    cmd.Parameters.AddWithValue("@CreatedBy", item.CreatedBy ?? (object)DBNull.Value);

                    if (item.CreatedDate <= new DateTime(1980, 1, 1))
                        cmd.Parameters.AddWithValue("@CreatedDate", DBNull.Value);
                    else
                        cmd.Parameters.AddWithValue("@CreatedDate", item.CreatedDate);

                    cmd.Parameters.AddWithValue("@LastModifiedBy", item.LastModifiedBy ?? (object)DBNull.Value);

                    if (item.LastModifiedDate <= new DateTime(1980, 1, 1))
                        cmd.Parameters.AddWithValue("@LastModifiedDate", DBNull.Value);
                    else
                        cmd.Parameters.AddWithValue("@LastModifiedDate", item.LastModifiedDate);                    
                    
                    if (item.PublishDate <= new DateTime(1980, 1, 1))
                        cmd.Parameters.AddWithValue("@PublishDate", DBNull.Value);
                    else
                        cmd.Parameters.AddWithValue("@PublishDate", item.PublishDate);

                    cmd.Parameters.AddWithValue("@PublishBy", item.PublishBy ?? (object)DBNull.Value);

                    if (item.EditedDate <= new DateTime(1980, 1, 1))
                        cmd.Parameters.AddWithValue("@EditedDate", DBNull.Value);
                    else
                        cmd.Parameters.AddWithValue("@EditedDate", item.EditedDate);                    

                    cmd.Parameters.AddWithValue("@EditedBy", item.EditedBy ?? (object)DBNull.Value);
                    cmd.Parameters.AddWithValue("@Url", item.Url ?? (object)DBNull.Value);
                    cmd.Parameters.AddWithValue("@Source", item.Source ?? (object)DBNull.Value);
                    cmd.Parameters.AddWithValue("@VideoRelation", item.VideoRelation ?? (object)DBNull.Value);
                    cmd.Parameters.AddWithValue("@FileName", item.FileName ?? (object)DBNull.Value);
                    cmd.Parameters.AddWithValue("@Duration", item.Duration ?? (object)DBNull.Value);
                    cmd.Parameters.AddWithValue("@Size", item.Size ?? (object)DBNull.Value);
                    cmd.Parameters.AddWithValue("@Capacity", item.Capacity);
                    cmd.Parameters.AddWithValue("@AllowAd", item.AllowAd);
                    cmd.Parameters.AddWithValue("@IsRemoveLogo", item.IsRemoveLogo);
                    cmd.Parameters.AddWithValue("@OriginalUrl", item.OriginalUrl);
                    cmd.Parameters.AddWithValue("@Type", item.Type);
                    cmd.Parameters.AddWithValue("@IsConverted", item.IsConverted);
                    cmd.Parameters.AddWithValue("@AvatarShareFacebook", item.AvatarShareFacebook ?? (object)DBNull.Value);
                    cmd.Parameters.AddWithValue("@Author", item.Author ?? (object)DBNull.Value);
                    cmd.Parameters.AddWithValue("@OriginalId", item.OriginalId);

                    id = item.Id;
                    var reader = cmd.ExecuteNonQuery();
                    count++;

                    if (progress != null)
                    {
                        processEntity.value = count;
                        progress.Report(processEntity);
                    }
                }

                con.Close();

                return count;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message + " ID: " + id.ToString());
                return count;
            }
        }
        #endregion

        #region ZoneVideo
        public static int SyncZoneVideoToDb(string nameSpace, string connectStr1, string connectStr2, int pageIndex, int pageSize, IProgress<InitDataDb.ProcessEntity> progress, InitDataDb.ProcessEntity processEntity)
        {
            try
            {
                string query = @"DECLARE @UpperBand int, @LowerBand int
                                SET @LowerBand  = (@pageIndex - 1) * @PageSize
                                SET @UpperBand  = (@pageIndex * @PageSize)
                                SELECT * FROM (
                                SELECT n.*,ROW_NUMBER() OVER(ORDER BY n.Id DESC) AS RowNumber 
                                FROM [ZoneVideo] n
                                ) AS temp
                                WHERE RowNumber > @LowerBand AND RowNumber <= @UpperBand";

                //test Id=20161214035128196
                //query = @"SELECT * FROM News WHERE Id=20161214035128196";

                var con = GetConnection(connectStr1);
                var cmd = new SqlCommand(query, con);
                //cmd.CommandType = CommandType.StoredProcedure;               
                var dateFrom = DBNull.Value;
                var dateTo = DBNull.Value;
                cmd.Parameters.AddWithValue("@PageIndex", pageIndex);
                cmd.Parameters.AddWithValue("@PageSize", pageSize);
                cmd.Parameters.AddWithValue("@DateFrom", dateFrom);
                cmd.Parameters.AddWithValue("@DateTo", dateTo);

                con.Open();
                var reader = cmd.ExecuteReader();

                var data = EntityMapper.FillCollection<ZoneVideoEntity>(reader);

                con.Close();

                var abc = InitZoneVideoToDb(nameSpace, connectStr2, data, progress, processEntity);

                return abc;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return 0;
            }
        }
        private static int InitZoneVideoToDb(string nameSpace, string connectStr2, List<ZoneVideoEntity> data, IProgress<InitDataDb.ProcessEntity> progress, InitDataDb.ProcessEntity processEntity)
        {
            long id = 0;
            var count = 0;
            try
            {
                var con = GetConnection(connectStr2);
                con.Open();

                foreach (var item in data)
                {
                    string query = @"SET IDENTITY_INSERT [ZoneVideo] ON;
                                    DELETE [ZoneVideo] WHERE Id=@Id;
                                    INSERT INTO [ZoneVideo]([Id]
                                                      ,[Name]
                                                      ,[Url]
                                                      ,[Order]
                                                      ,[ParentId]
                                                      ,[Status]
                                                      ,[CreatedDate]
                                                      ,[ModifiedDate]
                                                      ,[CatId]
                                                      ,[DisplayStyle]
                                                      ,[ShowOnHome]
                                                      ,[Invisibled]
                                                      ,[Keyword]
                                                      ,[ListNewsZoneId]
                                                      ,[Avatar]
                                                      ,[AvatarCover]
                                                      ,[ZoneRelation]
                                                ) VALUES(@Id
                                                        ,@Name
                                                        ,@Url
                                                        ,@Order
                                                        ,@ParentId
                                                        ,@Status
                                                        ,@CreatedDate
                                                        ,@ModifiedDate
                                                        ,@CatId
                                                        ,@DisplayStyle
                                                        ,@ShowOnHome
                                                        ,@Invisibled
                                                        ,@Keyword
                                                        ,@ListNewsZoneId
                                                        ,@Avatar
                                                        ,@AvatarCover
                                                        ,@ZoneRelation);
                                                SET IDENTITY_INSERT [ZoneVideo] OFF;";

                    var cmd = new SqlCommand(query, con);
                    //cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandType = CommandType.Text;
                    cmd.Parameters.AddWithValue("@Id", item.Id);                    
                    cmd.Parameters.AddWithValue("@Name", item.Name ?? (object)DBNull.Value);
                    cmd.Parameters.AddWithValue("@Url", item.Url ?? (object)DBNull.Value);
                    cmd.Parameters.AddWithValue("@Order", item.Order);
                    cmd.Parameters.AddWithValue("@ParentId", item.ParentId);
                    cmd.Parameters.AddWithValue("@Status", item.Status);

                    if (item.CreatedDate <= new DateTime(1980, 1, 1))
                        cmd.Parameters.AddWithValue("@CreatedDate", DBNull.Value);
                    else
                        cmd.Parameters.AddWithValue("@CreatedDate", item.CreatedDate);

                    if (item.ModifiedDate <= new DateTime(1980, 1, 1))
                        cmd.Parameters.AddWithValue("@ModifiedDate", DBNull.Value);
                    else
                        cmd.Parameters.AddWithValue("@ModifiedDate", item.ModifiedDate);

                    cmd.Parameters.AddWithValue("@CatId", item.CatId);
                    cmd.Parameters.AddWithValue("@DisplayStyle", item.DisplayStyle);
                    cmd.Parameters.AddWithValue("@ShowOnHome", item.ShowOnHome);
                    cmd.Parameters.AddWithValue("@Invisibled", item.Invisibled);
                    cmd.Parameters.AddWithValue("@Keyword", item.Keyword ?? (object)DBNull.Value);
                    cmd.Parameters.AddWithValue("@ListNewsZoneId", item.ListNewsZoneId ?? (object)DBNull.Value);
                    cmd.Parameters.AddWithValue("@Avatar", item.Avatar ?? (object)DBNull.Value);
                    cmd.Parameters.AddWithValue("@AvatarCover", item.AvatarCover ?? (object)DBNull.Value);
                    cmd.Parameters.AddWithValue("@ZoneRelation", item.ZoneRelation ?? (object)DBNull.Value);

                    id = item.Id;
                    var reader = cmd.ExecuteNonQuery();
                    count++;

                    if (progress != null)
                    {
                        processEntity.value = count;
                        progress.Report(processEntity);
                    }
                }

                con.Close();

                return count;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message + " ID: " + id.ToString());
                return count;
            }
        }
        public static int GetCountZoneVideo(string connectStr)
        {
            try
            {
                string query = @"SELECT COUNT(*) as [Count] FROM ZoneVideo";

                var con = GetConnection(connectStr);
                var cmd = new SqlCommand(query, con);

                con.Open();
                var reader = cmd.ExecuteReader();

                var data = EntityMapper.FillCollection<CountNewsEntity>(reader);

                con.Close();

                var count = 0;
                if (data != null && data.Count > 0)
                {
                    count = data.FirstOrDefault().Count;
                }

                return count;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return 0;                
            }
        }
        #endregion

        #region VideoInZone
        public static int SyncVideoInZoneToDb(string nameSpace, string connectStr1, string connectStr2, int pageIndex, int pageSize, IProgress<InitDataDb.ProcessEntity> progress, InitDataDb.ProcessEntity processEntity)
        {
            try
            {
                string query = @"DECLARE @UpperBand int, @LowerBand int
                                SET @LowerBand  = (@pageIndex - 1) * @PageSize
                                SET @UpperBand  = (@pageIndex * @PageSize)
                                SELECT * FROM (
                                SELECT n.*,ROW_NUMBER() OVER(ORDER BY n.VideoId DESC) AS RowNumber 
                                FROM [VideoInZone] n
                                ) AS temp
                                WHERE RowNumber > @LowerBand AND RowNumber <= @UpperBand";

                //test Id=20161214035128196
                //query = @"SELECT * FROM News WHERE Id=20161214035128196";

                var con = GetConnection(connectStr1);
                var cmd = new SqlCommand(query, con);
                //cmd.CommandType = CommandType.StoredProcedure;               
                var dateFrom = DBNull.Value;
                var dateTo = DBNull.Value;
                cmd.Parameters.AddWithValue("@PageIndex", pageIndex);
                cmd.Parameters.AddWithValue("@PageSize", pageSize);
                cmd.Parameters.AddWithValue("@DateFrom", dateFrom);
                cmd.Parameters.AddWithValue("@DateTo", dateTo);

                con.Open();
                var reader = cmd.ExecuteReader();

                var data = EntityMapper.FillCollection<VideoInZoneEntity>(reader);

                con.Close();

                var abc = InitVideoInZoneToDb(nameSpace, connectStr2, data, progress, processEntity);

                return abc;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return 0;
            }
        }
        private static int InitVideoInZoneToDb(string nameSpace, string connectStr2, List<VideoInZoneEntity> data, IProgress<InitDataDb.ProcessEntity> progress, InitDataDb.ProcessEntity processEntity)
        {
            long id = 0;
            var count = 0;
            try
            {
                var con = GetConnection(connectStr2);
                con.Open();

                foreach (var item in data)
                {
                    string query = @"DELETE [VideoInZone] WHERE VideoId=@VideoId and ZoneId=@ZoneId;
                                    INSERT INTO [VideoInZone]([VideoId]
                                                            ,[ZoneId]
                                                            ,[IsPrimary]
                                                            ,[LastModifiedDate]
                                                            ,[RootZoneId]
                                                ) VALUES(@VideoId
                                                          ,@ZoneId
                                                          ,@IsPrimary
                                                          ,@LastModifiedDate
                                                          ,@RootZoneId);
                                                ";

                    var cmd = new SqlCommand(query, con);
                    //cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandType = CommandType.Text;
                    cmd.Parameters.AddWithValue("@VideoId", item.VideoId);
                    cmd.Parameters.AddWithValue("@ZoneId", item.ZoneId);
                    cmd.Parameters.AddWithValue("@IsPrimary", item.IsPrimary);                    

                    if (item.LastModifiedDate <= new DateTime(1980, 1, 1))
                        cmd.Parameters.AddWithValue("@LastModifiedDate", DBNull.Value);
                    else
                        cmd.Parameters.AddWithValue("@LastModifiedDate", item.LastModifiedDate);
                  
                    cmd.Parameters.AddWithValue("@RootZoneId", item.RootZoneId);                    

                    id = item.VideoId;
                    var reader = cmd.ExecuteNonQuery();
                    count++;

                    if (progress != null)
                    {
                        processEntity.value = count;
                        progress.Report(processEntity);
                    }
                }

                con.Close();

                return count;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message + " ID: " + id.ToString());
                return count;
            }
        }
        public static int GetCountVideoInZone(string connectStr)
        {
            try
            {
                string query = @"SELECT COUNT(*) as [Count] FROM VideoInZone";

                var con = GetConnection(connectStr);
                var cmd = new SqlCommand(query, con);

                con.Open();
                var reader = cmd.ExecuteReader();

                var data = EntityMapper.FillCollection<CountNewsEntity>(reader);

                con.Close();

                var count = 0;
                if (data != null && data.Count > 0)
                {
                    count = data.FirstOrDefault().Count;
                }

                return count;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return 0;
            }
        }
        #endregion

        #region VideoTag
        public static int SyncVideoTagToDb(string nameSpace, string connectStr1, string connectStr2, int pageIndex, int pageSize, IProgress<InitDataDb.ProcessEntity> progress, InitDataDb.ProcessEntity processEntity)
        {
            try
            {
                string query = @"DECLARE @UpperBand int, @LowerBand int
                                SET @LowerBand  = (@pageIndex - 1) * @PageSize
                                SET @UpperBand  = (@pageIndex * @PageSize)
                                SELECT * FROM (
                                SELECT n.*,ROW_NUMBER() OVER(ORDER BY n.Id DESC) AS RowNumber 
                                FROM [VideoTag] n
                                ) AS temp
                                WHERE RowNumber > @LowerBand AND RowNumber <= @UpperBand";
                
                var con = GetConnection(connectStr1);
                var cmd = new SqlCommand(query, con);
                                           
                var dateFrom = DBNull.Value;
                var dateTo = DBNull.Value;
                cmd.Parameters.AddWithValue("@PageIndex", pageIndex);
                cmd.Parameters.AddWithValue("@PageSize", pageSize);
                cmd.Parameters.AddWithValue("@DateFrom", dateFrom);
                cmd.Parameters.AddWithValue("@DateTo", dateTo);

                con.Open();
                var reader = cmd.ExecuteReader();

                var data = EntityMapper.FillCollection<VideoTagEntity>(reader);

                con.Close();

                var abc = InitVideoTagToDb(nameSpace, connectStr2, data, progress, processEntity);

                return abc;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return 0;
            }
        }
        private static int InitVideoTagToDb(string nameSpace, string connectStr2, List<VideoTagEntity> data, IProgress<InitDataDb.ProcessEntity> progress, InitDataDb.ProcessEntity processEntity)
        {
            long id = 0;
            var count = 0;
            try
            {
                var con = GetConnection(connectStr2);
                con.Open();

                foreach (var item in data)
                {
                    string query = @"SET IDENTITY_INSERT [VideoTag] ON;
                                    DELETE [VideoTag] WHERE Id=@Id;
                                    INSERT INTO [VideoTag]([Id]
                                                          ,[ParentId]
                                                          ,[Name]
                                                          ,[UnsignName]
                                                          ,[Avatar]
                                                          ,[Description]
                                                          ,[Url]
                                                          ,[IsHotTag]
                                                          ,[CreatedDate]
                                                          ,[CreatedBy]
                                                          ,[Status]
                                                ) VALUES(@Id
                                                        ,@ParentId
                                                        ,@Name
                                                        ,@UnsignName
                                                        ,@Avatar
                                                        ,@Description
                                                        ,@Url
                                                        ,@IsHotTag
                                                        ,@CreatedDate
                                                        ,@CreatedBy
                                                        ,@Status);
                                                SET IDENTITY_INSERT [VideoTag] OFF;";

                    var cmd = new SqlCommand(query, con);                    
                    cmd.CommandType = CommandType.Text;

                    cmd.Parameters.AddWithValue("@Id", item.Id);
                    cmd.Parameters.AddWithValue("@ParentId", item.ParentId);
                    cmd.Parameters.AddWithValue("@Name", item.Name ?? (object)DBNull.Value);
                    cmd.Parameters.AddWithValue("@UnsignName", item.UnsignName ?? (object)DBNull.Value);
                    cmd.Parameters.AddWithValue("@Avatar", item.Avatar ?? (object)DBNull.Value);
                    cmd.Parameters.AddWithValue("@Description", item.Description ?? (object)DBNull.Value);
                    cmd.Parameters.AddWithValue("@Url", item.Url ?? (object)DBNull.Value);
                    cmd.Parameters.AddWithValue("@IsHotTag", item.IsHotTag);

                    if (item.CreatedDate <= new DateTime(1980, 1, 1))
                        cmd.Parameters.AddWithValue("@CreatedDate", DBNull.Value);
                    else
                        cmd.Parameters.AddWithValue("@CreatedDate", item.CreatedDate);
                                        
                    cmd.Parameters.AddWithValue("@CreatedBy", item.CreatedBy ?? (object)DBNull.Value);
                    cmd.Parameters.AddWithValue("@Status", item.Status);

                    id = item.Id;
                    var reader = cmd.ExecuteNonQuery();
                    count++;

                    if (progress != null)
                    {
                        processEntity.value = count;
                        progress.Report(processEntity);
                    }
                }

                con.Close();

                return count;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message + " ID: " + id.ToString());
                return count;
            }
        }
        public static int GetCountVideoTag(string connectStr)
        {
            try
            {
                string query = @"SELECT COUNT(*) as [Count] FROM VideoTag";

                var con = GetConnection(connectStr);
                var cmd = new SqlCommand(query, con);

                con.Open();
                var reader = cmd.ExecuteReader();

                var data = EntityMapper.FillCollection<CountNewsEntity>(reader);

                con.Close();

                var count = 0;
                if (data != null && data.Count > 0)
                {
                    count = data.FirstOrDefault().Count;
                }

                return count;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return 0;
            }
        }
        #endregion

        #region VideoInTag
        public static int SyncVideoInTagToDb(string nameSpace, string connectStr1, string connectStr2, int pageIndex, int pageSize, IProgress<InitDataDb.ProcessEntity> progress, InitDataDb.ProcessEntity processEntity)
        {
            try
            {
                string query = @"DECLARE @UpperBand int, @LowerBand int
                                SET @LowerBand  = (@pageIndex - 1) * @PageSize
                                SET @UpperBand  = (@pageIndex * @PageSize)
                                SELECT * FROM (
                                SELECT n.*,ROW_NUMBER() OVER(ORDER BY n.VideoTagId DESC) AS RowNumber 
                                FROM [VideoInTag] n
                                ) AS temp
                                WHERE RowNumber > @LowerBand AND RowNumber <= @UpperBand";

                var con = GetConnection(connectStr1);
                var cmd = new SqlCommand(query, con);

                var dateFrom = DBNull.Value;
                var dateTo = DBNull.Value;
                cmd.Parameters.AddWithValue("@PageIndex", pageIndex);
                cmd.Parameters.AddWithValue("@PageSize", pageSize);
                cmd.Parameters.AddWithValue("@DateFrom", dateFrom);
                cmd.Parameters.AddWithValue("@DateTo", dateTo);

                con.Open();
                var reader = cmd.ExecuteReader();

                var data = EntityMapper.FillCollection<VideoInTagEntity>(reader);

                con.Close();

                var abc = InitVideoInTagToDb(nameSpace, connectStr2, data, progress, processEntity);

                return abc;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return 0;
            }
        }
        private static int InitVideoInTagToDb(string nameSpace, string connectStr2, List<VideoInTagEntity> data, IProgress<InitDataDb.ProcessEntity> progress, InitDataDb.ProcessEntity processEntity)
        {
            long id = 0;
            var count = 0;
            try
            {
                var con = GetConnection(connectStr2);
                con.Open();

                foreach (var item in data)
                {
                    string query = @"DELETE [VideoInTag] WHERE VideoId=@VideoId and VideoTagId=@VideoTagId
                                    INSERT INTO [VideoInTag]([VideoTagId]
                                                              ,[VideoId]
                                                              ,[TagMode]
                                                              ,[Priority]                                                              
                                                ) VALUES(@VideoTagId
                                                        ,@VideoId
                                                        ,@TagMode                                                        
                                                        ,@Priority)";

                    using (var cmd = new SqlCommand(query, con))
                    {
                        cmd.CommandType = CommandType.Text;

                        cmd.Parameters.AddWithValue("@VideoTagId", item.VideoTagId);
                        cmd.Parameters.AddWithValue("@VideoId", item.VideoId);
                        cmd.Parameters.AddWithValue("@TagMode", item.TagMode);
                        cmd.Parameters.AddWithValue("@Priority", item.Priority);

                        id = item.VideoTagId;
                        var reader = cmd.ExecuteNonQuery();
                        count++;

                        if (progress != null)
                        {
                            processEntity.value = count;
                            progress.Report(processEntity);
                        }
                    }
                }

                con.Close();

                return count;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message + " ID: " + id.ToString());
                return count;
            }
        }
        public static int GetCountVideoInTag(string connectStr)
        {
            try
            {
                string query = @"SELECT COUNT(*) as [Count] FROM VideoInTag";

                var con = GetConnection(connectStr);
                var cmd = new SqlCommand(query, con);

                con.Open();
                var reader = cmd.ExecuteReader();

                var data = EntityMapper.FillCollection<CountNewsEntity>(reader);

                con.Close();

                var count = 0;
                if (data != null && data.Count > 0)
                {
                    count = data.FirstOrDefault().Count;
                }

                return count;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return 0;
            }
        }
        #endregion

        #region VideoTags
        public static int SyncVideoTagsToDb(string nameSpace, string connectStr1, string connectStr2, int pageIndex, int pageSize, IProgress<InitDataDb.ProcessEntity> progress, InitDataDb.ProcessEntity processEntity)
        {
            try
            {
                string query = @"DECLARE @UpperBand int, @LowerBand int
                                SET @LowerBand  = (@pageIndex - 1) * @PageSize
                                SET @UpperBand  = (@pageIndex * @PageSize)
                                SELECT * FROM (
                                SELECT n.*,ROW_NUMBER() OVER(ORDER BY n.TagId DESC) AS RowNumber 
                                FROM [VideoTags] n
                                ) AS temp
                                WHERE RowNumber > @LowerBand AND RowNumber <= @UpperBand";

                var con = GetConnection(connectStr1);
                var cmd = new SqlCommand(query, con);

                var dateFrom = DBNull.Value;
                var dateTo = DBNull.Value;
                cmd.Parameters.AddWithValue("@PageIndex", pageIndex);
                cmd.Parameters.AddWithValue("@PageSize", pageSize);
                cmd.Parameters.AddWithValue("@DateFrom", dateFrom);
                cmd.Parameters.AddWithValue("@DateTo", dateTo);

                con.Open();
                var reader = cmd.ExecuteReader();

                var data = EntityMapper.FillCollection<VideoTagsEntity>(reader);

                con.Close();

                var abc = InitVideoTagsToDb(nameSpace, connectStr2, data, progress, processEntity);

                return abc;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return 0;
            }
        }
        private static int InitVideoTagsToDb(string nameSpace, string connectStr2, List<VideoTagsEntity> data, IProgress<InitDataDb.ProcessEntity> progress, InitDataDb.ProcessEntity processEntity)
        {
            long id = 0;
            var count = 0;
            try
            {
                var con = GetConnection(connectStr2);
                con.Open();

                foreach (var item in data)
                {
                    string query = @"DELETE [VideoTags] WHERE VideoId=@VideoId and TagId=@TagId
                                    INSERT INTO [VideoTags]([TagId]
                                                              ,[VideoId]
                                                              ,[Type]
                                                              ,[Priority]                                                              
                                                ) VALUES(@TagId
                                                        ,@VideoId
                                                        ,@Type                                                        
                                                        ,@Priority)";

                    using (var cmd = new SqlCommand(query, con))
                    {
                        cmd.CommandType = CommandType.Text;

                        cmd.Parameters.AddWithValue("@TagId", item.TagId);
                        cmd.Parameters.AddWithValue("@VideoId", item.VideoId);
                        cmd.Parameters.AddWithValue("@Type", item.Type);
                        cmd.Parameters.AddWithValue("@Priority", item.Priority);
                        
                        id = item.TagId;
                        var reader = cmd.ExecuteNonQuery();
                        count++;

                        if (progress != null)
                        {
                            processEntity.value = count;
                            progress.Report(processEntity);
                        }
                    }
                }

                con.Close();

                return count;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message + " ID: " + id.ToString());
                return count;
            }
        }
        public static int GetCountVideoTags(string connectStr)
        {
            try
            {
                string query = @"SELECT COUNT(*) as [Count] FROM VideoTags";

                var con = GetConnection(connectStr);
                var cmd = new SqlCommand(query, con);

                con.Open();
                var reader = cmd.ExecuteReader();

                var data = EntityMapper.FillCollection<CountNewsEntity>(reader);

                con.Close();

                var count = 0;
                if (data != null && data.Count > 0)
                {
                    count = data.FirstOrDefault().Count;
                }

                return count;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return 0;
            }
        }
        #endregion

        #endregion
    }
}
