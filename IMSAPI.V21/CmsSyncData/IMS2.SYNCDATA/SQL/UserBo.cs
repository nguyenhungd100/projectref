﻿using IMS2.SYNCDATA.Common;
using IMS2.SYNCDATA.Entity;
using IMS2.SYNCDATA.ES;
using IMS2.SYNCDATA.Redis;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Windows.Forms;

namespace IMS2.SYNCDATA.SQL
{
    public class UserBo
    {
        public static SqlConnection GetConnection(string conn="")
        {
            if (string.IsNullOrEmpty(conn))
            {
                conn = "Data Source=172.16.41.125;Initial Catalog=IMS2_AFAMILY_DEV;Persist Security Info=True;User ID=ims_test;Password=ImsTest123$%^789";
            }            
            return new SqlConnection(conn);            
        }
               
        public static int SyncUserToEs(string nameSpace, string connectStr, string connectEs, int pageIndex, int pageSize)
        {
            try
            {                
                string query = @"CMS_User_InitAllUser";

                var con = GetConnection(connectStr);                                
                var cmd = new SqlCommand(query, con);
                cmd.CommandType = CommandType.StoredProcedure;                
                
                con.Open();
                var reader = cmd.ExecuteReader();                

                var data = EntityMapper.FillCollection<UserEntity>(reader);

                con.Close();

                var list = data.Select(s => new UserSearchEntity
                {
                    Id = s.Id,
                    FullName = s.FullName,
                    UserName = s.UserName,
                    Status = s.Status,
                    CreatedDate = s.CreatedDate
                }).ToList();

                if (pageIndex == 1)
                {
                    EsNewsBo.CreateIndexSettings<UserSearchEntity>(nameSpace, connectEs, "fullname");
                }
                var abc= EsNewsBo.CreateMany(nameSpace, connectEs, list);

                return 1;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return 0;                
            }
        }

        public static int GetCountUser(string connectStr)
        {
            try
            {
                string query = @"SELECT COUNT(*) as [Count] FROM [User]";

                var con = GetConnection(connectStr);
                var cmd = new SqlCommand(query, con);                               

                con.Open();
                var reader = cmd.ExecuteReader();

                var data = EntityMapper.FillCollection<CountNewsEntity>(reader);

                con.Close();

                var count = 0;
                if(data!=null && data.Count > 0)
                {
                    count = data.FirstOrDefault().Count;
                }

                return count;
            }
            catch (Exception ex)
            {
                return 0;
                //MessageBox.Show(ex.Message);
            }
        }

        public static int SyncUserToRedis(string nameSpace, string connectStr, string connectRedis, int pageIndex, int pageSize)
        {           
            try
            {
                string query = @"CMS_User_InitAllUser";

                var con = GetConnection(connectStr);
                var cmd = new SqlCommand(query, con);
                cmd.CommandType = CommandType.StoredProcedure;

                con.Open();
                var reader = cmd.ExecuteReader();

                var listNews = EntityMapper.FillCollection<UserEntity>(reader);

                con.Close();

                var data = new Dictionary<string, UserCachedEntity>();
                foreach (var entry in listNews)
                {
                    data[entry.Id.ToString()] = new UserCachedEntity() { User = entry };

                    RedisNewsBo.AddInSortedSet<UserEntity>(nameSpace, connectRedis, entry.UserName.ToLower(), entry.Id);
                }
                var abc = RedisNewsBo.AddInHash(nameSpace, connectRedis, data);

                return 1;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return 0;
            }
        }

        #region Afamily user
        public static int SyncUserToDb(string nameSpace, string connectStr1, string connectStr2, int pageIndex, int pageSize, IProgress<InitDataDb.ProcessEntity> progress, InitDataDb.ProcessEntity processEntity)
        {
            try
            {
                string query = @"DECLARE @UpperBand int, @LowerBand int
                                SET @LowerBand  = (@pageIndex - 1) * @PageSize
                                SET @UpperBand  = (@pageIndex * @PageSize)
                                SELECT * FROM (
                                SELECT n.*,ROW_NUMBER() OVER(ORDER BY n.Id DESC) AS RowNumber 
                                FROM [User] n
                                ) AS temp
                                WHERE RowNumber > @LowerBand AND RowNumber <= @UpperBand";

                //test Id=20161214035128196
                //query = @"SELECT * FROM News WHERE Id=20161214035128196";

                var con = GetConnection(connectStr1);
                var cmd = new SqlCommand(query, con);
                //cmd.CommandType = CommandType.StoredProcedure;               
                var dateFrom = DBNull.Value;
                var dateTo = DBNull.Value;
                cmd.Parameters.AddWithValue("@PageIndex", pageIndex);
                cmd.Parameters.AddWithValue("@PageSize", pageSize);
                cmd.Parameters.AddWithValue("@DateFrom", dateFrom);
                cmd.Parameters.AddWithValue("@DateTo", dateTo);

                con.Open();
                var reader = cmd.ExecuteReader();

                var data = EntityMapper.FillCollection<UserEntity>(reader);

                con.Close();

                var abc = InitUserToDb(nameSpace, connectStr2, data, progress, processEntity);

                return abc;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return 0;                
            }
        }
        private static int InitUserToDb(string nameSpace, string connectStr2, List<UserEntity> data, IProgress<InitDataDb.ProcessEntity> progress, InitDataDb.ProcessEntity processEntity)
        {
            long id = 0;
            var count = 0;
            try
            {
                var con = GetConnection(connectStr2);
                con.Open();

                foreach (var item in data)
                {
                    string query = @"SET IDENTITY_INSERT [User] ON;
                                    DELETE [User] WHERE Id=@Id;
                                    INSERT INTO [User]([Id]
                                                      ,[UserName]
                                                      ,[Password]
                                                      ,[FullName]
                                                      ,[Avatar]
                                                      ,[Email]
                                                      ,[Mobile]
                                                      ,[IsFullPermission]
                                                      ,[IsFullZone]
                                                      ,[Status]
                                                      ,[CreatedDate]
                                                      ,[ModifiedDate]
                                                      ,[LastLogined]
                                                      ,[LastChangePass]
                                                      ,[IsSystem]
                                                      ,[OtpSecretKey]
                                                ) VALUES(@Id
                                                          ,@UserName
                                                          ,@Password
                                                          ,@FullName
                                                          ,@Avatar
                                                          ,@Email
                                                          ,@Mobile
                                                          ,@IsFullPermission
                                                          ,@IsFullZone
                                                          ,@Status
                                                          ,@CreatedDate
                                                          ,@ModifiedDate
                                                          ,@LastLogined
                                                          ,@LastChangePass
                                                          ,@IsSystem
                                                          ,@OtpSecretKey);
                                                SET IDENTITY_INSERT [User] OFF;";

                    var cmd = new SqlCommand(query, con);
                    //cmd.CommandType = CommandType.StoredProcedure;  
                    cmd.CommandType = CommandType.Text;
                    cmd.Parameters.AddWithValue("@Id", item.Id);
                    cmd.Parameters.AddWithValue("@UserName", item.UserName ?? (object)DBNull.Value);
                    cmd.Parameters.AddWithValue("@Password", item.Password ?? (object)DBNull.Value);                    
                    cmd.Parameters.AddWithValue("@FullName", item.FullName ?? (object)DBNull.Value);
                    cmd.Parameters.AddWithValue("@Avatar", item.Avatar ?? (object)DBNull.Value);
                    cmd.Parameters.AddWithValue("@Email", item.Email ?? (object)DBNull.Value);
                    cmd.Parameters.AddWithValue("@Mobile", item.Mobile ?? (object)DBNull.Value);
                    cmd.Parameters.AddWithValue("@IsFullPermission", item.IsFullPermission);
                    cmd.Parameters.AddWithValue("@IsFullZone", item.IsFullZone);
                    cmd.Parameters.AddWithValue("@Status", item.Status);

                    if (item.ModifiedDate <= new DateTime(1980, 1, 1))
                        cmd.Parameters.AddWithValue("@ModifiedDate", DBNull.Value);
                    else
                        cmd.Parameters.AddWithValue("@ModifiedDate", item.ModifiedDate);

                    if (item.CreatedDate <= new DateTime(1980, 1, 1))
                        cmd.Parameters.AddWithValue("@CreatedDate", DBNull.Value);
                    else
                        cmd.Parameters.AddWithValue("@CreatedDate", item.CreatedDate);

                    if (item.LastLogined <= new DateTime(1980, 1, 1))
                        cmd.Parameters.AddWithValue("@LastLogined", DBNull.Value);
                    else
                        cmd.Parameters.AddWithValue("@LastLogined", item.LastLogined);

                    if (item.LastChangePass <= new DateTime(1980, 1, 1))
                        cmd.Parameters.AddWithValue("@LastChangePass", DBNull.Value);
                    else
                        cmd.Parameters.AddWithValue("@LastChangePass", item.LastChangePass);
                    
                    cmd.Parameters.AddWithValue("@IsSystem", item.IsSystem);
                    cmd.Parameters.AddWithValue("@OtpSecretKey", item.OtpSecretKey ?? (object)DBNull.Value);

                    id = item.Id;
                    var reader = cmd.ExecuteNonQuery();
                    count++;

                    if (progress != null)
                    {
                        processEntity.value = count;
                        progress.Report(processEntity);
                    }
                }

                con.Close();

                return count;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message + " ID: " + id.ToString());
                return count;
            }
        }

        #region UserProfile
        public static int SyncUserProfileToDb(string nameSpace, string connectStr1, string connectStr2, int pageIndex, int pageSize, IProgress<InitDataDb.ProcessEntity> progress, InitDataDb.ProcessEntity processEntity)
        {
            try
            {
                string query = @"DECLARE @UpperBand int, @LowerBand int
                                SET @LowerBand  = (@pageIndex - 1) * @PageSize
                                SET @UpperBand  = (@pageIndex * @PageSize)
                                SELECT * FROM (
                                SELECT n.*,ROW_NUMBER() OVER(ORDER BY n.UserId DESC) AS RowNumber 
                                FROM [UserProfile] n
                                ) AS temp
                                WHERE RowNumber > @LowerBand AND RowNumber <= @UpperBand";

                //test Id=20161214035128196
                //query = @"SELECT * FROM News WHERE Id=20161214035128196";

                var con = GetConnection(connectStr1);
                var cmd = new SqlCommand(query, con);
                //cmd.CommandType = CommandType.StoredProcedure;               
                var dateFrom = DBNull.Value;
                var dateTo = DBNull.Value;
                cmd.Parameters.AddWithValue("@PageIndex", pageIndex);
                cmd.Parameters.AddWithValue("@PageSize", pageSize);
                cmd.Parameters.AddWithValue("@DateFrom", dateFrom);
                cmd.Parameters.AddWithValue("@DateTo", dateTo);

                con.Open();
                var reader = cmd.ExecuteReader();

                var data = EntityMapper.FillCollection<UserProfileEntity>(reader);

                con.Close();

                var abc = InitUserProfileToDb(nameSpace, connectStr2, data, progress, processEntity);

                return abc;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return 0;
            }
        }
        private static int InitUserProfileToDb(string nameSpace, string connectStr2, List<UserProfileEntity> data, IProgress<InitDataDb.ProcessEntity> progress, InitDataDb.ProcessEntity processEntity)
        {
            long id = 0;
            var count = 0;
            try
            {
                var con = GetConnection(connectStr2);
                con.Open();

                foreach (var item in data)
                {
                    string query = @"DELETE [UserProfile] WHERE UserId=@UserId;
                                    INSERT INTO [UserProfile]([UserId]
                                                            ,[Address]
                                                            ,[Birthday]
                                                            ,[Description]
                                                ) VALUES(@UserId
                                                          ,@Address
                                                          ,@Birthday
                                                          ,@Description);";

                    var cmd = new SqlCommand(query, con);
                    //cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandType = CommandType.Text;
                    cmd.Parameters.AddWithValue("@UserId", item.UserId);
                    cmd.Parameters.AddWithValue("@Address", item.Address ?? (object)DBNull.Value);

                    if (item.Birthday <= new DateTime(1980, 1, 1))
                        cmd.Parameters.AddWithValue("@Birthday", DBNull.Value);
                    else
                        cmd.Parameters.AddWithValue("@Birthday", item.Birthday);
                    
                    cmd.Parameters.AddWithValue("@Description", item.Description ?? (object)DBNull.Value);                                       

                    id = item.UserId;
                    var reader = cmd.ExecuteNonQuery();
                    count++;

                    if (progress != null)
                    {
                        processEntity.value = count;
                        progress.Report(processEntity);
                    }
                }

                con.Close();

                return count;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message + " ID: " + id.ToString());
                return count;
            }
        }
        public static int GetCountUserProfile(string connectStr)
        {
            try
            {
                string query = @"SELECT COUNT(*) as [Count] FROM [UserProfile]";

                var con = GetConnection(connectStr);
                var cmd = new SqlCommand(query, con);

                con.Open();
                var reader = cmd.ExecuteReader();

                var data = EntityMapper.FillCollection<CountNewsEntity>(reader);

                con.Close();

                var count = 0;
                if (data != null && data.Count > 0)
                {
                    count = data.FirstOrDefault().Count;
                }

                return count;
            }
            catch (Exception ex)
            {
                return 0;
                //MessageBox.Show(ex.Message);
            }
        }
        #endregion

        #region UserPermission
        public static int SyncUserPermissionToDb(string nameSpace, string connectStr1, string connectStr2, int pageIndex, int pageSize, IProgress<InitDataDb.ProcessEntity> progress, InitDataDb.ProcessEntity processEntity)
        {
            try
            {
                string query = @"DECLARE @UpperBand int, @LowerBand int
                                SET @LowerBand  = (@pageIndex - 1) * @PageSize
                                SET @UpperBand  = (@pageIndex * @PageSize)
                                SELECT * FROM (
                                SELECT n.*,ROW_NUMBER() OVER(ORDER BY n.UserId DESC) AS RowNumber 
                                FROM [UserPermission] n
                                ) AS temp
                                WHERE RowNumber > @LowerBand AND RowNumber <= @UpperBand";

                //test Id=20161214035128196
                //query = @"SELECT * FROM News WHERE Id=20161214035128196";

                var con = GetConnection(connectStr1);
                var cmd = new SqlCommand(query, con);
                //cmd.CommandType = CommandType.StoredProcedure;               
                var dateFrom = DBNull.Value;
                var dateTo = DBNull.Value;
                cmd.Parameters.AddWithValue("@PageIndex", pageIndex);
                cmd.Parameters.AddWithValue("@PageSize", pageSize);
                cmd.Parameters.AddWithValue("@DateFrom", dateFrom);
                cmd.Parameters.AddWithValue("@DateTo", dateTo);

                con.Open();
                var reader = cmd.ExecuteReader();

                var data = EntityMapper.FillCollection<UserPermissionEntity>(reader);

                con.Close();

                var abc = InitUserPermissionToDb(nameSpace, connectStr2, data, progress, processEntity);

                return abc;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return 0;
            }
        }
        private static int InitUserPermissionToDb(string nameSpace, string connectStr2, List<UserPermissionEntity> data, IProgress<InitDataDb.ProcessEntity> progress, InitDataDb.ProcessEntity processEntity)
        {
            long id = 0;
            var count = 0;
            try
            {
                var con = GetConnection(connectStr2);
                con.Open();

                foreach (var item in data)
                {
                    string query = @"DELETE [UserPermission] WHERE UserId=@UserId and PermissionId=@PermissionId and ZoneId=@ZoneId;
                                    INSERT INTO [UserPermission]([UserId]
                                                            ,[PermissionId]
                                                            ,[ZoneId]
                                                ) VALUES(@UserId
                                                          ,@PermissionId
                                                          ,@ZoneId);";

                    var cmd = new SqlCommand(query, con);
                    //cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandType = CommandType.Text;
                    cmd.Parameters.AddWithValue("@UserId", item.UserId);
                    cmd.Parameters.AddWithValue("@PermissionId", item.PermissionId);                    
                    cmd.Parameters.AddWithValue("@ZoneId", item.ZoneId);

                    id = item.UserId;
                    var reader = cmd.ExecuteNonQuery();
                    count++;

                    if (progress != null)
                    {
                        processEntity.value = count;
                        progress.Report(processEntity);
                    }
                }

                con.Close();

                return count;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message + " ID: " + id.ToString());
                return count;
            }
        }
        public static int GetCountUserPermission(string connectStr)
        {
            try
            {
                string query = @"SELECT COUNT(*) as [Count] FROM [UserPermission]";

                var con = GetConnection(connectStr);
                var cmd = new SqlCommand(query, con);

                con.Open();
                var reader = cmd.ExecuteReader();

                var data = EntityMapper.FillCollection<CountNewsEntity>(reader);

                con.Close();

                var count = 0;
                if (data != null && data.Count > 0)
                {
                    count = data.FirstOrDefault().Count;
                }

                return count;
            }
            catch (Exception ex)
            {
                return 0;
                //MessageBox.Show(ex.Message);
            }
        }
        #endregion

        #region GroupPermission
        public static int SyncGroupPermissionToDb(string nameSpace, string connectStr1, string connectStr2, int pageIndex, int pageSize, IProgress<InitDataDb.ProcessEntity> progress, InitDataDb.ProcessEntity processEntity)
        {
            try
            {
                string query = @"DECLARE @UpperBand int, @LowerBand int
                                SET @LowerBand  = (@pageIndex - 1) * @PageSize
                                SET @UpperBand  = (@pageIndex * @PageSize)
                                SELECT * FROM (
                                SELECT n.*,ROW_NUMBER() OVER(ORDER BY n.Id DESC) AS RowNumber 
                                FROM [GroupPermission] n
                                ) AS temp
                                WHERE RowNumber > @LowerBand AND RowNumber <= @UpperBand";

                //test Id=20161214035128196
                //query = @"SELECT * FROM News WHERE Id=20161214035128196";

                var con = GetConnection(connectStr1);
                var cmd = new SqlCommand(query, con);
                //cmd.CommandType = CommandType.StoredProcedure;               
                var dateFrom = DBNull.Value;
                var dateTo = DBNull.Value;
                cmd.Parameters.AddWithValue("@PageIndex", pageIndex);
                cmd.Parameters.AddWithValue("@PageSize", pageSize);
                cmd.Parameters.AddWithValue("@DateFrom", dateFrom);
                cmd.Parameters.AddWithValue("@DateTo", dateTo);

                con.Open();
                var reader = cmd.ExecuteReader();

                var data = EntityMapper.FillCollection<GroupPermissionEntity>(reader);

                con.Close();

                var abc = InitGroupPermissionToDb(nameSpace, connectStr2, data, progress, processEntity);

                return abc;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return 0;
            }
        }
        private static int InitGroupPermissionToDb(string nameSpace, string connectStr2, List<GroupPermissionEntity> data, IProgress<InitDataDb.ProcessEntity> progress, InitDataDb.ProcessEntity processEntity)
        {
            long id = 0;
            var count = 0;
            try
            {
                var con = GetConnection(connectStr2);
                con.Open();

                foreach (var item in data)
                {
                    string query = @"DELETE [GroupPermission] WHERE Id=@Id;
                                    INSERT INTO [GroupPermission]([Id]
                                                                ,[Name]
                                                ) VALUES(@Id
                                                          ,@Name);";

                    var cmd = new SqlCommand(query, con);
                    //cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandType = CommandType.Text;
                    cmd.Parameters.AddWithValue("@Id", item.Id);
                    cmd.Parameters.AddWithValue("@Name", item.Name ?? (object)DBNull.Value);                    

                    id = item.Id;
                    var reader = cmd.ExecuteNonQuery();
                    count++;

                    if (progress != null)
                    {
                        processEntity.value = count;
                        progress.Report(processEntity);
                    }
                }

                con.Close();

                return count;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message + " ID: " + id.ToString());
                return count;
            }
        }
        public static int GetCountGroupPermission(string connectStr)
        {
            try
            {
                string query = @"SELECT COUNT(*) as [Count] FROM [GroupPermission]";

                var con = GetConnection(connectStr);
                var cmd = new SqlCommand(query, con);

                con.Open();
                var reader = cmd.ExecuteReader();

                var data = EntityMapper.FillCollection<CountNewsEntity>(reader);

                con.Close();

                var count = 0;
                if (data != null && data.Count > 0)
                {
                    count = data.FirstOrDefault().Count;
                }

                return count;
            }
            catch (Exception ex)
            {
                return 0;
                //MessageBox.Show(ex.Message);
            }
        }
        #endregion

        #region Permission
        public static int SyncPermissionToDb(string nameSpace, string connectStr1, string connectStr2, int pageIndex, int pageSize, IProgress<InitDataDb.ProcessEntity> progress, InitDataDb.ProcessEntity processEntity)
        {
            try
            {
                string query = @"DECLARE @UpperBand int, @LowerBand int
                                SET @LowerBand  = (@pageIndex - 1) * @PageSize
                                SET @UpperBand  = (@pageIndex * @PageSize)
                                SELECT * FROM (
                                SELECT n.*,ROW_NUMBER() OVER(ORDER BY n.Id DESC) AS RowNumber 
                                FROM [Permission] n
                                ) AS temp
                                WHERE RowNumber > @LowerBand AND RowNumber <= @UpperBand";

                //test Id=20161214035128196
                //query = @"SELECT * FROM News WHERE Id=20161214035128196";

                var con = GetConnection(connectStr1);
                var cmd = new SqlCommand(query, con);
                //cmd.CommandType = CommandType.StoredProcedure;               
                var dateFrom = DBNull.Value;
                var dateTo = DBNull.Value;
                cmd.Parameters.AddWithValue("@PageIndex", pageIndex);
                cmd.Parameters.AddWithValue("@PageSize", pageSize);
                cmd.Parameters.AddWithValue("@DateFrom", dateFrom);
                cmd.Parameters.AddWithValue("@DateTo", dateTo);

                con.Open();
                var reader = cmd.ExecuteReader();

                var data = EntityMapper.FillCollection<PermissionEntity>(reader);

                con.Close();

                var abc = InitPermissionToDb(nameSpace, connectStr2, data, progress, processEntity);

                return abc;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return 0;
            }
        }
        private static int InitPermissionToDb(string nameSpace, string connectStr2, List<PermissionEntity> data, IProgress<InitDataDb.ProcessEntity> progress, InitDataDb.ProcessEntity processEntity)
        {
            long id = 0;
            var count = 0;
            try
            {
                var con = GetConnection(connectStr2);
                con.Open();

                foreach (var item in data)
                {
                    string query = @"DELETE [Permission] WHERE Id=@Id;
                                    INSERT INTO [Permission]([Id]
                                                            ,[GroupId]
                                                            ,[Name]
                                                            ,[IsGrantByCategory]
                                                ) VALUES(@Id
                                                            ,@GroupId
                                                            ,@Name
                                                            ,@IsGrantByCategory);";

                    var cmd = new SqlCommand(query, con);
                    //cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandType = CommandType.Text;
                    cmd.Parameters.AddWithValue("@Id", item.Id);
                    cmd.Parameters.AddWithValue("@GroupId", item.GroupId);
                    cmd.Parameters.AddWithValue("@Name", item.Name ?? (object)DBNull.Value);
                    cmd.Parameters.AddWithValue("@IsGrantByCategory", item.IsGrantByCategory);

                    id = item.Id;
                    var reader = cmd.ExecuteNonQuery();
                    count++;

                    if (progress != null)
                    {
                        processEntity.value = count;
                        progress.Report(processEntity);
                    }
                }

                con.Close();

                return count;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message + " ID: " + id.ToString());
                return count;
            }
        }
        public static int GetCountPermission(string connectStr)
        {
            try
            {
                string query = @"SELECT COUNT(*) as [Count] FROM [Permission]";

                var con = GetConnection(connectStr);
                var cmd = new SqlCommand(query, con);

                con.Open();
                var reader = cmd.ExecuteReader();

                var data = EntityMapper.FillCollection<CountNewsEntity>(reader);

                con.Close();

                var count = 0;
                if (data != null && data.Count > 0)
                {
                    count = data.FirstOrDefault().Count;
                }

                return count;
            }
            catch (Exception ex)
            {
                return 0;
                //MessageBox.Show(ex.Message);
            }
        }
        #endregion

        #endregion
    }
}
