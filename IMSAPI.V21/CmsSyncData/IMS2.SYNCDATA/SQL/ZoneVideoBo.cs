﻿using IMS2.SYNCDATA.Common;
using IMS2.SYNCDATA.Entity;
using IMS2.SYNCDATA.ES;
using IMS2.SYNCDATA.Redis;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Windows.Forms;

namespace IMS2.SYNCDATA.SQL
{
    public class ZoneVideoBo
    {
        public static SqlConnection GetConnection(string conn="")
        {
            if (string.IsNullOrEmpty(conn))
            {
                conn = "Data Source=172.16.41.125;Initial Catalog=IMS2_AFAMILY_DEV;Persist Security Info=True;User ID=ims_test;Password=ImsTest123$%^789";
            }            
            return new SqlConnection(conn);            
        }
               
        public static bool SyncZoneVideoToEs(string nameSpace, string connectStr, string connectEs, int pageIndex, int pageSize)
        {
            try
            {                
                string query = @"CMS_User_InitAllUser";

                var con = GetConnection(connectStr);                                
                var cmd = new SqlCommand(query, con);
                cmd.CommandType = CommandType.StoredProcedure;                
                
                con.Open();
                var reader = cmd.ExecuteReader();                

                var data = EntityMapper.FillCollection<UserEntity>(reader);

                con.Close();

                var list = data.Select(s => new UserSearchEntity
                {
                    Id = s.Id,
                    FullName = s.FullName,
                    UserName = s.UserName,
                    Status = s.Status,
                    CreatedDate = s.CreatedDate
                }).ToList();

                if (pageIndex == 1)
                {
                    EsNewsBo.CreateIndexSettings<UserSearchEntity>(nameSpace, connectEs, "fullname");
                }
                var abc= EsNewsBo.CreateMany(nameSpace, connectEs, list);
                return abc;
            }
            catch (Exception ex)
            {
                return false;
                //MessageBox.Show(ex.Message);
            }
        }

        public static int GetCountZoneVideo(string connectStr)
        {
            try
            {
                string query = @"SELECT COUNT(*) as [Count] FROM ZoneVideo";

                var con = GetConnection(connectStr);
                var cmd = new SqlCommand(query, con);                               

                con.Open();
                var reader = cmd.ExecuteReader();

                var data = EntityMapper.FillCollection<CountNewsEntity>(reader);

                con.Close();

                var count = 0;
                if(data!=null && data.Count > 0)
                {
                    count = data.FirstOrDefault().Count;
                }

                return count;
            }
            catch (Exception ex)
            {
                return 0;
                //MessageBox.Show(ex.Message);
            }
        }

        public static int SyncZoneVideoToRedis(string nameSpace, string connectStr, string connectRedis, int pageIndex, int pageSize)
        {           
            try
            {
                string query = @"VideoCms_ZoneVideo_GetListByParentId";

                var con = GetConnection(connectStr);
                var cmd = new SqlCommand(query, con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@ParentId", -1);
                cmd.Parameters.AddWithValue("@Status", -1);

                con.Open();
                var reader = cmd.ExecuteReader();

                var listNews = EntityMapper.FillCollection<ZoneVideoEntity>(reader);

                con.Close();

                var data = new Dictionary<string, ZoneVideoEntity>();
                foreach (var entry in listNews)
                {
                    data[entry.Id.ToString()] = entry;                    
                }
                var abc = RedisNewsBo.AddInHash(nameSpace, connectRedis, data);

                return 1;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return 0;
            }
        }
        
    }
}
