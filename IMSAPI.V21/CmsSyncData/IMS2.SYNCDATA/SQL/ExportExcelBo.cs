﻿using IMS2.SYNCDATA.Common;
using IMS2.SYNCDATA.Entity;
using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ExcelUtility = ExcelUtilities.Library.Excel;

namespace IMS2.SYNCDATA.SQL
{
    public class ExportExcelBo
    {
        static string filePath = string.Empty;
        static ExcelUtility excel = null;
        static ExcelWorksheet ws = null;

        public static SqlConnection GetConnection(string conn = "")
        {
            if (string.IsNullOrEmpty(conn))
            {
                conn = "Data Source=172.16.41.125;Initial Catalog=IMS2_AFAMILY_DEV;Persist Security Info=True;User ID=ims_test;Password=ImsTest123$%^789";
            }
            return new SqlConnection(conn);
        }

        public static string ExportViewNews(string domain, string startDate, string endDate, List<NewsExcelEntity> items, int pageIndex, int page)
        {
            try
            {
                if (pageIndex == 1)
                {
                    var location = System.Reflection.Assembly.GetEntryAssembly().Location;
                    var rootApp = Path.GetDirectoryName(location);
                    
                    var tempExcel = rootApp+ @"\Template\ViewNewsReport.xlsx"; //@"C:\01.VCcorp\IMS\IMSAPI.V21\CmsSyncData\IMS2.SYNCDATA\Template\ViewNewsReport.xlsx";
                    var folderExport = rootApp + @"\Template\Out\"; //@"C:\01.VCcorp\IMS\IMSAPI.V21\CmsSyncData\IMS2.SYNCDATA\Template\Out\";
                    var fileName = string.Format("{0}_{1}.xlsx", domain.Replace(".vn",""), string.Format("{0:yyyymmddHHmmssFFF}", DateTime.Now));
                    filePath = folderExport.EndsWith("\\") ? folderExport + fileName : string.Format("{0}\\{1}", folderExport, fileName);
                    var rowConfig = tempExcel.Replace(".xlsx", ".xml");

                    excel = new ExcelUtility(tempExcel, filePath, rowConfig);
                    ws = excel.Worksheet(1);
                    excel.RowConfig.Header.Data.Add("startdate", startDate);
                    excel.RowConfig.Header.Data.Add("enddate", endDate);
                    excel.SetHeader(ws, excel.RowConfig.Header);
                }

                //for theo page
                excel.DataBind(ws, items);

                if (pageIndex == page)
                    excel.Save();

                return filePath;
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }

        public static int GetCountNewsExcel(string connectStr, string from, string to)
        {
            try
            {
                string query = @"SELECT COUNT(*) as [Count] FROM News n where (@DateFrom is null and (1=1)) or (@DateFrom is not null and (CONVERT(datetime,n.CreatedDate) >= CONVERT(datetime,@DateFrom) AND CONVERT(datetime,n.CreatedDate)<=CONVERT(datetime,@DateTo)))";

                var con = GetConnection(connectStr);
                var cmd = new SqlCommand(query, con);

                if (!string.IsNullOrEmpty(from) && !string.IsNullOrEmpty(to))
                {
                    cmd.Parameters.AddWithValue("@DateFrom", from);
                    cmd.Parameters.AddWithValue("@DateTo", to);
                }
                else
                {
                    cmd.Parameters.AddWithValue("@DateFrom", DBNull.Value);
                    cmd.Parameters.AddWithValue("@DateTo", DBNull.Value);
                }

                con.Open();
                var reader = cmd.ExecuteReader();

                var data = EntityMapper.FillCollection<CountNewsEntity>(reader);

                con.Close();

                var count = 0;
                if (data != null && data.Count > 0)
                {
                    count = data.FirstOrDefault().Count;
                }

                return count;
            }
            catch (Exception ex)
            {
                return 0;
                //MessageBox.Show(ex.Message);
            }
        }

        public static int SyncNewsToExcel(string connectStr, string from, string to, int pageIndex, int pageSize, int pageEx, string domain)
        {
            try
            {
                string query = @"DECLARE @UpperBand int, @LowerBand int

                                SELECT @totalRow = COUNT(*) FROM News n where (n.Status=8) AND (@DateFrom is not null and (CONVERT(datetime,n.DistributionDate) >= CONVERT(datetime,@DateFrom) AND CONVERT(datetime,n.DistributionDate)<=CONVERT(datetime,@DateTo)))

                                SET @LowerBand  = (@pageIndex - 1) * @PageSize
                                SET @UpperBand  = (@pageIndex * @PageSize)
                                SELECT * FROM (
                                SELECT n.Id,n.Title,n.DistributionDate,n.CreatedBy,n.PublishedBy,n.EditedBy,n.Tag,
                                n.Url,n.TagItem, z.Name as ZoneName,
                                ROW_NUMBER() OVER(ORDER BY n.DistributionDate) AS RowNumber 
                                FROM News n 
                                left join NewsInZone nz on nz.NewsId=n.Id and nz.isprimary=1
                                left join Zone z on z.Id=nz.ZoneId
                                where (n.Status=8) AND (@DateFrom is not null and (CONVERT(datetime,n.DistributionDate) >= CONVERT(datetime,@DateFrom) AND CONVERT(datetime,n.DistributionDate)<=CONVERT(datetime,@DateTo)))
                                ) AS temp
                                WHERE RowNumber > @LowerBand AND RowNumber <= @UpperBand";                

                var con = GetConnection(connectStr);
                using (var cmd = new SqlCommand(query, con))
                {
                    var totalRows = 0;
                    var totalRow = cmd.CreateParameter();
                    totalRow.ParameterName = "@TotalRow";
                    totalRow.Value = totalRows;
                    totalRow.Direction = ParameterDirection.Output;

                    cmd.Parameters.Add(totalRow);
                    cmd.Parameters.AddWithValue("@PageIndex", pageIndex);
                    cmd.Parameters.AddWithValue("@PageSize", pageSize);
                    if (!string.IsNullOrEmpty(from) && !string.IsNullOrEmpty(to))
                    {
                        cmd.Parameters.AddWithValue("@DateFrom", from);
                        cmd.Parameters.AddWithValue("@DateTo", to);
                    }
                    else
                    {
                        cmd.Parameters.AddWithValue("@DateFrom", DBNull.Value);
                        cmd.Parameters.AddWithValue("@DateTo", DBNull.Value);
                    }

                    con.Open();
                    var reader = cmd.ExecuteReader();

                    var data = EntityMapper.FillCollection<NewsExcelEntity>(reader);

                    con.Close();                    

                    ExportViewNews(domain, from, to, GetViewAdtech(data,domain), pageIndex, pageEx);                    

                    return 1;
                }
            }
            catch (Exception ex)
            {
                Logger.Log(TypeLog.Error, "SyncNewsToExcel => " + ex.Message + ", pageIndex: " + pageIndex);
                MessageBox.Show(ex.Message);
                return 0;
            }
        }

        public static List<NewsExcelEntity> GetViewAdtech(List<NewsExcelEntity> items, string domain)
        {
            try
            {
                var listIds = items.Select(n => n.Id).ToList();
                var url = "https://cmsanalytics.admicro.vn/analytics/cms_api/{0}/item/report?news_ids={1}";//CmsChannelConfiguration.GetAppSetting("UrlGetViewCount");
                var secretKey = "5e80e09f5155411e90153db772ce084b";//CmsChannelConfiguration.GetAppSetting("UrlGetViewCountSecretKey");

                var newsIds = "[" + string.Join(",", listIds) + "]";
                url = string.Format(url, domain, newsIds);

                WebRequest request = WebRequest.Create(url);
                request.Headers.Add("cms_api_key", secretKey ?? "5e80e09f5155411e90153db772ce084b");
                WebResponse response = request.GetResponse();
                Stream data = response.GetResponseStream();
                string html = string.Empty;
                using (StreamReader sr = new StreamReader(data))
                {
                    html = sr.ReadToEnd();
                }

                var dataViewCountDynamic = NewtonJson.Deserialize<dynamic>(html);
                var dataViewCount = new List<NewsExcelEntity>();
                for (int i = 0; i < dataViewCountDynamic.Count; i++)
                {
                    dataViewCount.Add(new NewsExcelEntity()
                    {
                        Id = dataViewCountDynamic[i].item_id,
                        ViewCount = dataViewCountDynamic[i].total_view.view_pc + dataViewCountDynamic[i].total_view.view_mob,
                        ViewPcCount = dataViewCountDynamic[i].total_view.view_pc,
                        ViewMobCount = dataViewCountDynamic[i].total_view.view_mob
                    });
                }


                foreach (var item in items)
                {
                    var newsItem = dataViewCount.SingleOrDefault(n => n.Id == item.Id);
                    if (newsItem != null)
                    {
                        item.ViewCount = newsItem.ViewCount;
                        item.ViewPcCount = newsItem.ViewPcCount;
                        item.ViewMobCount = newsItem.ViewMobCount;                        
                        item.Url = string.Format("http://{0}"+item.Url,domain);
                        item.Domain = domain;
                    }
                }
            }
            catch (Exception err)
            {
                return items;
            }

            return items;
        }
    }
}
