﻿using IMS2.SYNCDATA.Common;
using IMS2.SYNCDATA.Entity;
using IMS2.SYNCDATA.ES;
using IMS2.SYNCDATA.Redis;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Windows.Forms;
using HtmlAgilityPack;

namespace IMS2.SYNCDATA.SQL
{
    public class NewsBo
    {
        public static SqlConnection GetConnection(string conn="")
        {
            if (string.IsNullOrEmpty(conn))
            {
                conn = "Data Source=172.16.41.125;Initial Catalog=IMS2_AFAMILY_DEV;Persist Security Info=True;User ID=ims_test;Password=ImsTest123$%^789";
            }            
            return new SqlConnection(conn);            
        }

        public static List<ZoneEntity> GetZoneByNewsId(string conn, long id)
        {
            var con = GetConnection(conn);
            try
            {
                string query = @"SELECT Z.Id, Z.Name, Z.Description, Z.ModifiedDate, Z.CreatedDate, Z.ShortURL, Z.SortOrder, 
		                            Z.ParentId, Z.Invisibled, Z.Status, NZ.IsPrimary
	                            FROM Zone AS Z INNER JOIN
                                    NewsInZone AS NZ with(nolock) ON Z.Id = NZ.ZoneId
	                            WHERE NZ.NewsId = @NewsId";
                
                var cmd = new SqlCommand(query, con);                
                cmd.Parameters.AddWithValue("@NewsId", id);

                con.Open();
                var reader = cmd.ExecuteReader();
                var data = EntityMapper.FillCollection<ZoneEntity>(reader);
                con.Close();

                return data;
            }
            catch (Exception ex)
            {
                con.Close();
                con.Dispose();
                Logger.Log(TypeLog.Error, "GetZoneByNewsId=> " + ex.Message);
                return new List<ZoneEntity>();
            }
            finally
            {
                con.Close();
                con.Dispose();
            }
        }

        public static int SyncNewsToEs(string nameSpace, string connectStr, string connectEs, int pageIndex, int pageSize, string from, string to, bool isDeleteIndex=false)
        {
            try
            {
                string query = @"DECLARE @UpperBand int, @LowerBand int

                                SELECT @totalRow = COUNT(*) FROM News n where (@DateFrom is null and (1=1)) or (@DateFrom is not null and (CONVERT(datetime,n.CreatedDate) >= CONVERT(datetime,@DateFrom) AND CONVERT(datetime,n.CreatedDate)<=CONVERT(datetime,@DateTo)))

                                SET @LowerBand  = (@pageIndex - 1) * @PageSize
                                SET @UpperBand  = (@pageIndex * @PageSize)
                                SELECT * FROM (
                                SELECT n.Id,n.Title,n.SubTitle,n.Sapo,n.Body,n.Avatar,n.AvatarDesc,n.Avatar2,n.Avatar3,n.Avatar4,n.Avatar5,n.Author,n.NewsRelation,n.Status,
                                n.Source,n.IsFocus,n.Type,n.ThreadId,n.CreatedDate,n.LastModifiedDate,n.DistributionDate,n.CreatedBy,n.LastModifiedBy,n.PublishedBy,n.EditedBy,
                                n.LastReceiver,n.WordCount,n.ViewCount,n.Priority,n.Tag,n.Note,n.TagPrimary,n.Price,n.DisplayStyle,n.DisplayPosition,n.DisplayInSlide,n.AvatarCustom,
                                n.OriginalId,n.NewsType,n.IsOnHome,n.Url,n.NoteRoyalties,n.TagItem,n.NewsCategory,n.InitSapo,n.TemplateName,n.TemplateConfig,n.InterviewId,n.IsBreakingNews,
                                n.OriginalUrl,n.IsPr,n.AdStore,n.AdStoreUrl,n.PrBookingNumber,n.PegaBreakingNews,n.RollingNewsId,n.IsOnMobile,n.TagSubTitleId,n.PrPosition,n.LocationType,
                                n.ExpiredDate,n.SourceUrl,n.BonusPrice,n.ViewCountMobile,n.PhotoCount,n.VideoCount,n.ShortTitle,n.ParentNewsId,n.WarningLevel,n.ApprovedBy,
                                n.ApprovedDate,n.ReturnedBy,n.SentBy,n.ErrorCheckedBy,n.ErrorCheckedDate,n.SensitiveCheckedBy,n.SensitiveCheckedDate, z.Name as ZoneName, n.IsProd,
                                ROW_NUMBER() OVER(ORDER BY n.Id DESC) AS RowNumber 
                                FROM News n 
                                left join NewsInZone nz with(nolock) on nz.NewsId=n.Id and nz.isprimary=1
                                left join Zone z on z.Id=nz.ZoneId
                                where (@DateFrom is null and (1=1)) or (@DateFrom is not null and (CONVERT(datetime,n.CreatedDate) >= CONVERT(datetime,@DateFrom) AND CONVERT(datetime,n.CreatedDate)<=CONVERT(datetime,@DateTo)))
                                ) AS temp
                                WHERE RowNumber > @LowerBand AND RowNumber <= @UpperBand";

                //string query = @"CMS_News_InitESAllNews";

                var con = GetConnection(connectStr);
                using (var cmd = new SqlCommand(query, con))
                {                                        
                    var totalRows = 0;
                    var totalRow = cmd.CreateParameter();
                    totalRow.ParameterName = "@TotalRow";
                    totalRow.Value = totalRows;
                    totalRow.Direction = ParameterDirection.Output;

                    cmd.Parameters.Add(totalRow);
                    cmd.Parameters.AddWithValue("@PageIndex", pageIndex);
                    cmd.Parameters.AddWithValue("@PageSize", pageSize);
                    if (!string.IsNullOrEmpty(from) && !string.IsNullOrEmpty(to))
                    {
                        cmd.Parameters.AddWithValue("@DateFrom", from);
                        cmd.Parameters.AddWithValue("@DateTo", to);
                    }
                    else
                    {
                        cmd.Parameters.AddWithValue("@DateFrom", DBNull.Value);
                        cmd.Parameters.AddWithValue("@DateTo", DBNull.Value);
                    }

                    con.Open();
                    var reader = cmd.ExecuteReader();

                    var data = EntityMapper.FillCollection<NewsEntity>(reader);

                    con.Close();

                    var list = data.Select(s => new NewsSearchEntity
                    {
                        Id = s.Id,
                        EncryptId = s.Id.ToString(),
                        Title = s.Title,
                        ZoneIds = GetZoneByNewsId(connectStr, s.Id).Select(z => z.Id.ToString()).ToArray(),
                        Type = s.Type,
                        CreatedBy = s.CreatedBy,
                        LastModifiedBy = s.LastModifiedBy,
                        EditedBy = s.EditedBy,
                        PublishedBy = s.PublishedBy,
                        LastReceiver = s.LastReceiver,
                        ApprovedBy = s.ApprovedBy,
                        ReturnedBy = s.ReturnedBy,
                        CreatedDate = s.CreatedDate,
                        DistributionDate = s.DistributionDate,
                        LastModifiedDate = s.LastModifiedDate,
                        ApprovedDate = s.ApprovedDate,
                        ViewCount = s.ViewCount,
                        Status = s.Status,
                        IsOnHome = s.IsOnHome,
                        Avatar = s.Avatar,
                        ViewRoyalties = 0,
                        ZoneName = s.ZoneName,
                        Note = s.Note,
                        Author = s.Author,
                        ErrorCheckedBy = s.ErrorCheckedBy,
                        ErrorCheckedDate = s.ErrorCheckedDate,
                        SensitiveCheckedBy = s.SensitiveCheckedBy,
                        SensitiveCheckedDate = s.SensitiveCheckedDate,
                        WordCount = s.WordCount,
                        Sapo = s.Sapo,
                        Url = s.Url,
                        DisplayPosition = s.DisplayPosition,
                        Priority = s.Priority,
                        Source = s.Source,
                        IsFocus=s.IsFocus,
                        NewsType=s.NewsType,
                        IsProd=s.IsProd,
                        IsPr=s.IsPr,
                        OriginalId=s.OriginalId
                    }).ToList();

                    //var cis = false;
                    if (pageIndex == 1)
                    {
                        if (!string.IsNullOrEmpty(from) && !string.IsNullOrEmpty(to))
                        {
                            EsNewsBo.CreateIndexSettings<NewsSearchEntity>(nameSpace, connectEs, "title", "source");
                        }
                        else {
                            if(isDeleteIndex)
                                EsNewsBo.DeleteIndex(nameSpace, connectEs, "NewsSearchEntity");
                            EsNewsBo.CreateIndexSettings<NewsSearchEntity>(nameSpace, connectEs, "title", "source");
                        }
                    }

                    EsNewsBo.CreateMany(nameSpace, connectEs, list);

                    return 1;
                }
            }
            catch (Exception ex)
            {
                Logger.Log(TypeLog.Error, "SyncNewsToEs => " + ex.Message + ", pageIndex: "+ pageIndex);
                MessageBox.Show(ex.Message);                               
                return 0;                
            }
        }

        public static int SyncNewsToEsIMS1(string nameSpace, string connectStr, string connectEs, int pageIndex, int pageSize, string from, string to, bool isDeleteIndex = false)
        {
            try
            {
                string query = @"DECLARE @UpperBand int, @LowerBand int

                                SELECT @totalRow = COUNT(*) FROM News n where (@DateFrom is null and (1=1)) or (@DateFrom is not null and (CONVERT(datetime,n.CreatedDate) >= CONVERT(datetime,@DateFrom) AND CONVERT(datetime,n.CreatedDate)<=CONVERT(datetime,@DateTo)))

                                SET @LowerBand  = (@pageIndex - 1) * @PageSize
                                SET @UpperBand  = (@pageIndex * @PageSize)
                                SELECT * FROM (
                                SELECT n.Id,n.Title,n.SubTitle,n.Sapo,n.Body,n.Avatar,n.AvatarDesc,n.Avatar2,n.Avatar3,n.Avatar4,n.Avatar5,n.Author,n.NewsRelation,n.Status,
                                n.Source,n.IsFocus,n.Type,n.ThreadId,n.CreatedDate,n.LastModifiedDate,n.DistributionDate,n.CreatedBy,n.LastModifiedBy,n.PublishedBy,n.EditedBy,
                                n.LastReceiver,n.WordCount,n.ViewCount,n.Priority,n.Tag,n.Note,n.TagPrimary,n.Price,n.DisplayStyle,n.DisplayPosition,n.DisplayInSlide,n.AvatarCustom,
                                n.OriginalId,n.NewsType,n.IsOnHome,n.Url,n.NoteRoyalties,n.TagItem,n.NewsCategory,n.InitSapo,n.TemplateName,n.TemplateConfig,n.InterviewId,n.IsBreakingNews,
                                n.OriginalUrl,n.IsPr,n.AdStore,n.AdStoreUrl,n.PrBookingNumber,n.PegaBreakingNews,n.RollingNewsId,n.IsOnMobile,n.TagSubTitleId,n.PrPosition,n.LocationType,
                                n.ExpiredDate,n.SourceUrl,n.BonusPrice,n.ViewCountMobile,n.PhotoCount,n.VideoCount,n.ParentNewsId,n.WarningLevel,
                                ROW_NUMBER() OVER(ORDER BY n.Id DESC) AS RowNumber 
                                FROM News n 
                                left join NewsInZone nz with(nolock) on nz.NewsId=n.Id and nz.isprimary=1
                                left join Zone z on z.Id=nz.ZoneId
                                where (@DateFrom is null and (1=1)) or (@DateFrom is not null and (CONVERT(datetime,n.CreatedDate) >= CONVERT(datetime,@DateFrom) AND CONVERT(datetime,n.CreatedDate)<=CONVERT(datetime,@DateTo)))
                                ) AS temp
                                WHERE RowNumber > @LowerBand AND RowNumber <= @UpperBand";

                //string query = @"CMS_News_InitESAllNews";

                var con = GetConnection(connectStr);
                using (var cmd = new SqlCommand(query, con))
                {
                    var totalRows = 0;
                    var totalRow = cmd.CreateParameter();
                    totalRow.ParameterName = "@TotalRow";
                    totalRow.Value = totalRows;
                    totalRow.Direction = ParameterDirection.Output;

                    cmd.Parameters.Add(totalRow);
                    cmd.Parameters.AddWithValue("@PageIndex", pageIndex);
                    cmd.Parameters.AddWithValue("@PageSize", pageSize);
                    if (!string.IsNullOrEmpty(from) && !string.IsNullOrEmpty(to))
                    {
                        cmd.Parameters.AddWithValue("@DateFrom", from);
                        cmd.Parameters.AddWithValue("@DateTo", to);
                    }
                    else
                    {
                        cmd.Parameters.AddWithValue("@DateFrom", DBNull.Value);
                        cmd.Parameters.AddWithValue("@DateTo", DBNull.Value);
                    }

                    con.Open();
                    var reader = cmd.ExecuteReader();

                    var data = EntityMapper.FillCollection<NewsEntity>(reader);

                    con.Close();

                    var list = data.Select(s => new NewsSearchEntity
                    {
                        Id = s.Id,
                        EncryptId = s.Id.ToString(),
                        Title = s.Title,
                        ZoneIds = GetZoneByNewsId(connectStr, s.Id).Select(z => z.Id.ToString()).ToArray(),
                        Type = s.Type,
                        CreatedBy = s.CreatedBy,
                        LastModifiedBy = s.LastModifiedBy,
                        EditedBy = s.EditedBy,
                        PublishedBy = s.PublishedBy,
                        LastReceiver = s.LastReceiver,
                        ApprovedBy = s.ApprovedBy,
                        ReturnedBy = s.ReturnedBy,
                        CreatedDate = s.CreatedDate,
                        DistributionDate = s.DistributionDate,
                        LastModifiedDate = s.LastModifiedDate,
                        ApprovedDate = s.ApprovedDate,
                        ViewCount = s.ViewCount,
                        Status = s.Status,
                        IsOnHome = s.IsOnHome,
                        Avatar = s.Avatar,
                        ViewRoyalties = 0,
                        ZoneName = s.ZoneName,
                        Note = s.Note,
                        Author = s.Author,
                        ErrorCheckedBy = s.ErrorCheckedBy,
                        ErrorCheckedDate = s.ErrorCheckedDate,
                        SensitiveCheckedBy = s.SensitiveCheckedBy,
                        SensitiveCheckedDate = s.SensitiveCheckedDate,
                        WordCount = s.WordCount,
                        Sapo = s.Sapo,
                        Url = s.Url,
                        DisplayPosition = s.DisplayPosition,
                        Priority = s.Priority,
                        Source = s.Source,
                        IsFocus = s.IsFocus,
                        NewsType = s.NewsType,
                        IsProd = s.IsProd,
                        IsPr = s.IsPr,
                        OriginalId = s.OriginalId
                    }).ToList();

                    //var cis = false;
                    if (pageIndex == 1)
                    {
                        if (!string.IsNullOrEmpty(from) && !string.IsNullOrEmpty(to))
                        {
                            EsNewsBo.CreateIndexSettings<NewsSearchEntity>(nameSpace, connectEs, "title", "source");
                        }
                        else
                        {
                            if (isDeleteIndex)
                                EsNewsBo.DeleteIndex(nameSpace, connectEs, "NewsSearchEntity");
                            EsNewsBo.CreateIndexSettings<NewsSearchEntity>(nameSpace, connectEs, "title", "source");
                        }
                    }

                    EsNewsBo.CreateMany(nameSpace, connectEs, list);

                    return 1;
                }
            }
            catch (Exception ex)
            {
                Logger.Log(TypeLog.Error, "SyncNewsToEs => " + ex.Message + ", pageIndex: " + pageIndex);
                MessageBox.Show(ex.Message);
                return 0;
            }
        }

        public static int GetCountNewsES(string connectStr, string from, string to)
        {
            try
            {
                string query = @"SELECT COUNT(*) as [Count] FROM News n
                                left join NewsInZone nz with(nolock) on nz.NewsId=n.Id
                                left join Zone z on z.Id=nz.ZoneId
                                where nz.IsPrimary=1 and (@DateFrom is null and (1=1)) or (@DateFrom is not null and (n.CreatedDate>=@DateFrom and n.CreatedDate<=@DateTo))";                

                var con = GetConnection(connectStr);
                using (var cmd = new SqlCommand(query, con))
                {
                    if (!string.IsNullOrEmpty(from) && !string.IsNullOrEmpty(to))
                    {
                        cmd.Parameters.AddWithValue("@DateFrom", from);
                        cmd.Parameters.AddWithValue("@DateTo", to);
                    }
                    else
                    {
                        cmd.Parameters.AddWithValue("@DateFrom", DBNull.Value);
                        cmd.Parameters.AddWithValue("@DateTo", DBNull.Value);
                    }

                    con.Open();
                    var reader = cmd.ExecuteReader();

                    var data = EntityMapper.FillCollection<CountNewsEntity>(reader);

                    con.Close();

                    var count = 0;
                    if (data != null && data.Count > 0)
                    {
                        count = data.FirstOrDefault().Count;
                    }

                    return count;
                }
            }
            catch (Exception ex)
            {
                Logger.Log(TypeLog.Error, "GetCountNewsES => " + ex.Message);
                return 0;
                //MessageBox.Show(ex.Message);
            }
        }

        public static int SyncNewsToRedis(string nameSpace, string connectStr, string connectRedis, int pageIndex, int pageSize)
        {           
            try
            {
                string query = @"DECLARE @UpperBand int, @LowerBand int

                                SELECT @totalRow = COUNT(*) FROM News n where (@DateFrom is null and (1=1)) or (@DateFrom is not null and (CONVERT(datetime,n.CreatedDate) >= CONVERT(datetime,@DateFrom) AND CONVERT(datetime,n.CreatedDate)<=CONVERT(datetime,@DateTo)))

                                SET @LowerBand  = (@pageIndex - 1) * @PageSize
                                SET @UpperBand  = (@pageIndex * @PageSize)
                                SELECT * FROM (
                                SELECT n.Id,n.Title,n.SubTitle,n.Sapo,n.Body,n.Avatar,n.AvatarDesc,n.Avatar2,n.Avatar3,n.Avatar4,n.Avatar5,n.Author,n.NewsRelation,n.Status,
                                n.Source,n.IsFocus,n.Type,n.ThreadId,n.CreatedDate,n.LastModifiedDate,n.DistributionDate,n.CreatedBy,n.LastModifiedBy,n.PublishedBy,n.EditedBy,
                                n.LastReceiver,n.WordCount,n.ViewCount,n.Priority,n.Tag,n.Note,n.TagPrimary,n.Price,n.DisplayStyle,n.DisplayPosition,n.DisplayInSlide,n.AvatarCustom,
                                n.OriginalId,n.NewsType,n.IsOnHome,n.Url,n.NoteRoyalties,n.TagItem,n.NewsCategory,n.InitSapo,n.TemplateName,n.TemplateConfig,n.InterviewId,n.IsBreakingNews,
                                n.OriginalUrl,n.IsPr,n.AdStore,n.AdStoreUrl,n.PrBookingNumber,n.PegaBreakingNews,n.RollingNewsId,n.IsOnMobile,n.TagSubTitleId,n.PrPosition,n.LocationType,
                                n.ExpiredDate,n.SourceUrl,n.BonusPrice,n.ViewCountMobile,n.PhotoCount,n.VideoCount,n.ShortTitle,n.ParentNewsId,n.WarningLevel,n.ApprovedBy,
                                n.ApprovedDate,n.ReturnedBy,n.SentBy,n.ErrorCheckedBy,n.ErrorCheckedDate,n.SensitiveCheckedBy,n.SensitiveCheckedDate,n.IsProd, dbo.CMS_fGetListOfZoneIdByNewsId(n.Id) AS ListZoneId,
                                ROW_NUMBER() OVER(ORDER BY n.Id DESC) AS RowNumber 
                                FROM News n 
                                where (@DateFrom is null and (1=1)) or (@DateFrom is not null and (CONVERT(datetime,n.CreatedDate) >= CONVERT(datetime,@DateFrom) AND CONVERT(datetime,n.CreatedDate)<=CONVERT(datetime,@DateTo)))
                                ) AS temp
                                WHERE RowNumber > @LowerBand AND RowNumber <= @UpperBand";

                //string query = @"CMS_News_InitRedisAllNews";

                var con = GetConnection(connectStr);
                var cmd = new SqlCommand(query, con);
                //cmd.CommandType = CommandType.StoredProcedure;
                //var pageIndex = 1;
                //var pageSize = 10;
                var dateFrom = DBNull.Value;
                var dateTo = DBNull.Value;
                var totalRows = 0;

                var totalRow = cmd.CreateParameter();
                totalRow.ParameterName = "@TotalRow";
                totalRow.Value = totalRows;
                totalRow.Direction = ParameterDirection.Output;

                cmd.Parameters.Add(totalRow);
                cmd.Parameters.AddWithValue("@PageIndex", pageIndex);
                cmd.Parameters.AddWithValue("@PageSize", pageSize);
                cmd.Parameters.AddWithValue("@DateFrom", dateFrom);
                cmd.Parameters.AddWithValue("@DateTo", dateTo);

                con.Open();
                var reader = cmd.ExecuteReader();

                var listNews = EntityMapper.FillCollection<NewsEntity>(reader);

                con.Close();

                var data = new Dictionary<string, NewsCachedEntity>();                
                foreach (var entry in listNews)
                {
                    data[entry.Id.ToString()] = new NewsCachedEntity() { NewsInfo = entry };
                }
                if(pageIndex==1)
                    RedisNewsBo.Remove(nameSpace, connectRedis, "NewsCachedEntity");                

                var abc= RedisNewsBo.AddInHash(nameSpace, connectRedis, data);                

                return 1;
            }
            catch (Exception ex)
            {
                Logger.Log(TypeLog.Error, "SyncNewsToRedis => " + ex.Message + ", pageIndex: " + pageIndex);
                MessageBox.Show(ex.Message);
                return 0;
            }        
        }

        public static int SyncNewsToRedisUpdateBody(string nameSpace, string connectStr, string connectRedis, int pageIndex, int pageSize)
        {
            try
            {
                string query = @"DECLARE @UpperBand int, @LowerBand int

                                SELECT @totalRow = COUNT(*) FROM News n where (@DateFrom is null and (1=1)) or (@DateFrom is not null and (CONVERT(datetime,n.CreatedDate) >= CONVERT(datetime,@DateFrom) AND CONVERT(datetime,n.CreatedDate)<=CONVERT(datetime,@DateTo)))

                                SET @LowerBand  = (@pageIndex - 1) * @PageSize
                                SET @UpperBand  = (@pageIndex * @PageSize)
                                SELECT * FROM (
                                SELECT n.Id,n.Title,n.SubTitle,n.Sapo,n.Body,n.Avatar,n.AvatarDesc,n.Avatar2,n.Avatar3,n.Avatar4,n.Avatar5,n.Author,n.NewsRelation,n.Status,
                                n.Source,n.IsFocus,n.Type,n.ThreadId,n.CreatedDate,n.LastModifiedDate,n.DistributionDate,n.CreatedBy,n.LastModifiedBy,n.PublishedBy,n.EditedBy,
                                n.LastReceiver,n.WordCount,n.ViewCount,n.Priority,n.Tag,n.Note,n.TagPrimary,n.Price,n.DisplayStyle,n.DisplayPosition,n.DisplayInSlide,n.AvatarCustom,
                                n.OriginalId,n.NewsType,n.IsOnHome,n.Url,n.NoteRoyalties,n.TagItem,n.NewsCategory,n.InitSapo,n.TemplateName,n.TemplateConfig,n.InterviewId,n.IsBreakingNews,
                                n.OriginalUrl,n.IsPr,n.AdStore,n.AdStoreUrl,n.PrBookingNumber,n.PegaBreakingNews,n.RollingNewsId,n.IsOnMobile,n.TagSubTitleId,n.PrPosition,n.LocationType,
                                n.ExpiredDate,n.SourceUrl,n.BonusPrice,n.ViewCountMobile,n.PhotoCount,n.VideoCount,n.ShortTitle,n.ParentNewsId,n.WarningLevel,n.ApprovedBy,
                                n.ApprovedDate,n.ReturnedBy,n.SentBy,n.ErrorCheckedBy,n.ErrorCheckedDate,n.SensitiveCheckedBy,n.SensitiveCheckedDate,n.IsProd, --dbo.CMS_fGetListOfZoneIdByNewsId(n.Id) AS ListZoneId,
                                ROW_NUMBER() OVER(ORDER BY n.Id DESC) AS RowNumber 
                                FROM News n 
                                where (@DateFrom is null and (1=1)) or (@DateFrom is not null and (CONVERT(datetime,n.CreatedDate) >= CONVERT(datetime,@DateFrom) AND CONVERT(datetime,n.CreatedDate)<=CONVERT(datetime,@DateTo)))
                                ) AS temp
                                WHERE RowNumber > @LowerBand AND RowNumber <= @UpperBand";

                //string query = @"CMS_News_InitRedisAllNews";

                var con = GetConnection(connectStr);
                var cmd = new SqlCommand(query, con);
                //cmd.CommandType = CommandType.StoredProcedure;
                //var pageIndex = 1;
                //var pageSize = 10;
                var dateFrom = DBNull.Value;
                var dateTo = DBNull.Value;
                var totalRows = 0;

                var totalRow = cmd.CreateParameter();
                totalRow.ParameterName = "@TotalRow";
                totalRow.Value = totalRows;
                totalRow.Direction = ParameterDirection.Output;

                cmd.Parameters.Add(totalRow);
                cmd.Parameters.AddWithValue("@PageIndex", pageIndex);
                cmd.Parameters.AddWithValue("@PageSize", pageSize);
                cmd.Parameters.AddWithValue("@DateFrom", dateFrom);
                cmd.Parameters.AddWithValue("@DateTo", dateTo);

                con.Open();
                var reader = cmd.ExecuteReader();

                var listNews = EntityMapper.FillCollection<NewsEntity>(reader);

                con.Close();

                var data = new Dictionary<string, NewsCachedEntity>();
                var listBody = new List<NewsEntity>();
                foreach (var entry in listNews)
                {
                    //regex body
                    var body = replaceBody(entry.Body);
                    if (!string.IsNullOrEmpty(body))
                    {
                        listBody.Add(entry);
                        entry.Body = body;
                        data[entry.Id.ToString()] = new NewsCachedEntity() { NewsInfo = entry };
                    }
                }
                if (pageIndex == 1)
                    RedisNewsBo.Remove(nameSpace, connectRedis, "NewsCachedEntity");

                var db = UpdateBody(nameSpace, connectStr, listBody);

                var abc = RedisNewsBo.AddInHash(nameSpace, connectRedis, data);

                Logger.WriteFile(string.Join(",", listBody.Select(s => s.Id).ToList()), "nguon_listidinit" + pageIndex + "_" + listBody.Count);

                return 1;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return 0;
            }
        }

        private static string replaceBody(string body)
        {
            var data = body;//"<div class=\"toc-list-headings VCSortableInPreviewMode alignLeft\" data-heading-select=\"h2, h3\" type=\"toc\"><div class=\"toc-title\">Nội dung:</div><ul><li class=\"toc-level toc-level-0\"><a href=\"#1-sai-lam-khi-dieu-tri-viem-hong-dung-nuoc-ep-toi-de-nho-mui\">1. Sai lầm khi điều trị viêm họng: Dùng nước ép tỏi để nhỏ mũi</a></li><li class=\"toc-level toc-level-0\"><a href=\"#2-lam-dung-khang-sinh\">2. Lạm dụng kháng sinh</a></li><li class=\"toc-level toc-level-0\"><a href=\"#3-suc-hong-bang-nuoc-muoi-sai-cach\">3. Súc họng bằng nước muối sai cách  </a></li></ul></div><p>Bệnh viêm họng có thể xuất phát từ nguyên nhân như virus, vi khuẩn, thời tiết thay đổi, môi trường sống quá ô nhiễm, quá lạnh hoặc quá ẩm thấp.</p><p>Viêm họng nhẹ với biểu hiện ban đầu là sốt, sổ mũi, tuy nhiên nếu bệnh biến chứng thành viêm đường hô hấp thì sẽ nguy hiểm. Trong nhiều trường hợp, người bệnh tự ý điều trị viêm họng sẽ khiến bệnh tình ngày càng nặng hơn và có nguy cơ trở thành bệnh mãn tính</p><h2 id=\"1-sai-lam-khi-dieu-tri-viem-hong-dung-nuoc-ep-toi-de-nho-mui\">1. Sai lầm khi điều trị viêm họng: Dùng nước ép tỏi để nhỏ mũi</h2><p>Khi bị viêm mũi họng, nhiều bệnh nhân thường truyền nhau mẹo ép tỏi rồi trộn với nước muối sinh lý để nhỏ mũi. Mục đích của việc làm này là để rửa mũi, trị chứng sổ mũi, hắt hơi.</p><p>Tuy nhiên, đây là một quan niệm rất sai lầm khi điều trị viêm họng, đặc biệt là ở trẻ nhỏ. Tỏi chứa chất Allicin có khả năng diệt nấm và vi trùng, đồng thời phòng ngừa cúm. Tuy nhiên mặt khác nó có thể gây nóng rát, phù nề và làm bỏng niêm mạc mũi của trẻ. Nhất là khi niêm mạc mũi của trẻ rất mỏng và dễ bị tổn thương. Ngay cả người lớn cũng có thể bị bỏng niêm mạc mũi nếu sử dụng nước ép tỏi có nồng độ quá đặc.</p><p>Niêm mạc mũi bị bỏng rộp sẽ khiến bệnh nhân khó thở, phải bằng mũi mà buộc phải thở bằng miệng, dễ gây viêm đường hô hấp. Nguy hiểm hơn nữa, niêm mạc mũi bị bỏng rộp có thể dẫn tới hoại tử nếu không được điều trị sớm. Do đó, các mẹ nếu có con bị viêm mũi họng tuyệt đối không nên tự ý dùng nước ép tỏi nhỏ vào mũi của trẻ và người lớn cũng vậy.&nbsp;</p><h2 id=\"2-lam-dung-khang-sinh\">2. Lạm dụng kháng sinh</h2><p>Viêm họng là một trong số những bệnh dễ bị lạm dụng thuốc kháng sinh nhất. Đây là một sai lầm khi điều trị viêm họng mà rất nhiều người mắc phải. Đối với trẻ nhỏ, Không ít phụ huynh hễ con ốm sốt, hắt hơi, sổ mũi là có thói quen chạy ra hiệu thuốc mua liều kháng sinh về cho con uống. Trên thực tế thói quen này vô cùng nguy hiểm vì về lâu dài sẽ gây ra tình trạng nhờn kháng sinh, thậm chí nếu cho trẻ uống nhiều loại kháng sinh liên tục còn có thể gây ra tình trạng kháng kháng sinh.</p><p>Theo các bác sĩ nhi khoa, trẻ bị viêm mũi họng không cứ phải dùng kháng sinh. Tất nhiên, lúc cần thiết thì cũng phải dùng kháng sinh nhưng trong hầu hết các trường hợp việc điều trị bệnh này có thể chỉ cần dùng nước muối thật nhiều để rửa sạch mũi họng và kèm theo vài loại thuốc trị ho đơn giản là đủ. Quan trọng nhất là trong quá trình đó phải tìm cách tăng cường sức đề kháng cho trẻ nhỏ.</p><p>Đối với người lớn, việc sử dụng thuốc kháng sinh khi chưa có chỉ định của bác sĩ cũng sẽ khiến bạn có nguy cơ đối mặt với việc nhờn thuốc, bệnh tái đi tái lại nhiều lần, không những vậy kháng sinh còn có thể khiến người bệnh mệt mỏi, gặp phải một số tác dụng phụ.&nbsp;</p><div class=\"VCSortableInPreviewMode alignRight\" relatednewsboxtype=\"type-2\" type=\"RelatedNewsBox\" data-style=\"align-right\"><div class=\"kbwscwl-relatedbox type-2\"> <ul class=\"kbwscwlr-list\"><li class=\"kbwscwlrl\" data-id=\"20190417084435891\" data-avatar=\"http://suckhoehangngay.mediacdn.vn/2019/4/16/phan-loai-viem-hong-15553796624141935992700-crop-15554651788951571614671.jpg\" data-url=\"/viem-hong-do-virus-co-nen-su-dung-khang-sinh-hay-khong-20190417084435891.htm\" data-title=\"Viêm họng do virus: Có nên sử dụng kháng sinh hay không?\"> <h3 class=\"kbwscwlrl-title\"><a target=\"_blank\" href=\"/viem-hong-do-virus-co-nen-su-dung-khang-sinh-hay-khong-20190417084435891.htm\" class=\"title link-callout\">Viêm họng do virus: Có nên sử dụng kháng sinh hay không?</a> </h3> </li><li class=\"kbwscwlrl\" data-id=\"20180804161252546\" data-avatar=\"http://suckhoehangngay.mediacdn.vn/2018/8/4/lam-dung-thuoc-khang-sinh-va-nhung-hau-qua-khon-luong1-1533373547114176635666-63-0-638-1024-crop-1533373557359397054755.jpg\" data-url=\"/lam-dung-thuoc-khang-sinh-va-nhung-hau-qua-khon-luong-20180804161252546.htm\" data-title=\"Lạm dụng thuốc kháng sinh và những hậu quả khôn lường\"> <h3 class=\"kbwscwlrl-title\"><a target=\"_blank\" href=\"/lam-dung-thuoc-khang-sinh-va-nhung-hau-qua-khon-luong-20180804161252546.htm\" class=\"title link-callout\">Lạm dụng thuốc kháng sinh và những hậu quả khôn lường</a> </h3> </li></ul> </div></div><h2 id=\"3-suc-hong-bang-nuoc-muoi-sai-cach\">3. Súc họng bằng nước muối sai cách  </h2><p>Nước muối ấm có một công dụng tuyệt vời đối với sức khỏe, đơn cử như kích thích lưu thông máu tại chỗ, tập trung tế bào bạch cầu nhằm mục đích tiêu diệt vi khuẩn và làm chắc chân răng. Súc miệng bằng nước muối một vài lần trong ngày có thể giúp giảm sưng, tiêu đờm, đồng thời giúp tiêu diệt các vi khuẩn có hại và cơn đau.</p><p>Tuy nhiên, nếu dùng nước muối không đúng cách thì nguy cơ người bệnh phải đối mặt với nhiều tác dụng phụ là rất lớn. Đặc biệt là ở trẻ em, các bậc phụ huynh tuyệt đối không nên dùng nước muối cho con em mình ở nồng độ cao.</p><p>Không chỉ có thói quen súc miệng bằng nước muối có nồng độ cao, nhiều người còn ngậm trực tiếp muối hạt vào miệng vì nghĩ cách làm này sẽ hiệu quả hơn nhưng thực tế không phải vậy, nước muối ở liều cao là tác nhân chính khiến niêm mạc họng bị tổn thương về lâu dài còn khiến cơ thể bị dư thừa muối dẫn đến thận bị tổn thương, gây yếu xương, tăng nguy cơ mắc cao huyết áp và đột quỵ. Đây là một sai lầm nghiêm trọng trong điều trị viêm họng.&nbsp;</p><p><font style=\"line-height: 22px; font-size: 22px;\" color=\"#ff0000\"><u><b><i> Làm thế nào để ngăn ngừa viêm họng đúng cách?  </i></b></u></font></p><p>- Đi khám bác sĩ chuyên khoa Tai - Mũi - Họng hoặc phòng khám uy tín để xác định: loại bệnh viêm họng, mức độ bệnh, nguyên nhân gây bệnh.</p><p>- Điều chỉnh chế độ ăn uống, sinh hoạt, tăng cường sức đề kháng (dinh dưỡng, vitamin và khoáng chất) phù hợp.</p><p>- Điều trị đồng thời các bệnh lý liên quan như: viêm xoang mũi, trào ngược dạ dày/thực quản. Tuyệt đối không để dịch từ mũi chảy xuống cổ họng, ngủ kê cao đầu để tránh axit trào ngược lên họng.</p><p>- Đeo khẩu trang, giữ ấm cơ thể, vệ sinh mũi họng sạch sẽ bằng nước muối sinh lý.</p><p>- Hạn chế sử dụng kháng sinh tràn lan, kéo dài liên tục bởi có nguy cơ ảnh hưởng tới: gan, thận, dạ, dày,... và có nguy cơ kháng kháng sinh gây nguy hiểm về sau.</p><p>Bệnh viêm họng tuy là bệnh phổ biến và hết sức thông thường, nhưng nếu bạn không biết cách điều trị bệnh đúng cách và kịp thời thì rất dễ mắc những sai lầm khi điều trị viêm họng.</p><p style=\"text-align: right;\"><b style=\"\">Nguồn: Thông tin sức khỏe</b></p><p style=\"text-align: right;\"><i>Nguồn: Thông tin sức khoẻ</i></p><p style=\"text-align: right;\"><br></p><div class=\"VCSortableInPreviewMode link-content-footer IMSCurrentEditorEditObject\" type=\"link\"><b><a type=\"link_a\" href=\"/tre-em-viem-hong-cham-soc-the-nao-20180625102841593.htm\" title=\"Trẻ em bị viêm họng cần chăm sóc thế nào?\">Trẻ em bị viêm họng cần chăm sóc thế nào?</a></b></div>";
            try
            {
                var htmlDoc = new HtmlAgilityPack.HtmlDocument
                {
                    OptionFixNestedTags = true,
                    OptionCheckSyntax = false
                };
                htmlDoc.LoadHtml(data);

                var elms = htmlDoc.DocumentNode.SelectNodes("//p[@style='text-align: right;']");
                if (elms != null)
                {
                    var flag = false;
                    for (int i = 0; i < elms.Count; i++)
                    {
                        var nguon = "Nguồn:".Trim().ToLower();
                        try {
                            if (elms[i].InnerText.Trim().ToLower().Contains(nguon))
                            {
                                elms[i].Remove();

                                flag = true;
                            }
                        }
                        catch { }
                    }
                    if (flag)
                    {
                        return htmlDoc.DocumentNode.OuterHtml;
                    }
                }
                //data = htmlDoc.DocumentNode.OuterHtml;
            }
            catch { }
            return null;
        }

        public static int GetCountNewsRedis(string connectStr)
        {
            try
            {
                string query = @"SELECT COUNT(*) as [Count] FROM News";

                var con = GetConnection(connectStr);
                var cmd = new SqlCommand(query, con);

                con.Open();
                var reader = cmd.ExecuteReader();

                var data = EntityMapper.FillCollection<CountNewsEntity>(reader);

                con.Close();

                var count = 0;
                if (data != null && data.Count > 0)
                {
                    count = data.FirstOrDefault().Count;
                }

                return count;
            }
            catch (Exception ex)
            {
                Logger.Log(TypeLog.Error, "GetCountNewsRedis => " + ex.Message);
                return 0;
                //MessageBox.Show(ex.Message);
            }
        }

        public static int GetCountNewsRedisUpdateBody(string connectStr)
        {
            try
            {
                string query = @"SELECT COUNT(*) as [Count] FROM News";

                var con = GetConnection(connectStr);
                var cmd = new SqlCommand(query, con);

                con.Open();
                var reader = cmd.ExecuteReader();

                var data = EntityMapper.FillCollection<CountNewsEntity>(reader);

                con.Close();

                var count = 0;
                if (data != null && data.Count > 0)
                {
                    count = data.FirstOrDefault().Count;
                }

                return count;
            }
            catch (Exception ex)
            {
                return 0;
                //MessageBox.Show(ex.Message);
            }
        }

        private static int UpdateBody(string nameSpace, string connectStr2, List<NewsEntity> data)
        {
            long id = 0;
            var count = 0;
            try
            {
                var con = GetConnection(connectStr2);
                con.Open();

                foreach (var item in data)
                {
                    string query = @"Update News SET Body=@Body WHERE Id=@Id 

                                    Update NewsContent SET Body=@Body WHERE NewsId=@Id";


                    var cmd = new SqlCommand(query, con);
                    //cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandType = CommandType.Text;
                    cmd.Parameters.AddWithValue("@Id", item.Id);                    
                    cmd.Parameters.AddWithValue("@Body", item.Body ?? (object)DBNull.Value);                    

                    id = item.Id;
                    var reader = cmd.ExecuteNonQuery();
                    count++;
                    
                }

                con.Close();

                return count;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message + " ID: " + id.ToString());
                return count;
            }
        }

        public static int UpdateBodyTest(string nameSpace, string connectStr, string newsid)
        {
            long id = 0;
            var count = 0;
            try
            {
                var con = GetConnection(connectStr);
                con.Open();

                string query = @"Select * from news where id=" + newsid;

                var cmd = new SqlCommand(query, con);

                var reader = cmd.ExecuteReader();

                var data = EntityMapper.FillCollection<NewsEntity>(reader);

                foreach (var item in data)
                {
                    item.Body = replaceBody(item.Body);

                    string queryU = @"Update News SET Body=@Body WHERE Id=@Id";
                    var cmdU = new SqlCommand(queryU, con);
                    //cmd.CommandType = CommandType.StoredProcedure;  
                    cmdU.CommandType = CommandType.Text;
                    cmdU.Parameters.AddWithValue("@Id", item.Id);
                    cmdU.Parameters.AddWithValue("@Body", item.Body ?? (object)DBNull.Value);

                    var readerU = cmdU.ExecuteNonQuery();
                }


                con.Close();

                return count;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message + " ID: " + id.ToString());
                return count;
            }
        }

        #region AFamily

        #region news
        public static int SyncNewsToDb(string nameSpace, string connectStr1, string connectStr2, int pageIndex, int pageSize, IProgress<InitDataDb.ProcessEntity> progress, InitDataDb.ProcessEntity processEntity)
        {
            try
            {
                string query = @"DECLARE @UpperBand int, @LowerBand int
                                SET @LowerBand  = (@pageIndex - 1) * @PageSize
                                SET @UpperBand  = (@pageIndex * @PageSize)
                                SELECT * FROM (
                                SELECT n.*,ROW_NUMBER() OVER(ORDER BY n.Id DESC) AS RowNumber 
                                FROM News n where (@DateFrom is null and (1=1)) or (@DateFrom is not null and (CONVERT(datetime,n.CreatedDate) >= CONVERT(datetime,@DateFrom) AND CONVERT(datetime,n.CreatedDate)<=CONVERT(datetime,@DateTo)))
                                ) AS temp
                                WHERE RowNumber > @LowerBand AND RowNumber <= @UpperBand";

                //test Id=20161214035128196
                //query = @"SELECT * FROM News WHERE Id=20161214035128196";

                var con = GetConnection(connectStr1);
                var cmd = new SqlCommand(query, con);
                //cmd.CommandType = CommandType.StoredProcedure;               
                var dateFrom = DBNull.Value;
                var dateTo = DBNull.Value;                
                cmd.Parameters.AddWithValue("@PageIndex", pageIndex);
                cmd.Parameters.AddWithValue("@PageSize", pageSize);
                cmd.Parameters.AddWithValue("@DateFrom", dateFrom);
                cmd.Parameters.AddWithValue("@DateTo", dateTo);

                con.Open();
                var reader = cmd.ExecuteReader();

                var data = EntityMapper.FillCollection<NewsEntity>(reader);

                con.Close();                
                
                var abc = InitNewsToDb(nameSpace, connectStr2, data, progress, processEntity);

                return abc;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return 0;                
            }
        }
        private static int InitNewsToDb(string nameSpace, string connectStr2, List<NewsEntity> data, IProgress<InitDataDb.ProcessEntity> progress, InitDataDb.ProcessEntity processEntity)
        {
            long id = 0;
            var count = 0;
            try
            {               
                var con = GetConnection(connectStr2);
                con.Open();
                                
                foreach (var item in data)
                {
                    string query = @"DELETE [News] WHERE Id=@Id
                                    INSERT INTO [News]([Id]
                                                      ,[Title]
                                                      ,[SubTitle]
                                                      ,[Sapo]
                                                      ,[Body]
                                                      ,[Avatar]
                                                      ,[AvatarDesc]
                                                      ,[Avatar2]
                                                      ,[Avatar3]
                                                      ,[Avatar4]
                                                      ,[Avatar5]
                                                      ,[Author]
                                                      ,[NewsRelation]
                                                      ,[Status]
                                                      ,[Source]
                                                      ,[IsFocus]
                                                      ,[Type]
                                                      ,[ThreadId]
                                                      ,[CreatedDate]
                                                      ,[LastModifiedDate]
                                                      ,[DistributionDate]
                                                      ,[CreatedBy]
                                                      ,[LastModifiedBy]
                                                      ,[PublishedBy]
                                                      ,[EditedBy]
                                                      ,[LastReceiver]
                                                      ,[ApprovedBy]
                                                      ,[WordCount]
                                                      ,[ViewCount]
                                                      ,[Priority]
                                                      ,[Tag]
                                                      ,[Note]
                                                      ,[TagPrimary]
                                                      ,[Price]
                                                      ,[DisplayStyle]
                                                      ,[DisplayPosition]
                                                      ,[DisplayInSlide]
                                                      ,[AvatarCustom]
                                                      ,[OriginalId]
                                                      ,[NewsType]
                                                      ,[IsOnHome]
                                                      ,[Url]
                                                      ,[NoteRoyalties]
                                                      ,[TagItem]
                                                      ,[NewsCategory]
                                                      ,[InitSapo]
                                                      ,[TemplateName]
                                                      ,[TemplateConfig]
                                                      ,[InterviewId]
                                                      ,[IsBreakingNews]
                                                      ,[OriginalUrl]
                                                      ,[IsPr]
                                                      ,[AdStore]
                                                      ,[AdStoreUrl]
                                                      ,[PrBookingNumber]
                                                      ,[PegaBreakingNews]
                                                      ,[RollingNewsId]
                                                      ,[IsOnMobile]
                                                      ,[TagSubTitleId]
                                                      ,[PrPosition]
                                                      ,[LocationType]
                                                      ,[ExpiredDate]
                                                      ,[SourceUrl]
                                                      ,[BonusPrice]
                                                      ,[ViewCountMobile]
                                                      ,[PhotoCount]
                                                      ,[VideoCount]
                                                      ,[ShortTitle]
                                                      ,[ParentNewsId]
                                                      ,[WarningLevel]
                                                ) VALUES(@Id
                                                      ,@Title
                                                      ,@SubTitle
                                                      ,@Sapo
                                                      ,@Body
                                                      ,@Avatar
                                                      ,@AvatarDesc
                                                      ,@Avatar2
                                                      ,@Avatar3
                                                      ,@Avatar4
                                                      ,@Avatar5
                                                      ,@Author
                                                      ,@NewsRelation
                                                      ,@Status
                                                      ,@Source
                                                      ,@IsFocus
                                                      ,@Type
                                                      ,@ThreadId
                                                      ,@CreatedDate
                                                      ,@LastModifiedDate
                                                      ,@DistributionDate
                                                      ,@CreatedBy
                                                      ,@LastModifiedBy
                                                      ,@PublishedBy
                                                      ,@EditedBy
                                                      ,@LastReceiver
                                                      ,@ApprovedBy
                                                      ,@WordCount
                                                      ,@ViewCount
                                                      ,@Priority
                                                      ,@Tag
                                                      ,@Note
                                                      ,@TagPrimary
                                                      ,@Price
                                                      ,@DisplayStyle
                                                      ,@DisplayPosition
                                                      ,@DisplayInSlide
                                                      ,@AvatarCustom
                                                      ,@OriginalId
                                                      ,@NewsType
                                                      ,@IsOnHome
                                                      ,@Url
                                                      ,@NoteRoyalties
                                                      ,@TagItem
                                                      ,@NewsCategory
                                                      ,@InitSapo
                                                      ,@TemplateName
                                                      ,@TemplateConfig
                                                      ,@InterviewId
                                                      ,@IsBreakingNews
                                                      ,@OriginalUrl
                                                      ,@IsPr
                                                      ,@AdStore
                                                      ,@AdStoreUrl
                                                      ,@PrBookingNumber
                                                      ,@PegaBreakingNews
                                                      ,@RollingNewsId
                                                      ,@IsOnMobile
                                                      ,@TagSubTitleId
                                                      ,@PrPosition
                                                      ,@LocationType
                                                      ,@ExpiredDate
                                                      ,@SourceUrl
                                                      ,@BonusPrice
                                                      ,@ViewCountMobile
                                                      ,@PhotoCount
                                                      ,@VideoCount
                                                      ,@ShortTitle
                                                      ,@ParentNewsId
                                                      ,@WarningLevel)";

                    var cmd = new SqlCommand(query, con);
                    //cmd.CommandType = CommandType.StoredProcedure;  
                    cmd.CommandType = CommandType.Text;                    
                    cmd.Parameters.AddWithValue("@Id",item.Id);
                    cmd.Parameters.AddWithValue("@Title", item.Title ?? (object)DBNull.Value);
                    cmd.Parameters.AddWithValue("@SubTitle", item.SubTitle ?? (object)DBNull.Value);
                    cmd.Parameters.AddWithValue("@Sapo", item.Sapo ?? (object)DBNull.Value);
                    cmd.Parameters.AddWithValue("@Body", item.Body ?? (object)DBNull.Value);
                    cmd.Parameters.AddWithValue("@Avatar", item.Avatar ?? (object)DBNull.Value);
                    cmd.Parameters.AddWithValue("@AvatarDesc", item.AvatarDesc ?? (object)DBNull.Value);
                    cmd.Parameters.AddWithValue("@Avatar2", item.Avatar2 ?? (object)DBNull.Value);
                    cmd.Parameters.AddWithValue("@Avatar3", item.Avatar3 ?? (object)DBNull.Value);
                    cmd.Parameters.AddWithValue("@Avatar4", item.Avatar4 ?? (object)DBNull.Value);
                    cmd.Parameters.AddWithValue("@Avatar5", item.Avatar5 ?? (object)DBNull.Value);
                    cmd.Parameters.AddWithValue("@Author", item.Author ?? (object)DBNull.Value);
                    cmd.Parameters.AddWithValue("@NewsRelation", item.NewsRelation ?? (object)DBNull.Value);
                    cmd.Parameters.AddWithValue("@Status", item.Status);
                    cmd.Parameters.AddWithValue("@Source", item.Source ?? (object)DBNull.Value);
                    cmd.Parameters.AddWithValue("@IsFocus", item.IsFocus);

                    if (item.Type <= 0)
                        cmd.Parameters.AddWithValue("@Type", 0);
                    else
                        cmd.Parameters.AddWithValue("@Type", item.Type);

                    cmd.Parameters.AddWithValue("@ThreadId", item.ThreadId);

                    if (item.CreatedDate <= new DateTime(1980, 1, 1))
                        cmd.Parameters.AddWithValue("@CreatedDate", DBNull.Value);
                    else
                        cmd.Parameters.AddWithValue("@CreatedDate", item.CreatedDate);

                    if (item.LastModifiedDate <= new DateTime(1980, 1, 1))
                        cmd.Parameters.AddWithValue("@LastModifiedDate", DBNull.Value);
                    else
                        cmd.Parameters.AddWithValue("@LastModifiedDate", item.LastModifiedDate);

                    if (item.DistributionDate <= new DateTime(1980, 1, 1))
                        cmd.Parameters.AddWithValue("@DistributionDate", DBNull.Value);
                    else
                        cmd.Parameters.AddWithValue("@DistributionDate", item.DistributionDate);
                    
                    cmd.Parameters.AddWithValue("@CreatedBy", item.CreatedBy ?? (object)DBNull.Value);
                    cmd.Parameters.AddWithValue("@LastModifiedBy", item.LastModifiedBy ?? (object)DBNull.Value);
                    cmd.Parameters.AddWithValue("@PublishedBy", item.PublishedBy ?? (object)DBNull.Value);
                    cmd.Parameters.AddWithValue("@EditedBy", item.EditedBy ?? (object)DBNull.Value);
                    cmd.Parameters.AddWithValue("@LastReceiver", item.LastReceiver ?? (object)DBNull.Value);

                    if (item.Status == 6 && string.IsNullOrEmpty(item.ApprovedBy))
                        cmd.Parameters.AddWithValue("@ApprovedBy", item.PublishedBy ?? (object)DBNull.Value);
                    else
                        cmd.Parameters.AddWithValue("@ApprovedBy", item.ApprovedBy ?? (object)DBNull.Value);

                    if (item.WordCount<=0)
                        cmd.Parameters.AddWithValue("@WordCount", 0);
                    else
                        cmd.Parameters.AddWithValue("@WordCount", item.WordCount);

                    cmd.Parameters.AddWithValue("@ViewCount", item.ViewCount);

                    if (item.Priority <= 0)
                        cmd.Parameters.AddWithValue("@Priority", 0);
                    else
                        cmd.Parameters.AddWithValue("@Priority", item.Priority);

                    cmd.Parameters.AddWithValue("@Tag", item.Tag ?? (object)DBNull.Value);
                    cmd.Parameters.AddWithValue("@Note", item.Note ?? (object)DBNull.Value);
                    cmd.Parameters.AddWithValue("@TagPrimary", item.TagPrimary);

                    if (item.Price < 0)
                        cmd.Parameters.AddWithValue("@Price", DBNull.Value);
                    else
                        cmd.Parameters.AddWithValue("@Price", item.Price);

                    cmd.Parameters.AddWithValue("@DisplayStyle", item.DisplayStyle);
                    cmd.Parameters.AddWithValue("@DisplayPosition", item.DisplayPosition);
                    cmd.Parameters.AddWithValue("@DisplayInSlide", item.DisplayInSlide);
                    cmd.Parameters.AddWithValue("@AvatarCustom", item.AvatarCustom ?? (object)DBNull.Value);
                    cmd.Parameters.AddWithValue("@OriginalId", item.OriginalId);

                    if (item.NewsType <= 0)
                        cmd.Parameters.AddWithValue("@NewsType", 0);
                    else
                        cmd.Parameters.AddWithValue("@NewsType", item.NewsType);

                    cmd.Parameters.AddWithValue("@IsOnHome", item.IsOnHome);
                    cmd.Parameters.AddWithValue("@Url", item.Url ?? (object)DBNull.Value);
                    cmd.Parameters.AddWithValue("@NoteRoyalties", item.NoteRoyalties ?? (object)DBNull.Value);
                    cmd.Parameters.AddWithValue("@TagItem", item.TagItem ?? (object)DBNull.Value);
                    cmd.Parameters.AddWithValue("@NewsCategory", item.NewsCategory);
                    cmd.Parameters.AddWithValue("@InitSapo", item.InitSapo ?? (object)DBNull.Value);
                    cmd.Parameters.AddWithValue("@TemplateName", item.TemplateName ?? (object)DBNull.Value);
                    cmd.Parameters.AddWithValue("@TemplateConfig", item.TemplateConfig ?? (object)DBNull.Value);
                    cmd.Parameters.AddWithValue("@InterviewId", item.InterviewId);
                    cmd.Parameters.AddWithValue("@IsBreakingNews", item.IsBreakingNews);
                    cmd.Parameters.AddWithValue("@OriginalUrl", item.OriginalUrl ?? (object)DBNull.Value);
                    cmd.Parameters.AddWithValue("@IsPr", item.IsPr);
                    cmd.Parameters.AddWithValue("@AdStore", item.AdStore);
                    cmd.Parameters.AddWithValue("@AdStoreUrl", item.AdStoreUrl ?? (object)DBNull.Value);
                    cmd.Parameters.AddWithValue("@PrBookingNumber", item.PrBookingNumber ?? (object)DBNull.Value);
                    cmd.Parameters.AddWithValue("@PegaBreakingNews", item.PegaBreakingNews);
                    cmd.Parameters.AddWithValue("@RollingNewsId", item.RollingNewsId);
                    cmd.Parameters.AddWithValue("@IsOnMobile", item.IsOnMobile);
                    cmd.Parameters.AddWithValue("@TagSubTitleId", item.TagSubTitleId);
                    cmd.Parameters.AddWithValue("@PrPosition", item.PrPosition);
                    cmd.Parameters.AddWithValue("@LocationType", item.LocationType);

                    if(item.ExpiredDate <= new DateTime(1980, 1, 1))
                        cmd.Parameters.AddWithValue("@ExpiredDate", DBNull.Value);
                    else
                        cmd.Parameters.AddWithValue("@ExpiredDate", item.ExpiredDate);

                    cmd.Parameters.AddWithValue("@SourceUrl", item.SourceUrl ?? (object)DBNull.Value);
                    cmd.Parameters.AddWithValue("@BonusPrice", item.BonusPrice);
                    cmd.Parameters.AddWithValue("@ViewCountMobile", item.ViewCountMobile);
                    cmd.Parameters.AddWithValue("@PhotoCount", item.PhotoCount);
                    cmd.Parameters.AddWithValue("@VideoCount", item.VideoCount);
                    cmd.Parameters.AddWithValue("@ShortTitle", item.ShortTitle ?? (object)DBNull.Value);
                    cmd.Parameters.AddWithValue("@ParentNewsId", item.ParentNewsId);
                    cmd.Parameters.AddWithValue("@WarningLevel", item.WarningLevel);

                    id = item.Id;
                    var reader = cmd.ExecuteNonQuery();
                    count++;

                    if (progress != null)
                    {
                        processEntity.value = count;
                        progress.Report(processEntity);
                    }
                }  
                     
                con.Close();

                return count;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message+" ID: "+id.ToString());
                return count;               
            }
        }
        public static int GetCountNews(string connectStr)
        {
            try
            {
                string query = @"SELECT COUNT(*) as [Count] FROM News";

                var con = GetConnection(connectStr);
                var cmd = new SqlCommand(query, con);

                con.Open();
                var reader = cmd.ExecuteReader();

                var data = EntityMapper.FillCollection<CountNewsEntity>(reader);

                con.Close();

                var count = 0;
                if (data != null && data.Count > 0)
                {
                    count = data.FirstOrDefault().Count;
                }

                return count;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return 1;                
            }
        }       
        #endregion

        #region NewsPublish
        public static int SyncNewsPublishToDb(string nameSpace, string connectStr1, string connectStr2, int pageIndex, int pageSize, IProgress<InitDataDb.ProcessEntity> progress, InitDataDb.ProcessEntity processEntity)
        {
            try
            {
                string query = @"DECLARE @UpperBand int, @LowerBand int
                                SET @LowerBand  = (@pageIndex - 1) * @PageSize
                                SET @UpperBand  = (@pageIndex * @PageSize)
                                SELECT * FROM (
                                SELECT n.*,ROW_NUMBER() OVER(ORDER BY n.ID DESC) AS RowNumber 
                                FROM NewsPublish n where (@DateFrom is null and (1=1)) or (@DateFrom is not null and (CONVERT(datetime,n.DistributionDate) >= CONVERT(datetime,@DateFrom) AND CONVERT(datetime,n.DistributionDate)<=CONVERT(datetime,@DateTo)))
                                ) AS temp
                                WHERE RowNumber > @LowerBand AND RowNumber <= @UpperBand";
                
                var con = GetConnection(connectStr1);
                var cmd = new SqlCommand(query, con);
                //cmd.CommandType = CommandType.StoredProcedure;               
                var dateFrom = DBNull.Value;
                var dateTo = DBNull.Value;
                cmd.Parameters.AddWithValue("@PageIndex", pageIndex);
                cmd.Parameters.AddWithValue("@PageSize", pageSize);
                cmd.Parameters.AddWithValue("@DateFrom", dateFrom);
                cmd.Parameters.AddWithValue("@DateTo", dateTo);

                con.Open();
                var reader = cmd.ExecuteReader();

                var data = EntityMapper.FillCollection<NewsPublishEntity>(reader);

                con.Close();

                var abc = InitNewsPublishToDb(nameSpace, connectStr2, data, progress, processEntity);

                return abc;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return 0;
            }
        }
        private static int InitNewsPublishToDb(string nameSpace, string connectStr2, List<NewsPublishEntity> data, IProgress<InitDataDb.ProcessEntity> progress, InitDataDb.ProcessEntity processEntity)
        {
            long id = 0;
            var count = 0;
            try
            {
                var con = GetConnection(connectStr2);
                con.Open();

                foreach (var item in data)
                {
                    string query = @"SET IDENTITY_INSERT [NewsPublish] ON;
                                    DELETE [NewsPublish] WHERE ID=@ID;
                                    INSERT INTO [NewsPublish](ID
                                                              ,[NewsId]
                                                              ,[ZoneId]
                                                              ,[Title]
                                                              ,[SubTitle]
                                                              ,[Sapo]
                                                              ,[Avatar]
                                                              ,[AvatarDesc]
                                                              ,[Avatar1]
                                                              ,[Avatar2]
                                                              ,[Avatar3]
                                                              ,[Avatar4]
                                                              ,[Avatar5]
                                                              ,[Author]
                                                              ,[NewsRelation]
                                                              ,[Source]
                                                              ,[IsFocus]
                                                              ,[Type]
                                                              ,[ThreadId]
                                                              ,[DistributionDate]
                                                              ,[Url]
                                                              ,[DisplayStyle]
                                                              ,[DisplayPosition]
                                                              ,[DisplayInSlide]
                                                              ,[AvatarCustom]
                                                              ,[OriginalId]
                                                              ,[PublishedDate]
                                                              ,[IsOnHome]
                                                              ,[TagItem]
                                                              ,[IsPrimary]
                                                              ,[InitSapo]
                                                              ,[NewsType]
                                                              ,[InterviewId]
                                                              ,[IsBreakingNews]
                                                              ,[OriginalUrl]
                                                              ,[IsPr]
                                                              ,[AdStore]
                                                              ,[AdStoreUrl]
                                                              ,[LastModifiedDate]
                                                              ,[RollingNewsId]
                                                              ,[IsOnMobile]
                                                              ,[TagSubTitleId]
                                                              ,[Position]
                                                              ,[PrPosition]
                                                              ,[Priority]
                                                              ,[LastModifiedDateStamp]
                                                              ,[PublishedDateStamp]
                                                              ,[LocationType]
                                                              ,[ExpiredDate]
                                                              ,[DistributionID]
                                                              ,[PrimaryZoneId]
                                                              ,[TitleDetail]
                                                              ,[ParentNewsId]
                                                ) VALUES(@ID
                                                        ,@NewsId
                                                        ,@ZoneId
                                                        ,@Title
                                                        ,@SubTitle
                                                        ,@Sapo
                                                        ,@Avatar
                                                        ,@AvatarDesc
                                                        ,@Avatar1
                                                        ,@Avatar2
                                                        ,@Avatar3
                                                        ,@Avatar4
                                                        ,@Avatar5
                                                        ,@Author
                                                        ,@NewsRelation
                                                        ,@Source
                                                        ,@IsFocus
                                                        ,@Type
                                                        ,@ThreadId
                                                        ,@DistributionDate
                                                        ,@Url
                                                        ,@DisplayStyle
                                                        ,@DisplayPosition
                                                        ,@DisplayInSlide
                                                        ,@AvatarCustom
                                                        ,@OriginalId
                                                        ,@PublishedDate
                                                        ,@IsOnHome
                                                        ,@TagItem
                                                        ,@IsPrimary
                                                        ,@InitSapo
                                                        ,@NewsType
                                                        ,@InterviewId
                                                        ,@IsBreakingNews
                                                        ,@OriginalUrl
                                                        ,@IsPr
                                                        ,@AdStore
                                                        ,@AdStoreUrl
                                                        ,@LastModifiedDate
                                                        ,@RollingNewsId
                                                        ,@IsOnMobile
                                                        ,@TagSubTitleId
                                                        ,@Position
                                                        ,@PrPosition
                                                        ,@Priority
                                                        ,@LastModifiedDateStamp
                                                        ,@PublishedDateStamp
                                                        ,@LocationType
                                                        ,@ExpiredDate
                                                        ,@DistributionID
                                                        ,@PrimaryZoneId
                                                        ,@TitleDetail
                                                        ,@ParentNewsId);
                                                SET IDENTITY_INSERT [NewsPublish] OFF;";

                    using (var cmd = new SqlCommand(query, con))
                    {
                        cmd.CommandType = CommandType.Text;

                        cmd.Parameters.AddWithValue("@ID", item.ID);
                        cmd.Parameters.AddWithValue("@NewsId", item.NewsId);
                        cmd.Parameters.AddWithValue("@ZoneId", item.ZoneId);
                        cmd.Parameters.AddWithValue("@Title", item.Title ?? (object)DBNull.Value);
                        cmd.Parameters.AddWithValue("@SubTitle", item.SubTitle ?? (object)DBNull.Value);
                        cmd.Parameters.AddWithValue("@Sapo", item.Sapo ?? (object)DBNull.Value);
                        cmd.Parameters.AddWithValue("@Avatar", item.Avatar ?? (object)DBNull.Value);
                        cmd.Parameters.AddWithValue("@AvatarDesc", item.AvatarDesc ?? (object)DBNull.Value);
                        cmd.Parameters.AddWithValue("@Avatar1", item.Avatar1 ?? (object)DBNull.Value);
                        cmd.Parameters.AddWithValue("@Avatar2", item.Avatar2 ?? (object)DBNull.Value);
                        cmd.Parameters.AddWithValue("@Avatar3", item.Avatar3 ?? (object)DBNull.Value);
                        cmd.Parameters.AddWithValue("@Avatar4", item.Avatar4 ?? (object)DBNull.Value);
                        cmd.Parameters.AddWithValue("@Avatar5", item.Avatar5 ?? (object)DBNull.Value);
                        cmd.Parameters.AddWithValue("@Author", item.Author ?? (object)DBNull.Value);
                        cmd.Parameters.AddWithValue("@NewsRelation", item.NewsRelation ?? (object)DBNull.Value);
                        cmd.Parameters.AddWithValue("@Source", item.Source ?? (object)DBNull.Value);
                        cmd.Parameters.AddWithValue("@IsFocus", item.IsFocus);

                        if (item.Type <= 0)
                            cmd.Parameters.AddWithValue("@Type", 0);
                        else
                            cmd.Parameters.AddWithValue("@Type", item.Type);

                        cmd.Parameters.AddWithValue("@ThreadId", item.ThreadId);

                        if (item.DistributionDate <= new DateTime(1980, 1, 1))
                            cmd.Parameters.AddWithValue("@DistributionDate", DBNull.Value);
                        else
                            cmd.Parameters.AddWithValue("@DistributionDate", item.DistributionDate);

                        cmd.Parameters.AddWithValue("@Url", item.Url ?? (object)DBNull.Value);
                        cmd.Parameters.AddWithValue("@DisplayStyle", item.DisplayStyle);
                        cmd.Parameters.AddWithValue("@DisplayPosition", item.DisplayPosition);
                        cmd.Parameters.AddWithValue("@DisplayInSlide", item.DisplayInSlide);
                        cmd.Parameters.AddWithValue("@AvatarCustom", item.AvatarCustom ?? (object)DBNull.Value);
                        cmd.Parameters.AddWithValue("@OriginalId", item.OriginalId);
                        cmd.Parameters.AddWithValue("@PublishedDate", item.PublishedDate);
                        cmd.Parameters.AddWithValue("@IsOnHome", item.IsOnHome);
                        cmd.Parameters.AddWithValue("@TagItem", item.TagItem ?? (object)DBNull.Value);
                        cmd.Parameters.AddWithValue("@IsPrimary", item.IsPrimary);
                        cmd.Parameters.AddWithValue("@InitSapo", item.InitSapo ?? (object)DBNull.Value);

                        if (item.NewsType <= 0)
                            cmd.Parameters.AddWithValue("@NewsType", 0);
                        else
                            cmd.Parameters.AddWithValue("@NewsType", item.NewsType);

                        cmd.Parameters.AddWithValue("@InterviewId", item.InterviewId);
                        cmd.Parameters.AddWithValue("@IsBreakingNews", item.IsBreakingNews);
                        cmd.Parameters.AddWithValue("@OriginalUrl", item.OriginalUrl ?? (object)DBNull.Value);
                        cmd.Parameters.AddWithValue("@IsPr", item.IsPr);
                        cmd.Parameters.AddWithValue("@AdStore", item.AdStore);
                        cmd.Parameters.AddWithValue("@AdStoreUrl", item.AdStoreUrl ?? (object)DBNull.Value);

                        if (item.LastModifiedDate <= new DateTime(1980, 1, 1))
                            cmd.Parameters.AddWithValue("@LastModifiedDate", DBNull.Value);
                        else
                            cmd.Parameters.AddWithValue("@LastModifiedDate", item.LastModifiedDate);

                        cmd.Parameters.AddWithValue("@RollingNewsId", item.RollingNewsId);
                        cmd.Parameters.AddWithValue("@IsOnMobile", item.IsOnMobile);
                        cmd.Parameters.AddWithValue("@TagSubTitleId", item.TagSubTitleId);
                        cmd.Parameters.AddWithValue("@Position", item.Position);
                        cmd.Parameters.AddWithValue("@PrPosition", item.PrPosition);
                        cmd.Parameters.AddWithValue("@Priority", item.Priority);
                        cmd.Parameters.AddWithValue("@LastModifiedDateStamp", item.LastModifiedDateStamp);
                        cmd.Parameters.AddWithValue("@PublishedDateStamp", item.PublishedDateStamp);
                        cmd.Parameters.AddWithValue("@LocationType", item.LocationType);

                        if (item.ExpiredDate <= new DateTime(1980, 1, 1))
                            cmd.Parameters.AddWithValue("@ExpiredDate", DBNull.Value);
                        else
                            cmd.Parameters.AddWithValue("@ExpiredDate", item.ExpiredDate);

                        cmd.Parameters.AddWithValue("@DistributionID", item.DistributionID);
                        cmd.Parameters.AddWithValue("@PrimaryZoneId", item.PrimaryZoneId);
                        cmd.Parameters.AddWithValue("@TitleDetail", item.TitleDetail ?? (object)DBNull.Value);
                        cmd.Parameters.AddWithValue("@ParentNewsId", item.ParentNewsId);

                        id = item.ID;
                        var reader = cmd.ExecuteNonQuery();
                        count++;

                        if (progress != null)
                        {
                            processEntity.value = count;
                            progress.Report(processEntity);
                        }
                    }
                }

                con.Close();

                return count;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message + " ID: " + id.ToString());
                return count;
            }
        }
        public static int GetCountNewsPublish(string connectStr)
        {
            try
            {
                string query = @"SELECT COUNT(*) as [Count] FROM NewsPublish";

                var con = GetConnection(connectStr);
                var cmd = new SqlCommand(query, con);

                con.Open();
                var reader = cmd.ExecuteReader();

                var data = EntityMapper.FillCollection<CountNewsEntity>(reader);

                con.Close();

                var count = 0;
                if (data != null && data.Count > 0)
                {
                    count = data.FirstOrDefault().Count;
                }

                return count;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return 1;
            }
        }
        #endregion

        #region NewsContent
        public static int SyncNewsContentToDb(string nameSpace, string connectStr1, string connectStr2, int pageIndex, int pageSize, IProgress<InitDataDb.ProcessEntity> progress, InitDataDb.ProcessEntity processEntity)
        {
            try
            {
                string query = @"DECLARE @UpperBand int, @LowerBand int
                                SET @LowerBand  = (@pageIndex - 1) * @PageSize
                                SET @UpperBand  = (@pageIndex * @PageSize)
                                SELECT * FROM (
                                SELECT n.*,ROW_NUMBER() OVER(ORDER BY n.NewsId DESC) AS RowNumber 
                                FROM NewsContent n where (@DateFrom is null and (1=1)) or (@DateFrom is not null and (CONVERT(datetime,n.PublishedDate) >= CONVERT(datetime,@DateFrom) AND CONVERT(datetime,n.PublishedDate)<=CONVERT(datetime,@DateTo)))
                                ) AS temp
                                WHERE RowNumber > @LowerBand AND RowNumber <= @UpperBand";

                var con = GetConnection(connectStr1);
                var cmd = new SqlCommand(query, con);
                //cmd.CommandType = CommandType.StoredProcedure;               
                var dateFrom = DBNull.Value;
                var dateTo = DBNull.Value;
                cmd.Parameters.AddWithValue("@PageIndex", pageIndex);
                cmd.Parameters.AddWithValue("@PageSize", pageSize);
                cmd.Parameters.AddWithValue("@DateFrom", dateFrom);
                cmd.Parameters.AddWithValue("@DateTo", dateTo);

                con.Open();
                var reader = cmd.ExecuteReader();

                var data = EntityMapper.FillCollection<NewsContentEntity>(reader);

                con.Close();

                var abc = InitNewsContentToDb(nameSpace, connectStr2, data, progress, processEntity);

                return abc;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return 0;
            }
        }
        private static int InitNewsContentToDb(string nameSpace, string connectStr2, List<NewsContentEntity> data, IProgress<InitDataDb.ProcessEntity> progress, InitDataDb.ProcessEntity processEntity)
        {
            long id = 0;
            var count = 0;
            try
            {
                var con = GetConnection(connectStr2);
                con.Open();

                foreach (var item in data)
                {
                    string query = @"DELETE [NewsContent] WHERE NewsId=@NewsId;
                                    INSERT INTO [NewsContent]([NewsId]
                                                              ,[Title]
                                                              ,[Sapo]
                                                              ,[Avatar]
                                                              ,[Avatar2]
                                                              ,[Avatar3]
                                                              ,[Avatar4]
                                                              ,[Avatar5]
                                                              ,[Body]
                                                              ,[PublishedDate]
                                                              ,[Source]
                                                              ,[NewsRelation]
                                                              ,[Tags]
                                                              ,[Author]
                                                              ,[TagPrimary]
                                                              ,[Url]
                                                              ,[ZoneId]
                                                              ,[OriginalId]
                                                              ,[TagItem]
                                                              ,[SubTitle]
                                                              ,[InitSapo]
                                                              ,[InterviewId]
                                                              ,[OriginalUrl]
                                                              ,[Type]
                                                              ,[AvatarDesc]
                                                              ,[NewsType]
                                                              ,[RollingNewsId]
                                                              ,[AdStore]
                                                              ,[AdStoreUrl]
                                                              ,[TagSubTitleId]
                                                              ,[ThreadId]
                                                              ,[Position]
                                                              ,[PrPosition]
                                                              ,[IsOnMobile]
                                                              ,[UseTemplate]
                                                              ,[LocationType]
                                                              ,[ExpiredDate]
                                                              ,[SourceUrl]
                                                              ,[ParentNewsId]
                                                ) VALUES(@NewsId
                                                          ,@Title
                                                          ,@Sapo
                                                          ,@Avatar
                                                          ,@Avatar2
                                                          ,@Avatar3
                                                          ,@Avatar4
                                                          ,@Avatar5
                                                          ,@Body
                                                          ,@PublishedDate
                                                          ,@Source
                                                          ,@NewsRelation
                                                          ,@Tags
                                                          ,@Author
                                                          ,@TagPrimary
                                                          ,@Url
                                                          ,@ZoneId
                                                          ,@OriginalId
                                                          ,@TagItem
                                                          ,@SubTitle
                                                          ,@InitSapo
                                                          ,@InterviewId
                                                          ,@OriginalUrl
                                                          ,@Type
                                                          ,@AvatarDesc
                                                          ,@NewsType
                                                          ,@RollingNewsId
                                                          ,@AdStore
                                                          ,@AdStoreUrl
                                                          ,@TagSubTitleId
                                                          ,@ThreadId
                                                          ,@Position
                                                          ,@PrPosition
                                                          ,@IsOnMobile
                                                          ,@UseTemplate
                                                          ,@LocationType
                                                          ,@ExpiredDate
                                                          ,@SourceUrl
                                                          ,@ParentNewsId);
                                                ";

                    using (var cmd = new SqlCommand(query, con))
                    {
                        cmd.CommandType = CommandType.Text;
                        
                        cmd.Parameters.AddWithValue("@NewsId", item.NewsId);                        
                        cmd.Parameters.AddWithValue("@Title", item.Title ?? (object)DBNull.Value);                        
                        cmd.Parameters.AddWithValue("@Sapo", item.Sapo ?? (object)DBNull.Value);
                        cmd.Parameters.AddWithValue("@Avatar", item.Avatar ?? (object)DBNull.Value);                                                
                        cmd.Parameters.AddWithValue("@Avatar2", item.Avatar2 ?? (object)DBNull.Value);
                        cmd.Parameters.AddWithValue("@Avatar3", item.Avatar3 ?? (object)DBNull.Value);
                        cmd.Parameters.AddWithValue("@Avatar4", item.Avatar4 ?? (object)DBNull.Value);
                        cmd.Parameters.AddWithValue("@Avatar5", item.Avatar5 ?? (object)DBNull.Value);
                        cmd.Parameters.AddWithValue("@Body", item.Body ?? (object)DBNull.Value);

                        if (item.PublishedDate <= new DateTime(1980, 1, 1))
                            cmd.Parameters.AddWithValue("@PublishedDate", DBNull.Value);
                        else
                            cmd.Parameters.AddWithValue("@PublishedDate", item.PublishedDate);

                        cmd.Parameters.AddWithValue("@Source", item.Source ?? (object)DBNull.Value);
                        cmd.Parameters.AddWithValue("@NewsRelation", item.NewsRelation ?? (object)DBNull.Value);
                        cmd.Parameters.AddWithValue("@Tags", item.Tags ?? (object)DBNull.Value);
                        cmd.Parameters.AddWithValue("@Author", item.Author ?? (object)DBNull.Value);
                        cmd.Parameters.AddWithValue("@TagPrimary", item.TagPrimary ?? (object)DBNull.Value);                                                                    
                        cmd.Parameters.AddWithValue("@Url", item.Url ?? (object)DBNull.Value);
                        cmd.Parameters.AddWithValue("@ZoneId", item.ZoneId);
                        cmd.Parameters.AddWithValue("@OriginalId", item.OriginalId);
                        cmd.Parameters.AddWithValue("@TagItem", item.TagItem ?? (object)DBNull.Value);
                        cmd.Parameters.AddWithValue("@SubTitle", item.SubTitle ?? (object)DBNull.Value);
                        cmd.Parameters.AddWithValue("@InitSapo", item.InitSapo ?? (object)DBNull.Value);
                        cmd.Parameters.AddWithValue("@InterviewId", item.InterviewId);
                        cmd.Parameters.AddWithValue("@OriginalUrl", item.OriginalUrl ?? (object)DBNull.Value);

                        if (item.Type <= 0)
                            cmd.Parameters.AddWithValue("@Type", 0);
                        else
                            cmd.Parameters.AddWithValue("@Type", item.Type);

                        cmd.Parameters.AddWithValue("@AvatarDesc", item.AvatarDesc ?? (object)DBNull.Value);

                        if (item.NewsType <= 0)
                            cmd.Parameters.AddWithValue("@NewsType", 0);
                        else
                            cmd.Parameters.AddWithValue("@NewsType", item.NewsType);

                        cmd.Parameters.AddWithValue("@RollingNewsId", item.RollingNewsId);
                        cmd.Parameters.AddWithValue("@AdStore", item.AdStore);
                        cmd.Parameters.AddWithValue("@AdStoreUrl", item.AdStoreUrl ?? (object)DBNull.Value);
                        cmd.Parameters.AddWithValue("@TagSubTitleId", item.TagSubTitleId);
                        cmd.Parameters.AddWithValue("@ThreadId", item.ThreadId);
                        cmd.Parameters.AddWithValue("@Position", item.Position);
                        cmd.Parameters.AddWithValue("@PrPosition", item.PrPosition);
                        cmd.Parameters.AddWithValue("@IsOnMobile", item.IsOnMobile);
                        cmd.Parameters.AddWithValue("@UseTemplate", item.UseTemplate);
                        cmd.Parameters.AddWithValue("@LocationType", item.LocationType);

                        if (item.ExpiredDate <= new DateTime(1980, 1, 1))
                            cmd.Parameters.AddWithValue("@ExpiredDate", DBNull.Value);
                        else
                            cmd.Parameters.AddWithValue("@ExpiredDate", item.ExpiredDate);

                        cmd.Parameters.AddWithValue("@SourceUrl", item.SourceUrl ?? (object)DBNull.Value);
                        cmd.Parameters.AddWithValue("@ParentNewsId", item.ParentNewsId);
                                             
                        id = item.NewsId;
                        var reader = cmd.ExecuteNonQuery();
                        count++;

                        if (progress != null)
                        {
                            processEntity.value = count;
                            progress.Report(processEntity);
                        }
                    }
                }

                con.Close();

                return count;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message + " ID: " + id.ToString());
                return count;
            }
        }
        public static int GetCountNewsContent(string connectStr)
        {
            try
            {
                string query = @"SELECT COUNT(*) as [Count] FROM NewsContent";

                var con = GetConnection(connectStr);
                var cmd = new SqlCommand(query, con);

                con.Open();
                var reader = cmd.ExecuteReader();

                var data = EntityMapper.FillCollection<CountNewsEntity>(reader);

                con.Close();

                var count = 0;
                if (data != null && data.Count > 0)
                {
                    count = data.FirstOrDefault().Count;
                }

                return count;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return 1;
            }
        }
        #endregion

        #region NewsRelation
        public static int SyncNewsRelationToDb(string nameSpace, string connectStr1, string connectStr2, int pageIndex, int pageSize, IProgress<InitDataDb.ProcessEntity> progress, InitDataDb.ProcessEntity processEntity)
        {
            try
            {
                string query = @"DECLARE @UpperBand int, @LowerBand int
                                SET @LowerBand  = (@pageIndex - 1) * @PageSize
                                SET @UpperBand  = (@pageIndex * @PageSize)
                                SELECT * FROM (
                                SELECT n.*,ROW_NUMBER() OVER(ORDER BY n.NewsId DESC) AS RowNumber 
                                FROM NewsRelation n
                                ) AS temp
                                WHERE RowNumber > @LowerBand AND RowNumber <= @UpperBand";
               
                var con = GetConnection(connectStr1);
                var cmd = new SqlCommand(query, con);
                             
                var dateFrom = DBNull.Value;
                var dateTo = DBNull.Value;
                cmd.Parameters.AddWithValue("@PageIndex", pageIndex);
                cmd.Parameters.AddWithValue("@PageSize", pageSize);

                con.Open();
                var reader = cmd.ExecuteReader();

                var data = EntityMapper.FillCollection<NewsRelationEntity>(reader);

                con.Close();

                var abc = InitNewsRelationToDb(nameSpace, connectStr2, data, progress, processEntity);

                return abc;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return 0;
            }
        }
        private static int InitNewsRelationToDb(string nameSpace, string connectStr2, List<NewsRelationEntity> data, IProgress<InitDataDb.ProcessEntity> progress, InitDataDb.ProcessEntity processEntity)
        {
            long id = 0;
            var count = 0;
            try
            {
                var con = GetConnection(connectStr2);
                con.Open();

                foreach (var item in data)
                {
                    string query = @"DELETE [NewsRelation] WHERE NewsId=@NewsId and ZoneId=@ZoneId and NewsRelationId=@NewsRelationId
                                    INSERT INTO [NewsRelation]([NewsId]
                                                            ,[NewsRelationId]
                                                            ,[ZoneId]
                                                            ,[IsChange]
                                                            ,[Priority]
                                                            ,[Type]
                                                ) VALUES(@NewsId
                                                        ,@NewsRelationId
                                                        ,@ZoneId
                                                        ,@IsChange
                                                        ,@Priority
                                                        ,@Type)";

                    var cmd = new SqlCommand(query, con);                    
                    cmd.CommandType = CommandType.Text;

                    cmd.Parameters.AddWithValue("@NewsId", item.NewsId);
                    cmd.Parameters.AddWithValue("@NewsRelationId", item.NewsRelationId);
                    cmd.Parameters.AddWithValue("@ZoneId", item.ZoneId);
                    cmd.Parameters.AddWithValue("@IsChange", item.IsChange);
                    cmd.Parameters.AddWithValue("@Priority", item.Priority);
                    cmd.Parameters.AddWithValue("@Type", item.Type);

                    id = item.NewsId;
                    var reader = cmd.ExecuteNonQuery();
                    count++;

                    if (progress != null)
                    {
                        processEntity.value = count;
                        progress.Report(processEntity);
                    }
                }

                con.Close();

                return count;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message + " ID: " + id.ToString());
                return count;
            }
        }
        public static int GetCountNewsRelation(string connectStr)
        {
            try
            {
                string query = @"SELECT COUNT(*) as [Count] FROM NewsRelation";

                var con = GetConnection(connectStr);
                var cmd = new SqlCommand(query, con);

                con.Open();
                var reader = cmd.ExecuteReader();

                var data = EntityMapper.FillCollection<CountNewsEntity>(reader);

                con.Close();

                var count = 0;
                if (data != null && data.Count > 0)
                {
                    count = data.FirstOrDefault().Count;
                }

                return count;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return 1;
            }
        }
        #endregion

        #region newsextension
        public static int SyncNewsExtensionToDb(string nameSpace, string connectStr1, string connectStr2, int pageIndex, int pageSize, IProgress<InitDataDb.ProcessEntity> progress, InitDataDb.ProcessEntity processEntity)
        {
            try
            {
                string query = @"DECLARE @UpperBand int, @LowerBand int
                                SET @LowerBand  = (@pageIndex - 1) * @PageSize
                                SET @UpperBand  = (@pageIndex * @PageSize)
                                SELECT * FROM (
                                SELECT n.*,ROW_NUMBER() OVER(ORDER BY n.NewsId DESC) AS RowNumber 
                                FROM NewsExtension n
                                ) AS temp
                                WHERE RowNumber > @LowerBand AND RowNumber <= @UpperBand";

                //test Id=20161214035128196
                //query = @"SELECT * FROM News WHERE Id=20161214035128196";

                var con = GetConnection(connectStr1);
                var cmd = new SqlCommand(query, con);
                //cmd.CommandType = CommandType.StoredProcedure;               
                var dateFrom = DBNull.Value;
                var dateTo = DBNull.Value;
                cmd.Parameters.AddWithValue("@PageIndex", pageIndex);
                cmd.Parameters.AddWithValue("@PageSize", pageSize);                

                con.Open();
                var reader = cmd.ExecuteReader();

                var data = EntityMapper.FillCollection<NewsExtensionEntity>(reader);

                con.Close();

                var abc = InitNewsExtensionToDb(nameSpace, connectStr2, data, progress, processEntity);

                return abc;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return 0;                
            }
        }
        private static int InitNewsExtensionToDb(string nameSpace, string connectStr2, List<NewsExtensionEntity> data, IProgress<InitDataDb.ProcessEntity> progress, InitDataDb.ProcessEntity processEntity)
        {
            long id = 0;
            var count = 0;
            try
            {
                var con = GetConnection(connectStr2);
                con.Open();

                foreach (var item in data)
                {
                    string query = @"DELETE [NewsExtension] WHERE NewsId=@NewsId and [Type]=@Type
                                    INSERT INTO [NewsExtension]([NewsId]
                                                                ,[Type]
                                                                ,[Value]
                                                ) VALUES(@NewsId
                                                        ,@Type
                                                        ,@Value)";

                    var cmd = new SqlCommand(query, con);
                    //cmd.CommandType = CommandType.StoredProcedure;  
                    cmd.CommandType = CommandType.Text;
                    cmd.Parameters.AddWithValue("@NewsId", item.NewsId);
                    cmd.Parameters.AddWithValue("@Type", item.Type);
                    cmd.Parameters.AddWithValue("@Value", item.Value ?? (object)DBNull.Value);                   

                    id = item.NewsId;
                    var reader = cmd.ExecuteNonQuery();
                    count++;

                    if (progress != null)
                    {
                        processEntity.value = count;
                        progress.Report(processEntity);
                    }
                }

                con.Close();

                return count;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message + " ID: " + id.ToString());
                return count;
            }
        }
        public static int GetCountNewsExtension(string connectStr)
        {
            try
            {
                string query = @"SELECT COUNT(*) as [Count] FROM NewsExtension";

                var con = GetConnection(connectStr);
                var cmd = new SqlCommand(query, con);

                con.Open();
                var reader = cmd.ExecuteReader();

                var data = EntityMapper.FillCollection<CountNewsEntity>(reader);

                con.Close();

                var count = 0;
                if (data != null && data.Count > 0)
                {
                    count = data.FirstOrDefault().Count;
                }

                return count;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return 1;
            }
        }
        #endregion

        #region NewsPosition
        public static int SyncNewsPositionToDb(string nameSpace, string connectStr1, string connectStr2, int pageIndex, int pageSize, IProgress<InitDataDb.ProcessEntity> progress, InitDataDb.ProcessEntity processEntity)
        {
            try
            {
                string query = @"DECLARE @UpperBand int, @LowerBand int
                                SET @LowerBand  = (@pageIndex - 1) * @PageSize
                                SET @UpperBand  = (@pageIndex * @PageSize)
                                SELECT * FROM (
                                SELECT n.*,ROW_NUMBER() OVER(ORDER BY n.Id DESC) AS RowNumber 
                                FROM NewsPosition n
                                ) AS temp
                                WHERE RowNumber > @LowerBand AND RowNumber <= @UpperBand";

                //test Id=20161214035128196
                //query = @"SELECT * FROM News WHERE Id=20161214035128196";

                var con = GetConnection(connectStr1);
                var cmd = new SqlCommand(query, con);
                //cmd.CommandType = CommandType.StoredProcedure;               
                var dateFrom = DBNull.Value;
                var dateTo = DBNull.Value;
                cmd.Parameters.AddWithValue("@PageIndex", pageIndex);
                cmd.Parameters.AddWithValue("@PageSize", pageSize);

                con.Open();
                var reader = cmd.ExecuteReader();

                var data = EntityMapper.FillCollection<NewsPositionEntity>(reader);

                con.Close();

                var abc = InitNewsPositionToDb(nameSpace, connectStr2, data, progress, processEntity);

                return abc;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return 0;
            }
        }
        private static int InitNewsPositionToDb(string nameSpace, string connectStr2, List<NewsPositionEntity> data, IProgress<InitDataDb.ProcessEntity> progress, InitDataDb.ProcessEntity processEntity)
        {
            long id = 0;
            var count = 0;
            try
            {
                var con = GetConnection(connectStr2);
                con.Open();

                foreach (var item in data)
                {
                    string query = @"SET IDENTITY_INSERT [NewsPosition] ON;
                                    DELETE [NewsPosition] WHERE Id=@Id;
                                    INSERT INTO [NewsPosition]([Id]
                                                              ,[ZoneId]
                                                              ,[TypeId]
                                                              ,[Order]
                                                              ,[Lockable]
                                                              ,[Locked]
                                                              ,[ExpiredLock]
                                                              ,[NewsIdForBomd]
                                                              ,[NewsId]
                                                              ,[Title]
                                                              ,[SubTitle]
                                                              ,[Sapo]
                                                              ,[Avatar]
                                                              ,[AvatarDesc]
                                                              ,[Avatar1]
                                                              ,[Avatar2]
                                                              ,[Avatar3]
                                                              ,[Avatar4]
                                                              ,[Avatar5]
                                                              ,[Author]
                                                              ,[NewsRelation]
                                                              ,[Source]
                                                              ,[IsFocus]
                                                              ,[NewsType]
                                                              ,[ThreadId]
                                                              ,[DistributionDate]
                                                              ,[Url]
                                                              ,[AllowAutoUpdate]
                                                              ,[DisplayStyle]
                                                              ,[DisplayPosition]
                                                              ,[DisplayInSlide]
                                                              ,[AvatarCustom]
                                                              ,[OriginalId]
                                                              ,[InitSapo]
                                                              ,[InterviewId]
                                                              ,[IsBreakingNews]
                                                              ,[OriginalUrl]
                                                              ,[IsPr]
                                                              ,[AdStore]
                                                              ,[AdStoreUrl]
                                                              ,[RollingNewsId]
                                                              ,[LastModifiedDate]
                                                              ,[TagSubTitleId]
                                                              ,[ZoneIdForNews]
                                                              ,[Position]
                                                              ,[PrPosition]
                                                              ,[Type]
                                                              ,[Priority]
                                                              ,[LastModifiedDateStamp]
                                                              ,[PublishedDateStamp]
                                                              ,[LocationType]
                                                              ,[ExpiredDate]
                                                              ,[ScheduleDate]
                                                              ,[IsOnHome]
                                                              ,[ParentNewsId]
                                                ) VALUES(@Id
                                                        ,@ZoneId
                                                        ,@TypeId
                                                        ,@Order
                                                        ,@Lockable
                                                        ,@Locked
                                                        ,@ExpiredLock
                                                        ,@NewsIdForBomd
                                                        ,@NewsId
                                                        ,@Title
                                                        ,@SubTitle
                                                        ,@Sapo
                                                        ,@Avatar
                                                        ,@AvatarDesc
                                                        ,@Avatar1
                                                        ,@Avatar2
                                                        ,@Avatar3
                                                        ,@Avatar4
                                                        ,@Avatar5
                                                        ,@Author
                                                        ,@NewsRelation
                                                        ,@Source
                                                        ,@IsFocus
                                                        ,@NewsType
                                                        ,@ThreadId
                                                        ,@DistributionDate
                                                        ,@Url
                                                        ,@AllowAutoUpdate
                                                        ,@DisplayStyle
                                                        ,@DisplayPosition
                                                        ,@DisplayInSlide
                                                        ,@AvatarCustom
                                                        ,@OriginalId
                                                        ,@InitSapo
                                                        ,@InterviewId
                                                        ,@IsBreakingNews
                                                        ,@OriginalUrl
                                                        ,@IsPr
                                                        ,@AdStore
                                                        ,@AdStoreUrl
                                                        ,@RollingNewsId
                                                        ,@LastModifiedDate
                                                        ,@TagSubTitleId
                                                        ,@ZoneIdForNews
                                                        ,@Position
                                                        ,@PrPosition
                                                        ,@Type
                                                        ,@Priority
                                                        ,@LastModifiedDateStamp
                                                        ,@PublishedDateStamp
                                                        ,@LocationType
                                                        ,@ExpiredDate
                                                        ,@ScheduleDate
                                                        ,@IsOnHome
                                                        ,@ParentNewsId);
                                                SET IDENTITY_INSERT [NewsPosition] OFF;";

                    var cmd = new SqlCommand(query, con);
                    //cmd.CommandType = CommandType.StoredProcedure;  
                    cmd.CommandType = CommandType.Text;
                    cmd.Parameters.AddWithValue("@Id", item.Id);
                    cmd.Parameters.AddWithValue("@ZoneId", item.ZoneId);
                    cmd.Parameters.AddWithValue("@TypeId", item.TypeId);
                    cmd.Parameters.AddWithValue("@Order", item.Order);
                    cmd.Parameters.AddWithValue("@Lockable", item.Lockable);
                    cmd.Parameters.AddWithValue("@Locked", item.Locked);

                    if (item.ExpiredLock <= new DateTime(1980, 1, 1))
                        cmd.Parameters.AddWithValue("@ExpiredLock", DBNull.Value);
                    else
                        cmd.Parameters.AddWithValue("@ExpiredLock", item.ExpiredLock);                   

                    cmd.Parameters.AddWithValue("@NewsIdForBomd", item.NewsIdForBomd);
                    cmd.Parameters.AddWithValue("@NewsId", item.NewsId);
                    cmd.Parameters.AddWithValue("@Title", item.Title ?? (object)DBNull.Value);
                    cmd.Parameters.AddWithValue("@SubTitle", item.SubTitle ?? (object)DBNull.Value);
                    cmd.Parameters.AddWithValue("@Sapo", item.Sapo ?? (object)DBNull.Value);
                    cmd.Parameters.AddWithValue("@Avatar", item.Avatar ?? (object)DBNull.Value);
                    cmd.Parameters.AddWithValue("@AvatarDesc", item.AvatarDesc ?? (object)DBNull.Value);
                    cmd.Parameters.AddWithValue("@Avatar1", item.Avatar1 ?? (object)DBNull.Value);
                    cmd.Parameters.AddWithValue("@Avatar2", item.Avatar2 ?? (object)DBNull.Value);
                    cmd.Parameters.AddWithValue("@Avatar3", item.Avatar3 ?? (object)DBNull.Value);
                    cmd.Parameters.AddWithValue("@Avatar4", item.Avatar4 ?? (object)DBNull.Value);
                    cmd.Parameters.AddWithValue("@Avatar5", item.Avatar5 ?? (object)DBNull.Value);
                    cmd.Parameters.AddWithValue("@Author", item.Author ?? (object)DBNull.Value);
                    cmd.Parameters.AddWithValue("@NewsRelation", item.NewsRelation ?? (object)DBNull.Value);
                    cmd.Parameters.AddWithValue("@Source", item.Source ?? (object)DBNull.Value);
                    cmd.Parameters.AddWithValue("@IsFocus", item.IsFocus);

                    if (item.NewsType <= 0)
                        cmd.Parameters.AddWithValue("@NewsType", 0);
                    else
                        cmd.Parameters.AddWithValue("@NewsType", item.NewsType);
                    
                    cmd.Parameters.AddWithValue("@ThreadId", item.ThreadId);

                    if (item.DistributionDate <= new DateTime(1980, 1, 1))
                        cmd.Parameters.AddWithValue("@DistributionDate", DBNull.Value);
                    else
                        cmd.Parameters.AddWithValue("@DistributionDate", item.DistributionDate);
                    
                    cmd.Parameters.AddWithValue("@Url", item.Url ?? (object)DBNull.Value);
                    cmd.Parameters.AddWithValue("@AllowAutoUpdate", item.AllowAutoUpdate);
                    cmd.Parameters.AddWithValue("@DisplayStyle", item.DisplayStyle);
                    cmd.Parameters.AddWithValue("@DisplayPosition", item.DisplayPosition);
                    cmd.Parameters.AddWithValue("@DisplayInSlide", item.DisplayInSlide);
                    cmd.Parameters.AddWithValue("@AvatarCustom", item.AvatarCustom ?? (object)DBNull.Value);
                    cmd.Parameters.AddWithValue("@OriginalId", item.OriginalId);
                    cmd.Parameters.AddWithValue("@InitSapo", item.InitSapo ?? (object)DBNull.Value);
                    cmd.Parameters.AddWithValue("@InterviewId", item.InterviewId);
                    cmd.Parameters.AddWithValue("@IsBreakingNews", item.IsBreakingNews);
                    cmd.Parameters.AddWithValue("@OriginalUrl", item.OriginalUrl ?? (object)DBNull.Value);
                    cmd.Parameters.AddWithValue("@IsPr", item.IsPr);
                    cmd.Parameters.AddWithValue("@AdStore", item.AdStore);
                    cmd.Parameters.AddWithValue("@AdStoreUrl", item.AdStoreUrl ?? (object)DBNull.Value);
                    cmd.Parameters.AddWithValue("@RollingNewsId", item.RollingNewsId);

                    if (item.LastModifiedDate <= new DateTime(1980, 1, 1))
                        cmd.Parameters.AddWithValue("@LastModifiedDate", DBNull.Value);
                    else
                        cmd.Parameters.AddWithValue("@LastModifiedDate", item.LastModifiedDate);
                    
                    cmd.Parameters.AddWithValue("@TagSubTitleId", item.TagSubTitleId);
                    cmd.Parameters.AddWithValue("@ZoneIdForNews", item.ZoneIdForNews);
                    cmd.Parameters.AddWithValue("@Position", item.Position);
                    cmd.Parameters.AddWithValue("@PrPosition", item.PrPosition);
                    cmd.Parameters.AddWithValue("@Type", item.Type);
                    cmd.Parameters.AddWithValue("@Priority", item.Priority);
                    cmd.Parameters.AddWithValue("@LastModifiedDateStamp", item.LastModifiedDateStamp);
                    cmd.Parameters.AddWithValue("@PublishedDateStamp", item.PublishedDateStamp);
                    cmd.Parameters.AddWithValue("@LocationType", item.LocationType);

                    if (item.ExpiredDate <= new DateTime(1980, 1, 1))
                        cmd.Parameters.AddWithValue("@ExpiredDate", DBNull.Value);
                    else
                        cmd.Parameters.AddWithValue("@ExpiredDate", item.ExpiredDate);

                    if (item.ScheduleDate <= new DateTime(1980, 1, 1))
                        cmd.Parameters.AddWithValue("@ScheduleDate", DBNull.Value);
                    else
                        cmd.Parameters.AddWithValue("@ScheduleDate", item.ScheduleDate);
                    
                    cmd.Parameters.AddWithValue("@IsOnHome", item.IsOnHome);
                    cmd.Parameters.AddWithValue("@ParentNewsId", item.ParentNewsId);

                    id = item.Id;
                    var reader = cmd.ExecuteNonQuery();
                    count++;

                    if (progress != null)
                    {
                        processEntity.value = count;
                        progress.Report(processEntity);
                    }
                }

                con.Close();

                return count;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message + " ID: " + id.ToString());
                return count;
            }
        }
        public static int GetCountNewsPosition(string connectStr)
        {
            try
            {
                string query = @"SELECT COUNT(*) as [Count] FROM NewsPosition";

                var con = GetConnection(connectStr);
                var cmd = new SqlCommand(query, con);

                con.Open();
                var reader = cmd.ExecuteReader();

                var data = EntityMapper.FillCollection<CountNewsEntity>(reader);

                con.Close();

                var count = 0;
                if (data != null && data.Count > 0)
                {
                    count = data.FirstOrDefault().Count;
                }

                return count;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return 1;
            }
        }
        #endregion

        #region NewsPr
        public static int SyncNewsPrToDb(string nameSpace, string connectStr1, string connectStr2, int pageIndex, int pageSize, IProgress<InitDataDb.ProcessEntity> progress, InitDataDb.ProcessEntity processEntity)
        {
            try
            {
                string query = @"DECLARE @UpperBand int, @LowerBand int
                                SET @LowerBand  = (@pageIndex - 1) * @PageSize
                                SET @UpperBand  = (@pageIndex * @PageSize)
                                SELECT * FROM (
                                SELECT n.*,ROW_NUMBER() OVER(ORDER BY n.Id DESC) AS RowNumber 
                                FROM NewsPr n where (@DateFrom is null and (1=1)) or (@DateFrom is not null and (CONVERT(datetime,n.DistributionDate) >= CONVERT(datetime,@DateFrom) AND CONVERT(datetime,n.DistributionDate)<=CONVERT(datetime,@DateTo)))
                                ) AS temp
                                WHERE RowNumber > @LowerBand AND RowNumber <= @UpperBand";

                var con = GetConnection(connectStr1);
                var cmd = new SqlCommand(query, con);
                //cmd.CommandType = CommandType.StoredProcedure;               
                var dateFrom = DBNull.Value;
                var dateTo = DBNull.Value;
                cmd.Parameters.AddWithValue("@PageIndex", pageIndex);
                cmd.Parameters.AddWithValue("@PageSize", pageSize);
                cmd.Parameters.AddWithValue("@DateFrom", dateFrom);
                cmd.Parameters.AddWithValue("@DateTo", dateTo);

                con.Open();
                var reader = cmd.ExecuteReader();

                var data = EntityMapper.FillCollection<NewsPrEntity>(reader);

                con.Close();

                var abc = InitNewsPrToDb(nameSpace, connectStr2, data, progress, processEntity);

                return abc;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return 0;
            }
        }
        private static int InitNewsPrToDb(string nameSpace, string connectStr2, List<NewsPrEntity> data, IProgress<InitDataDb.ProcessEntity> progress, InitDataDb.ProcessEntity processEntity)
        {
            long id = 0;
            var count = 0;
            try
            {
                var con = GetConnection(connectStr2);
                con.Open();

                foreach (var item in data)
                {
                    string query = @"SET IDENTITY_INSERT [NewsPr] ON;
                                    DELETE [NewsPr] WHERE Id=@Id;
                                    INSERT INTO [NewsPr](Id
                                                        ,[ContentId]
                                                        ,[DistributionId]
                                                        ,[NewsId]
                                                        ,[Mode]
                                                        ,[IsFocus]
                                                        ,[DistributionDate]
                                                        ,[PublishDate]
                                                        ,[ExpireDate]
                                                        ,[CreatedDate]
                                                        ,[Duration]
                                                        ,[LastModifiedDate]
                                                        ,[BookingId]
                                                        ,[ContractNo]
                                                        ,[ZoneId]
                                                        ,[Price]
                                                        ,[ViewPlusBannerId]
                                                ) VALUES(@Id
                                                        ,@ContentId
                                                        ,@DistributionId
                                                        ,@NewsId
                                                        ,@Mode
                                                        ,@IsFocus
                                                        ,@DistributionDate
                                                        ,@PublishDate
                                                        ,@ExpireDate
                                                        ,@CreatedDate
                                                        ,@Duration
                                                        ,@LastModifiedDate
                                                        ,@BookingId
                                                        ,@ContractNo
                                                        ,@ZoneId
                                                        ,@Price
                                                        ,@ViewPlusBannerId);
                                                SET IDENTITY_INSERT [NewsPr] OFF;";

                    using (var cmd = new SqlCommand(query, con))
                    {
                        cmd.CommandType = CommandType.Text;

                        cmd.Parameters.AddWithValue("@Id", item.Id);
                        cmd.Parameters.AddWithValue("@ContentId", item.ContentId);
                        cmd.Parameters.AddWithValue("@DistributionId", item.DistributionId);
                        cmd.Parameters.AddWithValue("@NewsId", item.NewsId);
                        cmd.Parameters.AddWithValue("@Mode", item.Mode);
                        cmd.Parameters.AddWithValue("@IsFocus", item.IsFocus);

                        if (item.DistributionDate <= new DateTime(1980, 1, 1))
                            cmd.Parameters.AddWithValue("@DistributionDate", DBNull.Value);
                        else
                            cmd.Parameters.AddWithValue("@DistributionDate", item.DistributionDate);

                        if (item.PublishDate <= new DateTime(1980, 1, 1))
                            cmd.Parameters.AddWithValue("@PublishDate", DBNull.Value);
                        else
                            cmd.Parameters.AddWithValue("@PublishDate", item.PublishDate);

                        if (item.ExpireDate <= new DateTime(1980, 1, 1))
                            cmd.Parameters.AddWithValue("@ExpireDate", DBNull.Value);
                        else
                            cmd.Parameters.AddWithValue("@ExpireDate", item.ExpireDate);

                        if (item.CreatedDate <= new DateTime(1980, 1, 1))
                            cmd.Parameters.AddWithValue("@CreatedDate", DBNull.Value);
                        else
                            cmd.Parameters.AddWithValue("@CreatedDate", item.CreatedDate);

                        cmd.Parameters.AddWithValue("@Duration", item.Duration);

                        if (item.LastModifiedDate <= new DateTime(1980, 1, 1))
                            cmd.Parameters.AddWithValue("@LastModifiedDate", DBNull.Value);
                        else
                            cmd.Parameters.AddWithValue("@LastModifiedDate", item.LastModifiedDate);

                        if (item.ZoneId < 0)
                            cmd.Parameters.AddWithValue("@BookingId", DBNull.Value);
                        else
                            cmd.Parameters.AddWithValue("@BookingId", item.BookingId);

                        cmd.Parameters.AddWithValue("@ContractNo", item.ContractNo??(object)DBNull.Value);

                        if (item.ZoneId<0)
                            cmd.Parameters.AddWithValue("@ZoneId", DBNull.Value);
                        else
                            cmd.Parameters.AddWithValue("@ZoneId", item.ZoneId);

                        if (item.Price < 0)
                            cmd.Parameters.AddWithValue("@Price", DBNull.Value);
                        else
                            cmd.Parameters.AddWithValue("@Price", item.Price);

                        cmd.Parameters.AddWithValue("@ViewPlusBannerId", item.ViewPlusBannerId);                        

                        id = item.Id;
                        var reader = cmd.ExecuteNonQuery();
                        count++;

                        if (progress != null)
                        {
                            processEntity.value = count;
                            progress.Report(processEntity);
                        }
                    }
                }

                con.Close();

                return count;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message + " ID: " + id.ToString());
                return count;
            }
        }
        public static int GetCountNewsPr(string connectStr)
        {
            try
            {
                string query = @"SELECT COUNT(*) as [Count] FROM NewsPr";

                var con = GetConnection(connectStr);
                var cmd = new SqlCommand(query, con);

                con.Open();
                var reader = cmd.ExecuteReader();

                var data = EntityMapper.FillCollection<CountNewsEntity>(reader);

                con.Close();

                var count = 0;
                if (data != null && data.Count > 0)
                {
                    count = data.FirstOrDefault().Count;
                }

                return count;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return 1;
            }
        }
        #endregion

        #region NewsInZone
        public static int SyncNewsInZoneToDb(string nameSpace, string connectStr1, string connectStr2, int pageIndex, int pageSize, IProgress<InitDataDb.ProcessEntity> progress, InitDataDb.ProcessEntity processEntity)
        {
            try
            {
                string query = @"DECLARE @UpperBand int, @LowerBand int
                                SET @LowerBand  = (@pageIndex - 1) * @PageSize
                                SET @UpperBand  = (@pageIndex * @PageSize)
                                SELECT * FROM (
                                SELECT n.*,ROW_NUMBER() OVER(ORDER BY n.NewsId DESC) AS RowNumber 
                                FROM NewsInZone n
                                ) AS temp
                                WHERE RowNumber > @LowerBand AND RowNumber <= @UpperBand";

                //test Id=20161214035128196
                //query = @"SELECT * FROM News WHERE Id=20161214035128196";

                var con = GetConnection(connectStr1);
                var cmd = new SqlCommand(query, con);
                //cmd.CommandType = CommandType.StoredProcedure;               
                var dateFrom = DBNull.Value;
                var dateTo = DBNull.Value;
                cmd.Parameters.AddWithValue("@PageIndex", pageIndex);
                cmd.Parameters.AddWithValue("@PageSize", pageSize);

                con.Open();
                var reader = cmd.ExecuteReader();

                var data = EntityMapper.FillCollection<NewsInZoneEntity>(reader);

                con.Close();

                var abc = InitNewsInZoneToDb(nameSpace, connectStr2, data, progress, processEntity);

                return abc;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return 0;
            }
        }
        private static int InitNewsInZoneToDb(string nameSpace, string connectStr2, List<NewsInZoneEntity> data, IProgress<InitDataDb.ProcessEntity> progress, InitDataDb.ProcessEntity processEntity)
        {
            long id = 0;
            var count = 0;
            try
            {
                var con = GetConnection(connectStr2);
                con.Open();

                foreach (var item in data)
                {
                    string query = @"DELETE [NewsInZone] WHERE NewsId=@NewsId and ZoneId=@ZoneId and IsPrimary=@IsPrimary
                                    INSERT INTO [NewsInZone]([NewsId]
                                                            ,[ZoneId]
                                                            ,[IsPrimary]
                                                            ,[DistributionID]
                                                            ,[RootZoneId]
                                                ) VALUES(@NewsId
                                                        ,@ZoneId
                                                        ,@IsPrimary
                                                        ,@DistributionID
                                                        ,@RootZoneId)";

                    var cmd = new SqlCommand(query, con);
                    //cmd.CommandType = CommandType.StoredProcedure;  
                    cmd.CommandType = CommandType.Text;
                    cmd.Parameters.AddWithValue("@NewsId", item.NewsId);
                    cmd.Parameters.AddWithValue("@ZoneId", item.ZoneId);
                    cmd.Parameters.AddWithValue("@IsPrimary", item.IsPrimary);
                    cmd.Parameters.AddWithValue("@DistributionID", item.DistributionID);
                    cmd.Parameters.AddWithValue("@RootZoneId", item.RootZoneId);

                    id = item.NewsId;
                    var reader = cmd.ExecuteNonQuery();
                    count++;

                    if (progress != null)
                    {
                        processEntity.value = count;
                        progress.Report(processEntity);
                    }
                }

                con.Close();

                return count;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message + " ID: " + id.ToString());
                return count;
            }
        }
        public static int GetCountNewsInZone(string connectStr)
        {
            try
            {
                string query = @"SELECT COUNT(*) as [Count] FROM NewsInZone";

                var con = GetConnection(connectStr);
                var cmd = new SqlCommand(query, con);

                con.Open();
                var reader = cmd.ExecuteReader();

                var data = EntityMapper.FillCollection<CountNewsEntity>(reader);

                con.Close();

                var count = 0;
                if (data != null && data.Count > 0)
                {
                    count = data.FirstOrDefault().Count;
                }

                return count;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return 1;
            }
        }
        #endregion

        #region menuextension
        public static int SyncMenuExtensionToDb(string nameSpace, string connectStr1, string connectStr2, int pageIndex, int pageSize, IProgress<InitDataDb.ProcessEntity> progress, InitDataDb.ProcessEntity processEntity)
        {
            try
            {
                string query = @"DECLARE @UpperBand int, @LowerBand int
                                SET @LowerBand  = (@pageIndex - 1) * @PageSize
                                SET @UpperBand  = (@pageIndex * @PageSize)
                                SELECT * FROM (
                                SELECT n.*,ROW_NUMBER() OVER(ORDER BY n.Id DESC) AS RowNumber 
                                FROM MenuExtension n
                                ) AS temp
                                WHERE RowNumber > @LowerBand AND RowNumber <= @UpperBand";

                //test Id=20161214035128196
                //query = @"SELECT * FROM News WHERE Id=20161214035128196";

                var con = GetConnection(connectStr1);
                var cmd = new SqlCommand(query, con);
                //cmd.CommandType = CommandType.StoredProcedure;               
                var dateFrom = DBNull.Value;
                var dateTo = DBNull.Value;
                cmd.Parameters.AddWithValue("@PageIndex", pageIndex);
                cmd.Parameters.AddWithValue("@PageSize", pageSize);

                con.Open();
                var reader = cmd.ExecuteReader();

                var data = EntityMapper.FillCollection<MenuExtensionEntity>(reader);

                con.Close();

                var abc = InitMenuExtensionToDb(nameSpace, connectStr2, data, progress, processEntity);

                return abc;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return 0;
            }
        }
        private static int InitMenuExtensionToDb(string nameSpace, string connectStr2, List<MenuExtensionEntity> data, IProgress<InitDataDb.ProcessEntity> progress, InitDataDb.ProcessEntity processEntity)
        {
            long id = 0;
            var count = 0;
            try
            {
                var con = GetConnection(connectStr2);
                con.Open();

                foreach (var item in data)
                {
                    string query = @"SET IDENTITY_INSERT [MenuExtension] ON;
                                    DELETE [MenuExtension] WHERE Id=@Id;
                                    INSERT INTO [MenuExtension]([Id]
                                                                ,[ParentMenuId]
                                                                ,[GroupMenuId]
                                                                ,[ZoneId]
                                                                ,[ParentZoneId]
                                                                ,[TagId]
                                                                ,[Name]
                                                                ,[Avatar]
                                                                ,[Url]
                                                                ,[Priority]
                                                                ,[Status]
                                                                ,[OldId]
                                                                ,[OldParentId]
                                                                ,[OldType]
                                                                ,[OldIsTopMenu]
                                                                ,[OldCatId]
                                                ) VALUES(@Id
                                                        ,@ParentMenuId
                                                        ,@GroupMenuId
                                                        ,@ZoneId
                                                        ,@ParentZoneId
                                                        ,@TagId
                                                        ,@Name
                                                        ,@Avatar
                                                        ,@Url
                                                        ,@Priority
                                                        ,@Status
                                                        ,@OldId
                                                        ,@OldParentId
                                                        ,@OldType
                                                        ,@OldIsTopMenu
                                                        ,@OldCatId);
                                            SET IDENTITY_INSERT [MenuExtension] OFF;";

                    var cmd = new SqlCommand(query, con);
                    //cmd.CommandType = CommandType.StoredProcedure;  
                    cmd.CommandType = CommandType.Text;
                    cmd.Parameters.AddWithValue("@Id", item.Id);
                    cmd.Parameters.AddWithValue("@ParentMenuId", item.ParentMenuId);
                    cmd.Parameters.AddWithValue("@GroupMenuId", item.GroupMenuId);
                    cmd.Parameters.AddWithValue("@ZoneId", item.ZoneId);
                    cmd.Parameters.AddWithValue("@ParentZoneId", item.ParentZoneId);
                    cmd.Parameters.AddWithValue("@TagId", item.TagId);
                    cmd.Parameters.AddWithValue("@Name", item.Name ?? (object)DBNull.Value);
                    cmd.Parameters.AddWithValue("@Avatar", item.Avatar ?? (object)DBNull.Value);
                    cmd.Parameters.AddWithValue("@Url", item.Url ?? (object)DBNull.Value);
                    cmd.Parameters.AddWithValue("@Priority", item.Priority);
                    cmd.Parameters.AddWithValue("@Status", item.Status);
                    cmd.Parameters.AddWithValue("@OldId", item.OldId);
                    cmd.Parameters.AddWithValue("@OldParentId", item.OldParentId);
                    cmd.Parameters.AddWithValue("@OldType", item.OldType);
                    cmd.Parameters.AddWithValue("@OldIsTopMenu", item.OldIsTopMenu);
                    cmd.Parameters.AddWithValue("@OldCatId", item.OldCatId);

                    id = item.Id;
                    var reader = cmd.ExecuteNonQuery();
                    count++;

                    if (progress != null)
                    {
                        processEntity.value = count;
                        progress.Report(processEntity);
                    }
                }

                con.Close();

                return count;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message + " ID: " + id.ToString());
                return count;
            }
        }
        public static int GetCountMenuExtension(string connectStr)
        {
            try
            {
                string query = @"SELECT COUNT(*) as [Count] FROM MenuExtension";

                var con = GetConnection(connectStr);
                var cmd = new SqlCommand(query, con);

                con.Open();
                var reader = cmd.ExecuteReader();

                var data = EntityMapper.FillCollection<CountNewsEntity>(reader);

                con.Close();

                var count = 0;
                if (data != null && data.Count > 0)
                {
                    count = data.FirstOrDefault().Count;
                }

                return count;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return 1;
            }
        }
        #endregion

        #region SEOMetaNews
        public static int SyncSEOMetaNewsToDb(string nameSpace, string connectStr1, string connectStr2, int pageIndex, int pageSize, IProgress<InitDataDb.ProcessEntity> progress, InitDataDb.ProcessEntity processEntity)
        {
            try
            {
                string query = @"DECLARE @UpperBand int, @LowerBand int
                                SET @LowerBand  = (@pageIndex - 1) * @PageSize
                                SET @UpperBand  = (@pageIndex * @PageSize)
                                SELECT * FROM (
                                SELECT n.*,ROW_NUMBER() OVER(ORDER BY n.NewsId DESC) AS RowNumber 
                                FROM SEOMetaNews n
                                ) AS temp
                                WHERE RowNumber > @LowerBand AND RowNumber <= @UpperBand";
                
                var con = GetConnection(connectStr1);
                var cmd = new SqlCommand(query, con);
                
                var dateFrom = DBNull.Value;
                var dateTo = DBNull.Value;
                cmd.Parameters.AddWithValue("@PageIndex", pageIndex);
                cmd.Parameters.AddWithValue("@PageSize", pageSize);

                con.Open();
                var reader = cmd.ExecuteReader();

                var data = EntityMapper.FillCollection<SEOMetaNewsEntity>(reader);

                con.Close();

                var abc = InitSEOMetaNewsToDb(nameSpace, connectStr2, data, progress, processEntity);

                return abc;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return 0;
            }
        }
        private static int InitSEOMetaNewsToDb(string nameSpace, string connectStr2, List<SEOMetaNewsEntity> data, IProgress<InitDataDb.ProcessEntity> progress, InitDataDb.ProcessEntity processEntity)
        {
            long id = 0;
            var count = 0;
            try
            {
                var con = GetConnection(connectStr2);
                con.Open();

                foreach (var item in data)
                {
                    string query = @"DELETE [SEOMetaNews] WHERE NewsId=@NewsId;
                                    INSERT INTO [SEOMetaNews]([NewsId]
                                                              ,[MetaTitle]
                                                              ,[MetaKeyword]
                                                              ,[MetaDescription]
                                                              ,[MetaNewsKeyword]
                                                              ,[KeywordFocus]
                                                              ,[CreatedBy]
                                                              ,[SocialTitle]
                                                ) VALUES(@NewsId
                                                        ,@MetaTitle
                                                        ,@MetaKeyword
                                                        ,@MetaDescription
                                                        ,@MetaNewsKeyword
                                                        ,@KeywordFocus
                                                        ,@CreatedBy
                                                        ,@SocialTitle);
                                            ";

                    var cmd = new SqlCommand(query, con);
                    
                    cmd.CommandType = CommandType.Text;
                    cmd.Parameters.AddWithValue("@NewsId", item.NewsId);
                    cmd.Parameters.AddWithValue("@MetaTitle", item.MetaTitle ?? (object)DBNull.Value);
                    cmd.Parameters.AddWithValue("@MetaKeyword", item.MetaKeyword ?? (object)DBNull.Value);
                    cmd.Parameters.AddWithValue("@MetaDescription", item.MetaDescription ?? (object)DBNull.Value);
                    cmd.Parameters.AddWithValue("@MetaNewsKeyword", item.MetaNewsKeyword ?? (object)DBNull.Value);
                    cmd.Parameters.AddWithValue("@KeywordFocus", item.KeywordFocus ?? (object)DBNull.Value);
                    cmd.Parameters.AddWithValue("@CreatedBy", item.CreatedBy ?? (object)DBNull.Value);
                    cmd.Parameters.AddWithValue("@SocialTitle", item.SocialTitle ?? (object)DBNull.Value);
                    
                    id = item.NewsId;
                    var reader = cmd.ExecuteNonQuery();
                    count++;

                    if (progress != null)
                    {
                        processEntity.value = count;
                        progress.Report(processEntity);
                    }
                }

                con.Close();

                return count;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message + " ID: " + id.ToString());
                return count;
            }
        }
        public static int GetCountSEOMetaNews(string connectStr)
        {
            try
            {
                string query = @"SELECT COUNT(*) as [Count] FROM SEOMetaNews";

                var con = GetConnection(connectStr);
                var cmd = new SqlCommand(query, con);

                con.Open();
                var reader = cmd.ExecuteReader();

                var data = EntityMapper.FillCollection<CountNewsEntity>(reader);

                con.Close();

                var count = 0;
                if (data != null && data.Count > 0)
                {
                    count = data.FirstOrDefault().Count;
                }

                return count;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return 1;
            }
        }
        #endregion        

        #region SEOMetaVideo
        public static int SyncSEOMetaVideoToDb(string nameSpace, string connectStr1, string connectStr2, int pageIndex, int pageSize, IProgress<InitDataDb.ProcessEntity> progress, InitDataDb.ProcessEntity processEntity)
        {
            try
            {
                string query = @"DECLARE @UpperBand int, @LowerBand int
                                SET @LowerBand  = (@pageIndex - 1) * @PageSize
                                SET @UpperBand  = (@pageIndex * @PageSize)
                                SELECT * FROM (
                                SELECT n.*,ROW_NUMBER() OVER(ORDER BY n.VideoId DESC) AS RowNumber 
                                FROM SEOMetaVideo n
                                ) AS temp
                                WHERE RowNumber > @LowerBand AND RowNumber <= @UpperBand";

                var con = GetConnection(connectStr1);
                var cmd = new SqlCommand(query, con);

                var dateFrom = DBNull.Value;
                var dateTo = DBNull.Value;
                cmd.Parameters.AddWithValue("@PageIndex", pageIndex);
                cmd.Parameters.AddWithValue("@PageSize", pageSize);

                con.Open();
                var reader = cmd.ExecuteReader();

                var data = EntityMapper.FillCollection<SEOMetaVideoEntity>(reader);

                con.Close();

                var abc = InitSEOMetaVideoToDb(nameSpace, connectStr2, data, progress, processEntity);

                return abc;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return 0;
            }
        }
        private static int InitSEOMetaVideoToDb(string nameSpace, string connectStr2, List<SEOMetaVideoEntity> data, IProgress<InitDataDb.ProcessEntity> progress, InitDataDb.ProcessEntity processEntity)
        {
            long id = 0;
            var count = 0;
            try
            {
                var con = GetConnection(connectStr2);
                con.Open();

                foreach (var item in data)
                {
                    string query = @"DELETE [SEOMetaVideo] WHERE VideoId=@VideoId;
                                    INSERT INTO [SEOMetaVideo]([VideoId]
                                                              ,[MetaTitle]
                                                              ,[MetaKeyword]
                                                              ,[MetaDescription]
                                                              ,[MetaVideoKeyword]
                                                              ,[KeywordFocus]                                                             
                                                ) VALUES(@VideoId
                                                        ,@MetaTitle
                                                        ,@MetaKeyword
                                                        ,@MetaDescription
                                                        ,@MetaVideoKeyword
                                                        ,@KeywordFocus);
                                            ";

                    var cmd = new SqlCommand(query, con);

                    cmd.CommandType = CommandType.Text;
                    cmd.Parameters.AddWithValue("@VideoId", item.VideoId);
                    cmd.Parameters.AddWithValue("@MetaTitle", item.MetaTitle ?? (object)DBNull.Value);
                    cmd.Parameters.AddWithValue("@MetaKeyword", item.MetaKeyword ?? (object)DBNull.Value);
                    cmd.Parameters.AddWithValue("@MetaDescription", item.MetaDescription ?? (object)DBNull.Value);
                    cmd.Parameters.AddWithValue("@MetaVideoKeyword", item.MetaVideoKeyword ?? (object)DBNull.Value);
                    cmd.Parameters.AddWithValue("@KeywordFocus", item.KeywordFocus ?? (object)DBNull.Value);                    

                    id = item.VideoId;
                    var reader = cmd.ExecuteNonQuery();
                    count++;

                    if (progress != null)
                    {
                        processEntity.value = count;
                        progress.Report(processEntity);
                    }
                }

                con.Close();

                return count;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message + " ID: " + id.ToString());
                return count;
            }
        }
        public static int GetCountSEOMetaVideo(string connectStr)
        {
            try
            {
                string query = @"SELECT COUNT(*) as [Count] FROM SEOMetaVideo";

                var con = GetConnection(connectStr);
                var cmd = new SqlCommand(query, con);

                con.Open();
                var reader = cmd.ExecuteReader();

                var data = EntityMapper.FillCollection<CountNewsEntity>(reader);

                con.Close();

                var count = 0;
                if (data != null && data.Count > 0)
                {
                    count = data.FirstOrDefault().Count;
                }

                return count;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return 1;
            }
        }
        #endregion

        #region SEOTagZone
        public static int SyncSEOTagZoneToDb(string nameSpace, string connectStr1, string connectStr2, int pageIndex, int pageSize, IProgress<InitDataDb.ProcessEntity> progress, InitDataDb.ProcessEntity processEntity)
        {
            try
            {
                string query = @"DECLARE @UpperBand int, @LowerBand int
                                SET @LowerBand  = (@pageIndex - 1) * @PageSize
                                SET @UpperBand  = (@pageIndex * @PageSize)
                                SELECT * FROM (
                                SELECT n.*,ROW_NUMBER() OVER(ORDER BY n.TagId DESC) AS RowNumber 
                                FROM SEOTagZone n
                                ) AS temp
                                WHERE RowNumber > @LowerBand AND RowNumber <= @UpperBand";

                var con = GetConnection(connectStr1);
                var cmd = new SqlCommand(query, con);

                var dateFrom = DBNull.Value;
                var dateTo = DBNull.Value;
                cmd.Parameters.AddWithValue("@PageIndex", pageIndex);
                cmd.Parameters.AddWithValue("@PageSize", pageSize);

                con.Open();
                var reader = cmd.ExecuteReader();

                var data = EntityMapper.FillCollection<SEOTagZoneEntity>(reader);

                con.Close();

                var abc = InitSEOTagZoneToDb(nameSpace, connectStr2, data, progress, processEntity);

                return abc;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return 0;
            }
        }
        private static int InitSEOTagZoneToDb(string nameSpace, string connectStr2, List<SEOTagZoneEntity> data, IProgress<InitDataDb.ProcessEntity> progress, InitDataDb.ProcessEntity processEntity)
        {
            long id = 0;
            var count = 0;
            try
            {
                var con = GetConnection(connectStr2);
                con.Open();

                foreach (var item in data)
                {
                    string query = @"DELETE [SEOTagZone] WHERE TagId=@TagId and ZoneId=@ZoneId;
                                    INSERT INTO [SEOTagZone]([TagId]
                                                              ,[ZoneId]
                                                              ,[IsPrimary]                                                              
                                                ) VALUES(@TagId
                                                        ,@ZoneId
                                                        ,@IsPrimary);
                                            ";

                    var cmd = new SqlCommand(query, con);

                    cmd.CommandType = CommandType.Text;
                    cmd.Parameters.AddWithValue("@TagId", item.TagId);
                    cmd.Parameters.AddWithValue("@ZoneId", item.ZoneId);
                    cmd.Parameters.AddWithValue("@IsPrimary", item.IsPrimary);                    

                    id = item.TagId;
                    var reader = cmd.ExecuteNonQuery();
                    count++;

                    if (progress != null)
                    {
                        processEntity.value = count;
                        progress.Report(processEntity);
                    }
                }

                con.Close();

                return count;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message + " ID: " + id.ToString());
                return count;
            }
        }
        public static int GetCountSEOTagZone(string connectStr)
        {
            try
            {
                string query = @"SELECT COUNT(*) as [Count] FROM SEOTagZone";

                var con = GetConnection(connectStr);
                var cmd = new SqlCommand(query, con);

                con.Open();
                var reader = cmd.ExecuteReader();

                var data = EntityMapper.FillCollection<CountNewsEntity>(reader);

                con.Close();

                var count = 0;
                if (data != null && data.Count > 0)
                {
                    count = data.FirstOrDefault().Count;
                }

                return count;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return 1;
            }
        }
        #endregion

        #region sync table by newsids and date
        public static List<NewsEntity> GetDataNews(string connectStr, string newsIds)
        {
            var con = GetConnection(connectStr);
            try
            {
                var array = newsIds.Split(',');
                string query = @"SELECT * FROM News where Id in("+ newsIds + ")";

                using (var cmd = new SqlCommand(query, con))
                {
                    //cmd.Parameters.AddWithValue("@NewsIds", newsIds);

                    con.Open();
                    var reader = cmd.ExecuteReader();

                    var data = EntityMapper.FillCollection<NewsEntity>(reader);

                    con.Close();

                    return data;
                }
            }
            catch (Exception ex)
            {
                Logger.Log(TypeLog.Error, "GetDataNews => " + ex.Message);
                return new List<NewsEntity>();
            }
            finally
            {
                con.Close();
                con.Dispose();
            }
        }

        public static List<NewsEntity> GetDataNewsByDate(string connectStr, string from, string to)
        {
            var con = GetConnection(connectStr);
            try
            {
                string query = @"SELECT * FROM News where LastModifiedDate>=@DateFrom and LastModifiedDate<=@DateTo order by LastModifiedDate desc";

                using (var cmd = new SqlCommand(query, con))
                {                               
                    cmd.Parameters.AddWithValue("@DateFrom", from);
                    cmd.Parameters.AddWithValue("@DateTo", to);

                    con.Open();
                    var reader = cmd.ExecuteReader();

                    var data = EntityMapper.FillCollection<NewsEntity>(reader);

                    con.Close();

                    return data;
                }
            }
            catch (Exception ex)
            {
                Logger.Log(TypeLog.Error, "GetDataNewsByDate => " + ex.Message);
                return new List<NewsEntity>();
            }
            finally
            {
                con.Close();
                con.Dispose();
            }
        }

        public static int SyncDataNews(string connectStr1, string connectStr2, List<NewsEntity> data, string nameSpaceEs, string connectEs, string connectRedis, IProgress<InitDataDb.ProcessEntity> progress)
        {
            try
            {
                var processEntity = new InitDataDb.ProcessEntity();
                processEntity.page = data.Count;
                var count = 0;
                foreach (var item in data)
                {
                    try
                    {
                        if (item != null)
                        {
                            //news
                            var resNews = SyncNews(connectStr2, item);

                            //NewsInZone
                            SyncNewsInZone(connectStr1, connectStr2, item.Id);

                            //NewsExtension
                            SyncNewsExtension(connectStr1, connectStr2, item.Id);

                            //TagNews
                            SyncTagNews(connectStr1, connectStr2, item.Id);

                            //ThreadNews
                            SyncThreadNews(connectStr1, connectStr2, item.Id);

                            //NewsRelation
                            SyncNewsRelation(connectStr1, connectStr2, item.Id);

                            //NewsPublish
                            SyncNewsPublish(connectStr1, connectStr2, item);

                            //NewsContent
                            SyncNewsContent(connectStr1, connectStr2, item);

                            if (resNews)
                            {
                                //Update lai chuan avatar theo namespace
                                UpdateDbChuanHoaNoteByHuu(nameSpaceEs, connectStr2, item.Id);

                                //sync es, redis
                                SyncNewsEsAndReDist(connectStr2, item, nameSpaceEs, connectEs, connectRedis);

                                count++;
                                //log ket qua
                                if (count == data.Count)
                                {
                                    Logger.WriteFile(" => Totall: " + data.Count + " => Success: " + count, "ketquasync");
                                }

                                if (progress != null)
                                {
                                    processEntity.value = count;
                                    progress.Report(processEntity);
                                }
                            }
                        }
                        else
                        {
                            Logger.Log(TypeLog.Bug, "SyncDataNews => item is null");
                        }
                    }
                    catch (Exception ex)
                    {
                        Logger.Log(TypeLog.Error, "SyncDataNews => " + ex.Message);
                    }
                }
                return count;
            }
            catch (Exception ex)
            {
                Logger.Log(TypeLog.Error, "SyncDataNews for=> " + ex.Message);
                return 0;
            }
        }

        private static bool SyncNews(string connectStr, NewsEntity item)
        {
            var con = GetConnection(connectStr);
            try
            {
                string query = @"DELETE [News] WHERE Id=@Id
                                    INSERT INTO [News]([Id]
                                                      ,[Title]
                                                      ,[SubTitle]
                                                      ,[Sapo]
                                                      ,[Body]
                                                      ,[Avatar]
                                                      ,[AvatarDesc]
                                                      ,[Avatar2]
                                                      ,[Avatar3]
                                                      ,[Avatar4]
                                                      ,[Avatar5]
                                                      ,[Author]
                                                      ,[NewsRelation]
                                                      ,[Status]
                                                      ,[Source]
                                                      ,[IsFocus]
                                                      ,[Type]
                                                      ,[ThreadId]
                                                      ,[CreatedDate]
                                                      ,[LastModifiedDate]
                                                      ,[DistributionDate]
                                                      ,[CreatedBy]
                                                      ,[LastModifiedBy]
                                                      ,[PublishedBy]
                                                      ,[EditedBy]
                                                      ,[LastReceiver]
                                                      ,[ApprovedBy]
                                                      ,[WordCount]
                                                      ,[ViewCount]
                                                      ,[Priority]
                                                      ,[Tag]
                                                      ,[Note]
                                                      ,[TagPrimary]
                                                      ,[Price]
                                                      ,[DisplayStyle]
                                                      ,[DisplayPosition]
                                                      ,[DisplayInSlide]
                                                      ,[AvatarCustom]
                                                      ,[OriginalId]
                                                      ,[NewsType]
                                                      ,[IsOnHome]
                                                      ,[Url]
                                                      ,[NoteRoyalties]
                                                      ,[TagItem]
                                                      ,[NewsCategory]
                                                      ,[InitSapo]
                                                      ,[TemplateName]
                                                      ,[TemplateConfig]
                                                      ,[InterviewId]
                                                      ,[IsBreakingNews]
                                                      ,[OriginalUrl]
                                                      ,[IsPr]
                                                      ,[AdStore]
                                                      ,[AdStoreUrl]
                                                      ,[PrBookingNumber]
                                                      ,[PegaBreakingNews]
                                                      ,[RollingNewsId]
                                                      ,[IsOnMobile]
                                                      ,[TagSubTitleId]
                                                      ,[PrPosition]
                                                      ,[LocationType]
                                                      ,[ExpiredDate]
                                                      ,[SourceUrl]
                                                      ,[BonusPrice]
                                                      ,[ViewCountMobile]
                                                      ,[PhotoCount]
                                                      ,[VideoCount]
                                                      ,[ShortTitle]
                                                      ,[ParentNewsId]
                                                      ,[WarningLevel]
                                                ) VALUES(@Id
                                                      ,@Title
                                                      ,@SubTitle
                                                      ,@Sapo
                                                      ,@Body
                                                      ,@Avatar
                                                      ,@AvatarDesc
                                                      ,@Avatar2
                                                      ,@Avatar3
                                                      ,@Avatar4
                                                      ,@Avatar5
                                                      ,@Author
                                                      ,@NewsRelation
                                                      ,@Status
                                                      ,@Source
                                                      ,@IsFocus
                                                      ,@Type
                                                      ,@ThreadId
                                                      ,@CreatedDate
                                                      ,@LastModifiedDate
                                                      ,@DistributionDate
                                                      ,@CreatedBy
                                                      ,@LastModifiedBy
                                                      ,@PublishedBy
                                                      ,@EditedBy
                                                      ,@LastReceiver
                                                      ,@ApprovedBy
                                                      ,@WordCount
                                                      ,@ViewCount
                                                      ,@Priority
                                                      ,@Tag
                                                      ,@Note
                                                      ,@TagPrimary
                                                      ,@Price
                                                      ,@DisplayStyle
                                                      ,@DisplayPosition
                                                      ,@DisplayInSlide
                                                      ,@AvatarCustom
                                                      ,@OriginalId
                                                      ,@NewsType
                                                      ,@IsOnHome
                                                      ,@Url
                                                      ,@NoteRoyalties
                                                      ,@TagItem
                                                      ,@NewsCategory
                                                      ,@InitSapo
                                                      ,@TemplateName
                                                      ,@TemplateConfig
                                                      ,@InterviewId
                                                      ,@IsBreakingNews
                                                      ,@OriginalUrl
                                                      ,@IsPr
                                                      ,@AdStore
                                                      ,@AdStoreUrl
                                                      ,@PrBookingNumber
                                                      ,@PegaBreakingNews
                                                      ,@RollingNewsId
                                                      ,@IsOnMobile
                                                      ,@TagSubTitleId
                                                      ,@PrPosition
                                                      ,@LocationType
                                                      ,@ExpiredDate
                                                      ,@SourceUrl
                                                      ,@BonusPrice
                                                      ,@ViewCountMobile
                                                      ,@PhotoCount
                                                      ,@VideoCount
                                                      ,@ShortTitle
                                                      ,@ParentNewsId
                                                      ,@WarningLevel);";

                var cmd = new SqlCommand(query, con);
                cmd.CommandType = CommandType.Text;

                cmd.Parameters.AddWithValue("@Id", item.Id);
                cmd.Parameters.AddWithValue("@Title", item.Title ?? (object)DBNull.Value);
                cmd.Parameters.AddWithValue("@SubTitle", item.SubTitle ?? (object)DBNull.Value);
                cmd.Parameters.AddWithValue("@Sapo", item.Sapo ?? (object)DBNull.Value);
                cmd.Parameters.AddWithValue("@Body", item.Body ?? (object)DBNull.Value);
                cmd.Parameters.AddWithValue("@Avatar", item.Avatar ?? (object)DBNull.Value);
                cmd.Parameters.AddWithValue("@AvatarDesc", item.AvatarDesc ?? (object)DBNull.Value);
                cmd.Parameters.AddWithValue("@Avatar2", item.Avatar2 ?? (object)DBNull.Value);
                cmd.Parameters.AddWithValue("@Avatar3", item.Avatar3 ?? (object)DBNull.Value);
                cmd.Parameters.AddWithValue("@Avatar4", item.Avatar4 ?? (object)DBNull.Value);
                cmd.Parameters.AddWithValue("@Avatar5", item.Avatar5 ?? (object)DBNull.Value);
                cmd.Parameters.AddWithValue("@Author", item.Author ?? (object)DBNull.Value);
                cmd.Parameters.AddWithValue("@NewsRelation", item.NewsRelation ?? (object)DBNull.Value);
                cmd.Parameters.AddWithValue("@Status", item.Status);
                cmd.Parameters.AddWithValue("@Source", item.Source ?? (object)DBNull.Value);
                cmd.Parameters.AddWithValue("@IsFocus", item.IsFocus);

                if (item.Type <= 0)
                    cmd.Parameters.AddWithValue("@Type", 0);
                else
                    cmd.Parameters.AddWithValue("@Type", item.Type);

                cmd.Parameters.AddWithValue("@ThreadId", item.ThreadId);

                if (item.CreatedDate <= new DateTime(1980, 1, 1))
                    cmd.Parameters.AddWithValue("@CreatedDate", DBNull.Value);
                else
                    cmd.Parameters.AddWithValue("@CreatedDate", item.CreatedDate);

                if (item.LastModifiedDate <= new DateTime(1980, 1, 1))
                    cmd.Parameters.AddWithValue("@LastModifiedDate", DBNull.Value);
                else
                    cmd.Parameters.AddWithValue("@LastModifiedDate", item.LastModifiedDate);

                if (item.DistributionDate <= new DateTime(1980, 1, 1))
                    cmd.Parameters.AddWithValue("@DistributionDate", DBNull.Value);
                else
                    cmd.Parameters.AddWithValue("@DistributionDate", item.DistributionDate);

                cmd.Parameters.AddWithValue("@CreatedBy", item.CreatedBy ?? (object)DBNull.Value);
                cmd.Parameters.AddWithValue("@LastModifiedBy", item.LastModifiedBy ?? (object)DBNull.Value);
                cmd.Parameters.AddWithValue("@PublishedBy", item.PublishedBy ?? (object)DBNull.Value);
                cmd.Parameters.AddWithValue("@EditedBy", item.EditedBy ?? (object)DBNull.Value);
                cmd.Parameters.AddWithValue("@LastReceiver", item.LastReceiver ?? (object)DBNull.Value);

                if (item.Status == 6 && string.IsNullOrEmpty(item.ApprovedBy))
                    cmd.Parameters.AddWithValue("@ApprovedBy", item.PublishedBy ?? (object)DBNull.Value);
                else
                    cmd.Parameters.AddWithValue("@ApprovedBy", item.ApprovedBy ?? (object)DBNull.Value);

                if (item.WordCount <= 0)
                    cmd.Parameters.AddWithValue("@WordCount", 0);
                else
                    cmd.Parameters.AddWithValue("@WordCount", item.WordCount);

                cmd.Parameters.AddWithValue("@ViewCount", item.ViewCount);

                if (item.Priority <= 0)
                    cmd.Parameters.AddWithValue("@Priority", 0);
                else
                    cmd.Parameters.AddWithValue("@Priority", item.Priority);

                cmd.Parameters.AddWithValue("@Tag", item.Tag ?? (object)DBNull.Value);
                cmd.Parameters.AddWithValue("@Note", item.Note ?? (object)DBNull.Value);
                cmd.Parameters.AddWithValue("@TagPrimary", item.TagPrimary);

                if (item.Price < 0)
                    cmd.Parameters.AddWithValue("@Price", DBNull.Value);
                else
                    cmd.Parameters.AddWithValue("@Price", item.Price);

                cmd.Parameters.AddWithValue("@DisplayStyle", item.DisplayStyle);
                cmd.Parameters.AddWithValue("@DisplayPosition", item.DisplayPosition);
                cmd.Parameters.AddWithValue("@DisplayInSlide", item.DisplayInSlide);
                cmd.Parameters.AddWithValue("@AvatarCustom", item.AvatarCustom ?? (object)DBNull.Value);
                cmd.Parameters.AddWithValue("@OriginalId", item.OriginalId);

                if (item.NewsType <= 0)
                    cmd.Parameters.AddWithValue("@NewsType", 0);
                else
                    cmd.Parameters.AddWithValue("@NewsType", item.NewsType);

                cmd.Parameters.AddWithValue("@IsOnHome", item.IsOnHome);
                cmd.Parameters.AddWithValue("@Url", item.Url ?? (object)DBNull.Value);
                cmd.Parameters.AddWithValue("@NoteRoyalties", item.NoteRoyalties ?? (object)DBNull.Value);
                cmd.Parameters.AddWithValue("@TagItem", item.TagItem ?? (object)DBNull.Value);
                cmd.Parameters.AddWithValue("@NewsCategory", item.NewsCategory);
                cmd.Parameters.AddWithValue("@InitSapo", item.InitSapo ?? (object)DBNull.Value);
                cmd.Parameters.AddWithValue("@TemplateName", item.TemplateName ?? (object)DBNull.Value);
                cmd.Parameters.AddWithValue("@TemplateConfig", item.TemplateConfig ?? (object)DBNull.Value);
                cmd.Parameters.AddWithValue("@InterviewId", item.InterviewId);
                cmd.Parameters.AddWithValue("@IsBreakingNews", item.IsBreakingNews);
                cmd.Parameters.AddWithValue("@OriginalUrl", item.OriginalUrl ?? (object)DBNull.Value);
                cmd.Parameters.AddWithValue("@IsPr", item.IsPr);
                cmd.Parameters.AddWithValue("@AdStore", item.AdStore);
                cmd.Parameters.AddWithValue("@AdStoreUrl", item.AdStoreUrl ?? (object)DBNull.Value);
                cmd.Parameters.AddWithValue("@PrBookingNumber", item.PrBookingNumber ?? (object)DBNull.Value);
                cmd.Parameters.AddWithValue("@PegaBreakingNews", item.PegaBreakingNews);
                cmd.Parameters.AddWithValue("@RollingNewsId", item.RollingNewsId);
                cmd.Parameters.AddWithValue("@IsOnMobile", item.IsOnMobile);
                cmd.Parameters.AddWithValue("@TagSubTitleId", item.TagSubTitleId);
                cmd.Parameters.AddWithValue("@PrPosition", item.PrPosition);
                cmd.Parameters.AddWithValue("@LocationType", item.LocationType);

                if (item.ExpiredDate <= new DateTime(1980, 1, 1))
                    cmd.Parameters.AddWithValue("@ExpiredDate", DBNull.Value);
                else
                    cmd.Parameters.AddWithValue("@ExpiredDate", item.ExpiredDate);

                cmd.Parameters.AddWithValue("@SourceUrl", item.SourceUrl ?? (object)DBNull.Value);
                cmd.Parameters.AddWithValue("@BonusPrice", item.BonusPrice);
                cmd.Parameters.AddWithValue("@ViewCountMobile", item.ViewCountMobile);
                cmd.Parameters.AddWithValue("@PhotoCount", item.PhotoCount);
                cmd.Parameters.AddWithValue("@VideoCount", item.VideoCount);
                cmd.Parameters.AddWithValue("@ShortTitle", item.ShortTitle ?? (object)DBNull.Value);
                cmd.Parameters.AddWithValue("@ParentNewsId", item.ParentNewsId);
                cmd.Parameters.AddWithValue("@WarningLevel", item.WarningLevel);

                con.Open();
                var reader = cmd.ExecuteNonQuery();

                con.Close();

                return reader > 0;
            }
            catch (Exception ex)
            {
                Logger.Log(TypeLog.Error, "SyncNews => " + ex.Message);
                return false;
            }
            finally
            {
                con.Close();
                con.Dispose();
            }
        }

        #region ES

        private static void SyncNewsEsAndReDist(string connectStr, NewsEntity s, string nameSpace, string connectEs, string connectRedis)
        {
            var listZone = GetZoneByNewsId(connectStr, s.Id);
            var zoneIds = new string[] { };
            if (listZone != null && listZone.Count > 0)
            {
                zoneIds = listZone.Select(z => z.Id.ToString()).ToArray();
                s.ZoneName = listZone.Where(w => w.IsPrimary == true).Select(z => z.Name).FirstOrDefault();
            }

            var obj = new NewsSearchEntity
            {
                Id = s.Id,
                EncryptId = s.Id.ToString(),
                Title = s.Title,
                ZoneIds = zoneIds,
                Type = s.Type,
                CreatedBy = s.CreatedBy,
                LastModifiedBy = s.LastModifiedBy,
                EditedBy = s.EditedBy,
                PublishedBy = s.PublishedBy,
                LastReceiver = s.LastReceiver,
                ApprovedBy = s.ApprovedBy,
                ReturnedBy = s.ReturnedBy,
                CreatedDate = s.CreatedDate,
                DistributionDate = s.DistributionDate,
                LastModifiedDate = s.LastModifiedDate,
                ApprovedDate = s.ApprovedDate,
                ViewCount = s.ViewCount,
                Status = s.Status,
                IsOnHome = s.IsOnHome,
                Avatar = s.Avatar,
                ViewRoyalties = 0,
                ZoneName = s.ZoneName,
                Note = s.Note,
                Author = s.Author,
                ErrorCheckedBy = s.ErrorCheckedBy,
                ErrorCheckedDate = s.ErrorCheckedDate,
                SensitiveCheckedBy = s.SensitiveCheckedBy,
                SensitiveCheckedDate = s.SensitiveCheckedDate,
                WordCount = s.WordCount,
                Sapo = s.Sapo,
                Url = s.Url,
                DisplayPosition = s.DisplayPosition,
                Priority=s.Priority
            };
            //if (s.Status == 6)
            //{
            //    obj.ApprovedBy = s.PublishedBy;
            //}

            EsNewsBo.Create(nameSpace, connectEs, obj);

            RedisNewsBo.AddNews(nameSpace, connectRedis, s);
        }

        #endregion

        #region NewsInZone
        private static void SyncNewsInZone(string connectStr1, string connectStr2, long newsId)
        {
            var con = GetConnection(connectStr1);
            try
            {
                string query = @"SELECT n.* FROM NewsInZone n                                
                                WHERE n.NewsId=@NewsId";

                using (var cmd = new SqlCommand(query, con))
                {

                    cmd.Parameters.AddWithValue("@NewsId", newsId);

                    con.Open();
                    var reader = cmd.ExecuteReader();

                    var data = EntityMapper.FillCollection<NewsInZoneEntity>(reader);

                    con.Close();

                    InitNewsInZoneToDb(connectStr2, data);
                }
            }
            catch (Exception ex)
            {
                Logger.Log(TypeLog.Error, "SyncNewsInZone => " + ex.Message);
            }
            finally
            {
                con.Close();
                con.Dispose();
            }
        }
        private static void InitNewsInZoneToDb(string connectStr, List<NewsInZoneEntity> data)
        {
            var con = GetConnection(connectStr);
            con.Open();

            try
            {
                foreach (var item in data)
                {
                    string query = @"DELETE [NewsInZone] WHERE NewsId=@NewsId and ZoneId=@ZoneId and IsPrimary=@IsPrimary
                                    INSERT INTO [NewsInZone]([NewsId]
                                                            ,[ZoneId]
                                                            ,[IsPrimary]
                                                            ,[DistributionID]
                                                            ,[RootZoneId]
                                                ) VALUES(@NewsId
                                                        ,@ZoneId
                                                        ,@IsPrimary
                                                        ,@DistributionID
                                                        ,@RootZoneId)";

                    using (var cmd = new SqlCommand(query, con))
                    {
                        cmd.CommandType = CommandType.Text;

                        cmd.Parameters.AddWithValue("@NewsId", item.NewsId);
                        cmd.Parameters.AddWithValue("@ZoneId", item.ZoneId);
                        cmd.Parameters.AddWithValue("@IsPrimary", item.IsPrimary);

                        if (item.DistributionID < 0)
                            cmd.Parameters.AddWithValue("@DistributionID", DBNull.Value);
                        else
                            cmd.Parameters.AddWithValue("@DistributionID", item.DistributionID);

                        cmd.Parameters.AddWithValue("@RootZoneId", item.RootZoneId);

                        var reader = cmd.ExecuteNonQuery();
                    }
                }

                con.Close();
            }
            catch (Exception ex)
            {
                Logger.Log(TypeLog.Error, "InitNewsInZoneToDb => " + ex.Message);
            }
            finally
            {
                con.Close();
                con.Dispose();
            }
        }
        #endregion

        #region NewsExtension
        private static void SyncNewsExtension(string connectStr1, string connectStr2, long newsId)
        {
            var con = GetConnection(connectStr1);
            try
            {
                string query = @"SELECT n.* FROM NewsExtension n                                
                                WHERE n.NewsId=@NewsId";

                using (var cmd = new SqlCommand(query, con))
                {

                    cmd.Parameters.AddWithValue("@NewsId", newsId);

                    con.Open();
                    var reader = cmd.ExecuteReader();

                    var data = EntityMapper.FillCollection<NewsExtensionEntity>(reader);

                    con.Close();

                    InitNewsExtensionToDb(connectStr2, data);
                }
            }
            catch (Exception ex)
            {
                Logger.Log(TypeLog.Error, "SyncNewsExtension => " + ex.Message);
            }
            finally
            {
                con.Close();
            }
        }
        private static void InitNewsExtensionToDb(string connectStr, List<NewsExtensionEntity> data)
        {
            var con = GetConnection(connectStr);
            con.Open();

            try
            {
                foreach (var item in data)
                {
                    string query = @"DELETE [NewsExtension] WHERE NewsId=@NewsId and [Type]=@Type
                                    INSERT INTO [NewsExtension]([NewsId]
                                                                ,[Type]
                                                                ,[Value]
                                                ) VALUES(@NewsId
                                                        ,@Type
                                                        ,@Value)";

                    using (var cmd = new SqlCommand(query, con))
                    {
                        cmd.CommandType = CommandType.Text;

                        cmd.Parameters.AddWithValue("@NewsId", item.NewsId);
                        cmd.Parameters.AddWithValue("@Type", item.Type);
                        cmd.Parameters.AddWithValue("@Value", item.Value ?? (object)DBNull.Value);

                        var reader = cmd.ExecuteNonQuery();
                    }
                }

                con.Close();
            }
            catch (Exception ex)
            {
                Logger.Log(TypeLog.Error, "InitNewsExtensionToDb => " + ex.Message);
            }
            finally
            {
                con.Close();
                con.Dispose();
            }
        }
        #endregion

        #region TagNews
        private static void SyncTagNews(string connectStr1, string connectStr2, long newsId)
        {
            var con = GetConnection(connectStr1);
            try
            {
                string query = @"SELECT n.* FROM TagNews n                                
                                WHERE n.NewsId=@NewsId";

                using (var cmd = new SqlCommand(query, con))
                {

                    cmd.Parameters.AddWithValue("@NewsId", newsId);

                    con.Open();
                    var reader = cmd.ExecuteReader();

                    var data = EntityMapper.FillCollection<TagNewsEntity>(reader);

                    con.Close();

                    InitTagNewsToDb(connectStr2, data);
                }
            }
            catch (Exception ex)
            {
                Logger.Log(TypeLog.Error, "SyncTagNews => " + ex.Message);
            }
            finally
            {
                con.Close();
                con.Dispose();
            }
        }
        private static void InitTagNewsToDb(string connectStr, List<TagNewsEntity> data)
        {
            var con = GetConnection(connectStr);
            con.Open();

            try
            {
                foreach (var item in data)
                {
                    string query = @"DELETE [TagNews] WHERE NewsID=@NewsId and TagID=@TagID
                                    INSERT INTO [TagNews]([TagID]
                                                              ,[NewsID]
                                                              ,[TagMode]
                                                              ,[TagProperty]
                                                              ,[Priority]
                                                ) VALUES(@TagID
                                                        ,@NewsId
                                                        ,@TagMode
                                                        ,@TagProperty
                                                        ,@Priority)";

                    using (var cmd = new SqlCommand(query, con))
                    {
                        cmd.CommandType = CommandType.Text;

                        cmd.Parameters.AddWithValue("@TagID", item.TagID);
                        cmd.Parameters.AddWithValue("@NewsId", item.NewsID);

                        if (item.TagMode <= 0)
                            cmd.Parameters.AddWithValue("@TagMode", 0);
                        else
                            cmd.Parameters.AddWithValue("@TagMode", item.TagMode);

                        cmd.Parameters.AddWithValue("@TagProperty", item.TagProperty ?? (object)DBNull.Value);
                        cmd.Parameters.AddWithValue("@Priority", item.Priority);

                        var reader = cmd.ExecuteNonQuery();
                    }
                }

                con.Close();
            }
            catch (Exception ex)
            {
                Logger.Log(TypeLog.Error, "InitTagNewsToDb => " + ex.Message);
            }
            finally
            {
                con.Close();
                con.Dispose();
            }
        }
        #endregion

        #region ThreadNews
        private static void SyncThreadNews(string connectStr1, string connectStr2, long newsId)
        {
            var con = GetConnection(connectStr1);
            try
            {
                string query = @"SELECT n.* FROM ThreadNews n                                
                                WHERE n.NewsId=@NewsId";

                using (var cmd = new SqlCommand(query, con))
                {

                    cmd.Parameters.AddWithValue("@NewsId", newsId);

                    con.Open();
                    var reader = cmd.ExecuteReader();

                    var data = EntityMapper.FillCollection<ThreadNewsEntity>(reader);

                    con.Close();

                    InitThreadNewsToDb(connectStr2, data);
                }
            }
            catch (Exception ex)
            {
                Logger.Log(TypeLog.Error, "SyncThreadNews => " + ex.Message);
            }
            finally
            {
                con.Close();
                con.Dispose();
            }
        }
        private static void InitThreadNewsToDb(string connectStr, List<ThreadNewsEntity> data)
        {
            var con = GetConnection(connectStr);
            con.Open();

            try
            {
                foreach (var item in data)
                {
                    string query = @"DELETE [ThreadNews] WHERE NewsId=@NewsId and ThreadId=@ThreadId
                                    INSERT INTO [ThreadNews]([ThreadId]
                                                              ,[NewsId]
                                                              ,[Ordinary]                                                             
                                                              ,[LastModifiedDate]
                                                ) VALUES(@ThreadId
                                                        ,@NewsId                                                        
                                                        ,@Ordinary
                                                        ,@LastModifiedDate)";

                    using (var cmd = new SqlCommand(query, con))
                    {
                        cmd.CommandType = CommandType.Text;

                        cmd.Parameters.AddWithValue("@ThreadId", item.ThreadId);
                        cmd.Parameters.AddWithValue("@NewsId", item.NewsId);
                        cmd.Parameters.AddWithValue("@Ordinary", item.Ordinary);

                        if (item.LastModifiedDate <= new DateTime(1980, 1, 1))
                            cmd.Parameters.AddWithValue("@LastModifiedDate", DBNull.Value);
                        else
                            cmd.Parameters.AddWithValue("@LastModifiedDate", item.LastModifiedDate);

                        var reader = cmd.ExecuteNonQuery();
                    }
                }

                con.Close();
            }
            catch (Exception ex)
            {
                Logger.Log(TypeLog.Error, "InitThreadNewsToDb => " + ex.Message);
            }
            finally
            {
                con.Close();
                con.Dispose();
            }
        }
        #endregion

        #region NewsRelation
        private static void SyncNewsRelation(string connectStr1, string connectStr2, long newsId)
        {
            var con = GetConnection(connectStr1);
            try
            {
                string query = @"SELECT n.* FROM NewsRelation n                                
                                WHERE n.NewsId=@NewsId";

                using (var cmd = new SqlCommand(query, con))
                {

                    cmd.Parameters.AddWithValue("@NewsId", newsId);

                    con.Open();
                    var reader = cmd.ExecuteReader();

                    var data = EntityMapper.FillCollection<NewsRelationEntity>(reader);

                    con.Close();

                    InitNewsRelationToDb(connectStr2, data);
                }
            }
            catch (Exception ex)
            {
                con.Close();
                Logger.Log(TypeLog.Error, "SyncNewsRelation => " + ex.Message);
            }
            finally
            {
                con.Close();
                con.Dispose();
            }
        }
        private static void InitNewsRelationToDb(string connectStr, List<NewsRelationEntity> data)
        {
            var con = GetConnection(connectStr);
            con.Open();

            try
            {
                foreach (var item in data)
                {
                    string query = @"DELETE [NewsRelation] WHERE NewsId=@NewsId and NewsRelationId=@NewsRelationId and ZoneId=@ZoneId
                                    INSERT INTO [NewsRelation]([NewsId]
                                                              ,[NewsRelationId]
                                                              ,[ZoneId]
                                                              ,[IsChange]
                                                              ,[Priority]                                                             
                                                              ,[Type]
                                                ) VALUES(@NewsId
                                                        ,@NewsRelationId                                                        
                                                        ,@ZoneId
                                                        ,@IsChange
                                                        ,@Priority
                                                        ,@Type)";

                    using (var cmd = new SqlCommand(query, con))
                    {
                        cmd.CommandType = CommandType.Text;

                        cmd.Parameters.AddWithValue("@NewsID", item.NewsId);
                        cmd.Parameters.AddWithValue("@NewsRelationId", item.NewsRelationId);
                        cmd.Parameters.AddWithValue("@ZoneId", item.ZoneId);
                        cmd.Parameters.AddWithValue("@IsChange", item.IsChange);
                        cmd.Parameters.AddWithValue("@Priority", item.Priority);
                        cmd.Parameters.AddWithValue("@Type", item.Type);

                        var reader = cmd.ExecuteNonQuery();
                    }
                }

                con.Close();
            }
            catch (Exception ex)
            {
                Logger.Log(TypeLog.Error, "InitNewsRelationToDb => " + ex.Message);
            }
            finally
            {
                con.Close();
                con.Dispose();
            }
        }
        #endregion

        #region NewsPublish
        private static void SyncNewsPublish(string connectStr1, string connectStr2, NewsEntity item)
        {
            if (item.Status == 8)
            {
                var con = GetConnection(connectStr1);
                try
                {
                    string query = @"SELECT n.* FROM NewsPublish n                                
                                WHERE n.NewsId=@NewsId";

                    using (var cmd = new SqlCommand(query, con))
                    {

                        cmd.Parameters.AddWithValue("@NewsId", item.Id);

                        con.Open();
                        var reader = cmd.ExecuteReader();

                        var data = EntityMapper.FillCollection<NewsPublishEntity>(reader);

                        con.Close();

                        InitNewsPublishToDb(connectStr2, data);
                    }
                }
                catch (Exception ex)
                {
                    Logger.Log(TypeLog.Error, "SyncNewsPublish => " + ex.Message);
                }
                finally
                {
                    con.Close();
                    con.Dispose();
                }
            }
        }
        private static void InitNewsPublishToDb(string connectStr, List<NewsPublishEntity> data)
        {
            var con = GetConnection(connectStr);
            con.Open();

            try
            {
                foreach (var item in data)
                {
                    string query = @"DELETE [NewsPublish] WHERE NewsId=@NewsId and ZoneId=@ZoneId 
                                    INSERT INTO [NewsPublish]([NewsId]
                                                              ,[ZoneId]
                                                              ,[Title]
                                                              ,[SubTitle]
                                                              ,[Sapo]
                                                              ,[Avatar]
                                                              ,[AvatarDesc]
                                                              ,[Avatar1]
                                                              ,[Avatar2]
                                                              ,[Avatar3]
                                                              ,[Avatar4]
                                                              ,[Avatar5]
                                                              ,[Author]
                                                              ,[NewsRelation]
                                                              ,[Source]
                                                              ,[IsFocus]
                                                              ,[Type]
                                                              ,[ThreadId]
                                                              ,[DistributionDate]
                                                              ,[Url]
                                                              ,[DisplayStyle]
                                                              ,[DisplayPosition]
                                                              ,[DisplayInSlide]
                                                              ,[AvatarCustom]
                                                              ,[OriginalId]
                                                              ,[PublishedDate]
                                                              ,[IsOnHome]
                                                              ,[TagItem]
                                                              ,[IsPrimary]
                                                              ,[InitSapo]
                                                              ,[NewsType]
                                                              ,[InterviewId]
                                                              ,[IsBreakingNews]
                                                              ,[OriginalUrl]
                                                              ,[IsPr]
                                                              ,[AdStore]
                                                              ,[AdStoreUrl]
                                                              ,[LastModifiedDate]
                                                              ,[RollingNewsId]
                                                              ,[IsOnMobile]
                                                              ,[TagSubTitleId]
                                                              ,[Position]
                                                              ,[PrPosition]
                                                              ,[Priority]
                                                              ,[LastModifiedDateStamp]
                                                              ,[PublishedDateStamp]
                                                              ,[LocationType]
                                                              ,[ExpiredDate]
                                                              ,[DistributionID]
                                                              ,[PrimaryZoneId]
                                                              ,[TitleDetail]
                                                              ,[ParentNewsId]
                                                ) VALUES(@NewsId
                                                        ,@ZoneId
                                                        ,@Title
                                                        ,@SubTitle
                                                        ,@Sapo
                                                        ,@Avatar
                                                        ,@AvatarDesc
                                                        ,@Avatar1
                                                        ,@Avatar2
                                                        ,@Avatar3
                                                        ,@Avatar4
                                                        ,@Avatar5
                                                        ,@Author
                                                        ,@NewsRelation
                                                        ,@Source
                                                        ,@IsFocus
                                                        ,@Type
                                                        ,@ThreadId
                                                        ,@DistributionDate
                                                        ,@Url
                                                        ,@DisplayStyle
                                                        ,@DisplayPosition
                                                        ,@DisplayInSlide
                                                        ,@AvatarCustom
                                                        ,@OriginalId
                                                        ,@PublishedDate
                                                        ,@IsOnHome
                                                        ,@TagItem
                                                        ,@IsPrimary
                                                        ,@InitSapo
                                                        ,@NewsType
                                                        ,@InterviewId
                                                        ,@IsBreakingNews
                                                        ,@OriginalUrl
                                                        ,@IsPr
                                                        ,@AdStore
                                                        ,@AdStoreUrl
                                                        ,@LastModifiedDate
                                                        ,@RollingNewsId
                                                        ,@IsOnMobile
                                                        ,@TagSubTitleId
                                                        ,@Position
                                                        ,@PrPosition
                                                        ,@Priority
                                                        ,@LastModifiedDateStamp
                                                        ,@PublishedDateStamp
                                                        ,@LocationType
                                                        ,@ExpiredDate
                                                        ,@DistributionID
                                                        ,@PrimaryZoneId
                                                        ,@TitleDetail
                                                        ,@ParentNewsId)";

                    using (var cmd = new SqlCommand(query, con))
                    {
                        cmd.CommandType = CommandType.Text;

                        cmd.Parameters.AddWithValue("@NewsId", item.NewsId);
                        cmd.Parameters.AddWithValue("@ZoneId", item.ZoneId);
                        cmd.Parameters.AddWithValue("@Title", item.Title ?? (object)DBNull.Value);
                        cmd.Parameters.AddWithValue("@SubTitle", item.SubTitle ?? (object)DBNull.Value);
                        cmd.Parameters.AddWithValue("@Sapo", item.Sapo ?? (object)DBNull.Value);
                        cmd.Parameters.AddWithValue("@Avatar", item.Avatar ?? (object)DBNull.Value);
                        cmd.Parameters.AddWithValue("@AvatarDesc", item.AvatarDesc ?? (object)DBNull.Value);
                        cmd.Parameters.AddWithValue("@Avatar1", item.Avatar1 ?? (object)DBNull.Value);
                        cmd.Parameters.AddWithValue("@Avatar2", item.Avatar2 ?? (object)DBNull.Value);
                        cmd.Parameters.AddWithValue("@Avatar3", item.Avatar3 ?? (object)DBNull.Value);
                        cmd.Parameters.AddWithValue("@Avatar4", item.Avatar4 ?? (object)DBNull.Value);
                        cmd.Parameters.AddWithValue("@Avatar5", item.Avatar5 ?? (object)DBNull.Value);
                        cmd.Parameters.AddWithValue("@Author", item.Author ?? (object)DBNull.Value);
                        cmd.Parameters.AddWithValue("@NewsRelation", item.NewsRelation ?? (object)DBNull.Value);
                        cmd.Parameters.AddWithValue("@Source", item.Source ?? (object)DBNull.Value);
                        cmd.Parameters.AddWithValue("@IsFocus", item.IsFocus);

                        if (item.Type <= 0)
                            cmd.Parameters.AddWithValue("@Type", 0);
                        else
                            cmd.Parameters.AddWithValue("@Type", item.Type);

                        cmd.Parameters.AddWithValue("@ThreadId", item.ThreadId);

                        if (item.DistributionDate <= new DateTime(1980, 1, 1))
                            cmd.Parameters.AddWithValue("@DistributionDate", DBNull.Value);
                        else
                            cmd.Parameters.AddWithValue("@DistributionDate", item.DistributionDate);

                        cmd.Parameters.AddWithValue("@Url", item.Url ?? (object)DBNull.Value);
                        cmd.Parameters.AddWithValue("@DisplayStyle", item.DisplayStyle);
                        cmd.Parameters.AddWithValue("@DisplayPosition", item.DisplayPosition);
                        cmd.Parameters.AddWithValue("@DisplayInSlide", item.DisplayInSlide);
                        cmd.Parameters.AddWithValue("@AvatarCustom", item.AvatarCustom ?? (object)DBNull.Value);
                        cmd.Parameters.AddWithValue("@OriginalId", item.OriginalId);
                        cmd.Parameters.AddWithValue("@PublishedDate", item.PublishedDate);
                        cmd.Parameters.AddWithValue("@IsOnHome", item.IsOnHome);
                        cmd.Parameters.AddWithValue("@TagItem", item.TagItem ?? (object)DBNull.Value);
                        cmd.Parameters.AddWithValue("@IsPrimary", item.IsPrimary);
                        cmd.Parameters.AddWithValue("@InitSapo", item.InitSapo ?? (object)DBNull.Value);

                        if (item.NewsType <= 0)
                            cmd.Parameters.AddWithValue("@NewsType", 0);
                        else
                            cmd.Parameters.AddWithValue("@NewsType", item.NewsType);

                        cmd.Parameters.AddWithValue("@InterviewId", item.InterviewId);
                        cmd.Parameters.AddWithValue("@IsBreakingNews", item.IsBreakingNews);
                        cmd.Parameters.AddWithValue("@OriginalUrl", item.OriginalUrl ?? (object)DBNull.Value);
                        cmd.Parameters.AddWithValue("@IsPr", item.IsPr);
                        cmd.Parameters.AddWithValue("@AdStore", item.AdStore);
                        cmd.Parameters.AddWithValue("@AdStoreUrl", item.AdStoreUrl ?? (object)DBNull.Value);

                        if (item.LastModifiedDate <= new DateTime(1980, 1, 1))
                            cmd.Parameters.AddWithValue("@LastModifiedDate", DBNull.Value);
                        else
                            cmd.Parameters.AddWithValue("@LastModifiedDate", item.LastModifiedDate);

                        cmd.Parameters.AddWithValue("@RollingNewsId", item.RollingNewsId);
                        cmd.Parameters.AddWithValue("@IsOnMobile", item.IsOnMobile);
                        cmd.Parameters.AddWithValue("@TagSubTitleId", item.TagSubTitleId);
                        cmd.Parameters.AddWithValue("@Position", item.Position);
                        cmd.Parameters.AddWithValue("@PrPosition", item.PrPosition);
                        cmd.Parameters.AddWithValue("@Priority", item.Priority);
                        cmd.Parameters.AddWithValue("@LastModifiedDateStamp", item.LastModifiedDateStamp);
                        cmd.Parameters.AddWithValue("@PublishedDateStamp", item.PublishedDateStamp);
                        cmd.Parameters.AddWithValue("@LocationType", item.LocationType);

                        if (item.ExpiredDate <= new DateTime(1980, 1, 1))
                            cmd.Parameters.AddWithValue("@ExpiredDate", DBNull.Value);
                        else
                            cmd.Parameters.AddWithValue("@ExpiredDate", item.ExpiredDate);

                        cmd.Parameters.AddWithValue("@DistributionID", item.DistributionID);
                        cmd.Parameters.AddWithValue("@PrimaryZoneId", item.PrimaryZoneId);
                        cmd.Parameters.AddWithValue("@TitleDetail", item.TitleDetail ?? (object)DBNull.Value);
                        cmd.Parameters.AddWithValue("@ParentNewsId", item.ParentNewsId);

                        var reader = cmd.ExecuteNonQuery();
                    }
                }

                con.Close();
            }
            catch (Exception ex)
            {
                Logger.Log(TypeLog.Error, "InitNewsPublishToDb => " + ex.Message);
            }
            finally
            {
                con.Close();
                con.Dispose();
            }
        }
        #endregion

        #region NewsContent
        private static void SyncNewsContent(string connectStr1, string connectStr2, NewsEntity item)
        {
            if (item.Status == 8)
            {
                var con = GetConnection(connectStr1);
                try
                {
                    string query = @"SELECT n.* FROM NewsContent n                                
                                WHERE n.NewsId=@NewsId";

                    using (var cmd = new SqlCommand(query, con))
                    {

                        cmd.Parameters.AddWithValue("@NewsId", item.Id);

                        con.Open();
                        var reader = cmd.ExecuteReader();

                        var data = EntityMapper.FillCollection<NewsContentEntity>(reader);

                        con.Close();

                        InitNewsContentToDb(connectStr2, data);
                    }
                }
                catch (Exception ex)
                {
                    Logger.Log(TypeLog.Error, "SyncNewsContent => " + ex.Message);
                }
                finally
                {
                    con.Close();
                    con.Dispose();
                }
            }
        }
        private static void InitNewsContentToDb(string connectStr, List<NewsContentEntity> data)
        {
            var con = GetConnection(connectStr);
            con.Open();

            try
            {
                foreach (var item in data)
                {
                    string query = @"DELETE [NewsContent] WHERE NewsId=@NewsId;
                                    INSERT INTO [NewsContent]([NewsId]
                                                              ,[Title]
                                                              ,[Sapo]
                                                              ,[Avatar]
                                                              ,[Avatar2]
                                                              ,[Avatar3]
                                                              ,[Avatar4]
                                                              ,[Avatar5]
                                                              ,[Body]
                                                              ,[PublishedDate]
                                                              ,[Source]
                                                              ,[NewsRelation]
                                                              ,[Tags]
                                                              ,[Author]
                                                              ,[TagPrimary]
                                                              ,[Url]
                                                              ,[ZoneId]
                                                              ,[OriginalId]
                                                              ,[TagItem]
                                                              ,[SubTitle]
                                                              ,[InitSapo]
                                                              ,[InterviewId]
                                                              ,[OriginalUrl]
                                                              ,[Type]
                                                              ,[AvatarDesc]
                                                              ,[NewsType]
                                                              ,[RollingNewsId]
                                                              ,[AdStore]
                                                              ,[AdStoreUrl]
                                                              ,[TagSubTitleId]
                                                              ,[ThreadId]
                                                              ,[Position]
                                                              ,[PrPosition]
                                                              ,[IsOnMobile]
                                                              ,[UseTemplate]
                                                              ,[LocationType]
                                                              ,[ExpiredDate]
                                                              ,[SourceUrl]
                                                              ,[ParentNewsId]
                                                ) VALUES(@NewsId
                                                          ,@Title
                                                          ,@Sapo
                                                          ,@Avatar
                                                          ,@Avatar2
                                                          ,@Avatar3
                                                          ,@Avatar4
                                                          ,@Avatar5
                                                          ,@Body
                                                          ,@PublishedDate
                                                          ,@Source
                                                          ,@NewsRelation
                                                          ,@Tags
                                                          ,@Author
                                                          ,@TagPrimary
                                                          ,@Url
                                                          ,@ZoneId
                                                          ,@OriginalId
                                                          ,@TagItem
                                                          ,@SubTitle
                                                          ,@InitSapo
                                                          ,@InterviewId
                                                          ,@OriginalUrl
                                                          ,@Type
                                                          ,@AvatarDesc
                                                          ,@NewsType
                                                          ,@RollingNewsId
                                                          ,@AdStore
                                                          ,@AdStoreUrl
                                                          ,@TagSubTitleId
                                                          ,@ThreadId
                                                          ,@Position
                                                          ,@PrPosition
                                                          ,@IsOnMobile
                                                          ,@UseTemplate
                                                          ,@LocationType
                                                          ,@ExpiredDate
                                                          ,@SourceUrl
                                                          ,@ParentNewsId);
                                                ";

                    using (var cmd = new SqlCommand(query, con))
                    {
                        cmd.CommandType = CommandType.Text;

                        cmd.Parameters.AddWithValue("@NewsId", item.NewsId);
                        cmd.Parameters.AddWithValue("@Title", item.Title ?? (object)DBNull.Value);
                        cmd.Parameters.AddWithValue("@Sapo", item.Sapo ?? (object)DBNull.Value);
                        cmd.Parameters.AddWithValue("@Avatar", item.Avatar ?? (object)DBNull.Value);
                        cmd.Parameters.AddWithValue("@Avatar2", item.Avatar2 ?? (object)DBNull.Value);
                        cmd.Parameters.AddWithValue("@Avatar3", item.Avatar3 ?? (object)DBNull.Value);
                        cmd.Parameters.AddWithValue("@Avatar4", item.Avatar4 ?? (object)DBNull.Value);
                        cmd.Parameters.AddWithValue("@Avatar5", item.Avatar5 ?? (object)DBNull.Value);
                        cmd.Parameters.AddWithValue("@Body", item.Body ?? (object)DBNull.Value);

                        if (item.PublishedDate <= new DateTime(1980, 1, 1))
                            cmd.Parameters.AddWithValue("@PublishedDate", DBNull.Value);
                        else
                            cmd.Parameters.AddWithValue("@PublishedDate", item.PublishedDate);

                        cmd.Parameters.AddWithValue("@Source", item.Source ?? (object)DBNull.Value);
                        cmd.Parameters.AddWithValue("@NewsRelation", item.NewsRelation ?? (object)DBNull.Value);
                        cmd.Parameters.AddWithValue("@Tags", item.Tags ?? (object)DBNull.Value);
                        cmd.Parameters.AddWithValue("@Author", item.Author ?? (object)DBNull.Value);
                        cmd.Parameters.AddWithValue("@TagPrimary", item.TagPrimary ?? (object)DBNull.Value);
                        cmd.Parameters.AddWithValue("@Url", item.Url ?? (object)DBNull.Value);
                        cmd.Parameters.AddWithValue("@ZoneId", item.ZoneId);
                        cmd.Parameters.AddWithValue("@OriginalId", item.OriginalId);
                        cmd.Parameters.AddWithValue("@TagItem", item.TagItem ?? (object)DBNull.Value);
                        cmd.Parameters.AddWithValue("@SubTitle", item.SubTitle ?? (object)DBNull.Value);
                        cmd.Parameters.AddWithValue("@InitSapo", item.InitSapo ?? (object)DBNull.Value);
                        cmd.Parameters.AddWithValue("@InterviewId", item.InterviewId);
                        cmd.Parameters.AddWithValue("@OriginalUrl", item.OriginalUrl ?? (object)DBNull.Value);

                        if (item.Type <= 0)
                            cmd.Parameters.AddWithValue("@Type", 0);
                        else
                            cmd.Parameters.AddWithValue("@Type", item.Type);

                        cmd.Parameters.AddWithValue("@AvatarDesc", item.AvatarDesc ?? (object)DBNull.Value);

                        if (item.NewsType <= 0)
                            cmd.Parameters.AddWithValue("@NewsType", 0);
                        else
                            cmd.Parameters.AddWithValue("@NewsType", item.NewsType);

                        cmd.Parameters.AddWithValue("@RollingNewsId", item.RollingNewsId);
                        cmd.Parameters.AddWithValue("@AdStore", item.AdStore);
                        cmd.Parameters.AddWithValue("@AdStoreUrl", item.AdStoreUrl ?? (object)DBNull.Value);
                        cmd.Parameters.AddWithValue("@TagSubTitleId", item.TagSubTitleId);
                        cmd.Parameters.AddWithValue("@ThreadId", item.ThreadId);
                        cmd.Parameters.AddWithValue("@Position", item.Position);
                        cmd.Parameters.AddWithValue("@PrPosition", item.PrPosition);
                        cmd.Parameters.AddWithValue("@IsOnMobile", item.IsOnMobile);
                        cmd.Parameters.AddWithValue("@UseTemplate", item.UseTemplate);
                        cmd.Parameters.AddWithValue("@LocationType", item.LocationType);

                        if (item.ExpiredDate <= new DateTime(1980, 1, 1))
                            cmd.Parameters.AddWithValue("@ExpiredDate", DBNull.Value);
                        else
                            cmd.Parameters.AddWithValue("@ExpiredDate", item.ExpiredDate);

                        cmd.Parameters.AddWithValue("@SourceUrl", item.SourceUrl ?? (object)DBNull.Value);
                        cmd.Parameters.AddWithValue("@ParentNewsId", item.ParentNewsId);

                        var reader = cmd.ExecuteNonQuery();
                    }
                }

                con.Close();
            }
            catch (Exception ex)
            {
                Logger.Log(TypeLog.Error, "InitNewsContentToDb => " + ex.Message);
            }
            finally
            {
                con.Close();
                con.Dispose();
            }
        }
        #endregion

        #region Chuẩn hóa theo note của Hữu
        private static bool UpdateDbChuanHoaNoteByHuu(string nameSpace, string connectStr2, long newsId)
        {
            var flg = false;
            switch (nameSpace)
            {
                case "AFamily":
                    flg = Afamily_ChuanHoa(connectStr2, newsId);
                    break;
            }
            return flg;            
        }

        private static bool Afamily_ChuanHoa(string connectStr2, long newsId)
        {            
            var con = GetConnection(connectStr2);
            try
            {
                string query = @"UPDATE NewsPublish set NewsType=0, [Type]=8 WHERE NewsType=4 and NewsId=@NewsId;
                                
                                UPDATE News set NewsType=0, [Type]=8 WHERE NewsType=4 and Id=@NewsId;
                                
                                UPDATE NewsPublish set NewsType=0, [Type]=18 WHERE NewsType=5 and NewsId=@NewsId;
                                
                                UPDATE News set NewsType=0, [Type]=18 WHERE NewsType=5 and Id=@NewsId;
                                
                                UPDATE NewsExtension set [Type]=3005 WHERE [Type]=8008 and NewsId=@NewsId;
                                
                                UPDATE NewsExtension set [Type]=3006 WHERE [Type]=8009 and NewsId=@NewsId;
                                
                                UPDATE NewsExtension set [Type]=3007 WHERE [Type]=8010 and NewsId=@NewsId;
                                
                                UPDATE News set Avatar5=Avatar3,Avatar3=Avatar5 WHERE [Type]<>27 and Avatar5 is not null and avatar5<>'' and Id=@NewsId;
                                
                                UPDATE News set Avatar5=Avatar3 WHERE [Type]<>27 and (Avatar5 is null or avatar5 ='') and Id=@NewsId;
                                
                                UPDATE News set Avatar3=Avatar4 WHERE [Type]=27 and Id=@NewsId;
                                
                                UPDATE News set Avatar4=NewsExtension.Value FROM News 
                                INNER JOIN NewsExtension ON News.Id=NewsExtension.NewsId 
                                WHERE NewsExtension.[Type]=3004 and News.[Type]=27 and News.Id=@NewsId;";

                using (var cmd = new SqlCommand(query, con))
                {

                    cmd.Parameters.AddWithValue("@NewsId", newsId);

                    con.Open();
                    var reader = cmd.ExecuteNonQuery();                        
                    con.Close();

                    return reader > 0;
                }
            }
            catch (Exception ex)
            {
                Logger.Log(TypeLog.Error, "UpdateDbChuanHoaNoteByHuu=> " + ex.Message);
                return false;
            }
            finally
            {
                con.Close();
                con.Dispose();
            }
            
        }
        #endregion

        #endregion

        #endregion
    }
}
