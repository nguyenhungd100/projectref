﻿using IMS2.SYNCDATA.Common;
using IMS2.SYNCDATA.Entity;
using IMS2.SYNCDATA.ES;
using IMS2.SYNCDATA.Redis;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Windows.Forms;

namespace IMS2.SYNCDATA.SQL
{
    public class TopicBo
    {
        public static SqlConnection GetConnection(string conn="")
        {
            if (string.IsNullOrEmpty(conn))
            {
                conn = "Data Source=172.16.41.125;Initial Catalog=IMS2_AFAMILY_DEV;Persist Security Info=True;User ID=ims_test;Password=ImsTest123$%^789";
            }            
            return new SqlConnection(conn);            
        }        

        public static int SyncTopicToEs(string nameSpace, string connectStr, string connectEs, int pageIndex, int pageSize)
        {
            try
            {
                string query = @"DECLARE @UpperBand int, @LowerBand int
                                
                                SET @LowerBand  = (@pageIndex - 1) * @PageSize
                                SET @UpperBand  = (@pageIndex * @PageSize)
                                SELECT * FROM (
                                    SELECT *, ROW_NUMBER() OVER(ORDER BY T.Id DESC) AS RowNumber 
                                    FROM Topic T                                 
                                ) AS temp
                                WHERE RowNumber > @LowerBand AND RowNumber <= @UpperBand";

                var con = GetConnection(connectStr);
                var cmd = new SqlCommand(query, con);

                cmd.Parameters.AddWithValue("@PageIndex", pageIndex);
                cmd.Parameters.AddWithValue("@PageSize", pageSize);

                con.Open();
                var reader = cmd.ExecuteReader();

                var data = EntityMapper.FillCollection<TopicEntity>(reader);

                con.Close();

                var list = data.Select(s => new TopicSearchEntity
                {
                    Id = s.Id,
                    TopicName = s.TopicName,
                    ZoneIds = new List<string>().ToArray(),                    
                    IsActive = s.IsActive,
                    isTopToolbar=s.IsTopToolbar,
                    Logo=s.Logo
                }).ToList();

                if (pageIndex == 1)
                {
                    EsNewsBo.CreateIndexSettings<TopicSearchEntity>(nameSpace, connectEs, "topic_name");
                }
                var abc= EsNewsBo.CreateMany(nameSpace, connectEs, list);

                return 1;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return 0;                
            }
        }

        public static int GetCountTopic(string connectStr)
        {
            try
            {
                string query = @"SELECT COUNT(*) as [Count] FROM [Topic]";

                var con = GetConnection(connectStr);
                var cmd = new SqlCommand(query, con);                               

                con.Open();
                var reader = cmd.ExecuteReader();

                var data = EntityMapper.FillCollection<CountNewsEntity>(reader);

                con.Close();

                var count = 0;
                if(data!=null && data.Count > 0)
                {
                    count = data.FirstOrDefault().Count;
                }

                return count;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return 0;                
            }
        }

        public static int SyncTopicToRedis(string nameSpace, string connectStr, string connectRedis, int pageIndex, int pageSize)
        {           
            try
            {
                string query = @"DECLARE @UpperBand int, @LowerBand int
                                
                                SET @LowerBand  = (@pageIndex - 1) * @PageSize
                                SET @UpperBand  = (@pageIndex * @PageSize)
                                SELECT * FROM (
                                    SELECT *, ROW_NUMBER() OVER(ORDER BY T.Id DESC) AS RowNumber 
                                    FROM Topic T                                 
                                ) AS temp
                                WHERE RowNumber > @LowerBand AND RowNumber <= @UpperBand";

                var con = GetConnection(connectStr);
                var cmd = new SqlCommand(query, con);
                
                cmd.Parameters.AddWithValue("@PageIndex", pageIndex);
                cmd.Parameters.AddWithValue("@PageSize", pageSize);                

                con.Open();
                var reader = cmd.ExecuteReader();

                var listNews = EntityMapper.FillCollection<TopicEntity>(reader);

                con.Close();

                var data = new Dictionary<string, TopicEntity>();
                foreach (var entry in listNews)
                {
                    data[entry.Id.ToString()] = entry;
                }
                var abc = RedisNewsBo.AddInHash(nameSpace, connectRedis, data);

                return 1;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return 0;
            }
        }

        #region Afamily

        #region Topic
        public static int SyncTopicToDb(string nameSpace, string connectStr1, string connectStr2, int pageIndex, int pageSize, IProgress<InitDataDb.ProcessEntity> progress, InitDataDb.ProcessEntity processEntity)
        {
            try
            {
                string query = @"DECLARE @UpperBand int, @LowerBand int
                                SET @LowerBand  = (@pageIndex - 1) * @PageSize
                                SET @UpperBand  = (@pageIndex * @PageSize)
                                SELECT * FROM (
                                SELECT n.*,ROW_NUMBER() OVER(ORDER BY n.Id DESC) AS RowNumber 
                                FROM [Topic] n
                                ) AS temp
                                WHERE RowNumber > @LowerBand AND RowNumber <= @UpperBand";
                
                var con = GetConnection(connectStr1);
                var cmd = new SqlCommand(query, con);
                              
                var dateFrom = DBNull.Value;
                var dateTo = DBNull.Value;
                cmd.Parameters.AddWithValue("@PageIndex", pageIndex);
                cmd.Parameters.AddWithValue("@PageSize", pageSize);
                cmd.Parameters.AddWithValue("@DateFrom", dateFrom);
                cmd.Parameters.AddWithValue("@DateTo", dateTo);

                con.Open();
                var reader = cmd.ExecuteReader();

                var data = EntityMapper.FillCollection<TopicEntity>(reader);

                con.Close();

                var abc = InitTopicToDb(nameSpace, connectStr2, data, progress, processEntity);

                return abc;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return 0;
            }
        }
        private static int InitTopicToDb(string nameSpace, string connectStr2, List<TopicEntity> data, IProgress<InitDataDb.ProcessEntity> progress, InitDataDb.ProcessEntity processEntity)
        {
            long id = 0;
            var count = 0;
            try
            {
                var con = GetConnection(connectStr2);
                con.Open();

                foreach (var item in data)
                {
                    string query = @"SET IDENTITY_INSERT [Topic] ON;
                                    DELETE [Topic] WHERE Id=@Id;
                                    INSERT INTO [Topic]([Id]
                                                      ,[TopicName]
                                                      ,[Logo]
                                                      ,[Cover]
                                                      ,[IsActive]
                                                      ,[IsIconActive]
                                                      ,[DisplayUrl]
                                                      ,[Description]
                                                      ,[LogoFancyClose]
                                                      ,[LogoTopicName]
                                                      ,[LogoSubMenu]
                                                      ,[DefaultViewMode]
                                                      ,[GuideToSendMail]
                                                      ,[TopicEmail]
                                                      ,[isTopToolbar]
                                                ) VALUES(@Id
                                                        ,@TopicName
                                                        ,@Logo
                                                        ,@Cover
                                                        ,@IsActive
                                                        ,@IsIconActive
                                                        ,@DisplayUrl
                                                        ,@Description
                                                        ,@LogoFancyClose
                                                        ,@LogoTopicName
                                                        ,@LogoSubMenu
                                                        ,@DefaultViewMode
                                                        ,@GuideToSendMail
                                                        ,@TopicEmail
                                                        ,@isTopToolbar);
                                                SET IDENTITY_INSERT [Topic] OFF;";

                    var cmd = new SqlCommand(query, con);                    
                    cmd.CommandType = CommandType.Text;

                    cmd.Parameters.AddWithValue("@Id", item.Id);
                    cmd.Parameters.AddWithValue("@TopicName", item.TopicName ?? (object)DBNull.Value);
                    cmd.Parameters.AddWithValue("@Logo", item.Logo ?? (object)DBNull.Value);
                    cmd.Parameters.AddWithValue("@Cover", item.Cover ?? (object)DBNull.Value);
                    cmd.Parameters.AddWithValue("@IsActive", item.IsActive);
                    cmd.Parameters.AddWithValue("@IsIconActive", item.IsIconActive);
                    cmd.Parameters.AddWithValue("@DisplayUrl", item.DisplayUrl ?? (object)DBNull.Value);
                    cmd.Parameters.AddWithValue("@Description", item.Description ?? (object)DBNull.Value);
                    cmd.Parameters.AddWithValue("@LogoFancyClose", item.LogoFancyClose ?? (object)DBNull.Value);
                    cmd.Parameters.AddWithValue("@LogoTopicName", item.LogoTopicName ?? (object)DBNull.Value);
                    cmd.Parameters.AddWithValue("@LogoSubMenu", item.LogoSubMenu ?? (object)DBNull.Value);

                    if (item.DefaultViewMode < 0)
                    {
                        item.DefaultViewMode = 0;
                    }   
                    cmd.Parameters.AddWithValue("@DefaultViewMode", item.DefaultViewMode);

                    cmd.Parameters.AddWithValue("@GuideToSendMail", item.GuideToSendMail ?? (object)DBNull.Value);
                    cmd.Parameters.AddWithValue("@TopicEmail", item.TopicEmail ?? (object)DBNull.Value);
                    cmd.Parameters.AddWithValue("@isTopToolbar", item.IsTopToolbar);                    

                    id = item.Id;
                    var reader = cmd.ExecuteNonQuery();
                    count++;

                    if (progress != null)
                    {
                        processEntity.value = count;
                        progress.Report(processEntity);
                    }
                }

                con.Close();

                return count;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message + " ID: " + id.ToString());
                return count;
            }
        }
        #endregion

        #region TopicInZone
        public static int SyncTopicInZoneToDb(string nameSpace, string connectStr1, string connectStr2, int pageIndex, int pageSize, IProgress<InitDataDb.ProcessEntity> progress, InitDataDb.ProcessEntity processEntity)
        {
            try
            {
                string query = @"DECLARE @UpperBand int, @LowerBand int
                                SET @LowerBand  = (@pageIndex - 1) * @PageSize
                                SET @UpperBand  = (@pageIndex * @PageSize)
                                SELECT * FROM (
                                SELECT n.*,ROW_NUMBER() OVER(ORDER BY n.Id DESC) AS RowNumber 
                                FROM [TopicInZone] n
                                ) AS temp
                                WHERE RowNumber > @LowerBand AND RowNumber <= @UpperBand";
                
                var con = GetConnection(connectStr1);
                var cmd = new SqlCommand(query, con);
                           
                var dateFrom = DBNull.Value;
                var dateTo = DBNull.Value;
                cmd.Parameters.AddWithValue("@PageIndex", pageIndex);
                cmd.Parameters.AddWithValue("@PageSize", pageSize);
                cmd.Parameters.AddWithValue("@DateFrom", dateFrom);
                cmd.Parameters.AddWithValue("@DateTo", dateTo);

                con.Open();
                var reader = cmd.ExecuteReader();

                var data = EntityMapper.FillCollection<TopicInZoneEntity>(reader);

                con.Close();

                var abc = InitTopicInZoneToDb(nameSpace, connectStr2, data, progress, processEntity);

                return abc;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return 0;
            }
        }
        private static int InitTopicInZoneToDb(string nameSpace, string connectStr2, List<TopicInZoneEntity> data, IProgress<InitDataDb.ProcessEntity> progress, InitDataDb.ProcessEntity processEntity)
        {
            long id = 0;
            var count = 0;
            try
            {
                var con = GetConnection(connectStr2);
                con.Open();

                foreach (var item in data)
                {
                    string query = @"SET IDENTITY_INSERT [TopicInZone] ON;
                                    DELETE [TopicInZone] WHERE Id=@Id;
                                    INSERT INTO [TopicInZone]([Id]                                                        
                                                        ,[TopicId]
                                                        ,[ZoneId]                                                        
                                                ) VALUES(@Id                                                        
                                                        ,@TopicId
                                                        ,@ZoneId);
                                        SET IDENTITY_INSERT [TopicInZone] OFF;";

                    var cmd = new SqlCommand(query, con);                    
                    cmd.CommandType = CommandType.Text;

                    cmd.Parameters.AddWithValue("@Id", item.Id);
                    cmd.Parameters.AddWithValue("@TopicId", item.TopicId);
                    cmd.Parameters.AddWithValue("@ZoneId", item.ZoneId);
                                        
                    id = item.Id;
                    var reader = cmd.ExecuteNonQuery();
                    count++;

                    if (progress != null)
                    {
                        processEntity.value = count;
                        progress.Report(processEntity);
                    }
                }

                con.Close();

                return count;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message + " ID: " + id.ToString());
                return count;
            }
        }
        public static int GetCountTopicInZone(string connectStr)
        {
            try
            {
                string query = @"SELECT COUNT(*) as [Count] FROM [TopicInZone]";

                var con = GetConnection(connectStr);
                var cmd = new SqlCommand(query, con);

                con.Open();
                var reader = cmd.ExecuteReader();

                var data = EntityMapper.FillCollection<CountNewsEntity>(reader);

                con.Close();

                var count = 0;
                if (data != null && data.Count > 0)
                {
                    count = data.FirstOrDefault().Count;
                }

                return count;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return 0;
            }
        }
        #endregion

        #region NewsInTopic
        public static int SyncNewsInTopicToDb(string nameSpace, string connectStr1, string connectStr2, int pageIndex, int pageSize, IProgress<InitDataDb.ProcessEntity> progress, InitDataDb.ProcessEntity processEntity)
        {
            try
            {
                string query = @"DECLARE @UpperBand int, @LowerBand int
                                SET @LowerBand  = (@pageIndex - 1) * @PageSize
                                SET @UpperBand  = (@pageIndex * @PageSize)
                                SELECT * FROM (
                                SELECT n.*,ROW_NUMBER() OVER(ORDER BY n.Id DESC) AS RowNumber 
                                FROM [NewsInTopic] n
                                ) AS temp
                                WHERE RowNumber > @LowerBand AND RowNumber <= @UpperBand";
                
                var con = GetConnection(connectStr1);
                var cmd = new SqlCommand(query, con);
                          
                var dateFrom = DBNull.Value;
                var dateTo = DBNull.Value;
                cmd.Parameters.AddWithValue("@PageIndex", pageIndex);
                cmd.Parameters.AddWithValue("@PageSize", pageSize);
                cmd.Parameters.AddWithValue("@DateFrom", dateFrom);
                cmd.Parameters.AddWithValue("@DateTo", dateTo);

                con.Open();
                var reader = cmd.ExecuteReader();

                var data = EntityMapper.FillCollection<NewsInTopicEntity>(reader);

                con.Close();

                var abc = InitNewsInTopicToDb(nameSpace, connectStr2, data, progress, processEntity);

                return abc;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return 0;
            }
        }
        private static int InitNewsInTopicToDb(string nameSpace, string connectStr2, List<NewsInTopicEntity> data, IProgress<InitDataDb.ProcessEntity> progress, InitDataDb.ProcessEntity processEntity)
        {
            long id = 0;
            var count = 0;
            try
            {
                var con = GetConnection(connectStr2);
                con.Open();

                foreach (var item in data)
                {
                    string query = @"SET IDENTITY_INSERT [NewsInTopic] ON;
                                    DELETE [NewsInTopic] WHERE Id=@Id;
                                    INSERT INTO [NewsInTopic]([Id]
                                                          ,[NewsId]
                                                          ,[TopicId]                                                          
                                                ) VALUES(@Id
                                                        ,@NewsId
                                                        ,@TopicId);
                                    SET IDENTITY_INSERT [NewsInTopic] OFF;";

                    var cmd = new SqlCommand(query, con);
                    cmd.CommandType = CommandType.Text;

                    cmd.Parameters.AddWithValue("@Id", item.Id);
                    cmd.Parameters.AddWithValue("@NewsId", item.NewsId);
                    cmd.Parameters.AddWithValue("@TopicId", item.TopicId);

                    id = item.Id;
                    var reader = cmd.ExecuteNonQuery();
                    count++;

                    if (progress != null)
                    {
                        processEntity.value = count;
                        progress.Report(processEntity);
                    }
                }

                con.Close();

                return count;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message + " ID: " + id.ToString());
                return count;
            }
        }
        public static int GetCountNewsInTopic(string connectStr)
        {
            try
            {
                string query = @"SELECT COUNT(*) as [Count] FROM [NewsInTopic]";

                var con = GetConnection(connectStr);
                var cmd = new SqlCommand(query, con);

                con.Open();
                var reader = cmd.ExecuteReader();

                var data = EntityMapper.FillCollection<CountNewsEntity>(reader);

                con.Close();

                var count = 0;
                if (data != null && data.Count > 0)
                {
                    count = data.FirstOrDefault().Count;
                }

                return count;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return 0;
            }
        }
        #endregion        

        #endregion
    }
}
