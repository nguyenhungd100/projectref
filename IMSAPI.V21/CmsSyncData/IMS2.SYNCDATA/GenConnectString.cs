﻿using IMS2.SYNCDATA.Common;
using IMS2.SYNCDATA.Entity;
using IMS2.SYNCDATA.ES;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Windows.Forms;

namespace IMS2.SYNCDATA
{
    public partial class GenConnectString : Form
    {
        public GenConnectString()
        {
            InitializeComponent();
        }

        private void buttonEncrypt_Click(object sender, EventArgs e)
        {
            var decryptKey = ConfigurationManager.AppSettings["ConnectionDecryptKey"];
            textBoxResult.Text = Crypton.EncryptByKey(textBoxConnectString.Text, decryptKey);
        }

        private void buttonDecrypt_Click(object sender, EventArgs e)
        {
            var decryptKey = ConfigurationManager.AppSettings["ConnectionDecryptKey"];
            textBoxResult.Text = Crypton.DecryptByKey(textBoxConnectString.Text, decryptKey);
        }

        private void buttonMD5_Click(object sender, EventArgs e)
        {            
            textBoxResult.Text = Crypton.Md5Encrypt(textBoxConnectString.Text);
            var abc = !((false && false) || (false && false) || (false && false));
            MessageBox.Show(abc.ToString());
        }

        private void buttonCreatedIndexEs_Click(object sender, EventArgs e)
        {
            //if (!string.IsNullOrEmpty(textBoxConnectString.Text))
            //{
                textBoxResult.Text = "";
                var res=EsNewsBo.CreateIndexSettings<NewsSearchEntity>("AFamily", "http://192.168.60.95:9200/", "title");
                textBoxResult.Text = res.ToString();
                var abc = EsNewsBo.Create("AFamily", "http://192.168.60.95:9200/", new NewsSearchEntity
                {
                    Id = 123456,
                    EncryptId = "123456",
                    Title = "",
                    ZoneIds = new string[] { },
                    Type = 0,
                    CreatedBy = "chinhnguyenba@gmail.com",
                    LastModifiedBy = "chinhnguyenba@gmail.com",
                    EditedBy = "chinhnguyenba@gmail.com",
                    PublishedBy = "chinhnguyenba@gmail.com",
                    LastReceiver = "chinhnguyenba@gmail.com",
                    ApprovedBy = "chinhnguyenba@gmail.com",
                    ReturnedBy = "chinhnguyenba@gmail.com",
                    CreatedDate = DateTime.Now,
                    DistributionDate = DateTime.Now,
                    LastModifiedDate = DateTime.Now,
                    ApprovedDate = DateTime.Now,
                    ViewCount = 0,
                    Status = 6,
                    IsOnHome = true,
                    Avatar = "",
                    ViewRoyalties = 0,
                    ZoneName = "Hôn nhân",
                    Note = "",
                    Author = "chinhnb@gmail.com",
                    ErrorCheckedBy = "chinhnguyenba@gmail.com",
                    ErrorCheckedDate = DateTime.Now,
                    SensitiveCheckedBy = "chinhnguyenba@gmail.com",
                    SensitiveCheckedDate = DateTime.Now,
                    WordCount = 0,
                    Sapo = "",
                    Url = "",
                    DisplayPosition = 1
                });
            //}            
        }
    }
}
