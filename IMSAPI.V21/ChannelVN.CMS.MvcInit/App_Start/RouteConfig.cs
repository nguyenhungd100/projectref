﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace ChannelVN.CMS.MvcInit
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            //routes.MapRoute(
            //    name: "Default",
            //    url: "{controller}/{action}/{id}",
            //    defaults: new { action = "Index", id = UrlParameter.Optional }
            //);

            routes.MapRoute(
               name: "Home",
               url: "{controller}/{action}/{id}",
               defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
           );

            routes.MapRoute(
               name: "InitDb/ExecuteInitSqlToRedis",
               url: "{controller}/{action}/{id}",
               defaults: new { controller = "InitDb", action = "ExecuteInitSqlToRedis", id = UrlParameter.Optional }
           );

            routes.MapRoute(
              name: "InitDb/ExecuteInitSqlToES",
              url: "{controller}/{action}/{id}",
              defaults: new { controller = "InitDb", action = "ExecuteInitSqlToES", id = UrlParameter.Optional }
          );
        }
    }
}
