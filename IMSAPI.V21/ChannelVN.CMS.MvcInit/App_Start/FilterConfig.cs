﻿using System.Web;
using System.Web.Mvc;

namespace ChannelVN.CMS.MvcInit
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }
}
