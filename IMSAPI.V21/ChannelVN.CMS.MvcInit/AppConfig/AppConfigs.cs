﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;

namespace ChannelVN.CMS.MvcInit.AppConfig
{
    public class AppConfigs
    {
        public static string CmsApiNamespaceInit
        {
            get
            {
                return GetString("CmsApi.NamespaceInit");
            }
        }

        public static string CmsApiPassInit
        {
            get { return GetString("CmsApi.PassInit"); }
        }

        public static string CmsApiPasscode
        {
            get
            {
                return GetString("CmsApi.Passcode");
            }
        }

        public static string CmsApiDomainInit
        {
            get
            {
                return GetString("CmsApi.DomainInit");
            }
        }

        public static string CmsApiNamespace
        {
            get
            {
                return GetString("CmsApi.Namespace");
            }
        }

        

        public static string GetString(string key)
        {
            return !string.IsNullOrEmpty(ConfigurationManager.AppSettings[key]) ? ConfigurationManager.AppSettings[key].Trim() : string.Empty;
        }
    }
}