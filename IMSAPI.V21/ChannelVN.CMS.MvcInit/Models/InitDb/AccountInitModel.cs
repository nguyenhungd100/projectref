﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ChannelVN.CMS.MvcInit.Models.InitDb
{
    public class AccountInitModel
    {
        public string Username { get; set; }

        public string Password { get; set; }
    }
}