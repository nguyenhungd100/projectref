﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ChannelVN.CMS.MvcInit.Models.InitDb
{
    public class InitDataRedisESModel
    {
        [Required(AllowEmptyStrings = false, ErrorMessage = "Tên bảng không được để trống")]
        public string NameTable { get; set; }

        public DateTime? StartDate { get; set; }

        public DateTime? EndDate { get; set; }

        public int PageSize { get; set; }

        public string Action { get; set; }

        public int StartPage { get; set; } = 1;

        public string InitType { get; set; }
    }
}