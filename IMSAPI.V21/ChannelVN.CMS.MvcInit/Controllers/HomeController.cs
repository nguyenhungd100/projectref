﻿using ChannelVN.CMS.Common;
using ChannelVN.CMS.MvcInit.AppConfig;
using ChannelVN.CMS.MvcInit.Models;
using ChannelVN.CMS.MvcInit.Models.InitDb;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.Mvc;

namespace ChannelVN.CMS.MvcInit.Controllers
{
    public class HomeController : Controller
    {
        // GET: Home
        public ActionResult Index()
        {
            return View();
        }


        [HttpPost]
        //[ValidateAntiForgeryToken]
        [AllowAnonymous]
        public ActionResult Index(AccountInitModel model)
        {
            try
            {
                var context = System.Web.HttpContext.Current;
                var checkLogin = false;
                var access_token = string.Empty;
                var token = new DataTokenEntity();
                if (string.IsNullOrEmpty(model.Username))
                {
                    ModelState.AddModelError("", "Tài khoản không được để trống");
                }

                if (string.IsNullOrEmpty(model.Password))
                {
                    ModelState.AddModelError("", "Mật khẩu không được để trống");
                }

                if (Common.Crypton.Encrypt(model.Password) == AppConfigs.CmsApiPassInit && model.Username == "admin")
                {
                    using (var client = new HttpClient())
                    {
                        string base_url = AppConfigs.CmsApiDomainInit.TrimEnd('/');
                        var content = new FormUrlEncodedContent(new[]
                                {
                            new KeyValuePair<string, string>("username",model.Username),
                            new KeyValuePair<string, string>("password",AppConfigs.CmsApiPasscode),
                            new KeyValuePair<string, string>("grant_type","password"),
                            new KeyValuePair<string, string>("namespace",AppConfigs.CmsApiNamespaceInit)
                        });

                        var response = client.PostAsync(base_url + "/" + "oauth2/token", content).Result;
                        if (response.IsSuccessStatusCode)
                        {
                            var rep = response.Content.ReadAsStringAsync().Result;
                            if (!string.IsNullOrEmpty(rep))
                            {
                                token = NewtonJson.Deserialize<DataTokenEntity>(rep);
                                context.Session["user_login"] = model.Username;
                                checkLogin = true;
                            }
                        }
                    }
                }

                if (checkLogin)
                {
                    return Json(NewtonJson.Serialize(token));
                    //return RedirectToAction("Index", "InitDb");
                }
                else
                {
                    ModelState.AddModelError("", "Tên đăng nhập hoặc mật khẩu không tồn tại");
                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, "LoginInitDb=>" + ex.Message);
            }
            return View();
        }

        [HttpPost]
        //[Authorize]
        public ActionResult LogOut(string username)
        {
            try
            {
                var context = System.Web.HttpContext.Current;
                context.Session["user_login"] = null;
            }
            catch (Exception ex)
            {

            }
            //Response.Redirect("~/Home/Index", true);
            return Json(Url.Action("Index", "Home"));
            //return RedirectToAction("Index","Home"); ;
        }
    }
}