﻿using ChannelVN.CMS.BoCached.Entity.Init;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.MvcInit.AppConfig;
using ChannelVN.CMS.MvcInit.Models.InitDb;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace ChannelVN.CMS.MvcInit.Controllers
{
    public class InitDbController : Controller
    {
        // GET: InitDb
        //[Authorize]
        public ActionResult Index()
        {
            return View();
        }

        //[Authorize]
        [HttpPost]
        public ActionResult ExecuteInitSqlToRedis(InitDataRedisESModel initData)
        {
            var data = new LogInitAsyncEntity();
            try
            {
                var uri = string.Empty;
                switch (initData.InitType)
                {
                    case "FromSqlToRedis":
                        uri = "api/base/system/init_data_redis?";
                        break;
                    case "FromSqlToEs":
                        uri = "api/base/system/init_data_es?";
                        break;
                    default:
                        break;
                }

                using (var client = new HttpClient())
                {
                    string base_url = AppConfigs.CmsApiDomainInit.TrimEnd('/');
                    var strParamBuilder = new StringBuilder();
                    strParamBuilder.Append($"name={initData.NameTable}");
                    strParamBuilder.Append($"&startDate={initData.StartDate}");
                    strParamBuilder.Append($"&endDate={initData.EndDate}");
                    strParamBuilder.Append($"&num={initData.PageSize}");
                    if (initData.Action != "noaction")
                    {
                        strParamBuilder.Append($"&action={initData.Action}");
                    }
                    strParamBuilder.Append($"&startPage={initData.StartPage}");
                    strParamBuilder.Append($"&password=chinhnb");

                    var token = Request.Headers["Authorization"];
                    client.DefaultRequestHeaders.Add("Authorization", token);
                    var response = client.GetAsync(base_url + "/" + uri + strParamBuilder.ToString()).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        var rep = response.Content.ReadAsStringAsync().Result;
                        if (!string.IsNullOrEmpty(rep))
                        {
                            data = JsonConvert.DeserializeObject<LogInitAsyncEntity>(rep);
                        }
                    }
                    else
                    {
                        data.Message = response.ReasonPhrase;
                    }
                }
            }
            catch (Exception ex)
            {
                data.Message = ex.ToString();
                Logger.WriteLog(Logger.LogType.Error, "Execute SyncDb =>" + ex.Message);
            }
            return Json(data);
        }
    }
}