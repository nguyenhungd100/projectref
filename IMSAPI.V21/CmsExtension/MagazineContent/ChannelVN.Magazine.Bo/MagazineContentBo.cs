﻿using System;
using System.Collections.Generic;
using System.Linq;
using ChannelVN.MagazineContent.Dal;
using ChannelVN.MagazineContent.Entity;
using ChannelVN.MagazineContent.Entity.ErrorCode;
using ChannelVN.WcfExtensions;
using ChannelVN.CMS.Common;

namespace ChannelVN.MagazineContent.Bo
{
    public class MagazineContentBo
    {
        #region Magazine
        
        public static ErrorMapping.ErrorCodes InsertMagazineContent(MagazineContentEntity entity)
        {
            try
            {
                if (string.IsNullOrEmpty(entity.Title))
                {
                    return ErrorMapping.ErrorCodes.UpdateMagazineInvalidTitle;
                }
                return MagazineContentDal.Insert(entity)
                           ? ErrorMapping.ErrorCodes.Success
                           : ErrorMapping.ErrorCodes.BusinessError;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return ErrorMapping.ErrorCodes.BusinessError;
            }
        }

        public static ErrorMapping.ErrorCodes UpdateMagazineContent(MagazineContentEntity entity)
        {
            try
            {
                if (entity.Id <= 0)
                {
                    return ErrorMapping.ErrorCodes.UpdateMagazineNotFoundMagazine;
                }
                var currentMagazine = MagazineContentDal.GetById(entity.Id);
                if (currentMagazine == null)
                {
                    return ErrorMapping.ErrorCodes.UpdateMagazineNotFoundMagazine;
                }
                if (string.IsNullOrEmpty(entity.Title))
                {
                    return ErrorMapping.ErrorCodes.UpdateMagazineInvalidTitle;
                }
                DateTime MinDateTime = new DateTime(1980, 1, 1);
                if (entity.PublishedDate < MinDateTime)
                    entity.PublishedDate = currentMagazine.PublishedDate;
                return MagazineContentDal.Update(entity)
                           ? ErrorMapping.ErrorCodes.Success
                           : ErrorMapping.ErrorCodes.BusinessError;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return ErrorMapping.ErrorCodes.BusinessError;
            }
            
        }

        public static ErrorMapping.ErrorCodes DeleteMagazineContent(int magazineId)
        {
            try
            {
                if (magazineId <= 0)
                {
                    return ErrorMapping.ErrorCodes.UpdateMagazineNotFoundMagazine;
                }
                var currentMagazine = MagazineContentDal.GetById(magazineId);
                if (currentMagazine == null)
                {
                    return ErrorMapping.ErrorCodes.UpdateMagazineNotFoundMagazine;
                }
                return MagazineContentDal.Delete(magazineId)
                           ? ErrorMapping.ErrorCodes.Success
                           : ErrorMapping.ErrorCodes.BusinessError;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return ErrorMapping.ErrorCodes.BusinessError;
            }
            
        }
        public static ErrorMapping.ErrorCodes MagazineContent_UpdateStatus(int magazineId, MagazineContentStatus status, string UserName)
        {
            try
            {
                if (magazineId <= 0)
                {
                    return ErrorMapping.ErrorCodes.UpdateMagazineNotFoundMagazine;
                }
                var currentMagazine = MagazineContentDal.GetById(magazineId);
                if (currentMagazine == null)
                {
                    return ErrorMapping.ErrorCodes.UpdateMagazineNotFoundMagazine;
                }
                return MagazineContentDal.Update_Status(magazineId, (int)status, UserName)
                           ? ErrorMapping.ErrorCodes.Success
                           : ErrorMapping.ErrorCodes.BusinessError;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return ErrorMapping.ErrorCodes.BusinessError;
            }

        }
        public static ErrorMapping.ErrorCodes MagazineContent_UpdateIsHot(int magazineId, bool IsHot, string UserName)
        {
            try
            {
                if (magazineId <= 0)
                {
                    return ErrorMapping.ErrorCodes.UpdateMagazineNotFoundMagazine;
                }
                var currentMagazine = MagazineContentDal.GetById(magazineId);
                if (currentMagazine == null)
                {
                    return ErrorMapping.ErrorCodes.UpdateMagazineNotFoundMagazine;
                }
                return MagazineContentDal.Update_IsHot(magazineId, IsHot, UserName)
                           ? ErrorMapping.ErrorCodes.Success
                           : ErrorMapping.ErrorCodes.BusinessError;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return ErrorMapping.ErrorCodes.BusinessError;
            }

        }
        public static ErrorMapping.ErrorCodes MagazineContent_Published(int magazineId, DateTime PublishedDate, string UserName)
        {
            try
            {
                if (magazineId <= 0)
                {
                    return ErrorMapping.ErrorCodes.UpdateMagazineNotFoundMagazine;
                }
                var currentMagazine = MagazineContentDal.GetById(magazineId);
                if (currentMagazine == null)
                {
                    return ErrorMapping.ErrorCodes.UpdateMagazineNotFoundMagazine;
                }
                return MagazineContentDal.Published(magazineId, PublishedDate, UserName)
                           ? ErrorMapping.ErrorCodes.Success
                           : ErrorMapping.ErrorCodes.BusinessError;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return ErrorMapping.ErrorCodes.BusinessError;
            }

        }
        public static MagazineContentEntity GetMagazineContentById(int magazineId)
        {
            try
            {
                if (magazineId <= 0)
                {
                    return null;
                }
                return MagazineContentDal.GetById(magazineId);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return null;
            }
        }

        public static List<MagazineContentEntity> MagazineContentSearch(string keyword, MagazineContentIsHot IsHot, MagazineContentStatus status, string UserName, DateTime fromDate, DateTime toDate, int pageIndex, int pageSize, ref int totalRow)
        {
            try
            {
                return MagazineContentDal.Search(keyword, (int)IsHot, (int)status, UserName, fromDate, toDate, pageIndex, pageSize, ref totalRow);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return new List<MagazineContentEntity>();
            }
           
        }

        #endregion

    }
}
