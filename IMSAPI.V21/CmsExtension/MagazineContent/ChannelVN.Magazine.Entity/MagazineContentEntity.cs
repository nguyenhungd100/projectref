﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using ChannelVN.CMS.Common;

namespace ChannelVN.MagazineContent.Entity
{
    [DataContract]
    public enum MagazineContentStatus
    {  
        /// <summary>
        /// Tất cả các trạng thái
        /// </summary>
        [EnumMember]
        AllStatus = -1,
        /// <summary>
        /// Lưu tạm
        /// </summary>
        [EnumMember]
        Temporary = 0,
        /// <summary>
        /// Xuất bản
        /// </summary>
        [EnumMember]
        Active = 1,
        /// <summary>
        /// Gỡ Xuất bản
        /// </summary>
        [EnumMember]
        UnActive = 2
    }
    public enum MagazineContentIsHot
    {
        /// <summary>
        /// Tất cả
        /// </summary>
        [EnumMember]
        All = -1,
        /// <summary>
        /// NotIsHot
        /// </summary>
        [EnumMember]
        NotIsHot = 0,
        /// <summary>
        /// NotIsHot
        /// </summary>
        [EnumMember]
        IsHot = 1,
    }
    [DataContract]
    public class MagazineContentEntity : EntityBase
    {
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public string Title { get; set; }
        [DataMember]
        public string Avatar { get; set; }
        [DataMember]
        public string IndexUrl { get; set; }
        [DataMember]
        public string Description { get; set; }
        [DataMember]
        public DateTime PublishedDate { get; set; }
        [DataMember]
        public DateTime CreatedDate { get; set; }
        [DataMember]
        public string CreatedBy { get; set; }
        [DataMember]
        public DateTime LastModifiedDate { get; set; }
        [DataMember]
        public string LastModifiedBy { get; set; }
        [DataMember]
        public int Status { get; set; }
        [DataMember]
        public bool IsHot { get; set; }
    }
}
