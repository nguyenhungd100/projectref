﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ChannelVN.CMS.Common;
using ChannelVN.MagazineContent.Entity;
using ChannelVN.MagazineContent.MainDal.Databases;

namespace ChannelVN.MagazineContent.Dal
{
    public class MagazineContentDal
    {
        public static bool Insert(MagazineContentEntity magazine)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.MagazineContentMainDal.Insert(magazine);
            }
            return returnValue;
        }

        public static bool Update(MagazineContentEntity magazine)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.MagazineContentMainDal.Update(magazine);
            }
            return returnValue;
        }

        public static bool Delete(int MagazineId)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.MagazineContentMainDal.Delete(MagazineId);
            }
            return returnValue;
        }
        public static bool Update_Status(int Id, int status, string UserName)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.MagazineContentMainDal.Update_Status(Id, status,UserName);
            }
            return returnValue;
        }
        public static bool Update_IsHot(int Id, bool IsHot, string UserName)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.MagazineContentMainDal.Update_IsHot(Id, IsHot, UserName);
            }
            return returnValue;
        }
        public static bool Published(int Id, DateTime PublishedDate, string UserName)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.MagazineContentMainDal.Published(Id, PublishedDate, UserName);
            }
            return returnValue;
        }
        public static MagazineContentEntity GetById(int id)
        {
            MagazineContentEntity returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.MagazineContentMainDal.GetById(id);
            }
            return returnValue;
        }

        public static List<MagazineContentEntity> Search(string keyword, int IsHot, int status, string UserName, DateTime fromDate, DateTime toDate, int pageIndex, int pageSize, ref int totalRow)
        {
            List<MagazineContentEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.MagazineContentMainDal.Search(keyword, IsHot, status, UserName, fromDate, toDate, pageIndex, pageSize, ref totalRow);
            }
            return returnValue;
        }

    }
}
