﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ChannelVN.CMS.Common;
using ChannelVN.MagazineContent.Entity;
using ChannelVN.MagazineContent.MainDal.Common;
using ChannelVN.MagazineContent.MainDal.Databases;

namespace ChannelVN.MagazineContent.MainDal
{
    public abstract class MagazineContentDalBase
    {
        public bool Insert(MagazineContentEntity magazine)
        {
            const string commandText = "CMS_MagazineContent_Insert";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                // _db.AddParameter(cmd, "Id", newMagazineId, ParameterDirection.Output);
                _db.AddParameter(cmd, "Title", magazine.Title);
                _db.AddParameter(cmd, "Avatar", magazine.Avatar);
                _db.AddParameter(cmd, "IndexUrl", magazine.IndexUrl);
                _db.AddParameter(cmd, "Description", magazine.Description);
                _db.AddParameter(cmd, "PublishedDate", magazine.PublishedDate < Constants.MinDateTime ? magazine.PublishedDate : DateTime.Now);
                _db.AddParameter(cmd, "CreatedBy", magazine.CreatedBy);
                _db.AddParameter(cmd, "Status", magazine.Status);
                _db.AddParameter(cmd, "IsHot", magazine.IsHot);
                var numberOfRow = _db.ExecuteNonQuery(cmd);
                //newMagazineId = Utility.ConvertToInt(cmd.Parameters[0]);
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public bool Update(MagazineContentEntity magazine)
        {
            const string commandText = "CMS_MagazineContent_Update";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Title", magazine.Title);
                _db.AddParameter(cmd, "Avatar", magazine.Avatar);
                _db.AddParameter(cmd, "IndexUrl", magazine.IndexUrl);
                _db.AddParameter(cmd, "Description", magazine.Description);
                _db.AddParameter(cmd, "LastModifiedBy", magazine.LastModifiedBy);
                _db.AddParameter(cmd, "PublishedDate", magazine.PublishedDate);
                _db.AddParameter(cmd, "Status", magazine.Status);
                _db.AddParameter(cmd, "IsHot", magazine.IsHot);
                _db.AddParameter(cmd, "Id", magazine.Id);
                var numberOfRow = _db.ExecuteNonQuery(cmd);
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public bool Delete(int MagazineId)
        {
            const string commandText = "CMS_MagazineContent_Delete";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", MagazineId);
                var numberOfRow = _db.ExecuteNonQuery(cmd);
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public MagazineContentEntity GetById(int id)
        {
            const string commandText = "CMS_MagazineContent_GetbyId";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", id);
                var numberOfRow = _db.Get<MagazineContentEntity>(cmd);
                return numberOfRow;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public List<MagazineContentEntity> Search(string keyword, int IsHot, int status, string UserName, DateTime fromDate, DateTime toDate, int pageIndex, int pageSize, ref int totalRow)
        {
            const string commandText = "CMS_MagazineContent_Search";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "TotalRow", totalRow, ParameterDirection.Output);
                _db.AddParameter(cmd, "Keyword", keyword);
                _db.AddParameter(cmd, "Status", status);
                _db.AddParameter(cmd, "IsHot", IsHot);
                _db.AddParameter(cmd, "CreatedBy", UserName);
                if (fromDate < Constants.MinDateTime)
                    _db.AddParameter(cmd, "FromDate", DBNull.Value);
                else
                    _db.AddParameter(cmd, "FromDate", fromDate);
                if (toDate < Constants.MinDateTime)
                    _db.AddParameter(cmd, "ToDate", DBNull.Value);
                else
                    _db.AddParameter(cmd, "ToDate", toDate);
                _db.AddParameter(cmd, "PageIndex", pageIndex);
                _db.AddParameter(cmd, "PageSize", pageSize);
                var numberOfRow = _db.GetList<MagazineContentEntity>(cmd);
                totalRow = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 0));
                return numberOfRow;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool Update_Status(int Id, int status, string UserName)
        {
            const string commandText = "CMS_MagazineContent_UpdateStatus";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", Id);
                _db.AddParameter(cmd, "status", status);
                _db.AddParameter(cmd, "UserName", UserName);
                var numberOfRow = _db.ExecuteNonQuery(cmd);
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool Update_IsHot(int Id, bool IsHot, string UserName)
        {
            const string commandText = "CMS_MagazineContent_UpdateIsHot";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", Id);
                _db.AddParameter(cmd, "IsHot", IsHot);
                _db.AddParameter(cmd, "UserName", UserName);
                var numberOfRow = _db.ExecuteNonQuery(cmd);
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool Published(int Id, DateTime PublishedDate, string UserName)
        {
            const string commandText = "CMS_MagazineContent_Published";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", Id);
                _db.AddParameter(cmd, "PublishedDate", PublishedDate < Constants.MinDateTime ? DateTime.Now : PublishedDate);
                _db.AddParameter(cmd, "PublishedBy", UserName);
                var numberOfRow = _db.ExecuteNonQuery(cmd);
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        #region Core members

        private readonly CmsMainDb _db;

        protected MagazineContentDalBase(CmsMainDb db)
        {
            _db = db;
        }

        protected CmsMainDb Database
        {
            get { return _db; }
        }

        #endregion
    }
}
