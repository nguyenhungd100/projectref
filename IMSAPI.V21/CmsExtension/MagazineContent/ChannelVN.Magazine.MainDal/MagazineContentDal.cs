﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ChannelVN.CMS.Common;
using ChannelVN.MagazineContent.Entity;
using ChannelVN.MagazineContent.MainDal.Databases;

namespace ChannelVN.MagazineContent.MainDal
{
    public class MagazineContentDal : MagazineContentDalBase
    {
        internal MagazineContentDal(CmsMainDb db)
            : base(db)
        {
        }
    }
}
