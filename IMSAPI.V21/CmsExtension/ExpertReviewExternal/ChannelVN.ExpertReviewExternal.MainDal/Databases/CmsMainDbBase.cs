﻿using System.Data;
using ChannelVN.CMS.Common.DalCommon;

namespace ChannelVN.ExpertReviewExternal.MainDal.Databases
{
    public abstract class CmsMainDbBase : MainDbBase
    {
        #region Store procedures

        //private ZoneDal _zoneMainDal;
        //public ZoneDal ZoneMainDal
        //{
        //    get { return _zoneMainDal ?? (_zoneMainDal = new ZoneDal((CmsMainDb)this)); }
        //}
        private TagDal _TagMainDal;
        public TagDal TagMainDal
        {
            get { return _TagMainDal ?? (_TagMainDal = new TagDal((CmsMainDb)this)); }
        }

        private NewsDal _NewsMainDal;
        public NewsDal NewsMainDal
        {
            get { return _NewsMainDal ?? (_NewsMainDal = new NewsDal((CmsMainDb)this)); }
        }


        #endregion

        #region Constructors

        protected CmsMainDbBase()
        {
        }
        protected CmsMainDbBase(bool init)
        {
            if (init)
            {
                InitConnection();
            }
        }

        #endregion

        #region Protected members

        protected override sealed void InitConnection()
        {
            _connection = CreateConnection();
            _connection.Open();
        }

        /// <summary>
        /// Creates a new connection to the database.
        /// </summary>
        /// <returns>A reference to the <see cref="System.Data.IDbConnection"/> object.</returns>
        protected abstract IDbConnection CreateConnection();

        #endregion
    }
}
