﻿using ChannelVN.ExpertReviewExternal.MainDal.Databases;
using ChannelVN.ExpertReviewExternal.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.MainDal.Common;

namespace ChannelVN.ExpertReviewExternal.MainDal
{
    public class NewsDalBase
    {
        public bool UpdateNews(NewsExpertEntity entity, string TagIdList, string TagJSON)
        {
            const string commandText = "CMS_News_Expert_Update";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "NewsId", entity.NewsId);
                _db.AddParameter(cmd, "Body", entity.Body);
                _db.AddParameter(cmd, "MetaTitle", entity.MetaTitle);
                _db.AddParameter(cmd, "MetaKeyword", entity.MetaKeyword);
                _db.AddParameter(cmd, "MetaDescription", entity.MetaDescription);
                _db.AddParameter(cmd, "TagItem", entity.Tag);
                _db.AddParameter(cmd, "TagIdList", TagIdList);
                _db.AddParameter(cmd, "Tag", TagJSON);
                var numberOfRow = _db.ExecuteNonQuery(cmd);

                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public List<ExpertNewsInListEntity> SearchNewsPublishedByKeyword(string keyword, int pageSize, int pageIndex,  ref int TotalRows)
        {
            const string commandText = "CMS_News_Search_ByGoogleKeyword";
            try
            {
                List<ExpertNewsInListEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "TotalRows", TotalRows, ParameterDirection.Output);
                _db.AddParameter(cmd, "Keyword", keyword);
                _db.AddParameter(cmd, "PageSize", pageSize);
                _db.AddParameter(cmd, "PageIndex", pageIndex);
                data = _db.GetList<ExpertNewsInListEntity>(cmd);
                TotalRows = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 0));
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public List<ExpertNewsInListEntity> SearchNews(string keyword, string zoneIds, DateTime fromDate, DateTime toDate,int pageIndex, int pageSize, ref int totalRow)
        {
            const string commandText = "CMS_News_Search_Published_For_Seoer";
            try
            {
                List<ExpertNewsInListEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "TotalRow", totalRow, ParameterDirection.Output);
                _db.AddParameter(cmd, "Keyword", keyword);
                _db.AddParameter(cmd, "ZoneIds", zoneIds);
                if (fromDate <= Constants.MinDateTime)
                    _db.AddParameter(cmd, "DateFrom", DBNull.Value);
                else _db.AddParameter(cmd, "DateFrom", fromDate);
                if (toDate <= Constants.MinDateTime)
                    _db.AddParameter(cmd, "DateTo", DBNull.Value);
                else _db.AddParameter(cmd, "DateTo", toDate);
                _db.AddParameter(cmd, "PageIndex", pageIndex);
                _db.AddParameter(cmd, "PageSize", pageSize);
                data = _db.GetList<ExpertNewsInListEntity>(cmd);

                totalRow = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 0));

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        #region Constructor

        private readonly CmsMainDb _db;

        protected NewsDalBase(CmsMainDb db)
        {
            _db = db;
        }

        protected CmsMainDb Database
        {
            get { return _db; }
        }

        #endregion
    }
}
