﻿using ChannelVN.ExpertReviewExternal.MainDal.Databases;
using ChannelVN.ExpertReviewExternal.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using ChannelVN.CMS.Common;

namespace ChannelVN.ExpertReviewExternal.MainDal
{
    public class TagDalBase
    {
        public List<TagExpertEntity> SearchTag(string keyword, long parentTagId, int isThread, int zoneId, int type, int orderBy, int isHotTag, int pageIndex, int pageSize, ref int totalRow, bool getTagHasNewsOnly = false)
        {
            const string commandText = "CMS_Tag_Search";
            try
            {
                List<TagExpertEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "TotalRow", totalRow, ParameterDirection.Output);
                _db.AddParameter(cmd, "Keyword", keyword);
                _db.AddParameter(cmd, "ParentID", parentTagId);
                _db.AddParameter(cmd, "IsThread", isThread);
                _db.AddParameter(cmd, "IsHotTag", isHotTag);
                _db.AddParameter(cmd, "ZoneId", zoneId);
                _db.AddParameter(cmd, "Type", type);
                _db.AddParameter(cmd, "OrderBy", orderBy);
                _db.AddParameter(cmd, "PageIndex", pageIndex);
                _db.AddParameter(cmd, "PageSize", pageSize);
                _db.AddParameter(cmd, "GetTagHasNewsOnly", getTagHasNewsOnly);
                data = _db.GetList<TagExpertEntity>(cmd);
                totalRow = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 0));
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public List<TagExpertEntity> GetListTagNew(int zoneId, int pageIndex, int pageSize, ref int totalRow)
        {
            const string commandText = "CMS_Tag_GetTagNew";
            try
            {
                List<TagExpertEntity> data = new List<TagExpertEntity>();
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "TotalRow", totalRow, ParameterDirection.Output);
                _db.AddParameter(cmd, "PageIndex", pageIndex);
                _db.AddParameter(cmd, "PageSize", pageSize);
                _db.AddParameter(cmd, "ZoneId", zoneId);
                data = _db.GetList<TagExpertEntity>(cmd);
                totalRow = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 0));
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public TagDetailEntity GetTagByTagId(long tagId)
        {
            const string commandText = "CMS_Tag_GetTagByTagID";
            try
            {
                TagDetailEntity data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", tagId);
                data = _db.Get<TagDetailEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public List<TagZoneEntity> GetTagNewsByTagId(long tagId)
        {
            const string commandText = "CMS_TagZone_GetByTagId";
            try
            {
                List<TagZoneEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "TagId", tagId);
                data = _db.GetList<TagZoneEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool Tag_Insert(string TagNames, string CreatedBy, int PrimaryZoneId, string OtherZoneId, ref string Ids)
        {
            Ids = "";
            const string commandText = "CMS_Tag_Expert_Insert";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Ids",Ids, ParameterDirection.Output);
                _db.AddParameter(cmd, "Names", TagNames);
                _db.AddParameter(cmd, "CreatedBy", CreatedBy);
                _db.AddParameter(cmd, "PrimaryZoneId", PrimaryZoneId);
                _db.AddParameter(cmd, "OtherZoneId", OtherZoneId);
                var Data = cmd.ExecuteScalar();
                Ids = _db.GetParameterValueFromCommand(cmd, 0).ToString();
                Ids = Data.ToString();
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool Tag_UpdateStatus(long TagId, EnumTagStatus status)
        {
            const string commandText = "CMS_Tag_UpdateStatus";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", TagId);
                _db.AddParameter(cmd, "Status", (int)status);
                var Data = _db.ExecuteNonQuery(cmd);
                return Data > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        #region Constructor

        private readonly CmsMainDb _db;

        protected TagDalBase(CmsMainDb db)
        {
            _db = db;
        }

        protected CmsMainDb Database
        {
            get { return _db; }
        }

        #endregion
    }
}
