﻿using ChannelVN.CMS.BO.Base.Tag;
using ChannelVN.CMS.BO.Base.Thread;
using ChannelVN.CMS.BO.Common;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.Entity.Base.Tag;
using ChannelVN.CMS.Entity.Base.Thread;
using ChannelVN.ExpertReviewExternal.Dal;
using ChannelVN.ExpertReviewExternal.Entity;
using System;
using System.Collections.Generic;

namespace ChannelVN.ExpertReviewExternal.Bo
{
    public class TagExpertBo
    {
        public static List<TagExpertEntity> SearchTag(string channelMapping,string keyword, int zoneId, int type, int isHotTag, int pageIndex, int pageSize, ref int totalRow)
        {
            ChannelMapping.SetChannelNamespace(channelMapping);
            try
            {
                return TagDal.SearchTag(keyword, -1 ,-1, zoneId, type, 0, isHotTag, pageIndex, pageSize, ref totalRow);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.ToString());
            }
            return new List<TagExpertEntity>();
        }
        public static List<TagExpertEntity> GetListTagNew(string channelMapping, int zoneId, int pageIndex, int pageSize, ref int totalRow)
        {
            ChannelMapping.SetChannelNamespace(channelMapping);
            try
            {
                return TagDal.GetListTagNew(zoneId, pageIndex, pageSize, ref totalRow);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.ToString());
            }
            return new List<TagExpertEntity>();
        }
        public static TagDetailEntity GetTagByTagId(string channelMapping,long tagId)
        {
            ChannelMapping.SetChannelNamespace(channelMapping);
            var tagDetail = TagDal.GetTagByTagId(tagId);
            if (null != tagDetail)
            {
                tagDetail.TagZone = TagDal.GetTagNewsByTagId(tagDetail.Id);
                tagDetail.TagRelation = GetTagRelation(tagId);
            }
            return tagDetail;
        }
      public static List<TagRelationEntity> GetTagRelation(long tagId)
        {
            var listTag = new List<TagRelationEntity>();
            var tag = TagBo.Tag_GetRelation(tagId);
            foreach (var item in tag)
            {
                listTag.Add(new TagRelationEntity
                {
                    Id = item.Id,
                    Name=item.Name,
                    NewsCount =item.NewsCount,
                    UnsignName=item.UnsignName,
                    Url=item.Url
                });
            }
            return listTag;
        }
        public static TagEntity GetTagByTagName(string channelMapping, string name, bool getTagHasNewsOnly = false)
        {
            try
            {
                ChannelMapping.SetChannelNamespace(channelMapping);
                return BoFactory.GetInstance<TagBo>().GetTagByTagName(name, getTagHasNewsOnly);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.ToString());
                return null;
            }
            
        }

        public static bool Tag_Insert(string channelMapping, string TagNames, string CreatedBy, int PrimaryZoneId, string OtherZoneId, ref string Ids)
        {
            try
            {
                ChannelMapping.SetChannelNamespace(channelMapping);
                return TagDal.Tag_Insert(TagNames, CreatedBy, PrimaryZoneId, OtherZoneId, ref Ids);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.ToString());
            }
            return false;
        }
        public static bool Tag_UpdateStatus(string channelMapping, long tagId, EnumTagStatus tagStatus)
        {
            try
            {
                ChannelMapping.SetChannelNamespace(channelMapping);
                return TagDal.Tag_UpdateStatus(tagId, tagStatus);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.ToString());
            }
            return false;
        }

        public static List<ThreadEntity> SearchThread(string channelMapping, string keyword, int zoneId, int orderBy, int isHotThread, int pageIndex, int pageSize, ref int totalRow)
        {
            ChannelMapping.SetChannelNamespace(channelMapping);
            return BoFactory.GetInstance<ThreadBo>().SearchThread(keyword, zoneId, orderBy, isHotThread, pageIndex, pageSize, ref totalRow);
        }
    }
}
