﻿using ChannelVN.CMS.BO.Base.News;
using ChannelVN.CMS.BO.Base.Tag;
using ChannelVN.CMS.BO.Common;
using ChannelVN.CMS.BoCached.CacheObjects;
using ChannelVN.CMS.BoCached.Common;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.DAL.Base.News;
using ChannelVN.CMS.Entity.Base.News;
using ChannelVN.CMS.Entity.Base.Tag;
using ChannelVN.ExpertReviewExternal.Dal;
using ChannelVN.ExpertReviewExternal.Entity;
using ChannelVN.ExpertReviewExternal.Entity.ErrorCode;
using ChannelVN.SEO.BO.SEOMetaNews;
using ChannelVN.SocialNetwork.BO;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ChannelVN.ExpertReviewExternal.Bo
{
    public class NewsExpertBo
    {
        public static List<ExpertNewsInListEntity> SearchNews(string channelMapping, string keyword, string zoneIds, DateTime fromDate, DateTime toDate, int pageIndex, int pageSize, ref int totalRows)
        {
            ChannelMapping.SetChannelNamespace(channelMapping);
            try
            {
                return NewsExpertDal.SearchNews(keyword, zoneIds, fromDate, toDate, pageIndex, pageSize, ref totalRows);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Fatal, ex.ToString());
                return new List<ExpertNewsInListEntity>();
            }
        }
        public static List<ExpertNewsInListEntity> SearchNewsPublishedByKeyword(string channelMapping,string keyword, int pageSize, int pageIndex, ref int TotalRows)
        {
            ChannelMapping.SetChannelNamespace(channelMapping);
            try
            {
                return NewsExpertDal.SearchNewsPublishedByKeyword(keyword, pageSize, pageIndex, ref TotalRows);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Fatal, ex.ToString());
                return new List<ExpertNewsInListEntity>();
            }
        }
        public static NewsDetailEntity NewsGetById(string channelMapping, long NewsId)
        {
            ChannelMapping.SetChannelNamespace(channelMapping);
            try
            {
                var news =  BoFactory.GetInstance<NewsBo>().GetNewsForEditByNewsId(NewsId, "");
                if (news != null || news.NewsInfo != null)
                {
                    var NewsDetail = new NewsDetailEntity();
                    var Zone = news.NewsInZone.FirstOrDefault(k => k.IsPrimary == true);
                    var _news = news.NewsInfo;
                    if (Zone != null)
                        _news.ZoneId = Zone.ZoneId;

                    NewsDetail.News = _news;
                    NewsDetail.Seo = SEOMetaNewsBo.GetSEOMetaNewsById(NewsId);
                    return NewsDetail;
                }
                else
                    return new NewsDetailEntity();
               
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Fatal, ex.ToString());
                return new NewsDetailEntity();
            }
        }

        public static List<NewsPublishForObjectBoxEntity> SearchNewsForNewsRelation(string channelMapping, int zoneId, string keyword, int pageIndex, int pageSize, ref int totalRow, bool showBomb, bool isSearchQuery)
        {
            return NewsBo.SearchNewsForNewsRelation(zoneId, keyword, pageIndex, pageSize, ref totalRow, showBomb, isSearchQuery);
        }
        public static List<NewsEntity> SearchNewsByFocusKeyword(string channelMapping, string keyword, int top)
        {
            return NewsBo.SearchNewsByFocusKeyword(keyword, top);
        }

        public static ErrorMapping.ErrorCodes News_Expert_Update(string channelMapping, NewsExpertEntity news)
        {
            ChannelMapping.SetChannelNamespace(channelMapping);
            try
            {
                var existsNews = NewsDal.GetNewsById(news.NewsId);
                if(existsNews == null)
                {
                    return ErrorMapping.ErrorCodes.UpdateNewsNewsNotFound;
                }
                news.ReviewerName = "Seo_" + news.ReviewerName;


                string TagItem = "";
                if (!string.IsNullOrEmpty(news.Tag))
                {
                    var arrTag = news.Tag.Split(';').ToList().Distinct();
                    foreach (var item in arrTag)
                    {
                        if(!string.IsNullOrEmpty(item))
                        TagItem += ";" + item;
                    }
                    if (TagItem != "")
                        TagItem = TagItem.Substring(1);
                }
                string listTagId = "";
                string TagJSON = "";
                if (TagItem != "")
                {
                    int PrimaryZoneId = 0;
                    string OrtherZoneId = "";

                    var zone = NewsBo.GetNewsInZoneByNewsId(news.NewsId);
                    var ZoneP = zone.FirstOrDefault(k => k.IsPrimary == true);
                    if (ZoneP != null)
                        PrimaryZoneId = ZoneP.ZoneId;

                    var ortherZone = zone.Where(k => k.IsPrimary == false);
                    foreach (var item in ortherZone)
                    {
                        OrtherZoneId += ";"+item.ZoneId;
                    }
                    if (OrtherZoneId != "")
                        OrtherZoneId = OrtherZoneId.Substring(1);
                    TagExpertBo.Tag_Insert(channelMapping, TagItem, news.ReviewerName, PrimaryZoneId, OrtherZoneId, ref listTagId);

                    var tagList = BoFactory.GetInstance<TagBo>().GetTagByListOfTagId(listTagId);
                    var TagFormatJson = bool.Parse(BoConstants.TagFormatJson);
                    //Convert tag về chuỗi json
                    var tagLinkList = "";

                    var tagFormat = BoConstants.NewsUrlFormatForTag;
                    var tagItems = "";
                  
                    if (tagList != null && tagList.Count > 0)
                    {
                        var lstTag = new List<TagJson>();
                        tagList = tagList.Distinct().ToList();
                        foreach (var tag in tagList)
                        {
                            if (TagFormatJson)
                            {
                                var tagTmp = new TagJson
                                {
                                    Id = tag.Id,
                                    Name = tag.Name,
                                    Url = tag.Url
                                };
                                if (lstTag.FirstOrDefault(x => x.Id == tagTmp.Id) != null)
                                {
                                    continue;
                                }
                                lstTag.Add(tagTmp);
                            }
                            else
                            {
                                tagLinkList += "," + string.Format(tagFormat, tag.Url, tag.Id, tag.Name);
                            }
                            tagItems += ";" + tag.Name;
                        }

                        if (TagFormatJson)
                        {
                            TagJSON = NewtonJson.Serialize(lstTag);
                        }
                        else
                        {
                            if (tagLinkList != string.Empty) tagLinkList = tagLinkList.Remove(0, 1);
                        }
                        if (tagItems != string.Empty) tagItems = tagItems.Remove(0, 1);
                        
                    }
                    news.Tag = tagItems;
                    existsNews.TagItem = tagItems;
                    existsNews.Tag = TagJSON;
                }
                else
                {
                    news.Tag = existsNews.TagItem;
                    TagJSON = existsNews.Tag;
                }
                existsNews.Body = news.Body;
                


                var res = NewsExpertDal.UpdateNews(news, listTagId, TagJSON);
                if(res)
                {
                    // Log hành động sửa bài viết
                    NewsBo.UpdateVersion(existsNews, news.ReviewerName);
                    NewsBo.ReleaseVersion(news.NewsId, news.ReviewerName);
                    
                    ActivityBo.LogUpdateNews(existsNews.Id, news.ReviewerName, existsNews.Title, existsNews.Status);
                    CacheObjectBase.GetInstance<NewsCached>().RemoveAllCachedByGroup(news.NewsId.ToString());

                    return ErrorMapping.ErrorCodes.Success;
                }

                return ErrorMapping.ErrorCodes.BusinessError;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Fatal, ex.ToString());
                return ErrorMapping.ErrorCodes.BusinessError;
            }

        }
    }
  
}
