﻿using ChannelVN.CMS.BO.Base.Zone;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.Entity.Base.Zone;
using ChannelVN.ExpertReviewExternal.Dal;
using ChannelVN.ExpertReviewExternal.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChannelVN.ExpertReviewExternal.Bo
{
    public class ZoneExpertBo
    {
        public static List<ZoneEntity> GetAllZone(string channelMapping)
        {
            ChannelMapping.SetChannelNamespace(channelMapping);
            try
            {
                return ZoneBo.GetAllZone();
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Fatal, ex.ToString());
                return new List<ZoneEntity>();
            }
        }
        public static List<ZoneWithSimpleFieldEntity> GetAllZoneActiveWithTreeView(string channelMapping)
        {
            ChannelMapping.SetChannelNamespace(channelMapping);
            try
            {
                return ZoneBo.GetAllZoneActivedWithTreeView(false);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Fatal, ex.ToString());
                return new List<ZoneWithSimpleFieldEntity>();
            }
        }
    }
}
