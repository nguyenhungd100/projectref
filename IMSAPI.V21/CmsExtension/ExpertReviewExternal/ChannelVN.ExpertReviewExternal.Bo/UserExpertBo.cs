﻿using ChannelVN.CMS.BO.Base.Account;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.Entity.Base.Security;
using ChannelVN.ExpertReviewExternal.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ChannelVN.ExpertReviewExternal.Bo
{
    public class UserExpertBo
    {
        public static List<UserStandardEntity> ListAllUser(string channelMapping)
        {
            try
            {
                ChannelMapping.SetChannelNamespace(channelMapping);
                int totalRow = 0;
                var users = UserBo.Search("", UserStatus.Actived, UserSortExpression.UserNameAsc, 1, 1000, ref totalRow);
                foreach (var item in users)
                {
                    item.UserName = "IMS_" + item.UserName;
                }
                return users;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Fatal, ex.ToString());
                return new List<UserStandardEntity>();
            }
        }
    }
}
