﻿using ChannelVN.CMS.BO.Base.Thread;
using ChannelVN.CMS.BO.Common;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.DAL.Base.Thread;
using ChannelVN.CMS.Entity.Base.Thread;
using ChannelVN.ExpertReviewExternal.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ChannelVN.ExpertReviewExternal.Bo
{
    public class ThreadExpertBo
    {
        public static List<ThreadEntity> SearchThread(string channelMapping, string keyword, int zoneId, int pageIndex, int pageSize, ref int totalRow)
        {
            ChannelMapping.SetChannelNamespace(channelMapping);
            try
            {
                return ThreadDal.SearchThread(keyword, zoneId, 0, -1, pageIndex, pageSize, ref totalRow);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.ToString());
            }
            return new List<ThreadEntity>();
        }
        public static ThreadDetailEntity GetTagByTagId(string channelMapping, long threadId)
        {
            ChannelMapping.SetChannelNamespace(channelMapping);
            return BoFactory.GetInstance<ThreadBo>().GetThreadByThreadId(threadId);
        }
    }
}
