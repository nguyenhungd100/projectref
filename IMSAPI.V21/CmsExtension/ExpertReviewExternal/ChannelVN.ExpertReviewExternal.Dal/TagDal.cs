﻿using ChannelVN.ExpertReviewExternal.Entity;
using ChannelVN.ExpertReviewExternal.MainDal.Databases;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ChannelVN.ExpertReviewExternal.Dal
{
    public class TagDal
    {
        public static List<TagExpertEntity> SearchTag(string keyword, long parentTagId, int isThread, int zoneId, int type, int orderBy, int isHotTag, int pageIndex, int pageSize, ref int totalRow, bool getTagHasNewsOnly = false)
        {
            List<TagExpertEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.TagMainDal.SearchTag(keyword, parentTagId, isThread, zoneId, type, orderBy, isHotTag, pageIndex, pageSize, ref totalRow, getTagHasNewsOnly);
            }
            return returnValue;
        }

        public static TagDetailEntity GetTagByTagId(long tagId)
        {
            TagDetailEntity returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.TagMainDal.GetTagByTagId(tagId);
            }
            return returnValue;
        }

        public static List<TagZoneEntity> GetTagNewsByTagId(long tagId)
        {
            List<TagZoneEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.TagMainDal.GetTagNewsByTagId(tagId);
            }
            return returnValue;
        }

        public static List<TagExpertEntity> GetListTagNew(int zoneId, int pageIndex, int pageSize, ref int totalRow)
        {
            List<TagExpertEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.TagMainDal.GetListTagNew(zoneId, pageIndex, pageSize, ref totalRow);
            }
            return returnValue;
        }

        public static bool Tag_Insert(string TagNames, string CreatedBy, int PrimaryZoneId, string OtherZoneId, ref string Ids)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.TagMainDal.Tag_Insert(TagNames, CreatedBy, PrimaryZoneId, OtherZoneId, ref Ids);
            }
            return returnValue;
        }

        public static bool Tag_UpdateStatus(long tagId, EnumTagStatus status)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.TagMainDal.Tag_UpdateStatus(tagId, status);
            }
            return returnValue;
        }
    }
}
