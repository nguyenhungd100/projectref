﻿using ChannelVN.ExpertReviewExternal.Entity;
using ChannelVN.ExpertReviewExternal.MainDal.Databases;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ChannelVN.ExpertReviewExternal.Dal
{
    public class NewsExpertDal
    {
        public static bool UpdateNews(NewsExpertEntity entity, string TagIdList, string TagJSON)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsMainDal.UpdateNews(entity, TagIdList, TagJSON);
            }
            return returnValue;
        }

        public static List<ExpertNewsInListEntity> SearchNewsPublishedByKeyword(string keyword, int pageSize, int pageIndex, ref int TotalRows)
        {
            List<ExpertNewsInListEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsMainDal.SearchNewsPublishedByKeyword(keyword, pageSize, pageIndex, ref TotalRows);
            }
            return returnValue;
        }

        public static List<ExpertNewsInListEntity> SearchNews(string keyword, string zoneIds, DateTime fromDate, DateTime toDate, int pageIndex, int pageSize, ref int totalRow)
        {
            List<ExpertNewsInListEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsMainDal.SearchNews(keyword, zoneIds, fromDate, toDate, pageIndex, pageSize, ref totalRow);
            }
            return returnValue;
        }
    }
}
