﻿using ChannelVN.WcfExtensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace ChannelVN.ExpertReviewExternal.Entity
{
    [DataContract]
    public enum ChannelMappingExpert
    {
        [EnumMember]
        SohaNews = 1,
        [EnumMember]
        GenK = 2,
        [EnumMember]
        TimDiemThi = 3,
        [EnumMember]
        AutoPro = 4,
        [EnumMember]
        SucKhoeDoiSong = 5,
        [EnumMember]
        NguoiLaoDong = 6,
        [EnumMember]
        GameK = 7,
        [EnumMember]
        Kenh14 = 8,
        [EnumMember]
        Afamily = 9,
        [EnumMember]
        VnEconomy = 10,
        [EnumMember]
        GiaDinhNet = 11,
        [EnumMember]
        CafeF = 12,
        [EnumMember]
        CafeBiz = 13,
        [EnumMember]
        DanTri = 14
    }
    public class ChannelMapping
    {
        public static void SetChannelNamespace(string channelMapping)
        {
            WcfMessageHeader.CustomWcfHeader = new WcfMessageHeader
            {
                SecretKey = "",
                ClientUsername = "expert",
                Namespace = channelMapping
            };
        }
    }
}
