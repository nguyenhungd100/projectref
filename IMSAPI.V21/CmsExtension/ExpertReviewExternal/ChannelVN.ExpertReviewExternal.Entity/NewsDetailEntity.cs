﻿using ChannelVN.CMS.Common;
using ChannelVN.CMS.Entity.Base.News;
using ChannelVN.SEO.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace ChannelVN.ExpertReviewExternal.Entity
{
    public class NewsDetailEntity
    {
        public NewsEntity News { get; set; }
        public SEOMetaNewsEntity Seo { get; set; }
    }
    public class NewsExpertEntity
    {
        public long NewsId { get; set; }
        public string Body { get; set; }
        public string MetaTitle { get; set; }
        public string MetaKeyword { get; set; }
        public string MetaDescription { get; set; }
        public string ReviewerName { get; set; }
        public string Tag { get; set; }

    }
    [DataContract]
    public class ExpertNewsInListEntity
    {
        [DataMember]
        public long Id { get; set; }
        [DataMember]
        public string EncryptId { get { return CryptonForId.EncryptId(Id); } set { } }
        [DataMember]
        public string Title { get; set; }
        [DataMember]
        public string Sapo { get; set; }
        [DataMember]
        public string Avatar { get; set; }
        [DataMember]
        public DateTime CreatedDate { get; set; }
        [DataMember]
        public string CreatedBy { get; set; }
        [DataMember]
        public string EditedBy { get; set; }
        [DataMember]
        public DateTime PublishedDate { get; set; }
        [DataMember]
        public string PublishedBy { get; set; }
        [DataMember]
        public DateTime DistributionDate { get; set; }
        [DataMember]
        public string Note { get; set; }
        [DataMember]
        public int ViewCount { get; set; }
        [DataMember]
        public string Url { get; set; }
        [DataMember]
        public long ActiveUsers { get; set; }
        [DataMember]
        public string ZoneName { get; set; }
        [DataMember]
        public int ZoneId { get; set; }
        [DataMember]
        public int MobileCount { get; set; }
        [DataMember]
        public string TagItem { get; set; }
        [DataMember]
        public bool IsOnHome { get; set; }
    }
}
