﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ChannelVN.VCCorp.Dal;
using ChannelVN.VCCorp.Entity;
using ChannelVN.VCCorp.Entity.ErrorCode;

namespace ChannelVN.VCCorp.Bo
{
    public class CurriculumVitaeBo
    {
        public static List<CurriculumVitaeEntity> GetList(string keyword, int type, int status, int pageIndex, int pageSize, ref int totalRow)
        {
            return CurriculumVitaeDal.GetList(keyword, type, status, pageIndex, pageSize, ref totalRow);
        }
        public static ErrorMapping.ErrorCodes UpdateStatus(CurriculumVitaeEntity info)
        {
            return CurriculumVitaeDal.UpdateGiftProgramInfo(info) ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.UnknowError;
        }
        public static CurriculumVitaeEntity GetById(int Id)
        {
            return CurriculumVitaeDal.GetById(Id);
        }
    }
}
