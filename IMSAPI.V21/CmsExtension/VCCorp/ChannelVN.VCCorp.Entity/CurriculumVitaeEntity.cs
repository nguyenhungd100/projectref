﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using ChannelVN.CMS.Common;

namespace ChannelVN.VCCorp.Entity
{
    [DataContract]
    public class CurriculumVitaeEntity : EntityBase
    {
        [DataMember]
        public long Id { set; get; }
        [DataMember]
        public int CvType { set; get; }
        [DataMember]
        public string Name { set; get; }
        [DataMember]
        public string Email { set; get; }
        [DataMember]
        public string Description { set; get; }
        [DataMember]
        public string Attachment { set; get; }
        [DataMember]
        public int Status { set; get; }
        [DataMember]
        public DateTime CreatedDate { set; get; }
        [DataMember]
        public string Mobile { set; get; }
        [DataMember]
        public bool IsHasIntroduction { set; get; }
        [DataMember]
        public bool IsHasProduct { set; get; }
        [DataMember]
        public bool IsHasHighMark { set; get; }
        [DataMember]
        public int SourceReference { set; get; }
    }
}
