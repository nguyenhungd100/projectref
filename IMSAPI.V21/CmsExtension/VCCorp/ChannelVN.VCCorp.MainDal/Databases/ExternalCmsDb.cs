﻿using System.Data;
using System.Data.SqlClient;
using ChannelVN.CMS.Common.ChannelConfig;
using ChannelVN.VCCorp.MainDal.Common;
using ChannelVN.VCCorp.MainDal.Databases;
using ChannelVN.WcfExtensions;

namespace ChannelVN.VCCorp.MainDal.Databases
{
    public class ExternalCmsDb : ExternalCmsDbBase
    {
        private const string ConnectionStringName = "ExternalCmsDb";

        /// <summary>
        /// Creates a new connection to the database.
        /// </summary>
        /// <returns>An <see cref="System.Data.IDbConnection"/> object.</returns>
        protected override IDbConnection CreateConnection()
        {
            var strConn = ServiceChannelConfiguration.GetConnectionString(WcfMessageHeader.Current.Namespace,
                ConnectionStringName, Constants.ConnectionDecryptKey);
            return new SqlConnection(strConn);
        }
    }
}