﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ChannelVN.CMS.Common;
using ChannelVN.VCCorp.Dal.Common;
using ChannelVN.VCCorp.Entity;
using ChannelVN.VCCorp.MainDal.Databases;

namespace ChannelVN.VCCorp.Dal
{
    public class CurriculumVitaeDal
    {
        public static List<CurriculumVitaeEntity> GetList(string keyword, int type, int status,
                                                          int pageIndex, int pageSize, ref int totalRow)
        {
            List<CurriculumVitaeEntity> returnValue;
            using (var db = new ExternalCmsDb())
            {
                returnValue = db.CurriculumVitaeMainDal.GetList(keyword, type, status, pageIndex, pageSize, ref totalRow);
            }
            return returnValue;
        }

        public static bool UpdateGiftProgramInfo(CurriculumVitaeEntity info)
        {
            bool returnValue;
            using (var db = new ExternalCmsDb())
            {
                returnValue = db.CurriculumVitaeMainDal.UpdateStatus(info);
            }
            return returnValue;
        }

        public static CurriculumVitaeEntity GetById(int Id)
        {
            CurriculumVitaeEntity returnValue;
            using (var db = new ExternalCmsDb())
            {
                returnValue = db.CurriculumVitaeMainDal.GetById(Id);
            }
            return returnValue;
        }
    }
}
