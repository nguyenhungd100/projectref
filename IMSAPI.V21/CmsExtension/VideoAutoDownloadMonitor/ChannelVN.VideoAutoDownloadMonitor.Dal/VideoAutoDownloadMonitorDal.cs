﻿using ChannelVN.VideoAutoDownloadMonitor.Entity;
using ChannelVN.VideoAutoDownloadMonitor.MainDal.Databases;
using System.Collections.Generic;

namespace ChannelVN.VideoAutoDownloadMonitor.Dal
{
    public class VideoAutoDownloadMonitorDal
    {
        public static List<VideoAutoDownloadMonitorEntity> GetListByStatus(VideoAutoDownloadMonitorStatus status)
        {
            List<VideoAutoDownloadMonitorEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.VideoAutoDownloadMonitorMainDal.GetListByStatus(status);
            }
            return returnValue;
        }
        public static bool DownloadCompleted(long NewsId, string VideoInfo, string SaveFilePath, string KeyVideo)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.VideoAutoDownloadMonitorMainDal.DownloadCompleted(NewsId, VideoInfo, SaveFilePath, KeyVideo);
            }
            return returnValue;
        }
        public static bool SentDownloadRequest(int Id, string SaveFilePath)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.VideoAutoDownloadMonitorMainDal.SentDownloadRequest(Id, SaveFilePath);
            }
            return returnValue;
        }
        public static bool Update_Status(string Ids, VideoAutoDownloadMonitorStatus Status)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.VideoAutoDownloadMonitorMainDal.Update_Status(Ids, Status);
            }
            return returnValue;
        }
        public static bool Update_CounterCheckDownload(string Ids)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.VideoAutoDownloadMonitorMainDal.Update_CounterCheckDownload(Ids);
            }
            return returnValue;
        }

        public static bool InsertVideoAutoDownloadMonitor(VideoAutoDownloadMonitorEntity videoAutoDownloadMonitor, ref int videoAutoDownloadMonitorId)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.VideoAutoDownloadMonitorMainDal.InsertVideoAutoDownloadMonitor(videoAutoDownloadMonitor, ref videoAutoDownloadMonitorId);
            }
            return returnValue;
        }

        public static VideoAutoDownloadMonitorEntity GetVideoAutoDownloadMonitorByNewsId(long newsId)
        {
            VideoAutoDownloadMonitorEntity returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.VideoAutoDownloadMonitorMainDal.GetVideoAutoDownloadMonitorByNewsId(newsId);
            }
            return returnValue;
        }
    }
}
