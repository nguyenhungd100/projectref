﻿using System.Data;
using ChannelVN.CMS.Common.DalCommon;

namespace ChannelVN.VideoAutoDownloadMonitor.MainDal.Databases
{
    /// <summary>
    /// The base class for the <see cref="CmsMainDb"/> class that 
    /// represents a connection to the <c>CmsMainDb</c> database. 
    /// </summary>
    /// <remarks>
    /// Do not change this source code. Modify the CmsMainDb class
    /// if you need to add or change some functionality.
    /// </remarks>
    public abstract class CmsMainDbBase : MainDbBase
    {
        #region Store procedures

        private VideoAutoDownloadMonitorDal _VideoAutoDownloadMonitorMainDal;
        public VideoAutoDownloadMonitorDal VideoAutoDownloadMonitorMainDal
        {
            get { return _VideoAutoDownloadMonitorMainDal ?? (_VideoAutoDownloadMonitorMainDal = new VideoAutoDownloadMonitorDal((CmsMainDb)this)); }
        }
        #endregion

        #region Constructors

        protected CmsMainDbBase()
        {
        }
        protected CmsMainDbBase(bool init)
        {
            if (init)
            {
                InitConnection();
            }
        }

        #endregion

        #region Protected members

        protected override sealed void InitConnection()
        {
            _connection = CreateConnection();
            _connection.Open();
        }

        /// <summary>
        /// Creates a new connection to the database.
        /// </summary>
        /// <returns>A reference to the <see cref="System.Data.IDbConnection"/> object.</returns>
        protected abstract IDbConnection CreateConnection();

        #endregion
    }
}
