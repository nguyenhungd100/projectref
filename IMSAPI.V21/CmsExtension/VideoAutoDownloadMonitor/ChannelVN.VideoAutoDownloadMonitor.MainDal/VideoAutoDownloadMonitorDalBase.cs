﻿using ChannelVN.CMS.Common;
using ChannelVN.VideoAutoDownloadMonitor.Entity;
using ChannelVN.VideoAutoDownloadMonitor.MainDal.Databases;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace ChannelVN.VideoAutoDownloadMonitor.MainDal
{
    public class VideoAutoDownloadMonitorDalBase
    {
        public List<VideoAutoDownloadMonitorEntity> GetListByStatus(VideoAutoDownloadMonitorStatus status)
        {
            const string commandText = "CMS_VideoAutoDownloadMonitor_GetByStatus";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Status", (int)status);
                var numberOfRow = _db.GetList<VideoAutoDownloadMonitorEntity>(cmd);
                return numberOfRow;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool Update_Status(string Ids, VideoAutoDownloadMonitorStatus Status)
        {
            const string commandText = "CMS_VideoAutoDownloadMonitor_UpdateStatus";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Ids", Ids);
                _db.AddParameter(cmd, "Status", (int)Status);
                var numberOfRow = _db.ExecuteNonQuery(cmd);
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool Update_CounterCheckDownload(string Ids)
        {
            const string commandText = "CMS_VideoAutoDownloadMonitor_UpdateCounterCheckDownload";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Ids", Ids);
                var numberOfRow = _db.ExecuteNonQuery(cmd);
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool DownloadCompleted(long NewsId, string VideoInfo, string SaveFilePath, string KeyVideo)
        {
            const string commandText = "CMS_VideoAutoDownloadMonitor_DownloadCompleted";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "SaveFilePath", SaveFilePath);
                _db.AddParameter(cmd, "NewsId", NewsId);
                _db.AddParameter(cmd, "VideoInfo", VideoInfo);
                _db.AddParameter(cmd, "KeyVideo", KeyVideo);
                var numberOfRow = _db.ExecuteNonQuery(cmd);
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }        

        public bool SentDownloadRequest(int Id, string SaveFilePath)
        {
            const string commandText = "CMS_VideoAutoDownloadMonitor_SentDownloadRequest";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", Id);
                _db.AddParameter(cmd, "SaveFilePath", SaveFilePath);
                var numberOfRow = _db.ExecuteNonQuery(cmd);
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public bool InsertVideoAutoDownloadMonitor(VideoAutoDownloadMonitorEntity videoAutoDownloadMonitor, ref int videoAutoDownloadMonitorId)
        {
            const string commandText = "CMS_VideoAutoDownloadMonitor_Update";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", 0, ParameterDirection.Output);
                _db.AddParameter(cmd, "NewsId", videoAutoDownloadMonitor.NewsId);
                _db.AddParameter(cmd, "SourceUrl", videoAutoDownloadMonitor.SourceUrl);
                _db.AddParameter(cmd, "Status", videoAutoDownloadMonitor.Status);
                int numberOfRow = _db.ExecuteNonQuery(cmd);
                videoAutoDownloadMonitorId = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 0));

                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public VideoAutoDownloadMonitorEntity GetVideoAutoDownloadMonitorByNewsId(long newsId)
        {
            const string commandText = "CMS_VideoAutoDownloadMonitor_GetByNewsId";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "NewsId", newsId);
                VideoAutoDownloadMonitorEntity numberOfRow = _db.Get<VideoAutoDownloadMonitorEntity>(cmd);
                return numberOfRow;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        #region Core members

        private readonly CmsMainDb _db;

        protected VideoAutoDownloadMonitorDalBase(CmsMainDb db)
        {
            _db = db;
        }

        protected CmsMainDb Database
        {
            get { return _db; }
        }

        #endregion
    }
}
