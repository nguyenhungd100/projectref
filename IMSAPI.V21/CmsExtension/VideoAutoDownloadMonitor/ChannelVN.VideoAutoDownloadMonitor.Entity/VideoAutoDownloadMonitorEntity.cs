﻿using ChannelVN.CMS.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace ChannelVN.VideoAutoDownloadMonitor.Entity
{
    public enum VideoAutoDownloadMonitorStatus
    {
        [EnumMember]
        NoActived = 0,
        [EnumMember]
        SendDownload = 1,
        [EnumMember]
        DownloadCompleted = 2,
        [EnumMember]
        UpdateDb = 3,
        [EnumMember]
        DownloadError = 4,
        [EnumMember]
        UpdateDbError = 5,
        [EnumMember]
        Downloading = 6,
    }
    [DataContract]
    public class VideoAutoDownloadMonitorEntity : EntityBase
    {
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public long NewsId { get; set; }
        [DataMember]
        public string SourceUrl { get; set; }
        [DataMember]
        public string SaveFilePath { get; set; }
        [DataMember]
        public string KeyVideo { get; set; }
        [DataMember]
        public int Status { get; set; }
        [DataMember]
        public int CounterCheckDownLoad { get; set; }

    }
}
