﻿using ChannelVN.VideoAutoDownloadMonitor.Dal;
using ChannelVN.VideoAutoDownloadMonitor.Entity;
using System;
using System.Collections.Generic;
using ChannelVN.CMS.Common;
using ChannelVN.VideoAutoDownloadMonitor.Entity.ErrorCode;

namespace ChannelVN.VideoAutoDownloadMonitor.Bo
{
    public class VideoAutoDownloadMonitorBo
    {
        public static List<VideoAutoDownloadMonitorEntity> GetListByStatus(VideoAutoDownloadMonitorStatus status)
        {
            try
            {
                return VideoAutoDownloadMonitorDal.GetListByStatus(status);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return new List<VideoAutoDownloadMonitorEntity>();
            }
        }
        public static ErrorMapping.ErrorCodes DownloadCompleted(long NewsId, string VideoInfo, string SaveFilePath, string KeyVideo)
        {
            try
            {
                return VideoAutoDownloadMonitorDal.DownloadCompleted(NewsId, VideoInfo, SaveFilePath, KeyVideo) ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.UnknowError;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return ErrorMapping.ErrorCodes.BusinessError;
            }
        }
        public static ErrorMapping.ErrorCodes SentDownloadRequest(int Id, string SaveFilePath)
        {
            try
            {
                return VideoAutoDownloadMonitorDal.SentDownloadRequest(Id, SaveFilePath) ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.UnknowError;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return ErrorMapping.ErrorCodes.BusinessError;
            }
        }
        public static ErrorMapping.ErrorCodes Update_Status(string Ids, VideoAutoDownloadMonitorStatus Status)
        {
            try
            {
                return VideoAutoDownloadMonitorDal.Update_Status(Ids, Status) ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.UnknowError;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return ErrorMapping.ErrorCodes.BusinessError;
            }
        }
        public static ErrorMapping.ErrorCodes Update_CounterCheckDownload(string Ids)
        {
            try
            {
                return VideoAutoDownloadMonitorDal.Update_CounterCheckDownload(Ids) ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.UnknowError;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return ErrorMapping.ErrorCodes.BusinessError;
            }
        }

        public static ErrorMapping.ErrorCodes InsertVideoAutoDownloadMonitor(VideoAutoDownloadMonitorEntity videoAutoDownloadMonitor, ref int videoAutoDownloadMonitorId)
        {
            return VideoAutoDownloadMonitorDal.InsertVideoAutoDownloadMonitor(videoAutoDownloadMonitor, ref videoAutoDownloadMonitorId)
                       ? ErrorMapping.ErrorCodes.Success
                       : ErrorMapping.ErrorCodes.BusinessError;
        }

        public static VideoAutoDownloadMonitorEntity GetVideoAutoDownloadMonitorByNewsId(long newsId)
        {
            return VideoAutoDownloadMonitorDal.GetVideoAutoDownloadMonitorByNewsId(newsId);
        }
    }
}
