﻿using ChannelVN.CMS.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace ChannelVN.VTV.Entity
{
    [DataContract]
    public enum EnumStatusVTVGiftProgram
    {
        [EnumMember]
        NoActived = 0,//Không hiệu lực
        [EnumMember]
        Actived = 1 // hiệu lực
    }
    [DataContract]
    public class VTVGiftProgramEntity : EntityBase
    {
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public string NameProgram { get; set; }
        [DataMember]
        public DateTime CreatedDate { get; set; }
        [DataMember]
        public int Status { get; set; }
    }
}
