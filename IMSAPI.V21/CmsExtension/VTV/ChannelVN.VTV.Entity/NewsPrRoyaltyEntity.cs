﻿using ChannelVN.CMS.Common;
using System.Runtime.Serialization;

namespace ChannelVN.VTV.Entity
{
    [DataContract]
    public class NewsPrRoyaltyEntity : EntityBase
    {
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public long NewsId { get; set; }
        [DataMember]
        public string Price { get; set; }
        [DataMember]
        public string Discount { get; set; }
        [DataMember]
        public string TotalAmount { get; set; }
    }
}
