﻿using System;
using System.Runtime.Serialization;
using ChannelVN.CMS.Common;

namespace ChannelVN.VTV.Entity
{
    [DataContract]
    public class VtvVideoThreadEntity : EntityBase
    {
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public string Name { get; set; }
        [DataMember]
        public string Description { get; set; }
        [DataMember]
        public string Url { get; set; }
        [DataMember]
        public bool IsHotThread { get; set; }
        [DataMember]
        public DateTime CreatedDate { get; set; }
        [DataMember]
        public DateTime ModifiedDate { get; set; }
        [DataMember]
        public string CreatedBy { get; set; }
        [DataMember]
        public string EditedBy { get; set; }
        [DataMember]
        public DateTime DistributionDate { get; set; }
        [DataMember]
        public string UnsignName { get; set; }
        [DataMember]
        public string Avatar { get; set; }
        [DataMember]
        public int Status { get; set; } 
    }

    [DataContract]
    public class VtvVideoThreadZoneEntity : EntityBase
    {
        [DataMember]
        public int VideoThreadId { get; set; }
        [DataMember]
        public int VideoZoneId { get; set; }
        [DataMember]
        public bool IsPrimary { get; set; }
    }

    [DataContract]

    public class VtvVideoThreadVideoEntity : EntityBase
    {
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public int VideoThreadId { get; set; }
        [DataMember]
        public int VideoId { get; set; }       
    }

    public class VtvVideoThreadTagEntity : EntityBase
    {       
        [DataMember]
        public int VideoThreadId { get; set; }
        [DataMember]
        public int VideoTagId { get; set; }
        [DataMember]
        public string Name { get; set; }
        [DataMember]
        public string Url { get; set; }
    }
}
