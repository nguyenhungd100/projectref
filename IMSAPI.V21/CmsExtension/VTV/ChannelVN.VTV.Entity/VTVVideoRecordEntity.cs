﻿using ChannelVN.CMS.Common;
using System;
using System.Runtime.Serialization;

namespace ChannelVN.VTV.Entity
{
    [DataContract]
    public enum EnumStatusVTVVideoRecord
    {
        [EnumMember]
        NotStarted = 0,
        [EnumMember]
        Started = 1,
        [EnumMember]
        Stopped = 1,
        [EnumMember]
        Canceled = 1
    }
    [DataContract]
    public class VTVVideoRecordEntity : EntityBase
    {
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public string Name { get; set; }
        [DataMember]
        public int ProgramChannel { get; set; }
        [DataMember]
        public int ProgramScheduleDetailId { get; set; }
        [DataMember]
        public string AccountName { get; set; }
        [DataMember]
        public string FilePath { get; set; }
        [DataMember]
        public int Status { get; set; }
        [DataMember]
        public DateTime CreatedDate { get; set; }
        [DataMember]
        public DateTime CompletedDate { get; set; }
        [DataMember]
        public DateTime StartDate { get; set; }
        [DataMember]
        public DateTime EndDate { get; set; }
        [DataMember]
        public string ChannelName { get; set; }
    }
}
