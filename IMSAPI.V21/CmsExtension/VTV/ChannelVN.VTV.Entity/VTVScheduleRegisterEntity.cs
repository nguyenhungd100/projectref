﻿using ChannelVN.CMS.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace ChannelVN.VTV.Entity
{
    [DataContract]
    public enum EnumTypeVTVScheduleRegister
    {
        [EnumMember]
        NhanLichPhatSong = 1,
        [EnumMember]
        NhanTapChiTH = 2
    }
    [DataContract]
    public enum EnumStatusVTVScheduleRegister
    {
        [EnumMember]
        NoActived = 0,
        [EnumMember]
        Actived = 1
    }
    [DataContract]
    public class VTVScheduleRegisterEntity:EntityBase
    {
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public int Type { get; set; }
        [DataMember]
        public string FullName { get; set; }
        [DataMember]
        public string Email { get; set; }
        [DataMember]
        public DateTime RegisteredDate { get; set; }
        [DataMember]
        public int Status { get; set; }
        [DataMember]
        public int ProgramChannelId { get; set; }
        [DataMember]
        public DateTime FromDate { get; set; }
        [DataMember]
        public DateTime ToDate { get; set; }
    }
}
