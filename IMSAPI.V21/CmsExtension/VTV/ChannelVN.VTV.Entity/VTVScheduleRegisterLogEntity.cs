﻿using System;
using ChannelVN.CMS.Common;

namespace ChannelVN.VTV.Entity
{
    public class VTVScheduleRegisterLogEntity : EntityBase
    {
        public int Id { get; set; }
        public int VTVScheduleRegisterId { get; set; }
        public int ProgramChannelId { get; set; }
        public string Email { get; set; }
        public DateTime SendDate { get; set; }
        public int ScheduleOnWeek { get; set; }
        public int ScheduleOnMonth { get; set; }
        public int ScheduleOnYear { get; set; }
        public string ScheduleFilePath { get; set; }
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
    }
}
