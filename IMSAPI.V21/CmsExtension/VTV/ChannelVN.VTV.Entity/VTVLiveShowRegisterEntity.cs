﻿using ChannelVN.CMS.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace ChannelVN.VTV.Entity
{
    [DataContract]
    public enum EnumStatusVTVLiveShowRegister
    {
        [EnumMember]
        NotVerify = 0,
        [EnumMember]
        Verified = 1
    }
    [DataContract]
    public class VTVLiveShowRegisterEntity : EntityBase
    {
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public string LiveShowRegisterName { get; set; }
        [DataMember]
        public int LiveShowRegisterId { get; set; }
        [DataMember]
        public string RegisterName { get; set; }
        [DataMember]
        public string Nationality { get; set; }
        [DataMember]
        public string IdentityNumber { get; set; }
        [DataMember]
        public DateTime Birthday { get; set; }
        [DataMember]
        public int Gender { get; set; }
        [DataMember]
        public string Email { get; set; }
        [DataMember]
        public string Address { get; set; }
        [DataMember]
        public string Mobile { get; set; }
        [DataMember]
        public string Literacy { get; set; }
        [DataMember]
        public string Job { get; set; }
        [DataMember]
        public DateTime CreatedDate { get; set; }
        [DataMember]
        public int Status { get; set; }
        [DataMember]
        public string Avatar { set; get; }
    }
}
