﻿using ChannelVN.CMS.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace ChannelVN.VTV.Entity
{
    [DataContract]
    public enum EnumStatusVTVGiftRegister
    {
        [EnumMember]
        All = -1,
        [EnumMember]
        NoActived = 0,
        [EnumMember]
        Actived = 1,
        [EnumMember]
        Sended = 2,
    }
    [DataContract]
    public class VTVGiftRegisterEntity : EntityBase
    {
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public int GiftId { get; set; }
        [DataMember]
        public string FullName { get; set; }
        [DataMember]
        public string Address { get; set; }
        [DataMember]
        public string IdentityCard { get; set; }
        [DataMember]
        public string PhoneNumber { get; set; }
        [DataMember]
        public string Email { get; set; }
        [DataMember]
        public int Status { get; set; }
        [DataMember]
        public DateTime SendDate { get; set; }
        [DataMember]
        public string GiftName { get; set; }
    }
}
