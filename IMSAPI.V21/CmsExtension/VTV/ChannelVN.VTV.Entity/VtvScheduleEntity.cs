﻿using System;
using ChannelVN.CMS.Common;
using System.Runtime.Serialization;

namespace ChannelVN.VTV.Entity
{
    [DataContract]
    public class VtvScheduleEntity : EntityBase
    {
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public int ChannelId { get; set; }
        [DataMember]
        public string ScheduleName { get; set; }
        [DataMember]
        public DateTime StartDate { get; set; }
    }
}
