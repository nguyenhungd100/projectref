﻿using System;
using ChannelVN.CMS.Common;
using System.Runtime.Serialization;

namespace ChannelVN.VTV.Entity
{
    [DataContract]
    public class VTVProgramScheduleDetailEntity : EntityBase
    {
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public int ProgramScheduleId { get; set; }
        [DataMember]
        public DateTime ScheduleTime { get; set; }
        [DataMember]
        public string Title { get; set; }
        [DataMember]
        public string Description { get; set; }
        [DataMember]
        public string Avatar { get; set; }
        [DataMember]
        public int VideoId { get; set; }
        [DataMember]
        public string CreatedBy { get; set; }
        [DataMember]
        public DateTime CreatedDate { get; set; }
        [DataMember]
        public DateTime LastModifiedDate { get; set; }
        [DataMember]
        public string LastModifiedBy { get; set; }
        [DataMember]
        public int Status { get; set; }
        [DataMember]
        public bool ShowOnSchedule { get; set; }
    }
}
