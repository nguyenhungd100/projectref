﻿using ChannelVN.CMS.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace ChannelVN.VTV.Entity
{
    [DataContract]
    public enum EnumStatusBoxCountDownChannel
    {
        [EnumMember]
        NoActived = 0,//Không hiệu lực
        [EnumMember]
        Actived = 1 // hiệu lực
    }
    [DataContract]
    public class BoxCountDownChannelEntity : EntityBase
    {
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public int ZoneId { get; set; }
        [DataMember]
        public string Title { get; set; }
        [DataMember]
        public DateTime CountDownTime { get; set; }
        [DataMember]
        public bool IsActive { get; set; }
        [DataMember]
        public string Avatar { get; set; }
        [DataMember]
        public string FileName { get; set; }
        [DataMember]
        public string VideoKey { get; set; }
        [DataMember]
        public string Description { get; set; }
        [DataMember]
        public string NewsRelation { get; set; }
    }
}
