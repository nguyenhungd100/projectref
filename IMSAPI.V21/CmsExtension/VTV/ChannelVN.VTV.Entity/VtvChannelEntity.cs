﻿using ChannelVN.CMS.Common;
using System.Runtime.Serialization;

namespace ChannelVN.VTV.Entity
{
    [DataContract]
    public class VtvChannelEntity : EntityBase
    {
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public string ChannelName { get; set; }
        [DataMember]
        public bool Status { get; set; }  
    }
}
