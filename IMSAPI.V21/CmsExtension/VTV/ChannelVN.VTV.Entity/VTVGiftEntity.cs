﻿using ChannelVN.CMS.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace ChannelVN.VTV.Entity
{
    [DataContract]
    public enum EnumTypeVTVGift
    {
        [EnumMember]
        NhanvethamduCT = 1,
        [EnumMember]
        Nhanquatang = 2
    }
    [DataContract]
    public enum EnumStatusVTVGift
    {
        [EnumMember]
        NoActived = 0,
        [EnumMember]
        Actived = 1
    }
    [DataContract]
    public class VTVGiftEntity : EntityBase
    {
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public int IdGiftProgram { get; set; }
        [DataMember]
        public int TypeGift { get; set; }
        [DataMember]
        public string Name { get; set; }
        [DataMember]
        public string Description { get; set; }
        [DataMember]
        public string Avatar { get; set; }
        [DataMember]
        public DateTime StartDate { get; set; }
        [DataMember]
        public DateTime EndDate { get; set; }
        [DataMember]
        public int Quantity { get; set; }
        [DataMember]
        public int MaxQuantity { get; set; }
        [DataMember]
        public int Status { get; set; }
        [DataMember]
        public string CreatedBy { get; set; }
        [DataMember]
        public string LastModifiedBy { get; set; }
        [DataMember]
        public DateTime CreatedDate { get; set; }
        [DataMember]
        public DateTime LastModifiedDate { get; set; }
        [DataMember]
        public string NameProgram { get; set; }
    }
}
