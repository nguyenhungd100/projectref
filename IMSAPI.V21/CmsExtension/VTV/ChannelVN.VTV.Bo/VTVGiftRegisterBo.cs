﻿using ChannelVN.VTV.Dal;
using ChannelVN.VTV.Entity;
using ChannelVN.VTV.Entity.ErrorCode;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ChannelVN.VTV.Bo
{
    public class VTVGiftRegisterBo
    {
        /// <summary>
        /// Lấy toàn bộ danh sách đăng ký nhận quà tặng
        /// </summary>
        /// <param name="keyword">string</param>
        /// <param name="giftId">int</param>
        /// <param name="status">int</param>
        /// <param name="from">Datetime</param>
        /// <param name="to">Datetime</param>
        /// <param name="order">int</param>
        /// <param name="pageIndex">int</param>
        /// <param name="pageSize">int</param>
        /// <param name="totalRow">int output</param>
        /// <returns>list Entity</returns>
        public static List<VTVGiftRegisterEntity> ListGiftRegister(string keyword, int giftId, int status, DateTime from, DateTime to, int order,
                                                          int pageIndex, int pageSize, ref int totalRow)
        {
            return VTVGiftRegisterDal.ListGiftRegister(keyword, giftId, status, from, to,order, pageIndex, pageSize, ref totalRow);
        }
        /// <summary>
        /// Lấy thông tin chi tiết quà tặng
        /// </summary>
        /// <param name="Id">int</param>
        /// <returns>Entity</returns>
        public static VTVGiftRegisterEntity GetGiftRegisterById(int Id)
        {
            return VTVGiftRegisterDal.GetGiftRegisterById(Id);
        }
        /// <summary>
        /// Cập nhật thông tin quà tặng
        /// </summary>
        /// <param name="info">Entity</param>
        /// <returns>true/false</returns>
        public static ErrorMapping.ErrorCodes UpdateGiftRegister(VTVGiftRegisterEntity info)
        {
            return VTVGiftRegisterDal.UpdateGiftRegister(info) ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.UnknowError;
        }
        
        /// <summary>
        /// Xóa thông tin chương trình quà tặng. Kiểm tra nếu là quyền thư ký (ArticleAdmin) thì mới cho phép xóa
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public static ErrorMapping.ErrorCodes DeleteGiftRegister(int id)
        {
            return VTVGiftRegisterDal.DeleteGiftRegister(id) ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.UnknowError;
        }
    }
}
