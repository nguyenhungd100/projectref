﻿using ChannelVN.VTV.Dal;
using ChannelVN.VTV.Entity;
using ChannelVN.VTV.Entity.ErrorCode;
using System.Collections.Generic;

namespace ChannelVN.VTV.Bo
{
    public class NewsPrRoyaltyBo
    {
        public static NewsPrRoyaltyEntity GetByNewsId(long newsId)
        {
            return NewsPrRoyaltyDal.GetByNewsId(newsId);
        }
        public static ErrorMapping.ErrorCodes Update(NewsPrRoyaltyEntity elm)
        {
            return NewsPrRoyaltyDal.Update(elm) ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.UnknowError;
        }
    }
}
