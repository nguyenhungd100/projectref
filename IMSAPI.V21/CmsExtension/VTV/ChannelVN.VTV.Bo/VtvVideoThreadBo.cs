﻿using ChannelVN.VTV.Dal;
using ChannelVN.VTV.Entity;
using ChannelVN.VTV.Entity.ErrorCode;
using System.Collections.Generic;

namespace ChannelVN.VTV.Bo
{
    public class VtvVideoThreadBo
    {
        #region VIDEO
        public static List<VtvVideoThreadEntity> GetListVideoThread(string keyword,int isHot, int pageIndex, int pageSize, ref int totalRow)
        {
            return VtvVideoThreadDal.GetList(keyword, isHot, pageIndex, pageSize, ref totalRow);
        }
        public static ErrorMapping.ErrorCodes InsertVideoThread(VtvVideoThreadEntity elm, ref int id)
        {
            return VtvVideoThreadDal.Insert(elm, ref id) ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.UnknowError;
        }
        public static ErrorMapping.ErrorCodes UpdateVideoThread(VtvVideoThreadEntity elm)
        {
            return VtvVideoThreadDal.Update(elm) ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.UnknowError;
        }
        public static ErrorMapping.ErrorCodes DeleteVideoThread(int id)
        {
            return VtvVideoThreadDal.Delete(id) ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.UnknowError;
        }
        public static VtvVideoThreadEntity SelectVideoThread(int id)
        {
            return VtvVideoThreadDal.Select(id);
        }
        public static ErrorMapping.ErrorCodes IsHotVideoThread(int id)
        {
            return VtvVideoThreadDal.IsHot(id) ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.UnknowError;
        }
        #endregion

        #region ZONE
        public static ErrorMapping.ErrorCodes InsertVideoThreadZone(VtvVideoThreadZoneEntity elm)
        {
            return VtvVideoThreadDal.InsertZone(elm) ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.UnknowError;
        }
        public static VtvVideoThreadZoneEntity SelectVideoThreadZone(int videoThreadId, bool isPrimary)
        {
            return VtvVideoThreadDal.SelectZone(videoThreadId, isPrimary);
        }
        public static ErrorMapping.ErrorCodes DeleteVideoThreadZone(int videoThreadId)
        {
            return VtvVideoThreadDal.DeleteZone(videoThreadId) ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.UnknowError;
        }
        #endregion

        #region VIDEO IN VIDEO
        public static List<VtvVideoThreadVideoEntity> GetListVideoThreadVideo(int videoThreadId, int pageIndex, int pageSize, ref int totalRow)
        {
            return VtvVideoThreadDal.ListVideo(videoThreadId,pageIndex,pageSize,ref totalRow);
        }
        public static ErrorMapping.ErrorCodes InsertVideoThreadVideo(VtvVideoThreadVideoEntity elm)
        {
            return VtvVideoThreadDal.InsertVideo(elm) ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.UnknowError;
        }
        public static ErrorMapping.ErrorCodes DeleteVideoThreadVideo(int videoThreadId, int videoId)
        {
            return VtvVideoThreadDal.DeleteVideo(videoThreadId,videoId) ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.UnknowError;
        }
        public static VtvVideoThreadVideoEntity ExistVideoThreadZone(int videoThreadId, int videoId)
        {
            return VtvVideoThreadDal.ExistVideo(videoThreadId, videoId);
        }
        #endregion

        #region TAG
        public static List<VtvVideoThreadTagEntity> GetListVideoThreadTag(int videoThreadId)
        {
            return VtvVideoThreadDal.ListTag(videoThreadId);
        }
        public static ErrorMapping.ErrorCodes InsertVideoThreadTag(VtvVideoThreadTagEntity elm)
        {
            return VtvVideoThreadDal.InsertTag(elm) ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.UnknowError;
        }
        public static ErrorMapping.ErrorCodes DeleteVideoThreadTag(int videoThreadId)
        {
            return VtvVideoThreadDal.DeleteTag(videoThreadId) ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.UnknowError;
        }     
        #endregion
    }
}
