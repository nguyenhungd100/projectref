﻿using ChannelVN.VTV.Dal;
using ChannelVN.VTV.Entity;
using ChannelVN.VTV.Entity.ErrorCode;
using System;
using System.Collections.Generic;

namespace ChannelVN.VTV.Bo
{
    public class VtvScheduleBo
    {
        public static List<VtvScheduleEntity> GetListSchedule(string keyword, int channelId, DateTime filterDate,int pageIndex, int pageSize, ref int totalRow)
        {
            return VtvScheduleDal.GetList(keyword, channelId,filterDate, pageIndex, pageSize, ref totalRow);
        }
        public static ErrorMapping.ErrorCodes InsertSchedule(VtvScheduleEntity elm, ref int id)
        {
            return VtvScheduleDal.Insert(elm, ref id) ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.UnknowError;
        }
        public static ErrorMapping.ErrorCodes UpdateSchedule(VtvScheduleEntity elm)
        {
            return VtvScheduleDal.Update(elm) ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.UnknowError;
        }
        public static VtvScheduleEntity SelectSchedule(int id)
        {
            return VtvScheduleDal.Select(id);
        }
        public static ErrorMapping.ErrorCodes DeleteSchedule(int id)
        {
            return VtvScheduleDal.Delete(id) ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.UnknowError;
        }
    }
}
