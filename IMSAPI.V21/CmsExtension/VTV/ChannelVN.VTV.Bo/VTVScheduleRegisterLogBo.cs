﻿using ChannelVN.VTV.Dal;
using ChannelVN.VTV.Entity;
using ChannelVN.VTV.Entity.ErrorCode;
using System;
using System.Collections.Generic;

namespace ChannelVN.VTV.Bo
{
    public class VTVScheduleRegisterLogBo
    {
        /// <summary>
        /// Lấy danh sách log gửi mail
        /// Dữ liệu được lấy từ DB ngoài (VTV_EXT)
        /// </summary>
        /// <param name="scheduleRegisterId">int</param>
        /// <param name="pageIndex">int</param>
        /// <param name="pageSize">int</param>
        /// <param name="totalRow">int</param>
        /// <returns>List Entity</returns>
        public static List<VTVScheduleRegisterLogEntity> ListScheduleRegisterLog( int scheduleRegisterId, int pageIndex, int pageSize, ref int totalRow)
        {
            return VTVScheduleRegisterLogDal.ListScheduleRegisterLog(scheduleRegisterId, pageIndex, pageSize, ref totalRow);
        }

        /// <summary>
        /// </summary>
        /// <param name="entity"></param>
        /// <param name="id"></param>
        /// <returns>true/false</returns>
        public static ErrorMapping.ErrorCodes Insert(VTVScheduleRegisterLogEntity entity, ref int id)
        {
            return VTVScheduleRegisterLogDal.Insert(entity, ref id) ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.UnknowError;
        }
    }
}
