﻿using ChannelVN.VTV.Dal;
using ChannelVN.VTV.Entity;
using ChannelVN.VTV.Entity.ErrorCode;
using System.Collections.Generic;

namespace ChannelVN.VTV.Bo
{
    public class VTVProgramScheduleBo
    {
        public static ErrorMapping.ErrorCodes InsertSchedule(VTVProgramScheduleEntity entity, ref int id)
        {
            return VTVProgramScheduleDal.Insert(entity, ref id) ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.UnknowError;
        }
    }
}
