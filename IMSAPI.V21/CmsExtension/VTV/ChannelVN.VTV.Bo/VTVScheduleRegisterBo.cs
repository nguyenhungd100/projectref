﻿using ChannelVN.VTV.Dal;
using ChannelVN.VTV.Entity;
using ChannelVN.VTV.Entity.ErrorCode;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ChannelVN.VTV.Bo
{
    public class VTVScheduleRegisterBo
    {
        /// <summary>
        /// Lấy danh sách người dùng đăng ký nhận thông tin từ VTV (Lịch phát sóng, Tạp chí truyền hình)
        /// Dữ liệu được lấy từ DB ngoài (VTV_EXT)
        /// </summary>
        /// <param name="keyword">string</param>
        /// <param name="registeredDate">Datetime</param>
        /// <param name="type">int</param>
        /// <param name="status">int</param>
        /// <param name="pageIndex">int</param>
        /// <param name="pageSize">int</param>
        /// <param name="totalRow">int</param>
        /// <returns>List Entity</returns>
        public static List<VTVScheduleRegisterEntity> ListScheduleRegister(string keyword, DateTime registeredDate,
                                                    int type, int status, int order, int pageIndex, int pageSize, ref int totalRow)
        {
            return VTVScheduleRegisterDal.ListScheduleRegister(keyword, registeredDate, type, status,order ,pageIndex, pageSize, ref totalRow);
        }
        /// <summary>
        /// Lấy thông tin người đăng ký nhận quà tặng
        /// </summary>
        /// <param name="Id">int</param>
        /// <returns>Entity</returns>
        public static VTVScheduleRegisterEntity GetScheduleRegisterById(int Id)
        {
            return VTVScheduleRegisterDal.GetScheduleRegisterById(Id);
        }
        /// <summary>
        /// Cập nhật thông tin ScheduleRegister
        /// </summary>
        /// <param name="info">Entity</param>
        /// <returns>true/false</returns>
        public static ErrorMapping.ErrorCodes UpdateScheduleRegisterInfo(VTVScheduleRegisterEntity info)
        {
            return VTVScheduleRegisterDal.UpdateScheduleRegisterInfo(info) ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.UnknowError;
        }
    }
}
