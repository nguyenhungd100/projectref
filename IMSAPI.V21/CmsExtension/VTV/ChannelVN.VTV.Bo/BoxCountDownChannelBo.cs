﻿using ChannelVN.VTV.Dal;
using ChannelVN.VTV.Entity;
using ChannelVN.VTV.Entity.ErrorCode;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ChannelVN.VTV.Bo
{
    public class BoxCountDownChannelBo
    {
        /// <summary>
        /// Lấy toàn bộ danh sách chương trình quà tặng
        /// </summary>
        /// <param name="keyword">string</param>
        /// <param name="status">int</param>
        /// <param name="pageIndex">int</param>
        /// <param name="pageSize">int</param>
        /// <param name="totalRow">int output</param>
        /// <returns>list Entity</returns>
        public static List<BoxCountDownChannelEntity> ListBoxCountDown(string keyword,
                                                                            int status, int pageIndex, int pageSize, ref int totalRow)
        {
            return BoxCountDownChannelDal.ListBoxCountDown(keyword, status, pageIndex, pageSize, ref totalRow);
        }
        public static List<BoxCountDownChannelEntity> ListAllBoxCountDown()
        {
            return BoxCountDownChannelDal.ListBoxCountDown();
        }
        /// <summary>
        /// Lấy thông tin chi tiết chương trình quà tặng
        /// </summary>
        /// <param name="Id">int</param>
        /// <returns>Entity</returns>
        public static BoxCountDownChannelEntity GetBoxCountDownById(int Id)
        {
            return BoxCountDownChannelDal.GetBoxCountDownById(Id);
        }
        /// <summary>
        /// Cập nhật thông tin chương trình quà tặng
        /// </summary>
        /// <param name="info">Entity</param>
        /// <returns>true/false</returns>
        public static ErrorMapping.ErrorCodes UpdateBoxCountDownInfo(BoxCountDownChannelEntity info)
        {
            return BoxCountDownChannelDal.UpdateBoxCountDownInfo(info) ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.UnknowError;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="info"></param>
        /// <returns></returns>
        public static ErrorMapping.ErrorCodes InsertBoxCountDown(BoxCountDownChannelEntity info, ref int id)
        {
            return BoxCountDownChannelDal.InsertBoxCountDownInfo(info, ref id) ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.UnknowError;
        }
        /// <summary>
        /// Xóa thông tin chương trình quà tặng. Kiểm tra nếu là quyền thư ký (ArticleAdmin) thì mới cho phép xóa
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public static ErrorMapping.ErrorCodes DeleteBoxCountDown(int Id)
        {
            return BoxCountDownChannelDal.DeleteBoxCountDown(Id) ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.UnknowError;
        }
    }
}
