﻿using ChannelVN.VTV.Dal;
using ChannelVN.VTV.Entity;
using ChannelVN.VTV.Entity.ErrorCode;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ChannelVN.VTV.Bo
{
    public class VTVLiveShowRegisterBo
    {
        public static List<VTVLiveShowRegisterEntity> ListLiveShowRegister(string keyword, int status,
                                                          int pageIndex, int pageSize, ref int totalRow)
        {
            return VTVLiveShowRegisterDal.ListLiveShowRegister(keyword, status, pageIndex, pageSize, ref totalRow);
        }
        public static ErrorMapping.ErrorCodes UpdateLiveShowRegister(VTVLiveShowRegisterEntity info)
        {
            return VTVLiveShowRegisterDal.Update(info) ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.UnknowError;
        }
        public static VTVLiveShowRegisterEntity GetLiveShowRegisterById(int Id)
        {
            return VTVLiveShowRegisterDal.GetLiveShowRegisterById(Id);
        }
        public static ErrorMapping.ErrorCodes UpdateStatusLiveShowRegister(VTVLiveShowRegisterEntity info)
        {
            return VTVLiveShowRegisterDal.UpdateStatus(info) ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.UnknowError;
        }
    }
}
