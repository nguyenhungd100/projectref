﻿using ChannelVN.VTV.Dal;
using ChannelVN.VTV.Entity;
using ChannelVN.VTV.Entity.ErrorCode;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ChannelVN.VTV.Bo
{
    public class VTVGiftProgramBo
    {
        /// <summary>
        /// Lấy toàn bộ danh sách chương trình quà tặng
        /// </summary>
        /// <param name="keyword">string</param>
        /// <param name="status">int</param>
        /// <param name="pageIndex">int</param>
        /// <param name="pageSize">int</param>
        /// <param name="totalRow">int output</param>
        /// <returns>list Entity</returns>
        public static List<VTVGiftProgramEntity> ListGiftProgram(string keyword,
                                                                            int status, int pageIndex, int pageSize, ref int totalRow)
        {
            return VTVGiftProgramDal.ListGiftProgram(keyword, status, pageIndex, pageSize, ref totalRow);
        }
        public static List<VTVGiftProgramEntity> ListAllGiftProgram()
        {
            return VTVGiftProgramDal.ListGiftProgram();
        }
        /// <summary>
        /// Lấy thông tin chi tiết chương trình quà tặng
        /// </summary>
        /// <param name="Id">int</param>
        /// <returns>Entity</returns>
        public static VTVGiftProgramEntity GetGiftProgramById(int Id)
        {
            return VTVGiftProgramDal.GetGiftProgramById(Id);
        }
        /// <summary>
        /// Cập nhật thông tin chương trình quà tặng
        /// </summary>
        /// <param name="info">Entity</param>
        /// <returns>true/false</returns>
        public static ErrorMapping.ErrorCodes UpdateGiftProgramInfo(VTVGiftProgramEntity info)
        {
            return VTVGiftProgramDal.UpdateGiftProgramInfo(info) ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.UnknowError;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="info"></param>
        /// <returns></returns>
        public static ErrorMapping.ErrorCodes InsertGiftProgramInfo(VTVGiftProgramEntity info, ref int id)
        {
            return VTVGiftProgramDal.InsertGiftProgramInfo(info, ref id) ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.UnknowError;
        }
        /// <summary>
        /// Xóa thông tin chương trình quà tặng. Kiểm tra nếu là quyền thư ký (ArticleAdmin) thì mới cho phép xóa
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public static ErrorMapping.ErrorCodes DeleteGiftProgram(int Id)
        {
            return VTVGiftProgramDal.DeleteGiftProgram(Id) ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.UnknowError;
        }
    }
}
