﻿using ChannelVN.CMS.Common;
using ChannelVN.VTV.Dal;
using ChannelVN.VTV.Entity;
using ChannelVN.VTV.Entity.ErrorCode;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ChannelVN.VTV.Bo
{
    public class VTVVideoRecordBo
    {
        public static List<VTVVideoRecordEntity> ListVideoRecord(string keyword,int programChannel, string accountName, int status,
                                                        DateTime fromDate, DateTime toDate, int pageIndex, int pageSize, ref int totalRow)
        {
            return VTVVideoRecordDal.ListVideoRecord(keyword,programChannel, accountName, status,fromDate,toDate, pageIndex, pageSize, ref totalRow);
        }
        public static ErrorMapping.ErrorCodes UpdateStatus(VTVVideoRecordEntity info)
        {
            return VTVVideoRecordDal.UpdateStatus(info) ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.UnknowError;
        }
        public static ErrorMapping.ErrorCodes Insert(VTVVideoRecordEntity info, ref int id)
        {
            return VTVVideoRecordDal.Insert(info, ref id) ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.UnknowError;
        }
        public static List<VTVProgramChannelEntity> ListChannel(int status)
        {
            try
            {
                return VTVVideoRecordDal.ListChannel(status);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Fatal, ex.ToString());
            }
            return new List<VTVProgramChannelEntity>();
        }
        public static ErrorMapping.ErrorCodes UpdateStatusByChannel(int channelId, int status)
        {
            return VTVVideoRecordDal.UpdateStatus(channelId, status) ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.UnknowError;
        }
        public static VTVVideoRecordEntity GetVideoRecordById(int id)
        {
            return VTVVideoRecordDal.GetVideoRecordById(id);
        }
    }
}
