﻿using ChannelVN.VTV.Dal;
using ChannelVN.VTV.Entity;
using ChannelVN.VTV.Entity.ErrorCode;
using System.Collections.Generic;

namespace ChannelVN.VTV.Bo
{
    public class VtvChannelBo
    {
        public static List<VtvChannelEntity> GetListChannel(string keyword, int pageIndex, int pageSize, ref int totalRow)
        {
            return VtvChannelDal.GetList(keyword, pageIndex, pageSize, ref totalRow);
        }
        public static ErrorMapping.ErrorCodes InsertChannel(VtvChannelEntity elm, ref int id)
        {
            return VtvChannelDal.Insert(elm, ref id) ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.UnknowError;
        }
        public static ErrorMapping.ErrorCodes UpdateChannel(VtvChannelEntity elm)
        {
            return VtvChannelDal.Update(elm) ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.UnknowError;
        }
        public static VtvChannelEntity SelectChannel(int id)
        {
            return VtvChannelDal.Select(id);
        }
        public static ErrorMapping.ErrorCodes DeleteChannel(int id)
        {
            return VtvChannelDal.Delete(id) ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.UnknowError;
        }
    }
}
