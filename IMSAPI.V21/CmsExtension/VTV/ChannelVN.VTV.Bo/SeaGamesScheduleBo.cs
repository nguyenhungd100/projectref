﻿using ChannelVN.VTV.Dal;
using ChannelVN.VTV.Entity;
using ChannelVN.VTV.Entity.ErrorCode;
using System.Collections.Generic;

namespace ChannelVN.VTV.Bo
{
    public class SeaGamesScheduleBo
    {
        public static List<SeaGamesScheduleEntity> GetList(string keyword, int pageIndex, int pageSize, ref int totalRow)
        {
            return SeaGamesScheduleDal.GetList(keyword, pageIndex, pageSize, ref totalRow);
        }
        //public static ErrorMapping.ErrorCodes InsertChannel(SeaGamesScheduleEntity elm, ref int id)
        //{
        //    return SeaGamesScheduleDal.Insert(elm, ref id) ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.UnknowError;
        //}
        public static ErrorMapping.ErrorCodes Update(SeaGamesScheduleEntity elm)
        {
            return SeaGamesScheduleDal.Update(elm) ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.UnknowError;
        }
        public static SeaGamesScheduleEntity Select(int id)
        {
            return SeaGamesScheduleDal.Select(id);
        }
        public static ErrorMapping.ErrorCodes Delete(int id)
        {
            return SeaGamesScheduleDal.Delete(id) ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.UnknowError;
        }
    }
}
