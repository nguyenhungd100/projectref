﻿using ChannelVN.VTV.Dal;
using ChannelVN.VTV.Entity;
using ChannelVN.VTV.Entity.ErrorCode;
using System.Collections.Generic;

namespace ChannelVN.VTV.Bo
{
    public class VTVProgramScheduleDetailBo
    {
        public static ErrorMapping.ErrorCodes InsertSchedule(VTVProgramScheduleDetailEntity entity, ref int id)
        {
            return VTVProgramScheduleDetailDal.Insert(entity, ref id) ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.UnknowError;
        }
    }
}
