﻿using ChannelVN.VTV.Dal;
using ChannelVN.VTV.Entity;
using ChannelVN.VTV.Entity.ErrorCode;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ChannelVN.VTV.Bo
{
    public class VTVGiftBo
    {
        /// <summary>
        /// Lấy toàn bộ danh sách quà tặng
        /// </summary>
        /// <param name="keyword">string</param>
        /// <param name="giftProgramId">int</param>
        /// <param name="status">int</param>
        /// <param name="from">Datetime</param>
        /// <param name="to">Datetime</param>
        /// <param name="pageIndex">int</param>
        /// <param name="pageSize">int</param>
        /// <param name="totalRow">int output</param>
        /// <returns>list Entity</returns>
        public static List<VTVGiftEntity> ListGift(string keyword, int giftProgramId, int status, DateTime from, DateTime to,
                                                          int pageIndex, int pageSize, ref int totalRow)
        {
            return VTVGiftDal.ListGift(keyword, giftProgramId, status, from, to, pageIndex, pageSize, ref totalRow);
        }
        public static List<VTVGiftEntity> ListGift() {
            return VTVGiftDal.ListGift();
        }
        /// <summary>
        /// Lấy thông tin chi tiết quà tặng
        /// </summary>
        /// <param name="Id">int</param>
        /// <returns>Entity</returns>
        public static VTVGiftEntity GetGiftById(int Id)
        {
            return VTVGiftDal.GetGiftById(Id);
        }
        /// <summary>
        /// Cập nhật thông tin quà tặng
        /// </summary>
        /// <param name="info">Entity</param>
        /// <returns>true/false</returns>
        public static ErrorMapping.ErrorCodes UpdateGiftInfo(VTVGiftEntity info)
        {
            return VTVGiftDal.UpdateGiftInfo(info) ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.UnknowError;
        }
        /// <summary>
        /// tạo mới quà tặng
        /// </summary>
        /// <param name="info">Entity</param>
        /// <returns>true/false</returns>
        public static ErrorMapping.ErrorCodes InsertGiftInfo(VTVGiftEntity info, ref int id)
        {
            return VTVGiftDal.InsertGiftInfo(info, ref id) ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.UnknowError;
        }
        /// <summary>
        /// Xóa thông tin chương trình quà tặng. Kiểm tra nếu là quyền thư ký (ArticleAdmin) thì mới cho phép xóa
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public static ErrorMapping.ErrorCodes DeleteGift(int id)
        {
            return VTVGiftDal.DeleteGift(id) ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.UnknowError;
        }
    }
}
