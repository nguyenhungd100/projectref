﻿using ChannelVN.CMS.Common;
using ChannelVN.VTV.Dal.Common;
using ChannelVN.VTV.Entity;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using ChannelVN.VTV.MainDal.Databases;

namespace ChannelVN.VTV.Dal
{
    public class VTVGiftRegisterDal
    {
        /// <summary>
        /// Lấy toàn bộ danh sách đăng ký nhận quà tặng
        /// </summary>
        /// <param name="keyword">string</param>
        /// <param name="giftId">int</param>
        /// <param name="status">int</param>
        /// <param name="from">Datetime</param>
        /// <param name="to">Datetime</param>
        /// <param name="order">int</param>
        /// <param name="pageIndex">int</param>
        /// <param name="pageSize">int</param>
        /// <param name="totalRow">int output</param>
        /// <returns>list Entity</returns>
        public static List<VTVGiftRegisterEntity> ListGiftRegister(string keyword, int giftId, int status, DateTime from, DateTime to, int order,
                                                          int pageIndex, int pageSize, ref int totalRow)
        {
            List<VTVGiftRegisterEntity> returnValue;
            using (var db = new ExternalCmsDb())
            {
                returnValue = db.VTVGiftRegisterMainDal.ListGiftRegister(keyword, giftId, status, from, to, order, pageIndex, pageSize, ref totalRow);
            }
            return returnValue;
        }
        /// <summary>
        /// Lấy thông tin chi tiết quà tặng
        /// </summary>
        /// <param name="Id">int</param>
        /// <returns>Entity</returns>
        public static VTVGiftRegisterEntity GetGiftRegisterById(int Id)
        {
            VTVGiftRegisterEntity returnValue;
            using (var db = new ExternalCmsDb())
            {
                returnValue = db.VTVGiftRegisterMainDal.GetGiftRegisterById(Id);
            }
            return returnValue;
        }
        /// <summary>
        /// Cập nhật thông tin đăng ký quà tặng
        /// </summary>
        /// <param name="info">Entity</param>
        /// <returns>true/false</returns>
        public static bool UpdateGiftRegister(VTVGiftRegisterEntity info)
        {
            bool returnValue;
            using (var db = new ExternalCmsDb())
            {
                returnValue = db.VTVGiftRegisterMainDal.UpdateGiftRegister(info);
            }
            return returnValue;
        }
        /// <summary>
        /// Xóa thông tin. Kiểm tra nếu là quyền thư ký (ArticleAdmin) thì mới cho phép xóa
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public static bool DeleteGiftRegister(int id)
        {
            bool returnValue;
            using (var db = new ExternalCmsDb())
            {
                returnValue = db.VTVGiftRegisterMainDal.DeleteGiftRegister(id);
            }
            return returnValue;
        }
    }
}
