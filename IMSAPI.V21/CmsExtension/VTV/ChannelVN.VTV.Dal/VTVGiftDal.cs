﻿using ChannelVN.CMS.Common;
using ChannelVN.VTV.Dal.Common;
using ChannelVN.VTV.Entity;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using ChannelVN.VTV.MainDal.Databases;

namespace ChannelVN.VTV.Dal
{
    public class VTVGiftDal
    {
        /// <summary>
        /// Lấy toàn bộ danh sách quà tặng
        /// </summary>
        /// <param name="keyword">string</param>
        /// <param name="giftProgramId">int</param>
        /// <param name="status">int</param>
        /// <param name="from">Datetime</param>
        /// <param name="to">Datetime</param>
        /// <param name="pageIndex">int</param>
        /// <param name="pageSize">int</param>
        /// <param name="totalRow">int output</param>
        /// <returns>list Entity</returns>
        public static List<VTVGiftEntity> ListGift(string keyword, int giftProgramId, int status, DateTime from, DateTime to,
                                                          int pageIndex, int pageSize, ref int totalRow)
        {
            List<VTVGiftEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.VTVGiftMainDal.ListGift(keyword, giftProgramId, status, from, to, pageIndex, pageSize, ref totalRow);
            }
            return returnValue;
        }
        public static List<VTVGiftEntity> ListGift()
        {
            List<VTVGiftEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.VTVGiftMainDal.ListGift();
            }
            return returnValue;
        }
        /// <summary>
        /// Lấy thông tin chi tiết quà tặng
        /// </summary>
        /// <param name="Id">int</param>
        /// <returns>Entity</returns>
        public static VTVGiftEntity GetGiftById(int Id)
        {
            VTVGiftEntity returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.VTVGiftMainDal.GetGiftById(Id);
            }
            return returnValue;
        }
        /// <summary>
        /// Cập nhật thông tin quà tặng
        /// </summary>
        /// <param name="info">Entity</param>
        /// <returns>true/false</returns>
        public static bool UpdateGiftInfo(VTVGiftEntity info)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.VTVGiftMainDal.UpdateGiftInfo(info);
            }
            return returnValue;
        }
        /// <summary>
        /// tạo mới quà tặng
        /// </summary>
        /// <param name="info">Entity</param>
        /// <returns>true/false</returns>
        public static bool InsertGiftInfo(VTVGiftEntity info, ref int id)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.VTVGiftMainDal.InsertGiftInfo(info, ref id);
            }
            return returnValue;
        }
        /// <summary>
        /// Xóa thông tin chương trình quà tặng. Kiểm tra nếu là quyền thư ký (ArticleAdmin) thì mới cho phép xóa
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public static bool DeleteGift(int id)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.VTVGiftMainDal.DeleteGift(id);
            }
            return returnValue;
        }
    }
}
