﻿using System;
using System.Collections.Generic;
using ChannelVN.VTV.Entity;
using System.Data.SqlClient;
using ChannelVN.CMS.Common;
using System.Data;
using ChannelVN.VTV.Dal.Common;
using ChannelVN.VTV.MainDal.Databases;

namespace ChannelVN.VTV.Dal
{
    public class VtvScheduleDal
    {
        public static List<VtvScheduleEntity> GetList(string keyword, int channelId, DateTime filterDate, int pageIndex, int pageSize, ref int totalRow)
        {
            List<VtvScheduleEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.VtvScheduleMainDal.GetList(keyword, channelId, filterDate, pageIndex, pageSize, ref totalRow);
            }
            return returnValue;
        }
        public static bool Insert(VtvScheduleEntity elm, ref int id)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.VtvScheduleMainDal.Insert(elm, ref id);
            }
            return returnValue;
        }
        public static bool Update(VtvScheduleEntity elm)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.VtvScheduleMainDal.Update(elm);
            }
            return returnValue;
        }
        public static VtvScheduleEntity Select(int id)
        {
            VtvScheduleEntity returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.VtvScheduleMainDal.Select(id);
            }
            return returnValue;
        }
        public static bool Delete(int id)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.VtvScheduleMainDal.Delete(id);
            }
            return returnValue;
        }
    }
}
