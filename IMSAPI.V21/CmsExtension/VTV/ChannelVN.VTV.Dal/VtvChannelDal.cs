﻿using System;
using System.Collections.Generic;
using ChannelVN.VTV.Entity;
using System.Data.SqlClient;
using ChannelVN.CMS.Common;
using System.Data;
using ChannelVN.VTV.Dal.Common;
using ChannelVN.VTV.MainDal.Databases;

namespace ChannelVN.VTV.Dal
{
    public class VtvChannelDal
    {
        public static List<VtvChannelEntity> GetList(string keyword, int pageIndex, int pageSize, ref int totalRow)
        {
            List<VtvChannelEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.VtvChannelMainDal.GetList(keyword, pageIndex, pageSize, ref totalRow);
            }
            return returnValue;
        }
        public static bool Insert(VtvChannelEntity elm, ref int id)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.VtvChannelMainDal.Insert(elm, ref id);
            }
            return returnValue;
        }
        public static bool Update(VtvChannelEntity elm)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.VtvChannelMainDal.Update(elm);
            }
            return returnValue;
        }
        public static VtvChannelEntity Select(int id)
        {
            VtvChannelEntity returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.VtvChannelMainDal.Select(id);
            }
            return returnValue;
        }
        public static bool Delete(int id)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.VtvChannelMainDal.Delete(id);
            }
            return returnValue;
        }
    }
}
