﻿using System;
using System.Collections.Generic;
using ChannelVN.VTV.Entity;
using System.Data.SqlClient;
using ChannelVN.CMS.Common;
using System.Data;
using ChannelVN.VTV.Dal.Common;
using ChannelVN.VTV.MainDal.Databases;

namespace ChannelVN.VTV.Dal
{
    public class NewsPrRoyaltyDal
    {
        public static NewsPrRoyaltyEntity GetByNewsId(long newsId)
        {
            NewsPrRoyaltyEntity returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsPrRoyaltyMainDal.GetByNewsId(newsId);
            }
            return returnValue;
        }
        public static bool Update(NewsPrRoyaltyEntity elm)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsPrRoyaltyMainDal.Update(elm);
            }
            return returnValue;
        }
    }
}
