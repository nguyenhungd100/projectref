﻿using ChannelVN.CMS.Common;
using ChannelVN.VTV.Dal.Common;
using ChannelVN.VTV.Entity;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using ChannelVN.VTV.MainDal.Databases;

namespace ChannelVN.VTV.Dal
{
    public class VTVScheduleRegisterDal
    {
        /// <summary>
        /// Lấy danh sách người dùng đăng ký nhận thông tin từ VTV (Lịch phát sóng, Tạp chí truyền hình)
        /// Dữ liệu được lấy từ DB ngoài (VTV_EXT)
        /// </summary>
        /// <param name="keyword">string</param>
        /// <param name="registeredDate">Datetime</param>
        /// <param name="type">int</param>
        /// <param name="status">int</param>
        /// <param name="pageIndex">int</param>
        /// <param name="pageSize">int</param>
        /// <param name="totalRow">int</param>
        /// <returns>List Entity</returns>
        public static List<VTVScheduleRegisterEntity> ListScheduleRegister(string keyword, DateTime registeredDate,
                                                    int type, int status, int order, int pageIndex, int pageSize, ref int totalRow)
        {
            List<VTVScheduleRegisterEntity> returnValue;
            using (var db = new ExternalCmsDb())
            {
                returnValue = db.VTVScheduleRegisterMainDal.ListScheduleRegister(keyword, registeredDate, type, status, order, pageIndex, pageSize, ref totalRow);
            }
            return returnValue;
        }
        /// <summary>
        /// Lấy thông tin người đăng ký nhận quà tặng
        /// </summary>
        /// <param name="Id">int</param>
        /// <returns>Entity</returns>
        public static VTVScheduleRegisterEntity GetScheduleRegisterById(int Id)
        {
            VTVScheduleRegisterEntity returnValue;
            using (var db = new ExternalCmsDb())
            {
                returnValue = db.VTVScheduleRegisterMainDal.GetScheduleRegisterById(Id);
            }
            return returnValue;
        }
        /// <summary>
        /// Cập nhật thông tin ScheduleRegister
        /// </summary>
        /// <param name="info">Entity</param>
        /// <returns>true/false</returns>
        public static bool UpdateScheduleRegisterInfo(VTVScheduleRegisterEntity info)
        {
            bool returnValue;
            using (var db = new ExternalCmsDb())
            {
                returnValue = db.VTVScheduleRegisterMainDal.UpdateScheduleRegisterInfo(info);
            }
            return returnValue;
        }
    }
}
