﻿using System;
using System.Collections.Generic;
using ChannelVN.VTV.Entity;
using System.Data.SqlClient;
using ChannelVN.CMS.Common;
using System.Data;
using ChannelVN.VTV.Dal.Common;
using ChannelVN.VTV.MainDal.Databases;

namespace ChannelVN.VTV.Dal
{
    public class SeaGamesScheduleDal
    {
        public static List<SeaGamesScheduleEntity> GetList(string keyword, int pageIndex, int pageSize, ref int totalRow)
        {
            List<SeaGamesScheduleEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.SeaGamesScheduleMainDal.GetList(keyword, pageIndex, pageSize, ref totalRow);
            }
            return returnValue;
        }

        //public static bool Insert(SeaGamesScheduleEntity elm, ref int id)
        //{
        //    bool returnValue;
        //    using (var db = new CmsMainDb())
        //    {
        //        returnValue = db.SeaGamesScheduleMainDal.Insert(elm, ref id);
        //    }
        //    return returnValue;
        //}

        public static bool Update(SeaGamesScheduleEntity elm)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.SeaGamesScheduleMainDal.Update(elm);
            }
            return returnValue;
        }
        public static SeaGamesScheduleEntity Select(int id)
        {
            SeaGamesScheduleEntity returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.SeaGamesScheduleMainDal.Select(id);
            }
            return returnValue;
        }
        public static bool Delete(int id)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.SeaGamesScheduleMainDal.Delete(id);
            }
            return returnValue;
        }
    }
}
