﻿using ChannelVN.CMS.Common;
using ChannelVN.VTV.Dal.Common;
using ChannelVN.VTV.Entity;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using ChannelVN.VTV.MainDal.Databases;

namespace ChannelVN.VTV.Dal
{
    public class VTVGiftProgramDal
    {
        /// <summary>
        /// Lấy toàn bộ danh sách chương trình quà tặng
        /// </summary>
        /// <param name="keyword">string</param>
        /// <param name="status">int</param>
        /// <param name="pageIndex">int</param>
        /// <param name="pageSize">int</param>
        /// <param name="totalRow">int output</param>
        /// <returns>list Entity</returns>
        public static List<VTVGiftProgramEntity> ListGiftProgram(string keyword,
                                                                            int status, int pageIndex, int pageSize, ref int totalRow)
        {
            List<VTVGiftProgramEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.VTVGiftProgramMainDal.ListGiftProgram(keyword, status, pageIndex, pageSize, ref totalRow);
            }
            return returnValue;
        }
        public static List<VTVGiftProgramEntity> ListGiftProgram()
        {
            List<VTVGiftProgramEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.VTVGiftProgramMainDal.ListGiftProgram();
            }
            return returnValue;
        }
        /// <summary>
        /// Lấy thông tin chi tiết chương trình quà tặng
        /// </summary>
        /// <param name="Id">int</param>
        /// <returns>Entity</returns>
        public static VTVGiftProgramEntity GetGiftProgramById(int Id)
        {
            VTVGiftProgramEntity returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.VTVGiftProgramMainDal.GetGiftProgramById(Id);
            }
            return returnValue;
        }
        /// <summary>
        /// Cập nhật thông tin chương trình quà tặng
        /// </summary>
        /// <param name="info">Entity</param>
        /// <returns>true/false</returns>
        public static bool UpdateGiftProgramInfo(VTVGiftProgramEntity info)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.VTVGiftProgramMainDal.UpdateGiftProgramInfo(info);
            }
            return returnValue;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="info"></param>
        /// <returns></returns>
        public static bool InsertGiftProgramInfo(VTVGiftProgramEntity info, ref int id)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.VTVGiftProgramMainDal.InsertGiftProgramInfo(info, ref id);
            }
            return returnValue;
        }
        /// <summary>
        /// Xóa thông tin chương trình quà tặng. Kiểm tra nếu là quyền thư ký (ArticleAdmin) thì mới cho phép xóa
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public static bool DeleteGiftProgram(int id)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.VTVGiftProgramMainDal.DeleteGiftProgram(id);
            }
            return returnValue;
        }
    }
}
