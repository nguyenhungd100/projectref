﻿using ChannelVN.CMS.Common;
using ChannelVN.VTV.Dal.Common;
using ChannelVN.VTV.Entity;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using ChannelVN.VTV.MainDal.Databases;

namespace ChannelVN.VTV.Dal
{
    public class VTVScheduleRegisterLogDal
    {
        /// <summary>
        /// Lấy danh sách log gửi mail
        /// Dữ liệu được lấy từ DB ngoài (VTV_EXT)
        /// </summary>
        /// <param name="scheduleRegisterId">int</param>
        /// <param name="pageIndex">int</param>
        /// <param name="pageSize">int</param>
        /// <param name="totalRow">int</param>
        /// <returns>List Entity</returns>
        public static List<VTVScheduleRegisterLogEntity> ListScheduleRegisterLog(int scheduleRegisterId, int pageIndex, int pageSize, ref int totalRow)
        {
            List<VTVScheduleRegisterLogEntity> returnValue = null;
            using (var db = new ExternalCmsDb())
            {
                try
                {
                    returnValue = db.VTVScheduleRegisterLogMainDal.ListScheduleRegisterLog(scheduleRegisterId, pageIndex, pageSize, ref totalRow);
                }
                catch(Exception ex)
                {
                    Logger.WriteLog(Logger.LogType.Error, ex.Message);
                }
            }
            return returnValue;
        }

        public static bool Insert(VTVScheduleRegisterLogEntity entity, ref int id)
        {
            bool returnValue = false;
            using (var db = new ExternalCmsDb())
            {
                try
                {
                    returnValue = db.VTVScheduleRegisterLogMainDal.Insert(entity, ref id);
                }
                catch (Exception ex)
                {
                    Logger.WriteLog(Logger.LogType.Error, ex.Message);
                }
            }
            return returnValue;
        }
    }
}
