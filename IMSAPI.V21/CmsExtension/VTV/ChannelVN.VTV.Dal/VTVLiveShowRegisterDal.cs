﻿using ChannelVN.CMS.Common;
using ChannelVN.VTV.Dal.Common;
using ChannelVN.VTV.Entity;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using ChannelVN.VTV.MainDal.Databases;

namespace ChannelVN.VTV.Dal
{
    public class VTVLiveShowRegisterDal
    {
        public static bool Update(VTVLiveShowRegisterEntity info)
        {
            bool returnValue;
            using (var db = new ExternalCmsDb())
            {
                returnValue = db.VTVLiveShowRegisterMainDal.Update(info);
            }
            return returnValue;
        }
        public static List<VTVLiveShowRegisterEntity> ListLiveShowRegister(string keyword, int status,
                                                         int pageIndex, int pageSize, ref int totalRow)
        {
            List<VTVLiveShowRegisterEntity> returnValue;
            using (var db = new ExternalCmsDb())
            {
                returnValue = db.VTVLiveShowRegisterMainDal.ListLiveShowRegister(keyword, status, pageIndex, pageSize, ref totalRow);
            }
            return returnValue;
        }
        public static VTVLiveShowRegisterEntity GetLiveShowRegisterById(int Id)
        {
            VTVLiveShowRegisterEntity returnValue;
            using (var db = new ExternalCmsDb())
            {
                returnValue = db.VTVLiveShowRegisterMainDal.GetLiveShowRegisterById(Id);
            }
            return returnValue;
        }
        public static bool UpdateStatus(VTVLiveShowRegisterEntity info)
        {
            bool returnValue;
            using (var db = new ExternalCmsDb())
            {
                returnValue = db.VTVLiveShowRegisterMainDal.UpdateStatus(info);
            }
            return returnValue;
        }
    }
}
