﻿using ChannelVN.CMS.Common;
using ChannelVN.VTV.Dal.Common;
using ChannelVN.VTV.Entity;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using ChannelVN.VTV.MainDal.Databases;

namespace ChannelVN.VTV.Dal
{
    public class VTVVideoRecordDal
    {
        public static List<VTVVideoRecordEntity> ListVideoRecord(string keyword, int programChannel, string accountName, int status,
                                                         DateTime fromDate, DateTime toDate, int pageIndex, int pageSize, ref int totalRow)
        {
            List<VTVVideoRecordEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.VTVVideoRecordMainDal.ListVideoRecord(keyword, programChannel, accountName, status, fromDate, toDate, pageIndex, pageSize, ref totalRow);
            }
            return returnValue;
        }
        public static bool UpdateStatus(VTVVideoRecordEntity info)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.VTVVideoRecordMainDal.UpdateStatus(info);
            }
            return returnValue;
        }
        public static bool Insert(VTVVideoRecordEntity info, ref int id)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.VTVVideoRecordMainDal.Insert(info, ref id);
            }
            return returnValue;
        }
        public static List<VTVProgramChannelEntity> ListChannel(int status)
        {
            List<VTVProgramChannelEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.VTVVideoRecordMainDal.ListChannel(status);
            }
            return returnValue;
        }
        public static bool UpdateStatus(int channelId, int status)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.VTVVideoRecordMainDal.UpdateStatus(channelId, status);
            }
            return returnValue;
        }
        public static VTVVideoRecordEntity GetVideoRecordById(int id)
        {
            VTVVideoRecordEntity returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.VTVVideoRecordMainDal.GetVideoRecordById(id);
            }
            return returnValue;
        }
    }
}
