﻿using ChannelVN.CMS.Common;
using ChannelVN.VTV.Dal.Common;
using ChannelVN.VTV.Entity;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using ChannelVN.VTV.MainDal.Databases;

namespace ChannelVN.VTV.Dal
{
    public class BoxCountDownChannelDal
    {
        /// <summary>
        /// Lấy toàn bộ danh sách chương trình quà tặng
        /// </summary>
        /// <param name="keyword">string</param>
        /// <param name="status">int</param>
        /// <param name="pageIndex">int</param>
        /// <param name="pageSize">int</param>
        /// <param name="totalRow">int output</param>
        /// <returns>list Entity</returns>
        public static List<BoxCountDownChannelEntity> ListBoxCountDown(string keyword,
                                                                            int status, int pageIndex, int pageSize, ref int totalRow)
        {
            List<BoxCountDownChannelEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.BoxCountDownChannelMainDal.ListBoxCountDown(keyword, status, pageIndex, pageSize, ref totalRow);
            }
            return returnValue;
        }
        public static List<BoxCountDownChannelEntity> ListBoxCountDown()
        {
            List<BoxCountDownChannelEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.BoxCountDownChannelMainDal.ListBoxCountDown();
            }
            return returnValue;
        }
        /// <summary>
        /// Lấy thông tin chi tiết chương trình quà tặng
        /// </summary>
        /// <param name="Id">int</param>
        /// <returns>Entity</returns>
        public static BoxCountDownChannelEntity GetBoxCountDownById(int Id)
        {
            BoxCountDownChannelEntity returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.BoxCountDownChannelMainDal.GetBoxCountDownById(Id);
            }
            return returnValue;
        }
        /// <summary>
        /// Cập nhật thông tin chương trình quà tặng
        /// </summary>
        /// <param name="info">Entity</param>
        /// <returns>true/false</returns>
        public static bool UpdateBoxCountDownInfo(BoxCountDownChannelEntity info)
        {

            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.BoxCountDownChannelMainDal.UpdateBoxCountDownInfo(info);
            }
            return returnValue;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="info"></param>
        /// <returns></returns>
        public static bool InsertBoxCountDownInfo(BoxCountDownChannelEntity info, ref int id)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.BoxCountDownChannelMainDal.InsertBoxCountDownInfo(info, ref id);
            }
            return returnValue;
        }
        /// <summary>
        /// Xóa thông tin chương trình quà tặng. Kiểm tra nếu là quyền thư ký (ArticleAdmin) thì mới cho phép xóa
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public static bool DeleteBoxCountDown(int id)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.BoxCountDownChannelMainDal.DeleteBoxCountDown(id);
            }
            return returnValue;
        }
    }
}
