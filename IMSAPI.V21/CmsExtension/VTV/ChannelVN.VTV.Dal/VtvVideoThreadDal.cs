﻿using System;
using System.Collections.Generic;
using ChannelVN.VTV.Entity;
using System.Data.SqlClient;
using ChannelVN.CMS.Common;
using System.Data;
using ChannelVN.VTV.Dal.Common;
using ChannelVN.VTV.MainDal.Databases;

namespace ChannelVN.VTV.Dal
{
    public class VtvVideoThreadDal
    {
        #region VIDEO
        public static List<VtvVideoThreadEntity> GetList(string keyword, int isHot, int pageIndex, int pageSize, ref int totalRow)
        {
            List<VtvVideoThreadEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.VtvVideoThreadMainDal.GetList(keyword, isHot, pageIndex, pageSize, ref totalRow);
            }
            return returnValue;
        }
        public static bool Insert(VtvVideoThreadEntity elm, ref int id)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.VtvVideoThreadMainDal.Insert(elm, ref id);
            }
            return returnValue;
        }
        public static bool Update(VtvVideoThreadEntity elm)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.VtvVideoThreadMainDal.Update(elm);
            }
            return returnValue;
        }
        public static bool Delete(int id)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.VtvVideoThreadMainDal.Delete(id);
            }
            return returnValue;
        }
        public static VtvVideoThreadEntity Select(int id)
        {
            VtvVideoThreadEntity returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.VtvVideoThreadMainDal.Select(id);
            }
            return returnValue;
        }
        public static bool IsHot(int id)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.VtvVideoThreadMainDal.IsHot(id);
            }
            return returnValue;
        }
        #endregion

        #region ZONE
        public static bool InsertZone(VtvVideoThreadZoneEntity elm)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.VtvVideoThreadMainDal.InsertZone(elm);
            }
            return returnValue;
        }
        public static VtvVideoThreadZoneEntity SelectZone(int videoThreadId, bool isPrimary)
        {
            VtvVideoThreadZoneEntity returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.VtvVideoThreadMainDal.SelectZone(videoThreadId, isPrimary);
            }
            return returnValue;
        }
        public static bool DeleteZone(int videoThreadId)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.VtvVideoThreadMainDal.DeleteZone(videoThreadId);
            }
            return returnValue;
        }
        #endregion

        #region VIDEO IN VIDEO
        public static List<VtvVideoThreadVideoEntity> ListVideo(int videoThreadId, int pageIndex, int pageSize, ref int totalRow)
        {
            List<VtvVideoThreadVideoEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.VtvVideoThreadMainDal.ListVideo(videoThreadId, pageIndex, pageSize, ref totalRow);
            }
            return returnValue;
        }
        public static bool InsertVideo(VtvVideoThreadVideoEntity elm)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.VtvVideoThreadMainDal.InsertVideo(elm);
            }
            return returnValue;
        }
        public static bool DeleteVideo(int videoThreadId, int videoId)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.VtvVideoThreadMainDal.DeleteVideo(videoThreadId, videoId);
            }
            return returnValue;
        }
        public static VtvVideoThreadVideoEntity ExistVideo(int videoThreadId, int videoId)
        {
            VtvVideoThreadVideoEntity returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.VtvVideoThreadMainDal.ExistVideo(videoThreadId, videoId);
            }
            return returnValue;
        }
        #endregion

        #region TAG
        public static List<VtvVideoThreadTagEntity> ListTag(int videoThreadId)
        {
            List<VtvVideoThreadTagEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.VtvVideoThreadMainDal.ListTag(videoThreadId);
            }
            return returnValue;
        }
        public static bool InsertTag(VtvVideoThreadTagEntity elm)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.VtvVideoThreadMainDal.InsertTag(elm);
            }
            return returnValue;
        }
        public static bool DeleteTag(int videoThreadId)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.VtvVideoThreadMainDal.DeleteTag(videoThreadId);
            }
            return returnValue;
        }
        #endregion
    }
}
