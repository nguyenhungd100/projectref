﻿using System;
using System.Collections.Generic;
using ChannelVN.VTV.Entity;
using System.Data.SqlClient;
using ChannelVN.CMS.Common;
using System.Data;
using ChannelVN.VTV.Dal.Common;
using ChannelVN.VTV.MainDal.Databases;

namespace ChannelVN.VTV.Dal
{
    public class VTVProgramScheduleDetailDal
    {
        public static bool Insert(VTVProgramScheduleDetailEntity entity, ref int id)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.VTVProgramScheduleDetailMainDal.Insert(entity, ref id);
            }
            return returnValue;
        }
    }
}
