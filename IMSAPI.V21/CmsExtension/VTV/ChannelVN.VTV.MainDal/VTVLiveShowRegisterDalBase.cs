﻿using ChannelVN.CMS.Common;

using ChannelVN.VTV.Entity;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using ChannelVN.VTV.MainDal.Databases;

namespace ChannelVN.VTV.MainDal
{
    public abstract class VTVLiveShowRegisterDalBase
    {
        public bool Update(VTVLiveShowRegisterEntity info)
        {
            const string commandText = "CMS_LiveShowRegister_Update"; try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", info.Id);
                _db.AddParameter(cmd, "LiveShowRegisterName", info.LiveShowRegisterName);
                _db.AddParameter(cmd, "LiveShowRegisterId", info.LiveShowRegisterId);
                _db.AddParameter(cmd, "RegisterName", info.RegisterName);
                _db.AddParameter(cmd, "Nationality", info.Nationality);
                _db.AddParameter(cmd, "IdentityNumber", info.IdentityNumber);
                _db.AddParameter(cmd, "Birthday", info.Birthday);
                _db.AddParameter(cmd, "Gender", info.Gender);
                _db.AddParameter(cmd, "Email", info.Email);
                _db.AddParameter(cmd, "Address", info.Address);
                _db.AddParameter(cmd, "Mobile", info.Mobile);
                _db.AddParameter(cmd, "Literacy", info.Literacy);
                _db.AddParameter(cmd, "Job", info.Job);
                _db.AddParameter(cmd, "CreatedDate", info.CreatedDate);
                _db.AddParameter(cmd, "Status", info.Status);
                var numberOfRow = _db.ExecuteNonQuery(cmd);
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public List<VTVLiveShowRegisterEntity> ListLiveShowRegister(string keyword, int status,
                                                         int pageIndex, int pageSize, ref int totalRow)
        {
            const string commandText = "CMS_VTV_LiveShowRegister_GetList"; try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Keyword", keyword);
                _db.AddParameter(cmd, "Status", status);
                _db.AddParameter(cmd, "PageIndex", pageIndex);
                _db.AddParameter(cmd, "PageSize", pageSize);
                _db.AddParameter(cmd, "totalRow", totalRow, ParameterDirection.Output);
                var numberOfRow = _db.GetList<VTVLiveShowRegisterEntity>(cmd);
                totalRow = Utility.ConvertToInt(cmd.Parameters[cmd.Parameters.Count - 1]);
                return numberOfRow;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public VTVLiveShowRegisterEntity GetLiveShowRegisterById(int Id)
        {
            const string commandText = "CMS_VTV_LiveShowRegister_GetById"; try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", Id);
                var numberOfRow = _db.Get<VTVLiveShowRegisterEntity>(cmd);
                return numberOfRow;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool UpdateStatus(VTVLiveShowRegisterEntity info)
        {
            const string commandText = "CMS_LiveShowRegister_UpdateStatus"; try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", info.Id);
                _db.AddParameter(cmd, "Status", info.Status);
                var numberOfRow = _db.ExecuteNonQuery(cmd);
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        #region Core members

        private readonly ExternalCmsDbBase _db;

        protected VTVLiveShowRegisterDalBase(ExternalCmsDbBase db)
        {
            _db = db;
        }

        protected ExternalCmsDbBase Database
        {
            get { return _db; }
        }

        #endregion
    }
}
