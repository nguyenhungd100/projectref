﻿using ChannelVN.CMS.Common;

using ChannelVN.VTV.Entity;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using ChannelVN.VTV.MainDal.Databases;

namespace ChannelVN.VTV.MainDal
{
    public abstract class BoxCountDownChannelDalBase
    {
        /// <summary>
        /// Lấy toàn bộ danh sách chương trình quà tặng
        /// </summary>
        /// <param name="keyword">string</param>
        /// <param name="status">int</param>
        /// <param name="pageIndex">int</param>
        /// <param name="pageSize">int</param>
        /// <param name="totalRow">int output</param>
        /// <returns>list Entity</returns>
        public List<BoxCountDownChannelEntity> ListBoxCountDown(string keyword,
                                                                            int status, int pageIndex, int pageSize, ref int totalRow)
        {
            const string commandText = "CMS_BoxCountDownChannel_GetList"; try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Keyword", keyword);
                _db.AddParameter(cmd, "Status", status);
                _db.AddParameter(cmd, "PageIndex", pageIndex);
                _db.AddParameter(cmd, "PageSize", pageSize);
                _db.AddParameter(cmd, "totalRow", totalRow, ParameterDirection.Output);
                var numberOfRow = _db.GetList<BoxCountDownChannelEntity>(cmd);
                totalRow = Utility.ConvertToInt(cmd.Parameters[cmd.Parameters.Count - 1]);
                return numberOfRow;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public List<BoxCountDownChannelEntity> ListBoxCountDown()
        {
            const string commandText = "CMS_BoxCountDownChannel_GetListAll"; try
            {
                var cmd = _db.CreateCommand(commandText, true);
                var numberOfRow = _db.GetList<BoxCountDownChannelEntity>(cmd);
                return numberOfRow;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        /// <summary>
        /// Lấy thông tin chi tiết chương trình quà tặng
        /// </summary>
        /// <param name="Id">int</param>
        /// <returns>Entity</returns>
        public BoxCountDownChannelEntity GetBoxCountDownById(int Id)
        {
            const string commandText = "CMS_BoxCountDownChannel_GetById"; try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", Id);
                var numberOfRow = _db.Get<BoxCountDownChannelEntity>(cmd);
                return numberOfRow;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        /// <summary>
        /// Cập nhật thông tin chương trình quà tặng
        /// </summary>
        /// <param name="info">Entity</param>
        /// <returns>true/false</returns>
        public bool UpdateBoxCountDownInfo(BoxCountDownChannelEntity info)
        {
            const string commandText = "CMS_BoxCountDownChannel_UpdateInfo"; try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", info.Id);
                _db.AddParameter(cmd, "ZoneId", info.ZoneId);
                _db.AddParameter(cmd, "Title", info.Title);
                _db.AddParameter(cmd, "Avatar", info.Avatar);
                _db.AddParameter(cmd, "FileName", info.FileName);
                _db.AddParameter(cmd, "VideoKey", info.VideoKey);
                _db.AddParameter(cmd, "Description", info.Description);
                _db.AddParameter(cmd, "CountDownTime", info.CountDownTime);
                _db.AddParameter(cmd, "NewsRelation", info.NewsRelation);
                _db.AddParameter(cmd, "IsActive", info.IsActive);
                var numberOfRow = _db.ExecuteNonQuery(cmd);
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="info"></param>
        /// <returns></returns>
        public bool InsertBoxCountDownInfo(BoxCountDownChannelEntity info, ref int id)
        {
            const string commandText = "CMS_BoxCountDownChannel_Insert"; try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "ZoneId", info.ZoneId);
                _db.AddParameter(cmd, "Title", info.Title);
                _db.AddParameter(cmd, "Avatar", info.Avatar);
                _db.AddParameter(cmd, "FileName", info.FileName);
                _db.AddParameter(cmd, "VideoKey", info.VideoKey);
                _db.AddParameter(cmd, "Description", info.Description);
                _db.AddParameter(cmd, "CountDownTime", info.CountDownTime);
                _db.AddParameter(cmd, "NewsRelation", info.NewsRelation);
                _db.AddParameter(cmd, "IsActive", info.IsActive);
                var numberOfRow = _db.ExecuteNonQuery(cmd);
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        /// <summary>
        /// Xóa thông tin chương trình quà tặng. Kiểm tra nếu là quyền thư ký (ArticleAdmin) thì mới cho phép xóa
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public bool DeleteBoxCountDown(int id)
        {
            const string commandText = "CMS_BoxCountDownChannel_Delete"; try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", id);
                var numberOfRow = _db.ExecuteNonQuery(cmd);
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        #region Core members

        private readonly CmsMainDb _db;

        protected BoxCountDownChannelDalBase(CmsMainDb db)
        {
            _db = db;
        }

        protected CmsMainDb Database
        {
            get { return _db; }
        }

        #endregion
    }
}
