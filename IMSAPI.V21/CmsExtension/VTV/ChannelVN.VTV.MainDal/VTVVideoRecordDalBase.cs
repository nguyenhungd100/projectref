﻿using ChannelVN.CMS.Common;

using ChannelVN.VTV.Entity;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using ChannelVN.VTV.MainDal.Databases;

namespace ChannelVN.VTV.MainDal
{
    public abstract class VTVVideoRecordDalBase
    {
        public List<VTVVideoRecordEntity> ListVideoRecord(string keyword, int programChannel, string accountName, int status,
                                                         DateTime fromDate, DateTime toDate, int pageIndex, int pageSize, ref int totalRow)
        {
            const string commandText = "CMS_VideoRecord_GetList"; try
            {
                object objFromDate;
                if (fromDate > DateTime.MinValue) objFromDate = fromDate;
                else objFromDate = DBNull.Value;

                object objToDate;
                if (toDate > DateTime.MinValue) objToDate = toDate;
                else objToDate = DBNull.Value;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Keyword", keyword);
                _db.AddParameter(cmd, "ProgramChannel", programChannel);
                _db.AddParameter(cmd, "AccountName", accountName);
                _db.AddParameter(cmd, "Status", status);
                _db.AddParameter(cmd, "FromDate", objFromDate);
                _db.AddParameter(cmd, "ToDate", objToDate);
                _db.AddParameter(cmd, "PageIndex", pageIndex);
                _db.AddParameter(cmd, "PageSize", pageSize);
                _db.AddParameter(cmd, "totalRow", totalRow, ParameterDirection.Output);
                var numberOfRow = _db.GetList<VTVVideoRecordEntity>(cmd);
                totalRow = Utility.ConvertToInt(cmd.Parameters[cmd.Parameters.Count - 1]);
                return numberOfRow;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool UpdateStatus(VTVVideoRecordEntity info)
        {
            const string commandText = "CMS_VideoRecord_UpdateStatus"; try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", info.Id);
                _db.AddParameter(cmd, "Status", info.Status);
                _db.AddParameter(cmd, "FilePath", info.FilePath);
                var numberOfRow = _db.ExecuteNonQuery(cmd);
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool Insert(VTVVideoRecordEntity info, ref int id)
        {
            const string commandText = "CMS_VideoRecord_Insert"; try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "ProgramChannel", info.ProgramChannel);
                _db.AddParameter(cmd, "ProgramScheduleDetailId", info.ProgramScheduleDetailId);
                _db.AddParameter(cmd, "AccountName", info.AccountName);
                _db.AddParameter(cmd, "FilePath", info.FilePath);
                _db.AddParameter(cmd, "Status", info.Status);
                _db.AddParameter(cmd, "CompletedDate", DBNull.Value);
                _db.AddParameter(cmd, "StartDate", DBNull.Value);
                _db.AddParameter(cmd, "EndDate", DBNull.Value);
                _db.AddParameter(cmd, "Id", info.Id, ParameterDirection.Output);
                var numberOfRow = _db.ExecuteNonQuery(cmd);
                id = Utility.ConvertToInt(cmd.Parameters[cmd.Parameters.Count - 1]);
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public List<VTVProgramChannelEntity> ListChannel(int status)
        {
            const string commandText = "CMS_VideoRecord_GetChannelList"; try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Status", status);
                var numberOfRow = _db.GetList<VTVProgramChannelEntity>(cmd);
                return numberOfRow;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool UpdateStatus(int channelId, int status)
        {
            const string commandText = "CMS_VideoRecord_UpdateStatusByChannel"; try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", channelId);
                _db.AddParameter(cmd, "Status", status);
                var numberOfRow = _db.ExecuteNonQuery(cmd);
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public VTVVideoRecordEntity GetVideoRecordById(int id)
        {
            const string commandText = "CMS_VideoRecord_GetVideoRecordById"; try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "id", id);
                var numberOfRow = _db.Get<VTVVideoRecordEntity>(cmd);
                return numberOfRow;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        #region Core members

        private readonly CmsMainDb _db;

        protected VTVVideoRecordDalBase(CmsMainDb db)
        {
            _db = db;
        }

        protected CmsMainDb Database
        {
            get { return _db; }
        }

        #endregion
    }
}
