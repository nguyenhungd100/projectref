﻿using System;
using System.Collections.Generic;
using ChannelVN.VTV.Entity;
using System.Data.SqlClient;
using ChannelVN.CMS.Common;
using System.Data;
using ChannelVN.VTV.MainDal.Databases;


namespace ChannelVN.VTV.MainDal
{
    public abstract class VTVProgramScheduleDetailDalBase
    {
        public bool Insert(VTVProgramScheduleDetailEntity entity, ref int id)
        {
            const string commandText = "CMS_ProgramScheduleDetail_Insert"; try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "ProgramScheduleId", entity.ProgramScheduleId);
                _db.AddParameter(cmd, "ScheduleTime", entity.ScheduleTime);
                _db.AddParameter(cmd, "Title", entity.Title);
                _db.AddParameter(cmd, "Description", entity.Description);
                _db.AddParameter(cmd, "Avatar", entity.Avatar);
                _db.AddParameter(cmd, "VideoId", entity.VideoId);
                _db.AddParameter(cmd, "CreatedBy", entity.CreatedBy);
                _db.AddParameter(cmd, "Status", entity.Status);
                _db.AddParameter(cmd, "Id", id, ParameterDirection.Output);
                var numberOfRow = _db.ExecuteNonQuery(cmd);
                id = Utility.ConvertToInt(cmd.Parameters[cmd.Parameters.Count - 1]);
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        #region Core members

        private readonly CmsMainDb _db;

        protected VTVProgramScheduleDetailDalBase(CmsMainDb db)
        {
            _db = db;
        }

        protected CmsMainDb Database
        {
            get { return _db; }
        }

        #endregion
    }
}
