﻿using System;
using System.Collections.Generic;
using ChannelVN.VTV.Entity;
using System.Data.SqlClient;
using ChannelVN.CMS.Common;
using System.Data;
using ChannelVN.VTV.MainDal.Databases;


namespace ChannelVN.VTV.MainDal
{
    public abstract class VtvScheduleDalBase
    {
        public List<VtvScheduleEntity> GetList(string keyword, int channelId, DateTime filterDate, int pageIndex, int pageSize, ref int totalRow)
        {
            const string commandText = "CMS_VTV_Schedule_List"; try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Keyword", keyword);
                _db.AddParameter(cmd, "ChannelId", channelId);
                _db.AddParameter(cmd, "FilterDate", filterDate);
                _db.AddParameter(cmd, "PageIndex", pageIndex);
                _db.AddParameter(cmd, "PageSize", pageSize);
                _db.AddParameter(cmd, "TotalRow", totalRow, ParameterDirection.Output);
                var numberOfRow = _db.GetList<VtvScheduleEntity>(cmd);
                return numberOfRow;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool Insert(VtvScheduleEntity elm, ref int id)
        {
            const string commandText = "CMS_VTV_Schedule_Insert"; try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "ChannelId", elm.ChannelId);
                _db.AddParameter(cmd, "ScheduleName", elm.ScheduleName);
                _db.AddParameter(cmd, "StartDate", elm.StartDate);
                _db.AddParameter(cmd, "Id", id, ParameterDirection.Output);
                var numberOfRow = _db.ExecuteNonQuery(cmd);
                id = Utility.ConvertToInt(cmd.Parameters[cmd.Parameters.Count - 1]);
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool Update(VtvScheduleEntity elm)
        {
            const string commandText = "CMS_VTV_Schedule_Update"; try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", elm.Id);
                _db.AddParameter(cmd, "ChannelId", elm.ChannelId);
                _db.AddParameter(cmd, "ScheduleName", elm.ScheduleName);
                _db.AddParameter(cmd, "StartDate", elm.StartDate);
                var numberOfRow = _db.ExecuteNonQuery(cmd);
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public VtvScheduleEntity Select(int id)
        {
            const string commandText = "CMS_VTV_Schedule_Select"; try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", id);
                var numberOfRow = _db.Get<VtvScheduleEntity>(cmd);
                return numberOfRow;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool Delete(int id)
        {
            const string commandText = "CMS_VTV_Schedule_Delete"; try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", id);
                var numberOfRow = _db.ExecuteNonQuery(cmd);
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        #region Core members

        private readonly CmsMainDb _db;

        protected VtvScheduleDalBase(CmsMainDb db)
        {
            _db = db;
        }

        protected CmsMainDb Database
        {
            get { return _db; }
        }

        #endregion
    }
}
