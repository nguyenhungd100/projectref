﻿using ChannelVN.CMS.Common;
using ChannelVN.VTV.MainDal.Common;
using ChannelVN.VTV.MainDal.Databases;
using ChannelVN.VTV.Entity;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace ChannelVN.VTV.MainDal
{
    public abstract class VTVGiftDalBase
    {
        /// <summary>
        /// Lấy toàn bộ danh sách quà tặng
        /// </summary>
        /// <param name="keyword">string</param>
        /// <param name="giftProgramId">int</param>
        /// <param name="status">int</param>
        /// <param name="from">Datetime</param>
        /// <param name="to">Datetime</param>
        /// <param name="pageIndex">int</param>
        /// <param name="pageSize">int</param>
        /// <param name="totalRow">int output</param>
        /// <returns>list Entity</returns>
        public List<VTVGiftEntity> ListGift(string keyword, int giftProgramId, int status, DateTime from, DateTime to,
                                                          int pageIndex, int pageSize, ref int totalRow)
        {
            const string commandText = "CMS_VTV_Gift_GetList"; try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Keyword", keyword);
                if (from <= Constants.MinDateTime)
                    _db.AddParameter(cmd, "startDate", DBNull.Value);
                else _db.AddParameter(cmd, "startDate", from);
                if (to <= Constants.MinDateTime)
                    _db.AddParameter(cmd, "endDate", DBNull.Value);
                else _db.AddParameter(cmd, "endDate", to);
                _db.AddParameter(cmd, "giftProgramId", giftProgramId);
                _db.AddParameter(cmd, "Status", status);
                //_db.AddParameter(cmd, "Order", order);
                _db.AddParameter(cmd, "PageIndex", pageIndex);
                _db.AddParameter(cmd, "PageSize", pageSize);
                _db.AddParameter(cmd, "totalRow", totalRow, ParameterDirection.Output);
                var numberOfRow = _db.GetList<VTVGiftEntity>(cmd);
                totalRow = Utility.ConvertToInt(cmd.Parameters[cmd.Parameters.Count - 1]);
                return numberOfRow;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public List<VTVGiftEntity> ListGift()
        {
            const string commandText = "CMS_VTV_Gift_GetListAll"; try
            {
                var cmd = _db.CreateCommand(commandText, true);
                var numberOfRow = _db.GetList<VTVGiftEntity>(cmd);
                return numberOfRow;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        /// <summary>
        /// Lấy thông tin chi tiết quà tặng
        /// </summary>
        /// <param name="Id">int</param>
        /// <returns>Entity</returns>
        public VTVGiftEntity GetGiftById(int Id)
        {
            const string commandText = "CMS_VTV_Gift_GetById"; try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", Id);
                var numberOfRow = _db.Get<VTVGiftEntity>(cmd);
                return numberOfRow;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        /// <summary>
        /// Cập nhật thông tin quà tặng
        /// </summary>
        /// <param name="info">Entity</param>
        /// <returns>true/false</returns>
        public bool UpdateGiftInfo(VTVGiftEntity info)
        {
            const string commandText = "CMS_VTV_Gift_Update"; try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", info.Id);
                _db.AddParameter(cmd, "IdGiftProgram", info.IdGiftProgram);
                _db.AddParameter(cmd, "Name", info.Name);
                _db.AddParameter(cmd, "Description", info.Description);
                _db.AddParameter(cmd, "Avatar", info.Avatar);
                _db.AddParameter(cmd, "MaxQuantity", info.MaxQuantity);
                _db.AddParameter(cmd, "Quantity", info.Quantity);
                _db.AddParameter(cmd, "StartDate", info.StartDate);
                _db.AddParameter(cmd, "EndDate", info.EndDate);
                _db.AddParameter(cmd, "TypeGift", info.TypeGift);
                _db.AddParameter(cmd, "LastModifiedBy", info.LastModifiedBy);
                _db.AddParameter(cmd, "Status", info.Status);
                var numberOfRow = _db.ExecuteNonQuery(cmd);
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        /// <summary>
        /// tạo mới quà tặng
        /// </summary>
        /// <param name="info">Entity</param>
        /// <returns>true/false</returns>
        public bool InsertGiftInfo(VTVGiftEntity info, ref int id)
        {
            const string commandText = "CMS_VTV_Gift_Insert"; try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "IdGiftProgram", info.IdGiftProgram);
                _db.AddParameter(cmd, "Name", info.Name);
                _db.AddParameter(cmd, "Description", info.Description);
                _db.AddParameter(cmd, "Avatar", info.Avatar);
                _db.AddParameter(cmd, "MaxQuantity", info.MaxQuantity);
                _db.AddParameter(cmd, "Quantity", info.Quantity);
                _db.AddParameter(cmd, "StartDate", info.StartDate);
                _db.AddParameter(cmd, "EndDate", info.EndDate);
                _db.AddParameter(cmd, "TypeGift", info.TypeGift);
                _db.AddParameter(cmd, "CreatedBy", info.CreatedBy);
                _db.AddParameter(cmd, "Status", info.Status);
                _db.AddParameter(cmd, "Id", id, ParameterDirection.Output);
                var numberOfRow = _db.ExecuteNonQuery(cmd);
                id = Utility.ConvertToInt(cmd.Parameters[cmd.Parameters.Count - 1]);
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        /// <summary>
        /// Xóa thông tin chương trình quà tặng. Kiểm tra nếu là quyền thư ký (ArticleAdmin) thì mới cho phép xóa
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public bool DeleteGift(int id)
        {
            const string commandText = "CMS_VTV_Gift_Delete"; try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", id);
                var numberOfRow = _db.ExecuteNonQuery(cmd);
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        #region Core members

        private readonly CmsMainDb _db;

        protected VTVGiftDalBase(CmsMainDb db)
        {
            _db = db;
        }

        protected CmsMainDb Database
        {
            get { return _db; }
        }

        #endregion
    }
}
