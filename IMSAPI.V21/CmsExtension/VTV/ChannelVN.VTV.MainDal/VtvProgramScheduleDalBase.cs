﻿using System;
using System.Collections.Generic;
using ChannelVN.VTV.Entity;
using System.Data.SqlClient;
using ChannelVN.CMS.Common;
using System.Data;
using ChannelVN.VTV.MainDal.Databases;


namespace ChannelVN.VTV.MainDal
{
    public abstract class VtvProgramScheduleDalBase
    {
        public bool Insert(VTVProgramScheduleEntity entity, ref int id)
        {
            const string commandText = "CMS_ProgramSchedule_Insert"; try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "ProgramChannelId", entity.ProgramChannelId);
                _db.AddParameter(cmd, "ScheduleName", entity.ScheduleName);
                _db.AddParameter(cmd, "ScheduleAvatar", entity.ScheduleAvatar);
                _db.AddParameter(cmd, "PlayListId", entity.PlayListId);
                _db.AddParameter(cmd, "ScheduleDate", entity.ScheduleDate);
                _db.AddParameter(cmd, "CreatedBy", entity.CreatedBy);
                _db.AddParameter(cmd, "Status", entity.Status);
                _db.AddParameter(cmd, "Id", id, ParameterDirection.Output);
                var numberOfRow = _db.ExecuteNonQuery(cmd);
                id = Utility.ConvertToInt(cmd.Parameters[cmd.Parameters.Count - 1]);
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        #region Core members

        private readonly CmsMainDb _db;

        protected VtvProgramScheduleDalBase(CmsMainDb db)
        {
            _db = db;
        }

        protected CmsMainDb Database
        {
            get { return _db; }
        }

        #endregion
    }
}
