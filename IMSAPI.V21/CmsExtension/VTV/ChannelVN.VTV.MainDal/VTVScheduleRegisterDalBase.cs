﻿using ChannelVN.CMS.Common;

using ChannelVN.VTV.Entity;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using ChannelVN.VTV.MainDal.Common;
using ChannelVN.VTV.MainDal.Databases;

namespace ChannelVN.VTV.MainDal
{
    public abstract class VTVScheduleRegisterDalBase
    {
        /// <summary>
        /// Lấy danh sách người dùng đăng ký nhận thông tin từ VTV (Lịch phát sóng, Tạp chí truyền hình)
        /// Dữ liệu được lấy từ DB ngoài (VTV_EXT)
        /// </summary>
        /// <param name="keyword">string</param>
        /// <param name="registeredDate">Datetime</param>
        /// <param name="type">int</param>
        /// <param name="status">int</param>
        /// <param name="pageIndex">int</param>
        /// <param name="pageSize">int</param>
        /// <param name="totalRow">int</param>
        /// <returns>List Entity</returns>
        public List<VTVScheduleRegisterEntity> ListScheduleRegister(string keyword, DateTime registeredDate,
                                                    int type, int status, int order, int pageIndex, int pageSize, ref int totalRow)
        {
            const string commandText = "CMS_VTV_ScheduleRegister_GetList"; try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "KeyWord", keyword);
                if (registeredDate <= Constants.MinDateTime)
                    _db.AddParameter(cmd, "registeredDate", DBNull.Value);
                else _db.AddParameter(cmd, "registeredDate", registeredDate);
                _db.AddParameter(cmd, "Type", type);
                _db.AddParameter(cmd, "Status", status);
                _db.AddParameter(cmd, "Order", order);
                _db.AddParameter(cmd, "PageIndex", pageIndex);
                _db.AddParameter(cmd, "PageSize", pageSize);
                _db.AddParameter(cmd, "totalRow", totalRow, ParameterDirection.Output);
                var numberOfRow = _db.GetList<VTVScheduleRegisterEntity>(cmd);
                totalRow = Utility.ConvertToInt(cmd.Parameters[cmd.Parameters.Count - 1]);
                return numberOfRow;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        /// <summary>
        /// Lấy thông tin người đăng ký nhận quà tặng
        /// </summary>
        /// <param name="Id">int</param>
        /// <returns>Entity</returns>
        public VTVScheduleRegisterEntity GetScheduleRegisterById(int Id)
        {
            const string commandText = "CMS_VTV_ScheduleRegister_GetById"; try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", Id);
                var numberOfRow = _db.Get<VTVScheduleRegisterEntity>(cmd);
                return numberOfRow;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        /// <summary>
        /// Cập nhật thông tin ScheduleRegister
        /// </summary>
        /// <param name="info">Entity</param>
        /// <returns>true/false</returns>
        public bool UpdateScheduleRegisterInfo(VTVScheduleRegisterEntity info)
        {
            const string commandText = "CMS_VTV_ScheduleRegister_UpdateInfo"; try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", info.Id);
                _db.AddParameter(cmd, "FullName", info.FullName);
                _db.AddParameter(cmd, "Email", info.Email);
                _db.AddParameter(cmd, "Status", info.Status);
                var numberOfRow = _db.ExecuteNonQuery(cmd);
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        #region Core members

        private readonly ExternalCmsDb _db;

        protected VTVScheduleRegisterDalBase(ExternalCmsDb db)
        {
            _db = db;
        }

        protected ExternalCmsDb Database
        {
            get { return _db; }
        }

        #endregion
    }
}
