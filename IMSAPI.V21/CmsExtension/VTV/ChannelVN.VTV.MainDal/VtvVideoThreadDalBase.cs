﻿using System;
using System.Collections.Generic;
using ChannelVN.VTV.MainDal.Databases;
using ChannelVN.VTV.Entity;
using System.Data.SqlClient;
using ChannelVN.CMS.Common;
using System.Data;


namespace ChannelVN.VTV.MainDal
{
    public abstract class VtvVideoThreadDalBase
    {
        #region VIDEO
        public List<VtvVideoThreadEntity> GetList(string keyword, int isHot, int pageIndex, int pageSize, ref int totalRow)
        {
            const string commandText = "CMS_VideoThread_List"; try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Keyword", keyword);
                _db.AddParameter(cmd, "IsHotThread", isHot);
                _db.AddParameter(cmd, "PageIndex", pageIndex);
                _db.AddParameter(cmd, "PageSize", pageSize);
                _db.AddParameter(cmd, "TotalRow", totalRow, ParameterDirection.Output);
                var numberOfRow = _db.GetList<VtvVideoThreadEntity>(cmd);
                totalRow = Utility.ConvertToInt(cmd.Parameters[cmd.Parameters.Count - 1]);
                return numberOfRow;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool Insert(VtvVideoThreadEntity elm, ref int id)
        {
            const string commandText = "CMS_VideoThread_Insert"; try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Name", elm.Name);
                _db.AddParameter(cmd, "Description", elm.Description);
                _db.AddParameter(cmd, "Url", elm.Url);
                _db.AddParameter(cmd, "IsHotThread", elm.IsHotThread);
                _db.AddParameter(cmd, "CreatedDate", elm.CreatedDate);
                _db.AddParameter(cmd, "ModifiedDate", elm.ModifiedDate);
                _db.AddParameter(cmd, "CreatedBy", elm.CreatedBy);
                _db.AddParameter(cmd, "EditedBy", elm.EditedBy);
                _db.AddParameter(cmd, "DistributionDate", elm.DistributionDate);
                _db.AddParameter(cmd, "UnsignName", elm.UnsignName);
                _db.AddParameter(cmd, "Avatar", elm.Avatar);
                _db.AddParameter(cmd, "Status", elm.Status);
                _db.AddParameter(cmd, "Id", id, ParameterDirection.Output);
                var numberOfRow = _db.ExecuteNonQuery(cmd);
                id = Utility.ConvertToInt(cmd.Parameters[cmd.Parameters.Count - 1]);
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool Update(VtvVideoThreadEntity elm)
        {
            const string commandText = "CMS_VideoThread_Update"; try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", elm.Id);
                _db.AddParameter(cmd, "Name", elm.Name);
                _db.AddParameter(cmd, "Description", elm.Description);
                _db.AddParameter(cmd, "Url", elm.Url);
                _db.AddParameter(cmd, "IsHotThread", elm.IsHotThread);
                _db.AddParameter(cmd, "ModifiedDate", elm.ModifiedDate);
                _db.AddParameter(cmd, "EditedBy", elm.EditedBy);
                _db.AddParameter(cmd, "DistributionDate", elm.DistributionDate);
                _db.AddParameter(cmd, "UnsignName", elm.UnsignName);
                _db.AddParameter(cmd, "Avatar", elm.Avatar);
                _db.AddParameter(cmd, "Status", elm.Status);
                var numberOfRow = _db.ExecuteNonQuery(cmd);
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool Delete(int id)
        {
            const string commandText = "CMS_VideoThread_Delete"; try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", id);
                var numberOfRow = _db.ExecuteNonQuery(cmd);
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public VtvVideoThreadEntity Select(int id)
        {
            const string commandText = "CMS_VideoThread_Select"; try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", id);
                var numberOfRow = _db.Get<VtvVideoThreadEntity>(cmd);
                return numberOfRow;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool IsHot(int id)
        {
            const string commandText = "CMS_VideoThread_IsHot"; try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", id);
                var numberOfRow = _db.ExecuteNonQuery(cmd);
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        #endregion

        #region ZONE
        public bool InsertZone(VtvVideoThreadZoneEntity elm)
        {
            const string commandText = "CMS_VideoThread_Zone_Insert"; try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "VideoThreadId", elm.VideoThreadId);
                _db.AddParameter(cmd, "VideoZoneId", elm.VideoZoneId);
                _db.AddParameter(cmd, "IsPrimary", elm.IsPrimary);
                var numberOfRow = _db.ExecuteNonQuery(cmd);
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public VtvVideoThreadZoneEntity SelectZone(int videoThreadId, bool isPrimary)
        {
            const string commandText = "CMS_VideoThread_Zone_Select"; try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "VideoThreadId", videoThreadId);
                _db.AddParameter(cmd, "IsPrimary", isPrimary);
                var numberOfRow = _db.Get<VtvVideoThreadZoneEntity>(cmd);
                return numberOfRow;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool DeleteZone(int videoThreadId)
        {
            const string commandText = "CMS_VideoThread_Zone_Delete"; try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "VideoThreadId", videoThreadId);
                var numberOfRow = _db.ExecuteNonQuery(cmd);
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        #endregion

        #region VIDEO IN VIDEO
        public List<VtvVideoThreadVideoEntity> ListVideo(int videoThreadId, int pageIndex, int pageSize, ref int totalRow)
        {
            const string commandText = "CMS_VideoThread_Video_List"; try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "VideoThreadId", videoThreadId);
                _db.AddParameter(cmd, "PageIndex", pageIndex);
                _db.AddParameter(cmd, "PageSize", pageSize);
                _db.AddParameter(cmd, "TotalRow", totalRow, ParameterDirection.Output);
                var numberOfRow = _db.GetList<VtvVideoThreadVideoEntity>(cmd);
                totalRow = Utility.ConvertToInt(cmd.Parameters[cmd.Parameters.Count - 1]);
                return numberOfRow;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool InsertVideo(VtvVideoThreadVideoEntity elm)
        {
            const string commandText = "CMS_VideoThread_Video_Insert"; try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "VideoThreadId", elm.VideoThreadId);
                _db.AddParameter(cmd, "VideoId", elm.VideoId);
                var numberOfRow = _db.ExecuteNonQuery(cmd);
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool DeleteVideo(int videoThreadId, int videoId)
        {
            const string commandText = "CMS_VideoThread_Video_Delete"; try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "VideoThreadId", videoThreadId);
                _db.AddParameter(cmd, "VideoId", videoId);
                var numberOfRow = _db.ExecuteNonQuery(cmd);
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public VtvVideoThreadVideoEntity ExistVideo(int videoThreadId, int videoId)
        {
            const string commandText = "CMS_VideoThread_Video_Exist"; try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "VideoThreadId", videoThreadId);
                _db.AddParameter(cmd, "videoId", videoId);
                var numberOfRow = _db.Get<VtvVideoThreadVideoEntity>(cmd);
                return numberOfRow;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        #endregion

        #region TAG
        public List<VtvVideoThreadTagEntity> ListTag(int videoThreadId)
        {
            const string commandText = "CMS_VideoThread_Tag_List"; try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "VideoThreadId", videoThreadId);
                var numberOfRow = _db.GetList<VtvVideoThreadTagEntity>(cmd);
                return numberOfRow;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool InsertTag(VtvVideoThreadTagEntity elm)
        {
            const string commandText = "CMS_VideoThread_Tag_Insert"; try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "VideoThreadId", elm.VideoThreadId);
                _db.AddParameter(cmd, "VideoTagId", elm.VideoTagId);
                var numberOfRow = _db.ExecuteNonQuery(cmd);
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool DeleteTag(int videoThreadId)
        {
            const string commandText = "CMS_VideoThread_Tag_Delete"; try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "VideoThreadId", videoThreadId);
                var numberOfRow = _db.ExecuteNonQuery(cmd);
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        #endregion

        #region Core members

        private readonly CmsMainDb _db;

        protected VtvVideoThreadDalBase(CmsMainDb db)
        {
            _db = db;
        }

        protected CmsMainDb Database
        {
            get { return _db; }
        }

        #endregion
    }
}
