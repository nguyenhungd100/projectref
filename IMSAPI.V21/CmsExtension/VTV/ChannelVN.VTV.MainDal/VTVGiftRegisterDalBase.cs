﻿using ChannelVN.CMS.Common;

using ChannelVN.VTV.Entity;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using ChannelVN.VTV.MainDal.Common;
using ChannelVN.VTV.MainDal.Databases;

namespace ChannelVN.VTV.MainDal
{
    public abstract class VTVGiftRegisterDalBase
    {
        /// <summary>
        /// Lấy toàn bộ danh sách đăng ký nhận quà tặng
        /// </summary>
        /// <param name="keyword">string</param>
        /// <param name="giftId">int</param>
        /// <param name="status">int</param>
        /// <param name="from">Datetime</param>
        /// <param name="to">Datetime</param>
        /// <param name="order">int</param>
        /// <param name="pageIndex">int</param>
        /// <param name="pageSize">int</param>
        /// <param name="totalRow">int output</param>
        /// <returns>list Entity</returns>
        public List<VTVGiftRegisterEntity> ListGiftRegister(string keyword, int giftId, int status, DateTime from, DateTime to, int order,
                                                          int pageIndex, int pageSize, ref int totalRow)
        {
            const string commandText = "CMS_VTV_GiftRegister_GetList"; try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Keyword", keyword);
                if (from <= Constants.MinDateTime)
                    _db.AddParameter(cmd, "from", DBNull.Value);
                else _db.AddParameter(cmd, "from", from);
                if (to <= Constants.MinDateTime)
                    _db.AddParameter(cmd, "to", DBNull.Value);
                else _db.AddParameter(cmd, "to", to);
                _db.AddParameter(cmd, "giftId", giftId);
                _db.AddParameter(cmd, "Status", status);
                _db.AddParameter(cmd, "Order", order);
                _db.AddParameter(cmd, "PageIndex", pageIndex);
                _db.AddParameter(cmd, "PageSize", pageSize);
                _db.AddParameter(cmd, "totalRow", totalRow,ParameterDirection.Output);
                var numberOfRow = _db.GetList<VTVGiftRegisterEntity>(cmd);
                Utility.ConvertToInt(cmd.Parameters[cmd.Parameters.Count - 1]);
                return numberOfRow;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        /// <summary>
        /// Lấy thông tin chi tiết quà tặng
        /// </summary>
        /// <param name="Id">int</param>
        /// <returns>Entity</returns>
        public VTVGiftRegisterEntity GetGiftRegisterById(int Id)
        {
            const string commandText = "CMS_VTV_GiftRegister_GetById"; try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", Id);
                var numberOfRow = _db.Get<VTVGiftRegisterEntity>(cmd);
                return numberOfRow;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        /// <summary>
        /// Cập nhật thông tin đăng ký quà tặng
        /// </summary>
        /// <param name="info">Entity</param>
        /// <returns>true/false</returns>
        public bool UpdateGiftRegister(VTVGiftRegisterEntity info)
        {
            const string commandText = "CMS_VTV_GiftRegister_Update"; try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", info.Id);
                _db.AddParameter(cmd, "GiftId", info.GiftId);
                _db.AddParameter(cmd, "Name", info.FullName);
                _db.AddParameter(cmd, "Address", info.Address);
                _db.AddParameter(cmd, "IdentityCard", info.IdentityCard);
                _db.AddParameter(cmd, "PhoneNumber", info.PhoneNumber);
                _db.AddParameter(cmd, "Email", info.Email);
                _db.AddParameter(cmd, "Status", info.Status);
                var numberOfRow = _db.ExecuteNonQuery(cmd);
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        /// <summary>
        /// Xóa thông tin. Kiểm tra nếu là quyền thư ký (ArticleAdmin) thì mới cho phép xóa
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public bool DeleteGiftRegister(int id)
        {
            const string commandText = "CMS_VTV_GiftRegister_Delete"; try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", id);
                var numberOfRow = _db.ExecuteNonQuery(cmd);
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        #region Core members

        private readonly ExternalCmsDb _db;

        protected VTVGiftRegisterDalBase(ExternalCmsDb db)
        {
            _db = db;
        }

        protected ExternalCmsDb Database
        {
            get { return _db; }
        }

        #endregion
    }
}
