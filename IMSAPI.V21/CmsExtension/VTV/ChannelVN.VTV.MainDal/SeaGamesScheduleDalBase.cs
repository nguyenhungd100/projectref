﻿using System;
using System.Collections.Generic;
using ChannelVN.VTV.Entity;
using System.Data.SqlClient;
using ChannelVN.CMS.Common;
using System.Data;
using ChannelVN.VTV.MainDal.Databases;


namespace ChannelVN.VTV.MainDal
{
    public abstract class SeaGamesScheduleDalBase
    {
        public List<SeaGamesScheduleEntity> GetList(string keyword, int pageIndex, int pageSize, ref int totalRow)
        {
            const string commandText = "CMS_SeaGamesSchedule_GetList"; try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Keyword", keyword);
                _db.AddParameter(cmd, "PageIndex", pageIndex);
                _db.AddParameter(cmd, "PageSize", pageSize);
                _db.AddParameter(cmd, "TotalRow", totalRow, ParameterDirection.Output);
                var numberOfRow = _db.GetList<SeaGamesScheduleEntity>(cmd);
                totalRow = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, cmd.Parameters.Count - 1));
                return numberOfRow;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        //public bool Insert(SeaGamesScheduleEntity elm, ref int id)
        //{
        //    const string commandText = "CMS_SeaGamesSchedule_Insert"; try
        //    {
        //        var cmd = _db.CreateCommand(commandText, true);
        //        _db.AddParameter(cmd, "ChannelName", elm.ChannelName);
        //        _db.AddParameter(cmd, "Status", elm.Status);
        //        _db.AddParameter(cmd, "Id", id, ParameterDirection.Output);
        //        var numberOfRow = _db.ExecuteNonQuery(cmd);
        //        id = Utility.ConvertToInt(cmd.Parameters[cmd.Parameters.Count - 1]);
        //        return numberOfRow > 0;
        //    }
        //    catch (Exception ex)
        //    {
        //        throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
        //    }
        //}
        public bool Update(SeaGamesScheduleEntity elm)
        {
            const string commandText = "CMS_SeaGamesSchedule_Update"; try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", elm.Id);
                _db.AddParameter(cmd, "Title", elm.Title);
                _db.AddParameter(cmd, "Sapo", elm.Sapo);
                _db.AddParameter(cmd, "ScheduleDate", elm.ScheduleDate);
                _db.AddParameter(cmd, "ScheduleTime", elm.ScheduleTime);
                var numberOfRow = _db.ExecuteNonQuery(cmd);
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public SeaGamesScheduleEntity Select(int id)
        {
            const string commandText = "CMS_SeaGamesSchedule_GetById"; try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", id);
                var numberOfRow = _db.Get<SeaGamesScheduleEntity>(cmd);
                return numberOfRow;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool Delete(int id)
        {
            const string commandText = "CMS_SeaGamesSchedule_Delete"; try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", id);
                var numberOfRow = _db.ExecuteNonQuery(cmd);
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        #region Core members

        private readonly CmsMainDb _db;

        protected SeaGamesScheduleDalBase(CmsMainDb db)
        {
            _db = db;
        }

        protected CmsMainDb Database
        {
            get { return _db; }
        }

        #endregion
    }
}
