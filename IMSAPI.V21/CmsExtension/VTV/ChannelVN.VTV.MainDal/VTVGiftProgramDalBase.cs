﻿using ChannelVN.CMS.Common;

using ChannelVN.VTV.Entity;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using ChannelVN.VTV.MainDal.Databases;

namespace ChannelVN.VTV.MainDal
{
    public abstract class VTVGiftProgramDalBase
    {
        /// <summary>
        /// Lấy toàn bộ danh sách chương trình quà tặng
        /// </summary>
        /// <param name="keyword">string</param>
        /// <param name="status">int</param>
        /// <param name="pageIndex">int</param>
        /// <param name="pageSize">int</param>
        /// <param name="totalRow">int output</param>
        /// <returns>list Entity</returns>
        public List<VTVGiftProgramEntity> ListGiftProgram(string keyword,
                                                                            int status, int pageIndex, int pageSize, ref int totalRow)
        {
            const string commandText = "CMS_VTV_GiftProgram_GetList"; try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Keyword", keyword);
                //(registeredDate <= DbCommon.MinDateTime
                //     ? _db.AddParameter(cmd, "registeredDate", DBNull.Value)
                //     : _db.AddParameter(cmd, "registeredDate", registeredDate));
                //_db.AddParameter(cmd, "Type", type);
                _db.AddParameter(cmd, "Status", status);
                //_db.AddParameter(cmd, "Order", order);
                _db.AddParameter(cmd, "PageIndex", pageIndex);
                _db.AddParameter(cmd, "PageSize", pageSize);
                _db.AddParameter(cmd, "totalRow", totalRow, ParameterDirection.Output);
                var numberOfRow = _db.GetList<VTVGiftProgramEntity>(cmd);
                totalRow = Utility.ConvertToInt(cmd.Parameters[cmd.Parameters.Count - 1]);
                return numberOfRow;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public List<VTVGiftProgramEntity> ListGiftProgram()
        {
            const string commandText = "CMS_VTV_GiftProgram_GetListAll"; try
            {
                var cmd = _db.CreateCommand(commandText, true);
                var numberOfRow = _db.GetList<VTVGiftProgramEntity>(cmd);
                return numberOfRow;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        /// <summary>
        /// Lấy thông tin chi tiết chương trình quà tặng
        /// </summary>
        /// <param name="Id">int</param>
        /// <returns>Entity</returns>
        public VTVGiftProgramEntity GetGiftProgramById(int Id)
        {
            const string commandText = "CMS_VTV_GiftProgram_GetById"; try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", Id);
                var numberOfRow = _db.Get<VTVGiftProgramEntity>(cmd);
                return numberOfRow;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        /// <summary>
        /// Cập nhật thông tin chương trình quà tặng
        /// </summary>
        /// <param name="info">Entity</param>
        /// <returns>true/false</returns>
        public bool UpdateGiftProgramInfo(VTVGiftProgramEntity info)
        {
            const string commandText = "CMS_VTV_GiftProgram_UpdateInfo"; try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", info.Id);
                _db.AddParameter(cmd, "NameProgram", info.NameProgram);
                _db.AddParameter(cmd, "Status", info.Status);
                var numberOfRow = _db.ExecuteNonQuery(cmd);
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="info"></param>
        /// <returns></returns>
        public bool InsertGiftProgramInfo(VTVGiftProgramEntity info, ref int id)
        {
            const string commandText = "CMS_VTV_GiftProgram_Insert"; try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "NameProgram", info.NameProgram);
                _db.AddParameter(cmd, "Status", info.Status);
                _db.AddParameter(cmd, "Id", id, ParameterDirection.Output);
                var numberOfRow = _db.ExecuteNonQuery(cmd);
                id = Utility.ConvertToInt(cmd.Parameters[cmd.Parameters.Count - 1]);
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        /// <summary>
        /// Xóa thông tin chương trình quà tặng. Kiểm tra nếu là quyền thư ký (ArticleAdmin) thì mới cho phép xóa
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public bool DeleteGiftProgram(int id)
        {
            const string commandText = "CMS_VTV_GiftProgram_Delete"; try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", id);
                var numberOfRow = _db.ExecuteNonQuery(cmd);
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        #region Core members

        private readonly CmsMainDb _db;

        protected VTVGiftProgramDalBase(CmsMainDb db)
        {
            _db = db;
        }

        protected CmsMainDb Database
        {
            get { return _db; }
        }

        #endregion
    }
}
