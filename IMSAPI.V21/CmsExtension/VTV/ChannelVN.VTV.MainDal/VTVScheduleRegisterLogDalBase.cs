﻿using System;
using System.Collections.Generic;
using System.Data;
using ChannelVN.CMS.Common;
using ChannelVN.VTV.Entity;
using ChannelVN.VTV.MainDal.Databases;

namespace ChannelVN.VTV.MainDal
{
    public abstract class VTVScheduleRegisterLogDalBase
    {
        public List<VTVScheduleRegisterLogEntity> ListScheduleRegisterLog(int scheduleRegisterId, int pageIndex, int pageSize, ref int totalRow)
        {
            const string commandText = "CMS_VTV_VTVScheduleRegisterLog_GetByVTVScheduleRegisterId";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "VTVScheduleRegisterId", scheduleRegisterId);
                _db.AddParameter(cmd, "PageIndex", pageIndex);
                _db.AddParameter(cmd, "PageSize", pageSize);
                _db.AddParameter(cmd, "TotalRow", totalRow, ParameterDirection.Output);
                var numberOfRow = _db.GetList<VTVScheduleRegisterLogEntity>(cmd);
                totalRow = Utility.ConvertToInt(cmd.Parameters[cmd.Parameters.Count - 1]);
                return numberOfRow;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public bool Insert(VTVScheduleRegisterLogEntity entity, ref int id)
        {
            const string commandText = "CMS_VTV_VTVScheduleRegisterLog_LogSendScheduleToCustomer"; 
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "VTVScheduleRegisterId", entity.VTVScheduleRegisterId);
                _db.AddParameter(cmd, "ProgramChannelId", entity.ProgramChannelId);
                _db.AddParameter(cmd, "Email", entity.Email);
                _db.AddParameter(cmd, "ScheduleOnWeek", entity.ScheduleOnWeek);
                _db.AddParameter(cmd, "ScheduleOnMonth", entity.ScheduleOnMonth);
                _db.AddParameter(cmd, "ScheduleOnYear", entity.ScheduleOnYear);
                _db.AddParameter(cmd, "ScheduleFilePath", entity.ScheduleFilePath);
                _db.AddParameter(cmd, "FromDate", entity.FromDate);
                _db.AddParameter(cmd, "ToDate", entity.ToDate);
                _db.AddParameter(cmd, "Id", id, ParameterDirection.Output);
                var numberOfRow = _db.ExecuteNonQuery(cmd);
                id = Utility.ConvertToInt(cmd.Parameters[cmd.Parameters.Count - 1]);
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        #region Core members

        private readonly ExternalCmsDb _db;

        protected VTVScheduleRegisterLogDalBase(ExternalCmsDb db)
        {
            _db = db;
        }

        protected ExternalCmsDb Database
        {
            get { return _db; }
        }

        #endregion
    }
}
