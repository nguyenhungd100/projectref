﻿using System;
using System.Collections.Generic;
using ChannelVN.VTV.Entity;
using System.Data.SqlClient;
using ChannelVN.CMS.Common;
using System.Data;
using ChannelVN.VTV.MainDal.Databases;


namespace ChannelVN.VTV.MainDal
{
    public abstract class NewsPrRoyaltyDalBase
    {
        public NewsPrRoyaltyEntity GetByNewsId(long newsId)
        {
            const string commandText = "CMS_NewsPrRoyalty_GetByNewsId";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Newsid", newsId);
                var numberOfRow = _db.Get<NewsPrRoyaltyEntity>(cmd);
                return numberOfRow;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool Update(NewsPrRoyaltyEntity elm)
        {
            const string commandText = "CMS_NewsPrRoyalty_Update"; try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", elm.Id);
                _db.AddParameter(cmd, "Discount", elm.Discount);
                _db.AddParameter(cmd, "NewsId", elm.NewsId);
                _db.AddParameter(cmd, "Price", elm.Price);
                _db.AddParameter(cmd, "TotalAmount", elm.TotalAmount);
                var numberOfRow = _db.ExecuteNonQuery(cmd);
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        #region Core members

        private readonly CmsMainDb _db;

        protected NewsPrRoyaltyDalBase(CmsMainDb db)
        {
            _db = db;
        }

        protected CmsMainDb Database
        {
            get { return _db; }
        }

        #endregion
    }
}
