﻿using System.Data;
using ChannelVN.CMS.Common.DalCommon;

namespace ChannelVN.VTV.MainDal.Databases
{
    /// <summary>
    /// The base class for the <see cref="ExternalCmsDb"/> class that 
    /// represents a connection to the <c>ExternalCmsDb</c> database. 
    /// </summary>
    /// <remarks>
    /// Do not change this source code. Modify the ExternalCmsDb class
    /// if you need to add or change some functionality.
    /// </remarks>
    public abstract class ExternalCmsDbBase : MainDbBase
    {
        #region Store procedures

        #region VTVLiveShow Register

        private VTVLiveShowRegisterDal _vTVLiveShowRegisterMainDal;
        public VTVLiveShowRegisterDal VTVLiveShowRegisterMainDal
        {
            get { return _vTVLiveShowRegisterMainDal ?? (_vTVLiveShowRegisterMainDal = new VTVLiveShowRegisterDal((ExternalCmsDb)this)); }
        }

        #endregion

        #region VTV Schedule Register

        private VTVScheduleRegisterDal _vTVScheduleRegisterMainDal;
        public VTVScheduleRegisterDal VTVScheduleRegisterMainDal
        {
            get { return _vTVScheduleRegisterMainDal ?? (_vTVScheduleRegisterMainDal = new VTVScheduleRegisterDal((ExternalCmsDb)this)); }
        }

        #endregion

        #region VTV Schedule Register Log

        private VTVScheduleRegisterLogDal _vTVScheduleRegisterLogMainDal;
        public VTVScheduleRegisterLogDal VTVScheduleRegisterLogMainDal
        {
            get { return _vTVScheduleRegisterLogMainDal ?? (_vTVScheduleRegisterLogMainDal = new VTVScheduleRegisterLogDal((ExternalCmsDb)this)); }
        }

        #endregion

        #region VTVGift Register

        private VTVGiftRegisterDal _vTVGiftRegisterMainDal;
        public VTVGiftRegisterDal VTVGiftRegisterMainDal
        {
            get { return _vTVGiftRegisterMainDal ?? (_vTVGiftRegisterMainDal = new VTVGiftRegisterDal((ExternalCmsDb)this)); }
        }

        #endregion

        #endregion

        #region Constructors

        protected ExternalCmsDbBase()
        {
        }
        protected ExternalCmsDbBase(bool init)
        {
            if (init)
            {
                InitConnection();
            }
        }

        #endregion

        #region Protected members

        protected override sealed void InitConnection()
        {
            _connection = CreateConnection();
            _connection.Open();
        }

        /// <summary>
        /// Creates a new connection to the database.
        /// </summary>
        /// <returns>A reference to the <see cref="System.Data.IDbConnection"/> object.</returns>
        protected abstract IDbConnection CreateConnection();

        #endregion
    }
}