﻿using System.Data;
using ChannelVN.CMS.Common.DalCommon;

namespace ChannelVN.VTV.MainDal.Databases
{
    /// <summary>
    /// The base class for the <see cref="CmsMainDb"/> class that 
    /// represents a connection to the <c>CmsMainDb</c> database. 
    /// </summary>
    /// <remarks>
    /// Do not change this source code. Modify the CmsMainDb class
    /// if you need to add or change some functionality.
    /// </remarks>
    public abstract class CmsMainDbBase : MainDbBase
    {
        #region Store procedures

        #region VtvChannel

        private VtvChannelDal _vtvChannelMainDal;
        public VtvChannelDal VtvChannelMainDal
        {
            get { return _vtvChannelMainDal ?? (_vtvChannelMainDal = new VtvChannelDal((CmsMainDb)this)); }
        }

        #endregion

        #region VTVGift

        private VTVGiftDal _vTVGiftMainDal;
        public VTVGiftDal VTVGiftMainDal
        {
            get { return _vTVGiftMainDal ?? (_vTVGiftMainDal = new VTVGiftDal((CmsMainDb)this)); }
        }

        #endregion

        #region VTVGift Program

        private VTVGiftProgramDal _vTVGiftProgramMainDal;
        public VTVGiftProgramDal VTVGiftProgramMainDal
        {
            get { return _vTVGiftProgramMainDal ?? (_vTVGiftProgramMainDal = new VTVGiftProgramDal((CmsMainDb)this)); }
        }

        #endregion

        private BoxCountDownChannelDal _BoxCountDownChannelMainDal;
        public BoxCountDownChannelDal BoxCountDownChannelMainDal
        {
            get { return _BoxCountDownChannelMainDal ?? (_BoxCountDownChannelMainDal = new BoxCountDownChannelDal((CmsMainDb)this)); }
        }

        #region VTVLiveShow Register

        //private VTVLiveShowRegisterDal _vTVLiveShowRegisterMainDal;
        //public VTVLiveShowRegisterDal VTVLiveShowRegisterMainDal
        //{
        //    get { return _vTVLiveShowRegisterMainDal ?? (_vTVLiveShowRegisterMainDal = new VTVLiveShowRegisterDal((CmsMainDb)this)); }
        //}

        #endregion

        #region Vtv Program Schedule

        private VTVProgramScheduleDal _vTVProgramScheduleMainDal;
        public VTVProgramScheduleDal VTVProgramScheduleMainDal
        {
            get { return _vTVProgramScheduleMainDal ?? (_vTVProgramScheduleMainDal = new VTVProgramScheduleDal((CmsMainDb)this)); }
        }

        #endregion

        #region Vtv Program Schedule Detail

        private VTVProgramScheduleDetailDal _vTVProgramScheduleDetailMainDal;
        public VTVProgramScheduleDetailDal VTVProgramScheduleDetailMainDal
        {
            get { return _vTVProgramScheduleDetailMainDal ?? (_vTVProgramScheduleDetailMainDal = new VTVProgramScheduleDetailDal((CmsMainDb)this)); }
        }

        #endregion

        #region Vtv Schedule

        private VtvScheduleDal _vtvScheduleMainDal;
        public VtvScheduleDal VtvScheduleMainDal
        {
            get { return _vtvScheduleMainDal ?? (_vtvScheduleMainDal = new VtvScheduleDal((CmsMainDb)this)); }
        }

        #endregion

        

        #region VTVVideoRecord

        private VTVVideoRecordDal _vTVVideoRecordMainDal;
        public VTVVideoRecordDal VTVVideoRecordMainDal
        {
            get { return _vTVVideoRecordMainDal ?? (_vTVVideoRecordMainDal = new VTVVideoRecordDal((CmsMainDb)this)); }
        }

        #endregion

        #region VtvVideoThread

        private VtvVideoThreadDal _vtvVideoThreadMainDal;
        public VtvVideoThreadDal VtvVideoThreadMainDal
        {
            get { return _vtvVideoThreadMainDal ?? (_vtvVideoThreadMainDal = new VtvVideoThreadDal((CmsMainDb)this)); }
        }

        #endregion

        private SeaGamesScheduleDal _SeaGamesScheduleMainDal;
        public SeaGamesScheduleDal SeaGamesScheduleMainDal
        {
            get { return _SeaGamesScheduleMainDal ?? (_SeaGamesScheduleMainDal = new SeaGamesScheduleDal((CmsMainDb)this)); }
        }

        private NewsPrRoyaltyDal _NewsPrRoyaltyMainDal;
        public NewsPrRoyaltyDal NewsPrRoyaltyMainDal
        {
            get { return _NewsPrRoyaltyMainDal ?? (_NewsPrRoyaltyMainDal = new NewsPrRoyaltyDal((CmsMainDb)this)); }
        }
        #endregion

        #region Constructors

        protected CmsMainDbBase()
        {
        }
        protected CmsMainDbBase(bool init)
        {
            if (init)
            {
                InitConnection();
            }
        }

        #endregion

        #region Protected members

        protected override sealed void InitConnection()
        {
            _connection = CreateConnection();
            _connection.Open();
        }

        /// <summary>
        /// Creates a new connection to the database.
        /// </summary>
        /// <returns>A reference to the <see cref="System.Data.IDbConnection"/> object.</returns>
        protected abstract IDbConnection CreateConnection();

        #endregion
    }
}