﻿using System.Data;
using System.Data.SqlClient;
using ChannelVN.CMS.Common.ChannelConfig;
using ChannelVN.VTV.MainDal.Common;
using ChannelVN.WcfExtensions;

namespace ChannelVN.VTV.MainDal.Databases
{
    public class ExternalCmsDb : ExternalCmsDbBase
    {
        private const string ConnectionStringName = "ExternalCmsDb";

        /// <summary>
        /// Creates a new connection to the database.
        /// </summary>
        /// <returns>An <see cref="System.Data.IDbConnection"/> object.</returns>
        protected override IDbConnection CreateConnection()
        {
            var strConn = ServiceChannelConfiguration.GetConnectionString(WcfMessageHeader.Current.Namespace,
                ConnectionStringName, Constants.ConnectionDecryptKey);
            return new SqlConnection(strConn);
        }
    }
}