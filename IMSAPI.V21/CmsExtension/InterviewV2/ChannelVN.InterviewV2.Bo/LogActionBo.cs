﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using ChannelVN.CMS.BO;
using ChannelVN.InterviewV2.Entity.ErrorCode;
using ChannelVN.InterviewV2.Entity;
using ChannelVN.InterviewV2.Dal;

namespace ChannelVN.InterviewV2.Bo
{
    public class LogActionBo
    {
        public static ErrorMapping.ErrorCodes InsertLog(int interviewId, string userName, string action, ref int id)
        {
            var result = ErrorMapping.ErrorCodes.BusinessError;
            var createSuccess = LogAction.InsertLog(interviewId, userName, action, ref id);
            if (createSuccess && id > 0)
                result = ErrorMapping.ErrorCodes.Success;
            return result;
        }

        public static List<LogActionEntity> ListLog(int interviewId, int pageIndex, int pageSize, ref int totalRow) {
            return LogAction.ListLog(interviewId,pageIndex, pageSize, ref totalRow);
        }
    }
}
