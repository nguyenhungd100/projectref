﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using ChannelVN.CMS.BO;
using ChannelVN.CMS.Common;
using ChannelVN.InterviewV2.Entity.ErrorCode;
using ChannelVN.InterviewV2.Entity;
using ChannelVN.InterviewV2.Dal;


namespace ChannelVN.InterviewV2.Bo
{
    public class QuestionBo
    {
        public static List<QuestionEntity> ListQuestionRespondentInterView(int interviewId, int respondentId, EnumStatusQuestion status, int pageIndex, int pageSize, ref int totalRow)
        {
            return QuestionDal.ListQuestionByInterView(interviewId, respondentId,(int)status, pageIndex, pageSize, ref totalRow);
        }
        public static List<PublishedQuestionEntity> GetAllPublishedQuestion(int interviewId)
        {
            return QuestionDal.GetAllPublishedQuestion(interviewId);
        }
        public static List<QuestionEntity> ListQuestionByAnswerd(int interviewId, EnumStatusQuestion status,int orderBy, int pageIndex, int pageSize, ref int totalRow)
        {
            return QuestionDal.ListQuestionByAnswerded(interviewId, (int)status, orderBy, pageIndex, pageSize, ref totalRow);
        }
        public static QuestionEntity GetQuestionById(int questionId) {
            return QuestionDal.GetQuestionById(questionId);
        }
        public static ErrorMapping.ErrorCodes Answer(string AnswerContent, EnumStatusQuestion status, int Id)
        {
            if (QuestionDal.Answer(AnswerContent, (int)status, Id))
            {
                var interview = InterviewDal.SelectInterviewByQuestionId(Id);
                InterviewBo.UpdateInterviewQuestionIntoRedis(interview.ID);
                return ErrorMapping.ErrorCodes.Success;
            }
            else
            {
                return ErrorMapping.ErrorCodes.BusinessError;
            }
        }
        public static ErrorMapping.ErrorCodes InsertQuestion(QuestionEntity questionInfo, ref int id)
        {
            var result = ErrorMapping.ErrorCodes.BusinessError;
            var createSuccess = QuestionDal.InsertQuestion(questionInfo, ref id);
            if (createSuccess && id > 0)
                result = ErrorMapping.ErrorCodes.Success;
            return result;
        }
        public static ErrorMapping.ErrorCodes UpdateQuestion(QuestionEntity questionInfo)
        {
            var result = ErrorMapping.ErrorCodes.BusinessError;
            var createSuccess = QuestionDal.UpdateQuestion(questionInfo);
            if (createSuccess && questionInfo.ID > 0)
            {
                var question = QuestionDal.GetQuestionById(questionInfo.ID);
                if (question.Status == (int)EnumStatusQuestion.published || question.Status == (int)EnumStatusQuestion.removed)
                {
                    InterviewBo.UpdateInterviewQuestionIntoRedis(question.InterviewId);
                }
                result = ErrorMapping.ErrorCodes.Success;
            }
            return result;
        }
        public static ErrorMapping.ErrorCodes DeleteQuestion(int id)
        {
            var result = ErrorMapping.ErrorCodes.BusinessError;
            var question = QuestionDal.GetQuestionById(id);
            var createSuccess = QuestionDal.Delete(id);
            if (createSuccess)
            {
                if (question.Status == (int)EnumStatusQuestion.published)
                {
                    InterviewBo.UpdateInterviewQuestionIntoRedis(question.InterviewId);
                }
                result = ErrorMapping.ErrorCodes.Success;
            }
            return result;
        }
        public static ErrorMapping.ErrorCodes UpdatePriority(int interviewId, string listOfInterviewQuestionId) {

            var result = ErrorMapping.ErrorCodes.BusinessError;
            if (interviewId <= 0 || string.IsNullOrEmpty(listOfInterviewQuestionId))
            {
                return ErrorMapping.ErrorCodes.UpdateInterviewQuestionNotSuccess;
            }
            var interview = InterviewDal.SelectInterviewById(interviewId);
            if (interview == null)
            {
                return ErrorMapping.ErrorCodes.UpdateInterviewQuestionInterviewNotFound;
            }
            var createSuccess = QuestionDal.UpdatePriority(interviewId,listOfInterviewQuestionId);
            if (createSuccess && interviewId > 0)
            {
                InterviewBo.UpdateInterviewQuestionIntoRedis(interviewId);
                result = ErrorMapping.ErrorCodes.Success;
            }
            return result;
        }
        public static ErrorMapping.ErrorCodes InsertAnswer(string answerContent, int interviewId, EnumStatusQuestion status, ref int id)
        {
            var result = ErrorMapping.ErrorCodes.BusinessError;
            var createSuccess = QuestionDal.InsertAnswer(answerContent,interviewId,(int)status, ref id);
            if (createSuccess && id > 0)
            {
                if (status == EnumStatusQuestion.published)
                {
                    InterviewBo.UpdateInterviewQuestionIntoRedis(interviewId);
                }
                result = ErrorMapping.ErrorCodes.Success;
            }
            return result;
        }
    }
}
