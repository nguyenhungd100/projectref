﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using ChannelVN.CMS.BO;
using ChannelVN.InterviewV2.Entity.ErrorCode;
using ChannelVN.InterviewV2.Entity;
using ChannelVN.InterviewV2.Dal;


namespace ChannelVN.InterviewV2.Bo
{
    public class RespondentBo
    {
        public static ErrorMapping.ErrorCodes Insert(RespondentEntity info, ref int id)
        {
            var result = ErrorMapping.ErrorCodes.BusinessError;
            var createSuccess = RespondentDal.Insert(info, ref id);
            if (createSuccess && id > 0)
                result = ErrorMapping.ErrorCodes.Success;
            return result;
        }
        public static ErrorMapping.ErrorCodes Update(RespondentEntity info)
        {
            return RespondentDal.Update(info) ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.BusinessError;
        }
        public static ErrorMapping.ErrorCodes Delete(int id)
        {
            return RespondentDal.Delete(id) ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.BusinessError;
        }
        public static List<RespondentEntity> ListRespondentInterView(int interviewId)
        {
            return RespondentDal.ListRespondentInterView(interviewId);
        }
        public static List<RespondentEntity> ListRespondentInterView(int interviewId, int pageIndex, int pageSize, ref int totalRow)
        {
            return RespondentDal.ListRespondentInterView(interviewId, pageIndex, pageSize, ref totalRow);
        }
        public static RespondentEntity RespondentById(int id)
        {
            return RespondentDal.RespondentById(id);
        }
    }
}
