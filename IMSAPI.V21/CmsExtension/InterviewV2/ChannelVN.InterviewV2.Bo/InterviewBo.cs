﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using ChannelVN.CMS.BO;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.Common.ChannelConfig;
using ChannelVN.CMS.Entity.Base.Interview;
using ChannelVN.InterviewV2.Entity.ErrorCode;
using ChannelVN.InterviewV2.Entity;
using ChannelVN.InterviewV2.Dal;
using ChannelVN.WcfExtensions;
using ChannelVN.CMS.Common.RedisClientHelper;

namespace ChannelVN.InterviewV2.Bo
{
    public class InterviewBo
    {
        public static InterviewV2Entity SelectInterviewByNewsId(long newsId)
        {
            try
            {
                return InterviewDal.SelectInterviewByNewsId(newsId);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Fatal,  ex.ToString());
            }
            return null;
        }
        public static InterviewV2Entity SelectInterviewById(int interviewId)
        {
            return InterviewDal.SelectInterviewById(interviewId);
        }
        public static ErrorMapping.ErrorCodes Insert(InterviewV2Entity interviewInfo, ref int id)
        {
            var result = ErrorMapping.ErrorCodes.BusinessError;
            var createSuccess = InterviewDal.InsertInterview(interviewInfo, ref id);
            if (createSuccess && id > 0)
                result = ErrorMapping.ErrorCodes.Success;
            return result;
        }
        public static ErrorMapping.ErrorCodes UpdateStatusInterview(int id, EnumStatusInterview status)
        {
            var result = ErrorMapping.ErrorCodes.BusinessError;
            var createSuccess = InterviewDal.UpdateStatusInterview(id,(int)status);
            if (createSuccess && id > 0)
                result = ErrorMapping.ErrorCodes.Success;
            return result;
        }

        public static ErrorMapping.ErrorCodes UpdateInterviewQuestionIntoRedis(int interviewId)
        {
            try
            {
                var allData = new List<InterviewQuestionForPublishEntity>();

                var interviewQuestionHtmlFormatForAllContent = ServiceChannelConfiguration.GetAppSetting(WcfMessageHeader.Current.Namespace, "InterviewQuestionFormat");

                var allInterviewContent = new StringBuilder();
                var interviewQuestions = QuestionBo.GetAllPublishedQuestion(interviewId);
                if (interviewQuestions != null && interviewQuestions.Count > 0)
                {
                    foreach (var interviewQuestion in interviewQuestions)
                    {
                        var eventInfo = new InterviewQuestionForPublishEntity
                        {
                            Id = interviewQuestion.ID,
                            Priority = interviewQuestion.Priority,
                            Type = interviewQuestion.RespondentId == 0 ? 1 : 0,
                            Content = interviewQuestion.QuestionContent,
                            Answer = interviewQuestion.AnswerContent,
                            FullName = interviewQuestion.FullName,
                            Sex = -1,
                            Age = interviewQuestion.Age,
                            Email = interviewQuestion.Email,
                            Job = "",
                            Address = interviewQuestion.Address,
                            Mobile = "",
                            DistributedDate = interviewQuestion.AnswerDate,
                            ChannelId = interviewQuestion.RespondentId,
                            ChannelName = interviewQuestion.RespondentName,
                            ChannelAvatar = interviewQuestion.RespondentAvatar
                        };
                        allData.Add(eventInfo);

                        var askInfo = interviewQuestion.Address;
                        allInterviewContent.AppendFormat(interviewQuestionHtmlFormatForAllContent, askInfo, interviewQuestion.QuestionContent, interviewQuestion.RespondentName,
                                                        interviewQuestion.AnswerContent);
                    }





                    //var newsUseRollingNews = NewsDal.GetNewsUseRollingNews(interviewId);

                    //foreach (var news in newsUseRollingNews)
                    //{
                    //    NewsDal.PublishNewsContentOnly(news.Id, allInterviewContent);
                    //}


                    //var focusDataKeyFormat = ChannelConfiguration.GetAppSetting(WcfMessageHeader.Current.Namespace, "RedisKeyFormatForCmsRollingNewsHotData");
                    //var focusDataKey = string.Format(focusDataKeyFormat, rollingNewsId);
                    //RedisHelper.Remove(dbNumber, focusDataKey);
                    //RedisHelper.Add(dbNumber, focusDataKey, focusData);
                }

                var dbNumber = Utility.ConvertToInt(ServiceChannelConfiguration.GetAppSetting(WcfMessageHeader.Current.Namespace, "RedisPublishDb"));
                var allDataKeyFormat = ServiceChannelConfiguration.GetAppSetting(WcfMessageHeader.Current.Namespace, "RedisKeyFormatForCmsInterviewData");
                var allDataKey = string.Format(allDataKeyFormat, interviewId);
                var redisClient = new RedisStackClient(dbNumber);
                redisClient.Remove(allDataKey);
                redisClient.Add(allDataKey, allData, DateTime.Now.AddYears(1).TimeOfDay);

                redisClient.Remove(allDataKey+"all");
                redisClient.Add(allDataKey + "all", allInterviewContent.ToString(), DateTime.Now.AddYears(1));

                return ErrorMapping.ErrorCodes.Success;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error,
                                "UpdateInterviewQuestionIntoRedis(" + interviewId + ")" + ex);
            }
            return ErrorMapping.ErrorCodes.BusinessError;
        }
    }
}
