﻿using ChannelVN.CMS.Common;
using ChannelVN.InterviewV2.Entity;
using ChannelVN.InterviewV2.MainDal.Databases;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ChannelVN.InterviewV2.MainDal
{
    public abstract class QuestionDalBase
    {
        /// <summary>
        /// Thêm mới câu hỏi cho buổi giao lưu
        /// </summary>
        /// <param name="questionInfo"></param>
        /// <param name="id"></param>
        /// <returns></returns>
        public bool InsertQuestion(QuestionEntity questionInfo, ref int id)
        {

            const string storeProcedure = "CMS_InterviewV2Question_Insert";

            try
            {

                var cmd = _db.CreateCommand(storeProcedure, true);
                _db.AddParameter(cmd, "Id", id, System.Data.ParameterDirection.Output);
                _db.AddParameter(cmd, "InterviewId", questionInfo.InterviewId);
                _db.AddParameter(cmd, "FullName", questionInfo.FullName);
                _db.AddParameter(cmd, "Address", !string.IsNullOrEmpty(questionInfo.Address) ? questionInfo.Address : null);
                _db.AddParameter(cmd, "Phone", questionInfo.Phone);
                _db.AddParameter(cmd, "Age", questionInfo.Age);
                _db.AddParameter(cmd, "Email", questionInfo.Email);
                _db.AddParameter(cmd, "QuestionContent", !string.IsNullOrEmpty(questionInfo.QuestionContent) ? questionInfo.QuestionContent : null);
                _db.AddParameter(cmd, "AnswerContent", !string.IsNullOrEmpty(questionInfo.AnswerContent) ? questionInfo.AnswerContent : null);
                _db.AddParameter(cmd, "Status", questionInfo.Status);

                int numberOfRow = cmd.ExecuteNonQuery();
                id = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 0));

                return numberOfRow > 0 && id > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", storeProcedure, ex.Message));
            }
        }
        public bool UpdateQuestion(QuestionEntity questionInfo)
        {
            const string storeProcedure = "CMS_InterviewV2Question_Update";

            try
            {

                var cmd = _db.CreateCommand(storeProcedure, true);
                _db.AddParameter(cmd, "InterviewId", questionInfo.InterviewId);
                _db.AddParameter(cmd, "RespondentId", questionInfo.RespondentId);
                _db.AddParameter(cmd, "FullName", questionInfo.FullName);
                _db.AddParameter(cmd, "Address", !string.IsNullOrEmpty(questionInfo.Address) ? questionInfo.Address : null);
                _db.AddParameter(cmd, "Phone", questionInfo.Phone);
                _db.AddParameter(cmd, "Age", questionInfo.Age);
                _db.AddParameter(cmd, "Email", questionInfo.Email);
                _db.AddParameter(cmd, "QuestionContent", !string.IsNullOrEmpty(questionInfo.QuestionContent) ? questionInfo.QuestionContent : null);
                _db.AddParameter(cmd, "AnswerContent", !string.IsNullOrEmpty(questionInfo.AnswerContent) ? questionInfo.AnswerContent : null);
                _db.AddParameter(cmd, "Status", questionInfo.Status);
                _db.AddParameter(cmd, "Id", questionInfo.ID);

                int numberOfRow = cmd.ExecuteNonQuery();

                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", storeProcedure, ex.Message));
            }
        }
        /// <summary>
        /// Trả lời câu hỏi buổi giao lưu
        /// </summary>
        /// <param name="questionInfo"></param>
        /// <returns></returns>
        public bool Answer(string AnswerContent, int status, int Id)
        {
            const string storeProcedure = "CMS_InterviewV2Question_Answer";

            try
            {

                var cmd = _db.CreateCommand(storeProcedure, true);
                _db.AddParameter(cmd, "AnswerContent", AnswerContent);
                _db.AddParameter(cmd, "Status", status);
                _db.AddParameter(cmd, "Id", Id);

                int numberOfRow = cmd.ExecuteNonQuery();

                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", storeProcedure, ex.Message));
            }
        }
        public bool Delete(int Id)
        {
            const string storeProcedure = "CMS_InterviewV2Question_Delete";

            try
            {

                var cmd = _db.CreateCommand(storeProcedure, true);
                _db.AddParameter(cmd, "Id", Id);


                int numberOfRow = cmd.ExecuteNonQuery();

                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", storeProcedure, ex.Message));
            }
        }
        /// <summary>
        /// Cập nhật trạng thái câu hỏi và câu trả lời
        /// Đã chuyển câu hỏi cho người trả lời, câu hỏi đã được trả lời hay chưa
        /// </summary>
        /// <param name="questionId"></param>
        /// <param name="status"></param>
        /// <returns></returns>
        public bool UpdateStatusQuestion(int questionId, int status)
        {
            const string storeProcedure = "CMS_InterviewV2Question_UpdateStatus";

            try
            {

                var cmd = _db.CreateCommand(storeProcedure, true);
                _db.AddParameter(cmd, "Status", status);
                _db.AddParameter(cmd, "Id", questionId);

                int numberOfRow = cmd.ExecuteNonQuery();

                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", storeProcedure, ex.Message));
            }
        }/// <summary>
        /// Danh sách câu hỏi trong buổi giao lưu
        /// </summary>
        /// <param name="interviewId"></param>
        /// <returns></returns>
        public List<PublishedQuestionEntity> GetAllPublishedQuestion(int interviewId)
        {
            const string storeProcedure = "CMS_InterviewV2Question_GetAllPublishedQuestion";
            try
            {
                List<PublishedQuestionEntity> data = null;
                var cmd = _db.CreateCommand(storeProcedure, true);
                _db.AddParameter(cmd, "InterviewId", interviewId);
                data = _db.GetList<PublishedQuestionEntity>(cmd);
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format(storeProcedure + ":{0}", ex.Message));
            }
        }
        /// <summary>
        /// Danh sách câu hỏi trong buổi giao lưu
        /// </summary>
        /// <param name="interviewId"></param>
        /// <param name="respondentId"></param>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <param name="totalRow"></param>
        /// <returns></returns>
        public List<QuestionEntity> ListQuestionByInterView(int interviewId, int respondentId, int status, int pageIndex, int pageSize, ref int totalRow)
        {
            const string storeProcedure = "CMS_InterviewV2Question_ListdataByRespondent";
            try
            {
                List<QuestionEntity> data = null;
                var cmd = _db.CreateCommand(storeProcedure, true);
                _db.AddParameter(cmd, "TotalRow", totalRow, System.Data.ParameterDirection.Output);
                _db.AddParameter(cmd, "InterviewId", interviewId);
                _db.AddParameter(cmd, "RespondentId", respondentId);
                _db.AddParameter(cmd, "Status", status);
                _db.AddParameter(cmd, "PageIndex", pageIndex);
                _db.AddParameter(cmd, "PageSize", pageSize);
                data = _db.GetList<QuestionEntity>(cmd);
                totalRow = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 0));
                return data;

            }
            catch (Exception ex)
            {
                throw new Exception(string.Format(storeProcedure + ":{0}", ex.Message));
            }
        }
        public List<QuestionEntity> ListQuestionByAnswerded(int interviewId, int status, int orderBy, int pageIndex, int pageSize, ref int totalRow)
        {
            const string storeProcedure = "CMS_InterviewV2Question_ListdataByAnswer";
            try
            {
                List<QuestionEntity> data = null;
                var cmd = _db.CreateCommand(storeProcedure, true);
                _db.AddParameter(cmd, "TotalRow", totalRow, System.Data.ParameterDirection.Output);
                _db.AddParameter(cmd, "InterviewId", interviewId);
                _db.AddParameter(cmd, "Status", status);
                _db.AddParameter(cmd, "orderBy", orderBy);
                _db.AddParameter(cmd, "PageIndex", pageIndex);
                _db.AddParameter(cmd, "PageSize", pageSize);

                data = _db.GetList<QuestionEntity>(cmd);
                totalRow = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 0));
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format(storeProcedure + ":{0}", ex.Message));
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="questionId"></param>
        /// <returns></returns>
        public QuestionEntity GetQuestionById(int questionId)
        {
            const string storeProcedure = "CMS_InterviewV2Question_SelectById";

            try
            {
                QuestionEntity data = null;
                var cmd = _db.CreateCommand(storeProcedure, true);
                _db.AddParameter(cmd, "Id", questionId);
                data = _db.Get<QuestionEntity>(cmd);
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format(storeProcedure + ":{0}", ex.Message));
            }
        }

        public bool UpdatePriority(int interviewId, string listOfInterviewQuestionId)
        {
            const string storeProcedure = "CMS_InterviewV2Question_UpdatePriority";
            try
            {
                var cmd = _db.CreateCommand(storeProcedure, true);
                _db.AddParameter(cmd, "InterviewId", interviewId);
                _db.AddParameter(cmd, "ListOfInterviewQuestionId", listOfInterviewQuestionId);

                var numberOfRow = cmd.ExecuteNonQuery();
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("CMS_InterviewV2Question_UpdatePriority:{0}", ex.Message));
            }
        }

        public bool InsertAnswer(string answerContent, int interviewId, int status, ref int id)
        {
            const string storeProcedure = "CMS_InterviewV2Answer_Insert";

            try
            {
                var cmd = _db.CreateCommand(storeProcedure, true);
                _db.AddParameter(cmd, "InterviewId", interviewId);
                _db.AddParameter(cmd, "AnswerContent", answerContent);
                _db.AddParameter(cmd, "Status", status);
                _db.AddParameter(cmd, "Id", id, System.Data.ParameterDirection.Output);
                int numberOfRow = cmd.ExecuteNonQuery();

                id = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 0));

                return numberOfRow > 0 && id > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", storeProcedure, ex.Message));
            }
        }

        /// <summary>
        /// Chuyển đổi DAL cho QuestionDalBase
        /// minhduongvan
        /// 2014/01/12
        /// </summary>

        #region Constructor

        private readonly CmsMainDb _db;

        protected QuestionDalBase(CmsMainDb db)
        {
            _db = db;
        }

        protected CmsMainDb Database
        {
            get { return _db; }
        }

        #endregion
    }
}
