﻿using ChannelVN.CMS.Common;
using ChannelVN.InterviewV2.Entity;
using ChannelVN.InterviewV2.MainDal.Databases;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ChannelVN.InterviewV2.MainDal
{
    public abstract class InterviewDalBase
    {
        /// <summary>
        /// Thêm mới bài giao lưu cho bài viết
        /// </summary>
        /// <param name="interviewInfo"></param>
        /// <param name="id"></param>
        /// <returns></returns>
        public bool InsertInterview(InterviewV2Entity interviewInfo, ref int id)
        {
            const string storeProcedure = "CMS_InterviewV2_Insert";

            try
            {

                var cmd = _db.CreateCommand(storeProcedure, true);
                _db.AddParameter(cmd, "Id", id, System.Data.ParameterDirection.Output);
                _db.AddParameter(cmd, "NewsId", interviewInfo.NewsId);
                _db.AddParameter(cmd, "CreatedBy", interviewInfo.CreatedBy);

                int numberOfRow = cmd.ExecuteNonQuery();

                id = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 0));

                return numberOfRow > 0 && id > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", storeProcedure, ex.Message));
            }
        }
        /// <summary>
        /// Cập nhật thông tin bài giao lưu
        /// </summary>
        /// <param name="interviewInfo"></param>
        /// <returns></returns>
        public bool UpdateInterview(InterviewV2Entity interviewInfo)
        {
            const string storeProcedure = "CMS_InterviewV2_Update";

            try
            {

                var cmd = _db.CreateCommand(storeProcedure, true);
                _db.AddParameter(cmd, "NewsId", interviewInfo.NewsId);
                _db.AddParameter(cmd, "Status", interviewInfo.Status);
                _db.AddParameter(cmd, "Id", interviewInfo.ID);
                int numberOfRow = cmd.ExecuteNonQuery();

                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", storeProcedure, ex.Message));
            }
        }

        public bool UpdateStatusInterview(int id, int status)
        {
            const string storeProcedure = "CMS_InterviewV2_UpdateStatus";

            try
            {

                var cmd = _db.CreateCommand(storeProcedure, true);
                _db.AddParameter(cmd, "Status", status);
                _db.AddParameter(cmd, "Id", id);
                int numberOfRow = cmd.ExecuteNonQuery();

                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", storeProcedure, ex.Message));
            }
        }

        public InterviewV2Entity SelectInterviewByNewsId(long newsId)
        {
            const string storeProcedure = "CMS_InterviewV2_SelectByNewsId";
            try
            {
                InterviewV2Entity data = null;
                var cmd = _db.CreateCommand(storeProcedure, true);
                _db.AddParameter(cmd, "newsId", newsId);
                data = _db.Get<InterviewV2Entity>(cmd);
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", storeProcedure, ex.Message));
            }
        }

        public InterviewV2Entity SelectInterviewById(int interviewId)
        {
            const string storeProcedure = "CMS_InterviewV2_SelectById";
            try
            {
                InterviewV2Entity data = null;
                var cmd = _db.CreateCommand(storeProcedure, true);
                _db.AddParameter(cmd, "Id", interviewId);
                data = _db.Get<InterviewV2Entity>(cmd);
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", storeProcedure, ex.Message));
            }
        }

        public InterviewV2Entity SelectInterviewByQuestionId(int questionId)
        {
            const string storeProcedure = "CMS_InterviewV2_GetByQuestionId";
            try
            {
                InterviewV2Entity data = null;
                var cmd = _db.CreateCommand(storeProcedure, true);
                _db.AddParameter(cmd, "QuestionId", questionId);

                data = _db.Get<InterviewV2Entity>(cmd);
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", storeProcedure, ex.Message));
            }
        }

        /// <summary>
        /// Chuyển đổi DAL cho InterviewDalBase
        /// minhduongvan
        /// 2014/01/12
        /// </summary>

        #region Constructor

        private readonly CmsMainDb _db;

        protected InterviewDalBase(CmsMainDb db)
        {
            _db = db;
        }

        protected CmsMainDb Database
        {
            get { return _db; }
        }

        #endregion
    }
}
