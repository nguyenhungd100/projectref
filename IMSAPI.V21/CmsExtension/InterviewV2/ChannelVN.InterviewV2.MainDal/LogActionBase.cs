﻿using ChannelVN.CMS.Common;
using ChannelVN.InterviewV2.Entity;
using ChannelVN.InterviewV2.MainDal.Databases;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ChannelVN.InterviewV2.MainDal
{
    public abstract class LogActionBase
    {
        public bool InsertLog(int interviewId, string userName, string action, ref int id)
        {
            const string storeProcedure = "CMS_InterviewV2LogAction_Insert";

            try
            {
                var cmd = _db.CreateCommand(storeProcedure, true);
                _db.AddParameter(cmd, "Id", id, System.Data.ParameterDirection.Output);
                _db.AddParameter(cmd, "interviewId", interviewId);
                _db.AddParameter(cmd, "UserName", userName);
                _db.AddParameter(cmd, "Action", action);
                int numberOfRow = cmd.ExecuteNonQuery();

                if (numberOfRow > 0)
                    id = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 0));

                return numberOfRow > 0 && id > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", storeProcedure, ex.Message));
            }
        }

        public List<LogActionEntity> ListLog(int interviewId, int pageIndex, int pageSize, ref int totalRow)
        {
            const string storeProcedure = "CMS_InterviewV2LogAction_ListData";

            try
            {
                List<LogActionEntity> data = null;
                var cmd = _db.CreateCommand(storeProcedure, true);
                _db.AddParameter(cmd, "TotalRow", totalRow, System.Data.ParameterDirection.Output);
                _db.AddParameter(cmd, "interviewId", interviewId);
                _db.AddParameter(cmd, "PageIndex", pageIndex);
                _db.AddParameter(cmd, "PageSize", pageSize);

                data = _db.GetList<LogActionEntity>(cmd);
                totalRow = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 0));
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", storeProcedure, ex.Message));
            }
        }

        /// <summary>
        /// Chuyển đổi DAL cho LogActionBase
        /// minhduongvan
        /// 2014/01/12
        /// </summary>

        #region Constructor

        private readonly CmsMainDb _db;

        protected LogActionBase(CmsMainDb db)
        {
            _db = db;
        }

        protected CmsMainDb Database
        {
            get { return _db; }
        }

        #endregion
    }
}
