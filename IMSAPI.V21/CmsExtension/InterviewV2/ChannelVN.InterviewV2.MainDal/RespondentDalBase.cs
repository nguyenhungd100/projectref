﻿using ChannelVN.CMS.Common;
using ChannelVN.InterviewV2.Entity;
using ChannelVN.InterviewV2.MainDal.Databases;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ChannelVN.InterviewV2.MainDal
{
    public abstract class RespondentDalBase
    {
        /// <summary>
        /// Thêm mới người trả lời cho buổi giao lưu
        /// </summary>
        /// <param name="respondentInfo"></param>
        /// <param name="id"></param>
        /// <returns></returns>
        public bool Insert(RespondentEntity respondentInfo, ref int id)
        {
            const string storeProcedure = "CMS_InterviewV2Respondent_Insert";
            try
            {
                var cmd = _db.CreateCommand(storeProcedure, true);
                _db.AddParameter(cmd, "Id", id, System.Data.ParameterDirection.Output);
                _db.AddParameter(cmd, "InterviewId", respondentInfo.InterviewId);
                _db.AddParameter(cmd, "RespondentName", respondentInfo.RespondentName);
                _db.AddParameter(cmd, "RespondentPosition", respondentInfo.RespondentPosition);
                _db.AddParameter(cmd, "RespondentInfo", respondentInfo.RespondentInfo);
                _db.AddParameter(cmd, "RespondentAvatar", respondentInfo.RespondentAvatar);
                _db.AddParameter(cmd, "CreatedBy", respondentInfo.CreatedBy);
                _db.AddParameter(cmd, "Status", respondentInfo.Status);

                int numberOfRow = cmd.ExecuteNonQuery();
                id = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 0));

                return numberOfRow > 0 && id > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", storeProcedure, ex.Message));
            }
        }
        /// <summary>
        /// Sửa thông tin người trả lời
        /// </summary>
        /// <param name="respondentInfo"></param>
        /// <returns></returns>
        public bool Update(RespondentEntity respondentInfo)
        {
            const string storeProcedure = "CMS_InterviewV2Respondent_Update";

            try
            {
                var cmd = _db.CreateCommand(storeProcedure, true);
                _db.AddParameter(cmd, "RespondentName", respondentInfo.RespondentName);
                _db.AddParameter(cmd, "RespondentPosition", respondentInfo.RespondentPosition);
                _db.AddParameter(cmd, "RespondentInfo", respondentInfo.RespondentInfo);
                _db.AddParameter(cmd, "RespondentAvatar", respondentInfo.RespondentAvatar);
                _db.AddParameter(cmd, "Status", respondentInfo.Status);
                _db.AddParameter(cmd, "Id", respondentInfo.ID);

                int numberOfRow = cmd.ExecuteNonQuery();

                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", storeProcedure, ex.Message));
            }
        }
        /// <summary>
        /// Xóa người trả lời
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        public bool Delete(int Id)
        {
            const string storeProcedure = "CMS_InterviewV2Respondent_Delete";

            try
            {
                var cmd = _db.CreateCommand(storeProcedure, true);
                _db.AddParameter(cmd, "Id", Id);

                int numberOfRow = cmd.ExecuteNonQuery();

                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", storeProcedure, ex.Message));
            }
        }
        /// <summary>
        /// danh sách phân trang người trả lời buổi giao lưu 
        /// </summary>
        /// <param name="interviewId">mã giao lưu</param>
        /// <param name="pageIndex">trang hiện tại</param>
        /// <param name="pageSize">số bản ghi hiển thị 1 trang</param>
        /// <param name="totalRow">tổng số bản ghi</param>
        /// <returns></returns>
        public List<RespondentEntity> ListRespondentInterView(int interviewId, int pageIndex, int pageSize, ref int totalRow)
        {
            const string storeProcedure = "CMS_InterviewV2Respondent_Listdata";
            try
            {
                List<RespondentEntity> data = null;
                var cmd = _db.CreateCommand(storeProcedure, true);
                _db.AddParameter(cmd, "TotalRow", totalRow, System.Data.ParameterDirection.Output);
                _db.AddParameter(cmd, "InterviewId", interviewId);
                _db.AddParameter(cmd, "PageIndex", pageIndex);
                _db.AddParameter(cmd, "PageSize", pageSize);
                data = _db.GetList<RespondentEntity>(cmd);
                totalRow = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 0));
                return data;

            }
            catch (Exception ex)
            {
                throw new Exception(string.Format(storeProcedure + ":{0}", ex.Message));
            }
        }
        /// <summary>
        /// danh sách người trả lời buổi giao lưu theo mã bài giao lưu. Không phân trang
        /// </summary>
        /// <param name="interviewId"></param>
        /// <returns></returns>
        public List<RespondentEntity> ListRespondentInterView(int interviewId)
        {
            const string storeProcedure = "CMS_InterviewV2Respondent_ListdataByInterviewId";
            try
            {
                List<RespondentEntity> data = null;
                var cmd = _db.CreateCommand(storeProcedure, true);
                _db.AddParameter(cmd, "InterviewId", interviewId);
                data = _db.GetList<RespondentEntity>(cmd);
                return data;

            }
            catch (Exception ex)
            {
                throw new Exception(string.Format(storeProcedure + ":{0}", ex.Message));
            }
        }
        /// <summary>
        /// thông tin chi tiết người trả lời
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public RespondentEntity RespondentById(int id)
        {
            const string storeProcedure = "CMS_InterviewV2Respondent_RespondentById";
            try
            {
                RespondentEntity data = null;
                var cmd = _db.CreateCommand(storeProcedure, true);
                _db.AddParameter(cmd, "Id", id);
                data = _db.Get<RespondentEntity>(cmd);
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format(storeProcedure + ":{0}", ex.Message));
            }
        }

        /// <summary>
        /// Chuyển đổi DAL cho RespondentDalBase
        /// minhduongvan
        /// 2014/01/12
        /// </summary>

        #region Constructor

        private readonly CmsMainDb _db;

        protected RespondentDalBase(CmsMainDb db)
        {
            _db = db;
        }

        protected CmsMainDb Database
        {
            get { return _db; }
        }

        #endregion
    }
}
