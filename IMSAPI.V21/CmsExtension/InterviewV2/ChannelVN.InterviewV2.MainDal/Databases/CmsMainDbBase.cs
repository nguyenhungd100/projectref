﻿using System.Data;
using ChannelVN.CMS.Common.DalCommon;
using ChannelVN.InterviewV2.MainDal.Databases;
using ChannelVN.InterviewV2.MainDal;

namespace ChannelVN.InterviewV2.MainDal.Databases
{
    /// <summary>
    /// The base class for the <see cref="CmsMainDb"/> class that 
    /// represents a connection to the <c>CmsMainDb</c> database. 
    /// </summary>
    /// <remarks>
    /// Do not change this source code. Modify the CmsMainDb class
    /// if you need to add or change some functionality.
    /// </remarks>
    public abstract class CmsMainDbBase : MainDbBase
    {
        #region Store procedures

        #region Interview Dal
        private InterviewDal _interviewDal;
        public InterviewDalBase InterviewDal
        {
            get { return _interviewDal ?? (_interviewDal = new InterviewDal((CmsMainDb)this)); }
        }
        #endregion
        #region Log action
        private LogAction _logactionContentMainDal;
        public LogActionBase LogActionContentMainDal
        {
            get { return _logactionContentMainDal ?? (_logactionContentMainDal = new LogAction((CmsMainDb)this)); }
        }
        #endregion
        #region Question
        private QuestionDal _questionDal;
        public QuestionDalBase QuestionDal
        {
            get { return _questionDal ?? (_questionDal = new QuestionDal((CmsMainDb)this)); }
        }
        #endregion
        #region Respondent
        private RespondentDal _respondentDal;
        public RespondentDalBase RespondentDal
        {
            get { return _respondentDal ?? (_respondentDal = new RespondentDal((CmsMainDb)this)); }
        }
        #endregion


        #endregion

        #region Constructors

        protected CmsMainDbBase()
        {
        }
        protected CmsMainDbBase(bool init)
        {
            if (init)
            {
                InitConnection();
            }
        }

        #endregion

        #region Protected members

        protected override sealed void InitConnection()
        {
            _connection = CreateConnection();
            _connection.Open();
        }

        /// <summary>
        /// Creates a new connection to the database.
        /// </summary>
        /// <returns>A reference to the <see cref="System.Data.IDbConnection"/> object.</returns>
        protected abstract IDbConnection CreateConnection();

        #endregion
    }
}