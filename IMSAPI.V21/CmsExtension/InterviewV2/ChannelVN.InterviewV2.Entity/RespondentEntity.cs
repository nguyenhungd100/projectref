﻿using ChannelVN.CMS.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace ChannelVN.InterviewV2.Entity
{
    [DataContract]
    public class RespondentEntity:EntityBase
    {
        [DataMember]
        public int ID { get; set; }
        [DataMember]
        public int InterviewId { get; set; }
        [DataMember]
        public string RespondentName { get; set; }
        [DataMember]
        public string RespondentPosition { get; set; }
        [DataMember]
        public string RespondentInfo { get; set; }
        [DataMember]
        public string RespondentAvatar { get; set; }
        [DataMember]
        public string CreatedBy { get; set; }
        [DataMember]
        public DateTime CreatedDate { get; set; }
        [DataMember]
        public string TotalQuestion { get; set; }
        [DataMember]
        public string TotalAnswer { get; set; }
        [DataMember]
        public string TotalQuestionInterView { get; set; }
        [DataMember]
        public string TotalAnswerInterView { get; set; }
        [DataMember]
        public string TotalPublishInterView { get; set; }
        [DataMember]
        public long NewsId { get; set; }
        [DataMember]
        public int Status { get; set; }
    }
}
