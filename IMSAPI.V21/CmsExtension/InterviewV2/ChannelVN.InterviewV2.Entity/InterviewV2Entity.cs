﻿using ChannelVN.CMS.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace ChannelVN.InterviewV2.Entity
{
    [DataContract]
    public enum EnumStatusInterview
    {
        [EnumMember]
        normal = 1,
        [EnumMember]
        processing = 2,
        [EnumMember]
        finished = 3
    }
    [DataContract]
    public class InterviewV2Entity: EntityBase
    {
        [DataMember]
        public int ID { get; set; }
        [DataMember]
        public long NewsId { get; set; }
        [DataMember]
        public int Status { get; set; }
        [DataMember]
        public string CreatedBy { get; set; }
        [DataMember]
        public DateTime CreatedDate { get; set; }
        [DataMember]
        public string TotalQuestionInterView { get; set; }
        [DataMember]
        public string TotalAnswerInterView { get; set; }
        [DataMember]
        public string TotalPublishInterView { get; set; }
    }
}
