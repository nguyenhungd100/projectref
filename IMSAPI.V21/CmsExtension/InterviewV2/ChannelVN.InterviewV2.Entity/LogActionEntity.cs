﻿using ChannelVN.CMS.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace ChannelVN.InterviewV2.Entity
{
    [DataContract]
    public class LogActionEntity:EntityBase
    {
        [DataMember]
        public long ID { get; set; }
        [DataMember]
        public int InterviewId { get; set; }
        [DataMember]
        public string UserName { get; set; }
        [DataMember]
        public string Action { get; set; }
        [DataMember]
        public DateTime ActionDate { get; set; }
    }
}
