﻿using ChannelVN.CMS.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;


namespace ChannelVN.InterviewV2.Entity
{
    [DataContract]
    public enum EnumStatusQuestion
    {
        [EnumMember]
        All = 0,
        [EnumMember]
        normal = 1,
        [EnumMember]
        wait = 2,
        [EnumMember]
        answered = 3,
        [EnumMember]
        published = 4,
        [EnumMember]
        removed = 5
    }
    [DataContract]
    public class QuestionEntity:EntityBase
    {
        [DataMember]
        public int ID { get; set; }
        [DataMember]
        public int InterviewId { get; set; }
        [DataMember]
        public int RespondentId { get; set; }
        [DataMember]
        public string FullName { get; set; }
        [DataMember]
        public string Address { get; set; }
        [DataMember]
        public string Phone { get; set; }
        [DataMember]
        public int Age { get; set; }
        [DataMember]
        public string Email { get; set; }
        [DataMember]
        public string QuestionContent { get; set; }
        [DataMember]
        public string AnswerContent { get; set; }
        [DataMember]
        public int Status { get; set; }
        [DataMember]
        public DateTime CreatedDate { get; set; }
        [DataMember]
        public string RespondentName { get; set; }
        [DataMember]
        public DateTime AnswerDate { get; set; }
    }
    [DataContract]
    public class PublishedQuestionEntity : EntityBase
    {
        [DataMember]
        public int ID { get; set; }
        [DataMember]
        public int InterviewId { get; set; }
        [DataMember]
        public int RespondentId { get; set; }
        [DataMember]
        public string FullName { get; set; }
        [DataMember]
        public string Address { get; set; }
        [DataMember]
        public string Phone { get; set; }
        [DataMember]
        public int Age { get; set; }
        [DataMember]
        public string Email { get; set; }
        [DataMember]
        public string QuestionContent { get; set; }
        [DataMember]
        public string AnswerContent { get; set; }
        [DataMember]
        public DateTime CreatedDate { get; set; }
        [DataMember]
        public DateTime AnswerDate { get; set; }
        [DataMember]
        public int Priority { get; set; }
        [DataMember]
        public string RespondentName { get; set; }
        [DataMember]
        public string RespondentPosition { get; set; }
        [DataMember]
        public string RespondentAvatar { get; set; }
    }
}
