﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ChannelVN.InterviewV2.Entity;
using System.Data.SqlClient;
using ChannelVN.CMS.Common;
using System.Data;
using ChannelVN.InterviewV2.Dal.Common;
using ChannelVN.InterviewV2.MainDal.Databases;

namespace ChannelVN.InterviewV2.Dal
{
    public class LogAction
    {
        public static bool InsertLog(int interviewId, string userName, string action, ref int id)
        {
            bool retVal = false;
            using (var db = new CmsMainDb())
            {
                retVal = db.LogActionContentMainDal.InsertLog(interviewId, userName, action, ref id);
            }
            return retVal;
        }

        public static List<LogActionEntity> ListLog(int interviewId, int pageIndex, int pageSize, ref int totalRow)
        {
            List<LogActionEntity> retVal = null;
            using (var db = new CmsMainDb())
            {
                retVal = db.LogActionContentMainDal.ListLog(interviewId, pageIndex, pageSize, ref totalRow);
            }
            return retVal;
        }
    }
}
