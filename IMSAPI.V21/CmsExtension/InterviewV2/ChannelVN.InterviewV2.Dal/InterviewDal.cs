﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ChannelVN.InterviewV2.Entity;
using System.Data.SqlClient;
using ChannelVN.CMS.Common;
using System.Data;
using ChannelVN.InterviewV2.Dal.Common;
using ChannelVN.InterviewV2.MainDal.Databases;

namespace ChannelVN.InterviewV2.Dal
{
    public class InterviewDal
    {
        /// <summary>
        /// Thêm mới bài giao lưu cho bài viết
        /// </summary>
        /// <param name="interviewInfo"></param>
        /// <param name="id"></param>
        /// <returns></returns>
        public static bool InsertInterview(InterviewV2Entity interviewInfo, ref int id)
        {
            bool retVal = false;
            using (var db = new CmsMainDb())
            {
                retVal = db.InterviewDal.InsertInterview(interviewInfo, ref id);
            }
            return retVal;
        }
        /// <summary>
        /// Cập nhật thông tin bài giao lưu
        /// </summary>
        /// <param name="interviewInfo"></param>
        /// <returns></returns>
        public static bool UpdateInterview(InterviewV2Entity interviewInfo)
        {
            bool retVal = false;
            using (var db = new CmsMainDb())
            {
                retVal = db.InterviewDal.UpdateInterview(interviewInfo);
            }
            return retVal;
        }

        public static bool UpdateStatusInterview(int id, int status)
        {
            bool retVal = false;
            using (var db = new CmsMainDb())
            {
                retVal = db.InterviewDal.UpdateStatusInterview(id, status);
            }
            return retVal;
        }

        public static InterviewV2Entity SelectInterviewByNewsId(long newsId)
        {
            InterviewV2Entity retVal = null;
            using (var db = new CmsMainDb())
            {
                retVal = db.InterviewDal.SelectInterviewByNewsId(newsId);
            }
            return retVal;
        }

        public static InterviewV2Entity SelectInterviewById(int interviewId)
        {
            InterviewV2Entity retVal = null;
            using (var db = new CmsMainDb())
            {
                retVal = db.InterviewDal.SelectInterviewById(interviewId);
            }
            return retVal;
        }

        public static InterviewV2Entity SelectInterviewByQuestionId(int questionId)
        {
            InterviewV2Entity retVal = null;
            using (var db = new CmsMainDb())
            {
                retVal = db.InterviewDal.SelectInterviewByQuestionId(questionId);
            }
            return retVal;
        }
    }
}
