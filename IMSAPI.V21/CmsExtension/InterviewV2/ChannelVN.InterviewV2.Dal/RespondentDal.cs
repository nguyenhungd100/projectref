﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ChannelVN.InterviewV2.Entity;
using System.Data.SqlClient;
using ChannelVN.CMS.Common;
using System.Data;
using ChannelVN.InterviewV2.Dal.Common;
using ChannelVN.InterviewV2.MainDal.Databases;

namespace ChannelVN.InterviewV2.Dal
{
    public class RespondentDal
    {
        /// <summary>
        /// Thêm mới người trả lời cho buổi giao lưu
        /// </summary>
        /// <param name="respondentInfo"></param>
        /// <param name="id"></param>
        /// <returns></returns>
        public static bool Insert(RespondentEntity respondentInfo, ref int id)
        {
            bool retVal = false;
            using (var db = new CmsMainDb())
            {
                retVal = db.RespondentDal.Insert(respondentInfo, ref id);
            }
            return retVal;
        }
        /// <summary>
        /// Sửa thông tin người trả lời
        /// </summary>
        /// <param name="respondentInfo"></param>
        /// <returns></returns>
        public static bool Update(RespondentEntity respondentInfo)
        {
            bool retVal = false;
            using (var db = new CmsMainDb())
            {
                retVal = db.RespondentDal.Update(respondentInfo);
            }
            return retVal;
        }
        /// <summary>
        /// Xóa người trả lời
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        public static bool Delete(int Id)
        {
            bool retVal = false;
            using (var db = new CmsMainDb())
            {
                retVal = db.RespondentDal.Delete(Id);
            }
            return retVal;
        }
        /// <summary>
        /// danh sách phân trang người trả lời buổi giao lưu 
        /// </summary>
        /// <param name="interviewId">mã giao lưu</param>
        /// <param name="pageIndex">trang hiện tại</param>
        /// <param name="pageSize">số bản ghi hiển thị 1 trang</param>
        /// <param name="totalRow">tổng số bản ghi</param>
        /// <returns></returns>
        public static List<RespondentEntity> ListRespondentInterView(int interviewId, int pageIndex, int pageSize, ref int totalRow)
        {
            List<RespondentEntity> retVal = null;
            using (var db = new CmsMainDb())
            {
                retVal = db.RespondentDal.ListRespondentInterView(interviewId, pageIndex, pageSize, ref totalRow);
            }
            return retVal;
        }
        /// <summary>
        /// danh sách người trả lời buổi giao lưu theo mã bài giao lưu. Không phân trang
        /// </summary>
        /// <param name="interviewId"></param>
        /// <returns></returns>
        public static List<RespondentEntity> ListRespondentInterView(int interviewId)
        {
            List<RespondentEntity> retVal = null;
            using (var db = new CmsMainDb())
            {
                retVal = db.RespondentDal.ListRespondentInterView(interviewId);
            }
            return retVal;
        }
        /// <summary>
        /// thông tin chi tiết người trả lời
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public static RespondentEntity RespondentById(int id)
        {
            RespondentEntity retVal = null;
            using (var db = new CmsMainDb())
            {
                retVal = db.RespondentDal.RespondentById(id);
            }
            return retVal;
        }
    }
}
