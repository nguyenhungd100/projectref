﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ChannelVN.InterviewV2.Entity;
using System.Data.SqlClient;
using ChannelVN.CMS.Common;
using System.Data;
using ChannelVN.InterviewV2.Dal.Common;
using ChannelVN.InterviewV2.MainDal.Databases;
namespace ChannelVN.InterviewV2.Dal
{
    public class QuestionDal
    {
        /// <summary>
        /// Thêm mới câu hỏi cho buổi giao lưu
        /// </summary>
        /// <param name="questionInfo"></param>
        /// <param name="id"></param>
        /// <returns></returns>
        public static bool InsertQuestion(QuestionEntity questionInfo, ref int id)
        {
            bool retVal = false;
            using (var db = new CmsMainDb())
            {
                retVal = db.QuestionDal.InsertQuestion(questionInfo, ref id);
            }
            return retVal;
        }
        public static bool UpdateQuestion(QuestionEntity questionInfo)
        {
            bool retVal = false;
            using (var db = new CmsMainDb())
            {
                retVal = db.QuestionDal.UpdateQuestion(questionInfo);
            }
            return retVal;
        }
        /// <summary>
        /// Trả lời câu hỏi buổi giao lưu
        /// </summary>
        /// <param name="questionInfo"></param>
        /// <returns></returns>
        public static bool Answer(string AnswerContent, int status, int Id)
        {
            bool retVal = false;
            using (var db = new CmsMainDb())
            {
                retVal = db.QuestionDal.Answer(AnswerContent, status, Id);
            }
            return retVal;
        }
        public static bool Delete(int Id)
        {
            bool retVal = false;
            using (var db = new CmsMainDb())
            {
                retVal = db.QuestionDal.Delete(Id);
            }
            return retVal;
        }
        /// <summary>
        /// Cập nhật trạng thái câu hỏi và câu trả lời
        /// Đã chuyển câu hỏi cho người trả lời, câu hỏi đã được trả lời hay chưa
        /// </summary>
        /// <param name="questionId"></param>
        /// <param name="status"></param>
        /// <returns></returns>
        public static bool UpdateStatusQuestion(int questionId, int status)
        {
            bool retVal = false;
            using (var db = new CmsMainDb())
            {
                retVal = db.QuestionDal.UpdateStatusQuestion(questionId, status);
            }
            return retVal;
        }
        /// <summary>
        /// Danh sách câu hỏi trong buổi giao lưu
        /// </summary>
        /// <param name="interviewId"></param>
        /// <returns></returns>
        public static List<PublishedQuestionEntity> GetAllPublishedQuestion(int interviewId)
        {
            List<PublishedQuestionEntity> retVal = null;
            using (var db = new CmsMainDb())
            {
                retVal = db.QuestionDal.GetAllPublishedQuestion(interviewId);
            }
            return retVal;
        }
        /// <summary>
        /// Danh sách câu hỏi trong buổi giao lưu
        /// </summary>
        /// <param name="interviewId"></param>
        /// <param name="respondentId"></param>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <param name="totalRow"></param>
        /// <returns></returns>
        public static List<QuestionEntity> ListQuestionByInterView(int interviewId, int respondentId, int status, int pageIndex, int pageSize, ref int totalRow)
        {
            List<QuestionEntity> retVal = null;
            using (var db = new CmsMainDb())
            {
                retVal = db.QuestionDal.ListQuestionByInterView(interviewId, respondentId, status, pageIndex, pageSize, ref totalRow);
            }
            return retVal;
        }
        public static List<QuestionEntity> ListQuestionByAnswerded(int interviewId, int status, int orderBy, int pageIndex, int pageSize, ref int totalRow)
        {
            List<QuestionEntity> retVal = null;
            using (var db = new CmsMainDb())
            {
                retVal = db.QuestionDal.ListQuestionByAnswerded(interviewId, status, orderBy, pageIndex, pageSize, ref totalRow);
            }
            return retVal;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="questionId"></param>
        /// <returns></returns>
        public static QuestionEntity GetQuestionById(int questionId)
        {
            QuestionEntity retVal = null;
            using (var db = new CmsMainDb())
            {
                retVal = db.QuestionDal.GetQuestionById(questionId);
            }
            return retVal;
        }

        public static bool UpdatePriority(int interviewId, string listOfInterviewQuestionId)
        {
            bool retVal = false;
            using (var db = new CmsMainDb())
            {
                retVal = db.QuestionDal.UpdatePriority(interviewId, listOfInterviewQuestionId);
            }
            return retVal;
        }

        public static bool InsertAnswer(string answerContent, int interviewId, int status, ref int id)
        {
            bool retVal = false;
            using (var db = new CmsMainDb())
            {
                retVal = db.QuestionDal.InsertAnswer(answerContent, interviewId, status, ref id);
            }
            return retVal;
        }
    }
}
