﻿using ChannelVN.CafeBiz.Dal;
using ChannelVN.CafeBiz.Entity;
using ChannelVN.CafeBiz.Entity.ErrorCode;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ChannelVN.CafeBiz.Bo
{
    public class BizDocFieldBo
    {
        public static ErrorMapping.ErrorCodes Insert(BizDocFieldEntity info, ref int id)
        {
            var result = ErrorMapping.ErrorCodes.BusinessError;
            var createSuccess = BizDocFieldDal.Insert(info, ref id);
            if (createSuccess && id > 0)
                result = ErrorMapping.ErrorCodes.Success;
            return result;
        }

        public static ErrorMapping.ErrorCodes Update(BizDocFieldEntity info)
        {
            var result = ErrorMapping.ErrorCodes.BusinessError;
            var createSuccess = BizDocFieldDal.Update(info);
            if (createSuccess && info.Id > 0)
                result = ErrorMapping.ErrorCodes.Success;
            return result;
        }

        public static ErrorMapping.ErrorCodes Delete(int Id)
        {
            var result = ErrorMapping.ErrorCodes.BusinessError;
            var createSuccess = BizDocFieldDal.Delete(Id);
            if (createSuccess)
                result = ErrorMapping.ErrorCodes.Success;
            return result;
        }

        public static List<BizDocFieldEntity> SelectListField(int pageIndex, int pageSize, ref int totalRow)
        {
            return BizDocFieldDal.SelectListField(pageIndex, pageSize, ref totalRow);
        }

        public static List<BizDocFieldEntity> SelectListAllField() {
            return BizDocFieldDal.SelectListAllField();
        }

        public static BizDocFieldEntity SelectDataById(int id)
        {
            return BizDocFieldDal.SelectDataById(id);
        }
    }
}
