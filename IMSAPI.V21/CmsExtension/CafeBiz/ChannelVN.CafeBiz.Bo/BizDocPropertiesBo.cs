﻿using ChannelVN.CafeBiz.Dal;
using ChannelVN.CafeBiz.Entity;
using ChannelVN.CafeBiz.Entity.ErrorCode;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ChannelVN.CafeBiz.Bo
{
    public class BizDocPropertiesBo
    {
        public static List<BizDocPropertiesEntity> SelectListAllProperties() {
            return BizDocPropertiesDal.SelectListProperties();
        }
    }
}
