﻿using ChannelVN.CafeBiz.Dal;
using ChannelVN.CafeBiz.Entity;
using ChannelVN.CafeBiz.Entity.ErrorCode;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ChannelVN.CafeBiz.Bo
{
    public class BizDocBo
    {
        public static ErrorMapping.ErrorCodes Insert(BizDocEntity info, ref int id)
        {
            var result = ErrorMapping.ErrorCodes.BusinessError;
            var createSuccess = BizDocDal.Insert(info, ref id);
            if (createSuccess && id > 0)
                result = ErrorMapping.ErrorCodes.Success;
            return result;
        }

        public static ErrorMapping.ErrorCodes Update(BizDocEntity info)
        {
            var result = ErrorMapping.ErrorCodes.BusinessError;
            var createSuccess = BizDocDal.Update(info);
            if (createSuccess && info.Id > 0)
                result = ErrorMapping.ErrorCodes.Success;
            return result;
        }

        public static ErrorMapping.ErrorCodes Delete(long Id)
        {
            var result = ErrorMapping.ErrorCodes.BusinessError;
            var createSuccess = BizDocDal.Delete(Id);
            if (createSuccess)
                result = ErrorMapping.ErrorCodes.Success;
            return result;
        }

        public static List<BizDocEntity> SelectListField(int pageIndex, int pageSize, ref int totalRow, int fieldId = 0, int propId = 0)
        {
            return BizDocDal.SelectListBizDoc(pageIndex, pageSize, ref totalRow, fieldId, propId);
        }

        public static BizDocEntity SelectDataById(long id)
        {
            return BizDocDal.SelectDataById(id);
        }
    }
}
