﻿using ChannelVN.CMS.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace ChannelVN.CafeBiz.Entity
{
    [DataContract]
    public class BizDocEntity : EntityBase
    {
        [DataMember]
        public long Id { get; set; }
        [DataMember]
        public string Title { get; set; }
        [DataMember]
        public string FieldId { get; set; }
        [DataMember]
        public string PropId { get; set; }
        [DataMember]
        public string AgentIssued { get; set; }
        [DataMember]
        public string DocNumber { get; set; }
        [DataMember]
        public DateTime DateIssued { get; set; }
        [DataMember]
        public DateTime DateEffect { get; set; }
        [DataMember]
        public bool Active { get; set; }
        [DataMember]
        public string Source { get; set; }
        [DataMember]
        public int Downloaded { get; set; }
        [DataMember]
        public string listField { get; set; }
        [DataMember]
        public string listProperties { get; set; }
    }
}
