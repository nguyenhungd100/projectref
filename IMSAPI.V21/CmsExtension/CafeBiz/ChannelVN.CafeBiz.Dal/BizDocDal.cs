﻿using ChannelVN.CafeBiz.Dal.Common;
using ChannelVN.CafeBiz.Entity;
using ChannelVN.CafeBiz.MainDal.Databases;
using ChannelVN.CMS.Common;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace ChannelVN.CafeBiz.Dal
{
    public class BizDocDal
    {
        /// <summary>
        /// Lưu thông tin văn bản pháp luật
        /// </summary>
        /// <param name="info"></param>
        /// <param name="id">output</param>
        /// <returns>true/false</returns>
        public static bool Insert(BizDocEntity info, ref int id)
        {
            bool returnValue = false;
            using (var db = new CmsMainDb())
            {
                returnValue = db.BizDocContentMainDal.Insert(info, ref id);
            }
            return returnValue;
        }

        /// <summary>
        /// Cập nhật lĩnh vực văn bản pháp luật
        /// </summary>
        /// <param name="info"></param>
        /// <returns></returns>
        public static bool Update(BizDocEntity info)
        {
            bool returnValue = false;
            using (var db = new CmsMainDb())
            {
                returnValue = db.BizDocContentMainDal.Update(info);
            }
            return returnValue;
        }

        /// <summary>
        /// Xóa văn bản pháp luật
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        public static bool Delete(long Id)
        {
            bool returnValue = false;
            using (var db = new CmsMainDb())
            {
                returnValue = db.BizDocContentMainDal.Delete(Id);
            }
            return returnValue;
        }

        /// <summary>
        /// Lấy danh sách các văn bản pháp luật
        /// </summary>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <param name="totalRow"></param>
        /// <returns></returns>
        public static List<BizDocEntity> SelectListBizDoc(int pageIndex, int pageSize, ref int totalRow, int fieldId = 0, int propId = 0)
        {
            List<BizDocEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.BizDocContentMainDal.SelectListBizDoc(pageIndex, pageSize, ref totalRow, fieldId, propId);
            }
            return returnValue;
        }
        /// <summary>
        /// Lấy thông tin chi tiết văn bản pháp luật
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public static BizDocEntity SelectDataById(long id)
        {
            BizDocEntity returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.BizDocContentMainDal.SelectDataById(id);
            }
            return returnValue;
        }
    }
}
