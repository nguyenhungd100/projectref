﻿using ChannelVN.CafeBiz.Dal.Common;
using ChannelVN.CafeBiz.Entity;
using ChannelVN.CafeBiz.MainDal.Databases;
using ChannelVN.CMS.Common;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace ChannelVN.CafeBiz.Dal
{
    public class BizDocFieldDal
    {
        /// <summary>
        /// Lưu thông tin lĩnh vực văn bản pháp luật
        /// </summary>
        /// <param name="info"></param>
        /// <param name="id"></param>
        /// <returns></returns>
        public static bool Insert(BizDocFieldEntity info, ref int id)
        {
            bool returnValue = false;
            using (var db = new CmsMainDb())
            {
                returnValue = db.BizDocFieldContentMainDal.Insert(info, ref id);
            }
            return returnValue;
        }

        /// <summary>
        /// Cập nhật lĩnh vực văn bản pháp luật
        /// </summary>
        /// <param name="info"></param>
        /// <returns></returns>
        public static bool Update(BizDocFieldEntity info)
        {
            bool returnValue = false;
            using (var db = new CmsMainDb())
            {
                returnValue = db.BizDocFieldContentMainDal.Update(info);
            }
            return returnValue;
        }

        /// <summary>
        /// Xóa lĩnh vực văn bản pháp luật
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        public static bool Delete(int Id)
        {
            bool returnValue = false;
            using (var db = new CmsMainDb())
            {
                returnValue = db.BizDocFieldContentMainDal.Delete(Id);
            }
            return returnValue;
        }

        /// <summary>
        /// Lấy danh sách các lĩnh vực văn bản pháp luật theo paging
        /// </summary>
        /// <param name="pageIndex">int</param>
        /// <param name="pageSize">int</param>
        /// <param name="totalRow">output</param>
        /// <returns>List</returns>
        public static List<BizDocFieldEntity> SelectListField(int pageIndex, int pageSize, ref int totalRow)
        {
            List<BizDocFieldEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.BizDocFieldContentMainDal.SelectListField(pageIndex, pageSize, ref totalRow);
            }
            return returnValue;
        }

        /// <summary>
        /// Lấy toàn bộ lĩnh vực văn bản pháp luật
        /// </summary>
        /// <returns>List</returns>
        public static List<BizDocFieldEntity> SelectListAllField()
        {
            List<BizDocFieldEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.BizDocFieldContentMainDal.SelectListAllField();
            }
            return returnValue;
        }

        /// <summary>
        /// Lấy thông tin chi tiết lĩnh vực văn bản pháp luật
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public static BizDocFieldEntity SelectDataById(int id) {
            BizDocFieldEntity returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.BizDocFieldContentMainDal.SelectDataById(id);
            }
            return returnValue;
        }
    }
}
