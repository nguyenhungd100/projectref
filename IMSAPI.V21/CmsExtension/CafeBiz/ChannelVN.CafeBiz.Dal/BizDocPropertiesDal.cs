﻿using ChannelVN.CafeBiz.Dal.Common;
using ChannelVN.CafeBiz.Entity;
using ChannelVN.CafeBiz.MainDal.Databases;
using ChannelVN.CMS.Common;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;


namespace ChannelVN.CafeBiz.Dal
{
    public class BizDocPropertiesDal
    {
        public static List<BizDocPropertiesEntity> SelectListProperties()
        {
            List<BizDocPropertiesEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.BizDocPropertiesContentMainDal.SelectListProperties();
            }
            return returnValue;
        }
    }
}
