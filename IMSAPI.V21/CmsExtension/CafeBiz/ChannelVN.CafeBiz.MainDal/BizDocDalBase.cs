﻿using ChannelVN.CafeBiz.Entity;
using ChannelVN.CafeBiz.MainDal.Databases;
using ChannelVN.CMS.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChannelVN.CafeBiz.MainDal
{
    public abstract class BizDocDalBase
    {
        #region Function GET
        public List<BizDocEntity> SelectListBizDoc(int pageIndex, int pageSize, ref int totalRow, int fieldId = 0, int propId = 0)
        {
            const string commandText = "CMS_BizDoc_GetAllData";
            try
            {
                List<BizDocEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "TotalRow", totalRow, System.Data.ParameterDirection.Output);
                _db.AddParameter(cmd, "field", fieldId);
                _db.AddParameter(cmd, "prop", propId);
                _db.AddParameter(cmd, "PageIndex", pageIndex);
                _db.AddParameter(cmd, "PageSize", pageSize);
                data = _db.GetList<BizDocEntity>(cmd);
                totalRow = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 0));
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public BizDocEntity SelectDataById(long id)
        {
            const string commandText = "CMS_BizDoc_GetDataById";
            try
            {
                BizDocEntity data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", id);
                data = _db.Get<BizDocEntity>(cmd);
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        #endregion

        #region Function SET
        public bool Insert(BizDocEntity info, ref int id)
        {
            const string commandText = "CMS_BizDoc_Insert";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", id, System.Data.ParameterDirection.Output);
                _db.AddParameter(cmd, "Title", info.Title);
                _db.AddParameter(cmd, "FieldId", info.FieldId);
                _db.AddParameter(cmd, "PropId", info.PropId);
                _db.AddParameter(cmd, "AgentIssued", info.AgentIssued);
                _db.AddParameter(cmd, "DateIssued", info.DateIssued);
                _db.AddParameter(cmd, "DateEffect", info.DateEffect);
                _db.AddParameter(cmd, "Source", info.Source);
                _db.AddParameter(cmd, "Active", info.Active);
                _db.AddParameter(cmd, "DocNUmber", info.DocNumber);
                var result = cmd.ExecuteNonQuery();
                id = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 0));
                return result > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public bool Update(BizDocEntity info) {
            const string commandText = "CMS_BizDoc_Update";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", info.Id);
                _db.AddParameter(cmd, "Title", info.Title);
                _db.AddParameter(cmd, "FieldId", info.FieldId);
                _db.AddParameter(cmd, "PropId", info.PropId);
                _db.AddParameter(cmd, "AgentIssued", info.AgentIssued);
                _db.AddParameter(cmd, "DateIssued", info.DateIssued);
                _db.AddParameter(cmd, "DateEffect", info.DateEffect);
                _db.AddParameter(cmd, "Source", info.Source);
                _db.AddParameter(cmd, "Active", info.Active);
                _db.AddParameter(cmd, "DocNUmber", info.DocNumber);
                var result = cmd.ExecuteNonQuery();
                
                return result > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public bool Delete(long Id) {
            const string commandText = "CMS_BizDoc_Delete";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", Id);
                var result = cmd.ExecuteNonQuery();
                return result != 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        #endregion

        /// <summary>
        /// Chuyển đổi DAL cho BizDoc
        /// minhduongvan
        /// 2014/01/07
        /// </summary>

        #region Constructor

        private readonly CmsMainDb _db;

        protected BizDocDalBase(CmsMainDb db)
        {
            _db = db;
        }

        protected CmsMainDb Database
        {
            get { return _db; }
        }

        #endregion
    }
}
