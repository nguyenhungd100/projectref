﻿using ChannelVN.CafeBiz.Entity;
using ChannelVN.CafeBiz.MainDal.Databases;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ChannelVN.CafeBiz.MainDal
{
    public class BizDocPropertiesDalBase
    {
        #region function GET
        public List<BizDocPropertiesEntity> SelectListProperties() {
            const string commandText = "CMS_BizDocProperties_GetAllData";
            try
            {
                List<BizDocPropertiesEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                //_db.AddParameter(cmd, "TotalRow", totalRow, System.Data.ParameterDirection.Output);
                //_db.AddParameter(cmd, "PageIndex", pageIndex);
                //_db.AddParameter(cmd, "PageSize", pageSize);
                data = _db.GetList<BizDocPropertiesEntity>(cmd);
                //totalRow = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 0));
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        #endregion

        /// <summary>
        /// Chuyển đổi DAL cho BizDocProperties
        /// minhduongvan
        /// 2014/01/08
        /// </summary>

        #region Constructor

        private readonly CmsMainDb _db;

        protected BizDocPropertiesDalBase(CmsMainDb db)
        {
            _db = db;
        }

        protected CmsMainDb Database
        {
            get { return _db; }
        }

        #endregion
    }
}
