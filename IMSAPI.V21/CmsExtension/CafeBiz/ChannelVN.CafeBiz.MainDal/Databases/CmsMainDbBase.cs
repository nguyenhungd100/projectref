﻿using System.Data;
using ChannelVN.CMS.Common.DalCommon;
using ChannelVN.CafeBiz.MainDal.Databases;

namespace ChannelVN.CafeBiz.MainDal.Databases
{
    /// <summary>
    /// The base class for the <see cref="CmsMainDb"/> class that 
    /// represents a connection to the <c>CmsMainDb</c> database. 
    /// </summary>
    /// <remarks>
    /// Do not change this source code. Modify the CmsMainDb class
    /// if you need to add or change some functionality.
    /// </remarks>
    public abstract class CmsMainDbBase : MainDbBase
    {
        #region Store procedures

        #region Biz Doc
        private BizDocDal _bizDocContentMainDal;
        public BizDocDalBase BizDocContentMainDal
        {
            get { return _bizDocContentMainDal ?? (_bizDocContentMainDal = new BizDocDal((CmsMainDb)this)); }
        }
        #endregion

        #region  Biz Doc Field
        private BizDocFieldDal _bizDocFieldContentMainDal;
        public BizDocFieldDalBase BizDocFieldContentMainDal
        {
            get { return _bizDocFieldContentMainDal ?? (_bizDocFieldContentMainDal = new BizDocFieldDal((CmsMainDb)this)); }
        }
        #endregion


        #region BizDoc Properties
        private BizDocPropertiesDal _bizDocPropertiesContentMainDal;
        public BizDocPropertiesDalBase BizDocPropertiesContentMainDal
        {
            get { return _bizDocPropertiesContentMainDal ?? (_bizDocPropertiesContentMainDal = new BizDocPropertiesDal((CmsMainDb)this)); }
        }
        #endregion


        //#region Partner Properties
        //private PartnerDal _partnerContentMainDal;
        //public PartnerDalBase PartnerContentMainDal
        //{
        //    get { return _partnerContentMainDal ?? (_partnerContentMainDal = new PartnerDal((CmsMainDb)this)); }
        //}
        //#endregion

        
        //#region PartnerNews Properties
        //private PartnerNewsDal _partnerNewsContentMainDal;
        //public PartnerNewsDalBase PartnerNewsContentMainDal
        //{
        //    get { return _partnerNewsContentMainDal ?? (_partnerNewsContentMainDal = new PartnerNewsDal((CmsMainDb)this)); }
        //}
        //#endregion

        #endregion

        #region Constructors

        protected CmsMainDbBase()
        {
        }
        protected CmsMainDbBase(bool init)
        {
            if (init)
            {
                InitConnection();
            }
        }

        #endregion

        #region Protected members

        protected override sealed void InitConnection()
        {
            _connection = CreateConnection();
            _connection.Open();
        }

        /// <summary>
        /// Creates a new connection to the database.
        /// </summary>
        /// <returns>A reference to the <see cref="System.Data.IDbConnection"/> object.</returns>
        protected abstract IDbConnection CreateConnection();

        #endregion
    }
}