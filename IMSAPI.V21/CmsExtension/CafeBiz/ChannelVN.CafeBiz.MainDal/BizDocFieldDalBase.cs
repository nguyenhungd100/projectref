﻿using ChannelVN.CafeBiz.Entity;
using ChannelVN.CafeBiz.MainDal.Databases;
using ChannelVN.CMS.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ChannelVN.CafeBiz.MainDal
{
    public abstract class BizDocFieldDalBase
    {
        #region Function GET
        /// <summary>
        /// Lấy danh sách các lĩnh vực văn bản pháp luật theo paging
        /// </summary>
        /// <param name="pageIndex">int</param>
        /// <param name="pageSize">int</param>
        /// <param name="totalRow">output</param>
        /// <returns>List</returns>
        public List<BizDocFieldEntity> SelectListField(int pageIndex, int pageSize, ref int totalRow)
        {
            const string commandText = "CMS_BizDocField_GetAllData";
            try
            {
                List<BizDocFieldEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "TotalRow", totalRow, System.Data.ParameterDirection.Output);
                _db.AddParameter(cmd, "PageIndex", pageIndex);
                _db.AddParameter(cmd, "PageSize", pageSize);
                data = _db.GetList<BizDocFieldEntity>(cmd);
                totalRow = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 0));
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        /// <summary>
        /// Lấy toàn bộ lĩnh vực văn bản pháp luật
        /// </summary>
        /// <returns>List</returns>
        public List<BizDocFieldEntity> SelectListAllField()
        {
            const string commandText = "CMS_BizDocField_SelectListAllField";
            try
            {
                List<BizDocFieldEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                data = _db.GetList<BizDocFieldEntity>(cmd);
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        /// <summary>
        /// Lấy thông tin chi tiết lĩnh vực văn bản pháp luật
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public BizDocFieldEntity SelectDataById(int id)
        {
            const string commandText = "CMS_BizDocField_GetDataById";
            try
            {
                BizDocFieldEntity data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", id);
                data = _db.Get<BizDocFieldEntity>(cmd);
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        #endregion

        #region Function SET
        public bool Insert(BizDocFieldEntity info, ref int id)
        {
            const string commandText = "CMS_BizDocField_Insert";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", id, System.Data.ParameterDirection.Output);
                _db.AddParameter(cmd, "Name", info.Name);
                _db.AddParameter(cmd, "Description", info.Note);
                var data = cmd.ExecuteNonQuery();
                id = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 0));
                return data > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool Update(BizDocFieldEntity info)
        {
            const string commandText = "CMS_BizDocField_Update";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", info.Id);
                _db.AddParameter(cmd, "Name", info.Name);
                _db.AddParameter(cmd, "Description", info.Note);
                var data = cmd.ExecuteNonQuery();
                return data > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool Delete(int Id)
        {
            const string commandText = "CMS_BizDocField_Delete";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", Id);
                var data = cmd.ExecuteNonQuery();
                return data != 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        #endregion

        /// <summary>
        /// Chuyển đổi DAL cho BizDocField
        /// minhduongvan
        /// 2014/01/07
        /// </summary>

        #region Constructor

        private readonly CmsMainDb _db;

        protected BizDocFieldDalBase(CmsMainDb db)
        {
            _db = db;
        }

        protected CmsMainDb Database
        {
            get { return _db; }
        }

        #endregion
    }
}
