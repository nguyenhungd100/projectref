﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using ChannelVN.CMS.Common;

namespace ChannelVN.WorldCup.Entity
{    
    [DataContract]
    public class WCTopPlayersEntity : EntityBase
    {
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public string Name { get; set; }
        [DataMember]
        public int TeamId { get; set; }           
        [DataMember]
        public int Goal { get; set; }
    }
    
    [DataContract]
    public class WCTopPlayersDetailEntity : EntityBase
    {
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public string Name { get; set; }
        [DataMember]
        public int TeamId { get; set; }
        [DataMember]
        public string TeamName { get; set; }        
        [DataMember]
        public int Goal { get; set; }
    }
}
