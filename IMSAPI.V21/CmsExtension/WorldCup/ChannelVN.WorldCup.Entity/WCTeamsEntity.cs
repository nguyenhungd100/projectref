﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using ChannelVN.CMS.Common;

namespace ChannelVN.WorldCup.Entity
{    
    [DataContract]
    public class WCTeamsEntity : EntityBase
    {
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public string Name { get; set; }
        [DataMember]
        public string Avatar { get; set; }
        [DataMember]
        public string GroupName { get; set; }
        [DataMember]
        public int Played { get; set; }
        [DataMember]
        public int Win { get; set; }
        [DataMember]
        public int Draw { get; set; }
        [DataMember]
        public int Lost { get; set; }
        [DataMember]
        public int GoalFor { get; set; }
        [DataMember]
        public int GoalAgainst { get; set; }
        [DataMember]
        public int GoalDifference { get; set; }
        [DataMember]
        public int Point { get; set; }
    }
}
