﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using ChannelVN.CMS.Common;
using ChannelVN.WorldCup.Entity;
using ChannelVN.WorldCup.MainDal.Databases;

namespace ChannelVN.WorldCup.MainDal
{
    public abstract class WCTeamsDalBase
    {
        public List<WCTeamsEntity> Search(string groupName)
        {
            const string commandText = "CMS_WCTeams_Search";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "GroupName", groupName);
                var numberOfRow = _db.GetList<WCTeamsEntity>(cmd);
                return numberOfRow;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public WCTeamsEntity GetById(int id)
        {
            const string commandText = "CMS_WCTeams_GetById";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", id);
                var numberOfRow = _db.Get<WCTeamsEntity>(cmd);
                return numberOfRow;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool Insert(WCTeamsEntity entity)
        {
            const string commandText = "CMS_WCTeams_Insert";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", entity.Id);
                _db.AddParameter(cmd, "Name", entity.Name);
                _db.AddParameter(cmd, "Avatar", entity.Avatar);
                _db.AddParameter(cmd, "GroupName", entity.GroupName);
                _db.AddParameter(cmd, "Played", entity.Played);
                _db.AddParameter(cmd, "Win", entity.Win);
                _db.AddParameter(cmd, "Draw", entity.Draw);
                _db.AddParameter(cmd, "Lost", entity.Lost);
                _db.AddParameter(cmd, "GoalFor", entity.GoalFor);
                _db.AddParameter(cmd, "GoalAgainst", entity.GoalAgainst);
                _db.AddParameter(cmd, "GoalDifference", entity.GoalDifference);
                _db.AddParameter(cmd, "Point", entity.Point);
                var numberOfRow = _db.ExecuteNonQuery(cmd);
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool Update(WCTeamsEntity entity)
        {
            const string commandText = "CMS_WCTeams_Update";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", entity.Id);
                _db.AddParameter(cmd, "Name", entity.Name);
                _db.AddParameter(cmd, "Avatar", entity.Avatar);
                _db.AddParameter(cmd, "GroupName", entity.GroupName);
                _db.AddParameter(cmd, "Played", entity.Played);
                _db.AddParameter(cmd, "Win", entity.Win);
                _db.AddParameter(cmd, "Draw", entity.Draw);
                _db.AddParameter(cmd, "Lost", entity.Lost);
                _db.AddParameter(cmd, "GoalFor", entity.GoalFor);
                _db.AddParameter(cmd, "GoalAgainst", entity.GoalAgainst);
                _db.AddParameter(cmd, "GoalDifference", entity.GoalDifference);
                _db.AddParameter(cmd, "Point", entity.Point);
                var numberOfRow = _db.ExecuteNonQuery(cmd);
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool Delete(int id)
        {
            const string commandText = "CMS_WCTeams_DeleteById";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", id);
                var numberOfRow = _db.ExecuteNonQuery(cmd);
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        
        #region Core members

        private readonly CmsMainDb _db;

        protected WCTeamsDalBase(CmsMainDb db)
        {
            _db = db;
        }

        protected CmsMainDb Database
        {
            get { return _db; }
        }

        #endregion
    }
}
