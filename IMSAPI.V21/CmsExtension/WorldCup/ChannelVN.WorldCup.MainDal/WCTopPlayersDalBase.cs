﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using ChannelVN.CMS.Common;
using ChannelVN.WorldCup.Entity;
using ChannelVN.WorldCup.MainDal.Databases;

namespace ChannelVN.WorldCup.MainDal
{
    public abstract class WCTopPlayersDalBase
    {
        public List<WCTopPlayersDetailEntity> Search(string name)
        {
            const string commandText = "CMS_WCTopPlayers_Search";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Name", name);
                var numberOfRow = _db.GetList<WCTopPlayersDetailEntity>(cmd);
                return numberOfRow;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public WCTopPlayersEntity GetById(int id)
        {
            const string commandText = "CMS_WCTopPlayers_GetById";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", id);
                var numberOfRow = _db.Get<WCTopPlayersEntity>(cmd);
                return numberOfRow;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool Insert(WCTopPlayersEntity entity)
        {
            const string commandText = "CMS_WCTopPlayers_Insert";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", entity.Id);
                _db.AddParameter(cmd, "Name", entity.Name);
                _db.AddParameter(cmd, "Goal", entity.Goal);
                _db.AddParameter(cmd, "TeamId", entity.TeamId);
                var numberOfRow = _db.ExecuteNonQuery(cmd);
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool Update(WCTopPlayersEntity entity)
        {
            const string commandText = "CMS_WCTopPlayers_Update";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", entity.Id);
                _db.AddParameter(cmd, "Name", entity.Name);
                _db.AddParameter(cmd, "Goal", entity.Goal);
                _db.AddParameter(cmd, "TeamId", entity.TeamId);
                var numberOfRow = _db.ExecuteNonQuery(cmd);
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool Delete(int id)
        {
            const string commandText = "CMS_WCTopPlayers_DeleteById";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", id);
                var numberOfRow = _db.ExecuteNonQuery(cmd);
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        #region Core members

        private readonly CmsMainDb _db;

        protected WCTopPlayersDalBase(CmsMainDb db)
        {
            _db = db;
        }

        protected CmsMainDb Database
        {
            get { return _db; }
        }

        #endregion
    }
}
