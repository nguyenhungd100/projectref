﻿using ChannelVN.CMS.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ChannelVN.WorldCup.Entity.ErrorCode;
using ChannelVN.WorldCup.Entity;
using ChannelVN.WorldCup.Dal;

namespace ChannelVN.WorldCup.Bo
{
    public class WCTeamsBo
    {

        #region WCTeams

        public static List<WCTeamsEntity> SearchTeams(string groupName)
        {
            return WCTeamsDal.Search(groupName);
        }
        public static WCTeamsEntity GetWCTeamsById(int id)
        {
            return WCTeamsDal.GetById(id);
        }
        public static ErrorMapping.ErrorCodes InsertTeam(WCTeamsEntity entity)
        {
            return WCTeamsDal.Insert(entity)
                       ? ErrorMapping.ErrorCodes.Success
                       : ErrorMapping.ErrorCodes.BusinessError;
        }
        public static ErrorMapping.ErrorCodes UpdateTeam(WCTeamsEntity entity)
        {
            var team = WCTeamsDal.GetById(entity.Id);
            if (team == null)
            {
                return ErrorMapping.ErrorCodes.BusinessError;
            }
            team.Name = entity.Name;
            team.Avatar = entity.Avatar;
            team.Played = entity.Played;
            team.Win = entity.Win;
            team.Draw = entity.Draw;
            team.Lost = entity.Lost;
            team.GroupName = entity.GroupName;
            team.GoalFor = entity.GoalFor;
            team.GoalAgainst = entity.GoalAgainst;
            team.GoalDifference = entity.GoalDifference;
            team.Point = entity.Point;

            return WCTeamsDal.Update(entity)
                       ? ErrorMapping.ErrorCodes.Success
                       : ErrorMapping.ErrorCodes.BusinessError;
        }
        public static ErrorMapping.ErrorCodes DeteleTeam(int id)
        {
            if (id <= 0)
            {
                return ErrorMapping.ErrorCodes.BusinessError;
            }
            return WCTeamsDal.Delete(id)
                       ? ErrorMapping.ErrorCodes.Success
                       : ErrorMapping.ErrorCodes.BusinessError;
        }

        #endregion

        #region WCTopPlayers

        public static List<WCTopPlayersDetailEntity> SearchTopPlayers(string name)
        {
            return WCTopPlayersDal.Search(name);
        }
        public static WCTopPlayersEntity GetTopPlayerById(int id)
        {
            return WCTopPlayersDal.GetById(id);
        }
        public static ErrorMapping.ErrorCodes InsertTopPlayer(WCTopPlayersEntity entity)
        {
            var success = WCTopPlayersDal.Insert(entity);
            if (success)
            {
                return ErrorMapping.ErrorCodes.Success;
            }
            return ErrorMapping.ErrorCodes.BusinessError;
        }
        public static ErrorMapping.ErrorCodes UpdateTopPlayer(WCTopPlayersEntity entity)
        {
            var playersEntity = WCTopPlayersDal.GetById(entity.Id);

            playersEntity.Name = entity.Name;
            playersEntity.Goal = entity.Goal;
            playersEntity.TeamId = entity.TeamId;
            
            return WCTopPlayersDal.Update(playersEntity)
                       ? ErrorMapping.ErrorCodes.Success
                       : ErrorMapping.ErrorCodes.BusinessError;
        }
        public static ErrorMapping.ErrorCodes DeleteTopPlayer(int id)
        {            
            return WCTopPlayersDal.Delete(id)
                       ? ErrorMapping.ErrorCodes.Success
                       : ErrorMapping.ErrorCodes.BusinessError;
        }
        #endregion
    }
}
