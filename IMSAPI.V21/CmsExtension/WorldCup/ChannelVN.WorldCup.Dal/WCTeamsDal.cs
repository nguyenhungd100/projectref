﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using ChannelVN.CMS.Common;
using ChannelVN.WorldCup.Dal.Common;
using ChannelVN.WorldCup.Entity;
using ChannelVN.WorldCup.MainDal.Databases;

namespace ChannelVN.WorldCup.Dal
{
    public class WCTeamsDal
    {
        public static List<WCTeamsEntity> Search(string groupName)
        {
            List<WCTeamsEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.WCTeamsMainDal.Search(groupName);
            }
            return returnValue;
        }
        public static WCTeamsEntity GetById(int id)
        {
            WCTeamsEntity returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.WCTeamsMainDal.GetById(id);
            }
            return returnValue;
        }
        public static bool Insert(WCTeamsEntity entity)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.WCTeamsMainDal.Insert(entity);
            }
            return returnValue;
        }
        public static bool Update(WCTeamsEntity entity)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.WCTeamsMainDal.Update(entity);
            }
            return returnValue;
        }
        public static bool Delete(int id)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.WCTeamsMainDal.Delete(id);
            }
            return returnValue;
        }
    }
}
