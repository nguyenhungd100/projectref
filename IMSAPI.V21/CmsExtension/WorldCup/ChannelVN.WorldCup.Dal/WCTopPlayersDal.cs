﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using ChannelVN.CMS.Common;
using ChannelVN.WorldCup.Dal.Common;
using ChannelVN.WorldCup.Entity;
using ChannelVN.WorldCup.MainDal.Databases;

namespace ChannelVN.WorldCup.Dal
{
    public class WCTopPlayersDal
    {
        public static List<WCTopPlayersDetailEntity> Search(string name)
        {
            List<WCTopPlayersDetailEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.WCTopPlayersMainDal.Search(name);
            }
            return returnValue;
        }
        public static WCTopPlayersEntity GetById(int id)
        {
            WCTopPlayersEntity returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.WCTopPlayersMainDal.GetById(id);
            }
            return returnValue;
        }
        public static bool Insert(WCTopPlayersEntity entity)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.WCTopPlayersMainDal.Insert(entity);
            }
            return returnValue;
        }
        public static bool Update(WCTopPlayersEntity entity)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.WCTopPlayersMainDal.Update(entity);
            }
            return returnValue;
        }
        public static bool Delete(int id)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.WCTopPlayersMainDal.Delete(id);
            }
            return returnValue;
        }
    }
}
