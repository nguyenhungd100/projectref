﻿using System.Runtime.Serialization;

namespace ChannelVN.Expert.Entity
{
    [DataContract]
    public class ExpertInNews
    {
        [DataMember]
        public int Id { set; get; }
        [DataMember]
        public long NewsId { set; get; }
        [DataMember]
        public int ExpertId { set; get; }
        [DataMember]
        public string Quote { set; get; }
    }
}
