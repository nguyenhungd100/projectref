﻿using System.Runtime.Serialization;

namespace ChannelVN.Expert.Entity
{
    [DataContract]
    public class ExpertInZone
    {
        [DataMember]
        public int Id { set; get; }
        [DataMember]
        public int ZoneId { set; get; }
        [DataMember]
        public int ExpertId { set; get; }
    }
}
