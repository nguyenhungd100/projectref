﻿using System;
using System.Runtime.Serialization;

namespace ChannelVN.Expert.Entity
{
    [DataContract]
    public class ExpertEntity
    {
        [DataMember]
        public int Id { set; get; }
        [DataMember]
        public string ExpertName { set; get; }
        [DataMember]
        public string JobTitle { set; get; }
        [DataMember]
        public string Description { set; get; }
        [DataMember]
        public string Avatar { set; get; }
        [DataMember]
        public bool DisplayJobTitle { set; get; }
        [DataMember]
        public string Quote { set; get; }
        [DataMember]
        public bool AcceptFeedback { set; get; }
        [DataMember]
        public string FacebookLink { set; get; }
        [DataMember]
        public string Mobile { set; get; }
        [DataMember]
        public int NewsCount { get; set; }
        [DataMember]
        public long TelegramId { get; set; }
    }
    [DataContract]
    public class ExpertInNewsEntity
    {
        [DataMember]
        public long Id { set; get; }
        [DataMember]
        public long NewsId { set; get; }
        [DataMember]
        public string EncryptId { get { return NewsId.ToString(); } set { } }
        [DataMember]
        public int ExpertId { set; get; }
        [DataMember]
        public string Quote { set; get; }
        [DataMember]
        public string Avatar { set; get; }
        [DataMember]
        public string Title { set; get; }
        [DataMember]
        public string Sapo { set; get; }
        [DataMember]
        public string ZoneName { set; get; }
        [DataMember]
        public string Url { set; get; }
        [DataMember]
        public DateTime DistributionDate { set; get; }

    }
}
