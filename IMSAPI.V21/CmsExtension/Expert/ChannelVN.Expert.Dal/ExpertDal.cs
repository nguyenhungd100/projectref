﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.Entity.Base.News;
using ChannelVN.Expert.Entity;
using ChannelVN.Expert.MainDal.Databases;

namespace ChannelVN.Expert.Dal
{
    public class ExpertDal
    {
        public static bool Insert(ExpertEntity expertEntity, ref int newsAuthorId)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.ExpertMainDal.Insert(expertEntity, ref newsAuthorId);
            }
            return returnValue;
        }

        public static ExpertEntity GetById(int id)
        {
            ExpertEntity returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.ExpertMainDal.GetById(id);
            }
            return returnValue;
        }

        public static bool Update(ExpertEntity expertEntity)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.ExpertMainDal.Update(expertEntity);
            }
            return returnValue;
        }
        public static bool UpdateTelegramId(int expertId, long telegramId)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.ExpertMainDal.UpdateTelegramId(expertId, telegramId);
            }
            return returnValue;
        }

        public static List<ExpertEntity> Search(string keyword)
        {
            List<ExpertEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.ExpertMainDal.Search(keyword);
            }
            return returnValue;
        }

        public static List<ExpertEntity> SearchExpert(string keyword, int pageIndex, int pageSize, ref int totalRow)
        {
            List<ExpertEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.ExpertMainDal.SearchExpert(keyword, pageIndex, pageSize, ref totalRow);
            }
            return returnValue;
        }

        public static bool Delete(int id)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.ExpertMainDal.Delete(id);
            }
            return returnValue;
        }

        public static ExpertEntity GetExpertByNewsId(long id)
        {
            ExpertEntity returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.ExpertMainDal.GetExpertByNewsId(id);
            }
            return returnValue;
        }

        public static bool InsertExpertToNews(ExpertInNews expertEntity)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.ExpertMainDal.InsertExpertToNews(expertEntity);
            }
            return returnValue;
        }

        public static bool UpdateExpertInNews(ExpertInNews expertEntity)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.ExpertMainDal.UpdateExpertToNews(expertEntity);
            }
            return returnValue;
        }

        public static bool DeleteExpertNewsId(long id)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.ExpertMainDal.DeleteExpertInNews(id);
            }
            return returnValue;
        }

        public static List<NewsInListEntity> SearchExpertNewsWhichPublished(int zoneId, string keyword, int type, int displayPosition, DateTime distributedDateFrom, DateTime distributedDateTo, string excludeNewsIds, int newstype, int pageIndex, int pageSize, ref int totalRow)
        {
            List<NewsInListEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.ExpertMainDal.SearchExpertNewsWhichPublished(zoneId, keyword, type, displayPosition, distributedDateFrom, distributedDateTo, excludeNewsIds, newstype, pageIndex, pageSize, ref  totalRow);
            }
            return returnValue;
        }

        public static List<ExpertInZone> GetExpertByExpertId(int id)
        {
            List<ExpertInZone> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.ExpertMainDal.GetExpertByExpertId(id);
            }
            return returnValue;
        }

        public static bool InsertExpertToZone(ExpertInZone expertEntity)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.ExpertMainDal.InsertExpertToZone(expertEntity);
            }
            return returnValue;
        }

        public static bool UpdateExpertInZone(ExpertInZone expertEntity)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.ExpertMainDal.UpdateExpertToZone(expertEntity);
            }
            return returnValue;
        }

        public static bool DeleteExpertZoneId(int id)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.ExpertMainDal.DeleteExpertInZone(id);
            }
            return returnValue;
        }

        #region ExpertInNews
        public static List<ExpertInNewsEntity> ExpertInNews_GetByExpertId(int expertId, int pageIndex, int pageSize, ref int totalRow)
        {
            List<ExpertInNewsEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.ExpertMainDal.ExpertInNews_GetByExpertId(expertId, pageIndex, pageSize, ref totalRow);
            }
            return returnValue;
        }

        public static bool ExpertInNews_UpdateNews(int expertId, string deleteNewsId, string addNewsId)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.ExpertMainDal.ExpertInNews_UpdateNews(expertId, deleteNewsId, addNewsId);
            }
            return returnValue;

        }
        #endregion
    }
}
