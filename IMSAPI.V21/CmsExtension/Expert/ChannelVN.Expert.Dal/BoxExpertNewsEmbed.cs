﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using ChannelVN.CMS.Common;
using ChannelVN.Expert.Entity;
using ChannelVN.Expert.MainDal.Databases;

namespace ChannelVN.Expert.Dal
{
    public class BoxExpertNewsEmbedDal
    {
        public static List<BoxExpertNewsEmbedListEntity> GetListBoxExpertNewsEmbed(int zoneId, int type)
        {
            List<BoxExpertNewsEmbedListEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.BoxExpertNewsEmbedMainDal.GetListNewsEmbedBoxOnPage(zoneId, type);
            }
            return returnValue;
        }

        public static bool Insert(BoxExpertNewsEmbedEntity newsEmbebBox)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.BoxExpertNewsEmbedMainDal.Insert(newsEmbebBox);
            }
            return returnValue;
        }

        public static void Update(string listNewsId, int zoneId, int type)
        {
            using (var db = new CmsMainDb())
            {
                db.BoxExpertNewsEmbedMainDal.Update(listNewsId, zoneId, type);
            }
        }

        public static void Delete(long newsId, int zoneId, int type)
        {
            using (var db = new CmsMainDb())
            {
                db.BoxExpertNewsEmbedMainDal.Delete(newsId, zoneId, type);
            }
        }
    }
}
