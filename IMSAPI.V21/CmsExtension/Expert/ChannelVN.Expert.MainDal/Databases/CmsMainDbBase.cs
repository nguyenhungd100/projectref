﻿using System.Data;
using ChannelVN.CMS.Common.DalCommon;
//using ChannelVN.CMS.MainDal.External.CafeF;

namespace ChannelVN.Expert.MainDal.Databases
{
    /// <summary>
    /// The base class for the <see cref="CmsMainDb"/> class that 
    /// represents a connection to the <c>CmsMainDb</c> database. 
    /// </summary>
    /// <remarks>
    /// Do not change this source code. Modify the CmsMainDb class
    /// if you need to add or change some functionality.
    /// </remarks>
    public abstract class CmsMainDbBase : MainDbBase
    {

        private ExpertDal _expertMainDal;
        public ExpertDal ExpertMainDal
        {
            get { return _expertMainDal ?? (_expertMainDal = new ExpertDal((CmsMainDb)this)); }
        }

        private BoxExpertNewsEmbedDal _boxExpertNewsEmbedMainDal;
        public BoxExpertNewsEmbedDal BoxExpertNewsEmbedMainDal
        {
            get { return _boxExpertNewsEmbedMainDal ?? (_boxExpertNewsEmbedMainDal = new BoxExpertNewsEmbedDal((CmsMainDb)this)); }
        }

        #region Constructors

        protected CmsMainDbBase()
        {
        }
        protected CmsMainDbBase(bool init)
        {
            if (init)
            {
                InitConnection();
            }
        }

        #endregion

        #region Protected members

        protected override sealed void InitConnection()
        {
            _connection = CreateConnection();
            _connection.Open();
        }

        /// <summary>
        /// Creates a new connection to the database.
        /// </summary>
        /// <returns>A reference to the <see cref="System.Data.IDbConnection"/> object.</returns>
        protected abstract IDbConnection CreateConnection();

        #endregion
    }
}