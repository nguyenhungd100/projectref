﻿using System;
using System.Collections.Generic;
using ChannelVN.Expert.Entity;
using ChannelVN.Expert.MainDal.Databases;

namespace ChannelVN.Expert.MainDal
{
    public abstract class BoxExpertNewsEmbedDalBase
    {
        public List<BoxExpertNewsEmbedListEntity> GetListNewsEmbedBoxOnPage(int zoneId, int type)
        {
            const string commandText = "CMS_BoxExpertNewsEmbed_GetListByZone";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "ZoneId", Convert.ToInt32(zoneId));
                _db.AddParameter(cmd, "Type", Convert.ToInt32(type));
                List<BoxExpertNewsEmbedListEntity> data = _db.GetList<BoxExpertNewsEmbedListEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public bool Insert(BoxExpertNewsEmbedEntity newsEmbebBox)
        {
            const string commandText = "CMS_BoxExpertNewsEmbed_Insert";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "ZoneId", newsEmbebBox.ZoneId);
                _db.AddParameter(cmd, "NewsId", newsEmbebBox.NewsId);
                _db.AddParameter(cmd, "SortOrder", newsEmbebBox.SortOrder);
                _db.AddParameter(cmd, "Type", newsEmbebBox.Type);
                bool data = _db.ExecuteNonQuery(cmd) > 0;

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public void Update(string listNewsId, int zoneId, int type)
        {
            const string commandText = "CMS_BoxExpertNewsEmbed_Update";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "ListOfPriority", listNewsId);
                _db.AddParameter(cmd, "ZoneId", zoneId);
                _db.AddParameter(cmd, "Type", type);
                bool data = _db.ExecuteNonQuery(cmd) > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public void Delete(long newsId, int zoneId, int type)
        {
            const string commandText = "CMS_BoxExpertNewsEmbed_Delete";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "NewsId", newsId);
                _db.AddParameter(cmd, "ZoneId", zoneId);
                _db.AddParameter(cmd, "Type", type);
                bool data = _db.ExecuteNonQuery(cmd) > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        #region Core members

        private readonly CmsMainDb _db;

        protected BoxExpertNewsEmbedDalBase(CmsMainDb db)
        {
            _db = db;
        }

        protected CmsMainDb Database
        {
            get { return _db; }
        }

        #endregion
    }
}
