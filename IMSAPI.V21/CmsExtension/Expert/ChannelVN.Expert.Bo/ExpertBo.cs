﻿using System;
using System.Collections.Generic;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.Entity.Base.News;
using ChannelVN.CMS.Entity.Base.Security;
using ChannelVN.CMS.Entity.ErrorCode;
using ChannelVN.Expert.Dal;
using ChannelVN.Expert.Entity;

namespace ChannelVN.Expert.Bo
{
    public class ExpertBo
    {
        public static ErrorMapping.ErrorCodes Insert(ExpertEntity ExpertEntity, ref int ExpertId)
        {
            try
            {
                var inserted = ExpertDal.Insert(ExpertEntity, ref ExpertId);
                return inserted ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.UnknowError;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return ErrorMapping.ErrorCodes.BusinessError;
            }
        }

        public static ExpertEntity GetById(int id)
        {
            try
            {
                return ExpertDal.GetById(id);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return new ExpertEntity();
            }
        }

        public static ErrorMapping.ErrorCodes Update(ExpertEntity expertEntity)
        {
            try
            {
                var inserted = ExpertDal.Update(expertEntity);
                return inserted ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.UnknowError;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return ErrorMapping.ErrorCodes.BusinessError;
            }
        }

        public static ErrorMapping.ErrorCodes UpdateTelegramId(int expertId, long telegramId)
        {
            var returnData = ExpertDal.UpdateTelegramId(expertId, telegramId);
            return returnData ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.UnknowError;
        }

        public static List<ExpertEntity> Search(string keyword)
        {
            try
            {
                return ExpertDal.Search(keyword);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return new List<ExpertEntity>();
            }
        }

        public static List<ExpertEntity> SearchExpert(string keyword, int pageIndex, int pageSize, ref int totalRow)
        {

            try
            {
                return ExpertDal.SearchExpert(keyword, pageIndex, pageSize, ref totalRow);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.ToString());
                return new List<ExpertEntity>();
            }            
        }

        public static ErrorMapping.ErrorCodes Delete(int id)
        {
            try
            {
                var inserted = ExpertDal.Delete(id);
                return inserted ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.UnknowError;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return ErrorMapping.ErrorCodes.BusinessError;
            }
        }

        public static ExpertEntity GetExpertByNewsId(long id)
        {
            try
            {
                //tam comment cached
                //var expert = CMS.BoCached.CmsExtension.Expert.ExpertDalFactory.GetExpertByNewsId(id);
                //if (expert == null || (expert != null && expert.Id==0))
                //{
                //    expert = ExpertDal.GetExpertByNewsId(id);
                //    if (expert != null)
                //    {
                //        //add to redis by newsid
                //        CMS.BoCached.CmsExtension.Expert.ExpertDalFactory.AddExpertByNewsId(id, expert);
                //    }
                //}
                //return expert;             
                   
                return ExpertDal.GetExpertByNewsId(id);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return new ExpertEntity();
            }
        }

        public static ErrorMapping.ErrorCodes InsertExpertIntoNews(ExpertInNews expertEntity)
        {
            try
            {
                var expertInNews = ExpertDal.GetExpertByNewsId(expertEntity.NewsId);
                // Nếu không chọn chuyên gia
                if (expertEntity.ExpertId == 0)
                {
                    // Nếu đã chèn chuyên gia trước
                    if (expertInNews != null && expertInNews.Id > 0)
                    {
                        // Xóa chuyên gia
                        ExpertDal.DeleteExpertNewsId(expertEntity.NewsId);
                    }
                    
                    return ErrorMapping.ErrorCodes.Success;
                }
                bool inserted;
                if (expertInNews != null && expertInNews.Id > 0)
                {
                    if (expertEntity.ExpertId == 0)
                    {
                        ExpertDal.DeleteExpertNewsId(expertEntity.NewsId);
                        return ErrorMapping.ErrorCodes.Success;
                    }
                    inserted = ExpertDal.UpdateExpertInNews(expertEntity);
                    return inserted ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.UnknowError;
                }
                inserted = ExpertDal.InsertExpertToNews(expertEntity);
                return inserted ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.UnknowError;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return ErrorMapping.ErrorCodes.BusinessError;
            }
        }

        public static List<NewsInListEntity> SearchExpertNewsWhichPublished(int zoneId, string keyword, int type,
                                                                      int displayPosition,
                                                                      DateTime distributedDateFrom,
                                                                      DateTime distributedDateTo, string excludeNewsIds, int newsType,
                                                                      int pageIndex, int pageSize, ref int totalRow)
        {
            try
            {
                return ExpertDal.SearchExpertNewsWhichPublished(zoneId, keyword, type, displayPosition, distributedDateFrom,
                                                        distributedDateTo, excludeNewsIds, newsType, pageIndex, pageSize,
                                                        ref totalRow);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return null;
            }
        }

        public static List<ExpertInZone> GetExpertByExpertId(int id)
        {
            try
            {
                return ExpertDal.GetExpertByExpertId(id);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return new List<ExpertInZone>();
            }
        }

        public static ErrorMapping.ErrorCodes InsertExpertIntoZone(ExpertInZone expertEntity)
        {
            try
            {
                bool inserted = ExpertDal.InsertExpertToZone(expertEntity);
                return inserted ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.UnknowError;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return ErrorMapping.ErrorCodes.BusinessError;
            }
        }

        public static ErrorMapping.ErrorCodes DeleteExpertZone(int expertId)
        {
            try
            {
                bool inserted = ExpertDal.DeleteExpertZoneId(expertId);
                return inserted ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.UnknowError;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return ErrorMapping.ErrorCodes.BusinessError;
            }
        }

        #region ExpertInNews
        public static ErrorMapping.ErrorCodes ExpertInNews_UpdateNews(int expertId, string deleteNewsId, string addNewsId)
        {
            try
            {
                bool inserted = ExpertDal.ExpertInNews_UpdateNews(expertId, deleteNewsId, addNewsId);
                return inserted ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.UnknowError;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return ErrorMapping.ErrorCodes.BusinessError;
            }
        }

        public static List<ExpertInNewsEntity> ExpertInNews_GetByExpertId(int expertId, int pageIndex, int pageSize, ref int totalRow)
        {
            try
            {
                return ExpertDal.ExpertInNews_GetByExpertId(expertId, pageIndex, pageSize, ref totalRow);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return new List<ExpertInNewsEntity>();
            }
        }
        #endregion
    }
}
