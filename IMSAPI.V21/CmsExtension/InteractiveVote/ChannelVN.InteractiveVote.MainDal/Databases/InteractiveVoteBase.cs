﻿using System.Data;
using ChannelVN.CMS.Common.DalCommon;

namespace ChannelVN.InteractiveVote.MainDal.Databases
{
    /// <summary>
    /// The base class for the <see cref="InteractiveVoteDb"/> class that 
    /// represents a connection to the <c>InfoGraphDb</c> database. 
    /// </summary>
    /// <remarks>
    /// Do not change this source code. Modify the InfoGraphDb class
    /// if you need to add or change some functionality.
    /// </remarks>
    public abstract class InteractiveVoteDbBase : MainDbBase
    {
        #region Store procedures
        private InteractiveVoteDal _quizContentDal;
        public InteractiveVoteDalBase InteractiveVoteDal
        {
            get { return _quizContentDal ?? (_quizContentDal = new InteractiveVoteDal((InteractiveVoteDb)this)); }
        }
        
        #endregion

        #region Constructors

        protected InteractiveVoteDbBase()
        {
        }
        protected InteractiveVoteDbBase(bool init)
        {
            if (init)
            {
                InitConnection();
            }
        }

        #endregion

        #region Protected members

        protected override sealed void InitConnection()
        {
            _connection = CreateConnection();
            _connection.Open();
        }

        /// <summary>
        /// Creates a new connection to the database.
        /// </summary>
        /// <returns>A reference to the <see cref="System.Data.IDbConnection"/> object.</returns>
        protected abstract IDbConnection CreateConnection();

        #endregion
    }
}