﻿using ChannelVN.InteractiveVote.MainDal.Databases;

namespace ChannelVN.InteractiveVote.MainDal
{
    public class InteractiveVoteDal : InteractiveVoteDalBase
    {
        internal InteractiveVoteDal(InteractiveVoteDb db) : base(db) { }
    }
}
