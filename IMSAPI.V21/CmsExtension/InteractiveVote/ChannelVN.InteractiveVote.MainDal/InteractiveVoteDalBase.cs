﻿using ChannelVN.CMS.Common;
using ChannelVN.CMS.MainDal.Databases;
using ChannelVN.InteractiveVote.Entity;
using ChannelVN.InteractiveVote.MainDal.Databases;
using System;
using System.Collections.Generic;

namespace ChannelVN.InteractiveVote.MainDal
{
    public abstract class InteractiveVoteDalBase
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="vote"></param>
        /// <returns></returns>
        public bool Insert(InteractiveVoteEntity vote)
        {
            const string storeProcedure = "CMS_Vote_Insert";

            try
            {

                var cmd = _db.CreateCommand(storeProcedure, true);
                _db.AddParameter(cmd, "Type", vote.Type);
                _db.AddParameter(cmd, "Title", vote.Title);
                _db.AddParameter(cmd, "Sapo", vote.Sapo);
                _db.AddParameter(cmd, "Avatar", vote.Avatar);
                _db.AddParameter(cmd, "BackgroundCover", vote.BackgroundCover);
                _db.AddParameter(cmd, "Author", vote.Author);
                _db.AddParameter(cmd, "Source", vote.Source);
                _db.AddParameter(cmd, "VoteData", vote.VoteData);
                _db.AddParameter(cmd, "VoteConfig", vote.VoteConfig);
                _db.AddParameter(cmd, "CreatedBy", vote.CreatedBy);
                _db.AddParameter(cmd, "Status", vote.Status);
                _db.AddParameter(cmd, "SiteId", vote.SiteId);
                _db.AddParameter(cmd, "FacebookShareTitle", vote.FacebookShareTitle);
                _db.AddParameter(cmd, "FacebookShareAvatar", vote.FacebookShareAvatar);
                _db.AddParameter(cmd, "FacebookShareDescription", vote.FacebookShareDescription);

                int numberOfRow = cmd.ExecuteNonQuery();

                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", storeProcedure, ex.Message));
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="vote"></param>
        /// <returns></returns>
        public bool Update(InteractiveVoteEntity vote)
        {
            const string storeProcedure = "CMS_Vote_Update";

            try
            {

                var cmd = _db.CreateCommand(storeProcedure, true);
                _db.AddParameter(cmd, "Id", vote.Id);
                _db.AddParameter(cmd, "Type", vote.Type);
                _db.AddParameter(cmd, "Title", vote.Title);
                _db.AddParameter(cmd, "Sapo", vote.Sapo);
                _db.AddParameter(cmd, "Avatar", vote.Avatar);
                _db.AddParameter(cmd, "BackgroundCover", vote.BackgroundCover);
                _db.AddParameter(cmd, "Author", vote.Author);
                _db.AddParameter(cmd, "Source", vote.Source);
                _db.AddParameter(cmd, "VoteData", vote.VoteData);
                _db.AddParameter(cmd, "VoteConfig", vote.VoteConfig);
                _db.AddParameter(cmd, "LastModifiedBy", vote.LastModifiedBy);
                _db.AddParameter(cmd, "Status", vote.Status);
                _db.AddParameter(cmd, "FacebookShareTitle", vote.FacebookShareTitle);
                _db.AddParameter(cmd, "FacebookShareAvatar", vote.FacebookShareAvatar);
                _db.AddParameter(cmd, "FacebookShareDescription", vote.FacebookShareDescription);
                int numberOfRow = cmd.ExecuteNonQuery();

                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", storeProcedure, ex.Message));
            }
        }
        public bool UpdateData(int id, string voteData, string lastModifiedBy)
        {
            const string storeProcedure = "CMS_Vote_UpdateConfig";

            try
            {

                var cmd = _db.CreateCommand(storeProcedure, true);
                _db.AddParameter(cmd, "Id", id);
                _db.AddParameter(cmd, "VoteData", voteData);
                _db.AddParameter(cmd, "LastModifiedBy", lastModifiedBy);
                int numberOfRow = cmd.ExecuteNonQuery();

                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", storeProcedure, ex.Message));
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <param name="voteData"></param>
        /// <param name="voteConfig"></param>
        /// <param name="lastModifiedBy"></param>
        /// <returns></returns>
        public bool UpdateConfig(int id, string voteData, string lastModifiedBy)
        {
            const string storeProcedure = "CMS_Vote_UpdateData";

            try
            {

                var cmd = _db.CreateCommand(storeProcedure, true);
                _db.AddParameter(cmd, "Id", id);
                _db.AddParameter(cmd, "VoteData", voteData);
                _db.AddParameter(cmd, "LastModifiedBy", lastModifiedBy);
                int numberOfRow = cmd.ExecuteNonQuery();

                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", storeProcedure, ex.Message));
            }
        }
        public InteractiveVoteEntity GetById(long id)
        {
            const string storeProcedure = "CMS_Vote_GetById";
            try
            {
                InteractiveVoteEntity data = null;
                var cmd = _db.CreateCommand(storeProcedure, true);
                _db.AddParameter(cmd, "Id", id);
                data = _db.Get<InteractiveVoteEntity>(cmd);
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", storeProcedure, ex.Message));
            }
        }
        public List<InteractiveVoteEntity> Search(string keyword, int type, string createdBy, int status, int siteId, int pageIndex, int pageSize, ref int totalRow)
        {
            const string storeProcedure = "CMS_Vote_Search";
            try
            {
                List<InteractiveVoteEntity> data = null;
                var cmd = _db.CreateCommand(storeProcedure, true);
                _db.AddParameter(cmd, "TotalRow", totalRow, System.Data.ParameterDirection.Output);
                _db.AddParameter(cmd, "Keyword", keyword);
                _db.AddParameter(cmd, "Type", type);
                _db.AddParameter(cmd, "CreatedBy", createdBy);
                _db.AddParameter(cmd, "Status", status);
                _db.AddParameter(cmd, "SiteId", siteId);
                _db.AddParameter(cmd, "PageIndex", pageIndex);
                _db.AddParameter(cmd, "PageSize", pageSize);
                data = _db.GetList<InteractiveVoteEntity>(cmd);
                totalRow = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 0));
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format(storeProcedure + ":{0}", ex.Message));
            }
        }

        #region Constructor

        private readonly InteractiveVoteDb _db;

        protected InteractiveVoteDalBase(InteractiveVoteDb db)
        {
            _db = db;
        }

        protected InteractiveVoteDb Database
        {
            get { return _db; }
        }

        #endregion
    }
}
