﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ChannelVN.CMS.Common;

namespace ChannelVN.InteractiveVote.Entity
{
    public enum EnumInteractiveVoteType : int
    {
        All = -1,
        Basic = 1,
        ItemWithPhoto = 2
    }

    public class InteractiveVoteEntity : EntityBase
    {
        public int Id { get; set; }
        public int Type { get; set; }
        public string Title { get; set; }
        public string Sapo { get; set; }
        public string Avatar { get; set; }
        public string BackgroundCover { get; set; }
        public string Author { get; set; }
        public string Source { get; set; }
        public string VoteData { get; set; }
        public string VoteConfig { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public string LastModifiedBy { get; set; }
        public DateTime LastModifiedDate { get; set; }
        public int Status { get; set; }
        public int SiteId { get; set; }
        public string FacebookShareTitle { get; set; }
        public string FacebookShareAvatar { get; set; }
        public string FacebookShareDescription { get; set; }
    }
}
