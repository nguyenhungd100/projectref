﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ChannelVN.InteractiveVote.Entity;
using ChannelVN.InteractiveVote.MainDal.Databases;

namespace ChannelVN.InteractiveVote.Dal
{
    public class InteractiveVoteDal
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="quiz"></param>
        /// <returns></returns>
        public static bool InsertInteractiveVote(InteractiveVoteEntity quiz)
        {
            bool retVal = false;
            using (var db = new InteractiveVoteDb())
            {
                retVal = db.InteractiveVoteDal.Insert(quiz);
            }
            return retVal;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="quiz"></param>
        /// <returns></returns>
        public static bool UpdateInteractiveVote(InteractiveVoteEntity quiz)
        {
            bool retVal = false;
            using (var db = new InteractiveVoteDb())
            {
                retVal = db.InteractiveVoteDal.Update(quiz);
            }
            return retVal;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="quiz"></param>
        /// <returns></returns>
        public static bool UpdateInteractiveVoteData(int id, string interactiveData, string lastModifiedBy)
        {
            bool retVal = false;
            using (var db = new InteractiveVoteDb())
            {
                retVal = db.InteractiveVoteDal.UpdateConfig(id, interactiveData, lastModifiedBy);
            }
            return retVal;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public static InteractiveVoteEntity GetInteractiveVoteById(int id)
        {
            InteractiveVoteEntity retVal = null;
            using (var db = new InteractiveVoteDb())
            {
                retVal = db.InteractiveVoteDal.GetById(id);
            }
            return retVal;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="keyword"></param>
        /// <param name="type"></param>
        /// <param name="createdBy"></param>
        /// <param name="status"></param>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <param name="totalRow"></param>
        /// <returns></returns>
        public static List<InteractiveVoteEntity> SearchInteractiveVote(string keyword, int type, string createdBy, int status, int siteId, int pageIndex, int pageSize, ref int totalRow)
        {
            List<InteractiveVoteEntity> retVal = new List<InteractiveVoteEntity>();
            using (var db = new InteractiveVoteDb())
            {
                retVal = db.InteractiveVoteDal.Search(keyword, type, createdBy, status, siteId, pageIndex, pageSize, ref totalRow);
            }
            return retVal;
        }
    }
}
