﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ChannelVN.InteractiveVote.Dal;
using ChannelVN.InteractiveVote.Entity;
using ChannelVN.InteractiveVote.Entity.ErrorCode;
using ChannelVN.CMS.Common;

namespace ChannelVN.InteractiveVote.Bo
{
    public class InteractiveVoteBo
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="quiz"></param>
        /// <returns></returns>
        public static ErrorMapping.ErrorCodes InsertInteractiveVote(InteractiveVoteEntity quiz)
        {
            try
            {
                quiz.CreatedBy = WcfExtensions.WcfMessageHeader.Current.ClientUsername;
                return InteractiveVoteDal.InsertInteractiveVote(quiz)
                    ? ErrorMapping.ErrorCodes.Success
                    : ErrorMapping.ErrorCodes.BusinessError;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Fatal, ex.ToString());
                return ErrorMapping.ErrorCodes.Exception;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="quiz"></param>
        /// <returns></returns>
        public static ErrorMapping.ErrorCodes UpdateInteractiveVote(InteractiveVoteEntity quiz)
        {
            try
            {
                quiz.LastModifiedBy = WcfExtensions.WcfMessageHeader.Current.ClientUsername;
                return InteractiveVoteDal.UpdateInteractiveVote(quiz)
                    ? ErrorMapping.ErrorCodes.Success
                    : ErrorMapping.ErrorCodes.BusinessError;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Fatal, ex.ToString());
                return ErrorMapping.ErrorCodes.Exception;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <param name="interactiveData"></param>
        /// <param name="interactiveConfig"></param>
        /// <returns></returns>
        public static ErrorMapping.ErrorCodes UpdateInteractiveVoteData(int id, string interactiveData)
        {
            try
            {
                return InteractiveVoteDal.UpdateInteractiveVoteData(id, interactiveData, WcfExtensions.WcfMessageHeader.Current.ClientUsername)
                    ? ErrorMapping.ErrorCodes.Success
                    : ErrorMapping.ErrorCodes.BusinessError;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Fatal, ex.ToString());
                return ErrorMapping.ErrorCodes.Exception;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <param name="interactiveData"></param>
        /// <param name="interactiveConfig"></param>
        /// <returns></returns>
        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public static InteractiveVoteEntity GetInteractiveVoteById(int id)
        {
            try
            {
                return InteractiveVoteDal.GetInteractiveVoteById(id);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Fatal, ex.ToString());
                return null;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="keyword"></param>
        /// <param name="type"></param>
        /// <param name="createdBy"></param>
        /// <param name="status"></param>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <param name="totalRow"></param>
        /// <returns></returns>
        public static List<InteractiveVoteEntity> SearchInteractiveVote(string keyword, EnumInteractiveVoteType type, string createdBy, int status, int siteId, int pageIndex, int pageSize, ref int totalRow)
        {
            try
            {
                return InteractiveVoteDal.SearchInteractiveVote(keyword, (int)type, createdBy, status, siteId, pageIndex, pageSize, ref totalRow);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Fatal, ex.ToString());
                return new List<InteractiveVoteEntity>();
            }
        }
    }
}
