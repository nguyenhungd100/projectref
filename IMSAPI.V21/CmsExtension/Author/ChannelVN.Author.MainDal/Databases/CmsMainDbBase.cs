﻿using System.Data;
using ChannelVN.CMS.Common.DalCommon;
using ChannelVN.Author.MainDal.Databases;

namespace ChannelVN.Author.MainDal.Databases
{
    /// <summary>
    /// The base class for the <see cref="CmsMainDb"/> class that 
    /// represents a connection to the <c>CmsMainDb</c> database. 
    /// </summary>
    /// <remarks>
    /// Do not change this source code. Modify the CmsMainDb class
    /// if you need to add or change some functionality.
    /// </remarks>
    public abstract class CmsMainDbBase : MainDbBase
    {
        #region Store procedures

        #region NewsContent
        private AuthorDal _authorContentMainDal;
        public AuthorDalBase AuthorContentMainDal
        {
            get { return _authorContentMainDal ?? (_authorContentMainDal = new AuthorDal((CmsMainDb)this)); }
        }
        #endregion

        #endregion

        #region Constructors

        protected CmsMainDbBase()
        {
        }
        protected CmsMainDbBase(bool init)
        {
            if (init)
            {
                InitConnection();
            }
        }

        #endregion

        #region Protected members

        protected override sealed void InitConnection()
        {
            _connection = CreateConnection();
            _connection.Open();
        }

        /// <summary>
        /// Creates a new connection to the database.
        /// </summary>
        /// <returns>A reference to the <see cref="System.Data.IDbConnection"/> object.</returns>
        protected abstract IDbConnection CreateConnection();

        #endregion
    }
}