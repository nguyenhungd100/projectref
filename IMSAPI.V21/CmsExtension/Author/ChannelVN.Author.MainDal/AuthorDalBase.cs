﻿using ChannelVN.Author.Entity;
using ChannelVN.Author.MainDal.Databases;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChannelVN.Author.MainDal
{
    public abstract class AuthorDalBase
    {
        /// <summary>
        /// Các hàm xử lý cho Author
        /// </summary>
        /// <returns></returns>
        /// 
        public List<AuthorEntity> SelectListAuthor()
        {
            const string commandText = "CMS_Author_GetAllData";
            try
            {
                List<AuthorEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                data = _db.GetList<AuthorEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public AuthorEntity SelectAuthorById(int Id)
        {
            const string commandText = "CMS_Author_GetDataById";
            try
            {
                AuthorEntity data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "AuthorId", Id);
                data = _db.Get<AuthorEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public bool InsertAuthor(NewsAuthorEntity info)
        {
            const string commandText = "CMS_NewsAuthor_Insert";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "NewsId", info.NewsId);
                _db.AddParameter(cmd, "AuthorId", info.AuthorId);
                _db.AddParameter(cmd, "AuthorName", info.AuthorName);
                _db.AddParameter(cmd, "Note", info.Note);

                var numberOfRow = cmd.ExecuteNonQuery();

                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public bool UpdateAuthor(NewsAuthorEntity info)
        {
            const string commandText = "CMS_NewsAuthor_Update";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "NewsId", info.NewsId);
                _db.AddParameter(cmd, "AuthorId", info.AuthorId);
                _db.AddParameter(cmd, "AuthorName", info.AuthorName);
                _db.AddParameter(cmd, "Note", info.Note);

                var numberOfRow = cmd.ExecuteNonQuery();

                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        
        /// <summary>
        /// Chuyển đổi DAL cho Author
        /// minhduongvan
        /// 2014/01/06
        /// </summary>

        #region Constructor

        private readonly CmsMainDb _db;

        protected AuthorDalBase(CmsMainDb db)
        {
            _db = db;
        }

        protected CmsMainDb Database
        {
            get { return _db; }
        }

        #endregion
    }
}
