﻿using ChannelVN.CMS.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace ChannelVN.Author.Entity
{
    [DataContract]
    public class NewsAuthorEntity : EntityBase
    {
        [DataMember]
        public long NewsId { get; set; }
        [DataMember]
        public int AuthorId { get; set; }
        [DataMember]
        public string AuthorName { get; set; }
        [DataMember]
        public string Note { get; set; }
    }
}
