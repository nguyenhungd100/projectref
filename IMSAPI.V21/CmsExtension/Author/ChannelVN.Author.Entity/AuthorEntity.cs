﻿using ChannelVN.CMS.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace ChannelVN.Author.Entity
{
    [DataContract]
    public class AuthorEntity:EntityBase
    {
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public string UserData { get; set; }
        [DataMember]
        public string AuthorName { get; set; }
        [DataMember]
        public int AuthorType { get; set; }
        [DataMember]
        public string OriginalName { get; set; }
        [DataMember]
        public string UnsignName { get; set; }
        [DataMember]
        public string Avatar { get; set; }
        [DataMember]
        public int NewsRoyaltiesRoleId { get; set; }
    }
}
