﻿using ChannelVN.Author.Dal.Common;
using ChannelVN.Author.Entity;
using ChannelVN.Author.MainDal.Databases;
using ChannelVN.CMS.Common;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace ChannelVN.Author.Dal
{
    public class AuthorDal
    {
        public static List<AuthorEntity> SelectListAuthor()
        {
            List<AuthorEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.AuthorContentMainDal.SelectListAuthor();
            }
            return returnValue;
        }

        public static AuthorEntity SelectAuthorById(int Id)
        {
            AuthorEntity returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.AuthorContentMainDal.SelectAuthorById(Id);
            }
            return returnValue;
        }

        public static bool InsertAuthor(NewsAuthorEntity info)
        {
            bool returnValue = false;
            using (var db = new CmsMainDb())
            {
                returnValue = db.AuthorContentMainDal.InsertAuthor(info);
            }
            return returnValue;
        }

        public static bool UpdateAuthor(NewsAuthorEntity info)
        {
            bool returnValue = false;
            using (var db = new CmsMainDb())
            {
                returnValue = db.AuthorContentMainDal.UpdateAuthor(info);
            }
            return returnValue;
        }
    }
}
