﻿using ChannelVN.Author.Dal;
using ChannelVN.Author.Entity;
using ChannelVN.Author.Entity.ErrorCode;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ChannelVN.Author.Bo
{
   public  class AuthorBo
    {
       public static List<AuthorEntity> SelectListAuthor() {
           return AuthorDal.SelectListAuthor();
       }

       public static AuthorEntity SelectAuthorById(int authorId)
       {
           return AuthorDal.SelectAuthorById(authorId);
       }
        
       public static ErrorMapping.ErrorCodes InsertNewsAuthor(NewsAuthorEntity info)
       {
           var result = ErrorMapping.ErrorCodes.BusinessError;
           var createSuccess = AuthorDal.InsertAuthor(info);
           if (createSuccess)
               result = ErrorMapping.ErrorCodes.Success;
           return result;
       }

       public static ErrorMapping.ErrorCodes UpdateNewsAuthor(NewsAuthorEntity info)
       {
           var result = ErrorMapping.ErrorCodes.BusinessError;
           var createSuccess = AuthorDal.UpdateAuthor(info);
           if (createSuccess)
               result = ErrorMapping.ErrorCodes.Success;
           return result;
       }
    }
}
