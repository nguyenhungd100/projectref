﻿using System;
using System.Collections.Generic;
using ChannelVN.InfoGraph.Entity;
using System.Data.SqlClient;
using ChannelVN.CMS.Common;
using System.Data;
using ChannelVN.InfoGraph.Dal.Common;
using ChannelVN.InfoGraph.MainDal.Databases;

namespace ChannelVN.InfoGraph.Dal
{
    public class InfoGraphDal
    {
        #region InfoGraph
        public static List<InfoGraphEntity> List(string keyword, string createdBy, int status, int pageIndex, int pageSize, ref int totalRow)
        {
            var retVal = new List<InfoGraphEntity>();
            using (var db = new CmsMainDb())
            {
                try
                {
                    retVal = db.InfoGraphContentMainDal.List(keyword, createdBy, status, pageIndex, pageSize, ref totalRow);
                }
                catch (Exception ex)
                {
                    Logger.WriteLog(Logger.LogType.Fatal, ex.ToString());
                }
            }
            return retVal;
        }
        public static InfoGraphEntity Select(int id)
        {
            InfoGraphEntity retVal = null;
            using (var db = new CmsMainDb())
            {
                try
                {
                    retVal = db.InfoGraphContentMainDal.Select(id);
                }
                catch (Exception ex)
                {
                    Logger.WriteLog(Logger.LogType.Fatal, ex.ToString());
                }
            }
            return retVal;
        }
        public static bool Insert(InfoGraphEntity elm, ref int id)
        {
            bool retVal = false;
            using (var db = new CmsMainDb())
            {
                try
                {
                    retVal = db.InfoGraphContentMainDal.Insert(elm, ref id);
                }
                catch (Exception ex)
                {
                    Logger.WriteLog(Logger.LogType.Fatal, ex.ToString());
                }
            }
            return retVal;
        }
        public static bool Update(InfoGraphEntity elm)
        {
            var retVal = false;
            using (var db = new CmsMainDb())
            {
                try
                {
                    retVal = db.InfoGraphContentMainDal.Update(elm);
                }
                catch (Exception ex)
                {
                    Logger.WriteLog(Logger.LogType.Fatal, ex.ToString());
                }
            }
            return retVal;
        }
        public static bool Delete(int id)
        {
            bool retVal = false;
            using (var db = new CmsMainDb())
            {
                retVal = db.InfoGraphContentMainDal.Delete(id);
            }
            return retVal;
        }
        #endregion

        #region InfoGraph Chart
        public static bool ChartInsert(InfoGraphChartEntity elm, ref int id)
        {
            bool retVal = false;
            using (var db = new CmsMainDb())
            {
                try
                {
                    retVal = db.InfoGraphContentMainDal.ChartInsert(elm, ref id);
                }
                catch (Exception ex)
                {
                    Logger.WriteLog(Logger.LogType.Fatal, ex.ToString());
                }
            }
            return retVal;
        }
        public static InfoGraphChartEntity ChartSelect(string name, int type)
        {
            InfoGraphChartEntity retVal = null;
            using (var db = new CmsMainDb())
            {
                try
                {
                    retVal = db.InfoGraphContentMainDal.ChartSelect(name, type);
                }
                catch (Exception ex)
                {
                    Logger.WriteLog(Logger.LogType.Fatal, ex.ToString());
                }
            }
            return retVal;
        }
        #endregion

        #region InfoGraph Folder
        public static List<InfoGraphFolderEntity> ListFolder(string keyword, string createdBy, int visible, int pub, int type, int pageIndex, int pageSize, ref int totalRow)
        {
            List<InfoGraphFolderEntity> retVal = null;
            using (var db = new CmsMainDb())
            {
                retVal = db.InfoGraphContentMainDal.ListFolder(keyword, createdBy, visible, pub, type, pageIndex, pageSize, ref totalRow);
            }
            return retVal;
        }
        public static bool InsertFolder(InfoGraphFolderEntity elm, ref int id)
        {
            bool retVal = false;
            using (var db = new CmsMainDb())
            {
                retVal = db.InfoGraphContentMainDal.InsertFolder(elm, ref id);
            }
            return retVal;
        }
        public static bool UpdateFolder(InfoGraphFolderEntity elm)
        {
            bool retVal = false;
            using (var db = new CmsMainDb())
            {
                retVal = db.InfoGraphContentMainDal.UpdateFolder(elm);
            }
            return retVal;
        }
        public static bool DeleteFolder(int id)
        {
            bool retVal = false;
            using (var db = new CmsMainDb())
            {
                retVal = db.InfoGraphContentMainDal.DeleteFolder(id);
            }
            return retVal;
        }
        public static bool InsertUsage(int folderId, string username, ref int id)
        {
            const string storeProcedure = "CMS_InfoGraph_Usage_Insert";
            try
            {
                id = 0;
                var parameters = new[]
                    {
                        new SqlParameter("@Username", username),
                        new SqlParameter("@FolderId", folderId),        
                        new SqlParameter("@Id", id) {Direction = ParameterDirection.Output}
                    };

                SqlHelperParameterCache.CacheParameterSet(DbCommon.GetConnectionString(DbCommon.Connection.CmsMainDb),
                                                          DbCommon.DatabaseSchema + storeProcedure,
                                                          parameters);

                var numberOfRow = SqlHelper.ExecuteNonQuery(DbCommon.GetConnectionString(DbCommon.Connection.CmsMainDb),
                                                            CommandType.StoredProcedure,
                                                            DbCommon.DatabaseSchema + storeProcedure,
                                                            parameters);

                id = int.Parse(parameters[parameters.Length - 1].Value.ToString());

                return numberOfRow > 0 && id > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format(DbCommon.DatabaseSchema + "{0}:{1}", storeProcedure, ex.Message));
            }
        }
        public static bool DeleteUsage(int folderId, string username)
        {
            const string storeProcedure = "CMS_InfoGraph_Usage_Delete";
            try
            {

                var parameters = new[]
                    {   
                        new SqlParameter("@Username", username),
                        new SqlParameter("@FolderId", folderId)
                    };

                SqlHelperParameterCache.CacheParameterSet(DbCommon.GetConnectionString(DbCommon.Connection.CmsMainDb),
                                                          DbCommon.DatabaseSchema + storeProcedure,
                                                          parameters);

                var numberOfRow = SqlHelper.ExecuteNonQuery(DbCommon.GetConnectionString(DbCommon.Connection.CmsMainDb),
                                                            CommandType.StoredProcedure,
                                                            DbCommon.DatabaseSchema + storeProcedure,
                                                            parameters);

                return numberOfRow > 0 && folderId > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format(DbCommon.DatabaseSchema + "{0}:{1}", storeProcedure, ex.Message));
            }
        }
        #endregion

        #region InfoGraph Image
        public static List<InfoGraphImageEntity> ListImage(string keyword, int folderId, int pageIndex, int pageSize, ref int totalRow)
        {
            List<InfoGraphImageEntity> retVal = null;
            using (var db = new CmsMainDb())
            {
                retVal = db.InfoGraphContentMainDal.ListImage(keyword, folderId, pageIndex, pageSize, ref totalRow);
            }
            return retVal;
        }
        public static InfoGraphImageEntity SelectImage(int id)
        {
            InfoGraphImageEntity retVal = null;
            using (var db = new CmsMainDb())
            {
                retVal = db.InfoGraphContentMainDal.SelectImage(id);
            }
            return retVal;
        }
        public static bool InsertImage(InfoGraphImageEntity elm, ref int id)
        {
            bool retVal = false;
            using (var db = new CmsMainDb())
            {
                retVal = db.InfoGraphContentMainDal.InsertImage(elm, ref id);
            }
            return retVal;
        }
        public static bool UpdateImage(InfoGraphImageEntity elm)
        {
            bool retVal = false;
            using (var db = new CmsMainDb())
            {
                retVal = db.InfoGraphContentMainDal.UpdateImage(elm);
            }
            return retVal;
        }
        public static bool DeleteImage(int id)
        {
            bool retVal = false;
            using (var db = new CmsMainDb())
            {
                retVal = db.InfoGraphContentMainDal.DeleteImage(id);
            }
            return retVal;
        }
        #endregion

        #region InfoGraph Svg
        public static List<InfoGraphSvgEntity> ListSvg(string keyword, int folderId, int pageIndex, int pageSize, ref int totalRow)
        {
            List<InfoGraphSvgEntity> retVal = null;
            using (var db = new InfoGraphDb())
            {
                try
                {
                    retVal = db.InfoGraphContentMainDal.ListSvg(keyword, folderId, pageIndex, pageSize, ref totalRow);
                }
                catch (Exception ex)
                {
                    Logger.WriteLog(Logger.LogType.Fatal, ex.ToString());
                }
            }
            return retVal;
        }
        public static InfoGraphSvgEntity SelectSvg(int id)
        {
            InfoGraphSvgEntity retVal = null;
            using (var db = new InfoGraphDb())
            {
                try
                { 
                    retVal = db.InfoGraphContentMainDal.SelectSvg(id);
                }
                catch (Exception ex)
                {
                    Logger.WriteLog(Logger.LogType.Fatal, ex.ToString());
                }
            }
            return retVal;
        }
        public static bool UpdateSvg(InfoGraphSvgEntity elm)
        {
            bool retVal = false;
            using (var db = new InfoGraphDb())
            {
                try
                {
                    retVal = db.InfoGraphContentMainDal.UpdateSvg(elm);
                }
                catch (Exception ex)
                {
                    Logger.WriteLog(Logger.LogType.Fatal, ex.ToString());
                }
            }
            return retVal;
        }
        public static bool DeleteSvg(int id)
        {
            bool retVal = false;
            using (var db = new InfoGraphDb())
            {
                try
                {
                    retVal = db.InfoGraphContentMainDal.DeleteSvg(id);
                }
                catch (Exception ex)
                {
                    Logger.WriteLog(Logger.LogType.Fatal, ex.ToString());
                }
            }
            return retVal;
        }
        public static List<InfoGraphSvgFolderEntity> SvgFolder(bool showAll, string channelName, string keyWord)
        {
            List<InfoGraphSvgFolderEntity> retVal = new List<InfoGraphSvgFolderEntity>();
            using (var db = new InfoGraphDb())
            {
                try
                {
                    retVal = db.InfoGraphContentMainDal.FolderSvg(showAll, channelName, keyWord);
                }
                catch (Exception ex)
                {
                    Logger.WriteLog(Logger.LogType.Fatal, ex.ToString());
                }
            }
            return retVal;
        }
        public static bool InsertSvgChannel(string channelName, int svgFolderId)
        {
            bool retVal = false;
            using (var db = new InfoGraphDb())
            {
                try
                {
                    retVal = db.InfoGraphContentMainDal.InsertSvgChannel(channelName, svgFolderId);
                }
                catch (Exception ex)
                {
                    Logger.WriteLog(Logger.LogType.Fatal, ex.ToString());
                }
            }
            return retVal;
        }
        public static bool DeleteSvgChannel(string channelName)
        {
            bool retVal = false;
            using (var db = new InfoGraphDb())
            {
                try
                {
                    retVal = db.InfoGraphContentMainDal.DeleteSvgChannel(channelName);
                }
                catch (Exception ex)
                {
                    Logger.WriteLog(Logger.LogType.Fatal, ex.ToString());
                }
            }
            return retVal;
        }
        #endregion

        #region InfoGraph Template

        public static List<InfoGraphTemplateEntity> ListTemplate(string keyword, int folderId, int pageIndex, int pageSize, ref int totalRow)
        {
            List<InfoGraphTemplateEntity> retVal = null;
            using (var db = new CmsMainDb())
            {
                retVal = db.InfoGraphContentMainDal.ListTemplate(keyword, folderId, pageIndex, pageSize, ref totalRow);
            }
            return retVal;
        }
        public static InfoGraphTemplateEntity SelectTemplate(int id)
        {
            InfoGraphTemplateEntity retVal = null;
            using (var db = new CmsMainDb())
            {
                retVal = db.InfoGraphContentMainDal.SelectTemplate(id);
            }
            return retVal;
        }
        public static bool InsertTemplate(InfoGraphTemplateEntity elm, ref int id)
        {
            bool retVal = false;
            using (var db = new CmsMainDb())
            {
                retVal = db.InfoGraphContentMainDal.InsertTemplate(elm, ref id);
            }
            return retVal;
        }
        public static bool UpdateTemplate(InfoGraphTemplateEntity elm)
        {
            bool retVal = false;
            using (var db = new CmsMainDb())
            {
                retVal = db.InfoGraphContentMainDal.UpdateTemplate(elm);
            }
            return retVal;
        }
        public static bool DeleteTemplate(int id)
        {
            bool retVal = false;
            using (var db = new CmsMainDb())
            {
                retVal = db.InfoGraphContentMainDal.DeleteTemplate(id);
            }
            return retVal;
        }
        public static bool InsertTemplateChannel(InfoGraphTemplateChannelEntity elm)
        {
            bool retVal = false;
            using (var db = new InfoGraphDb())
            {
                try
                {
                    retVal = db.InfoGraphContentMainDal.InsertTemplateChannel(elm);
                }
                catch (Exception ex)
                {
                    Logger.WriteLog(Logger.LogType.Fatal, ex.ToString());
                }
            }
            return retVal;
        }
        public static bool UpdateTemplateChannel(InfoGraphTemplateChannelEntity elm)
        {
            bool retVal = false;
            using (var db = new InfoGraphDb())
            {
                try
                {
                    retVal = db.InfoGraphContentMainDal.UpdateTemplateChannel(elm);
                }
                catch (Exception ex)
                {
                    Logger.WriteLog(Logger.LogType.Fatal, ex.ToString());
                }
            }
            return retVal;
        }
        public static List<InfoGraphTemplateChannelEntity> ListTemplateChannel(int status)
        {
            List<InfoGraphTemplateChannelEntity> retVal = null;
            using (var db = new InfoGraphDb())
            {
                try
                {
                    retVal = db.InfoGraphContentMainDal.ListTemplateChannel(status);
                }
                catch (Exception ex)
                {
                    Logger.WriteLog(Logger.LogType.Fatal, ex.ToString());
                }
            }
            return retVal;
        }
        public static InfoGraphTemplateChannelEntity SelectTemplateChannel(int id)
        {
            InfoGraphTemplateChannelEntity retVal = null;
            using (var db = new InfoGraphDb())
            {
                try
                {
                    retVal = db.InfoGraphContentMainDal.SelectTemplateChannel(id);
                }
                catch (Exception ex)
                {
                    Logger.WriteLog(Logger.LogType.Fatal, ex.ToString());
                }
            }
            return retVal;
        }
        #endregion
    }
}
