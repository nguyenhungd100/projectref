﻿using ChannelVN.CMS.Common;
using ChannelVN.InfoGraph.Dal;
using ChannelVN.InfoGraph.Entity;
using ChannelVN.InfoGraph.Entity.ErrorCode;
using System;
using System.Collections.Generic;

namespace ChannelVN.InfoGraph.Bo
{
    public class InfoGraphBo
    {
        #region InfoGraph
        public static List<InfoGraphEntity> ListInfoGraph(string keyword, string createdBy, int status, int pageIndex, int pageSize, ref int totalRow)
        {
            return InfoGraphDal.List(keyword, createdBy, status, pageIndex, pageSize, ref totalRow);
        }
        public static InfoGraphEntity SelectInfoGraph(int id)
        {
            return InfoGraphDal.Select(id);
        }
        public static ErrorMapping.ErrorCodes InsertInfoGraph(InfoGraphEntity elm, ref int id)
        {
            return InfoGraphDal.Insert(elm, ref id) ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.UnknowError;
        }
        public static ErrorMapping.ErrorCodes UpdateInfoGraph(InfoGraphEntity elm)
        {
            return InfoGraphDal.Update(elm) ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.UnknowError;
        }
        public static ErrorMapping.ErrorCodes DeleteInfoGraph(int id)
        {
            return InfoGraphDal.Delete(id) ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.UnknowError;
        }
        #endregion

        #region InfoGraph Chart
        public static ErrorMapping.ErrorCodes InsertInfoGraphChart(InfoGraphChartEntity elm, ref int id)
        {
            return InfoGraphDal.ChartInsert(elm, ref id) ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.UnknowError;
        }
        public static InfoGraphChartEntity SelectInfoGraphChart(string name, int type)
        {
            return InfoGraphDal.ChartSelect(name, type);
        }
        #endregion

        #region InfoGraph Folder
        public static List<InfoGraphFolderEntity> ListInfoGraphFolder(string keyword, string createdBy, int visible, int pub, int type, int pageIndex, int pageSize, ref int totalRow)
        {
            return InfoGraphDal.ListFolder(keyword, createdBy, visible, pub, type, pageIndex, pageSize, ref totalRow);
        }
        public static ErrorMapping.ErrorCodes InsertInfoGraphFolder(InfoGraphFolderEntity elm, ref int id)
        {
            return InfoGraphDal.InsertFolder(elm, ref id) ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.UnknowError;
        }
        public static ErrorMapping.ErrorCodes UpdateInfoGraphFolder(InfoGraphFolderEntity elm)
        {
            return InfoGraphDal.UpdateFolder(elm) ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.UnknowError;
        }
        public static ErrorMapping.ErrorCodes DeleteInfoGraphFolder(int id)
        {
            return InfoGraphDal.DeleteFolder(id) ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.UnknowError;
        }
        public static ErrorMapping.ErrorCodes InsertInfoGraphUsage(int folderId, string username, ref int id)
        {
            return InfoGraphDal.InsertUsage(folderId, username, ref id) ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.UnknowError;
        }
        public static ErrorMapping.ErrorCodes DeleteInfoGraphUsage(int folderId, string username)
        {
            return InfoGraphDal.DeleteUsage(folderId, username) ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.UnknowError;
        }
        #endregion

        #region InfoGraph Image
        public static List<InfoGraphImageEntity> ListInfoGraphImage(string keyword, int folderId, int pageIndex, int pageSize, ref int totalRow)
        {
            return InfoGraphDal.ListImage(keyword, folderId, pageIndex, pageSize, ref totalRow);
        }
        public static InfoGraphImageEntity SelectInfoGraphImage(int id)
        {
            return InfoGraphDal.SelectImage(id);
        }
        public static ErrorMapping.ErrorCodes InsertInfoGraphImage(InfoGraphImageEntity elm, ref int id)
        {
            return InfoGraphDal.InsertImage(elm, ref id) ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.UnknowError;
        }
        public static ErrorMapping.ErrorCodes UpdateInfoGraphImage(InfoGraphImageEntity elm)
        {
            return InfoGraphDal.UpdateImage(elm) ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.UnknowError;
        }
        public static ErrorMapping.ErrorCodes DeleteInfoGraphImage(int id)
        {
            return InfoGraphDal.DeleteImage(id) ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.UnknowError;
        }
        #endregion

        #region InfoGraph Svg
        public static List<InfoGraphSvgEntity> ListInfoGraphSvg(string keyword, int folderId, int pageIndex, int pageSize, ref int totalRow)
        {
            return InfoGraphDal.ListSvg(keyword, folderId, pageIndex, pageSize, ref totalRow);
        }
        public static InfoGraphSvgEntity SelectInfoGraphSvg(int id)
        {
            return InfoGraphDal.SelectSvg(id);
        }
        public static List<InfoGraphSvgFolderEntity> SvgFolder(bool showAll, string channelName, string keyWord)
        {
            try
            {
                return InfoGraphDal.SvgFolder(showAll, channelName, keyWord);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Fatal, ex.ToString());
            }
            return new List<InfoGraphSvgFolderEntity>();
        }
        public static ErrorMapping.ErrorCodes UpdateSvg(InfoGraphSvgEntity elm)
        {
            return InfoGraphDal.UpdateSvg(elm) ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.UnknowError;
        }
        public static ErrorMapping.ErrorCodes InsertSvgChannel(string channelName, int svgFolderId)
        {
            return InfoGraphDal.InsertSvgChannel(channelName, svgFolderId) ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.UnknowError;
        }
        public static ErrorMapping.ErrorCodes DeleteSvgChannel(string channelName)
        {
            return InfoGraphDal.DeleteSvgChannel(channelName) ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.UnknowError;
        }

        #endregion

        #region InfoGraph Template

        public static List<InfoGraphTemplateEntity> ListInfoGraphTemplate(string keyword, int folderId, int pageIndex, int pageSize, ref int totalRow)
        {
            return InfoGraphDal.ListTemplate(keyword, folderId, pageIndex, pageSize, ref totalRow);
        }
        public static InfoGraphTemplateEntity SelectInfoGraphTemplate(int id)
        {
            return InfoGraphDal.SelectTemplate(id);
        }
        public static ErrorMapping.ErrorCodes InsertInfoGraphTemplate(InfoGraphTemplateEntity elm, ref int id)
        {
            return InfoGraphDal.InsertTemplate(elm, ref id) ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.UnknowError;
        }
        public static ErrorMapping.ErrorCodes UpdateInfoGraphTemplate(InfoGraphTemplateEntity elm)
        {
            return InfoGraphDal.UpdateTemplate(elm) ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.UnknowError;
        }
        public static ErrorMapping.ErrorCodes DeleteInfoGraphTemplate(int id)
        {
            return InfoGraphDal.DeleteTemplate(id) ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.UnknowError;
        }
        public static ErrorMapping.ErrorCodes DeleteSvg(int id)
        {
            return InfoGraphDal.DeleteSvg(id) ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.UnknowError;
        }
        public static ErrorMapping.ErrorCodes InsertTemplateChannel(InfoGraphTemplateChannelEntity elm)
        {
            return InfoGraphDal.InsertTemplateChannel(elm) ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.UnknowError;
        }
        public static ErrorMapping.ErrorCodes UpdateTemplateChannel(InfoGraphTemplateChannelEntity elm)
        {
            return InfoGraphDal.UpdateTemplateChannel(elm) ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.UnknowError;
        }
        public static List<InfoGraphTemplateChannelEntity> ListTemplateChannel(int status)
        {
            try
            {
                return InfoGraphDal.ListTemplateChannel(status);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Fatal, ex.ToString());
            }
            return new List<InfoGraphTemplateChannelEntity>();
        }
        public static InfoGraphTemplateChannelEntity SelectTemplateChannel(int id)
        {
            return InfoGraphDal.SelectTemplateChannel(id);
        }
        #endregion
    }
}
