﻿using System;
using System.Runtime.Serialization;
using ChannelVN.CMS.Common;

namespace ChannelVN.InfoGraph.Entity
{
    [DataContract]
    public class InfoGraphFolderEntity : EntityBase
    {
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public string Name { get; set; }
        [DataMember]
        public string CreatedBy { get; set; }
        [DataMember]
        public DateTime CreatedDate { get; set; }
        [DataMember]
        public bool Visible { get; set; }
        [DataMember]
        public bool Public { get; set; }
        [DataMember]
        public int Type { get; set; }
        [DataMember]
        public int TotalItem { get; set; }
    }
}
