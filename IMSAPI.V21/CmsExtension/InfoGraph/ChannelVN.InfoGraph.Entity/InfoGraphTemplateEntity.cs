﻿using System;
using System.Runtime.Serialization;
using ChannelVN.CMS.Common;

namespace ChannelVN.InfoGraph.Entity
{
    [DataContract]
    public class InfoGraphTemplateEntity : EntityBase
    {
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public int FolderId { get; set; }
        [DataMember]
        public string Name { get; set; }
        [DataMember]
        public string Xml { get; set; }
        [DataMember]
        public string Preview { get; set; }
        [DataMember]
        public string Thumb { get; set; }
        [DataMember]
        public int Width { get; set; }
        [DataMember]
        public int Height { get; set; }
        [DataMember]
        public int Type { get; set; }
    }
    [DataContract]
    public class InfoGraphTemplateChannelEntity : EntityBase
    {
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public int FolderId { get; set; }
        [DataMember]
        public string Name { get; set; }
        [DataMember]
        public string XmlContent { get; set; }
        [DataMember]
        public string Preview { get; set; }
        [DataMember]
        public string ChannelName { get; set; }
        [DataMember]
        public string CreatedBy { get; set; }
        [DataMember]
        public DateTime CreatedDate { get; set; }
        [DataMember]
        public bool Status { get; set; }
        [DataMember]
        public bool Type { get; set; }
    }

    public enum EnumInfoGraphTemplateType : int
    {
        All = -1,
        /// <summary>
        /// đối tượng dựng sẵn
        /// </summary>
        CompositeObject = 1,
        /// <summary>
        /// mẫu bài
        /// </summary>
        Template = 2
    }
}
