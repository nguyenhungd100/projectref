﻿using System.Runtime.Serialization;
using ChannelVN.CMS.Common;

namespace ChannelVN.InfoGraph.Entity
{
    [DataContract]
    public class InfoGraphSvgEntity : EntityBase
    {
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public int FolderId { get; set; }
        [DataMember]
        public string Name { get; set; }
        [DataMember]
        public string TagName { get; set; }
        [DataMember]
        public string SvgContent { get; set; }
        [DataMember]
        public int Width { get; set; }
        [DataMember]
        public int Height { get; set; }
        [DataMember]
        public string Color { get; set; }
        //[DataMember]
        //public bool IsMultiColor
        //{
        //    get
        //    {
        //        return Utility.ConvertToString(this.Color).Split(new char[] { ';' }).Length > 0;
        //    }
        //}
        [DataMember]
        public string Aspect { get; set; }
    }
    [DataContract]
    public class InfoGraphSvgFolderEntity : EntityBase
    {
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public string Name { get; set; }
        [DataMember]
        public int TotalSvg { get; set; }
        [DataMember]
        public int Selected { get; set; }
    }
}
