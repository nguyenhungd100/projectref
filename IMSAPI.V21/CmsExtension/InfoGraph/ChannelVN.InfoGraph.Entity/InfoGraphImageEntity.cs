﻿using System.Runtime.Serialization;
using ChannelVN.CMS.Common;

namespace ChannelVN.InfoGraph.Entity
{
    [DataContract]
    public class InfoGraphImageEntity : EntityBase
    {
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public int FolderId { get; set; }
        [DataMember]
        public string Name { get; set; }
        [DataMember]
        public string Url { get; set; }
        [DataMember]
        public int Width { get; set; }
        [DataMember]
        public int Height { get; set; }
        [DataMember]
        public string Color { get; set; }
        [DataMember]
        public string Aspect { get; set; }
        [DataMember]
        public string TagName { get; set; }
    }
}
