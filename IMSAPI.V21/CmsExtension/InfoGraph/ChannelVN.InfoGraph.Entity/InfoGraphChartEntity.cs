﻿using System.Runtime.Serialization;
using ChannelVN.CMS.Common;

namespace ChannelVN.InfoGraph.Entity
{
    [DataContract]
    public class InfoGraphChartEntity : EntityBase
    {
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public string Name { get; set; }
        [DataMember]
        public int Type { get; set; }
        [DataMember]
        public int Width { get; set; }
        [DataMember]
        public int Height { get; set; }
        [DataMember]
        public string VerTitle { get; set; }
        [DataMember]
        public string HorTitle { get; set; }
        [DataMember]
        public string Data { get; set; }
        [DataMember]
        public string DataX { get; set; }
        [DataMember]
        public string Options { get; set; }
        [DataMember]
        public string TypeName { get; set; }
    }
}
