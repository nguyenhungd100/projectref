﻿using System;
using System.Runtime.Serialization;
using ChannelVN.CMS.Common;

namespace ChannelVN.InfoGraph.Entity
{
   [DataContract]
    public class InfoGraphEntity : EntityBase
    {
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public string Name { get; set; }
        [DataMember]
        public string Description { get; set; }
        [DataMember]
        public string Avatar { get; set; }
        [DataMember]
        public string XmlContent { get; set; }
        [DataMember]
        public string ImgPath { get; set; }
        [DataMember]
        public string CreatedBy { get; set; }
        [DataMember]
        public DateTime CreatedDate { get; set; }
        [DataMember]
        public string LastUpdateBy { get; set; }
        [DataMember]
        public DateTime LastUpdateDate { get; set; }
        [DataMember]
        public int Status { get; set; } 
    }
}
