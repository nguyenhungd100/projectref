﻿using System.Data;
using ChannelVN.CMS.Common.DalCommon;
using ChannelVN.InfoGraph.MainDal.Databases;
using ChannelVN.InfoGraph.MainDal;

namespace ChannelVN.InfoGraph.MainDal.Databases
{
    /// <summary>
    /// The base class for the <see cref="InfoGraphDb"/> class that 
    /// represents a connection to the <c>InfoGraphDb</c> database. 
    /// </summary>
    /// <remarks>
    /// Do not change this source code. Modify the InfoGraphDb class
    /// if you need to add or change some functionality.
    /// </remarks>
    public abstract class InfoGraphDbBase : MainDbBase
    {
        #region Store procedures
        private InfoGraphSvgDal _infoGraphContentMainDal;
        public InfoGraphSvgDalBase InfoGraphContentMainDal
        {
            get { return _infoGraphContentMainDal ?? (_infoGraphContentMainDal = new InfoGraphSvgDal((InfoGraphDb)this)); }
        }
        #endregion

        #region Constructors

        protected InfoGraphDbBase()
        {
        }
        protected InfoGraphDbBase(bool init)
        {
            if (init)
            {
                InitConnection();
            }
        }

        #endregion

        #region Protected members

        protected override sealed void InitConnection()
        {
            _connection = CreateConnection();
            _connection.Open();
        }

        /// <summary>
        /// Creates a new connection to the database.
        /// </summary>
        /// <returns>A reference to the <see cref="System.Data.IDbConnection"/> object.</returns>
        protected abstract IDbConnection CreateConnection();

        #endregion
    }
}