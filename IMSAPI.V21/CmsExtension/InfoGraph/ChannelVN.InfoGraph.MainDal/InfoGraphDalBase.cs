﻿using ChannelVN.CMS.Common;
using ChannelVN.InfoGraph.Entity;
using ChannelVN.InfoGraph.MainDal.Databases;
using System;
using System.Collections.Generic;

namespace ChannelVN.InfoGraph.MainDal
{
    public abstract class InfoGraphDalBase
    {
        #region InfoGraph
        public List<InfoGraphEntity> List(string keyword, string createdBy, int status, int pageIndex, int pageSize, ref int totalRow)
        {
            const string commandText = "CMS_InfoGraph_List";
            try
            {
                List<InfoGraphEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "TotalRow", totalRow, System.Data.ParameterDirection.Output);
                _db.AddParameter(cmd, "Keyword", keyword);
                _db.AddParameter(cmd, "CreatedBy", createdBy);
                _db.AddParameter(cmd, "Status", status);
                _db.AddParameter(cmd, "PageIndex", pageIndex);
                _db.AddParameter(cmd, "PageSize", pageSize);
                data = _db.GetList<InfoGraphEntity>(cmd);
                totalRow = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 0));
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public InfoGraphEntity Select(int id)
        {
            const string storeProcedure = "CMS_InfoGraph_Select";
            try
            {
                InfoGraphEntity data = null;
                var cmd = _db.CreateCommand(storeProcedure, true);
                _db.AddParameter(cmd, "Id", id);
                data = _db.Get<InfoGraphEntity>(cmd);
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", storeProcedure, ex.Message));
            }
        }
        public bool Insert(InfoGraphEntity elm, ref int id)
        {
            const string storeProcedure = "CMS_InfoGraph_Insert";

            try
            {
                id = 0;
                var cmd = _db.CreateCommand(storeProcedure, true);
                _db.AddParameter(cmd, "Id", id, System.Data.ParameterDirection.Output);
                _db.AddParameter(cmd, "Name", elm.Name);
                _db.AddParameter(cmd, "Description", elm.Description);
                _db.AddParameter(cmd, "Avatar", elm.Avatar);
                _db.AddParameter(cmd, "XmlContent", elm.XmlContent);
                _db.AddParameter(cmd, "ImgPath", elm.ImgPath);
                _db.AddParameter(cmd, "CreatedBy", elm.CreatedBy);
                _db.AddParameter(cmd, "CreatedDate", elm.CreatedDate);
                _db.AddParameter(cmd, "Status", elm.Status);
                var numberOfRow = cmd.ExecuteNonQuery();
                id = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 0));
                return numberOfRow > 0 && id > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", storeProcedure, ex.Message));
            }
        }
        public bool Update(InfoGraphEntity elm)
        {
            const string storeProcedure = "CMS_InfoGraph_Update";

            try
            {
                var cmd = _db.CreateCommand(storeProcedure, true);
                _db.AddParameter(cmd, "Id", elm.Id);
                _db.AddParameter(cmd, "Name", elm.Name);
                _db.AddParameter(cmd, "Description", elm.Description);
                _db.AddParameter(cmd, "XmlContent", elm.XmlContent);
                _db.AddParameter(cmd, "ImgPath", elm.ImgPath);
                _db.AddParameter(cmd, "Avatar", elm.Avatar);
                _db.AddParameter(cmd, "LastUpdateBy", elm.LastUpdateBy);
                _db.AddParameter(cmd, "LastUpdateDate", elm.LastUpdateDate);
                _db.AddParameter(cmd, "Status", elm.Status);
                var numberOfRow = cmd.ExecuteNonQuery();
                var id = elm.Id;
                return numberOfRow > 0 && id > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", storeProcedure, ex.Message));
            }
        }
        public bool Delete(int id)
        {
            const string storeProcedure = "CMS_InfoGraph_Delete";

            try
            {
                var cmd = _db.CreateCommand(storeProcedure, true);
                _db.AddParameter(cmd, "Id", id);
                var numberOfRow = cmd.ExecuteNonQuery();
                return numberOfRow > 0 && id > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", storeProcedure, ex.Message));
            }
        }
        #endregion

        #region InfoGraph Chart
        public bool ChartInsert(InfoGraphChartEntity elm, ref int id)
        {
            const string storeProcedure = "CMS_InfoGraph_Chart_Insert";

            try
            {
                var cmd = _db.CreateCommand(storeProcedure, true);
                _db.AddParameter(cmd, "Id", id, System.Data.ParameterDirection.Output);
                _db.AddParameter(cmd, "Name", elm.Name);
                _db.AddParameter(cmd, "Type", elm.Type);
                _db.AddParameter(cmd, "Width", elm.Width);
                _db.AddParameter(cmd, "Height", elm.Height);
                _db.AddParameter(cmd, "VerTitle", elm.VerTitle);
                _db.AddParameter(cmd, "HorTitle", elm.HorTitle);
                _db.AddParameter(cmd, "Data", elm.Data);
                _db.AddParameter(cmd, "DataX", elm.DataX);
                _db.AddParameter(cmd, "Options", elm.Options);
                _db.AddParameter(cmd, "TypeName", elm.TypeName);

                var numberOfRow = cmd.ExecuteNonQuery();

                id = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 0));

                return numberOfRow > 0 && id > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", storeProcedure, ex.Message));
            }
        }
        public InfoGraphChartEntity ChartSelect(string name, int type)
        {
            const string storeProcedure = "CMS_InfoGraph_Chart_Select";
            try
            {
                InfoGraphChartEntity data = null;
                var cmd = _db.CreateCommand(storeProcedure, true);
                _db.AddParameter(cmd, "Name", name);
                _db.AddParameter(cmd, "Type", type);

                data = _db.Get<InfoGraphChartEntity>(cmd);
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", storeProcedure, ex.Message));
            }
        }
        #endregion

        #region InfoGraph Folder
        public List<InfoGraphFolderEntity> ListFolder(string keyword, string createdBy, int visible, int pub, int type, int pageIndex, int pageSize, ref int totalRow)
        {
            const string storeProcedure = "CMS_InfoGraph_Folder_List";
            try
            {
                List<InfoGraphFolderEntity> data = null;
                var cmd = _db.CreateCommand(storeProcedure, true);
                _db.AddParameter(cmd, "TotalRow", totalRow, System.Data.ParameterDirection.Output);
                _db.AddParameter(cmd, "Keyword", keyword);
                _db.AddParameter(cmd, "CreatedBy", createdBy);
                _db.AddParameter(cmd, "Visible", visible);
                _db.AddParameter(cmd, "Public", pub);
                _db.AddParameter(cmd, "Type", type);
                _db.AddParameter(cmd, "PageIndex", pageIndex);
                _db.AddParameter(cmd, "PageSize", pageSize);
                data = _db.GetList<InfoGraphFolderEntity>(cmd);
                totalRow = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 0));
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", storeProcedure, ex.Message));
            }
        }
        public bool InsertFolder(InfoGraphFolderEntity elm, ref int id)
        {
            const string storeProcedure = "CMS_InfoGraph_Folder_Insert";
            try
            {
                List<InfoGraphFolderEntity> data = null;
                var cmd = _db.CreateCommand(storeProcedure, true);
                _db.AddParameter(cmd, "Id", id, System.Data.ParameterDirection.Output);
                _db.AddParameter(cmd, "Name", elm.Name);
                _db.AddParameter(cmd, "CreatedBy", elm.CreatedBy);
                _db.AddParameter(cmd, "CreatedDate", elm.CreatedDate);
                _db.AddParameter(cmd, "Public", elm.Public);
                _db.AddParameter(cmd, "Type", elm.Type);

                var numberOfRow = cmd.ExecuteNonQuery();

                id = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 0));
                return numberOfRow > 0 && id > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", storeProcedure, ex.Message));
            }
        }
        public bool UpdateFolder(InfoGraphFolderEntity elm)
        {
            const string storeProcedure = "CMS_InfoGraph_Folder_Update";

            try
            {
                List<InfoGraphFolderEntity> data = null;
                var cmd = _db.CreateCommand(storeProcedure, true);
                _db.AddParameter(cmd, "Id", elm.Id);
                _db.AddParameter(cmd, "Name", elm.Name);
                _db.AddParameter(cmd, "Public", elm.Public);


                var numberOfRow = cmd.ExecuteNonQuery();
                var id = elm.Id;
                return numberOfRow > 0 && id > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", storeProcedure, ex.Message));
            }
        }
        public bool DeleteFolder(int id)
        {
            const string storeProcedure = "CMS_InfoGraph_Folder_Delete";

            try
            {
                List<InfoGraphFolderEntity> data = null;
                var cmd = _db.CreateCommand(storeProcedure, true);
                _db.AddParameter(cmd, "Id", id);

                var numberOfRow = cmd.ExecuteNonQuery();
                return numberOfRow > 0 && id > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", storeProcedure, ex.Message));
            }
        }
        #endregion

        #region InfoGraph Image
        public List<InfoGraphImageEntity> ListImage(string keyword, int folderId, int pageIndex, int pageSize, ref int totalRow)
        {
            const string storeProcedure = "CMS_InfoGraph_Image_List";
            try
            {
                List<InfoGraphImageEntity> data = null;
                var cmd = _db.CreateCommand(storeProcedure, true);
                _db.AddParameter(cmd, "TotalRow", totalRow, System.Data.ParameterDirection.Output);
                _db.AddParameter(cmd, "Keyword", keyword);
                _db.AddParameter(cmd, "FolderId", folderId);
                _db.AddParameter(cmd, "PageIndex", pageIndex);
                _db.AddParameter(cmd, "PageSize", pageSize);
                data = _db.GetList<InfoGraphImageEntity>(cmd);
                totalRow = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 0));
                return data;

            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", storeProcedure, ex.Message));
            }
        }
        public InfoGraphImageEntity SelectImage(int id)
        {
            const string storeProcedure = "CMS_InfoGraph_Image_Select";
            try
            {
                InfoGraphImageEntity data = null;
                var cmd = _db.CreateCommand(storeProcedure, true);
                _db.AddParameter(cmd, "Id", id);
                data = _db.Get<InfoGraphImageEntity>(cmd);
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", storeProcedure, ex.Message));
            }
        }
        public bool InsertImage(InfoGraphImageEntity elm, ref int id)
        {
            const string storeProcedure = "CMS_InfoGraph_Image_Insert";

            try
            {

                var cmd = _db.CreateCommand(storeProcedure, true);
                _db.AddParameter(cmd, "Id", id, System.Data.ParameterDirection.Output);
                _db.AddParameter(cmd, "FolderId", elm.FolderId);
                _db.AddParameter(cmd, "Name", elm.Name);
                _db.AddParameter(cmd, "Url", elm.Url);
                _db.AddParameter(cmd, "Width", elm.Width);
                _db.AddParameter(cmd, "Height", elm.Height);
                _db.AddParameter(cmd, "Color", elm.Color);
                _db.AddParameter(cmd, "Aspect", elm.Aspect);
                _db.AddParameter(cmd, "TagName", elm.TagName);

                var numberOfRow = cmd.ExecuteNonQuery();
                id = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 0));

                return numberOfRow > 0 && id > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", storeProcedure, ex.Message));
            }
        }
        public bool UpdateImage(InfoGraphImageEntity elm)
        {
            const string storeProcedure = "CMS_InfoGraph_Image_Update";

            try
            {
                var cmd = _db.CreateCommand(storeProcedure, true);
                _db.AddParameter(cmd, "Id", elm.Id);
                _db.AddParameter(cmd, "Name", elm.Name);
                var numberOfRow = cmd.ExecuteNonQuery();
                var id = elm.Id;
                return numberOfRow > 0 && id > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", storeProcedure, ex.Message));
            }
        }
        public bool DeleteImage(int id)
        {
            const string storeProcedure = "CMS_InfoGraph_Image_Delete";

            try
            {
                var cmd = _db.CreateCommand(storeProcedure, true);
                _db.AddParameter(cmd, "Id", id);
                var numberOfRow = cmd.ExecuteNonQuery();

                return numberOfRow > 0 && id > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", storeProcedure, ex.Message));
            }
        }
        #endregion

        #region InfoGraph Svg
        public List<InfoGraphSvgEntity> ListSvg(string keyword, int folderId, int pageIndex, int pageSize, ref int totalRow)
        {
            const string storeProcedure = "CMS_InfoGraph_Svg_List";
            try
            {
                List<InfoGraphSvgEntity> data = null;
                var cmd = _db.CreateCommand(storeProcedure, true);
                _db.AddParameter(cmd, "TotalRow", totalRow, System.Data.ParameterDirection.Output);
                _db.AddParameter(cmd, "Keyword", keyword);
                _db.AddParameter(cmd, "FolderId", folderId);
                _db.AddParameter(cmd, "PageIndex", pageIndex);
                _db.AddParameter(cmd, "PageSize", pageSize);
                data = _db.GetList<InfoGraphSvgEntity>(cmd);
                totalRow = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 0));
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", storeProcedure, ex.Message));
            }
        }
        public InfoGraphSvgEntity SelectSvg(int id)
        {
            const string storeProcedure = "CMS_InfoGraph_Svg_Select";
            try
            {
                InfoGraphSvgEntity data = null;
                var cmd = _db.CreateCommand(storeProcedure, true);
                _db.AddParameter(cmd, "Id", id);
                data = _db.Get<InfoGraphSvgEntity>(cmd);
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", storeProcedure, ex.Message));
            }
        }
        #endregion

        #region InfoGraph Template

        public List<InfoGraphTemplateEntity> ListTemplate(string keyword, int folderId, int pageIndex, int pageSize, ref int totalRow)
        {
            const string storeProcedure = "CMS_InfoGraph_Template_List";
            try
            {
                List<InfoGraphTemplateEntity> data = null;
                var cmd = _db.CreateCommand(storeProcedure, true);
                _db.AddParameter(cmd, "TotalRow", totalRow, System.Data.ParameterDirection.Output);
                _db.AddParameter(cmd, "Keyword", keyword);
                _db.AddParameter(cmd, "FolderId", folderId);                
                _db.AddParameter(cmd, "PageIndex", pageIndex);
                _db.AddParameter(cmd, "PageSize", pageSize);

                data = _db.GetList<InfoGraphTemplateEntity>(cmd);
                totalRow = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 0));
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", storeProcedure, ex.Message));
            }
        }
        public InfoGraphTemplateEntity SelectTemplate(int id)
        {
            const string storeProcedure = "CMS_InfoGraph_Template_Select";
            try
            {
                InfoGraphTemplateEntity data = null;
                var cmd = _db.CreateCommand(storeProcedure, true);
                _db.AddParameter(cmd, "Id", id);
                data = _db.Get<InfoGraphTemplateEntity>(cmd);
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", storeProcedure, ex.Message));
            }
        }
        public bool InsertTemplate(InfoGraphTemplateEntity elm, ref int id)
        {
            const string storeProcedure = "CMS_InfoGraph_Template_Insert";

            try
            {
                var cmd = _db.CreateCommand(storeProcedure, true);
                _db.AddParameter(cmd, "Id", id, System.Data.ParameterDirection.Output);
                _db.AddParameter(cmd, "FolderId", elm.FolderId);
                _db.AddParameter(cmd, "Name", elm.Name);
                _db.AddParameter(cmd, "Xml", elm.Xml);
                _db.AddParameter(cmd, "Preview", elm.Preview);
                _db.AddParameter(cmd, "Thumb", elm.Thumb);
                _db.AddParameter(cmd, "Width", elm.Width);
                _db.AddParameter(cmd, "Height", elm.Height);
                //_db.AddParameter(cmd, "Type", elm.Type);

                var numberOfRow = cmd.ExecuteNonQuery();
                id = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 0));
                return numberOfRow > 0 && id > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", storeProcedure, ex.Message));
            }
        }
        public bool UpdateTemplate(InfoGraphTemplateEntity elm)
        {
            const string storeProcedure = "CMS_InfoGraph_Template_Update";

            try
            {
                var cmd = _db.CreateCommand(storeProcedure, true);
                _db.AddParameter(cmd, "Id", elm.Id);
                //_db.AddParameter(cmd, "FolderId", elm.FolderId);
                _db.AddParameter(cmd, "Name", elm.Name);
                _db.AddParameter(cmd, "Xml", elm.Xml);
                _db.AddParameter(cmd, "Preview", elm.Preview);
                _db.AddParameter(cmd, "Thumb", elm.Thumb);
                _db.AddParameter(cmd, "Width", elm.Width);
                _db.AddParameter(cmd, "Height", elm.Height);
                //_db.AddParameter(cmd, "Type", elm.Type);
                var numberOfRow = cmd.ExecuteNonQuery();
                var id = elm.Id;
                return numberOfRow > 0 && id > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", storeProcedure, ex.Message));
            }
        }
        public bool DeleteTemplate(int id)
        {
            const string storeProcedure = "CMS_InfoGraph_Template_Delete";

            try
            {
                var cmd = _db.CreateCommand(storeProcedure, true);
                _db.AddParameter(cmd, "Id", id);
                var numberOfRow = cmd.ExecuteNonQuery();
                return numberOfRow > 0 && id > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", storeProcedure, ex.Message));
            }
        }
        #endregion

        /// <summary>
        /// Chuyển đổi DAL cho InfoGraphDalBase
        /// minhduongvan
        /// 2014/01/12
        /// </summary>

        #region Constructor

        private readonly CmsMainDb _db;

        protected InfoGraphDalBase(CmsMainDb db)
        {
            _db = db;
        }

        protected CmsMainDb Database
        {
            get { return _db; }
        }

        #endregion
    }
}
