﻿using ChannelVN.CMS.Common;
using ChannelVN.InfoGraph.Entity;
using ChannelVN.InfoGraph.MainDal.Databases;
using System;
using System.Collections.Generic;

namespace ChannelVN.InfoGraph.MainDal
{
    public abstract class InfoGraphSvgDalBase
    {
        #region SVG
        public List<InfoGraphSvgFolderEntity> FolderSvg(bool showAll, string channelName, string keyWord)
        {
            const string storeProcedure = "InfoGraph_Folder_Svg";
            try
            {
                List<InfoGraphSvgFolderEntity> data = null;
                var cmd = _db.CreateCommand(storeProcedure, true);
                _db.AddParameter(cmd, "ShowAll", showAll);
                _db.AddParameter(cmd, "ChannelName", channelName);
                _db.AddParameter(cmd, "KeyWord", keyWord);
                data = _db.GetList<InfoGraphSvgFolderEntity>(cmd);
                return data;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Fatal, ex.Message);
                throw new Exception(string.Format("{0}:{1}", storeProcedure, ex.Message));
            }
        }
        public List<InfoGraphSvgEntity> ListSvg(string keyword, int folderId, int pageIndex, int pageSize, ref int totalRow)
        {
            const string storeProcedure = "InfoGraph_List_Svg";
            try
            {
                var cmd = _db.CreateCommand(storeProcedure, true);
                _db.AddParameter(cmd, "TotalRow", totalRow, System.Data.ParameterDirection.Output);
                _db.AddParameter(cmd, "Keyword", keyword);
                _db.AddParameter(cmd, "FolderId", folderId);
                _db.AddParameter(cmd, "PageIndex", pageIndex);
                _db.AddParameter(cmd, "PageSize", pageSize);
                var data = _db.GetList<InfoGraphSvgEntity>(cmd);
                totalRow = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 0));
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", storeProcedure, ex.Message));
            }
        }
        public InfoGraphSvgEntity SelectSvg(int id)
        {
            const string storeProcedure = "InfoGraph_Select_Svg";
            try
            {
                InfoGraphSvgEntity data = null;
                var cmd = _db.CreateCommand(storeProcedure, true);
                _db.AddParameter(cmd, "Id", id);
                data = _db.Get<InfoGraphSvgEntity>(cmd);
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", storeProcedure, ex.Message));
            }
        }
        public bool UpdateSvg(InfoGraphSvgEntity elm)
        {
            const string storeProcedure = "CMS_InfoGraph_Svg_Update";
            try
            {
                var cmd = _db.CreateCommand(storeProcedure, true);
                _db.AddParameter(cmd, "Id", elm.Id);
                _db.AddParameter(cmd, "FolderId", elm.FolderId);
                _db.AddParameter(cmd, "Name", elm.Name);
                _db.AddParameter(cmd, "TagName", elm.TagName);
                _db.AddParameter(cmd, "Width", elm.Width);
                _db.AddParameter(cmd, "Height", elm.Height);
                _db.AddParameter(cmd, "Color", elm.Color);
                _db.AddParameter(cmd, "Aspect", elm.Aspect);
                _db.AddParameter(cmd, "SvgContent", elm.SvgContent);
                cmd.ExecuteNonQuery();
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", storeProcedure, ex.Message));
            }
        }
        public bool DeleteSvg(int id)
        {
            const string storeProcedure = "InfoGraph_Svg_Delete";
            try
            {
                var cmd = _db.CreateCommand(storeProcedure, true);
                _db.AddParameter(cmd, "Id", id);
                cmd.ExecuteNonQuery();
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", storeProcedure, ex.Message));
            }
        }
        public bool InsertSvgChannel(string channelName, int svgFolderId)
        {
            const string storeProcedure = "InfoGraph_Channel_Svg_Insert";
            try
            {
                var cmd = _db.CreateCommand(storeProcedure, true);
                _db.AddParameter(cmd, "ChannelName", channelName);
                _db.AddParameter(cmd, "SvgFolderId", svgFolderId);
                cmd.ExecuteNonQuery();
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", storeProcedure, ex.Message));
            }
        }
        public bool DeleteSvgChannel(string channelName)
        {
            const string storeProcedure = "InfoGraph_Channel_Svg_Delete";
            try
            {
                var cmd = _db.CreateCommand(storeProcedure, true);
                _db.AddParameter(cmd, "ChannelName", channelName);
                cmd.ExecuteNonQuery();
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", storeProcedure, ex.Message));
            }
        }
        #endregion

        #region TEMPLATE
        public bool InsertTemplateChannel(InfoGraphTemplateChannelEntity elm)
        {
            const string storeProcedure = "InfoGraph_Channel_Template_Insert";
            try
            {
                var cmd = _db.CreateCommand(storeProcedure, true);
                _db.AddParameter(cmd, "FolderId", elm.FolderId);
                _db.AddParameter(cmd, "Name", elm.Name);
                _db.AddParameter(cmd, "XmlContent", elm.XmlContent);
                _db.AddParameter(cmd, "Preview", elm.Preview);
                _db.AddParameter(cmd, "ChannelName", elm.ChannelName);
                _db.AddParameter(cmd, "CreatedBy", elm.CreatedBy);
                _db.AddParameter(cmd, "CreatedDate", elm.CreatedDate);
                _db.AddParameter(cmd, "Status", elm.Status);
                cmd.ExecuteNonQuery();
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", storeProcedure, ex.Message));
            }
        }
        public bool UpdateTemplateChannel(InfoGraphTemplateChannelEntity elm)
        {
            const string storeProcedure = "InfoGraph_Channel_Template_Update";
            try
            {
                var cmd = _db.CreateCommand(storeProcedure, true);
                _db.AddParameter(cmd, "Id", elm.Id);
                _db.AddParameter(cmd, "FolderId", elm.FolderId);
                _db.AddParameter(cmd, "Name", elm.Name);
                _db.AddParameter(cmd, "Status", elm.Status);
                cmd.ExecuteNonQuery();
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", storeProcedure, ex.Message));
            }
        }
        public List<InfoGraphTemplateChannelEntity> ListTemplateChannel(int status)
        {
            const string storeProcedure = "InfoGraph_Channel_Template_List";
            try
            {
                var cmd = _db.CreateCommand(storeProcedure, true);
                _db.AddParameter(cmd, "Status", status);
                var data = _db.GetList<InfoGraphTemplateChannelEntity>(cmd);
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", storeProcedure, ex.Message));
            }
        }
        public InfoGraphTemplateChannelEntity SelectTemplateChannel(int id)
        {
            const string storeProcedure = "InfoGraph_Channel_Template_Select";
            try
            {
                var cmd = _db.CreateCommand(storeProcedure, true);
                _db.AddParameter(cmd, "Id", id);
                var data = _db.Get<InfoGraphTemplateChannelEntity>(cmd);
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", storeProcedure, ex.Message));
            }
        }
        #endregion
        
        #region Constructor

        private readonly InfoGraphDb _db;

        protected InfoGraphSvgDalBase(InfoGraphDb db)
        {
            _db = db;
        }

        protected InfoGraphDb Database
        {
            get { return _db; }
        }

        #endregion
    }
}
