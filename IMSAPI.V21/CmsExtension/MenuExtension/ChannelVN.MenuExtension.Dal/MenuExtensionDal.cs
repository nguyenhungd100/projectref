﻿using ChannelVN.MenuExtension.Entity;
using ChannelVN.MenuExtension.MainDal.Databases;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChannelVN.MenuExtension.Dal
{
    public class MenuExtensionDal
    {
        public static bool InsertTag(int menuId, int GroupId, int ZoneId, string tags)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.MenuExtensionDal.InsertTag(menuId, GroupId, ZoneId, tags);
            }
            return returnValue;
        }
        public static bool Insert(MenuExtensionEntity MenuExtension, string listTag, ref int menuExtensionId)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.MenuExtensionDal.Insert(MenuExtension, listTag, ref menuExtensionId);
            }
            return returnValue;
        }
        public static bool Update(MenuExtensionEntity MenuExtension, string listTag)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.MenuExtensionDal.Update(MenuExtension, listTag);
            }
            return returnValue;
        }
        public static bool Update_Status(int MenuExtensionId, int status)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.MenuExtensionDal.MenuExtension_Update_Status(MenuExtensionId, status);
            }
            return returnValue;
        }
        public static bool Delete(int MenuExtensionId)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.MenuExtensionDal.Delete(MenuExtensionId);
            }
            return returnValue;
        }
        public static MenuExtensionEntity GetMenuExtensionById(int MenuExtensionId)
        {
            MenuExtensionEntity returnValue = null;
            using (var db = new CmsMainDb())
            {
                returnValue = db.MenuExtensionDal.GetMenuExtensionById(MenuExtensionId);
            }
            return returnValue;
        }
        public static List<MenuExtensionEntity> Search(int ZoneId, int status, int GroupId, int ParentMenuId, bool IsTag)
        {
            List<MenuExtensionEntity> returnValue = null;
            using (var db = new CmsMainDb())
            {
                returnValue = db.MenuExtensionDal.Search(ZoneId, status, GroupId, ParentMenuId, IsTag);
            }
            return returnValue;
        }
    }
}
