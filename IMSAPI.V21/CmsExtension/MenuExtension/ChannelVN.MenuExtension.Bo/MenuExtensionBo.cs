﻿using ChannelVN.MenuExtension.Entity;
using ChannelVN.MenuExtension.Dal;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ChannelVN.MenuExtension.Entity.ErrorCode;
using ChannelVN.CMS.Common;

namespace ChannelVN.MenuExtension.Bo
{
    public class MenuExtensionBo
    {
        public static ErrorMapping.ErrorCodes InsertTag(int menuId, int GroupId, int ZoneId, string tags)
        {
            try
            {
                var returnvalue = MenuExtensionDal.InsertTag(menuId, GroupId, ZoneId, tags);
                return returnvalue
                           ? ErrorMapping.ErrorCodes.Success
                           : ErrorMapping.ErrorCodes.BusinessError;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return ErrorMapping.ErrorCodes.BusinessError;
            }
        }
        public static ErrorMapping.ErrorCodes InsertMenuExtension(MenuExtensionEntity entity, string listTag, ref int menuExtensionId)
        {
            try
            {
                var returnvalue = MenuExtensionDal.Insert(entity, listTag, ref menuExtensionId);
                return returnvalue
                           ? ErrorMapping.ErrorCodes.Success
                           : ErrorMapping.ErrorCodes.BusinessError;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return ErrorMapping.ErrorCodes.BusinessError;
            }
        }
        public static List<MenuExtensionEntity> BindAllOfZoneToTreeviewFullDepth(List<MenuExtensionEntity> allZones, string prefix)
        {

            allZones.OrderBy(item => item.ParentMenuId);
            var outputZones = new List<MenuExtensionEntity>();
            BindAllOfZoneToTreeviewFullDepth(allZones.ToList(), 0, prefix, ref outputZones);
            return outputZones;
        }
        private static void BindAllOfZoneToTreeviewFullDepth(List<MenuExtensionEntity> allZones, int currentParentId, string prefix, ref List<MenuExtensionEntity> outputZones)
        {
            if (allZones == null || allZones.Count <= 0) return;

            var currentPrefix = prefix;
            if (currentParentId <= 0)
            {
                currentPrefix = "";
            }
            var count = allZones.Count;
            for (var i = 0; i < count; i++)
            {
                if (allZones[i].ParentMenuId == currentParentId)
                {
                    allZones[i].Name = currentPrefix + " " + allZones[i].Name;
                    outputZones.Add(allZones[i]);
                    BindAllOfZoneToTreeviewFullDepth(allZones, allZones[i].Id, currentPrefix + prefix, ref outputZones);
                }
            }
        }
        public static List<MenuExtensionEntity> GetAllMenuExtensionWithTreeView(bool getParentZoneOnly)
        {
            try
            {
                if (getParentZoneOnly)
                {
                    return BindAllOfZoneToTreeviewFullDepth(Search(0,-1,-1,-1,false), "--");
                }
                else
                {
                    return BindAllOfZoneToTreeviewFullDepth(Search(0, -1, -1, -1, false),"--");
                }

            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return new List<MenuExtensionEntity>();
            }
        }
        public static ErrorMapping.ErrorCodes UpdateMenuExtension(MenuExtensionEntity entity, string listTag)
        {
            try
            {
                return MenuExtensionDal.Update(entity, listTag)
                        ? ErrorMapping.ErrorCodes.Success
                        : ErrorMapping.ErrorCodes.BusinessError;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return ErrorMapping.ErrorCodes.BusinessError;
            }
            
        }
      
        public static ErrorMapping.ErrorCodes MenuExtension_UpdateStatus(int Id, int Status)
        {
            try
            {
                return MenuExtensionDal.Update_Status(Id, Status)
                       ? ErrorMapping.ErrorCodes.Success
                       : ErrorMapping.ErrorCodes.BusinessError;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return ErrorMapping.ErrorCodes.BusinessError;
            }
        }
      
        public static ErrorMapping.ErrorCodes DeleteMenuExtension(int Id)
        {
            try
            {
                return MenuExtensionDal.Delete(Id)
                       ? ErrorMapping.ErrorCodes.Success
                       : ErrorMapping.ErrorCodes.BusinessError;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return ErrorMapping.ErrorCodes.BusinessError;
            }
        }
        public static List<MenuExtensionEntity> Search(int ZoneId, int status, int GroupId, int ParentMenuId, bool IsTag)
        {
            try
            {
                var Data = MenuExtensionDal.Search(ZoneId, status, GroupId, ParentMenuId, IsTag);
                if (IsTag)
                    return Data;
                return BindAllOfZoneToTreeviewFullDepth(Data, "--");
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return new List<MenuExtensionEntity>();
            }
        }
        public static MenuExtensionEntity GetMenuExtensionById(int Id)
        {
            try
            {
                return MenuExtensionDal.GetMenuExtensionById(Id);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return null;
            }
        }
    }
}
