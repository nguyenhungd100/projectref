﻿using ChannelVN.MenuExtension.Entity;
using ChannelVN.MenuExtension.MainDal.Databases;
using ChannelVN.CMS.Common;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChannelVN.MenuExtension.MainDal
{
    public abstract class MenuExtensionDalBase
    {
        public bool Insert(MenuExtensionEntity MenuExtension, string listTag, ref int menuExtensionId)
        {
            const string commandText = "CMS_MenuExtension_Insert";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", 0, ParameterDirection.Output);
                _db.AddParameter(cmd, "Avatar", MenuExtension.Avatar);
                _db.AddParameter(cmd, "GroupMenuId", MenuExtension.GroupMenuId);
                _db.AddParameter(cmd, "Name", MenuExtension.Name);
                _db.AddParameter(cmd, "ParentMenuId", MenuExtension.ParentMenuId);
                _db.AddParameter(cmd, "ParentZoneId", MenuExtension.ParentZoneId);
                _db.AddParameter(cmd, "Priority", MenuExtension.Priority);
                _db.AddParameter(cmd, "Url", MenuExtension.Url);
                _db.AddParameter(cmd, "ZoneId", MenuExtension.ZoneId);
                _db.AddParameter(cmd, "Status", MenuExtension.Status);
                _db.AddParameter(cmd, "listTag", listTag);
                var numberOfRow = _db.ExecuteNonQuery(cmd);
                menuExtensionId = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 0));
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool InsertTag(int menuId, int GroupId, int ZoneId, string tags)
        {
            const string commandText = "CMS_MenuExtension_InsertTags";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "MenuId", menuId);
                _db.AddParameter(cmd, "GroupId", GroupId);
                _db.AddParameter(cmd, "ZoneId", ZoneId);
                _db.AddParameter(cmd, "Tags", tags);
                var numberOfRow = _db.ExecuteNonQuery(cmd);
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool Update(MenuExtensionEntity MenuExtension, string listTag)
        {
            const string commandText = "CMS_MenuExtension_Update";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", MenuExtension.Id);
                _db.AddParameter(cmd, "Avatar", MenuExtension.Avatar);
                _db.AddParameter(cmd, "GroupMenuId", MenuExtension.GroupMenuId);
                _db.AddParameter(cmd, "Name", MenuExtension.Name);
                _db.AddParameter(cmd, "ParentMenuId", MenuExtension.ParentMenuId);
                _db.AddParameter(cmd, "ParentZoneId", MenuExtension.ParentZoneId);
                _db.AddParameter(cmd, "Priority", MenuExtension.Priority);
                _db.AddParameter(cmd, "Url", MenuExtension.Url);
                _db.AddParameter(cmd, "ZoneId", MenuExtension.ZoneId);
                _db.AddParameter(cmd, "Status", MenuExtension.Status);
                _db.AddParameter(cmd, "listTag", listTag);
                var numberOfRow = _db.ExecuteNonQuery(cmd);
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool Delete(int MenuExtensionId)
        {
            const string commandText = "CMS_MenuExtension_Delete";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", MenuExtensionId);
                var numberOfRow = _db.ExecuteNonQuery(cmd);
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool MenuExtension_Update_Status(int MenuExtensionId, int status)
        {
            const string commandText = "CMS_MenuExtension_Update_Status";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", MenuExtensionId);
                _db.AddParameter(cmd, "status", status);
                var numberOfRow = _db.ExecuteNonQuery(cmd);
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
      
        public MenuExtensionEntity GetMenuExtensionById(int id)
        {
            const string commandText = "CMS_MenuExtension_GetbyId";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", id);
                var numberOfRow = _db.Get<MenuExtensionEntity>(cmd);
                return numberOfRow;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public List<MenuExtensionEntity> Search(int ZoneId,  int status, int GroupId, int ParentMenuId, bool IsTag)
        {
            const string commandText = "CMS_MenuExtension_Search";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "ZoneId", ZoneId);
                _db.AddParameter(cmd, "ParentMenuId", ParentMenuId);
                _db.AddParameter(cmd, "IsTag", IsTag);
                _db.AddParameter(cmd, "Status", status);
                _db.AddParameter(cmd, "GroupId", GroupId);
                var numberOfRow = _db.GetList<MenuExtensionEntity>(cmd);
                return numberOfRow;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        } 
        #region Core members

        private readonly CmsMainDb _db;

        protected MenuExtensionDalBase(CmsMainDb db)
        {
            _db = db;
        }

        protected CmsMainDb Database
        {
            get { return _db; }
        }

        #endregion

    }
}
