﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace ChannelVN.MenuExtension.Entity
{
    public class MenuExtensionEntity
    {
        public int Id { get; set; }
        public int ParentMenuId { get; set; }
        public int GroupMenuId { get; set; }
        public int ZoneId { get; set; }
        public int ParentZoneId { get; set; }
        public string Name { get; set; }
        public string Avatar { get; set; }
        public string Url { get; set; }
        public int Priority { get; set; }
        public int Status { get; set; }
        public long TagId { get; set; }
    }
    public class MenuExtensionUseTagEntity
    {
        public int Id { get; set; }
        public int MenuExtensionId { get; set; }
        public int TagId { get; set; }
        public string TagName { get; set; }
        public string TagUrl { get; set; }
        public int Status { get; set; }
    }
    public enum MenuExtensionStatus
    {
        /// <summary>
        /// Tất cả các trạng thái
        /// </summary>
        [EnumMember]
        All = -1,
        /// <summary>
        /// khóa
        /// </summary>
        [EnumMember]
        Lock = 0,
        /// <summary>
        /// Xuất bản
        /// </summary>
        [EnumMember]
        Actived = 1
    }
}
