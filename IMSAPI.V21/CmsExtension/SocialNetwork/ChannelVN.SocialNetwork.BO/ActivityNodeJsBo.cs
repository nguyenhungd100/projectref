﻿using ChannelVN.CMS.Common;
using ChannelVN.CMS.Common.ChannelConfig;
using ChannelVN.SocialNetwork.Entity;
using ChannelVN.WcfExtensions;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Runtime.Serialization;
using System.Text;
using System.Reflection;
using System.Web;

namespace ChannelVN.SocialNetwork.BO
{
    public class ActivityNodeJsBo
    {
        public static bool InsertLog(ActivityEntity log)
        {
            try
            {
                var ip = getIP(HttpContext.Current);
                log.Ip = string.IsNullOrEmpty(ip) ? log.Ip : ip;
                log.UserAgent = string.IsNullOrEmpty(HttpContext.Current?.Request?.UserAgent) ? log.UserAgent : HttpContext.Current?.Request?.UserAgent;

                var client = new RestClient();
                var sb = new StringBuilder();
                sb.AppendFormat("channel_id={0}", client.Channel_Id);
                sb.AppendFormat("&object_id={0}", log.ApplicationId);
                sb.AppendFormat("&source_id={0}", log.SourceId);
                sb.AppendFormat("&source_name={0}", log.SourceName);
                sb.AppendFormat("&destination_id={0}", log.DestinationId);
                sb.AppendFormat("&destination_name={0}", log.DestinationName);
                sb.AppendFormat("&created_by={0}", log.OwnerId);
                sb.AppendFormat("&object_type={0}", log.Type);
                sb.AppendFormat("&action_type={0}", log.ActionTypeDetail);
                sb.AppendFormat("&action_text={0}", log.ActionText);
                sb.AppendFormat("&message={0}", log.Message);
                sb.AppendFormat("&description={0}", log.Description);
                sb.AppendFormat("&ip_log={0}", log.Ip);
                sb.AppendFormat("&machine_name={0}", log.UserAgent);
                sb.AppendFormat("&secret_key={0}", client.SecretKey);

                client.PostData = sb.ToString();
                client.ActionName = "add";
                var data = JsonConvert.DeserializeObject<ApiActionResponse>(client.MakeRequest());
                if (!data.Success)
                {
                    Logger.WriteLog(Logger.LogType.Error, "InsertLog=>" + data.Message);
                }

                //push log Huan
                LogActionCenter.PushLog(log);

                return data.Success;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, "CHINHNB InsertLog => " + ex.Message);
                return false;
            }
        }

        private static string getIP(HttpContext c)
        {
            try
            {
                string ips = c?.Request?.ServerVariables.Get("HTTP_X_FORWARDED_FOR");
                if (!string.IsNullOrEmpty(ips))
                {
                    return ips.Split(',')[0];
                }
                return c?.Request?.ServerVariables["REMOTE_ADDR"];
            }
            catch
            {
                try
                {
                    return c?.Request?.ServerVariables["REMOTE_ADDR"] ?? c?.Request?.UserHostAddress;
                }
                catch
                {
                    return c?.Request?.UserHostAddress;
                }
            }
        }

        public static List<ActivityEntity> SearchLog(string keyword, int pageIndex, int pageSize, long applicationId, int actionTypeDetail, int type, string sourceId, DateTime dateFrom, DateTime dateTo, ref int totalRows)
        {
            try
            {
                var client = new RestClient();
                var sb = new StringBuilder();
                sb.AppendFormat("channel_id={0}", client.Channel_Id);
                if (applicationId > 0)
                    sb.AppendFormat("&object_id={0}", applicationId);
                sb.AppendFormat("&object_type={0}", type);
                if(actionTypeDetail >= 0)
                    sb.AppendFormat("&action_type={0}", actionTypeDetail);
                //sb.AppendFormat("&source_id={0}", applicationId);
                sb.AppendFormat("&page_index={0}", pageIndex);
                sb.AppendFormat("&page_size={0}", pageSize);
                if (dateFrom > new DateTime(1980,1,1))
                    sb.AppendFormat("&created_date_from={0}", dateFrom.ToString("yyyy/MM/dd"));
                if (dateTo > new DateTime(1980, 1, 1))
                    sb.AppendFormat("&created_date_to={0}", dateTo.ToString("yyyy/MM/dd"));
                sb.AppendFormat("&secret_key={0}", client.SecretKey);

                if (!string.IsNullOrEmpty(keyword))
                    sb.AppendFormat("&keyword={0}", keyword);
                if (!string.IsNullOrEmpty(sourceId))
                    sb.AppendFormat("&source_id={0}", sourceId);

                client.PostData = sb.ToString();
                client.ActionName = "search";
                Logger.WriteLog(Logger.LogType.Debug, sb.ToString());
                var ParseObject = JsonConvert.DeserializeObject<ResponseActivity>(client.MakeRequest());
                if (ParseObject == null) ParseObject = new ResponseActivity();
                totalRows = 0;
                if (!ParseObject.Success)
                    Logger.WriteLog(Logger.LogType.Error, "SearchLog=>" + ParseObject.Message);

                //Logger.WriteLog(Logger.LogType.Trace, "SearchLog=>" + NewtonJson.Serialize(ParseObject.data));
                var listLog = new List<ActivityEntity>();
                foreach (var item in ParseObject.data)
                {
                    listLog.Add(new ActivityEntity
                    {                        
                        ActionText = item.ActionText,
                        ActionTypeDetail = item.ActionTypeDetail,
                        ApplicationId = item.ApplicationId,
                        CreatedDate = item.CreatedDate,
                        DestinationId = item.DestinationId,
                        DestinationName = item.DestinationName,
                        EncryptId = item.Id,
                        Message = item.Message,
                        SourceId = item.SourceId,
                        SourceName = item.SourceName,
                        Type = item.Type,
                        OwnerId=item.OwnerId,
                        Description=item.Description,
                        Ip=item.Ip,
                        UserAgent=item.UserAgent
                    });
                }                
                //Logger.WriteLog(Logger.LogType.Trace, "SearchLog=>" + NewtonJson.Serialize(listLog));
                return listLog;

            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, "SearchLog=>" + ex.Message);
                return new List<ActivityEntity>();
            }
        }

        public class ApiActionResponse
        {
            [DataMember]
            public bool Success { get; set; }
            [DataMember]
            public int Code { get; set; }
            [DataMember]
            public string Message { get; set; }
            [DataMember]
            public string Data { get; set; }
            public ApiActionResponse()
            {
                Success = false;
                Code = 0;
                Message = "";
                Data = "";
            }
        }
        [DataContract]
        public class ResponseActivity
        {
            [DataMember]
            public bool Success { get; set; }
            [DataMember]
            public int Code { get; set; }
            [DataMember]
            public string Message { get; set; }
            [DataMember]
            public List<ActivityNodeJsEntity> data { get; set; }
            public ResponseActivity()
            {
                Success = false;
                Message = "";
                data = new List<ActivityNodeJsEntity>();
            }
        }
    }
    public enum HttpMethod
    {
        GET,
        POST,
        PUT,
        DELETE
    }
    public class RestClient
    {
        public string EndPoint { get; set; }
        public HttpMethod Method { get; set; }
        public string ContentType { get; set; }
        public string PostData { get; set; }
        public string ActionName { get; set; }
        public string Channel_Id { get; set; }
        public string SecretKey { get; set; }
        public RestClient()
        {
            EndPoint = CmsChannelConfiguration.GetAppSetting(WcfMessageHeader.Current.Namespace, "LogActionNodeJsApi_Url");
            Method = HttpMethod.POST;
            ContentType = "application/x-www-form-urlencoded; charset=utf-8";
            Channel_Id = CmsChannelConfiguration.GetAppSetting(WcfMessageHeader.Current.Namespace, "LogActionNodeJsApi_Id");
            SecretKey = CmsChannelConfiguration.GetAppSetting(WcfMessageHeader.Current.Namespace, "LogActionNodeJsApi_SecretKey");
            PostData = "";
            ActionName = "";
        }
        public string MakeRequest()
        {
            try
            {
                var request = (HttpWebRequest)WebRequest.Create(EndPoint + ActionName);

                request.Method = Method.ToString();
                request.ContentLength = 0;
                request.ContentType = ContentType;
                //Logger.WriteLog(Logger.LogType.Trace, "SearchLog:MakeRequest:PostData=>" + PostData);
                //Logger.WriteLog(Logger.LogType.Trace, "SearchLog:MakeRequest:Channel_Id=>" + Channel_Id);
                //Logger.WriteLog(Logger.LogType.Trace, "SearchLog:MakeRequest:SecretKey=>" + SecretKey);
                //Logger.WriteLog(Logger.LogType.Trace, "SearchLog:MakeRequest:EndPoint=>" + EndPoint);
                if (!string.IsNullOrEmpty(PostData) && Method == HttpMethod.POST)
                {
                    var bytes = UTF8Encoding.UTF8.GetBytes(PostData);
                    request.ContentLength = bytes.Length;

                    using (var writeStream = request.GetRequestStream())
                    {
                        writeStream.Write(bytes, 0, bytes.Length);
                    }
                }

                using (var response = (HttpWebResponse)request.GetResponse())
                {
                    var responseValue = string.Empty;

                    if (response.StatusCode != HttpStatusCode.OK)
                    {
                        var message = String.Format("Request failed. Received HTTP {0}", response.StatusCode);
                        throw new ApplicationException(message);
                    }

                    using (var responseStream = response.GetResponseStream())
                    {
                        if (responseStream != null)
                            using (var reader = new StreamReader(responseStream))
                            {
                                responseValue = reader.ReadToEnd();
                            }
                    }
                    //Logger.WriteLog(Logger.LogType.Trace, "CHINHNB:MakeRequest: =>" + responseValue + " ActionName: "+ ActionName);

                    return responseValue;
                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, "MakeRequest:" + ActionName + " =>" + ex.Message);
                return "";
            }
        }
    }
}