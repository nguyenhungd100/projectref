﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.Entity.Base.News;
using ChannelVN.CMS.Entity.Base.NewsPosition;
using ChannelVN.CMS.Entity.ErrorCode;
using ChannelVN.SocialNetwork.DAL;
using ChannelVN.SocialNetwork.Entity;
using ChannelVN.WcfExtensions;
using ChannelVN.CMS.Common.ChannelConfig;
using ChannelVN.CMS.Entity.Base.AuthenticateLog;
using ChannelVN.CMS.Entity.Base.Security;

namespace ChannelVN.SocialNetwork.BO
{
    public class ActivityBo
    {
        /*#########################################################*/
        /*  Các hành động liên quan tới nghiệp vụ bài viết
        /*  Author: admin   
        /*  Created date: 2017-03-05
        /*#########################################################*/
        public static bool ApiNodeJsEnable { get { return Utility.ConvertToBoolean(CmsChannelConfiguration.GetAppSetting(WcfMessageHeader.Current.Namespace, "LogActionNodeJs_Enable")); } set { } }

        #region LogActivity On News

        /// <summary>
        /// Log hành động thêm bài viết 
        /// </summary>
        /// <param name="newsId"></param>
        /// <param name="modifiedBy"></param>
        /// <param name="newsTitle"></param>
        public static void LogAddNews(long newsId, string modifiedBy, string newsTitle, int status)
        {
            var actionText = "<span class=\"cms_bm_account_name\">{0}</span> thêm bài viết <a href=\"#{1}\" title=\"{2}\" class=\"cms_bm_action_text\" status=\"{3}\">{4}</a>";
            actionText = string.Format(actionText, modifiedBy, newsId, newsTitle, status, Utility.SubWordInString(newsTitle, 10));

            try
            {
                var logObj = new ActivityEntity
                {
                    ActionText = actionText,
                    ApplicationId = newsId,
                    CreatedDate = DateTime.Now,
                    SourceId = modifiedBy,
                    SourceName = modifiedBy,
                    DestinationId = string.Empty,
                    DestinationName = string.Empty,
                    GroupTimeLine = Utility.SetPublishedDate(),
                    Message = newsTitle,
                    OwnerId = modifiedBy,
                    Type = (int)EnumActivityType.News,
                    ActionTypeDetail = (int)NewsActionType.Addnew,
                    Title= newsTitle,
                    Avatar="",
                    Account = modifiedBy
                };
                if (ApiNodeJsEnable)
                {
                    if (!ActivityNodeJsBo.InsertLog(logObj))
                    {
                        var id = ActivityDal.Insert(logObj);
                        if (id > 0)
                        {
                            logObj.Id = id;
                            CMS.BoCached.CmsExtension.SocialNetwork.ActivityDalFactory.AddActivity(logObj);
                            CMS.BoSearch.CmsExtension.SocialNetwork.ActivityDalFactory.AddActivity(logObj);
                        }
                    }
                }
                else
                {
                    var id = ActivityDal.Insert(logObj);
                    if (id > 0)
                    {
                        logObj.Id = id;
                        CMS.BoCached.CmsExtension.SocialNetwork.ActivityDalFactory.AddActivity(logObj);
                        CMS.BoSearch.CmsExtension.SocialNetwork.ActivityDalFactory.AddActivity(logObj);
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
            }
        }

        /// <summary>
        /// Log hành động sửa bài viết 
        /// </summary>
        /// <param name="newsId"></param>
        /// <param name="modifiedBy"></param>
        /// <param name="newsTitle"></param>
        public static void LogUpdateNews(long newsId, string modifiedBy, string newsTitle, int status)
        {
            var actionText = "<span class=\"cms_bm_account_name\">{0}</span> sửa bài viết <a href=\"#{1}\" title=\"{2}\" class=\"cms_bm_action_text\" status=\"{3}\">{4}</a>";
            actionText = string.Format(actionText, modifiedBy, newsId, newsTitle, status, Utility.SubWordInString(newsTitle, 10));

            try
            {
                var logObj = new ActivityEntity
                {
                    ActionText = actionText,
                    ApplicationId = newsId,
                    CreatedDate = DateTime.Now,
                    SourceId = modifiedBy,
                    SourceName = modifiedBy,
                    DestinationId = string.Empty,
                    DestinationName = string.Empty,
                    GroupTimeLine = Utility.SetPublishedDate(),
                    Message = newsTitle,
                    OwnerId = modifiedBy,
                    Type = (int)EnumActivityType.News,
                    ActionTypeDetail = (int)NewsActionType.Update,
                    Title = newsTitle,
                    Avatar = "",
                    Account = modifiedBy
                };
                if (ApiNodeJsEnable)
                {
                    if (!ActivityNodeJsBo.InsertLog(logObj))
                    {
                        var id = ActivityDal.Insert(logObj);
                        if (id > 0)
                        {
                            logObj.Id = id;
                            CMS.BoCached.CmsExtension.SocialNetwork.ActivityDalFactory.AddActivity(logObj);
                            CMS.BoSearch.CmsExtension.SocialNetwork.ActivityDalFactory.AddActivity(logObj);
                        }
                    }
                }
                else
                {
                    var id = ActivityDal.Insert(logObj);
                    if (id > 0)
                    {
                        logObj.Id = id;
                        CMS.BoCached.CmsExtension.SocialNetwork.ActivityDalFactory.AddActivity(logObj);
                        CMS.BoSearch.CmsExtension.SocialNetwork.ActivityDalFactory.AddActivity(logObj);
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
            }
        }

        /// <summary>
        /// Log hành động xuất bản bài viết
        /// </summary>
        /// <param name="newsId"></param>
        /// <param name="modifiedBy"></param>
        /// <param name="newsTitle"></param>
        /// <param name="tagUsers"></param>
        public static void LogPublishNews(long newsId, string modifiedBy, string newsTitle, int status)
        {
            var actionText = "<span class=\"cms_bm_account_name\">{0}</span> xuất bản bài viết <a href=\"#{1}\" class=\"cms_bm_action_text\" title=\"{2}\" status=\"{3}\">{4}</a>";
            actionText = string.Format(actionText, modifiedBy, newsId, newsTitle, status, Utility.SubWordInString(newsTitle, 10));

            try
            {
                var logObj = new ActivityEntity
                {
                    ActionText = actionText,
                    ApplicationId = newsId,
                    CreatedDate = DateTime.Now,
                    SourceId = modifiedBy,
                    SourceName = modifiedBy,
                    DestinationId = string.Empty,
                    DestinationName = string.Empty,
                    GroupTimeLine = Utility.SetPublishedDate(),
                    Message = newsTitle,
                    OwnerId = modifiedBy,
                    Type = (int)EnumActivityType.News,
                    ActionTypeDetail = (int)NewsActionType.Publish,
                    Title = newsTitle,
                    Avatar = "",
                    Account = modifiedBy
                };
                if (ApiNodeJsEnable)
                {
                    if (!ActivityNodeJsBo.InsertLog(logObj))
                        ActivityDal.Insert(logObj);
                }
                else
                    ActivityDal.Insert(logObj);
                //if (newActivityId > 0)
                //{
                //    var discussionTopic = new DiscussionTopicEntity
                //    {
                //        ApplicationId = (int)EnumTopicApplicationId.CommentOnActivity,
                //        ZoneId = 0,
                //        ObjectId = newsId,
                //        ObjectTitle = newsTitle,
                //        ObjectData = actionText,
                //        CreatedBy = WcfMessageHeader.Current.ClientUsername,
                //        CreatedDate = DateTime.Now,
                //        IsPrivate = false,
                //        Status = (int)EnumTopicStatus.Actived
                //    };
                //    DiscussionBo.CreateDiscussionTopic(discussionTopic, new List<string>(), new List<DiscussionAttachmentEntity>());
                //}
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
            }
        }

        /// <summary>
        /// Log hành động xuất bản bài viết
        /// </summary>
        /// <param name="newsId"></param>
        /// <param name="modifiedBy"></param>
        /// <param name="newsTitle"></param>
        /// <param name="tagUsers"></param>
        public static void LogUnPublishNews(long newsId, string modifiedBy, string newsTitle, int status)
        {
            try
            {
                var actionText = "<span class=\"cms_bm_account_name\">{0}</span> gỡ bài viết <a href=\"#{1}\" class=\"cms_bm_action_text\" title=\"{2}\" status=\"{3}\">{4}</a>";
                actionText = string.Format(actionText, modifiedBy, newsId, newsTitle, status, Utility.SubWordInString(newsTitle, 10));

                var logObj = new ActivityEntity
                {
                    ActionText = actionText,
                    ApplicationId = newsId,
                    CreatedDate = DateTime.Now,
                    SourceId = modifiedBy,
                    SourceName = modifiedBy,
                    DestinationId = string.Empty,
                    DestinationName = string.Empty,
                    GroupTimeLine = Utility.SetPublishedDate(),
                    Message = newsTitle,
                    OwnerId = modifiedBy,
                    Type = (int)EnumActivityType.News,
                    ActionTypeDetail = (int)NewsActionType.UnPublish,
                    Title = newsTitle,
                    Avatar = "",
                    Account = modifiedBy,
                    Status=status
                };
                if (ApiNodeJsEnable)
                {
                    if (!ActivityNodeJsBo.InsertLog(logObj))
                        ActivityDal.Insert(logObj);
                }
                else
                    ActivityDal.Insert(logObj);
                //if (newActivityId > 0)
                //{
                //    var discussionTopic = new DiscussionTopicEntity
                //    {
                //        ApplicationId = (int)EnumTopicApplicationId.CommentOnActivity,
                //        ZoneId = 0,
                //        ObjectId = newsId,
                //        ObjectTitle = newsTitle,
                //        ObjectData = actionText,
                //        CreatedBy = WcfMessageHeader.Current.ClientUsername,
                //        CreatedDate = DateTime.Now,
                //        IsPrivate = false,
                //        Status = (int)EnumTopicStatus.Actived
                //    };
                //    DiscussionBo.CreateDiscussionTopic(discussionTopic, new List<string>(), new List<DiscussionAttachmentEntity>());
                //}
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Trace, ex.StackTrace);
            }
        }

        /// <summary>
        /// Log hành động xóa bài viết
        /// </summary>
        /// <param name="newsId"></param>
        /// <param name="modifiedBy"></param>
        /// <param name="newsTitle"></param>
        public static void LogDeleteNews(long newsId, string modifiedBy, string newsTitle, int status)
        {
            try
            {
                var actionText = "<span class=\"cms_bm_account_name\">{0}</span> xóa bài viết <a href=\"#{1}\" class=\"cms_bm_action_text\" title=\"{2}\" status=\"{3}\">{4}</a>";
                actionText = string.Format(actionText, modifiedBy, newsId, newsTitle, status, Utility.SubWordInString(newsTitle, 10));

                var logObj = new ActivityEntity
                {
                    ActionText = actionText,
                    ApplicationId = newsId,
                    CreatedDate = DateTime.Now,
                    SourceId = modifiedBy,
                    SourceName = modifiedBy,
                    DestinationId = string.Empty,
                    DestinationName = string.Empty,
                    GroupTimeLine = Utility.SetPublishedDate(),
                    Message = newsTitle,
                    OwnerId = modifiedBy,
                    Type = (int)EnumActivityType.News,
                    ActionTypeDetail = (int)NewsActionType.Delete,
                    Title = newsTitle,
                    Avatar = "",
                    Account = modifiedBy,
                    Status= status
                };

                if (ApiNodeJsEnable)
                {
                    if (!ActivityNodeJsBo.InsertLog(logObj))
                        ActivityDal.Insert(logObj);
                }
                else
                    ActivityDal.Insert(logObj);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
            }
        }

        /// <summary>
        /// Log hành động nhận bài viết
        /// </summary>
        /// <param name="newsId"></param>
        /// <param name="modifiedBy"></param>
        /// <param name="newsTitle"></param>
        /// <param name="newsStatus"></param>
        public static void LogReceiveNews(long newsId, string modifiedBy, string newsTitle, int newsStatus)
        {
            var actionText = "<span class=\"cms_bm_account_name\">{0}</span> nhận <b>{1}</b> bài viết <a href=\"#{2}\" class=\"cms_bm_action_text\" title=\"{3}\">{4}</a>";
            var actionOnNewsText = "";
            var actionTypeDetail = (int)NewsActionType.ReceiveToEdit;
            switch (newsStatus)
            {
                case (int)NewsStatus.WaitForEdit:
                    actionOnNewsText = "biên tập";
                    actionTypeDetail = (int)NewsActionType.ReceiveToEdit;
                    break;
                case (int)NewsStatus.WaitForPublish:
                    actionOnNewsText = "xuất bản";
                    actionTypeDetail = (int)NewsActionType.ReceiveToPublish;
                    break;
            }
            actionText = string.Format(actionText, modifiedBy, actionOnNewsText, newsId,
                                        //newsTitle.Replace("\"", "&quot;"), 
                                        newsTitle,
                                       Utility.SubWordInString(newsTitle, 10));
            try
            {
                var logObj = new ActivityEntity
                {
                    ActionText = actionText,
                    ApplicationId = newsId,
                    CreatedDate = DateTime.Now,
                    SourceId = modifiedBy,
                    SourceName = modifiedBy,
                    DestinationId = string.Empty,
                    DestinationName = string.Empty,
                    GroupTimeLine = Utility.SetPublishedDate(),
                    Message = newsTitle,
                    OwnerId = modifiedBy,
                    Type = (int)EnumActivityType.News,
                    ActionTypeDetail = actionTypeDetail,
                    Title = newsTitle,
                    Avatar = "",
                    Account = modifiedBy
                };

                if (ApiNodeJsEnable)
                {
                    if (!ActivityNodeJsBo.InsertLog(logObj))
                        ActivityDal.Insert(logObj);
                }
                else
                    ActivityDal.Insert(logObj);
                //if (newActivityId > 0)
                //{
                //    var discussionTopic = new DiscussionTopicEntity
                //    {
                //        ApplicationId = (int)EnumTopicApplicationId.CommentOnActivity,
                //        ZoneId = 0,
                //        ObjectId = newsId,
                //        ObjectTitle = newsTitle,
                //        ObjectData = actionText,
                //        CreatedBy = WcfMessageHeader.Current.ClientUsername,
                //        CreatedDate = DateTime.Now,
                //        IsPrivate = false,
                //        Status = (int)EnumTopicStatus.Actived
                //    };
                //    DiscussionBo.CreateDiscussionTopic(discussionTopic, new List<string>(), new List<DiscussionAttachmentEntity>());
                //}
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
            }
        }

        /// <summary>
        /// Log hành động đổi bài nổi bật
        /// </summary>
        /// <param name="newsId"></param>
        /// <param name="sourceId"></param>
        /// <param name="newsTitle"></param>
        /// <param name="position"></param>
        /// <param name="positionOrder"></param>
        /// <param name="zoneName"></param>
        public static void LogSetNewsPosition(long newsId, string sourceId, string newsTitle, int position, int positionOrder, string zoneName)
        {
            var actionText = "<span class=\"cms_bm_account_name\">{0}</span> đặt bài viết <a href=\"#{1}\" class=\"cms_bm_action_text\" title=\"{2}\">{3}</a> vào vị trí nổi bật <b>{4}</b> vị trí {5}";
            var positionText = "";
            const int actionTypeDetail = (int)NewsActionType.SetNewsPosition;

            if (position == (int)CMS.Entity.External.SohaNews.NewsPosition.NewsPositionType.HighlightHomeFocus ||
                position == (int)CMS.Entity.External.SohaNews.NewsPosition.NewsPositionForZoneSportType.NewsFocusOnHome ||
                position == (int)CMS.Entity.External.GenK.NewsPosition.NewsPositionType.HomeNewsFocus)
            {
                positionText = "trang chủ";
            }
            else
            {
                positionText = "mục " + zoneName;
            }
            try
            {
                actionText = string.Format(actionText, sourceId, newsId, newsTitle, Utility.SubWordInString(newsTitle, 10), positionText, positionOrder);
                var logObj = new ActivityEntity
                {
                    ActionText = actionText,
                    ApplicationId = newsId,
                    CreatedDate = DateTime.Now,
                    SourceId = sourceId,
                    SourceName = sourceId,
                    DestinationId = string.Empty,
                    DestinationName = string.Empty,
                    GroupTimeLine = Utility.SetPublishedDate(),
                    Message = newsTitle,
                    OwnerId = sourceId,
                    Type = (int)EnumActivityType.News,
                    ActionTypeDetail = actionTypeDetail,
                    Title = newsTitle,
                    Avatar = "",
                    Account = sourceId
                };

                if (ApiNodeJsEnable)
                {
                    if (!ActivityNodeJsBo.InsertLog(logObj))
                        ActivityDal.Insert(logObj);
                }
                else
                    ActivityDal.Insert(logObj);

                //if (newActivityId > 0)
                //{
                //    var discussionTopic = new DiscussionTopicEntity
                //    {
                //        ApplicationId = (int)EnumTopicApplicationId.CommentOnActivity,
                //        ZoneId = 0,
                //        ObjectId = newsId,
                //        ObjectTitle = newsTitle,
                //        ObjectData = actionText,
                //        CreatedBy = WcfMessageHeader.Current.ClientUsername,
                //        CreatedDate = DateTime.Now,
                //        IsPrivate = false,
                //        Status = (int)EnumTopicStatus.Actived
                //    };
                //    DiscussionBo.CreateDiscussionTopic(discussionTopic, new List<string>(), new List<DiscussionAttachmentEntity>());
                //}
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
            }
        }

        /// <summary>
        /// Log hành động đổi bài nổi bật
        /// </summary>
        /// <param name="newsId"></param>
        /// <param name="sourceId"></param>
        /// <param name="newsTitle"></param>
        /// <param name="position"></param>
        /// <param name="positionOrder"></param>
        /// <param name="zoneName"></param>
        public static void LogSwapNewsPosition(long newsId, string sourceId, string newsTitle, int position, int positionOrder, string zoneName)
        {
            try
            {
                var actionText = "<span class=\"cms_bm_account_name\">{0}</span> đảo bài viết <a href=\"#{1}\" class=\"cms_bm_action_text\" title=\"{2}\">{3}</a> vào vị trí nổi bật <b>{4}</b> vị trí thứ {5}";
                var positionText = "";
                const int actionTypeDetail = (int)NewsActionType.SwapNewsPosition;

                if (position == (int)CMS.Entity.External.SohaNews.NewsPosition.NewsPositionType.HighlightHomeFocus ||
                    position == (int)CMS.Entity.External.SohaNews.NewsPosition.NewsPositionForZoneSportType.NewsFocusOnHome ||
                    position == (int)CMS.Entity.External.GenK.NewsPosition.NewsPositionType.HomeNewsFocus ||
                position == (int)CMS.Entity.External.DanTri.NewsPosition.NewsPositionType.HomeNewsFocus)
                {
                    positionText = "Trang chủ";
                }
                else if (position == (int)CMS.Entity.External.DanTri.NewsPosition.NewsPositionType.HotDailyEvent)
                {
                    //Dân trí
                    positionText = "Tin tức sự kiện";
                }
                else
                {
                    if (zoneName != "")
                        positionText = zoneName;
                    else
                        positionText = "Type " + position; //Fox: 06/11/2015 Các trường hợp không bắt được text của vị trí, thì vẫn phải lưu type để check
                }

                actionText = string.Format(actionText, sourceId, newsId, newsTitle, Utility.SubWordInString(newsTitle, 10), positionText, positionOrder);
                var logObj = new ActivityEntity
                {
                    ActionText = actionText,
                    ApplicationId = newsId,
                    CreatedDate = DateTime.Now,
                    SourceId = sourceId,
                    SourceName = sourceId,
                    DestinationId = string.Empty,
                    DestinationName = string.Empty,
                    GroupTimeLine = Utility.SetPublishedDate(),
                    Message = positionOrder + " ===> " + newsTitle + " ===> " + positionText,
                    OwnerId = sourceId,
                    Type = (int)EnumActivityType.News,
                    ActionTypeDetail = actionTypeDetail,
                    Title = newsTitle,
                    Avatar = "",
                    Account = sourceId
                };

                if (ApiNodeJsEnable)
                {
                    if (!ActivityNodeJsBo.InsertLog(logObj))
                        ActivityDal.Insert(logObj);
                }
                else
                    ActivityDal.Insert(logObj);                
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
            }
        }


        /// <summary>
        /// Log hành động đổi bài nổi bật
        /// </summary>
        /// <param name="newsId"></param>
        /// <param name="sourceId"></param>
        /// <param name="newsTitle"></param>
        /// <param name="position"></param>
        /// <param name="positionOrder"></param>
        /// <param name="zoneName"></param>
        public static void LogSetNewsPositionOnHomeStream(long newsId, string sourceId, string newsTitle, int type, int sortOrder, bool locked)
        {
            var actionText = "<span class=\"cms_bm_account_name\">{0}</span> đặt bài viết <a href=\"#{1}\" class=\"cms_bm_action_text\" title=\"{2}\">{3}</a> vào vị trí <b>{4}</b> trên Stream <b>{5}</b> (Vị trí ở trạng thái <b>{6}</b>)";
            var positionText = "S" + type;
            var lockState = locked ? "Khóa" : "Tự động";
            const int actionTypeDetail = (int)NewsActionType.SetNewsPosition;

            try
            {
                actionText = string.Format(actionText, sourceId, newsId,
                                             //newsTitle.Replace("\"", "&quot;"),
                                             newsTitle,
                                           Utility.SubWordInString(newsTitle, 10), sortOrder, positionText, lockState);
                var logObj = new ActivityEntity
                {
                    ActionText = actionText,
                    ApplicationId = newsId,
                    CreatedDate = DateTime.Now,
                    SourceId = sourceId,
                    SourceName = sourceId,
                    DestinationId = string.Empty,
                    DestinationName = string.Empty,
                    GroupTimeLine = Utility.SetPublishedDate(),
                    Message = newsTitle,
                    OwnerId = sourceId,
                    Type = (int)EnumActivityType.News,
                    ActionTypeDetail = actionTypeDetail,
                    Title = newsTitle,
                    Avatar = "",
                    Account = sourceId
                };

                if (ApiNodeJsEnable)
                {
                    if (!ActivityNodeJsBo.InsertLog(logObj))
                        ActivityDal.Insert(logObj);
                }
                else
                    ActivityDal.Insert(logObj);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
            }
        }
        /// <summary>
        /// Log hành động đổi bài nổi bật
        /// </summary>
        /// <param name="newsId"></param>
        /// <param name="sourceId"></param>
        /// <param name="newsTitle"></param>
        /// <param name="position"></param>
        /// <param name="positionOrder"></param>
        /// <param name="zoneName"></param>
        public static void LogBombNewsPosition(long newsId, string sourceId, string newsTitle, int position, int positionOrder, string zoneName)
        {
            var actionText = "<span class=\"cms_bm_account_name\">{0}</span> bomb bài viết <a href=\"#{1}\" class=\"cms_bm_action_text\" title=\"{2}\">{3}</a> vào vị trí nổi bật <b>{4}</b> vị trí thứ {5}";
            var positionText = "";
            const int actionTypeDetail = (int)NewsActionType.BombNewsPosition;

            if (position == (int)CMS.Entity.External.SohaNews.NewsPosition.NewsPositionType.HighlightHomeFocus ||
                position == (int)CMS.Entity.External.SohaNews.NewsPosition.NewsPositionForZoneSportType.NewsFocusOnHome ||
                position == (int)CMS.Entity.External.GenK.NewsPosition.NewsPositionType.HomeNewsFocus)
            {
                positionText = "Trang chủ";
            }
            else
            {
                positionText = zoneName;
            }

            try
            {
                actionText = string.Format(actionText, sourceId, newsId,
                                             //newsTitle.Replace("\"", "&quot;"),
                                             newsTitle,
                                           Utility.SubWordInString(newsTitle, 10), positionText, positionOrder);
                var logObj = new ActivityEntity
                {
                    ActionText = actionText,
                    ApplicationId = newsId,
                    CreatedDate = DateTime.Now,
                    SourceId = sourceId,
                    SourceName = sourceId,
                    DestinationId = string.Empty,
                    DestinationName = string.Empty,
                    GroupTimeLine = Utility.SetPublishedDate(),
                    Message = positionOrder + " ===> " + newsTitle + " ===> " + positionText,
                    OwnerId = sourceId,
                    Type = (int)EnumActivityType.News,
                    ActionTypeDetail = actionTypeDetail,
                    Title = newsTitle,
                    Avatar = "",
                    Account = sourceId
                };

                if (ApiNodeJsEnable)
                {
                    if (!ActivityNodeJsBo.InsertLog(logObj))
                        ActivityDal.Insert(logObj);
                }
                else
                    ActivityDal.Insert(logObj);

                //if (newActivityId > 0)
                //{
                //    var discussionTopic = new DiscussionTopicEntity
                //    {
                //        ApplicationId = (int)EnumTopicApplicationId.CommentOnActivity,
                //        ZoneId = 0,
                //        ObjectId = newsId,
                //        ObjectTitle = newsTitle,
                //        ObjectData = actionText,
                //        CreatedBy = WcfMessageHeader.Current.ClientUsername,
                //        CreatedDate = DateTime.Now,
                //        IsPrivate = false,
                //        Status = (int)EnumTopicStatus.Actived
                //    };
                //    DiscussionBo.CreateDiscussionTopic(discussionTopic, new List<string>(), new List<DiscussionAttachmentEntity>());
                //}
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
            }
        }

        /// <summary>
        /// Log hành động gửi bài viết
        /// </summary>
        /// <param name="newsId"></param>
        /// <param name="sourceId"></param>
        /// <param name="destinationId"></param>
        /// <param name="newsTitle"></param>
        /// <param name="newsStatus"></param>
        public static void LogSendNews(long newsId, int newsStatus, string sourceId, string destinationId, string newsTitle)
        {
            var actionText = "<span class=\"cms_bm_account_name\">{0}</span> gửi bài viết <a href=\"#{1}\" class=\"cms_bm_action_text\" title=\"{2}\">{3}</a> lên danh sách <a href=\"#\">{4}</a>";
            var actionSendText = " chờ biên tập";
            var actionTypeDetail = (int)NewsActionType.SendToEdit;
            switch (newsStatus)
            {
                case (int)NewsStatus.WaitForEdit:                
                    actionSendText = " chờ biên tập";
                    actionTypeDetail = (int)NewsActionType.SendToEdit;
                    break;
                case (int)NewsStatus.WaitForPublish:
                    actionSendText = " chờ xuất bản";
                    actionTypeDetail = (int)NewsActionType.SendToPublish;
                    break;
                case (int)NewsStatus.WaitForEditorialBoard:
                    actionSendText = " chờ tổng biên tập duyệt";
                    actionTypeDetail = (int)NewsActionType.SendToEditorialBoard;
                    break;
            }
            actionText = string.Format(actionText, sourceId, newsId, newsTitle, Utility.SubWordInString(newsTitle, 10), actionSendText);
            try
            {
                var logObj = new ActivityEntity
                {
                    ActionText = actionText,
                    ApplicationId = newsId,
                    CreatedDate = DateTime.Now,
                    SourceId = sourceId,
                    SourceName = sourceId,
                    DestinationId = string.Empty,
                    DestinationName = string.Empty,
                    GroupTimeLine = Utility.SetPublishedDate(),
                    Message = newsTitle,
                    OwnerId = destinationId,
                    Type = (int)EnumActivityType.News,
                    ActionTypeDetail = actionTypeDetail,
                    Title = newsTitle,
                    Avatar = "",
                    Account = sourceId
                };

                if (ApiNodeJsEnable)
                {
                    if (!ActivityNodeJsBo.InsertLog(logObj))
                    {
                        var id = ActivityDal.Insert(logObj);
                        if (id > 0)
                        {
                            logObj.Id = id;
                            CMS.BoCached.CmsExtension.SocialNetwork.ActivityDalFactory.AddActivity(logObj);
                            CMS.BoSearch.CmsExtension.SocialNetwork.ActivityDalFactory.AddActivity(logObj);
                        }
                    }
                }
                else {
                    var id = ActivityDal.Insert(logObj);
                    if (id > 0)
                    {
                        logObj.Id = id;
                        CMS.BoCached.CmsExtension.SocialNetwork.ActivityDalFactory.AddActivity(logObj);
                        CMS.BoSearch.CmsExtension.SocialNetwork.ActivityDalFactory.AddActivity(logObj);
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
            }
        }

        /// <summary>
        /// Log hành động trả bài viết
        /// </summary>
        /// <param name="newsId"></param>
        /// <param name="sourceId"></param>
        /// <param name="destinationId"></param>
        /// <param name="newsTitle"></param>
        public static void LogReturnNews(long newsId, string sourceId, string destinationId, string newsTitle, int newsStatus)
        {
            var actionText = "<span class=\"cms_bm_account_name\">{0}</span> trả bài viết <a href=\"#{1}\" class=\"cms_bm_action_text\" title=\"{2}\">{3}</a> cho <a href=\"#\">{4}</a>";

            var actionTypeDetail = (int)NewsActionType.ReturnEditor;
            switch (newsStatus)
            {
                case (int)NewsStatus.ReturnedToReporter:                    
                    actionTypeDetail = (int)NewsActionType.ReturnReporter;
                    break;
                case (int)NewsStatus.ReturnedToEditor:                    
                    actionTypeDetail = (int)NewsActionType.ReturnEditor;
                    break;
                case (int)NewsStatus.ReturnedToEditorialSecretary:
                    actionTypeDetail = (int)NewsActionType.ReturnEditor;
                    break;
                case (int)NewsStatus.ReturnedToMyEditor:
                    actionTypeDetail = (int)NewsActionType.ReturnReporter;
                    break;
            }

            actionText = string.Format(actionText, sourceId, newsId, newsTitle, Utility.SubWordInString(newsTitle, 10), destinationId);
            try
            {
                var logObj = new ActivityEntity
                {
                    ActionText = actionText,
                    ApplicationId = newsId,
                    CreatedDate = DateTime.Now,
                    SourceId = sourceId,
                    SourceName = sourceId,
                    DestinationId = string.Empty,
                    DestinationName = string.Empty,
                    GroupTimeLine = Utility.SetPublishedDate(),
                    Message = newsTitle,
                    OwnerId = destinationId,
                    Type = (int)EnumActivityType.News,
                    ActionTypeDetail = actionTypeDetail,
                    Title = newsTitle,
                    Avatar = "",
                    Account = sourceId
                };

                if (ApiNodeJsEnable)
                {
                    if (!ActivityNodeJsBo.InsertLog(logObj))
                        ActivityDal.Insert(logObj);
                }
                else
                    ActivityDal.Insert(logObj);               
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
            }
        }

        /// <summary>
        /// Log hành động sửa bài viết 
        /// </summary>
        /// <param name="newsId"></param>
        /// <param name="modifiedBy"></param>
        /// <param name="newsTitle"></param>
        public static void LogUpdateNewsByErrorCheck(long newsId, string modifiedBy, string newsTitle, int status)
        {
            var actionText = "<span class=\"cms_bm_account_name\">{0}</span> soát lỗi bài viết <a href=\"#{1}\" title=\"{2}\" class=\"cms_bm_action_text\" status=\"{3}\">{4}</a>";
            actionText = string.Format(actionText, modifiedBy, newsId, newsTitle, status, Utility.SubWordInString(newsTitle, 10));

            try
            {
                var logObj = new ActivityEntity
                {
                    ActionText = actionText,
                    ApplicationId = newsId,
                    CreatedDate = DateTime.Now,
                    SourceId = modifiedBy,
                    SourceName = modifiedBy,
                    DestinationId = string.Empty,
                    DestinationName = string.Empty,
                    GroupTimeLine = Utility.SetPublishedDate(),
                    Message = newsTitle,
                    OwnerId = modifiedBy,
                    Type = (int)EnumActivityType.News,
                    ActionTypeDetail = (int)NewsActionType.ErrorCheck,
                    Title = newsTitle,
                    Avatar = "",
                    Account = modifiedBy
                };
                if (ApiNodeJsEnable)
                {
                    if (!ActivityNodeJsBo.InsertLog(logObj))
                    {
                        var id = ActivityDal.Insert(logObj);
                        if (id > 0)
                        {
                            logObj.Id = id;
                            CMS.BoCached.CmsExtension.SocialNetwork.ActivityDalFactory.AddActivity(logObj);
                            CMS.BoSearch.CmsExtension.SocialNetwork.ActivityDalFactory.AddActivity(logObj);
                        }
                    }
                }
                else
                {
                    var id = ActivityDal.Insert(logObj);
                    if (id > 0)
                    {
                        logObj.Id = id;
                        CMS.BoCached.CmsExtension.SocialNetwork.ActivityDalFactory.AddActivity(logObj);
                        CMS.BoSearch.CmsExtension.SocialNetwork.ActivityDalFactory.AddActivity(logObj);
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
            }
        }

        /// <summary>
        /// Log hành động sửa bài viết 
        /// </summary>
        /// <param name="newsId"></param>
        /// <param name="modifiedBy"></param>
        /// <param name="newsTitle"></param>
        public static void LogUpdateBodyNewsExpert(long newsId, string modifiedBy, string newsTitle, int status, string expertName)
        {
            var actionText = "<span class=\"cms_bm_account_name\">{0}</span> Release version bài viết <a href=\"#{1}\" title=\"{2}\" class=\"cms_bm_action_text\" status=\"{3}\">{4}</a> (version of <span class=\"cms_bm_account_name\">{5}</span>)";
            actionText = string.Format(actionText, modifiedBy, newsId, newsTitle, status, Utility.SubWordInString(newsTitle, 10), expertName);

            try
            {
                var logObj = new ActivityEntity
                {
                    ActionText = actionText,
                    ApplicationId = newsId,
                    CreatedDate = DateTime.Now,
                    SourceId = modifiedBy,
                    SourceName = modifiedBy,
                    DestinationId = string.Empty,
                    DestinationName = string.Empty,
                    GroupTimeLine = Utility.SetPublishedDate(),
                    Message = newsTitle,
                    OwnerId = modifiedBy,
                    Type = (int)EnumActivityType.News,
                    ActionTypeDetail = (int)NewsActionType.Update,
                    Title = newsTitle,
                    Avatar = "",
                    Account = modifiedBy
                };
                if (ApiNodeJsEnable)
                {
                    if (!ActivityNodeJsBo.InsertLog(logObj))
                    {
                        var id = ActivityDal.Insert(logObj);
                        if (id > 0)
                        {
                            logObj.Id = id;
                            CMS.BoCached.CmsExtension.SocialNetwork.ActivityDalFactory.AddActivity(logObj);
                            CMS.BoSearch.CmsExtension.SocialNetwork.ActivityDalFactory.AddActivity(logObj);
                        }
                    }
                }
                else
                {
                    var id = ActivityDal.Insert(logObj);
                    if (id > 0)
                    {
                        logObj.Id = id;
                        CMS.BoCached.CmsExtension.SocialNetwork.ActivityDalFactory.AddActivity(logObj);
                        CMS.BoSearch.CmsExtension.SocialNetwork.ActivityDalFactory.AddActivity(logObj);
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
            }
        }

        public static void LogUpdateVesionNewsOfExpert(long newsId, string expertName, string newsTitle, int status, string version)
        {
            var actionText = "<span class=\"cms_bm_account_name\">{0}</span> Sửa version bài viết <a href=\"#{1}\" title=\"{2}\" class=\"cms_bm_action_text\" status=\"{3}\">{4}</a> (version {5})";
            actionText = string.Format(actionText, expertName, newsId, newsTitle, status, Utility.SubWordInString(newsTitle, 10), version);

            try
            {
                var logObj = new ActivityEntity
                {
                    ActionText = actionText,
                    ApplicationId = newsId,
                    CreatedDate = DateTime.Now,
                    SourceId = expertName,
                    SourceName = expertName,
                    DestinationId = string.Empty,
                    DestinationName = string.Empty,
                    GroupTimeLine = Utility.SetPublishedDate(),
                    Message = newsTitle,
                    OwnerId = expertName,
                    Type = (int)EnumActivityType.News,
                    ActionTypeDetail = (int)NewsActionType.Update,
                    Title = newsTitle,
                    Avatar = "",
                    Account = expertName
                };
                if (ApiNodeJsEnable)
                {
                    if (!ActivityNodeJsBo.InsertLog(logObj))
                    {
                        var id = ActivityDal.Insert(logObj);
                        if (id > 0)
                        {
                            logObj.Id = id;
                            CMS.BoCached.CmsExtension.SocialNetwork.ActivityDalFactory.AddActivity(logObj);
                            CMS.BoSearch.CmsExtension.SocialNetwork.ActivityDalFactory.AddActivity(logObj);
                        }
                    }
                }
                else
                {
                    var id = ActivityDal.Insert(logObj);
                    if (id > 0)
                    {
                        logObj.Id = id;
                        CMS.BoCached.CmsExtension.SocialNetwork.ActivityDalFactory.AddActivity(logObj);
                        CMS.BoSearch.CmsExtension.SocialNetwork.ActivityDalFactory.AddActivity(logObj);
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
            }
        }

        #endregion

        #region LogActivity For Video

        [DataContract]
        public enum VideoActionType
        {
            [EnumMember]
            AllAction = 0,
            [EnumMember]
            UploadVideo = 1,
            [EnumMember]
            AddNewVideo = 2,
            [EnumMember]
            UpdateVideo = 3,
            [EnumMember]
            DeleteVideo = 4,
            [EnumMember]
            SendVideoToWaitToEdit = 5,
            [EnumMember]
            SendVideoToWaitToPublish = 6,
            [EnumMember]
            ReturnVideo = 7,
            [EnumMember]
            PublishVideo = 8,
            [EnumMember]
            UnpublishVideo = 9,
            [EnumMember]
            ChangeVideoMode = 10,
            [EnumMember]
            AddNewVideoFromYoutube = 11,
            [EnumMember]
            UpdateVideoFromYoutube = 12,
            [EnumMember]
            DeleteVideoFromYoutube = 13,
            [EnumMember]
            SendVideoFromYoutube = 14,
            [EnumMember]
            PublishVideoFromYoutube = 15,
        }

        /// <summary>
        /// Log hành động upload video 
        /// </summary>
        /// <param name="videoId"></param>
        /// <param name="modifiedBy"></param>
        /// <param name="videoTitle"></param>
        public static void LogUploadVideo(int videoId, string modifiedBy, string videoTitle)
        {
            var actionText = "<span class=\"cms_bm_account_name\">{0}</span> upload video <a ref=\"video\" href=\"#{1}\" title=\"{2}\" class=\"cms_bm_action_text\">{3}</a>";
            actionText = string.Format(actionText, modifiedBy, videoId, videoTitle.Replace("\"", "&quot;"), Utility.SubWordInString(videoTitle, 10));
            try
            {
                var logObj = new ActivityEntity
                {
                    ActionText = actionText,
                    ApplicationId = videoId,
                    CreatedDate = DateTime.Now,
                    SourceId = modifiedBy,
                    SourceName = modifiedBy,
                    DestinationId = string.Empty,
                    DestinationName = string.Empty,
                    GroupTimeLine = Utility.SetPublishedDate(),
                    Message = videoTitle,
                    OwnerId = modifiedBy,
                    Type = (int)EnumActivityType.Video,
                    ActionTypeDetail = (int)VideoActionType.UploadVideo,
                    Title = videoTitle,
                    Avatar = "",
                };

                if (ApiNodeJsEnable)
                {
                    if (!ActivityNodeJsBo.InsertLog(logObj))
                    {
                        var id = ActivityDal.Insert(logObj);
                        if (id > 0)
                        {
                            logObj.Id = id;
                            CMS.BoCached.CmsExtension.SocialNetwork.ActivityDalFactory.AddActivity(logObj);
                            CMS.BoSearch.CmsExtension.SocialNetwork.ActivityDalFactory.AddActivity(logObj);
                        }
                    }
                }
                else {
                    var id = ActivityDal.Insert(logObj);
                    if (id > 0)
                    {
                        logObj.Id = id;
                        CMS.BoCached.CmsExtension.SocialNetwork.ActivityDalFactory.AddActivity(logObj);
                        CMS.BoSearch.CmsExtension.SocialNetwork.ActivityDalFactory.AddActivity(logObj);
                    }
                }

            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
            }
        }

        /// <summary>
        /// Log thêm mới thông tin video 
        /// </summary>
        /// <param name="videoId"></param>
        /// <param name="modifiedBy"></param>
        /// <param name="videoTitle"></param>
        public static void LogAddNewVideo(int videoId, string modifiedBy, string videoTitle)
        {
            var actionText = "<span class=\"cms_bm_account_name\">{0}</span> thêm mới thông tin video <a ref=\"video\" href=\"#{1}\" title=\"{2}\" class=\"cms_bm_action_text\">{3}</a>";
            actionText = string.Format(actionText, modifiedBy, videoId, videoTitle.Replace("\"", "&quot;"), Utility.SubWordInString(videoTitle, 10));
            try
            {
                var logObj = new ActivityEntity
                {
                    ActionText = actionText,
                    ApplicationId = videoId,
                    CreatedDate = DateTime.Now,
                    SourceId = modifiedBy,
                    SourceName = modifiedBy,
                    DestinationId = string.Empty,
                    DestinationName = string.Empty,
                    GroupTimeLine = Utility.SetPublishedDate(),
                    Message = videoTitle,
                    OwnerId = modifiedBy,
                    Type = (int)EnumActivityType.Video,
                    ActionTypeDetail = (int)VideoActionType.AddNewVideo,
                    Title = videoTitle,
                    Avatar = "",
                };

                if (ApiNodeJsEnable)
                {
                    if (!ActivityNodeJsBo.InsertLog(logObj))
                    {
                        var id = ActivityDal.Insert(logObj);
                        if (id > 0)
                        {
                            logObj.Id = id;
                            CMS.BoCached.CmsExtension.SocialNetwork.ActivityDalFactory.AddActivity(logObj);
                            CMS.BoSearch.CmsExtension.SocialNetwork.ActivityDalFactory.AddActivity(logObj);
                        }
                    }
                }
                else {
                    var id = ActivityDal.Insert(logObj);
                    if (id > 0)
                    {
                        logObj.Id = id;
                        CMS.BoCached.CmsExtension.SocialNetwork.ActivityDalFactory.AddActivity(logObj);
                        CMS.BoSearch.CmsExtension.SocialNetwork.ActivityDalFactory.AddActivity(logObj);
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
            }
        }

        public static void LogAddNewPlayList(int playListId, string modifiedBy, string playListTitle)
        {
            var actionText = "<span class=\"cms_bm_account_name\">{0}</span> thêm mới thông tin playlist <a ref=\"video\" href=\"#{1}\" title=\"{2}\" class=\"cms_bm_action_text\">{3}</a>";
            actionText = string.Format(actionText, modifiedBy, playListId, playListTitle.Replace("\"", "&quot;"), Utility.SubWordInString(playListTitle, 10));
            try
            {
                var logObj = new ActivityEntity
                {
                    ActionText = actionText,
                    ApplicationId = playListId,
                    CreatedDate = DateTime.Now,
                    SourceId = modifiedBy,
                    SourceName = modifiedBy,
                    DestinationId = string.Empty,
                    DestinationName = string.Empty,
                    GroupTimeLine = Utility.SetPublishedDate(),
                    Message = playListTitle,
                    OwnerId = modifiedBy,
                    Type = (int)EnumActivityType.Video,
                    ActionTypeDetail = (int)VideoActionType.AddNewVideo,
                    Title = playListTitle,
                    Avatar = "",
                };

                if (ApiNodeJsEnable)
                {
                    if (!ActivityNodeJsBo.InsertLog(logObj))
                    {
                        var id = ActivityDal.Insert(logObj);
                        if (id > 0)
                        {
                            logObj.Id = id;
                            CMS.BoCached.CmsExtension.SocialNetwork.ActivityDalFactory.AddActivity(logObj);
                            CMS.BoSearch.CmsExtension.SocialNetwork.ActivityDalFactory.AddActivity(logObj);
                        }
                    }
                }
                else {
                    var id = ActivityDal.Insert(logObj);
                    if (id > 0)
                    {
                        logObj.Id = id;
                        CMS.BoCached.CmsExtension.SocialNetwork.ActivityDalFactory.AddActivity(logObj);
                        CMS.BoSearch.CmsExtension.SocialNetwork.ActivityDalFactory.AddActivity(logObj);
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
            }
        }

        /// <summary>
        /// Log hành động sửa thông tin video 
        /// </summary>
        /// <param name="videoId"></param>
        /// <param name="modifiedBy"></param>
        /// <param name="videoTitle"></param>
        public static void LogUpdateVideo(int videoId, string modifiedBy, string videoTitle, string obj = "")
        {
            var actionText = "<span class=\"cms_bm_account_name\" data-log-obj=\"{4}\">{0}</span> sửa thông tin video <a ref=\"video\" href=\"#{1}\" title=\"{2}\" class=\"cms_bm_action_text\">{3}</a>";
            actionText = string.Format(actionText, modifiedBy, videoId, videoTitle.Replace("\"", "&quot;"), Utility.SubWordInString(videoTitle, 10), obj);
            try
            {
                var logObj = new ActivityEntity
                {
                    ActionText = actionText,
                    ApplicationId = videoId,
                    CreatedDate = DateTime.Now,
                    SourceId = modifiedBy,
                    SourceName = modifiedBy,
                    DestinationId = string.Empty,
                    DestinationName = string.Empty,
                    GroupTimeLine = Utility.SetPublishedDate(),
                    Message = videoTitle,
                    OwnerId = modifiedBy,
                    Type = (int)EnumActivityType.Video,
                    ActionTypeDetail = (int)VideoActionType.UpdateVideo,
                    Title = videoTitle,
                    Avatar = "",
                };

                if (ApiNodeJsEnable)
                {
                    if (!ActivityNodeJsBo.InsertLog(logObj))
                        ActivityDal.Insert(logObj);
                }
                else
                    ActivityDal.Insert(logObj);

            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
            }
        }

        /// <summary>
        /// Log hành động xóa video 
        /// </summary>
        /// <param name="videoId"></param>
        /// <param name="modifiedBy"></param>
        /// <param name="videoTitle"></param>
        public static void LogDeleteVideo(int videoId, string modifiedBy, string videoTitle)
        {
            var actionText = "<span class=\"cms_bm_account_name\">{0}</span> xóa thông tin video <a ref=\"video\" href=\"#{1}\" title=\"{2}\" class=\"cms_bm_action_text\">{3}</a>";
            actionText = string.Format(actionText, modifiedBy, videoId, videoTitle.Replace("\"", "&quot;"), Utility.SubWordInString(videoTitle, 10));
            try
            {
                var logObj = new ActivityEntity
                {
                    ActionText = actionText,
                    ApplicationId = videoId,
                    CreatedDate = DateTime.Now,
                    SourceId = modifiedBy,
                    SourceName = modifiedBy,
                    DestinationId = string.Empty,
                    DestinationName = string.Empty,
                    GroupTimeLine = Utility.SetPublishedDate(),
                    Message = videoTitle,
                    OwnerId = modifiedBy,
                    Type = (int)EnumActivityType.Video,
                    ActionTypeDetail = (int)VideoActionType.DeleteVideo,
                    Title = videoTitle,
                    Avatar = "",
                };

                if (ApiNodeJsEnable)
                {
                    if (!ActivityNodeJsBo.InsertLog(logObj))
                        ActivityDal.Insert(logObj);
                }
                else
                    ActivityDal.Insert(logObj);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
            }
        }

        /// <summary>
        /// Log hành động gửi video lên 
        /// </summary>
        /// <param name="videoId"></param>
        /// <param name="modifiedBy"></param>
        /// <param name="videoTitle"></param>
        public static void LogSendVideoToWaitToEdit(int videoId, string modifiedBy, string videoTitle)
        {
            var actionText = "<span class=\"cms_bm_account_name\">{0}</span> gửi video <a ref=\"video\" href=\"#{1}\" title=\"{2}\" class=\"cms_bm_action_text\">{3}</a> chờ biên tập";
            actionText = string.Format(actionText, modifiedBy, videoId, videoTitle.Replace("\"", "&quot;"), Utility.SubWordInString(videoTitle, 10));
            try
            {
                var logObj = new ActivityEntity
                {
                    ActionText = actionText,
                    ApplicationId = videoId,
                    CreatedDate = DateTime.Now,
                    SourceId = modifiedBy,
                    SourceName = modifiedBy,
                    DestinationId = string.Empty,
                    DestinationName = string.Empty,
                    GroupTimeLine = Utility.SetPublishedDate(),
                    Message = videoTitle,
                    OwnerId = modifiedBy,
                    Type = (int)EnumActivityType.Video,
                    ActionTypeDetail = (int)VideoActionType.SendVideoToWaitToEdit,
                    Title = videoTitle,
                    Avatar = "",
                };

                if (ApiNodeJsEnable)
                {
                    if (!ActivityNodeJsBo.InsertLog(logObj))
                        ActivityDal.Insert(logObj);
                }
                else
                    ActivityDal.Insert(logObj);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
            }
        }

        /// <summary>
        /// Log hành động gửi video lên 
        /// </summary>
        /// <param name="videoId"></param>
        /// <param name="modifiedBy"></param>
        /// <param name="videoTitle"></param>
        public static void LogSendVideoToWaitToPublish(int videoId, string modifiedBy, string videoTitle)
        {
            var actionText = "<span class=\"cms_bm_account_name\">{0}</span> gửi video <a ref=\"video\" href=\"#{1}\" title=\"{2}\" class=\"cms_bm_action_text\">{3}</a> chờ xuất bản";
            actionText = string.Format(actionText, modifiedBy, videoId, videoTitle.Replace("\"", "&quot;"), Utility.SubWordInString(videoTitle, 10));
            try
            {
                var logObj = new ActivityEntity
                {
                    ActionText = actionText,
                    ApplicationId = videoId,
                    CreatedDate = DateTime.Now,
                    SourceId = modifiedBy,
                    SourceName = modifiedBy,
                    DestinationId = string.Empty,
                    DestinationName = string.Empty,
                    GroupTimeLine = Utility.SetPublishedDate(),
                    Message = videoTitle,
                    OwnerId = modifiedBy,
                    Type = (int)EnumActivityType.Video,
                    ActionTypeDetail = (int)VideoActionType.SendVideoToWaitToPublish,
                    Title = videoTitle,
                    Avatar = "",
                };

                if (ApiNodeJsEnable)
                {
                    if (!ActivityNodeJsBo.InsertLog(logObj))
                        ActivityDal.Insert(logObj);
                }
                else
                    ActivityDal.Insert(logObj);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
            }
        }

        /// <summary>
        /// Log hành động trả lại video 
        /// </summary>
        /// <param name="videoId"></param>
        /// <param name="modifiedBy"></param>
        /// <param name="videoTitle"></param>
        public static void LogReturnVideo(int videoId, string modifiedBy, string videoTitle)
        {
            var actionText = "<span class=\"cms_bm_account_name\">{0}</span> trả lại video <a ref=\"video\" href=\"#{1}\" title=\"{2}\" class=\"cms_bm_action_text\">{3}</a>";
            actionText = string.Format(actionText, modifiedBy, videoId, videoTitle.Replace("\"", "&quot;"), Utility.SubWordInString(videoTitle, 10));
            try
            {
                var logObj = new ActivityEntity
                {
                    ActionText = actionText,
                    ApplicationId = videoId,
                    CreatedDate = DateTime.Now,
                    SourceId = modifiedBy,
                    SourceName = modifiedBy,
                    DestinationId = string.Empty,
                    DestinationName = string.Empty,
                    GroupTimeLine = Utility.SetPublishedDate(),
                    Message = videoTitle,
                    OwnerId = modifiedBy,
                    Type = (int)EnumActivityType.Video,
                    ActionTypeDetail = (int)VideoActionType.ReturnVideo,
                    Title = videoTitle,
                    Avatar = "",
                };

                if (ApiNodeJsEnable)
                {
                    if (!ActivityNodeJsBo.InsertLog(logObj))
                        ActivityDal.Insert(logObj);
                }
                else
                    ActivityDal.Insert(logObj);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
            }
        }

        /// <summary>
        /// Log hành động xuất bản video 
        /// </summary>
        /// <param name="videoId"></param>
        /// <param name="modifiedBy"></param>
        /// <param name="videoTitle"></param>
        public static void LogPublishVideo(int videoId, string modifiedBy, string videoTitle)
        {
            var actionText = "<span class=\"cms_bm_account_name\">{0}</span> xuất bản video <a ref=\"video\" href=\"#{1}\" title=\"{2}\" class=\"cms_bm_action_text\">{3}</a>";
            actionText = string.Format(actionText, modifiedBy, videoId, videoTitle.Replace("\"", "&quot;"), Utility.SubWordInString(videoTitle, 10));
            try
            {
                var logObj = new ActivityEntity
                {
                    ActionText = actionText,
                    ApplicationId = videoId,
                    CreatedDate = DateTime.Now,
                    SourceId = modifiedBy,
                    SourceName = modifiedBy,
                    DestinationId = string.Empty,
                    DestinationName = string.Empty,
                    GroupTimeLine = Utility.SetPublishedDate(),
                    Message = videoTitle,
                    OwnerId = modifiedBy,
                    Type = (int)EnumActivityType.Video,
                    ActionTypeDetail = (int)VideoActionType.PublishVideo,
                    Title = videoTitle,
                    Avatar = "",
                };

                if (ApiNodeJsEnable)
                {
                    if (!ActivityNodeJsBo.InsertLog(logObj))
                        ActivityDal.Insert(logObj);
                }
                else
                    ActivityDal.Insert(logObj);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
            }
        }

        /// <summary>
        /// Log hành động gỡ xuất bản video 
        /// </summary>
        /// <param name="videoId"></param>
        /// <param name="modifiedBy"></param>
        /// <param name="videoTitle"></param>
        public static void LogUnpublishVideo(int videoId, string modifiedBy, string videoTitle)
        {
            var actionText = "<span class=\"cms_bm_account_name\">{0}</span> gỡ xuất bản video <a ref=\"video\" href=\"#{1}\" title=\"{2}\" class=\"cms_bm_action_text\">{3}</a>";
            actionText = string.Format(actionText, modifiedBy, videoId, videoTitle.Replace("\"", "&quot;"), Utility.SubWordInString(videoTitle, 10));
            try
            {
                var logObj = new ActivityEntity
                {
                    ActionText = actionText,
                    ApplicationId = videoId,
                    CreatedDate = DateTime.Now,
                    SourceId = modifiedBy,
                    SourceName = modifiedBy,
                    DestinationId = string.Empty,
                    DestinationName = string.Empty,
                    GroupTimeLine = Utility.SetPublishedDate(),
                    Message = videoTitle,
                    OwnerId = modifiedBy,
                    Type = (int)EnumActivityType.Video,
                    ActionTypeDetail = (int)VideoActionType.UnpublishVideo,
                    Title = videoTitle,
                    Avatar = "",
                };

                if (ApiNodeJsEnable)
                {
                    if (!ActivityNodeJsBo.InsertLog(logObj))
                    {
                        var id = ActivityDal.Insert(logObj);
                        if (id > 0)
                        {
                            logObj.Id = id;
                            CMS.BoCached.CmsExtension.SocialNetwork.ActivityDalFactory.AddActivity(logObj);
                            CMS.BoSearch.CmsExtension.SocialNetwork.ActivityDalFactory.AddActivity(logObj);
                        }
                    }
                }
                else {
                    var id = ActivityDal.Insert(logObj);
                    if (id > 0)
                    {
                        logObj.Id = id;
                        CMS.BoCached.CmsExtension.SocialNetwork.ActivityDalFactory.AddActivity(logObj);
                        CMS.BoSearch.CmsExtension.SocialNetwork.ActivityDalFactory.AddActivity(logObj);
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
            }
        }

        /// <summary>
        /// Log hành động thay đổi trạng thái nổi bật của video 
        /// </summary>
        /// <param name="videoId"></param>
        /// <param name="modifiedBy"></param>
        /// <param name="videoTitle"></param>
        /// <param name="videoMode"></param>
        public static void LogChangeVideoMode(int videoId, string modifiedBy, string videoTitle, int videoMode)
        {
            var actionText = "<span class=\"cms_bm_account_name\">{0}</span> đổi trạng thái nổi bật video <a ref=\"video\" href=\"#{1}\" title=\"{2}\" class=\"cms_bm_action_text\">{3}</a> thành <strong>{4}</strong>";
            actionText = string.Format(actionText, modifiedBy, videoId, videoTitle.Replace("\"", "&quot;"),
                                       Utility.SubWordInString(videoTitle, 10),
                                       (videoMode == 0 ? "Thông thường" : "Nổi bật"));
            try
            {
                var logObj = new ActivityEntity
                {
                    ActionText = actionText,
                    ApplicationId = videoId,
                    CreatedDate = DateTime.Now,
                    SourceId = modifiedBy,
                    SourceName = modifiedBy,
                    DestinationId = string.Empty,
                    DestinationName = string.Empty,
                    GroupTimeLine = Utility.SetPublishedDate(),
                    Message = videoTitle,
                    OwnerId = modifiedBy,
                    Type = (int)EnumActivityType.Video,
                    ActionTypeDetail = (int)VideoActionType.ChangeVideoMode,
                    Title = videoTitle,
                    Avatar = "",
                };

                if (ApiNodeJsEnable)
                {
                    if (!ActivityNodeJsBo.InsertLog(logObj))
                        ActivityDal.Insert(logObj);
                }
                else
                    ActivityDal.Insert(logObj);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
            }
        }

        /// <summary>
        /// Log hành động upload video từ youtube 
        /// </summary>
        /// <param name="videoId"></param>
        /// <param name="modifiedBy"></param>
        /// <param name="videoTitle"></param>
        /// <param name="sourceUrl"></param>
        public static void LogAddNewVideoFromYoutube(int videoId, string modifiedBy, string videoTitle, string sourceUrl)
        {
            var actionText = "<span class=\"cms_bm_account_name\">{0}</span> upload video youtube <a ref=\"video\" href=\"#{1}\" title=\"{2}\" class=\"cms_bm_action_text\">{3}</a> tại địa chỉ <a href=\"{4}\">{4}</a>";
            actionText = string.Format(actionText, modifiedBy, videoId, videoTitle.Replace("\"", "&quot;"), Utility.SubWordInString(videoTitle, 10), sourceUrl);
            try
            {
                var logObj = new ActivityEntity
                {
                    ActionText = actionText,
                    ApplicationId = videoId,
                    CreatedDate = DateTime.Now,
                    SourceId = modifiedBy,
                    SourceName = modifiedBy,
                    DestinationId = string.Empty,
                    DestinationName = string.Empty,
                    GroupTimeLine = Utility.SetPublishedDate(),
                    Message = videoTitle,
                    OwnerId = modifiedBy,
                    Type = (int)EnumActivityType.Video,
                    ActionTypeDetail = (int)VideoActionType.AddNewVideoFromYoutube,
                    Title = videoTitle,
                    Avatar = "",
                };

                if (ApiNodeJsEnable)
                {
                    if (!ActivityNodeJsBo.InsertLog(logObj))
                        ActivityDal.Insert(logObj);
                }
                else
                    ActivityDal.Insert(logObj);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
            }
        }


        /// <summary>
        /// Log hành động cập nhật video từ youtube 
        /// </summary>
        /// <param name="videoId"></param>
        /// <param name="modifiedBy"></param>
        /// <param name="videoTitle"></param>
        /// <param name="sourceUrl"></param>
        public static void LogUpdateVideoFromYoutube(int videoId, string modifiedBy, string videoTitle, string sourceUrl)
        {
            var actionText = "<span class=\"cms_bm_account_name\">{0}</span> cập nhật video youtube <a ref=\"video\" href=\"#{1}\" title=\"{2}\" class=\"cms_bm_action_text\">{3}</a> với địa chỉ <a href=\"{4}\">{4}</a>";
            actionText = string.Format(actionText, modifiedBy, videoId, videoTitle.Replace("\"", "&quot;"), Utility.SubWordInString(videoTitle, 10), sourceUrl);
            try
            {
                var logObj = new ActivityEntity
                {
                    ActionText = actionText,
                    ApplicationId = videoId,
                    CreatedDate = DateTime.Now,
                    SourceId = modifiedBy,
                    SourceName = modifiedBy,
                    DestinationId = string.Empty,
                    DestinationName = string.Empty,
                    GroupTimeLine = Utility.SetPublishedDate(),
                    Message = videoTitle,
                    OwnerId = modifiedBy,
                    Type = (int)EnumActivityType.Video,
                    ActionTypeDetail = (int)VideoActionType.UpdateVideoFromYoutube,
                    Title = videoTitle,
                    Avatar = "",
                };

                if (ApiNodeJsEnable)
                {
                    if (!ActivityNodeJsBo.InsertLog(logObj))
                        ActivityDal.Insert(logObj);
                }
                else
                    ActivityDal.Insert(logObj);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
            }
        }


        /// <summary>
        /// Log hành động xóa video từ youtube 
        /// </summary>
        /// <param name="videoId"></param>
        /// <param name="modifiedBy"></param>
        /// <param name="videoTitle"></param>
        public static void LogDeleteVideoFromYoutube(int videoId, string modifiedBy, string videoTitle)
        {
            var actionText = "<span class=\"cms_bm_account_name\">{0}</span> xóa video youtube <a ref=\"video\" href=\"#{1}\" title=\"{2}\" class=\"cms_bm_action_text\">{3}</a>";
            actionText = string.Format(actionText, modifiedBy, videoId, videoTitle.Replace("\"", "&quot;"), Utility.SubWordInString(videoTitle, 10));
            try
            {
                var logObj = new ActivityEntity
                {
                    ActionText = actionText,
                    ApplicationId = videoId,
                    CreatedDate = DateTime.Now,
                    SourceId = modifiedBy,
                    SourceName = modifiedBy,
                    DestinationId = string.Empty,
                    DestinationName = string.Empty,
                    GroupTimeLine = Utility.SetPublishedDate(),
                    Message = videoTitle,
                    OwnerId = modifiedBy,
                    Type = (int)EnumActivityType.Video,
                    ActionTypeDetail = (int)VideoActionType.DeleteVideoFromYoutube,
                    Title = videoTitle,
                    Avatar = "",
                };

                if (ApiNodeJsEnable)
                {
                    if (!ActivityNodeJsBo.InsertLog(logObj))
                        ActivityDal.Insert(logObj);
                }
                else
                    ActivityDal.Insert(logObj);

            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
            }
        }


        /// <summary>
        /// Log hành động gửi video từ youtube lên chờ xuất bản
        /// </summary>
        /// <param name="videoId"></param>
        /// <param name="modifiedBy"></param>
        /// <param name="videoTitle"></param>
        public static void LogSendVideoFromYoutube(int videoId, string modifiedBy, string videoTitle)
        {
            var actionText = "<span class=\"cms_bm_account_name\">{0}</span> gửi video youtube <a ref=\"video\" href=\"#{1}\" title=\"{2}\" class=\"cms_bm_action_text\">{3}</a> lên chờ xuất bản";
            actionText = string.Format(actionText, modifiedBy, videoId, videoTitle.Replace("\"", "&quot;"), Utility.SubWordInString(videoTitle, 10));
            try
            {
                var logObj = new ActivityEntity
                {
                    ActionText = actionText,
                    ApplicationId = videoId,
                    CreatedDate = DateTime.Now,
                    SourceId = modifiedBy,
                    SourceName = modifiedBy,
                    DestinationId = string.Empty,
                    DestinationName = string.Empty,
                    GroupTimeLine = Utility.SetPublishedDate(),
                    Message = videoTitle,
                    OwnerId = modifiedBy,
                    Type = (int)EnumActivityType.Video,
                    ActionTypeDetail = (int)VideoActionType.SendVideoFromYoutube,
                    Title = videoTitle,
                    Avatar = "",
                };

                if (ApiNodeJsEnable)
                {
                    if (!ActivityNodeJsBo.InsertLog(logObj))
                        ActivityDal.Insert(logObj);
                }
                else
                    ActivityDal.Insert(logObj);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
            }
        }


        /// <summary>
        /// Log hành động publish video từ youtube 
        /// </summary>
        /// <param name="videoId"></param>
        /// <param name="modifiedBy"></param>
        /// <param name="videoTitle"></param>
        public static void LogPublishVideoFromYoutube(int videoId, string modifiedBy, string videoTitle)
        {
            var actionText = "<span class=\"cms_bm_account_name\">{0}</span> xuất bản video youtube <a ref=\"video\" href=\"#{1}\" title=\"{2}\" class=\"cms_bm_action_text\">{3}</a>";
            actionText = string.Format(actionText, modifiedBy, videoId, videoTitle.Replace("\"", "&quot;"), Utility.SubWordInString(videoTitle, 10));
            try
            {
                var logObj = new ActivityEntity
                {
                    ActionText = actionText,
                    ApplicationId = videoId,
                    CreatedDate = DateTime.Now,
                    SourceId = modifiedBy,
                    SourceName = modifiedBy,
                    DestinationId = string.Empty,
                    DestinationName = string.Empty,
                    GroupTimeLine = Utility.SetPublishedDate(),
                    Message = videoTitle,
                    OwnerId = modifiedBy,
                    Type = (int)EnumActivityType.Video,
                    ActionTypeDetail = (int)VideoActionType.PublishVideoFromYoutube,
                    Title = videoTitle,
                    Avatar = "",
                };

                if (ApiNodeJsEnable)
                {
                    if (!ActivityNodeJsBo.InsertLog(logObj))
                        ActivityDal.Insert(logObj);
                }
                else
                    ActivityDal.Insert(logObj);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
            }
        }

        #endregion

        #region LogActivity On Comment

        [DataContract]
        public enum CommentActionType
        {
            [EnumMember]
            AllAction = -1,
            [EnumMember]
            DeleteComment = 3,
            [EnumMember]
            PublishComment = 2,
            [EnumMember]
            UnpublishComment = 1
        }

        /// <summary>
        /// Log hành động xuất bản bình luận
        /// </summary>
        /// <param name="commentId"></param>
        /// <param name="modifiedBy"></param>
        /// <param name="commentTitle"></param>
        public static void LogPublishComment(long commentId, string modifiedBy, string commentTitle)
        {
            var actionText = "<span class=\"cms_bm_account_name\">{0}</span> xuất bản bình luận <a href=\"#{1}\" class=\"cms_bm_action_text\" title=\"{2}\">{3}</a>";
            actionText = string.Format(actionText, modifiedBy, commentId, commentTitle.Replace("\"", "&quot;"), Utility.SubWordInString(commentTitle, 10));

            try
            {
                var logObj = new ActivityEntity
                {
                    ActionText = actionText,
                    ApplicationId = commentId,
                    CreatedDate = DateTime.Now,
                    SourceId = modifiedBy,
                    SourceName = modifiedBy,
                    DestinationId = string.Empty,
                    DestinationName = string.Empty,
                    GroupTimeLine = Utility.SetPublishedDate(),
                    Message = commentTitle,
                    OwnerId = modifiedBy,
                    Type = (int)EnumActivityType.Comment,
                    ActionTypeDetail = (int)CommentActionType.PublishComment,
                    Title = commentTitle,
                    Avatar = "",
                };
                if (ApiNodeJsEnable)
                {
                    if (!ActivityNodeJsBo.InsertLog(logObj))
                        ActivityDal.Insert(logObj);
                }
                else
                    ActivityDal.Insert(logObj);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
            }
        }

        /// <summary>
        /// Log hành động gỡ xuất bản bình luận
        /// </summary>
        /// <param name="commentId"></param>
        /// <param name="modifiedBy"></param>
        /// <param name="commentTitle"></param>
        public static void LogUnPublishComment(long commentId, string modifiedBy, string commentTitle)
        {
            try
            {
                var actionText = "<span class=\"cms_bm_account_name\">{0}</span> gỡ bình luận <a href=\"#{1}\" class=\"cms_bm_action_text\" title=\"{2}\">{3}</a>";
                actionText = string.Format(actionText, modifiedBy, commentId, commentTitle.Replace("\"", "&quot;"), Utility.SubWordInString(commentTitle, 10));

                var logObj = new ActivityEntity
                {
                    ActionText = actionText,
                    ApplicationId = commentId,
                    CreatedDate = DateTime.Now,
                    SourceId = modifiedBy,
                    SourceName = modifiedBy,
                    DestinationId = string.Empty,
                    DestinationName = string.Empty,
                    GroupTimeLine = Utility.SetPublishedDate(),
                    Message = commentTitle,
                    OwnerId = modifiedBy,
                    Type = (int)EnumActivityType.Comment,
                    ActionTypeDetail = (int)CommentActionType.UnpublishComment,
                    Title = commentTitle,
                    Avatar = "",
                };
                if (ApiNodeJsEnable)
                {
                    if (!ActivityNodeJsBo.InsertLog(logObj))
                        ActivityDal.Insert(logObj);
                }
                else
                    ActivityDal.Insert(logObj);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Trace, ex.StackTrace);
            }
        }

        /// <summary>
        /// Log hành động xóa bình luận
        /// </summary>
        /// <param name="commentId"></param>
        /// <param name="modifiedBy"></param>
        /// <param name="commentTitle"></param>
        public static void LogDeleteComment(long commentId, string modifiedBy, string commentTitle)
        {
            try
            {
                var actionText = "<span class=\"cms_bm_account_name\">{0}</span> xóa bình luận <a href=\"#{1}\" class=\"cms_bm_action_text\" title=\"{2}\">{3}</a>";
                actionText = string.Format(actionText, modifiedBy, commentId, commentTitle.Replace("\"", "&quot;"), Utility.SubWordInString(commentTitle, 10));

                var logObj = new ActivityEntity
                {
                    ActionText = actionText,
                    ApplicationId = commentId,
                    CreatedDate = DateTime.Now,
                    SourceId = modifiedBy,
                    SourceName = modifiedBy,
                    DestinationId = string.Empty,
                    DestinationName = string.Empty,
                    GroupTimeLine = Utility.SetPublishedDate(),
                    Message = commentTitle,
                    OwnerId = modifiedBy,
                    Type = (int)EnumActivityType.Comment,
                    ActionTypeDetail = (int)CommentActionType.DeleteComment,
                    Title = commentTitle,
                    Avatar = "",
                };
                if (ApiNodeJsEnable)
                {
                    if (!ActivityNodeJsBo.InsertLog(logObj))
                        ActivityDal.Insert(logObj);
                }
                else
                    ActivityDal.Insert(logObj);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
            }
        }
        #endregion

        #region AuthenticateLog
        /// <summary>
        /// Log hành động đăng nhập
        /// </summary>
        /// <param name="videoId"></param>
        /// <param name="modifiedBy"></param>
        /// <param name="videoTitle"></param>
        public static bool LogActionLogin(AuthenticateLogEntity auth)
        {            
            var actionText = "<span class=\"cms_bm_account_name\">{0}</span> đăng nhập <a ref=\"authen\" href=\"#{1}\" title=\"{2}\" class=\"cms_bm_action_text\">{3}</a>";            
            if (!auth.Status)
            {
                actionText = "<span class=\"cms_bm_account_name\">{0}</span> đăng xuất <a ref=\"authen\" href=\"#{1}\" title=\"{2}\" class=\"cms_bm_action_text\">{3}</a>";
            }
            actionText = string.Format(actionText, auth.UserName, 0, auth.UrlReferrer, auth.UrlReferrer);
            try
            {
                var logObj = new ActivityEntity
                {
                    ActionText = actionText,
                    ApplicationId = 0,
                    CreatedDate = DateTime.Now,
                    SourceId = auth.UserName,
                    SourceName = auth.UrlReferrer,
                    DestinationId = auth.IpAddress,
                    DestinationName = auth.MachineName,
                    GroupTimeLine = Utility.SetPublishedDate(),
                    Message = auth.UserAgent,
                    OwnerId = auth.Os,
                    Type = (int)EnumActivityType.AuthenticateLog,
                    ActionTypeDetail = auth.Status ? (int)EnumAuthenticateActionType.Login : (int)EnumAuthenticateActionType.Logout,
                    Description=auth.Description,
                    Ip= auth.IpAddress,
                    UserAgent = auth.UserAgent,
                    Account= auth.UserName,
                    Otp=auth.Otp
                };

                if (ApiNodeJsEnable)
                {
                    if (!ActivityNodeJsBo.InsertLog(logObj))
                    {
                        ActivityDal.Insert(logObj);
                        return true;
                    }
                    return true;
                }
                else
                    ActivityDal.Insert(logObj);
                return true;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return false;
            }
        }

        #endregion

        #region NewsHistory
        /// <summary>
        /// Log hành động đăng nhập
        /// </summary>
        /// <param name="videoId"></param>
        /// <param name="modifiedBy"></param>
        /// <param name="videoTitle"></param>
        public static void NewsHistory(long newsId, string modifiedBy, int newStatus, string NewsTitle, string actionName, string Message)
        {
            var actionText = "<span class=\"cms_bm_account_name\">{0}</span> {5} <a ref=\"newshistory\" href=\"#{1}\" title=\"{2}\" class=\"cms_bm_action_text\">{3}</a> {4}";
            actionText = string.Format(actionText, modifiedBy, newsId, NewsTitle, Utility.SubWordInString(NewsTitle, 10), Message, actionName);
            try
            {
                var logObj = new ActivityEntity
                {
                    ActionText = actionText,
                    ApplicationId = newsId,
                    CreatedDate = DateTime.Now,
                    SourceId = modifiedBy,
                    SourceName = modifiedBy,
                    DestinationId = string.Empty,
                    DestinationName = actionName,
                    GroupTimeLine = Utility.SetPublishedDate(),
                    Message = NewsTitle,
                    OwnerId = modifiedBy,
                    Type = (int)EnumActivityType.NewsHistory,
                    ActionTypeDetail = newStatus,
                    Title = NewsTitle,
                    Avatar = "",
                };

                if (ApiNodeJsEnable)
                {
                    if (!ActivityNodeJsBo.InsertLog(logObj))
                    {
                        var id = ActivityDal.Insert(logObj);
                        if (id > 0)
                        {
                            logObj.Id = id;
                            CMS.BoCached.CmsExtension.SocialNetwork.ActivityDalFactory.AddActivity(logObj);
                            CMS.BoSearch.CmsExtension.SocialNetwork.ActivityDalFactory.AddActivity(logObj);
                        }
                    }
                }
                else {
                    var id = ActivityDal.Insert(logObj);
                    if (id > 0)
                    {
                        logObj.Id = id;
                        CMS.BoCached.CmsExtension.SocialNetwork.ActivityDalFactory.AddActivity(logObj);
                        CMS.BoSearch.CmsExtension.SocialNetwork.ActivityDalFactory.AddActivity(logObj);
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
            }
        }

        #endregion

        #region DirectTag

        /// <summary>
        /// Log sửa directTag bài viết
        /// </summary>
        /// <param name="newsIds"></param>
        /// <param name="modifiedBy"></param>
        /// <param name="tagIds"></param>
        public static void LogInsertDirectTagNews(string tagIds, string newsIds, int type, string modifiedBy)
        {
            var actionText = "<span class=\"cms_bm_account_name\">{0}</span> sửa tag ids:  {1} cho bài viết ids: {2} với type: {3}";
            actionText = string.Format(actionText, modifiedBy, tagIds, newsIds, type);

            try
            {
                var logObj = new ActivityEntity
                {
                    ActionText = actionText,
                    ApplicationId = 0,
                    CreatedDate = DateTime.Now,
                    SourceId = modifiedBy,
                    SourceName = modifiedBy,
                    DestinationId = string.Empty,
                    DestinationName = string.Empty,
                    GroupTimeLine = Utility.SetPublishedDate(),
                    Message = tagIds + newsIds + type,
                    OwnerId = modifiedBy,
                    Type = (int)EnumActivityType.SeoNews,
                    ActionTypeDetail = (int)NewsActionType.InsertDerectTag
                };
                if (ApiNodeJsEnable)
                {
                    if (!ActivityNodeJsBo.InsertLog(logObj))
                    {
                        var id = ActivityDal.Insert(logObj);
                        if (id > 0)
                        {
                            logObj.Id = id;
                            CMS.BoCached.CmsExtension.SocialNetwork.ActivityDalFactory.AddActivity(logObj);
                            CMS.BoSearch.CmsExtension.SocialNetwork.ActivityDalFactory.AddActivity(logObj);
                        }
                    }
                }
                else
                {
                    var id = ActivityDal.Insert(logObj);
                    if (id > 0)
                    {
                        logObj.Id = id;
                        CMS.BoCached.CmsExtension.SocialNetwork.ActivityDalFactory.AddActivity(logObj);
                        CMS.BoSearch.CmsExtension.SocialNetwork.ActivityDalFactory.AddActivity(logObj);
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
            }
        }

        public static void LogDeleteDirectTagNews(int tagId, long newsId, int type, string modifiedBy)
        {
            var actionText = "<span class=\"cms_bm_account_name\">{0}</span> xóa tag id:  {1} cho bài viết id: {2} với type: {3}";
            actionText = string.Format(actionText, modifiedBy, tagId, newsId, type);

            try
            {
                var logObj = new ActivityEntity
                {
                    ActionText = actionText,
                    ApplicationId = newsId,
                    CreatedDate = DateTime.Now,
                    SourceId = modifiedBy,
                    SourceName = modifiedBy,
                    DestinationId = string.Empty,
                    DestinationName = string.Empty,
                    GroupTimeLine = Utility.SetPublishedDate(),
                    Message = "" + tagId + newsId + type,
                    OwnerId = modifiedBy,
                    Type = (int)EnumActivityType.SeoNews,
                    ActionTypeDetail = (int)NewsActionType.DeleteDerectTag
                };
                if (ApiNodeJsEnable)
                {
                    if (!ActivityNodeJsBo.InsertLog(logObj))
                    {
                        var id = ActivityDal.Insert(logObj);
                        if (id > 0)
                        {
                            logObj.Id = id;
                            CMS.BoCached.CmsExtension.SocialNetwork.ActivityDalFactory.AddActivity(logObj);
                            CMS.BoSearch.CmsExtension.SocialNetwork.ActivityDalFactory.AddActivity(logObj);
                        }
                    }
                }
                else
                {
                    var id = ActivityDal.Insert(logObj);
                    if (id > 0)
                    {
                        logObj.Id = id;
                        CMS.BoCached.CmsExtension.SocialNetwork.ActivityDalFactory.AddActivity(logObj);
                        CMS.BoSearch.CmsExtension.SocialNetwork.ActivityDalFactory.AddActivity(logObj);
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
            }
        }

        #endregion

        #region User
        public static void LogAddUser(string modifiedBy, string account)
        {
            var actionText = "<span class=\"cms_bm_account_name\">{0}</span> tạo tài khoản <span class=\"cms_bm_account_name\">{1}</span>";
            actionText = string.Format(actionText, modifiedBy, account);

            try
            {
                var logObj = new ActivityEntity
                {
                    ActionText = actionText,
                    ApplicationId = 0,
                    CreatedDate = DateTime.Now,
                    SourceId = modifiedBy,
                    SourceName = modifiedBy,
                    DestinationId = string.Empty,
                    DestinationName = string.Empty,
                    GroupTimeLine = Utility.SetPublishedDate(),
                    Message = actionText,
                    OwnerId = modifiedBy,
                    Type = (int)EnumActivityType.User,
                    ActionTypeDetail = (int)UserActionType.Add
                };
                if (ApiNodeJsEnable)
                {
                    if (!ActivityNodeJsBo.InsertLog(logObj))
                    {
                        var id = ActivityDal.Insert(logObj);
                        if (id > 0)
                        {
                            logObj.Id = id;
                            CMS.BoCached.CmsExtension.SocialNetwork.ActivityDalFactory.AddActivity(logObj);
                            CMS.BoSearch.CmsExtension.SocialNetwork.ActivityDalFactory.AddActivity(logObj);
                        }
                    }
                }
                else
                {
                    var id = ActivityDal.Insert(logObj);
                    if (id > 0)
                    {
                        logObj.Id = id;
                        CMS.BoCached.CmsExtension.SocialNetwork.ActivityDalFactory.AddActivity(logObj);
                        CMS.BoSearch.CmsExtension.SocialNetwork.ActivityDalFactory.AddActivity(logObj);
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
            }
        }
        public static void LogUpdateProfile(string modifiedBy, string account)
        {
            var actionText = "<span class=\"cms_bm_account_name\">{0}</span> sửa thông tin tài khoản <span class=\"cms_bm_account_name\">{1}</span>";
            actionText = string.Format(actionText, modifiedBy, account);

            try
            {
                var logObj = new ActivityEntity
                {
                    ActionText = actionText,
                    ApplicationId = 0,
                    CreatedDate = DateTime.Now,
                    SourceId = modifiedBy,
                    SourceName = modifiedBy,
                    DestinationId = string.Empty,
                    DestinationName = string.Empty,
                    GroupTimeLine = Utility.SetPublishedDate(),
                    Message = actionText,
                    OwnerId = modifiedBy,
                    Type = (int)EnumActivityType.User,
                    ActionTypeDetail = (int)UserActionType.Update
                };
                if (ApiNodeJsEnable)
                {
                    if (!ActivityNodeJsBo.InsertLog(logObj))
                    {
                        var id = ActivityDal.Insert(logObj);
                        if (id > 0)
                        {
                            logObj.Id = id;
                            CMS.BoCached.CmsExtension.SocialNetwork.ActivityDalFactory.AddActivity(logObj);
                            CMS.BoSearch.CmsExtension.SocialNetwork.ActivityDalFactory.AddActivity(logObj);
                        }
                    }
                }
                else
                {
                    var id = ActivityDal.Insert(logObj);
                    if (id > 0)
                    {
                        logObj.Id = id;
                        CMS.BoCached.CmsExtension.SocialNetwork.ActivityDalFactory.AddActivity(logObj);
                        CMS.BoSearch.CmsExtension.SocialNetwork.ActivityDalFactory.AddActivity(logObj);
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
            }
        }
        public static void LogUpdatePermission(string modifiedBy, string account)
        {
            var actionText = "<span class=\"cms_bm_account_name\">{0}</span> phân lại quyền cho tài khoản <span class=\"cms_bm_account_name\">{1}</span>";
            actionText = string.Format(actionText, modifiedBy, account);

            try
            {
                var logObj = new ActivityEntity
                {
                    ActionText = actionText,
                    ApplicationId = 0,
                    CreatedDate = DateTime.Now,
                    SourceId = modifiedBy,
                    SourceName = modifiedBy,
                    DestinationId = string.Empty,
                    DestinationName = string.Empty,
                    GroupTimeLine = Utility.SetPublishedDate(),
                    Message = actionText,
                    OwnerId = modifiedBy,
                    Type = (int)EnumActivityType.User,
                    ActionTypeDetail = (int)UserActionType.UpdatePermission
                };
                if (ApiNodeJsEnable)
                {
                    if (!ActivityNodeJsBo.InsertLog(logObj))
                    {
                        var id = ActivityDal.Insert(logObj);
                        if (id > 0)
                        {
                            logObj.Id = id;
                            CMS.BoCached.CmsExtension.SocialNetwork.ActivityDalFactory.AddActivity(logObj);
                            CMS.BoSearch.CmsExtension.SocialNetwork.ActivityDalFactory.AddActivity(logObj);
                        }
                    }
                }
                else
                {
                    var id = ActivityDal.Insert(logObj);
                    if (id > 0)
                    {
                        logObj.Id = id;
                        CMS.BoCached.CmsExtension.SocialNetwork.ActivityDalFactory.AddActivity(logObj);
                        CMS.BoSearch.CmsExtension.SocialNetwork.ActivityDalFactory.AddActivity(logObj);
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
            }
        }
        public static void LogResetOtp(string modifiedBy, string account)
        {
            var actionText = "<span class=\"cms_bm_account_name\">{0}</span> reset lại OTP cho tài khoản <span class=\"cms_bm_account_name\">{1}</span>";
            actionText = string.Format(actionText, modifiedBy, account);

            try
            {
                var logObj = new ActivityEntity
                {
                    ActionText = actionText,
                    ApplicationId = 0,
                    CreatedDate = DateTime.Now,
                    SourceId = modifiedBy,
                    SourceName = modifiedBy,
                    DestinationId = string.Empty,
                    DestinationName = string.Empty,
                    GroupTimeLine = Utility.SetPublishedDate(),
                    Message = actionText,
                    OwnerId = modifiedBy,
                    Type = (int)EnumActivityType.User,
                    ActionTypeDetail = (int)UserActionType.ResetOtp
                };
                if (ApiNodeJsEnable)
                {
                    if (!ActivityNodeJsBo.InsertLog(logObj))
                    {
                        var id = ActivityDal.Insert(logObj);
                        if (id > 0)
                        {
                            logObj.Id = id;
                            CMS.BoCached.CmsExtension.SocialNetwork.ActivityDalFactory.AddActivity(logObj);
                            CMS.BoSearch.CmsExtension.SocialNetwork.ActivityDalFactory.AddActivity(logObj);
                        }
                    }
                }
                else
                {
                    var id = ActivityDal.Insert(logObj);
                    if (id > 0)
                    {
                        logObj.Id = id;
                        CMS.BoCached.CmsExtension.SocialNetwork.ActivityDalFactory.AddActivity(logObj);
                        CMS.BoSearch.CmsExtension.SocialNetwork.ActivityDalFactory.AddActivity(logObj);
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
            }
        }
        public static void LogResetPassword(string modifiedBy, string account)
        {
            var actionText = "<span class=\"cms_bm_account_name\">{0}</span> reset lại mật khẩu cho tài khoản <span class=\"cms_bm_account_name\">{1}</span>";
            actionText = string.Format(actionText, modifiedBy, account);

            try
            {
                var logObj = new ActivityEntity
                {
                    ActionText = actionText,
                    ApplicationId = 0,
                    CreatedDate = DateTime.Now,
                    SourceId = modifiedBy,
                    SourceName = modifiedBy,
                    DestinationId = string.Empty,
                    DestinationName = string.Empty,
                    GroupTimeLine = Utility.SetPublishedDate(),
                    Message = actionText,
                    OwnerId = modifiedBy,
                    Type = (int)EnumActivityType.User,
                    ActionTypeDetail = (int)UserActionType.ResetOtp
                };
                if (ApiNodeJsEnable)
                {
                    if (!ActivityNodeJsBo.InsertLog(logObj))
                    {
                        var id = ActivityDal.Insert(logObj);
                        if (id > 0)
                        {
                            logObj.Id = id;
                            CMS.BoCached.CmsExtension.SocialNetwork.ActivityDalFactory.AddActivity(logObj);
                            CMS.BoSearch.CmsExtension.SocialNetwork.ActivityDalFactory.AddActivity(logObj);
                        }
                    }
                }
                else
                {
                    var id = ActivityDal.Insert(logObj);
                    if (id > 0)
                    {
                        logObj.Id = id;
                        CMS.BoCached.CmsExtension.SocialNetwork.ActivityDalFactory.AddActivity(logObj);
                        CMS.BoSearch.CmsExtension.SocialNetwork.ActivityDalFactory.AddActivity(logObj);
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
            }
        }
        public static void LogUpdateStatusUser(string modifiedBy, string account, int status)
        {            
            var actionText = "<span class=\"cms_bm_account_name\">{0}</span> khóa tài khoản <span class=\"cms_bm_account_name\">{1}</span>";
            var lockUser = (int)UserActionType.LockUser;
            if (status == 1)
            {
                actionText = "<span class=\"cms_bm_account_name\">{0}</span> mở tài khoản <span class=\"cms_bm_account_name\">{1}</span>";
                lockUser = (int)UserActionType.UnLockUser;
            }

            actionText = string.Format(actionText, modifiedBy, account);

            try
            {
                var logObj = new ActivityEntity
                {
                    ActionText = actionText,
                    ApplicationId = 0,
                    CreatedDate = DateTime.Now,
                    SourceId = modifiedBy,
                    SourceName = modifiedBy,
                    DestinationId = string.Empty,
                    DestinationName = string.Empty,
                    GroupTimeLine = Utility.SetPublishedDate(),
                    Message = actionText,
                    OwnerId = modifiedBy,
                    Type = (int)EnumActivityType.User,
                    ActionTypeDetail = lockUser
                };
                if (ApiNodeJsEnable)
                {
                    if (!ActivityNodeJsBo.InsertLog(logObj))
                    {
                        var id = ActivityDal.Insert(logObj);
                        if (id > 0)
                        {
                            logObj.Id = id;
                            CMS.BoCached.CmsExtension.SocialNetwork.ActivityDalFactory.AddActivity(logObj);
                            CMS.BoSearch.CmsExtension.SocialNetwork.ActivityDalFactory.AddActivity(logObj);
                        }
                    }
                }
                else
                {
                    var id = ActivityDal.Insert(logObj);
                    if (id > 0)
                    {
                        logObj.Id = id;
                        CMS.BoCached.CmsExtension.SocialNetwork.ActivityDalFactory.AddActivity(logObj);
                        CMS.BoSearch.CmsExtension.SocialNetwork.ActivityDalFactory.AddActivity(logObj);
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
            }
        }
        #endregion

        #region PR
        public static void LogUpdatePrStatus(string modifiedBy, string message)
        {
            var actionText = "<span class=\"cms_bm_account_name\">{0}</span> {1}";
            actionText = string.Format(actionText, modifiedBy, message);

            try
            {
                var logObj = new ActivityEntity
                {
                    ActionText = message,
                    ApplicationId = 0,
                    CreatedDate = DateTime.Now,
                    SourceId = modifiedBy,
                    SourceName = modifiedBy,
                    DestinationId = string.Empty,
                    DestinationName = string.Empty,
                    GroupTimeLine = Utility.SetPublishedDate(),
                    Message = message,
                    OwnerId = modifiedBy,
                    Type = (int)EnumActivityType.PR,
                    ActionTypeDetail = (int)NewsActionType.UpdateStausNewsPR
                };
                if (ApiNodeJsEnable)
                {
                    if (!ActivityNodeJsBo.InsertLog(logObj))
                    {
                        var id = ActivityDal.Insert(logObj);
                        if (id > 0)
                        {
                            logObj.Id = id;
                            CMS.BoCached.CmsExtension.SocialNetwork.ActivityDalFactory.AddActivity(logObj);
                            CMS.BoSearch.CmsExtension.SocialNetwork.ActivityDalFactory.AddActivity(logObj);
                        }
                    }
                }
                else
                {
                    var id = ActivityDal.Insert(logObj);
                    if (id > 0)
                    {
                        logObj.Id = id;
                        CMS.BoCached.CmsExtension.SocialNetwork.ActivityDalFactory.AddActivity(logObj);
                        CMS.BoSearch.CmsExtension.SocialNetwork.ActivityDalFactory.AddActivity(logObj);
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
            }
        }
        #endregion

        #region Expert
        public static void LogExpertAction(int actionType, string listIds, string note, string modifiedBy)
        {
            var message = "<span class=\"cms_bm_account_name\">{0}</span> {1} <a href=\"#\" title=\"{2}\" class=\"cms_bm_action_text\">{2}</a>";
            var actionText = string.Empty;
            switch (actionType)
            {
                case 1: actionText = "Đã gửi bài viết "; break;
                case 2: actionText = "Đã gửi lại bài viết "; break;
                case 3: actionText = "Đã rút lại bài viết "; break;
                case 4: actionText = "Đã xác nhận bài viết "; break;
                case 5: actionText = "Đã trả lại bài viết "; break;
            }
            message = string.Format(message, modifiedBy, actionText, listIds);

            try
            {
                var logObj = new ActivityEntity
                {
                    ActionText = message,
                    ApplicationId = 0,
                    CreatedDate = DateTime.Now,
                    SourceId = modifiedBy,
                    SourceName = modifiedBy,
                    DestinationId = string.Empty,
                    DestinationName = string.Empty,
                    GroupTimeLine = Utility.SetPublishedDate(),
                    Message = message,
                    OwnerId = modifiedBy,
                    Type = (int)EnumActivityType.Expert,
                    ActionTypeDetail = actionType
                };
                if (ApiNodeJsEnable)
                {
                    if (!ActivityNodeJsBo.InsertLog(logObj))
                    {
                        var id = ActivityDal.Insert(logObj);
                        if (id > 0)
                        {
                            logObj.Id = id;
                            CMS.BoCached.CmsExtension.SocialNetwork.ActivityDalFactory.AddActivity(logObj);
                            CMS.BoSearch.CmsExtension.SocialNetwork.ActivityDalFactory.AddActivity(logObj);
                        }
                    }
                }
                else
                {
                    var id = ActivityDal.Insert(logObj);
                    if (id > 0)
                    {
                        logObj.Id = id;
                        CMS.BoCached.CmsExtension.SocialNetwork.ActivityDalFactory.AddActivity(logObj);
                        CMS.BoSearch.CmsExtension.SocialNetwork.ActivityDalFactory.AddActivity(logObj);
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
            }
        }
        #endregion

        #region Photo
        public static void LogActionPhoto(int actionType, string title, string modifiedBy)
        {
            var message = "<span class=\"cms_bm_account_name\">{0}</span> {1} <a href=\"#\" title=\"{2}\" class=\"cms_bm_action_text\">{2}</a>";
            var actionText = string.Empty;
            switch (actionType)
            {
                case 1: actionText = "Đã gửi photo "; break;
                case 2: actionText = "Đã xuất bản photo "; break;
                case 3: actionText = "Đã gỡ photo "; break;
                case 4: actionText = "Đã trả lại photo "; break;
            }
            message = string.Format(message, modifiedBy, actionText, title);

            try
            {
                var logObj = new ActivityEntity
                {
                    ActionText = message,
                    ApplicationId = 0,
                    CreatedDate = DateTime.Now,
                    SourceId = modifiedBy,
                    SourceName = modifiedBy,
                    DestinationId = string.Empty,
                    DestinationName = string.Empty,
                    GroupTimeLine = Utility.SetPublishedDate(),
                    Message = message,
                    OwnerId = modifiedBy,
                    Type = (int)EnumActivityType.Photo,
                    ActionTypeDetail = actionType
                };
                if (ApiNodeJsEnable)
                {
                    if (!ActivityNodeJsBo.InsertLog(logObj))
                    {
                        var id = ActivityDal.Insert(logObj);
                        if (id > 0)
                        {
                            logObj.Id = id;
                            CMS.BoCached.CmsExtension.SocialNetwork.ActivityDalFactory.AddActivity(logObj);
                            CMS.BoSearch.CmsExtension.SocialNetwork.ActivityDalFactory.AddActivity(logObj);
                        }
                    }
                }
                else
                {
                    var id = ActivityDal.Insert(logObj);
                    if (id > 0)
                    {
                        logObj.Id = id;
                        CMS.BoCached.CmsExtension.SocialNetwork.ActivityDalFactory.AddActivity(logObj);
                        CMS.BoSearch.CmsExtension.SocialNetwork.ActivityDalFactory.AddActivity(logObj);
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
            }
        }
        public static void LogActionAlbum(int actionType, string title, string modifiedBy)
        {
            var message = "<span class=\"cms_bm_account_name\">{0}</span> {1} <a href=\"#\" title=\"{2}\" class=\"cms_bm_action_text\">{2}</a>";
            var actionText = string.Empty;
            switch (actionType)
            {
                case 1: actionText = "Đã gửi album "; break;
                case 2: actionText = "Đã xuất bản album "; break;
                case 3: actionText = "Đã gỡ album "; break;
                case 4: actionText = "Đã trả lại album "; break;                
            }
            message = string.Format(message, modifiedBy, actionText, title);

            try
            {
                var logObj = new ActivityEntity
                {
                    ActionText = message,
                    ApplicationId = 0,
                    CreatedDate = DateTime.Now,
                    SourceId = modifiedBy,
                    SourceName = modifiedBy,
                    DestinationId = string.Empty,
                    DestinationName = string.Empty,
                    GroupTimeLine = Utility.SetPublishedDate(),
                    Message = message,
                    OwnerId = modifiedBy,
                    Type = (int)EnumActivityType.Album,
                    ActionTypeDetail = actionType
                };
                if (ApiNodeJsEnable)
                {
                    if (!ActivityNodeJsBo.InsertLog(logObj))
                    {
                        var id = ActivityDal.Insert(logObj);
                        if (id > 0)
                        {
                            logObj.Id = id;
                            CMS.BoCached.CmsExtension.SocialNetwork.ActivityDalFactory.AddActivity(logObj);
                            CMS.BoSearch.CmsExtension.SocialNetwork.ActivityDalFactory.AddActivity(logObj);
                        }
                    }
                }
                else
                {
                    var id = ActivityDal.Insert(logObj);
                    if (id > 0)
                    {
                        logObj.Id = id;
                        CMS.BoCached.CmsExtension.SocialNetwork.ActivityDalFactory.AddActivity(logObj);
                        CMS.BoSearch.CmsExtension.SocialNetwork.ActivityDalFactory.AddActivity(logObj);
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
            }
        }
        #endregion

        #region Sets

        /// <summary>
        /// Log hành động chung
        /// </summary>
        /// <param name="applicationId"></param>
        /// <param name="sourceId"></param>
        /// <param name="sourceName"></param>
        /// <param name="destinationId"></param>
        /// <param name="destinationName"></param>
        /// <param name="message"></param>
        /// <param name="actionText"></param>
        public static void LogActivity(long applicationId, string sourceId, string sourceName, string destinationId, string destinationName, string message, string actionText, EnumActivityType logType)
        {
            try
            {
                Insert(new ActivityEntity
                {
                    ActionText = actionText,
                    ApplicationId = applicationId,
                    CreatedDate = DateTime.Now,
                    SourceId = sourceId,
                    SourceName = sourceName,
                    DestinationId = destinationId,
                    DestinationName = destinationName,
                    GroupTimeLine = Utility.SetPublishedDate(),
                    Message = message,
                    OwnerId = sourceId,
                    Type = (int)logType
                });
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
            }
        }

        public static long Insert(ActivityEntity logObj)
        {
            try
            {
                if (ApiNodeJsEnable)
                {
                    if (!ActivityNodeJsBo.InsertLog(logObj))
                    {
                        var id = ActivityDal.Insert(logObj);
                        if (id > 0)
                        {
                            logObj.Id = id;
                            CMS.BoCached.CmsExtension.SocialNetwork.ActivityDalFactory.AddActivity(logObj);
                            CMS.BoSearch.CmsExtension.SocialNetwork.ActivityDalFactory.AddActivity(logObj);
                        }
                    }
                }
                else
                {
                    var id = ActivityDal.Insert(logObj);
                    if (id > 0)
                    {
                        logObj.Id = id;
                        CMS.BoCached.CmsExtension.SocialNetwork.ActivityDalFactory.AddActivity(logObj);
                        CMS.BoSearch.CmsExtension.SocialNetwork.ActivityDalFactory.AddActivity(logObj);
                    }
                }                
                return logObj.Id;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return 0;
            }
        }

        public static ErrorMapping.ErrorCodes Delete(long activityId, string accountName)
        {
            try
            {
                return ActivityDal.Delete(activityId, accountName) ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.BusinessError;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return ErrorMapping.ErrorCodes.UnknowError;
            }
        }

        #endregion

        #region Gets

        public static List<ActivityEntity> GetTopLatest(int top, long applicationId, int type)
        {
            try
            {
                return ActivityDal.GetTopLatest(top, applicationId, type);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return null;
            }
        }

        public static List<ActivityEntity> GetTopLatest(int top, int actionTypeDetail, int type)
        {
            try
            {
                return ActivityDal.GetTopLatest(top, actionTypeDetail, type);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return null;
            }
        }

        public static List<ActivityEntity> GetTopLatest(int top, string actionTypeDetailList, int type)
        {
            try
            {
                return ActivityDal.GetTopLatest(top, actionTypeDetailList, type);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return null;
            }
        }

        public static List<ActivityEntity> Search(string keyword, int pageIndex, int pageSize, long applicationId, int actionTypeDetail, int type, string sourceId, DateTime dateFrom, DateTime dateTo, ref int totalRows)
        {
            try
            {
                if (ApiNodeJsEnable)
                {
                    return ActivityNodeJsBo.SearchLog(keyword, pageIndex, pageSize, applicationId, actionTypeDetail, type, sourceId, dateFrom, dateTo, ref totalRows);
                }
                else {
                    var listId = CMS.BoSearch.CmsExtension.SocialNetwork.ActivityDalFactory.SearchActivity(pageIndex, pageSize, applicationId, actionTypeDetail, type, dateFrom, dateTo, ref totalRows);
                    if (listId != null && listId.Count() > 0)
                    {
                        var data = CMS.BoCached.CmsExtension.SocialNetwork.ActivityDalFactory.GetActivityByListId(listId);
                        if (data == null)
                        {
                            data = ActivityDal.Search(pageIndex, pageSize, applicationId, actionTypeDetail, type, dateFrom, dateTo, ref totalRows);
                        }
                        return data;
                    }
                    return null;
                }

            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return null;
            }
        }

        #endregion
    }
}
