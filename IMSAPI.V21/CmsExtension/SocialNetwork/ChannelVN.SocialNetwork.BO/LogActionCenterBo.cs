﻿using ChannelVN.CMS.Common;
using ChannelVN.CMS.Common.ChannelConfig;
using ChannelVN.SocialNetwork.Entity;
using ChannelVN.WcfExtensions;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Runtime.Serialization;
using System.Text;
using System.Reflection;
using System.Threading.Tasks;
using System.Threading;
using System.Web;
using ChannelVN.CMS.Entity.Base.News;
using ChannelVN.CMS.Entity.Base.Security;
using ChannelVN.CMS.Entity.Base.AuthenticateLog;

namespace ChannelVN.SocialNetwork.BO
{
    public class LogActionCenter
    {
        public static bool PushLog(ActivityEntity log)
        {
            var context = HttpContext.Current;
            new Thread(()=>{
                HttpContext.Current = context;
                try
                {
                    if (CmsChannelConfiguration.GetAppSettingInBoolean(WcfMessageHeader.Current.Namespace, "LogActionCenterApiEnabled"))
                    {
                        var ip = getIP(HttpContext.Current);
                        log.Ip = string.IsNullOrEmpty(ip) ? log.Ip : ip;
                        log.UserAgent = string.IsNullOrEmpty(HttpContext.Current?.Request?.UserAgent) ? log.UserAgent : HttpContext.Current?.Request?.UserAgent;

                        var client = new LogActionCenterRestClient();                       
                        var obj = new
                        {
                            @namespace=client.Channel_Id?.ToLower(),
                            subNamespace="ims2",
                            action=GetActionString(log.Type, log.ActionTypeDetail),
                            time=Utility.DateTimeToSpan(DateTime.Now),
                            account=string.IsNullOrEmpty(log.Account) ? log.SourceId : log.Account,
                            Txt="",
                            userAgent=log.UserAgent?? "anonymous",
                            otp=log.Otp??"",
                            ip= log.Ip??"",
                            id = log.ApplicationId,
                            Title= log.Title??"",
                            Avatar=log.Avatar??"",
                            DataType= GetDataTypeString(log.Type)
                        };

                        if (Enum.IsDefined(typeof(EnumActionMapping), obj.action))
                        {
                            client.PostData = NewtonJson.Serialize(obj);
                            client.ActionName = "action";

                            Logger.WriteLog(Logger.LogType.Trace, "Huan => Start PostData => " + client.PostData);

                            var result = client.MakeRequest();
                            if (!string.IsNullOrEmpty(result))
                            {
                                var data = JsonConvert.DeserializeObject<ApiActionResponse>(result);
                                if (data.status != 200)
                                {
                                    Logger.WriteLog(Logger.LogType.Trace, "Huan => Error PushLog => status: " + data.status + " =>" + data.message + client.PostData);
                                }
                            }
                        }
                        //else
                        //{
                        //    Logger.WriteLog(Logger.LogType.Trace, "Huan => not push action: => " + obj.action);
                        //}
                    }                 
                }
                catch (Exception ex)
                {
                    Logger.WriteLog(Logger.LogType.Error, "PushLog => " + ex.Message + " PostData => " + NewtonJson.Serialize(log));                    
                }
            }).Start();

            return true;
        }
        private static string getIP(HttpContext c)
        {
            try {
                string ips = c?.Request?.ServerVariables.Get("HTTP_X_FORWARDED_FOR");
                if (!string.IsNullOrEmpty(ips))
                {
                    return ips.Split(',')[0];
                }
                return c?.Request?.ServerVariables["REMOTE_ADDR"];
            }
            catch
            {
                try {
                    return c?.Request?.ServerVariables["REMOTE_ADDR"] ?? c?.Request?.UserHostAddress;
                }
                catch
                {
                    return c?.Request?.UserHostAddress;
                }              
            }
        }        

        private static string GetActionString(int type, int actionTypeDetail)
        {
            var action= type + "_" + actionTypeDetail;
            try
            {
                switch (type)
                {
                    //news
                    case (int)EnumActivityType.News:
                        switch (actionTypeDetail)
                        {
                            case (int)NewsActionType.Addnew:
                                action = Enum.GetName(typeof(NewsActionType), actionTypeDetail);
                                break;
                            case (int)NewsActionType.Update:
                                action = Enum.GetName(typeof(EnumActionMapping), 3);
                                break;
                            case (int)NewsActionType.Publish:
                                action = Enum.GetName(typeof(EnumActionMapping), 1);
                                break;
                            case (int)NewsActionType.UnPublish:
                                //chỉ bắn bài xb khi gỡ => action delete_news
                                action = Enum.GetName(typeof(EnumActionMapping), 2);
                                break;
                            case (int)NewsActionType.Delete:
                                //action = Enum.GetName(typeof(EnumActionMapping), 2);
                                action = Enum.GetName(typeof(NewsActionType), actionTypeDetail);
                                break;
                            default:
                                action = Enum.GetName(typeof(NewsActionType), actionTypeDetail);
                                break;
                        }
                        break;
                    case (int)EnumActivityType.Video:
                        switch (actionTypeDetail)
                        {                            
                            default:
                                action = Enum.GetName(typeof(ActivityBo.VideoActionType), actionTypeDetail);
                                break;
                        }
                        break;
                    case (int)EnumActivityType.User:
                        switch (actionTypeDetail)
                        {
                            default:
                                action = Enum.GetName(typeof(UserActionType), actionTypeDetail);
                                break;
                        }
                        break;
                    case (int)EnumActivityType.Comment:
                        switch (actionTypeDetail)
                        {
                            default:
                                action = Enum.GetName(typeof(ActivityBo.CommentActionType), actionTypeDetail);
                                break;
                        }
                        break;
                    case (int)EnumActivityType.AuthenticateLog:
                        switch (actionTypeDetail)
                        {
                            case (int)EnumAuthenticateActionType.Login:
                                action = Enum.GetName(typeof(EnumActionMapping), 4);
                                break;
                            case (int)EnumAuthenticateActionType.Logout:
                                action = Enum.GetName(typeof(EnumActionMapping), 5);
                                break;
                            default:
                                action = Enum.GetName(typeof(EnumActivityType), 4);
                                break;
                        }
                        break;
                    case (int)EnumActivityType.SeoNews:
                        switch (actionTypeDetail)
                        {
                            default:
                                action = Enum.GetName(typeof(NewsActionType), actionTypeDetail);
                                break;
                        }
                        break;
                    case (int)EnumActivityType.NewsHistory:
                        switch (actionTypeDetail)
                        {
                            default:
                                action = Enum.GetName(typeof(EnumActivityType), 5);
                                break;
                        }
                        break;
                    case (int)EnumActivityType.PR:
                        switch (actionTypeDetail)
                        {
                            default:
                                action = Enum.GetName(typeof(NewsActionType), actionTypeDetail);
                                break;
                        }
                        break;
                    case (int)EnumActivityType.CheckNodeComment: break;                    
                    default:
                        break;
                }

                return action?.ToLower();
            }
            catch
            {
                return action?.ToLower();
            }
        }

        private static string GetDataTypeString(int type)
        {
            var datatype = ""+ type;
            try
            {
                datatype = Enum.GetName(typeof(EnumActivityType), type);
                switch (type)
                {
                    //news
                    case (int)EnumActivityType.News:
                        datatype = Enum.GetName(typeof(EnumDataTypeMapping), 1);
                        break;
                    case (int)EnumActivityType.Video:
                        datatype = Enum.GetName(typeof(EnumDataTypeMapping), 2);
                        break;
                    case (int)EnumActivityType.User:
                        //datatype = Enum.GetName(typeof(EnumDataTypeMapping), 2);
                        break;
                    case (int)EnumActivityType.Comment:
                        //datatype = Enum.GetName(typeof(EnumDataTypeMapping), 2);
                        break;
                    case (int)EnumActivityType.AuthenticateLog:
                        //datatype = Enum.GetName(typeof(EnumDataTypeMapping), 2);
                        break;
                    case (int)EnumActivityType.SeoNews:
                        //datatype = Enum.GetName(typeof(EnumDataTypeMapping), 2);
                        break;
                    case (int)EnumActivityType.NewsHistory:
                        datatype = Enum.GetName(typeof(EnumDataTypeMapping), 2);
                        break;
                    case (int)EnumActivityType.PR:
                        //datatype = Enum.GetName(typeof(EnumDataTypeMapping), 2);
                        break;
                    case (int)EnumActivityType.CheckNodeComment: break;
                    default:
                        break;
                }
                return datatype?.ToLower();
            }
            catch
            {
                return datatype?.ToLower();
            }
        }

        public enum EnumActionMapping
        {            
            publish_news = 1,
            delete_news = 2,
            save_news = 3,
            signin = 4,
            signout = 5
        }
        public enum EnumDataTypeMapping
        {
            none = 0,
            news = 1,
            video = 2
        }

        public class ApiActionResponse
        {            
            [DataMember]
            public int status { get; set; }
            [DataMember]
            public string message { get; set; }            
            public ApiActionResponse()
            {               
                status = 0;
                message = "";              
            }
        }
        [DataContract]
        public class ResponseActivity
        {
            [DataMember]
            public bool Success { get; set; }
            [DataMember]
            public int Code { get; set; }
            [DataMember]
            public string Message { get; set; }
            [DataMember]
            public List<ActivityNodeJsEntity> data { get; set; }
            public ResponseActivity()
            {
                Success = false;
                Message = "";
                data = new List<ActivityNodeJsEntity>();
            }
        }       
    }    
    public class LogActionCenterRestClient
    {
        public string EndPoint { get; set; }
        public HttpMethod Method { get; set; }
        public string ContentType { get; set; }
        public string PostData { get; set; }
        public string ActionName { get; set; }
        public string Channel_Id { get; set; }
        public string SecretKey { get; set; }
        public LogActionCenterRestClient()
        {
            EndPoint = CmsChannelConfiguration.GetAppSetting(WcfMessageHeader.Current.Namespace, "LogActionCenterApiUrl");
            Method = HttpMethod.POST;
            ContentType = "application/json";
            Channel_Id = WcfMessageHeader.Current.Namespace;
            SecretKey = CmsChannelConfiguration.GetAppSetting(WcfMessageHeader.Current.Namespace, "LogActionCenterApiSecretKey");
            PostData = "";
            ActionName = "";
        }
        public string MakeRequest()
        {
            try
            {
                var request = (HttpWebRequest)WebRequest.Create(EndPoint + ActionName);

                request.Method = Method.ToString();
                request.Timeout = 3*1000;
                request.ContentType = ContentType;                
                if (!string.IsNullOrEmpty(PostData) && Method == HttpMethod.POST)
                {
                    var bytes = UTF8Encoding.UTF8.GetBytes(PostData);
                    request.ContentLength = bytes.Length;

                    using (var writeStream = request.GetRequestStream())
                    {
                        writeStream.Write(bytes, 0, bytes.Length);
                    }
                }

                using (var response = (HttpWebResponse)request.GetResponse())
                {
                    var responseValue = string.Empty;

                    if (response.StatusCode != HttpStatusCode.OK)
                    {
                        var message = string.Format("Huan:Request failed. Received HTTP {0}", response.StatusCode);
                        throw new ApplicationException(message);
                    }

                    using (var responseStream = response.GetResponseStream())
                    {
                        if (responseStream != null)
                            using (var reader = new StreamReader(responseStream))
                            {
                                responseValue = reader.ReadToEnd();
                            }
                    }
                    //Logger.WriteLog(Logger.LogType.Trace, "Huan:MakeRequest: =>" + responseValue + " ActionName: " + ActionName);

                    return responseValue;
                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, "Huan:MakeRequest:" + ActionName + " =>" + ex.Message);
                return "";
            }
        }
    }
}