﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ChannelVN.SocialNetwork.DAL;
using ChannelVN.SocialNetwork.Entity;
using ChannelVN.SocialNetwork.Entity.ErrorCode;

namespace ChannelVN.SocialNetwork.BO.CmsDiscussion
{
    public class CmsDiscussionForCalendarManagerBo
    {
        public static List<CmsDiscussionEntity> GetTopDiscussionByCalendarId(int calendarId, int topParentDiscussion, int topChildDiscussion, ref int totalParentDiscussion)
        {
            var discussions = new List<CmsDiscussionEntity>();
            var parentDiscussions =
                CmsDiscussionDal.GetByApplicationAndObjectId((int)EnumCmsDiscussionApplicationId.CalendarComment,
                                                               calendarId, 0, 1, topParentDiscussion, ref totalParentDiscussion);
            foreach (var parentDiscussion in parentDiscussions)
            {
                discussions.Add(parentDiscussion);
                var totalChildDiscussion = 0;
                var childDiscussions =
                    CmsDiscussionDal.GetByApplicationAndObjectId(
                        (int)EnumCmsDiscussionApplicationId.CalendarComment,
                        calendarId, parentDiscussion.Id, 1, topChildDiscussion, ref totalChildDiscussion);

                discussions.AddRange(childDiscussions);
            }
            return discussions;
        }

        public static ErrorMapping.ErrorCodes SendDiscussion(int calendarId, long parentDiscussionId, string discussionContent, ref long newDiscussionId)
        {
            if (string.IsNullOrEmpty(discussionContent))
            {
                return ErrorMapping.ErrorCodes.BusinessError;
            }
            var discussion = new CmsDiscussionEntity
            {
                ApplicationId = (int)EnumCmsDiscussionApplicationId.CalendarComment,
                DiscussionContent = discussionContent,
                CreatedBy = WcfExtensions.WcfMessageHeader.Current.ClientUsername
            };

            if (parentDiscussionId > 0)
            {
                var parentDiscussion = CmsDiscussionDal.GetByDiscussionId(parentDiscussionId);
                if (null == parentDiscussion)
                {
                    return ErrorMapping.ErrorCodes.BusinessError;
                }
                discussion.ParentDiscussionId = parentDiscussionId;
                discussion.ObjectId = parentDiscussion.ObjectId;
                discussion.ObjectData = parentDiscussion.ObjectData;
            }
            else
            {
                discussion.ParentDiscussionId = 0;
                discussion.ObjectId = calendarId;
                discussion.ObjectData = "";
            }

            if (CmsDiscussionDal.SendDiscussion(discussion, ref newDiscussionId))
            {
                return ErrorMapping.ErrorCodes.Success;
            }
            else
            {
                return ErrorMapping.ErrorCodes.BusinessError;
            }
        }
    }
}
