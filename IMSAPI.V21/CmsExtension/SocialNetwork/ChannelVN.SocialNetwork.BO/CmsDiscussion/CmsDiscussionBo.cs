﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ChannelVN.SocialNetwork.DAL;
using ChannelVN.SocialNetwork.Entity;

namespace ChannelVN.SocialNetwork.BO.CmsDiscussion
{
    public class CmsDiscussionBo
    {
        public static CmsDiscussionEntity GetDiscussionByDiscussionId(long discussionId)
        {
            return CmsDiscussionDal.GetByDiscussionId(discussionId);
        }
    }
}
