﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.DAL.Base.Account;
using ChannelVN.CMS.DAL.Base.Zone;
using ChannelVN.CMS.Entity.Base.Security;
using ChannelVN.SocialNetwork.DAL;
using ChannelVN.SocialNetwork.Entity;
using ChannelVN.WcfExtensions;

namespace ChannelVN.SocialNetwork.BO
{
    public class DiscussionBo
    {
        public static long CreateDiscussionTopic(DiscussionTopicEntity discussionTopic, List<string> discussionTopicTags, List<DiscussionAttachmentEntity> discussionAttachments)
        {
            var discussionTopicId = 0L;
            try
            {
                var tagUsers = discussionTopicTags.Aggregate("", (current, tagUser) => current + (";" + tagUser));
                if (!string.IsNullOrEmpty(tagUsers)) tagUsers = tagUsers.Substring(1);

                discussionTopicId = DiscussionTopicDal.Insert(discussionTopic, tagUsers);

                FollowDiscussionTopic(discussionTopicId);

                if (discussionTopicId > 0)
                {
                    foreach (var discussionAttachment in discussionAttachments)
                    {
                        discussionAttachment.DiscussionId = 0;
                        discussionAttachment.DiscussionTopicId = discussionTopicId;
                        DiscussionAttachmentDal.Insert(discussionAttachment);
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Fatal, ex.ToString());
            }
            return discussionTopicId;
        }
        public static bool UpdateDiscussionTopic(DiscussionTopicEntity discussionTopic, List<string> discussionTopicTags, List<DiscussionAttachmentEntity> discussionAttachments)
        {
            var isUpdateSuccess = false;
            try
            {
                var tagUsers = discussionTopicTags.Aggregate("", (current, tagUser) => current + (";" + tagUser));
                if (!string.IsNullOrEmpty(tagUsers)) tagUsers = tagUsers.Substring(1);

                isUpdateSuccess = DiscussionTopicDal.Update(discussionTopic, tagUsers);

                if (isUpdateSuccess)
                {
                    DiscussionAttachmentDal.DeleteByTopicAndDiscussion(discussionTopic.Id, 0);

                    foreach (var discussionAttachment in discussionAttachments)
                    {
                        discussionAttachment.DiscussionId = 0;
                        discussionAttachment.DiscussionTopicId = discussionTopic.Id;
                        DiscussionAttachmentDal.Insert(discussionAttachment);
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Fatal, ex.ToString());
            }
            return isUpdateSuccess;
        }
        public static bool FollowDiscussionTopic(long discussionTopicId)
        {
            var followBy = WcfMessageHeader.Current.ClientUsername;
            var discussionTopic = DiscussionTopicDal.GetByDiscussionTopicId(discussionTopicId, followBy);
            if (discussionTopic == null)
            {
                return false;
            }

            var user = UserDal.GetUserByUsername(followBy);
            if (!user.IsFullPermission || !user.IsFullZone)
            {
                var permissions = UserPermissionDal.GetListUserPermissionByUsernameAndZoneId(followBy, discussionTopic.ZoneId);
                if (!user.IsFullPermission)
                {
                    if (permissions == null) return false;
                }
                else
                {
                    if (!permissions.Exists(
                        permission =>
                        permission.PermissionId == (int)EnumPermission.CreateTopicDiscussion ||
                        permission.PermissionId == (int)EnumPermission.CommentOnlyOnDiscussion))
                    {
                        return false;
                    }
                }
            }
            return DiscussionTopicFollowDal.FollowTopic(discussionTopicId, followBy);
        }
        public static bool UnfollowDiscussionTopic(long discussionTopicId)
        {
            var followBy = WcfMessageHeader.Current.ClientUsername;
            return DiscussionTopicFollowDal.UnfollowTopic(discussionTopicId, followBy);
        }
        public static bool UpdateDiscussionTopicPriority(string listDiscussionTopicId)
        {
            return DiscussionTopicDal.UpdatePriority(listDiscussionTopicId);
        }
        public static bool UpdateDiscussionPriority(string listDiscussionId)
        {
            return DiscussionDal.UpdatePriority(listDiscussionId);
        }
        public static bool ResolveDiscussionTopic(long discussionTopicId, string userDoAction)
        {
            try
            {
                var discussionTopic = DiscussionTopicDal.GetByDiscussionTopicId(discussionTopicId, userDoAction);
                if (discussionTopic != null)
                {
                    // Khong phai la nguoi tao topic
                    if (discussionTopic.CreatedBy != userDoAction)
                    {
                        // La topic con
                        if (discussionTopic.ParentId != 0)
                        {
                            // Lay topic cha
                            var parentDicussionTopic =
                                DiscussionTopicDal.GetByDiscussionTopicId(discussionTopic.ParentId, userDoAction);
                            // Neu tim thay topic cha va la nguoi tao topic cha => duoc phep dong
                            if (parentDicussionTopic != null && parentDicussionTopic.CreatedBy == userDoAction)
                            {
                                return DiscussionTopicDal.ResolveTopic(discussionTopicId, userDoAction);
                            }
                            else // Khong phai la nguoi tao topic cha
                            {
                                // Lay danh sach nhung nguoi follow
                                var listFollow = DiscussionTopicFollowDal.GetByDiscussionTopicId(discussionTopicId);
                                // La nguoi follow topic cha => duoc phep dong
                                if (listFollow != null && listFollow.Exists(item => item.FollowBy == userDoAction))
                                {
                                    return DiscussionTopicDal.ResolveTopic(discussionTopicId, userDoAction);
                                }
                            }
                        }
                    }
                    else // La nguoi tao topic => duoc phep dong
                    {
                        return DiscussionTopicDal.ResolveTopic(discussionTopicId, userDoAction);
                    }
                }
                return false;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Fatal, ex.ToString());
            }
            return false;
        }
        public static bool DeleteDiscussionTopic(long discussionTopicId, string userDoAction)
        {
            try
            {
                return DiscussionTopicDal.DeleteTopic(discussionTopicId, userDoAction);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Fatal, ex.ToString());
            }
            return false;
        }
        public static long SendDiscussion(DiscussionEntity discussion, List<string> discussionTopicTags, List<DiscussionAttachmentEntity> discussionAttachments)
        {
            var discussionId = 0L;
            try
            {
                var tagUsers = discussionTopicTags.Aggregate("", (current, tagUser) => current + (";" + tagUser));
                if (!string.IsNullOrEmpty(tagUsers)) tagUsers = tagUsers.Substring(1);
                discussionId = DiscussionDal.Insert(discussion, tagUsers);

                if (discussionId > 0)
                {
                    foreach (var discussionAttachment in discussionAttachments)
                    {
                        discussionAttachment.DiscussionId = discussionId;
                        discussionAttachment.DiscussionTopicId = 0;
                        DiscussionAttachmentDal.Insert(discussionAttachment);
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Fatal, ex.ToString());
            }
            return discussionId;
        }
        public static List<DiscussionTopicEntity> GetDiscussionTopicByApplicationIdAndObjectId(EnumTopicApplicationId applicationId, long parentId, long objectId, string userDoAction, string keyword, int zoneId, EnumTopicStatus status, int isFocus, bool isFilterByPermission, int pageIndex, int pageSize, ref int totalRow)
        {
            try
            {
                var zoneIds = "";
                if (zoneId > 0)
                {
                    zoneIds = zoneId.ToString();
                }
                else
                {
                    var permissionIds = "";
                    permissionIds += (int)EnumPermission.CreateTopicDiscussion;
                    permissionIds += ";" + (int)EnumPermission.CommentOnlyOnDiscussion;
                    var zones = ZoneDal.GetZoneByUsernameAndPermissionIds(userDoAction, permissionIds);
                    zoneIds = zones.Aggregate(zoneIds, (current, zone) => current + (";" + zone.Id));
                    zoneIds = "0" + zoneIds;
                }
                var discussionTopics = DiscussionTopicDal.GetByApplicationIdAndObjectId((int)applicationId, parentId,
                                                                                        objectId, userDoAction,
                                                                                        keyword, zoneIds, (int)status,
                                                                                        isFocus, pageIndex, pageSize,
                                                                                        ref totalRow);
                var listTopicId = discussionTopics.Aggregate("",
                                                             (current, discussionTopic) =>
                                                             current + (";" + discussionTopic.Id));
                if (!string.IsNullOrEmpty(listTopicId)) listTopicId = listTopicId.Remove(0, 1);

                var topicFollows = DiscussionTopicFollowDal.GetByListOfDiscussionTopicId(listTopicId);

                foreach (var discussionTopic in discussionTopics)
                {
                    discussionTopic.DiscussionTopicFollows = new List<DiscussionTopicFollowEntity>();
                    foreach (var topicFollow in topicFollows.Where(topicFollow => topicFollow.DiscussionTopicId == discussionTopic.Id))
                    {
                        discussionTopic.DiscussionTopicFollows.Add(topicFollow);
                    }
                }
                return discussionTopics;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Fatal, ex.ToString());
            }
            return new List<DiscussionTopicEntity>();
        }
        public static List<DiscussionEntity> GetDiscussionByApplicationIdAndObjectId(EnumTopicApplicationId applicationId, long objectId, string userDoAction, int pageIndex, int pageSize, ref int totalRow)
        {
            try
            {
                return DiscussionDal.GetByApplicationIdAndObjectId((int)applicationId, objectId, userDoAction, pageIndex, pageSize, ref totalRow);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Fatal, ex.ToString());
            }
            return new List<DiscussionEntity>();
        }
        public static List<DiscussionWithChildEntity> GetDiscussionByDiscussionTopicId(long dicussionTopicId, long parentDiscussionId, string userDoAction, int pageIndex, int pageSize, ref int totalRow)
        {
            var parentDiscussions = new List<DiscussionWithChildEntity>();
            try
            {
                var discussions = DiscussionDal.GetByDiscussionTopicId(dicussionTopicId, parentDiscussionId, userDoAction, pageIndex, pageSize, ref totalRow);
                var sortedDiscussions = discussions.OrderBy(item => item.ParentId).ThenByDescending(item => item.CreatedDate);

                foreach (var discussion in sortedDiscussions)
                {
                    if (discussion.ParentId == 0)
                    {
                        parentDiscussions.Add(new DiscussionWithChildEntity
                        {
                            Id = discussion.Id,
                            TopicId = discussion.TopicId,
                            ParentId = discussion.ParentId,
                            ObjectId = discussion.ObjectId,
                            Detail = discussion.Detail,
                            CreatedBy = discussion.CreatedBy,
                            CreatedDate = discussion.CreatedDate,
                            DeletedBy = discussion.DeletedBy,
                            DeletedDate = discussion.DeletedDate,
                            AttachmentCount = discussion.AttachmentCount,
                            Status = discussion.Status,
                            Data = discussion.Data,
                            Priority = discussion.Priority,
                            ChildDiscussions = new List<DiscussionEntity>(),
                            LastSendDiscussion = discussion.LastSendDiscussion
                        });
                    }
                    else
                    {
                        foreach (var parentDiscussion in parentDiscussions)
                        {
                            if (parentDiscussion.Id != discussion.ParentId) continue;
                            parentDiscussion.ChildDiscussions.Add(discussion);
                            break;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Fatal, ex.ToString());
            }
            return parentDiscussions;
        }
        public static List<DiscussionTopicDetailEntity> GetDiscussionTopicDetailByApplicationIdAndObjectId(EnumTopicApplicationId applicationId, long parentId, long objectId, string userDoAction, string keyword, int zoneId, EnumTopicStatus status, int isFocus, bool isFilterByPermission, int pageIndex, int pageSize, ref int totalRow)
        {
            try
            {
                var zoneIds = "";
                if (zoneId > 0)
                {
                    zoneIds = zoneId.ToString();
                }
                else
                {
                    var permissionIds = "";
                    permissionIds += (int)EnumPermission.CreateTopicDiscussion;
                    permissionIds += ";" + (int)EnumPermission.CreateTopicDiscussion;
                    var zones = ZoneDal.GetZoneByUsernameAndPermissionIds(userDoAction, permissionIds);
                    zoneIds = zones.Aggregate(zoneIds, (current, zone) => current + (";" + zone.Id));
                    zoneIds = "0" + zoneIds;
                }
                var discussionTopics = DiscussionTopicDal.GetByApplicationIdAndObjectId((int)applicationId, parentId, objectId,
                                                                                        userDoAction, keyword, zoneIds,
                                                                                        (int)status, isFocus, pageIndex, pageSize,
                                                                                        ref totalRow);
                return discussionTopics.Select(discussionTopic => new DiscussionTopicDetailEntity()
                    {
                        DiscussionTopic = discussionTopic,
                        DiscussionTags =
                            DiscussionTagDal.GetByDiscussionTopicId(
                                discussionTopic.Id, false),
                        DiscussionAttachments =
                            DiscussionAttachmentDal.GetByDiscussionTopicId
                                                                      (discussionTopic.Id),
                        DiscussionTopicFollows =
                            DiscussionTopicFollowDal.GetByDiscussionTopicId
                                                                      (discussionTopic.Id)
                    }).ToList();
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Fatal, ex.ToString());
            }
            return new List<DiscussionTopicDetailEntity>();
        }
        public static List<DiscussionDetailEntity> GetDiscussionDetailByApplicationIdAndObjectId(EnumTopicApplicationId applicationId, long objectId, string userDoAction, int pageIndex, int pageSize, ref int totalRow)
        {
            try
            {
                var discussions = DiscussionDal.GetByApplicationIdAndObjectId((int)applicationId, objectId, userDoAction, pageIndex, pageSize, ref totalRow);
                return discussions.Select(discussion => new DiscussionDetailEntity()
                {
                    Discussion = discussion,
                    DiscussionTags =
                        DiscussionTagDal.GetByDiscussionId(
                            discussion.Id),
                    DiscussionAttachments =
                        DiscussionAttachmentDal.GetByDiscussionId
                        (discussion.Id)
                }).ToList();
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Fatal, ex.ToString());
            }
            return new List<DiscussionDetailEntity>();
        }
        public static DiscussionTopicDetailEntity GetDiscussionTopicDetail(long discussionTopicId, string userDoAction)
        {
            try
            {
                return new DiscussionTopicDetailEntity
                    {
                        DiscussionTopic =
                            DiscussionTopicDal.GetByDiscussionTopicId(discussionTopicId,
                                                                      userDoAction),
                        DiscussionTags =
                            DiscussionTagDal.GetByDiscussionTopicId(discussionTopicId, true),
                        DiscussionAttachments =
                            DiscussionAttachmentDal.GetByDiscussionTopicId(discussionTopicId),
                        DiscussionTopicFollows =
                            DiscussionTopicFollowDal.GetByDiscussionTopicId
                                (discussionTopicId)
                    };
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Fatal, ex.ToString());
            }
            return null;
        }
        public static DiscussionDetailEntity GetDiscussionDetail(long discussionId, string userDoAction)
        {
            try
            {
                return new DiscussionDetailEntity
                {
                    Discussion =
                        DiscussionDal.GetByDiscussionId(discussionId,
                                                                  userDoAction),
                    DiscussionTags =
                        DiscussionTagDal.GetByDiscussionId(discussionId),
                    DiscussionAttachments =
                        DiscussionAttachmentDal.GetByDiscussionId(discussionId)
                };
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Fatal, ex.ToString());
            }
            return null;
        }
        public static int CountDiscussionTopicByApplicationIdAndObjectId(EnumTopicApplicationId applicationId, long objectId, string userDoAction, string keyword, int zoneId, EnumTopicStatus status)
        {
            try
            {
                return DiscussionTopicDal.CountDiscussionTopicByApplicationIdAndObjectId((int)applicationId, objectId,
                                                                                         userDoAction, keyword, zoneId,
                                                                                         (int)status);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Fatal, ex.ToString());
            }
            return 0;
        }
        public static int CountDiscussionByApplicationIdAndObjectId(EnumTopicApplicationId applicationId, long objectId, string userDoAction)
        {
            try
            {
                return DiscussionDal.CountDiscussionByApplicationIdAndObjectId((int)applicationId, objectId, userDoAction);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Fatal, ex.ToString());
            }
            return 0;
        }
        public static int CountDiscussionByTopicId(long topicId)
        {
            try
            {
                return DiscussionDal.CountDiscussionByTopicId(topicId);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Fatal, ex.ToString());
            }
            return 0;
        }

        public static bool InsertDiscussionAttachment(DiscussionAttachmentEntity discussionAttachment)
        {
            try
            {
                return DiscussionAttachmentDal.Insert(discussionAttachment);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Fatal, ex.ToString());
            }
            return false;
        }
        public static bool DeleteDiscussionAttachment(long discussionAttachmentId)
        {
            try
            {
                return DiscussionAttachmentDal.Delete(discussionAttachmentId);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Fatal, ex.ToString());
            }
            return false;
        }
        public static List<DiscussionAttachmentEntity> GetDiscussionAttachmentByDiscussionTopicId(long discussionTopicId)
        {
            try
            {
                return DiscussionAttachmentDal.GetByDiscussionTopicId(discussionTopicId);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Fatal, ex.ToString());
            }
            return new List<DiscussionAttachmentEntity>();
        }
        public static List<DiscussionAttachmentEntity> GetDiscussionAttachmentByDiscussionId(long discussionId)
        {
            try
            {
                return DiscussionAttachmentDal.GetByDiscussionId(discussionId);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Fatal, ex.ToString());
            }
            return new List<DiscussionAttachmentEntity>();
        }

        public static List<string> GetListUserInDiscussionTopic(long discussionTopicId, string userDoAction)
        {
            try
            {
                var listUser = new List<string>();
                var discussionTopic = DiscussionTopicDal.GetByDiscussionTopicId(discussionTopicId, userDoAction);
                if (discussionTopic != null)
                {
                    listUser.Add(discussionTopic.CreatedBy);
                    var discussionTags = DiscussionTagDal.GetByDiscussionTopicId(discussionTopicId, true);

                    foreach (
                        var discussionTag in
                            discussionTags.Where(discussionTag => listUser.IndexOf(discussionTag.TagTo) < 0))
                    {
                        listUser.Add(discussionTag.TagTo);
                    }
                }
                return listUser;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Fatal, ex.ToString());
            }
            return new List<string>();
        }

        public static List<DiscussionInTopicEntity> GetTopDiscussionTopicWithTopDiscussion(int topDiscussionTopic, int topDiscussion, long objectId, string userDoAction, EnumTopicStatus status, params EnumTopicApplicationId[] applicationIds)
        {
            var listApplicationId = applicationIds.Aggregate("", (current, applicationId) => current + (";" + (int)applicationId));
            if (!string.IsNullOrEmpty(listApplicationId)) listApplicationId = listApplicationId.Remove(0, 1);

            var discussionTopics = DiscussionTopicDal.GetTopTopicByApplicationIdAndObjectId(topDiscussionTopic, listApplicationId, objectId,
                                                                                    userDoAction, (int)status);
            var discussionTopicIds = discussionTopics.Aggregate("",
                                                                (current, discussionTopic) =>
                                                                current + (";" + discussionTopic.Id));

            if (!string.IsNullOrEmpty(discussionTopicIds)) discussionTopicIds = discussionTopicIds.Remove(0, 1);
            var allDiscussions = DiscussionDal.GetTopDiscussionByListDiscussionTopicId(topDiscussion, discussionTopicIds,
                                                                                       userDoAction);
            var sortedDiscussions = allDiscussions.OrderBy(item => item.ParentId).ThenByDescending(item => item.CreatedDate);

            var parentDiscussions = new List<DiscussionWithChildEntity>();
            foreach (var discussion in sortedDiscussions)
            {
                if (discussion.ParentId == 0)
                {
                    parentDiscussions.Add(new DiscussionWithChildEntity
                                              {
                                                  Id = discussion.Id,
                                                  TopicId = discussion.TopicId,
                                                  ParentId = discussion.ParentId,
                                                  ObjectId = discussion.ObjectId,
                                                  Detail = discussion.Detail,
                                                  CreatedBy = discussion.CreatedBy,
                                                  CreatedDate = discussion.CreatedDate,
                                                  DeletedBy = discussion.DeletedBy,
                                                  DeletedDate = discussion.DeletedDate,
                                                  AttachmentCount = discussion.AttachmentCount,
                                                  Status = discussion.Status,
                                                  Data = discussion.Data,
                                                  Priority = discussion.Priority,
                                                  ChildDiscussions = new List<DiscussionEntity>(),
                                                  LastSendDiscussion = discussion.LastSendDiscussion
                                              });
                }
                else
                {
                    foreach (var parentDiscussion in parentDiscussions)
                    {
                        if (parentDiscussion.Id != discussion.ParentId) continue;
                        parentDiscussion.ChildDiscussions.Add(discussion);
                        break;
                    }
                }
            }
            return discussionTopics.Select(discussionTopic => new DiscussionInTopicEntity
                                                                      {
                                                                          DiscussionTopic = discussionTopic,
                                                                          Discussions =
                                                                              parentDiscussions.Where(
                                                                                  discussion =>
                                                                                  discussion.TopicId == discussionTopic.Id).
                                                                              ToList()
                                                                      }).ToList();
        }
        //public static List<DiscussionInTopicEntity> GetTopDiscussionTopicAndDiscussionForNews(int topDiscussion, string userDoAction, EnumTopicStatus status, params EnumTopicApplicationId[] applicationIds)
        //{
        //    var returnDiscussionInTopic = new List<DiscussionInTopicEntity>();

        //    var listApplicationId = applicationIds.Aggregate("", (current, applicationId) => current + (";" + (int)applicationId));
        //    if (!string.IsNullOrEmpty(listApplicationId)) listApplicationId = listApplicationId.Remove(0, 1);

        //    #region For discussion topic

        //    var discussionTopics = DiscussionTopicDal.GetTopTopicForNews(topDiscussion, listApplicationId,
        //                                                                 userDoAction, (int)status);

        //    foreach (var discussionTopic in discussionTopics)
        //    {
        //        var existsItemIndex = -1;

        //        if (returnDiscussionInTopic.Count > 0)
        //        {
        //            existsItemIndex = returnDiscussionInTopic.FindIndex(item => item.DiscussionTopic.ObjectId == discussionTopic.ObjectId);
        //        }

        //        if (existsItemIndex < 0)
        //        {
        //            #region ObjectId not exists

        //            returnDiscussionInTopic.Add(new DiscussionInTopicEntity
        //                                            {
        //                                                DiscussionTopic = discussionTopic,
        //                                                Discussions = new List<DiscussionEntity>
        //                                                                  {
        //                                                                      new DiscussionEntity
        //                                                                          {
        //                                                                              Id = 0,
        //                                                                              TopicId = discussionTopic.Id,
        //                                                                              ParentId = 0,
        //                                                                              ObjectId =
        //                                                                                  discussionTopic.ObjectId,
        //                                                                              Detail =
        //                                                                                  discussionTopic.TopicContent,
        //                                                                              CreatedBy =
        //                                                                                  discussionTopic.CreatedBy,
        //                                                                              CreatedDate =
        //                                                                                  discussionTopic.CreatedDate,
        //                                                                              Status = 1
        //                                                                          }
        //                                                                  }
        //                                            });

        //            #endregion
        //        }
        //        else
        //        {
        //            #region ObjectId exists

        //            var existsItem = returnDiscussionInTopic[existsItemIndex];
        //            existsItem.Discussions.Add(new DiscussionEntity
        //                                           {
        //                                               Id = 0,
        //                                               TopicId = discussionTopic.Id,
        //                                               ParentId = 0,
        //                                               ObjectId =
        //                                                   discussionTopic.ObjectId,
        //                                               Detail =
        //                                                   discussionTopic.TopicContent,
        //                                               CreatedBy =
        //                                                   discussionTopic.CreatedBy,
        //                                               CreatedDate =
        //                                                   discussionTopic.CreatedDate,
        //                                               Status = 1
        //                                           });
        //            returnDiscussionInTopic[existsItemIndex] = existsItem;

        //            #endregion
        //        }
        //    }

        //    #endregion

        //    #region For discussion

        //    var discussions = DiscussionDal.GetTopDiscussionForNews(topDiscussion, listApplicationId,
        //                                                                 userDoAction, (int)status);

        //    foreach (var discussion in discussions)
        //    {
        //        var existsItemIndex = -1;

        //        if (returnDiscussionInTopic.Count > 0)
        //        {
        //            existsItemIndex = returnDiscussionInTopic.FindIndex(item => item.DiscussionTopic.ObjectId == discussion.ObjectId);
        //        }

        //        if (existsItemIndex < 0)
        //        {
        //            #region ObjectId not exists

        //            returnDiscussionInTopic.Add(new DiscussionInTopicEntity
        //                                            {
        //                                                DiscussionTopic = new DiscussionTopicEntity
        //                                                                      {
        //                                                                          ApplicationId =
        //                                                                              (int)
        //                                                                              EnumTopicApplicationId.
        //                                                                                  CommentOnNews,
        //                                                                          ObjectId = discussion.ObjectId,
        //                                                                          CreatedDate = discussion.CreatedDate,
        //                                                                          CreatedBy = discussion.CreatedBy,
        //                                                                          IsPrivate = false,
        //                                                                          ObjectTitle =
        //                                                                              Utility.ConvertToString(
        //                                                                                  discussion["NewsTitle"]),
        //                                                                          TopicContent = discussion.Detail,
        //                                                                          LastSendDiscussion = discussion.CreatedDate
        //                                                                      },
        //                                                Discussions = new List<DiscussionEntity> { discussion }
        //                                            });

        //            #endregion
        //        }
        //        else
        //        {
        //            #region ObjectId exists

        //            var existsItem = returnDiscussionInTopic[existsItemIndex];
        //            existsItem.Discussions.Add(discussion);
        //            returnDiscussionInTopic[existsItemIndex] = existsItem;

        //            #endregion
        //        }
        //    }

        //    #endregion

        //    return
        //        new List<DiscussionInTopicEntity>(
        //            returnDiscussionInTopic.OrderByDescending(item => item.DiscussionTopic.LastSendDiscussion));
        //}
        public static List<DiscussionTopicEntity> GetTopTopicByApplicationIdAndObjectId(int topTopic, long objectId, string userDoAction, EnumTopicStatus status, params EnumTopicApplicationId[] applicationIds)
        {
            var listApplicationId = applicationIds.Aggregate("", (current, applicationId) => current + (";" + (int)applicationId));
            if (!string.IsNullOrEmpty(listApplicationId)) listApplicationId = listApplicationId.Remove(0, 1);

            return DiscussionTopicDal.GetTopTopicByApplicationIdAndObjectId(topTopic,
                                                                            listApplicationId,
                                                                            objectId,
                                                                            userDoAction, (int)status);
        }
    }
}
