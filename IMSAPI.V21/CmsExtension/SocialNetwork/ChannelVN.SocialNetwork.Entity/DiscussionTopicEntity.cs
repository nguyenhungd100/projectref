﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using ChannelVN.CMS.Common;

namespace ChannelVN.SocialNetwork.Entity
{
    [DataContract]
    public enum EnumTopicApplicationId : int
    {
        [EnumMember]
        CheckErrorComment = 1,
        [EnumMember]
        CommentOnNews = 2,
        [EnumMember]
        CommentOnNewsVersion = 3,
        [EnumMember]
        CommentOnTopic = 4,
        [EnumMember]
        CommentOnActivity = 5
    }
    [DataContract]
    public enum EnumTopicStatus : int
    {
        [EnumMember]
        All = 0,
        [EnumMember]
        Actived = 1,
        [EnumMember]
        Resolved = 2,
        [EnumMember]
        Deleted = 99
    }
    [DataContract]
    public class DiscussionTopicEntity : EntityBase
    {
        [DataMember]
        public long Id { get; set; }
        [DataMember]
        public int ZoneId { get; set; }
        [DataMember]
        public int ApplicationId { get; set; }
        [DataMember]
        public long ObjectId { get; set; }
        [DataMember]
        public string ObjectTitle { get; set; }
        [DataMember]
        public string ObjectData { get; set; }
        [DataMember]
        public string TopicContent { get; set; }
        [DataMember]
        public int TopicPriority { get; set; }
        [DataMember]
        public string CreatedBy { get; set; }
        [DataMember]
        public DateTime CreatedDate { get; set; }
        [DataMember]
        public string ResolvedBy { get; set; }
        [DataMember]
        public DateTime ResovedDate { get; set; }
        [DataMember]
        public string DeletedBy { get; set; }
        [DataMember]
        public DateTime DeletedDate { get; set; }
        [DataMember]
        public int Status { get; set; }
        [DataMember]
        public bool IsPrivate { get; set; }
        [DataMember]
        public bool IsFocus { get; set; }
        [DataMember]
        public long ParentId { get; set; }
        [DataMember]
        public string TopicNote { get; set; }
        [DataMember]
        public DateTime LastSendDiscussion { get; set; }
        [DataMember]
        public int AttachmentCount { get; set; }
        [DataMember]
        public int DiscussionCount { get; set; }
        [DataMember]
        public List<DiscussionTopicFollowEntity> DiscussionTopicFollows { get; set; }
    }

    [DataContract]
    public class DiscussionTopicDetailEntity : EntityBase
    {
        [DataMember]
        public DiscussionTopicEntity DiscussionTopic { get; set; }
        [DataMember]
        public List<DiscussionTagEntity> DiscussionTags { get; set; }
        [DataMember]
        public List<DiscussionAttachmentEntity> DiscussionAttachments { get; set; }
        [DataMember]
        public List<DiscussionTopicFollowEntity> DiscussionTopicFollows { get; set; }
    }

    [DataContract]
    public class DiscussionInTopicEntity : EntityBase
    {
        [DataMember]
        public DiscussionTopicEntity DiscussionTopic { get; set; }
        [DataMember]
        public List<DiscussionWithChildEntity> Discussions { get; set; }
    }
}
