﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ChannelVN.CMS.Common;

namespace ChannelVN.SocialNetwork.Entity
{
    public class DiscussionTagEntity : EntityBase
    {
        public long Id { get; set; }
        public long DiscussionTopicId { get; set; }
        public long DiscussionId { get; set; }
        public string CreatedBy { get; set; }
        public string TagTo { get; set; }
        public DateTime CreatedDate { get; set; }
    }
}
