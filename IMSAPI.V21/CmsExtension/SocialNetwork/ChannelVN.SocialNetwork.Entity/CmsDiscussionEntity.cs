﻿using System;
using System.Runtime.Serialization;
using ChannelVN.CMS.Common;

namespace ChannelVN.SocialNetwork.Entity
{
    [DataContract]
    public enum EnumCmsDiscussionApplicationId
    {
        [EnumMember]
        NewsCrawlerComment = 1,
        [EnumMember]
        JobComment = 2,
        [EnumMember]
        JobAssignmentComment = 3,
        [EnumMember]
        NewsComment = 4,
        [EnumMember]
        NewsCheckNodeComment = 5,
        [EnumMember]
        TopicComment = 6,
        [EnumMember]
        CalendarComment = 7
    }

    [DataContract]
    public class CmsDiscussionEntity : EntityBase
    {
        [DataMember]
        public long Id { get; set; }
        [DataMember]
        public long ParentDiscussionId { get; set; }
        [DataMember]
        public int ApplicationId { get; set; }
        [DataMember]
        public long ObjectId { get; set; }
        [DataMember]
        public string ObjectData { get; set; }
        [DataMember]
        public string DiscussionContent { get; set; }
        [DataMember]
        public DateTime CreatedDate { get; set; }
        [DataMember]
        public string CreatedBy { get; set; }
        [DataMember]
        public int Status { get; set; }
    }
}
