﻿using System;
using System.Runtime.Serialization;
using ChannelVN.CMS.Common;

namespace ChannelVN.SocialNetwork.Entity
{
    [DataContract]
    public enum EnumActivityType
    {
        [EnumMember]
        CheckNodeComment = 0,
        [EnumMember]
        News = 1,
        [EnumMember]
        Video = 2,
        [EnumMember]
        Comment = 3,
        [EnumMember]
        AuthenticateLog = 4,
        [EnumMember]
        NewsHistory = 5,
        [EnumMember]
        User = 6,
        [EnumMember]
        SeoNews = 7,
        [EnumMember]
        PR =8,
        [EnumMember]
        Expert = 9,
        [EnumMember]
        Photo = 10,
        [EnumMember]
        Album = 11
    }

    [DataContract]
    public class ActivityEntity : EntityBase
    {
        [DataMember]
        public long Id { get; set; }
        [DataMember]
        public string EncryptId
        {
            get { return CryptonForId.EncryptId(Id); }
            set { }
        }

        [DataMember]
        public long ApplicationId { get; set; }

        [DataMember]
        public string ApplicationStringId
        {
            get { return CryptonForId.EncryptId(ApplicationId); }
            set { }
        }

        [DataMember]
        public string SourceId { get; set; }

        [DataMember]
        public string SourceName { get; set; }

        [DataMember]
        public string DestinationId { get; set; }

        [DataMember]
        public string DestinationName { get; set; }

        [DataMember]
        public DateTime CreatedDate { get; set; }

        [DataMember]
        public string ActionText { get; set; }

        [DataMember]
        public string Message { get; set; }

        [DataMember]
        public string OwnerId { get; set; }

        [DataMember]
        public int Type { get; set; }

        [DataMember]
        public int ActionTypeDetail { get; set; }

        [DataMember]
        public long GroupTimeLine { get; set; }

        [DataMember]
        public string Description { get; set; }

        [DataMember]
        public string UserAgent { get; set; }
        [DataMember]
        public string Ip { get; set; }
        [DataMember]
        public string Title { get; set; }
        [DataMember]
        public string Avatar { get; set; }
        [DataMember]
        public int Status { get; set; }
        [DataMember]
        public string Otp { get; set; }
        [DataMember]
        public string Account { get; set; }
    }
}
