﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using ChannelVN.CMS.Common;

namespace ChannelVN.SocialNetwork.Entity
{
    [DataContract]
    public class DiscussionEntity : EntityBase
    {
        [DataMember]
        public long Id { get; set; }
        [DataMember]
        public long TopicId { get; set; }
        [DataMember]
        public long ParentId { get; set; }
        [DataMember]
        public string Detail { get; set; }
        [DataMember]
        public string CreatedBy { get; set; }
        [DataMember]
        public DateTime CreatedDate { get; set; }
        [DataMember]
        public string DeletedBy { get; set; }
        [DataMember]
        public DateTime DeletedDate { get; set; }
        [DataMember]
        public int Status { get; set; }
        [DataMember]
        public string Data { get; set; }
        [DataMember]
        public int Priority { get; set; }
        [DataMember]
        public int AttachmentCount { get; set; }
        [DataMember]
        public long ObjectId { get; set; }
        [DataMember]
        public DateTime LastSendDiscussion { get; set; }
    }
    [DataContract]
    public class DiscussionWithChildEntity : EntityBase
    {
        [DataMember]
        public long Id { get; set; }
        [DataMember]
        public long TopicId { get; set; }
        [DataMember]
        public long ParentId { get; set; }
        [DataMember]
        public string Detail { get; set; }
        [DataMember]
        public string CreatedBy { get; set; }
        [DataMember]
        public DateTime CreatedDate { get; set; }
        [DataMember]
        public string DeletedBy { get; set; }
        [DataMember]
        public DateTime DeletedDate { get; set; }
        [DataMember]
        public int Status { get; set; }
        [DataMember]
        public string Data { get; set; }
        [DataMember]
        public int Priority { get; set; }
        [DataMember]
        public int AttachmentCount { get; set; }
        [DataMember]
        public long ObjectId { get; set; }

        [DataMember]
        public List<DiscussionEntity> ChildDiscussions { get; set; }

        [DataMember]
        public DateTime LastSendDiscussion { get; set; }
    }

    [DataContract]
    public class DiscussionDetailEntity : EntityBase
    {
        [DataMember]
        public DiscussionEntity Discussion { get; set; }
        [DataMember]
        public List<DiscussionTagEntity> DiscussionTags { get; set; }
        [DataMember]
        public List<DiscussionAttachmentEntity> DiscussionAttachments { get; set; }
        [DataMember]
        public List<DiscussionEntity> ChildDiscussions { get; set; }
    }
}
