﻿using System;
using System.Runtime.Serialization;
using Newtonsoft.Json;

namespace ChannelVN.SocialNetwork.Entity
{
    [DataContract]
    public class ActivityNodeJsEntity
    {

        [JsonProperty("id")]
        public string Id { get; set; }

        [JsonProperty("object_id")]
        public long ApplicationId { get; set; }

        [JsonProperty("source_id")]
        public string SourceId { get; set; }

        [JsonProperty("source_name")]
        public string SourceName { get; set; }

        [JsonProperty("destination_id")]
        public string DestinationId { get; set; }

        [JsonProperty("destination_name")]
        public string DestinationName { get; set; }

        [JsonProperty("created_date")]
        public DateTime CreatedDate { get; set; }

        [JsonProperty("action_text")]
        public string ActionText { get; set; }

        [JsonProperty("message")]        
        public string Message { get; set; }

        [JsonProperty("created_by")]
        public string OwnerId { get; set; }

        [JsonProperty("object_type")]
        public int Type { get; set; }

        [JsonProperty("action_type")]
        public int ActionTypeDetail { get; set; }        

        [JsonProperty("description")]        
        public string Description { get; set; }

        [JsonProperty("machine_name")]        
        public string UserAgent { get; set; }

        [JsonProperty("ip_log")]        
        public string Ip { get; set; }
    }
}
