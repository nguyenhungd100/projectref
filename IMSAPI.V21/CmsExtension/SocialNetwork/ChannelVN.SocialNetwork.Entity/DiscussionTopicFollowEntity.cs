﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using ChannelVN.CMS.Common;

namespace ChannelVN.SocialNetwork.Entity
{
    [DataContract]
    public class DiscussionTopicFollowEntity : EntityBase
    {
        [DataMember]
        public long Id { get; set; }
        [DataMember]
        public long DiscussionTopicId { get; set; }
        [DataMember]
        public string FollowBy { get; set; }
        [DataMember]
        public DateTime CreatedDate { get; set; }
    }
}
