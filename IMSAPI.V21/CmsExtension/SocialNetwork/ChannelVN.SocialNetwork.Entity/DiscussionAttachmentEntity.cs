﻿using System;
using ChannelVN.CMS.Common;

namespace ChannelVN.SocialNetwork.Entity
{
    public class DiscussionAttachmentEntity : EntityBase
    {
        public long Id { get; set; }
        public long DiscussionTopicId { get; set; }
        public long DiscussionId { get; set; }
        public string FileName { get; set; }
        public string FileUrl { get; set; }
        public int Size { get; set; }
        public DateTime CreatedDate { get; set; }
        public string CreatedBy { get; set; }
    }
}
