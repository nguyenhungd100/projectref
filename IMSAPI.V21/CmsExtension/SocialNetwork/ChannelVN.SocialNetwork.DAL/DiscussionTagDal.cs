﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using ChannelVN.CMS.Common;
using ChannelVN.SocialNetwork.DAL.Common;
using ChannelVN.SocialNetwork.Entity;
using ChannelVN.SocialNetwork.MainDal.Databases;

namespace ChannelVN.SocialNetwork.DAL
{
    public class DiscussionTagDal
    {
        public static List<DiscussionTagEntity> GetByDiscussionTopicId(long discussionTopicId, bool includeDiscussionTag)
        {
            List<DiscussionTagEntity> returnValue;
            using (var db = new CmsSocialNetworkDb())
            {
                returnValue = db.DiscussionTagMainDal.GetByDiscussionTopicId(discussionTopicId, includeDiscussionTag);
            }
            return returnValue;
        }
        public static List<DiscussionTagEntity> GetByDiscussionId(long discussionId)
        {
            List<DiscussionTagEntity> returnValue;
            using (var db = new CmsSocialNetworkDb())
            {
                returnValue = db.DiscussionTagMainDal.GetByDiscussionId(discussionId);
            }
            return returnValue;
        }
    }
}
