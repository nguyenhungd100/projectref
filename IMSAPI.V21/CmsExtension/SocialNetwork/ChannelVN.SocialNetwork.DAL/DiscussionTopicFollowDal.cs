﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using ChannelVN.CMS.Common;
using ChannelVN.SocialNetwork.DAL.Common;
using ChannelVN.SocialNetwork.Entity;
using ChannelVN.SocialNetwork.MainDal.Databases;

namespace ChannelVN.SocialNetwork.DAL
{
    public class DiscussionTopicFollowDal
    {
        public static List<DiscussionTopicFollowEntity> GetByDiscussionTopicId(long discussionTopicId)
        {
            List<DiscussionTopicFollowEntity> returnValue;
            using (var db = new CmsSocialNetworkDb())
            {
                returnValue = db.DiscussionTopicFollowMainDal.GetByDiscussionTopicId(discussionTopicId);
            }
            return returnValue;
        }
        public static List<DiscussionTopicFollowEntity> GetByListOfDiscussionTopicId(string discussionTopicIds)
        {
            List<DiscussionTopicFollowEntity> returnValue;
            using (var db = new CmsSocialNetworkDb())
            {
                returnValue = db.DiscussionTopicFollowMainDal.GetByListOfDiscussionTopicId(discussionTopicIds);
            }
            return returnValue;
        }

        public static bool FollowTopic(long discussionTopicId, string username)
        {
            bool returnValue;
            using (var db = new CmsSocialNetworkDb())
            {
                returnValue = db.DiscussionTopicFollowMainDal.FollowTopic(discussionTopicId, username);
            }
            return returnValue;
        }
        public static bool UnfollowTopic(long discussionTopicId, string username)
        {
            bool returnValue;
            using (var db = new CmsSocialNetworkDb())
            {
                returnValue = db.DiscussionTopicFollowMainDal.UnfollowTopic(discussionTopicId, username);
            }
            return returnValue;
        }
    }
}
