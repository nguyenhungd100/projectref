﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using ChannelVN.CMS.Common;
using ChannelVN.SocialNetwork.DAL.Common;
using ChannelVN.SocialNetwork.Entity;
using ChannelVN.SocialNetwork.MainDal.Databases;

namespace ChannelVN.SocialNetwork.DAL
{
    public class DiscussionDal
    {
        public static long Insert(DiscussionEntity discussion, string discussionTags)
        {
            long returnValue;
            using (var db = new CmsSocialNetworkDb())
            {
                returnValue = db.DiscussionMainDal.Insert(discussion, discussionTags);
            }
            return returnValue;
        }

        public static bool UpdatePriority(string listDiscussionId)
        {
            bool returnValue;
            using (var db = new CmsSocialNetworkDb())
            {
                returnValue = db.DiscussionMainDal.UpdatePriority(listDiscussionId);
            }
            return returnValue;
        }
        public static List<DiscussionEntity> GetByApplicationIdAndObjectId(int applicationId, long objectId, string userDoAction, int pageIndex, int pageSize, ref int totalRow)
        {
            List<DiscussionEntity> returnValue;
            using (var db = new CmsSocialNetworkDb())
            {
                returnValue = db.DiscussionMainDal.GetByApplicationIdAndObjectId(applicationId, objectId, userDoAction, pageIndex, pageSize, ref totalRow);
            }
            return returnValue;
        }
        public static List<DiscussionEntity> GetByDiscussionTopicId(long discussionTopicId, long parentDiscussionId, string userDoAction, int pageIndex, int pageSize, ref int totalRow)
        {
            List<DiscussionEntity> returnValue;
            using (var db = new CmsSocialNetworkDb())
            {
                returnValue = db.DiscussionMainDal.GetByDiscussionTopicId(discussionTopicId, parentDiscussionId, userDoAction, pageIndex, pageSize, ref totalRow);
            }
            return returnValue;
        }
        public static List<DiscussionEntity> GetTopDiscussionByListDiscussionTopicId(int topDiscussion, string listDiscussionTopicId, string userDoAction)
        {
            List<DiscussionEntity> returnValue;
            using (var db = new CmsSocialNetworkDb())
            {
                returnValue = db.DiscussionMainDal.GetTopDiscussionByListDiscussionTopicId(topDiscussion, listDiscussionTopicId, userDoAction);
            }
            return returnValue;
        }
        public static List<DiscussionEntity> GetTopDiscussionForNews(int topDiscussion, string listApplicationId, string userDoAction, int topicStatus)
        {
            List<DiscussionEntity> returnValue;
            using (var db = new CmsSocialNetworkDb())
            {
                returnValue = db.DiscussionMainDal.GetTopDiscussionForNews(topDiscussion, listApplicationId, userDoAction, topicStatus);
            }
            return returnValue;
        }
        public static int CountDiscussionByApplicationIdAndObjectId(int applicationId, long objectId, string userDoAction)
        {
            int returnValue;
            using (var db = new CmsSocialNetworkDb())
            {
                returnValue = db.DiscussionMainDal.CountDiscussionByApplicationIdAndObjectId(applicationId, objectId, userDoAction);
            }
            return returnValue;
        }
        public static int CountDiscussionByTopicId(long topicId)
        {
            int returnValue;
            using (var db = new CmsSocialNetworkDb())
            {
                returnValue = db.DiscussionMainDal.CountDiscussionByTopicId(topicId);
            }
            return returnValue;
        }
        public static DiscussionEntity GetByDiscussionId(long discussionTopicId, string userDoAction)
        {
            DiscussionEntity returnValue;
            using (var db = new CmsSocialNetworkDb())
            {
                returnValue = db.DiscussionMainDal.GetByDiscussionId(discussionTopicId, userDoAction);
            }
            return returnValue;
        }
    }
}
