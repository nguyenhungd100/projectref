﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using ChannelVN.CMS.Common;
using ChannelVN.SocialNetwork.DAL.Common;
using ChannelVN.SocialNetwork.Entity;
using ChannelVN.SocialNetwork.MainDal.Databases;

namespace ChannelVN.SocialNetwork.DAL
{
    public class ActivityDal
    {
        #region Sets

        public static long Insert(ActivityEntity activityEntity)
        {
            long returnValue;
            using (var db = new CmsSocialNetworkDb())
            {
                returnValue = db.ActivityMainDal.Insert(activityEntity);
            }
            return returnValue;
        }

        public static bool Delete(long activityId, string accountName)
        {
            bool returnValue;
            using (var db = new CmsSocialNetworkDb())
            {
                returnValue = db.ActivityMainDal.Delete(activityId, accountName);
            }
            return returnValue;
        }

        #endregion

        #region Gets

        public static List<ActivityEntity> GetTopLatest(int top, long applicationId, int type)
        {
            List<ActivityEntity> returnValue;
            using (var db = new CmsSocialNetworkDb())
            {
                returnValue = db.ActivityMainDal.GetTopLatest(top, applicationId, type);
            }
            return returnValue;
        }

        public static List<ActivityEntity> GetTopLatest(int top, int actionTypeDetail, int type)
        {
            List<ActivityEntity> returnValue;
            using (var db = new CmsSocialNetworkDb())
            {
                returnValue = db.ActivityMainDal.GetTopLatest(top, actionTypeDetail, type);
            }
            return returnValue;
        }

        public static List<ActivityEntity> GetTopLatest(int top, string actionTypeDetailList, int type)
        {
            List<ActivityEntity> returnValue;
            using (var db = new CmsSocialNetworkDb())
            {
                returnValue = db.ActivityMainDal.GetTopLatest(top, actionTypeDetailList, type);
            }
            return returnValue;
        }

        public static List<ActivityEntity> Search(int pageIndex, int pageSize, long applicationId, int actionTypeDetail, int type, DateTime dateFrom, DateTime dateTo, ref int totalRows)
        {
            List<ActivityEntity> returnValue;
            using (var db = new CmsSocialNetworkDb())
            {
                returnValue = db.ActivityMainDal.Search(pageIndex, pageSize, applicationId, actionTypeDetail, type, dateFrom, dateTo, ref totalRows);
            }
            return returnValue;
        }

        #endregion
    }
}
