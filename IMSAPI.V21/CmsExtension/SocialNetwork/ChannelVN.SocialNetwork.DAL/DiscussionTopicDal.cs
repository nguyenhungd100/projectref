﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using ChannelVN.CMS.Common;
using ChannelVN.SocialNetwork.DAL.Common;
using ChannelVN.SocialNetwork.Entity;
using ChannelVN.SocialNetwork.MainDal.Databases;

namespace ChannelVN.SocialNetwork.DAL
{
    public class DiscussionTopicDal
    {
        public static long Insert(DiscussionTopicEntity discussionTopic, string discussionTopicTags)
        {
            long returnValue;
            using (var db = new CmsSocialNetworkDb())
            {
                returnValue = db.DiscussionTopicMainDal.Insert(discussionTopic, discussionTopicTags);
            }
            return returnValue;
        }
        public static bool Update(DiscussionTopicEntity discussionTopic, string discussionTopicTags)
        {
            bool returnValue;
            using (var db = new CmsSocialNetworkDb())
            {
                returnValue = db.DiscussionTopicMainDal.Update(discussionTopic, discussionTopicTags);
            }
            return returnValue;
        }
        public static bool UpdatePriority(string listDiscussionTopicId)
        {
            bool returnValue;
            using (var db = new CmsSocialNetworkDb())
            {
                returnValue = db.DiscussionTopicMainDal.UpdatePriority(listDiscussionTopicId);
            }
            return returnValue;
        }
        public static bool ResolveTopic(long discussionTopicId, string userDoAction)
        {
            bool returnValue;
            using (var db = new CmsSocialNetworkDb())
            {
                returnValue = db.DiscussionTopicMainDal.ResolveTopic(discussionTopicId, userDoAction);
            }
            return returnValue;
        }
        public static bool DeleteTopic(long discussionTopicId, string userDoAction)
        {
            bool returnValue;
            using (var db = new CmsSocialNetworkDb())
            {
                returnValue = db.DiscussionTopicMainDal.DeleteTopic(discussionTopicId, userDoAction);
            }
            return returnValue;
        }
        public static List<DiscussionTopicEntity> GetByApplicationIdAndObjectId(int applicationId, long parentId, long objectId, string userDoAction, string keyword, string zoneIds, int status, int isFocus, int pageIndex, int pageSize, ref int totalRow)
        {
            List<DiscussionTopicEntity> returnValue;
            using (var db = new CmsSocialNetworkDb())
            {
                returnValue = db.DiscussionTopicMainDal.GetByApplicationIdAndObjectId(applicationId, parentId, objectId, userDoAction, keyword, zoneIds, status, isFocus, pageIndex, pageSize, ref totalRow);
            }
            return returnValue;
        }
        public static List<DiscussionTopicEntity> GetTopTopicByApplicationIdAndObjectId(int topTopic, string listApplicationId, long objectId, string userDoAction, int status)
        {
            List<DiscussionTopicEntity> returnValue;
            using (var db = new CmsSocialNetworkDb())
            {
                returnValue = db.DiscussionTopicMainDal.GetTopTopicByApplicationIdAndObjectId(topTopic, listApplicationId, objectId, userDoAction, status);
            }
            return returnValue;
        }
        public static List<DiscussionTopicEntity> GetTopTopicForNews(int topTopic, string listApplicationId, string userDoAction, int status)
        {
            List<DiscussionTopicEntity> returnValue;
            using (var db = new CmsSocialNetworkDb())
            {
                returnValue = db.DiscussionTopicMainDal.GetTopTopicForNews(topTopic, listApplicationId, userDoAction, status);
            }
            return returnValue;
        }
        public static DiscussionTopicEntity GetByDiscussionTopicId(long discussionTopicId, string userDoAction)
        {
            DiscussionTopicEntity returnValue;
            using (var db = new CmsSocialNetworkDb())
            {
                returnValue = db.DiscussionTopicMainDal.GetByDiscussionTopicId(discussionTopicId, userDoAction);
            }
            return returnValue;
        }
        public static int CountDiscussionTopicByApplicationIdAndObjectId(int applicationId, long objectId, string userDoAction, string keyword, int zoneId, int status)
        {
            int returnValue;
            using (var db = new CmsSocialNetworkDb())
            {
                returnValue = db.DiscussionTopicMainDal.CountDiscussionTopicByApplicationIdAndObjectId(applicationId, objectId, userDoAction, keyword, zoneId, status);
            }
            return returnValue;
        }
    }
}
