﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using ChannelVN.CMS.Common;
using ChannelVN.SocialNetwork.DAL.Common;
using ChannelVN.SocialNetwork.Entity;
using ChannelVN.SocialNetwork.MainDal.Databases;

namespace ChannelVN.SocialNetwork.DAL
{
    public class DiscussionAttachmentDal
    {
        public static bool Insert(DiscussionAttachmentEntity discussionAttachment)
        {
            bool returnValue;
            using (var db = new CmsSocialNetworkDb())
            {
                returnValue = db.DiscussionAttachmentMainDal.Insert(discussionAttachment);
            }
            return returnValue;
        }
        public static bool Delete(long discussionAttachmentId)
        {
            bool returnValue;
            using (var db = new CmsSocialNetworkDb())
            {
                returnValue = db.DiscussionAttachmentMainDal.Delete(discussionAttachmentId);
            }
            return returnValue;
        }
        public static bool DeleteByTopicAndDiscussion(long discussionTopicId, long discussionId)
        {
            bool returnValue;
            using (var db = new CmsSocialNetworkDb())
            {
                returnValue = db.DiscussionAttachmentMainDal.DeleteByTopicAndDiscussion(discussionTopicId, discussionId);
            }
            return returnValue;
        }
        public static List<DiscussionAttachmentEntity> GetByDiscussionTopicId(long discussionTopicId)
        {
            List<DiscussionAttachmentEntity> returnValue;
            using (var db = new CmsSocialNetworkDb())
            {
                returnValue = db.DiscussionAttachmentMainDal.GetByDiscussionTopicId(discussionTopicId);
            }
            return returnValue;
        }
        public static List<DiscussionAttachmentEntity> GetByDiscussionId(long discussionId)
        {
            List<DiscussionAttachmentEntity> returnValue;
            using (var db = new CmsSocialNetworkDb())
            {
                returnValue = db.DiscussionAttachmentMainDal.GetByDiscussionId(discussionId);
            }
            return returnValue;
        }
    }
}
