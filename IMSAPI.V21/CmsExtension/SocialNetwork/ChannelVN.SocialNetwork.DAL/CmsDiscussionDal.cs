﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using ChannelVN.CMS.Common;
using ChannelVN.SocialNetwork.DAL.Common;
using ChannelVN.SocialNetwork.Entity;
using ChannelVN.SocialNetwork.MainDal.Databases;

namespace ChannelVN.SocialNetwork.DAL
{
    public class CmsDiscussionDal
    {
        public static List<CmsDiscussionEntity> GetByApplicationAndObjectId(int applicationId, long objectId, long parentDiscussionId, int pageIndex, int pageSize, ref int totalRow)
        {
            List<CmsDiscussionEntity> returnValue;
            using (var db = new CmsSocialNetworkDb())
            {
                returnValue = db.CmsDiscussionMainDal.GetByApplicationAndObjectId(applicationId, objectId, parentDiscussionId, pageIndex, pageSize, ref totalRow);
            }
            return returnValue;
        }
        public static List<CmsDiscussionEntity> GetByApplicationAndObjectData(int applicationId, string objectData, long parentDiscussionId, int pageIndex, int pageSize, ref int totalRow)
        {
            List<CmsDiscussionEntity> returnValue;
            using (var db = new CmsSocialNetworkDb())
            {
                returnValue = db.CmsDiscussionMainDal.GetByApplicationAndObjectData(applicationId, objectData, parentDiscussionId, pageIndex, pageSize, ref totalRow);
            }
            return returnValue;
        }
        public static CmsDiscussionEntity GetByDiscussionId(long discussionId)
        {
            CmsDiscussionEntity returnValue;
            using (var db = new CmsSocialNetworkDb())
            {
                returnValue = db.CmsDiscussionMainDal.GetByDiscussionId(discussionId);
            }
            return returnValue;
        }

        public static bool SendDiscussion(CmsDiscussionEntity discussion, ref long newDiscussionId)
        {
            bool returnValue;
            using (var db = new CmsSocialNetworkDb())
            {
                returnValue = db.CmsDiscussionMainDal.SendDiscussion(discussion, ref newDiscussionId);
            }
            return returnValue;
        }
    }
}
