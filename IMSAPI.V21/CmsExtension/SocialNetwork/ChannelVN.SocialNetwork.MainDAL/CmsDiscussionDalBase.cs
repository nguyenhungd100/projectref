﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using ChannelVN.CMS.Common;

using ChannelVN.SocialNetwork.Entity;
using ChannelVN.SocialNetwork.MainDal.Databases;

namespace ChannelVN.SocialNetwork.MainDAL
{
    public abstract class CmsDiscussionDalBase
    {
        public List<CmsDiscussionEntity> GetByApplicationAndObjectId(int applicationId, long objectId, long parentDiscussionId, int pageIndex, int pageSize, ref int totalRow)
        {
            const string commandText = "CMS_CmsDiscussion_GetByApplicationIdAndObjectId";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "applicationId", applicationId);
                _db.AddParameter(cmd, "objectId", objectId);
                _db.AddParameter(cmd, "ParentDiscussionId", parentDiscussionId);
                _db.AddParameter(cmd, "PageIndex", pageIndex);
                _db.AddParameter(cmd, "PageSize", pageSize);
                _db.AddParameter(cmd, "TotalRow", totalRow, ParameterDirection.Output);
                var numberOfRow = _db.GetList<CmsDiscussionEntity>(cmd);
                totalRow = Utility.ConvertToInt(cmd.Parameters[cmd.Parameters.Count - 1]);
                return numberOfRow;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public List<CmsDiscussionEntity> GetByApplicationAndObjectData(int applicationId, string objectData, long parentDiscussionId, int pageIndex, int pageSize, ref int totalRow)
        {
            const string commandText = "CMS_CmsDiscussion_GetByApplicationIdAndObjectData";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "ApplicationId", applicationId);
                _db.AddParameter(cmd, "ObjectData", objectData);
                _db.AddParameter(cmd, "ParentDiscussionId", parentDiscussionId);
                _db.AddParameter(cmd, "PageIndex", pageIndex);
                _db.AddParameter(cmd, "PageSize", pageSize);
                _db.AddParameter(cmd, "TotalRow", totalRow, ParameterDirection.Output);
                var numberOfRow = _db.GetList<CmsDiscussionEntity>(cmd);
                totalRow = Utility.ConvertToInt(cmd.Parameters[cmd.Parameters.Count - 1]);
                return numberOfRow;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public CmsDiscussionEntity GetByDiscussionId(long discussionId)
        {
            const string commandText = "CMS_CmsDiscussion_GetById";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", discussionId);
                var numberOfRow = _db.Get<CmsDiscussionEntity>(cmd);
                return numberOfRow;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public bool SendDiscussion(CmsDiscussionEntity discussion, ref long newDiscussionId)
        {
            const string commandText = "CMS_CmsDiscussion_SendDiscussion";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "ParentDiscussionId", discussion.ParentDiscussionId);
                _db.AddParameter(cmd, "ApplicationId", discussion.ApplicationId);
                _db.AddParameter(cmd, "ObjectId", discussion.ObjectId);
                _db.AddParameter(cmd, "ObjectData", discussion.ObjectData);
                _db.AddParameter(cmd, "DiscussionContent", discussion.DiscussionContent);
                _db.AddParameter(cmd, "CreatedBy", discussion.CreatedBy);
                _db.AddParameter(cmd, "Id", newDiscussionId, ParameterDirection.Output);
                var numberOfRow = _db.ExecuteNonQuery(cmd);
                newDiscussionId = Utility.ConvertToInt(cmd.Parameters[cmd.Parameters.Count - 1]);
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        #region Core members

        private readonly CmsSocialNetworkDb _db;

        protected CmsDiscussionDalBase(CmsSocialNetworkDb db)
        {
            _db = db;
        }

        protected CmsSocialNetworkDb Database
        {
            get { return _db; }
        }

        #endregion
    }
}
