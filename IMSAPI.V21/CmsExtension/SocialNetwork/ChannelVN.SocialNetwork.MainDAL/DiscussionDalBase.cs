﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using ChannelVN.CMS.Common;

using ChannelVN.SocialNetwork.Entity;
using ChannelVN.SocialNetwork.MainDal.Common;
using ChannelVN.SocialNetwork.MainDal.Databases;

namespace ChannelVN.SocialNetwork.MainDAL
{
    public abstract class DiscussionDalBase
    {
        public long Insert(DiscussionEntity discussion, string discussionTags)
        {
            const string commandText = "Social_Discussion_Insert";
            try
            {
                var dicussionId = 0L;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", dicussionId, ParameterDirection.Output);
                _db.AddParameter(cmd, "TopicId", discussion.TopicId);
                _db.AddParameter(cmd, "ParentId", discussion.ParentId);
                _db.AddParameter(cmd, "Detail", discussion.Detail);
                _db.AddParameter(cmd, "CreatedBy", discussion.CreatedBy);
                if (discussion.CreatedDate <= Constants.MinDateTime)
                    _db.AddParameter(cmd, "CreatedDate", DateTime.Now);
                else _db.AddParameter(cmd, "CreatedDate", discussion.CreatedDate);
                _db.AddParameter(cmd, "Data", discussion.Data);
                _db.AddParameter(cmd, "Priority", discussion.Priority);
                _db.AddParameter(cmd, "DiscussionTags", discussionTags);
                var numberOfRow = _db.ExecuteNonQuery(cmd);
                dicussionId = Utility.ConvertToInt(cmd.Parameters[0]);
                return dicussionId;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public bool UpdatePriority(string listDiscussionId)
        {
            const string commandText = "Social_Discussion_UpdatePriority";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "LisDiscussionId", listDiscussionId);
                var numberOfRow = _db.ExecuteNonQuery(cmd);
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public List<DiscussionEntity> GetByApplicationIdAndObjectId(int applicationId, long objectId, string userDoAction, int pageIndex, int pageSize, ref int totalRow)
        {
            const string commandText = "Social_Discussion_GetByApplicationIdAndObjectId";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "ApplicationId", applicationId);
                _db.AddParameter(cmd, "ObjectId", objectId);
                _db.AddParameter(cmd, "UserDoAction", userDoAction);
                _db.AddParameter(cmd, "PageIndex", pageIndex);
                _db.AddParameter(cmd, "PageSize", pageSize);
                _db.AddParameter(cmd, "TotalRow", totalRow, ParameterDirection.Output);
                var numberOfRow = _db.GetList<DiscussionEntity>(cmd);
                totalRow = Utility.ConvertToInt(cmd.Parameters[cmd.Parameters.Count - 1]);
                return numberOfRow;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public List<DiscussionEntity> GetByDiscussionTopicId(long discussionTopicId, long parentDiscussionId, string userDoAction, int pageIndex, int pageSize, ref int totalRow)
        {
            const string commandText = "Social_Discussion_GetByDiscussionTopicId";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "DiscussionTopicId", discussionTopicId);
                _db.AddParameter(cmd, "ParentDiscussionId", parentDiscussionId);
                _db.AddParameter(cmd, "UserDoAction", userDoAction);
                _db.AddParameter(cmd, "PageIndex", pageIndex);
                _db.AddParameter(cmd, "PageSize", pageSize);
                _db.AddParameter(cmd, "TotalRow", totalRow, ParameterDirection.Output);
                var numberOfRow = _db.GetList<DiscussionEntity>(cmd);
                totalRow = Utility.ConvertToInt(cmd.Parameters[cmd.Parameters.Count - 1]);
                return numberOfRow;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public List<DiscussionEntity> GetTopDiscussionByListDiscussionTopicId(int topDiscussion, string listDiscussionTopicId, string userDoAction)
        {
            const string commandText = "Social_Discussion_GetTopDiscussionByListDiscussionTopicId";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "TopDiscussion", topDiscussion);
                _db.AddParameter(cmd, "ListDiscussionTopicId", listDiscussionTopicId);
                _db.AddParameter(cmd, "UserDoAction", userDoAction);
                var numberOfRow = _db.GetList<DiscussionEntity>(cmd);
                return numberOfRow;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public List<DiscussionEntity> GetTopDiscussionForNews(int topDiscussion, string listApplicationId, string userDoAction, int topicStatus)
        {
            const string commandText = "Social_Discussion_GetTopDiscussionForNews";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "TopDiscussion", topDiscussion);
                _db.AddParameter(cmd, "ListApplicationId", listApplicationId);
                _db.AddParameter(cmd, "UserDoAction", userDoAction);
                _db.AddParameter(cmd, "TopicStatus", topicStatus);
                var numberOfRow = _db.GetList<DiscussionEntity>(cmd);
                return numberOfRow;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public int CountDiscussionByApplicationIdAndObjectId(int applicationId, long objectId, string userDoAction)
        {
            const string commandText = "Social_Discussion_CountByApplicationIdAndObjectId";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "ApplicationId", applicationId);
                _db.AddParameter(cmd, "ObjectId", objectId);
                _db.AddParameter(cmd, "UserDoAction", userDoAction);
                var numberOfRow = _db.GetFirtDataRecord(cmd);
                return Utility.ConvertToInt(numberOfRow);
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public int CountDiscussionByTopicId(long topicId)
        {
            const string commandText = "Social_Discussion_CountByTopicId";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "TopicId", topicId);
                var numberOfRow = _db.GetFirtDataRecord(cmd);
                return Utility.ConvertToInt(numberOfRow);
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public DiscussionEntity GetByDiscussionId(long discussionTopicId, string userDoAction)
        {
            const string commandText = "Social_Discussion_GetByDiscussionId";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "DiscussionId", discussionTopicId);
                _db.AddParameter(cmd, "UserDoAction", userDoAction);
                var numberOfRow = _db.Get<DiscussionEntity>(cmd);
                return numberOfRow;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        #region Core members

        private readonly CmsSocialNetworkDb _db;

        protected DiscussionDalBase(CmsSocialNetworkDb db)
        {
            _db = db;
        }

        protected CmsSocialNetworkDb Database
        {
            get { return _db; }
        }

        #endregion
    }
}
