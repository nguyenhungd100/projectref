﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using ChannelVN.CMS.Common;

using ChannelVN.SocialNetwork.Entity;
using ChannelVN.SocialNetwork.MainDal.Common;
using ChannelVN.SocialNetwork.MainDal.Databases;

namespace ChannelVN.SocialNetwork.MainDAL
{
    public abstract class DiscussionAttachmentDalBase
    {
        public bool Insert(DiscussionAttachmentEntity discussionAttachment)
        {
            const string commandText = "Social_DiscussionAttachment_Insert";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "DiscussionTopicId", discussionAttachment.DiscussionTopicId);
                _db.AddParameter(cmd, "DiscussionId", discussionAttachment.DiscussionId);
                _db.AddParameter(cmd, "FileName", discussionAttachment.FileName);
                _db.AddParameter(cmd, "FileUrl", discussionAttachment.FileUrl);
                _db.AddParameter(cmd, "Size", discussionAttachment.Size);
                _db.AddParameter(cmd, "CreatedBy", discussionAttachment.CreatedBy);
                if (discussionAttachment.CreatedDate <= Constants.MinDateTime)
                    _db.AddParameter(cmd, "CreatedDate", DateTime.Now);
                else _db.AddParameter(cmd, "CreatedDate", discussionAttachment.CreatedDate);
                var numberOfRow = _db.ExecuteNonQuery(cmd);
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool Delete(long discussionAttachmentId)
        {
            const string commandText = "Social_DiscussionAttachment_Delete";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", discussionAttachmentId);
                var numberOfRow = _db.ExecuteNonQuery(cmd);
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool DeleteByTopicAndDiscussion(long discussionTopicId, long discussionId)
        {
            const string commandText = "Social_DiscussionAttachment_DeleteByTopicAndDiscussion";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "DiscussionTopicId", discussionTopicId);
                _db.AddParameter(cmd, "DiscussionId", discussionId);
                var numberOfRow = _db.ExecuteNonQuery(cmd);
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public List<DiscussionAttachmentEntity> GetByDiscussionTopicId(long discussionTopicId)
        {
            const string commandText = "Social_DiscussionAttachment_GetByDiscussionTopicId";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "DiscussionTopicId", discussionTopicId);
                var numberOfRow = _db.GetList<DiscussionAttachmentEntity>(cmd);
                return numberOfRow;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public List<DiscussionAttachmentEntity> GetByDiscussionId(long discussionId)
        {
            const string commandText = "Social_DiscussionAttachment_GetByDiscussionId";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "DiscussionId", discussionId);
                var numberOfRow = _db.GetList<DiscussionAttachmentEntity>(cmd);
                return numberOfRow;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        #region Core members

        private readonly CmsSocialNetworkDb _db;

        protected DiscussionAttachmentDalBase(CmsSocialNetworkDb db)
        {
            _db = db;
        }

        protected CmsSocialNetworkDb Database
        {
            get { return _db; }
        }

        #endregion
    }
}
