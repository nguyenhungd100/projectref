﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using ChannelVN.CMS.Common;

using ChannelVN.SocialNetwork.Entity;
using ChannelVN.SocialNetwork.MainDal.Databases;

namespace ChannelVN.SocialNetwork.MainDAL
{
    public abstract class DiscussionTopicFollowDalBase
    {
        public List<DiscussionTopicFollowEntity> GetByDiscussionTopicId(long discussionTopicId)
        {
            const string commandText = "Social_DiscussionTopicFollow_GetByDiscussionTopicId";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "DiscussionTopicId", discussionTopicId);
                var numberOfRow = _db.GetList<DiscussionTopicFollowEntity>(cmd);
                return numberOfRow;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public List<DiscussionTopicFollowEntity> GetByListOfDiscussionTopicId(string discussionTopicIds)
        {
            const string commandText = "Social_DiscussionTopicFollow_GetByListOfDiscussionTopicId";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "DiscussionTopicIds", discussionTopicIds);
                var numberOfRow = _db.GetList<DiscussionTopicFollowEntity>(cmd);
                return numberOfRow;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public bool FollowTopic(long discussionTopicId, string username)
        {
            const string commandText = "Social_DiscussionTopicFollow_FollowTopic";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "DiscussionTopicId", discussionTopicId);
                _db.AddParameter(cmd, "FollowBy", username);
                var numberOfRow = _db.ExecuteNonQuery(cmd);
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool UnfollowTopic(long discussionTopicId, string username)
        {
            const string commandText = "Social_DiscussionTopicFollow_UnfollowTopic";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "DiscussionTopicId", discussionTopicId);
                _db.AddParameter(cmd, "FollowBy", username);
                var numberOfRow = _db.ExecuteNonQuery(cmd);
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        #region Core members

        private readonly CmsSocialNetworkDb _db;

        protected DiscussionTopicFollowDalBase(CmsSocialNetworkDb db)
        {
            _db = db;
        }

        protected CmsSocialNetworkDb Database
        {
            get { return _db; }
        }

        #endregion
    }
}
