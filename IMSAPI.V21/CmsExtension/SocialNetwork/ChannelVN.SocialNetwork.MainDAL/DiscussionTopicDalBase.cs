﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using ChannelVN.CMS.Common;

using ChannelVN.SocialNetwork.Entity;
using ChannelVN.SocialNetwork.MainDal.Common;
using ChannelVN.SocialNetwork.MainDal.Databases;

namespace ChannelVN.SocialNetwork.MainDAL
{
    public abstract class DiscussionTopicDalBase
    {
        public long Insert(DiscussionTopicEntity discussionTopic, string discussionTopicTags)
        {
            const string commandText = "Social_DiscussionTopic_Insert";
            try
            {
                var discussionTopicId = 0L;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", discussionTopicId, ParameterDirection.Output);
                _db.AddParameter(cmd, "ZoneId", discussionTopic.ZoneId);
                _db.AddParameter(cmd, "ApplicationId", discussionTopic.ApplicationId);
                _db.AddParameter(cmd, "ObjectId", discussionTopic.ObjectId);
                _db.AddParameter(cmd, "ObjectTitle", discussionTopic.ObjectTitle);
                _db.AddParameter(cmd, "ObjectData", discussionTopic.ObjectData);
                _db.AddParameter(cmd, "CreatedBy", discussionTopic.CreatedBy);
                if (discussionTopic.CreatedDate <= Constants.MinDateTime)
                    _db.AddParameter(cmd, "CreatedDate", DateTime.Now);
                else _db.AddParameter(cmd, "CreatedDate", discussionTopic.CreatedDate);
                _db.AddParameter(cmd, "IsPrivate", discussionTopic.IsPrivate);
                _db.AddParameter(cmd, "IsFocus", discussionTopic.IsFocus);
                _db.AddParameter(cmd, "TopicNote", discussionTopic.TopicNote);
                _db.AddParameter(cmd, "TopicContent", discussionTopic.TopicContent);
                _db.AddParameter(cmd, "TopicPriority", discussionTopic.TopicPriority);
                _db.AddParameter(cmd, "DiscussionTopicTags", discussionTopicTags);
                _db.AddParameter(cmd, "ParentId", discussionTopic.ParentId);
                var numberOfRow = _db.ExecuteNonQuery(cmd);
                discussionTopicId = Utility.ConvertToInt(cmd.Parameters[0]);
                return discussionTopicId;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool Update(DiscussionTopicEntity discussionTopic, string discussionTopicTags)
        {
            const string commandText = "Social_DiscussionTopic_Update";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", discussionTopic.Id);
                _db.AddParameter(cmd, "ZoneId", discussionTopic.ZoneId);
                _db.AddParameter(cmd, "ApplicationId", discussionTopic.ApplicationId);
                _db.AddParameter(cmd, "ObjectId", discussionTopic.ObjectId);
                _db.AddParameter(cmd, "ObjectTitle", discussionTopic.ObjectTitle);
                _db.AddParameter(cmd, "ObjectData", discussionTopic.ObjectData);
                _db.AddParameter(cmd, "IsPrivate", discussionTopic.IsPrivate);
                _db.AddParameter(cmd, "IsFocus", discussionTopic.IsFocus);
                _db.AddParameter(cmd, "TopicContent", discussionTopic.TopicContent);
                _db.AddParameter(cmd, "TopicPriority", discussionTopic.TopicPriority);
                _db.AddParameter(cmd, "DiscussionTopicTags", discussionTopicTags);
                _db.AddParameter(cmd, "TopicNote", discussionTopic.TopicNote);
                _db.AddParameter(cmd, "ParentId", discussionTopic.ParentId);
                var numberOfRow = _db.ExecuteNonQuery(cmd);
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool UpdatePriority(string listDiscussionTopicId)
        {
            const string commandText = "Social_DiscussionTopic_UpdatePriority";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "LisDiscussionTopicId", listDiscussionTopicId);
                var numberOfRow = _db.ExecuteNonQuery(cmd);
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool ResolveTopic(long discussionTopicId, string userDoAction)
        {
            const string commandText = "Social_DiscussionTopic_Resolved";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "DiscussionTopicId", discussionTopicId);
                _db.AddParameter(cmd, "UserDoAction", userDoAction);
                var numberOfRow = _db.ExecuteNonQuery(cmd);
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool DeleteTopic(long discussionTopicId, string userDoAction)
        {
            const string commandText = "Social_DiscussionTopic_Delete";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "DiscussionTopicId", discussionTopicId);
                _db.AddParameter(cmd, "UserDoAction", userDoAction);
                var numberOfRow = _db.ExecuteNonQuery(cmd);
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public List<DiscussionTopicEntity> GetByApplicationIdAndObjectId(int applicationId, long parentId, long objectId, string userDoAction, string keyword, string zoneIds, int status, int isFocus, int pageIndex, int pageSize, ref int totalRow)
        {
            const string commandText = "Social_DiscussionTopic_GetByApplicationIdAndObjectId";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "ApplicationId", applicationId);
                _db.AddParameter(cmd, "ObjectId", objectId);
                _db.AddParameter(cmd, "UserDoAction", userDoAction);
                _db.AddParameter(cmd, "Keyword", keyword);
                _db.AddParameter(cmd, "ZoneIds", zoneIds);
                _db.AddParameter(cmd, "Status", status);
                _db.AddParameter(cmd, "ParentId", parentId);
                _db.AddParameter(cmd, "IsFocus", isFocus);
                _db.AddParameter(cmd, "PageIndex", pageIndex);
                _db.AddParameter(cmd, "PageSize", pageSize);
                _db.AddParameter(cmd, "TotalRow", totalRow, ParameterDirection.Output);
                var numberOfRow = _db.GetList<DiscussionTopicEntity>(cmd);
                totalRow = Utility.ConvertToInt(cmd.Parameters[cmd.Parameters.Count - 1]);
                return numberOfRow;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public List<DiscussionTopicEntity> GetTopTopicByApplicationIdAndObjectId(int topTopic, string listApplicationId, long objectId, string userDoAction, int status)
        {
            const string commandText = "Social_DiscussionTopic_GetTopTopicByApplicationIdAndObjectId";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "TopTopic", topTopic);
                _db.AddParameter(cmd, "ListApplicationId", listApplicationId);
                _db.AddParameter(cmd, "ObjectId", objectId);
                _db.AddParameter(cmd, "UserDoAction", userDoAction);
                _db.AddParameter(cmd, "Status", status);
                var numberOfRow = _db.GetList<DiscussionTopicEntity>(cmd);
                return numberOfRow;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public List<DiscussionTopicEntity> GetTopTopicForNews(int topTopic, string listApplicationId, string userDoAction, int status)
        {
            const string commandText = "Social_DiscussionTopic_GetTopTopicForNews";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "TopTopic", topTopic);
                _db.AddParameter(cmd, "ListApplicationId", listApplicationId);
                _db.AddParameter(cmd, "UserDoAction", userDoAction);
                _db.AddParameter(cmd, "Status", status);
                var numberOfRow = _db.GetList<DiscussionTopicEntity>(cmd);
                return numberOfRow;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public DiscussionTopicEntity GetByDiscussionTopicId(long discussionTopicId, string userDoAction)
        {
            const string commandText = "Social_DiscussionTopic_GetByDiscussionTopicId";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "DiscussionTopicId", discussionTopicId);
                _db.AddParameter(cmd, "UserDoAction", userDoAction);
                var numberOfRow = _db.Get<DiscussionTopicEntity>(cmd);
                return numberOfRow;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public int CountDiscussionTopicByApplicationIdAndObjectId(int applicationId, long objectId, string userDoAction, string keyword, int zoneId, int status)
        {
            const string commandText = "Social_DiscussionTopic_CountByApplicationIdAndObjectId";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "ApplicationId", applicationId);
                _db.AddParameter(cmd, "ObjectId", objectId);
                _db.AddParameter(cmd, "UserDoAction", userDoAction);
                _db.AddParameter(cmd, "Keyword", keyword);
                _db.AddParameter(cmd, "ZoneId", zoneId);
                _db.AddParameter(cmd, "Status", status);
                var numberOfRow = _db.GetFirtDataRecord(cmd);
                return Utility.ConvertToInt(numberOfRow);
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        #region Core members

        private readonly CmsSocialNetworkDb _db;

        protected DiscussionTopicDalBase(CmsSocialNetworkDb db)
        {
            _db = db;
        }

        protected CmsSocialNetworkDb Database
        {
            get { return _db; }
        }

        #endregion
    }
}
