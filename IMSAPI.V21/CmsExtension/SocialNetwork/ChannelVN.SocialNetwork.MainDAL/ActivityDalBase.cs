﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using ChannelVN.CMS.Common;

using ChannelVN.SocialNetwork.Entity;
using ChannelVN.SocialNetwork.MainDal.Databases;

namespace ChannelVN.SocialNetwork.MainDAL
{
    public abstract class ActivityDalBase
    {
        #region Sets

        public long Insert(ActivityEntity activityEntity)
        {
            const string commandText = "Social_Activity_Insert";
            try
            {
                var activityId = 0L;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", activityId, ParameterDirection.Output);
                _db.AddParameter(cmd, "ApplicationId", activityEntity.ApplicationId);
                _db.AddParameter(cmd, "SourceId", activityEntity.SourceId);
                _db.AddParameter(cmd, "SourceName", activityEntity.SourceName);
                _db.AddParameter(cmd, "DestinationId", activityEntity.DestinationId);
                _db.AddParameter(cmd, "DestinationName", activityEntity.DestinationName);
                _db.AddParameter(cmd, "ActionText", activityEntity.ActionText);
                _db.AddParameter(cmd, "Message", activityEntity.Message);
                _db.AddParameter(cmd, "OwnerId", activityEntity.OwnerId);
                _db.AddParameter(cmd, "Type", activityEntity.Type);
                _db.AddParameter(cmd, "ActionTypeDetail", activityEntity.ActionTypeDetail);
                _db.AddParameter(cmd, "GroupTimeLine", Utility.SetPublishedDate());
                var numberOfRow = _db.ExecuteNonQuery(cmd);
                activityId = Utility.ConvertToInt(cmd.Parameters[0]);
                return activityId;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public bool Delete(long activityId, string accountName)
        {
            const string commandText = "Social_Activity_Delete";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", activityId);
                _db.AddParameter(cmd, "ApplicationId", accountName);
                var numberOfRow = _db.ExecuteNonQuery(cmd);
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        #endregion

        #region Gets

        public List<ActivityEntity> GetTopLatest(int top, long applicationId, int type)
        {
            const string commandText = "Social_Activity_GetTopLatest";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Top", top);
                _db.AddParameter(cmd, "ApplicationId", applicationId);
                _db.AddParameter(cmd, "Type", type);
                var numberOfRow = _db.GetList<ActivityEntity>(cmd);
                return numberOfRow;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public List<ActivityEntity> GetTopLatest(int top, int actionTypeDetail, int type)
        {
            const string commandText = "Social_Activity_GetTopLatestByAction";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Top", top);
                _db.AddParameter(cmd, "ActionTypeDetail", actionTypeDetail);
                _db.AddParameter(cmd, "Type", type); ;
                var numberOfRow = _db.GetList<ActivityEntity>(cmd);
                return numberOfRow;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public List<ActivityEntity> GetTopLatest(int top, string actionTypeDetailList, int type)
        {
            const string commandText = "Social_Activity_GetTopLatestByMultiAction";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Top", top);
                _db.AddParameter(cmd, "ActionTypeDetailList", actionTypeDetailList);
                _db.AddParameter(cmd, "Type", type);
                var numberOfRow = _db.GetList<ActivityEntity>(cmd);
                return numberOfRow;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public List<ActivityEntity> Search(int pageIndex, int pageSize, long applicationId, int actionTypeDetail, int type, DateTime dateFrom, DateTime dateTo, ref int totalRows)
        {
            const string commandText = "Social_Activity_Filter";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "ApplicationId", applicationId);
                _db.AddParameter(cmd, "ActionTypeDetail", actionTypeDetail);
                _db.AddParameter(cmd, "Type", type);
                if (dateFrom != DateTime.MinValue)
                    _db.AddParameter(cmd, "DateFrom", dateFrom);
                else _db.AddParameter(cmd, "DateFrom", DBNull.Value);
                if (dateTo != DateTime.MinValue) _db.AddParameter(cmd, "DateTo", dateTo); else _db.AddParameter(cmd, "DateTo", DBNull.Value);
                _db.AddParameter(cmd, "PageIndex", pageIndex);
                _db.AddParameter(cmd, "PageSize", pageSize);
                _db.AddParameter(cmd, "TotalRows", 0, ParameterDirection.Output);
                var numberOfRow = _db.GetList<ActivityEntity>(cmd);
                totalRows = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, cmd.Parameters.Count - 1));
                return numberOfRow;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        #endregion

        #region Core members

        private readonly CmsSocialNetworkDb _db;

        protected ActivityDalBase(CmsSocialNetworkDb db)
        {
            _db = db;
        }

        protected CmsSocialNetworkDb Database
        {
            get { return _db; }
        }

        #endregion
    }
}
