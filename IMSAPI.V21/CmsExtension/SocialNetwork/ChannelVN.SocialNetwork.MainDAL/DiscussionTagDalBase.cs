﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using ChannelVN.CMS.Common;

using ChannelVN.SocialNetwork.Entity;
using ChannelVN.SocialNetwork.MainDal.Databases;

namespace ChannelVN.SocialNetwork.MainDAL
{
    public abstract class DiscussionTagDalBase
    {
        public List<DiscussionTagEntity> GetByDiscussionTopicId(long discussionTopicId, bool includeDiscussionTag)
        {
            const string commandText = "Social_DiscussionTag_GetByDiscussionTopicId";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "DiscussionTopicId", discussionTopicId);
                _db.AddParameter(cmd, "IncludeDiscussionTag", includeDiscussionTag);
                var numberOfRow = _db.GetList<DiscussionTagEntity>(cmd);
                return numberOfRow;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public List<DiscussionTagEntity> GetByDiscussionId(long discussionId)
        {
            const string commandText = "Social_DiscussionTag_GetByDiscussionId";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "DiscussionId", discussionId);
                var numberOfRow = _db.GetList<DiscussionTagEntity>(cmd);
                return numberOfRow;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        #region Core members

        private readonly CmsSocialNetworkDb _db;

        protected DiscussionTagDalBase(CmsSocialNetworkDb db)
        {
            _db = db;
        }

        protected CmsSocialNetworkDb Database
        {
            get { return _db; }
        }

        #endregion
    }
}
