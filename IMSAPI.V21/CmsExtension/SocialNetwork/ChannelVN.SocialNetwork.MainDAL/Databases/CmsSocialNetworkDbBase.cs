﻿using System.Data;
using ChannelVN.CMS.Common.DalCommon;
using ChannelVN.SocialNetwork.MainDAL;

namespace ChannelVN.SocialNetwork.MainDal.Databases
{
    /// <summary>
    /// The base class for the <see cref="CmsMainDb"/> class that 
    /// represents a connection to the <c>CmsMainDb</c> database. 
    /// </summary>
    /// <remarks>
    /// Do not change this source code. Modify the CmsMainDb class
    /// if you need to add or change some functionality.
    /// </remarks>
    public abstract class CmsSocialNetworkDbBase : MainDbBase
    {
        #region Store procedures

        #region ProgramChannel

        private ActivityDal _activityMainDal;
        public ActivityDal ActivityMainDal
        {
            get { return _activityMainDal ?? (_activityMainDal = new ActivityDal((CmsSocialNetworkDb)this)); }
        }

        #endregion

        #region CmsDiscussion

        private CmsDiscussionDal _cmsDiscussionMainDal;
        public CmsDiscussionDal CmsDiscussionMainDal
        {
            get { return _cmsDiscussionMainDal ?? (_cmsDiscussionMainDal = new CmsDiscussionDal((CmsSocialNetworkDb)this)); }
        }

        #endregion

        #region DiscussionAttachment

        private DiscussionAttachmentDal _discussionAttachmentMainDal;
        public DiscussionAttachmentDal DiscussionAttachmentMainDal
        {
            get { return _discussionAttachmentMainDal ?? (_discussionAttachmentMainDal = new DiscussionAttachmentDal((CmsSocialNetworkDb)this)); }
        }

        #endregion

        #region Discussion

        private DiscussionDal _discussionMainDal;
        public DiscussionDal DiscussionMainDal
        {
            get { return _discussionMainDal ?? (_discussionMainDal = new DiscussionDal((CmsSocialNetworkDb)this)); }
        }

        #endregion

        #region DiscussionTag

        private DiscussionTagDal _discussionTagMainDal;
        public DiscussionTagDal DiscussionTagMainDal
        {
            get { return _discussionTagMainDal ?? (_discussionTagMainDal = new DiscussionTagDal((CmsSocialNetworkDb)this)); }
        }

        #endregion

        #region DiscussionTopic

        private DiscussionTopicDal _discussionTopicMainMainDal;
        public DiscussionTopicDal DiscussionTopicMainDal
        {
            get { return _discussionTopicMainMainDal ?? (_discussionTopicMainMainDal = new DiscussionTopicDal((CmsSocialNetworkDb)this)); }
        }

        #endregion

        #region DiscussionTopicFollow

        private DiscussionTopicFollowDal _discussionTopicFollowMainDal;
        public DiscussionTopicFollowDal DiscussionTopicFollowMainDal
        {
            get { return _discussionTopicFollowMainDal ?? (_discussionTopicFollowMainDal = new DiscussionTopicFollowDal((CmsSocialNetworkDb)this)); }
        }

        #endregion

        #endregion

        #region Constructors

        protected CmsSocialNetworkDbBase()
        {
        }
        protected CmsSocialNetworkDbBase(bool init)
        {
            if (init)
            {
                InitConnection();
            }
        }

        #endregion

        #region Protected members

        protected override sealed void InitConnection()
        {
            _connection = CreateConnection();
            _connection.Open();
        }

        /// <summary>
        /// Creates a new connection to the database.
        /// </summary>
        /// <returns>A reference to the <see cref="System.Data.IDbConnection"/> object.</returns>
        protected abstract IDbConnection CreateConnection();

        #endregion
    }
}