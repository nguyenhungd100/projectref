﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace ChannelVN.LiveMatch.Entity
{
    public class LiveMatchNodejs
    {
    }

    #region Entity

    [DataContract]
    public class ResponseLiveMatch
    {
        [DataMember]
        public bool Success { get; set; }
        [DataMember]
        public int Code { get; set; }
        [DataMember]
        public string Message { get; set; }
        [DataMember]
        public NodeJs_LiveMatchEntity Data { get; set; }
        public ResponseLiveMatch()
        {
            Success = false;
            Code = 0;
            Message = "";
            Data = null;
        }
    }
    public class ResponseListLiveMatchEvent
    {
        [DataMember]
        public bool Success { get; set; }
        [DataMember]
        public int Code { get; set; }
        [DataMember]
        public string Message { get; set; }
        [DataMember]
        public List<NodeJs_LiveMatchEventEntity> Data { get; set; }
        public ResponseListLiveMatchEvent()
        {
            Success = false;
            Code = 0;
            Message = "";
            Data = new List<NodeJs_LiveMatchEventEntity>();
        }
    }
    public class ResponseListLiveMatchPenalty
    {
        [DataMember]
        public bool Success { get; set; }
        [DataMember]
        public int Code { get; set; }
        [DataMember]
        public string Message { get; set; }
        [DataMember]
        public List<NodeJs_LiveMatchPenaltyEntity> Data { get; set; }
        public ResponseListLiveMatchPenalty()
        {
            Success = false;
            Code = 0;
            Message = "";
            Data = new List<NodeJs_LiveMatchPenaltyEntity>();
        }
    }
    public class ResponseListLiveMatchTimeLines
    {
        [DataMember]
        public bool Success { get; set; }
        [DataMember]
        public int Code { get; set; }
        [DataMember]
        public string Message { get; set; }
        [DataMember]
        public List<NodeJs_LiveMatchTimeLinesEntity> Data { get; set; }
        public ResponseListLiveMatchTimeLines()
        {
            Success = false;
            Code = 0;
            Message = "";
            Data = new List<NodeJs_LiveMatchTimeLinesEntity>();
        }
    }
    [DataContract]
    public class ResponseListLiveMatch
    {
        [DataMember]
        public bool Success { get; set; }
        [DataMember]
        public int Code { get; set; }
        [DataMember]
        public string Message { get; set; }
        [DataMember]
        public NodeJs_LiveMatchData Data { get; set; }
        public ResponseListLiveMatch()
        {
            Success = false;
            Code = 0;
            Message = "";
            Data = new NodeJs_LiveMatchData();
        }
    }
    [DataContract]
    public class ResponseListLiveMatchByIds
    {        
        [DataMember]
        public string message { get; set; }
        [DataMember]
        public List<NodeJs_LiveMatchEntity> data { get; set; }
        [DataMember]
        public bool success { get; set; }
        [DataMember]
        public int code { get; set; }
        [DataMember]
        public string content { get; set; }
    }
    public class NodeJs_LiveMatchData
    {
        [DataMember]
        public List<NodeJs_LiveMatchEntity> data { get; set; }
        [DataMember]
        public int total_count { get; set; }
    }
    public class ResponseLiveMatchTimeLines
    {
        [DataMember]
        public bool Success { get; set; }
        [DataMember]
        public int Code { get; set; }
        [DataMember]
        public string Message { get; set; }
        [DataMember]
        public NodeJs_LiveMatchTimeLinesEntity Data { get; set; }
        public ResponseLiveMatchTimeLines()
        {
            Success = false;
            Code = 0;
            Message = "";
            Data = null;
        }
    }
    public class ResponseLiveMatchPenalty
    {
        [DataMember]
        public bool Success { get; set; }
        [DataMember]
        public int Code { get; set; }
        [DataMember]
        public string Message { get; set; }
        [DataMember]
        public NodeJs_LiveMatchPenaltyEntity Data { get; set; }
        public ResponseLiveMatchPenalty()
        {
            Success = false;
            Code = 0;
            Message = "";
            Data = null;
        }
    }
    public class ResponseLiveMatchEvent
    {
        [DataMember]
        public bool Success { get; set; }
        [DataMember]
        public int Code { get; set; }
        [DataMember]
        public string Message { get; set; }
        [DataMember]
        public NodeJs_LiveMatchEventEntity Data { get; set; }
        public ResponseLiveMatchEvent()
        {
            Success = false;
            Code = 0;
            Message = "";
            Data = null;
        }
    }

    [DataContract]
    public class NodeJs_LiveMatchEventEntity
    {
        [JsonProperty("id")]
        public string Id { get; set; }

        [JsonProperty("livematch_id")]
        public string LiveMatchId { get; set; }

        [JsonProperty("channel_id")]
        public string ChannelId { get; set; }

        [JsonProperty("application_id")]
        public string ApplicationId { get; set; }

        [JsonProperty("created_date")]
        public DateTime CreatedDate { get; set; }

        [JsonProperty("in_minute")]
        public int InMinute { get; set; }

        [JsonProperty("event_type")]
        public int EventType { get; set; }

        [JsonProperty("is_team_a")]
        public string IsTeamA { get; set; }
        //public bool IsTeamA
        //{
        //    get { return _IsTeamA == "1" ? true : false; }
        //    set
        //    {
        //        _IsTeamA = value ? "1" : "0";
        //    }
        //}


        [JsonProperty("player_name")]
        public string PlayerName { get; set; }

        [JsonProperty("player_name_out")]
        public string PlayerNameOut { get; set; }


        [JsonProperty("created_by")]
        public string CreatedBy { get; set; }

    }
    public class NodeJs_LiveMatchPenaltyEntity
    {
        [JsonProperty("id")]
        public string Id { get; set; }

        [JsonProperty("livematch_id")]
        public string LiveMatchId { get; set; }

        [JsonProperty("channel_id")]
        public string ChannelId { get; set; }

        [JsonProperty("application_id")]
        public string ApplicationId { get; set; }

        [JsonProperty("created_date")]
        public DateTime CreatedDate { get; set; }

        [JsonProperty("turn")]
        public int Turn { get; set; }

        [JsonProperty("is_team_a")]
        public string IsTeamA { get; set; }
        //public bool IsTeamA
        //{
        //    get { return _IsTeamA == "1" ? true : false; }
        //    set
        //    {
        //        _IsTeamA = value ? "1" : "0";
        //    }
        //}


        [JsonProperty("is_goal")]
        public string IsGoal { get; set; }
        //public bool IsGoal
        //{
        //    get { return _IsGoal == "1" ? true : false; }
        //    set
        //    {
        //        _IsGoal = value ? "1" : "0";
        //    }
        //}

        [JsonProperty("player_name")]
        public string PlayerName { get; set; }



        [JsonProperty("created_by")]
        public string CreatedBy { get; set; }

    }
    public class NodeJs_LiveMatchTimeLinesEntity
    {
        [JsonProperty("id")]
        public string Id { get; set; }

        [JsonProperty("livematch_id")]
        public string LiveMatchId { get; set; }

        [JsonProperty("channel_id")]
        public string ChannelId { get; set; }

        [JsonProperty("application_id")]
        public string ApplicationId { get; set; }

        [JsonProperty("created_date")]
        public DateTime CreatedDate { get; set; }

        [JsonProperty("in_minute")]
        public int InMinute { get; set; }

        [JsonProperty("event_type")]
        public int EventType { get; set; }

        [JsonProperty("description")]
        public string Description { get; set; }


        [JsonProperty("video_key")]
        public string VideoKey { get; set; }

        [JsonProperty("video_avatar")]
        public string VideoAvatar { get; set; }

        [JsonProperty("images")]
        public string Images { get; set; }

        [JsonProperty("extra_time")]
        public int ExtraTime { get; set; }

        [JsonProperty("status")]
        public int Status { get; set; }


        [JsonProperty("created_by")]
        public string CreatedBy { get; set; }

    }
    public class NodeJs_LiveMatchEntity
    {
        [JsonProperty("id")]
        public string Id { get; set; }

        [JsonProperty("title")]
        public string Title { get; set; }

        [JsonProperty("team_a")]
        public string TeamA { get; set; }

        [JsonProperty("team_b")]
        public string TeamB { get; set; }

        [JsonProperty("logo_team_a")]
        public string LogoTeamA { get; set; }

        [JsonProperty("logo_team_b")]
        public string LogoTeamB { get; set; }

        [JsonProperty("score_of_team_a")]
        public int ScoreOfTeamA { get; set; }

        [JsonProperty("score_of_team_b")]
        public int ScoreOfTeamB { get; set; }

        [JsonProperty("stadium")]
        public string Stadium { get; set; }

        [JsonProperty("referee")]
        public string Referee { get; set; }

        [JsonProperty(PropertyName = "begin_first_half")]
        public string BeginFirstHalf { get; set; }
        //public bool BeginFirstHalf
        //{
        //    get { return _BeginFirstHalf == "1" ? true : false; }
        //    set
        //    {
        //        _BeginFirstHalf = value ? "1" : "0";
        //    }
        //}

        [JsonProperty("finish_first_half")]
        public string FinishFirstHalf { get; set; }
        //public bool FinishFirstHalf
        //{
        //    get { return _FinishFirstHalf == "1" ? true : false; }
        //    set
        //    {
        //        _FinishFirstHalf = value ? "1" : "0";
        //    }
        //}

        [JsonProperty("begin_second_half")]
        public string BeginSecondHalf { get; set; }
        //public bool BeginSecondHalf
        //{
        //    get { return _BeginSecondHalf == "1" ? true : false; }
        //    set
        //    {
        //        _BeginSecondHalf = value ? "1" : "0";
        //    }
        //}

        [JsonProperty("finish_second_half")]
        public string FinishSecondHalf { get; set; }
        //public bool FinishSecondHalf
        //{
        //    get { return _FinishSecondHalf == "1" ? true : false; }
        //    set
        //    {
        //        _FinishSecondHalf = value ? "1" : "0";
        //    }
        //}

        [JsonProperty("begin_first_extra_time")]
        public string BeginFirstExtraTime { get; set; }
        //public bool BeginFirstExtraTime
        //{
        //    get { return _BeginFirstExtraTime == "1" ? true : false; }
        //    set
        //    {
        //        _BeginFirstExtraTime = value ? "1" : "0";
        //    }
        //}

        [JsonProperty("finish_first_extra_time")]
        public string FinishFirstExtraTime { get; set; }
        //public bool FinishFirstExtraTime
        //{
        //    get { return _FinishFirstExtraTime == "1" ? true : false; }
        //    set
        //    {
        //        _FinishFirstExtraTime = value ? "1" : "0";
        //    }
        //}

        [JsonProperty("begin_second_extra_time")]
        public string BeginSecondExtraTime { get; set; }
        //public bool BeginSecondExtraTime
        //{
        //    get { return _BeginSecondExtraTime == "1" ? true : false; }
        //    set
        //    {
        //        _BeginSecondExtraTime = value ? "1" : "0";
        //    }
        //}

        [JsonProperty("finish_second_extra_time")]
        public string FinishSecondExtraTime { get; set; }
        //public bool FinishSecondExtraTime
        //{
        //    get { return _FinishSecondExtraTime == "1" ? true : false; }
        //    set
        //    {
        //        _FinishSecondExtraTime = value ? "1" : "0";
        //    }
        //}

        [JsonProperty("is_penalty")]
        public string IsPenalty { get; set; }
        //public bool IsPenalty
        //{
        //    get { return _IsPenalty == "1" ? true : false; }
        //    set
        //    {
        //        _IsPenalty = value ? "1" : "0";
        //    }
        //}

        [JsonProperty("match_finished")]
        public string MatchFinished { get; set; }
        //public bool MatchFinished
        //{
        //    get { return _MatchFinished == "1" ? true : false; }
        //    set
        //    {
        //        _MatchFinished = value ? "1" : "0";
        //    }
        //}


        [JsonProperty("time_of_match_begin")]
        public DateTime TimeOfMatchBegin { get; set; }

        [JsonProperty("time_of_finish_first_half")]
        public DateTime TimeOfFinishFirstHalf { get; set; }

        [JsonProperty("time_of_begin_second_half")]
        public DateTime TimeOfBeginSecondHalf { get; set; }

        [JsonProperty("time_of_finish_second_half")]
        public DateTime TimeOfFinishSecondHalf { get; set; }

        [JsonProperty("time_of_begin_first_extra_time")]
        public DateTime TimeOfBeginFirstExtraTime { get; set; }

        [JsonProperty("time_of_finish_first_extra_time")]
        public DateTime TimeOfFinishFirstExtraTime { get; set; }

        [JsonProperty("time_of_begin_second_extra_time")]
        public DateTime TimeOfBeginSecondExtraTime { get; set; }

        [JsonProperty("time_of_finish_second_extra_time")]
        public DateTime TimeOfFinishSecondExtraTime { get; set; }

        [JsonProperty("time_of_match_finished")]
        public DateTime TimeOfMatchFinished { get; set; }

        [JsonProperty("status")]
        public int Status { get; set; }

        [JsonProperty("link_livetv_1")]
        public string LinkLiveTV1 { get; set; }

        [JsonProperty("link_livetv_2")]
        public string LinkLiveTV2 { get; set; }

        [JsonProperty("link_livetv_3")]
        public string LinkLiveTV3 { get; set; }

        [JsonProperty("link_livetv_4")]
        public string LinkLiveTV4 { get; set; }

        [JsonProperty("link_livetv_5")]
        public string LinkLiveTV5 { get; set; }

        [JsonProperty("show_on_livetv")]
        public string ShowOnLiveTV { get; set; }
        //public bool ShowOnLiveTV
        //{
        //    get { return _ShowOnLiveTV == "1" ? true : false; }
        //    set
        //    {
        //        _ShowOnLiveTV = value ? "1" : "0";
        //    }
        //}

        [JsonProperty("infomation_before_match")]
        public string InfomationBeforeMatch { get; set; }


        [JsonProperty("match_status")]
        public string MatchStatus { get; set; }

        [JsonProperty("created_date")]
        public DateTime CreatedDate { get; set; }

        [JsonProperty("created_by")]
        public string CreatedBy { get; set; }

        [JsonProperty("channel_id")]
        public string ChannelId { get; set; }

        [JsonProperty("application_id")]
        public string ApplicationId { get; set; }

        [JsonProperty("NewsId")]
        public string NewsId { get; set; }
    }
    public class NodeJs_LiveMatch
    {
        public string Id { get; set; }
        public string Title { get; set; }
        public bool ShowTime { get; set; }
        public bool ShowAuthor { get; set; }
        public DateTime StartDate { get; set; }
        public string PublishedContent { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public string LastModifiedBy { get; set; }
        public int Status { get; set; }
        public bool IsShowLiveMatchLabel { get; set; }

    }

    #endregion

    #region Enums

    public enum NodeJs_LiveMatchTimelineEventType
    {
        /// <summary>
        /// Bắt đầu trận đấu
        /// </summary>
        StartMatch = 0,
        /// <summary>
        /// Sút vào
        /// </summary>
        ShotsOnGoal = 1,
        /// <summary>
        /// Nguy hiểm
        /// </summary>
        Dangerous = 2,
        /// <summary>
        /// Penalty vào
        /// </summary>
        PenaltyGoal = 3,
        /// <summary>
        /// Penalty hụt
        /// </summary>
        PenaltyMissed = 4,
        /// <summary>
        /// Thẻ vàng 1
        /// </summary>
        FirstYeallowCard = 5,
        /// <summary>
        /// Thẻ vàng 2
        /// </summary>
        SecondYeallowCard = 6,
        /// <summary>
        /// Thẻ đỏ
        /// </summary>
        RedCard = 7,
        /// <summary>
        /// Thay người
        /// </summary>
        ChangePlayer = 8,
        /// <summary>
        /// Chấn thương
        /// </summary>
        Injury = 9,
        /// <summary>
        /// Kiến thiết
        /// </summary>
        Assists = 10,
        /// <summary>
        /// Xung đột
        /// </summary>
        Conflict = 11,
        /// <summary>
        /// Kết thúc trận đấu
        /// </summary>
        EndOfMatch = 12,
        /// <summary>
        /// Bình luận
        /// </summary>
        Expository = 13
    }

    public enum NodeJs_LiveMatchEventType
    {
        /// <summary>
        /// Sút vào
        /// </summary>
        ShotsOnGoal = 0,
        /// <summary>
        /// Thẻ vàng 1
        /// </summary>
        FirstYeallowCard = 1,
        /// <summary>
        /// Thẻ vàng 2
        /// </summary>
        SecondYeallowCard = 2,
        /// <summary>
        /// Thẻ đỏ
        /// </summary>
        RedCard = 3,
        /// <summary>
        /// Thay người
        /// </summary>
        ChangePlayer = 4,
    }

    #endregion
}
