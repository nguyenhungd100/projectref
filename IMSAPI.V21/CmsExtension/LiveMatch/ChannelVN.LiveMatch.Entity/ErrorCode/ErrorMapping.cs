﻿using System.Runtime.Serialization;
using ChannelVN.CMS.Common;

namespace ChannelVN.LiveMatch.Entity.ErrorCode
{
    [DataContract]
    public class ErrorMapping : BoErrorMappingBase<ErrorMapping.ErrorCodes>
    {
        private static ErrorMapping _current;
        public static ErrorMapping Current
        {
            get { return _current ?? (_current = new ErrorMapping()); }
        }

        protected override void InitErrorMapping()
        {
            InnerHashtable[ErrorCodes.UpdateLiveMatchTimelineNotFoundLiveMatch] = "Không tìm thấy thông tin trận đấu";
            InnerHashtable[ErrorCodes.UpdateLiveMatchTimelineNotFoundTimeLine] = "Không tìm thấy thông tin tường thuật";

            InnerHashtable[ErrorCodes.UpdateLiveMatchEventNotFoundLiveMatch] = "Không tìm thấy thông tin trận đấu";
            InnerHashtable[ErrorCodes.UpdateLiveMatchEventNotFoundEvent] = "Không tìm thấy thông tin sự kiện";

            InnerHashtable[ErrorCodes.UpdateLiveMatchPenaltyNotFoundLiveMatch] = "Không tìm thấy thông tin trận đấu";
            InnerHashtable[ErrorCodes.UpdateLiveMatchPenaltyNotFoundPenalty] = "Không tìm thấy thông tin đá luân lưu";

            InnerHashtable[ErrorCodes.UpdateLiveMatchNotFoundLiveMatch] = "Không tìm thấy thông tin trận đấu";
            InnerHashtable[ErrorCodes.UpdateLiveMatchInvalidTitle] = "Tiêu đề không hợp lệ";
            InnerHashtable[ErrorCodes.UpdateLiveMatchInvalidTeamA] = "Tên đội A không hợp lệ";
            InnerHashtable[ErrorCodes.UpdateLiveMatchInvalidTeamB] = "Tên đội B không hợp lệ";
            InnerHashtable[ErrorCodes.UpdateLiveMatchInvalidTimeOfMatchBegin] = "Thời gian bắt đầu không hợp lệ";
        }
        [DataContract]
        public enum ErrorCodes
        {
            #region General error

            [EnumMember]
            Success = ErrorCodeBase.Success,
            [EnumMember]
            UnknowError = ErrorCodeBase.UnknowError,
            [EnumMember]
            Exception = ErrorCodeBase.Exception,
            [EnumMember]
            BusinessError = ErrorCodeBase.BusinessError,
            [EnumMember]
            InvalidRequest = ErrorCodeBase.InvalidRequest,
            [EnumMember]
            TimeOutSession = ErrorCodeBase.TimeOutSession,

            #endregion

            UpdateLiveMatchTimelineNotFoundLiveMatch = 10000,
            UpdateLiveMatchTimelineNotFoundTimeLine = 10001,

            UpdateLiveMatchEventNotFoundLiveMatch = 20000,
            UpdateLiveMatchEventNotFoundEvent = 20001,

            UpdateLiveMatchPenaltyNotFoundLiveMatch = 30000,
            UpdateLiveMatchPenaltyNotFoundPenalty = 30001,

            UpdateLiveMatchNotFoundLiveMatch = 40000,
            UpdateLiveMatchInvalidTitle = 40001,
            UpdateLiveMatchInvalidTeamA = 40002,
            UpdateLiveMatchInvalidTeamB = 40003,
            UpdateLiveMatchInvalidTimeOfMatchBegin = 40004,
        }
    }
}
