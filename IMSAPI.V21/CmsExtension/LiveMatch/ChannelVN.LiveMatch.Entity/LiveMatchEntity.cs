﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using ChannelVN.CMS.Common;

namespace ChannelVN.LiveMatch.Entity
{
    [DataContract]
    public class LiveMatchEntity : EntityBase
    {
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public string Title { get; set; }
        [DataMember]
        public string TeamA { get; set; }
        [DataMember]
        public string TeamB { get; set; }
        [DataMember]
        public string LogoTeamA { get; set; }
        [DataMember]
        public string LogoTeamB { get; set; }
        [DataMember]
        public int ScoreOfTeamA { get; set; }
        [DataMember]
        public int ScoreOfTeamB { get; set; }
        [DataMember]
        public string Stadium { get; set; }
        [DataMember]
        public string Referee { get; set; }
        [DataMember]
        public bool BeginFirstHalf { get; set; }
        [DataMember]
        public bool FinishFirstHalf { get; set; }
        [DataMember]
        public bool BeginSecondHalf { get; set; }
        [DataMember]
        public bool FinishSecondHalf { get; set; }
        [DataMember]
        public bool BeginFirstExtraTime { get; set; }
        [DataMember]
        public bool FinishFirstExtraTime { get; set; }
        [DataMember]
        public bool BeginSecondExtraTime { get; set; }
        [DataMember]
        public bool FinishSecondExtraTime { get; set; }
        [DataMember]
        public bool IsPenalty { get; set; }
        [DataMember]
        public bool MatchFinished { get; set; }
        [DataMember]
        public DateTime TimeOfMatchBegin { get; set; }
        [DataMember]
        public DateTime TimeOfFinishFirstHalf { get; set; }
        [DataMember]
        public DateTime TimeOfBeginSecondHalf { get; set; }
        [DataMember]
        public DateTime TimeOfFinishSecondHalf { get; set; }
        [DataMember]
        public DateTime TimeOfBeginFirstExtraTime { get; set; }
        [DataMember]
        public DateTime TimeOfFinishFirstExtraTime { get; set; }
        [DataMember]
        public DateTime TimeOfBeginSecondExtraTime { get; set; }
        [DataMember]
        public DateTime TimeOfFinishSecondExtraTime { get; set; }
        [DataMember]
        public DateTime TimeOfMatchFinished { get; set; }
        [DataMember]
        public int Status { get; set; }
        [DataMember]
        public string LinkLiveTV1 { get; set; }
        [DataMember]
        public string LinkLiveTV2 { get; set; }
        [DataMember]
        public string LinkLiveTV3 { get; set; }
        [DataMember]
        public string LinkLiveTV4 { get; set; }
        [DataMember]
        public string LinkLiveTV5 { get; set; }
        [DataMember]
        public bool ShowOnLiveTV { get; set; }
        
        
        [DataMember]
        public string InfomationBeforeMatch { get; set; }
        [DataMember]
        public int MatchStatus { get; set; }
    }

    [DataContract]
    public class LiveMatchDetailEntity : EntityBase
    {
        [DataMember]
        public LiveMatchEntity LiveMatch { get; set; }
        [DataMember]
        public List<LiveMatchTimelineEntity> LiveMatchTimelines { get; set; }
        [DataMember]
        public List<LiveMatchEventEntity> LiveMatchEvents { get; set; }
        [DataMember]
        public List<LiveMatchPenaltyEntity> LiveMatchPenalty { get; set; }
    }
}
