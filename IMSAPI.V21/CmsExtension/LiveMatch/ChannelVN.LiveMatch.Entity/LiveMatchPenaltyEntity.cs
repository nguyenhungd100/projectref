﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using ChannelVN.CMS.Common;

namespace ChannelVN.LiveMatch.Entity
{
    [DataContract]
    public class LiveMatchPenaltyEntity : EntityBase
    {
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public int LiveMatchId { get; set; }
        [DataMember]
        public int Turn { get; set; }
        [DataMember]
        public string PlayerName { get; set; }
        [DataMember]
        public bool IsTeamA { get; set; }
        [DataMember]
        public bool IsGoal { get; set; }
    }
}
