﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using ChannelVN.CMS.Common;

namespace ChannelVN.LiveMatch.Entity
{
    [DataContract]
    public class LiveMatchTimelineEntity : EntityBase
    {
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public int LiveMatchId { get; set; }
        [DataMember]
        public int InMinute { get; set; }
        [DataMember]
        public int EventType { get; set; }
        [DataMember]
        public string Description { get; set; }
        [DataMember]
        public string VideoKey { get; set; }
        [DataMember]
        public string VideoAvatar { get; set; }
        [DataMember]
        public string Images { get; set; }
        [DataMember]
        public int ExtraTime { get; set; }
        [DataMember]
        public int Status { get; set; }
    }
}
