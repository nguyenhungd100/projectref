﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using ChannelVN.CMS.Common;
using ChannelVN.LiveGameMatch.MainDal.Databases;
using ChannelVN.LiveMatch.Entity;

namespace ChannelVN.LiveMatch.MainDal
{
    public abstract class LiveMatchTimelineDalBase
    {
        public List<LiveMatchTimelineEntity> GetByLiveMatchId(int liveMatchId)
        {
            const string commandText = "CMS_LiveMatchTimeline_GetByLiveMatchId";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "LiveMatchId", liveMatchId);
                var numberOfRow = _db.GetList<LiveMatchTimelineEntity>(cmd);
                return numberOfRow;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public LiveMatchTimelineEntity GetById(int liveMatchTimelineId)
        {
            const string commandText = "CMS_LiveMatchTimeline_GetById";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", liveMatchTimelineId);
                var numberOfRow = _db.Get<LiveMatchTimelineEntity>(cmd);
                return numberOfRow;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public bool Insert(LiveMatchTimelineEntity liveMatchTimeline, ref int timelineId)
        {
            const string commandText = "CMS_LiveMatchTimeline_Insert";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", 0, ParameterDirection.Output);
                _db.AddParameter(cmd, "LiveMatchId", liveMatchTimeline.LiveMatchId);
                _db.AddParameter(cmd, "InMinute", liveMatchTimeline.InMinute);
                _db.AddParameter(cmd, "EventType", liveMatchTimeline.EventType);
                _db.AddParameter(cmd, "Description", liveMatchTimeline.Description);
                _db.AddParameter(cmd, "VideoKey", liveMatchTimeline.VideoKey);
                _db.AddParameter(cmd, "VideoAvatar", liveMatchTimeline.VideoAvatar);
                _db.AddParameter(cmd, "Images", liveMatchTimeline.Images);
                _db.AddParameter(cmd, "ExtraTime", liveMatchTimeline.ExtraTime);
                _db.AddParameter(cmd, "Status", liveMatchTimeline.Status);
                var numberOfRow = _db.ExecuteNonQuery(cmd);
                timelineId = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 0));

                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, string.Format("{0}:{1}", commandText, ex.Message));
            }
            return false;
        }
        public bool Update(LiveMatchTimelineEntity liveMatchTimeline)
        {
            const string commandText = "CMS_LiveMatchTimeline_Update";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", liveMatchTimeline.Id);
                _db.AddParameter(cmd, "LiveMatchId", liveMatchTimeline.LiveMatchId);
                _db.AddParameter(cmd, "InMinute", liveMatchTimeline.InMinute);
                _db.AddParameter(cmd, "EventType", liveMatchTimeline.EventType);
                _db.AddParameter(cmd, "Description", liveMatchTimeline.Description);
                _db.AddParameter(cmd, "VideoKey", liveMatchTimeline.VideoKey);
                _db.AddParameter(cmd, "VideoAvatar", liveMatchTimeline.VideoAvatar);
                _db.AddParameter(cmd, "Images", liveMatchTimeline.Images);
                _db.AddParameter(cmd, "ExtraTime", liveMatchTimeline.ExtraTime);
                _db.AddParameter(cmd, "Status", liveMatchTimeline.Status);
                var numberOfRow = _db.ExecuteNonQuery(cmd);
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool Delete(int liveMatchTimelineId)
        {
            const string commandText = "CMS_LiveMatchTimeline_Delete";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", liveMatchTimelineId);
                var numberOfRow = _db.ExecuteNonQuery(cmd);
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        #region Core members

        private readonly CmsMainDb _db;

        protected LiveMatchTimelineDalBase(CmsMainDb db)
        {
            _db = db;
        }

        protected CmsMainDb Database
        {
            get { return _db; }
        }

        #endregion
    }
}
