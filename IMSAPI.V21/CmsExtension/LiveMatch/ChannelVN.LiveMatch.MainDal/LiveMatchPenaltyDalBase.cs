﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using ChannelVN.CMS.Common;
using ChannelVN.LiveGameMatch.MainDal.Databases;
using ChannelVN.LiveMatch.Entity;

namespace ChannelVN.LiveMatch.MainDal
{
    public abstract class LiveMatchPenaltyDalBase
    {
        public List<LiveMatchPenaltyEntity> GetByLiveMatchId(int liveMatchId)
        {
            const string commandText = "CMS_LiveMatchPenalty_GetByLiveMatchId";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "LiveMatchId", liveMatchId);
                var numberOfRow = _db.GetList<LiveMatchPenaltyEntity>(cmd);

                return numberOfRow;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public LiveMatchPenaltyEntity GetById(int liveMatchPenaltyId)
        {
            const string commandText = "CMS_LiveMatchPenalty_GetById";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", liveMatchPenaltyId);
                var numberOfRow = _db.Get<LiveMatchPenaltyEntity>(cmd);

                return numberOfRow;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public bool Insert(LiveMatchPenaltyEntity liveMatchPenalty)
        {
            const string commandText = "CMS_LiveMatchPenalty_Insert";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "LiveMatchId", liveMatchPenalty.LiveMatchId);
                _db.AddParameter(cmd, "Turn", liveMatchPenalty.Turn);
                _db.AddParameter(cmd, "PlayerName", liveMatchPenalty.PlayerName);
                _db.AddParameter(cmd, "IsTeamA", liveMatchPenalty.IsTeamA);
                _db.AddParameter(cmd, "IsGoal", liveMatchPenalty.IsGoal);
                var numberOfRow = _db.ExecuteNonQuery(cmd);

                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool Update(LiveMatchPenaltyEntity liveMatchPenalty)
        {
            const string commandText = "CMS_LiveMatchPenalty_Update";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", liveMatchPenalty.Id);
                _db.AddParameter(cmd, "LiveMatchId", liveMatchPenalty.LiveMatchId);
                _db.AddParameter(cmd, "Turn", liveMatchPenalty.Turn);
                _db.AddParameter(cmd, "PlayerName", liveMatchPenalty.PlayerName);
                _db.AddParameter(cmd, "IsTeamA", liveMatchPenalty.IsTeamA);
                _db.AddParameter(cmd, "IsGoal", liveMatchPenalty.IsGoal);
                var numberOfRow = _db.ExecuteNonQuery(cmd);

                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool Delete(int liveMatchPenaltyId)
        {
            const string commandText = "CMS_LiveMatchPenalty_Delete";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", liveMatchPenaltyId); ;
                var numberOfRow = _db.ExecuteNonQuery(cmd);

                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        #region Core members

        private readonly CmsMainDb _db;

        protected LiveMatchPenaltyDalBase(CmsMainDb db)
        {
            _db = db;
        }

        protected CmsMainDb Database
        {
            get { return _db; }
        }

        #endregion
    }
}
