﻿using System.Data;
using ChannelVN.CMS.Common.DalCommon;
using ChannelVN.LiveMatch.MainDal;

namespace ChannelVN.LiveGameMatch.MainDal.Databases
{
    /// <summary>
    /// The base class for the <see cref="CmsMainDb"/> class that 
    /// represents a connection to the <c>CmsMainDb</c> database. 
    /// </summary>
    /// <remarks>
    /// Do not change this source code. Modify the CmsMainDb class
    /// if you need to add or change some functionality.
    /// </remarks>
    public abstract class CmsMainDbBase : MainDbBase
    {
        #region Store procedures

        #region LiveMatch

        private LiveMatchDal _liveMatchMainDal;
        public LiveMatchDal LiveMatchMainDal
        {
            get { return _liveMatchMainDal ?? (_liveMatchMainDal = new LiveMatchDal((CmsMainDb)this)); }
        }

        #endregion

        #region Live Match Event

        private LiveMatchEventDal _liveMatchEventMainDal;
        public LiveMatchEventDal LiveMatchEventMainDal
        {
            get { return _liveMatchEventMainDal ?? (_liveMatchEventMainDal = new LiveMatchEventDal((CmsMainDb)this)); }
        }

        #endregion

        #region Live Match Penalty

        private LiveMatchPenaltyDal _liveMatchPenaltyMainDal;
        public LiveMatchPenaltyDal LiveMatchPenaltyMainDal
        {
            get { return _liveMatchPenaltyMainDal ?? (_liveMatchPenaltyMainDal = new LiveMatchPenaltyDal((CmsMainDb)this)); }
        }

        #endregion

        #region Live Match Timeline

        private LiveMatchTimelineDal _liveMatchTimelineMainDal;
        public LiveMatchTimelineDal LiveMatchTimelineMainDal
        {
            get { return _liveMatchTimelineMainDal ?? (_liveMatchTimelineMainDal = new LiveMatchTimelineDal((CmsMainDb)this)); }
        }

        #endregion

        #endregion

        #region Constructors

        protected CmsMainDbBase()
        {
        }
        protected CmsMainDbBase(bool init)
        {
            if (init)
            {
                InitConnection();
            }
        }

        #endregion

        #region Protected members

        protected override sealed void InitConnection()
        {
            _connection = CreateConnection();
            _connection.Open();
        }

        /// <summary>
        /// Creates a new connection to the database.
        /// </summary>
        /// <returns>A reference to the <see cref="System.Data.IDbConnection"/> object.</returns>
        protected abstract IDbConnection CreateConnection();

        #endregion
    }
}