﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using ChannelVN.CMS.Common;
using ChannelVN.LiveGameMatch.MainDal.Common;
using ChannelVN.LiveGameMatch.MainDal.Databases;
using ChannelVN.LiveMatch.Entity;

namespace ChannelVN.LiveMatch.MainDal
{
    public abstract class LiveMatchDalBase
    {
        public List<LiveMatchEntity> Search(string keyword, int pageIndex, int pageSize, ref int totalRow)
        {
            const string commandText = "CMS_LiveMatch_Search";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "TotalRow", totalRow, ParameterDirection.Output);
                _db.AddParameter(cmd, "Keyword", keyword);
                _db.AddParameter(cmd, "PageIndex", pageIndex);
                _db.AddParameter(cmd, "PageSize", pageSize);
                var numberOfRow = _db.GetList<LiveMatchEntity>(cmd);
                totalRow = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 0));
                return numberOfRow;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public LiveMatchEntity GetById(int liveMatchId)
        {
            const string commandText = "CMS_LiveMatch_GetById";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", liveMatchId);
                var numberOfRow = _db.Get<LiveMatchEntity>(cmd);
                return numberOfRow;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public bool Insert(LiveMatchEntity liveMatch)
        {
            const string commandText = "CMS_LiveMatch_Insert";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Title", liveMatch.Title);
                _db.AddParameter(cmd, "TeamA", liveMatch.TeamA);
                _db.AddParameter(cmd, "TeamB", liveMatch.TeamB);
                _db.AddParameter(cmd, "LogoTeamA", liveMatch.LogoTeamA);
                _db.AddParameter(cmd, "LogoTeamB", liveMatch.LogoTeamB);
                _db.AddParameter(cmd, "ScoreOfTeamA", liveMatch.ScoreOfTeamA);
                _db.AddParameter(cmd, "ScoreOfTeamB", liveMatch.ScoreOfTeamB);
                _db.AddParameter(cmd, "Stadium", liveMatch.Stadium);
                _db.AddParameter(cmd, "Referee", liveMatch.Referee);
                _db.AddParameter(cmd, "BeginFirstHalf", liveMatch.BeginFirstHalf);
                _db.AddParameter(cmd, "FinishFirstHalf", liveMatch.FinishFirstHalf);
                _db.AddParameter(cmd, "BeginSecondHalf", liveMatch.BeginSecondHalf);
                _db.AddParameter(cmd, "FinishSecondHalf", liveMatch.FinishSecondHalf);
                _db.AddParameter(cmd, "BeginFirstExtraTime", liveMatch.BeginFirstExtraTime);
                _db.AddParameter(cmd, "FinishFirstExtraTime", liveMatch.FinishFirstExtraTime);
                _db.AddParameter(cmd, "BeginSecondExtraTime", liveMatch.BeginSecondExtraTime);
                _db.AddParameter(cmd, "FinishSecondExtraTime", liveMatch.FinishSecondExtraTime);
                _db.AddParameter(cmd, "IsPenalty", liveMatch.IsPenalty);
                _db.AddParameter(cmd, "MatchFinished", liveMatch.MatchFinished);
                _db.AddParameter(cmd, "TimeOfMatchBegin",
                                 liveMatch.TimeOfMatchBegin < Constants.MinDateTime
                                     ? DateTime.Now
                                     : liveMatch.TimeOfMatchBegin);
                if (liveMatch.TimeOfFinishFirstHalf <= Constants.MinDateTime)
                    _db.AddParameter(cmd, "TimeOfFinishFirstHalf", DBNull.Value);
                else _db.AddParameter(cmd, "TimeOfFinishFirstHalf", liveMatch.TimeOfFinishFirstHalf);
                if (liveMatch.TimeOfBeginSecondHalf <= Constants.MinDateTime)
                    _db.AddParameter(cmd, "TimeOfBeginSecondHalf", DBNull.Value);
                else _db.AddParameter(cmd, "TimeOfBeginSecondHalf", liveMatch.TimeOfBeginSecondHalf);
                if (liveMatch.TimeOfFinishSecondHalf <= Constants.MinDateTime)
                    _db.AddParameter(cmd, "TimeOfFinishSecondHalf", DBNull.Value);
                else _db.AddParameter(cmd, "TimeOfFinishSecondHalf", liveMatch.TimeOfFinishSecondHalf);
                if (liveMatch.TimeOfBeginFirstExtraTime <= Constants.MinDateTime)
                    _db.AddParameter(cmd, "TimeOfBeginFirstExtraTime", DBNull.Value);
                else _db.AddParameter(cmd, "TimeOfBeginFirstExtraTime", liveMatch.TimeOfBeginFirstExtraTime);
                if (liveMatch.TimeOfFinishFirstExtraTime <= Constants.MinDateTime)
                    _db.AddParameter(cmd, "TimeOfFinishFirstExtraTime", DBNull.Value);
                else _db.AddParameter(cmd, "TimeOfFinishFirstExtraTime", liveMatch.TimeOfFinishFirstExtraTime);
                if (liveMatch.TimeOfBeginSecondExtraTime <= Constants.MinDateTime)
                    _db.AddParameter(cmd, "TimeOfBeginSecondExtraTime", DBNull.Value);
                else _db.AddParameter(cmd, "TimeOfBeginSecondExtraTime", liveMatch.TimeOfBeginSecondExtraTime);
                if (liveMatch.TimeOfFinishSecondExtraTime <= Constants.MinDateTime)
                    _db.AddParameter(cmd, "TimeOfFinishSecondExtraTime", DBNull.Value);
                else _db.AddParameter(cmd, "TimeOfFinishSecondExtraTime", liveMatch.TimeOfFinishSecondExtraTime);
                if (liveMatch.TimeOfMatchFinished <= Constants.MinDateTime)
                    _db.AddParameter(cmd, "TimeOfMatchFinished", DBNull.Value);
                else _db.AddParameter(cmd, "TimeOfMatchFinished", liveMatch.TimeOfMatchFinished);
                _db.AddParameter(cmd, "Status", liveMatch.Status);
                _db.AddParameter(cmd, "LinkLiveTV1", liveMatch.LinkLiveTV1);
                _db.AddParameter(cmd, "LinkLiveTV2", liveMatch.LinkLiveTV2);
                _db.AddParameter(cmd, "LinkLiveTV3", liveMatch.LinkLiveTV3);
                _db.AddParameter(cmd, "LinkLiveTV4", liveMatch.LinkLiveTV4);
                _db.AddParameter(cmd, "LinkLiveTV5", liveMatch.LinkLiveTV5);
                _db.AddParameter(cmd, "ShowOnLiveTV", liveMatch.ShowOnLiveTV);
                var numberOfRow = cmd.ExecuteNonQuery();
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool InsertV2(LiveMatchEntity liveMatch,ref int Id)
        {
            const string commandText = "CMS_LiveMatch_InsertV2";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", Id, ParameterDirection.Output);
                _db.AddParameter(cmd, "Title", liveMatch.Title);
                _db.AddParameter(cmd, "TeamA", liveMatch.TeamA);
                _db.AddParameter(cmd, "TeamB", liveMatch.TeamB);
                _db.AddParameter(cmd, "LogoTeamA", liveMatch.LogoTeamA);
                _db.AddParameter(cmd, "LogoTeamB", liveMatch.LogoTeamB);
                _db.AddParameter(cmd, "ScoreOfTeamA", liveMatch.ScoreOfTeamA);
                _db.AddParameter(cmd, "ScoreOfTeamB", liveMatch.ScoreOfTeamB);
                _db.AddParameter(cmd, "Stadium", liveMatch.Stadium);
                _db.AddParameter(cmd, "Referee", liveMatch.Referee);
                _db.AddParameter(cmd, "BeginFirstHalf", liveMatch.BeginFirstHalf);
                _db.AddParameter(cmd, "FinishFirstHalf", liveMatch.FinishFirstHalf);
                _db.AddParameter(cmd, "BeginSecondHalf", liveMatch.BeginSecondHalf);
                _db.AddParameter(cmd, "FinishSecondHalf", liveMatch.FinishSecondHalf);
                _db.AddParameter(cmd, "BeginFirstExtraTime", liveMatch.BeginFirstExtraTime);
                _db.AddParameter(cmd, "FinishFirstExtraTime", liveMatch.FinishFirstExtraTime);
                _db.AddParameter(cmd, "BeginSecondExtraTime", liveMatch.BeginSecondExtraTime);
                _db.AddParameter(cmd, "FinishSecondExtraTime", liveMatch.FinishSecondExtraTime);
                _db.AddParameter(cmd, "IsPenalty", liveMatch.IsPenalty);
                _db.AddParameter(cmd, "MatchFinished", liveMatch.MatchFinished);
                _db.AddParameter(cmd, "TimeOfMatchBegin",
                                 liveMatch.TimeOfMatchBegin < Constants.MinDateTime
                                     ? DateTime.Now
                                     : liveMatch.TimeOfMatchBegin);
                if (liveMatch.TimeOfFinishFirstHalf <= Constants.MinDateTime)
                    _db.AddParameter(cmd, "TimeOfFinishFirstHalf", DBNull.Value);
                else _db.AddParameter(cmd, "TimeOfFinishFirstHalf", liveMatch.TimeOfFinishFirstHalf);
                if (liveMatch.TimeOfBeginSecondHalf <= Constants.MinDateTime)
                    _db.AddParameter(cmd, "TimeOfBeginSecondHalf", DBNull.Value);
                else _db.AddParameter(cmd, "TimeOfBeginSecondHalf", liveMatch.TimeOfBeginSecondHalf);
                if (liveMatch.TimeOfFinishSecondHalf <= Constants.MinDateTime)
                    _db.AddParameter(cmd, "TimeOfFinishSecondHalf", DBNull.Value);
                else _db.AddParameter(cmd, "TimeOfFinishSecondHalf", liveMatch.TimeOfFinishSecondHalf);
                if (liveMatch.TimeOfBeginFirstExtraTime <= Constants.MinDateTime)
                    _db.AddParameter(cmd, "TimeOfBeginFirstExtraTime", DBNull.Value);
                else _db.AddParameter(cmd, "TimeOfBeginFirstExtraTime", liveMatch.TimeOfBeginFirstExtraTime);
                if (liveMatch.TimeOfFinishFirstExtraTime <= Constants.MinDateTime)
                    _db.AddParameter(cmd, "TimeOfFinishFirstExtraTime", DBNull.Value);
                else _db.AddParameter(cmd, "TimeOfFinishFirstExtraTime", liveMatch.TimeOfFinishFirstExtraTime);
                if (liveMatch.TimeOfBeginSecondExtraTime <= Constants.MinDateTime)
                    _db.AddParameter(cmd, "TimeOfBeginSecondExtraTime", DBNull.Value);
                else _db.AddParameter(cmd, "TimeOfBeginSecondExtraTime", liveMatch.TimeOfBeginSecondExtraTime);
                if (liveMatch.TimeOfFinishSecondExtraTime <= Constants.MinDateTime)
                    _db.AddParameter(cmd, "TimeOfFinishSecondExtraTime", DBNull.Value);
                else _db.AddParameter(cmd, "TimeOfFinishSecondExtraTime", liveMatch.TimeOfFinishSecondExtraTime);
                if (liveMatch.TimeOfMatchFinished <= Constants.MinDateTime)
                    _db.AddParameter(cmd, "TimeOfMatchFinished", DBNull.Value);
                else _db.AddParameter(cmd, "TimeOfMatchFinished", liveMatch.TimeOfMatchFinished);
                _db.AddParameter(cmd, "Status", liveMatch.Status);
                _db.AddParameter(cmd, "LinkLiveTV1", liveMatch.LinkLiveTV1);
                _db.AddParameter(cmd, "LinkLiveTV2", liveMatch.LinkLiveTV2);
                _db.AddParameter(cmd, "LinkLiveTV3", liveMatch.LinkLiveTV3);
                _db.AddParameter(cmd, "LinkLiveTV4", liveMatch.LinkLiveTV4);
                _db.AddParameter(cmd, "LinkLiveTV5", liveMatch.LinkLiveTV5);
                _db.AddParameter(cmd, "ShowOnLiveTV", liveMatch.ShowOnLiveTV);
                _db.AddParameter(cmd, "InfomationBeforeMatch", liveMatch.InfomationBeforeMatch);

                var numberOfRow = cmd.ExecuteNonQuery();
                Id = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 0));
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public bool Update(LiveMatchEntity liveMatch)
        {
            const string commandText = "CMS_LiveMatch_Update";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", liveMatch.Id);
                _db.AddParameter(cmd, "Title", liveMatch.Title);
                _db.AddParameter(cmd, "TeamA", liveMatch.TeamA);
                _db.AddParameter(cmd, "TeamB", liveMatch.TeamB);
                _db.AddParameter(cmd, "LogoTeamA", liveMatch.LogoTeamA);
                _db.AddParameter(cmd, "LogoTeamB", liveMatch.LogoTeamB);
                _db.AddParameter(cmd, "ScoreOfTeamA", liveMatch.ScoreOfTeamA);
                _db.AddParameter(cmd, "ScoreOfTeamB", liveMatch.ScoreOfTeamB);
                _db.AddParameter(cmd, "Stadium", liveMatch.Stadium);
                _db.AddParameter(cmd, "Referee", liveMatch.Referee);
                _db.AddParameter(cmd, "BeginFirstHalf", liveMatch.BeginFirstHalf);
                _db.AddParameter(cmd, "FinishFirstHalf", liveMatch.FinishFirstHalf);
                _db.AddParameter(cmd, "BeginSecondHalf", liveMatch.BeginSecondHalf);
                _db.AddParameter(cmd, "FinishSecondHalf", liveMatch.FinishSecondHalf);
                _db.AddParameter(cmd, "BeginFirstExtraTime", liveMatch.BeginFirstExtraTime);
                _db.AddParameter(cmd, "FinishFirstExtraTime", liveMatch.FinishFirstExtraTime);
                _db.AddParameter(cmd, "BeginSecondExtraTime", liveMatch.BeginSecondExtraTime);
                _db.AddParameter(cmd, "FinishSecondExtraTime", liveMatch.FinishSecondExtraTime);
                _db.AddParameter(cmd, "IsPenalty", liveMatch.IsPenalty);
                _db.AddParameter(cmd, "MatchFinished", liveMatch.MatchFinished);
                _db.AddParameter(cmd, "TimeOfMatchBegin",
                                 liveMatch.TimeOfMatchBegin < Constants.MinDateTime
                                     ? DateTime.Now
                                     : liveMatch.TimeOfMatchBegin);
                if (liveMatch.TimeOfFinishFirstHalf <= Constants.MinDateTime)
                    _db.AddParameter(cmd, "TimeOfFinishFirstHalf", DBNull.Value);
                else _db.AddParameter(cmd, "TimeOfFinishFirstHalf", liveMatch.TimeOfFinishFirstHalf);
                if (liveMatch.TimeOfBeginSecondHalf <= Constants.MinDateTime)
                    _db.AddParameter(cmd, "TimeOfBeginSecondHalf", DBNull.Value);
                else _db.AddParameter(cmd, "TimeOfBeginSecondHalf", liveMatch.TimeOfBeginSecondHalf);
                if (liveMatch.TimeOfFinishSecondHalf <= Constants.MinDateTime)
                    _db.AddParameter(cmd, "TimeOfFinishSecondHalf", DBNull.Value);
                else _db.AddParameter(cmd, "TimeOfFinishSecondHalf", liveMatch.TimeOfFinishSecondHalf);
                if (liveMatch.TimeOfBeginFirstExtraTime <= Constants.MinDateTime)
                    _db.AddParameter(cmd, "TimeOfBeginFirstExtraTime", DBNull.Value);
                else _db.AddParameter(cmd, "TimeOfBeginFirstExtraTime", liveMatch.TimeOfBeginFirstExtraTime);
                if (liveMatch.TimeOfFinishFirstExtraTime <= Constants.MinDateTime)
                    _db.AddParameter(cmd, "TimeOfFinishFirstExtraTime", DBNull.Value);
                else _db.AddParameter(cmd, "TimeOfFinishFirstExtraTime", liveMatch.TimeOfFinishFirstExtraTime);
                if (liveMatch.TimeOfBeginSecondExtraTime <= Constants.MinDateTime)
                    _db.AddParameter(cmd, "TimeOfBeginSecondExtraTime", DBNull.Value);
                else _db.AddParameter(cmd, "TimeOfBeginSecondExtraTime", liveMatch.TimeOfBeginSecondExtraTime);
                if (liveMatch.TimeOfFinishSecondExtraTime <= Constants.MinDateTime)
                    _db.AddParameter(cmd, "TimeOfFinishSecondExtraTime", DBNull.Value);
                else _db.AddParameter(cmd, "TimeOfFinishSecondExtraTime", liveMatch.TimeOfFinishSecondExtraTime);
                if (liveMatch.TimeOfMatchFinished <= Constants.MinDateTime)
                    _db.AddParameter(cmd, "TimeOfMatchFinished", DBNull.Value);
                else _db.AddParameter(cmd, "TimeOfMatchFinished", liveMatch.TimeOfMatchFinished);
                _db.AddParameter(cmd, "Status", liveMatch.Status);
                _db.AddParameter(cmd, "LinkLiveTV1", liveMatch.LinkLiveTV1);
                _db.AddParameter(cmd, "LinkLiveTV2", liveMatch.LinkLiveTV2);
                _db.AddParameter(cmd, "LinkLiveTV3", liveMatch.LinkLiveTV3);
                _db.AddParameter(cmd, "LinkLiveTV4", liveMatch.LinkLiveTV4);
                _db.AddParameter(cmd, "LinkLiveTV5", liveMatch.LinkLiveTV5);
                _db.AddParameter(cmd, "ShowOnLiveTV", liveMatch.ShowOnLiveTV);
                _db.AddParameter(cmd, "InfomationBeforeMatch", liveMatch.InfomationBeforeMatch);
                _db.AddParameter(cmd, "MatchStatus", liveMatch.MatchStatus);
                var numberOfRow = cmd.ExecuteNonQuery();
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public bool Delete(int liveMatchId)
        {
            const string commandText = "CMS_LiveMatch_Delete";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", liveMatchId);
                var numberOfRow = cmd.ExecuteNonQuery();
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        #region Core members

        private readonly CmsMainDb _db;

        protected LiveMatchDalBase(CmsMainDb db)
        {
            _db = db;
        }

        protected CmsMainDb Database
        {
            get { return _db; }
        }

        #endregion
    }
}
