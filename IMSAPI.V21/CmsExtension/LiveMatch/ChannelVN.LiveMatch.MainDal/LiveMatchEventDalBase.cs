﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using ChannelVN.CMS.Common;
using ChannelVN.LiveGameMatch.MainDal.Databases;
using ChannelVN.LiveMatch.Entity;

namespace ChannelVN.LiveMatch.MainDal
{
    public abstract class LiveMatchEventDalBase
    {
        public List<LiveMatchEventEntity> GetByLiveMatchId(int liveMatchId)
        {
            const string commandText = "CMS_LiveMatchEvent_GetByLiveMatchId";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "LiveMatchId", liveMatchId);
                var numberOfRow = _db.GetList<LiveMatchEventEntity>(cmd);
                return numberOfRow;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public LiveMatchEventEntity GetById(int liveMatchEventId)
        {
            const string commandText = "CMS_LiveMatchEvent_GetById";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", liveMatchEventId);
                var numberOfRow = _db.Get<LiveMatchEventEntity>(cmd);
                return numberOfRow;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public bool Insert(LiveMatchEventEntity liveMatchEvent)
        {
            const string commandText = "CMS_LiveMatchEvent_Insert";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "LiveMatchId", liveMatchEvent.LiveMatchId);
                _db.AddParameter(cmd, "InMinute", liveMatchEvent.InMinute);
                _db.AddParameter(cmd, "EventType", liveMatchEvent.EventType);
                _db.AddParameter(cmd, "IsTeamA", liveMatchEvent.IsTeamA);
                _db.AddParameter(cmd, "PlayerName", liveMatchEvent.PlayerName);
                _db.AddParameter(cmd, "PlayerNameOut", liveMatchEvent.PlayerNameOut);
                var numberOfRow = _db.ExecuteNonQuery(cmd);
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool Update(LiveMatchEventEntity liveMatchEvent)
        {
            const string commandText = "CMS_LiveMatchEvent_Update";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", liveMatchEvent.Id);
                _db.AddParameter(cmd, "LiveMatchId", liveMatchEvent.LiveMatchId);
                _db.AddParameter(cmd, "InMinute", liveMatchEvent.InMinute);
                _db.AddParameter(cmd, "EventType", liveMatchEvent.EventType);
                _db.AddParameter(cmd, "IsTeamA", liveMatchEvent.IsTeamA);
                _db.AddParameter(cmd, "PlayerName", liveMatchEvent.PlayerName);
                _db.AddParameter(cmd, "PlayerNameOut", liveMatchEvent.PlayerNameOut);
                var numberOfRow = _db.ExecuteNonQuery(cmd);
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool Delete(int liveMatchEventId)
        {const string commandText = "CMS_LiveMatchEvent_Delete";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", liveMatchEventId);
                var numberOfRow = _db.ExecuteNonQuery(cmd);
                return numberOfRow>0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        #region Core members

        private readonly CmsMainDb _db;

        protected LiveMatchEventDalBase(CmsMainDb db)
        {
            _db = db;
        }

        protected CmsMainDb Database
        {
            get { return _db; }
        }

        #endregion
    }
}
