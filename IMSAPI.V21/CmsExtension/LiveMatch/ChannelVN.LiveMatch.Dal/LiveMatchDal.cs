﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using ChannelVN.CMS.Common;
using ChannelVN.LiveGameMatch.MainDal.Databases;
using ChannelVN.LiveMatch.Dal.Common;
using ChannelVN.LiveMatch.Entity;

namespace ChannelVN.LiveMatch.Dal
{
    public class LiveMatchDal
    {
        public static List<LiveMatchEntity> Search(string keyword, int pageIndex, int pageSize, ref int totalRow)
        {
            List<LiveMatchEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.LiveMatchMainDal.Search(keyword, pageIndex, pageSize, ref totalRow);
            }
            return returnValue;
        }
        public static LiveMatchEntity GetById(int liveMatchId)
        {
            LiveMatchEntity returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.LiveMatchMainDal.GetById(liveMatchId);
            }
            return returnValue;
        }

        public static bool Insert(LiveMatchEntity liveMatch)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.LiveMatchMainDal.Insert(liveMatch);
            }
            return returnValue;
        }
        public static bool InsertV2(LiveMatchEntity liveMatch,ref int Id)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.LiveMatchMainDal.InsertV2(liveMatch, ref Id);
            }
            return returnValue;
        }
        public static bool Update(LiveMatchEntity liveMatch)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.LiveMatchMainDal.Update(liveMatch);
            }
            return returnValue;
        }
        public static bool Delete(int liveMatchId)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.LiveMatchMainDal.Delete(liveMatchId);
            }
            return returnValue;
        }
    }
}
