﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using ChannelVN.CMS.Common;
using ChannelVN.LiveGameMatch.MainDal.Databases;
using ChannelVN.LiveMatch.Dal.Common;
using ChannelVN.LiveMatch.Entity;

namespace ChannelVN.LiveMatch.Dal
{
    public class LiveMatchPenaltyDal
    {
        public static List<LiveMatchPenaltyEntity> GetByLiveMatchId(int liveMatchId)
        {
            List<LiveMatchPenaltyEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.LiveMatchPenaltyMainDal.GetByLiveMatchId(liveMatchId);
            }
            return returnValue;
        }
        public static LiveMatchPenaltyEntity GetById(int liveMatchPenaltyId)
        {
            LiveMatchPenaltyEntity returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.LiveMatchPenaltyMainDal.GetById(liveMatchPenaltyId);
            }
            return returnValue;
        }

        public static bool Insert(LiveMatchPenaltyEntity liveMatchPenalty)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.LiveMatchPenaltyMainDal.Insert(liveMatchPenalty);
            }
            return returnValue;
        }
        public static bool Update(LiveMatchPenaltyEntity liveMatchPenalty)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.LiveMatchPenaltyMainDal.Update(liveMatchPenalty);
            }
            return returnValue;
        }
        public static bool Delete(int liveMatchPenaltyId)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.LiveMatchPenaltyMainDal.Delete(liveMatchPenaltyId);
            }
            return returnValue;
        }
    }
}
