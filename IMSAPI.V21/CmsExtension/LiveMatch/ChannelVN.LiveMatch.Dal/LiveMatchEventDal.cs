﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using ChannelVN.CMS.Common;
using ChannelVN.LiveGameMatch.MainDal.Databases;
using ChannelVN.LiveMatch.Dal.Common;
using ChannelVN.LiveMatch.Entity;

namespace ChannelVN.LiveMatch.Dal
{
    public class LiveMatchEventDal
    {
        public static List<LiveMatchEventEntity> GetByLiveMatchId(int liveMatchId)
        {
            List<LiveMatchEventEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.LiveMatchEventMainDal.GetByLiveMatchId(liveMatchId);
            }
            return returnValue;
        }
        public static LiveMatchEventEntity GetById(int liveMatchEventId)
        {
            LiveMatchEventEntity returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.LiveMatchEventMainDal.GetById(liveMatchEventId);
            }
            return returnValue;
        }

        public static bool Insert(LiveMatchEventEntity liveMatchEvent)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.LiveMatchEventMainDal.Insert(liveMatchEvent);
            }
            return returnValue;
        }
        public static bool Update(LiveMatchEventEntity liveMatchEvent)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.LiveMatchEventMainDal.Update(liveMatchEvent);
            }
            return returnValue;
        }
        public static bool Delete(int liveMatchEventId)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.LiveMatchEventMainDal.Delete(liveMatchEventId);
            }
            return returnValue;
        }
    }
}
