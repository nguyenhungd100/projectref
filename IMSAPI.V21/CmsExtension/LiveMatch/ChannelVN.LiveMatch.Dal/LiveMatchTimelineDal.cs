﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using ChannelVN.CMS.Common;
using ChannelVN.LiveGameMatch.MainDal.Databases;
using ChannelVN.LiveMatch.Dal.Common;
using ChannelVN.LiveMatch.Entity;

namespace ChannelVN.LiveMatch.Dal
{
    public class LiveMatchTimelineDal
    {
        public static List<LiveMatchTimelineEntity> GetByLiveMatchId(int liveMatchId)
        {
            List<LiveMatchTimelineEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.LiveMatchTimelineMainDal.GetByLiveMatchId(liveMatchId);
            }
            return returnValue;
        }
        public static LiveMatchTimelineEntity GetById(int liveMatchTimelineId)
        {
            LiveMatchTimelineEntity returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.LiveMatchTimelineMainDal.GetById(liveMatchTimelineId);
            }
            return returnValue;
        }

        public static bool Insert(LiveMatchTimelineEntity liveMatchTimeline, ref int timelineId)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.LiveMatchTimelineMainDal.Insert(liveMatchTimeline, ref timelineId);
            }
            return returnValue;
        }
        public static bool Update(LiveMatchTimelineEntity liveMatchTimeline)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.LiveMatchTimelineMainDal.Update(liveMatchTimeline);
            }
            return returnValue;
        }
        public static bool Delete(int liveMatchTimelineId)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.LiveMatchTimelineMainDal.Delete(liveMatchTimelineId);
            }
            return returnValue;
        }
    }
}
