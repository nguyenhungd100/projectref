﻿using ChannelVN.CMS.Common;
using ChannelVN.CMS.Common.ChannelConfig;
using ChannelVN.LiveMatch.Entity;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChannelVN.LiveMatch.Bo.Nodejs
{
    public class LiveMatchNodeJsServices
    {
        private static NodeJsRestClient GetRestClient()
        {
            return new NodeJsRestClient
            {
                EndPoint = CmsChannelConfiguration.GetAppSetting("LiveMatchApiUrl"),
                Method = NodeJsRestClient.HttpMethod.POST,
                ContentType = "application/x-www-form-urlencoded; charset=utf-8",
                Channel_Id = CmsChannelConfiguration.GetAppSetting("LiveMatchApiChannel"),
                SecretKey = CmsChannelConfiguration.GetAppSetting("LiveMatchApiSecretKey"),
                PostData = "",
                ActionName = ""
            };
        }
        const string DateTimeFormat = "yyyy/MM/dd HH:mm";
        public static List<NodeJs_LiveMatchEntity> SearchLiveMatch(string keyWord, int pageIndex, int pageSize, ref int totalRow)
        {
            try
            {
                var client = GetRestClient();
                var sb = new StringBuilder();
                sb.AppendFormat("channel_id={0}", client.Channel_Id);
                sb.AppendFormat("&keyword={0}", keyWord);
                sb.AppendFormat("&size={0}", pageSize);
                sb.AppendFormat("&page={0}", pageIndex);
                sb.AppendFormat("&secret_key={0}", client.SecretKey);
                client.PostData = sb.ToString();
                client.ActionName = "search";
                var strData = client.MakeRequest();
                var ParseObject = JsonConvert.DeserializeObject<ResponseListLiveMatch>(strData);

                if (ParseObject == null) ParseObject = new ResponseListLiveMatch();
                totalRow = ParseObject.Data.total_count;
                if (!ParseObject.Success)
                    Logger.WriteLog(Logger.LogType.Error, "LiveMatch_Search=>" + ParseObject.Message);
                return ParseObject.Data == null ? new List<NodeJs_LiveMatchEntity>() : ParseObject.Data.data;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, "LiveMatch_Search=>" + ex.Message);
                return new List<NodeJs_LiveMatchEntity>();
            }
        }
        public static List<NodeJs_LiveMatchEntity> ListLiveMatchByIds(string ids)
        {
            try
            {
                var client = GetRestClient();
                var sb = new StringBuilder();
                sb.AppendFormat("channel_id={0}", client.Channel_Id);
                sb.AppendFormat("&ids={0}", ids);                
                sb.AppendFormat("&secret_key={0}", client.SecretKey);
                client.PostData = sb.ToString();
                client.ActionName = "get-by-list-ids";
                var strData = client.MakeRequest();
                var ParseObject = JsonConvert.DeserializeObject<ResponseListLiveMatchByIds>(strData);

                if (ParseObject == null) ParseObject = new ResponseListLiveMatchByIds();
                
                if (!ParseObject.success)
                    Logger.WriteLog(Logger.LogType.Error, "ListLiveMatchByIds=>" + ParseObject.message);
                return ParseObject.data == null ? new List<NodeJs_LiveMatchEntity>() : ParseObject.data;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, "ListLiveMatchByIds=>" + ex.Message);
                return new List<NodeJs_LiveMatchEntity>();
            }
        }
        public static NodeJs_LiveMatchEntity GetLiveMatchDetail(string Id)
        {
            try
            {
                var client = GetRestClient();
                var sb = new StringBuilder();
                sb.AppendFormat("channel_id={0}", client.Channel_Id);
                sb.AppendFormat("&id={0}", Id);
                sb.AppendFormat("&secret_key={0}", client.SecretKey);
                client.PostData = sb.ToString();
                client.ActionName = "get-by-id";
                var strData = client.MakeRequest();
                var ParseObject = JsonConvert.DeserializeObject<ResponseLiveMatch>(strData);

                if (ParseObject == null) ParseObject = new ResponseLiveMatch();
                //totalRow = ParseObject.Data.total_count;
                if (!ParseObject.Success)
                    Logger.WriteLog(Logger.LogType.Error, "LiveMatch_GetById=>" + ParseObject.Message);
                return ParseObject.Data;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, "LiveMatch_GetById=>" + ex.Message);
                return null;
            }
        }

        public static List<NodeJs_LiveMatchEventEntity> GetEventByLiveMatchId(string Id)
        {
            try
            {
                var client = GetRestClient();
                var sb = new StringBuilder();
                sb.AppendFormat("channel_id={0}", client.Channel_Id);
                sb.AppendFormat("&livematch_id={0}", Id);
                sb.AppendFormat("&secret_key={0}", client.SecretKey);
                client.PostData = sb.ToString();
                client.ActionName = "event/get-by-livematch-id";
                var strData = client.MakeRequest();
                var ParseObject = JsonConvert.DeserializeObject<ResponseListLiveMatchEvent>(strData);

                if (ParseObject == null) ParseObject = new ResponseListLiveMatchEvent();
                //totalRow = ParseObject.Data.total_count;
                if (!ParseObject.Success)
                    Logger.WriteLog(Logger.LogType.Error, "LiveMatch_GetEventByLiveMatchId=>" + ParseObject.Message);
                return ParseObject.Data;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, "LiveMatch_GetEventByLiveMatchId=>" + ex.Message);
                return new List<NodeJs_LiveMatchEventEntity>();
            }
        }
        public static List<NodeJs_LiveMatchPenaltyEntity> GetPenaltyByLiveMatchId(string Id)
        {
            try
            {
                var client = GetRestClient();
                var sb = new StringBuilder();
                sb.AppendFormat("channel_id={0}", client.Channel_Id);
                sb.AppendFormat("&livematch_id={0}", Id);
                sb.AppendFormat("&secret_key={0}", client.SecretKey);
                client.PostData = sb.ToString();
                client.ActionName = "penalty/get-by-livematch-id";
                var strData = client.MakeRequest();
                var ParseObject = JsonConvert.DeserializeObject<ResponseListLiveMatchPenalty>(strData);

                if (ParseObject == null) ParseObject = new ResponseListLiveMatchPenalty();
                //totalRow = ParseObject.Data.total_count;
                if (!ParseObject.Success)
                    Logger.WriteLog(Logger.LogType.Error, "LiveMatch_GetEventByLiveMatchId=>" + ParseObject.Message);
                return ParseObject.Data;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, "LiveMatch_GetEventByLiveMatchId=>" + ex.Message);
                return new List<NodeJs_LiveMatchPenaltyEntity>();
            }
        }
        public static List<NodeJs_LiveMatchTimeLinesEntity> GetTimeLinesByLiveMatchId(string Id)
        {
            try
            {
                var client = GetRestClient();
                var sb = new StringBuilder();
                sb.AppendFormat("channel_id={0}", client.Channel_Id);
                sb.AppendFormat("&livematch_id={0}", Id);
                sb.AppendFormat("&secret_key={0}", client.SecretKey);
                client.PostData = sb.ToString();
                client.ActionName = "timeline/get-by-livematch-id";
                var strData = client.MakeRequest();
                var ParseObject = JsonConvert.DeserializeObject<ResponseListLiveMatchTimeLines>(strData);

                if (ParseObject == null) ParseObject = new ResponseListLiveMatchTimeLines();
                //totalRow = ParseObject.Data.total_count;
                if (!ParseObject.Success)
                    Logger.WriteLog(Logger.LogType.Error, "LiveMatch_GetEventByLiveMatchId=>" + ParseObject.Message);
                return ParseObject.Data;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, "LiveMatch_GetEventByLiveMatchId=>" + ex.Message);
                return new List<NodeJs_LiveMatchTimeLinesEntity>(); ;
            }
        }

        public static WcfActionResponse LiveMatch_Update(NodeJs_LiveMatchEntity obj)
        {
            try
            {
                var client = GetRestClient();
                var sb = new StringBuilder();
                sb.AppendFormat("channel_id={0}", client.Channel_Id);
                sb.AppendFormat("&secret_key={0}", client.SecretKey);
                sb.AppendFormat("&id={0}", obj.Id);
                sb.AppendFormat("&title={0}", obj.Title);
                sb.AppendFormat("&team_a={0}", obj.TeamA);
                sb.AppendFormat("&team_b={0}", obj.TeamB);
                sb.AppendFormat("&logo_team_a={0}", obj.LogoTeamA);
                sb.AppendFormat("&logo_team_b={0}", obj.LogoTeamB);
                sb.AppendFormat("&score_of_team_a={0}", obj.ScoreOfTeamA);
                sb.AppendFormat("&score_of_team_b={0}", obj.ScoreOfTeamB);
                sb.AppendFormat("&stadium={0}", obj.Stadium);
                sb.AppendFormat("&referee={0}", obj.Referee);
                sb.AppendFormat("&begin_first_half={0}", obj.BeginFirstHalf);
                sb.AppendFormat("&finish_first_half={0}", obj.FinishFirstHalf);
                sb.AppendFormat("&begin_second_half={0}", obj.BeginSecondHalf);
                sb.AppendFormat("&finish_second_half={0}", obj.FinishSecondHalf);
                sb.AppendFormat("&begin_first_extra_time={0}", obj.BeginFirstExtraTime);
                sb.AppendFormat("&finish_first_extra_time={0}", obj.FinishFirstExtraTime);
                sb.AppendFormat("&begin_second_extra_time={0}", obj.BeginSecondExtraTime);
                sb.AppendFormat("&finish_second_extra_time={0}", obj.FinishSecondExtraTime);
                sb.AppendFormat("&is_penalty={0}", obj.IsPenalty);
                sb.AppendFormat("&match_finished={0}", obj.MatchFinished);
                sb.AppendFormat("&time_of_match_begin={0}", obj.TimeOfMatchBegin);
                sb.AppendFormat("&time_of_finish_first_half={0}", obj.TimeOfFinishFirstHalf);
                sb.AppendFormat("&time_of_begin_second_half={0}", obj.TimeOfBeginSecondHalf);
                sb.AppendFormat("&time_of_finish_second_half={0}", obj.TimeOfFinishSecondHalf);
                sb.AppendFormat("&time_of_begin_first_extra_time={0}", obj.TimeOfBeginFirstExtraTime);
                sb.AppendFormat("&time_of_finish_first_extra_time={0}", obj.TimeOfFinishFirstExtraTime);
                sb.AppendFormat("&time_of_begin_second_extra_time={0}", obj.TimeOfBeginSecondExtraTime);
                sb.AppendFormat("&time_of_finish_second_extra_time={0}", obj.TimeOfFinishSecondExtraTime);
                sb.AppendFormat("&time_of_match_finished={0}", obj.TimeOfMatchFinished);
                sb.AppendFormat("&status={0}", obj.Status);
                sb.AppendFormat("&link_livetv_1={0}", obj.LinkLiveTV1);
                sb.AppendFormat("&link_livetv_2={0}", obj.LinkLiveTV2);
                sb.AppendFormat("&link_livetv_3={0}", obj.LinkLiveTV3);
                sb.AppendFormat("&link_livetv_4={0}", obj.LinkLiveTV4);
                sb.AppendFormat("&link_livetv_5={0}", obj.LinkLiveTV5);
                sb.AppendFormat("&show_on_livetv={0}", obj.ShowOnLiveTV);
                sb.AppendFormat("&infomation_before_match={0}", System.Web.HttpContext.Current.Server.UrlEncode(obj.InfomationBeforeMatch));
                sb.AppendFormat("&match_status={0}", obj.MatchStatus);
                client.PostData = sb.ToString();
                client.ActionName = "update";
                var data = JsonConvert.DeserializeObject<NodeJsRestClient.ApiActionResponse>(client.MakeRequest());

                return NodeJsRestClient.ConvertWcfResponse(data);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, "LiveMatch_Update=>" + ex.Message);
                return new WcfActionResponse();
            }
        }

        public static WcfActionResponse LiveMatch_Insert(NodeJs_LiveMatchEntity obj)
        {
            try
            {
                var client = GetRestClient();
                var sb = new StringBuilder();
                sb.AppendFormat("channel_id={0}", client.Channel_Id);
                sb.AppendFormat("&secret_key={0}", client.SecretKey);
                sb.AppendFormat("&title={0}", obj.Title);
                sb.AppendFormat("&team_a={0}", obj.TeamA);
                sb.AppendFormat("&team_b={0}", obj.TeamB);
                sb.AppendFormat("&logo_team_a={0}", obj.LogoTeamA);
                sb.AppendFormat("&logo_team_b={0}", obj.LogoTeamB);
                sb.AppendFormat("&score_of_team_a={0}", obj.ScoreOfTeamA);
                sb.AppendFormat("&score_of_team_b={0}", obj.ScoreOfTeamB);
                sb.AppendFormat("&stadium={0}", obj.Stadium);
                sb.AppendFormat("&referee={0}", obj.Referee);
                sb.AppendFormat("&link_livetv_1={0}", obj.LinkLiveTV1);
                sb.AppendFormat("&link_livetv_2={0}", obj.LinkLiveTV2);
                sb.AppendFormat("&link_livetv_3={0}", obj.LinkLiveTV3);
                sb.AppendFormat("&link_livetv_4={0}", obj.LinkLiveTV4);
                sb.AppendFormat("&link_livetv_5={0}", obj.LinkLiveTV5);
                sb.AppendFormat("&show_on_livetv={0}", obj.ShowOnLiveTV);
                sb.AppendFormat("&time_of_match_begin={0}", obj.TimeOfMatchBegin);
                sb.AppendFormat("&created_by={0}", obj.CreatedBy);

                client.PostData = sb.ToString();
                client.ActionName = "add";
                var data = JsonConvert.DeserializeObject<NodeJsRestClient.ApiActionResponse>(client.MakeRequest());

                return NodeJsRestClient.ConvertWcfResponse(data);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, "LiveMatch_Insert=>" + ex.Message);
                return new WcfActionResponse();
            }
        }


        public static WcfActionResponse LiveMatch_InsertPenalty(NodeJs_LiveMatchPenaltyEntity obj)
        {
            try
            {
                var client = GetRestClient();
                var sb = new StringBuilder();
                sb.AppendFormat("channel_id={0}", client.Channel_Id);
                sb.AppendFormat("&secret_key={0}", client.SecretKey);
                sb.AppendFormat("&livematch_id={0}", obj.LiveMatchId);
                sb.AppendFormat("&turn={0}", obj.Turn);
                sb.AppendFormat("&player_name={0}", obj.PlayerName);
                sb.AppendFormat("&is_team_a={0}", obj.IsTeamA);
                sb.AppendFormat("&is_goal={0}", obj.IsGoal);
                sb.AppendFormat("&created_by={0}", obj.CreatedBy);
                client.PostData = sb.ToString();
                client.ActionName = "penalty/add";
                var data = JsonConvert.DeserializeObject<NodeJsRestClient.ApiActionResponse>(client.MakeRequest());

                return NodeJsRestClient.ConvertWcfResponse(data);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, "LiveMatch_InsertPenalty=>" + ex.Message);
                return new WcfActionResponse();
            }
        }

        public static WcfActionResponse LiveMatch_UpdatePenalty(NodeJs_LiveMatchPenaltyEntity obj)
        {
            try
            {
                var client = GetRestClient();
                var sb = new StringBuilder();
                sb.AppendFormat("channel_id={0}", client.Channel_Id);
                sb.AppendFormat("&secret_key={0}", client.SecretKey);
                sb.AppendFormat("&id={0}", obj.Id);
                sb.AppendFormat("&turn={0}", obj.Turn);
                sb.AppendFormat("&player_name={0}", obj.PlayerName);
                sb.AppendFormat("&is_team_a={0}", obj.IsTeamA);
                sb.AppendFormat("&is_goal={0}", obj.IsGoal);
                sb.AppendFormat("&created_by={0}", obj.CreatedBy);
                client.PostData = sb.ToString();
                client.ActionName = "penalty/update";
                var data = JsonConvert.DeserializeObject<NodeJsRestClient.ApiActionResponse>(client.MakeRequest());

                return NodeJsRestClient.ConvertWcfResponse(data);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, "LiveMatch_UpdatePenalty=>" + ex.Message);
                return new WcfActionResponse();
            }
        }

        public static WcfActionResponse LiveMatch_DeletePenalty(string Id)
        {
            try
            {
                var client = GetRestClient();
                var sb = new StringBuilder();
                sb.AppendFormat("channel_id={0}", client.Channel_Id);
                sb.AppendFormat("&secret_key={0}", client.SecretKey);
                sb.AppendFormat("&id={0}", Id);
                client.PostData = sb.ToString();
                client.ActionName = "penalty/remove";
                var data = JsonConvert.DeserializeObject<NodeJsRestClient.ApiActionResponse>(client.MakeRequest());

                return NodeJsRestClient.ConvertWcfResponse(data);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, "LiveMatch_DeletePenalty=>" + ex.Message);
                return new WcfActionResponse();
            }
        }

        public static NodeJs_LiveMatchPenaltyEntity GetLiveMatchPenaltyById(string Id)
        {
            try
            {
                var client = GetRestClient();
                var sb = new StringBuilder();
                sb.AppendFormat("channel_id={0}", client.Channel_Id);
                sb.AppendFormat("&id={0}", Id);
                sb.AppendFormat("&secret_key={0}", client.SecretKey);
                client.PostData = sb.ToString();
                client.ActionName = "penalty/get-by-id";
                var strData = client.MakeRequest();
                var ParseObject = JsonConvert.DeserializeObject<ResponseLiveMatchPenalty>(strData);

                if (ParseObject == null) ParseObject = new ResponseLiveMatchPenalty();
                //totalRow = ParseObject.Data.total_count;
                if (!ParseObject.Success)
                    Logger.WriteLog(Logger.LogType.Error, "LiveMatch_GetById=>" + ParseObject.Message);
                return ParseObject.Data;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, "LiveMatch_GetById=>" + ex.Message);
                return null;
            }
        }


        public static WcfActionResponse LiveMatch_InsertEvent(NodeJs_LiveMatchEventEntity obj)
        {
            try
            {
                var client = GetRestClient();
                var sb = new StringBuilder();
                sb.AppendFormat("channel_id={0}", client.Channel_Id);
                sb.AppendFormat("&secret_key={0}", client.SecretKey);
                sb.AppendFormat("&livematch_id={0}", obj.LiveMatchId);
                sb.AppendFormat("&in_minute={0}", obj.InMinute);
                sb.AppendFormat("&event_type={0}", obj.EventType);
                sb.AppendFormat("&is_team_a={0}", obj.IsTeamA);
                sb.AppendFormat("&player_name={0}", obj.PlayerName);
                sb.AppendFormat("&player_name_out={0}", obj.PlayerNameOut);
                sb.AppendFormat("&created_by={0}", obj.CreatedBy);
                client.PostData = sb.ToString();
                client.ActionName = "event/add";
                var data = JsonConvert.DeserializeObject<NodeJsRestClient.ApiActionResponse>(client.MakeRequest());

                return NodeJsRestClient.ConvertWcfResponse(data);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, "LiveMatch_InsertEvent=>" + ex.Message);
                return new WcfActionResponse();
            }
        }

        public static WcfActionResponse LiveMatch_UpdateEvent(NodeJs_LiveMatchEventEntity obj)
        {
            try
            {
                var client = GetRestClient();
                var sb = new StringBuilder();
                sb.AppendFormat("channel_id={0}", client.Channel_Id);
                sb.AppendFormat("&secret_key={0}", client.SecretKey);
                sb.AppendFormat("&id={0}", obj.Id);
                sb.AppendFormat("&in_minute={0}", obj.InMinute);
                sb.AppendFormat("&event_type={0}", obj.EventType);
                sb.AppendFormat("&is_team_a={0}", obj.IsTeamA);
                sb.AppendFormat("&player_name={0}", obj.PlayerName);
                sb.AppendFormat("&player_name_out={0}", obj.PlayerNameOut);
                sb.AppendFormat("&created_by={0}", obj.CreatedBy);
                client.PostData = sb.ToString();
                client.ActionName = "event/update";
                var data = JsonConvert.DeserializeObject<NodeJsRestClient.ApiActionResponse>(client.MakeRequest());

                return NodeJsRestClient.ConvertWcfResponse(data);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, "LiveMatch_UpdateEvent=>" + ex.Message);
                return new WcfActionResponse();
            }
        }

        public static WcfActionResponse LiveMatch_DeleteEvent(string Id)
        {
            try
            {
                var client = GetRestClient();
                var sb = new StringBuilder();
                sb.AppendFormat("channel_id={0}", client.Channel_Id);
                sb.AppendFormat("&secret_key={0}", client.SecretKey);
                sb.AppendFormat("&id={0}", Id);
                client.PostData = sb.ToString();
                client.ActionName = "event/remove";
                var data = JsonConvert.DeserializeObject<NodeJsRestClient.ApiActionResponse>(client.MakeRequest());

                return NodeJsRestClient.ConvertWcfResponse(data);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, "LiveMatch_DeleteEvent=>" + ex.Message);
                return new WcfActionResponse();
            }
        }

        public static NodeJs_LiveMatchEventEntity GetLiveMatchEventById(string Id)
        {
            try
            {
                var client = GetRestClient();
                var sb = new StringBuilder();
                sb.AppendFormat("channel_id={0}", client.Channel_Id);
                sb.AppendFormat("&id={0}", Id);
                sb.AppendFormat("&secret_key={0}", client.SecretKey);
                client.PostData = sb.ToString();
                client.ActionName = "event/get-by-id";
                var strData = client.MakeRequest();
                var ParseObject = JsonConvert.DeserializeObject<ResponseLiveMatchEvent>(strData);

                if (ParseObject == null) ParseObject = new ResponseLiveMatchEvent();
                //totalRow = ParseObject.Data.total_count;
                if (!ParseObject.Success)
                    Logger.WriteLog(Logger.LogType.Error, "LiveMatchEvent_GetById=>" + ParseObject.Message);
                return ParseObject.Data;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, "LiveMatchEvent_GetById=>" + ex.Message);
                return null ;
            }
        }



        public static WcfActionResponse LiveMatch_InsertTimeLines(NodeJs_LiveMatchTimeLinesEntity obj)
        {
            try
            {
                var client = GetRestClient();
                var sb = new StringBuilder();
                sb.AppendFormat("channel_id={0}", client.Channel_Id);
                sb.AppendFormat("&secret_key={0}", client.SecretKey);
                sb.AppendFormat("&livematch_id={0}", obj.LiveMatchId);
                sb.AppendFormat("&in_minute={0}", obj.InMinute);
                sb.AppendFormat("&event_type={0}", obj.EventType);
                sb.AppendFormat("&description={0}", obj.Description);
                sb.AppendFormat("&video_key={0}", obj.VideoKey);
                sb.AppendFormat("&video_avatar={0}", obj.VideoAvatar);
                sb.AppendFormat("&images={0}", obj.Images);
                sb.AppendFormat("&extra_time={0}", obj.ExtraTime);
                sb.AppendFormat("&status={0}", obj.Status);
                sb.AppendFormat("&created_by={0}", obj.CreatedBy);
                client.PostData = sb.ToString();
                client.ActionName = "timeline/add";
                var data = JsonConvert.DeserializeObject<NodeJsRestClient.ApiActionResponse>(client.MakeRequest());

                return NodeJsRestClient.ConvertWcfResponse(data);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, "LiveMatch_InsertTimeLines=>" + ex.Message);
                return new WcfActionResponse();
            }
        }

        public static WcfActionResponse LiveMatch_UpdateTimeLines(NodeJs_LiveMatchTimeLinesEntity obj)
        {
            try
            {
                var client = GetRestClient();
                var sb = new StringBuilder();
                sb.AppendFormat("channel_id={0}", client.Channel_Id);
                sb.AppendFormat("&secret_key={0}", client.SecretKey);
                sb.AppendFormat("&id={0}", obj.Id);
                sb.AppendFormat("&in_minute={0}", obj.InMinute);
                sb.AppendFormat("&event_type={0}", obj.EventType);
                sb.AppendFormat("&description={0}", obj.Description);
                sb.AppendFormat("&video_key={0}", obj.VideoKey);
                sb.AppendFormat("&video_avatar={0}", obj.VideoAvatar);
                sb.AppendFormat("&images={0}", obj.Images);
                sb.AppendFormat("&extra_time={0}", obj.ExtraTime);
                sb.AppendFormat("&status={0}", obj.Status);
                client.PostData = sb.ToString();
                client.ActionName = "timeline/update";

                var res = client.MakeRequest();
                if (string.IsNullOrEmpty(res))
                {
                    return new WcfActionResponse();
                }

                var data = JsonConvert.DeserializeObject<NodeJsRestClient.ApiActionResponse>(res);

                return NodeJsRestClient.ConvertWcfResponse(data);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, "LiveMatch_UpdateTimeLines=>" + ex.Message);
                return new WcfActionResponse();
            }
        }

        public static WcfActionResponse LiveMatch_DeleteTimeLines(string Id)
        {
            try
            {
                var client = GetRestClient();
                var sb = new StringBuilder();
                sb.AppendFormat("channel_id={0}", client.Channel_Id);
                sb.AppendFormat("&secret_key={0}", client.SecretKey);
                sb.AppendFormat("&id={0}", Id);
                client.PostData = sb.ToString();
                client.ActionName = "timeline/remove";
                var data = JsonConvert.DeserializeObject<NodeJsRestClient.ApiActionResponse>(client.MakeRequest());

                return NodeJsRestClient.ConvertWcfResponse(data);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, "LiveMatch_DeleteTimeLines=>" + ex.Message);
                return new WcfActionResponse();
            }
        }

        public static NodeJs_LiveMatchTimeLinesEntity GetLiveMatchTimelinesDetail(string Id)
        {
            try
            {
                var client = GetRestClient();
                var sb = new StringBuilder();
                sb.AppendFormat("channel_id={0}", client.Channel_Id);
                sb.AppendFormat("&id={0}", Id);
                sb.AppendFormat("&secret_key={0}", client.SecretKey);
                client.PostData = sb.ToString();
                client.ActionName = "timeline/get-by-id";
                var strData = client.MakeRequest();
                var ParseObject = JsonConvert.DeserializeObject<ResponseLiveMatchTimeLines>(strData);

                if (ParseObject == null) ParseObject = new ResponseLiveMatchTimeLines();
                //totalRow = ParseObject.Data.total_count;
                if (!ParseObject.Success)
                    Logger.WriteLog(Logger.LogType.Trace, "GetLiveMatchTimelinesDetail=>" + ParseObject.Message);
                return ParseObject.Data;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, "GetLiveMatchTimelinesDetail=>" + ex.Message);
                return null;
            }
        }
    }
}
