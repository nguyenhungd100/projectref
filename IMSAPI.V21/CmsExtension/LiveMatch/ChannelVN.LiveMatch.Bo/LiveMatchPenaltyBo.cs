﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ChannelVN.LiveMatch.Dal;
using ChannelVN.LiveMatch.Entity;
using ChannelVN.LiveMatch.Entity.ErrorCode;
using ChannelVN.LiveMatch.Bo.Nodejs;

namespace ChannelVN.LiveMatch.Bo
{
    public class LiveMatchPenaltyBo
    {
        public static List<NodeJs_LiveMatchPenaltyEntity> GetLiveMatchPenaltyByLiveMatchId(string liveMatchId)
        {
            //return LiveMatchPenaltyDal.GetByLiveMatchId(liveMatchId);
            //goi tu nodejs
            return LiveMatchNodeJsServices.GetPenaltyByLiveMatchId(liveMatchId);
        }
        public static NodeJs_LiveMatchPenaltyEntity GetLiveMatchPenaltyById(string liveMatchPenaltyId)
        {
            //return LiveMatchPenaltyDal.GetById(liveMatchPenaltyId);
            return LiveMatchNodeJsServices.GetLiveMatchPenaltyById(liveMatchPenaltyId);
        }
        public static ErrorMapping.ErrorCodes InsertLiveMatchPenalty(NodeJs_LiveMatchPenaltyEntity liveMatchPenalty, ref string Id)
        {
            if (string.IsNullOrEmpty(liveMatchPenalty.LiveMatchId))
            {
                return ErrorMapping.ErrorCodes.UpdateLiveMatchPenaltyNotFoundLiveMatch;
            }
            //var liveMatch = LiveMatchDal.GetById(liveMatchPenalty.LiveMatchId);
            //if (liveMatch == null)
            //{
            //    return ErrorMapping.ErrorCodes.UpdateLiveMatchPenaltyNotFoundLiveMatch;
            //}
            //return LiveMatchPenaltyDal.Insert(liveMatchPenalty)
            //           ? ErrorMapping.ErrorCodes.Success
            //           : ErrorMapping.ErrorCodes.BusinessError;

            var response = LiveMatchNodeJsServices.LiveMatch_InsertPenalty(liveMatchPenalty);
            Id = response.Data;
            return response.Success ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.BusinessError;
        }
        public static ErrorMapping.ErrorCodes UpdateLiveMatchPenalty(NodeJs_LiveMatchPenaltyEntity liveMatchPenalty)
        {
            //if (liveMatchPenalty.Id <= 0)
            //{
            //    return ErrorMapping.ErrorCodes.UpdateLiveMatchPenaltyNotFoundPenalty;
            //}
            //var currentPenalty = LiveMatchPenaltyDal.GetById(liveMatchPenalty.Id);
            //if (currentPenalty == null)
            //{
            //    return ErrorMapping.ErrorCodes.UpdateLiveMatchPenaltyNotFoundPenalty;
            //}
            //if (liveMatchPenalty.LiveMatchId <= 0)
            //{
            //    return ErrorMapping.ErrorCodes.UpdateLiveMatchPenaltyNotFoundLiveMatch;
            //}
            //var liveMatch = LiveMatchDal.GetById(liveMatchPenalty.LiveMatchId);
            //if (liveMatch == null)
            //{
            //    return ErrorMapping.ErrorCodes.UpdateLiveMatchPenaltyNotFoundLiveMatch;
            //}
            //currentPenalty.Turn = liveMatchPenalty.Turn;
            //currentPenalty.PlayerName = liveMatchPenalty.PlayerName;
            //currentPenalty.IsTeamA = liveMatchPenalty.IsTeamA;
            //currentPenalty.IsGoal = liveMatchPenalty.IsGoal;

            //return LiveMatchPenaltyDal.Update(currentPenalty)
            //           ? ErrorMapping.ErrorCodes.Success
            //           : ErrorMapping.ErrorCodes.BusinessError;

            //goi nodejs
            var response = LiveMatchNodeJsServices.LiveMatch_UpdatePenalty(liveMatchPenalty);
            return response.Success ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.BusinessError;
        }
        public static ErrorMapping.ErrorCodes DeleteLiveMatchPenalty(string liveMatchPenaltyId)
        {
            if (string.IsNullOrEmpty(liveMatchPenaltyId))
            {
                return ErrorMapping.ErrorCodes.UpdateLiveMatchPenaltyNotFoundPenalty;
            }
            //var currentPenalty = LiveMatchPenaltyDal.GetById(liveMatchPenaltyId);
            //if (currentPenalty == null)
            //{
            //    return ErrorMapping.ErrorCodes.UpdateLiveMatchPenaltyNotFoundPenalty;
            //}
            //return LiveMatchPenaltyDal.Delete(liveMatchPenaltyId)
            //           ? ErrorMapping.ErrorCodes.Success
            //           : ErrorMapping.ErrorCodes.BusinessError;

            //goi nodejs
            var response = LiveMatchNodeJsServices.LiveMatch_DeletePenalty(liveMatchPenaltyId);
            return response.Success ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.BusinessError;
        }
    }
}
