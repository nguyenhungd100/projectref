﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ChannelVN.LiveMatch.Dal;
using ChannelVN.LiveMatch.Entity;
using ChannelVN.LiveMatch.Entity.ErrorCode;
using ChannelVN.LiveMatch.Bo.Nodejs;

namespace ChannelVN.LiveMatch.Bo
{
    public class LiveMatchTimelineBo
    {
        public static List<NodeJs_LiveMatchTimeLinesEntity> GetLiveMatchTimelineByLiveMatchId(string liveMatchId)
        {
            //return LiveMatchTimelineDal.GetByLiveMatchId(liveMatchId);
            //goi tu nodejs
            return LiveMatchNodeJsServices.GetTimeLinesByLiveMatchId(liveMatchId);
        }
        public static NodeJs_LiveMatchTimeLinesEntity GetLiveMatchTimelineById(string liveMatchTimelineId)
        {
            //return LiveMatchTimelineDal.GetById(liveMatchTimelineId);
            return LiveMatchNodeJsServices.GetLiveMatchTimelinesDetail(liveMatchTimelineId);
        }
        public static ErrorMapping.ErrorCodes InsertLiveMatchTimeline(NodeJs_LiveMatchTimeLinesEntity liveMatchTimeline, ref string Id)
        {
            if (string.IsNullOrEmpty(liveMatchTimeline.LiveMatchId))
            {
                return ErrorMapping.ErrorCodes.UpdateLiveMatchTimelineNotFoundLiveMatch;
            }
            //var liveMatch = LiveMatchDal.GetById(liveMatchTimeline.LiveMatchId);
            //if (liveMatch == null)
            //{
            //    return ErrorMapping.ErrorCodes.UpdateLiveMatchTimelineNotFoundLiveMatch;
            //}
            //return LiveMatchTimelineDal.Insert(liveMatchTimeline, ref timelineId)
            //           ? ErrorMapping.ErrorCodes.Success
            //           : ErrorMapping.ErrorCodes.BusinessError;

            var response = LiveMatchNodeJsServices.LiveMatch_InsertTimeLines(liveMatchTimeline);
            Id = response.Data;
            return response.Success ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.BusinessError;
        }
        public static ErrorMapping.ErrorCodes UpdateLiveMatchTimeline(NodeJs_LiveMatchTimeLinesEntity liveMatchTimeline)
        {
            //if (liveMatchTimeline.Id <= 0)
            //{
            //    return ErrorMapping.ErrorCodes.UpdateLiveMatchTimelineNotFoundTimeLine;
            //}
            //var currentTimeline = LiveMatchTimelineDal.GetById(liveMatchTimeline.Id);
            //if (currentTimeline == null)
            //{
            //    return ErrorMapping.ErrorCodes.UpdateLiveMatchTimelineNotFoundTimeLine;
            //}
            //if (liveMatchTimeline.LiveMatchId <= 0)
            //{
            //    return ErrorMapping.ErrorCodes.UpdateLiveMatchTimelineNotFoundLiveMatch;
            //}
            //var liveMatch = LiveMatchDal.GetById(liveMatchTimeline.LiveMatchId);
            //if (liveMatch == null)
            //{
            //    return ErrorMapping.ErrorCodes.UpdateLiveMatchTimelineNotFoundLiveMatch;
            //}
            //currentTimeline.InMinute = liveMatchTimeline.InMinute;
            //currentTimeline.EventType = liveMatchTimeline.EventType;
            //currentTimeline.Description = liveMatchTimeline.Description;
            //currentTimeline.VideoKey = liveMatchTimeline.VideoKey;
            //currentTimeline.VideoAvatar = liveMatchTimeline.VideoAvatar;
            //currentTimeline.Images = liveMatchTimeline.Images;
            //currentTimeline.ExtraTime = liveMatchTimeline.ExtraTime;
            //currentTimeline.Status = liveMatchTimeline.Status;

            //return LiveMatchTimelineDal.Update(currentTimeline)
            //           ? ErrorMapping.ErrorCodes.Success
            //           : ErrorMapping.ErrorCodes.BusinessError;


            //goi nodejs
            var response = LiveMatchNodeJsServices.LiveMatch_UpdateTimeLines(liveMatchTimeline);
            return response.Success ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.BusinessError;
        }
        public static ErrorMapping.ErrorCodes DeleteLiveMatchTimeline(string liveMatchTimelineId)
        {
            if (string.IsNullOrEmpty(liveMatchTimelineId))
            {
                return ErrorMapping.ErrorCodes.UpdateLiveMatchTimelineNotFoundTimeLine;
            }
            //var currentTimeline = LiveMatchTimelineDal.GetById(liveMatchTimelineId);
            //if (currentTimeline == null)
            //{
            //    return ErrorMapping.ErrorCodes.UpdateLiveMatchTimelineNotFoundTimeLine;
            //}
            //return LiveMatchTimelineDal.Delete(liveMatchTimelineId)
            //           ? ErrorMapping.ErrorCodes.Success
            //           : ErrorMapping.ErrorCodes.BusinessError;

            //goi nodejs
            var response = LiveMatchNodeJsServices.LiveMatch_DeleteTimeLines(liveMatchTimelineId);
            return response.Success ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.BusinessError;
        }
    }
}
