﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ChannelVN.LiveMatch.Dal;
using ChannelVN.LiveMatch.Entity;
using ChannelVN.LiveMatch.Entity.ErrorCode;
using ChannelVN.LiveMatch.Bo.Nodejs;

namespace ChannelVN.LiveMatch.Bo
{
    public class LiveMatchEventBo
    {
        public static List<NodeJs_LiveMatchEventEntity> GetLiveMatchEventByLiveMatchId(string liveMatchId)
        {
            //return LiveMatchEventDal.GetByLiveMatchId(liveMatchId);

            //goi nodejs
            return LiveMatchNodeJsServices.GetEventByLiveMatchId(liveMatchId);
        }
        public static NodeJs_LiveMatchEventEntity GetLiveMatchEventById(string liveMatchEventId)
        {
            //return LiveMatchEventDal.GetById(liveMatchEventId);

            //goi tu nodejs
            return LiveMatchNodeJsServices.GetLiveMatchEventById(liveMatchEventId);
        }
               
        public static ErrorMapping.ErrorCodes InsertLiveMatchEvent(NodeJs_LiveMatchEventEntity livematchEvent, ref string Id)
        {
            if (string.IsNullOrEmpty(livematchEvent.LiveMatchId))
            {
                return ErrorMapping.ErrorCodes.UpdateLiveMatchEventNotFoundLiveMatch;
            }

            //    var liveMatch = LiveMatchDal.GetById(liveMatchEvent.LiveMatchId);
            //    if (liveMatch == null)
            //    {
            //        return ErrorMapping.ErrorCodes.UpdateLiveMatchEventNotFoundLiveMatch;
            //    }
            //    return LiveMatchEventDal.Insert(liveMatchEvent)
            //               ? ErrorMapping.ErrorCodes.Success
            //               : ErrorMapping.ErrorCodes.BusinessError;

            //goi nodejs
            var response = LiveMatchNodeJsServices.LiveMatch_InsertEvent(livematchEvent);
            Id = response.Data;
            return response.Success ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.BusinessError;
        }

        public static ErrorMapping.ErrorCodes UpdateLiveMatchEvent(NodeJs_LiveMatchEventEntity liveMatchEvent)
        {
            //if (liveMatchEvent.Id <= 0)
            //{
            //    return ErrorMapping.ErrorCodes.UpdateLiveMatchEventNotFoundEvent;
            //}
            //var currentEvent = LiveMatchEventDal.GetById(liveMatchEvent.Id);
            //if (currentEvent == null)
            //{
            //    return ErrorMapping.ErrorCodes.UpdateLiveMatchEventNotFoundEvent;
            //}
            //if (liveMatchEvent.LiveMatchId <= 0)
            //{
            //    return ErrorMapping.ErrorCodes.UpdateLiveMatchTimelineNotFoundLiveMatch;
            //}
            //var liveMatch = LiveMatchDal.GetById(liveMatchEvent.LiveMatchId);
            //if (liveMatch == null)
            //{
            //    return ErrorMapping.ErrorCodes.UpdateLiveMatchTimelineNotFoundLiveMatch;
            //}
            //currentEvent.InMinute = liveMatchEvent.InMinute;
            //currentEvent.EventType = liveMatchEvent.EventType;
            //currentEvent.IsTeamA = liveMatchEvent.IsTeamA;
            //currentEvent.PlayerName = liveMatchEvent.PlayerName;
            //currentEvent.PlayerNameOut = liveMatchEvent.PlayerNameOut;

            //return LiveMatchEventDal.Update(currentEvent)
            //           ? ErrorMapping.ErrorCodes.Success
            //           : ErrorMapping.ErrorCodes.BusinessError;

            //goi nodejs
            var response = LiveMatchNodeJsServices.LiveMatch_UpdateEvent(liveMatchEvent);
            return response.Success ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.BusinessError;
        }
        public static ErrorMapping.ErrorCodes DeleteLiveMatchEvent(string liveMatchEventId)
        {
            if (string.IsNullOrEmpty(liveMatchEventId))
            {
                return ErrorMapping.ErrorCodes.UpdateLiveMatchEventNotFoundEvent;
            }

            //return LiveMatchEventDal.Delete(liveMatchEventId)
            //           ? ErrorMapping.ErrorCodes.Success
            //           : ErrorMapping.ErrorCodes.BusinessError;

            //goi nodejs
            var response = LiveMatchNodeJsServices.LiveMatch_DeleteEvent(liveMatchEventId);
            return response.Success ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.BusinessError;
        }
    }
}
