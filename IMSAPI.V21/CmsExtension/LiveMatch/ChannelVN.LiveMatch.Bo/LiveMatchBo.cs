﻿using System;
using System.Collections.Generic;
using ChannelVN.CMS.Common;
using ChannelVN.LiveMatch.Entity;
using ChannelVN.LiveMatch.Entity.ErrorCode;
using ChannelVN.LiveMatch.Bo.Nodejs;
using ChannelVN.CMS.Entity.Base.News;
using System.Linq;

namespace ChannelVN.LiveMatch.Bo
{
    public class LiveMatchBo
    {

        public static List<NodeJs_LiveMatchEntity> SearchLiveMatch(string keyword, int pageIndex, int pageSize, ref int totalRow)
        {
            //return LiveMatchDal.Search(keyword, pageIndex, pageSize, ref totalRow);

            //goi tu nodejs
            return LiveMatchNodeJsServices.SearchLiveMatch(keyword, pageIndex, pageSize, ref totalRow);
        }
        public static List<NodeJs_LiveMatchEntity> ListLiveMatchByIds(string ids)
        {
            //lay id nodejs trong extension to redis
            var newsIds = new List<string>();
            newsIds.AddRange(ids.Split(','));
            var data = CMS.BoCached.Base.News.NewsDalFactory.GetNewsCachedByListId(newsIds);
            var node_ids = new List<NewsExtensionEntity>();
            if (data != null && data.Count > 0)
            {
                foreach (var item in data)
                {
                    var ext = item.NewsExtensions;
                    if (ext != null && ext.Count > 0)
                    {
                        foreach (var e in ext)
                        {
                            if (e.Type == (int)EnumNewsExtensionType.LiveMatchId && e.Value != "0")
                            {
                                node_ids.Add(e);
                                break;
                            }
                        }
                    }                    
                }
            }
            //goi tu nodejs
            var list = new List<NodeJs_LiveMatchEntity>();
            if (node_ids != null && node_ids.Count > 0)
                list = LiveMatchNodeJsServices.ListLiveMatchByIds(string.Join(",", node_ids.Select(s=>s.Value)));

            var listEdit = new List<NodeJs_LiveMatchEntity>();
            if (list != null && list.Count > 0)
            {
                foreach (var itm in list)
                {
                    var flg = false;
                    foreach (var elm in node_ids)
                    {
                        if (itm.Id == elm.Value)
                        {
                            itm.NewsId = elm.NewsId.ToString();
                            flg = true;
                            listEdit.Add(itm);
                            break;
                        }
                    }
                    if (!flg)
                    {
                        listEdit.Add(itm);
                    }
                }
            }
            return listEdit;           
        }
        //public static List<LiveMatchEntity> SearchLiveMatchForSuggestion(string keyword, int top)
        //{
        //    var totalRow = 0;
        //    return LiveMatchDal.Search(keyword, 1, top, ref totalRow);
        //}
        public static NodeJs_LiveMatchEntity GetLiveMatchById(string id)
        {
            //return LiveMatchDal.GetById(liveMatchId);
            //goi nodejs
            return LiveMatchNodeJsServices.GetLiveMatchDetail(id);
        }
        //public static LiveMatchDetailEntity GetLiveMatchDetail(int liveMatchId)
        //{
        //    var liveMatch = LiveMatchDal.GetById(liveMatchId);
        //    if (liveMatch != null)
        //    {
        //        return new LiveMatchDetailEntity
        //        {
        //            LiveMatch = liveMatch,
        //            LiveMatchTimelines = LiveMatchTimelineDal.GetByLiveMatchId(liveMatchId),
        //            LiveMatchEvents = LiveMatchEventDal.GetByLiveMatchId(liveMatchId),
        //            LiveMatchPenalty = LiveMatchPenaltyDal.GetByLiveMatchId(liveMatchId)
        //        };
        //    }
        //    return null;
        //}
        //public static ErrorMapping.ErrorCodes InsertLiveMatch(LiveMatchEntity liveMatch)
        //{
        //    if (string.IsNullOrEmpty(liveMatch.Title))
        //    {
        //        return ErrorMapping.ErrorCodes.UpdateLiveMatchInvalidTitle;
        //    }
        //    if (string.IsNullOrEmpty(liveMatch.TeamA))
        //    {
        //        return ErrorMapping.ErrorCodes.UpdateLiveMatchInvalidTeamA;
        //    }
        //    if (string.IsNullOrEmpty(liveMatch.TeamB))
        //    {
        //        return ErrorMapping.ErrorCodes.UpdateLiveMatchInvalidTeamB;
        //    }
        //    if (liveMatch.TimeOfMatchBegin <= DbCommon.MinDateTime)
        //    {
        //        return ErrorMapping.ErrorCodes.UpdateLiveMatchInvalidTimeOfMatchBegin;
        //    }
        //    return LiveMatchDal.Insert(liveMatch)
        //               ? ErrorMapping.ErrorCodes.Success
        //               : ErrorMapping.ErrorCodes.BusinessError;
        //}
        public static ErrorMapping.ErrorCodes InsertLiveMatchV2(NodeJs_LiveMatchEntity liveMatch, ref string Id)
        {
            if (string.IsNullOrEmpty(liveMatch.Title))
            {
                return ErrorMapping.ErrorCodes.UpdateLiveMatchInvalidTitle;
            }
            if (string.IsNullOrEmpty(liveMatch.TeamA))
            {
                return ErrorMapping.ErrorCodes.UpdateLiveMatchInvalidTeamA;
            }
            if (string.IsNullOrEmpty(liveMatch.TeamB))
            {
                return ErrorMapping.ErrorCodes.UpdateLiveMatchInvalidTeamB;
            }
            //if (liveMatch.TimeOfMatchBegin <= DbCommon.MinDateTime)
            //{
            //    return ErrorMapping.ErrorCodes.UpdateLiveMatchInvalidTimeOfMatchBegin;
            //}
            //return LiveMatchDal.InsertV2(liveMatch, ref Id)
            //           ? ErrorMapping.ErrorCodes.Success
            //           : ErrorMapping.ErrorCodes.BusinessError;

            //goi nodejs
            var response = LiveMatchNodeJsServices.LiveMatch_Insert(liveMatch);
            Id = response.Data;
            return response.Success ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.BusinessError;
        }
        public static ErrorMapping.ErrorCodes UpdateLiveMatch(NodeJs_LiveMatchEntity liveMatch)
        {
            try
            {
                if (string.IsNullOrEmpty(liveMatch.Id))
                {
                    return ErrorMapping.ErrorCodes.UpdateLiveMatchNotFoundLiveMatch;
                }
                //var currentLiveMatch = LiveMatchNodeJsServices.GetLiveMatchDetail(liveMatch.Id);
                //if (currentLiveMatch == null)
                //{
                //    return ErrorMapping.ErrorCodes.UpdateLiveMatchNotFoundLiveMatch;
                //}
                //if (string.IsNullOrEmpty(liveMatch.Title))
                //{
                //    return ErrorMapping.ErrorCodes.UpdateLiveMatchInvalidTitle;
                //}
                //if (string.IsNullOrEmpty(liveMatch.TeamA))
                //{
                //    return ErrorMapping.ErrorCodes.UpdateLiveMatchInvalidTeamA;
                //}
                //if (string.IsNullOrEmpty(liveMatch.TeamB))
                //{
                //    return ErrorMapping.ErrorCodes.UpdateLiveMatchInvalidTeamB;
                //}
                //if (liveMatch.TimeOfMatchBegin <= DbCommon.MinDateTime)
                //{
                //    return ErrorMapping.ErrorCodes.UpdateLiveMatchInvalidTimeOfMatchBegin;
                //}
                //currentLiveMatch.Title = liveMatch.Title;
                //currentLiveMatch.TeamA = liveMatch.TeamA;
                //currentLiveMatch.TeamB = liveMatch.TeamB;
                //currentLiveMatch.LogoTeamA = liveMatch.LogoTeamA;
                //currentLiveMatch.LogoTeamB = liveMatch.LogoTeamB;
                //currentLiveMatch.ScoreOfTeamA = liveMatch.ScoreOfTeamA;
                //currentLiveMatch.ScoreOfTeamB = liveMatch.ScoreOfTeamB;
                //currentLiveMatch.Stadium = liveMatch.Stadium;
                //currentLiveMatch.Referee = liveMatch.Referee;
                //currentLiveMatch.BeginFirstHalf = liveMatch.BeginFirstHalf;
                //currentLiveMatch.FinishFirstHalf = liveMatch.FinishFirstHalf;
                //currentLiveMatch.BeginSecondHalf = liveMatch.BeginSecondHalf;
                //currentLiveMatch.FinishSecondHalf = liveMatch.FinishSecondHalf;
                //currentLiveMatch.BeginFirstExtraTime = liveMatch.BeginFirstExtraTime;
                //currentLiveMatch.FinishFirstExtraTime = liveMatch.FinishFirstExtraTime;
                //currentLiveMatch.BeginSecondExtraTime = liveMatch.BeginSecondExtraTime;
                //currentLiveMatch.FinishSecondExtraTime = liveMatch.FinishSecondExtraTime;
                //currentLiveMatch.IsPenalty = liveMatch.IsPenalty;
                //currentLiveMatch.MatchFinished = liveMatch.MatchFinished;
                //currentLiveMatch.TimeOfMatchBegin = liveMatch.TimeOfMatchBegin;
                //currentLiveMatch.TimeOfFinishFirstHalf = liveMatch.TimeOfFinishFirstHalf;
                //currentLiveMatch.TimeOfBeginSecondHalf = liveMatch.TimeOfBeginSecondHalf;
                //currentLiveMatch.TimeOfFinishSecondHalf = liveMatch.TimeOfFinishSecondHalf;
                //currentLiveMatch.TimeOfBeginFirstExtraTime = liveMatch.TimeOfBeginFirstExtraTime;
                //currentLiveMatch.TimeOfFinishFirstExtraTime = liveMatch.TimeOfFinishFirstExtraTime;
                //currentLiveMatch.TimeOfBeginSecondExtraTime = liveMatch.TimeOfBeginSecondExtraTime;
                //currentLiveMatch.TimeOfFinishSecondExtraTime = liveMatch.TimeOfFinishSecondExtraTime;
                //currentLiveMatch.TimeOfMatchFinished = liveMatch.TimeOfMatchFinished;
                //currentLiveMatch.Status = liveMatch.Status;
                //currentLiveMatch.LinkLiveTV1 = liveMatch.LinkLiveTV1;
                //currentLiveMatch.LinkLiveTV2 = liveMatch.LinkLiveTV2;
                //currentLiveMatch.LinkLiveTV3 = liveMatch.LinkLiveTV3;
                //currentLiveMatch.LinkLiveTV4 = liveMatch.LinkLiveTV4;
                //currentLiveMatch.LinkLiveTV5 = liveMatch.LinkLiveTV5;
                //currentLiveMatch.ShowOnLiveTV = liveMatch.ShowOnLiveTV;
                //currentLiveMatch.InfomationBeforeMatch = liveMatch.InfomationBeforeMatch;
                //currentLiveMatch.MatchStatus = liveMatch.MatchStatus;

                //goi nodejs
                var response = LiveMatchNodeJsServices.LiveMatch_Update(liveMatch);
                return response.Success ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.BusinessError;

                //return LiveMatchDal.Update(currentLiveMatch)
                //           ? ErrorMapping.ErrorCodes.Success
                //           : ErrorMapping.ErrorCodes.BusinessError;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message + ex.InnerException);
                return ErrorMapping.ErrorCodes.BusinessError;
            }
        }
        //public static ErrorMapping.ErrorCodes DeleteLiveMatch(int liveMatchId)
        //{
        //    if (liveMatchId <= 0)
        //    {
        //        return ErrorMapping.ErrorCodes.UpdateLiveMatchNotFoundLiveMatch;
        //    }
        //    var currentLiveMatch = LiveMatchDal.GetById(liveMatchId);
        //    if (currentLiveMatch == null)
        //    {
        //        return ErrorMapping.ErrorCodes.UpdateLiveMatchNotFoundLiveMatch;
        //    }
        //    return LiveMatchDal.Delete(liveMatchId)
        //               ? ErrorMapping.ErrorCodes.Success
        //               : ErrorMapping.ErrorCodes.BusinessError;
        //}


    }
}
