﻿using System.Runtime.Serialization;
using ChannelVN.CMS.Common;

namespace ChannelVN.LiveTeamGame.Entity.ErrorCode
{
    [DataContract]
    public class ErrorMapping : BoErrorMappingBase<ErrorMapping.ErrorCodes>
    {
        private static ErrorMapping _current;
        public static ErrorMapping Current
        {
            get { return _current ?? (_current = new ErrorMapping()); }
        }

        protected override void InitErrorMapping()
        {
            InnerHashtable[ErrorCodes.Invalid] = "Không tìm thấy dữ liệu";
            InnerHashtable[ErrorCodes.DelInvite] = "Bạn không thể xóa sự kiện này!";
        }
        [DataContract]
        public enum ErrorCodes
        {
            #region General error

            [EnumMember]
            Success = ErrorCodeBase.Success,
            [EnumMember]
            UnknowError = ErrorCodeBase.UnknowError,
            [EnumMember]
            Exception = ErrorCodeBase.Exception,
            [EnumMember]
            BusinessError = ErrorCodeBase.BusinessError,
            [EnumMember]
            InvalidRequest = ErrorCodeBase.InvalidRequest,
            [EnumMember]
            TimeOutSession = ErrorCodeBase.TimeOutSession,

            #endregion

            #region LiveTeamGame error

            Invalid = 10001,
            DelInvite = 10002,

            #endregion
        }
    }
}
