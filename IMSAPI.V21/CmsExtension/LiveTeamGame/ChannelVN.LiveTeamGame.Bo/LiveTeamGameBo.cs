﻿using ChannelVN.LiveTeamGame.Dal;
using ChannelVN.LiveTeamGame.Entity;
using ChannelVN.LiveTeamGame.Entity.ErrorCode;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ChannelVN.LiveTeamGame.Bo
{
    public class LiveTeamGameBo
    {
        public static ErrorMapping.ErrorCodes Insert(LiveTeamGameEntity info, ref int id)
        {
            var result = ErrorMapping.ErrorCodes.BusinessError;
            var createSuccess = LiveTeamGameDal.Insert(info, ref id);
            if (createSuccess && id > 0)
                result = ErrorMapping.ErrorCodes.Success;
            return result;
        }

        public static ErrorMapping.ErrorCodes Update(LiveTeamGameEntity info)
        {
            var result = ErrorMapping.ErrorCodes.BusinessError;
            var createSuccess = LiveTeamGameDal.Update(info);
            if (createSuccess && info.Id > 0)
                result = ErrorMapping.ErrorCodes.Success;
            return result;
        }

        public static ErrorMapping.ErrorCodes Delete(int id)
        {
            var result = ErrorMapping.ErrorCodes.BusinessError;
            var createSuccess = LiveTeamGameDal.Delete(id);
            if (createSuccess && id > 0)
                result = ErrorMapping.ErrorCodes.Success;
            return result;
        }

        public static List<LiveTeamGameEntity> ListLiveTeamGameByPages(string search, int pageIndex, int pageSize, ref int totalRow)
        {
            return LiveTeamGameDal.ListLiveTeamGameByPages(search, pageIndex, pageSize, ref totalRow);
        }

        public static LiveTeamGameEntity GetLiveTeamGameById(int Id)
        {
            return LiveTeamGameDal.GetLiveTeamGameById(Id);
        }
        public static List<LiveTeamGameEntity> ListLiveTeamGameByMultiId(int teamA, int teamB) {
            return LiveTeamGameDal.ListLiveTeamGameByMultiId(teamA, teamB);
        }
    }
}
