﻿using System;
using System.Collections.Generic;
using System.Data;
using ChannelVN.CMS.Common;
using ChannelVN.BrandContent.MainDal.Databases;
using ChannelVN.BrandContent.Entity;

namespace ChannelVN.BrandContent.MainDal
{
    public abstract class BrandContentDalBase
    {
        public List<BrandContentEntity> BrandContent_Search(string Keyword, int pageIndex, int pageSize, ref int totalRow)
        {
            const string commandText = "CMS_BrandContent_Search";
            try
            {
                List<BrandContentEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "TotalRow", totalRow, ParameterDirection.Output);
                _db.AddParameter(cmd, "Keyword", Keyword);
                _db.AddParameter(cmd, "PageIndex", pageIndex);
                _db.AddParameter(cmd, "PageSize", pageSize);
                data = _db.GetList<BrandContentEntity>(cmd);
                totalRow = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 0));
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public List<BrandContentEntity> BrandInNews_GetByNewsId(long NewsId)
        {
            const string commandText = "CMS_BrandContent_GetByNewId";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "NewsId", NewsId);
                return _db.GetList<BrandContentEntity>(cmd);
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public BrandContentEntity BrandContent_GetById(int BrandId)
        {
            const string commandText = "CMS_BrandContent_GetById";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "BrandId", BrandId);
                return _db.Get<BrandContentEntity>(cmd);
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool BrandContent_Delete(int BrandId)
        {
            const string commandText = "CMS_BrandContent_Delete";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "BrandId", BrandId);
                var numberOfRow = _db.ExecuteNonQuery(cmd);
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool BrandInNews_Insert(string BrandIds, long NewsId)
        {
            const string commandText = "CMS_BrandInNews_Insert";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "BrandIds", BrandIds);
                _db.AddParameter(cmd, "NewsId", NewsId);
                var numberOfRow = _db.ExecuteNonQuery(cmd);
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public bool BrandContent_Insert(BrandContentEntity obj)
        {
            const string commandText = "CMS_BrandContent_Insert";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "BrandName", obj.BrandName);
                _db.AddParameter(cmd, "BrandUrl", obj.BrandUrl);
                _db.AddParameter(cmd, "Icon", obj.Icon);
                _db.AddParameter(cmd, "Logo", obj.Logo);
                _db.AddParameter(cmd, "LogoStream", obj.LogoStream);
                var numberOfRow = _db.ExecuteNonQuery(cmd);
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool BrandContent_Update(BrandContentEntity obj)
        {
            const string commandText = "CMS_BrandContent_Update";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "BrandId", obj.BrandId);
                _db.AddParameter(cmd, "BrandName", obj.BrandName);
                _db.AddParameter(cmd, "BrandUrl", obj.BrandUrl);
                _db.AddParameter(cmd, "Icon", obj.Icon);
                _db.AddParameter(cmd, "Logo", obj.Logo);
                _db.AddParameter(cmd, "LogoStream", obj.LogoStream);
                var numberOfRow = _db.ExecuteNonQuery(cmd);
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        #region Core members

        private readonly CmsMainDb _db;

        protected BrandContentDalBase(CmsMainDb db)
        {
            _db = db;
        }

        protected CmsMainDb Database
        {
            get { return _db; }
        }

        #endregion
    }
}
