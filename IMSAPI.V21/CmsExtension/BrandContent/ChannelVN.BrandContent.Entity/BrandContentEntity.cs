﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ChannelVN.BrandContent.Entity
{
    public class BrandContentEntity
    {
        public int BrandId
        { get; set; }
        public string BrandUrl
        { get; set; }
        public string Icon
        { get; set; }
        public string Logo
        { get; set; }
        public string LogoStream
        { get; set; }
        public string BrandName { get; set; }
        public long NewsId { get; set; }
    }
}
