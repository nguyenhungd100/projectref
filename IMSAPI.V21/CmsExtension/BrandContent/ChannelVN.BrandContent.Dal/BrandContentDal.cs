﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ChannelVN.BrandContent.Entity;
using ChannelVN.BrandContent.MainDal.Databases;
using ChannelVN.CMS.Common;

namespace ChannelVN.BrandContent.Dal
{
    public class BrandContentDal
    {
        public static bool BrandContent_Insert(BrandContentEntity BrandContentEntity)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.BrandContentMainDal.BrandContent_Insert(BrandContentEntity);
            }
            return returnValue;

        }
        public static bool BrandContent_Update(BrandContentEntity BrandContentEntity)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.BrandContentMainDal.BrandContent_Update(BrandContentEntity);
            }
            return returnValue;

        }

        public static List<BrandContentEntity> BrandContent_Search(string Keyword, int pageIndex, int pageSize, ref int TotalRow)
        {
            List<BrandContentEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.BrandContentMainDal.BrandContent_Search(Keyword, pageIndex, pageSize, ref TotalRow);
            }
            return returnValue;

        }

        public static List<BrandContentEntity> BrandInNews_GetByNewsId(long NewsId)
        {
            List<BrandContentEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.BrandContentMainDal.BrandInNews_GetByNewsId(NewsId);
            }
            return returnValue;

        }

        public static BrandContentEntity BrandContent_GetById(int BrandId)
        {
            BrandContentEntity returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.BrandContentMainDal.BrandContent_GetById(BrandId);
            }
            return returnValue;
        }

        public static bool BrandInNews_Insert(string BrandIds, long NewsId)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.BrandContentMainDal.BrandInNews_Insert(BrandIds, NewsId);
            }
            return returnValue;
        }

        public static bool BrandContent_Delete(int id)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.BrandContentMainDal.BrandContent_Delete(id);
            }
            return returnValue;
        }

    }
}
