﻿using System;
using System.Collections.Generic;
using System.Linq;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.Entity.ErrorCode;
using ChannelVN.BrandContent.Entity;
using ChannelVN.BrandContent.Dal;

namespace ChannelVN.BrandContent.Bo
{
    public class BrandContentBo
    {
        public static ErrorMapping.ErrorCodes BrandContent_Insert(BrandContentEntity BrandContent)
        {
            try
            {
                var inserted = BrandContentDal.BrandContent_Insert(BrandContent);
                return inserted ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.UnknowError;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return ErrorMapping.ErrorCodes.BusinessError;
            }
        }

        public static ErrorMapping.ErrorCodes BrandContent_Update(BrandContentEntity BrandContent)
        {
            try
            {
                var inserted = BrandContentDal.BrandContent_Update(BrandContent);
                return inserted ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.UnknowError;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return ErrorMapping.ErrorCodes.BusinessError;
            }
        }

        public static List<BrandContentEntity> BrandContent_Search(string Keyword, int pageIndex, int pageSize, ref int TotalRow)
        {
            try
            {
                return BrandContentDal.BrandContent_Search(Keyword,  pageIndex, pageSize, ref TotalRow);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return new List<BrandContentEntity>();
            }
        }
        public static List<BrandContentEntity> BrandInNews_GetByNewsId(long NewsId)
        {
            try
            {
                //tam bo cached
                //var list = CMS.BoCached.CmsExtension.BrandContent.BrandContentDalFactory.GetBrandContentByNewsId(NewsId);
                //if (list == null)
                //{
                //    list= BrandContentDal.BrandInNews_GetByNewsId(NewsId);
                //    if(list!=null && list.Count() > 0)
                //    {
                //        CMS.BoCached.CmsExtension.BrandContent.BrandContentDalFactory.AddBrandContentByNewsId(NewsId,list);
                //    }
                //}
                //return list;

                return BrandContentDal.BrandInNews_GetByNewsId(NewsId);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return new List<BrandContentEntity>();
            }
        }
        public static BrandContentEntity BrandContent_GetById(int BrandId)
        {
            try
            {
                return BrandContentDal.BrandContent_GetById(BrandId);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return new BrandContentEntity();
            }
        }
        public static ErrorMapping.ErrorCodes BrandInNews_Insert(string brandIds, long NewsId)
        {
            try
            {
                var inserted = BrandContentDal.BrandInNews_Insert(brandIds, NewsId);
                return inserted ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.UnknowError;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return ErrorMapping.ErrorCodes.BusinessError;
            }
        }
        public static ErrorMapping.ErrorCodes BrandContent_Delete(int id)
        {
            try
            {
                var inserted = BrandContentDal.BrandContent_Delete(id);
                return inserted ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.UnknowError;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return ErrorMapping.ErrorCodes.BusinessError;
            }
        }
    }
}
