﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using ChannelVN.CMS.Common;

namespace ChannelVN.ErrorReport.Entity
{
    [DataContract]
    public enum EnumErrorReportType
    {
        [EnumMember]
        All = -1,
        /// <summary>
        /// Lỗi IMS
        /// </summary>
        [EnumMember]
        IMS = 1,
        /// <summary>
        /// Lỗi ngoài trang
        /// </summary>
        [EnumMember]
        FrontEnd = 2
    }
    [DataContract]
    public enum EnumErrorReportCommonStatus
    {
        [EnumMember]
        All = -1,
        /// <summary>
        /// Đang xử lý
        /// </summary>
        [EnumMember]
        Processing = 1,
        /// <summary>
        /// Đã xử lý
        /// </summary>
        [EnumMember]
        Processed = 2,
        /// <summary>
        /// Không xử lý
        /// </summary>
        [EnumMember]
        NoProcess = 3
    }
    [DataContract]
    public enum EnumErrorReportProcessStatus
    {
        [EnumMember]
        All = -1,
        /// <summary>
        /// Đang xử lý
        /// </summary>
        [EnumMember]
        Processing = 1,
        /// <summary>
        /// Đã sửa ở local
        /// </summary>
        [EnumMember]
        LocalFixed = 2,
        /// <summary>
        /// Đã sửa & update lên beta
        /// </summary>
        [EnumMember]
        BetaFixed = 3,
        /// <summary>
        /// Đã sửa và update lên chính
        /// </summary>
        [EnumMember]
        LiveFixed = 4
    }
    [DataContract]
    public enum EnumErrorReportTestStatus
    {
        [EnumMember]
        All = -1,
        /// <summary>
        /// Test thành công trước khi code fix
        /// </summary>
        [EnumMember]
        Pass = 1,
        /// <summary>
        /// Test thất bại trước khi code fix ==> báo code
        /// </summary>
        [EnumMember]
        Fail = 2,
        /// <summary>
        /// Test trên beta thành công
        /// </summary>
        [EnumMember]
        TestBetaPass = 3,
        /// <summary>
        /// Test trên beta thất bại
        /// </summary>
        [EnumMember]
        TestBetaFail = 4,
        /// <summary>
        /// Test trên chính thành công
        /// </summary>
        [EnumMember]
        TestLivePass = 5,
        /// <summary>
        /// Test trên chính thất bại
        /// </summary>
        [EnumMember]
        TestLiveFail = 6,
        /// <summary>
        /// Bỏ qua không test (không phải lỗi/ không cần fix)
        /// </summary>
        [EnumMember]
        Skip = 7
    }

    [DataContract]
    public enum EnumErrorReportPriority
    {
        [EnumMember]
        All = -1,
        /// <summary>
        /// Lỗi nghiêm trọng
        /// </summary>
        [EnumMember]
        Critical = 1,
        /// <summary>
        /// Lỗi bình thường
        /// </summary>
        [EnumMember]
        Normal = 2,
        /// <summary>
        /// Lỗi không quan trọng
        /// </summary>
        [EnumMember]
        Trivial = 3
    }

    [DataContract]
    public class ErrorReportEntity : EntityBase
    {
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public string NameSpace { get; set; }
        [DataMember]
        public int Type { get; set; }
        [DataMember]
        public string Title { get; set; }
        [DataMember]
        public string Body { get; set; }
        [DataMember]
        public string CreatedBy { get; set; }
        [DataMember]
        public DateTime CreatedDate { get; set; }
        [DataMember]
        public string ProcessBy { get; set; }
        [DataMember]
        public int ProcessStatus { get; set; }
        /// <summary>
        /// Ngày code cập nhật trạng thái cuối cùng
        /// </summary>
        [DataMember]
        public DateTime LastProcessDate { get; set; }
        [DataMember]
        public string ProcessNote { get; set; }
        /// <summary>
        /// Trạng thái test
        /// </summary>
        [DataMember]
        public int TestStatus { get; set; }
        [DataMember]
        public string TestNote { get; set; }
        /// <summary>
        /// Ngày tester cập nhật trạng thái cuối cùng
        /// </summary>
        [DataMember]
        public DateTime LastTestDate { get; set; }
        [DataMember]
        public bool IsEmailSent { get; set; }
        /// <summary>
        /// Đã gửi sms báo hay chưa
        /// </summary>
        [DataMember]
        public bool IsSmsSent { get; set; }
        /// <summary>
        /// Trạng thái chung của tác vụ
        /// </summary>
        [DataMember]
        public int CommonStatus { get; set; }

        [DataMember]
        public int Priority { get; set; }
    }
}
