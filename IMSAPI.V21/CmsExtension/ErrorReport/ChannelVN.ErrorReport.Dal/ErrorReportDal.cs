﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using ChannelVN.CMS.Common;
using ChannelVN.ErrorReport.Dal.Common;
using ChannelVN.ErrorReport.Entity;
using ChannelVN.ErrorReport.MainDal.Databases;

namespace ChannelVN.ErrorReport.DAL
{
    public class ErrorReportDal
    {
        /// <summary>
        /// Thêm báo lỗi mới
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        public static int Insert(ErrorReportEntity entity)
        {
            int returnValue;
            using (var db = new CmsErrorReportDb())
            {
                returnValue = db.ErrorReportMainDal.Insert(entity);
            }
            return returnValue;
        }

        /// <summary>
        /// Cập nhật trạng thái test
        /// </summary>
        /// <param name="id"></param>
        /// <param name="testStatus"></param>
        /// <param name="note"></param>
        /// <returns></returns>
        public static bool UpdateTestStatus(int id, int testStatus, string note)
        {
            bool returnValue;
            using (var db = new CmsErrorReportDb())
            {
                returnValue = db.ErrorReportMainDal.UpdateTestStatus(id, testStatus, note);
            }
            return returnValue;
        }

        /// <summary>
        /// Cập nhật trạng thái code
        /// </summary>
        /// <param name="id"></param>
        /// <param name="processStatus"></param>
        /// <param name="note"></param>
        /// <returns></returns>
        public static bool UpdateProcessStatus(int id, int processStatus, string note, string processBy)
        {
            bool returnValue;
            using (var db = new CmsErrorReportDb())
            {
                returnValue = db.ErrorReportMainDal.UpdateProcessStatus(id, processStatus, note, processBy);
            }
            return returnValue;
        }
        public static bool UpdatePriority(int id, int priority)
        {
            bool returnValue;
            using (var db = new CmsErrorReportDb())
            {
                returnValue = db.ErrorReportMainDal.UpdatePriority(id, priority);
            }
            return returnValue;
        }

        /// <summary>
        /// Tìm kiếm
        /// </summary>
        /// <param name="keyword"></param>
        /// <param name="nameSpace"></param>
        /// <param name="type"></param>
        /// <param name="dateFrom"></param>
        /// <param name="dateTo"></param>
        /// <param name="testStatus"></param>
        /// <param name="processStatus"></param>
        /// <param name="commonStatus"></param>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <param name="totalRows"></param>
        /// <returns></returns>
        public static List<ErrorReportEntity> Search(string keyword, string nameSpace, int type, DateTime dateFrom, DateTime dateTo, int testStatus, int processStatus, int commonStatus, int pageIndex, int pageSize, ref int totalRows)
        {
            List<ErrorReportEntity> returnValue;
            using (var db = new CmsErrorReportDb())
            {
                returnValue = db.ErrorReportMainDal.Search(keyword, nameSpace, type, dateFrom, dateTo, testStatus, processStatus, commonStatus, pageIndex, pageSize, ref totalRows);
            }
            return returnValue;
        }

        public static ErrorReportEntity GetById(int id)
        {
            ErrorReportEntity returnValue;
            using (var db = new CmsErrorReportDb())
            {
                returnValue = db.ErrorReportMainDal.GetById(id);
            }
            return returnValue;
        }
    }
}
