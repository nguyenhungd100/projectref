﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using ChannelVN.CMS.Common;
using ChannelVN.ErrorReport.Entity;
using ChannelVN.ErrorReport.MainDal.Databases;

namespace ChannelVN.ErrorReport.MainDal
{
    public abstract class ErrorReportDalBase
    {
        public int Insert(ErrorReportEntity eEntity)
        {
            const string commandText = "CMS_ErrorReport_Insert";
            try
            {
                var id = 0;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", id, ParameterDirection.Output);
                _db.AddParameter(cmd, "Title", eEntity.Title);
                _db.AddParameter(cmd, "Body", eEntity.Body);
                _db.AddParameter(cmd, "CreatedBy", eEntity.CreatedBy);
                _db.AddParameter(cmd, "Type", eEntity.Type);
                _db.AddParameter(cmd, "NameSpace", eEntity.NameSpace);
                var numberOfRow = _db.ExecuteNonQuery(cmd);
                id = Utility.ConvertToInt(cmd.Parameters[0]);
                return id;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public bool UpdateTestStatus(int id, int testStatus, string note)
        {
            const string commandText = "CMS_ErrorReport_UpdateTestStatus";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", id);
                _db.AddParameter(cmd, "TestStatus", testStatus);
                _db.AddParameter(cmd, "TestNote", note);
                var numberOfRow = _db.ExecuteNonQuery(cmd);
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public bool UpdateProcessStatus(int id, int processStatus, string note, string processBy)
        {
            const string commandText = "CMS_ErrorReport_UpdateProcessStatus";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", id);
                _db.AddParameter(cmd, "ProcessStatus", processStatus);
                _db.AddParameter(cmd, "ProcessNote", note);
                _db.AddParameter(cmd, "ProcessBy", processBy);
                var numberOfRow = _db.ExecuteNonQuery(cmd);
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public bool UpdatePriority(int id, int priority)
        {
            const string commandText = "CMS_ErrorReport_UpdatePriority";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", id);
                _db.AddParameter(cmd, "Priority", priority);
                var numberOfRow = _db.ExecuteNonQuery(cmd);
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public List<ErrorReportEntity> Search(string keyword, string nameSpace, int type, DateTime dateFrom, DateTime dateTo, int testStatus, int processStatus, int commonStatus, int pageIndex, int pageSize, ref int totalRows)
        {
            const string commandText = "CMS_ErrorReport_Search";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Keyword", keyword);
                _db.AddParameter(cmd, "Namespace", nameSpace);
                _db.AddParameter(cmd, "Type", type);
                if (dateFrom != DateTime.MinValue)
                    _db.AddParameter(cmd, "DateFrom", dateFrom);
                else _db.AddParameter(cmd, "DateFrom", DBNull.Value);
                if (dateTo != DateTime.MinValue) _db.AddParameter(cmd, "DateTo", dateTo); else _db.AddParameter(cmd, "DateTo", DBNull.Value);
                _db.AddParameter(cmd, "TestStatus", testStatus);
                _db.AddParameter(cmd, "ProcessStatus", processStatus);
                _db.AddParameter(cmd, "CommonStatus", commonStatus);
                _db.AddParameter(cmd, "PageIndex", pageIndex);
                _db.AddParameter(cmd, "PageSize", pageSize);
                _db.AddParameter(cmd, "TotalRow", 0, ParameterDirection.Output);
                var numberOfRow = _db.GetList<ErrorReportEntity>(cmd);
                totalRows = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, cmd.Parameters.Count - 1));
                return numberOfRow;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public ErrorReportEntity GetById(int id)
        {
            const string commandText = "CMS_ErrorReport_GetById";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", id);
                var numberOfRow = _db.Get<ErrorReportEntity>(cmd);
                return numberOfRow;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        #region Core members

        private readonly CmsErrorReportDb _db;

        protected ErrorReportDalBase(CmsErrorReportDb db)
        {
            _db = db;
        }

        protected CmsErrorReportDb Database
        {
            get { return _db; }
        }

        #endregion
    }
}
