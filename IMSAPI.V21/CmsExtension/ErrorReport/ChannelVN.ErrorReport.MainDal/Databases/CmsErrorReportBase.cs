﻿using System.Data;
using ChannelVN.CMS.Common.DalCommon;
using ChannelVN.ErrorReport.MainDal;

namespace ChannelVN.ErrorReport.MainDal.Databases
{
    /// <summary>
    /// The base class for the <see cref="CmsMainDb"/> class that 
    /// represents a connection to the <c>CmsMainDb</c> database. 
    /// </summary>
    /// <remarks>
    /// Do not change this source code. Modify the CmsMainDb class
    /// if you need to add or change some functionality.
    /// </remarks>
    public abstract class CmsErrorReportDbBase : MainDbBase
    {
       
        #region ErrorReport

        private ErrorReportDal _errorReportMainDal;
        public ErrorReportDal ErrorReportMainDal
        {
            get { return _errorReportMainDal ?? (_errorReportMainDal = new ErrorReportDal((CmsErrorReportDb)this)); }
        }

        #endregion
        #region Constructors

        protected CmsErrorReportDbBase()
        {
        }
        protected CmsErrorReportDbBase(bool init)
        {
            if (init)
            {
                InitConnection();
            }
        }

        #endregion

        #region Protected members

        protected override sealed void InitConnection()
        {
            _connection = CreateConnection();
            _connection.Open();
        }

        /// <summary>
        /// Creates a new connection to the database.
        /// </summary>
        /// <returns>A reference to the <see cref="System.Data.IDbConnection"/> object.</returns>
        protected abstract IDbConnection CreateConnection();

        #endregion
    }
}