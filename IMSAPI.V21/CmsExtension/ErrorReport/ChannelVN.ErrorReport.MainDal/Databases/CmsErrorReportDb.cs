﻿using System.Data;
using System.Data.SqlClient;
using ChannelVN.CMS.Common.ChannelConfig;
using ChannelVN.ErrorReport.MainDal.Common;
using ChannelVN.WcfExtensions;

namespace ChannelVN.ErrorReport.MainDal.Databases
{
    public class CmsErrorReportDb : CmsErrorReportDbBase
    {
        private const string ConnectionStringName = "CmsErrorReportDb";

        /// <summary>
        /// Creates a new connection to the database.
        /// </summary>
        /// <returns>An <see cref="System.Data.IDbConnection"/> object.</returns>
        protected override IDbConnection CreateConnection()
        {
            var strConn = ServiceChannelConfiguration.GetConnectionString(WcfMessageHeader.Current.Namespace,
                ConnectionStringName, Constants.ConnectionDecryptKey);
            return new SqlConnection(strConn);
        }
    }
}