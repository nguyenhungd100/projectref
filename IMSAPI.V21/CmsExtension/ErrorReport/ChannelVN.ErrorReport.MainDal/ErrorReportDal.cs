﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using ChannelVN.CMS.Common;

using ChannelVN.ErrorReport.Entity;
using ChannelVN.ErrorReport.MainDal.Databases;

namespace ChannelVN.ErrorReport.MainDal
{
    public class ErrorReportDal : ErrorReportDalBase
    {
        internal ErrorReportDal(CmsErrorReportDb db)
            : base(db)
        {
        }
    }
}
