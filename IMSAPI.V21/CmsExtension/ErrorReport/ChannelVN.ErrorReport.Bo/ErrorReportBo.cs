﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using ChannelVN.CMS.Common;
using ChannelVN.ErrorReport.Entity;
using ChannelVN.WcfExtensions;
using ChannelVN.ErrorReport.DAL;
using ChannelVN.ErrorReport.Entity.ErrorCode;

namespace ChannelVN.ErrorReport.Bo
{
    public class ErrorReportBo
    {
        public static ErrorMapping.ErrorCodes Insert(ErrorReportEntity entity, ref int id)
        {
            try
            {
                id = ErrorReportDal.Insert(entity);
                return id > 0 ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.BusinessError;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return 0;
            }
        }

        public static ErrorMapping.ErrorCodes UpdateTestStatus(int id, int testStatus, string note)
        {
            try
            {
                return ErrorReportDal.UpdateTestStatus(id, testStatus, note) ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.BusinessError;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return ErrorMapping.ErrorCodes.UnknowError;
            }
        }

        public static ErrorMapping.ErrorCodes UpdateProcessStatus(int id, int processStatus, string note,string processBy)
        {
            try
            {
                return ErrorReportDal.UpdateProcessStatus(id, processStatus, note, processBy) ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.BusinessError;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return ErrorMapping.ErrorCodes.UnknowError;
            }
        }
        public static ErrorMapping.ErrorCodes UpdatePriority(int id, EnumErrorReportPriority priority)
        {
            try
            {
                return ErrorReportDal.UpdatePriority(id, (int)priority) ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.BusinessError;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return ErrorMapping.ErrorCodes.UnknowError;
            }
        }

        #region Gets

        public static List<ErrorReportEntity> Search(string keyword, string nameSpace, EnumErrorReportType type, DateTime dateFrom, DateTime dateTo, EnumErrorReportTestStatus testStatus, EnumErrorReportProcessStatus processStatus, EnumErrorReportCommonStatus commonStatus, int pageIndex, int pageSize, ref int totalRows)
        {
            try
            {
                return ErrorReportDal.Search(keyword, nameSpace, (int)type, dateFrom, dateTo, (int)testStatus, (int)processStatus,
                    (int)commonStatus, pageIndex, pageSize, ref totalRows);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return null;
            }
        }

        public static ErrorReportEntity GetById(int id)
        {
            try
            {
                return ErrorReportDal.GetById(id);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return null;
            }
        }

        #endregion
    }
}
