﻿using ChannelVN.Feed.Entity;
using ChannelVN.Feed.MainDal.Databases;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ChannelVN.Feed.DAL
{
   public class FeedDal
    {
        public static bool InsertFeedItem(FeedItemEntity feedItem, ref int Id)
        {
            bool returnValue;
            using (var db = new CmsFeedDb())
            {
                returnValue = db.FeedMainDal.InsertItem(feedItem, ref Id);
            }
            return returnValue;
        }
        public static bool Update_Status(int Id, FeedItemStatus status, int DownloadMediaId = -1)
        {
            bool returnValue;
            using (var db = new CmsFeedDb())
            {
                returnValue = db.FeedMainDal.Update_Status(Id, status, DownloadMediaId);
            }
            return returnValue;
        }
        public static bool Update_Status_ByMediaIds(string MediaIds)
        {
            bool returnValue;
            using (var db = new CmsFeedDb())
            {
                returnValue = db.FeedMainDal.Update_Status_ByMediaIds(MediaIds);
            }
            return returnValue;
        }
        public static List<FeedEntity> GetListFeed()
        {
            List<FeedEntity> returnValue;
            using (var db = new CmsFeedDb())
            {
                returnValue = db.FeedMainDal.GetListFeed();
            }
            return returnValue;
        }
        public static List<FeedItemEntity> GetListFeedItem()
        {
            List<FeedItemEntity> returnValue;
            using (var db = new CmsFeedDb())
            {
                returnValue = db.FeedMainDal.GetListFeedItem();
            }
            return returnValue;
        }
        public static List<FeedItemEntity> GetListFeedItem_Download()
        {
            List<FeedItemEntity> returnValue;
            using (var db = new CmsFeedDb())
            {
                returnValue = db.FeedMainDal.GetListFeedItem_Download();
            }
            return returnValue;
        }
        public static List<FeedItemEntity> GetListFeedItem_ReDownload()
        {
            List<FeedItemEntity> returnValue;
            using (var db = new CmsFeedDb())
            {
                returnValue = db.FeedMainDal.GetListFeedItem_ReDownload();
            }
            return returnValue;
        }

        #region FeedItemV2

        public static bool InsertFeedItemV2(FeedItemV2Entity feedItem, ref int Id)
        {
            bool returnValue;
            using (var db = new CmsFeedDb())
            {
                returnValue = db.FeedMainDal.InsertFeedItemV2(feedItem, ref Id);
            }
            return returnValue;
        }
        public static List<FeedItemV2Entity> FeedItemV2_GetListCheckDownload()
        {
            var returnValue = new List<FeedItemV2Entity>();
            using (var db = new CmsFeedDb())
            {
                returnValue = db.FeedMainDal.FeedItemV2_GetListCheckDownload();
            }
            return returnValue;
        }
        public static List<FeedItemV2Entity> FeedItemV2_GetListDownloadFinished()
        {
            var returnValue = new List<FeedItemV2Entity>();
            using (var db = new CmsFeedDb())
            {
                returnValue = db.FeedMainDal.FeedItemV2_GetListDownloadFinished();
            }
            return returnValue;
        }
        public static FeedItemV2Entity FeedItemV2_GetById(int Id)
        {
            var returnValue = new FeedItemV2Entity();
            using (var db = new CmsFeedDb())
            {
                returnValue = db.FeedMainDal.FeedItemV2_GetById(Id);
            }
            return returnValue;
        }
        public static bool FeedItemV2_Downloading(string IdAndMediaIds)
        {
            bool returnValue;
            using (var db = new CmsFeedDb())
            {
                returnValue = db.FeedMainDal.FeedItemV2_Downloading(IdAndMediaIds);
            }
            return returnValue;
        }
        public static bool FeedItemV2_DownloadFinished(string MediaIds)
        {
            bool returnValue;
            using (var db = new CmsFeedDb())
            {
                returnValue = db.FeedMainDal.FeedItemV2_DownloadFinished(MediaIds);
            }
            return returnValue;
        }
        public static bool FeedItemV2_UpdateByComplete(FeedItemV2Entity obj)
        {
            bool returnValue;
            using (var db = new CmsFeedDb())
            {
                returnValue = db.FeedMainDal.FeedItemV2_UpdateByComplete(obj);
            }
            return returnValue;
        }
        public static bool FeedItemV2_UpdateStatus(int Id, FeedItemStatus status)
        {
            bool returnValue;
            using (var db = new CmsFeedDb())
            {
                returnValue = db.FeedMainDal.FeedItemV2_UpdateStatus(Id, status);
            }
            return returnValue;
        }
        #endregion
    }
}
