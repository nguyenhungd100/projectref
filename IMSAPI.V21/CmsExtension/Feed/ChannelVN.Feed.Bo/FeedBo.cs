﻿using ChannelVN.CMS.Common;
using ChannelVN.Feed.DAL;
using ChannelVN.Feed.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ChannelVN.Feed.BO
{
    public class FeedBo
    {
        public static bool Insert_FeedItem(FeedItemEntity feedItem, ref int Id)
        {
            try
            {
                return FeedDal.InsertFeedItem(feedItem, ref Id);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return false;
            }
        }
        public static bool Update_Status(int Id, FeedItemStatus status, int DownloadMediaId = -1)
        {
            try
            {
                return FeedDal.Update_Status(Id, status, DownloadMediaId);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return false;
            }
        }
        public static bool Update_Status_ByMediaIds(string MediaIds)
        {
            try
            {
                return FeedDal.Update_Status_ByMediaIds(MediaIds);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return false;
            }
        }
        public static List<FeedEntity> Feed_GetList()
        {
            try
            {
                return FeedDal.GetListFeed();
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return new List<FeedEntity>();
            }
        }
        public static List<FeedItemEntity> FeedItem_GetList()
        {
            try
            {
                return FeedDal.GetListFeedItem();
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return new List<FeedItemEntity>();
            }
        }
        public static List<FeedItemEntity> FeedItem_GetList_Download()
        {
            try
            {
                return FeedDal.GetListFeedItem_Download();
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return new List<FeedItemEntity>();
            }
        }
        public static List<FeedItemEntity> FeedItem_GetList_ReDownload()
        {
            try
            {
                return FeedDal.GetListFeedItem_ReDownload();
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return new List<FeedItemEntity>();
            }
        }

        #region FeedItemV2
        public static bool Insert_FeedItemV2(FeedItemV2Entity feedItem, ref int Id)
        {
            try
            {
                return FeedDal.InsertFeedItemV2(feedItem, ref Id);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return false;
            }
        }
        public static bool FeedItemV2_UpdateByComplete(FeedItemV2Entity feedItem)
        {
            try
            {
                return FeedDal.FeedItemV2_UpdateByComplete(feedItem);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return false;
            }
        }
        public static bool FeedItemV2_DownloadFinished(string MediaIds)
        {
            try
            {
                return FeedDal.FeedItemV2_DownloadFinished(MediaIds);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return false;
            }
        }
        public static bool FeedItemV2_Downloading(string IdAndMediaIds)
        {
            try
            {
                return FeedDal.FeedItemV2_Downloading(IdAndMediaIds);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return false;
            }
        }
        public static bool FeedItemV2_UpdateStatus(int Id, FeedItemStatus Status)
        {
            try
            {
                return FeedDal.FeedItemV2_UpdateStatus(Id, Status);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return false;
            }
        }
        public static List<FeedItemV2Entity> FeedItemV2_GetListCheckDownload()
        {
            try
            {
                return FeedDal.FeedItemV2_GetListCheckDownload();
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return new List<FeedItemV2Entity>();
            }
        }
        public static List<FeedItemV2Entity> FeedItemV2_GetListDownloadFinished()
        {
            try
            {
                return FeedDal.FeedItemV2_GetListDownloadFinished();
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return new List<FeedItemV2Entity>();
            }
        }
        public static FeedItemV2Entity FeedItemV2_GetById(int Id)
        {
            try
            {
                return FeedDal.FeedItemV2_GetById(Id);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return new FeedItemV2Entity();
            }
        }

        #endregion
    }
}
