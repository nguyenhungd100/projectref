﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace ChannelVN.Feed.Entity
{
    [DataContract]
    public enum FeedStatus
    {
        [EnumMember]
        Active = 1,
        [EnumMember]
        Deactive = 0
    }
    [DataContract]
    public enum FeedItemStatus
    {
        [EnumMember]
        Insert = 0,
        [EnumMember]
        Download = 1,
        [EnumMember]
        Download_Done = 2,
        [EnumMember]
        Approve = 3,
        [EnumMember]
        Start_Error = 4,
        [EnumMember]
        CallBack_Error = 5
    }

    [DataContract]
    public class FeedEntity
    {
        [DataMember]
        public int Id { set; get; }
        [DataMember]
        public string Title { set; get; }
        [DataMember]
        public string Url { set; get; }
        [DataMember]
        public int Status { set; get; }
    }
    [DataContract]
    public class FeedItemEntity
    {
        [DataMember]
        public int Id { set; get; }
        [DataMember]
        public int FeedId { set; get; }
        [DataMember]
        public string Title { set; get; }
        [DataMember]
        public string Link { set; get; }
        [DataMember]
        public string Description { set; get; }
        [DataMember]
        public string MediaUrl { set; get; }
        [DataMember]
        public string MediaTitle { set; get; }
        [DataMember]
        public string MediaKeywords { set; get; }
        [DataMember]
        public string MediaThumbnail { set; get; }
        [DataMember]
        public int Status { set; get; }
        [DataMember]
        public int DownloadMediaId { get; set; }
        [DataMember]
        public DateTime CreatedDate { get; set; }
    }
    [DataContract]
    public class FeedItemV2Entity
    {
        [DataMember]
        public int Id { set; get; }
        [DataMember]
        public int FeedId { set; get; }
        [DataMember]
        public string Title { set; get; }
        [DataMember]
        public string Link { set; get; }
        [DataMember]
        public string Description { set; get; }
        [DataMember]
        public string MediaUrl { set; get; }
        [DataMember]
        public string MediaTitle { set; get; }
        [DataMember]
        public string MediaKeywords { set; get; }
        [DataMember]
        public string MediaThumbnail { set; get; }
        [DataMember]
        public int Status { set; get; }
        [DataMember]
        public int DownloadMediaId { get; set; }
        [DataMember]
        public DateTime CreatedDate { get; set; }
        [DataMember]
        public string KeyVideo { get; set; }
        [DataMember]
        public string FilePath { get; set; }
        [DataMember]
        public string Avatar { get; set; }
        [DataMember]
        public string Embed { get; set; }
        [DataMember]
        public string Duration { get; set; }
    }
    [DataContract]
    public class FeedItem_DownloadEntity
    {
        [DataMember]
        public int Id { set; get; }
        [DataMember]
        public int DownloadMediaId { get; set; }
    }
}
