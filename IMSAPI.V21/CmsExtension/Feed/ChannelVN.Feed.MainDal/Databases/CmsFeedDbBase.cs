﻿using System.Data;
using ChannelVN.CMS.Common.DalCommon;
using ChannelVN.Feed.MainDal;

namespace ChannelVN.Feed.MainDal.Databases
{
    public abstract class CmsFeedDbBase : MainDbBase
    {

        private FeedDal _FeedMainDal;
        public FeedDal FeedMainDal
        {
            get { return _FeedMainDal ?? (_FeedMainDal = new FeedDal((CmsFeedDb)this)); }
        }


        #region Constructors

        protected CmsFeedDbBase()
        {
        }
        protected CmsFeedDbBase(bool init)
        {
            if (init)
            {
                InitConnection();
            }
        }

        #endregion

        #region Protected members

        protected override sealed void InitConnection()
        {
            _connection = CreateConnection();
            _connection.Open();
        }

        /// <summary>
        /// Creates a new connection to the database.
        /// </summary>
        /// <returns>A reference to the <see cref="System.Data.IDbConnection"/> object.</returns>
        protected abstract IDbConnection CreateConnection();

        #endregion
    }
}