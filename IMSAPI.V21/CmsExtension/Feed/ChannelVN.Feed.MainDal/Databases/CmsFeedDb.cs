﻿using System.Data;
using System.Data.SqlClient;
using ChannelVN.CMS.Common.ChannelConfig;
using ChannelVN.Feed.MainDal.Common;
using ChannelVN.WcfExtensions;
using ChannelVN.CMS.Common;

namespace ChannelVN.Feed.MainDal.Databases
{
    public class CmsFeedDb : CmsFeedDbBase
    {
        private const string ConnectionStringName = "CmsFeedDb";

        /// <summary>
        /// Creates a new connection to the database.
        /// </summary>
        /// <returns>An <see cref="System.Data.IDbConnection"/> object.</returns>
        protected override IDbConnection CreateConnection()
        {
            var strConn = ServiceChannelConfiguration.GetConnectionString(WcfMessageHeader.Current.Namespace,
                ConnectionStringName, Constants.ConnectionDecryptKey);
            return new SqlConnection(strConn);
        }
    }
} 