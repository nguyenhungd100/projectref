﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using ChannelVN.CMS.Common;
using ChannelVN.Feed.MainDal.Common;
using ChannelVN.Feed.MainDal.Databases;
using ChannelVN.Feed.Entity;

namespace ChannelVN.Feed.MainDal
{
    public abstract class FeedDalBase
    {
        public List<FeedEntity> GetListFeed()
        {
            const string commandText = "CMS_GetAllFeed";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                return _db.GetList<FeedEntity>(cmd);
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public List<FeedItemEntity> GetListFeedItem()
        {
            const string commandText = "CMS_GetAllFeedItem";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                return _db.GetList<FeedItemEntity>(cmd);
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public List<FeedItemEntity> GetListFeedItem_Download()
        {
            const string commandText = "CMS_GetAllFeedItem_Download";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                return _db.GetList<FeedItemEntity>(cmd);
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public List<FeedItemEntity> GetListFeedItem_ReDownload()
        {
            const string commandText = "CMS_FeedItem_GetReDownload";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                return _db.GetList<FeedItemEntity>(cmd);
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public bool InsertItem(FeedItemEntity obj, ref int Id)
        {
            const string commandText = "CMS_FeedItem_Insert";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", Id, ParameterDirection.Output);
                _db.AddParameter(cmd, "Title", obj.Title);
                _db.AddParameter(cmd, "Description", obj.Description);
                _db.AddParameter(cmd, "FeedId", obj.FeedId);
                _db.AddParameter(cmd, "Link", obj.Link);
                _db.AddParameter(cmd, "MediaKeywords", obj.MediaKeywords);
                _db.AddParameter(cmd, "MediaThumbnail", obj.MediaThumbnail);
                _db.AddParameter(cmd, "MediaTitle", obj.MediaTitle);
                _db.AddParameter(cmd, "MediaUrl", obj.MediaUrl);
                _db.AddParameter(cmd, "Status", obj.Status);
                _db.AddParameter(cmd, "CreatedDate", obj.CreatedDate);
                var numberOfRow = _db.ExecuteNonQuery(cmd);
                Id = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 0));
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool Update_Status(int Id, FeedItemStatus status, int DownloadMediaId = -1)
        {
            const string commandText = "CMS_Feed_Update_Status";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", Id);
                _db.AddParameter(cmd, "Status", (int)status);
                _db.AddParameter(cmd, "DownloadMediaId", DownloadMediaId);
                return _db.ExecuteNonQuery(cmd) > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool Update_Status_ByMediaIds(string MediaIds)
        {
            const string commandText = "CMS_Feed_Update_Status_ByMediaIds";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Status", (int)FeedItemStatus.Download_Done);
                _db.AddParameter(cmd, "DownloadMediaIds", MediaIds);
                return _db.ExecuteNonQuery(cmd) > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        
        #region FeedItemV2

        public FeedItemV2Entity FeedItemV2_GetById(int Id)
        {
            const string commandText = "CMS_FeedItemV2_GetById";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", Id);
                return _db.Get<FeedItemV2Entity>(cmd);
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public List<FeedItemV2Entity> FeedItemV2_GetListCheckDownload()
        {
            const string commandText = "CMS_FeedItemV2_GetListCheckDownload";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                return _db.GetList<FeedItemV2Entity>(cmd);
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public List<FeedItemV2Entity> FeedItemV2_GetListDownloadFinished()
        {
            const string commandText = "CMS_GetAllFeedItemV2_DownloadFinished";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                return _db.GetList<FeedItemV2Entity>(cmd);
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public bool InsertFeedItemV2(FeedItemV2Entity obj, ref int Id)
        {
            const string commandText = "CMS_FeedItemV2_Insert";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", Id, ParameterDirection.Output);
                _db.AddParameter(cmd, "Title", obj.Title);
                _db.AddParameter(cmd, "Description", obj.Description);
                _db.AddParameter(cmd, "FeedId", obj.FeedId);
                _db.AddParameter(cmd, "Link", obj.Link);
                _db.AddParameter(cmd, "MediaKeywords", obj.MediaKeywords);
                _db.AddParameter(cmd, "MediaThumbnail", obj.MediaThumbnail);
                _db.AddParameter(cmd, "MediaTitle", obj.MediaTitle);
                _db.AddParameter(cmd, "MediaUrl", obj.MediaUrl);
                _db.AddParameter(cmd, "Status", obj.Status);
                _db.AddParameter(cmd, "CreatedDate", obj.CreatedDate);
                _db.AddParameter(cmd, "Duration", obj.Duration);
                _db.AddParameter(cmd, "FilePath", obj.FilePath);
                var numberOfRow = _db.ExecuteNonQuery(cmd);
                Id = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 0));
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool FeedItemV2_UpdateByComplete(FeedItemV2Entity obj)
        {
            const string commandText = "CMS_FeedItemV2_UpdateByComplete";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", obj.Id);
                _db.AddParameter(cmd, "KeyVideo", obj.KeyVideo);
                _db.AddParameter(cmd, "Avatar", obj.Avatar);
                _db.AddParameter(cmd, "Embed", obj.Embed);
                _db.AddParameter(cmd, "FilePath", obj.FilePath);
                return _db.ExecuteNonQuery(cmd) > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool FeedItemV2_UpdateStatus(int Id, FeedItemStatus status)
        {
            const string commandText = "CMS_FeedItemV2_UpdateStatus";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Status", (int)status);
                return _db.ExecuteNonQuery(cmd) > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool FeedItemV2_DownloadFinished(string MediaIds)
        {
            const string commandText = "CMS_FeedItemV2_FinishedDownload";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "MediaIds", MediaIds);
                return _db.ExecuteNonQuery(cmd) > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool FeedItemV2_Downloading(string IdAndMediaIds)
        {
            const string commandText = "CMS_FeedItemV2_Downloading";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "IdAndMediaIds", IdAndMediaIds);
                return _db.ExecuteNonQuery(cmd) > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        #endregion

        #region Core members

        private readonly CmsFeedDb _db;

        protected FeedDalBase(CmsFeedDb db)
        {
            _db = db;
        }

        protected CmsFeedDb Database
        {
            get { return _db; }
        }

        #endregion
    }
}
