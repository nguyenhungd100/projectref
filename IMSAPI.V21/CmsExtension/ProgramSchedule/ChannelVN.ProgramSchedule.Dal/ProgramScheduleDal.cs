﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ChannelVN.ProgramSchedule.Entity;
using ChannelVN.ProgramSchedule.Dal.Common;
using System.Data.SqlClient;
using ChannelVN.CMS.Common;
using System.Data;
using ChannelVN.ProgramSchedule.MainDal.Databases;

namespace ChannelVN.ProgramSchedule.Dal
{
    public class ProgramScheduleDal
    {
        public static bool Insert(ProgramScheduleEntity programSchedule, ref int newProgramScheduleId)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.ProgramScheduleMainDal.Insert(programSchedule, ref newProgramScheduleId);
            }
            return returnValue;
        }
        public static bool Update(ProgramScheduleEntity programSchedule)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.ProgramScheduleMainDal.Update(programSchedule);
            }
            return returnValue;
        }
        public static bool Delete(int programScheduleId)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.ProgramScheduleMainDal.Delete(programScheduleId);
            }
            return returnValue;
        }
        public static List<ProgramScheduleEntity> Search(string keyword, DateTime date, int status, int programChannelId, int pageIndex, int pageSize, ref int totalRow)
        {
            List<ProgramScheduleEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.ProgramScheduleMainDal.Search(keyword, date, status, programChannelId, pageIndex, pageSize, ref totalRow);
            }
            return returnValue;
        }
        public static ProgramScheduleEntity GetByProgramScheduleId(int id)
        {
            ProgramScheduleEntity returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.ProgramScheduleMainDal.GetByProgramScheduleId(id);
            }
            return returnValue;
        }
        public static List<ProgramScheduleEntity> GetByProgramScheduleDate(int programChannelId, DateTime scheduleDate)
        {
            List<ProgramScheduleEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.ProgramScheduleMainDal.GetByProgramScheduleDate(programChannelId, scheduleDate);
            }
            return returnValue;
        }
        public static bool CheckExist(ProgramScheduleEntity programSchedule, ref int newProgramScheduleId)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.ProgramScheduleMainDal.CheckExist(programSchedule, ref newProgramScheduleId);
            }
            return returnValue;
        }
    }
}
