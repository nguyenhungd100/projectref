﻿using System;
using System.Collections.Generic;
using ChannelVN.ProgramSchedule.Entity;
using ChannelVN.ProgramSchedule.Dal.Common;
using System.Data.SqlClient;
using ChannelVN.CMS.Common;
using System.Data;
using ChannelVN.ProgramSchedule.MainDal.Databases;

namespace ChannelVN.ProgramSchedule.Dal
{
    public class ProgramScheduleDetailDal
    {
        public static bool Insert(ProgramScheduleDetailEntity programScheduleDetail, ref int newProgramScheduleDetailId)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.ProgramScheduleDetailMainDal.Insert(programScheduleDetail, ref newProgramScheduleDetailId);
            }
            return returnValue;
        }
        public static bool Update(ProgramScheduleDetailEntity programScheduleDetail)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.ProgramScheduleDetailMainDal.Update(programScheduleDetail);
            }
            return returnValue;
        }
        public static bool Delete(int programScheduleDetailId)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.ProgramScheduleDetailMainDal.Delete(programScheduleDetailId);
            }
            return returnValue;
        }
        public static bool DeleteList(string listId)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.ProgramScheduleDetailMainDal.DeleteList(listId);
            }
            return returnValue;
        }
        public static List<ProgramScheduleDetailEntity> GetByProgramScheduleId(int programScheduleId, int status)
        {
            List<ProgramScheduleDetailEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.ProgramScheduleDetailMainDal.GetByProgramScheduleId(programScheduleId, status);
            }
            return returnValue;
        }

        public static List<ProgramScheduleDetailEntity> ProgramScheduleDetailSearchExport(int programChannelId, DateTime fromDate, DateTime toDate, int status)
        {
            List<ProgramScheduleDetailEntity> returnValue = null;
            using (var db = new CmsMainDb())
            {
                try
                {
                    returnValue = db.ProgramScheduleDetailMainDal.SearchExport(programChannelId, fromDate, toDate, status);
                }
                catch (Exception ex)
                {
                    Logger.WriteLog(Logger.LogType.Error, ex.Message);
                }
            }
            return returnValue;
        }
        public static List<ProgramScheduleDetailEntity> GetByScheduleDate(int programChannelId, DateTime scheduleDate, int status)
        {
            List<ProgramScheduleDetailEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.ProgramScheduleDetailMainDal.GetByScheduleDate(programChannelId, scheduleDate, status);
            }
            return returnValue;
        }
        public static ProgramScheduleDetailEntity GetByProgramScheduleDetailId(int id)
        {
            ProgramScheduleDetailEntity returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.ProgramScheduleDetailMainDal.GetByProgramScheduleDetailId(id);
            }
            return returnValue;
        }
    }
}
