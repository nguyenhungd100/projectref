﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ChannelVN.ProgramSchedule.Entity;
using ChannelVN.ProgramSchedule.Dal.Common;
using System.Data.SqlClient;
using ChannelVN.CMS.Common;
using System.Data;
using ChannelVN.ProgramSchedule.MainDal.Databases;

namespace ChannelVN.ProgramSchedule.Dal
{
    public class ProgramChannelDal
    {
        public static bool Insert(ProgramChannelEntity programChannel, ref int newProgramChannelId)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.ProgramChannelMainDal.Insert(programChannel, ref newProgramChannelId);
            }
            return returnValue;
        }
        public static bool Update(ProgramChannelEntity programChannel)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.ProgramChannelMainDal.Update(programChannel);
            }
            return returnValue;
        }
        public static bool Delete(int programChannelId)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.ProgramChannelMainDal.Delete(programChannelId);
            }
            return returnValue;
        }
        public static bool MoveUp(int programChannelId)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.ProgramChannelMainDal.MoveUp(programChannelId);
            }
            return returnValue;
        }
        public static bool MoveDown(int programChannelId)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.ProgramChannelMainDal.MoveDown(programChannelId);
            }
            return returnValue;
        }
        public static List<ProgramChannelEntity> GetByStatus(int status)
        {
            List<ProgramChannelEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.ProgramChannelMainDal.GetByStatus(status);
            }
            return returnValue;
        }

        public static List<ProgramChannelZoneVideoEntity> GetZoneVideo()
        {
            List<ProgramChannelZoneVideoEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.ProgramChannelMainDal.GetZoneVideo();
            }
            return returnValue;
        }
        public static ProgramChannelEntity GetByProgramChannelId(int id)
        {
            ProgramChannelEntity returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.ProgramChannelMainDal.GetByProgramChannelId(id);
            }
            return returnValue;
        }

        public static List<ProgramChannelEntity> Search(string keyword, int status, int pageIndex, int pageSize, ref int totalRow)
        {
            List<ProgramChannelEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.ProgramChannelMainDal.Search(keyword, status, pageIndex, pageSize, ref totalRow);
            }
            return returnValue;
        }
    }
}
