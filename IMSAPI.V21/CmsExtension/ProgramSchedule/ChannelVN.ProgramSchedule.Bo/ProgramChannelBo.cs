﻿using System;
using System.Collections.Generic;
using ChannelVN.ProgramSchedule.Dal;
using ChannelVN.ProgramSchedule.Entity;
using ChannelVN.ProgramSchedule.Entity.ErrorCode;

namespace ChannelVN.ProgramSchedule.Bo
{
    public class ProgramChannelBo
    {
        public static ErrorMapping.ErrorCodes Save(ProgramChannelEntity programChannelEntity, ref int id)
        {
            try
            {
                if (null == programChannelEntity)
                {
                    return ErrorMapping.ErrorCodes.InvalidRequest;
                }
                if (programChannelEntity.Id > 0)
                {
                    id = programChannelEntity.Id;
                    return Update(programChannelEntity);
                }
                return Insert(programChannelEntity, ref id);
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("ProgramChannelBo.Save:{0}", ex.Message));
            }
        }


        public static ErrorMapping.ErrorCodes Insert(ProgramChannelEntity programChannel, ref int newProgramChannelId)
        {
            if (string.IsNullOrEmpty(programChannel.Name))
            {
                return ErrorMapping.ErrorCodes.ProgramChannelMustHaveName;
            }
            return ProgramChannelDal.Insert(programChannel, ref newProgramChannelId)
                       ? ErrorMapping.ErrorCodes.Success
                       : ErrorMapping.ErrorCodes.BusinessError;
        }
        public static ErrorMapping.ErrorCodes Update(ProgramChannelEntity programChannel)
        {
            if (string.IsNullOrEmpty(programChannel.Name))
            {
                return ErrorMapping.ErrorCodes.ProgramChannelMustHaveName;
            }
            return ProgramChannelDal.Update(programChannel)
                       ? ErrorMapping.ErrorCodes.Success
                       : ErrorMapping.ErrorCodes.BusinessError;
        }
        public static ErrorMapping.ErrorCodes Delete(int programChannelId)
        {
            return ProgramChannelDal.Delete(programChannelId)
                       ? ErrorMapping.ErrorCodes.Success
                       : ErrorMapping.ErrorCodes.BusinessError;
        }
        public static ErrorMapping.ErrorCodes MoveUp(int programChannelId)
        {
            return ProgramChannelDal.MoveUp(programChannelId)
                       ? ErrorMapping.ErrorCodes.Success
                       : ErrorMapping.ErrorCodes.BusinessError;
        }
        public static ErrorMapping.ErrorCodes MoveDown(int programChannelId)
        {
            return ProgramChannelDal.MoveDown(programChannelId)
                       ? ErrorMapping.ErrorCodes.Success
                       : ErrorMapping.ErrorCodes.BusinessError;
        }
        public static List<ProgramChannelZoneVideoEntity> GetZoneVideo()
        {
            return ProgramChannelDal.GetZoneVideo();
        }
        public static List<ProgramChannelEntity> GetByStatus(EnumProgramChannelStatus status)
        {
            return ProgramChannelDal.GetByStatus((int)status);
        }
        public static ProgramChannelEntity GetByProgramChannelId(int id)
        {
            return ProgramChannelDal.GetByProgramChannelId(id);
        }
        public static List<ProgramChannelEntity> Search(string keyword, int status, int pageIndex, int pageSize, ref int totalRow)
        {
            return ProgramChannelDal.Search(keyword, status, pageIndex, pageSize, ref totalRow);
        }
    }
}
