﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ChannelVN.ProgramSchedule.Dal;
using ChannelVN.ProgramSchedule.Entity;
using ChannelVN.ProgramSchedule.Entity.ErrorCode;

namespace ChannelVN.ProgramSchedule.Bo
{
    public class ProgramScheduleBo
    {
        #region Program schedule
        public static ErrorMapping.ErrorCodes InsertProgramSchedule(ProgramScheduleEntity programSchedule, ref int newProgramScheduleId)
        {
            if (string.IsNullOrEmpty(programSchedule.ScheduleName))
            {
                return ErrorMapping.ErrorCodes.ProgramScheduleMustHaveName;
            }
            return ProgramScheduleDal.Insert(programSchedule, ref newProgramScheduleId)
                       ? ErrorMapping.ErrorCodes.Success
                       : ErrorMapping.ErrorCodes.BusinessError;
        }
        public static ErrorMapping.ErrorCodes UpdateProgramSchedule(ProgramScheduleEntity programSchedule)
        {
            if (string.IsNullOrEmpty(programSchedule.ScheduleName))
            {
                return ErrorMapping.ErrorCodes.ProgramScheduleMustHaveName;
            }
            return ProgramScheduleDal.Update(programSchedule)
                       ? ErrorMapping.ErrorCodes.Success
                       : ErrorMapping.ErrorCodes.BusinessError;
        }
        public static ErrorMapping.ErrorCodes DeleteProgramSchedule(int programScheduleId)
        {
            return ProgramScheduleDal.Delete(programScheduleId)
                       ? ErrorMapping.ErrorCodes.Success
                       : ErrorMapping.ErrorCodes.BusinessError;
        }
        public static List<ProgramScheduleEntity> SearchProgramSchedule(string keyword, DateTime date, EnumProgramScheduleStatus status, int programChannelId, int pageIndex, int pageSize, ref int totalRow)
        {
            return ProgramScheduleDal.Search(keyword, date, (int)status, programChannelId, pageIndex, pageSize, ref totalRow);
        }
        public static ProgramScheduleEntity GetProgramScheduleByProgramScheduleId(int id)
        {
            return ProgramScheduleDal.GetByProgramScheduleId(id);
        }
        public static List<ProgramScheduleEntity> GetByProgramScheduleDate(int programChannelId, DateTime scheduleDate)
        {
            return ProgramScheduleDal.GetByProgramScheduleDate(programChannelId, scheduleDate);
        }
        public static ErrorMapping.ErrorCodes CheckExistProgramSchedule(ProgramScheduleEntity programSchedule, ref int newProgramScheduleId)
        {
            return ProgramScheduleDal.CheckExist(programSchedule, ref newProgramScheduleId)
                       ? ErrorMapping.ErrorCodes.Success
                       : ErrorMapping.ErrorCodes.BusinessError;
        }
        #endregion

        #region Program schedule detail

        public static ErrorMapping.ErrorCodes InsertProgramScheduleDetail(ProgramScheduleDetailEntity programScheduleDetail, ref int newProgramScheduleDetailId)
        {
            if (string.IsNullOrEmpty(programScheduleDetail.Title))
            {
                return ErrorMapping.ErrorCodes.ProgramScheduleDetailMustHaveName;
            }
            return ProgramScheduleDetailDal.Insert(programScheduleDetail, ref newProgramScheduleDetailId)
                       ? ErrorMapping.ErrorCodes.Success
                       : ErrorMapping.ErrorCodes.BusinessError;
        }
        public static ErrorMapping.ErrorCodes UpdateProgramScheduleDetail(ProgramScheduleDetailEntity programScheduleDetail)
        {
            if (string.IsNullOrEmpty(programScheduleDetail.Title))
            {
                return ErrorMapping.ErrorCodes.ProgramScheduleDetailMustHaveName;
            }
            return ProgramScheduleDetailDal.Update(programScheduleDetail)
                       ? ErrorMapping.ErrorCodes.Success
                       : ErrorMapping.ErrorCodes.BusinessError;
        }
        public static ErrorMapping.ErrorCodes DeleteProgramScheduleDetail(int programScheduleDetailId)
        {
            return ProgramScheduleDetailDal.Delete(programScheduleDetailId)
                       ? ErrorMapping.ErrorCodes.Success
                       : ErrorMapping.ErrorCodes.BusinessError;
        }
        public static ErrorMapping.ErrorCodes DeleteListProgramScheduleDetail(string listId)
        {
            return ProgramScheduleDetailDal.DeleteList(listId)
                       ? ErrorMapping.ErrorCodes.Success
                       : ErrorMapping.ErrorCodes.BusinessError;
        }
        public static List<ProgramScheduleDetailEntity> GetProgramScheduleDetailByProgramScheduleId(int programScheduleId, EnumProgramScheduleDetailStatus status)
        {
            return ProgramScheduleDetailDal.GetByProgramScheduleId(programScheduleId, (int)status);
        }
        public static List<ProgramScheduleDetailEntity> ProgramScheduleDetailSearchExport(int programChannelId, DateTime fromDate, DateTime toDate, EnumProgramScheduleDetailStatus status)
        {
            return ProgramScheduleDetailDal.ProgramScheduleDetailSearchExport(programChannelId, fromDate, toDate, (int)status);
        }
        public static List<ProgramScheduleDetailEntity> GetProgramScheduleDetailByScheduleDate(int programChannelId, DateTime scheduleDate, EnumProgramScheduleDetailStatus status)
        {
            return ProgramScheduleDetailDal.GetByScheduleDate(programChannelId, scheduleDate, (int)status);
        }
        public static ProgramScheduleDetailEntity GetProgramScheduleDetailByProgramScheduleDetailId(int id)
        {
            return ProgramScheduleDetailDal.GetByProgramScheduleDetailId(id);
        }

        #endregion
    }
}
