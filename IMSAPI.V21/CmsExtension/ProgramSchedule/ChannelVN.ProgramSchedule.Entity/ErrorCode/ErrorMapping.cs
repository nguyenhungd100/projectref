﻿using System.Runtime.Serialization;
using ChannelVN.CMS.Common;

namespace ChannelVN.ProgramSchedule.Entity.ErrorCode
{
    [DataContract]
    public class ErrorMapping : BoErrorMappingBase<ErrorMapping.ErrorCodes>
    {
        private static ErrorMapping _current;
        public static ErrorMapping Current
        {
            get { return _current ?? (_current = new ErrorMapping()); }
        }

        protected override void InitErrorMapping()
        {
            InnerHashtable[ErrorCodes.ProgramChannelMustHaveName] = "Bạn chưa nhập tên kênh chương trình";
            InnerHashtable[ErrorCodes.ProgramScheduleMustHaveName] = "Bạn chưa nhập tên lịch phát chương trình";
            InnerHashtable[ErrorCodes.ProgramScheduleDetailMustHaveName] = "Bạn chưa nhập tên kênh chương trình";
            InnerHashtable[ErrorCodes.ProgramChannelNotAvailable] = "ProgramChannelNotAvailable kênh chương trình";
        }
        [DataContract]
        public enum ErrorCodes
        {
            #region General error

            [EnumMember]
            Success = ErrorCodeBase.Success,
            [EnumMember]
            UnknowError = ErrorCodeBase.UnknowError,
            [EnumMember]
            Exception = ErrorCodeBase.Exception,
            [EnumMember]
            BusinessError = ErrorCodeBase.BusinessError,
            [EnumMember]
            InvalidRequest = ErrorCodeBase.InvalidRequest,
            [EnumMember]
            TimeOutSession = ErrorCodeBase.TimeOutSession,

            #endregion

            [EnumMember]
            ProgramChannelMustHaveName = 07014,
            [EnumMember]
            ProgramScheduleMustHaveName = 07015,
            [EnumMember]
            ProgramScheduleDetailMustHaveName = 07016,
            [EnumMember]
            ProgramChannelNotAvailable =07017,
        }
    }
}
