﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ChannelVN.CMS.Common;
using System.Runtime.Serialization;

namespace ChannelVN.ProgramSchedule.Entity
{
    [DataContract]
    public enum EnumProgramScheduleDetailStatus : int
    {
        [EnumMember]
        AllStatus = -1,
        [EnumMember]
        Actived = 1,
        [EnumMember]
        Inactived = 0
    }
    [DataContract]
    public class ProgramScheduleDetailEntity : EntityBase
    {
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public int ProgramChannelId { get; set; }
        [DataMember]
        public int ProgramScheduleId { get; set; }
        [DataMember]
        public DateTime ScheduleTime { get; set; }
        [DataMember]
        public string Title { get; set; }
        [DataMember]
        public string Description { get; set; }
        [DataMember]
        public string Avatar { get; set; }
        [DataMember]
        public int VideoId { get; set; }
        [DataMember]
        public string CreatedBy { get; set; }
        [DataMember]
        public DateTime CreatedDate { get; set; }
        [DataMember]
        public string LastModifiedBy { get; set; }
        [DataMember]
        public DateTime LastModifiedDate { get; set; }
        [DataMember]
        public string PublishedBy { get; set; }
        [DataMember]
        public DateTime PublishedDate { get; set; }
        [DataMember]
        public bool ShowOnSchedule { get; set; }
        [DataMember]
        public int Status { get; set; }
        [DataMember]
        public int ExternalProgram { get; set; }
        [DataMember]
        public string VideoUrl { get; set; }

        [DataMember]
        public string VideoDuration { get; set; }
            [DataMember]
        public int ZoneVideoId { get; set; }

        [DataMember]
            public string ZoneVideoName { get; set; }
    }
}
