﻿using System;
using System.Collections.Generic;
using ChannelVN.CMS.Common;
using System.Runtime.Serialization;

namespace ChannelVN.ProgramSchedule.Entity
{
    [DataContract]
    public enum EnumProgramChannelName : int
    {
        [EnumMember]
        VTV1 = 1,
        [EnumMember]
        VTV2 = 2,
        [EnumMember]
        VTV3 = 3,
        [EnumMember]
        VTV4 = 4,
        [EnumMember]
        VTV5 = 5,
        [EnumMember]
        VTV6 = 6,
        [EnumMember]
        VTV7 = 7,
        [EnumMember]
        VTV8 = 8,
        [EnumMember]
        VTV9 = 9,
        [EnumMember]
        VTV_Online = 10,
        [EnumMember]
        VTVCab2 = 11,
        [EnumMember]
        VTVCab4 = 12,
        [EnumMember]
        VTVCab6 = 13,
        [EnumMember]
        VTVCab8 = 14,
        [EnumMember]
        KPlus1 = 15
    }
    [DataContract]
    public enum EnumProgramChannelStatus : int
    {
        [EnumMember]
        AllStatus = -1,
        [EnumMember]
        Actived = 1,
        [EnumMember]
        Inactived = 0
    }
    [DataContract]
    public class ProgramChannelEntity : EntityBase
    {
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public string Name { get; set; }
        [DataMember]
        public string Avatar { get; set; }
        [DataMember]
        public string CreatedBy { get; set; }
        [DataMember]
        public DateTime CreatedDate { get; set; }
        [DataMember]
        public string LastModifiedBy { get; set; }
        [DataMember]
        public DateTime LastModifiedDate { get; set; }
        [DataMember]
        public int Priority { get; set; }
        [DataMember]
        public int Status { get; set; }
        [DataMember]
        public int Type { get; set; }
        [DataMember]
        public int DefaultVideoTagId { get; set; }
        [DataMember]
        public List<ProgramScheduleEntity> ListProgramSchedule { get; set; }
        [DataMember]
        public int ZoneVideoId { get; set; }
        [DataMember]
        public string LiveTvThumb { get; set; }
        [DataMember]
        public string LiveTvEmbed { get; set; }
        [DataMember]
        public string FullName { get; set; }
    }


    public class ProgramChannelZoneVideoEntity : EntityBase
    {
        [DataMember]
        public int ProgramChannelId { get; set; }
        [DataMember]
        public string ZoneName { get; set; }
        [DataMember]
        public int ZoneId { get; set; }
        [DataMember]
        public string Keyword { get; set; }
    }
}
