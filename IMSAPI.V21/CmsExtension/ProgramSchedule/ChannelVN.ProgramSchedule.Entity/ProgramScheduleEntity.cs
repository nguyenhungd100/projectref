﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ChannelVN.CMS.Common;
using System.Runtime.Serialization;

namespace ChannelVN.ProgramSchedule.Entity
{
    [DataContract]
    public enum EnumProgramScheduleStatus : int
    {
        [EnumMember]
        AllStatus = -1,
        [EnumMember]
        Actived = 1,
        [EnumMember]
        Inactived = 0
    }
    [DataContract]
    public class ProgramScheduleEntity : EntityBase
    {
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public int ProgramChannelId { get; set; }
        [DataMember]
        public string ScheduleName { get; set; }
        [DataMember]
        public string ScheduleAvatar { get; set; }
        [DataMember]
        public int PlayListId { get; set; }
        [DataMember]
        public DateTime ScheduleDate { get; set; }
        [DataMember]
        public string CreatedBy { get; set; }
        [DataMember]
        public DateTime CreatedDate { get; set; }
        [DataMember]
        public string LastModifiedBy { get; set; }
        [DataMember]
        public DateTime LastModifiedDate { get; set; }
        [DataMember]
        public int Status { get; set; }
        [DataMember]
        public List<ProgramScheduleDetailEntity> ListProgramScheduleDetail { get; set; }
    }
}
