﻿using System;
using System.Collections.Generic;
using ChannelVN.ProgramSchedule.Entity;

using System.Data.SqlClient;
using ChannelVN.CMS.Common;
using System.Data;
using ChannelVN.ProgramSchedule.MainDal.Databases;

namespace ChannelVN.ProgramSchedule.MainDal
{
    public abstract class ProgramScheduleDetailDalBase
    {
        public bool Insert(ProgramScheduleDetailEntity programScheduleDetail, ref int newProgramScheduleDetailId)
        {
            const string commandText = "CMS_ProgramScheduleDetail_Insert"; try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "ProgramScheduleId", programScheduleDetail.ProgramScheduleId);
                _db.AddParameter(cmd, "ScheduleTime", programScheduleDetail.ScheduleTime);
                _db.AddParameter(cmd, "Title", programScheduleDetail.Title);
                _db.AddParameter(cmd, "Description", programScheduleDetail.Description);
                _db.AddParameter(cmd, "Avatar", programScheduleDetail.Avatar);
                _db.AddParameter(cmd, "VideoId", programScheduleDetail.VideoId);
                _db.AddParameter(cmd, "CreatedBy", programScheduleDetail.CreatedBy);
                _db.AddParameter(cmd, "ShowOnSchedule", programScheduleDetail.ShowOnSchedule);
                _db.AddParameter(cmd, "Status", programScheduleDetail.Status);
                _db.AddParameter(cmd, "ExternalProgram", programScheduleDetail.ExternalProgram);
                _db.AddParameter(cmd, "ZoneVideoId", programScheduleDetail.ZoneVideoId);
                _db.AddParameter(cmd, "Id", newProgramScheduleDetailId, ParameterDirection.Output);
                var numberOfRow = _db.ExecuteNonQuery(cmd);
                newProgramScheduleDetailId = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, cmd.Parameters.Count - 1));
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool Update(ProgramScheduleDetailEntity programScheduleDetail)
        {
            const string commandText = "CMS_ProgramScheduleDetail_Update"; try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", programScheduleDetail.Id);
                _db.AddParameter(cmd, "ProgramScheduleId", programScheduleDetail.ProgramScheduleId);
                _db.AddParameter(cmd, "ScheduleTime", programScheduleDetail.ScheduleTime);
                _db.AddParameter(cmd, "Title", programScheduleDetail.Title);
                _db.AddParameter(cmd, "Description", programScheduleDetail.Description);
                _db.AddParameter(cmd, "Avatar", programScheduleDetail.Avatar);
                _db.AddParameter(cmd, "VideoId", programScheduleDetail.VideoId);
                _db.AddParameter(cmd, "LastModifiedBy", programScheduleDetail.LastModifiedBy);
                _db.AddParameter(cmd, "ShowOnSchedule", programScheduleDetail.ShowOnSchedule);
                _db.AddParameter(cmd, "ExternalProgram", programScheduleDetail.ExternalProgram);
                _db.AddParameter(cmd, "ZoneVideoId", programScheduleDetail.ZoneVideoId);
                _db.AddParameter(cmd, "Status", programScheduleDetail.Status);
                var numberOfRow = _db.ExecuteNonQuery(cmd);
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool Delete(int programScheduleDetailId)
        {
            const string commandText = "CMS_ProgramScheduleDetail_Delete"; try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", programScheduleDetailId);
                var numberOfRow = _db.ExecuteNonQuery(cmd);
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool DeleteList(string listId)
        {
            const string commandText = "CMS_ProgramScheduleDetail_Delete_List"; try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "scheduleDetailIds", listId);
                var numberOfRow = _db.ExecuteNonQuery(cmd);
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public List<ProgramScheduleDetailEntity> GetByProgramScheduleId(int programScheduleId, int status)
        {
            const string commandText = "CMS_ProgramScheduleDetail_GetByProgramScheduleId"; try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "ProgramScheduleId", programScheduleId);
                _db.AddParameter(cmd, "Status", status);
                var numberOfRow = _db.GetList<ProgramScheduleDetailEntity>(cmd);
                return numberOfRow;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public List<ProgramScheduleDetailEntity> SearchExport(int programChannelId, DateTime fromDate, DateTime toDate, int status)
        {
            const string commandText = "CMS_ProgramScheduleDetail_Search_Export";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "ProgramChannelId", programChannelId);
                _db.AddParameter(cmd, "FromDate", fromDate);
                _db.AddParameter(cmd, "ToDate", toDate);
                _db.AddParameter(cmd, "Status", status);
                var numberOfRow = _db.GetList<ProgramScheduleDetailEntity>(cmd);
                return numberOfRow;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public List<ProgramScheduleDetailEntity> GetByScheduleDate(int programChannelId, DateTime scheduleDate, int status)
        {
            const string commandText = "CMS_ProgramScheduleDetail_GetByScheduleDate";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "ProgramChannelId", programChannelId);
                _db.AddParameter(cmd, "ScheduleDate", scheduleDate);
                _db.AddParameter(cmd, "Status", status);
                var numberOfRow = _db.GetList<ProgramScheduleDetailEntity>(cmd);
                return numberOfRow;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public ProgramScheduleDetailEntity GetByProgramScheduleDetailId(int id)
        {
            const string commandText = "CMS_ProgramScheduleDetail_GetbyId"; try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", id);
                var numberOfRow = _db.Get<ProgramScheduleDetailEntity>(cmd);
                return numberOfRow;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        #region Core members

        private readonly CmsMainDb _db;

        protected ProgramScheduleDetailDalBase(CmsMainDb db)
        {
            _db = db;
        }

        protected CmsMainDb Database
        {
            get { return _db; }
        }

        #endregion
    }
}
