﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ChannelVN.ProgramSchedule.Entity;

using System.Data.SqlClient;
using ChannelVN.CMS.Common;
using System.Data;
using ChannelVN.ProgramSchedule.MainDal.Databases;

namespace ChannelVN.ProgramSchedule.MainDal
{
    public abstract class ProgramScheduleDalBase
    {
        public bool Insert(ProgramScheduleEntity programSchedule, ref int newProgramScheduleId)
        {
            const string commandText = "CMS_ProgramSchedule_Insert";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "ProgramChannelId", programSchedule.ProgramChannelId);
                _db.AddParameter(cmd, "ScheduleName", programSchedule.ScheduleName);
                _db.AddParameter(cmd, "ScheduleAvatar", programSchedule.ScheduleAvatar);
                _db.AddParameter(cmd, "PlayListId", programSchedule.PlayListId);
                _db.AddParameter(cmd, "ScheduleDate", programSchedule.ScheduleDate);
                _db.AddParameter(cmd, "CreatedBy", programSchedule.CreatedBy);
                _db.AddParameter(cmd, "Status", programSchedule.Status);
                _db.AddParameter(cmd, "Id", newProgramScheduleId, ParameterDirection.Output);
                var numberOfRow = _db.ExecuteNonQuery(cmd);
                newProgramScheduleId = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, cmd.Parameters.Count - 1));
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool Update(ProgramScheduleEntity programSchedule)
        {
            const string commandText = "CMS_ProgramSchedule_Update";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", programSchedule.Id);
                _db.AddParameter(cmd, "ProgramChannelId", programSchedule.ProgramChannelId);
                _db.AddParameter(cmd, "ScheduleName", programSchedule.ScheduleName);
                _db.AddParameter(cmd, "ScheduleAvatar", programSchedule.ScheduleAvatar);
                _db.AddParameter(cmd, "PlayListId", programSchedule.PlayListId);
                _db.AddParameter(cmd, "ScheduleDate", programSchedule.ScheduleDate);
                _db.AddParameter(cmd, "LastModifiedBy", programSchedule.LastModifiedBy);
                _db.AddParameter(cmd, "Status", programSchedule.Status);
                var numberOfRow = _db.ExecuteNonQuery(cmd);
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool Delete(int programScheduleId)
        {
            const string commandText = "CMS_ProgramSchedule_Delete";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", programScheduleId);
                var numberOfRow = _db.ExecuteNonQuery(cmd);
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public List<ProgramScheduleEntity> Search(string keyword, DateTime date, int status, int programChannelId, int pageIndex, int pageSize, ref int totalRow)
        {
            const string commandText = "CMS_ProgramSchedule_Search";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "ProgramChannelId", programChannelId);
                _db.AddParameter(cmd, "Keyword", keyword);
                _db.AddParameter(cmd, "ScheduleDate", date);
                _db.AddParameter(cmd, "Status", status);
                _db.AddParameter(cmd, "PageIndex", pageIndex);
                _db.AddParameter(cmd, "PageSize", pageSize);
                _db.AddParameter(cmd, "TotalRow", totalRow, ParameterDirection.Output);
                var numberOfRow = _db.GetList<ProgramScheduleEntity>(cmd);
                totalRow = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, cmd.Parameters.Count - 1));
                return numberOfRow;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public ProgramScheduleEntity GetByProgramScheduleId(int id)
        {
            const string commandText = "CMS_ProgramSchedule_GetbyId"; try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", id);
                var numberOfRow = _db.Get<ProgramScheduleEntity>(cmd);
                return numberOfRow;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public List<ProgramScheduleEntity> GetByProgramScheduleDate(int programChannelId, DateTime scheduleDate)
        {
            const string commandText = "CMS_ProgramSchedule_GetbyScheduleDate"; try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "ProgramChannelId", programChannelId);
                _db.AddParameter(cmd, "ScheduleDate", scheduleDate);
                var numberOfRow = _db.GetList<ProgramScheduleEntity>(cmd);
                return numberOfRow;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool CheckExist(ProgramScheduleEntity programSchedule, ref int newProgramScheduleId)
        {
            const string commandText = "CMS_ProgramSchedule_CheckExist"; try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "ProgramChannelId", programSchedule.ProgramChannelId);
                _db.AddParameter(cmd, "ScheduleDate", programSchedule.ScheduleDate);
                _db.AddParameter(cmd, "Id", newProgramScheduleId, ParameterDirection.Output);
                var numberOfRow = _db.ExecuteNonQuery(cmd);
                newProgramScheduleId = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, cmd.Parameters.Count - 1));
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        #region Core members

        private readonly CmsMainDb _db;

        protected ProgramScheduleDalBase(CmsMainDb db)
        {
            _db = db;
        }

        protected CmsMainDb Database
        {
            get { return _db; }
        }

        #endregion
    }
}
