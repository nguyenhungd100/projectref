﻿using System.Data;
using ChannelVN.CMS.Common.DalCommon;

namespace ChannelVN.ProgramSchedule.MainDal.Databases
{
    /// <summary>
    /// The base class for the <see cref="CmsMainDb"/> class that 
    /// represents a connection to the <c>CmsMainDb</c> database. 
    /// </summary>
    /// <remarks>
    /// Do not change this source code. Modify the CmsMainDb class
    /// if you need to add or change some functionality.
    /// </remarks>
    public abstract class CmsMainDbBase : MainDbBase
    {
        #region Store procedures

        #region ProgramChannel

        private ProgramChannelDal _programChannelMainDal;
        public ProgramChannelDal ProgramChannelMainDal
        {
            get { return _programChannelMainDal ?? (_programChannelMainDal = new ProgramChannelDal((CmsMainDb)this)); }
        }

        #endregion

        #region ProgramSchedule

        private ProgramScheduleDal _programScheduleMainDal;
        public ProgramScheduleDal ProgramScheduleMainDal
        {
            get { return _programScheduleMainDal ?? (_programScheduleMainDal = new ProgramScheduleDal((CmsMainDb)this)); }
        }

        #endregion

        #region Program Schedule Detail

        private ProgramScheduleDetailDal _programScheduleDetailMainDal;
        public ProgramScheduleDetailDal ProgramScheduleDetailMainDal
        {
            get { return _programScheduleDetailMainDal ?? (_programScheduleDetailMainDal = new ProgramScheduleDetailDal((CmsMainDb)this)); }
        }

        #endregion

        #endregion

        #region Constructors

        protected CmsMainDbBase()
        {
        }
        protected CmsMainDbBase(bool init)
        {
            if (init)
            {
                InitConnection();
            }
        }

        #endregion

        #region Protected members

        protected override sealed void InitConnection()
        {
            _connection = CreateConnection();
            _connection.Open();
        }

        /// <summary>
        /// Creates a new connection to the database.
        /// </summary>
        /// <returns>A reference to the <see cref="System.Data.IDbConnection"/> object.</returns>
        protected abstract IDbConnection CreateConnection();

        #endregion
    }
}