﻿using System.Data;
using System.Data.SqlClient;
using ChannelVN.CMS.Common.ChannelConfig;
using ChannelVN.ProgramSchedule.MainDal.Common;
using ChannelVN.WcfExtensions;
using ChannelVN.CMS.Common;

namespace ChannelVN.ProgramSchedule.MainDal.Databases
{
    public class CmsMainDb : CmsMainDbBase
    {
        private const string ConnectionStringName = "CmsMainDb";
        private const string ConnectionStringName_Dev = "CmsMainDb_Dev";

        /// <summary>
        /// Creates a new connection to the database.
        /// </summary>
        /// <returns>An <see cref="System.Data.IDbConnection"/> object.</returns>
        protected override IDbConnection CreateConnection()
        {
            var strConn = ServiceChannelConfiguration.GetConnectionString(WcfMessageHeader.Current.Namespace,
                ConnectionStringName, Constants.ConnectionDecryptKey);
            if (AppSettings.GetBool("IsDevEnv"))
            {
                strConn = ServiceChannelConfiguration.GetConnectionString(WcfMessageHeader.Current.Namespace,
                ConnectionStringName_Dev, Constants.ConnectionDecryptKey);
            }

            return new SqlConnection(strConn);
        }
    }
}