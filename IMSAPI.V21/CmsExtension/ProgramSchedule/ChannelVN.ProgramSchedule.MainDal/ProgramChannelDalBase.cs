﻿using System;
using System.Collections.Generic;
using ChannelVN.ProgramSchedule.Entity;
using ChannelVN.CMS.Common;
using System.Data;
using ChannelVN.ProgramSchedule.MainDal.Databases;

namespace ChannelVN.ProgramSchedule.MainDal
{
    public abstract class ProgramChannelDalBase
    {
        public bool Insert(ProgramChannelEntity programChannel, ref int newProgramChannelId)
        {
            const string commandText = "CMS_ProgramChannel_Insert";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Name", programChannel.Name);
                _db.AddParameter(cmd, "Avatar", programChannel.Avatar);
                _db.AddParameter(cmd, "CreatedBy", programChannel.CreatedBy);
                _db.AddParameter(cmd, "Priority", programChannel.Priority);
                _db.AddParameter(cmd, "Status", programChannel.Status);
                _db.AddParameter(cmd, "Type", programChannel.Type);
                _db.AddParameter(cmd, "LiveTvThumb", programChannel.LiveTvThumb);
                _db.AddParameter(cmd, "LiveTvEmbed", programChannel.LiveTvEmbed);
                _db.AddParameter(cmd, "FullName", programChannel.FullName);
                //_db.AddParameter(cmd, "ZoneVideoId", programChannel.ZoneVideoId);
                _db.AddParameter(cmd, "Id", newProgramChannelId, ParameterDirection.Output);
                var numberOfRow = _db.ExecuteNonQuery(cmd);
                newProgramChannelId = Utility.ConvertToInt(cmd.Parameters[cmd.Parameters.Count - 1]);
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool Update(ProgramChannelEntity programChannel)
        {
            const string commandText = "CMS_ProgramChannel_Update"; try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", programChannel.Id);
                _db.AddParameter(cmd, "Name", programChannel.Name);
                _db.AddParameter(cmd, "Avatar", programChannel.Avatar);
                _db.AddParameter(cmd, "LastModifiedBy", programChannel.LastModifiedBy);
                _db.AddParameter(cmd, "Priority", programChannel.Priority);
                _db.AddParameter(cmd, "Status", programChannel.Status);
                _db.AddParameter(cmd, "Type", programChannel.Type);
                _db.AddParameter(cmd, "LiveTvThumb", programChannel.LiveTvThumb);
                _db.AddParameter(cmd, "LiveTvEmbed", programChannel.LiveTvEmbed);
                _db.AddParameter(cmd, "FullName", programChannel.FullName);
                //_db.AddParameter(cmd, "ZoneVideoId", programChannel.ZoneVideoId);
                var numberOfRow = _db.ExecuteNonQuery(cmd);
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool Delete(int programChannelId)
        {
            const string commandText = "CMS_ProgramChannel_Delete"; try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", programChannelId);
                var numberOfRow = _db.ExecuteNonQuery(cmd);
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool MoveUp(int programChannelId)
        {
            const string commandText = "CMS_ProgramChannel_MoveUp"; try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", programChannelId);
                var numberOfRow = _db.ExecuteNonQuery(cmd);
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool MoveDown(int programChannelId)
        {
            const string commandText = "CMS_ProgramChannel_MoveDown"; try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", programChannelId);
                var numberOfRow = _db.ExecuteNonQuery(cmd);
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public List<ProgramChannelEntity> GetByStatus(int status)
        {
            const string commandText = "CMS_ProgramChannel_GetByStatus"; try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Status", status);
                var numberOfRow = _db.GetList<ProgramChannelEntity>(cmd);
                return numberOfRow;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public List<ProgramChannelZoneVideoEntity> GetZoneVideo()
        {
            const string commandText = "CMS_ProgramChannel_GetAllZoneVideo";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                var numberOfRow = _db.GetList<ProgramChannelZoneVideoEntity>(cmd);
                return numberOfRow;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public ProgramChannelEntity GetByProgramChannelId(int id)
        {
            const string commandText = "CMS_ProgramChannel_GetbyId"; try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", id);
                var numberOfRow = _db.Get<ProgramChannelEntity>(cmd);
                return numberOfRow;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public List<ProgramChannelEntity> Search(string keyword, int status, int pageIndex, int pageSize, ref int totalRow)
        {
            const string commandText = "CMS_ProgramChannel_Search"; try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "TotalRow", totalRow, ParameterDirection.Output);
                _db.AddParameter(cmd, "Keyword", keyword);
                _db.AddParameter(cmd, "Status", status);
                _db.AddParameter(cmd, "PageIndex", pageIndex);
                _db.AddParameter(cmd, "PageSize", pageSize);                
                var data = _db.GetList<ProgramChannelEntity>(cmd);
                totalRow = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 0));
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        #region Core members

        private readonly CmsMainDb _db;

        protected ProgramChannelDalBase(CmsMainDb db)
        {
            _db = db;
        }

        protected CmsMainDb Database
        {
            get { return _db; }
        }

        #endregion
    }
}
