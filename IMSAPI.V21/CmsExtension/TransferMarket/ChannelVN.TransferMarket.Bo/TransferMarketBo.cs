﻿using ChannelVN.CMS.Common;
using ChannelVN.TransferMarket.Bo.Common;
using ChannelVN.TransferMarket.Dal;
using ChannelVN.TransferMarket.Dal.Common;
using ChannelVN.TransferMarket.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ChannelVN.TransferMarket.Entity.ErrorCode;

namespace ChannelVN.TransferMarket.Bo
{
    public class TransferMarketBo
    {
        private static long SetPrimaryId()
        {
            return long.Parse(DateTime.Now.ToString(BoConstants.TRANSFER_MARKET_NEWS_FORMAT_ID));
        }

        #region TransferMarket

        public static List<TransferMarketEntity> SearchTransferMarket(string keyword, EnumTransferMarketStatus status, int pageIndex, int pageSize, ref int totalRow)
        {
            return TransferMarketDal.Search(keyword, (int)status, pageIndex, pageSize, ref totalRow);
        }
        public static TransferMarketEntity GetTransferMarketByTransferMarketId(long transferMarketId)
        {
            return TransferMarketDal.GetById(transferMarketId);
        }
        public static ErrorMapping.ErrorCodes InsertTransferMarket(TransferMarketEntity transferMarket)
        {
            if (string.IsNullOrEmpty(transferMarket.Title))
            {
                return ErrorMapping.ErrorCodes.UpdateTransferMarketInvalidTitle;
            }
            if (transferMarket.StartedDate <= DbCommon.MinDateTime)
            {
                return ErrorMapping.ErrorCodes.UpdateTransferMarketInvalidStartDate;
            }
            if (transferMarket.ClosedDate <= DbCommon.MinDateTime || transferMarket.ClosedDate < transferMarket.StartedDate)
            {
                return ErrorMapping.ErrorCodes.UpdateTransferMarketInvalidEndDate;
            }
            transferMarket.CreatedBy = WcfExtensions.WcfMessageHeader.Current.ClientUsername;
            return TransferMarketDal.Insert(transferMarket)
                       ? ErrorMapping.ErrorCodes.Success
                       : ErrorMapping.ErrorCodes.BusinessError;
        }
        public static ErrorMapping.ErrorCodes UpdateTransferMarket(TransferMarketEntity transferMarket)
        {
            if (transferMarket.Id <= 0)
            {
                return ErrorMapping.ErrorCodes.UpdateTransferMarketNotFoundTransferMarket;
            }
            var currentTransferMarket = TransferMarketDal.GetById(transferMarket.Id);
            if (currentTransferMarket == null)
            {
                return ErrorMapping.ErrorCodes.UpdateTransferMarketNotFoundTransferMarket;
            }
            if (string.IsNullOrEmpty(transferMarket.Title))
            {
                return ErrorMapping.ErrorCodes.UpdateTransferMarketInvalidTitle;
            }
            if (transferMarket.StartedDate <= DbCommon.MinDateTime)
            {
                return ErrorMapping.ErrorCodes.UpdateTransferMarketInvalidStartDate;
            }
            if (transferMarket.ClosedDate <= DbCommon.MinDateTime || transferMarket.ClosedDate < transferMarket.StartedDate)
            {
                return ErrorMapping.ErrorCodes.UpdateTransferMarketInvalidEndDate;
            }
            currentTransferMarket.Title = transferMarket.Title;
            currentTransferMarket.Avatar = transferMarket.Avatar;
            currentTransferMarket.StartedDate = transferMarket.StartedDate;
            currentTransferMarket.ClosedDate = transferMarket.ClosedDate;
            currentTransferMarket.LastModifiedBy = WcfExtensions.WcfMessageHeader.Current.ClientUsername;
            currentTransferMarket.Status = transferMarket.Status;

            return TransferMarketDal.Update(currentTransferMarket)
                       ? ErrorMapping.ErrorCodes.Success
                       : ErrorMapping.ErrorCodes.BusinessError;
        }
        public static ErrorMapping.ErrorCodes DeleteTransferMarket(long transferMarketId)
        {
            if (transferMarketId <= 0)
            {
                return ErrorMapping.ErrorCodes.UpdateTransferMarketNotFoundTransferMarket;
            }
            var currentTransferMarket = TransferMarketDal.GetById(transferMarketId);
            if (currentTransferMarket == null)
            {
                return ErrorMapping.ErrorCodes.UpdateTransferMarketNotFoundTransferMarket;
            }
            return TransferMarketDal.Delete(transferMarketId)
                       ? ErrorMapping.ErrorCodes.Success
                       : ErrorMapping.ErrorCodes.BusinessError;
        }

        #endregion

        #region TransferMarketNews

        public static List<TransferMarketNewsEntity> SearchTransferMarketNews(string keyword, long transferMarketId, EnumTransferMarketNewsLabelType labelType, int mode, EnumTransferMarketNewsStatus status, int pageIndex, int pageSize, ref int totalRow)
        {
            return TransferMarketNewsDal.Search(keyword, transferMarketId, (int) labelType, mode, (int) status,
                                                pageIndex, pageSize, ref totalRow);
        }
        public static TransferMarketNewsEntity GetTransferMarketNewsByTransferMarketNewsId(long transferMarketNewsId)
        {
            return TransferMarketNewsDal.GetById(transferMarketNewsId);
        }
        public static ErrorMapping.ErrorCodes InsertTransferMarketNews(TransferMarketNewsEntity transferMarketNews, ref long newsId)
        {
            transferMarketNews.Id = SetPrimaryId();
            if (transferMarketNews.TransferMarketId <= 0)
            {
                return ErrorMapping.ErrorCodes.UpdateTransferMarketNewsInvalidTransferMarketId;
            }
            var transferMarket = TransferMarketDal.GetById(transferMarketNews.TransferMarketId);
            if (transferMarket == null)
            {
                return ErrorMapping.ErrorCodes.UpdateTransferMarketNewsInvalidTransferMarketId;
            }
            if (string.IsNullOrEmpty(transferMarketNews.Title))
            {
                return ErrorMapping.ErrorCodes.UpdateTransferMarketNewsInvalidTitle;
            }
            transferMarketNews.CreatedBy = WcfExtensions.WcfMessageHeader.Current.ClientUsername;
            var success = TransferMarketNewsDal.Insert(transferMarketNews);
            if (success)
            {
                newsId = transferMarketNews.Id;
                return ErrorMapping.ErrorCodes.Success;
            }
            return ErrorMapping.ErrorCodes.BusinessError;
        }
        public static ErrorMapping.ErrorCodes UpdateTransferMarketNews(TransferMarketNewsEntity transferMarketNews)
        {
            if (transferMarketNews.Id <= 0)
            {
                return ErrorMapping.ErrorCodes.UpdateTransferMarketNewsNotFoundTransferMarketNews;
            }
            var currentTransferMarketNews = TransferMarketNewsDal.GetById(transferMarketNews.Id);
            if (currentTransferMarketNews == null)
            {
                return ErrorMapping.ErrorCodes.UpdateTransferMarketNewsNotFoundTransferMarketNews;
            }
            if (transferMarketNews.TransferMarketId <= 0)
            {
                return ErrorMapping.ErrorCodes.UpdateTransferMarketNewsInvalidTransferMarketId;
            }
            var transferMarket = TransferMarketDal.GetById(transferMarketNews.TransferMarketId);
            if (transferMarket == null)
            {
                return ErrorMapping.ErrorCodes.UpdateTransferMarketNewsInvalidTransferMarketId;
            }
            if (string.IsNullOrEmpty(transferMarketNews.Title))
            {
                return ErrorMapping.ErrorCodes.UpdateTransferMarketNewsInvalidTitle;
            }
            currentTransferMarketNews.Title = transferMarketNews.Title;
            currentTransferMarketNews.SubTitle = transferMarketNews.SubTitle;
            currentTransferMarketNews.LabelType = transferMarketNews.LabelType;
            currentTransferMarketNews.Description = transferMarketNews.Description;
            currentTransferMarketNews.Avatar = transferMarketNews.Avatar;
            currentTransferMarketNews.Avatar2 = transferMarketNews.Avatar2;
            currentTransferMarketNews.Avatar3 = transferMarketNews.Avatar3;
            currentTransferMarketNews.Mode = transferMarketNews.Mode;
            currentTransferMarketNews.Tags = transferMarketNews.Tags;
            currentTransferMarketNews.MetaTitle = transferMarketNews.MetaTitle;
            currentTransferMarketNews.MetaDescription = transferMarketNews.MetaDescription;
            currentTransferMarketNews.LastModifiedBy = WcfExtensions.WcfMessageHeader.Current.ClientUsername;

            return TransferMarketNewsDal.Update(currentTransferMarketNews)
                       ? ErrorMapping.ErrorCodes.Success
                       : ErrorMapping.ErrorCodes.BusinessError;
        }
        public static ErrorMapping.ErrorCodes DeleteTransferMarketNews(long transferMarketNewsId)
        {
            if (transferMarketNewsId <= 0)
            {
                return ErrorMapping.ErrorCodes.UpdateTransferMarketNewsNotFoundTransferMarketNews;
            }
            var currentTransferMarketNews = TransferMarketNewsDal.GetById(transferMarketNewsId);
            if (currentTransferMarketNews == null)
            {
                return ErrorMapping.ErrorCodes.UpdateTransferMarketNewsNotFoundTransferMarketNews;
            }
            return TransferMarketNewsDal.Delete(transferMarketNewsId)
                       ? ErrorMapping.ErrorCodes.Success
                       : ErrorMapping.ErrorCodes.BusinessError;
        }
        public static ErrorMapping.ErrorCodes ApproveTransferMarketNews(long transferMarketNewsId)
        {
            if (transferMarketNewsId <= 0)
            {
                return ErrorMapping.ErrorCodes.UpdateTransferMarketNewsNotFoundTransferMarketNews;
            }
            var currentTransferMarketNews = TransferMarketNewsDal.GetById(transferMarketNewsId);
            if (currentTransferMarketNews == null)
            {
                return ErrorMapping.ErrorCodes.UpdateTransferMarketNewsNotFoundTransferMarketNews;
            }
            return TransferMarketNewsDal.Approve(transferMarketNewsId,
                                                 WcfExtensions.WcfMessageHeader.Current.ClientUsername, DateTime.Now,
                                                 Utility.ConvertToLong(DateTime.Now.ToString("yyyyMMddHHmmssFFF")))
                       ? ErrorMapping.ErrorCodes.Success
                       : ErrorMapping.ErrorCodes.BusinessError;
        }
        public static ErrorMapping.ErrorCodes UnpublishTransferMarketNews(long transferMarketNewsId)
        {
            if (transferMarketNewsId <= 0)
            {
                return ErrorMapping.ErrorCodes.UpdateTransferMarketNewsNotFoundTransferMarketNews;
            }
            var currentTransferMarketNews = TransferMarketNewsDal.GetById(transferMarketNewsId);
            if (currentTransferMarketNews == null)
            {
                return ErrorMapping.ErrorCodes.UpdateTransferMarketNewsNotFoundTransferMarketNews;
            }
            return TransferMarketNewsDal.Unpublish(transferMarketNewsId,
                                                   WcfExtensions.WcfMessageHeader.Current.ClientUsername)
                       ? ErrorMapping.ErrorCodes.Success
                       : ErrorMapping.ErrorCodes.BusinessError;
        }

        #endregion
    }
}
