﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using ChannelVN.CMS.Common;
using ChannelVN.TransferMarket.Dal.Common;
using ChannelVN.TransferMarket.Entity;
using ChannelVN.TransferMarket.MainDal.Databases;

namespace ChannelVN.TransferMarket.Dal
{
    public class TransferMarketNewsDal
    {
        public static List<TransferMarketNewsEntity> Search(string keyword, long transferMarketId, int labelType, int mode, int status, int pageIndex, int pageSize, ref int totalRow)
        {
            List<TransferMarketNewsEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.TransferMarketNewsMainDal.Search(keyword, transferMarketId, labelType, mode, status, pageIndex, pageSize, ref totalRow);
            }
            return returnValue;
        }
        public static TransferMarketNewsEntity GetById(long transferMarketNewsId)
        {
            TransferMarketNewsEntity returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.TransferMarketNewsMainDal.GetById(transferMarketNewsId);
            }
            return returnValue;
        }

        public static bool Insert(TransferMarketNewsEntity transferMarketNews)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.TransferMarketNewsMainDal.Insert(transferMarketNews);
            }
            return returnValue;
        }
        public static bool Update(TransferMarketNewsEntity transferMarketNews)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.TransferMarketNewsMainDal.Update(transferMarketNews);
            }
            return returnValue;
        }
        public static bool Delete(long transferMarketNewsId)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.TransferMarketNewsMainDal.Delete(transferMarketNewsId);
            }
            return returnValue;
        }
        public static bool Approve(long transferMarketNewsId, string approvedBy, DateTime publishedDate, long publishedDateNumber)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.TransferMarketNewsMainDal.Approve(transferMarketNewsId, approvedBy, publishedDate, publishedDateNumber);
            }
            return returnValue;
        }
        public static bool Unpublish(long transferMarketNewsId, string unpublishBy)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.TransferMarketNewsMainDal.Unpublish(transferMarketNewsId, unpublishBy);
            }
            return returnValue;
        }
    }
}
