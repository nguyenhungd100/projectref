﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using ChannelVN.CMS.Common;
using ChannelVN.TransferMarket.Dal.Common;
using ChannelVN.TransferMarket.Entity;
using ChannelVN.TransferMarket.MainDal.Databases;

namespace ChannelVN.TransferMarket.Dal
{
    public class TransferMarketDal
    {
        public static List<TransferMarketEntity> Search(string keyword, int status, int pageIndex, int pageSize, ref int totalRow)
        {
            List<TransferMarketEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.TransferMarketMainDal.Search(keyword, status, pageIndex, pageSize, ref totalRow);
            }
            return returnValue;
        }
        public static TransferMarketEntity GetById(long transferMarketId)
        {
            TransferMarketEntity returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.TransferMarketMainDal.GetById(transferMarketId);
            }
            return returnValue;
        }

        public static bool Insert(TransferMarketEntity transferMarket)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.TransferMarketMainDal.Insert(transferMarket);
            }
            return returnValue;
        }
        public static bool Update(TransferMarketEntity transferMarket)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.TransferMarketMainDal.Update(transferMarket);
            }
            return returnValue;
        }
        public static bool Delete(long transferMarketId)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.TransferMarketMainDal.Delete(transferMarketId);
            }
            return returnValue;
        }
    }
}
