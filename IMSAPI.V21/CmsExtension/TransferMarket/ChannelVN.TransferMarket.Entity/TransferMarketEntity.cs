﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using ChannelVN.CMS.Common;

namespace ChannelVN.TransferMarket.Entity
{
    [DataContract]
    public enum EnumTransferMarketStatus
    {
        [EnumMember]
        AllStatus = -1,
        [EnumMember]
        Hidden = 0,
        [EnumMember]
        Show = 1,
        [EnumMember]
        Closed = 2
    }
    [DataContract]
    public class TransferMarketEntity : EntityBase
    {
        [DataMember]
        public long Id { get; set; }
        [DataMember]
        public string Title { get; set; }
        [DataMember]
        public string Avatar { get; set; }
        [DataMember]
        public DateTime StartedDate { get; set; }
        [DataMember]
        public DateTime ClosedDate { get; set; }
        [DataMember]
        public DateTime CreatedDate { get; set; }
        [DataMember]
        public string CreatedBy { get; set; }
        [DataMember]
        public DateTime LastModifiedDate { get; set; }
        [DataMember]
        public string LastModifiedBy { get; set; }
        [DataMember]
        public int Status { get; set; }
    }
}
