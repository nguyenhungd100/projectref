﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using ChannelVN.CMS.Common;

namespace ChannelVN.TransferMarket.Entity
{
    [DataContract]
    public enum EnumTransferMarketNewsLabelType
    {
        [EnumMember]
        AllLabel = -1,
        [EnumMember]
        NormalNews = 0,
        [EnumMember]
        RumorsNews = 1,
        [EnumMember]
        OfficialNews = 2
    }
    [DataContract]
    public enum EnumTransferMarketNewsStatus
    {
        [EnumMember]
        AllStatus = -1,
        [EnumMember]
        Temporary = 0,
        [EnumMember]
        Published = 1,
        [EnumMember]
        Unpublished = 2
    }
    [DataContract]
    public class TransferMarketNewsEntity : EntityBase
    {
        [DataMember]
        public long Id { get; set; }
        [DataMember]
        public int TransferMarketId { get; set; }
        [DataMember]
        public string Title { get; set; }
        [DataMember]
        public string SubTitle { get; set; }
        [DataMember]
        public int LabelType { get; set; }
        [DataMember]
        public string Description { get; set; }
        [DataMember]
        public string Avatar { get; set; }
        [DataMember]
        public string Avatar2 { get; set; }
        [DataMember]
        public string Avatar3 { get; set; }
        [DataMember]
        public int Mode { get; set; }
        [DataMember]
        public string Tags { get; set; }
        [DataMember]
        public string MetaTitle { get; set; }
        [DataMember]
        public string MetaDescription { get; set; }
        [DataMember]
        public int Priority { get; set; }
        [DataMember]
        public int Dislike { get; set; }
        [DataMember]
        public DateTime CreatedDate { get; set; }
        [DataMember]
        public string CreatedBy { get; set; }
        [DataMember]
        public DateTime LastModifiedDate { get; set; }
        [DataMember]
        public string LastModifiedBy { get; set; }
        [DataMember]
        public DateTime PublishedDate { get; set; }
        [DataMember]
        public long PublishedDateNumber { get; set; }
        [DataMember]
        public string ApprovedBy { get; set; }
        [DataMember]
        public int Status { get; set; }
    }
}
