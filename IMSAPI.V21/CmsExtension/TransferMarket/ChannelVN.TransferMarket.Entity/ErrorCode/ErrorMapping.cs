﻿using System.Runtime.Serialization;
using ChannelVN.CMS.Common;

namespace ChannelVN.TransferMarket.Entity.ErrorCode
{
    [DataContract]
    public class ErrorMapping : BoErrorMappingBase<ErrorMapping.ErrorCodes>
    {
        private static ErrorMapping _current;
        public static ErrorMapping Current
        {
            get { return _current ?? (_current = new ErrorMapping()); }
        }

        protected override void InitErrorMapping()
        {
        }
        [DataContract]
        public enum ErrorCodes
        {
            #region General error

            [EnumMember]
            Success = ErrorCodeBase.Success,
            [EnumMember]
            UnknowError = ErrorCodeBase.UnknowError,
            [EnumMember]
            Exception = ErrorCodeBase.Exception,
            [EnumMember]
            BusinessError = ErrorCodeBase.BusinessError,
            [EnumMember]
            InvalidRequest = ErrorCodeBase.InvalidRequest,
            [EnumMember]
            TimeOutSession = ErrorCodeBase.TimeOutSession,

            #endregion

            UpdateTransferMarketInvalidTitle = 10001,
            UpdateTransferMarketInvalidStartDate = 10002,
            UpdateTransferMarketInvalidEndDate = 10003,
            UpdateTransferMarketNotFoundTransferMarket = 10004,

            UpdateTransferMarketNewsInvalidTransferMarketId = 20001,
            UpdateTransferMarketNewsInvalidTitle = 20002,
            UpdateTransferMarketNewsNotFoundTransferMarketNews = 20003,
        }
    }
}
