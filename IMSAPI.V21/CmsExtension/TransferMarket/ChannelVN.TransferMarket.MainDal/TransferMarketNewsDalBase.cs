﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using ChannelVN.CMS.Common;

using ChannelVN.TransferMarket.Entity;
using ChannelVN.TransferMarket.MainDal.Databases;

namespace ChannelVN.TransferMarket.MainDal
{
    public abstract class TransferMarketNewsDalBase
    {
        public List<TransferMarketNewsEntity> Search(string keyword, long transferMarketId, int labelType, int mode, int status, int pageIndex, int pageSize, ref int totalRow)
        {
            const string commandText = "CMS_TransferMarketNews_Search"; try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Keyword", keyword);
                _db.AddParameter(cmd, "TransferMarketId", transferMarketId);
                _db.AddParameter(cmd, "LabelType", labelType);
                _db.AddParameter(cmd, "Mode", mode);
                _db.AddParameter(cmd, "Status", status);
                _db.AddParameter(cmd, "PageIndex", pageIndex);
                _db.AddParameter(cmd, "PageSize", pageSize);
                _db.AddParameter(cmd, "TotalRow", totalRow, ParameterDirection.Output);
                var numberOfRow = _db.GetList<TransferMarketNewsEntity>(cmd);
                totalRow = Utility.ConvertToInt(cmd.Parameters[cmd.Parameters.Count - 1]);
                return numberOfRow;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public TransferMarketNewsEntity GetById(long transferMarketNewsId)
        {
            const string commandText = "CMS_ProgramChannel_Update"; try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", transferMarketNewsId);
                var numberOfRow = _db.Get<TransferMarketNewsEntity>(cmd);
                return numberOfRow;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public bool Insert(TransferMarketNewsEntity transferMarketNews)
        {
            const string commandText = "CMS_TransferMarketNews_Insert"; try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", transferMarketNews.Id);
                _db.AddParameter(cmd, "TransferMarketId", transferMarketNews.TransferMarketId);
                _db.AddParameter(cmd, "Title", transferMarketNews.Title);
                _db.AddParameter(cmd, "SubTitle", transferMarketNews.SubTitle);
                _db.AddParameter(cmd, "LabelType", transferMarketNews.LabelType);
                _db.AddParameter(cmd, "Description", transferMarketNews.Description);
                _db.AddParameter(cmd, "Avatar", transferMarketNews.Avatar);
                _db.AddParameter(cmd, "Avatar2", transferMarketNews.Avatar2);
                _db.AddParameter(cmd, "Avatar3", transferMarketNews.Avatar3);
                _db.AddParameter(cmd, "Mode", transferMarketNews.Mode);
                _db.AddParameter(cmd, "Tags", transferMarketNews.Tags);
                _db.AddParameter(cmd, "MetaTitle", transferMarketNews.MetaTitle);
                _db.AddParameter(cmd, "MetaDescription", transferMarketNews.MetaDescription);
                _db.AddParameter(cmd, "CreatedBy", transferMarketNews.CreatedBy);
                var numberOfRow = _db.ExecuteNonQuery(cmd);
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool Update(TransferMarketNewsEntity transferMarketNews)
        {
            const string commandText = "CMS_TransferMarketNews_Update"; try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", transferMarketNews.Id);
                _db.AddParameter(cmd, "TransferMarketId", transferMarketNews.TransferMarketId);
                _db.AddParameter(cmd, "Title", transferMarketNews.Title);
                _db.AddParameter(cmd, "SubTitle", transferMarketNews.SubTitle);
                _db.AddParameter(cmd, "LabelType", transferMarketNews.LabelType);
                _db.AddParameter(cmd, "Description", transferMarketNews.Description);
                _db.AddParameter(cmd, "Avatar", transferMarketNews.Avatar);
                _db.AddParameter(cmd, "Avatar2", transferMarketNews.Avatar2);
                _db.AddParameter(cmd, "Avatar3", transferMarketNews.Avatar3);
                _db.AddParameter(cmd, "Mode", transferMarketNews.Mode);
                _db.AddParameter(cmd, "Tags", transferMarketNews.Tags);
                _db.AddParameter(cmd, "MetaTitle", transferMarketNews.MetaTitle);
                _db.AddParameter(cmd, "MetaDescription", transferMarketNews.MetaDescription);
                _db.AddParameter(cmd, "LastModifiedBy", transferMarketNews.LastModifiedBy);
                var numberOfRow = _db.ExecuteNonQuery(cmd);
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool Delete(long transferMarketNewsId)
        {
            const string commandText = "CMS_TransferMarketNews_Delete"; try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", transferMarketNewsId);
                var numberOfRow = _db.ExecuteNonQuery(cmd);
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool Approve(long transferMarketNewsId, string approvedBy, DateTime publishedDate, long publishedDateNumber)
        {
            const string commandText = "CMS_TransferMarketNews_Approve"; try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", transferMarketNewsId);
                _db.AddParameter(cmd, "ApprovedBy", approvedBy);
                _db.AddParameter(cmd, "PublishedDate", publishedDate);
                _db.AddParameter(cmd, "PublishedDateNumber", publishedDateNumber);
                var numberOfRow = _db.ExecuteNonQuery(cmd);
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool Unpublish(long transferMarketNewsId, string unpublishBy)
        {
            const string commandText = "CMS_TransferMarketNews_Unpublish"; try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", transferMarketNewsId);
                _db.AddParameter(cmd, "UnpublishedBy", unpublishBy);
                var numberOfRow = _db.ExecuteNonQuery(cmd);
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        #region Core members

        private readonly CmsMainDb _db;

        protected TransferMarketNewsDalBase(CmsMainDb db)
        {
            _db = db;
        }

        protected CmsMainDb Database
        {
            get { return _db; }
        }

        #endregion
    }
}
