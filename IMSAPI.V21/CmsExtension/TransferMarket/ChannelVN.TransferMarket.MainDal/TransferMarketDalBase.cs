﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using ChannelVN.CMS.Common;

using ChannelVN.TransferMarket.Entity;
using ChannelVN.TransferMarket.MainDal.Common;
using ChannelVN.TransferMarket.MainDal.Databases;

namespace ChannelVN.TransferMarket.MainDal
{
    public abstract class TransferMarketDalBase
    {
        public List<TransferMarketEntity> Search(string keyword, int status, int pageIndex, int pageSize, ref int totalRow)
        {
            const string commandText = "CMS_TransferMarket_Search"; try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Keyword", keyword);
                _db.AddParameter(cmd, "Status", status);
                _db.AddParameter(cmd, "PageIndex", pageIndex);
                _db.AddParameter(cmd, "PageSize", pageSize);
                _db.AddParameter(cmd, "TotalRow", totalRow, ParameterDirection.Output);
                var numberOfRow = _db.GetList<TransferMarketEntity>(cmd);
                totalRow = Utility.ConvertToInt(cmd.Parameters[cmd.Parameters.Count - 1]);
                return numberOfRow;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public TransferMarketEntity GetById(long transferMarketId)
        {
            const string commandText = "CMS_TransferMarket_GetById"; try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", transferMarketId);
                var numberOfRow = _db.Get<TransferMarketEntity>(cmd);
                return numberOfRow;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public bool Insert(TransferMarketEntity transferMarket)
        {
            const string commandText = "CMS_TransferMarket_Insert"; try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Title", transferMarket.Title);
                _db.AddParameter(cmd, "Avatar", transferMarket.Avatar);
                if (transferMarket.StartedDate <= Constants.MinDateTime)
                    _db.AddParameter(cmd, "StartedDate", DBNull.Value);
                else _db.AddParameter(cmd, "StartedDate", transferMarket.StartedDate);
                if (transferMarket.ClosedDate <= Constants.MinDateTime)
                    _db.AddParameter(cmd, "ClosedDate", DBNull.Value);
                else _db.AddParameter(cmd, "ClosedDate", transferMarket.ClosedDate);
                _db.AddParameter(cmd, "CreatedBy", transferMarket.CreatedBy);
                _db.AddParameter(cmd, "Status", transferMarket.Status);
                var numberOfRow = _db.ExecuteNonQuery(cmd);
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool Update(TransferMarketEntity transferMarket)
        {
            const string commandText = "CMS_TransferMarket_Update"; try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", transferMarket.Id);
                _db.AddParameter(cmd, "Title", transferMarket.Title);
                _db.AddParameter(cmd, "Avatar", transferMarket.Avatar);
                if (transferMarket.StartedDate <= Constants.MinDateTime)
                    _db.AddParameter(cmd, "StartedDate", DBNull.Value);
                else _db.AddParameter(cmd, "StartedDate", transferMarket.StartedDate);
                if (transferMarket.ClosedDate <= Constants.MinDateTime)
                    _db.AddParameter(cmd, "ClosedDate", DBNull.Value);
                else _db.AddParameter(cmd, "ClosedDate", transferMarket.ClosedDate);
                _db.AddParameter(cmd, "LastModifiedBy", transferMarket.LastModifiedBy);
                _db.AddParameter(cmd, "Status", transferMarket.Status);
                var numberOfRow = _db.ExecuteNonQuery(cmd);
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool Delete(long transferMarketId)
        {
            const string commandText = "CMS_TransferMarket_Delete"; try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", transferMarketId);
                var numberOfRow = _db.ExecuteNonQuery(cmd);
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        #region Core members

        private readonly CmsMainDb _db;

        protected TransferMarketDalBase(CmsMainDb db)
        {
            _db = db;
        }

        protected CmsMainDb Database
        {
            get { return _db; }
        }

        #endregion
    }
}
