﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ChannelVN.CMS.Common;
using ChannelVN.StreamMobile.Entity;
using ChannelVN.StreamMobile.MainDal.Common;
using ChannelVN.StreamMobile.MainDal.Databases;

namespace ChannelVN.StreamMobile.MainDal
{
    public abstract class NewsDalBase
    {
        public void UpdateOffTime_NewsMobileStream(int TotalSize, string fromDate, string toDate)
        {
            const string commandText = "CMS_NewsMobileStream_UpdateOffTime";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "TotalSize", TotalSize);
                _db.AddParameter(cmd, "FromDate", fromDate);
                _db.AddParameter(cmd, "ToDate", toDate);
                _db.ExecuteNonQuery(cmd);
               }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        #region Core members

        private readonly CmsMainDb _db;

        protected NewsDalBase(CmsMainDb db)
        {
            _db = db;
        }

        protected CmsMainDb Database
        {
            get { return _db; }
        }

        #endregion

    }
}
