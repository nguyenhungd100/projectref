﻿using ChannelVN.CMS.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ChannelVN.StreamMobile.Entity
{
    public class NewsEntity : EntityBase
    {
        public long Id { get; set; }
        public string Title { get; set; }
        public string SubTitle { get; set; }
        public string Sapo { get; set; }
        public string Body { get; set; }
        public string Avatar { get; set; }
        public string AvatarDesc { get; set; }
        public string Avatar2 { get; set; }
        public string Avatar3 { get; set; }
        public string Avatar4 { get; set; }
        public string Avatar5 { get; set; }
        public string Author { get; set; }
        public string NewsRelation { get; set; }
        public int Status { get; set; }
        public string Source { get; set; }
        public bool IsFocus { get; set; }
        public byte Type { get; set; }
        public int ThreadId { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime LastModifiedDate { get; set; }
        public DateTime DistributionDate { get; set; }
        public string CreatedBy { get; set; }
        public string LastModifiedBy { get; set; }
        public string PublishedBy { get; set; }
        public string EditedBy { get; set; }
        public string LastReceiver { get; set; }
        public int WordCount { get; set; }
        public int ViewCount { get; set; }
        public byte Priority { get; set; }
        public string Tag { get; set; }
        public string Note { get; set; }
        public string TagPrimary { get; set; }
        public decimal Price { get; set; }
        public int DisplayStyle { get; set; }
        public int DisplayPosition { get; set; }
        public int DisplayInSlide { get; set; }
        public string AvatarCustom { get; set; }
        public int OriginalId { get; set; }
        public byte NewsType { get; set; }
        public bool IsOnHome { get; set; }
        public string Url { get; set; }
        public string NoteRoyalties { get; set; }
        public string TagItem { get; set; }
        public int NewsCategory { get; set; }
        public string InitSapo { get; set; }
        public string TemplateName { get; set; }
        public string TemplateConfig { get; set; }
        public int InterviewId { get; set; }
        public bool IsBreakingNews { get; set; }
        public string OriginalUrl { get; set; }
        public bool IsPr { get; set; }
        public bool AdStore { get; set; }
        public string AdStoreUrl { get; set; }
        public string PrBookingNumber { get; set; }
        public int PegaBreakingNews { get; set; }
        public int RollingNewsId { get; set; }
        public bool IsOnMobile { get; set; }
        public int TagSubTitleId { get; set; }
        public int PrPosition { get; set; }
        public int LocationType { get; set; }
        public DateTime ExpiredDate { get; set; }
        public string SourceUrl { get; set; }
        public decimal BonusPrice { get; set; }
        public int ViewCountMobile { get; set; }
        public long OriginalNewsId { get; set; }
        public int PhotoCount { get; set; }
        public int VideoCount { get; set; }
    }
}
