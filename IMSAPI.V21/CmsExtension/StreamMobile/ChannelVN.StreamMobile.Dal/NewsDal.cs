﻿using ChannelVN.CMS.Common;
using ChannelVN.StreamMobile.Dal.Common;
using ChannelVN.StreamMobile.Entity;
using ChannelVN.StreamMobile.MainDal.Databases;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace ChannelVN.StreamMobile.Dal
{
    public class NewsDal
    {
        public static void UpdateOffTime_NewsMobileStream(int TotalSize, string fromDate, string toDate)
        {
            using (var db = new CmsMainDb())
            {
                db.NewsMainDal.UpdateOffTime_NewsMobileStream(TotalSize, fromDate, toDate);
            }
        }
    }
}
