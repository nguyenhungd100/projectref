﻿using ChannelVN.StreamMobile.Dal;
using System;
using ChannelVN.StreamMobile.Entity.ErrorCode;

namespace ChannelVN.StreamMobile.Bo
{
    public class NewsBo
    {
        public static void UpdateOffTime_NewsMobileStream(int TotalSize, string fromDate, string toDate)
        {
            NewsDal.UpdateOffTime_NewsMobileStream(TotalSize,fromDate,toDate);
        }

    }
}
