﻿using System;
using System.Runtime.Serialization;
using ChannelVN.CMS.Common;

namespace ChannelVN.TheGuide.Entity
{
    //public enum EnumBreakingNewsPromotionStatus
    //{
    //    [EnumMember]
    //    Short = 1,
    //    [EnumMember]
    //    Event = 2,
    //    [EnumMember]
    //    Promotion = 3
    //}
    [DataContract]
    public class BreakingNewsPromotionEntity : EntityBase
    {
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public int BreakingNewsId { get; set; }
        [DataMember]
        public int LocationId { get; set; }
        [DataMember]
        public int BusinessLocationId { get; set; }
        [DataMember]
        public int PromotionTypeId { get; set; }
        [DataMember]
        public DateTime FormDate { get; set; }
        [DataMember]
        public DateTime ToDate { get; set; }
        [DataMember]
        public decimal Price { get; set; }
        [DataMember]
        public string PercentageDiscount { get; set; }
    }

    [DataContract]
    public class BreakingNewsPromotionBusinessLocationEntity : EntityBase
    {
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public int BreakingNewsLocationId { get; set; }
        [DataMember]
        public string Name { get; set; }
        [DataMember]
        public DateTime CreatedDate { get; set; }
        [DataMember]
        public string CreatedBy { get; set; }
        [DataMember]
        public DateTime LastModifiedDate { get; set; }
        [DataMember]
        public string LastModifiedBy { get; set; }
        [DataMember]
        public int Status { get; set; }
        [DataMember]
        public int Priority { get; set; }

    }

    [DataContract]
    public class BreakingNewsPromotionLocationEntity : EntityBase
    {
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public string Name { get; set; }
        [DataMember]
        public DateTime CreatedDate { get; set; }
        [DataMember]
        public string CreatedBy { get; set; }
        [DataMember]
        public DateTime LastModifiedDate { get; set; }
        [DataMember]
        public string LastModifiedBy { get; set; }
        [DataMember]
        public int Status { get; set; }
        [DataMember]
        public int Priority { get; set; }

    }

    [DataContract]
    public class BreakingNewsPromotionTypeEntity : EntityBase
    {
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public string Name { get; set; }
        [DataMember]
        public DateTime CreatedDate { get; set; }
        [DataMember]
        public string CreatedBy { get; set; }
        [DataMember]
        public DateTime LastModifiedDate { get; set; }
        [DataMember]
        public string LastModifiedBy { get; set; }
        [DataMember]
        public int Status { get; set; }
        [DataMember]
        public int Priority { get; set; }

    }
}
