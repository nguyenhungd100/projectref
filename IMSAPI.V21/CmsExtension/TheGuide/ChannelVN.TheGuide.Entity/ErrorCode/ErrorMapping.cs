﻿using System.Runtime.Serialization;
using ChannelVN.CMS.Common;

namespace ChannelVN.TheGuide.Entity.ErrorCode
{
    [DataContract]
    public class ErrorMapping : BoErrorMappingBase<ErrorMapping.ErrorCodes>
    {
        private static ErrorMapping _current;
        public static ErrorMapping Current
        {
            get { return _current ?? (_current = new ErrorMapping()); }
        }

        protected override void InitErrorMapping()
        {
        }
        [DataContract]
        public enum ErrorCodes
        {
            #region General error

            [EnumMember]
            Success = ErrorCodeBase.Success,
            [EnumMember]
            UnknowError = ErrorCodeBase.UnknowError,
            [EnumMember]
            Exception = ErrorCodeBase.Exception,
            [EnumMember]
            BusinessError = ErrorCodeBase.BusinessError,
            [EnumMember]
            InvalidRequest = ErrorCodeBase.InvalidRequest,
            [EnumMember]
            TimeOutSession = ErrorCodeBase.TimeOutSession,

            #endregion

            UpdateTheGuideInvalidTitle = 10001,
            UpdateTheGuideInvalidStartDate = 10002,
            UpdateTheGuideInvalidEndDate = 10003,
            UpdateTheGuideNotFoundTheGuide = 10004,

            UpdateTheGuideNewsInvalidTheGuideId = 20001,
            UpdateTheGuideNewsInvalidTitle = 20002,
            UpdateTheGuideNewsNotFoundTheGuideNews = 20003,
        }
    }
}
