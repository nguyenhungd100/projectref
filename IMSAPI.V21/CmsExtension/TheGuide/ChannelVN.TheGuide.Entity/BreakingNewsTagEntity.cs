﻿using System;
using System.Runtime.Serialization;
using ChannelVN.CMS.Common;

namespace ChannelVN.TheGuide.Entity
{
    [DataContract]
    public class BreakingNewsTagWithTagInfoEntity : EntityBase
    {
        [DataMember]
        public long TagId { get; set; }
        [DataMember]
        public long BreakingNewsId { get; set; }
        [DataMember]
        public Int16 TagMode { get; set; }
        [DataMember]
        public string TagProperty { get; set; }
        [DataMember]
        public int Priority { get; set; }
        [DataMember]
        public string Name { get; set; }
        [DataMember]
        public string Url { get; set; }
    }
}
