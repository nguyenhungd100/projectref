﻿using System;
using System.Runtime.Serialization;
using ChannelVN.CMS.Common;

namespace ChannelVN.TheGuide.Entity
{
    [DataContract]
    public enum EnumBreakingNewsType
    {
        [EnumMember]
        All = -1,
        [EnumMember]
        Normal = 1,
        [EnumMember]
        Event = 2,
        [EnumMember]
        Promotion = 3,
    }
    [DataContract]
    public enum EnumBreakingNewsStatus
    {
        [EnumMember]
        All = -1,
        [EnumMember]
        Temporary = 1,
        [EnumMember]
        Published = 2,
        [EnumMember]
        Unpublished = 3,
    }
    [DataContract]
    public class BreakingNewsEntity : EntityBase
    {
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public int Type { get; set; }
        [DataMember]
        public int ZoneId { get; set; }
        [DataMember]
        public string Title { get; set; }
        [DataMember]
        public string SubTitle { get; set; }
        [DataMember]
        public string Body { get; set; }
        [DataMember]
        public string Avatar { get; set; }
        [DataMember]
        public string Avatar2 { get; set; }
        [DataMember]
        public string AvatarDesc { get; set; }
        [DataMember]
        public string Author { get; set; }
        [DataMember]
        public DateTime DistributionDate { get; set; }
        [DataMember]
        public DateTime CreatedDate { get; set; }
        [DataMember]
        public string CreatedBy { get; set; }
        [DataMember]
        public DateTime LastModifiedDate { get; set; }
        [DataMember]
        public string LastModifiedBy { get; set; }
        [DataMember]
        public DateTime PublishedDate { get; set; }
        [DataMember]
        public string PublishedBy { get; set; }
        [DataMember]
        public int Status { get; set; }
    }

    [DataContract]
    public class BreakingNewsLocationEntity : EntityBase
    {
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public string Name { get; set; }
        [DataMember]
        public DateTime CreatedDate { get; set; }
        [DataMember]
        public string CreatedBy { get; set; }
        [DataMember]
        public DateTime LastModifiedDate { get; set; }
        [DataMember]
        public string LastModifiedBy { get; set; }
        [DataMember]
        public int Status { get; set; }
        [DataMember]
        public int Priority { get; set; }
    }
}
