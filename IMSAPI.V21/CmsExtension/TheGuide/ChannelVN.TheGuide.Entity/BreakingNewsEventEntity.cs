﻿using System;
using System.Runtime.Serialization;
using ChannelVN.CMS.Common;

namespace ChannelVN.TheGuide.Entity
{

    //[DataContract]
    //public enum EnumBreakingNewsEventStatus
    //{
    //    [EnumMember]
    //    Short = 1,
    //    [EnumMember]
    //    Event = 2,
    //    [EnumMember]
    //    Promotion = 3
    //}
    [DataContract]
    public class BreakingNewsEventEntity : EntityBase
    {
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public int BreakingNewsId { get; set; }
        [DataMember]
        public int EventTypeId { get; set; }
        [DataMember]
        public int EventLocationId { get; set; }
        [DataMember]
        public DateTime FormDate { get; set; }
        [DataMember]
        public DateTime ToDate { get; set; }

    }

    [DataContract]
    public class BreakingNewsEventLocationEntity : EntityBase
    {
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public int BreakingNewsLocationId { get; set; }
        [DataMember]
        public string Name { get; set; }
        [DataMember]
        public DateTime CreatedDate { get; set; }
        [DataMember]
        public string CreatedBy { get; set; }
        [DataMember]
        public DateTime LastModifiedDate { get; set; }
        [DataMember]
        public string LastModifiedBy { get; set; }
        [DataMember]
        public int Status { get; set; }
        [DataMember]
        public int Priority { get; set; }

    }

    [DataContract]
    public class BreakingNewsEventTypeEntity : EntityBase
    {
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public string Name { get; set; }
        [DataMember]
        public DateTime CreatedDate { get; set; }
        [DataMember]
        public string CreatedBy { get; set; }
        [DataMember]
        public DateTime LastModifiedDate { get; set; }
        [DataMember]
        public string LastModifiedBy { get; set; }
        [DataMember]
        public int Status { get; set; }
        [DataMember]
        public int Priority { get; set; }

    }

}
