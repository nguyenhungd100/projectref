﻿using ChannelVN.TheGuide.Bo.Common;
using ChannelVN.TheGuide.Dal;
using ChannelVN.TheGuide.Entity;
using System;
using System.Collections.Generic;
using ChannelVN.TheGuide.Entity.ErrorCode;

namespace ChannelVN.TheGuide.Bo
{
    public class BreakingNewsEventBo
    {
        public static BreakingNewsEventEntity GetById(int id)
        {
            return BreakingNewsEventDal.BreakingNewsEventGetById(id);
        }
        public static BreakingNewsEventEntity GetByBreakingNewsId(int id)
        {
            return BreakingNewsEventDal.BreakingNewsEventGetByBreakingNewsId(id);
        }
        public static ErrorMapping.ErrorCodes Insert(BreakingNewsEventEntity breakingNewsEventEntity)
        {
            return BreakingNewsEventDal.BreakingNewsEventInsert(breakingNewsEventEntity)
                       ? ErrorMapping.ErrorCodes.Success
                       : ErrorMapping.ErrorCodes.BusinessError;
        }
        public static ErrorMapping.ErrorCodes Update(BreakingNewsEventEntity breakingNewsEventEntity)
        {
            return BreakingNewsEventDal.BreakingNewsEventUpdate(breakingNewsEventEntity)
                       ? ErrorMapping.ErrorCodes.Success
                       : ErrorMapping.ErrorCodes.BusinessError;
        }

        #region BreakingNewsEventLocation
        public static List<BreakingNewsEventLocationEntity> BreakingNewsEventLocationGetAll(string keyword, int status, int breakingNewsLocationId)
        {
            return BreakingNewsEventDal.BreakingNewsEventLocationGetAll( keyword,  status,breakingNewsLocationId);
        }
        public static BreakingNewsEventLocationEntity BreakingNewsEventLocationGetById(int id)
        {
            return BreakingNewsEventDal.BreakingNewsEventLocationGetById(id);
        }
        public static ErrorMapping.ErrorCodes BreakingNewsEventLocationInsert(BreakingNewsEventLocationEntity breakingNewsEventLocation)
        {
            return BreakingNewsEventDal.BreakingNewsEventLocationInsert(breakingNewsEventLocation)
                       ? ErrorMapping.ErrorCodes.Success
                       : ErrorMapping.ErrorCodes.BusinessError;
        }
        public static ErrorMapping.ErrorCodes BreakingNewsEventLocationUpdate(BreakingNewsEventLocationEntity breakingNewsEventLocation)
        {
            return BreakingNewsEventDal.BreakingNewsEventLocationUpdate(breakingNewsEventLocation)
                       ? ErrorMapping.ErrorCodes.Success
                       : ErrorMapping.ErrorCodes.BusinessError;
        }
        public static ErrorMapping.ErrorCodes BreakingNewsEventLocationDelete(int id)
        {
            return BreakingNewsEventDal.BreakingNewsEventLocationDelete(id)
                       ? ErrorMapping.ErrorCodes.Success
                       : ErrorMapping.ErrorCodes.BusinessError;
        }

        public static ErrorMapping.ErrorCodes BreakingNewsEventLocationMoveUp(int id)
        {
            return BreakingNewsEventDal.BreakingNewsEventLocationMoveUp(id)
                       ? ErrorMapping.ErrorCodes.Success
                       : ErrorMapping.ErrorCodes.BusinessError;
        }
        public static ErrorMapping.ErrorCodes BreakingNewsEventLocationMoveDown(int id)
        {
            return BreakingNewsEventDal.BreakingNewsEventLocationMoveDown(id)
                       ? ErrorMapping.ErrorCodes.Success
                       : ErrorMapping.ErrorCodes.BusinessError;
        }
        #endregion

        #region BreakingNewsEventType
        public static List<BreakingNewsEventTypeEntity> BreakingNewsEventTypeGetAll(string keyword, int status)
        {
            return BreakingNewsEventDal.BreakingNewsEventTypeGetAll(keyword, status);
        }
        public static BreakingNewsEventTypeEntity BreakingNewsEventTypeGetById(int id)
        {
            return BreakingNewsEventDal.BreakingNewsEventTypeGetById(id);
        }
        public static ErrorMapping.ErrorCodes BreakingNewsEventTypeInsert(BreakingNewsEventTypeEntity breakingNewsEventType)
        {
            return BreakingNewsEventDal.BreakingNewsEventTypeInsert(breakingNewsEventType)
                       ? ErrorMapping.ErrorCodes.Success
                       : ErrorMapping.ErrorCodes.BusinessError;
        }
        public static ErrorMapping.ErrorCodes BreakingNewsEventTypeUpdate(BreakingNewsEventTypeEntity breakingNewsEventType)
        {
            return BreakingNewsEventDal.BreakingNewsEventTypeUpdate(breakingNewsEventType)
                       ? ErrorMapping.ErrorCodes.Success
                       : ErrorMapping.ErrorCodes.BusinessError;
        }
        public static ErrorMapping.ErrorCodes BreakingNewsEventTypeDelete(int id)
        {
            return BreakingNewsEventDal.BreakingNewsEventTypeDelete(id)
                       ? ErrorMapping.ErrorCodes.Success
                       : ErrorMapping.ErrorCodes.BusinessError;
        }
        public static ErrorMapping.ErrorCodes BreakingNewsEventTypeMoveUp(int id)
        {
            return BreakingNewsEventDal.BreakingNewsEventTypeMoveUp(id)
                       ? ErrorMapping.ErrorCodes.Success
                       : ErrorMapping.ErrorCodes.BusinessError;
        }
        public static ErrorMapping.ErrorCodes BreakingNewsEventTypeMoveDown(int id)
        {
            return BreakingNewsEventDal.BreakingNewsEventTypeMoveDown(id)
                       ? ErrorMapping.ErrorCodes.Success
                       : ErrorMapping.ErrorCodes.BusinessError;
        }
        #endregion
    }
}
