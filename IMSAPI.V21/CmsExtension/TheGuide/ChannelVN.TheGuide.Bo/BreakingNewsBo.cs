﻿using ChannelVN.TheGuide.Dal;
using ChannelVN.TheGuide.Entity;
using System.Collections.Generic;
using ChannelVN.TheGuide.Entity.ErrorCode;

namespace ChannelVN.TheGuide.Bo
{
    public class BreakingNewsBo
    {


        public static List<BreakingNewsEntity> Search(string keyword, EnumBreakingNewsStatus status, EnumBreakingNewsType type, int zoneId, int pageIndex, int pageSize, ref int totalRow)
        {
            return BreakingNewsDal.BreakingNewsSearch(keyword, (int)status, (int)type, zoneId, pageIndex, pageSize, ref totalRow);
        }
        public static BreakingNewsEntity GetById(int id)
        {
            return BreakingNewsDal.BreakingNewsGetById(id);
        }
        public static ErrorMapping.ErrorCodes Insert(BreakingNewsEntity breakingNewsEntity, string tagIdList, ref int id)
        {
            return BreakingNewsDal.BreakingNewsInsert(breakingNewsEntity, tagIdList, ref id)
                       ? ErrorMapping.ErrorCodes.Success
                       : ErrorMapping.ErrorCodes.BusinessError;
        }
        public static ErrorMapping.ErrorCodes Update(BreakingNewsEntity breakingNewsEntity, string tagIdList)
        {
            return BreakingNewsDal.BreakingNewsUpdate(breakingNewsEntity, tagIdList)
                       ? ErrorMapping.ErrorCodes.Success
                       : ErrorMapping.ErrorCodes.BusinessError;
        }
        public static ErrorMapping.ErrorCodes Delete(int id)
        {
            return BreakingNewsDal.BreakingNewsDelete(id)
                       ? ErrorMapping.ErrorCodes.Success
                       : ErrorMapping.ErrorCodes.BusinessError;
        }


        #region BreakingNewsLocation
        public static List<BreakingNewsLocationEntity> BreakingNewsLocationGetAll(string keyword, int status)
        {
            return BreakingNewsDal.BreakingNewsLocationGetAll(keyword, status);
        }
        public static BreakingNewsLocationEntity BreakingNewsLocationGetById(int id)
        {
            return BreakingNewsDal.BreakingNewsLocationGetById(id);
        }
        public static ErrorMapping.ErrorCodes BreakingNewsLocationInsert(BreakingNewsLocationEntity BreakingNewsLocation)
        {
            return BreakingNewsDal.BreakingNewsLocationInsert(BreakingNewsLocation)
                       ? ErrorMapping.ErrorCodes.Success
                       : ErrorMapping.ErrorCodes.BusinessError;
        }
        public static ErrorMapping.ErrorCodes BreakingNewsLocationUpdate(BreakingNewsLocationEntity BreakingNewsLocation)
        {
            return BreakingNewsDal.BreakingNewsLocationUpdate(BreakingNewsLocation)
                       ? ErrorMapping.ErrorCodes.Success
                       : ErrorMapping.ErrorCodes.BusinessError;
        }
        public static ErrorMapping.ErrorCodes BreakingNewsLocationDelete(int id)
        {
            return BreakingNewsDal.BreakingNewsLocationDelete(id)
                       ? ErrorMapping.ErrorCodes.Success
                       : ErrorMapping.ErrorCodes.BusinessError;
        }
        public static ErrorMapping.ErrorCodes BreakingNewsLocationMoveUp(int id)
        {
            return BreakingNewsDal.BreakingNewsLocationMoveUp(id)
                       ? ErrorMapping.ErrorCodes.Success
                       : ErrorMapping.ErrorCodes.BusinessError;
        }
        public static ErrorMapping.ErrorCodes BreakingNewsLocationMoveDown(int id)
        {
            return BreakingNewsDal.BreakingNewsLocationMoveDown(id)
                       ? ErrorMapping.ErrorCodes.Success
                       : ErrorMapping.ErrorCodes.BusinessError;
        }
        #endregion
    }
}
