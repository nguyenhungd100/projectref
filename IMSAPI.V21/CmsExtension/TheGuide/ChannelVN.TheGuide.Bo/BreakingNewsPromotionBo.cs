﻿using ChannelVN.TheGuide.Bo.Common;
using ChannelVN.TheGuide.Dal;
using ChannelVN.TheGuide.Entity;
using System;
using System.Collections.Generic;
using ChannelVN.TheGuide.Entity.ErrorCode;

namespace ChannelVN.TheGuide.Bo
{
    public class BreakingNewsPromotionBo
    {
        public static BreakingNewsPromotionEntity GetById(int id)
        {
            return BreakingNewsPromotionDal.BreakingNewsPromotionGetById(id);
        }
        public static BreakingNewsPromotionEntity GetByBreakingNewsId(int id)
        {
            return BreakingNewsPromotionDal.BreakingNewsPromotionGetByBreakingNewsId(id);
        }
        public static ErrorMapping.ErrorCodes Insert(BreakingNewsPromotionEntity breakingNewsPromotionEntity)
        {
            return BreakingNewsPromotionDal.BreakingNewsPromotionInsert(breakingNewsPromotionEntity)
                       ? ErrorMapping.ErrorCodes.Success
                       : ErrorMapping.ErrorCodes.BusinessError;
        }
        public static ErrorMapping.ErrorCodes Update(BreakingNewsPromotionEntity breakingNewsPromotionEntity)
        {
            return BreakingNewsPromotionDal.BreakingNewsPromotionUpdate(breakingNewsPromotionEntity)
                       ? ErrorMapping.ErrorCodes.Success
                       : ErrorMapping.ErrorCodes.BusinessError;
        }


        #region BreakingNewsPromotionBusinessLocation
        public static List<BreakingNewsPromotionBusinessLocationEntity> BreakingNewsPromotionBusinessLocationGetAll(string keyword, int status, int breakingNewsLocationId)
        {
            return BreakingNewsPromotionDal.BreakingNewsPromotionBusinessLocationGetAll(keyword, status,  breakingNewsLocationId);
        }
        public static BreakingNewsPromotionBusinessLocationEntity BreakingNewsPromotionBusinessLocationGetById(int id)
        {
            return BreakingNewsPromotionDal.BreakingNewsPromotionBusinessLocationGetById(id);
        }
        public static ErrorMapping.ErrorCodes BreakingNewsPromotionBusinessLocationInsert(BreakingNewsPromotionBusinessLocationEntity breakingNewsPromotionBusinessLocation)
        {
            return BreakingNewsPromotionDal.BreakingNewsPromotionBusinessLocationInsert(breakingNewsPromotionBusinessLocation)
                       ? ErrorMapping.ErrorCodes.Success
                       : ErrorMapping.ErrorCodes.BusinessError;
        }
        public static ErrorMapping.ErrorCodes BreakingNewsPromotionBusinessLocationUpdate(BreakingNewsPromotionBusinessLocationEntity breakingNewsPromotionBusinessLocation)
        {
            return BreakingNewsPromotionDal.BreakingNewsPromotionBusinessLocationUpdate(breakingNewsPromotionBusinessLocation)
                       ? ErrorMapping.ErrorCodes.Success
                       : ErrorMapping.ErrorCodes.BusinessError;
        }
        public static ErrorMapping.ErrorCodes BreakingNewsPromotionBusinessLocationDelete(int id)
        {
            return BreakingNewsPromotionDal.BreakingNewsPromotionBusinessLocationDelete(id)
                       ? ErrorMapping.ErrorCodes.Success
                       : ErrorMapping.ErrorCodes.BusinessError;
        }

        public static ErrorMapping.ErrorCodes BreakingNewsPromotionBusinessLocationMoveUp(int id)
        {
            return BreakingNewsPromotionDal.BreakingNewsPromotionBusinessLocationMoveUp(id)
                       ? ErrorMapping.ErrorCodes.Success
                       : ErrorMapping.ErrorCodes.BusinessError;
        }
        public static ErrorMapping.ErrorCodes BreakingNewsPromotionBusinessLocationMoveDown(int id)
        {
            return BreakingNewsPromotionDal.BreakingNewsPromotionBusinessLocationMoveDown(id)
                       ? ErrorMapping.ErrorCodes.Success
                       : ErrorMapping.ErrorCodes.BusinessError;
        }
        #endregion

        #region BreakingNewsPromotionLocation
        public static List<BreakingNewsPromotionLocationEntity> BreakingNewsPromotionLocationGetAll(string keyword, int status)
        {
            return BreakingNewsPromotionDal.BreakingNewsPromotionLocationGetAll(keyword, status);
        }
        public static BreakingNewsPromotionLocationEntity BreakingNewsPromotionLocationGetById(int id)
        {
            return BreakingNewsPromotionDal.BreakingNewsPromotionLocationGetById(id);
        }
        public static ErrorMapping.ErrorCodes BreakingNewsPromotionLocationInsert(BreakingNewsPromotionLocationEntity breakingNewsPromotionLocation)
        {
            return BreakingNewsPromotionDal.BreakingNewsPromotionLocationInsert(breakingNewsPromotionLocation)
                       ? ErrorMapping.ErrorCodes.Success
                       : ErrorMapping.ErrorCodes.BusinessError;
        }
        public static ErrorMapping.ErrorCodes BreakingNewsPromotionLocationUpdate(BreakingNewsPromotionLocationEntity breakingNewsPromotionLocation)
        {
            return BreakingNewsPromotionDal.BreakingNewsPromotionLocationUpdate(breakingNewsPromotionLocation)
                       ? ErrorMapping.ErrorCodes.Success
                       : ErrorMapping.ErrorCodes.BusinessError;
        }
        public static ErrorMapping.ErrorCodes BreakingNewsPromotionLocationDelete(int id)
        {
            return BreakingNewsPromotionDal.BreakingNewsPromotionLocationDelete(id)
                       ? ErrorMapping.ErrorCodes.Success
                       : ErrorMapping.ErrorCodes.BusinessError;
        }
        public static ErrorMapping.ErrorCodes BreakingNewsPromotionLocationMoveUp(int id)
        {
            return BreakingNewsPromotionDal.BreakingNewsPromotionLocationMoveUp(id)
                       ? ErrorMapping.ErrorCodes.Success
                       : ErrorMapping.ErrorCodes.BusinessError;
        }
        public static ErrorMapping.ErrorCodes BreakingNewsPromotionLocationMoveDown(int id)
        {
            return BreakingNewsPromotionDal.BreakingNewsPromotionLocationMoveDown(id)
                       ? ErrorMapping.ErrorCodes.Success
                       : ErrorMapping.ErrorCodes.BusinessError;
        }
        #endregion

        #region BreakingNewsPromotionType
        public static List<BreakingNewsPromotionTypeEntity> BreakingNewsPromotionTypeGetAll(string keyword, int status)
        {
            return BreakingNewsPromotionDal.BreakingNewsPromotionTypeGetAll(keyword, status);
        }
        public static BreakingNewsPromotionTypeEntity BreakingNewsPromotionTypeGetById(int id)
        {
            return BreakingNewsPromotionDal.BreakingNewsPromotionTypeGetById(id);
        }
        public static ErrorMapping.ErrorCodes BreakingNewsPromotionTypeInsert(BreakingNewsPromotionTypeEntity breakingNewsPromotionType)
        {
            return BreakingNewsPromotionDal.BreakingNewsPromotionTypeInsert(breakingNewsPromotionType)
                       ? ErrorMapping.ErrorCodes.Success
                       : ErrorMapping.ErrorCodes.BusinessError;
        }
        public static ErrorMapping.ErrorCodes BreakingNewsPromotionTypeUpdate(BreakingNewsPromotionTypeEntity breakingNewsPromotionType)
        {
            return BreakingNewsPromotionDal.BreakingNewsPromotionTypeUpdate(breakingNewsPromotionType)
                       ? ErrorMapping.ErrorCodes.Success
                       : ErrorMapping.ErrorCodes.BusinessError;
        }
        public static ErrorMapping.ErrorCodes BreakingNewsPromotionTypeDelete(int id)
        {
            return BreakingNewsPromotionDal.BreakingNewsPromotionTypeDelete(id)
                       ? ErrorMapping.ErrorCodes.Success
                       : ErrorMapping.ErrorCodes.BusinessError;
        }
        public static ErrorMapping.ErrorCodes BreakingNewsPromotionTypeMoveUp(int id)
        {
            return BreakingNewsPromotionDal.BreakingNewsPromotionTypeMoveUp(id)
                       ? ErrorMapping.ErrorCodes.Success
                       : ErrorMapping.ErrorCodes.BusinessError;
        }
        public static ErrorMapping.ErrorCodes BreakingNewsPromotionTypeMoveDown(int id)
        {
            return BreakingNewsPromotionDal.BreakingNewsPromotionTypeMoveDown(id)
                       ? ErrorMapping.ErrorCodes.Success
                       : ErrorMapping.ErrorCodes.BusinessError;
        }
        #endregion
    }
}
