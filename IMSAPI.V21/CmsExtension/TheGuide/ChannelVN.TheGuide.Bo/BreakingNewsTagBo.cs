﻿using ChannelVN.TheGuide.Dal;
using ChannelVN.TheGuide.Entity;
using System.Collections.Generic;
using ChannelVN.TheGuide.Entity.ErrorCode;

namespace ChannelVN.TheGuide.Bo
{
    public class BreakingNewsTagBo
    {
        public static List<BreakingNewsTagWithTagInfoEntity> GetBreakingNewsTagByBreakingNewsId(int breakingNewsId)
        {
            return BreakingNewsTagDal.GetBreakingNewsTagByBreakingNewsId(breakingNewsId);
        }
    }
}
