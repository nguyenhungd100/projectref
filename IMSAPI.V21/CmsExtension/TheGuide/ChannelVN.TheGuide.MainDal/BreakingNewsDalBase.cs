﻿
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using ChannelVN.CMS.Common;

using ChannelVN.TheGuide.Entity;
using ChannelVN.TheGuide.MainDal.Common;
using ChannelVN.TheGuide.MainDal.Databases;

namespace ChannelVN.TheGuide.MainDal
{
    public abstract class BreakingNewsDalBase
    {
        #region BreakingNew
        public List<BreakingNewsEntity> BreakingNewsSearch(string keyword, int status, int type, int zoneId, int pageIndex, int pageSize, ref int totalRow)
        {
            const string commandText = "CMS_BreakingNews_Search";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "TotalRow", totalRow, ParameterDirection.Output);
                _db.AddParameter(cmd, "Keyword", keyword);
                _db.AddParameter(cmd, "Status", status);
                _db.AddParameter(cmd, "Type", type);
                _db.AddParameter(cmd, "ZoneId", zoneId);
                _db.AddParameter(cmd, "PageIndex", pageIndex);
                _db.AddParameter(cmd, "PageSize", pageSize);
                var numberOfRow = _db.GetList<BreakingNewsEntity>(cmd);
                totalRow = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 0));
                return numberOfRow;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public BreakingNewsEntity BreakingNewsGetById(int id)
        {
            const string commandText = "CMS_BreakingNews_GetById";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", id);
                var numberOfRow = _db.Get<BreakingNewsEntity>(cmd);
                return numberOfRow;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool BreakingNewsInsert(BreakingNewsEntity breakingNewsEntity,string tagIdList, ref int id)
        {
            const string commandText = "CMS_BreakingNews_Insert";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", id, ParameterDirection.Output);
                _db.AddParameter(cmd, "Type", breakingNewsEntity.Type);
                _db.AddParameter(cmd, "ZoneId", breakingNewsEntity.ZoneId);
                _db.AddParameter(cmd, "Title", breakingNewsEntity.Title);
                _db.AddParameter(cmd, "SubTitle", breakingNewsEntity.SubTitle);
                _db.AddParameter(cmd, "Body", breakingNewsEntity.Body);
                _db.AddParameter(cmd, "Avatar", breakingNewsEntity.Avatar);
                _db.AddParameter(cmd, "Avatar2", breakingNewsEntity.Avatar2);
                _db.AddParameter(cmd, "AvatarDesc", breakingNewsEntity.AvatarDesc);
                _db.AddParameter(cmd, "Author", breakingNewsEntity.Author);
                if (breakingNewsEntity.DistributionDate <= Constants.MinDateTime)
                    _db.AddParameter(cmd, "DistributionDate", DBNull.Value);
                else _db.AddParameter(cmd, "DistributionDate", breakingNewsEntity.DistributionDate);
                if (breakingNewsEntity.CreatedDate <= Constants.MinDateTime)
                    _db.AddParameter(cmd, "CreatedDate", DateTime.Now);
                else _db.AddParameter(cmd, "CreatedDate", breakingNewsEntity.CreatedDate);
                _db.AddParameter(cmd, "CreatedBy", breakingNewsEntity.CreatedBy);
                if (breakingNewsEntity.LastModifiedDate <= Constants.MinDateTime)
                    _db.AddParameter(cmd, "LastModifiedDate", DateTime.Now);
                else _db.AddParameter(cmd, "LastModifiedDate", breakingNewsEntity.LastModifiedDate);
                _db.AddParameter(cmd, "LastModifiedBy", breakingNewsEntity.LastModifiedBy);
                if (breakingNewsEntity.PublishedDate <= Constants.MinDateTime)
                    _db.AddParameter(cmd, "PublishedDate", DateTime.Now);
                else _db.AddParameter(cmd, "PublishedDate", breakingNewsEntity.PublishedDate);
                _db.AddParameter(cmd, "PublishedBy", breakingNewsEntity.PublishedBy);
                _db.AddParameter(cmd, "Status", breakingNewsEntity.Status);
                _db.AddParameter(cmd, "TagIdList", tagIdList);
                var numberOfRow = _db.ExecuteNonQuery(cmd);
                id = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 0));
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool BreakingNewsUpdate(BreakingNewsEntity breakingNewsEntity, string tagIdList)
        {
            const string commandText = "CMS_BreakingNews_Update";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", breakingNewsEntity.Id);
                _db.AddParameter(cmd, "Type", breakingNewsEntity.Type);
                _db.AddParameter(cmd, "ZoneId", breakingNewsEntity.ZoneId);
                _db.AddParameter(cmd, "Title", breakingNewsEntity.Title);
                _db.AddParameter(cmd, "SubTitle", breakingNewsEntity.SubTitle);
                _db.AddParameter(cmd, "Body", breakingNewsEntity.Body);
                _db.AddParameter(cmd, "Avatar", breakingNewsEntity.Avatar);
                _db.AddParameter(cmd, "Avatar2", breakingNewsEntity.Avatar2);
                _db.AddParameter(cmd, "AvatarDesc", breakingNewsEntity.AvatarDesc);
                _db.AddParameter(cmd, "Author", breakingNewsEntity.Author);
                if (breakingNewsEntity.DistributionDate <= Constants.MinDateTime)
                    _db.AddParameter(cmd, "DistributionDate", DBNull.Value);
                else _db.AddParameter(cmd, "DistributionDate", breakingNewsEntity.DistributionDate);
                if (breakingNewsEntity.LastModifiedDate <= Constants.MinDateTime)
                    _db.AddParameter(cmd, "LastModifiedDate", DateTime.Now);
                else _db.AddParameter(cmd, "LastModifiedDate", breakingNewsEntity.LastModifiedDate);
                _db.AddParameter(cmd, "LastModifiedBy", breakingNewsEntity.LastModifiedBy);
                if (breakingNewsEntity.PublishedDate <= Constants.MinDateTime)
                    _db.AddParameter(cmd, "PublishedDate", DateTime.Now);
                else _db.AddParameter(cmd, "PublishedDate", breakingNewsEntity.PublishedDate);
                _db.AddParameter(cmd, "PublishedBy", breakingNewsEntity.PublishedBy);
                _db.AddParameter(cmd, "Status", breakingNewsEntity.Status);
                _db.AddParameter(cmd, "TagIdList", tagIdList);
                var numberOfRow = _db.ExecuteNonQuery(cmd);
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool BreakingNewsDelete(int id)
        {
            const string commandText = "CMS_BreakingNews_Delete";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", id);
                var numberOfRow = _db.ExecuteNonQuery(cmd);
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        #endregion

        #region BreakingNewsLocation
        public List<BreakingNewsLocationEntity> BreakingNewsLocationGetAll(string keyword, int status)
        {
            const string commandText = "CMS_BreakingNewsLocation_GetAll";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Keyword", keyword);
                _db.AddParameter(cmd, "Status", status);
                var numberOfRow = _db.GetList<BreakingNewsLocationEntity>(cmd);
                return numberOfRow;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public BreakingNewsLocationEntity BreakingNewsLocationGetById(int id)
        {
            const string commandText = "CMS_BreakingNewsLocation_GetById";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", id);
                var numberOfRow = _db.Get<BreakingNewsLocationEntity>(cmd);
                return numberOfRow;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool BreakingNewsLocationInsert(BreakingNewsLocationEntity BreakingNewsLocationEntity)
        {
            const string commandText = "CMS_BreakingNewsLocation_Insert";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Name", BreakingNewsLocationEntity.Name);
                if (BreakingNewsLocationEntity.CreatedDate <= Constants.MinDateTime)
                    _db.AddParameter(cmd, "CreatedDate", DateTime.Now);
                else _db.AddParameter(cmd, "CreatedDate", BreakingNewsLocationEntity.CreatedDate);
                _db.AddParameter(cmd, "CreatedBy", BreakingNewsLocationEntity.CreatedBy);
                if (BreakingNewsLocationEntity.LastModifiedDate <= Constants.MinDateTime)
                    _db.AddParameter(cmd, "LastModifiedDate", DateTime.Now);
                else _db.AddParameter(cmd, "LastModifiedDate", BreakingNewsLocationEntity.LastModifiedDate);
                _db.AddParameter(cmd, "LastModifiedBy", BreakingNewsLocationEntity.LastModifiedBy);
                _db.AddParameter(cmd, "Status", BreakingNewsLocationEntity.Status);
                _db.AddParameter(cmd, "Priority", BreakingNewsLocationEntity.Priority);
                var numberOfRow = _db.ExecuteNonQuery(cmd);
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool BreakingNewsLocationUpdate(BreakingNewsLocationEntity BreakingNewsLocationEntity)
        {
            const string commandText = "CMS_BreakingNewsLocation_Update";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", BreakingNewsLocationEntity.Id);
                _db.AddParameter(cmd, "Name", BreakingNewsLocationEntity.Name);
                if (BreakingNewsLocationEntity.LastModifiedDate <= Constants.MinDateTime)
                    _db.AddParameter(cmd, "LastModifiedDate", DateTime.Now);
                else _db.AddParameter(cmd, "LastModifiedDate", BreakingNewsLocationEntity.LastModifiedDate);
                _db.AddParameter(cmd, "LastModifiedBy", BreakingNewsLocationEntity.LastModifiedBy);
                _db.AddParameter(cmd, "Status", BreakingNewsLocationEntity.Status);
                _db.AddParameter(cmd, "Priority", BreakingNewsLocationEntity.Priority);
                var numberOfRow = _db.ExecuteNonQuery(cmd);
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool BreakingNewsLocationDelete(int id)
        {
            const string commandText = "CMS_BreakingNewsLocation_Delete";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", id);
                var numberOfRow = _db.ExecuteNonQuery(cmd);
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public bool BreakingNewsLocationMoveUp(int id)
        {
            const string commandText = "CMS_BreakingNewsLocation_MoveUp";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", id);
                var numberOfRow = _db.ExecuteNonQuery(cmd);
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool BreakingNewsLocationMoveDown(int id)
        {
            const string commandText = "CMS_BreakingNewsLocation_MoveDown";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", id);
                var numberOfRow = _db.ExecuteNonQuery(cmd);
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        #endregion

        #region BreakingNewsTag
        public List<BreakingNewsTagWithTagInfoEntity> GetBreakingNewsTagByBreakingNewsId(int breakingNewsId)
        {
            const string commandText = "CMS_BreakingNewsTag_GetByBreakingNewsId";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "BreakingNewsId", breakingNewsId);
                var numberOfRow = _db.GetList<BreakingNewsTagWithTagInfoEntity>(cmd);
                return numberOfRow;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        
        #endregion

        #region Core members

        private readonly CmsMainDb _db;

        protected BreakingNewsDalBase(CmsMainDb db)
        {
            _db = db;
        }

        protected CmsMainDb Database
        {
            get { return _db; }
        }

        #endregion
    }

}
