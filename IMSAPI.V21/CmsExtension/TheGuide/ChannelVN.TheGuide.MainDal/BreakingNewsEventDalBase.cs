﻿
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using ChannelVN.CMS.Common;

using ChannelVN.TheGuide.Entity;
using ChannelVN.TheGuide.MainDal.Common;
using ChannelVN.TheGuide.MainDal.Databases;

namespace ChannelVN.TheGuide.MainDal
{
    public abstract class BreakingNewsEventDalBase
    {
        #region BreakingNewsEvent
        public BreakingNewsEventEntity BreakingNewsEventGetById(int id)
        {
            const string commandText = "CMS_BreakingNewsEvent_GetById";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", id);
                var numberOfRow = _db.Get<BreakingNewsEventEntity>(cmd);
                return numberOfRow;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public BreakingNewsEventEntity BreakingNewsEventGetByBreakingNewsId(int id)
        {
            const string commandText = "CMS_BreakingNewsEvent_GetByBreakingNewsId";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "BreakingNewsId", id);
                var numberOfRow = _db.Get<BreakingNewsEventEntity>(cmd);
                return numberOfRow;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool BreakingNewsEventInsert(BreakingNewsEventEntity breakingNewsEventEntity)
        {
            const string commandText = "CMS_BreakingNewsEvent_Insert";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "BreakingNewsId", breakingNewsEventEntity.BreakingNewsId);
                _db.AddParameter(cmd, "EventTypeId", breakingNewsEventEntity.EventTypeId);
                _db.AddParameter(cmd, "EventLocationId", breakingNewsEventEntity.EventLocationId);
                if (breakingNewsEventEntity.FormDate <= Constants.MinDateTime)
                    _db.AddParameter(cmd, "FormDate", DBNull.Value);
                else _db.AddParameter(cmd, "FormDate", breakingNewsEventEntity.FormDate);
                if (breakingNewsEventEntity.FormDate <= Constants.MinDateTime)
                    _db.AddParameter(cmd, "ToDate", DBNull.Value);
                else _db.AddParameter(cmd, "ToDate", breakingNewsEventEntity.ToDate);
                var numberOfRow = _db.ExecuteNonQuery(cmd);
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool BreakingNewsEventUpdate(BreakingNewsEventEntity breakingNewsEventEntity)
        {
            const string commandText = "CMS_BreakingNewsEvent_Update";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", breakingNewsEventEntity.Id);
                _db.AddParameter(cmd, "BreakingNewsId", breakingNewsEventEntity.BreakingNewsId);
                _db.AddParameter(cmd, "EventTypeId", breakingNewsEventEntity.EventTypeId);
                _db.AddParameter(cmd, "EventLocationId", breakingNewsEventEntity.EventLocationId);
                if (breakingNewsEventEntity.FormDate <= Constants.MinDateTime)
                    _db.AddParameter(cmd, "FormDate", DBNull.Value);
                else _db.AddParameter(cmd, "FormDate", breakingNewsEventEntity.FormDate);
                if (breakingNewsEventEntity.FormDate <= Constants.MinDateTime)
                    _db.AddParameter(cmd, "ToDate", DBNull.Value);
                else _db.AddParameter(cmd, "ToDate", breakingNewsEventEntity.ToDate);
                var numberOfRow = _db.ExecuteNonQuery(cmd);
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        #endregion

        #region BreakingNewsEventLocation
        public List<BreakingNewsEventLocationEntity> BreakingNewsEventLocationGetAll(string keyword, int status, int breakingNewsLocationId)
        {
            const string commandText = "CMS_BreakingNewsEventLocation_GetAll";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Keyword", keyword);
                _db.AddParameter(cmd, "Status", status);
                _db.AddParameter(cmd, "BreakingNewsLocationId", breakingNewsLocationId);
                var numberOfRow = _db.GetList<BreakingNewsEventLocationEntity>(cmd);
                return numberOfRow;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public BreakingNewsEventLocationEntity BreakingNewsEventLocationGetById(int id)
        {
            const string commandText = "CMS_BreakingNewsEventLocation_GetById";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", id);
                var numberOfRow = _db.Get<BreakingNewsEventLocationEntity>(cmd);
                return numberOfRow;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool BreakingNewsEventLocationInsert(BreakingNewsEventLocationEntity breakingNewsEventLocationEntity)
        {
            const string commandText = "CMS_BreakingNewsEventLocation_Insert";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "BreakingNewsLocationId", breakingNewsEventLocationEntity.BreakingNewsLocationId);
                _db.AddParameter(cmd, "Name", breakingNewsEventLocationEntity.Name);
                if (breakingNewsEventLocationEntity.CreatedDate <= Constants.MinDateTime)
                    _db.AddParameter(cmd, "CreatedDate", DateTime.Now);
                else _db.AddParameter(cmd, "CreatedDate", breakingNewsEventLocationEntity.CreatedDate);
                _db.AddParameter(cmd, "CreatedBy", breakingNewsEventLocationEntity.CreatedBy);
                if (breakingNewsEventLocationEntity.LastModifiedDate <= Constants.MinDateTime)
                    _db.AddParameter(cmd, "LastModifiedDate", DateTime.Now);
                else _db.AddParameter(cmd, "LastModifiedDate", breakingNewsEventLocationEntity.LastModifiedDate);
                _db.AddParameter(cmd, "LastModifiedBy", breakingNewsEventLocationEntity.LastModifiedBy);
                _db.AddParameter(cmd, "Status", breakingNewsEventLocationEntity.Status);
                _db.AddParameter(cmd, "Priority", breakingNewsEventLocationEntity.Priority);
                var numberOfRow = _db.ExecuteNonQuery(cmd);
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool BreakingNewsEventLocationUpdate(BreakingNewsEventLocationEntity breakingNewsEventLocationEntity)
        {
            const string commandText = "CMS_BreakingNewsEventLocation_Update";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", breakingNewsEventLocationEntity.Id);
                _db.AddParameter(cmd, "BreakingNewsLocationId", breakingNewsEventLocationEntity.BreakingNewsLocationId);
                _db.AddParameter(cmd, "Name", breakingNewsEventLocationEntity.Name);
                if (breakingNewsEventLocationEntity.LastModifiedDate <= Constants.MinDateTime)
                    _db.AddParameter(cmd, "LastModifiedDate", DateTime.Now);
                else _db.AddParameter(cmd, "LastModifiedDate", breakingNewsEventLocationEntity.LastModifiedDate);
                _db.AddParameter(cmd, "LastModifiedBy", breakingNewsEventLocationEntity.LastModifiedBy);
                _db.AddParameter(cmd, "Status", breakingNewsEventLocationEntity.Status);
                _db.AddParameter(cmd, "Priority", breakingNewsEventLocationEntity.Priority);
                var numberOfRow = _db.ExecuteNonQuery(cmd);
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool BreakingNewsEventLocationDelete(int id)
        {
            const string commandText = "CMS_BreakingNewsEventLocation_Delete";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", id);
                var numberOfRow = _db.ExecuteNonQuery(cmd);
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public bool BreakingNewsEventLocationMoveUp(int id)
        {
            const string commandText = "CMS_BreakingNewsEventLocation_MoveUp";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", id);
                var numberOfRow = _db.ExecuteNonQuery(cmd);
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool BreakingNewsEventLocationMoveDown(int id)
        {
            const string commandText = "CMS_BreakingNewsEventLocation_MoveDown";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", id);
                var numberOfRow = _db.ExecuteNonQuery(cmd);
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        #endregion

        #region BreakingNewsEventType
        public List<BreakingNewsEventTypeEntity> BreakingNewsEventTypeGetAll(string keyword, int status)
        {
            const string commandText = "CMS_BreakingNewsEventType_GetAll";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Keyword", keyword);
                _db.AddParameter(cmd, "Status", status);
                var numberOfRow = _db.GetList<BreakingNewsEventTypeEntity>(cmd);
                return numberOfRow;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public BreakingNewsEventTypeEntity BreakingNewsEventTypeGetById(int id)
        {
            const string commandText = "CMS_BreakingNewsEventType_GetById";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", id);
                var numberOfRow = _db.Get<BreakingNewsEventTypeEntity>(cmd);
                return numberOfRow;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool BreakingNewsEventTypeInsert(BreakingNewsEventTypeEntity breakingNewsEventLocationEntity)
        {
            const string commandText = "CMS_BreakingNewsEventType_Insert";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Name", breakingNewsEventLocationEntity.Name);
                if (breakingNewsEventLocationEntity.CreatedDate <= Constants.MinDateTime)
                    _db.AddParameter(cmd, "CreatedDate", DateTime.Now);
                else _db.AddParameter(cmd, "CreatedDate", breakingNewsEventLocationEntity.CreatedDate);
                _db.AddParameter(cmd, "CreatedBy", breakingNewsEventLocationEntity.CreatedBy);
                if (breakingNewsEventLocationEntity.LastModifiedDate <= Constants.MinDateTime)
                    _db.AddParameter(cmd, "LastModifiedDate", DateTime.Now);
                else _db.AddParameter(cmd, "LastModifiedDate", breakingNewsEventLocationEntity.LastModifiedDate);
                _db.AddParameter(cmd, "LastModifiedBy", breakingNewsEventLocationEntity.LastModifiedBy);
                _db.AddParameter(cmd, "Status", breakingNewsEventLocationEntity.Status);
                _db.AddParameter(cmd, "Priority", breakingNewsEventLocationEntity.Priority);
                var numberOfRow = _db.ExecuteNonQuery(cmd);
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool BreakingNewsEventTypeUpdate(BreakingNewsEventTypeEntity breakingNewsEventLocationEntity)
        {
            const string commandText = "CMS_BreakingNewsEventType_Update";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", breakingNewsEventLocationEntity.Id);
                _db.AddParameter(cmd, "Name", breakingNewsEventLocationEntity.Name);
                if (breakingNewsEventLocationEntity.LastModifiedDate <= Constants.MinDateTime)
                    _db.AddParameter(cmd, "LastModifiedDate", DateTime.Now);
                else _db.AddParameter(cmd, "LastModifiedDate", breakingNewsEventLocationEntity.LastModifiedDate);
                _db.AddParameter(cmd, "LastModifiedBy", breakingNewsEventLocationEntity.LastModifiedBy);
                _db.AddParameter(cmd, "Status", breakingNewsEventLocationEntity.Status);
                _db.AddParameter(cmd, "Priority", breakingNewsEventLocationEntity.Priority);
                var numberOfRow = _db.ExecuteNonQuery(cmd);
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool BreakingNewsEventTypeDelete(int id)
        {
            const string commandText = "CMS_BreakingNewsEventType_Delete";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", id);
                var numberOfRow = _db.ExecuteNonQuery(cmd);
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public bool BreakingNewsEventTypeMoveUp(int id)
        {
            const string commandText = "CMS_BreakingNewsEventType_MoveUp";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", id);
                var numberOfRow = _db.ExecuteNonQuery(cmd);
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool BreakingNewsEventTypeMoveDown(int id)
        {
            const string commandText = "CMS_BreakingNewsEventType_MoveDown";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", id);
                var numberOfRow = _db.ExecuteNonQuery(cmd);
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        #endregion

        #region Core members

        private readonly CmsMainDb _db;

        protected BreakingNewsEventDalBase(CmsMainDb db)
        {
            _db = db;
        }

        protected CmsMainDb Database
        {
            get { return _db; }
        }

        #endregion
    }

}
