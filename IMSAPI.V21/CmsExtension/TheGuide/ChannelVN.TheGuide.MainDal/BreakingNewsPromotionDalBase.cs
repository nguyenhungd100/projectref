﻿
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using ChannelVN.CMS.Common;

using ChannelVN.TheGuide.Entity;
using ChannelVN.TheGuide.MainDal.Common;
using ChannelVN.TheGuide.MainDal.Databases;

namespace ChannelVN.TheGuide.MainDal
{
    public abstract class BreakingNewsPromotionDalBase
    {
        public BreakingNewsPromotionEntity BreakingNewsPromotionGetById(int id)
        {
            const string commandText = "CMS_BreakingNewsPromotion_GetById";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", id);
                var numberOfRow = _db.Get<BreakingNewsPromotionEntity>(cmd);
                return numberOfRow;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public BreakingNewsPromotionEntity BreakingNewsPromotionGetByBreakingNewsId(int id)
        {
            const string commandText = "CMS_BreakingNewsPromotion_GetByBreakingNewsId";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "BreakingNewsId", id);
                var numberOfRow = _db.Get<BreakingNewsPromotionEntity>(cmd);
                return numberOfRow;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool BreakingNewsPromotionInsert(BreakingNewsPromotionEntity breakingNewsPromotionEntity)
        {
            const string commandText = "CMS_BreakingNewsPromotion_Insert";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "BreakingNewsId", breakingNewsPromotionEntity.BreakingNewsId);
                _db.AddParameter(cmd, "LocationId", breakingNewsPromotionEntity.LocationId);
                _db.AddParameter(cmd, "BusinessLocationId", breakingNewsPromotionEntity.BusinessLocationId);
                _db.AddParameter(cmd, "PromotionTypeId", breakingNewsPromotionEntity.PromotionTypeId);
                if (breakingNewsPromotionEntity.FormDate <= Constants.MinDateTime)
                    _db.AddParameter(cmd, "FormDate", DBNull.Value);
                else _db.AddParameter(cmd, "FormDate", breakingNewsPromotionEntity.FormDate);
                if (breakingNewsPromotionEntity.FormDate <= Constants.MinDateTime)
                    _db.AddParameter(cmd, "ToDate", DBNull.Value);
                else _db.AddParameter(cmd, "ToDate", breakingNewsPromotionEntity.ToDate);
                _db.AddParameter(cmd, "Price", breakingNewsPromotionEntity.Price);
                _db.AddParameter(cmd, "PercentageDiscount", breakingNewsPromotionEntity.PercentageDiscount);
                var numberOfRow = _db.ExecuteNonQuery(cmd);
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool BreakingNewsPromotionUpdate(BreakingNewsPromotionEntity breakingNewsPromotionEntity)
        {
            const string commandText = "CMS_BreakingNewsPromotion_Update";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", breakingNewsPromotionEntity.Id);
                _db.AddParameter(cmd, "BreakingNewsId", breakingNewsPromotionEntity.BreakingNewsId);
                _db.AddParameter(cmd, "LocationId", breakingNewsPromotionEntity.LocationId);
                _db.AddParameter(cmd, "BusinessLocationId", breakingNewsPromotionEntity.BusinessLocationId);
                _db.AddParameter(cmd, "PromotionTypeId", breakingNewsPromotionEntity.PromotionTypeId);
                if (breakingNewsPromotionEntity.FormDate <= Constants.MinDateTime)
                    _db.AddParameter(cmd, "FormDate", DBNull.Value);
                else _db.AddParameter(cmd, "FormDate", breakingNewsPromotionEntity.FormDate);
                if (breakingNewsPromotionEntity.FormDate <= Constants.MinDateTime)
                    _db.AddParameter(cmd, "ToDate", DBNull.Value);
                else _db.AddParameter(cmd, "ToDate", breakingNewsPromotionEntity.ToDate);
                _db.AddParameter(cmd, "Price", breakingNewsPromotionEntity.Price);
                _db.AddParameter(cmd, "PercentageDiscount", breakingNewsPromotionEntity.PercentageDiscount);
                var numberOfRow = _db.ExecuteNonQuery(cmd);
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }


        #region BreakingNewsPromotionBusinessLocation
        public List<BreakingNewsPromotionBusinessLocationEntity> BreakingNewsPromotionBusinessLocationGetAll(string keyword, int status, int breakingNewsLocationId)
        {
            const string commandText = "CMS_BreakingNewsPromotionBusinessLocation_GetAll";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Keyword", keyword);
                _db.AddParameter(cmd, "Status", status);
                _db.AddParameter(cmd, "BreakingNewsLocationId", breakingNewsLocationId);
                var numberOfRow = _db.GetList<BreakingNewsPromotionBusinessLocationEntity>(cmd);
                return numberOfRow;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public BreakingNewsPromotionBusinessLocationEntity BreakingNewsPromotionBusinessLocationGetById(int id)
        {
            const string commandText = "CMS_BreakingNewsPromotionBusinessLocation_GetById";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", id);
                var numberOfRow = _db.Get<BreakingNewsPromotionBusinessLocationEntity>(cmd);
                return numberOfRow;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool BreakingNewsPromotionBusinessLocationInsert(BreakingNewsPromotionBusinessLocationEntity breakingNewsPromotionLocationEntity)
        {
            const string commandText = "CMS_BreakingNewsPromotionBusinessLocation_Insert";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "BreakingNewsLocationId",
                                 breakingNewsPromotionLocationEntity.BreakingNewsLocationId);
                _db.AddParameter(cmd, "Name", breakingNewsPromotionLocationEntity.Name);
                if (breakingNewsPromotionLocationEntity.CreatedDate <= Constants.MinDateTime)
                    _db.AddParameter(cmd, "CreatedDate", DateTime.Now);
                else _db.AddParameter(cmd, "CreatedDate", breakingNewsPromotionLocationEntity.CreatedDate);
                _db.AddParameter(cmd, "CreatedBy", breakingNewsPromotionLocationEntity.CreatedBy);
                if (breakingNewsPromotionLocationEntity.LastModifiedDate <= Constants.MinDateTime)
                    _db.AddParameter(cmd, "LastModifiedDate", DateTime.Now);
                else _db.AddParameter(cmd, "LastModifiedDate", breakingNewsPromotionLocationEntity.LastModifiedDate);
                _db.AddParameter(cmd, "LastModifiedBy", breakingNewsPromotionLocationEntity.LastModifiedBy);
                _db.AddParameter(cmd, "Status", breakingNewsPromotionLocationEntity.Status);
                _db.AddParameter(cmd, "Priority", breakingNewsPromotionLocationEntity.Priority);
                var numberOfRow = _db.ExecuteNonQuery(cmd);
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool BreakingNewsPromotionBusinessLocationUpdate(BreakingNewsPromotionBusinessLocationEntity breakingNewsPromotionLocationEntity)
        {
            const string commandText = "CMS_BreakingNewsPromotionBusinessLocation_Update";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", breakingNewsPromotionLocationEntity.Id);
                _db.AddParameter(cmd, "BreakingNewsLocationId",
                                 breakingNewsPromotionLocationEntity.BreakingNewsLocationId);
                _db.AddParameter(cmd, "Name", breakingNewsPromotionLocationEntity.Name);
                if (breakingNewsPromotionLocationEntity.LastModifiedDate <= Constants.MinDateTime)
                    _db.AddParameter(cmd, "LastModifiedDate", DateTime.Now);
                else _db.AddParameter(cmd, "LastModifiedDate", breakingNewsPromotionLocationEntity.LastModifiedDate);
                _db.AddParameter(cmd, "LastModifiedBy", breakingNewsPromotionLocationEntity.LastModifiedBy);
                _db.AddParameter(cmd, "Status", breakingNewsPromotionLocationEntity.Status);
                _db.AddParameter(cmd, "Priority", breakingNewsPromotionLocationEntity.Priority);
                var numberOfRow = _db.ExecuteNonQuery(cmd);
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool BreakingNewsPromotionBusinessLocationDelete(int id)
        {
            const string commandText = "CMS_BreakingNewsPromotionBusinessLocation_Delete";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", id); ;
                var numberOfRow = _db.ExecuteNonQuery(cmd);
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool BreakingNewsPromotionBusinessLocationMoveUp(int id)
        {
            const string commandText = "CMS_BreakingNewsPromotionBusinessLocation_MoveUp";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", id);
                var numberOfRow = _db.ExecuteNonQuery(cmd);
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool BreakingNewsPromotionBusinessLocationMoveDown(int id)
        {
            const string commandText = "CMS_BreakingNewsPromotionBusinessLocation_MoveDown";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", id);
                var numberOfRow = _db.ExecuteNonQuery(cmd);
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        #endregion

        #region BreakingNewsPromotionLocation
        public List<BreakingNewsPromotionLocationEntity> BreakingNewsPromotionLocationGetAll(string keyword, int status)
        {
            const string commandText = "CMS_BreakingNewsPromotionLocation_GetAll";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Keyword", keyword);
                _db.AddParameter(cmd, "Status", status);
                var numberOfRow = _db.GetList<BreakingNewsPromotionLocationEntity>(cmd);
                return numberOfRow;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public BreakingNewsPromotionLocationEntity BreakingNewsPromotionLocationGetById(int id)
        {
            const string commandText = "CMS_BreakingNewsPromotionLocation_GetById";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", id);
                var numberOfRow = _db.Get<BreakingNewsPromotionLocationEntity>(cmd);
                return numberOfRow;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool BreakingNewsPromotionLocationInsert(BreakingNewsPromotionLocationEntity breakingNewsPromotionLocationEntity)
        {
            const string commandText = "CMS_BreakingNewsPromotionLocation_Insert";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Name", breakingNewsPromotionLocationEntity.Name);
                if (breakingNewsPromotionLocationEntity.CreatedDate <= Constants.MinDateTime)
                    _db.AddParameter(cmd, "CreatedDate", DateTime.Now);
                else _db.AddParameter(cmd, "CreatedDate", breakingNewsPromotionLocationEntity.CreatedDate);
                _db.AddParameter(cmd, "CreatedBy", breakingNewsPromotionLocationEntity.CreatedBy);
                if (breakingNewsPromotionLocationEntity.LastModifiedDate <= Constants.MinDateTime)
                    _db.AddParameter(cmd, "LastModifiedDate", DateTime.Now);
                else _db.AddParameter(cmd, "LastModifiedDate", breakingNewsPromotionLocationEntity.LastModifiedDate);
                _db.AddParameter(cmd, "LastModifiedBy", breakingNewsPromotionLocationEntity.LastModifiedBy);
                _db.AddParameter(cmd, "Status", breakingNewsPromotionLocationEntity.Status);
                _db.AddParameter(cmd, "Priority", breakingNewsPromotionLocationEntity.Priority);
                var numberOfRow = _db.ExecuteNonQuery(cmd);
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool BreakingNewsPromotionLocationUpdate(BreakingNewsPromotionLocationEntity breakingNewsPromotionLocationEntity)
        {
            const string commandText = "CMS_BreakingNewsPromotionLocation_Update";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", breakingNewsPromotionLocationEntity.Id);
                _db.AddParameter(cmd, "Name", breakingNewsPromotionLocationEntity.Name);
                if (breakingNewsPromotionLocationEntity.LastModifiedDate <= Constants.MinDateTime)
                    _db.AddParameter(cmd, "LastModifiedDate", DateTime.Now);
                else _db.AddParameter(cmd, "LastModifiedDate", breakingNewsPromotionLocationEntity.LastModifiedDate);
                _db.AddParameter(cmd, "LastModifiedBy", breakingNewsPromotionLocationEntity.LastModifiedBy);
                _db.AddParameter(cmd, "Status", breakingNewsPromotionLocationEntity.Status);
                _db.AddParameter(cmd, "Priority", breakingNewsPromotionLocationEntity.Priority);
                var numberOfRow = _db.ExecuteNonQuery(cmd);
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool BreakingNewsPromotionLocationDelete(int id)
        {
            const string commandText = "CMS_BreakingNewsPromotionLocation_Delete";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", id); ;
                var numberOfRow = _db.ExecuteNonQuery(cmd);
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public bool BreakingNewsPromotionLocationMoveUp(int id)
        {
            const string commandText = "CMS_BreakingNewsPromotionLocation_MoveUp";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", id);
                var numberOfRow = _db.ExecuteNonQuery(cmd);
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool BreakingNewsPromotionLocationMoveDown(int id)
        {
            const string commandText = "CMS_BreakingNewsPromotionLocation_MoveDown";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", id);
                var numberOfRow = _db.ExecuteNonQuery(cmd);
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        #endregion

        #region BreakingNewsPromotionType
        public List<BreakingNewsPromotionTypeEntity> BreakingNewsPromotionTypeGetAll(string keyword, int status)
        {
            const string commandText = "CMS_BreakingNewsPromotionType_GetAll";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Keyword", keyword);
                _db.AddParameter(cmd, "Status", status);
                var numberOfRow = _db.GetList<BreakingNewsPromotionTypeEntity>(cmd);
                return numberOfRow;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public BreakingNewsPromotionTypeEntity BreakingNewsPromotionTypeGetById(int id)
        {
            const string commandText = "CMS_BreakingNewsPromotionType_GetById";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", id);
                var numberOfRow = _db.Get<BreakingNewsPromotionTypeEntity>(cmd);
                return numberOfRow;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool BreakingNewsPromotionTypeInsert(BreakingNewsPromotionTypeEntity breakingNewsPromotionLocationEntity)
        {
            const string commandText = "CMS_BreakingNewsPromotionType_Insert";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Name", breakingNewsPromotionLocationEntity.Name);
                if (breakingNewsPromotionLocationEntity.CreatedDate <= Constants.MinDateTime)
                    _db.AddParameter(cmd, "CreatedDate", DateTime.Now);
                else _db.AddParameter(cmd, "CreatedDate", breakingNewsPromotionLocationEntity.CreatedDate);
                _db.AddParameter(cmd, "CreatedBy", breakingNewsPromotionLocationEntity.CreatedBy);
                if (breakingNewsPromotionLocationEntity.LastModifiedDate <= Constants.MinDateTime)
                    _db.AddParameter(cmd, "LastModifiedDate", DateTime.Now);
                else _db.AddParameter(cmd, "LastModifiedDate", breakingNewsPromotionLocationEntity.LastModifiedDate);
                _db.AddParameter(cmd, "LastModifiedBy", breakingNewsPromotionLocationEntity.LastModifiedBy);
                _db.AddParameter(cmd, "Status", breakingNewsPromotionLocationEntity.Status);
                _db.AddParameter(cmd, "Priority", breakingNewsPromotionLocationEntity.Priority);
                var numberOfRow = _db.ExecuteNonQuery(cmd);
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool BreakingNewsPromotionTypeUpdate(BreakingNewsPromotionTypeEntity breakingNewsPromotionLocationEntity)
        {
            const string commandText = "CMS_BreakingNewsPromotionType_Update";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", breakingNewsPromotionLocationEntity.Id);
                _db.AddParameter(cmd, "Name", breakingNewsPromotionLocationEntity.Name);
                if (breakingNewsPromotionLocationEntity.LastModifiedDate <= Constants.MinDateTime)
                    _db.AddParameter(cmd, "LastModifiedDate", DateTime.Now);
                else _db.AddParameter(cmd, "LastModifiedDate", breakingNewsPromotionLocationEntity.LastModifiedDate);
                _db.AddParameter(cmd, "LastModifiedBy", breakingNewsPromotionLocationEntity.LastModifiedBy);
                _db.AddParameter(cmd, "Status", breakingNewsPromotionLocationEntity.Status);
                _db.AddParameter(cmd, "Priority", breakingNewsPromotionLocationEntity.Priority);
                var numberOfRow = _db.ExecuteNonQuery(cmd);
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool BreakingNewsPromotionTypeDelete(int id)
        {
            const string commandText = "CMS_BreakingNewsPromotionType_Delete";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", id); ;
                var numberOfRow = _db.ExecuteNonQuery(cmd);
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public bool BreakingNewsPromotionTypeMoveUp(int id)
        {
            const string commandText = "CMS_BreakingNewsPromotionType_MoveUp";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", id);
                var numberOfRow = _db.ExecuteNonQuery(cmd);
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool BreakingNewsPromotionTypeMoveDown(int id)
        {
            const string commandText = "CMS_BreakingNewsPromotionType_MoveDown";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", id);
                var numberOfRow = _db.ExecuteNonQuery(cmd);
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        #endregion

        #region Core members

        private readonly CmsMainDb _db;

        protected BreakingNewsPromotionDalBase(CmsMainDb db)
        {
            _db = db;
        }

        protected CmsMainDb Database
        {
            get { return _db; }
        }

        #endregion
    }

}
