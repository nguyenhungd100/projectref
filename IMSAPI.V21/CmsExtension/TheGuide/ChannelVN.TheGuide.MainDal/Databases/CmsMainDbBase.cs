﻿using System.Data;
using ChannelVN.CMS.Common.DalCommon;

namespace ChannelVN.TheGuide.MainDal.Databases
{
    /// <summary>
    /// The base class for the <see cref="CmsMainDb"/> class that 
    /// represents a connection to the <c>CmsMainDb</c> database. 
    /// </summary>
    /// <remarks>
    /// Do not change this source code. Modify the CmsMainDb class
    /// if you need to add or change some functionality.
    /// </remarks>
    public abstract class CmsMainDbBase : MainDbBase
    {
        #region Store procedures

        #region BreakingNews

        private BreakingNewsDal _breakingNewsMainDal;
        public BreakingNewsDal BreakingNewsMainDal
        {
            get { return _breakingNewsMainDal ?? (_breakingNewsMainDal = new BreakingNewsDal((CmsMainDb)this)); }
        }

        #endregion

        #region BreakingNewsEvent

        private BreakingNewsEventDal _breakingNewsEventMainDal;
        public BreakingNewsEventDal BreakingNewsEventMainDal
        {
            get { return _breakingNewsEventMainDal ?? (_breakingNewsEventMainDal = new BreakingNewsEventDal((CmsMainDb)this)); }
        }

        #endregion

        #region BreakingNewsPromotion

        private BreakingNewsPromotionDal _breakingNewsPromotionMainDal;
        public BreakingNewsPromotionDal BreakingNewsPromotionMainDal
        {
            get { return _breakingNewsPromotionMainDal ?? (_breakingNewsPromotionMainDal = new BreakingNewsPromotionDal((CmsMainDb)this)); }
        }

        #endregion

        #endregion

        #region Constructors

        protected CmsMainDbBase()
        {
        }
        protected CmsMainDbBase(bool init)
        {
            if (init)
            {
                InitConnection();
            }
        }

        #endregion

        #region Protected members

        protected override sealed void InitConnection()
        {
            _connection = CreateConnection();
            _connection.Open();
        }

        /// <summary>
        /// Creates a new connection to the database.
        /// </summary>
        /// <returns>A reference to the <see cref="System.Data.IDbConnection"/> object.</returns>
        protected abstract IDbConnection CreateConnection();

        #endregion
    }
}