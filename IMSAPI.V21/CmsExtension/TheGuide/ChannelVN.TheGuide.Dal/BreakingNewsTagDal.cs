﻿
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using ChannelVN.CMS.Common;
using ChannelVN.TheGuide.Dal.Common;
using ChannelVN.TheGuide.Entity;
using ChannelVN.TheGuide.MainDal.Databases;

namespace ChannelVN.TheGuide.Dal
{
    public class BreakingNewsTagDal
    {
        public static List<BreakingNewsTagWithTagInfoEntity> GetBreakingNewsTagByBreakingNewsId(int breakingNewsId)
        {
            List<BreakingNewsTagWithTagInfoEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.BreakingNewsMainDal.GetBreakingNewsTagByBreakingNewsId(breakingNewsId);
            }
            return returnValue;
        }   
    }
}
