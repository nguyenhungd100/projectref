﻿
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using ChannelVN.CMS.Common;
using ChannelVN.TheGuide.Dal.Common;
using ChannelVN.TheGuide.Entity;
using ChannelVN.TheGuide.MainDal.Databases;

namespace ChannelVN.TheGuide.Dal
{
    public class BreakingNewsDal
    {
        #region BreakingNew
        public static List<BreakingNewsEntity> BreakingNewsSearch(string keyword, int status, int type, int zoneId, int pageIndex, int pageSize, ref int totalRow)
        {
            List<BreakingNewsEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.BreakingNewsMainDal.BreakingNewsSearch(keyword, status, type, zoneId, pageIndex, pageSize, ref totalRow);
            }
            return returnValue;
        }
        public static BreakingNewsEntity BreakingNewsGetById(int id)
        {
            BreakingNewsEntity returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.BreakingNewsMainDal.BreakingNewsGetById(id);
            }
            return returnValue;
        }
        public static bool BreakingNewsInsert(BreakingNewsEntity breakingNewsEntity, string tagIdList, ref int id)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.BreakingNewsMainDal.BreakingNewsInsert(breakingNewsEntity, tagIdList, ref id);
            }
            return returnValue;
        }
        public static bool BreakingNewsUpdate(BreakingNewsEntity breakingNewsEntity, string tagIdList)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.BreakingNewsMainDal.BreakingNewsUpdate(breakingNewsEntity, tagIdList);
            }
            return returnValue;
        }
        public static bool BreakingNewsDelete(int id)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.BreakingNewsMainDal.BreakingNewsDelete(id);
            }
            return returnValue;
        }
        #endregion

        #region BreakingNewsLocation
        public static List<BreakingNewsLocationEntity> BreakingNewsLocationGetAll(string keyword, int status)
        {
            List<BreakingNewsLocationEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.BreakingNewsMainDal.BreakingNewsLocationGetAll(keyword, status);
            }
            return returnValue;
        }
        public static BreakingNewsLocationEntity BreakingNewsLocationGetById(int id)
        {
            BreakingNewsLocationEntity returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.BreakingNewsMainDal.BreakingNewsLocationGetById(id);
            }
            return returnValue;
        }
        public static bool BreakingNewsLocationInsert(BreakingNewsLocationEntity BreakingNewsLocationEntity)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.BreakingNewsMainDal.BreakingNewsLocationInsert(BreakingNewsLocationEntity);
            }
            return returnValue;
        }
        public static bool BreakingNewsLocationUpdate(BreakingNewsLocationEntity BreakingNewsLocationEntity)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.BreakingNewsMainDal.BreakingNewsLocationUpdate(BreakingNewsLocationEntity);
            }
            return returnValue;
        }
        public static bool BreakingNewsLocationDelete(int id)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.BreakingNewsMainDal.BreakingNewsLocationDelete(id);
            }
            return returnValue;
        }

        public static bool BreakingNewsLocationMoveUp(int id)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.BreakingNewsMainDal.BreakingNewsLocationMoveUp(id);
            }
            return returnValue;
        }
        public static bool BreakingNewsLocationMoveDown(int id)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.BreakingNewsMainDal.BreakingNewsLocationMoveDown(id);
            }
            return returnValue;
        }
        #endregion
    }

}
