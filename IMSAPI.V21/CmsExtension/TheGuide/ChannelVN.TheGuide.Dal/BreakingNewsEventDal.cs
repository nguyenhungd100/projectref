﻿
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using ChannelVN.CMS.Common;
using ChannelVN.TheGuide.Dal.Common;
using ChannelVN.TheGuide.Entity;
using ChannelVN.TheGuide.MainDal.Databases;

namespace ChannelVN.TheGuide.Dal
{
    public class BreakingNewsEventDal
    {
        #region BreakingNewsEvent
        public static BreakingNewsEventEntity BreakingNewsEventGetById(int id)
        {
            BreakingNewsEventEntity returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.BreakingNewsEventMainDal.BreakingNewsEventGetById(id);
            }
            return returnValue;
        }
        public static BreakingNewsEventEntity BreakingNewsEventGetByBreakingNewsId(int id)
        {
            BreakingNewsEventEntity returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.BreakingNewsEventMainDal.BreakingNewsEventGetByBreakingNewsId(id);
            }
            return returnValue;
        }
        public static bool BreakingNewsEventInsert(BreakingNewsEventEntity breakingNewsEventEntity)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.BreakingNewsEventMainDal.BreakingNewsEventInsert(breakingNewsEventEntity);
            }
            return returnValue;
        }
        public static bool BreakingNewsEventUpdate(BreakingNewsEventEntity breakingNewsEventEntity)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.BreakingNewsEventMainDal.BreakingNewsEventUpdate(breakingNewsEventEntity);
            }
            return returnValue;
        }
        #endregion

        #region BreakingNewsEventLocation
        public static List<BreakingNewsEventLocationEntity> BreakingNewsEventLocationGetAll(string keyword, int status, int breakingNewsLocationId)
        {
            List<BreakingNewsEventLocationEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.BreakingNewsEventMainDal.BreakingNewsEventLocationGetAll(keyword, status, breakingNewsLocationId);
            }
            return returnValue;
        }
        public static BreakingNewsEventLocationEntity BreakingNewsEventLocationGetById(int id)
        {
            BreakingNewsEventLocationEntity returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.BreakingNewsEventMainDal.BreakingNewsEventLocationGetById(id);
            }
            return returnValue;
        }
        public static bool BreakingNewsEventLocationInsert(BreakingNewsEventLocationEntity breakingNewsEventLocationEntity)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.BreakingNewsEventMainDal.BreakingNewsEventLocationInsert(breakingNewsEventLocationEntity);
            }
            return returnValue;
        }
        public static bool BreakingNewsEventLocationUpdate(BreakingNewsEventLocationEntity breakingNewsEventLocationEntity)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.BreakingNewsEventMainDal.BreakingNewsEventLocationUpdate(breakingNewsEventLocationEntity);
            }
            return returnValue;
        }
        public static bool BreakingNewsEventLocationDelete(int id)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.BreakingNewsEventMainDal.BreakingNewsEventLocationDelete(id);
            }
            return returnValue;
        }

        public static bool BreakingNewsEventLocationMoveUp(int id)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.BreakingNewsEventMainDal.BreakingNewsEventLocationMoveUp(id);
            }
            return returnValue;
        }
        public static bool BreakingNewsEventLocationMoveDown(int id)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.BreakingNewsEventMainDal.BreakingNewsEventLocationMoveDown(id);
            }
            return returnValue;
        }
        #endregion

        #region BreakingNewsEventType
        public static List<BreakingNewsEventTypeEntity> BreakingNewsEventTypeGetAll(string keyword, int status)
        {
            List<BreakingNewsEventTypeEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.BreakingNewsEventMainDal.BreakingNewsEventTypeGetAll(keyword, status);
            }
            return returnValue;
        }
        public static BreakingNewsEventTypeEntity BreakingNewsEventTypeGetById(int id)
        {
            BreakingNewsEventTypeEntity returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.BreakingNewsEventMainDal.BreakingNewsEventTypeGetById(id);
            }
            return returnValue;
        }
        public static bool BreakingNewsEventTypeInsert(BreakingNewsEventTypeEntity breakingNewsEventLocationEntity)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.BreakingNewsEventMainDal.BreakingNewsEventTypeInsert(breakingNewsEventLocationEntity);
            }
            return returnValue;
        }
        public static bool BreakingNewsEventTypeUpdate(BreakingNewsEventTypeEntity breakingNewsEventLocationEntity)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.BreakingNewsEventMainDal.BreakingNewsEventTypeUpdate(breakingNewsEventLocationEntity);
            }
            return returnValue;
        }
        public static bool BreakingNewsEventTypeDelete(int id)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.BreakingNewsEventMainDal.BreakingNewsEventTypeDelete(id);
            }
            return returnValue;
        }

        public static bool BreakingNewsEventTypeMoveUp(int id)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.BreakingNewsEventMainDal.BreakingNewsEventTypeMoveUp(id);
            }
            return returnValue;
        }
        public static bool BreakingNewsEventTypeMoveDown(int id)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.BreakingNewsEventMainDal.BreakingNewsEventTypeMoveDown(id);
            }
            return returnValue;
        }
        #endregion
    }

}
