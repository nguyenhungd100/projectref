﻿
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using ChannelVN.CMS.Common;
using ChannelVN.TheGuide.Dal.Common;
using ChannelVN.TheGuide.Entity;
using ChannelVN.TheGuide.MainDal.Databases;

namespace ChannelVN.TheGuide.Dal
{
    public class BreakingNewsPromotionDal
    {
        public static BreakingNewsPromotionEntity BreakingNewsPromotionGetById(int id)
        {
            BreakingNewsPromotionEntity returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.BreakingNewsPromotionMainDal.BreakingNewsPromotionGetById(id);
            }
            return returnValue;
        }
        public static BreakingNewsPromotionEntity BreakingNewsPromotionGetByBreakingNewsId(int id)
        {
            BreakingNewsPromotionEntity returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.BreakingNewsPromotionMainDal.BreakingNewsPromotionGetByBreakingNewsId(id);
            }
            return returnValue;
        }
        public static bool BreakingNewsPromotionInsert(BreakingNewsPromotionEntity breakingNewsPromotionEntity)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.BreakingNewsPromotionMainDal.BreakingNewsPromotionInsert(breakingNewsPromotionEntity);
            }
            return returnValue;
        }
        public static bool BreakingNewsPromotionUpdate(BreakingNewsPromotionEntity breakingNewsPromotionEntity)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.BreakingNewsPromotionMainDal.BreakingNewsPromotionUpdate(breakingNewsPromotionEntity);
            }
            return returnValue;
        }
        
        #region BreakingNewsPromotionBusinessLocation
        public static List<BreakingNewsPromotionBusinessLocationEntity> BreakingNewsPromotionBusinessLocationGetAll(string keyword, int status, int breakingNewsLocationId)
        {
            List<BreakingNewsPromotionBusinessLocationEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.BreakingNewsPromotionMainDal.BreakingNewsPromotionBusinessLocationGetAll(keyword, status, breakingNewsLocationId);
            }
            return returnValue;
        }
        public static BreakingNewsPromotionBusinessLocationEntity BreakingNewsPromotionBusinessLocationGetById(int id)
        {
            BreakingNewsPromotionBusinessLocationEntity returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.BreakingNewsPromotionMainDal.BreakingNewsPromotionBusinessLocationGetById(id);
            }
            return returnValue;
        }
        public static bool BreakingNewsPromotionBusinessLocationInsert(BreakingNewsPromotionBusinessLocationEntity breakingNewsPromotionLocationEntity)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.BreakingNewsPromotionMainDal.BreakingNewsPromotionBusinessLocationInsert(breakingNewsPromotionLocationEntity);
            }
            return returnValue;
        }
        public static bool BreakingNewsPromotionBusinessLocationUpdate(BreakingNewsPromotionBusinessLocationEntity breakingNewsPromotionLocationEntity)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.BreakingNewsPromotionMainDal.BreakingNewsPromotionBusinessLocationUpdate(breakingNewsPromotionLocationEntity);
            }
            return returnValue;
        }
        public static bool BreakingNewsPromotionBusinessLocationDelete(int id)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.BreakingNewsPromotionMainDal.BreakingNewsPromotionBusinessLocationDelete(id);
            }
            return returnValue;
        }
        public static bool BreakingNewsPromotionBusinessLocationMoveUp(int id)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.BreakingNewsPromotionMainDal.BreakingNewsPromotionBusinessLocationMoveUp(id);
            }
            return returnValue;
        }
        public static bool BreakingNewsPromotionBusinessLocationMoveDown(int id)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.BreakingNewsPromotionMainDal.BreakingNewsPromotionBusinessLocationMoveDown(id);
            }
            return returnValue;
        }
        #endregion

        #region BreakingNewsPromotionLocation
        public static List<BreakingNewsPromotionLocationEntity> BreakingNewsPromotionLocationGetAll(string keyword, int status)
        {
            List<BreakingNewsPromotionLocationEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.BreakingNewsPromotionMainDal.BreakingNewsPromotionLocationGetAll(keyword, status);
            }
            return returnValue;
        }
        public static BreakingNewsPromotionLocationEntity BreakingNewsPromotionLocationGetById(int id)
        {
            BreakingNewsPromotionLocationEntity returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.BreakingNewsPromotionMainDal.BreakingNewsPromotionLocationGetById(id);
            }
            return returnValue;
        }
        public static bool BreakingNewsPromotionLocationInsert(BreakingNewsPromotionLocationEntity breakingNewsPromotionLocationEntity)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.BreakingNewsPromotionMainDal.BreakingNewsPromotionLocationInsert(breakingNewsPromotionLocationEntity);
            }
            return returnValue;
        }
        public static bool BreakingNewsPromotionLocationUpdate(BreakingNewsPromotionLocationEntity breakingNewsPromotionLocationEntity)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.BreakingNewsPromotionMainDal.BreakingNewsPromotionLocationUpdate(breakingNewsPromotionLocationEntity);
            }
            return returnValue;
        }
        public static bool BreakingNewsPromotionLocationDelete(int id)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.BreakingNewsPromotionMainDal.BreakingNewsPromotionLocationDelete(id);
            }
            return returnValue;
        }

        public static bool BreakingNewsPromotionLocationMoveUp(int id)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.BreakingNewsPromotionMainDal.BreakingNewsPromotionLocationMoveUp(id);
            }
            return returnValue;
        }
        public static bool BreakingNewsPromotionLocationMoveDown(int id)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.BreakingNewsPromotionMainDal.BreakingNewsPromotionLocationMoveDown(id);
            }
            return returnValue;
        }
        #endregion

        #region BreakingNewsPromotionType
        public static List<BreakingNewsPromotionTypeEntity> BreakingNewsPromotionTypeGetAll(string keyword, int status)
        {
            List<BreakingNewsPromotionTypeEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.BreakingNewsPromotionMainDal.BreakingNewsPromotionTypeGetAll(keyword, status);
            }
            return returnValue;
        }
        public static BreakingNewsPromotionTypeEntity BreakingNewsPromotionTypeGetById(int id)
        {
            BreakingNewsPromotionTypeEntity returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.BreakingNewsPromotionMainDal.BreakingNewsPromotionTypeGetById(id);
            }
            return returnValue;
        }
        public static bool BreakingNewsPromotionTypeInsert(BreakingNewsPromotionTypeEntity breakingNewsPromotionLocationEntity)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.BreakingNewsPromotionMainDal.BreakingNewsPromotionTypeInsert(breakingNewsPromotionLocationEntity);
            }
            return returnValue;
        }
        public static bool BreakingNewsPromotionTypeUpdate(BreakingNewsPromotionTypeEntity breakingNewsPromotionLocationEntity)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.BreakingNewsPromotionMainDal.BreakingNewsPromotionTypeUpdate(breakingNewsPromotionLocationEntity);
            }
            return returnValue;
        }
        public static bool BreakingNewsPromotionTypeDelete(int id)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.BreakingNewsPromotionMainDal.BreakingNewsPromotionTypeDelete(id);
            }
            return returnValue;
        }

        public static bool BreakingNewsPromotionTypeMoveUp(int id)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.BreakingNewsPromotionMainDal.BreakingNewsPromotionTypeMoveUp(id);
            }
            return returnValue;
        }
        public static bool BreakingNewsPromotionTypeMoveDown(int id)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.BreakingNewsPromotionMainDal.BreakingNewsPromotionTypeMoveDown(id);
            }
            return returnValue;
        }
        #endregion
    }

}
