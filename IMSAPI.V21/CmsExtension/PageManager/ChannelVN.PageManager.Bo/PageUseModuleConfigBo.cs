﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Web.UI;
using ChannelVN.CMS.Common;
using ChannelVN.PageManager.Dal;
using ChannelVN.PageManager.Dal.Common;
using ChannelVN.PageManager.Entity;
using ChannelVN.PageManager.Entity.ErrorCode;

namespace ChannelVN.PageManager.Bo
{
    public class PageUseModuleConfigBo
    {
        public static List<PageUseModuleConfigEntity> GetPageUseModuleConfigByModuleIdAndPageId(int pageId, int moduleId)
        {
            return PageUseModuleConfigDal.GetPageUseModuleConfigByModuleIdAndPageId(pageId, moduleId);
        }

        public static ErrorMapping.ErrorCodes SavePageModule(int pageId, List<PageUseModuleDetailEntity> allModuleInPage)
        {
            try
            {
                // Delete All PageUserModule and PageUserModuleConfig
                PageUseModuleBo.Delete(pageId);
                for (var i = 0; i < allModuleInPage.Count; i++)
                {
                    var pageUseModuleId = 0;
                    var pageUseModuleEntity = new PageUseModuleEntity
                    {
                        Id = allModuleInPage[i].Id,
                        PageId = allModuleInPage[i].PageId,
                        PageSlotId = allModuleInPage[i].PageSlotId,
                        PageModuleId = allModuleInPage[i].PageModuleId,
                        Priority = allModuleInPage[i].Priority,
                    };
                    // Insert PageUserModule
                    PageUseModuleBo.Insert(pageUseModuleEntity, ref pageUseModuleId);
                    var moduleConfigs = allModuleInPage[i].ModuleConfigs;
                    for (var j = 0; j < moduleConfigs.Count; j++)
                    {
                        var pageUseModuleConfigEntity = new PageUseModuleConfigEntity
                        {
                            Id = moduleConfigs[j].Id,
                            PageUseModuleId = pageUseModuleId,
                            ConfigName = moduleConfigs[j].ConfigName,
                            ConfigValue = moduleConfigs[j].ConfigValue
                        };
                        var pageUseModuleConfigId = 0;
                        // Insert PageUseModuleConfig
                        Insert(pageUseModuleConfigEntity, ref pageUseModuleConfigId);
                    }
                }
                //PageBo.PublishPageUseModule(pageId,);
                return ErrorMapping.ErrorCodes.Success;
            }
            catch (Exception)
            {
                return ErrorMapping.ErrorCodes.Exception;
            }
        }

        public static ErrorMapping.ErrorCodes Insert(PageUseModuleConfigEntity pageUseModuleCnfigEntity, ref int pageUseModuleConfigId)
        {
            return PageUseModuleConfigDal.Insert(pageUseModuleCnfigEntity, ref pageUseModuleConfigId) ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.UnknowError;
        }


    }
}
