﻿using System.Collections.Generic;
using ChannelVN.PageManager.Dal;
using ChannelVN.PageManager.Entity;
namespace ChannelVN.PageManager.Bo
{
    public class PageModuleBo
    {
        public static List<PageModuleEntity> GetPageModuleByPageId(int pageId)
        {
            return PageModuleDal.GetPageModuleByPageId(pageId);
        }
    }
}
