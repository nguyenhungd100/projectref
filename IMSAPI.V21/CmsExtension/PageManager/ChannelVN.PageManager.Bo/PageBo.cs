﻿using System.Collections.Generic;
using System.Linq;
using ChannelVN.CMS.Common;
using ChannelVN.PageManager.Dal;
using ChannelVN.PageManager.Entity;
namespace ChannelVN.PageManager.Bo
{
    public class PageBo
    {
        public static List<PageEntity> GetPageByURL(string pageUrl)
        {
            return PageDal.GetPageByURL(pageUrl);
        }
        public static bool PublishPageUseModule(int pageId, string configs, string lastModifiedBy)
        {
            return PageDal.PublishPageUseModule(pageId, configs, lastModifiedBy);
        }
        public static bool PublishPageUseModuleByUrl(string pageUrl, string configs, string lastModifiedBy)
        {
            return PageDal.PublishPageUseModuleByUrl(pageUrl, configs, lastModifiedBy);
        }
        public static string GetPagePublishedConfig(int pageId)
        {
            return PageDal.GetPagePublishedConfig(pageId);
        }
    }
}
