﻿using System.Collections.Generic;
using System.Linq;
using ChannelVN.CMS.Common;
using ChannelVN.PageManager.Dal;
using ChannelVN.PageManager.Entity;
using ChannelVN.PageManager.Entity.ErrorCode;

namespace ChannelVN.PageManager.Bo
{
    public class PageUseModuleBo
    {
        public static ErrorMapping.ErrorCodes Insert(PageUseModuleEntity pageUseModuleEntity, ref int pageUseModuleEntityId)
        {
            return PageUseModuleDal.Insert(pageUseModuleEntity, ref pageUseModuleEntityId) ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.UnknowError;
        }

        public static ErrorMapping.ErrorCodes Delete(int pageUseModuleId)
        {
            return PageUseModuleDal.DeleteAllUseModuleAndUseModuleConfig(pageUseModuleId) ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.UnknowError;
        }
    }
}
