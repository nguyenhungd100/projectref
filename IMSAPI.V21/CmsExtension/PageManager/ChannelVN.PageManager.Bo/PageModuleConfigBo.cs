﻿using System.Collections.Generic;
using ChannelVN.PageManager.Dal;
using ChannelVN.PageManager.Entity;
using ChannelVN.PageManager.Entity.ErrorCode;

namespace ChannelVN.PageManager.Bo
{
    public class PageModuleConfigBo
    {
        public static List<PageModuleConfigEntity> GetPageModuleConfigByModuleId(int moduleId)
        {
            return PageModuleConfigDal.GetPageModuleConfigByModuleId(moduleId);
        }

    
    }
}
