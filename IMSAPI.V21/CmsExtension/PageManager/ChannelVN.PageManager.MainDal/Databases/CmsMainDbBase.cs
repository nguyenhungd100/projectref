﻿using System.Data;
using ChannelVN.CMS.Common.DalCommon;

namespace ChannelVN.PageManager.MainDal.Databases
{
    /// <summary>
    /// The base class for the <see cref="CmsMainDb"/> class that 
    /// represents a connection to the <c>CmsMainDb</c> database. 
    /// </summary>
    /// <remarks>
    /// Do not change this source code. Modify the CmsMainDb class
    /// if you need to add or change some functionality.
    /// </remarks>
    public abstract class CmsMainDbBase : MainDbBase
    {
        #region Store procedures

        #region Page

        private PageDal _pageMainDal;
        public PageDal PageMainDal
        {
            get { return _pageMainDal ?? (_pageMainDal = new PageDal((CmsMainDb)this)); }
        }

        #endregion

        #region PageModuleConfig

        private PageModuleConfigDal _pageModuleConfigMainDal;
        public PageModuleConfigDal PageModuleConfigMainDal
        {
            get { return _pageModuleConfigMainDal ?? (_pageModuleConfigMainDal = new PageModuleConfigDal((CmsMainDb)this)); }
        }

        #endregion

        #region PageModule

        private PageModuleDal _pageModuleMainDal;
        public PageModuleDal PageModuleMainDal
        {
            get { return _pageModuleMainDal ?? (_pageModuleMainDal = new PageModuleDal((CmsMainDb)this)); }
        }

        #endregion

        #region PageUseModuleConfig

        private PageUseModuleConfigDal _pageUseModuleConfigMainDal;
        public PageUseModuleConfigDal PageUseModuleConfigMainDal
        {
            get { return _pageUseModuleConfigMainDal ?? (_pageUseModuleConfigMainDal = new PageUseModuleConfigDal((CmsMainDb)this)); }
        }

        #endregion

        #region PageUseModule

        private PageUseModuleDal _pageUseModuleMainDal;
        public PageUseModuleDal PageUseModuleMainDal
        {
            get { return _pageUseModuleMainDal ?? (_pageUseModuleMainDal = new PageUseModuleDal((CmsMainDb)this)); }
        }

        #endregion

        #endregion

        #region Constructors

        protected CmsMainDbBase()
        {
        }
        protected CmsMainDbBase(bool init)
        {
            if (init)
            {
                InitConnection();
            }
        }

        #endregion

        #region Protected members

        protected override sealed void InitConnection()
        {
            _connection = CreateConnection();
            _connection.Open();
        }

        /// <summary>
        /// Creates a new connection to the database.
        /// </summary>
        /// <returns>A reference to the <see cref="System.Data.IDbConnection"/> object.</returns>
        protected abstract IDbConnection CreateConnection();

        #endregion
    }
}