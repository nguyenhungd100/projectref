﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using ChannelVN.CMS.Common;
using System.Data;

using ChannelVN.PageManager.Entity;
using ChannelVN.PageManager.MainDal.Databases;

namespace ChannelVN.PageManager.MainDal
{
    public abstract class PageDalBase
    {
        public List<PageEntity> GetPageByURL(string pageUrl)
        {
            const string commandText = "CMS_Page_GetByURL";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "PageUrl", pageUrl);
                var numberOfRow = _db.GetList<PageEntity>(cmd);
                return numberOfRow;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public string GetPagePublishedConfig(int pageId)
        {
            const string commandText = "CMS_Page_GetPublishedConfig";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "PageId", pageId);
                var numberOfRow = _db.GetFirtDataRecord(cmd);
                return numberOfRow.ToString();
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public bool PublishPageUseModule(int pageId, string pageUseModuleConfig)
        {
            const string commandText = "CMS_Page_PublishPageUseModule";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "PageId", pageId);
                _db.AddParameter(cmd, "PageUseModuleConfig", pageUseModuleConfig);
                var numberOfRow = _db.ExecuteNonQuery(cmd);
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public bool PublishPageUseModuleByUrl(string pageUrl, string pageUseModuleConfig)
        {
            const string commandText = "CMS_Page_PublishPageUseModuleByUrl";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "PageUrl", pageUrl);
                _db.AddParameter(cmd, "PageUseModuleConfig", pageUseModuleConfig);
                var numberOfRow = _db.ExecuteNonQuery(cmd);
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        
        #region Core members

        private readonly CmsMainDb _db;

        protected PageDalBase(CmsMainDb db)
        {
            _db = db;
        }

        protected CmsMainDb Database
        {
            get { return _db; }
        }

        #endregion
    }
}
