﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using ChannelVN.CMS.Common;
using System.Data;

using ChannelVN.PageManager.Entity;
using ChannelVN.PageManager.MainDal.Databases;

namespace ChannelVN.PageManager.MainDal
{
    public abstract class PageUseModuleDalBase
    {
        public bool Insert(PageUseModuleEntity pageUseModuleEntity, ref int pageUseModuleId)
        {
            const string commandText = "CMS_PageUseModule_Insert";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "ID", pageUseModuleEntity.Id, ParameterDirection.Output);
                _db.AddParameter(cmd, "PageId", pageUseModuleEntity.PageId);
                _db.AddParameter(cmd, "PageSlotId", pageUseModuleEntity.PageSlotId);
                _db.AddParameter(cmd, "PageModuleId", pageUseModuleEntity.PageModuleId);
                _db.AddParameter(cmd, "Priority", pageUseModuleEntity.Priority);
                var numberOfRow = _db.ExecuteNonQuery(cmd);
                pageUseModuleId = Utility.ConvertToInt(cmd.Parameters[0]);
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public bool DeleteAllUseModuleAndUseModuleConfig(int pageId)
        {
            const string commandText = "CMS_PageUseModule_Delete";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "PageId", pageId);
                var numberOfRow = _db.ExecuteNonQuery(cmd);
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public List<PageUseModuleEntity> GetByPageId(int pageId)
        {
            const string commandText = "CMS_PageUseModule_GetByPageId";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "PageId", pageId);
                var numberOfRow = _db.GetList<PageUseModuleEntity>(cmd);
                return numberOfRow;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        #region Core members

        private readonly CmsMainDb _db;

        protected PageUseModuleDalBase(CmsMainDb db)
        {
            _db = db;
        }

        protected CmsMainDb Database
        {
            get { return _db; }
        }

        #endregion
    }
}
