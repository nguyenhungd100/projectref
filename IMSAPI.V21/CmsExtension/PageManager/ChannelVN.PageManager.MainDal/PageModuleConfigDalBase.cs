﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using ChannelVN.CMS.Common;
using System.Data;

using ChannelVN.PageManager.Entity;
using ChannelVN.PageManager.MainDal.Databases;

namespace ChannelVN.PageManager.MainDal
{
    public abstract class PageModuleConfigDalBase
    {
        public List<PageModuleConfigEntity> GetPageModuleConfigByModuleId(int moduleId)
        {
            const string commandText = "CMS_PageModuleConfig_GetByModuleId";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "PageModuleId", moduleId);
                var numberOfRow = _db.GetList<PageModuleConfigEntity>(cmd);
                return numberOfRow;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        #region Core members

        private readonly CmsMainDb _db;

        protected PageModuleConfigDalBase(CmsMainDb db)
        {
            _db = db;
        }

        protected CmsMainDb Database
        {
            get { return _db; }
        }

        #endregion
    }
}
