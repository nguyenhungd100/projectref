﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using ChannelVN.CMS.Common;
using System.Data;

using ChannelVN.PageManager.Entity;
using ChannelVN.PageManager.MainDal.Databases;

namespace ChannelVN.PageManager.MainDal
{
    public abstract class PageUseModuleConfigDalBase
    {
        public List<PageUseModuleConfigEntity> GetPageUseModuleConfigByModuleIdAndPageId(int pageId, int moduleId)
        {
            const string commandText = "CMS_PageUseModuleConfig_GetByModuleIdAndPageId";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "PageId", pageId);
                _db.AddParameter(cmd, "PageModuleId", moduleId);
                var numberOfRow = _db.GetList<PageUseModuleConfigEntity>(cmd);
                return numberOfRow;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public List<PageUseModuleConfigEntity> GetByPageUseModuleId(int pageUseModuleId)
        {
            const string commandText = "CMS_PageUseModuleConfig_GetByPageUseModuleId";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "PageUseModuleId", pageUseModuleId);
                var numberOfRow = _db.GetList<PageUseModuleConfigEntity>(cmd);
                return numberOfRow;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool Insert(PageUseModuleConfigEntity pageUseModuleConfigEntity, ref int pageUseModuleConfigId)
        {
            const string commandText = "CMS_PageUseModuleConfig_Insert";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "ID", pageUseModuleConfigEntity.Id, ParameterDirection.Output);
                _db.AddParameter(cmd, "PageUseModuleId", pageUseModuleConfigEntity.PageUseModuleId);
                _db.AddParameter(cmd, "ConfigName", pageUseModuleConfigEntity.ConfigName);
                _db.AddParameter(cmd, "ConfigValue", pageUseModuleConfigEntity.ConfigValue);
                var numberOfRow = _db.ExecuteNonQuery(cmd);
                pageUseModuleConfigId = Utility.ConvertToInt(cmd.Parameters[0]);
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        #region Core members

        private readonly CmsMainDb _db;

        protected PageUseModuleConfigDalBase(CmsMainDb db)
        {
            _db = db;
        }

        protected CmsMainDb Database
        {
            get { return _db; }
        }

        #endregion
    }
}
