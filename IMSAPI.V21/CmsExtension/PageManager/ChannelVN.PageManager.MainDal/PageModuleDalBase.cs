﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using ChannelVN.CMS.Common;
using System.Data;

using ChannelVN.PageManager.Entity;
using ChannelVN.PageManager.MainDal.Databases;

namespace ChannelVN.PageManager.MainDal
{
    public abstract class PageModuleDalBase
    {
        public List<PageModuleEntity> GetPageModuleByPageId(int pageId)
        {
            const string commandText = "CMS_PageModule_GetByPageId";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "PageId", pageId);
                var numberOfRow = _db.GetList<PageModuleEntity>(cmd);
                return numberOfRow;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }


        #region Core members

        private readonly CmsMainDb _db;

        protected PageModuleDalBase(CmsMainDb db)
        {
            _db = db;
        }

        protected CmsMainDb Database
        {
            get { return _db; }
        }

        #endregion
    }
}
