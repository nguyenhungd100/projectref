﻿using System.Runtime.Serialization;
using ChannelVN.CMS.Common;

namespace ChannelVN.PageManager.Entity
{
    public class PageModuleConfigEntity : EntityBase
    {
        [DataMember]
        public int Id { get; set; } 
        [DataMember]
        public int PageModuleId { get; set; }
        [DataMember]
        public string ConfigName { get; set; }
        [DataMember]
        public string ConfigLabel { get; set; }  
        [DataMember]
        public string ConfigValue { get; set; }
        [DataMember]
        public int Status { get; set; }
    }
}
