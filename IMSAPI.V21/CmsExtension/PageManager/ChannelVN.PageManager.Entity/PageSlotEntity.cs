﻿using System.Runtime.Serialization;
using ChannelVN.CMS.Common;

namespace ChannelVN.PageManager.Entity
{
    public class PageSlotEntity : EntityBase
    {
        [DataMember]
        public int Id { get; set; } 
        [DataMember]
        public int PageId { get; set; }
        [DataMember]
        public string PageSlotName { get; set; }
        [DataMember]
        public int Status { get; set; }
    }
}
