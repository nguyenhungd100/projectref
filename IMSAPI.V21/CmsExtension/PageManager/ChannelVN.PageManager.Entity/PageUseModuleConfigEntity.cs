﻿using System.Runtime.Serialization;
using ChannelVN.CMS.Common;

namespace ChannelVN.PageManager.Entity
{
    public class PageUseModuleConfigEntity : EntityBase
    {
        [DataMember]
        public int Id { get; set; } 
        [DataMember]
        public int PageUseModuleId { get; set; }  
        [DataMember]
        public string ConfigName { get; set; }  
        [DataMember]
        public string ConfigValue { get; set; }
    }
}
