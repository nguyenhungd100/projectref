﻿using System.Runtime.Serialization;
using ChannelVN.CMS.Common;

namespace ChannelVN.PageManager.Entity
{
    public class PageEntity : EntityBase
    {
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public string PageName { get; set; }
        [DataMember]
        public string PageUrl { get; set; }
        [DataMember]
        public int Status { get; set; }
    }
}
