﻿using System.Collections.Generic;
using System.Runtime.Serialization;
using ChannelVN.CMS.Common;

namespace ChannelVN.PageManager.Entity
{
    public class PageUseModuleEntity : EntityBase
    {
        [DataMember]
        public int Id { get; set; } 
        [DataMember]
        public int PageId { get; set; }  
        [DataMember]
        public int PageSlotId { get; set; }  
        [DataMember]
        public int PageModuleId { get; set; }
        [DataMember]
        public int Priority { get; set; }
    }

    public class PageUseModuleDetailEntity : EntityBase
    {
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public int PageId { get; set; }
        [DataMember]
        public int PageSlotId { get; set; }
        [DataMember]
        public int PageModuleId { get; set; }
        [DataMember]
        public int Priority { get; set; }
        [DataMember]
        public List<PageUseModuleConfigEntity> ModuleConfigs { get; set; }
    }
}
