﻿using System.Runtime.Serialization;
using ChannelVN.CMS.Common;

namespace ChannelVN.PageManager.Entity
{
    public class PageModuleEntity : EntityBase
    {
        [DataMember]
        public int Id { get; set; } 
        [DataMember]
        public int PageSlotId { get; set; }
        [DataMember]
        public string PageModuleName { get; set; }
        [DataMember]
        public string PageModuleThumb { get; set; }
        [DataMember]
        public int Status { get; set; }
    }
}
