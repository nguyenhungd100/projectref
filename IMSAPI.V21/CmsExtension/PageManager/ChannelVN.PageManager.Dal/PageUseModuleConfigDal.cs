﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using ChannelVN.CMS.Common;
using System.Data;
using ChannelVN.PageManager.Dal.Common;
using ChannelVN.PageManager.Entity;
using ChannelVN.PageManager.MainDal.Databases;

namespace ChannelVN.PageManager.Dal
{
    public class PageUseModuleConfigDal
    {
        public static List<PageUseModuleConfigEntity> GetPageUseModuleConfigByModuleIdAndPageId(int pageId, int moduleId)
        {
            List<PageUseModuleConfigEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.PageUseModuleConfigMainDal.GetPageUseModuleConfigByModuleIdAndPageId(pageId, moduleId);
            }
            return returnValue;
        }
        public static List<PageUseModuleConfigEntity> GetByPageUseModuleId(int pageUseModuleId)
        {
            List<PageUseModuleConfigEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.PageUseModuleConfigMainDal.GetByPageUseModuleId(pageUseModuleId);
            }
            return returnValue;
        }
        public static bool Insert(PageUseModuleConfigEntity pageUseModuleConfigEntity, ref int pageUseModuleConfigId)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.PageUseModuleConfigMainDal.Insert(pageUseModuleConfigEntity, ref pageUseModuleConfigId);
            }
            return returnValue;
        }
    }
}
