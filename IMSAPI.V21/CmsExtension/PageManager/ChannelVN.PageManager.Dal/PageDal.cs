﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using ChannelVN.CMS.Common;
using System.Data;
using ChannelVN.PageManager.Dal.Common;
using ChannelVN.PageManager.Entity;
using ChannelVN.PageManager.MainDal.Databases;

namespace ChannelVN.PageManager.Dal
{
    public class PageDal
    {
        public static List<PageEntity> GetPageByURL(string pageUrl)
        {
            List<PageEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.PageMainDal.GetPageByURL(pageUrl);
            }
            return returnValue;
        }
        public static string GetPagePublishedConfig(int pageId)
        {
            string returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.PageMainDal.GetPagePublishedConfig(pageId);
            }
            return returnValue;
        }

        public static bool PublishPageUseModule(int pageId, string pageUseModuleConfig, string lastModifiedBy)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.PageMainDal.PublishPageUseModule(pageId, pageUseModuleConfig);
            }
            return returnValue;
        }
        public static bool PublishPageUseModuleByUrl(string pageUrl, string pageUseModuleConfig, string lastModifiedBy)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.PageMainDal.PublishPageUseModuleByUrl(pageUrl, pageUseModuleConfig);
            }
            return returnValue;
        }
    }
}
