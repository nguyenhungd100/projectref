﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using ChannelVN.CMS.Common;
using System.Data;
using ChannelVN.PageManager.Dal.Common;
using ChannelVN.PageManager.Entity;
using ChannelVN.PageManager.MainDal.Databases;

namespace ChannelVN.PageManager.Dal
{
    public class PageModuleDal
    {
        public static List<PageModuleEntity> GetPageModuleByPageId(int pageId)
        {
            List<PageModuleEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.PageModuleMainDal.GetPageModuleByPageId(pageId);
            }
            return returnValue;
        }

    }
}
