﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using ChannelVN.CMS.Common;
using System.Data;
using ChannelVN.PageManager.Dal.Common;
using ChannelVN.PageManager.Entity;
using ChannelVN.PageManager.MainDal.Databases;

namespace ChannelVN.PageManager.Dal
{
    public class PageModuleConfigDal
    {
        public static List<PageModuleConfigEntity> GetPageModuleConfigByModuleId(int moduleId)
        {
            List<PageModuleConfigEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.PageModuleConfigMainDal.GetPageModuleConfigByModuleId(moduleId);
            }
            return returnValue;
        }

    }
}
