﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using ChannelVN.CMS.Common;
using System.Data;
using ChannelVN.PageManager.Dal.Common;
using ChannelVN.PageManager.Entity;
using ChannelVN.PageManager.MainDal.Databases;

namespace ChannelVN.PageManager.Dal
{
    public class PageUseModuleDal
    {
        public static bool Insert(PageUseModuleEntity pageUseModuleEntity, ref int pageUseModuleId)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.PageUseModuleMainDal.Insert(pageUseModuleEntity, ref pageUseModuleId);
            }
            return returnValue;
        }

        public static bool DeleteAllUseModuleAndUseModuleConfig(int pageId)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.PageUseModuleMainDal.DeleteAllUseModuleAndUseModuleConfig(pageId);
            }
            return returnValue;
        }
        public static List<PageUseModuleEntity> GetByPageId(int pageId)
        {
            List<PageUseModuleEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.PageUseModuleMainDal.GetByPageId(pageId);
            }
            return returnValue;
        }
    }
}
