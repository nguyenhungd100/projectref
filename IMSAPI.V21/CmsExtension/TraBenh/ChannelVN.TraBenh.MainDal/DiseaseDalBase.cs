﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using ChannelVN.CMS.Common;
using System.Data;
using ChannelVN.TraBenh.Entity;
using ChannelVN.TraBenh.MainDal.Databases;


namespace ChannelVN.TraBenh.MainDal
{
    public abstract class DiseaseDalBase
    {
        #region GroupDisease
        /// <summary>
        /// Get List Group Disease By Keyword - ngocnh
        /// </summary>
        /// <param name="keyword"></param>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <param name="totalRow"></param>
        /// <returns></returns>
        public List<GroupDiseaseEntity> GetListGroupDisease(string keyword, int pageIndex, int pageSize, ref int totalRow)
        {
            const string commandText = "CMS_Group_Disease_Get_List";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Keyword", keyword);
                _db.AddParameter(cmd, "PageIndex", pageIndex);
                _db.AddParameter(cmd, "PageSize", pageSize);
                _db.AddParameter(cmd, "TotalRow", totalRow, ParameterDirection.Output);
                var numberOfRow = _db.GetList<GroupDiseaseEntity>(cmd);
                totalRow = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, cmd.Parameters.Count - 1));
                return numberOfRow;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        /// <summary>
        /// NANIA
        /// 2015-05-14
        /// Gets tất cả nhóm bệnh > để chèn vào dropdownlist
        /// </summary>
        /// <returns>List Of GroupDiseaseEntity</returns>
        public List<GroupDiseaseEntity> GroupDiseaseGetAll()
        {
            const string commandText = "CMS_GroupDisease_GetAll";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                var numberOfRow = _db.GetList<GroupDiseaseEntity>(cmd);
                return numberOfRow;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        /// <summary>
        /// Update Group Disease - ngocnh
        /// </summary>
        /// <param name="info"></param>
        /// <param name="id"></param>
        /// <returns></returns>
        public bool UpdateGroupDisease(GroupDiseaseEntity info, ref int id)
        {
            const string commandText = "CMS_GroupDisease_Update";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Name", info.Name);
                _db.AddParameter(cmd, "Description", info.Description);
                _db.AddParameter(cmd, "Order", info.Order);
                _db.AddParameter(cmd, "Id", info.Id, ParameterDirection.InputOutput);
                var numberOfRow = _db.ExecuteNonQuery(cmd);
                id = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, cmd.Parameters.Count - 1));
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        /// <summary>
        /// Get Group Disease By Id - ngocnh
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public GroupDiseaseEntity GetGroupDiseaseById(int Id)
        {
            const string commandText = "CMS_GroupDisease_GetById"; try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", Id);
                var numberOfRow = _db.Get<GroupDiseaseEntity>(cmd);
                return numberOfRow;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        /// <summary>
        /// Delete Group Disease By Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public bool DeleteGroupDiseaseById(int id)
        {
            const string commandText = "CMS_GroupDisease_DeleteById"; try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", id);
                var numberOfRow = _db.ExecuteNonQuery(cmd);
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        #endregion

        #region ExpertOnTheDisease
        /// <summary>
        /// Get List Group Disease By Keyword - ngocnh
        /// </summary>
        /// <param name="keyword"></param>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <param name="totalRow"></param>
        /// <returns></returns>
        public List<ExpertOnTheDiseaseEntity> GetListExpertOnTheDisease(string keyword, int pageIndex, int pageSize, ref int totalRow)
        {
            const string commandText = "CMS_ExpertOnTheDisease_GetListByName";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Keyword", keyword);
                _db.AddParameter(cmd, "PageIndex", pageIndex);
                _db.AddParameter(cmd, "PageSize", pageSize);
                _db.AddParameter(cmd, "TotalRow", totalRow, ParameterDirection.Output);
                var numberOfRow = _db.GetList<ExpertOnTheDiseaseEntity>(cmd);
                totalRow = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, cmd.Parameters.Count - 1));
                return numberOfRow;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        /// <summary>
        /// Update Group Disease - ngocnh
        /// </summary>
        /// <param name="info"></param>
        /// <param name="id"></param>
        /// <returns></returns>
        /// 
        public bool UpdateExpertOnTheDisease(ExpertOnTheDiseaseEntity info, ref int id)
        {
            const string commandText = "CMS_ExpertOnTheDisease_Update";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "AcademicTitle", info.AcademicTitle);
                _db.AddParameter(cmd, "Avatar", info.Avatar);
                _db.AddParameter(cmd, "DateOfBirth", info.DateOfBirth);
                _db.AddParameter(cmd, "Degrees", info.Degrees);
                _db.AddParameter(cmd, "Description", info.Description);
                _db.AddParameter(cmd, "DiseaseCount", info.DiseaseCount);
                _db.AddParameter(cmd, "Gender", info.Gender);
                _db.AddParameter(cmd, "Name", info.Name);
                _db.AddParameter(cmd, "NewsCount", info.NewsCount);
                _db.AddParameter(cmd, "ShortDescription", info.ShortDescription);
                _db.AddParameter(cmd, "Status", info.Status);
                _db.AddParameter(cmd, "Titles", info.Titles);
                _db.AddParameter(cmd, "Type", info.Type);
                _db.AddParameter(cmd, "Id", info.Id, ParameterDirection.InputOutput);
                var numberOfRow = _db.ExecuteNonQuery(cmd);
                id = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, cmd.Parameters.Count - 1));
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        /// <summary>
        /// Get Group Disease By Id - ngocnh
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ExpertOnTheDiseaseEntity GetExpertOnTheDiseaseById(int Id)
        {
            const string commandText = "CMS_ExpertOnTheDisease_GetById"; try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", Id);
                var numberOfRow = _db.Get<ExpertOnTheDiseaseEntity>(cmd);
                return numberOfRow;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        /// <summary>
        /// Delete Group Disease By Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public bool DeleteExpertOnTheDiseaseById(int id)
        {
            const string commandText = "CMS_ExpertOnTheDisease_DeleteById"; try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", id);
                var numberOfRow = _db.ExecuteNonQuery(cmd);
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        /// <summary>
        /// Ngocnh
        /// 18/05/2015
        /// Lấy toàn bộ danh sách chuyên gia
        /// </summary>
        /// <returns></returns>
        public List<ExpertOnTheDiseaseEntity> GetAllListExpertOnTheDisease()
        {
            const string commandText = "CMS_ExpertOnTheDisease_GetAll";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                var numberOfRow = _db.GetList<ExpertOnTheDiseaseEntity>(cmd);
                return numberOfRow;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        /// <summary>
        /// Get Expert In News
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ExpertOnTheDiseaseEntity GetExpertOnTheDiseaseByNewsId(long id)
        {
            const string commandText = "CMS_ExpertOnDiseaseInNews_GetByNewsId";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "NewsId", id);
                var numberOfRow = _db.Get<ExpertOnTheDiseaseEntity>(cmd);
                return numberOfRow;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        #endregion

        #region Disease

        /// <summary>
        /// NANIA
        /// 2015-05-14
        /// Gets danh sách thông tin bệnh có lọc theo điều kiện
        /// </summary>
        /// <param name="keyword">Từ khóa: search theo tên bệnh</param>
        /// <param name="status">Trạng thái</param>
        /// <param name="type">Loại bệnh</param>
        /// <param name="mode">(Vị trí trên trang)</param>
        /// <param name="groupId">Nhóm bệnh</param>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <param name="totalRow">CÓ OUTPUT</param>
        /// <returns></returns>
        public List<DiseaseEntity> DiseaseGetList(string keyword, int status, int type, int mode, int groupId, int pageIndex, int pageSize, ref int totalRow)
        {
            const string commandText = "CMS_Disease_GetByFilter";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Keyword", keyword);
                _db.AddParameter(cmd, "Status", status);
                _db.AddParameter(cmd, "Type", type);
                _db.AddParameter(cmd, "Mode", mode);
                _db.AddParameter(cmd, "GroupId", groupId);
                _db.AddParameter(cmd, "PageIndex", pageIndex);
                _db.AddParameter(cmd, "PageSize", pageSize);
                _db.AddParameter(cmd, "TotalRows", totalRow, ParameterDirection.InputOutput);
                var numberOfRow = _db.GetList<DiseaseEntity>(cmd);
                totalRow = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, cmd.Parameters.Count - 1));
                return numberOfRow;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        /// <summary>
        /// NANIA
        /// 2015-05-14
        /// Trả kết quả suggest theo từ khóa hoặc chữ cái đầu tiên của bệnh
        /// </summary>
        /// <param name="keyword"></param>
        /// <param name="firstChar"></param>
        /// <param name="top"></param>
        /// <returns></returns>
        public List<DiseaseEntity> DiseaseSuggest(string firstChar, string keyword, int top)
        {
            const string commandText = "CMS_Disease_Suggest";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "FirstChar", firstChar);
                _db.AddParameter(cmd, "Keyword", keyword);
                _db.AddParameter(cmd, "Top", top);
                var numberOfRow = _db.GetList<DiseaseEntity>(cmd);
                return numberOfRow;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        /// <summary>
        /// NANIA
        /// 2015-05-14
        /// Gets chi tiết thông tin bệnh theo DisaeseId. Đã bao gồm cả thông tin mở rộng từ DiseaseStaticInfo
        /// </summary>
        /// <param name="diseaseId"></param>
        /// <returns></returns>
        public DiseaseEntity DiseaseGetById(int diseaseId)
        {
            const string commandText = "CMS_Disease_GetById"; try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "DiseaseId", diseaseId);
                var numberOfRow = _db.Get<DiseaseEntity>(cmd);
                return numberOfRow;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        /// <summary>
        /// NANIA
        /// 2015-05-18
        /// Gets danh sách bệnh theo danh sách IDs
        /// </summary>
        /// <param name="diseaseIdList"></param>
        /// <returns></returns>
        public List<DiseaseEntity> DiseaseGetListByIds(string diseaseIdList)
        {
            const string commandText = "CMS_Disease_GetByListId";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "ListId", diseaseIdList);
                var numberOfRow = _db.GetList<DiseaseEntity>(cmd);
                return numberOfRow;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        /// <summary>
        /// NANIA
        /// 2015-05-14
        /// Gets chi tiết thông tin bệnh theo [Name]. Đã bao gồm cả thông tin mở rộng từ DiseaseStaticInfo
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public DiseaseEntity DiseaseGetByName(string name)
        {
            const string commandText = "CMS_Disease_GetByName"; try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Name", name);
                var numberOfRow = _db.Get<DiseaseEntity>(cmd);
                return numberOfRow;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        /// <summary>
        /// NANIA
        /// 2015-05-14
        /// </summary>
        /// <param name="diseaseEntity"></param>
        /// <returns></returns>
        public bool UpdateDisease(DiseaseEntity diseaseEntity)
        {
            const string commandText = "CMS_Disease_Update"; try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Name", diseaseEntity.Name);
                _db.AddParameter(cmd, "EnglishName", diseaseEntity.EnglishName);
                _db.AddParameter(cmd, "ShortName", diseaseEntity.ShortName);
                _db.AddParameter(cmd, "Description", diseaseEntity.Description);
                _db.AddParameter(cmd, "DiscoveredDate", diseaseEntity.DiscoveredDate);
                _db.AddParameter(cmd, "DiscoveredMan", diseaseEntity.DiscoveredMan);
                _db.AddParameter(cmd, "CreatedDate", diseaseEntity.CreatedDate);
                _db.AddParameter(cmd, "LastModifiedDate", diseaseEntity.LastModifiedDate);
                _db.AddParameter(cmd, "LastModifiedDateStamp", diseaseEntity.LastModifiedDateStamp);
                _db.AddParameter(cmd, "CreatedBy", diseaseEntity.CreatedBy);
                _db.AddParameter(cmd, "ModifiedBy", diseaseEntity.ModifiedBy);
                _db.AddParameter(cmd, "Status", diseaseEntity.Status);
                _db.AddParameter(cmd, "Type", diseaseEntity.Type);
                _db.AddParameter(cmd, "Mode", diseaseEntity.Mode);
                _db.AddParameter(cmd, "TitleForSeo", diseaseEntity.TitleForSeo);
                _db.AddParameter(cmd, "DescriptionForSeo", diseaseEntity.DescriptionForSeo);
                _db.AddParameter(cmd, "KeywordForSeo", diseaseEntity.KeywordForSeo);
                _db.AddParameter(cmd, "TagNames", diseaseEntity.TagNames);
                _db.AddParameter(cmd, "TagJson", diseaseEntity.TagJson);
                _db.AddParameter(cmd, "ExpertId", diseaseEntity.ExpertId);
                _db.AddParameter(cmd, "GroupId", diseaseEntity.GroupId);
                _db.AddParameter(cmd, "DiseaseRelation", diseaseEntity.DiseaseRelation);
                _db.AddParameter(cmd, "Avatar", diseaseEntity.Avatar);
                _db.AddParameter(cmd, "Overview", diseaseEntity.Overview);
                _db.AddParameter(cmd, "Symptom", diseaseEntity.Symptom);
                _db.AddParameter(cmd, "Cause", diseaseEntity.Cause);
                _db.AddParameter(cmd, "Treatment", diseaseEntity.Treatment);
                _db.AddParameter(cmd, "Preventative", diseaseEntity.Preventative);
                _db.AddParameter(cmd, "DiseaseId", diseaseEntity.DiseaseId, ParameterDirection.InputOutput);
                var numberOfRow = _db.ExecuteNonQuery(cmd);
                if (diseaseEntity.DiseaseId <= 0)
                {
                    diseaseEntity.DiseaseId = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, cmd.Parameters.Count - 1));
                }
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        /// <summary>
        /// Get Name, Id thông tin bệnh gắn vào bài - ngocnh
        /// </summary>
        /// <param name="newsId"></param>
        /// <returns></returns>
        public List<DiseaseEntity> DiseaseGetByNewsId(long newsId)
        {
            const string commandText = "CMS_Disease_GetByNewsId"; try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "NewsId", newsId);
                var numberOfRow = _db.GetList<DiseaseEntity>(cmd);
                return numberOfRow;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public List<NewsInDisease> DiseaseGetNewsDiseaseId(int diseaseId)
        {
            const string commandText = "CMS_Disease_GetNewsById"; try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "DiseaseId", diseaseId);
                var newsList = _db.GetList<NewsInDisease>(cmd);
                return newsList;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        /// <summary>
        /// Insert Bệnh vào tin theo list Id Bệnh
        /// </summary>
        /// <param name="newsId"></param>
        /// <param name="listDiseases"></param>
        /// <returns></returns>
        public bool UpdateDiseaseInNews(long newsId, string listDiseases)
        {
            const string commandText = "CMS_NewsInDisease_Update"; try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "NewsId", newsId);
                _db.AddParameter(cmd, "ListDisease", listDiseases);
                var numberOfRow = _db.ExecuteNonQuery(cmd);
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool AddNewsToDisease(int diseaseId, string listNewsId)
        {
            const string commandText = "CMS_Disease_AddNewsInDisease"; try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "DiseaseId", diseaseId);
                _db.AddParameter(cmd, "NewsIds", listNewsId);
                var numberOfRow = _db.ExecuteNonQuery(cmd);
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        /// <summary>
        /// Ngocnh
        /// 22/5/2015
        /// Cập nhật chuyên gia của bệnh vào tin
        /// </summary>
        /// <param name="newsId"></param>
        /// <param name="expertId"></param>
        /// <returns></returns>
        public bool UpdateExpertOnDiseaseInNews(long newsId, int expertId)
        {
            const string commandText = "CMS_ExpertOnDiseaseInNews_Update";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "NewsId", newsId);
                _db.AddParameter(cmd, "ExpertId", expertId);
                var numberOfRow = _db.ExecuteNonQuery(cmd);
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        #endregion

        #region Menu

        public int UpdateMenu(MenuEntity menuEntity)
        {
            const string commandText = "CMS_Menu_Update";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", menuEntity.Id, ParameterDirection.InputOutput);
                _db.AddParameter(cmd, "Name", menuEntity.Name);
                _db.AddParameter(cmd, "UnsignName", menuEntity.UnsignName);
                _db.AddParameter(cmd, "ParentId", menuEntity.ParentId);
                _db.AddParameter(cmd, "ZoneId", menuEntity.ZoneId);
                _db.AddParameter(cmd, "ObjectId", menuEntity.ObjectId);
                _db.AddParameter(cmd, "ObjectLink", menuEntity.ObjectLink);
                _db.AddParameter(cmd, "Type", menuEntity.Type);
                _db.AddParameter(cmd, "Priority", menuEntity.Priority);
                _db.AddParameter(cmd, "Status", menuEntity.Status);
                _db.AddParameter(cmd, "Icon", menuEntity.Icon);
                _db.AddParameter(cmd, "Position", menuEntity.Position);
                var numberOfRow = _db.ExecuteNonQuery(cmd);
                return numberOfRow > 0 ? Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 0)) : 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public bool DeleteMenu(int id)
        {
            const string commandText = "CMS_Menu_DeleteById";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", id);
                var numberOfRow = _db.ExecuteNonQuery(cmd);
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public List<MenuEntity> GetAllOfMenu()
        {
            const string commandText = "CMS_Menu_GetAllOfMenu";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                return _db.GetList<MenuEntity>(cmd);
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public List<MenuEntity> GetListMenuByParent(int parentId)
        {
            const string commandText = "CMS_Menu_GetListByParent";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "ParentId", parentId);
                return _db.GetList<MenuEntity>(cmd);
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public MenuEntity GetMenuDetails(int id)
        {
            const string commandText = "CMS_Menu_GetById";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", id);
                return _db.Get<MenuEntity>(cmd);
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        #endregion

        #region Core members

        private readonly CmsMainDb _db;

        protected DiseaseDalBase(CmsMainDb db)
        {
            _db = db;
        }

        protected CmsMainDb Database
        {
            get { return _db; }
        }

        #endregion
    }
}
