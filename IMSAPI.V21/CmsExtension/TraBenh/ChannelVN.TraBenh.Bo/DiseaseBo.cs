﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ChannelVN.CMS.Common;
using ChannelVN.TraBenh.Dal;
using ChannelVN.TraBenh.Entity;
using ChannelVN.TraBenh.Entity.ErrorCode;

namespace ChannelVN.TraBenh.Bo
{
    public class DiseaseBo
    {
        #region Group Disease

        /// <summary>
        /// Get List Group Disease By Keyword - ngocnh
        /// </summary>
        /// <param name="keyword"></param>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <param name="totalRow"></param>
        /// <returns></returns>
        public static List<GroupDiseaseEntity> GroupDiseaseGetList(string keyword, int pageIndex, int pageSize,
                                                                   ref int totalRow)
        {
            return DiseaseDal.GroupDiseaseGetList(keyword, pageIndex, pageSize, ref totalRow);
        }

        /// <summary>
        /// NANIA
        /// 2015-05-14
        /// Gets tất cả nhóm bệnh > để chèn vào dropdownlist
        /// </summary>
        /// <returns>List Of GroupDiseaseEntity</returns>
        public static List<GroupDiseaseEntity> GroupDiseaseGetAll()
        {
            try
            {
                return DiseaseDal.GroupDiseaseGetAll();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.ToString());
            }
        }

        /// <summary>
        /// Update Group Disease - ngocnh
        /// </summary>
        /// <param name="info"></param>
        /// <param name="id"></param>
        /// <returns></returns>
        public static ErrorMapping.ErrorCodes UpdateGroupDisease(GroupDiseaseEntity info, ref int id)
        {
            return DiseaseDal.UpdateGroupDisease(info, ref id)
                       ? ErrorMapping.ErrorCodes.Success
                       : ErrorMapping.ErrorCodes.UnknowError;
        }

        /// <summary>
        /// Get Group Disease By Id - ngocnh
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public static GroupDiseaseEntity GetGroupDiseaseById(int Id)
        {
            return DiseaseDal.GetGroupDiseaseById(Id);
        }

        /// <summary>
        /// Delete Group Disease By Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public static ErrorMapping.ErrorCodes DeleteGroupDiseaseById(int id)
        {
            return DiseaseDal.DeleteGroupDiseaseById(id)
                       ? ErrorMapping.ErrorCodes.Success
                       : ErrorMapping.ErrorCodes.UnknowError;
        }

        #endregion

        #region ExpertOnTheDisease

        /// <summary>
        /// Get List Group Disease By Keyword - ngocnh
        /// </summary>
        /// <param name="keyword"></param>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <param name="totalRow"></param>
        /// <returns></returns>
        public static List<ExpertOnTheDiseaseEntity> ExpertOnTheDiseaseGetList(string keyword, int pageIndex,
                                                                               int pageSize, ref int totalRow)
        {
            return DiseaseDal.ExpertOnTheDiseaseGetList(keyword, pageIndex, pageSize, ref totalRow);
        }

        /// <summary>
        /// Update Group Disease - ngocnh
        /// </summary>
        /// <param name="info"></param>
        /// <param name="id"></param>
        /// <returns></returns>
        public static ErrorMapping.ErrorCodes UpdateExpertOnTheDisease(ExpertOnTheDiseaseEntity info, ref int id)
        {
            return DiseaseDal.UpdateExpertOnTheDisease(info, ref id)
                       ? ErrorMapping.ErrorCodes.Success
                       : ErrorMapping.ErrorCodes.UnknowError;
        }

        /// <summary>
        /// Get Group Disease By Id - ngocnh
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public static ExpertOnTheDiseaseEntity GetExpertOnTheDiseaseById(int Id)
        {
            return DiseaseDal.GetExpertOnTheDiseaseById(Id);
        }

        /// <summary>
        /// Delete Group Disease By Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public static ErrorMapping.ErrorCodes DeleteExpertOnTheDiseaseById(int id)
        {
            return DiseaseDal.DeleteExpertOnTheDiseaseById(id)
                       ? ErrorMapping.ErrorCodes.Success
                       : ErrorMapping.ErrorCodes.UnknowError;
        }

        /// <summary>
        /// Ngocnh
        /// 18/05/2015
        /// Lấy toàn bộ danh sách chuyên gia
        /// </summary>
        /// <returns></returns>
        public static List<ExpertOnTheDiseaseEntity> GetAllListExpertOnTheDisease()
        {
            return DiseaseDal.GetAllListExpertOnTheDisease();
        }

        /// <summary>
        /// Ngocnh
        /// 18/05/2015
        /// Get Expert By NewsId
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public static ExpertOnTheDiseaseEntity GetExpertOnTheDiseaseByNewsId(long Id)
        {
            return DiseaseDal.GetExpertOnTheDiseaseByNewsId(Id);
        }

        #endregion

        #region Disease

        /// <summary>
        /// NANIA
        /// 2015-05-14
        /// Gets danh sách thông tin bệnh có lọc theo điều kiện
        /// </summary>
        /// <param name="keyword">Từ khóa: search theo tên bệnh</param>
        /// <param name="status">Trạng thái</param>
        /// <param name="type">Loại bệnh</param>
        /// <param name="mode">(Vị trí trên trang)</param>
        /// <param name="groupId">Nhóm bệnh</param>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <param name="totalRow">CÓ OUTPUT</param>
        /// <returns></returns>
        public static List<DiseaseEntity> DiseaseGetList(string keyword, int status, int type, int mode, int groupId,
                                                         int pageIndex, int pageSize, ref int totalRow)
        {
            try
            {
                return DiseaseDal.DiseaseGetList(keyword, status, type, mode, groupId, pageIndex, pageSize, ref totalRow);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.ToString());
                return null;
            }
        }

        /// <summary>
        /// NANIA
        /// 2015-05-14
        /// Trả kết quả suggest theo từ khóa hoặc chữ cái đầu tiên của bệnh
        /// </summary>
        /// <param name="firstChar"></param>
        /// <param name="keyword"></param>
        /// <param name="top"></param>
        /// <returns></returns>
        public static List<DiseaseEntity> DiseaseSuggest(string firstChar, string keyword, int top)
        {
            try
            {
                return DiseaseDal.DiseaseSuggest(firstChar, keyword, top);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.ToString());
                return null;
            }
        }

        /// <summary>
        /// NANIA
        /// 2015-05-14
        /// Gets chi tiết thông tin bệnh theo DisaeseId. Đã bao gồm cả thông tin mở rộng từ DiseaseStaticInfo
        /// </summary>
        /// <param name="diseaseId"></param>
        /// <returns></returns>
        public static DiseaseEntity DiseaseGetById(int diseaseId)
        {
            try
            {
                return DiseaseDal.DiseaseGetById(diseaseId);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.ToString());
                return null;
            }
        }

        
        /// <summary>
        /// NANIA
        /// 2015-05-18
        /// Gets danh sách bệnh theo danh sách IDs
        /// </summary>
        /// <param name="diseaseIdList"></param>
        /// <returns></returns>
        public static List<DiseaseEntity> DiseaseGetListByIds(string diseaseIdList)
        {
            try
            {
                return DiseaseDal.DiseaseGetListByIds(diseaseIdList);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.ToString());
                return null;
            }
        }

        /// <summary>
        /// NANIA
        /// 2015-05-14
        /// Gets chi tiết thông tin bệnh theo [Name]. Đã bao gồm cả thông tin mở rộng từ DiseaseStaticInfo
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public static DiseaseEntity DiseaseGetByName(string name)
        {
            try
            {
                return DiseaseDal.DiseaseGetByName(name);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.ToString());
                return null;
            }
        }


        /// <summary>
        /// NANIA
        /// 2015-05-14
        /// Cập nhật thông tin bệnh, bao gồm cả thông tin mở rộng: DiseaseStaticInfo
        /// </summary>
        /// <param name="diseaseEntity"></param>
        /// <param name="newsRelationIds"></param>
        /// <returns></returns>
        public static ErrorMapping.ErrorCodes UpdateDisease(DiseaseEntity diseaseEntity, string newsRelationIds)
        {
            ErrorMapping.ErrorCodes errorCodes;
            try
            {
                var existsDisease = DiseaseDal.DiseaseGetByName(diseaseEntity.Name);
                if (existsDisease != null && existsDisease.DiseaseId != diseaseEntity.DiseaseId)
                {
                    errorCodes = ErrorMapping.ErrorCodes.UpdateDiseaseExists;
                }
                else
                {
                    errorCodes = DiseaseDal.UpdateDisease(diseaseEntity)
                                     ? ErrorMapping.ErrorCodes.Success
                                     : ErrorMapping.ErrorCodes.UnknowError;
                    if (errorCodes == ErrorMapping.ErrorCodes.Success && !string.IsNullOrEmpty(newsRelationIds))
                    {
                        try
                        {
                            DiseaseDal.AddNewsToDisease(diseaseEntity.DiseaseId, newsRelationIds);
                        }
                        catch (Exception ex)
                        {
                            Logger.WriteLog(Logger.LogType.Error, ex.ToString());
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                errorCodes = ErrorMapping.ErrorCodes.BusinessError;
                Logger.WriteLog(Logger.LogType.Error, ex.ToString());
            }
            return errorCodes;
        }

        /// <summary>
        /// Get Name, Id thông tin bệnh gắn vào bài - ngocnh
        /// </summary>
        /// <param name="newsId"></param>
        /// <returns></returns>
        public static List<DiseaseEntity> DiseaseGetByNewsId(long newsId)
        {
            try
            {
                return DiseaseDal.DiseaseGetByNewsId(newsId);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.ToString());
                return null;
            }
        }

        public static List<NewsInDisease> DiseaseGetNewsDiseaseId(int diseaseId)
        {
            try
            {
                return DiseaseDal.DiseaseGetNewsDiseaseId(diseaseId);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.ToString());
                return null;
            }
        }

        /// <summary>
        /// Insert Bệnh vào tin theo list Id Bệnh
        /// </summary>
        /// <param name="newsId"></param>
        /// <param name="listDiseases"></param>
        /// <returns></returns>
        public static ErrorMapping.ErrorCodes UpdateDiseaseInNews(long newsId, string listDiseases)
        {
            ErrorMapping.ErrorCodes errorCodes;
            try
            {
                errorCodes = DiseaseDal.UpdateDiseaseInNews(newsId, listDiseases)
                                     ? ErrorMapping.ErrorCodes.Success
                                     : ErrorMapping.ErrorCodes.UnknowError;
            }
            catch (Exception ex)
            {
                errorCodes = ErrorMapping.ErrorCodes.BusinessError;
                Logger.WriteLog(Logger.LogType.Error, ex.ToString());
            }
            return errorCodes;
        }

        /// <summary>
        /// Ngocnh
        /// 22/5/2015
        /// Cập nhật chuyên gia của bệnh vào tin
        /// </summary>
        /// <param name="newsId"></param>
        /// <param name="expertId"></param>
        /// <returns></returns>
        public static ErrorMapping.ErrorCodes UpdateExpertOnDiseaseInNews(long newsId, int expertId)
        {
            ErrorMapping.ErrorCodes errorCodes;
            try
            {
                errorCodes = DiseaseDal.UpdateExpertOnDiseaseInNews(newsId, expertId)
                                     ? ErrorMapping.ErrorCodes.Success
                                     : ErrorMapping.ErrorCodes.UnknowError;
            }
            catch (Exception ex)
            {
                errorCodes = ErrorMapping.ErrorCodes.BusinessError;
                Logger.WriteLog(Logger.LogType.Error, ex.ToString());
            }
            return errorCodes;
        }


        #endregion

        #region Menu

        public static int UpdateMenu(MenuEntity menuEntity)
        {
            try
            {
                return DiseaseDal.UpdateMenu(menuEntity);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.ToString());
                return 0;
            }
        }
        public static ErrorMapping.ErrorCodes DeleteMenu(int id)
        {
            ErrorMapping.ErrorCodes errorCodes;
            try
            {
                errorCodes = DiseaseDal.DeleteMenu(id)
                                     ? ErrorMapping.ErrorCodes.Success
                                     : ErrorMapping.ErrorCodes.UnknowError;
            }
            catch (Exception ex)
            {
                errorCodes = ErrorMapping.ErrorCodes.BusinessError;
                Logger.WriteLog(Logger.LogType.Error, ex.ToString());
            }
            return errorCodes;
        }
        public static List<MenuEntity> GetAllOfMenu()
        {
            try
            {
                return DiseaseDal.GetAllOfMenu();
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.ToString());
                return null;
            }
        }
        public static List<MenuEntity> GetListMenuByParent(int parentId)
        {
            try
            {
                return DiseaseDal.GetListMenuByParent(parentId);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.ToString());
                return null;
            }
        }
        public static MenuEntity GetMenuDetails(int id)
        {
            try
            {
                return DiseaseDal.GetMenuDetails(id);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.ToString());
                return null;
            }
        }

        #endregion
    }
}
