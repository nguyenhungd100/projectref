﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using ChannelVN.CMS.Common;

namespace ChannelVN.TraBenh.Entity
{

    /// <summary>
    /// Thông tin tĩnh về bệnh: Tổng quan, nguyên nhân, triệu chứng,...
    /// </summary>
    [DataContract]
    public class DiseaseStaticInfoEntity : EntityBase
    {
        /// <summary>
        /// ID của bệnh
        /// </summary>
        [DataMember]
        public int DiseaseId { get; set; }

        /// <summary>
        /// Tổng quan
        /// </summary>
        [DataMember]
        public string Overview { get; set; }

        /// <summary>
        /// Triệu chứng
        /// </summary>
        [DataMember]
        public string Symptom { get; set; }

        /// <summary>
        /// Nguyên nhân
        /// </summary>
        [DataMember]
        public string Cause { get; set; }

        /// <summary>
        /// Điều trị
        /// </summary>
        [DataMember]
        public string Treatment { get; set; }

        /// <summary>
        /// Phòng ngừa
        /// </summary>
        [DataMember]
        public string Preventative { get; set; }

    }

    /// <summary>
    /// Nhóm bệnh
    /// </summary>
    [DataContract]
    public class GroupDiseaseEntity : EntityBase
    {
        [DataMember]
        public int Id { get; set; }

        [DataMember]
        public string Name { get; set; }

        [DataMember]
        public string Description { get; set; }
        [DataMember]
        public int Order { get; set; }
    }

    /// <summary>
    /// Thông tin bệnh
    /// </summary>
    [DataContract]
    public class DiseaseEntity : EntityBase
    {
        [DataMember]
        public int DiseaseId { get; set; }
        [DataMember]
        public string Name { get; set; }
        [DataMember]
        public string EnglishName { get; set; }
        [DataMember]
        public string ShortName { get; set; }
        [DataMember]
        public string Description { get; set; }
        [DataMember]
        public DateTime DiscoveredDate { get; set; }
        [DataMember]
        public string DiscoveredMan { get; set; }
        [DataMember]
        public DateTime CreatedDate { get; set; }
        [DataMember]
        public DateTime LastModifiedDate { get; set; }
        [DataMember]
        public long LastModifiedDateStamp { get; set; }
        [DataMember]
        public string CreatedBy { get; set; }
        [DataMember]
        public string ModifiedBy { get; set; }
        [DataMember]
        public int Status { get; set; }
        [DataMember]
        public int Type { get; set; }
        [DataMember]
        public int Mode { get; set; }
        [DataMember]
        public string TitleForSeo { get; set; }
        [DataMember]
        public string DescriptionForSeo { get; set; }
        [DataMember]
        public string KeywordForSeo { get; set; }
        [DataMember]
        public string TagNames { get; set; }
        [DataMember]
        public string TagJson { get; set; }
        [DataMember]
        public int ExpertId { get; set; }
        [DataMember]
        public int GroupId { get; set; }
        [DataMember]
        public string DiseaseRelation { get; set; }
        [DataMember]
        public string Avatar { get; set; }

        #region DiseaseStaticInfo

        [DataMember]
        public string Overview { get; set; }
        [DataMember]
        public string Symptom { get; set; }
        [DataMember]
        public string Cause { get; set; }
        [DataMember]
        public string Treatment { get; set; }
        [DataMember]
        public string Preventative { get; set; }

        #endregion
    }

    [DataContract]
    public class ExpertOnTheDiseaseEntity : EntityBase
    {
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public string Name { get; set; }
        [DataMember]
        public string Titles { get; set; }
        [DataMember]
        public string Avatar { get; set; }
        [DataMember]
        public DateTime DateOfBirth { get; set; }
        [DataMember]
        public int Gender { get; set; }
        [DataMember]
        public string ShortDescription { get; set; }
        [DataMember]
        public string Description { get; set; }
        [DataMember]
        public string Degrees { get; set; }
        [DataMember]
        public string AcademicTitle { get; set; }
        [DataMember]
        public int Type { get; set; }
        [DataMember]
        public int Status { get; set; }
        [DataMember]
        public int NewsCount { get; set; }
        [DataMember]
        public int DiseaseCount { get; set; }
    }

    [DataContract]
    public class NewsInDisease : EntityBase
    {
        [DataMember]
        public long NewsId { get; set; }
        [DataMember]
        public string Title { get; set; }
    }

    [DataContract]
    public class MenuEntity : EntityBase
    {
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public string Name { get; set; }
        [DataMember]
        public string UnsignName { get; set; }
        [DataMember]
        public int ParentId { get; set; }
        [DataMember]
        public int ZoneId { get; set; }
        [DataMember]
        public long ObjectId { get; set; }
        [DataMember]
        public string ObjectLink { get; set; }
        [DataMember]
        public int Status { get; set; }
        [DataMember]
        public string Icon { get; set; }
        [DataMember]
        public int Priority { get; set; }
        [DataMember]
        public int Type { get; set; }
        [DataMember]
        public int Position { get; set; }

        public enum EnumType
        {
            Menu = 0, // default
            Disease = 1, // Link tới thông tin bệnh
            Tag = 2, // Link tới thông tin tag
            News = 3, // Link tới thông tin bài
        }

        public enum EnumStatus
        {
            Hide = 0, // default - ẩn
            Show = 1
        }

        public enum EnumPosition
        {
            Top = 0, //Default: Horizontal menu
            Left = 1, // Position at left and Vertical menu
        }
    }
}
