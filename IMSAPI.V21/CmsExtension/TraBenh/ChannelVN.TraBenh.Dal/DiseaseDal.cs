﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ChannelVN.CMS.Common;
using ChannelVN.TraBenh.Dal.Common;
using ChannelVN.TraBenh.Entity;
using ChannelVN.TraBenh.MainDal.Databases;

namespace ChannelVN.TraBenh.Dal
{
    public class DiseaseDal
    {
        #region GroupDisease
        /// <summary>
        /// Get List Group Disease By Keyword - ngocnh
        /// </summary>
        /// <param name="keyword"></param>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <param name="totalRow"></param>
        /// <returns></returns>
        public static List<GroupDiseaseEntity> GroupDiseaseGetList(string keyword, int pageIndex, int pageSize, ref int totalRow)
        {
            List<GroupDiseaseEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.DiseaseMainDal.GetListGroupDisease(keyword, pageIndex, pageSize, ref totalRow);
            }
            return returnValue;
        }


        /// <summary>
        /// NANIA
        /// 2015-05-14
        /// Gets tất cả nhóm bệnh > để chèn vào dropdownlist
        /// </summary>
        /// <returns>List Of GroupDiseaseEntity</returns>
        public static List<GroupDiseaseEntity> GroupDiseaseGetAll()
        {
            List<GroupDiseaseEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.DiseaseMainDal.GroupDiseaseGetAll();
            }
            return returnValue;
        }

        /// <summary>
        /// Update Group Disease - ngocnh
        /// </summary>
        /// <param name="info"></param>
        /// <param name="id"></param>
        /// <returns></returns>
        public static bool UpdateGroupDisease(GroupDiseaseEntity info, ref int id)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.DiseaseMainDal.UpdateGroupDisease(info, ref id);
            }
            return returnValue;
        }
        /// <summary>
        /// Get Group Disease By Id - ngocnh
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public static GroupDiseaseEntity GetGroupDiseaseById(int Id)
        {
            GroupDiseaseEntity returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.DiseaseMainDal.GetGroupDiseaseById(Id);
            }
            return returnValue;
        }
        /// <summary>
        /// Delete Group Disease By Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public static bool DeleteGroupDiseaseById(int id)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.DiseaseMainDal.DeleteGroupDiseaseById(id);
            }
            return returnValue;
        }
        #endregion

        #region ExpertOnTheDisease
        /// <summary>
        /// Get List Group Disease By Keyword - ngocnh
        /// </summary>
        /// <param name="keyword"></param>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <param name="totalRow"></param>
        /// <returns></returns>
        public static List<ExpertOnTheDiseaseEntity> ExpertOnTheDiseaseGetList(string keyword, int pageIndex, int pageSize, ref int totalRow)
        {
            List<ExpertOnTheDiseaseEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.DiseaseMainDal.GetListExpertOnTheDisease(keyword, pageIndex, pageSize, ref totalRow);
            }
            return returnValue;
        }
        /// <summary>
        /// Update Group Disease - ngocnh
        /// </summary>
        /// <param name="info"></param>
        /// <param name="id"></param>
        /// <returns></returns>
        public static bool UpdateExpertOnTheDisease(ExpertOnTheDiseaseEntity info, ref int id)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.DiseaseMainDal.UpdateExpertOnTheDisease(info, ref id);
            }
            return returnValue;
        }
        /// <summary>
        /// Get Group Disease By Id - ngocnh
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public static ExpertOnTheDiseaseEntity GetExpertOnTheDiseaseById(int Id)
        {
            ExpertOnTheDiseaseEntity returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.DiseaseMainDal.GetExpertOnTheDiseaseById(Id);
            }
            return returnValue;
        }
        /// <summary>
        /// Delete Group Disease By Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public static bool DeleteExpertOnTheDiseaseById(int id)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.DiseaseMainDal.DeleteExpertOnTheDiseaseById(id);
            }
            return returnValue;
        }
        /// <summary>
        /// Ngocnh
        /// 18/05/2015
        /// Lấy toàn bộ danh sách chuyên gia
        /// </summary>
        /// <returns></returns>
        public static List<ExpertOnTheDiseaseEntity> GetAllListExpertOnTheDisease()
        {
            List<ExpertOnTheDiseaseEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.DiseaseMainDal.GetAllListExpertOnTheDisease();
            }
            return returnValue;
        }
        /// <summary>
        /// Get Expert By NewsId - ngocnh
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public static ExpertOnTheDiseaseEntity GetExpertOnTheDiseaseByNewsId(long Id)
        {
            ExpertOnTheDiseaseEntity returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.DiseaseMainDal.GetExpertOnTheDiseaseByNewsId(Id);
            }
            return returnValue;
        }
        #endregion

        #region Disease

        /// <summary>
        /// NANIA
        /// 2015-05-14
        /// Cập nhật thông tin bệnh
        /// </summary>
        /// <param name="diseaseEntity"></param>
        /// <returns></returns>
        public static bool UpdateDisease(DiseaseEntity diseaseEntity)
        {
            bool returnValue;
            using (CmsMainDb db = new CmsMainDb())
            {
                returnValue = db.DiseaseMainDal.UpdateDisease(diseaseEntity);
            }
            return returnValue;
        }

        /// <summary>
        /// NANIA
        /// 2015-05-14
        /// Gets danh sách thông tin bệnh có lọc theo điều kiện
        /// </summary>
        /// <param name="keyword">Từ khóa: search theo tên bệnh</param>
        /// <param name="status">Trạng thái</param>
        /// <param name="type">Loại bệnh</param>
        /// <param name="mode">(Vị trí trên trang)</param>
        /// <param name="groupId">Nhóm bệnh</param>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <param name="totalRow">CÓ OUTPUT</param>
        /// <returns></returns>
        public static List<DiseaseEntity> DiseaseGetList(string keyword, int status, int type, int mode, int groupId, int pageIndex, int pageSize, ref int totalRow)
        {
            try
            {
                List<DiseaseEntity> returnValue;
                using (var db = new CmsMainDb())
                {
                    returnValue = db.DiseaseMainDal.DiseaseGetList(keyword, status, type, mode, groupId, pageIndex,
                        pageSize, ref totalRow);
                }
                return returnValue;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.ToString());
            }
        }

        /// <summary>
        /// NANIA
        /// 2015-05-14
        /// Trả kết quả suggest theo từ khóa hoặc chữ cái đầu tiên của bệnh
        /// </summary>
        /// <param name="firstChar"></param>
        /// <param name="keyword"></param>
        /// <param name="top"></param>
        /// <returns></returns>
        public static List<DiseaseEntity> DiseaseSuggest(string firstChar, string keyword, int top)
        {
            try
            {
                List<DiseaseEntity> returnValue;
                using (var db = new CmsMainDb())
                {
                    returnValue = db.DiseaseMainDal.DiseaseSuggest(firstChar, keyword, top);
                }
                return returnValue;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.ToString());
            }
        }

        /// <summary>
        /// NANIA
        /// 2015-05-14
        /// Gets chi tiết thông tin bệnh theo DisaeseId. Đã bao gồm cả thông tin mở rộng từ DiseaseStaticInfo
        /// </summary>
        /// <param name="diseaseId"></param>
        /// <returns></returns>
        public static DiseaseEntity DiseaseGetById(int diseaseId)
        {
            try
            {
                DiseaseEntity returnValue;
                using (var db = new CmsMainDb())
                {
                    returnValue = db.DiseaseMainDal.DiseaseGetById(diseaseId);
                }
                return returnValue;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.ToString());
            }
        }

        /// <summary>
        /// NANIA
        /// 2015-05-18
        /// Gets danh sách bệnh theo danh sách IDs
        /// </summary>
        /// <param name="diseaseIdList"></param>
        /// <returns></returns>
        public static List<DiseaseEntity> DiseaseGetListByIds(string diseaseIdList)
        {
            try
            {
                List<DiseaseEntity> returnValue;
                using (var db = new CmsMainDb())
                {
                    returnValue = db.DiseaseMainDal.DiseaseGetListByIds(diseaseIdList);
                }
                return returnValue;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.ToString());
            }
        }

        /// <summary>
        /// NANIA
        /// 2015-05-14
        /// Gets chi tiết thông tin bệnh theo DisaeseId. Đã bao gồm cả thông tin mở rộng từ DiseaseStaticInfo
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public static DiseaseEntity DiseaseGetByName(string name)
        {
            try
            {
                DiseaseEntity returnValue;
                using (var db = new CmsMainDb())
                {
                    returnValue = db.DiseaseMainDal.DiseaseGetByName(name);
                }
                return returnValue;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.ToString());
            }
        }

        /// <summary>
        /// Get Name, Id thông tin bệnh gắn vào bài - ngocnh
        /// </summary>
        /// <param name="newsId"></param>
        /// <returns></returns>
        public static List<DiseaseEntity> DiseaseGetByNewsId(long newsId)
        {
            try
            {
                List<DiseaseEntity> returnValue;
                using (var db = new CmsMainDb())
                {
                    returnValue = db.DiseaseMainDal.DiseaseGetByNewsId(newsId);
                }
                return returnValue;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.ToString());
            }
        }

        public static List<NewsInDisease> DiseaseGetNewsDiseaseId(int diseaseId)
        {
            try
            {
                List<NewsInDisease> returnValue;
                using (var db = new CmsMainDb())
                {
                    returnValue = db.DiseaseMainDal.DiseaseGetNewsDiseaseId(diseaseId);
                }
                return returnValue;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.ToString());
            }
        }

        /// <summary>
        /// Insert Bệnh vào tin theo list Id Bệnh
        /// </summary>
        /// <param name="newsId"></param>
        /// <param name="listDiseases"></param>
        /// <returns></returns>
        public static bool UpdateDiseaseInNews(long newsId, string listDiseases)
        {
            bool returnValue;
            using (CmsMainDb db = new CmsMainDb())
            {
                returnValue = db.DiseaseMainDal.UpdateDiseaseInNews(newsId, listDiseases);
            }
            return returnValue;
        }
        public static bool AddNewsToDisease(int diseaseId, string listNewsId)
        {
            bool returnValue;
            using (CmsMainDb db = new CmsMainDb())
            {
                returnValue = db.DiseaseMainDal.AddNewsToDisease(diseaseId, listNewsId);
            }
            return returnValue;
        }
        /// <summary>
        /// Ngocnh
        /// 22/5/2015
        /// Cập nhật chuyên gia của bệnh vào tin
        /// </summary>
        /// <param name="newsId"></param>
        /// <param name="expertId"></param>
        /// <returns></returns>
        public static bool UpdateExpertOnDiseaseInNews(long newsId, int expertId)
        {
            bool returnValue;
            using (CmsMainDb db = new CmsMainDb())
            {
                returnValue = db.DiseaseMainDal.UpdateExpertOnDiseaseInNews(newsId, expertId);
            }
            return returnValue;
        }

        #endregion

        #region Menu

        public static int UpdateMenu(MenuEntity menuEntity)
        {
            int returnValue;
            using (CmsMainDb db = new CmsMainDb())
            {
                returnValue = db.DiseaseMainDal.UpdateMenu(menuEntity);
            }
            return returnValue;
        }

        public static bool DeleteMenu(int id)
        {
            bool returnValue;
            using (CmsMainDb db = new CmsMainDb())
            {
                returnValue = db.DiseaseMainDal.DeleteMenu(id);
            }
            return returnValue;
        }

        public static List<MenuEntity> GetAllOfMenu()
        {
            try
            {
                List<MenuEntity> returnValue;
                using (var db = new CmsMainDb())
                {
                    returnValue = db.DiseaseMainDal.GetAllOfMenu();
                }
                return returnValue;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.ToString());
            }
        }

        public static List<MenuEntity> GetListMenuByParent(int parentId)
        {
            try
            {
                List<MenuEntity> returnValue;
                using (var db = new CmsMainDb())
                {
                    returnValue = db.DiseaseMainDal.GetListMenuByParent(parentId);
                }
                return returnValue;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.ToString());
            }
        }

        public static MenuEntity GetMenuDetails(int id)
        {
            try
            {
                MenuEntity returnValue;
                using (var db = new CmsMainDb())
                {
                    returnValue = db.DiseaseMainDal.GetMenuDetails(id);
                }
                return returnValue;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.ToString());
            }
        }

        #endregion
    }
}
