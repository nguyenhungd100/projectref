﻿using ChannelVN.CMS.Common;
using ChannelVN.DiscussionV2.Entity;
using ChannelVN.DiscussionV2.MainDal.Databases;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ChannelVN.DiscussionV2.MainDal
{
    public abstract class DiscussionV2DalBase
    {
        #region Function GET
        public List<DiscussionV2Entity> GetListDiscussionTopic(string userName, int pageIndex, int pageSize, ref int totalRow)
        {
            const string commandText = "CMS_DiscussionV2_GetListAllDataByTopicId";
            try
            {
                List<DiscussionV2Entity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "TotalRow", totalRow, System.Data.ParameterDirection.Output);
                _db.AddParameter(cmd, "UserName", userName);
                _db.AddParameter(cmd, "PageIndex", pageIndex);
                _db.AddParameter(cmd, "PageSize", pageSize);
                data = _db.GetList<DiscussionV2Entity>(cmd);
                totalRow = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 0));
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public List<DiscussionV2Entity> GetTopDiscussionOlder(string userName, int DiscussionTopicId)
        {
            const string commandText = "CMS_DiscussionV2_GetTopDiscussionOlder";
            try
            {
                List<DiscussionV2Entity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "DiscussionTopicId", DiscussionTopicId);
                _db.AddParameter(cmd, "UserName", userName);
                data = _db.GetList<DiscussionV2Entity>(cmd);
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public List<DiscussionV2Entity> GetListDiscussionAll(string userName, int DiscussionTopicId)
        {
            const string commandText = "CMS_DiscussionV2_GetListDiscussionAll";
            try
            {
                List<DiscussionV2Entity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "DiscussionTopicId", DiscussionTopicId);
                _db.AddParameter(cmd, "UserName", userName);
                data = _db.GetList<DiscussionV2Entity>(cmd);
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public DiscussionV2Entity GetDiscussionById(int DiscussionId)
        {
            const string commandText = "CMS_DiscussionV2_GetDiscussionById";
            try
            {
                DiscussionV2Entity data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "DiscussionId", DiscussionId);
                data = _db.Get<DiscussionV2Entity>(cmd);
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public List<ViewDiscussionV2Entity> GetListDiscussionUnRead(string userName, ref int TotalRows)
        {
            const string commandText = "CMS_DiscussionTopicV2_GetListAllData_Dashboard";
            try
            {
                List<ViewDiscussionV2Entity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "TotalRow", TotalRows, System.Data.ParameterDirection.Output);
                _db.AddParameter(cmd, "UserName", userName);
                data = _db.GetList<ViewDiscussionV2Entity>(cmd);
                TotalRows = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 0));
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        #endregion

        #region function SET
        public bool Insert(DiscussionV2Entity info, ref int id)
        {
            const string commandText = "CMS_DiscussionV2_Insert";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", id, System.Data.ParameterDirection.Output);
                _db.AddParameter(cmd, "DiscussionTopicId", info.DiscussionTopicId);
                _db.AddParameter(cmd, "ParentId", info.ParentId);
                _db.AddParameter(cmd, "Content", info.Content);
                _db.AddParameter(cmd, "CreatedBy", info.CreatedBy);
                _db.AddParameter(cmd, "ListReceiver", info.ListReceiver);

                var result = _db.ExecuteNonQuery(cmd);
                id = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 0));
                return result > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public bool Delete(int id, string userAssigned) {
            const string commandText = "CMS_DiscussionV2_Delete";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "id", id);
                _db.AddParameter(cmd, "userAssigned",userAssigned);

                var result = cmd.ExecuteNonQuery();
                return result > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public int countDiscussionReaded(int DiscussionTopicId, string OldId) {
            const string commandText = "CMS_DiscussionV2_GetCountDiscussion";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "OldId", OldId);
                _db.AddParameter(cmd, "DiscussionTopicId", DiscussionTopicId);
                var reader = cmd.ExecuteReader();
                var total = 0;
                if(reader.Read())
                    total = int.Parse(reader[0].ToString());
                return total;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        #endregion

        /// <summary>
        /// Chuyển đổi DAL cho Discussion
        /// minhduongvan
        /// 2014/01/08
        /// </summary>

        #region Constructor

        private readonly CmsMainDb _db;

        protected DiscussionV2DalBase(CmsMainDb db)
        {
            _db = db;
        }

        protected CmsMainDb Database
        {
            get { return _db; }
        }

        #endregion
    }
}
