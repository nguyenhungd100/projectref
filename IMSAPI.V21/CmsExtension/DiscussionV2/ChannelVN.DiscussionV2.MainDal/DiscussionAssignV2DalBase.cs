﻿using ChannelVN.CMS.Common;
using ChannelVN.DiscussionV2.Entity;
using ChannelVN.DiscussionV2.MainDal.Databases;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ChannelVN.DiscussionV2.MainDal
{
    public abstract class DiscussionAssignV2DalBase
    {
        #region function GET
        public List<DiscussionAssignV2Entity> GetNewDiscussionAssignByTopicId(int TopicId, string UserName)
        {
            const string commandText = "CMS_DiscussionV2_GetNewDiscussionAssignByTopicId";
            try
            {
                List<DiscussionAssignV2Entity> data;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "TopicId", TopicId);
                _db.AddParameter(cmd, "UserName", UserName);
                data = _db.GetList<DiscussionAssignV2Entity>(cmd);
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        #endregion

        #region function SET
        public bool Insert(int DiscussionTopicId, int DiscussionId, string ListToUser, string ListCCUser)
        {

            const string commandText = "CMS_DiscussionAssignV2_Insert";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "DiscussionTopicId", DiscussionTopicId);
                _db.AddParameter(cmd, "DiscussionId", DiscussionId);
                _db.AddParameter(cmd, "ListToUser", ListToUser);
                _db.AddParameter(cmd, "ListCCUser", ListCCUser);
                var result = cmd.ExecuteNonQuery();
                return result > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool Insert(DiscussionAssignV2Entity info, ref int id)
        {
            const string commandText = "CMS_DiscussionAssignV2_InsertOneAssign";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", id, System.Data.ParameterDirection.Output);
                _db.AddParameter(cmd, "DiscussionTopicId", info.DiscussionTopicId);
                _db.AddParameter(cmd, "DiscussionId", info.DiscussionId);
                _db.AddParameter(cmd, "UserAssigned", info.UserAssigned);
                _db.AddParameter(cmd, "Type", info.Type);
                _db.AddParameter(cmd, "IsRead", info.IsRead);
                var result = cmd.ExecuteNonQuery();
                id = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 0));

                return result > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool UpdateIsRead(int DiscussionTopicId, string UserAssigned) {
            const string commandText = "CMS_DiscussionV2_UpdateIsReadDiscussion";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "DiscussionTopicId", DiscussionTopicId);
                _db.AddParameter(cmd, "UserAssigned", UserAssigned);
                var result = cmd.ExecuteNonQuery();
                return result > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        #endregion

        /// <summary>
        /// Chuyển đổi DAL cho Discussion Assign
        /// minhduongvan
        /// 2014/01/09
        /// </summary>

        #region Constructor
        private readonly CmsMainDb _db;

        protected DiscussionAssignV2DalBase(CmsMainDb db)
        {
            _db = db;
        }

        protected CmsMainDb Database
        {
            get { return _db; }
        }
        #endregion
    }
}
