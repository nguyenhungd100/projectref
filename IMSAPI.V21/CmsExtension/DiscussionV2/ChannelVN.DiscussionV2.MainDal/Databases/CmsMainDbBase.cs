﻿using System.Data;
using ChannelVN.CMS.Common.DalCommon;
using ChannelVN.DiscussionV2.MainDal.Databases;

namespace ChannelVN.DiscussionV2.MainDal.Databases
{
    /// <summary>
    /// The base class for the <see cref="CmsMainDb"/> class that 
    /// represents a connection to the <c>CmsMainDb</c> database. 
    /// </summary>
    /// <remarks>
    /// Do not change this source code. Modify the CmsMainDb class
    /// if you need to add or change some functionality.
    /// </remarks>
    public abstract class CmsMainDbBase : MainDbBase
    {
        #region Store procedures

        #region DiscussionV2
        private DiscussionV2Dal _discussionContentMainDal;
        public DiscussionV2DalBase DiscussionContentMainDal
        {
            get { return _discussionContentMainDal ?? (_discussionContentMainDal = new DiscussionV2Dal((CmsMainDb)this)); }
        }
        #endregion

        #region DiscussionTopicV2
        private DiscussionTopicV2Dal _discussionTopicContentMainDal;
        public DiscussionTopicV2DalBase DiscussionTopicContentMainDal
        {
            get { return _discussionTopicContentMainDal ?? (_discussionTopicContentMainDal = new DiscussionTopicV2Dal((CmsMainDb)this)); }
        }
        #endregion

        #region DiscussionAttach
        private DiscussionAttachV2Dal _discussionAttachMainDal;
        public DiscussionAttachV2DalBase DiscussionAttachMainDal
        {
            get { return _discussionAttachMainDal ?? (_discussionAttachMainDal = new DiscussionAttachV2Dal((CmsMainDb)this)); }
        }
        #endregion

        #region Discussion Assign
        private DiscussionAssignV2Dal _discussionAssignMainDal;
        public DiscussionAssignV2DalBase DiscussionAssignMainDal
        {
            get { return _discussionAssignMainDal ?? (_discussionAssignMainDal = new DiscussionAssignV2Dal((CmsMainDb)this)); }
        }
        #endregion

        #endregion

        #region Constructors

        protected CmsMainDbBase()
        {
        }
        protected CmsMainDbBase(bool init)
        {
            if (init)
            {
                InitConnection();
            }
        }

        #endregion

        #region Protected members

        protected override sealed void InitConnection()
        {
            _connection = CreateConnection();
            _connection.Open();
        }

        /// <summary>
        /// Creates a new connection to the database.
        /// </summary>
        /// <returns>A reference to the <see cref="System.Data.IDbConnection"/> object.</returns>
        protected abstract IDbConnection CreateConnection();

        #endregion
    }
}