﻿using ChannelVN.CMS.Common;
using ChannelVN.DiscussionV2.Entity;
using ChannelVN.DiscussionV2.MainDal.Databases;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ChannelVN.DiscussionV2.MainDal
{
    public abstract class DiscussionAttachV2DalBase
    {
        #region function GET
        public List<DiscussionAttachV2Entity> ListAttachByDiscussionId(int discussionId) {
            const string commandText = "CMS_DiscussionAttachV2_GetListAllData";
            try
            {
                List<DiscussionAttachV2Entity> data;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "discussionId", discussionId);
                data = _db.GetList<DiscussionAttachV2Entity>(cmd);
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        #endregion

        #region function SET
        public bool Insert(DiscussionAttachV2Entity info, ref int id)
        {
            const string commandText = "CMS_DiscussionAttachV2_Insert";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", id, System.Data.ParameterDirection.Output);
                _db.AddParameter(cmd, "DiscussionTopicId", info.DiscussionTopicId);
                _db.AddParameter(cmd, "DiscussionId", info.DiscussionId);
                _db.AddParameter(cmd, "AttachName", info.AttachName);
                _db.AddParameter(cmd, "AttachUrl", info.AttachUrl);
                _db.AddParameter(cmd, "Note", info.Note);
                _db.AddParameter(cmd, "Avatar", info.Avatar);
                var result = cmd.ExecuteNonQuery();
                id = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 0));
                return result > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        #endregion

        /// <summary>
        /// Chuyển đổi DAL cho Discussion Attach
        /// minhduongvan
        /// 2014/01/09
        /// </summary>

        #region Constructor
        private readonly CmsMainDb _db;

        protected DiscussionAttachV2DalBase(CmsMainDb db)
        {
            _db = db;
        }

        protected CmsMainDb Database
        {
            get { return _db; }
        }
        #endregion
    }
}
