﻿using ChannelVN.CMS.Common;
using ChannelVN.CMS.MainDal.Common;
using ChannelVN.DiscussionV2.Entity;
using ChannelVN.DiscussionV2.MainDal.Databases;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace ChannelVN.DiscussionV2.MainDal
{
    public abstract class DiscussionTopicV2DalBase
    {
        #region function GET
        public List<ViewDiscussionV2Entity> GetListDiscussionTopic(string keyword, int type, bool status,
                                            string userName, int pageIndex, int pageSize, ref int totalRow)
        {
            const string commandText = "CMS_DiscussionTopicV2_GetListAllData";
            try
            {
                List<ViewDiscussionV2Entity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "TotalRow", totalRow, System.Data.ParameterDirection.Output);
                _db.AddParameter(cmd, "Keyword", keyword);
                _db.AddParameter(cmd, "Type", type);
                _db.AddParameter(cmd, "Status", status);
                _db.AddParameter(cmd, "UserName", userName);
                _db.AddParameter(cmd, "PageIndex", pageIndex);
                _db.AddParameter(cmd, "PageSize", pageSize);
                data = _db.GetList<ViewDiscussionV2Entity>(cmd);
                totalRow = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 0));
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }

        }

        public List<ViewDiscussionV2Entity> InitESAllDiscussionTopic(string userName, int pageIndex, int pageSize, DateTime fromDate, DateTime toDate, ref int totalRow)
        {
            //const string commandText = "CMS_DiscussionTopicV2_InitESAllDiscussionTopic";
            const string commandText = @"
                                        DECLARE @UpperBand int, @LowerBand int

                                        SELECT @TotalRow = COUNT(*) FROM DiscussionTopicV2 dt where (@DateFrom is null and (1=1)) or (@DateFrom is not null and (dt.CreatedDate BETWEEN @DateFrom AND @DateTo))

                                        SET @LowerBand  = (@pageIndex - 1) * @PageSize
                                        SET @UpperBand  = (@pageIndex * @PageSize)
                                        SELECT * FROM (
                                        SELECT dt.*,
                                            [dbo].[CMS_fGetLastDiscussionByTopicId](dt.Id,1,@UserName) as ContentDiscussion,
				                            [dbo].[CMS_fGetLastDiscussionByTopicId](dt.Id,2,@UserName) as ListUser,
				                            CONVERT(datetime, [dbo].[CMS_fGetLastDiscussionByTopicId](dt.Id,3,@UserName)) As lastSendDate,
				                            CAST( [dbo].[CMS_fCountDiscussionByTopicId](dt.Id,@UserName) as INT) As TotalDiscussionByTopic,
				                            [dbo].[CMS_fGetLastDiscussionByTopicId](dt.Id,4,@UserName) As AttachDiscussion,
				                            CAST ([dbo].[CMS_fGetLastDiscussionByTopicId](dt.Id,5,@UserName) as bit) As IsRead,
                                            ROW_NUMBER() OVER(ORDER BY dt.CreatedDate DESC) AS RowNumber 
                                        FROM DiscussionTopicV2 dt                                                                              
                                        where (@DateFrom is null and (1=1)) or (@DateFrom is not null and (dt.CreatedDate BETWEEN @DateFrom AND @DateTo))
                                        ) AS temp
                                        WHERE RowNumber > @LowerBand AND RowNumber <= @UpperBand";
            try
            {
                List<ViewDiscussionV2Entity> data = null;
                var cmd = _db.CreateCommand(commandText);
                _db.AddParameter(cmd, "TotalRow", totalRow, ParameterDirection.Output);
                _db.AddParameter(cmd, "PageIndex", pageIndex);
                _db.AddParameter(cmd, "PageSize", pageSize);
                if (fromDate <= Constants.MinDateTime)
                    _db.AddParameter(cmd, "DateFrom", DBNull.Value);
                else _db.AddParameter(cmd, "DateFrom", fromDate);
                if (toDate <= Constants.MinDateTime)
                    _db.AddParameter(cmd, "DateTo", DBNull.Value);
                else _db.AddParameter(cmd, "DateTo", toDate);
                _db.AddParameter(cmd, "UserName", userName);                

                data = _db.GetList<ViewDiscussionV2Entity>(cmd);

                totalRow = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 0));

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public DiscussionTopicV2Entity GetTopicByNewsId(Int64 newsId)
        {
            const string commandText = "CMS_DiscussionTopicV2_GetTopicByNewsId";
            try
            {
                DiscussionTopicV2Entity data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "newsId", newsId);
                data = _db.Get<DiscussionTopicV2Entity>(cmd);
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public DiscussionTopicV2Entity GetTopicById(int topicId)
        {
            const string commandText = "CMS_DiscussionTopicV2_GetTopicById";
            try
            {
                DiscussionTopicV2Entity data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "topicId", topicId);
                data = _db.Get<DiscussionTopicV2Entity>(cmd);
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        #endregion

        #region function SET
        public bool Insert(DiscussionTopicV2Entity info, string ListUserFollow, ref int id)
        {
            const string commandText = "CMS_DiscussionTopicV2_Insert";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", id, System.Data.ParameterDirection.Output);
                _db.AddParameter(cmd, "ObjectId", info.ObjectId);
                _db.AddParameter(cmd, "Title", info.Title);
                _db.AddParameter(cmd, "Type", info.Type);
                _db.AddParameter(cmd, "CreatedBy", info.CreatedBy);
                _db.AddParameter(cmd, "ListUserFollow", ListUserFollow);

                var result = cmd.ExecuteNonQuery();

                id = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 0));

                return result > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool Delete(int id, string username)
        {
            const string commandText = "CMS_DiscussionTopicV2_Delete";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", id);
                _db.AddParameter(cmd, "UserName", username);

                var result = cmd.ExecuteNonQuery();

                return result > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool SetStatus(int id)
        {
            const string commandText = "CMS_DiscussionTopicV2_SetStatus";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", id);
                var result = cmd.ExecuteNonQuery();

                return result > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public bool InsertFollowTopic(int DiscussionTopicId, string listUserFollow, ref int id)
        {
            const string commandText = "CMS_DiscussionTopicFollowV2_Insert";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", id, System.Data.ParameterDirection.Output);
                _db.AddParameter(cmd, "DiscussionTopicId", DiscussionTopicId);
                _db.AddParameter(cmd, "ListUserFollow", listUserFollow);
                var result = cmd.ExecuteNonQuery();

                return result > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public bool UpdateIsFollowTopic(int DiscussionTopicId, string UserFollow, bool isFollow)
        {
            const string commandText = "CMS_DiscussionTopicFollowV2_Update";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "DiscussionTopicId", DiscussionTopicId);
                _db.AddParameter(cmd, "UserFollow", UserFollow);
                _db.AddParameter(cmd, "isFollow", isFollow);
                var result = cmd.ExecuteNonQuery();

                return result > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        #endregion

        /// <summary>
        /// Chuyển đổi DAL cho Discussion Topic
        /// minhduongvan
        /// 2014/01/08
        /// </summary>

        #region Constructor
        private readonly CmsMainDb _db;

        protected DiscussionTopicV2DalBase(CmsMainDb db)
        {
            _db = db;
        }

        protected CmsMainDb Database
        {
            get { return _db; }
        }
        #endregion
    }
}
