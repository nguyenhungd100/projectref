﻿using ChannelVN.CMS.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace ChannelVN.DiscussionV2.Entity
{
    [DataContract]
    public class DiscussionAssignV2Entity : EntityBase
    {
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public int DiscussionTopicId { get; set; }
        [DataMember]
        public int DiscussionId { get; set; }
        [DataMember]
        public int Type { get; set; }
        [DataMember]
        public string UserAssigned { get; set; }
        [DataMember]
        public bool IsRead { get; set; }
        [DataMember]
        public bool IsHighlight { get; set; }
        [DataMember]
        public bool IsFollow { get; set; }
    }

    public enum DiscussionAssignEnum
    {
        Sender = 0,
        To = 1,
        CC = 2,
        BCC= 3
    }
}
