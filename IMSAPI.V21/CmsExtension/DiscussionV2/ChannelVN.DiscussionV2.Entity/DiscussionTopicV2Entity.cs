﻿using ChannelVN.CMS.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace ChannelVN.DiscussionV2.Entity
{
    [DataContract]
    public class DiscussionTopicV2Entity : EntityBase
    {
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public long ObjectId { get; set; }
        [DataMember]
        public string Title { get; set; }
        [DataMember]
        public int Type { get; set; }
        [DataMember]
        public string CreatedBy { get; set; }
        [DataMember]
        public DateTime CreatedDate { get; set; }
        [DataMember]
        public DateTime CloseDate { get; set; }
        [DataMember]
        public bool Status { get; set; }
        [DataMember]
        public string ListToUser { get; set; }
        [DataMember]
        public string ListCCUser { get; set; }
        [DataMember]
        public string Content { get; set; }
    }

    public enum DiscussionTopicEnum
    {
        Topic = 1,
        News = 2
    }
}
