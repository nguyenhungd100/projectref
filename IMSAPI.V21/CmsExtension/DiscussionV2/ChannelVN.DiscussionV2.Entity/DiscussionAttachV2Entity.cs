﻿using ChannelVN.CMS.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace ChannelVN.DiscussionV2.Entity
{
    [DataContract]
    public class DiscussionAttachV2Entity : EntityBase
    {
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public int DiscussionTopicId { get; set; }
        [DataMember]
        public int DiscussionId { get; set; }
        [DataMember]
        public string AttachName { get; set; }
        [DataMember]
        public string AttachUrl { get; set; }
        [DataMember]
        public string Note { get; set; }
        [DataMember]
        public string Avatar { get; set; }
    }

}
