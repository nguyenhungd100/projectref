﻿using ChannelVN.CMS.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace ChannelVN.DiscussionV2.Entity
{
    [DataContract]
    public class DiscussionV2Entity : EntityBase
    {
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public int ParentId { get; set; }
        [DataMember]
        public int DiscussionTopicId   { get; set; }
        [DataMember]
        public string Content { get; set; }
        [DataMember]
        public string CreatedBy { get; set; }
        [DataMember]
        public DateTime CreatedDate { get; set; }
        [DataMember]
        public string ListReceiver { get; set; }
        [DataMember]
        public bool IsRead { get; set; }
        [DataMember]
        public string ListToUser { get; set; }
        [DataMember]
        public string ListCCUser { get; set; }
        [DataMember]
        public bool hasAttach { get; set; }
    }
    [DataContract]
    public class ViewDiscussionV2Entity : EntityBase
    {
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public int ParentId { get; set; }
        [DataMember]
        public int DiscussionTopicId { get; set; }
        [DataMember]
        public string Title { get; set; }
        [DataMember]
        public int Type { get; set; }
        [DataMember]
        public bool Status { get; set; }
        [DataMember]
        public bool IsRead { get; set; }
        [DataMember]
        public string CreatedBy { get; set; }
        [DataMember]
        public DateTime CreatedDate { get; set; }
        [DataMember]
        public string ContentDiscussion { get; set; }
        [DataMember]
        public string ListUser { get; set; }
        [DataMember]
        public DateTime lastSendDate { get; set; }
        [DataMember]
        public int TotalDiscussionByTopic { get; set; }
        [DataMember]
        public string AttachDiscussion { get; set; }
    }
    [DataContract]
    public class ViewDiscussionByTopicEntity : EntityBase {
        [DataMember]
        public List<DiscussionV2Entity> lstDiscussionOlder { get; set; }
        [DataMember]
        public List<DiscussionV2Entity> lstDiscussionNewAndUnRead { get; set; }
        [DataMember]
        public int  countReaded { get; set; }
    }
}
