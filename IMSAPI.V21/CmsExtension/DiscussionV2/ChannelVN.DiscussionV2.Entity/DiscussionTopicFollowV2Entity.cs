﻿using ChannelVN.CMS.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace ChannelVN.DiscussionV2.Entity
{
    [DataContract]
    public class DiscussionTopicFollowV2Entity : EntityBase
    {
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public int DiscussionTopicId { get; set; }
        [DataMember]
        public int Type { get; set; }
        [DataMember]
        public string UserFollow { get; set; }
        [DataMember]
        public bool IsFollow { get; set; }
        [DataMember]
        public DateTime LastModifiedDate { get; set; }
    }

}
