﻿using System.Runtime.Serialization;
using ChannelVN.CMS.Common;

namespace ChannelVN.DiscussionV2.Entity.ErrorCode
{
    [DataContract]
    public class ErrorMapping : BoErrorMappingBase<ErrorMapping.ErrorCodes>
    {
        private static ErrorMapping _current;
        public static ErrorMapping Current
        {
            get { return _current ?? (_current = new ErrorMapping()); }
        }

        protected override void InitErrorMapping()
        {
            InnerHashtable[ErrorCodes.Invalid] = "Không tìm thấy dữ liệu";
            InnerHashtable[ErrorCodes.DelInvite] = "Bạn không thể xóa thảo luận này!";
        }
        [DataContract]
        public enum ErrorCodes
        {
            #region General error

            [EnumMember]
            Success = ErrorCodeBase.Success,
            [EnumMember]
            UnknowError = ErrorCodeBase.UnknowError,
            [EnumMember]
            Exception = ErrorCodeBase.Exception,
            [EnumMember]
            BusinessError = ErrorCodeBase.BusinessError,
            [EnumMember]
            InvalidRequest = ErrorCodeBase.InvalidRequest,
            [EnumMember]
            TimeOutSession = ErrorCodeBase.TimeOutSession,
            [EnumMember]
            UpdateInterviewQuestionNotSuccess = 11013,
            [EnumMember]
            UpdateInterviewQuestionInterviewNotFound=11014,

            #endregion

            #region Calendar error

            Invalid = 10001,
            DelInvite = 10002,

            #endregion
        }
    }
}
