﻿using ChannelVN.CMS.BoSearch.Entity;
using ChannelVN.CMS.Common;
using ChannelVN.DiscussionV2.Dal;
using ChannelVN.DiscussionV2.Entity;
using ChannelVN.DiscussionV2.Entity.ErrorCode;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ChannelVN.DiscussionV2.Bo
{
    public class DiscussionTopicV2Bo
    {
        /// <summary>
        /// Lấy danh sách topic/tin bài thảo luận
        /// </summary>
        /// <param name="userName">string</param>
        /// <param name="pageIndex">int</param>
        /// <param name="pageSize">int</param>
        /// <param name="totalRow">int</param>
        /// <returns>List data</returns>
        public static List<ViewDiscussionV2Entity> GetListDiscussionTopic(string keyword, int type, bool status, string userName, int pageIndex, int pageSize, ref int totalRow)
        {
            var listDiscussionTopic = new List<ViewDiscussionV2Entity>();
            try {
                var data = CMS.BoSearch.CmsExtension.DiscussionV2.DiscussionV2DalFactory.SearchDiscussionTopicV2(keyword, type, status, userName, pageIndex, pageSize, ref totalRow);
                if (null != data && data.Count > 0)
                {
                    listDiscussionTopic = CMS.BoCached.CmsExtension.DiscussionV2.DiscussionV2DalFactory.GetDiscussionTopicByListId(data);
                }
                if (listDiscussionTopic == null || (listDiscussionTopic != null && listDiscussionTopic.Count() <= 0))
                {
                    //db sql
                    listDiscussionTopic = DiscussionTopicV2Dal.GetListDiscussionTopic(keyword, type, status, userName, pageIndex, pageSize, ref totalRow);
                }
                return listDiscussionTopic;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, string.Format("DiscussionTopicBo.GetListDiscussionTopic:{0}", ex.Message));
                return listDiscussionTopic;
            }
        }
        public static CMS.BoCached.Entity.Init.LogInitAsyncEntity InitEsByDiscussionTopicAsync(string userName, string name, DateTime startDate, DateTime endDate, int pageSize, string action = null)
        {
            var totalRows = 0;
            return CMS.BoCached.Common.Queue.InitSyncData(name, 1, startDate, endDate, () => {
                var data = DiscussionTopicV2Dal.InitESAllDiscussionTopic(userName, 1, pageSize, startDate, endDate, ref totalRows);
                var totalPages = (int)Math.Ceiling((double)totalRows / pageSize);
                return totalPages;
            }, (page) => {
                var data = DiscussionTopicV2Dal.InitESAllDiscussionTopic(userName, page, pageSize, startDate, endDate, ref totalRows);
                var list = data.Select(s => new DiscussionTopicSearchEntity
                {
                    Id = s.Id,
                    Title = s.Title,
                    Type=s.Type,                                                     
                    Status = s.Status,
                    CreatedDate=s.CreatedDate
                }).ToList();
                CMS.BoSearch.CmsExtension.DiscussionV2.DiscussionV2DalFactory.InitAllDiscussionTopic(list);
            }, action);
        }
        public static CMS.BoCached.Entity.Init.LogInitAsyncEntity InitRedisByDiscussionTopicAsync(string userName, string name, DateTime startDate, DateTime endDate, int pageSize, string action = null)
        {
            var totalRows = 0;
            return CMS.BoCached.Common.Queue.InitSyncData(name, 1, startDate, endDate, () => {
                var data = DiscussionTopicV2Dal.InitESAllDiscussionTopic(userName, 1, pageSize, startDate, endDate, ref totalRows);
                var totalPages = (int)Math.Ceiling((double)totalRows / pageSize);
                return totalPages;
            }, (page) => {
                var data = DiscussionTopicV2Dal.InitESAllDiscussionTopic(userName, page, pageSize, startDate, endDate, ref totalRows);
                CMS.BoCached.CmsExtension.DiscussionV2.DiscussionV2DalFactory.InitAllDiscussionTopic(data);
            }, action);
        }

        /// <summary>
        /// Thêm mới một topic
        /// </summary>
        /// <param name="info">Entity</param>
        /// <param name="id">out put</param>
        /// <returns>true/false</returns>
        public static ErrorMapping.ErrorCodes Insert(DiscussionTopicV2Entity info, List<DiscussionAttachV2Entity> listAttach, ref int TopicId)
        {
            string ListUserFollow = "";
            string ListReceiver = "";
            if (!string.IsNullOrEmpty(info.ListCCUser))
            {
                ListUserFollow = info.CreatedBy + ',' + info.ListToUser + "," + info.ListCCUser;
                ListReceiver = info.ListToUser + "," + info.ListCCUser;
            }
            else
            {
                ListUserFollow = info.CreatedBy + ',' + info.ListToUser;
                ListReceiver = info.ListToUser;
            }

            var result = ErrorMapping.ErrorCodes.BusinessError;
            var createSuccess = DiscussionTopicV2Dal.Insert(info, ListUserFollow, ref TopicId);
            if (TopicId > 0)
            {
                var discussionInfo = new DiscussionV2Entity
                {
                    DiscussionTopicId = TopicId,
                    Content = info.Content,
                    CreatedBy = info.CreatedBy,
                    ListReceiver = ListReceiver,
                    ParentId = 0
                };
                int disId = 0;
                DiscussionV2Dal.Insert(discussionInfo, ref disId);
                if (disId > 0)
                {
                    int Retassign = 0;
                    var assignInfo = new DiscussionAssignV2Entity
                    {
                        DiscussionTopicId = TopicId,
                        DiscussionId = disId,
                        IsRead = true,
                        Type = (int)DiscussionAssignEnum.Sender,
                        UserAssigned = info.CreatedBy
                    };
                    DiscussionAssignV2Dal.Insert(assignInfo, ref Retassign);
                    DiscussionAssignV2Dal.Insert(TopicId, disId, info.ListToUser, info.ListCCUser);
                    //Đính kèm Attach
                    if (listAttach != null && listAttach.Count > 0)
                    {
                        foreach (var item in listAttach)
                        {
                            int idAttach = 0;
                            item.DiscussionTopicId = TopicId;
                            item.DiscussionId = disId;
                            DiscussionAttachV2Dal.Insert(item, ref idAttach);
                        }
                    }
                }
                createSuccess = true;
            }
            if (createSuccess && TopicId > 0)
                result = ErrorMapping.ErrorCodes.Success;
            return result;
        }

        /// <summary>
        /// Xóa topic
        /// </summary>
        /// <param name="id">int</param>
        /// <param name="username">string</param>
        /// <returns></returns>
        public static ErrorMapping.ErrorCodes Delete(int id, string username)
        {
            var result = ErrorMapping.ErrorCodes.BusinessError;
            var createSuccess = DiscussionTopicV2Dal.Delete(id, username);
            if (createSuccess)
                result = ErrorMapping.ErrorCodes.Success;
            return result;
        }
        public static ErrorMapping.ErrorCodes SetStatus(int id)
        {
            var result = ErrorMapping.ErrorCodes.BusinessError;
            var createSuccess = DiscussionTopicV2Dal.SetStatus(id);
            if (createSuccess)
                result = ErrorMapping.ErrorCodes.Success;
            return result;
        }

        /// <summary>
        /// Thêm user follow theo topic
        /// </summary>
        /// <param name="DiscussionTopicId">int</param>
        /// <param name="listUserFollow">danh sách user cách nhau bởi dấu [;] vd(a;b;c)</param>
        /// <returns></returns>
        public static ErrorMapping.ErrorCodes InsertFollowTopic(int DiscussionTopicId, string listUserFollow, ref int id)
        {
            var result = ErrorMapping.ErrorCodes.BusinessError;
            var createSuccess = DiscussionTopicV2Dal.InsertFollowTopic(DiscussionTopicId, listUserFollow, ref id);
            if (createSuccess && id > 0)
                result = ErrorMapping.ErrorCodes.Success;
            return result;
        }

        /// <summary>
        /// Cập nhật trạng thái follow cho user
        /// </summary>
        /// <param name="DiscussionTopicId">int</param>
        /// <param name="UserFollow">string</param>
        /// <param name="isFollow">bool</param>
        /// <returns></returns>
        public static ErrorMapping.ErrorCodes UpdateIsFollowTopic(int DiscussionTopicId, string UserFollow, bool isFollow)
        {
            var result = ErrorMapping.ErrorCodes.BusinessError;
            var createSuccess = DiscussionTopicV2Dal.UpdateIsFollowTopic(DiscussionTopicId, UserFollow, isFollow);
            if (createSuccess)
                result = ErrorMapping.ErrorCodes.Success;
            return result;
        }

        /// <summary>
        /// Lấy thông tin topic theo NewsId
        /// </summary>
        /// <param name="newsId"></param>
        /// <returns></returns>
        public static DiscussionTopicV2Entity GetTopicByNewsId(long newsId) {
            return DiscussionTopicV2Dal.GetTopicByNewsId(newsId);
        }

        /// <summary>
        /// Lấy thông tin topic theo topic Id
        /// </summary>
        /// <param name="topicId"></param>
        /// <returns></returns>
        public static DiscussionTopicV2Entity GetTopicById(int topicId) {
            return DiscussionTopicV2Dal.GetTopicById(topicId);
        }

        public static ErrorMapping.ErrorCodes InsertDisscussion(DiscussionV2Entity discussionInfo, ref int Id) {
            var result = ErrorMapping.ErrorCodes.BusinessError;
            var createSuccess = DiscussionV2Dal.Insert(discussionInfo, ref Id);
            if (Id > 0)
                createSuccess = true;
            if (createSuccess)
                result = ErrorMapping.ErrorCodes.Success;
            return result;
        }
    }
}
