﻿using ChannelVN.DiscussionV2.Dal;
using ChannelVN.DiscussionV2.Entity;
using ChannelVN.DiscussionV2.Entity.ErrorCode;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ChannelVN.DiscussionV2.Bo
{
    public class DiscussionAssignV2Bo
    {
        public static ErrorMapping.ErrorCodes InsertMultiUser(int DiscussionTopicId, int DiscussionId, string ListToUser, string ListCCUser)
        {
            var result = ErrorMapping.ErrorCodes.BusinessError;
            var createSuccess = DiscussionAssignV2Dal.Insert(DiscussionTopicId, DiscussionId, ListToUser, ListCCUser);
            if (createSuccess)
                result = ErrorMapping.ErrorCodes.Success;
            return result;
        }

        public static ErrorMapping.ErrorCodes InsertOnUser(DiscussionAssignV2Entity info, ref int id)
        {
            var result = ErrorMapping.ErrorCodes.BusinessError;
            var createSuccess = DiscussionAssignV2Dal.Insert(info, ref id);
            if (createSuccess && id > 0)
                result = ErrorMapping.ErrorCodes.Success;
            return result;
        }

        public static ErrorMapping.ErrorCodes UpdateIsRead(int DiscussionTopicId, string UserAssigned)
        {
            var result = ErrorMapping.ErrorCodes.BusinessError;
            var createSuccess = DiscussionAssignV2Dal.UpdateIsRead(DiscussionTopicId, UserAssigned);
            if (createSuccess)
                result = ErrorMapping.ErrorCodes.Success;
            return result;
        }

        public static List<DiscussionAssignV2Entity> GetNewDiscussionAssignByTopicId(int topicId, string createdBy) {
            return DiscussionAssignV2Dal.GetNewDiscussionAssignByTopicId(topicId, createdBy);
        }
    }
}
