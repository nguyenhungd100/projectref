﻿using ChannelVN.CMS.Common;
using ChannelVN.DiscussionV2.Dal;
using ChannelVN.DiscussionV2.Entity;
using ChannelVN.DiscussionV2.Entity.ErrorCode;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ChannelVN.DiscussionV2.Bo
{
    public class DiscussionV2Bo
    {
        public static ViewDiscussionByTopicEntity GetListDiscussionByTopic(string userName, int DiscustionTopicId)
        {
            ViewDiscussionByTopicEntity obj = new ViewDiscussionByTopicEntity();
            //obj.lstDiscussionOlder = DiscussionV2Dal.GetTopDiscussionOlder(userName, DiscustionTopicId);
            //if (obj.lstDiscussionOlder.Count > 0)
            //    obj.lstDiscussionNewAndUnRead = DiscussionV2Dal.GetListDiscussionUnRead(userName, DiscustionTopicId, obj.lstDiscussionOlder[0].Id);
            //else
            obj.lstDiscussionNewAndUnRead = DiscussionV2Dal.GetListDiscussionAll(userName, DiscustionTopicId);

            #region Chưa sử dụng đến
            //string listId = obj.lstDiscussionOlder[0].Id.ToString();
            //if (obj.lstDiscussionNewAndUnRead.Count > 0)
            //{
            //    foreach (var item in obj.lstDiscussionNewAndUnRead)
            //    {
            //        listId += "," + item.Id;
            //    }
            //    obj.countReaded = DiscussionV2Dal.countDiscussionReaded(DiscustionTopicId, listId);
            //}
            #endregion

            return obj;
        }

        public static List<ViewDiscussionV2Entity> GetListDiscussionUnread(string userName, ref int totalRow)
        {
            try
            {
                var data = DiscussionV2Dal.GetListDiscussionUnRead(userName, ref totalRow);
                return data;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Fatal, ex.ToString());
            }
            return new List<ViewDiscussionV2Entity>();
        }

        /// <summary>
        /// Thêm mới Discussion
        /// </summary>
        /// <param name="info">Entity</param>
        /// <param name="id">int</param>
        /// <returns>True/False</returns>
        public static ErrorMapping.ErrorCodes Insert(DiscussionV2Entity info, List<DiscussionAttachV2Entity> listAttach, ref int id)
        {
            var result = ErrorMapping.ErrorCodes.BusinessError;
            string ListUser = "";

            if (!string.IsNullOrEmpty(info.ListCCUser))
                ListUser = info.ListToUser + "," + info.ListCCUser;
            else
                ListUser = info.ListToUser;

            info.ListReceiver = ListUser;
            var createSuccess = DiscussionV2Dal.Insert(info, ref id);
            if (id > 0)
            {
                int Retassign = 0;
                var infoTopic = DiscussionTopicV2Dal.GetTopicById(info.DiscussionTopicId);

                var assignInfo = new DiscussionAssignV2Entity
                {
                    DiscussionTopicId = info.DiscussionTopicId,
                    DiscussionId = id,
                    IsRead = true,
                    Type = (infoTopic.ObjectId > 0) ? (int)DiscussionAssignEnum.To : (int)DiscussionAssignEnum.Sender,
                    UserAssigned = info.CreatedBy
                };
                DiscussionAssignV2Dal.Insert(assignInfo, ref Retassign);
                DiscussionAssignV2Dal.Insert(info.DiscussionTopicId, id, info.ListToUser, info.ListCCUser);
                //Đính kèm Attach
                if (listAttach.Count > 0)
                {
                    foreach (var item in listAttach)
                    {
                        int idAttach = 0;
                        item.DiscussionTopicId = info.DiscussionTopicId;
                        item.DiscussionId = id;
                        DiscussionAttachV2Dal.Insert(item, ref idAttach);
                    }
                }
                createSuccess = true;
            }
            if (createSuccess && id > 0)
                result = ErrorMapping.ErrorCodes.Success;
            return result;
        }

        /// <summary>
        /// Xóa một discussion
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public static ErrorMapping.ErrorCodes Delete(int id, string userAssigned)
        {
            var result = ErrorMapping.ErrorCodes.BusinessError;
            var createSuccess = DiscussionV2Dal.Delete(id, userAssigned);
            if (createSuccess && id > 0)
                result = ErrorMapping.ErrorCodes.Success;
            return result;
        }
    }
}
