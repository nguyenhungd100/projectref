﻿using ChannelVN.DiscussionV2.Dal;
using ChannelVN.DiscussionV2.Entity;
using ChannelVN.DiscussionV2.Entity.ErrorCode;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ChannelVN.DiscussionV2.Bo
{
    public class DiscussionAttachV2Bo
    {
        public static ErrorMapping.ErrorCodes Insert(DiscussionAttachV2Entity info, ref int id)
        {
            var result = ErrorMapping.ErrorCodes.BusinessError;
            var createSuccess = DiscussionAttachV2Dal.Insert(info, ref id);
            if (id > 0)
                createSuccess = true;
            if (createSuccess)
                result = ErrorMapping.ErrorCodes.Success;
            return result;
        }

        public static List<DiscussionAttachV2Entity> ListAttachByDiscussionId(int discussionId)
        {
            return DiscussionAttachV2Dal.ListAttachByDiscussionId(discussionId);
        }
    }
}
