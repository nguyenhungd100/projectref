﻿using ChannelVN.CMS.Common;
using ChannelVN.DiscussionV2.Dal.Common;
using ChannelVN.DiscussionV2.Entity;
using ChannelVN.DiscussionV2.MainDal.Databases;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace ChannelVN.DiscussionV2.Dal
{
    public class DiscussionV2Dal
    {
        public static List<DiscussionV2Entity> GetListDiscussionTopic(string userName, int pageIndex, int pageSize, ref int totalRow)
        {
            List<DiscussionV2Entity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.DiscussionContentMainDal.GetListDiscussionTopic(userName, pageIndex,
                    pageSize, ref totalRow);
            }
            return returnValue;
        }
        public static List<DiscussionV2Entity> GetTopDiscussionOlder(string userName, int DiscussionTopicId)
        {
            List<DiscussionV2Entity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.DiscussionContentMainDal.GetTopDiscussionOlder(userName, DiscussionTopicId);
            }
            return returnValue;
        }
        public static List<DiscussionV2Entity> GetListDiscussionAll(string userName, int DiscussionTopicId)
        {
            List<DiscussionV2Entity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.DiscussionContentMainDal.GetListDiscussionAll(userName, DiscussionTopicId);
            }
            return returnValue;
        }
        public static DiscussionV2Entity GetDiscussionById(int DiscussionId)
        {
            DiscussionV2Entity returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.DiscussionContentMainDal.GetDiscussionById(DiscussionId);
            }
            return returnValue;
        }
        public static bool Insert(DiscussionV2Entity info, ref int id)
        {
            bool returnValue = false;
            using (var db = new CmsMainDb())
            {
                returnValue = db.DiscussionContentMainDal.Insert(info, ref id);
            }
            return returnValue;
        }
        public static bool Delete(int id, string userAssigned)
        {
            bool returnValue = false;
            using (var db = new CmsMainDb())
            {
                returnValue = db.DiscussionContentMainDal.Delete(id, userAssigned);
            }
            return returnValue;
        }
        public static int countDiscussionReaded(int DiscussionTopicId, string OldId)
        {
            int returnValue = 0;
            using (var db = new CmsMainDb())
            {
                returnValue = db.DiscussionContentMainDal.countDiscussionReaded(DiscussionTopicId, OldId);
            }
            return returnValue;
        }

        public static List<ViewDiscussionV2Entity> GetListDiscussionUnRead(string userName, ref int TotalRows)
        {
            List<ViewDiscussionV2Entity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.DiscussionContentMainDal.GetListDiscussionUnRead(userName, ref TotalRows);
            }
            return returnValue;
        }
    }
}
