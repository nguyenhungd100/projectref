﻿using ChannelVN.CMS.Common;
using ChannelVN.DiscussionV2.Dal.Common;
using ChannelVN.DiscussionV2.Entity;
using ChannelVN.DiscussionV2.MainDal.Databases;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace ChannelVN.DiscussionV2.Dal
{
    public class DiscussionAttachV2Dal
    {
        public static bool Insert(DiscussionAttachV2Entity info, ref int id)
        {
            bool returnValue = false;
            using (var db = new CmsMainDb())
            {
                returnValue = db.DiscussionAttachMainDal.Insert(info, ref id);
            }
            return returnValue;
        }

        public static List<DiscussionAttachV2Entity> ListAttachByDiscussionId(int discussionId) {
            List<DiscussionAttachV2Entity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.DiscussionAttachMainDal.ListAttachByDiscussionId(discussionId);
            }
            return returnValue;
        }
    }
}
