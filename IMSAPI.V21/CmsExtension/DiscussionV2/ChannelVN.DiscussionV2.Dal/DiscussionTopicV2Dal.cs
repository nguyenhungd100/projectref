﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ChannelVN.DiscussionV2.Entity;
using System.Data.SqlClient;
using ChannelVN.CMS.Common;
using System.Data;
using ChannelVN.DiscussionV2.Dal.Common;
using ChannelVN.DiscussionV2.MainDal.Databases;

namespace ChannelVN.DiscussionV2.Dal
{
    public class DiscussionTopicV2Dal
    {
        /// <summary>
        /// Lấy danh sách topic/tin bài thảo luận
        /// </summary>
        /// <param name="userName">string</param>
        /// <param name="pageIndex">int</param>
        /// <param name="pageSize">int</param>
        /// <param name="totalRow">int</param>
        /// <returns>List data</returns>
        public static List<ViewDiscussionV2Entity> GetListDiscussionTopic(string keyword, int type, bool status, string userName, int pageIndex, int pageSize, ref int totalRow)
        {
            List<ViewDiscussionV2Entity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.DiscussionTopicContentMainDal.GetListDiscussionTopic(keyword, type, status
                    , userName, pageIndex, pageSize, ref totalRow);
            }
            return returnValue;
        }
        public static List<ViewDiscussionV2Entity> InitESAllDiscussionTopic(string userName,int pageIndex, int pageSize, DateTime fromDate, DateTime toDate, ref int totalRow)
        {
            List<ViewDiscussionV2Entity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.DiscussionTopicContentMainDal.InitESAllDiscussionTopic(userName, pageIndex, pageSize, fromDate, toDate, ref totalRow);
            }
            return returnValue;
        }
        /// <summary>
        /// Lấy thông tin chi tiết topic theo NewsId
        /// </summary>
        /// <param name="topicId">int</param>
        /// <returns>Entity</returns>
        public static DiscussionTopicV2Entity GetTopicByNewsId(Int64 newsId)
        {
            DiscussionTopicV2Entity returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.DiscussionTopicContentMainDal.GetTopicByNewsId(newsId);
            }
            return returnValue;
        }
        /// <summary>
        /// Lấy thông tin topic theo topic id
        /// </summary>
        /// <param name="topicId">int</param>
        /// <returns>Entity</returns>
        public static DiscussionTopicV2Entity GetTopicById(int topicId)
        {
            DiscussionTopicV2Entity returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.DiscussionTopicContentMainDal.GetTopicById(topicId);
            }
            return returnValue;
        }
        /// <summary>
        /// Thêm mới một topic
        /// </summary>
        /// <param name="info">Entity</param>
        /// <param name="id">out put</param>
        /// <returns>true/false</returns>
        public static bool Insert(DiscussionTopicV2Entity info, string ListUserFollow, ref int id)
        {
            bool returnValue = false;
            using (var db = new CmsMainDb())
            {
                returnValue = db.DiscussionTopicContentMainDal.Insert(info, ListUserFollow, ref id);
            }
            return returnValue;
        }
        /// <summary>
        /// Xóa topic
        /// </summary>
        /// <param name="id">int</param>
        /// <param name="username">string</param>
        /// <returns></returns>
        public static bool Delete(int id, string username)
        {
            bool returnValue = false;
            using (var db = new CmsMainDb())
            {
                returnValue = db.DiscussionTopicContentMainDal.Delete(id, username);
            }
            return returnValue;
        }
        public static bool SetStatus(int id)
        {
            bool returnValue = false;
            using (var db = new CmsMainDb())
            {
                returnValue = db.DiscussionTopicContentMainDal.SetStatus(id);
            }
            return returnValue;
        }

        #region Topic Follow
        /// <summary>
        /// Thêm user follow theo topic
        /// </summary>
        /// <param name="DiscussionTopicId">int</param>
        /// <param name="listUserFollow">danh sách user cách nhau bởi dấu [;] vd(a;b;c)</param>
        /// <returns></returns>
        public static bool InsertFollowTopic(int DiscussionTopicId, string listUserFollow, ref int id)
        {
            bool returnValue = false;
            using (var db = new CmsMainDb())
            {
                returnValue = db.DiscussionTopicContentMainDal.InsertFollowTopic(DiscussionTopicId, listUserFollow, ref id);
            }
            return returnValue;
        }
        /// <summary>
        /// Cập nhật trạng thái follow cho user
        /// </summary>
        /// <param name="DiscussionTopicId">int</param>
        /// <param name="UserFollow">string</param>
        /// <param name="isFollow">bool</param>
        /// <returns></returns>
        public static bool UpdateIsFollowTopic(int DiscussionTopicId, string UserFollow, bool isFollow)
        {
            bool returnValue = false;
            using (var db = new CmsMainDb())
            {
                returnValue = db.DiscussionTopicContentMainDal.UpdateIsFollowTopic(DiscussionTopicId, UserFollow, isFollow);
            }
            return returnValue;
        }
        #endregion
    }
}
