﻿using ChannelVN.CMS.Common;
using ChannelVN.DiscussionV2.Dal.Common;
using ChannelVN.DiscussionV2.Entity;
using ChannelVN.DiscussionV2.MainDal.Databases;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace ChannelVN.DiscussionV2.Dal
{
    public class DiscussionAssignV2Dal
    {
        /// <summary>
        /// Thêm mới một danh sách user vào bảng DiscussionAssign
        /// </summary>
        /// <param name="DiscussionTopicId"></param>
        /// <param name="DiscussionId"></param>
        /// <param name="ListToUser"></param>
        /// <param name="ListCCUser"></param>
        /// <param name="ListBccUser"></param>
        /// <param name="id"></param>
        /// <returns></returns>
        public static bool Insert(int DiscussionTopicId, int DiscussionId, string ListToUser, string ListCCUser)
        {
            bool returnValue = false;
            using (var db = new CmsMainDb())
            {
                returnValue = db.DiscussionAssignMainDal.Insert(DiscussionTopicId, DiscussionId, ListToUser, ListCCUser);
            }
            return returnValue;
        }
        /// <summary>
        /// Assign User to Discussion
        /// </summary>
        /// <param name="info"></param>
        /// <param name="id"></param>
        /// <returns></returns>
        public static bool Insert(DiscussionAssignV2Entity info, ref int id)
        {
            bool returnValue = false;
            using (var db = new CmsMainDb())
            {
                returnValue = db.DiscussionAssignMainDal.Insert(info, ref id);
            }
            return returnValue;
        }

        public static bool UpdateIsRead(int DiscussionTopicId, string UserAssigned)
        {
            bool returnValue = false;
            using (var db = new CmsMainDb())
            {
                returnValue = db.DiscussionAssignMainDal.UpdateIsRead(DiscussionTopicId, UserAssigned);
            }
            return returnValue;
        }

        public static List<DiscussionAssignV2Entity> GetNewDiscussionAssignByTopicId(int TopicId, string UserName)
        {
            List<DiscussionAssignV2Entity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.DiscussionAssignMainDal.GetNewDiscussionAssignByTopicId(TopicId, UserName);
            }
            return returnValue;
        }
    }
}
