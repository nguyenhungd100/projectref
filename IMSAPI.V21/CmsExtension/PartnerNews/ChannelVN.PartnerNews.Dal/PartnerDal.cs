﻿using ChannelVN.PartnerNews.Entity;
using ChannelVN.PartnerNews.MainDal.Databases;
using System.Collections.Generic;
namespace ChannelVN.PartnerNews.Dal
{
    public class PartnerDal
    {
        /// <summary>
        /// Thêm mới
        /// </summary>
        /// <param name="info"></param>
        /// <param name="id"></param>
        /// <returns></returns>
        public static bool Insert(PartnerEntity info, ref int id)
        {
            bool returnValue = false;
            using (var db = new CmsMainDb())
            {
                returnValue = db.PartnerContentMainDal.Insert(info, ref id);
            }
            return returnValue;

        }
        /// <summary>
        /// Cập nhật 
        /// </summary>
        /// <param name="info"></param>
        /// <returns></returns>
        public static bool Update(PartnerEntity info)
        {
            bool returnValue = false;
            using (var db = new CmsMainDb())
            {
                returnValue = db.PartnerContentMainDal.Update(info);
            }
            return returnValue;

        }
        /// <summary>
        /// Xóa
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        public static bool Delete(int Id)
        {
            bool returnValue = false;
            using (var db = new CmsMainDb())
            {
                returnValue = db.PartnerContentMainDal.Delete(Id);
            }
            return returnValue;
        }
        /// <summary>
        /// Lấy danh sách có phân trang
        /// </summary>
        /// <param name="pageIndex">int</param>
        /// <param name="pageSize">int</param>
        /// <param name="totalRow">output</param>
        /// <returns>List</returns>
        public static List<PartnerEntity> SelectListField(int pageIndex, int pageSize, ref int totalRow)
        {
            List<PartnerEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.PartnerContentMainDal.SelectListField(pageIndex, pageSize, ref totalRow);
            }
            return returnValue;

        }
        /// <summary>
        /// Lấy danh sách
        /// </summary>
        /// <returns>List</returns>
        public static List<PartnerEntity> SelectListAllField()
        {
            List<PartnerEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.PartnerContentMainDal.SelectListAllField();
            }
            return returnValue;
        }
        /// <summary>
        /// Lấy thông tin chi tiết
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public static PartnerEntity SelectDataById(int id)
        {

            PartnerEntity returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.PartnerContentMainDal.SelectDataById(id);
            }
            return returnValue;
        }
    }
}
