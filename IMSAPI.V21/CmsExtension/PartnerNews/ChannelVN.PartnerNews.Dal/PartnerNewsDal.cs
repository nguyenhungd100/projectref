﻿using ChannelVN.PartnerNews.Entity;
using ChannelVN.PartnerNews.MainDal.Databases;
using System.Collections.Generic;
namespace ChannelVN.PartnerNews.Dal
{
    public class PartnerNewsDal
    {
        /// <summary>
        /// Thêm mới
        /// </summary>
        /// <param name="info"></param>
        /// <param name="id"></param>
        /// <returns></returns>
        public static bool Insert(PartnerNewsEntity info, ref int id)
        {
            bool returnValue = false;
            using (var db = new CmsMainDb())
            {
                returnValue = db.PartnerNewsContentMainDal.Insert(info, ref id);
            }
            return returnValue;

        }
        /// <summary>
        /// Cập nhật 
        /// </summary>
        /// <param name="info"></param>
        /// <returns></returns>
        public static bool Update(PartnerNewsEntity info)
        {
            bool returnValue = false;
            using (var db = new CmsMainDb())
            {
                returnValue = db.PartnerNewsContentMainDal.Update(info);
            }
            return returnValue;

        }
        /// <summary>
        /// Cập nhật Status theo NewsId
        /// </summary>
        /// <param name="newsid"></param>
        /// <param name="status"></param>
        /// <returns></returns>
        public static bool UpdateStatusByNewsId(long newsid,int status)
        {
            bool returnValue = false;
            using (var db = new CmsMainDb())
            {
                returnValue = db.PartnerNewsContentMainDal.UpdateStatusByNewsId(newsid, status);
            }
            return returnValue;

        }
        /// <summary>
        /// Xóa
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        public static bool Delete(long Id)
        {
            bool returnValue = false;
            using (var db = new CmsMainDb())
            {
                returnValue = db.PartnerNewsContentMainDal.Delete(Id);
            }
            return returnValue;
        }

        /// <summary>
        /// Lấy danh sách có phân trang
        /// </summary>
        /// <param name="status"></param>
        /// <param name="pageIndex">int</param>
        /// <param name="pageSize">int</param>
        /// <param name="totalRow">output</param>
        /// <param name="keyword"></param>
        /// <param name="partnerId"></param>
        /// <returns>List</returns>
        public static List<PartnerNewsEntity> SelectListField(string keyword, int partnerId, int status, int pageIndex, int pageSize, ref int totalRow)
        {
            List<PartnerNewsEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.PartnerNewsContentMainDal.SelectListField(keyword, partnerId, status, pageIndex, pageSize, ref totalRow);
            }
            return returnValue;

        }
        /// <summary>
        /// Lấy danh sách
        /// </summary>
        /// <returns>List</returns>
        public static List<PartnerNewsEntity> SelectListAllField()
        {
            List<PartnerNewsEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.PartnerNewsContentMainDal.SelectListAllField();
            }
            return returnValue;
        }
        /// <summary>
        /// Lấy thông tin chi tiết
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public static PartnerNewsEntity SelectDataById(long id)
        {
            PartnerNewsEntity returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.PartnerNewsContentMainDal.SelectDataById(id);
            }
            return returnValue;
        }
    }
}
