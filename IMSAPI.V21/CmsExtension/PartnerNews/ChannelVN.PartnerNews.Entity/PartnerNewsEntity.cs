﻿using ChannelVN.CMS.Common;
using System;
using System.Runtime.Serialization;

namespace ChannelVN.PartnerNews.Entity
{

    [DataContract]
    public enum EnumPartnerNewsStatus : int
    {
        //0: Không chuyển sang viettel (mặc định)
        //1: Chờ chuyển sang viettel
        //2: Đã chuyển thành công
        //3: Chuyển không thành công có lỗi
        //4: Bài cập nhật (sẽ update lại sang viettel)
        //5: Đã remove tin bên viettel thành công (trường hợp gỡ bài)
        //6: Trạng thái bỏ bài viettel đã chuyển sang
        [EnumMember]
        AllStatus = -1,
        [EnumMember]
        Default = 0,
        [EnumMember]
        Waiting = 1,
        [EnumMember]
        Succesfuly = 2,
        [EnumMember]
        Eroor = 3,
        [EnumMember]
        Update = 4,
        [EnumMember]
        Remove = 5,
        [EnumMember]
        UnMove = 6
    }

    [DataContract]
    public class PartnerNewsEntity : EntityBase
    {

        [DataMember]
        public long Id { get; set; }
        [DataMember]
        public int PartnerId { get; set; }
        [DataMember]
        public long NewsId { get; set; }
        [DataMember]
        public string Title { get; set; }
        [DataMember]
        public string SubTitle { get; set; }
        [DataMember]
        public string Sapo { get; set; }
        [DataMember]
        public string Avatar { get; set; }
        [DataMember]
        public string Url { get; set; }
        [DataMember]
        public DateTime DistributionDate { get; set; }
        [DataMember]
        public DateTime LastSendDate { get; set; }
        [DataMember]
        public DateTime CreatedDate { get; set; }
        [DataMember]
        public string CreatedBy { get; set; }
        [DataMember]
        public int Status { get; set; }
    }
}
