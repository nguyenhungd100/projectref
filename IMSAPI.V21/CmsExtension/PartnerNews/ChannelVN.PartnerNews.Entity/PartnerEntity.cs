﻿using ChannelVN.CMS.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace ChannelVN.PartnerNews.Entity
{
    [DataContract]
    public class PartnerEntity : EntityBase
    {

        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public string Name { get; set; }
        [DataMember]
        public string Description { get; set; }
        [DataMember]
        public int Status { get; set; }
        [DataMember]
        public int Priority { get; set; }
         
    }

}
