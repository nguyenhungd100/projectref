﻿using System.Data;
using ChannelVN.CMS.Common.DalCommon;
using ChannelVN.PartnerNews.MainDal.Databases;

namespace ChannelVN.PartnerNews.MainDal.Databases
{
    /// <summary>
    /// The base class for the <see cref="CmsMainDb"/> class that 
    /// represents a connection to the <c>CmsMainDb</c> database. 
    /// </summary>
    /// <remarks>
    /// Do not change this source code. Modify the CmsMainDb class
    /// if you need to add or change some functionality.
    /// </remarks>
    public abstract class CmsMainDbBase : MainDbBase
    {
        #region Store procedures

        #region Partner Properties
        private PartnerDal _partnerContentMainDal;
        public PartnerDalBase PartnerContentMainDal
        {
            get { return _partnerContentMainDal ?? (_partnerContentMainDal = new PartnerDal((CmsMainDb)this)); }
        }
        #endregion

        
        #region PartnerNews Properties
        private PartnerNewsDal _partnerNewsContentMainDal;
        public PartnerNewsDalBase PartnerNewsContentMainDal
        {
            get { return _partnerNewsContentMainDal ?? (_partnerNewsContentMainDal = new PartnerNewsDal((CmsMainDb)this)); }
        }
        #endregion

        #endregion

        #region Constructors

        protected CmsMainDbBase()
        {
        }
        protected CmsMainDbBase(bool init)
        {
            if (init)
            {
                InitConnection();
            }
        }

        #endregion

        #region Protected members

        protected override sealed void InitConnection()
        {
            _connection = CreateConnection();
            _connection.Open();
        }

        /// <summary>
        /// Creates a new connection to the database.
        /// </summary>
        /// <returns>A reference to the <see cref="System.Data.IDbConnection"/> object.</returns>
        protected abstract IDbConnection CreateConnection();

        #endregion
    }
}