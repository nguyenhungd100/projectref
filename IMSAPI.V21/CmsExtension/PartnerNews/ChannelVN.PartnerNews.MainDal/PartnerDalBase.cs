﻿using ChannelVN.PartnerNews.Entity;
using ChannelVN.PartnerNews.MainDal.Databases;
using System;
using System.Collections.Generic;
using ChannelVN.CMS.Common;

namespace ChannelVN.PartnerNews.MainDal
{
    public class PartnerDalBase
    {
        #region Function GET
        /// <summary>
        /// Lấy danh sách có phân trang
        /// </summary>
        /// <param name="pageIndex">int</param>
        /// <param name="pageSize">int</param>
        /// <param name="totalRow">output</param>
        /// <returns>List</returns>
        public List<PartnerEntity> SelectListField(int pageIndex, int pageSize, ref int totalRow)
        {
            const string commandText = "CMS_Partner_GetAllData";
            try
            {
                List<PartnerEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "TotalRow", totalRow, System.Data.ParameterDirection.Output);
                _db.AddParameter(cmd, "PageIndex", pageIndex);
                _db.AddParameter(cmd, "PageSize", pageSize);
                data = _db.GetList<PartnerEntity>(cmd);
                totalRow = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 0));
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        /// <summary>
        /// Lấy danh sách 
        /// </summary>
        /// <returns>List</returns>
        public List<PartnerEntity> SelectListAllField()
        {
            const string commandText = "CMS_Partner_SelectListAllField";
            try
            {
                List<PartnerEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                data = _db.GetList<PartnerEntity>(cmd);
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        /// <summary>
        /// Lấy thông tin chi tiết 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public PartnerEntity SelectDataById(int id)
        {
            const string commandText = "CMS_Partner_GetDataById";
            try
            {
                PartnerEntity data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", id);
                data = _db.Get<PartnerEntity>(cmd);
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        #endregion
        
        #region Function SET
        public bool Insert(PartnerEntity info, ref int id)
        {
            const string commandText = "CMS_Partner_Insert";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", id, System.Data.ParameterDirection.Output);
                _db.AddParameter(cmd, "Name", info.Name);
                _db.AddParameter(cmd, "Description", info.Description);
                _db.AddParameter(cmd, "Status", info.Status);
                _db.AddParameter(cmd, "Priority", info.Priority);
                var data = cmd.ExecuteNonQuery();
                id = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 0));
                return data > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool Update(PartnerEntity info)
        {
            const string commandText = "CMS_Partner_Update";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", info.Id);
                _db.AddParameter(cmd, "Name", info.Name);
                _db.AddParameter(cmd, "Description", info.Description);
                _db.AddParameter(cmd, "Status", info.Status);
                _db.AddParameter(cmd, "Priority", info.Priority);
                var data = cmd.ExecuteNonQuery();
                return data > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool Delete(int Id)
        {
            const string commandText = "CMS_Partner_Delete";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", Id);
                var data = cmd.ExecuteNonQuery();
                return data != 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        #endregion

        /// <summary>
        /// CuongNP
        /// 2014/11/17
        /// </summary>

        #region Constructor

        private readonly CmsMainDb _db;

        protected PartnerDalBase(CmsMainDb db)
        {
            _db = db;
        }

        protected CmsMainDb Database
        {
            get { return _db; }
        }

        #endregion
    }
}
