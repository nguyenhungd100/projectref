﻿using ChannelVN.PartnerNews.Entity;
using ChannelVN.PartnerNews.MainDal.Databases;
using System;
using System.Collections.Generic;
using ChannelVN.CMS.Common;

namespace ChannelVN.PartnerNews.MainDal
{
    public class PartnerNewsDalBase
    {
        #region Function GET

        /// <summary>
        /// Lấy danh sách có phân trang
        /// </summary>
        /// <param name="status"></param>
        /// <param name="pageIndex">int</param>
        /// <param name="pageSize">int</param>
        /// <param name="totalRow">output</param>
        /// <param name="keyword"></param>
        /// <param name="partnerId"></param>
        /// <returns>List</returns>
        public List<PartnerNewsEntity> SelectListField(string keyword ,int partnerId, int status, int pageIndex, int pageSize, ref int totalRow)
        {
            const string commandText = "CMS_PartnerNews_GetAllData";
            try
            {
                List<PartnerNewsEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "TotalRow", totalRow, System.Data.ParameterDirection.Output);
                _db.AddParameter(cmd, "Keyword", keyword);
                _db.AddParameter(cmd, "PartnerId", partnerId);
                _db.AddParameter(cmd, "Status", status);
                _db.AddParameter(cmd, "PageIndex", pageIndex);
                _db.AddParameter(cmd, "PageSize", pageSize);
                data = _db.GetList<PartnerNewsEntity>(cmd);
                totalRow = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 0));
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        /// <summary>
        /// Lấy danh sách 
        /// </summary>
        /// <returns>List</returns>
        public List<PartnerNewsEntity> SelectListAllField()
        {
            const string commandText = "CMS_PartnerNews_SelectListAllField";
            try
            {
                List<PartnerNewsEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                data = _db.GetList<PartnerNewsEntity>(cmd);
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        /// <summary>
        /// Lấy thông tin chi tiết 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public PartnerNewsEntity SelectDataById(long id)
        {
            const string commandText = "CMS_PartnerNews_GetDataById";
            try
            {
                PartnerNewsEntity data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", id);
                data = _db.Get<PartnerNewsEntity>(cmd);
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        #endregion
        
        #region Function SET
        public bool Insert(PartnerNewsEntity info, ref int id)
        {
            const string commandText = "CMS_PartnerNews_Insert";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", id, System.Data.ParameterDirection.Output);
                _db.AddParameter(cmd, "PartnerId", info.PartnerId);
                _db.AddParameter(cmd, "NewsId", info.NewsId);
                _db.AddParameter(cmd, "DistributionDate", info.DistributionDate);
                _db.AddParameter(cmd, "LastSendDate", info.LastSendDate);
                _db.AddParameter(cmd, "CreatedDate", info.CreatedDate);
                _db.AddParameter(cmd, "CreatedBy", info.CreatedBy);
                _db.AddParameter(cmd, "Status", info.Status);
                var data = cmd.ExecuteNonQuery();
                id = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 0));
                return data > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool Update(PartnerNewsEntity info)
        {
            const string commandText = "CMS_PartnerNews_Update";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "PartnerId", info.PartnerId);
                _db.AddParameter(cmd, "NewsId", info.NewsId);
                _db.AddParameter(cmd, "DistributionDate", info.DistributionDate);
                _db.AddParameter(cmd, "LastSendDate", info.LastSendDate);
                _db.AddParameter(cmd, "Status", info.Status);
                var data = cmd.ExecuteNonQuery();
                return data > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool UpdateStatusByNewsId(long newsid,int status)
        {
            const string commandText = "CMS_PartnerNews_UpdateStatusByNewsId";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "NewsId", newsid);
                _db.AddParameter(cmd, "Status", status);
                var data = cmd.ExecuteNonQuery();
                return data > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool Delete(long Id)
        {
            const string commandText = "CMS_PartnerNews_Delete";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", Id);
                var data = cmd.ExecuteNonQuery();
                return data != 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        #endregion

        /// <summary>
        /// CuongNP
        /// 2014/11/17
        /// </summary>

        #region Constructor

        private readonly CmsMainDb _db;

        protected PartnerNewsDalBase(CmsMainDb db)
        {
            _db = db;
        }

        protected CmsMainDb Database
        {
            get { return _db; }
        }

        #endregion
    }
}
