﻿using ChannelVN.PartnerNews.Dal;
using ChannelVN.PartnerNews.Entity;
using ChannelVN.PartnerNews.Entity.ErrorCode;
using System.Collections.Generic;

namespace ChannelVN.PartnerNews.Bo
{
    public class PartnerBo
    {
        public static ErrorMapping.ErrorCodes Insert(PartnerEntity info, ref int id)
        {
            var result = ErrorMapping.ErrorCodes.BusinessError;
            var createSuccess = PartnerDal.Insert(info, ref id);
            if (createSuccess && id > 0)
                result = ErrorMapping.ErrorCodes.Success;
            return result;
        }

        public static ErrorMapping.ErrorCodes Update(PartnerEntity info)
        {
            var result = ErrorMapping.ErrorCodes.BusinessError;
            var createSuccess = PartnerDal.Update(info);
            if (createSuccess && info.Id > 0)
                result = ErrorMapping.ErrorCodes.Success;
            return result;
        }
        public static ErrorMapping.ErrorCodes Delete(int Id)
        {
            var result = ErrorMapping.ErrorCodes.BusinessError;
            var createSuccess = PartnerDal.Delete(Id);
            if (createSuccess)
                result = ErrorMapping.ErrorCodes.Success;
            return result;
        }
        public static List<PartnerEntity> SelectListField(int pageIndex, int pageSize, ref int totalRow)
        {
            return PartnerDal.SelectListField(pageIndex, pageSize, ref totalRow);
        }
        public static List<PartnerEntity> SelectListAllField() {
            return PartnerDal.SelectListAllField();
        }
        public static PartnerEntity SelectDataById(int id)
        {
            return PartnerDal.SelectDataById(id);
        }
    }
}
