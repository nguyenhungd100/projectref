﻿using ChannelVN.PartnerNews.Dal;
using ChannelVN.PartnerNews.Entity;
using ChannelVN.PartnerNews.Entity.ErrorCode;
using System.Collections.Generic;

namespace ChannelVN.PartnerNews.Bo
{
    public class PartnerNewsBo
    {
        public static ErrorMapping.ErrorCodes Insert(PartnerNewsEntity info, ref int id)
        {
            var result = ErrorMapping.ErrorCodes.BusinessError;
            var createSuccess = PartnerNewsDal.Insert(info, ref id);
            if (createSuccess && id > 0)
                result = ErrorMapping.ErrorCodes.Success;
            return result;
        }

        public static ErrorMapping.ErrorCodes Update(PartnerNewsEntity info)
        {
            var result = ErrorMapping.ErrorCodes.BusinessError;
            var createSuccess = PartnerNewsDal.Update(info);
            if (createSuccess && info.Id > 0)
                result = ErrorMapping.ErrorCodes.Success;
            return result;
        }
        public static ErrorMapping.ErrorCodes UpdateStatusByNewsId(long newsid, int status)
        {
            var result = ErrorMapping.ErrorCodes.BusinessError;
            var createSuccess = PartnerNewsDal.UpdateStatusByNewsId(newsid, status);
            if (createSuccess)
                result = ErrorMapping.ErrorCodes.Success;
            return result;
        }
        public static ErrorMapping.ErrorCodes Delete(long Id)
        {
            var result = ErrorMapping.ErrorCodes.BusinessError;
            var createSuccess = PartnerNewsDal.Delete(Id);
            if (createSuccess)
                result = ErrorMapping.ErrorCodes.Success;
            return result;
        }
        public static List<PartnerNewsEntity> SelectListField(string keyword, int partnerId, int status, int pageIndex, int pageSize, ref int totalRow)
        {
            return PartnerNewsDal.SelectListField(keyword, partnerId, status, pageIndex, pageSize, ref totalRow);
        }
        public static List<PartnerNewsEntity> SelectListAllField() {
            return PartnerNewsDal.SelectListAllField();
        }
        public static PartnerNewsEntity SelectDataById(long id)
        {
            return PartnerNewsDal.SelectDataById(id);
        }
    }
}
