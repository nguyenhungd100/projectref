﻿using System;
using System.Data;
using System.Data.SqlClient;
using ChannelVN.CMS.Common;
using ChannelVN.ForExternalApplication.Dal.Common;
using ChannelVN.ForExternalApplication.Entity;
using ChannelVN.ForExternalApplication.MainDal.Databases;

namespace ChannelVN.ForExternalApplication.Dal
{
    public class NewsHistoryDal
    {
        public static bool InsertNewsHistoryForAdPage(long newsId, string actionName, string sentBy, string receivedBy, int status, string description)
        {
            bool retVal = false;
            using (var db = new CmsMainDb())
            {
                retVal = db.NewsHistoryMainDal.InsertNewsHistoryForAdPage(newsId, actionName, sentBy, receivedBy, status, description);
            }
            return retVal;
        }
    }
}
