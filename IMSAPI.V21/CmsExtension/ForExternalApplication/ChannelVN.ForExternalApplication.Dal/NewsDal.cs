﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using ChannelVN.CMS.Common;
using ChannelVN.ForExternalApplication.Dal.Common;
using ChannelVN.ForExternalApplication.Entity;
using ChannelVN.ForExternalApplication.MainDal.Databases;

namespace ChannelVN.ForExternalApplication.Dal
{
    public class NewsDal
    {
        public static List<NewsForAdStoreEntity> SearchNewsForAdStore(string keyword, int zoneId, DateTime fromDistributionDate, DateTime toDistributionDate, int pageIndex, int pageSize, ref int totalRow)
        {
            List<NewsForAdStoreEntity> retVal;
            using (var db = new CmsMainDb())
            {
                retVal = db.NewsMainDal.SearchNewsForAdStore(keyword, zoneId, fromDistributionDate, toDistributionDate, pageIndex, pageSize, ref totalRow);
            }
            return retVal;
        }

        public static NewsForAdStoreEntity GetNewsForAdStoreById(long newsId)
        {
            NewsForAdStoreEntity retVal;
            using (var db = new CmsMainDb())
            {
                retVal = db.NewsMainDal.GetNewsForAdStoreById(newsId);
            }
            return retVal;
        }

        public static bool UpdateAdStoreMode(long newsId, int adStore, string adStoreUrl)
        {
            bool retVal = false;
            using (var db = new CmsMainDb())
            {
                retVal = db.NewsMainDal.UpdateAdStoreMode(newsId, adStore, adStoreUrl);
            }
            return retVal;
        }
    }
}
