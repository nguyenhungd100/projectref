﻿using System;
using ChannelVN.CMS.Common.ChannelConfig;
using ChannelVN.ForExternalApplication.Entity;
using ChannelVN.WcfExtensions;
using ChannelVN.CMS.Common;

namespace ChannelVN.ForExternalApplication.Dal.Common
{
    public class DbCommon
    {
        private const string _CONNECTION_DECRYPT_KEY = "nfvsMof35XnUdQEWuxgAZta";

        private static string GetConnectionName(Connection connection)
        {
            switch (connection)
            {
                case Connection.CmsMainDb:
                    return "CmsMainDb";
                case Connection.ExternalCmsDb:
                    return "ExternalCmsDb";
                case Connection.CmsCrawlerDb:
                    return "CmsCrawlerDb";
                case Connection.CmsVideoDb:
                    return "CmsVideoDb";
                case Connection.CmsPhotoDb:
                    return "CmsPhotoDb";
                default:
                    return "";
            }
        }

        public enum Connection
        {
            CmsMainDb = 1,
            ExternalCmsDb = 2,
            CmsCrawlerDb = 3,
            CmsVideoDb = 4,
            CmsPhotoDb = 5
        }
        public static DateTime MinDateTime = new DateTime(1980, 1, 1);
        public static string DatabaseSchema = "[dbo].";
        public static string GetConnectionString(Connection connection)
        {
            return ServiceChannelConfiguration.GetConnectionString(WcfMessageHeader.Current.Namespace, GetConnectionName(connection),
                                                            _CONNECTION_DECRYPT_KEY);
        }
        public static bool IsUseMainDal
        {
            get
            {
                return Utility.ConvertToBoolean(ServiceChannelConfiguration.GetAppSetting(WcfMessageHeader.Current.Namespace, "IsUseMainDal"));
            }
        }
    }
}
