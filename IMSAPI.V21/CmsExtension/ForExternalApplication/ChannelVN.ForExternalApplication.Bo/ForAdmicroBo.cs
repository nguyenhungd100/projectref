﻿using System;
using System.Collections.Generic;
using ChannelVN.CMS.BO.Base.News;
using ChannelVN.CMS.Common;
using ChannelVN.ForExternalApplication.Dal;
using ChannelVN.ForExternalApplication.Entity;
using ChannelVN.ForExternalApplication.Entity.ErrorCode;
using System.Linq;
using ChannelVN.CMS.BoSearch.Entity;

namespace ChannelVN.ForExternalApplication.Bo
{
    public class ForAdmicroBo
    {
        public static ErrorMapping.ErrorCodes UpdateAdStoreMode(ChannelMappingForExternalApplication channelMapping, long newsId, EnumAdPageMode adStore, string adStoreUrl="")
        {
            try
            {
                ChannelMapping.SetChannelNamespace(channelMapping);

                if (newsId <= 0)
                {
                    return ErrorMapping.ErrorCodes.UpdateNewsNewsNotFound;
                }
                var existsNews = NewsDal.GetNewsForAdStoreById(newsId);
                if (existsNews == null)
                {
                    return ErrorMapping.ErrorCodes.UpdateNewsNewsNotFound;
                }
                
                if (adStore == EnumAdPageMode.AdPageNews)
                {
                    if(string.IsNullOrEmpty(adStoreUrl))
                        adStoreUrl = NewsBo.BuildLinkUrlAdStore(existsNews.Id, existsNews.PrimaryZoneUrl, existsNews.PrimaryZoneId,existsNews.Title);
                }
                if (NewsDal.UpdateAdStoreMode(newsId, (int)adStore, adStoreUrl))
                {
                    //update redis
                    CMS.BoCached.Base.News.NewsDalFactory.UpdateAdStoreMode(newsId, (int)adStore, adStoreUrl);

                    CMS.BoSearch.Base.News.NewsDalFactory.UpdateAdStoreMode(newsId, (int)adStore, adStoreUrl);

                    NewsHistoryDal.InsertNewsHistoryForAdPage(newsId, "Đổi trạng thái AdPage", "Admicro",
                                                              "", existsNews.Status, adStore == EnumAdPageMode.AdPageNews
                                                                                         ? "Thay đổi trạng thái thành bài AdPage, Url: "+ adStoreUrl
                                                                                         : "Thay đổi trạng thái thành bài thông thường Url: "+ adStoreUrl);
                    return ErrorMapping.ErrorCodes.Success;
                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Fatal, ex.ToString());
            }
            return ErrorMapping.ErrorCodes.BusinessError;
        }

        public static NewsForAdStoreEntity GetNewsForAdStoreById(ChannelMappingForExternalApplication channelMapping, long newsId)
        {
            ChannelMapping.SetChannelNamespace(channelMapping);

            var data = CMS.BoCached.Base.News.NewsDalFactory.GetNewsById(newsId);
            if (data != null)
            {
                return new NewsForAdStoreEntity {
                    Id=data.Id,
                    AdStore=data.AdStore,
                    AdStoreUrl=data.AdStoreUrl,
                    Avatar = data.Avatar,
                    DistributionDate = data.DistributionDate,
                    IsPr = data.IsPr,
                    ZoneId = data.ZoneId,
                    ZoneName = data.ZoneName,
                    Title=data.Title,
                    Sapo=data.Sapo,
                    Status=data.Status,
                    Url=data.Url,
                    PrPosition=data.PrPosition                                        
                };
            }

            return NewsDal.GetNewsForAdStoreById(newsId);
        }

        public static List<NewsForAdStoreEntity> SearchNewsForAdStore(ChannelMappingForExternalApplication channelMapping, string keyword, int zoneId,
                                                                         DateTime fromDistributionDate,
                                                                         DateTime toDistributionDate, int pageIndex,
                                                                         int pageSize, ref int totalRow)
        {
            ChannelMapping.SetChannelNamespace(channelMapping);

            //es
            var data = CMS.BoSearch.Base.News.NewsDalFactory.SearchNewsForAdPage(keyword, zoneId, fromDistributionDate, toDistributionDate, pageIndex, pageSize, ref totalRow);
            if(data!=null && data.Count > 0)
            {
                return data.Select(s=> new NewsForAdStoreEntity
                {
                    Id = s.Id,
                    AdStore = s.AdStore,
                    AdStoreUrl = s.AdStoreUrl,
                    Avatar = s.Avatar,
                    DistributionDate = s.DistributionDate,
                    IsPr = s.IsPr,
                    ZoneId = s.ZoneId,
                    ZoneName = s.ZoneName,
                    Title = s.Title,
                    Sapo = s.Sapo,
                    Status = s.Status,
                    Url = s.Url,
                    PrPosition = s.PrPosition                    
                }).ToList();
            }
            var db= NewsDal.SearchNewsForAdStore(keyword, zoneId, fromDistributionDate, toDistributionDate,
                                                      pageIndex, pageSize, ref totalRow);
            if (db != null && db.Count > 0)
            {
                CMS.BoSearch.Base.News.NewsDalFactory.InitAllNewsForAdPage(db.Select(s => new NewsSearchForAdStoreEntity
                {
                    Id = s.Id,
                    AdStore = s.AdStore,
                    AdStoreUrl = s.AdStoreUrl,
                    Avatar = s.Avatar,
                    DistributionDate = s.DistributionDate,
                    IsPr = s.IsPr,
                    ZoneId = s.ZoneId,
                    ZoneName = s.ZoneName,
                    Title = s.Title,
                    Sapo = s.Sapo,
                    Status = s.Status,
                    Url = s.Url,
                    PrPosition = s.PrPosition
                }).ToList());
            }

            return db;
        }

    }
}
