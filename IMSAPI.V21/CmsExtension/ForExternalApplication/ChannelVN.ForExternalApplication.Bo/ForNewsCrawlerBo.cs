﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ChannelVN.CMS.BO.Base.News;
using ChannelVN.CMS.BO.Base.Zone;
using ChannelVN.CMS.Entity.Base.News;
using ChannelVN.ForExternalApplication.Entity;
using ChannelVN.ForExternalApplication.Entity.ErrorCode;

namespace ChannelVN.ForExternalApplication.Bo
{
    public class ForNewsCrawlerBo
    {
        public static ErrorMapping.ErrorCodes CrawlAndPublish(ChannelMappingForExternalApplication channelMapping, NewsCrawlerResultEntity newsCrawlerResult, ref long newsId, ref string newsUrl)
        {
            ChannelMapping.SetChannelNamespace(channelMapping);

            var zoneId = newsCrawlerResult.ZoneId;
            var zone = ZoneBo.GetZoneById(zoneId);
            if (zone == null)
            {
                return ErrorMapping.ErrorCodes.BusinessError;
            }

            const string imageFormat =
                "<div class=\"VCSortableInPreviewMode\" style=\"display:inline-block;width:100%; text-align:center;-webkit-box-sizing: border-box;-moz-box-sizing: border-box;box-sizing: border-box;\" type=\"Photo\"><div><img alt=\"\" id=\"img_{0}\" photoid=\"{0}\" rel=\"{1}\" src=\"{2}\" style=\"max-width:100%;\" title=\"{3}\" type=\"photo\" /></div><div class=\"PhotoCMS_Caption\">{3}</div></div><p>&nbsp;</p>";
            const string videoFormat =
                            "<div class=\"VCSortableInPreviewMode\" style=\"display:inline-block;width:100%; text-align:center;-webkit-box-sizing: border-box;-moz-box-sizing: border-box;box-sizing: border-box;\" type=\"Video\"><div id=\"videoid_{0}\" videoid=\"{0}\">{1}</div><div class=\"VideoCMS_Caption\">{2}</div></div><p>&nbsp;</p>";

            var news = new NewsEntity
                {
                    Title = newsCrawlerResult.Title,
                    SubTitle = newsCrawlerResult.SubTitle,
                    Sapo = newsCrawlerResult.Sapo,
                    Body = newsCrawlerResult.Body,
                    Avatar = newsCrawlerResult.Avatar,
                    AvatarDesc = newsCrawlerResult.AvatarDesc,
                    Source = newsCrawlerResult.Source,
                    Url = newsCrawlerResult.Url
                };
            var newsIdEncrypt = "";
            if (NewsBo.InsertNews(news, zoneId, "", "", "", "", "crawler", ref newsId, ref newsIdEncrypt,
                                  new List<string>(), 0, 0, new List<NewsExtensionEntity>()) ==
                CMS.Entity.ErrorCode.ErrorMapping.ErrorCodes.Success)
            {
                if (NewsBo.Publish(newsId, 0, 0, 0, 0, "crawler", ref newsUrl, "") ==
                    CMS.Entity.ErrorCode.ErrorMapping.ErrorCodes.Success)
                {
                    return ErrorMapping.ErrorCodes.Success;
                }
            }
            return ErrorMapping.ErrorCodes.BusinessError;
        }

        public static List<ZoneForCrawlerEntity> GetAllZoneForCrawler(
            ChannelMappingForExternalApplication channelMapping)
        {
            ChannelMapping.SetChannelNamespace(channelMapping);

            var allZone = ZoneBo.GetAllZoneActived();
            return allZone.Select(zone => new ZoneForCrawlerEntity
                {
                    Id = zone.Id,
                    Name = zone.Name,
                    ShortUrl = zone.ShortUrl
                }).ToList();
        }
    }
}
