﻿using ChannelVN.ForExternalApplication.MainDal.Databases;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ChannelVN.ForExternalApplication.MainDal
{
    public abstract class NewsHistoryDalBase
    {
        #region SET

        public bool InsertNewsHistoryForAdPage(long newsId, string actionName, string sentBy, string receivedBy, int status, string description)
        {
            const string commandText = "CMS_NewsHistory_InsertNewsHistory";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "NewsId", newsId);
                _db.AddParameter(cmd, "ActionName", actionName);
                _db.AddParameter(cmd, "SentBy", sentBy);
                _db.AddParameter(cmd, "ReceivedBy", receivedBy);
                _db.AddParameter(cmd, "Status", status);
                _db.AddParameter(cmd, "Description", description);
                var result = cmd.ExecuteNonQuery();

                return result > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        #endregion

        /// Chuyển đổi DAL cho NewsHistoryDalBase
        /// minhduongvan
        /// 2014/01/09
        /// </summary>

        #region Constructor

        private readonly CmsMainDb _db;

        protected NewsHistoryDalBase(CmsMainDb db)
        {
            _db = db;
        }

        protected CmsMainDb Database
        {
            get { return _db; }
        }

        #endregion
    }
}
