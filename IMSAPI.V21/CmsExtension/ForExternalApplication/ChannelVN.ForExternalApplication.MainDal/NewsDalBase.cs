﻿using ChannelVN.CMS.Common;
using ChannelVN.CMS.MainDal.Common;
using ChannelVN.ForExternalApplication.Entity;
using ChannelVN.ForExternalApplication.MainDal.Databases;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChannelVN.ForExternalApplication.MainDal
{
    public abstract class NewsDalBase
    {
        public List<NewsForAdStoreEntity> SearchNewsForAdStore(string keyword, int zoneId, DateTime fromDistributionDate, DateTime toDistributionDate, int pageIndex, int pageSize, ref int totalRow)
        {
            const string commandText = "CMS_News_SearchForAdStore";
            try
            {                
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "TotalRow", totalRow, System.Data.ParameterDirection.Output);
                _db.AddParameter(cmd, "Keyword", keyword);
                _db.AddParameter(cmd, "ZoneId", zoneId);                
                if (fromDistributionDate <= Constants.MinDateTime)
                    _db.AddParameter(cmd, "DistributionDateFrom", DBNull.Value);
                else _db.AddParameter(cmd, "DistributionDateFrom", fromDistributionDate);
                if (toDistributionDate <= Constants.MinDateTime)
                    _db.AddParameter(cmd, "DistributionDateTo", DBNull.Value);
                else _db.AddParameter(cmd, "DistributionDateTo", toDistributionDate);
                _db.AddParameter(cmd, "PageIndex", pageIndex);
                _db.AddParameter(cmd, "PageSize", pageSize);

                var data = _db.GetList<NewsForAdStoreEntity>(cmd);
                totalRow = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 0));

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public NewsForAdStoreEntity GetNewsForAdStoreById(long newsId)
        {
            const string commandText = "CMS_News_GetForAdStoreByNewsId";
            try
            {
                NewsForAdStoreEntity data;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "NewsId", newsId);
                data = _db.Get<NewsForAdStoreEntity>(cmd);
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool UpdateAdStoreMode(long newsId, int adStore, string adStoreUrl)
        {
            const string commandText = "CMS_News_UpdateAdStoreMode";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "NewsId", newsId);
                _db.AddParameter(cmd, "AdStore", adStore);
                _db.AddParameter(cmd, "AdStoreUrl", adStoreUrl);
                var data = cmd.ExecuteNonQuery();
                return data > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        /// Chuyển đổi DAL cho NewsDalBase
        /// minhduongvan
        /// 2014/01/09
        /// </summary>

        #region Constructor

        private readonly CmsMainDb _db;

        protected NewsDalBase(CmsMainDb db)
        {
            _db = db;
        }

        protected CmsMainDb Database
        {
            get { return _db; }
        }

        #endregion
    }
}
