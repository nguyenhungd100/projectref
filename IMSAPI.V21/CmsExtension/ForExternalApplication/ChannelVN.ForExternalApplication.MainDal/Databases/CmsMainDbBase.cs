﻿using System.Data;
using ChannelVN.CMS.Common.DalCommon;
using ChannelVN.ForExternalApplication.MainDal.Databases;
using ChannelVN.ForExternalApplication.MainDal;

namespace ChannelVN.ForExternalApplication.MainDal.Databases
{
    /// <summary>
    /// The base class for the <see cref="CmsMainDb"/> class that 
    /// represents a connection to the <c>CmsMainDb</c> database. 
    /// </summary>
    /// <remarks>
    /// Do not change this source code. Modify the CmsMainDb class
    /// if you need to add or change some functionality.
    /// </remarks>
    public abstract class CmsMainDbBase : MainDbBase
    {
        #region Store procedures

        #region News Dal
        private NewsDal _newsMainDal;
        public NewsDalBase NewsMainDal
        {
            get { return _newsMainDal ?? (_newsMainDal = new NewsDal((CmsMainDb)this)); }
        }
        #endregion
        #region News History Dal
        private NewsHistoryDal _newsHistoryMainDal;
        public NewsHistoryDalBase NewsHistoryMainDal
        {
            get { return _newsHistoryMainDal ?? (_newsHistoryMainDal = new NewsHistoryDal((CmsMainDb)this)); }
        }
        #endregion


        #endregion

        #region Constructors

        protected CmsMainDbBase()
        {
        }
        protected CmsMainDbBase(bool init)
        {
            if (init)
            {
                InitConnection();
            }
        }

        #endregion

        #region Protected members

        protected override sealed void InitConnection()
        {
            _connection = CreateConnection();
            _connection.Open();
        }

        /// <summary>
        /// Creates a new connection to the database.
        /// </summary>
        /// <returns>A reference to the <see cref="System.Data.IDbConnection"/> object.</returns>
        protected abstract IDbConnection CreateConnection();

        #endregion
    }
}