﻿using System.Runtime.Serialization;
using ChannelVN.WcfExtensions;

namespace ChannelVN.ForExternalApplication.Entity
{
    [DataContract]
    public enum ChannelMappingForExternalApplication
    {
        [EnumMember]
        SohaNews = 1,
        [EnumMember]
        GenK = 2,
        [EnumMember]
        TimDiemThi = 3,
        [EnumMember]
        AutoPro = 4,
        [EnumMember]
        SucKhoeDoiSong = 5,
        [EnumMember]
        NguoiLaoDong = 6,
        [EnumMember]
        GameK = 7,
        [EnumMember]
        Kenh14 = 8,
        [EnumMember]
        Afamily = 9,
        [EnumMember]
        VnEconomy = 10,
        [EnumMember]
        GiaDinhNet = 11,
        [EnumMember]
        CafeF = 12,
        [EnumMember]
        CafeBiz = 13,
        [EnumMember]
        DanTri = 14
    }
    public class ChannelMapping
    {
        public static void SetChannelNamespace(ChannelMappingForExternalApplication channelMapping)
        {
            switch (channelMapping)
            {
                case ChannelMappingForExternalApplication.SohaNews:
                    WcfMessageHeader.CustomWcfHeader = new WcfMessageHeader
                        {
                            SecretKey = "",
                            ClientUsername = "admicro",
                            Namespace = "SohaNews"
                        };
                    break;
                case ChannelMappingForExternalApplication.GenK:
                    WcfMessageHeader.CustomWcfHeader = new WcfMessageHeader
                        {
                            SecretKey = "",
                            ClientUsername = "admicro",
                            Namespace = "GenK"
                        };
                    break;
                case ChannelMappingForExternalApplication.TimDiemThi:
                    WcfMessageHeader.CustomWcfHeader = new WcfMessageHeader
                    {
                        SecretKey = "",
                        ClientUsername = "admicro",
                        Namespace = "TimDiemThi"
                    };
                    break;
                case ChannelMappingForExternalApplication.AutoPro:
                    WcfMessageHeader.CustomWcfHeader = new WcfMessageHeader
                    {
                        SecretKey = "",
                        ClientUsername = "admicro",
                        Namespace = "AutoPro"
                    };
                    break;
                case ChannelMappingForExternalApplication.SucKhoeDoiSong:
                    WcfMessageHeader.CustomWcfHeader = new WcfMessageHeader
                    {
                        SecretKey = "",
                        ClientUsername = "admicro",
                        Namespace = "SucKhoeDoiSong"
                    };
                    break;
                case ChannelMappingForExternalApplication.NguoiLaoDong:
                    WcfMessageHeader.CustomWcfHeader = new WcfMessageHeader
                    {
                        SecretKey = "",
                        ClientUsername = "admicro",
                        Namespace = "NguoiLaoDong"
                    };
                    break;
                case ChannelMappingForExternalApplication.GameK:
                    WcfMessageHeader.CustomWcfHeader = new WcfMessageHeader
                    {
                        SecretKey = "",
                        ClientUsername = "admicro",
                        Namespace = "GameK"
                    };
                    break;
                case ChannelMappingForExternalApplication.Kenh14:
                    WcfMessageHeader.CustomWcfHeader = new WcfMessageHeader
                    {
                        SecretKey = "",
                        ClientUsername = "admicro",
                        Namespace = "Kenh14"
                    };
                    break;
                case ChannelMappingForExternalApplication.Afamily:
                    WcfMessageHeader.CustomWcfHeader = new WcfMessageHeader
                    {
                        SecretKey = "",
                        ClientUsername = "",
                        Namespace = "Afamily"
                    };
                    break;
                case ChannelMappingForExternalApplication.GiaDinhNet:
                    WcfMessageHeader.CustomWcfHeader = new WcfMessageHeader
                    {
                        SecretKey = "",
                        ClientUsername = "admicro",
                        Namespace = "GiaDinhNet"
                    };
                    break;
                case ChannelMappingForExternalApplication.CafeBiz:
                    WcfMessageHeader.CustomWcfHeader = new WcfMessageHeader
                    {
                        SecretKey = "",
                        ClientUsername = "admicro",
                        Namespace = "CafeBiz"
                    };
                    break;
                case ChannelMappingForExternalApplication.CafeF:
                    WcfMessageHeader.CustomWcfHeader = new WcfMessageHeader
                    {
                        SecretKey = "",
                        ClientUsername = "admicro",
                        Namespace = "CafeF"
                    };
                    break;
                case ChannelMappingForExternalApplication.DanTri:
                    WcfMessageHeader.CustomWcfHeader = new WcfMessageHeader
                    {
                        SecretKey = "",
                        ClientUsername = "admicro",
                        Namespace = "DanTri"
                    };
                    break;
                case ChannelMappingForExternalApplication.VnEconomy:
                    WcfMessageHeader.CustomWcfHeader = new WcfMessageHeader
                    {
                        SecretKey = "",
                        ClientUsername = "admicro",
                        Namespace = "VNEconomy"
                    };
                    break;
                default:
                    WcfMessageHeader.CustomWcfHeader = new WcfMessageHeader
                        {
                            SecretKey = "",
                            ClientUsername = "admicro",
                            Namespace = "InvalidNamespace"
                        };
                    break;
            }
        }
    }
}
