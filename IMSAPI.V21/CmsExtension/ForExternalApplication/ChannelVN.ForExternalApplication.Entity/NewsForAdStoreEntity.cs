﻿using System;
using System.Runtime.Serialization;
using ChannelVN.CMS.Common;

namespace ChannelVN.ForExternalApplication.Entity
{
    [DataContract]
    public enum EnumAdPageMode
    {
        [EnumMember]
        NormalNews = 0,
        [EnumMember]
        AdPageNews = 1
    }

    [DataContract]
    public class NewsForAdStoreEntity : EntityBase
    {
        [DataMember]
        public long Id { get; set; }
        [DataMember]
        public string EncryptId { get { return CryptonForId.EncryptId(Id); } set { } }
        [DataMember]
        public int PrimaryZoneId { get; set; }
        [DataMember]
        public string PrimaryZoneUrl { get; set; }
        [DataMember]
        public string Title { get; set; }
        [DataMember]
        public string Sapo { get; set; }
        [DataMember]
        public string Avatar { get; set; }
        [DataMember]
        public DateTime DistributionDate { get; set; }
        [DataMember]
        public string Url { get; set; }
        [DataMember]
        public bool IsPr { get; set; }
        [DataMember]
        public bool AdStore { get; set; }
        [DataMember]
        public string AdStoreUrl { get; set; }
        [DataMember]
        public int Status { get; set; }

        [DataMember]
        public int PrPosition { get; set; }
        [DataMember]
        public int ZoneId { get; set; }
        [DataMember]
        public string ZoneName { get; set; }
    }
}
