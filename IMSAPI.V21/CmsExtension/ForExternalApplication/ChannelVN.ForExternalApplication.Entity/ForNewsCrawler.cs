﻿using System.Runtime.Serialization;
using ChannelVN.CMS.Common;

namespace ChannelVN.ForExternalApplication.Entity
{
    [DataContract]
    public class NewsCrawlerResultEntity : EntityBase
    {
        [DataMember]
        public int ZoneId { get; set; }
        [DataMember]
        public string Title { get; set; }
        [DataMember]
        public string SubTitle { get; set; }
        [DataMember]
        public string Sapo { get; set; }
        [DataMember]
        public string Body { get; set; }
        [DataMember]
        public string Avatar { get; set; }
        [DataMember]
        public string AvatarDesc { get; set; }
        [DataMember]
        public string Source { get; set; }
        [DataMember]
        public string Url { get; set; }
    }
    [DataContract]
    public class ZoneForCrawlerEntity : EntityBase
    {
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public string Name { get; set; }
        [DataMember]
        public string ShortUrl { get; set; }
    }
}
