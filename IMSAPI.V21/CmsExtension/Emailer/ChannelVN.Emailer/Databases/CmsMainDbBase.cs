﻿using System.Data;
using ChannelVN.CMS.Common.DalCommon;
using ChannelVN.Emailer.MainDal;

namespace ChannelVN.Emailer.Databases
{
    /// <summary>
    /// The base class for the <see cref="CmsMainDb"/> class that 
    /// represents a connection to the <c>CmsMainDb</c> database. 
    /// </summary>
    /// <remarks>
    /// Do not change this source code. Modify the CmsMainDb class
    /// if you need to add or change some functionality.
    /// </remarks>
    public abstract class CmsMainDbBase : MainDbBase
    {
        #region Store procedures

        #region Email
        private EmailDal _emailContentMainDal;
        public EmailDalBase EmailContentMainDal
        {
            get { return _emailContentMainDal ?? (_emailContentMainDal = new EmailDal((CmsMainDb)this)); }
        }
        #endregion

        #region Email Group
        private EmailGroupDal _emailGroupMainDal;
        public EmailGroupDalBase EmailGroupMainDal
        {
            get { return _emailGroupMainDal ?? (_emailGroupMainDal = new EmailGroupDal((CmsMainDb)this)); }
        }
        #endregion

        #region Email Queue
        private EmailQueueDal _emailQueueMainDal;
        public EmailQueueDalBase EmailQueueMainDal
        {
            get { return _emailQueueMainDal ?? (_emailQueueMainDal = new EmailQueueDal((CmsMainDb)this)); }
        }
        #endregion

        #endregion

        #region Constructors

        protected CmsMainDbBase()
        {
        }
        protected CmsMainDbBase(bool init)
        {
            if (init)
            {
                InitConnection();
            }
        }

        #endregion

        #region Protected members

        protected override sealed void InitConnection()
        {
            _connection = CreateConnection();
            _connection.Open();
        }

        /// <summary>
        /// Creates a new connection to the database.
        /// </summary>
        /// <returns>A reference to the <see cref="System.Data.IDbConnection"/> object.</returns>
        protected abstract IDbConnection CreateConnection();

        #endregion
    }
}