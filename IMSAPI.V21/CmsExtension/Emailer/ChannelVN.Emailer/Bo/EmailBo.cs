﻿using System;
using System.Collections.Generic;
using ChannelVN.Emailer.Dal;
using ChannelVN.Emailer.Entity;
using ChannelVN.Emailer.Entity.ErrorCode;

namespace ChannelVN.Emailer.Bo
{
    public class EmailBo
    {

        public static EmailEntity GetById(string email)
        {
            return EmailDal.GetById(email);
        }

        public static List<EmailEntity> GetList(string keyword, int pageIndex, int pageSize, int status, int groupId, ref int totalRow)
        {
            return EmailDal.GetList(keyword, pageIndex, pageSize, status, groupId, ref totalRow);
        }


        public static ErrorMapping.ErrorCodes Insert(EmailEntity email)
        {
            try
            {
                if (null == email)
                {
                    return ErrorMapping.ErrorCodes.InvalidRequest;
                }
                return EmailDal.Insert(email)
                           ? ErrorMapping.ErrorCodes.Success
                           : ErrorMapping.ErrorCodes.BusinessError;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("EmailDal.Insert:{0}", ex.Message));
            }

        }



        public static ErrorMapping.ErrorCodes Update(EmailEntity email)
        {
            return EmailDal.Update(email) ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.UnknowError;
        }

    }
}
