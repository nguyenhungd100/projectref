﻿using System;
using System.Collections.Generic;
using ChannelVN.Emailer.Dal;
using ChannelVN.Emailer.Entity;
using ChannelVN.Emailer.Entity.ErrorCode;

namespace ChannelVN.Emailer.Bo
{
   public  class EmailQueueBo
    {

       public static ErrorMapping.ErrorCodes Insert(EmailQueueEntity emailQueueEntity)
       {
           try
           {
               if (null == emailQueueEntity)
               {
                   return ErrorMapping.ErrorCodes.InvalidRequest;
               }
               return EmailQueueDal.Insert(emailQueueEntity)
                          ? ErrorMapping.ErrorCodes.Success
                          : ErrorMapping.ErrorCodes.BusinessError;
           }
           catch (Exception ex)
           {
               throw new Exception(string.Format("EmailQueueDal.Insert:{0}", ex.Message));
           }

       }

       public static List<EmailQueueEntity >GetListTop(int top)
       {
           return EmailQueueDal.GetListTop(top);

       }

        
       public static ErrorMapping.ErrorCodes UpdateStatus(long NewsId)
       {
           try
           {
               return EmailQueueDal.Update(NewsId)
                          ? ErrorMapping.ErrorCodes.Success
                          : ErrorMapping.ErrorCodes.BusinessError;
           }
           catch (Exception ex)
           {
               throw new Exception(string.Format("EmailQueueDal.UpdateStatus:{0}", ex.Message));
           }

       }

       public static ErrorMapping.ErrorCodes UpdateStatusById(int id,int status)
       {
           try
           {
               return EmailQueueDal.UpdateStatus(id, status)
                          ? ErrorMapping.ErrorCodes.Success
                          : ErrorMapping.ErrorCodes.BusinessError;
           }
           catch (Exception ex)
           {
               throw new Exception(string.Format("EmailQueueDal.UpdateStatusById:{0}", ex.Message));
           }

       }
    }
}
