﻿using System;
using System.Collections.Generic;
using ChannelVN.Emailer.Dal;
using ChannelVN.Emailer.Entity;
using ChannelVN.Emailer.Entity.ErrorCode;

namespace ChannelVN.Emailer.Bo
{
   public  class EmailGroupBo
    {
       public static EmailGroupEntity GetById(int id)
        {
            return EmailGroupDal.GetById(id);
        }

       public static List<EmailGroupEntity> GetList(string keyword, int pageIndex, int pageSize, int status, ref int totalRow)
        {
            return EmailGroupDal.GetList(keyword, pageIndex, pageSize, status, ref totalRow);
        }


        public static ErrorMapping.ErrorCodes Insert(EmailGroupEntity emailGroup)
        {
            try
            {
                if (null == emailGroup)
                {
                    return ErrorMapping.ErrorCodes.InvalidRequest;
                }
                return EmailGroupDal.Insert(emailGroup)
                           ? ErrorMapping.ErrorCodes.Success
                           : ErrorMapping.ErrorCodes.BusinessError;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("EmailGroupDal.Insert:{0}", ex.Message));
            }

        }



        public static ErrorMapping.ErrorCodes Update(EmailGroupEntity emailGroup)
        {
            return EmailGroupDal.Update(emailGroup) ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.UnknowError;
        }
    }
}
