﻿using ChannelVN.Emailer.Databases;
using ChannelVN.Emailer.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ChannelVN.Emailer.MainDal
{
    public class EmailQueueDalBase
    {
        #region function GET
        public List<EmailQueueEntity> GetListTop(int top)
        {
            const string commandText = "CMS_EmailQueue_GetListTop";
            try
            {
                List<EmailQueueEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Top", top);
                data = _db.GetList<EmailQueueEntity>(cmd);
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        #endregion

        #region function SET
        public bool Insert(EmailQueueEntity emailQueueEntity)
        {
            const string commandText = "CMS_EmailQueue_Insert";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "ListEmail", emailQueueEntity.Email);
                _db.AddParameter(cmd, "NewsId", emailQueueEntity.NewsId);
                _db.AddParameter(cmd, "Title", emailQueueEntity.Title);
                _db.AddParameter(cmd, "Body", emailQueueEntity.Body);
                _db.AddParameter(cmd, "SendEmailDate", emailQueueEntity.SendEmailDate);
                var result = cmd.ExecuteNonQuery();
                return result > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool Update(long NewsId)
        {
            const string commandText = "CMS_EmailQueue_UpdateStatus";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "NewsId", NewsId);
                var result = cmd.ExecuteNonQuery();
                return result > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool UpdateStatus(int id, int status)
        {
            const string commandText = "CMS_EmailQueue_UpdateStatusById";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", id);
                _db.AddParameter(cmd, "Status", status);
                var result = cmd.ExecuteNonQuery();
                return result > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        #endregion

        /// <summary>
        /// Chuyển đổi DAL cho Email Queue
        /// minhduongvan
        /// 2014/01/09
        /// </summary>

        #region Constructor

        private readonly CmsMainDb _db;

        protected EmailQueueDalBase(CmsMainDb db)
        {
            _db = db;
        }

        protected CmsMainDb Database
        {
            get { return _db; }
        }

        #endregion
    }
}
