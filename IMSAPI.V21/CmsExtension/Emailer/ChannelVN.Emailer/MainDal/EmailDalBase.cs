﻿using ChannelVN.CMS.Common;
using ChannelVN.Emailer.Databases;
using ChannelVN.Emailer.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ChannelVN.Emailer.MainDal
{
    public abstract class EmailDalBase
    {
        #region funtion GET
        public EmailEntity GetById(string email)
        {
            const string commandText = "CMS_Email_GetById";
            try
            {
                EmailEntity data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "email", email);
                data = _db.Get<EmailEntity>(cmd);
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public List<EmailEntity> GetList(string keyword, int pageIndex, int pageSize, int status, int groupId, ref int totalRow)
        {
            const string commandText = "CMS_Email_GetList";
            try
            {
                List<EmailEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "TotalRow", totalRow, System.Data.ParameterDirection.Output);
                _db.AddParameter(cmd, "Keyword", keyword);
                _db.AddParameter(cmd, "Status", status);
                _db.AddParameter(cmd, "GroupId", groupId);
                _db.AddParameter(cmd, "PageIndex", pageIndex);
                _db.AddParameter(cmd, "PageSize", pageSize);
                data = _db.GetList<EmailEntity>(cmd);
                totalRow = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 0));
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        #endregion

        #region function SET
        public bool Insert(EmailEntity email)
        {
            const string commandText = "CMS_Email_Insert";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Email", email.Email);
                _db.AddParameter(cmd, "GroupId", email.GroupId);
                _db.AddParameter(cmd, "Name", email.Name);
                _db.AddParameter(cmd, "Status", email.Status);
                _db.AddParameter(cmd, "Address", email.Address);
                _db.AddParameter(cmd, "Description", email.Description);
                var result = cmd.ExecuteNonQuery();
                return result > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool Update(EmailEntity email)
        {
            const string commandText = "CMS_Email_Update";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Email", email.Email);
                _db.AddParameter(cmd, "GroupId", email.GroupId);
                _db.AddParameter(cmd, "Name", email.Name);
                _db.AddParameter(cmd, "Status", email.Status);
                _db.AddParameter(cmd, "Address", email.Address);
                _db.AddParameter(cmd, "Description", email.Description);
                var result = cmd.ExecuteNonQuery();
                return result > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        #endregion

        /// <summary>
        /// Chuyển đổi DAL cho Emailer
        /// minhduongvan
        /// 2014/01/09
        /// </summary>

        #region Constructor

        private readonly CmsMainDb _db;

        protected EmailDalBase(CmsMainDb db)
        {
            _db = db;
        }

        protected CmsMainDb Database
        {
            get { return _db; }
        }

        #endregion
    }
}
