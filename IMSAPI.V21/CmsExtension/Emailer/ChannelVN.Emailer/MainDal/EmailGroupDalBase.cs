﻿using ChannelVN.CMS.Common;
using ChannelVN.Emailer.Databases;
using ChannelVN.Emailer.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ChannelVN.Emailer.MainDal
{
    public abstract class EmailGroupDalBase
    {
        #region function GET
        public EmailGroupEntity GetById(int id)
        {
            const string commandText = "CMS_EmailGroup_GetById";
            try
            {
                EmailGroupEntity data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", id);
                data = _db.Get<EmailGroupEntity>(cmd);
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public List<EmailGroupEntity> GetList(string keyword, int pageIndex,
                                                int pageSize, int status, ref int totalRow)
        {

            const string commandText = "CMS_EmailGroup_GetList";
            try
            {
                List<EmailGroupEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "TotalRow", totalRow, System.Data.ParameterDirection.Output);
                _db.AddParameter(cmd,"Keyword", keyword);
                _db.AddParameter(cmd,"Status", status);
                _db.AddParameter(cmd,"PageIndex", pageIndex);
                _db.AddParameter(cmd,"PageSize", pageSize);
                data = _db.GetList<EmailGroupEntity>(cmd);
                totalRow = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 0));
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        #endregion

        #region function SET
        public bool Insert(EmailGroupEntity emailGroup) {
            const string commandText = "CMS_EmailGroup_Insert";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Name", emailGroup.Name);
                _db.AddParameter(cmd, "Description", emailGroup.Description);
                _db.AddParameter(cmd, "Status", emailGroup.Status);
                var result = cmd.ExecuteNonQuery();
                return result > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool Update(EmailGroupEntity emailGroup) {
            const string commandText = "CMS_EmailGroup_Update";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", emailGroup.Id);
                _db.AddParameter(cmd, "Name", emailGroup.Name);
                _db.AddParameter(cmd, "Description", emailGroup.Description);
                _db.AddParameter(cmd, "Status", emailGroup.Status);
                var result = cmd.ExecuteNonQuery();
                return result > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        #endregion

        /// <summary>
        /// Chuyển đổi DAL cho Email Group
        /// minhduongvan
        /// 2014/01/09
        /// </summary>

        #region Constructor

        private readonly CmsMainDb _db;

        protected EmailGroupDalBase(CmsMainDb db)
        {
            _db = db;
        }

        protected CmsMainDb Database
        {
            get { return _db; }
        }

        #endregion
    }
}
