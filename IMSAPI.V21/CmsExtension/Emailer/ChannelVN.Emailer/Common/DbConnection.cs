﻿using System;
using System.Web;
using System.Web.Caching;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.Common.ChannelConfig;
using ChannelVN.WcfExtensions;

namespace ChannelVN.Emailer.Common
{
    public class DbCommon
    {
        private const string _CONNECTION_DECRYPT_KEY = "nfvsMof35XnUdQEWuxgAZta";

        private static string GetConnectionName(Connection connection)
        {
            switch (connection)
            {
                case Connection.CmsMainDb:
                    return "CmsMainDb";
                default:
                    return "";
            }
        }

        public enum Connection
        {
            CmsMainDb = 1
        }

        public static DateTime MinDateTime = new DateTime(1980, 1, 1);
        public static string DatabaseSchema = "[dbo].";

        public static string GetConnectionString(Connection connection)
        {
            return ServiceChannelConfiguration.GetConnectionString(WcfMessageHeader.Current.Namespace, GetConnectionName(connection),
                                                            _CONNECTION_DECRYPT_KEY);
        }

        public static bool IsUseMainDal
        {
            get
            {
                return Utility.ConvertToBoolean(ServiceChannelConfiguration.GetAppSetting(WcfMessageHeader.Current.Namespace, "IsUseMainDal"));
            }
        }

        //#region properties

        //private static string _connectionString = string.Empty;
        //private static string _databaseSchema = "[dbo].";
        ///// <summary>
        ///// Gets or sets string value for ConnectionString property
        ///// </summary>
        //public static string ConnectionString
        //{
        //    get { return _connectionString; }
        //    set { _connectionString = value; }
        //}

        ///// <summary>
        ///// Gets or sets string value for Database Schema property
        ///// </summary>
        //public static string DatabaseSchema
        //{
        //    get { return _databaseSchema; }
        //    set { _databaseSchema = value; }
        //}
        //#endregion
    }

    //public class DbConnection
    //{
    //    public enum ConnectionName
    //    {
    //        CmsBookmark = 1,
    //        ExternalCms = 2,
    //        CmsTagBookmark = 3,
    //        VideoCms = 4,
    //        PhotoCms = 5,
    //        Mp3Cms = 6,
    //        CmsDynamicLayout = 7,
    //        CrawlerArticle = 8,
    //        Emailer =9
    //    }

    //    public static void SetConnection(ConnectionName connectionName)
    //    {
    //        var nameCachedConnection = "connectionName_" + connectionName;
    //        if (HttpContext.Current.Cache[nameCachedConnection] != null && !string.IsNullOrEmpty(HttpContext.Current.Cache[nameCachedConnection].ToString()))
    //        {
    //            DbCommon.GetConnectionString(DbCommon.Connection.CmsEmailerDb) = HttpContext.Current.Cache[nameCachedConnection].ToString();
    //        }
    //        else
    //        {
    //            switch (connectionName)
    //            {
    //                case ConnectionName.CmsBookmark:
    //                    try
    //                    {
    //                        var keyConnectionString = AppSettings.GetString(Constants.CONNECTION_STRING);
    //                        var connectionStringEncrypt = AppSettings.GetConnection(keyConnectionString);
    //                        DbCommon.GetConnectionString(DbCommon.Connection.CmsEmailerDb) = Crypton.DecryptByKey(connectionStringEncrypt, Constants.CONNECTION_DECRYPT_KEY);

    //                    }
    //                    catch (Exception ex)
    //                    {
    //                        throw new Exception(string.Format("Generate connection string fail:{0}", ex.Message));
    //                    }
    //                    break;
    //                case ConnectionName.ExternalCms:
    //                    try
    //                    {
    //                        var keyConnectionString = AppSettings.GetString(Constants.CONNECTION_STRING_EXTERNAL_CMS);
    //                        var connectionStringEncrypt = AppSettings.GetConnection(keyConnectionString);
    //                        DbCommon.GetConnectionString(DbCommon.Connection.CmsEmailerDb) = Crypton.DecryptByKey(connectionStringEncrypt,
    //                                                                             Constants.CONNECTION_DECRYPT_KEY);
    //                        //Logger.WriteLog(Logger.LogType.Debug, DbCommon.GetConnectionString(DbCommon.Connection.CmsEmailerDb));
    //                    }
    //                    catch (Exception ex)
    //                    {
    //                        throw new Exception(string.Format("Generate connection string fail:{0}", ex.Message));
    //                    }
    //                    break;
    //                case ConnectionName.CmsTagBookmark:
    //                    try
    //                    {
    //                        var keyConnectionString = AppSettings.GetString(Constants.CONNECTION_STRING_TAG);
    //                        var connectionStringEncrypt = AppSettings.GetConnection(keyConnectionString);
    //                        DbCommon.GetConnectionString(DbCommon.Connection.CmsEmailerDb) = Crypton.DecryptByKey(connectionStringEncrypt,
    //                                                                         Constants.CONNECTION_DECRYPT_KEY);
    //                    }
    //                    catch (Exception ex)
    //                    {
    //                        throw new Exception(string.Format("Generate connection string fail:{0}", ex.Message));
    //                    }
    //                    break;
    //                case ConnectionName.CrawlerArticle:
    //                    try
    //                    {
    //                        var keyConnectionString = AppSettings.GetString(Constants.CONNECTION_STRING_CRAWLER);
    //                        var connectionStringEncrypt = AppSettings.GetConnection(keyConnectionString);
    //                        DbCommon.GetConnectionString(DbCommon.Connection.CmsEmailerDb) = Crypton.DecryptByKey(connectionStringEncrypt,
    //                                                                         Constants.CONNECTION_DECRYPT_KEY);
    //                    }
    //                    catch (Exception ex)
    //                    {
    //                        throw new Exception(string.Format("Generate connection string fail:{0}", ex.Message));
    //                    }
    //                    break;
    //                case ConnectionName.VideoCms:
    //                    try
    //                    {
    //                        var keyConnectionString = AppSettings.GetString(Constants.VIDEO_CONNECTION_STRING);
    //                        var connectionStringEncrypt = AppSettings.GetConnection(keyConnectionString);
    //                        DbCommon.GetConnectionString(DbCommon.Connection.CmsEmailerDb) = Crypton.DecryptByKey(connectionStringEncrypt,
    //                                                                         Constants.CONNECTION_DECRYPT_KEY);
    //                    }
    //                    catch (Exception ex)
    //                    {
    //                        throw new Exception(string.Format("Generate connection string fail:{0}", ex.Message));
    //                    }
    //                    break;
    //                case ConnectionName.PhotoCms:
    //                    try
    //                    {
    //                        var keyConnectionString = AppSettings.GetString(Constants.PHOTO_CONNECTION_STRING);
    //                        var connectionStringEncrypt = AppSettings.GetConnection(keyConnectionString);
    //                        DbCommon.GetConnectionString(DbCommon.Connection.CmsEmailerDb) = Crypton.DecryptByKey(connectionStringEncrypt,
    //                                                                         Constants.CONNECTION_DECRYPT_KEY);
    //                    }
    //                    catch (Exception ex)
    //                    {
    //                        throw new Exception(string.Format("Generate connection string fail:{0}", ex.Message));
    //                    }
    //                    break;
    //                case ConnectionName.Mp3Cms:
    //                    try
    //                    {
    //                        var keyConnectionString = AppSettings.GetString(Constants.MP3_CONNECTION_STRING);
    //                        var connectionStringEncrypt = AppSettings.GetConnection(keyConnectionString);
    //                        DbCommon.GetConnectionString(DbCommon.Connection.CmsEmailerDb) = Crypton.DecryptByKey(connectionStringEncrypt,
    //                                                                         Constants.CONNECTION_DECRYPT_KEY);
    //                    }
    //                    catch (Exception ex)
    //                    {
    //                        throw new Exception(string.Format("Generate connection string fail:{0}", ex.Message));
    //                    }
    //                    break;
    //                case ConnectionName.CmsDynamicLayout:
    //                    try
    //                    {
    //                        var keyConnectionString = AppSettings.GetString(Constants.DYNAMIC_CONNECTION_STRING);
    //                        var connectionStringEncrypt = AppSettings.GetConnection(keyConnectionString);
    //                        DbCommon.GetConnectionString(DbCommon.Connection.CmsEmailerDb) = Crypton.DecryptByKey(connectionStringEncrypt,
    //                                                                         Constants.CONNECTION_DECRYPT_KEY);
    //                    }
    //                    catch (Exception ex)
    //                    {
    //                        throw new Exception(string.Format("Generate connection string fail:{0}", ex.Message));
    //                    }
    //                    break;
    //                case ConnectionName.Emailer:
    //                    try
    //                    {
    //                        var keyConnectionString = AppSettings.GetString(Constants.CONNECTION_STRING_EMAILER);
    //                        var connectionStringEncrypt = AppSettings.GetConnection(keyConnectionString);
    //                        DbCommon.GetConnectionString(DbCommon.Connection.CmsEmailerDb) = Crypton.DecryptByKey(connectionStringEncrypt,
    //                                                                         Constants.CONNECTION_DECRYPT_KEY);
    //                    }
    //                    catch (Exception ex)
    //                    {
    //                        throw new Exception(string.Format("Generate connection string fail:{0}", ex.Message));
    //                    }
    //                    break;
    //            }
    //            HttpContext.Current.Cache.Add(nameCachedConnection, DbCommon.GetConnectionString(DbCommon.Connection.CmsEmailerDb), null,
    //                                          DateTime.Now.AddHours(10), TimeSpan.Zero, CacheItemPriority.High, null);
    //        }
    //    }

    //    public static void SetDbSchema(string schema)
    //    {
    //        DbCommon.DatabaseSchema = schema;
    //    }
    //}
}
