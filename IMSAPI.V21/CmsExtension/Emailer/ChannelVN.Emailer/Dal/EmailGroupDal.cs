﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using ChannelVN.CMS.Common;
using ChannelVN.Emailer.Common;
using ChannelVN.Emailer.Entity;
using ChannelVN.Emailer.Databases;

namespace ChannelVN.Emailer.Dal
{
    public class EmailGroupDal
    {
        public static EmailGroupEntity GetById(int id)
        {
            EmailGroupEntity retVal;
            using (var db = new CmsMainDb())
            {
                retVal = db.EmailGroupMainDal.GetById(id);
            }
            return retVal;
        }
        public static List<EmailGroupEntity> GetList(string keyword, int pageIndex, int pageSize, int status, ref int totalRow)
        {
            List<EmailGroupEntity> retVal;
            using (var db = new CmsMainDb())
            {
                retVal = db.EmailGroupMainDal.GetList(keyword, pageIndex, pageSize, status, ref totalRow);
            }
            return retVal;
        }
        public static bool Insert(EmailGroupEntity emailGroup)
        {
            bool retVal = false;
            using (var db = new CmsMainDb())
            {
                retVal = db.EmailGroupMainDal.Insert(emailGroup);
            }
            return retVal;
        }

        public static bool Update(EmailGroupEntity emailGroup)
        {
            bool retVal = false;
            using (var db = new CmsMainDb())
            {
                retVal = db.EmailGroupMainDal.Update(emailGroup);
            }
            return retVal;
        } 
    }
}
