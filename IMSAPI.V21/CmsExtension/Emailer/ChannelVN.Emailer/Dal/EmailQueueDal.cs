﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using ChannelVN.CMS.Common;
using ChannelVN.Emailer.Common;
using ChannelVN.Emailer.Entity;
using ChannelVN.Emailer.Databases;


namespace ChannelVN.Emailer.Dal
{
    public class EmailQueueDal
    {
        public static bool Insert(EmailQueueEntity emailQueueEntity)
        {
            bool retVal = false;
            using (var db = new CmsMainDb())
            {
                retVal = db.EmailQueueMainDal.Insert(emailQueueEntity);
            }
            return retVal;
        }

        public static List<EmailQueueEntity> GetListTop(int top)
        {
            List<EmailQueueEntity> retVal;
            using (var db = new CmsMainDb())
            {
                retVal = db.EmailQueueMainDal.GetListTop(top);
            }
            return retVal;
        }

        public static bool Update(long NewsId)
        {
            bool retVal = false;
            using (var db = new CmsMainDb())
            {
                retVal = db.EmailQueueMainDal.Update(NewsId);
            }
            return retVal;
        }

        // CuongNP
        public static bool UpdateStatus(int id, int status)
        {
            bool retVal = false;
            using (var db = new CmsMainDb())
            {
                retVal = db.EmailQueueMainDal.UpdateStatus(id, status);
            }
            return retVal;
        }
    }
}
