﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using ChannelVN.CMS.Common;
using ChannelVN.Emailer.Common;
using ChannelVN.Emailer.Entity;
using ChannelVN.Emailer.Databases;

namespace ChannelVN.Emailer.Dal
{
    public  class EmailDal
    {
        public static EmailEntity GetById(string email)
        {
            EmailEntity retVal;
            using (var db = new CmsMainDb())
            {
                retVal = db.EmailContentMainDal.GetById(email);
            }
            return retVal;
        }
        public static List<EmailEntity> GetList(string keyword, int pageIndex, int pageSize, int status,int groupId, ref int totalRow)
        {
            List<EmailEntity> retVal;
            using (var db = new CmsMainDb())
            {
                retVal = db.EmailContentMainDal.GetList(keyword, pageIndex, pageSize, status, groupId,
                        ref totalRow);
            }
            return retVal;
        }
        public static bool Insert(EmailEntity email)
        {
            bool returnValue = false;
            using (var db = new CmsMainDb())
            {
                returnValue = db.EmailContentMainDal.Insert(email);
            }
            return returnValue;
        }

        public static bool Update(EmailEntity email)
        {
            bool returnValue = false;
            using (var db = new CmsMainDb())
            {
                returnValue = db.EmailContentMainDal.Update(email);
            }
            return returnValue;
        }
    }
}
