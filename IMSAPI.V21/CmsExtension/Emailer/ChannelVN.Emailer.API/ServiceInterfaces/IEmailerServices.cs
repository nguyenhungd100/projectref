﻿using System.ServiceModel;
using System.ServiceModel.Web;
using ChannelVN.CMS.Common;
using ChannelVN.Emailer.Entity;

namespace ChannelVN.Emailer.API.ServiceInterfaces 
{
    [ServiceContract(Name = "IEmailerServices", Namespace = "ChannelVN.Emailer.API.ServiceInterfaces")]
    public interface IEmailerServices
    {
        [OperationContract(Name = "EmailerInsert")]
        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        WcfResponseData EmailerInsert(string secretKey, EmailEntity emailEntity);

        [OperationContract(Name = "EmailerUpdate")]
        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        WcfResponseData EmailerUpdate(string secretKey, EmailEntity emailEntity);
        
        [OperationContract(Name = "EmailerGetList")]
        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        WcfResponseData EmailerGetList(string secretKey, string keyword, int status, int groupId,int pageIndex, int pageSize);

        [OperationContract(Name = "EmailerGetById")] 
        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        WcfResponseData EmailerGetById(string secretKey, string email);
        
        [OperationContract(Name = "EmailerGroupInsert")]
        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        WcfResponseData EmailerGroupInsert(string secretKey, EmailGroupEntity emailGroupEntity);

        [OperationContract(Name = "EmailerGroupUpdate")]
        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        WcfResponseData EmailerGroupUpdate(string secretKey, EmailGroupEntity emailGroupEntity);
        
        [OperationContract(Name = "EmailerGroupGetList")] 
        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        WcfResponseData EmailerGroupGetList(string secretKey, string keyword, int status, int pageIndex, int pageSize);

        [OperationContract(Name = "EmailerGroupGetById")]
        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        WcfResponseData EmailerGroupGetById(string secretKey, int id);
        
        [OperationContract(Name = "EmailerQueueInsert")]
        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        WcfResponseData EmailerQueueInsert(string secretKey, EmailQueueEntity emailQueueEntity);
               
        [OperationContract(Name = "EmailerQueueUpdateStatus")]
        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        WcfResponseData EmailerQueueUpdateStatus(string secretKey, long newsId);
        
        [OperationContract(Name = "EmailerQueueGetListTop")]
        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        WcfResponseData EmailerQueueGetListTop(string secretKey, int top);
    }
}
