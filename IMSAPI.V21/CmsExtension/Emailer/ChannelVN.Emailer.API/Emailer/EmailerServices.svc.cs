﻿using System.ServiceModel;
using System.ServiceModel.Activation;
using ChannelVN.CMS.Common;
using ChannelVN.Emailer.API.Common;
using ChannelVN.Emailer.API.ServiceInterfaces;
using ChannelVN.Emailer.Bo;
using ChannelVN.Emailer.Entity;
using ChannelVN.Emailer.Entity.ErrorCode;

namespace ChannelVN.Emailer.API.Emailer
{
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    [ServiceBehavior(InstanceContextMode = InstanceContextMode.PerCall)]
    public class EmailerServices : IEmailerServices
    {
        #region Emailer

        public WcfResponseData EmailerInsert(string secretKey, EmailEntity email)
        {
            if (!SecretKey.IsValid(secretKey)) return WcfResponseData.CreateErrorResponseForInvalidRequest();

            var wcfResponseData = WcfResponseData.CreateSuccessResponse();
            var voteId = 0;

            if (EmailBo.Insert(email) == ErrorMapping.ErrorCodes.Success)
            {
                wcfResponseData.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.Success];
                wcfResponseData.Data = voteId.ToString();
                wcfResponseData.ErrorCode = (int)ErrorMapping.ErrorCodes.Success;
            }
            else
            {
                wcfResponseData.ErrorCode = (int)ErrorMapping.ErrorCodes.UnknowError;
                wcfResponseData.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError];
            }

            return wcfResponseData;
        }
        
        public WcfResponseData EmailerUpdate(string secretKey, EmailEntity email)
        {
            if (!SecretKey.IsValid(secretKey)) return WcfResponseData.CreateErrorResponseForInvalidRequest();
            var wcfResponseData = WcfResponseData.CreateSuccessResponse();

            var voteId = 0;
            if (EmailBo.Update(email) == ErrorMapping.ErrorCodes.Success)
            {
                wcfResponseData.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.Success];
                wcfResponseData.Data = voteId.ToString();
                wcfResponseData.ErrorCode = (int)ErrorMapping.ErrorCodes.Success;
            }
            else
            {
                wcfResponseData.ErrorCode = (int)ErrorMapping.ErrorCodes.UnknowError;
                wcfResponseData.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError];
            }

            return wcfResponseData;
        }
        
        public WcfResponseData EmailerGetById(string secretKey, string email)
        {
            if (!SecretKey.IsValid(secretKey)) return WcfResponseData.CreateErrorResponseForInvalidRequest();
            WcfResponseData responseData = WcfResponseData.CreateSuccessResponse();

            var data = EmailBo.GetById(email);
            responseData.Data = NewtonJson.Serialize(data);
            responseData.TotalRow = (null != data ? 1 : 0);

            return responseData;
        }

        public WcfResponseData EmailerGetList(string secretKey, string keyword, int status, int groupId, int pageIndex, int pageSize)
        {
            if (!SecretKey.IsValid(secretKey)) return WcfResponseData.CreateErrorResponseForInvalidRequest();
            WcfResponseData responseData = WcfResponseData.CreateSuccessResponse();
            int totalRow = 0;
            var data = EmailBo.GetList(keyword, pageIndex, pageSize, status, groupId, ref totalRow);
            responseData.Data = NewtonJson.Serialize(data);
            responseData.TotalRow = totalRow;
            return responseData;
        }
        #endregion

        #region EmailerGroup

        public WcfResponseData EmailerGroupInsert(string secretKey, EmailGroupEntity emailGroupEntity)
        {

            if (!SecretKey.IsValid(secretKey)) return WcfResponseData.CreateErrorResponseForInvalidRequest();
            var wcfResponseData = WcfResponseData.CreateSuccessResponse();
            var voteId = 0;
            if (EmailGroupBo.Insert(emailGroupEntity) == ErrorMapping.ErrorCodes.Success)
            {
                wcfResponseData.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.Success];
                wcfResponseData.Data = voteId.ToString();
                wcfResponseData.ErrorCode = (int)ErrorMapping.ErrorCodes.Success;
            }
            else
            {
                wcfResponseData.ErrorCode = (int)ErrorMapping.ErrorCodes.UnknowError;
                wcfResponseData.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError];
            }

            return wcfResponseData;
        }
        
        public WcfResponseData EmailerGroupUpdate(string secretKey, EmailGroupEntity emailGroupEntity)
        {
            if (!SecretKey.IsValid(secretKey)) return WcfResponseData.CreateErrorResponseForInvalidRequest();
            var wcfResponseData = WcfResponseData.CreateSuccessResponse();

            var voteId = 0;
            if (EmailGroupBo.Update(emailGroupEntity) == ErrorMapping.ErrorCodes.Success)
            {
                wcfResponseData.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.Success];
                wcfResponseData.Data = voteId.ToString();
                wcfResponseData.ErrorCode = (int)ErrorMapping.ErrorCodes.Success;
            }
            else
            {
                wcfResponseData.ErrorCode = (int)ErrorMapping.ErrorCodes.UnknowError;
                wcfResponseData.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError];
            }

            return wcfResponseData;
        }
        
        public WcfResponseData EmailerGroupGetById(string secretKey, int id)
        {
            if (!SecretKey.IsValid(secretKey)) return WcfResponseData.CreateErrorResponseForInvalidRequest();
            WcfResponseData responseData = WcfResponseData.CreateSuccessResponse();

            var data = EmailGroupBo.GetById(id);
            responseData.Data = NewtonJson.Serialize(data);
            responseData.TotalRow = (null != data ? 1 : 0);

            return responseData;
        }

        public WcfResponseData EmailerGroupGetList(string secretKey, string keyword, int status, int pageIndex, int pageSize)
        {
            if (!SecretKey.IsValid(secretKey)) return WcfResponseData.CreateErrorResponseForInvalidRequest();
            WcfResponseData responseData = WcfResponseData.CreateSuccessResponse();
            int totalRow = 0;
            var data = EmailGroupBo.GetList(keyword, pageIndex, pageSize, status, ref totalRow);
            responseData.Data = NewtonJson.Serialize(data);
            responseData.TotalRow = totalRow;
            return responseData;
        }
        #endregion

        #region EmailQueue

        public WcfResponseData EmailerQueueInsert(string secretKey, EmailQueueEntity emailQueueEntity)
        {

            if (!SecretKey.IsValid(secretKey)) return WcfResponseData.CreateErrorResponseForInvalidRequest();
            var wcfResponseData = WcfResponseData.CreateSuccessResponse();
            var voteId = 0;
            if (EmailQueueBo.Insert(emailQueueEntity) == ErrorMapping.ErrorCodes.Success)
            {
                wcfResponseData.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.Success];
                wcfResponseData.Data = voteId.ToString();
                wcfResponseData.ErrorCode = (int)ErrorMapping.ErrorCodes.Success;
            }
            else
            {
                wcfResponseData.ErrorCode = (int)ErrorMapping.ErrorCodes.UnknowError;
                wcfResponseData.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError];
            }

            return wcfResponseData;
        }

        public WcfResponseData EmailerQueueUpdateStatus(string secretKey, long newsId)
        {
            if (!SecretKey.IsValid(secretKey)) return WcfResponseData.CreateErrorResponseForInvalidRequest();
            var wcfResponseData = WcfResponseData.CreateSuccessResponse();
            var voteId = 0;
            if (EmailQueueBo.UpdateStatus(newsId) == ErrorMapping.ErrorCodes.Success)
            {
                wcfResponseData.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.Success];
                wcfResponseData.Data = voteId.ToString();
                wcfResponseData.ErrorCode = (int)ErrorMapping.ErrorCodes.Success;
            }
            else
            {
                wcfResponseData.ErrorCode = (int)ErrorMapping.ErrorCodes.UnknowError;
                wcfResponseData.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError];
            }

            return wcfResponseData;
        }
        public WcfResponseData EmailerQueueUpdateStatusById(string secretKey, int id,int status)
        {

            if (!SecretKey.IsValid(secretKey)) return WcfResponseData.CreateErrorResponseForInvalidRequest();
            var wcfResponseData = WcfResponseData.CreateSuccessResponse();
            if (EmailQueueBo.UpdateStatusById(id, status) == ErrorMapping.ErrorCodes.Success)
            {
                wcfResponseData.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.Success];
                wcfResponseData.ErrorCode = (int)ErrorMapping.ErrorCodes.Success;
            }
            else
            {
                wcfResponseData.ErrorCode = (int)ErrorMapping.ErrorCodes.UnknowError;
                wcfResponseData.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError];
            }

            return wcfResponseData;
        }

        public WcfResponseData EmailerQueueGetListTop(string secretKey, int top)
        {
            if (!SecretKey.IsValid(secretKey)) return WcfResponseData.CreateErrorResponseForInvalidRequest();
            WcfResponseData responseData = WcfResponseData.CreateSuccessResponse();
            int totalRow = 0;
            var data = EmailQueueBo.GetListTop(top);
            responseData.Data = NewtonJson.Serialize(data);
            responseData.TotalRow = totalRow;
            return responseData;
        }
        #endregion

    }
}
