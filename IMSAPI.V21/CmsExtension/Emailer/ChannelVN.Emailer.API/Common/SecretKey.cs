﻿using ChannelVN.CMS.Common;

namespace ChannelVN.Emailer.API.Common
{
    public class SecretKey
    {
        public static bool IsValid(string key)
        {
            var secretKey = Utility.CreateMD5Checksum(AppSettings.GetString("CmsWcfSecretKey"));
            return secretKey.Equals(key);
        }
    }
}