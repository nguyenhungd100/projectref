﻿using System.Runtime.Serialization;
using ChannelVN.CMS.Common;

namespace ChannelVN.Emailer.Entity
{
    [DataContract]
    public class EmailGroupEntity : EntityBase
    {
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public string Name { get; set; }
        [DataMember]
        public string Description { get; set; }
        [DataMember]
        public int Status { get; set; }
    }
}
