﻿using System;
using System.Runtime.Serialization;
using ChannelVN.CMS.Common;

namespace ChannelVN.Emailer.Entity
{
    [DataContract]
    public class EmailQueueEntity : EntityBase
    {

        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public long NewsId { get; set; }
        [DataMember]
        public string Email { get; set; }
        [DataMember]
        public string Title { get; set; }
        [DataMember]
        public string Body { get; set; }
        [DataMember]
        public DateTime SendEmailDate { get; set; }
        [DataMember]
        public int Status { get; set; }

    }
}

        