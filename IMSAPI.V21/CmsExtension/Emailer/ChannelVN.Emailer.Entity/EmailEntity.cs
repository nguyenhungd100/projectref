﻿using System;
using System.Runtime.Serialization;
using ChannelVN.CMS.Common;

namespace ChannelVN.Emailer.Entity
{
    [DataContract]
    public class EmailEntity : EntityBase
    {
        [DataMember]
        public string Email { get; set; }
        [DataMember]
        public int GroupId { get; set; }
        [DataMember]
        public string Name { get; set; }
        [DataMember]
        public int Status { get; set; }
        [DataMember]
        public string Address { get; set; }
        [DataMember]
        public DateTime Brithday { get; set; }
        [DataMember]
        public string Description { get; set; }
    }
}
