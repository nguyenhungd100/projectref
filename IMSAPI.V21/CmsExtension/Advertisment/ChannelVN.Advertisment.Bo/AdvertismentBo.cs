﻿using ChannelVN.Advertisment.Dal;
using ChannelVN.Advertisment.Entity;
using ChannelVN.Advertisment.Entity.ErrorCode;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ChannelVN.Advertisment.Bo
{
    public class AdvertismentBo
    {
        public static List<AdvertismentEntity> GetAllAdvertisment()
        {
            return AdvertismentDal.GetAll();
        }

        public static List<AdvertismentEntity> Search(string keyword, EnumAdvertismentType typeId, int zoneId, int displayStyle, EnumAdvertismentStatus status, int positionId, int pageIndex, int pageSize, ref int totalRow)
        {
            return AdvertismentDal.GetByPosition((int)typeId, zoneId, displayStyle, (int)status, positionId);
        }

        public static List<AdvertismentEntity> GetAdvertismentByPosition(EnumAdvertismentType typeId, int zoneId, int displayStyle, EnumAdvertismentStatus status, int positionId)
        {
            return AdvertismentDal.GetByPosition((int)typeId, zoneId, displayStyle, (int)status, positionId);
        }

        public static AdvertismentDetailEntity GetAdvertismentById(int id)
        {
            return new AdvertismentDetailEntity
                       {
                           Info = AdvertismentDal.GetById(id),
                           AdvertismentZones = AdvertismentDal.GetAdvertismentZoneByAdvertismentId(id)
                       };
        }

        public static ErrorMapping.ErrorCodes UpdateAdvertismentStatus(int id, EnumAdvertismentStatus status)
        {
            return AdvertismentDal.UpdateStatus(id, (int)status)
                       ? ErrorMapping.ErrorCodes.Success
                       : ErrorMapping.ErrorCodes.BusinessError;
        }

        public static ErrorMapping.ErrorCodes DeleteById(int id)
        {
            return AdvertismentDal.DeleteById(id) ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.UnknowError;
        }

        public static ErrorMapping.ErrorCodes InsertAdvertisment(AdvertismentEntity info, string listZones)
        {
            var result = ErrorMapping.ErrorCodes.BusinessError;
            var createSuccess = AdvertismentDal.Insert(info, listZones);
            if (createSuccess)
                result = ErrorMapping.ErrorCodes.Success;
            return result;
        }
        public static ErrorMapping.ErrorCodes UpdateAdvertisment(AdvertismentEntity info, string listZones)
        {
            var result = ErrorMapping.ErrorCodes.BusinessError;
            var createSuccess = AdvertismentDal.Update(info, listZones);
            if (createSuccess)
                result = ErrorMapping.ErrorCodes.Success;
            return result;
        }
    }
}
