﻿using ChannelVN.Advertisment.Dal.Common;
using ChannelVN.Advertisment.Entity;
using ChannelVN.Advertisment.MainDal.Databases;
using ChannelVN.CMS.Common;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace ChannelVN.Advertisment.Dal
{
    public class AdvertismentDal
    {
        #region Get

        public static List<AdvertismentEntity> GetAll()
        {
            List<AdvertismentEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.AdvContentMainDal.GetAll();
            }
            return returnValue;
        }

        public static List<AdvertismentZoneEntity> GetAdvertismentZoneByAdvertismentId(int id)
        {
            List<AdvertismentZoneEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.AdvContentMainDal.GetAdvertismentZoneByAdvertismentId(id);
            }
            return returnValue;
        }

        public static List<AdvertismentEntity> GetByPosition(int typeId, int zoneId, int displayStyle, int status, int positionId)
        {
            List<AdvertismentEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.AdvContentMainDal.GetByPosition(typeId, zoneId, displayStyle, status, positionId);
            }
            return returnValue;
        }
        public static AdvertismentEntity GetById(int Id)
        {
            AdvertismentEntity returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.AdvContentMainDal.GetById(Id);
            }
            return returnValue;
        }
        #endregion

        #region Set

        public static bool Insert(AdvertismentEntity info, string listZones)
        {
            bool returnValue = false;
            using (var db = new CmsMainDb())
            {
                returnValue = db.AdvContentMainDal.Insert(info, listZones);
            }
            return returnValue;
        }

        public static bool Update(AdvertismentEntity info, string listZones)
        {
            bool returnValue = false;
            using (var db = new CmsMainDb())
            {
                returnValue = db.AdvContentMainDal.Update(info, listZones);
            }
            return returnValue;
        }

        public static bool UpdateStatus(int id, int status)
        {

            bool returnValue = false;
            using (var db = new CmsMainDb())
            {
                returnValue = db.AdvContentMainDal.UpdateStatus(id, status);
            }
            return returnValue;
        }

        public static bool DeleteById(int id)
        {
            bool returnValue = false;
            using (var db = new CmsMainDb())
            {
                returnValue = db.AdvContentMainDal.DeleteById(id);
            }
            return returnValue;
        }
        #endregion
    }
}
