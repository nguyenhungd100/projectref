﻿using ChannelVN.Advertisment.Entity;
using ChannelVN.Advertisment.MainDal.Databases;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChannelVN.Advertisment.MainDal
{
    public abstract class AdvertismentDalBase
    {
        /// <summary>
        /// Chuyển đổi DAL cho Advertisment
        /// minhduongvan
        /// 2014/01/06
        /// </summary>

        #region Constructor

        private readonly CmsMainDb _db;

        protected AdvertismentDalBase(CmsMainDb db)
        {
            _db = db;
        }

        protected CmsMainDb Database
        {
            get { return _db; }
        }

        #endregion

        /// <summary>
        /// Các hàm xử lý cho Advertisment
        /// </summary>
        /// <returns></returns>

        #region GET
        public List<AdvertismentEntity> GetAll()
        {
            const string commandText = "CMS_Advertisment_GetAll";
            try
            {
                List<AdvertismentEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                //_db.AddParameter(cmd, "UserId", userId);
                //_db.AddParameter(cmd, "IsGetChildZone", isGetChildZone);
                data = _db.GetList<AdvertismentEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public List<AdvertismentZoneEntity> GetAdvertismentZoneByAdvertismentId(int id)
        {
            const string commandText = "CMS_Advertisment_GetZonesV2";
            try
            {
                List<AdvertismentZoneEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", id);
                data = _db.GetList<AdvertismentZoneEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public List<AdvertismentEntity> Search(string keyword, int typeId, int zoneId, int displayStyle, int status, int positionId, int pageindex, int pagesize)
        {
            const string commandText = "CMS_Advertisment_GetByPosition";
            try
            {
                List<AdvertismentEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "TypeId", typeId);
                _db.AddParameter(cmd, "ZoneId", zoneId);
                _db.AddParameter(cmd, "DisplayStyle", displayStyle);
                _db.AddParameter(cmd, "Status", status);
                _db.AddParameter(cmd, "PositionId", positionId);
                data = _db.GetList<AdvertismentEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public List<AdvertismentEntity> GetByPosition(int typeId, int zoneId, int displayStyle, int status, int positionId)
        {
            const string commandText = "CMS_Advertisment_GetByPosition";
            try
            {
                List<AdvertismentEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "TypeId", typeId);
                _db.AddParameter(cmd, "ZoneId", zoneId);
                _db.AddParameter(cmd, "DisplayStyle", displayStyle);
                _db.AddParameter(cmd, "Status", status);
                _db.AddParameter(cmd, "PositionId", positionId);
                data = _db.GetList<AdvertismentEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public AdvertismentEntity GetById(int Id)
        {
            const string commandText = "CMS_Advertisment_GetById";
            try
            {
                AdvertismentEntity data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", Id);
                data = _db.Get<AdvertismentEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        #endregion

        #region SET
        public bool Insert(AdvertismentEntity info, string listZones)
        {
            const string commandText = "CMS_Advertisment_Insert";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Title", info.Title);
                _db.AddParameter(cmd, "TargetUrl", info.TargetUrl);
                _db.AddParameter(cmd, "AdCode", info.AdCode);
                _db.AddParameter(cmd, "Note", info.Note);
                _db.AddParameter(cmd, "Priority", info.Priority);
                _db.AddParameter(cmd, "TypeId", info.TypeId);
                _db.AddParameter(cmd, "ZoneId", info.ZoneId);
                _db.AddParameter(cmd, "DisplayStyle", info.DisplayStyle);
                _db.AddParameter(cmd, "Status", info.Status);
                _db.AddParameter(cmd, "PositionId", info.PositionId);
                _db.AddParameter(cmd, "StartDate", info.StartDate);
                _db.AddParameter(cmd, "EndDate", info.EndDate);
                _db.AddParameter(cmd, "Width", info.Width);
                _db.AddParameter(cmd, "Height", info.Height);
                _db.AddParameter(cmd, "ZoneIdList", listZones);
                var numberOfRow = cmd.ExecuteNonQuery();

                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public bool Update(AdvertismentEntity info, string listZones) {
            const string commandText = "CMS_Advertisment_Update";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", info.Id);
                _db.AddParameter(cmd, "Title", info.Title);
                _db.AddParameter(cmd, "TargetUrl", info.TargetUrl);
                _db.AddParameter(cmd, "AdCode", info.AdCode);
                _db.AddParameter(cmd, "Note", info.Note);
                _db.AddParameter(cmd, "Priority", info.Priority);
                _db.AddParameter(cmd, "TypeId", info.TypeId);
                _db.AddParameter(cmd, "ZoneId", info.ZoneId);
                _db.AddParameter(cmd, "DisplayStyle", info.DisplayStyle);
                _db.AddParameter(cmd, "Status", info.Status);
                _db.AddParameter(cmd, "PositionId", info.PositionId);
                _db.AddParameter(cmd, "StartDate", info.StartDate);
                _db.AddParameter(cmd, "EndDate", info.EndDate);
                _db.AddParameter(cmd, "Width", info.Width);
                _db.AddParameter(cmd, "Height", info.Height);
                _db.AddParameter(cmd, "ZoneIdList", listZones);
                var numberOfRow = cmd.ExecuteNonQuery();

                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool UpdateStatus(int id, int status) {
            const string commandText = "CMS_Advertisment_UpdateStatus";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", id);
                _db.AddParameter(cmd, "Status", status);
                var numberOfRow = cmd.ExecuteNonQuery();

                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public bool DeleteById(int id) {
            const string commandText = "CMS_Advertisment_DeleteByID";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", id);
                var numberOfRow = cmd.ExecuteNonQuery();

                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        #endregion
    }
}
