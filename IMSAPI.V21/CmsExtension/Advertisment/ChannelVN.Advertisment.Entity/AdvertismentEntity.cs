﻿using ChannelVN.CMS.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace ChannelVN.Advertisment.Entity
{
    [DataContract]
    public class AdvertismentEntity : EntityBase
    {
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public string Title { get; set; }
        [DataMember]
        public string TargetUrl { get; set; }
        [DataMember]
        public string AdCode { get; set; }
        [DataMember]
        public string Note { get; set; }
        [DataMember]
        public int Priority { get; set; }
        [DataMember]
        public int TypeId { get; set; }
        [DataMember]
        public int ZoneId { get; set; }
        [DataMember]
        public int DisplayStyle { get; set; }
        [DataMember]
        public int Status { get; set; }
        [DataMember]
        public int PositionId { get; set; }
        [DataMember]
        public DateTime StartDate { get; set; }
        [DataMember]
        public DateTime EndDate { get; set; }
        [DataMember]
        public int Width { get; set; }
        [DataMember]
        public int Height { get; set; }
    }

    [DataContract]
    public class AdvertismentDetailEntity : EntityBase
    {
        [DataMember]
        public AdvertismentEntity Info { get; set; }        
        [DataMember]
        public List<AdvertismentZoneEntity> AdvertismentZones { get; set; }
    }

    [DataContract]
    public class AdvertismentZoneEntity : EntityBase
    {
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public string Name { get; set; }
    }

    [DataContract]
    public enum EnumAdvertismentType : int
    {
        [EnumMember]
        AllType = -1,
        [EnumMember]
        Photo = 1,
        [EnumMember]
        Flash = 2,
        [EnumMember]
        EmbedCode = 3
    }

    [DataContract]
    public enum EnumAdvertismentStatus : int
    {
        [EnumMember]
        AllStatus = -1,
        [EnumMember]
        Active = 1,
        [EnumMember]
        Deactive = 2
    }
}
