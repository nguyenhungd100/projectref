﻿using ChannelVN.CMS.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace ChannelVN.BigStory.Entity
{
    public class BigStoryEntity : EntityBase
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime LastModifiedDate { get; set; }
        public string CreatedBy { get; set; }
        public string LastModifiedBy { get; set; }
        public string Avatar { get; set; }
        public string Description { get; set; }
        public int Status { get; set; }
        public bool IsFocus { get; set; }
    }
    public enum BigStoryStatus
    {
        /// <summary>
        /// Tất cả các trạng thái
        /// </summary>
        [EnumMember]
        All = -1,
        /// <summary>
        /// Lưu tạm
        /// </summary>
        [EnumMember]
        Temporary = 0,
        /// <summary>
        /// Xuất bản
        /// </summary>
        [EnumMember]
        Published = 1,
         /// <summary>
        /// Gỡ xuất bản
        /// </summary>
        [EnumMember]
        Unpublished = 2
    }
    public enum BigStoryIsFocus
    {
        /// <summary>
        /// Tất cả các trạng thái
        /// </summary>
        [EnumMember]
        All = -1,
        /// <summary>
        /// True
        /// </summary>
        [EnumMember]
        IsFocus = 1,
        /// <summary>
        /// False
        /// </summary>
        [EnumMember]
        NotIsFocus = 0
    }
}
