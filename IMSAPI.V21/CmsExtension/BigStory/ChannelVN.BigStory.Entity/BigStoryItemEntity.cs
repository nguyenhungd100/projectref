﻿using ChannelVN.CMS.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace ChannelVN.BigStory.Entity
{
    public class BigStoryItemEntity : EntityBase
    {
        public int Id { get; set; }
        public int BigStoryId { get; set; }
        public string Title { get; set; }
        public string Url { get; set; }
        public string Body { get; set; }
        public DateTime PublishedDate { get; set; }
        public DateTime CreatedDate { get; set; }
        public string CreatedBy { get; set; }
        public int Status { get; set; }
        public bool IsHighlight { get; set; }
        public string TabId { get; set; }
        public bool IsFocus { get; set; }
        public string Avatar { get; set; }
        public bool IsHot { get; set; }
    }
    public enum BigStoryItemStatus
    {
        /// <summary>
        /// Tất cả các trạng thái
        /// </summary>
        [EnumMember]
        All = -1,
        /// <summary>
        /// Lưu tạm
        /// </summary>
        [EnumMember]
        Temporary = 0,
        /// <summary>
        /// Xuất bản
        /// </summary>
        [EnumMember]
        Published = 1,
        /// <summary>
        /// Gỡ xuất bản
        /// </summary>
        [EnumMember]
        Unpublished = 2
    }
    public enum BigStoryItemIsHighlight
    {
        /// <summary>
        /// Tất cả các trạng thái
        /// </summary>
        [EnumMember]
        All = -1,
        /// <summary>
        /// True
        /// </summary>
        [EnumMember]
        IsFocus = 1,
        /// <summary>
        /// False
        /// </summary>
        [EnumMember]
        NotIsFocus = 0
    }
}
