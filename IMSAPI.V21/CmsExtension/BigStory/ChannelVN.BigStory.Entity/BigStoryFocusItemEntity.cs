﻿using ChannelVN.CMS.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace ChannelVN.BigStory.Entity
{
    public class BigStoryFocusItemEntity : EntityBase
    {
        public int Id { get; set; }
        public int BigStoryId { get; set; }
        public string TabId { get; set; }
        public string Title { get; set; }
        public string Url { get; set; }       
        public DateTime CreatedDate { get; set; }
        public string CreatedBy { get; set; }        
    }    
}
