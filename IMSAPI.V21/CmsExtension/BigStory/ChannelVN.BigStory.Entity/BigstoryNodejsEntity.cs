﻿using ChannelVN.CMS.Common;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace ChannelVN.BigStory.Entity
{
    public class BigstoryNodejsEntity
    {
    }

    #region Entity
    //public enum BigStoryIsFocus : int
    //{
    //    All = -1,
    //    IsFocus = 1,
    //    NotIsFocus = 0,
    //}

    //public enum BigStoryStatus : int
    //{

    //    All = -1,
    //    Temporary = 0,
    //    Published = 1,
    //    Unpublished = 2,
    //}

    [DataContract]
    public class ResponseListBigStory
    {
        [DataMember]
        public bool Success { get; set; }
        [DataMember]
        public int Code { get; set; }
        [DataMember]
        public string Message { get; set; }
        [DataMember]
        public List<NodeJs_BigStoryEntity> Data { get; set; }
        public ResponseListBigStory()
        {
            Success = false;
            Code = 0;
            Message = "";
            Data = new List<NodeJs_BigStoryEntity>();
        }
    }
    [DataContract]
    public class ResponseBigStory
    {
        [DataMember]
        public bool Success { get; set; }
        [DataMember]
        public int Code { get; set; }
        [DataMember]
        public string Message { get; set; }
        [DataMember]
        public NodeJs_BigStoryEntity Data { get; set; }
        public ResponseBigStory()
        {
            Success = false;
            Code = 0;
            Message = "";
            Data = null;
        }
    }
    [DataContract]
    public class ResponseBigStoryItem
    {
        [DataMember]
        public bool Success { get; set; }
        [DataMember]
        public int Code { get; set; }
        [DataMember]
        public string Message { get; set; }
        [DataMember]
        public NodeJs_BigStoryItemEntity Data { get; set; }
        public ResponseBigStoryItem()
        {
            Success = false;
            Code = 0;
            Message = "";
            Data = null;
        }
    }
    [DataContract]
    public class ResponseListBigStoryItem
    {
        [DataMember]
        public bool Success { get; set; }
        [DataMember]
        public int Code { get; set; }
        [DataMember]
        public string Message { get; set; }
        [DataMember]
        public List<NodeJs_BigStoryItemEntity> Data { get; set; }
        public ResponseListBigStoryItem()
        {
            Success = false;
            Code = 0;
            Message = "";
            Data = new List<NodeJs_BigStoryItemEntity>();
        }
    }
    [DataContract]
    public class NodeJs_BigStoryEntity
    {
        [JsonProperty("id")]
        public string Id { get; set; }

        [JsonProperty("title")]
        public string Title { get; set; }

        [JsonProperty("created_date")]
        public DateTime CreatedDate { get; set; }

        [JsonProperty("created_by")]
        public string CreatedBy { get; set; }

        [JsonProperty("modified_date")]
        //public string ModifiedDateStr { get; set; }
        //public DateTime ModifiedDate { get { return Utility.ConvertToDateTime(ModifiedDateStr); } set { } }
        public DateTime ModifiedDate { get; set; }

        [JsonProperty("modified_by")]
        public string ModifiedBy { get; set; }

        [JsonProperty("status")]
        public int Status { get; set; }

        [JsonProperty("is_focus")]
        public int IsFocus { get; set; }

        [JsonProperty("avatar")]
        public string Avatar { get; set; }

        [JsonProperty("description")]
        public string Description { get; set; }

        [JsonProperty("channel_id")]
        public string ChannelId { get; set; }
    }

    [DataContract]
    public class NodeJs_BigStoryItemEntity
    {
        [JsonProperty("id")]
        public string Id { get; set; }

        [JsonProperty("bigstory_id")]
        public string BigstoryId { get; set; }
        [JsonProperty("title")]
        public string Title { get; set; }
        [JsonProperty("url")]
        public string Url { get; set; }
        [JsonProperty("body")]
        public string Body { get; set; }

        [JsonProperty("created_date")]
        public DateTime CreatedDate { get; set; }
        [JsonProperty("created_by")]
        public string CreatedBy { get; set; }

        [JsonProperty("published_date")]
        public DateTime PublishedDate { get; set; }
        [DataMember]
        public int Status { get; set; }
        [DataMember]
        //public int is_focus { get; set; }
        [JsonProperty("is_focus")]
        //public bool IsFocus { get { return is_focus == 1 ? true : false; } set { } }
        public int IsFocus { get; set; }
        [DataMember]
        //public int is_highlight { get; set; }
        [JsonProperty("is_highlight")]        
        //public bool IsHighlight { get { return is_highlight == 1 ? true : false; } set { } }
        public int IsHighlight { get; set; }
        [JsonProperty("tab_id")]
        public string TabId { get; set; }
        [DataMember]
        //public int is_hot { get; set; }
        [JsonProperty("is_hot")]
        //public bool IsHot { get { return is_hot == 1 ? true : false; } set { } }
        public int IsHot { get; set; }
        [JsonProperty("avatar")]
        public string Avatar { get; set; }
        //[JsonProperty("description")]
        //public string Description { get; set; }

        [JsonProperty("channel_id")]
        public string ChannelId { get; set; }
    }
    public class BigStoryItemMappingEntity
    {
        public string Id { get; set; }
        public string BigStoryId { get; set; }
        public string Title { get; set; }
        public string Url { get; set; }
        public string Body { get; set; }
        public DateTime PublishedDate { get; set; }
        public DateTime CreatedDate { get; set; }
        public string CreatedBy { get; set; }
        public int Status { get; set; }
        public int IsHighlight { get; set; }
        public string TabId { get; set; }
        public int IsFocus { get; set; }
        public string Avatar { get; set; }
        public int IsHot { get; set; }
        public bool IsRemove { get; set; }
    }
    #endregion
}
