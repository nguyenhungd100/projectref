﻿using ChannelVN.BigStory.Entity;
using ChannelVN.BigStory.Dal;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ChannelVN.BigStory.Entity.ErrorCode;
using ChannelVN.CMS.Common;
using ChannelVN.BigStory.Bo.Nodejs;

namespace ChannelVN.BigStory.Bo
{
    public class BigStoryItemBo
    {
        public static ErrorMapping.ErrorCodes InsertBigItemStory(NodeJs_BigStoryItemEntity entity, ref string Id)
        {
            try
            {
                //var checkBigstoryId = BigStoryBo.GetBigStoryById(entity.BigStoryId);
                //if (checkBigstoryId == null)
                //{
                //    return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.UnknowError, "BigstoryId không tồn tại!");
                //}
                //return BigStoryItemDal.Insert(entity) ? WcfActionResponse.CreateSuccessResponse()
                //       : WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.BusinessError,
                //                                               ErrorMapping.Current[ErrorMapping.ErrorCodes.BusinessError]);

                //goi nodejs
                var response = BigStoryNodeJsServices.BigStory_Item_Insert(entity);
                Id = response.Data;
                return response.Success ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.BusinessError;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return ErrorMapping.ErrorCodes.BusinessError;
            }

        }

        public static WcfActionResponse InsertBigItemStoryByList(List<BigStoryItemMappingEntity> list, string bigstory_id)
        {
            try
            {
                //foreach (var item in list)
                //{
                //    var checkBigstoryId = BigStoryBo.GetBigStoryById(item.BigStoryId);
                //    if (checkBigstoryId == null)
                //    {
                //        return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.UnknowError, "BigstoryId không tồn tại!");
                //    }
                //    BigStoryItemDal.Insert(item);
                //}
                //return WcfActionResponse.CreateSuccessResponse();

                //goi nodejs
                var response = BigStoryNodeJsServices.BigStory_Item_Insert_ByList(list, bigstory_id);              
                return response;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.BusinessError,
                                                               ErrorMapping.Current[ErrorMapping.ErrorCodes.BusinessError]);
            }

        }

        //public static WcfActionResponse UpdateBigItemStoryByList(List<BigStoryItemEntity> list, int bigStoryId)
        //{
        //    try
        //    {
        //        if (bigStoryId > 0)
        //            DeleteBigStoryItemByBigStoryID(bigStoryId);
        //        InsertBigItemStoryByList(list);
        //        return WcfActionResponse.CreateSuccessResponse();
        //    }
        //    catch (Exception ex)
        //    {
        //        Logger.WriteLog(Logger.LogType.Error, ex.Message);
        //        return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.BusinessError,
        //                                                       ErrorMapping.Current[ErrorMapping.ErrorCodes.BusinessError]);
        //    }
        //}

        public static WcfActionResponse UpdateBigItemStory(NodeJs_BigStoryItemEntity entity)
        {
            try
            {
                //var checkBigstoryId = BigStoryBo.GetBigStoryById(entity.BigStoryId);
                //if (checkBigstoryId == null)
                //{
                //    return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.UnknowError, "BigstoryId không tồn tại!");
                //}
                //return BigStoryItemDal.Update(entity) ? WcfActionResponse.CreateSuccessResponse()
                //       : WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.BusinessError,
                //                                               ErrorMapping.Current[ErrorMapping.ErrorCodes.BusinessError]);

                //goi nodejs
                var response = BigStoryNodeJsServices.BigStory_Item_Update(entity);
                return response;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.BusinessError,
                                                               ErrorMapping.Current[ErrorMapping.ErrorCodes.BusinessError]);
            }
        }

        public static ErrorMapping.ErrorCodes DeleteBigStoryItem(string Id)
        {
            try
            {
                //return BigStoryItemDal.Delete(Id)
                //        ? ErrorMapping.ErrorCodes.Success
                //        : ErrorMapping.ErrorCodes.BusinessError;

                //goi nodejs
                var response = BigStoryNodeJsServices.BigStory_Item_Delete(Id);
                return response.Success ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.BusinessError;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return ErrorMapping.ErrorCodes.BusinessError;
            }

        }

        //public static ErrorMapping.ErrorCodes DeleteBigStoryItemByBigStoryID(int BigStoryId)
        //{
        //    try
        //    {
        //        return BigStoryItemDal.DeleteByBigStoryID(BigStoryId)
        //                ? ErrorMapping.ErrorCodes.Success
        //                : ErrorMapping.ErrorCodes.BusinessError;
        //    }
        //    catch (Exception ex)
        //    {
        //        Logger.WriteLog(Logger.LogType.Error, ex.Message);
        //        return ErrorMapping.ErrorCodes.BusinessError;
        //    }

        //}

        //public static ErrorMapping.ErrorCodes BigStoryItem_UpdateStatus(int Id, int Status)
        //{
        //    try
        //    {
        //        return BigStoryItemDal.UpdateStatus(Id, Status)
        //                  ? ErrorMapping.ErrorCodes.Success
        //                  : ErrorMapping.ErrorCodes.BusinessError;
        //    }
        //    catch (Exception ex)
        //    {
        //        Logger.WriteLog(Logger.LogType.Error, ex.Message);
        //        return ErrorMapping.ErrorCodes.BusinessError;
        //    }
        //}

        //public static ErrorMapping.ErrorCodes BigStoryItem_UpdateIsHighlight(int Id, bool IsHighlight)
        //{
        //    try
        //    {
        //        return BigStoryItemDal.UpdateHighlight(Id, IsHighlight)
        //               ? ErrorMapping.ErrorCodes.Success
        //               : ErrorMapping.ErrorCodes.BusinessError;
        //    }
        //    catch (Exception ex)
        //    {
        //        Logger.WriteLog(Logger.LogType.Error, ex.Message);
        //        return ErrorMapping.ErrorCodes.BusinessError;
        //    }

        //}

        public static List<NodeJs_BigStoryItemEntity> BigStoryItem_GetByBigStoryId(string BigstoryId)
        {
            try
            {
                //return BigStoryItemDal.GetByBigStoryId(BigstoryId);
                //goi nodejs
                return BigStoryNodeJsServices.BigStoryItem_GetListByBigStoryId(BigstoryId);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return new List<NodeJs_BigStoryItemEntity>();
            }
        }

        public static NodeJs_BigStoryItemEntity BigStoryItem_GetById(string Id)
        {
            try
            {
                //return BigStoryItemDal.GetById(Id);
                //goi nodejs
                return BigStoryNodeJsServices.BigStoryItem_GetById(Id);       
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return null;
            }
        }
    }
}
