﻿using ChannelVN.BigStory.Entity;
using ChannelVN.BigStory.Dal;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ChannelVN.BigStory.Entity.ErrorCode;
using ChannelVN.CMS.Common;
using ChannelVN.BigStory.Bo.Nodejs;

namespace ChannelVN.BigStory.Bo
{
    public class BigStoryBo
    {
        //public static ErrorMapping.ErrorCodes InsertBigStory(BigStoryEntity entity, List<BigStoryItemEntity> ListBigStoryItem)
        //{
        //    try
        //    {
        //        int BigstoryId = 0;
        //        var returnvalue = BigStoryDal.Insert(entity, ref BigstoryId);
        //        foreach (var item in ListBigStoryItem)
        //        {
        //            item.BigStoryId = BigstoryId;
        //            BigStoryItemDal.Insert(item);
        //        }
        //        return returnvalue
        //                   ? ErrorMapping.ErrorCodes.Success
        //                   : ErrorMapping.ErrorCodes.BusinessError;
        //    }
        //    catch (Exception ex)
        //    {
        //        Logger.WriteLog(Logger.LogType.Error, ex.Message);
        //        return ErrorMapping.ErrorCodes.BusinessError;
        //    }
           
        //}
        public static ErrorMapping.ErrorCodes InsertBigStory(NodeJs_BigStoryEntity entity, ref string Id)
        {
            try
            {
                //var returnvalue = BigStoryDal.Insert(entity, ref BigstoryId);
                //return returnvalue
                //           ? ErrorMapping.ErrorCodes.Success
                //           : ErrorMapping.ErrorCodes.BusinessError;

                //goi nodejs
                var response = BigStoryNodeJsServices.BigStory_Insert(entity);
                Id = response.Data;
                return response.Success ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.BusinessError;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return ErrorMapping.ErrorCodes.BusinessError;
            }
        }
        public static ErrorMapping.ErrorCodes UpdateBigStory(NodeJs_BigStoryEntity entity)
        {
            try
            {
                //return BigStoryDal.Update(entity)
                //        ? ErrorMapping.ErrorCodes.Success
                //        : ErrorMapping.ErrorCodes.BusinessError;

                //goi nodejs
                var response = BigStoryNodeJsServices.BigStory_Update(entity);                
                return response.Success ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.BusinessError;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return ErrorMapping.ErrorCodes.BusinessError;
            }
            
        }
        //public static ErrorMapping.ErrorCodes UpdateBigStory(BigStoryEntity entity, List<BigStoryItemEntity> ListBigStoryItem)
        //{
        //    try
        //    {
        //        var returnvalue = BigStoryDal.Update(entity);
        //        foreach (var item in ListBigStoryItem)
        //        {
        //            item.BigStoryId = entity.Id;
        //            BigStoryItemDal.Insert(item);
        //        }
        //        return returnvalue
        //                   ? ErrorMapping.ErrorCodes.Success
        //                   : ErrorMapping.ErrorCodes.BusinessError;
        //    }
        //    catch (Exception ex)
        //    {
        //        Logger.WriteLog(Logger.LogType.Error, ex.Message);
        //        return ErrorMapping.ErrorCodes.BusinessError;
        //    }
            
        //}
        //public static ErrorMapping.ErrorCodes BigStory_UpdateStatus(int Id, int Status)
        //{
        //    try
        //    {
        //        return BigStoryDal.Update_Status(Id, Status)
        //               ? ErrorMapping.ErrorCodes.Success
        //               : ErrorMapping.ErrorCodes.BusinessError;
        //    }
        //    catch (Exception ex)
        //    {
        //        Logger.WriteLog(Logger.LogType.Error, ex.Message);
        //        return ErrorMapping.ErrorCodes.BusinessError;
        //    }
        //}
        //public static ErrorMapping.ErrorCodes BigStory_UpdateIsFocus(int Id, bool IsFocus)
        //{
        //    try
        //    {
        //        return BigStoryDal.Update_IsFocus(Id, IsFocus)
        //               ? ErrorMapping.ErrorCodes.Success
        //               : ErrorMapping.ErrorCodes.BusinessError;
        //    }
        //    catch (Exception ex)
        //    {
        //        Logger.WriteLog(Logger.LogType.Error, ex.Message);
        //        return ErrorMapping.ErrorCodes.BusinessError;
        //    }
        //}
        //public static ErrorMapping.ErrorCodes DeleteBigStory(int Id)
        //{
        //    try
        //    {
        //        return BigStoryDal.Delete(Id)
        //               ? ErrorMapping.ErrorCodes.Success
        //               : ErrorMapping.ErrorCodes.BusinessError;
        //    }
        //    catch (Exception ex)
        //    {
        //        Logger.WriteLog(Logger.LogType.Error, ex.Message);
        //        return ErrorMapping.ErrorCodes.BusinessError;
        //    }
        //}
        public static List<NodeJs_BigStoryEntity> Search(DateTime fromDate, DateTime toDate, int pageIndex, int pageSize, ref int totalRow)
        {
            try
            {
                //return BigStoryDal.Search(keyword, CreatedBy, IsFocus, status, fromDate, toDate, pageIndex, pageSize, ref totalRow);
                //goi nodejs
                return BigStoryNodeJsServices.BigStory_Search(fromDate, toDate, pageIndex, pageSize, ref totalRow);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return new List<NodeJs_BigStoryEntity>();
            }
        }
        public static NodeJs_BigStoryEntity GetBigStoryById(string Id)
        {
            try
            {
                //return BigStoryDal.GetBigStoryById(Id);
                //goi nodejs
                return BigStoryNodeJsServices.BigStory_GetById(Id);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return null;
            }
        }
    }
}
