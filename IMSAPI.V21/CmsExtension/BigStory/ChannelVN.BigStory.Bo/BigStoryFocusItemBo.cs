﻿using ChannelVN.BigStory.Entity;
using ChannelVN.BigStory.Dal;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ChannelVN.BigStory.Entity.ErrorCode;
using ChannelVN.CMS.Common;

namespace ChannelVN.BigStory.Bo
{
    public class BigStoryFocusItemBo
    {
        //public static WcfActionResponse InsertBigItemStory(BigStoryFocusItemEntity entity)
        //{
        //    try
        //    {
        //        var checkBigstoryId = BigStoryBo.GetBigStoryById(entity.BigStoryId);
        //        if (checkBigstoryId == null)
        //        {
        //            return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.UnknowError, "BigstoryId không tồn tại!");
        //        }
        //        return BigStoryFocusItemDal.Insert(entity) ? WcfActionResponse.CreateSuccessResponse()
        //               : WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.BusinessError,
        //                                                       ErrorMapping.Current[ErrorMapping.ErrorCodes.BusinessError]);
        //    }
        //    catch (Exception ex)
        //    {
        //        Logger.WriteLog(Logger.LogType.Error, ex.Message);
        //        return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.BusinessError,
        //                                                       ErrorMapping.Current[ErrorMapping.ErrorCodes.BusinessError]);
        //    }

        //}

        //public static WcfActionResponse InsertBigItemStoryByList(List<BigStoryFocusItemEntity> list)
        //{
        //    try
        //    {
        //        foreach (var item in list)
        //        {
        //            var checkBigstoryId = BigStoryBo.GetBigStoryById(item.BigStoryId);
        //            if (checkBigstoryId == null)
        //            {
        //                return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.UnknowError, "BigstoryId không tồn tại!");
        //            }
        //            BigStoryFocusItemDal.Insert(item);
        //        }
        //        return WcfActionResponse.CreateSuccessResponse();
        //    }
        //    catch (Exception ex)
        //    {
        //        Logger.WriteLog(Logger.LogType.Error, ex.Message);
        //        return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.BusinessError,
        //                                                       ErrorMapping.Current[ErrorMapping.ErrorCodes.BusinessError]);
        //    }

        //}
        //public static WcfActionResponse UpdateBigItemStoryByList(List<BigStoryFocusItemEntity> list, int bigStoryId)
        //{
        //    try
        //    {
        //        if (bigStoryId > 0)
        //            DeleteBigStoryItemByBigStoryID(bigStoryId);
        //        InsertBigItemStoryByList(list);
        //        return WcfActionResponse.CreateSuccessResponse();
        //    }
        //    catch (Exception ex)
        //    {
        //        Logger.WriteLog(Logger.LogType.Error, ex.Message);
        //        return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.BusinessError,
        //                                                       ErrorMapping.Current[ErrorMapping.ErrorCodes.BusinessError]);
        //    }
        //}
        //public static WcfActionResponse UpdateBigItemStory(BigStoryFocusItemEntity entity)
        //{
        //    try
        //    {
        //        var checkBigstoryId = BigStoryBo.GetBigStoryById(entity.BigStoryId);
        //        if (checkBigstoryId == null)
        //        {
        //            return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.UnknowError, "BigstoryId không tồn tại!");
        //        }
        //        return BigStoryFocusItemDal.Update(entity) ? WcfActionResponse.CreateSuccessResponse()
        //               : WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.BusinessError,
        //                                                       ErrorMapping.Current[ErrorMapping.ErrorCodes.BusinessError]);
        //    }
        //    catch (Exception ex)
        //    {
        //        Logger.WriteLog(Logger.LogType.Error, ex.Message);
        //        return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.BusinessError,
        //                                                       ErrorMapping.Current[ErrorMapping.ErrorCodes.BusinessError]);
        //    }
        //}

        //public static ErrorMapping.ErrorCodes DeleteBigStoryItem(int Id)
        //{
        //    try
        //    {
        //        return BigStoryFocusItemDal.Delete(Id)
        //                ? ErrorMapping.ErrorCodes.Success
        //                : ErrorMapping.ErrorCodes.BusinessError;
        //    }
        //    catch (Exception ex)
        //    {
        //        Logger.WriteLog(Logger.LogType.Error, ex.Message);
        //        return ErrorMapping.ErrorCodes.BusinessError;
        //    }

        //}

        //public static ErrorMapping.ErrorCodes DeleteBigStoryItemByBigStoryID(int BigStoryId)
        //{
        //    try
        //    {
        //        return BigStoryFocusItemDal.DeleteByBigStoryID(BigStoryId)
        //                ? ErrorMapping.ErrorCodes.Success
        //                : ErrorMapping.ErrorCodes.BusinessError;
        //    }
        //    catch (Exception ex)
        //    {
        //        Logger.WriteLog(Logger.LogType.Error, ex.Message);
        //        return ErrorMapping.ErrorCodes.BusinessError;
        //    }

        //}

        //public static List<BigStoryFocusItemEntity> BigStoryItem_GetByBigStoryId(int BigstoryId)
        //{
        //    try
        //    {
        //        return BigStoryFocusItemDal.GetByBigStoryId(BigstoryId);
        //    }
        //    catch (Exception ex)
        //    {
        //        Logger.WriteLog(Logger.LogType.Error, ex.Message);
        //        return new List<BigStoryFocusItemEntity>();
        //    }
        //}
        //public static BigStoryFocusItemEntity BigStoryItem_GetById(int Id)
        //{
        //    try
        //    {
        //        return BigStoryFocusItemDal.GetById(Id);
        //    }
        //    catch (Exception ex)
        //    {
        //        Logger.WriteLog(Logger.LogType.Error, ex.Message);
        //        return null;
        //    }
        //}
    }
}
