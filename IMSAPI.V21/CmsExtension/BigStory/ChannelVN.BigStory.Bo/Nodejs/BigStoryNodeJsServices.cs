﻿using ChannelVN.Bigstory.Bo.Nodejs;
using ChannelVN.BigStory.Entity;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.Common.ChannelConfig;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace ChannelVN.BigStory.Bo.Nodejs
{
    public class BigStoryNodeJsServices
    {
        private static NodeJsRestClient GetRestClient()
        {
            return new NodeJsRestClient
            {
                EndPoint = CmsChannelConfiguration.GetAppSetting("BigStoryApiUrl"),
                Method = NodeJsRestClient.HttpMethod.POST,
                ContentType = "application/x-www-form-urlencoded; charset=utf-8",
                Channel_Id = CmsChannelConfiguration.GetAppSetting("BigStoryApiChannel"),
                SecretKey = CmsChannelConfiguration.GetAppSetting("BigStoryApiSecretKey"),
                PostData = "",
                ActionName = ""
            };
        }


        const string DateTimeFormat = "yyyy/MM/dd HH:mm";

        #region BigStory
        public static List<NodeJs_BigStoryEntity> BigStory_Search(DateTime fromDate, DateTime toDate, int pageIndex, int pageSize, ref int totalRow)
        {
            try
            {
                var client = GetRestClient();
                var sb = new StringBuilder();
                sb.AppendFormat("channel_id={0}", client.Channel_Id);
                var fDate = fromDate.ToString(DateTimeFormat);
                var tDate = toDate.ToString(DateTimeFormat);
                if (fDate != "0001/01/01 00:00")
                    sb.AppendFormat("&created_date_from={0}", fDate);
                if (tDate != "0001/01/01 00:00")
                    sb.AppendFormat("&created_date_to={0}", tDate);
                sb.AppendFormat("&page_index={0}", pageIndex);
                sb.AppendFormat("&page_size={0}", pageSize);
                sb.AppendFormat("&secret_key={0}", client.SecretKey);
                client.PostData = sb.ToString();
                client.ActionName = "search";
                var strData = client.MakeRequest();
                var ParseObject = JsonConvert.DeserializeObject<ResponseListBigStory>(strData);

                if (ParseObject == null) ParseObject = new ResponseListBigStory();
                //totalRow = ParseObject.Data.total_count;
                if (!ParseObject.Success)
                    Logger.WriteLog(Logger.LogType.Error, "BigStory_Search=>" + ParseObject.Message);
                return ParseObject.Data == null ? new List<NodeJs_BigStoryEntity>() : ParseObject.Data;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, "BigStory_Search=>" + ex.Message);
                return new List<NodeJs_BigStoryEntity>();
            }
        }
        public static NodeJs_BigStoryEntity BigStory_GetById(string Id)
        {
            try
            {
                var client = GetRestClient();
                var sb = new StringBuilder();
                sb.AppendFormat("channel_id={0}", client.Channel_Id);
                sb.AppendFormat("&id={0}", Id);
                sb.AppendFormat("&secret_key={0}", client.SecretKey);
                client.PostData = sb.ToString();
                client.ActionName = "get-by-id";
                var strData = client.MakeRequest();
                var ParseObject = JsonConvert.DeserializeObject<ResponseBigStory>(strData);

                if (ParseObject == null) ParseObject = new ResponseBigStory();
                //totalRow = ParseObject.Data.total_count;
                if (!ParseObject.Success)
                    Logger.WriteLog(Logger.LogType.Error, "BigStory_GetById=>" + ParseObject.Message);
                return ParseObject.Data;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, "BigStory_GetById=>" + ex.Message);
                return null;
            }
        }
        public static WcfActionResponse BigStory_Insert(NodeJs_BigStoryEntity obj)
        {
            try
            {
                var client = GetRestClient();
                var sb = new StringBuilder();
                sb.AppendFormat("channel_id={0}", client.Channel_Id);
                sb.AppendFormat("&secret_key={0}", client.SecretKey);
                sb.AppendFormat("&title={0}", obj.Title);
                sb.AppendFormat("&created_by={0}", obj.CreatedBy);
                sb.AppendFormat("&is_focus={0}", obj.IsFocus);
                sb.AppendFormat("&avatar={0}", obj.Avatar);
                sb.AppendFormat("&status={0}", obj.Status);
                sb.AppendFormat("&description={0}", obj.Description);
                client.PostData = sb.ToString();
                client.ActionName = "add";
                var data = JsonConvert.DeserializeObject<NodeJsRestClient.ApiActionResponse>(client.MakeRequest());

                return NodeJsRestClient.ConvertWcfResponse(data);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, "BigStory_Insert=>" + ex.Message);
                return new WcfActionResponse();
            }
        }
        public static WcfActionResponse BigStory_Update(NodeJs_BigStoryEntity obj)
        {
            try
            {
                var client = GetRestClient();
                var sb = new StringBuilder();
                sb.AppendFormat("channel_id={0}", client.Channel_Id);
                sb.AppendFormat("&secret_key={0}", client.SecretKey);
                sb.AppendFormat("&id={0}", obj.Id);
                sb.AppendFormat("&title={0}", obj.Title);
                sb.AppendFormat("&modified_by={0}", obj.ModifiedBy);
                sb.AppendFormat("&is_focus={0}", obj.IsFocus);
                sb.AppendFormat("&avatar={0}", obj.Avatar);
                sb.AppendFormat("&status={0}", obj.Status);
                sb.AppendFormat("&description={0}", obj.Description);
                client.PostData = sb.ToString();
                client.ActionName = "update";
                var data = JsonConvert.DeserializeObject<NodeJsRestClient.ApiActionResponse>(client.MakeRequest());

                return NodeJsRestClient.ConvertWcfResponse(data);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, "BigStory_Update=>" + ex.Message);
                return new WcfActionResponse();
            }
        }
        #endregion

        #region BigStory_Item
        public static NodeJs_BigStoryItemEntity BigStoryItem_GetById(string Id)
        {
            try
            {
                var client = GetRestClient();
                var sb = new StringBuilder();
                sb.AppendFormat("channel_id={0}", client.Channel_Id);
                sb.AppendFormat("&secret_key={0}", client.SecretKey);
                sb.AppendFormat("&id={0}", Id);
                client.PostData = sb.ToString();
                client.ActionName = "item/get-by-id";
                var strData = client.MakeRequest();
                var ParseObject = JsonConvert.DeserializeObject<ResponseBigStoryItem>(strData);

                if (ParseObject == null) ParseObject = new ResponseBigStoryItem();
                if (!ParseObject.Success)
                    Logger.WriteLog(Logger.LogType.Error, "BigStoryItem_GetById=>" + ParseObject.Message);
                return ParseObject.Data;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, "BigStoryItem_GetById=>" + ex.Message);
                return new NodeJs_BigStoryItemEntity();
            }
        }
        public static List<NodeJs_BigStoryItemEntity> BigStoryItem_GetListByBigStoryId(string BigstoryId)
        {
            try
            {
                var client = GetRestClient();
                var sb = new StringBuilder();
                sb.AppendFormat("channel_id={0}", client.Channel_Id);
                sb.AppendFormat("&secret_key={0}", client.SecretKey);
                sb.AppendFormat("&bigstory_id={0}", BigstoryId);
                client.PostData = sb.ToString();
                client.ActionName = "item/get-by-bigstory-id";
                var strData = client.MakeRequest();
                var ParseObject = JsonConvert.DeserializeObject<ResponseListBigStoryItem>(strData);

                if (ParseObject == null) ParseObject = new ResponseListBigStoryItem();
                if (!ParseObject.Success)
                    Logger.WriteLog(Logger.LogType.Error, "BigStoryItem_GetListByBigStoryId=>" + ParseObject.Message);
                return ParseObject.Data == null ? new List<NodeJs_BigStoryItemEntity>() : ParseObject.Data;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, "BigStoryItem_GetListByBigStoryId=>" + ex.Message);
                return new List<NodeJs_BigStoryItemEntity>();
            }
        }
        public static WcfActionResponse BigStory_Item_Insert(NodeJs_BigStoryItemEntity obj)
        {
            try
            {
                var client = GetRestClient();
                var sb = new StringBuilder();
                sb.AppendFormat("channel_id={0}", client.Channel_Id);
                sb.AppendFormat("&secret_key={0}", client.SecretKey);
                sb.AppendFormat("&bigstory_id={0}", obj.BigstoryId);
                sb.AppendFormat("&title={0}", obj.Title);
                sb.AppendFormat("&url={0}", obj.Url);
                sb.AppendFormat("&body={0}", obj.Body);
                sb.AppendFormat("&is_highlight={0}", obj.IsHighlight);
                sb.AppendFormat("&status={0}", obj.Status);
                sb.AppendFormat("&created_by={0}", obj.CreatedBy);
                sb.AppendFormat("&tab_id={0}", obj.TabId);
                sb.AppendFormat("&is_focus={0}", obj.IsFocus);
                sb.AppendFormat("&is_hot={0}", obj.IsHot);
                sb.AppendFormat("&avatar={0}", obj.Avatar);
                sb.AppendFormat("&published_date={0}", obj.PublishedDate);
                client.PostData = sb.ToString();
                client.ActionName = "item/add";
                var data = JsonConvert.DeserializeObject<NodeJsRestClient.ApiActionResponse>(client.MakeRequest());

                return NodeJsRestClient.ConvertWcfResponse(data);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, "BigStory_Item_Insert=>" + ex.Message);
                return new WcfActionResponse();
            }
        }
        public static WcfActionResponse BigStory_Item_Update(NodeJs_BigStoryItemEntity obj)
        {
            try
            {
                var client = GetRestClient();
                var sb = new StringBuilder();
                sb.AppendFormat("channel_id={0}", client.Channel_Id);
                sb.AppendFormat("&secret_key={0}", client.SecretKey);
                sb.AppendFormat("&id={0}", obj.Id);
                sb.AppendFormat("&bigstory_id={0}", obj.BigstoryId);
                sb.AppendFormat("&title={0}", obj.Title);
                sb.AppendFormat("&url={0}", obj.Url);
                sb.AppendFormat("&body={0}", obj.Body);
                sb.AppendFormat("&is_highlight={0}", obj.IsHighlight);
                sb.AppendFormat("&status={0}", obj.Status);
                sb.AppendFormat("&created_by={0}", obj.CreatedBy);
                sb.AppendFormat("&tab_id={0}", obj.TabId);
                sb.AppendFormat("&is_focus={0}", obj.IsFocus);
                sb.AppendFormat("&is_hot={0}", obj.IsHot);
                sb.AppendFormat("&avatar={0}", obj.Avatar);
                sb.AppendFormat("&published_date={0}", obj.PublishedDate);
                client.PostData = sb.ToString();
                client.ActionName = "item/update";
                var data = JsonConvert.DeserializeObject<NodeJsRestClient.ApiActionResponse>(client.MakeRequest());

                return NodeJsRestClient.ConvertWcfResponse(data);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, "BigStory_Item_Update=>" + ex.Message);
                return new WcfActionResponse();
            }
        }
        public static WcfActionResponse BigStory_Item_Delete(string Id)
        {
            try
            {
                var client = GetRestClient();
                var sb = new StringBuilder();
                sb.AppendFormat("channel_id={0}", client.Channel_Id);
                sb.AppendFormat("&secret_key={0}", client.SecretKey);
                sb.AppendFormat("&id={0}", Id);
                client.PostData = sb.ToString();
                client.ActionName = "item/remove";
                var data = JsonConvert.DeserializeObject<NodeJsRestClient.ApiActionResponse>(client.MakeRequest());

                return NodeJsRestClient.ConvertWcfResponse(data);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, "BigStory_Item_Delete=>" + ex.Message);
                return new WcfActionResponse();
            }
        }
        public static WcfActionResponse BigStory_Item_Insert_ByList(List<BigStoryItemMappingEntity> obj, string bigstory_id)
        {
            try
            {
                var client = GetRestClient();
                var tempList = obj.Select(bigStoryItem => new
                {
                    bigstory_id = bigStoryItem.BigStoryId,
                    title = bigStoryItem.Title,
                    url = bigStoryItem.Url,
                    body = bigStoryItem.Body,
                    is_highlight = bigStoryItem.IsHighlight,
                    status = bigStoryItem.Status,
                    created_by = bigStoryItem.CreatedBy,
                    is_focus = bigStoryItem.IsFocus,
                    is_hot = bigStoryItem.IsHot,
                    avatar = bigStoryItem.Avatar,
                    tab_id = bigStoryItem.TabId,
                    published_date = bigStoryItem.PublishedDate.ToString(DateTimeFormat),
                    channel_id = client.Channel_Id
                }).ToList();

                var sb = new StringBuilder();
                sb.AppendFormat("channel_id={0}", client.Channel_Id);
                sb.AppendFormat("&secret_key={0}", client.SecretKey);
                sb.AppendFormat("&bigstory_id={0}", bigstory_id);
                sb.AppendFormat("&data={0}", HttpContext.Current.Server.UrlEncode(NewtonJson.Serialize(tempList)));
                client.PostData = sb.ToString();
                client.ActionName = "item/add-list";
                var str = client.MakeRequest();
                var data = JsonConvert.DeserializeObject<NodeJsRestClient.NodejsActionResponse>(str);
                //var data = JsonConvert.DeserializeObject<ApiActionResponse>(str);

                return NodeJsRestClient.ConvertWcfResponseByObj(data);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, "BigStory_Item_Insert_ByList=>" + ex.Message);
                return new WcfActionResponse();
            }
        }
        public static WcfActionResponse BigStory_Item_Update_ByList(List<BigStoryItemMappingEntity> obj)
        {
            if (obj.Count == 0)
                return new WcfActionResponse();
            try
            {
                var client = GetRestClient();
                var tempList = obj.Select(bigStoryItem => new
                {
                    id = bigStoryItem.Id,
                    title = bigStoryItem.Title,
                    url = bigStoryItem.Url,
                    body = bigStoryItem.Body,
                    is_highlight = bigStoryItem.IsHighlight,
                    status = bigStoryItem.Status,
                    created_by = bigStoryItem.CreatedBy,
                    is_focus = bigStoryItem.IsFocus,
                    is_hot = bigStoryItem.IsHot,
                    published_date = bigStoryItem.PublishedDate.ToString(DateTimeFormat),
                    avatar = bigStoryItem.Avatar,
                    tab_id = bigStoryItem.TabId,
                    channel_id = client.Channel_Id
                });
                var sb = new StringBuilder();
                sb.AppendFormat("channel_id={0}", client.Channel_Id);
                sb.AppendFormat("&secret_key={0}", client.SecretKey);
                sb.AppendFormat("&data={0}", NewtonJson.Serialize(tempList));
                client.PostData = sb.ToString();
                client.ActionName = "item/update-list";
                var data = JsonConvert.DeserializeObject<NodeJsRestClient.NodejsActionResponse>(client.MakeRequest());

                return NodeJsRestClient.ConvertWcfResponseByObj(data);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, "BigStory_Item_Update_ByList=>" + ex.Message);
                return new WcfActionResponse();
            }
        }

        #endregion
    }
}
