﻿using ChannelVN.BigStory.Entity;
using ChannelVN.BigStory.MainDal.Databases;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChannelVN.BigStory.Dal
{
    public class BigStoryItemDal
    {
        public static bool Insert(BigStoryItemEntity BigStoryItem)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.BigStoryItemMainDal.Insert(BigStoryItem);
            }
            return returnValue;
        }

        public static bool Update(BigStoryItemEntity BigStoryItem)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.BigStoryItemMainDal.Update(BigStoryItem);
            }
            return returnValue;
        }

        public static bool Delete(int Id)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.BigStoryItemMainDal.Delete(Id);
            }
            return returnValue;
        }

        public static bool DeleteByBigStoryID(int Id)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.BigStoryItemMainDal.DeleteByBigStoryID(Id);
            }
            return returnValue;
        }

        public static bool UpdateStatus(int Id, int status)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.BigStoryItemMainDal.BigStoryItem_UpdateStatus(Id, status);
            }
            return returnValue;
        }

        public static bool UpdateHighlight(int Id, bool Highlight)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.BigStoryItemMainDal.BigStoryItem_UpdateHighlight(Id, Highlight);
            }
            return returnValue;
        }

        public static List<BigStoryItemEntity> GetByBigStoryId(int BigstoryId)
        {
            List<BigStoryItemEntity> returnValue = null;
            using (var db = new CmsMainDb())
            {
                returnValue = db.BigStoryItemMainDal.BigStoryItem_GetByBigStoryId(BigstoryId);
            }
            return returnValue;
        }

        public static BigStoryItemEntity GetById(int Id)
        {
            BigStoryItemEntity returnValue = null;
            using (var db = new CmsMainDb())
            {
                returnValue = db.BigStoryItemMainDal.BigStoryItem_GetById(Id);
            }
            return returnValue;
        }
    }
}
