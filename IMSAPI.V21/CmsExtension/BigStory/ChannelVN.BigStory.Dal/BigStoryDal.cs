﻿using ChannelVN.BigStory.Entity;
using ChannelVN.BigStory.MainDal.Databases;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChannelVN.BigStory.Dal
{
    public class BigStoryDal
    {
        public static bool Insert(BigStoryEntity BigStory, ref int newBigStoryId)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.BigStoryMainDal.Insert(BigStory, ref newBigStoryId);
            }
            return returnValue;
        }

        public static bool Update(BigStoryEntity BigStory)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.BigStoryMainDal.Update(BigStory);
            }
            return returnValue;
        }

        public static bool Update_Status(int BigStoryId, int status)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.BigStoryMainDal.BigStory_Update_Status(BigStoryId, status);
            }
            return returnValue;
        }

        public static bool Update_IsFocus(int BigStoryId, bool IsFocus)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.BigStoryMainDal.BigStory_Update_IsFocus(BigStoryId, IsFocus);
            }
            return returnValue;
        }

        public static bool Delete(int BigStoryId)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.BigStoryMainDal.Delete(BigStoryId);
            }
            return returnValue;
        }

        public static BigStoryEntity GetBigStoryById(int BigStoryId)
        {
            BigStoryEntity returnValue = null;
            using (var db = new CmsMainDb())
            {
                returnValue = db.BigStoryMainDal.GetBigStoryById(BigStoryId);
            }
            return returnValue;
        }

        public static List<BigStoryEntity> Search(string keyword, string CreatedBy, int IsFocus, int status, DateTime fromDate, DateTime toDate, int pageIndex, int pageSize, ref int totalRow)
        {
            List<BigStoryEntity> returnValue = null;
            using (var db = new CmsMainDb())
            {
                returnValue = db.BigStoryMainDal.Search(keyword, CreatedBy, IsFocus, status, fromDate, toDate, pageIndex, pageSize, ref totalRow);
            }
            return returnValue;
        }
    }
}
