﻿using ChannelVN.BigStory.Entity;
using ChannelVN.BigStory.MainDal.Databases;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChannelVN.BigStory.Dal
{
    public class BigStoryFocusItemDal
    {
        public static bool Insert(BigStoryFocusItemEntity BigStoryItem)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.BigStoryFocusItemMainDal.Insert(BigStoryItem);
            }
            return returnValue;
        }

        public static bool Update(BigStoryFocusItemEntity BigStoryItem)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.BigStoryFocusItemMainDal.Update(BigStoryItem);
            }
            return returnValue;
        }

        public static bool Delete(int Id)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.BigStoryFocusItemMainDal.Delete(Id);
            }
            return returnValue;
        }

        public static bool DeleteByBigStoryID(int Id)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.BigStoryFocusItemMainDal.DeleteByBigStoryID(Id);
            }
            return returnValue;
        }
                
        public static List<BigStoryFocusItemEntity> GetByBigStoryId(int BigstoryId)
        {
            List<BigStoryFocusItemEntity> returnValue = null;
            using (var db = new CmsMainDb())
            {
                returnValue = db.BigStoryFocusItemMainDal.BigStoryFocusItem_GetByBigStoryId(BigstoryId);
            }
            return returnValue;
        }

        public static BigStoryFocusItemEntity GetById(int Id)
        {
            BigStoryFocusItemEntity returnValue = null;
            using (var db = new CmsMainDb())
            {
                returnValue = db.BigStoryFocusItemMainDal.BigStoryFocusItem_GetById(Id);
            }
            return returnValue;
        }
    }
}
