﻿using ChannelVN.BigStory.Entity;
using ChannelVN.BigStory.MainDal.Databases;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChannelVN.BigStory.MainDal
{
    public abstract class BigStoryItemDalBase
    {
        public bool Insert(BigStoryItemEntity BigStoryItem)
        {
            const string commandText = "CMS_BigStoryItem_Insert";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Title", BigStoryItem.Title);
                _db.AddParameter(cmd, "IsHighlight", BigStoryItem.IsHighlight);
                _db.AddParameter(cmd, "Url", BigStoryItem.Url);
                _db.AddParameter(cmd, "BigStoryId", BigStoryItem.BigStoryId);
                _db.AddParameter(cmd, "Body", BigStoryItem.Body);
                _db.AddParameter(cmd, "PublishedDate", BigStoryItem.PublishedDate);
                _db.AddParameter(cmd, "CreatedBy", BigStoryItem.CreatedBy);
                _db.AddParameter(cmd, "Status", BigStoryItem.Status);
                _db.AddParameter(cmd, "TabId", BigStoryItem.TabId);
                _db.AddParameter(cmd, "IsFocus", BigStoryItem.IsFocus);
                _db.AddParameter(cmd, "Avatar", BigStoryItem.Avatar);
                _db.AddParameter(cmd, "IsHot", BigStoryItem.IsHot);
                var numberOfRow = _db.ExecuteNonQuery(cmd);
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public bool Update(BigStoryItemEntity BigStoryItem)
        {
            const string commandText = "CMS_BigStoryItem_Update";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", BigStoryItem.Id);
                _db.AddParameter(cmd, "Title", BigStoryItem.Title);
                _db.AddParameter(cmd, "IsHighlight", BigStoryItem.IsHighlight);
                _db.AddParameter(cmd, "Url", BigStoryItem.Url);
                _db.AddParameter(cmd, "Body", BigStoryItem.Body);
                _db.AddParameter(cmd, "PublishedDate", BigStoryItem.PublishedDate);
                _db.AddParameter(cmd, "CreatedBy", BigStoryItem.CreatedBy);
                _db.AddParameter(cmd, "Status", BigStoryItem.Status);
                _db.AddParameter(cmd, "TabId", BigStoryItem.TabId);
                var numberOfRow = _db.ExecuteNonQuery(cmd);
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public bool BigStoryItem_UpdateStatus(int Id, int status)
        {
            const string commandText = "CMS_BigStoryItem_Update_Status";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", Id);
                _db.AddParameter(cmd, "status", status);
                var numberOfRow = _db.ExecuteNonQuery(cmd);
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public bool BigStoryItem_UpdateHighlight(int Id, bool IsHighlight)
        {
            const string commandText = "CMS_BigStoryItem_Update_IsHighlight";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", Id);
                _db.AddParameter(cmd, "IsHighlight", IsHighlight);
                var numberOfRow = _db.ExecuteNonQuery(cmd);
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public bool Delete(int Id)
        {
            const string commandText = "CMS_BigStoryItem_Delete";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", Id);
                var numberOfRow = _db.ExecuteNonQuery(cmd);
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public bool DeleteByBigStoryID(int BigStoryId)
        {
            const string commandText = "CMS_BigStoryItem_DeleteByBigStoryId";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "BigStoryId", BigStoryId);
                var numberOfRow = _db.ExecuteNonQuery(cmd);
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public List<BigStoryItemEntity> BigStoryItem_GetByBigStoryId(int id)
        {
            const string commandText = "CMS_BigStoryItem_GetByBigStoryId";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "BigStoryId", id);
                var data = _db.GetList<BigStoryItemEntity>(cmd);
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public BigStoryItemEntity BigStoryItem_GetById(int id)
        {
            const string commandText = "CMS_BigStoryItem_GetById";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", id);
                var data = _db.Get<BigStoryItemEntity>(cmd);
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        #region Core members

        private readonly CmsMainDb _db;

        protected BigStoryItemDalBase(CmsMainDb db)
        {
            _db = db;
        }

        protected CmsMainDb Database
        {
            get { return _db; }
        }

        #endregion
    }
}
