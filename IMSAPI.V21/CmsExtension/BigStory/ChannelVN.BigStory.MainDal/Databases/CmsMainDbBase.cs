﻿using System.Data;
using ChannelVN.CMS.Common.DalCommon;

namespace ChannelVN.BigStory.MainDal.Databases
{
    /// <summary>
    /// The base class for the <see cref="CmsMainDb"/> class that 
    /// represents a connection to the <c>CmsMainDb</c> database. 
    /// </summary>
    /// <remarks>
    /// Do not change this source code. Modify the CmsMainDb class
    /// if you need to add or change some functionality.
    /// </remarks>
    public abstract class CmsMainDbBase : MainDbBase
    {


        private BigStoryDal _BigStoryMainDal;
        public BigStoryDal BigStoryMainDal
        {
            get { return _BigStoryMainDal ?? (_BigStoryMainDal = new BigStoryDal((CmsMainDb)this)); }
        }

        private BigStoryItemDal _BigStoryItemMainDal;
        public BigStoryItemDal BigStoryItemMainDal
        {
            get { return _BigStoryItemMainDal ?? (_BigStoryItemMainDal = new BigStoryItemDal((CmsMainDb)this)); }
        }

        private BigStoryFocusItemDal _BigStoryFocusItemMainDal;
        public BigStoryFocusItemDal BigStoryFocusItemMainDal
        {
            get { return _BigStoryFocusItemMainDal ?? (_BigStoryFocusItemMainDal = new BigStoryFocusItemDal((CmsMainDb)this)); }
        }

        #region Constructors

        protected CmsMainDbBase()
        {
        }
        protected CmsMainDbBase(bool init)
        {
            if (init)
            {
                InitConnection();
            }
        }

        #endregion

        #region Protected members

        protected override sealed void InitConnection()
        {
            _connection = CreateConnection();
            _connection.Open();
        }

        /// <summary>
        /// Creates a new connection to the database.
        /// </summary>
        /// <returns>A reference to the <see cref="System.Data.IDbConnection"/> object.</returns>
        protected abstract IDbConnection CreateConnection();

        #endregion
    }
}