﻿using ChannelVN.BigStory.Entity;
using ChannelVN.BigStory.MainDal.Databases;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChannelVN.BigStory.MainDal
{
    public abstract class BigStoryFocusItemDalBase
    {
        public bool Insert(BigStoryFocusItemEntity BigStoryItem)
        {
            const string commandText = "CMS_BigStoryItemFocus_Insert";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Title", BigStoryItem.Title);                
                _db.AddParameter(cmd, "Url", BigStoryItem.Url);
                _db.AddParameter(cmd, "BigStoryId", BigStoryItem.BigStoryId);
                _db.AddParameter(cmd, "TabId", BigStoryItem.TabId);                                
                _db.AddParameter(cmd, "CreatedBy", BigStoryItem.CreatedBy);                
                var numberOfRow = _db.ExecuteNonQuery(cmd);
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public bool Update(BigStoryFocusItemEntity BigStoryItem)
        {
            const string commandText = "CMS_BigStoryItemFocus_Update";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", BigStoryItem.Id);
                _db.AddParameter(cmd, "Title", BigStoryItem.Title);
                _db.AddParameter(cmd, "Url", BigStoryItem.Url);                
                _db.AddParameter(cmd, "TabId", BigStoryItem.TabId);
                _db.AddParameter(cmd, "CreatedBy", BigStoryItem.CreatedBy);
                var numberOfRow = _db.ExecuteNonQuery(cmd);
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        
        public bool Delete(int Id)
        {
            const string commandText = "CMS_BigStoryItemFocus_Delete";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", Id);
                var numberOfRow = _db.ExecuteNonQuery(cmd);
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public bool DeleteByBigStoryID(int BigStoryId)
        {
            const string commandText = "CMS_BigStoryItemFocus_DeleteByBigStoryId";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "BigStoryId", BigStoryId);
                var numberOfRow = _db.ExecuteNonQuery(cmd);
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public List<BigStoryFocusItemEntity> BigStoryFocusItem_GetByBigStoryId(int id)
        {
            const string commandText = "CMS_BigStoryItemFocus_GetByBigStoryId";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "BigStoryId", id);
                var data = _db.GetList<BigStoryFocusItemEntity>(cmd);
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public BigStoryFocusItemEntity BigStoryFocusItem_GetById(int id)
        {
            const string commandText = "CMS_BigStoryItemFocus_GetById";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", id);
                var data = _db.Get<BigStoryFocusItemEntity>(cmd);
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        #region Core members

        private readonly CmsMainDb _db;

        protected BigStoryFocusItemDalBase(CmsMainDb db)
        {
            _db = db;
        }

        protected CmsMainDb Database
        {
            get { return _db; }
        }

        #endregion
    }
}
