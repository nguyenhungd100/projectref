﻿using ChannelVN.BigStory.Entity;
using ChannelVN.BigStory.MainDal.Databases;
using ChannelVN.CMS.Common;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChannelVN.BigStory.MainDal
{
    public abstract class BigStoryDalBase
    {
        public bool Insert(BigStoryEntity BigStory, ref int BigStoryId)
        {
            const string commandText = "CMS_BigStory_Insert";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id",SqlDbType.Int, ParameterDirection.Output);
                _db.AddParameter(cmd, "Title", BigStory.Title);
                _db.AddParameter(cmd, "IsFocus", BigStory.IsFocus);
                _db.AddParameter(cmd, "Avatar", BigStory.Avatar);
                _db.AddParameter(cmd, "Description", BigStory.Description);
                _db.AddParameter(cmd, "CreatedBy", BigStory.CreatedBy);
                _db.AddParameter(cmd, "Status", BigStory.Status);
                var numberOfRow = _db.ExecuteNonQuery(cmd);
                BigStoryId = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 0));
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public bool Update(BigStoryEntity BigStory)
        {
            const string commandText = "CMS_BigStory_Update";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Title", BigStory.Title);
                _db.AddParameter(cmd, "IsFocus", BigStory.IsFocus);
                _db.AddParameter(cmd, "CreatedBy", BigStory.CreatedBy);
                _db.AddParameter(cmd, "LastModifiedDate", BigStory.LastModifiedDate);
                _db.AddParameter(cmd, "LastModifiedBy", BigStory.LastModifiedBy);
                _db.AddParameter(cmd, "Avatar", BigStory.Avatar);
                _db.AddParameter(cmd, "Description", BigStory.Description);
                _db.AddParameter(cmd, "Status", BigStory.Status);
                _db.AddParameter(cmd, "Id", BigStory.Id);
                var numberOfRow = _db.ExecuteNonQuery(cmd);
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public bool Delete(int BigStoryId)
        {
            const string commandText = "CMS_BigStory_Delete";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", BigStoryId);
                var numberOfRow = _db.ExecuteNonQuery(cmd);
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public bool BigStory_Update_Status(int BigStoryId, int status)
        {
            const string commandText = "CMS_BigStory_Update_Status";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", BigStoryId);
                _db.AddParameter(cmd, "status", status);
                var numberOfRow = _db.ExecuteNonQuery(cmd);
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public bool BigStory_Update_IsFocus(int BigStoryId, bool IsFocus)
        {
            const string commandText = "CMS_BigStory_Update_IsFocus";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", BigStoryId);
                _db.AddParameter(cmd, "IsFocus", IsFocus);
                var numberOfRow = _db.ExecuteNonQuery(cmd);
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public BigStoryEntity GetBigStoryById(int id)
        {
            const string commandText = "CMS_BigStory_GetbyId";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", id);
                var numberOfRow = _db.Get<BigStoryEntity>(cmd);
                return numberOfRow;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public List<BigStoryEntity> Search(string keyword, string CreatedBy, int IsFocus, int status, DateTime fromDate, DateTime toDate, int pageIndex, int pageSize, ref int totalRow)
        {
            const string commandText = "CMS_BigStory_Search";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "TotalRow", totalRow, ParameterDirection.Output);
                _db.AddParameter(cmd, "Keyword", keyword);
                _db.AddParameter(cmd, "Status", status);
                _db.AddParameter(cmd, "IsFocus", IsFocus);
                _db.AddParameter(cmd, "CreatedBy", CreatedBy);
                _db.AddParameter(cmd, "FromDate", fromDate);
                _db.AddParameter(cmd, "ToDate", toDate);
                _db.AddParameter(cmd, "PageIndex", pageIndex);
                _db.AddParameter(cmd, "PageSize", pageSize);
                var numberOfRow = _db.GetList<BigStoryEntity>(cmd);
                totalRow = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 0));
                return numberOfRow;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        } 

        #region Core members

        private readonly CmsMainDb _db;

        protected BigStoryDalBase(CmsMainDb db)
        {
            _db = db;
        }

        protected CmsMainDb Database
        {
            get { return _db; }
        }

        #endregion
    }
}
