﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ChannelVN.CMS.Common;

namespace ChannelVN.InteractiveContent.Entity
{
    public class InteractiveComponentEntity : EntityBase
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public string Data { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
    }
}
