﻿using System.Runtime.Serialization;
using ChannelVN.CMS.Common;

namespace ChannelVN.InteractiveContent.Entity
{
    [DataContract]
    public class InteractiveImageEntity : EntityBase
    {
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public int FolderId { get; set; }
        [DataMember]
        public string Name { get; set; }
        [DataMember]
        public string Url { get; set; }
        [DataMember]
        public string Color { get; set; }
        [DataMember]
        public int Width { get; set; }
        [DataMember]
        public int Height { get; set; }
        [DataMember]
        public string TagName { get; set; }
        [DataMember]
        public bool IsRepeat { get; set; }
        [DataMember]
        public bool IsActive { get; set; }
    }
}
