﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ChannelVN.CMS.Common;

namespace ChannelVN.InteractiveContent.Entity
{
    public enum EnumInteractiveDetailType : int
    {
        All = -1,
        Page = 1,
        Popup = 2,
        Article = 3
    }

    public class InteractiveDetailEntity : EntityBase
    {
        public int Id { get; set; }
        public int InteractiveId { get; set; }
        public string InteractivePageId { get; set; }
        public string InteractivePageData { get; set; }
        public string InteractivePageHead { get; set; }
        public string InteractivePageBody { get; set; }
        public string Title { get; set; }
        public string Sapo { get; set; }
        public string Url { get; set; }
        public string Avatar { get; set; }
        public EnumInteractiveDetailType InteractivePageType { get; set; }
        public int Priority { get; set; }
        public DateTime LastmodifiedDate { get; set; }
        public bool IsMasterPage { get; set; }
    }
    public class InteractiveDetailForListEntity : EntityBase
    {
        public int Id { get; set; }
        public int InteractiveId { get; set; }
        public string InteractivePageId { get; set; }
        public string InteractivePageData { get; set; }
        public string InteractivePageHead { get; set; }
        public string InteractivePageBody { get; set; }
        public int InteractivePageType { get; set; }
        public int Priority { get; set; }
        public string Title { get; set; }
        public string Sapo { get; set; }
        public string Url { get; set; }
        public string Avatar { get; set; }
        public DateTime LastmodifiedDate { get; set; }
        public bool IsMasterPage { get; set; }
    }
}
