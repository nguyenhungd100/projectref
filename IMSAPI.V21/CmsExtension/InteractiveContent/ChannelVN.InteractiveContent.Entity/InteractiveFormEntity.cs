﻿using System;
using System.Runtime.Serialization;
using ChannelVN.CMS.Common;

namespace ChannelVN.InteractiveContent.Entity
{
    [DataContract]
    public class InteractiveFormEntity : EntityBase
    {
        [DataMember]
        public string Id { get; set; }
        [DataMember]
        public int InteractiveId { get; set; }       
        [DataMember]
        public string Name { get; set; }
    }
    [DataContract]
    public class InteractiveFormResultEntity : EntityBase
    {
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public int InteractiveId { get; set; }
        [DataMember]
        public DateTime CreatedDate { get; set; }
        [DataMember]
        public string FormId { get; set; }
        [DataMember]
        public string FormHtml { get; set; }
        [DataMember]
        public string FormData { get; set; }
        [DataMember]
        public int Status { get; set; }
        [DataMember]
        public string Name { get; set; }
    }
    [DataContract]
    public class InteractiveFormResultDetailEntity : EntityBase
    {
        [DataMember]
        public int FormResultId { get; set; }
        [DataMember]
        public string FormItemId { get; set; }
        [DataMember]
        public string FormItemValue { get; set; }
        [DataMember]
        public string FormItemValueId { get; set; }
        [DataMember]
        public string Label { get; set; }
        [DataMember]
        public string Type { get; set; }
    }
}
