﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ChannelVN.CMS.Common;

namespace ChannelVN.InteractiveContent.Entity
{
    public enum EnumInteractiveContentType : int
    {
        All = -1,
        SlideMedia = 1,
        InfoGraphic = 2,
        InfoGraphicInteractive = 3,
        EmbedObject = 4,
        Page = 5
    }

    public class InteractiveContentEntity : EntityBase
    {
        public int Id { get; set; }
        public int Type { get; set; }
        public string Title { get; set; }
        public string Sapo { get; set; }
        public string Avatar { get; set; }
        public string BackgroundCover { get; set; }
        public string Author { get; set; }
        public string Source { get; set; }
        public string InteractiveData { get; set; }
        public string InteractiveConfig { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public string LastModifiedBy { get; set; }
        public DateTime LastModifiedDate { get; set; }
        public int Status { get; set; }
        public int SiteId { get; set; }

        public string InteractivePublishedData { get; set; }
        public string InteractivePublishedConfig { get; set; }
    }
}
