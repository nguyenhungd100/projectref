﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ChannelVN.CMS.Common;

namespace ChannelVN.InteractiveContent.Entity
{
    public enum EnumInteractiveTemplateType : int
    {
        All = -1,
        LandingPage = 1,
        Interactive = 2
    }

    public class InteractiveTemplateEntity : EntityBase
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Preview { get; set; }
        public string Data { get; set; }
        public int Type { get; set; }
    }
}
