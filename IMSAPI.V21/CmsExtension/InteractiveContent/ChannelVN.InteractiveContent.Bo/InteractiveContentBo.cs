﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ChannelVN.InteractiveContent.Dal;
using ChannelVN.InteractiveContent.Entity;
using ChannelVN.InteractiveContent.Entity.ErrorCode;
using ChannelVN.CMS.Common;

namespace ChannelVN.InteractiveContent.Bo
{
    public class InteractiveContentBo
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="interactiveContent"></param>
        /// <returns></returns>
        public static ErrorMapping.ErrorCodes InsertInteractiveContent(InteractiveContentEntity interactiveContent)
        {
            try
            {
                interactiveContent.CreatedBy = WcfExtensions.WcfMessageHeader.Current.ClientUsername;
                return InteractiveContentDal.InsertInteractiveContent(interactiveContent)
                    ? ErrorMapping.ErrorCodes.Success
                    : ErrorMapping.ErrorCodes.BusinessError;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Fatal, ex.ToString());
                return ErrorMapping.ErrorCodes.Exception;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="interactiveContent"></param>
        /// <returns></returns>
        public static ErrorMapping.ErrorCodes UpdateInteractiveContent(InteractiveContentEntity interactiveContent)
        {
            try
            {
                interactiveContent.LastModifiedBy = WcfExtensions.WcfMessageHeader.Current.ClientUsername;
                return InteractiveContentDal.UpdateInteractiveContent(interactiveContent)
                    ? ErrorMapping.ErrorCodes.Success
                    : ErrorMapping.ErrorCodes.BusinessError;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Fatal, ex.ToString());
                return ErrorMapping.ErrorCodes.Exception;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <param name="interactiveData"></param>
        /// <param name="interactiveConfig"></param>
        /// <returns></returns>
        public static ErrorMapping.ErrorCodes UpdateInteractiveContentConfig(int id, string interactiveData, string interactiveConfig)
        {
            try
            {
                return InteractiveContentDal.UpdateInteractiveContentConfig(id, interactiveData, interactiveConfig, WcfExtensions.WcfMessageHeader.Current.ClientUsername)
                    ? ErrorMapping.ErrorCodes.Success
                    : ErrorMapping.ErrorCodes.BusinessError;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Fatal, ex.ToString());
                return ErrorMapping.ErrorCodes.Exception;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <param name="interactiveData"></param>
        /// <param name="interactiveConfig"></param>
        /// <returns></returns>
        public static ErrorMapping.ErrorCodes InteractiveContentPublish(int id, string interactiveData, string interactiveConfig)
        {
            try
            {
                return InteractiveContentDal.InteractiveContentPublish(id, interactiveData, interactiveConfig, WcfExtensions.WcfMessageHeader.Current.ClientUsername)
                    ? ErrorMapping.ErrorCodes.Success
                    : ErrorMapping.ErrorCodes.BusinessError;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Fatal, ex.ToString());
                return ErrorMapping.ErrorCodes.Exception;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public static InteractiveContentEntity GetInteractiveContentById(int id)
        {
            try
            {
                return InteractiveContentDal.GetInteractiveContentById(id);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Fatal, ex.ToString());
                return null;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="keyword"></param>
        /// <param name="type"></param>
        /// <param name="createdBy"></param>
        /// <param name="status"></param>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <param name="totalRow"></param>
        /// <returns></returns>
        public static List<InteractiveContentEntity> SearchInteractiveContent(string keyword, EnumInteractiveContentType type, string createdBy, int status, int siteId, int pageIndex, int pageSize, ref int totalRow)
        {
            try
            {
                return InteractiveContentDal.SearchInteractiveContent(keyword, (int)type, createdBy, status, siteId, pageIndex, pageSize, ref totalRow);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Fatal, ex.ToString());
                return new List<InteractiveContentEntity>();
            }
        }

        #region Component
        public static ErrorMapping.ErrorCodes InsertInteractiveComponent(InteractiveComponentEntity interactiveContent)
        {
            try
            {
                interactiveContent.CreatedBy = WcfExtensions.WcfMessageHeader.Current.ClientUsername;
                return InteractiveComponentDal.InsertInteractiveComponent(interactiveContent)
                    ? ErrorMapping.ErrorCodes.Success
                    : ErrorMapping.ErrorCodes.BusinessError;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Fatal, ex.ToString());
                return ErrorMapping.ErrorCodes.Exception;
            }
        }
        public static ErrorMapping.ErrorCodes UpdateInteractiveComponent(InteractiveComponentEntity interactiveContent)
        {
            try
            {
                return InteractiveComponentDal.UpdateInteractiveComponent(interactiveContent)
                    ? ErrorMapping.ErrorCodes.Success
                    : ErrorMapping.ErrorCodes.BusinessError;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Fatal, ex.ToString());
                return ErrorMapping.ErrorCodes.Exception;
            }
        }
        public static ErrorMapping.ErrorCodes DeleteInteractiveComponent(long id)
        {
            try
            {
                return InteractiveComponentDal.DeleteInteractiveComponent(id)
                    ? ErrorMapping.ErrorCodes.Success
                    : ErrorMapping.ErrorCodes.BusinessError;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Fatal, ex.ToString());
                return ErrorMapping.ErrorCodes.Exception;
            }
        }
        public static InteractiveComponentEntity GetInteractiveComponentById(int id)
        {
            try
            {
                return InteractiveComponentDal.GetInteractiveComponentById(id);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Fatal, ex.ToString());
                return null;
            }
        }
        public static List<InteractiveComponentEntity> SearchInteractiveComponent(string keyword, string createdBy, int pageIndex, int pageSize, ref int totalRow)
        {
            try
            {
                return InteractiveComponentDal.SearchInteractiveComponent(keyword, createdBy, pageIndex, pageSize, ref totalRow);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Fatal, ex.ToString());
                return new List<InteractiveComponentEntity>();
            }
        }

        #endregion

        #region Template

        public static ErrorMapping.ErrorCodes UpdateInteractiveTemplate(InteractiveTemplateEntity interactiveContent)
        {
            try
            {
                return InteractiveTemplateDal.UpdateInteractiveTemplate(interactiveContent)
                    ? ErrorMapping.ErrorCodes.Success
                    : ErrorMapping.ErrorCodes.BusinessError;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Fatal, ex.ToString());
                return ErrorMapping.ErrorCodes.Exception;
            }
        }
        public static ErrorMapping.ErrorCodes DeleteInteractiveTemplate(long id)
        {
            try
            {
                return InteractiveTemplateDal.DeleteInteractiveTemplate(id)
                    ? ErrorMapping.ErrorCodes.Success
                    : ErrorMapping.ErrorCodes.BusinessError;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Fatal, ex.ToString());
                return ErrorMapping.ErrorCodes.Exception;
            }
        }
        public static InteractiveTemplateEntity GetInteractiveTemplateById(int id)
        {
            try
            {
                return InteractiveTemplateDal.GetInteractiveTemplateById(id);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Fatal, ex.ToString());
                return null;
            }
        }
        public static List<InteractiveTemplateEntity> SearchInteractiveTemplate(EnumInteractiveTemplateType type)
        {
            try
            {
                return InteractiveTemplateDal.SearchInteractiveTemplate(type);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Fatal, ex.ToString());
                return new List<InteractiveTemplateEntity>();
            }
        }

        #endregion
    }
}
