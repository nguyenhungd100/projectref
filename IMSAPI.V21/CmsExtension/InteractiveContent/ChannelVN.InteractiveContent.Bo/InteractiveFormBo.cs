﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ChannelVN.InteractiveContent.Dal;
using ChannelVN.InteractiveContent.Entity;
using ChannelVN.InteractiveContent.Entity.ErrorCode;
using ChannelVN.CMS.Common;

namespace ChannelVN.InteractiveContent.Bo
{
    public class InteractiveFormBo
    {
        #region InsertFormResult
        public static ErrorMapping.ErrorCodes UpdateForm(InteractiveFormEntity elm)
        {
            return InteractiveFormDal.UpdateForm(elm) ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.UnknowError;
        }
        public static List<InteractiveFormEntity> GetFormByInteractiveId(int interactiveId)
        {
            return InteractiveFormDal.GetFormByInteractiveId(interactiveId);
        }
        public static List<InteractiveFormResultEntity> GetFormResultByFormResultId(string formResultId)
        {
            return InteractiveFormDal.GetFormResultByFormResultId(formResultId);
        }
        public static ErrorMapping.ErrorCodes InsertFormResult(InteractiveFormResultEntity elm, ref int id)
        {
            return InteractiveFormDal.InsertFormResult(elm, ref id) ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.UnknowError;
        }
        public static ErrorMapping.ErrorCodes DeleteFormResult(int id)
        {
            return InteractiveFormDal.DeleteFromResult(id) ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.UnknowError;
        }
        #endregion

        #region FormResultDetail
        public static ErrorMapping.ErrorCodes InsertFormResultDetail(InteractiveFormResultDetailEntity elm)
        {
            return InteractiveFormDal.InsertFormResultDetail(elm) ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.UnknowError;
        }
        public static List<InteractiveFormResultDetailEntity> GetFormResultDetailByFormResultId(int formResultId)
        {
            return InteractiveFormDal.GetFormResultDetailByFormResultId(formResultId);
        }
        #endregion
    }
}
