﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ChannelVN.InteractiveContent.Dal;
using ChannelVN.InteractiveContent.Entity;
using ChannelVN.InteractiveContent.Entity.ErrorCode;
using ChannelVN.CMS.Common;

namespace ChannelVN.InteractiveContent.Bo
{
    public class InteractiveImageBo
    {

        #region InteractiveImage Folder
        public static List<InteractiveImageFolderEntity> ListInteractiveImageFolder()
        {
            return InteractiveImageDal.ListFolder();
        }
       
        #endregion

        #region InteractiveImage 
        public static List<InteractiveImageEntity> ListInteractiveImage(string keyword, int folderId, int pageIndex, int pageSize, ref int totalRow)
        {
            return InteractiveImageDal.ListImage(keyword, folderId, pageIndex, pageSize, ref totalRow);
        }
        public static InteractiveImageEntity SelectInteractiveImage(int id)
        {
            return InteractiveImageDal.SelectImage(id);
        }
        public static ErrorMapping.ErrorCodes InsertInteractiveImage(InteractiveImageEntity elm, ref int id)
        {
            return InteractiveImageDal.InsertImage(elm, ref id) ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.UnknowError;
        }
        public static ErrorMapping.ErrorCodes UpdateInteractiveImage(InteractiveImageEntity elm)
        {
            return InteractiveImageDal.UpdateImage(elm) ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.UnknowError;
        }
        public static ErrorMapping.ErrorCodes DeleteInteractiveImage(int id)
        {
            return InteractiveImageDal.DeleteImage(id) ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.UnknowError;
        }
        #endregion
    }
}
