﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ChannelVN.InteractiveContent.Dal;
using ChannelVN.InteractiveContent.Entity;
using ChannelVN.InteractiveContent.Entity.ErrorCode;
using ChannelVN.CMS.Common;

namespace ChannelVN.InteractiveContent.Bo
{
    public class InteractiveDetailBo
    {

        public static ErrorMapping.ErrorCodes UpdateInteractiveDetail(InteractiveDetailEntity interactiveDetail)
        {
            try
            {
                return InteractiveDetailDal.UpdateInteractiveDetail(interactiveDetail)
                    ? ErrorMapping.ErrorCodes.Success
                    : ErrorMapping.ErrorCodes.BusinessError;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Fatal, ex.ToString());
                return ErrorMapping.ErrorCodes.Exception;
            }
        }
        public static List<InteractiveDetailForListEntity> GetByInteractiveId(int interactiveId)
        {
            try
            {
                return InteractiveDetailDal.GetByInteractiveId(interactiveId);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Fatal, ex.ToString());
                return new List<InteractiveDetailForListEntity>();
            }
        }
        public static InteractiveDetailForListEntity GetByPageId(string pageId)
        {
            try
            {
                return InteractiveDetailDal.GetByPageId(pageId);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Fatal, ex.ToString());
                return new InteractiveDetailForListEntity();
            }
        }
        public static ErrorMapping.ErrorCodes DeleteByInteractiveId(int interactiveId)
        {
            try
            {
                return InteractiveDetailDal.DeleteByInteractiveId(interactiveId)
                    ? ErrorMapping.ErrorCodes.Success
                    : ErrorMapping.ErrorCodes.BusinessError;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Fatal, ex.ToString());
                return ErrorMapping.ErrorCodes.Exception;
            }
        }
    }
}
