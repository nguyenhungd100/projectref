﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ChannelVN.InteractiveContent.Entity;
using ChannelVN.InteractiveContent.MainDal.Databases;

namespace ChannelVN.InteractiveContent.Dal
{
    public class InteractiveDetailDal
    {
        public static bool UpdateInteractiveDetail(InteractiveDetailEntity interactiveDetail)
        {
            bool retVal = false;
            using (var db = new InteractiveDb())
            {
                retVal = db.InteractiveDetailDal.Update(interactiveDetail);
            }
            return retVal;
        }
        public static List<InteractiveDetailForListEntity> GetByInteractiveId(int interactiveId)
        {
            List<InteractiveDetailForListEntity> retVal = new List<InteractiveDetailForListEntity>();
            using (var db = new InteractiveDb())
            {
                retVal = db.InteractiveDetailDal.GetByInteractiveId(interactiveId);
            }
            return retVal;
        }
        public static InteractiveDetailForListEntity GetByPageId(string pageId)
        {
            InteractiveDetailForListEntity retVal = new InteractiveDetailForListEntity();
            using (var db = new InteractiveDb())
            {
                retVal = db.InteractiveDetailDal.GetByPageId(pageId);
            }
            return retVal;
        }
        public static bool DeleteByInteractiveId(int interactiveId)
        {
            bool retVal = false;
            using (var db = new InteractiveDb())
            {
                retVal = db.InteractiveDetailDal.DeleteByInteractiveId(interactiveId);
            }
            return retVal;
        }
    }
}
