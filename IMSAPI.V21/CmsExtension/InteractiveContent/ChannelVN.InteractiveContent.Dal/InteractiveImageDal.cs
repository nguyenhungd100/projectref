﻿using System;
using System.Collections.Generic;
using ChannelVN.InteractiveContent.Entity;
using ChannelVN.InteractiveContent.MainDal.Databases;
namespace ChannelVN.InteractiveContent.Dal
{
    public class InteractiveImageDal
    {

        #region Interactive Image Folder
        public static List<InteractiveImageFolderEntity> ListFolder()
        {

            List<InteractiveImageFolderEntity> retVal = null;
            using (var db = new InteractiveDb())
            {
                retVal = db.InteractiveImageDal.ListFolder();
            }
            return retVal;
        }
        #endregion

        #region Interactive Image
        public static List<InteractiveImageEntity> ListImage(string keyword, int folderId, int pageIndex, int pageSize, ref int totalRow)
        {
            List<InteractiveImageEntity> retVal = null;
            using (var db = new InteractiveDb())
            {
                retVal = db.InteractiveImageDal.ListImage(keyword, folderId, pageIndex, pageSize, ref totalRow);
            }
            return retVal;

        }
        public static InteractiveImageEntity SelectImage(int id)
        {
            InteractiveImageEntity retVal = null;
            using (var db = new InteractiveDb())
            {
                retVal = db.InteractiveImageDal.SelectImage(id);
            }
            return retVal;
        }
        public static bool InsertImage(InteractiveImageEntity elm, ref int id)
        {
            bool retVal = false;
            using (var db = new InteractiveDb())
            {
                retVal = db.InteractiveImageDal.InsertImage(elm, ref id);
            }
            return retVal;

        }
        public static bool UpdateImage(InteractiveImageEntity elm)
        {
            bool retVal = false;
            using (var db = new InteractiveDb())
            {
                retVal = db.InteractiveImageDal.UpdateImage(elm);
            }
            return retVal;
        }
        public static bool DeleteImage(int id)
        {
            bool retVal = false;
            using (var db = new InteractiveDb())
            {
                retVal = db.InteractiveImageDal.DeleteImage(id);
            }
            return retVal;
        }
        #endregion
    }
}
