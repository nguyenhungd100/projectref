﻿using System;
using System.Collections.Generic;
using ChannelVN.InteractiveContent.Entity;
using ChannelVN.InteractiveContent.MainDal.Databases;
namespace ChannelVN.InteractiveContent.Dal
{
    public class InteractiveFormDal
    {
        #region Interactive Image 
        public static bool UpdateForm(InteractiveFormEntity elm)
        {
            bool retVal = false;
            using (var db = new InteractiveDb())
            {
                retVal = db.InteractiveFormDal.UpdateForm(elm);
            }
            return retVal;

        }
        public static List<InteractiveFormEntity> GetFormByInteractiveId(int interactiveId)
        {
            List<InteractiveFormEntity> retVal = null;
            using (var db = new InteractiveDb())
            {
                retVal = db.InteractiveFormDal.GetFormByInteractiveId(interactiveId);
            }
            return retVal;

        }
        public static List<InteractiveFormResultEntity> GetFormResultByFormResultId(string formResultId)
        {
            List<InteractiveFormResultEntity> retVal = null;
            using (var db = new InteractiveDb())
            {
                retVal = db.InteractiveFormDal.GetFormResultByFormResultId(formResultId);
            }
            return retVal;

        }
        public static bool InsertFormResult(InteractiveFormResultEntity elm, ref int id)
        {
            bool retVal = false;
            using (var db = new InteractiveDb())
            {
                retVal = db.InteractiveFormDal.InsertFormResult(elm, ref id);
            }
            return retVal;

        }
        public static bool DeleteFromResult(int id)
        {
            bool retVal = false;
            using (var db = new InteractiveDb())
            {
                retVal = db.InteractiveFormDal.DeleteFromResult(id);
            }
            return retVal;
        }
        #endregion

        #region FormResultDetail

        public static bool InsertFormResultDetail(InteractiveFormResultDetailEntity elm)
        {
            bool retVal = false;
            using (var db = new InteractiveDb())
            {
                retVal = db.InteractiveFormDal.InsertFormResultDetail(elm);
            }
            return retVal;

        }

        public static List<InteractiveFormResultDetailEntity> GetFormResultDetailByFormResultId(int formResultId)
        {
            List<InteractiveFormResultDetailEntity> retVal = null;
            using (var db = new InteractiveDb())
            {
                retVal = db.InteractiveFormDal.GetFormResultDetailByFormResultId(formResultId);
            }
            return retVal;

        }

        #endregion
    }
}
