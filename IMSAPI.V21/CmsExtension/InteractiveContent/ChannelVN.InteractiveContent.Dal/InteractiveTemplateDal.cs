﻿using System;
using System.Collections.Generic;
using ChannelVN.InteractiveContent.Entity;
using ChannelVN.InteractiveContent.MainDal.Databases;
namespace ChannelVN.InteractiveContent.Dal
{
    public class InteractiveTemplateDal
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="interactiveContent"></param>
        /// <returns></returns>
        public static bool UpdateInteractiveTemplate(InteractiveTemplateEntity it)
        {
            bool retVal = false;
            using (var db = new InteractiveDb())
            {
                retVal = db.InteractiveTemplateDal.Update(it);
            }
            return retVal;
        }
       
        public static bool DeleteInteractiveTemplate(long id)
        {
            bool retVal = false;
            using (var db = new InteractiveDb())
            {
                retVal = db.InteractiveTemplateDal.Delete(id);
            }
            return retVal;
        }

        public static InteractiveTemplateEntity GetInteractiveTemplateById(int id)
        {
            InteractiveTemplateEntity retVal = null;
            using (var db = new InteractiveDb())
            {
                retVal = db.InteractiveTemplateDal.GetById(id);
            }
            return retVal;
        }
        public static List<InteractiveTemplateEntity> SearchInteractiveTemplate(EnumInteractiveTemplateType type)
        {
            List<InteractiveTemplateEntity> retVal = new List<InteractiveTemplateEntity>();
            using (var db = new InteractiveDb())
            {
                retVal = db.InteractiveTemplateDal.Search(type);
            }
            return retVal;
        }
    }
}
