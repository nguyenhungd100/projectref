﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ChannelVN.InteractiveContent.Entity;
using ChannelVN.InteractiveContent.MainDal.Databases;

namespace ChannelVN.InteractiveContent.Dal
{
    public class InteractiveComponentDal
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="interactiveContent"></param>
        /// <returns></returns>
        public static bool InsertInteractiveComponent(InteractiveComponentEntity interactiveContent)
        {
            bool retVal = false;
            using (var db = new InteractiveDb())
            {
                retVal = db.InteractiveComponentDal.Insert(interactiveContent);
            }
            return retVal;
        }
     
        public static bool UpdateInteractiveComponent(InteractiveComponentEntity interactiveContent)
        {
            bool retVal = false;
            using (var db = new InteractiveDb())
            {
                retVal = db.InteractiveComponentDal.Update(interactiveContent);
            }
            return retVal;
        }public static bool DeleteInteractiveComponent(long id)
        {
            bool retVal = false;
            using (var db = new InteractiveDb())
            {
                retVal = db.InteractiveComponentDal.Delete(id);
            }
            return retVal;
        }

        public static InteractiveComponentEntity GetInteractiveComponentById(int id)
        {
            InteractiveComponentEntity retVal = null;
            using (var db = new InteractiveDb())
            {
                retVal = db.InteractiveComponentDal.GetById(id);
            }
            return retVal;
        }
        public static List<InteractiveComponentEntity> SearchInteractiveComponent(string keyword, string createdBy, int pageIndex, int pageSize, ref int totalRow)
        {
            List<InteractiveComponentEntity> retVal = new List<InteractiveComponentEntity>();
            using (var db = new InteractiveDb())
            {
                retVal = db.InteractiveComponentDal.Search(keyword, createdBy, pageIndex, pageSize, ref totalRow);
            }
            return retVal;
        }
    }
}
