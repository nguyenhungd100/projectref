﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ChannelVN.InteractiveContent.Entity;
using ChannelVN.InteractiveContent.MainDal.Databases;

namespace ChannelVN.InteractiveContent.Dal
{
    public class InteractiveContentDal
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="interactiveContent"></param>
        /// <returns></returns>
        public static bool InsertInteractiveContent(InteractiveContentEntity interactiveContent)
        {
            bool retVal = false;
            using (var db = new InteractiveDb())
            {
                retVal = db.InteractiveContentDal.Insert(interactiveContent);
            }
            return retVal;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="interactiveContent"></param>
        /// <returns></returns>
        public static bool UpdateInteractiveContent(InteractiveContentEntity interactiveContent)
        {
            bool retVal = false;
            using (var db = new InteractiveDb())
            {
                retVal = db.InteractiveContentDal.Update(interactiveContent);
            }
            return retVal;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="interactiveContent"></param>
        /// <returns></returns>
        public static bool UpdateInteractiveContentConfig(int id, string interactiveData, string interactiveConfig, string lastModifiedBy)
        {
            bool retVal = false;
            using (var db = new InteractiveDb())
            {
                retVal = db.InteractiveContentDal.UpdateConfig(id, interactiveData, interactiveConfig, lastModifiedBy);
            }
            return retVal;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <param name="interactiveData"></param>
        /// <param name="interactiveConfig"></param>
        /// <param name="lastModifiedBy"></param>
        /// <returns></returns>
        public static bool InteractiveContentPublish(int id, string interactiveData, string interactiveConfig, string lastModifiedBy)
        {
            bool retVal = false;
            using (var db = new InteractiveDb())
            {
                retVal = db.InteractiveContentDal.Publish(id, interactiveData, interactiveConfig, lastModifiedBy);
            }
            return retVal;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public static InteractiveContentEntity GetInteractiveContentById(int id)
        {
            InteractiveContentEntity retVal = null;
            using (var db = new InteractiveDb())
            {
                retVal = db.InteractiveContentDal.GetById(id);
            }
            return retVal;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="keyword"></param>
        /// <param name="type"></param>
        /// <param name="createdBy"></param>
        /// <param name="status"></param>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <param name="totalRow"></param>
        /// <returns></returns>
        public static List<InteractiveContentEntity> SearchInteractiveContent(string keyword, int type, string createdBy, int status, int siteId, int pageIndex, int pageSize, ref int totalRow)
        {
            List<InteractiveContentEntity> retVal = new List<InteractiveContentEntity>();
            using (var db = new InteractiveDb())
            {
                retVal = db.InteractiveContentDal.Search(keyword, type, createdBy, status, siteId, pageIndex, pageSize, ref totalRow);
            }
            return retVal;
        }
    }
}
