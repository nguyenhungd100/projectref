﻿using ChannelVN.CMS.Common;
using ChannelVN.CMS.MainDal.Databases;
using ChannelVN.InteractiveContent.Entity;
using ChannelVN.InteractiveContent.MainDal.Databases;
using System;
using System.Collections.Generic;

namespace ChannelVN.InteractiveContent.MainDal
{
    public abstract class InteractiveContentDalBase
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="interactiveContent"></param>
        /// <returns></returns>
        public bool Insert(InteractiveContentEntity interactiveContent)
        {
            const string storeProcedure = "CMS_InteractiveContent_Insert";

            try
            {

                var cmd = _db.CreateCommand(storeProcedure, true);
                _db.AddParameter(cmd, "Type", interactiveContent.Type);
                _db.AddParameter(cmd, "Title", interactiveContent.Title);
                _db.AddParameter(cmd, "Sapo", interactiveContent.Sapo);
                _db.AddParameter(cmd, "Avatar", interactiveContent.Avatar);
                _db.AddParameter(cmd, "BackgroundCover", interactiveContent.BackgroundCover);
                _db.AddParameter(cmd, "Author", interactiveContent.Author);
                _db.AddParameter(cmd, "Source", interactiveContent.Source);
                _db.AddParameter(cmd, "InteractiveData", interactiveContent.InteractiveData);
                _db.AddParameter(cmd, "InteractiveConfig", interactiveContent.InteractiveConfig);
                _db.AddParameter(cmd, "CreatedBy", interactiveContent.CreatedBy);
                _db.AddParameter(cmd, "Status", interactiveContent.Status);
                _db.AddParameter(cmd, "SiteId", interactiveContent.SiteId);

                int numberOfRow = cmd.ExecuteNonQuery();

                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", storeProcedure, ex.Message));
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="interactiveContent"></param>
        /// <returns></returns>
        public bool Update(InteractiveContentEntity interactiveContent)
        {
            const string storeProcedure = "CMS_InteractiveContent_Update";

            try
            {

                var cmd = _db.CreateCommand(storeProcedure, true);
                _db.AddParameter(cmd, "Id", interactiveContent.Id);
                _db.AddParameter(cmd, "Type", interactiveContent.Type);
                _db.AddParameter(cmd, "Title", interactiveContent.Title);
                _db.AddParameter(cmd, "Sapo", interactiveContent.Sapo);
                _db.AddParameter(cmd, "Avatar", interactiveContent.Avatar);
                _db.AddParameter(cmd, "BackgroundCover", interactiveContent.BackgroundCover);
                _db.AddParameter(cmd, "Author", interactiveContent.Author);
                _db.AddParameter(cmd, "Source", interactiveContent.Source);
                _db.AddParameter(cmd, "InteractiveData", interactiveContent.InteractiveData);
                _db.AddParameter(cmd, "InteractiveConfig", interactiveContent.InteractiveConfig);
                _db.AddParameter(cmd, "LastModifiedBy", interactiveContent.LastModifiedBy);
                _db.AddParameter(cmd, "Status", interactiveContent.Status);
                _db.AddParameter(cmd, "SiteId", interactiveContent.SiteId);

                int numberOfRow = cmd.ExecuteNonQuery();

                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", storeProcedure, ex.Message));
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <param name="interactiveData"></param>
        /// <param name="interactiveConfig"></param>
        /// <param name="lastModifiedBy"></param>
        /// <returns></returns>
        public bool UpdateConfig(int id, string interactiveData, string interactiveConfig, string lastModifiedBy)
        {
            const string storeProcedure = "CMS_InteractiveContent_UpdateConfig";

            try
            {

                var cmd = _db.CreateCommand(storeProcedure, true);
                _db.AddParameter(cmd, "Id", id);
                _db.AddParameter(cmd, "InteractiveData", interactiveData);
                _db.AddParameter(cmd, "InteractiveConfig", interactiveConfig);
                _db.AddParameter(cmd, "LastModifiedBy", lastModifiedBy);
                int numberOfRow = cmd.ExecuteNonQuery();

                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", storeProcedure, ex.Message));
            }
        }
        public bool Publish(int id, string interactiveData, string interactiveConfig, string lastModifiedBy)
        {
            const string storeProcedure = "CMS_InteractiveContent_Publish";

            try
            {

                var cmd = _db.CreateCommand(storeProcedure, true);
                _db.AddParameter(cmd, "Id", id);
                _db.AddParameter(cmd, "InteractiveData", interactiveData);
                _db.AddParameter(cmd, "InteractiveConfig", interactiveConfig);
                _db.AddParameter(cmd, "LastModifiedBy", lastModifiedBy);
                int numberOfRow = cmd.ExecuteNonQuery();

                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", storeProcedure, ex.Message));
            }
        }
        public InteractiveContentEntity GetById(long id)
        {
            const string storeProcedure = "CMS_InteractiveContent_GetByid";
            try
            {
                InteractiveContentEntity data = null;
                var cmd = _db.CreateCommand(storeProcedure, true);
                _db.AddParameter(cmd, "Id", id);
                data = _db.Get<InteractiveContentEntity>(cmd);
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", storeProcedure, ex.Message));
            }
        }
        public List<InteractiveContentEntity> Search(string keyword, int type, string createdBy, int status, int siteId, int pageIndex, int pageSize, ref int totalRow)
        {
            const string storeProcedure = "CMS_InteractiveContent_Search";
            try
            {
                List<InteractiveContentEntity> data = null;
                var cmd = _db.CreateCommand(storeProcedure, true);
                _db.AddParameter(cmd, "TotalRow", totalRow, System.Data.ParameterDirection.Output);
                _db.AddParameter(cmd, "Keyword", keyword);
                _db.AddParameter(cmd, "Type", type);
                _db.AddParameter(cmd, "CreatedBy", createdBy);
                _db.AddParameter(cmd, "Status", status);
                _db.AddParameter(cmd, "SiteId", siteId);
                _db.AddParameter(cmd, "PageIndex", pageIndex);
                _db.AddParameter(cmd, "PageSize", pageSize);

                data = _db.GetList<InteractiveContentEntity>(cmd);
                totalRow = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 0));
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format(storeProcedure + ":{0}", ex.Message));
            }
        }

        /// <summary>
        /// Chuyển đổi DAL cho InteractiveContentDalBase
        /// minhduongvan
        /// 2014/01/12
        /// </summary>

        #region Constructor

        private readonly InteractiveDb _db;

        protected InteractiveContentDalBase(InteractiveDb db)
        {
            _db = db;
        }

        protected InteractiveDb Database
        {
            get { return _db; }
        }

        #endregion
    }
}
