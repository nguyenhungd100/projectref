﻿using ChannelVN.CMS.Common;
using ChannelVN.CMS.MainDal.Databases;
using ChannelVN.InteractiveContent.Entity;
using ChannelVN.InteractiveContent.MainDal.Databases;
using System;
using System.Collections.Generic;

namespace ChannelVN.InteractiveContent.MainDal
{
    public abstract class InteractiveDetailDalBase
    {
        public bool Update(InteractiveDetailEntity interactiveDetail)
        {
            const string storeProcedure = "CMS_InteractiveDetail_Update";

            try
            {
                var cmd = _db.CreateCommand(storeProcedure, true);
                _db.AddParameter(cmd, "Id", interactiveDetail.Id);
                _db.AddParameter(cmd, "InteractivePageType", (int)interactiveDetail.InteractivePageType);
                _db.AddParameter(cmd, "InteractiveId", interactiveDetail.InteractiveId);
                _db.AddParameter(cmd, "Priority", interactiveDetail.Priority);
                _db.AddParameter(cmd, "InteractivePageId", interactiveDetail.InteractivePageId);
                _db.AddParameter(cmd, "InteractivePageData", interactiveDetail.InteractivePageData);
                _db.AddParameter(cmd, "InteractivePageHead", interactiveDetail.InteractivePageHead);
                _db.AddParameter(cmd, "InteractivePageBody", interactiveDetail.InteractivePageBody);
                _db.AddParameter(cmd, "Title", interactiveDetail.Title);
                _db.AddParameter(cmd, "Sapo", interactiveDetail.Sapo);
                _db.AddParameter(cmd, "Avatar", interactiveDetail.Avatar);
                _db.AddParameter(cmd, "Url", interactiveDetail.Url);
                _db.AddParameter(cmd, "IsMasterPage", interactiveDetail.IsMasterPage);
                int numberOfRow = cmd.ExecuteNonQuery();

                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", storeProcedure, ex.Message));
            }
        }

        public List<InteractiveDetailForListEntity> GetByInteractiveId(int interactiveId)
        {
            const string storeProcedure = "CMS_InteractiveDetail_GetByInteractiveId";
            try
            {
                List<InteractiveDetailForListEntity> data = null;
                var cmd = _db.CreateCommand(storeProcedure, true);
                _db.AddParameter(cmd, "InteractiveId", interactiveId);

                data = _db.GetList<InteractiveDetailForListEntity>(cmd);
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format(storeProcedure + ":{0}", ex.Message));
            }
        }

        public InteractiveDetailForListEntity GetByPageId(string pageId)
        {
            const string storeProcedure = "CMS_InteractiveDetail_GetByPageId";
            try
            {
                InteractiveDetailForListEntity data = null;
                var cmd = _db.CreateCommand(storeProcedure, true);
                _db.AddParameter(cmd, "PageId", pageId);

                data = _db.Get<InteractiveDetailForListEntity>(cmd);
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format(storeProcedure + ":{0}", ex.Message));
            }
        }

        public bool DeleteByInteractiveId(int interactiveId)
        {
            const string storeProcedure = "CMS_InteractiveDetail_DeleteByInteractiveId";

            try
            {
                var cmd = _db.CreateCommand(storeProcedure, true);
                _db.AddParameter(cmd, "InteractiveId", interactiveId);
                int numberOfRow = cmd.ExecuteNonQuery();

                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", storeProcedure, ex.Message));
            }
        }

        #region Constructor

        private readonly InteractiveDb _db;

        protected InteractiveDetailDalBase(InteractiveDb db)
        {
            _db = db;
        }

        protected InteractiveDb Database
        {
            get { return _db; }
        }

        #endregion
    }
}
