﻿using ChannelVN.CMS.Common;
using ChannelVN.CMS.MainDal.Databases;
using ChannelVN.InteractiveContent.Entity;
using ChannelVN.InteractiveContent.MainDal.Databases;
using System;
using System.Collections.Generic;

namespace ChannelVN.InteractiveContent.MainDal
{
    public abstract class InteractiveComponentDalBase
    {
        public bool Insert(InteractiveComponentEntity interactiveContent)
        {
            const string storeProcedure = "CMS_InteractiveComponent_Insert";

            try
            {
                var cmd = _db.CreateCommand(storeProcedure, true);
                _db.AddParameter(cmd, "Name", interactiveContent.Name);
                _db.AddParameter(cmd, "Data", interactiveContent.Data);
                _db.AddParameter(cmd, "CreatedBy", interactiveContent.CreatedBy);
                int numberOfRow = cmd.ExecuteNonQuery();

                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", storeProcedure, ex.Message));
            }
        }

        public bool Update(InteractiveComponentEntity interactiveContent)
        {
            const string storeProcedure = "CMS_InteractiveComponent_Update";

            try
            {

                var cmd = _db.CreateCommand(storeProcedure, true);
                _db.AddParameter(cmd, "Id", interactiveContent.Id);
                _db.AddParameter(cmd, "Name", interactiveContent.Name);
                _db.AddParameter(cmd, "Data", interactiveContent.Data);
                int numberOfRow = cmd.ExecuteNonQuery();

                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", storeProcedure, ex.Message));
            }
        }
        public bool Delete(long id)
        {
            const string storeProcedure = "CMS_InteractiveComponent_Delete";

            try
            {

                var cmd = _db.CreateCommand(storeProcedure, true);
                _db.AddParameter(cmd, "Id", id);
                int numberOfRow = cmd.ExecuteNonQuery();

                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", storeProcedure, ex.Message));
            }
        }

        public InteractiveComponentEntity GetById(long id)
        {
            const string storeProcedure = "CMS_InteractiveComponent_GetById";
            try
            {
                InteractiveComponentEntity data = null;
                var cmd = _db.CreateCommand(storeProcedure, true);
                _db.AddParameter(cmd, "Id", id);
                data = _db.Get<InteractiveComponentEntity>(cmd);
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", storeProcedure, ex.Message));
            }
        }
        public List<InteractiveComponentEntity> Search(string keyword, string createdBy, int pageIndex, int pageSize, ref int totalRow)
        {
            const string storeProcedure = "CMS_InteractiveComponent_Search";
            try
            {
                List<InteractiveComponentEntity> data = null;
                var cmd = _db.CreateCommand(storeProcedure, true);
                _db.AddParameter(cmd, "TotalRow", totalRow, System.Data.ParameterDirection.Output);
                _db.AddParameter(cmd, "Keyword", keyword);
                _db.AddParameter(cmd, "CreatedBy", createdBy);
                _db.AddParameter(cmd, "PageIndex", pageIndex);
                _db.AddParameter(cmd, "PageSize", pageSize);

                data = _db.GetList<InteractiveComponentEntity>(cmd);
                totalRow = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 0));
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format(storeProcedure + ":{0}", ex.Message));
            }
        }
        
        #region Constructor

        private readonly InteractiveDb _db;

        protected InteractiveComponentDalBase(InteractiveDb db)
        {
            _db = db;
        }

        protected InteractiveDb Database
        {
            get { return _db; }
        }

        #endregion
    }
}
