﻿using ChannelVN.CMS.Common;
using ChannelVN.CMS.MainDal.Databases;
using ChannelVN.InteractiveContent.Entity;
using ChannelVN.InteractiveContent.MainDal.Databases;
using System;
using System.Collections.Generic;

namespace ChannelVN.InteractiveContent.MainDal
{
    public abstract class InteractiveFormDalBase
    {
        #region InteractiveForm Form

        public bool UpdateForm(InteractiveFormEntity elm)
        {
            const string storeProcedure = "CMS_InteractiveForm_Update";

            try
            {
                var cmd = _db.CreateCommand(storeProcedure, true);
                _db.AddParameter(cmd, "Id", elm.Id);
                _db.AddParameter(cmd, "InteractiveId", elm.InteractiveId);
                _db.AddParameter(cmd, "Name", elm.Name);

                var numberOfRow = cmd.ExecuteNonQuery();
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", storeProcedure, ex.Message));
            }
        }


        public List<InteractiveFormEntity> GetFormByInteractiveId(int interactiveId)
        {
            const string storeProcedure = "CMS_InteractiveForm_GetByInteractiveId";
            try
            {
                List<InteractiveFormEntity> data = null;
                var cmd = _db.CreateCommand(storeProcedure, true);
                _db.AddParameter(cmd, "InteractiveId", interactiveId);
                data = _db.GetList<InteractiveFormEntity>(cmd);
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", storeProcedure, ex.Message));
            }
        }
        public List<InteractiveFormResultEntity> GetFormResultByFormResultId(string formResultId)
        {
            const string storeProcedure = "CMS_InteractiveFormResult_GetByFormResultId";
            try
            {
                List<InteractiveFormResultEntity> data = null;
                var cmd = _db.CreateCommand(storeProcedure, true);
                _db.AddParameter(cmd, "FormResultId", formResultId);
                data = _db.GetList<InteractiveFormResultEntity>(cmd);
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", storeProcedure, ex.Message));
            }
        }
        public bool InsertFormResult(InteractiveFormResultEntity elm, ref int id)
        {
            const string storeProcedure = "CMS_InteractiveFormResult_Insert";

            try
            {

                var cmd = _db.CreateCommand(storeProcedure, true);
                _db.AddParameter(cmd, "Id", elm.Id, System.Data.ParameterDirection.Output);
                _db.AddParameter(cmd, "InteractiveId", elm.InteractiveId);
                _db.AddParameter(cmd, "FormId", elm.FormId);
                _db.AddParameter(cmd, "FormHtml", elm.FormHtml);
                _db.AddParameter(cmd, "FormData", elm.FormData);
                _db.AddParameter(cmd, "Name", elm.Name);

                var numberOfRow = cmd.ExecuteNonQuery();
                id = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 0));

                return numberOfRow > 0 && id > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", storeProcedure, ex.Message));
            }
        }
        public bool DeleteFromResult(int id)
        {
            const string storeProcedure = "CMS_InteractiveFormResult_Delete";

            try
            {
                var cmd = _db.CreateCommand(storeProcedure, true);
                _db.AddParameter(cmd, "Id", id);
                var numberOfRow = cmd.ExecuteNonQuery();

                return numberOfRow > 0 && id > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", storeProcedure, ex.Message));
            }
        }
        #endregion

        #region FormResultDetail
        public bool InsertFormResultDetail(InteractiveFormResultDetailEntity elm)
        {
            const string storeProcedure = "CMS_InteractiveFormResultDetail_Insert";

            try
            {
                var cmd = _db.CreateCommand(storeProcedure, true);
                _db.AddParameter(cmd, "FormResultId", elm.FormResultId);
                _db.AddParameter(cmd, "FormItemId", elm.FormItemId);
                _db.AddParameter(cmd, "FormItemValue", elm.FormItemValue);
                _db.AddParameter(cmd, "FormItemValueId", elm.FormItemValueId);
                _db.AddParameter(cmd, "Label", elm.Label);
                _db.AddParameter(cmd, "Type", elm.Type);

                var numberOfRow = cmd.ExecuteNonQuery();

                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", storeProcedure, ex.Message));
            }
        }

        public List<InteractiveFormResultDetailEntity> GetFormResultDetailByFormResultId(int formResultId)
        {
            const string storeProcedure = "CMS_InteractiveFormResultDetail_GetByFormResultId";
            try
            {
                List<InteractiveFormResultDetailEntity> data = null;
                var cmd = _db.CreateCommand(storeProcedure, true);
                _db.AddParameter(cmd, "FormResultId", formResultId);
                data = _db.GetList<InteractiveFormResultDetailEntity>(cmd);
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", storeProcedure, ex.Message));
            }
        }
        #endregion

        #region Constructor

        private readonly InteractiveDb _db;

        protected InteractiveFormDalBase(InteractiveDb db)
        {
            _db = db;
        }

        protected InteractiveDb Database
        {
            get { return _db; }
        }

        #endregion
    }
}
