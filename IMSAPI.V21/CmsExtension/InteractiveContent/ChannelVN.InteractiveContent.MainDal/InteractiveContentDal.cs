﻿using ChannelVN.InteractiveContent.MainDal.Databases;

namespace ChannelVN.InteractiveContent.MainDal
{
    public class InteractiveContentDal : InteractiveContentDalBase
    {
        internal InteractiveContentDal(InteractiveDb db) : base(db) { }
    }
}
