﻿using ChannelVN.InteractiveContent.MainDal.Databases;

namespace ChannelVN.InteractiveContent.MainDal
{
    public class InteractiveComponentDal : InteractiveComponentDalBase
    {
        internal InteractiveComponentDal(InteractiveDb db) : base(db) { }
    }
}
