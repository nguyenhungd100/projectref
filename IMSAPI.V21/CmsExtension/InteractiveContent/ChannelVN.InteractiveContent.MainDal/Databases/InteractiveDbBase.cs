﻿using System.Data;
using ChannelVN.CMS.Common.DalCommon;

namespace ChannelVN.InteractiveContent.MainDal.Databases
{
    /// <summary>
    /// The base class for the <see cref="InteractiveDb"/> class that 
    /// represents a connection to the <c>InfoGraphDb</c> database. 
    /// </summary>
    /// <remarks>
    /// Do not change this source code. Modify the InfoGraphDb class
    /// if you need to add or change some functionality.
    /// </remarks>
    public abstract class InteractiveDbBase : MainDbBase
    {
        #region Store procedures
        private InteractiveContentDal _interactiveContentDal;
        public InteractiveContentDalBase InteractiveContentDal
        {
            get { return _interactiveContentDal ?? (_interactiveContentDal = new InteractiveContentDal((InteractiveDb)this)); }
        }
        private InteractiveComponentDal _interactiveComponentDal;
        public InteractiveComponentDalBase InteractiveComponentDal
        {
            get { return _interactiveComponentDal ?? (_interactiveComponentDal = new InteractiveComponentDal((InteractiveDb)this)); }
        }

        private InteractiveImageDal _interactiveImageDal;
        public InteractiveImageDalBase InteractiveImageDal
        {
            get { return _interactiveImageDal ?? (_interactiveImageDal = new InteractiveImageDal((InteractiveDb)this)); }
        }
        private InteractiveFormDal _interactiveFormDal;
        public InteractiveFormDalBase InteractiveFormDal
        {
            get { return _interactiveFormDal ?? (_interactiveFormDal = new InteractiveFormDal((InteractiveDb)this)); }
        }
        private InteractiveDetailDal _interactiveDetailDal;
        public InteractiveDetailDalBase InteractiveDetailDal
        {
            get { return _interactiveDetailDal ?? (_interactiveDetailDal = new InteractiveDetailDal((InteractiveDb)this)); }
        }

        private InteractiveTemplateDal _interactiveTemplateDal;
        public InteractiveTemplateDalBase InteractiveTemplateDal
        {
            get { return _interactiveTemplateDal ?? (_interactiveTemplateDal = new InteractiveTemplateDal((InteractiveDb)this)); }
        }

        #endregion

        #region Constructors

        protected InteractiveDbBase()
        {
        }
        protected InteractiveDbBase(bool init)
        {
            if (init)
            {
                InitConnection();
            }
        }

        #endregion

        #region Protected members

        protected override sealed void InitConnection()
        {
            _connection = CreateConnection();
            _connection.Open();
        }

        /// <summary>
        /// Creates a new connection to the database.
        /// </summary>
        /// <returns>A reference to the <see cref="System.Data.IDbConnection"/> object.</returns>
        protected abstract IDbConnection CreateConnection();

        #endregion
    }
}