﻿using ChannelVN.CMS.Common;
using ChannelVN.CMS.MainDal.Databases;
using ChannelVN.InteractiveContent.Entity;
using ChannelVN.InteractiveContent.MainDal.Databases;
using System;
using System.Collections.Generic;

namespace ChannelVN.InteractiveContent.MainDal
{
    public abstract class InteractiveTemplateDalBase
    {
        public bool Update(InteractiveTemplateEntity it)
        {
            const string storeProcedure = "CMS_InteractiveTemplate_Update";

            try
            {
                var cmd = _db.CreateCommand(storeProcedure, true);
                _db.AddParameter(cmd, "Id", it.Id);
                _db.AddParameter(cmd, "Name", it.Name);
                _db.AddParameter(cmd, "Data", it.Data);
                _db.AddParameter(cmd, "Type", it.Type);
                _db.AddParameter(cmd, "Preview", it.Preview);
                int numberOfRow = cmd.ExecuteNonQuery();

                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", storeProcedure, ex.Message));
            }
        }

        public bool Delete(long id)
        {
            const string storeProcedure = "CMS_InteractiveTemplate_Delete";

            try
            {

                var cmd = _db.CreateCommand(storeProcedure, true);
                _db.AddParameter(cmd, "Id", id);
                int numberOfRow = cmd.ExecuteNonQuery();

                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", storeProcedure, ex.Message));
            }
        }

        public InteractiveTemplateEntity GetById(long id)
        {
            const string storeProcedure = "CMS_InteractiveTemplate_GetById";
            try
            {
                InteractiveTemplateEntity data = null;
                var cmd = _db.CreateCommand(storeProcedure, true);
                _db.AddParameter(cmd, "Id", id);
                data = _db.Get<InteractiveTemplateEntity>(cmd);
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", storeProcedure, ex.Message));
            }
        }
        public List<InteractiveTemplateEntity> Search(EnumInteractiveTemplateType type)
        {
            const string storeProcedure = "CMS_InteractiveTemplate_Search";
            try
            {
                List<InteractiveTemplateEntity> data = null;
                var cmd = _db.CreateCommand(storeProcedure, true);
                _db.AddParameter(cmd, "Type", (int)type);

                data = _db.GetList<InteractiveTemplateEntity>(cmd);
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format(storeProcedure + ":{0}", ex.Message));
            }
        }

        #region Constructor

        private readonly InteractiveDb _db;

        protected InteractiveTemplateDalBase(InteractiveDb db)
        {
            _db = db;
        }

        protected InteractiveDb Database
        {
            get { return _db; }
        }

        #endregion
    }
}
