﻿using ChannelVN.CMS.Common;
using ChannelVN.CMS.MainDal.Databases;
using ChannelVN.InteractiveContent.Entity;
using ChannelVN.InteractiveContent.MainDal.Databases;
using System;
using System.Collections.Generic;

namespace ChannelVN.InteractiveContent.MainDal
{
    public abstract class InteractiveImageDalBase
    {
        #region InteractiveImage Folder
        public List<InteractiveImageFolderEntity> ListFolder()
        {
            const string storeProcedure = "CMS_InteractiveImageFolder_List";
            try
            {
                List<InteractiveImageFolderEntity> data = null;
                var cmd = _db.CreateCommand(storeProcedure, true);
                data = _db.GetList<InteractiveImageFolderEntity>(cmd);
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", storeProcedure, ex.Message));
            }
        }
        #endregion

        #region InteractiveImage Image
        public List<InteractiveImageEntity> ListImage(string keyword, int folderId, int pageIndex, int pageSize, ref int totalRow)
        {
            const string storeProcedure = "CMS_InteractiveImage_List";
            try
            {
                List<InteractiveImageEntity> data = null;
                var cmd = _db.CreateCommand(storeProcedure, true);
                _db.AddParameter(cmd, "TotalRow", totalRow, System.Data.ParameterDirection.Output);
                _db.AddParameter(cmd, "Keyword", keyword);
                _db.AddParameter(cmd, "FolderId", folderId);
                _db.AddParameter(cmd, "PageIndex", pageIndex);
                _db.AddParameter(cmd, "PageSize", pageSize);
                data = _db.GetList<InteractiveImageEntity>(cmd);
                totalRow = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 0));
                return data;

            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", storeProcedure, ex.Message));
            }
        }
        public InteractiveImageEntity SelectImage(int id)
        {
            const string storeProcedure = "CMS_InteractiveImage_Select";
            try
            {
                InteractiveImageEntity data = null;
                var cmd = _db.CreateCommand(storeProcedure, true);
                _db.AddParameter(cmd, "Id", id);
                data = _db.Get<InteractiveImageEntity>(cmd);
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", storeProcedure, ex.Message));
            }
        }
        public bool InsertImage(InteractiveImageEntity elm, ref int id)
        {
            const string storeProcedure = "CMS_InteractiveImage_Insert";

            try
            {

                var cmd = _db.CreateCommand(storeProcedure, true);
                _db.AddParameter(cmd, "Id", id, System.Data.ParameterDirection.Output);
                _db.AddParameter(cmd, "FolderId", elm.FolderId);
                _db.AddParameter(cmd, "Name", elm.Name);
                _db.AddParameter(cmd, "Url", elm.Url);
                _db.AddParameter(cmd, "Color", elm.Color);
                _db.AddParameter(cmd, "Width", elm.Width);
                _db.AddParameter(cmd, "Height", elm.Height);
                _db.AddParameter(cmd, "TagName", elm.TagName);
                _db.AddParameter(cmd, "IsRepeat", elm.IsRepeat);
                _db.AddParameter(cmd, "IsActive", elm.IsActive);

                var numberOfRow = cmd.ExecuteNonQuery();
                id = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 0));

                return numberOfRow > 0 && id > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", storeProcedure, ex.Message));
            }
        }
        public bool UpdateImage(InteractiveImageEntity elm)
        {
            const string storeProcedure = "CMS_InteractiveImage_Update";

            try
            {
                var cmd = _db.CreateCommand(storeProcedure, true);
                _db.AddParameter(cmd, "Id", elm.Id);
                _db.AddParameter(cmd, "FolderId", elm.FolderId);
                _db.AddParameter(cmd, "Name", elm.Name);
                _db.AddParameter(cmd, "IsRepeat", elm.IsRepeat);
                var numberOfRow = cmd.ExecuteNonQuery();
                var id = elm.Id;
                return numberOfRow > 0 && id > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", storeProcedure, ex.Message));
            }
        }
        public bool DeleteImage(int id)
        {
            const string storeProcedure = "CMS_InteractiveImage_Delete";

            try
            {
                var cmd = _db.CreateCommand(storeProcedure, true);
                _db.AddParameter(cmd, "Id", id);
                var numberOfRow = cmd.ExecuteNonQuery();

                return numberOfRow > 0 && id > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", storeProcedure, ex.Message));
            }
        }
        #endregion
        
        #region Constructor

        private readonly InteractiveDb _db;

        protected InteractiveImageDalBase(InteractiveDb db)
        {
            _db = db;
        }

        protected InteractiveDb Database
        {
            get { return _db; }
        }

        #endregion
    }
}
