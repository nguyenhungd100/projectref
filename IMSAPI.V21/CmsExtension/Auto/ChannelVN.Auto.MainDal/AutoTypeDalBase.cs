﻿using ChannelVN.Auto.Entity;
using ChannelVN.Auto.MainDal.Databases;
using ChannelVN.CMS.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChannelVN.Auto.MainDal
{
    public abstract class AutoTypeDalBase
    {
        #region Function GET

        public List<AutoTypeEntity> Search(string keyword, int pageIndex, int pageSize, ref int totalRow)
        {
            const string commandText = "CMS_AutoType_Search";
            try
            {
                List<AutoTypeEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "TotalRow", totalRow, System.Data.ParameterDirection.Output);
                _db.AddParameter(cmd, "Keyword", keyword);
                _db.AddParameter(cmd, "PageIndex", pageIndex);
                _db.AddParameter(cmd, "PageSize", pageSize);
                data = _db.GetList<AutoTypeEntity>(cmd);
                totalRow = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 0));
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public AutoTypeEntity GetById(long id)
        {
            const string commandText = "CMS_AutoType_GetById";
            try
            {
                AutoTypeEntity data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", id);
                data = _db.Get<AutoTypeEntity>(cmd);
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public List<AutoTypeEntity> GetByParentId(long id)
        {
            const string commandText = "CMS_AutoType_GetByParentId";
            try
            {
                List<AutoTypeEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", id);
                data = _db.GetList<AutoTypeEntity>(cmd);
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public List<AutoTypeEntity> SearchByParent(string keyword, int status, int parentId, int pageIndex, int pageSize, ref int totalRow)
        {
            const string commandText = "CMS_AutoType_SearchParent";
            try
            {
                List<AutoTypeEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "TotalRow", totalRow, System.Data.ParameterDirection.Output);
                _db.AddParameter(cmd, "Keyword", keyword);
                _db.AddParameter(cmd, "PageIndex", pageIndex);
                _db.AddParameter(cmd, "PageSize", pageSize);
                _db.AddParameter(cmd, "Status", status);
                _db.AddParameter(cmd, "ParentId", parentId);
                data = _db.GetList<AutoTypeEntity>(cmd);
                totalRow = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 0));
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        #endregion

        #region Function SET
        public bool Insert(AutoTypeEntity autoType)
        {
            const string commandText = "CMS_AutoType_Insert";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Name", autoType.AutoTypeName);
                _db.AddParameter(cmd, "Avatar", autoType.Avatar);
                var result = cmd.ExecuteNonQuery();
                return result > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool Update(AutoTypeEntity autoType)
        {
            const string commandText = "CMS_AutoType_Update";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", autoType.Id);
                _db.AddParameter(cmd, "Name", autoType.AutoTypeName);
                _db.AddParameter(cmd, "Avatar", autoType.Avatar);
                var result = cmd.ExecuteNonQuery();
                return result > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool UpdateStatus(int id, int status)
        {
            const string commandText = "CMS_AutoType_UpdateStatus";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", id);
                _db.AddParameter(cmd, "Status", status);
                var result = cmd.ExecuteNonQuery();
                return result > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool DeleteById(long tagId)
        {
            const string commandText = "CMS_AutoType_DeleteByID";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", tagId);
                var result = cmd.ExecuteNonQuery();
                return result > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        #endregion
        
        #region Constructor

        private readonly CmsMainDb _db;

        protected AutoTypeDalBase(CmsMainDb db)
        {
            _db = db;
        }

        protected CmsMainDb Database
        {
            get { return _db; }
        }

        #endregion
    }
}
