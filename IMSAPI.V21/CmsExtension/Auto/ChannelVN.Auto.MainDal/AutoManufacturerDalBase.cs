﻿using ChannelVN.Auto.Entity;
using ChannelVN.Auto.MainDal.Databases;
using ChannelVN.CMS.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChannelVN.Auto.MainDal
{
    public abstract class AutoManufacturerDalBase
    {
        #region function GET

        public List<AutoManufacturerEntity> Search(string keyword, int status, int pageIndex, int pageSize, ref int totalRow)
        {
            const string commandText = "CMS_AutoManufacturer_Search";
            try
            {
                List<AutoManufacturerEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "TotalRow", totalRow, System.Data.ParameterDirection.Output);
                _db.AddParameter(cmd, "Keyword", keyword);
                _db.AddParameter(cmd, "PageIndex", pageIndex);
                _db.AddParameter(cmd, "PageSize", pageSize);
                _db.AddParameter(cmd, "Status", status);
                data = _db.GetList<AutoManufacturerEntity>(cmd);
                totalRow = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 0));
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public AutoManufacturerEntity GetById(long id)
        {
            const string commandText = "CMS_AutoManufacturer_GetById";
            try
            {
                AutoManufacturerEntity data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", id);
                data = _db.Get<AutoManufacturerEntity>(cmd);
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public List<AutoManufacturerEntity> GetByParentId(long id)
        {
            const string commandText = "CMS_AutoManufacturer_GetByParentId";
            try
            {
                List<AutoManufacturerEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", id);
                data = _db.GetList<AutoManufacturerEntity>(cmd);
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public List<AutoManufacturerEntity> SearchByParent(string keyword, int status, int parentId, int pageIndex, int pageSize, ref int totalRow)
        {
            const string commandText = "CMS_AutoManufacturer_SearchParent";
            try
            {
                List<AutoManufacturerEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "TotalRow", totalRow, System.Data.ParameterDirection.Output);
                _db.AddParameter(cmd, "Keyword", keyword);
                _db.AddParameter(cmd, "PageIndex", pageIndex);
                _db.AddParameter(cmd, "PageSize", pageSize);
                _db.AddParameter(cmd, "Status", status);
                _db.AddParameter(cmd, "ParentId", parentId);
                data = _db.GetList<AutoManufacturerEntity>(cmd);
                totalRow = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 0));
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        #endregion

        #region Function SET
        public bool Insert(AutoManufacturerEntity autoManufacturer)
        {
            const string commandText = "CMS_AutoManufacturer_Insert";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Name", autoManufacturer.Name);
                _db.AddParameter(cmd, "UnsignName", autoManufacturer.UnsignName);
                _db.AddParameter(cmd, "SortOrder", autoManufacturer.SortOrder);
                _db.AddParameter(cmd, "Avatar", autoManufacturer.Avatar);
                _db.AddParameter(cmd, "Description", autoManufacturer.Description);
                _db.AddParameter(cmd, "Status", autoManufacturer.Status);
                _db.AddParameter(cmd, "ParentId", autoManufacturer.ParentId);
                _db.AddParameter(cmd, "TagId", autoManufacturer.TagId);
                var result = cmd.ExecuteNonQuery();
                return result > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool Update(AutoManufacturerEntity autoManufacturer)
        {
            const string commandText = "CMS_AutoManufacturer_Update";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", autoManufacturer.Id);
                _db.AddParameter(cmd, "Name", autoManufacturer.Name);
                _db.AddParameter(cmd, "UnsignName", autoManufacturer.UnsignName);
                _db.AddParameter(cmd, "SortOrder", autoManufacturer.SortOrder);
                _db.AddParameter(cmd, "Avatar", autoManufacturer.Avatar);
                _db.AddParameter(cmd, "Description", autoManufacturer.Description);
                _db.AddParameter(cmd, "Status", autoManufacturer.Status);
                _db.AddParameter(cmd, "ParentId", autoManufacturer.ParentId);
                _db.AddParameter(cmd, "TagId", autoManufacturer.TagId);
                var result = cmd.ExecuteNonQuery();
                return result > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool UpdateStatus(int id, int status)
        {
            const string commandText = "CMS_AutoManufacturer_UpdateStatus";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", id);
                _db.AddParameter(cmd, "Status", status);
                var result = cmd.ExecuteNonQuery();
                return result > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool DeleteById(long tagId)
        {
            const string commandText = "CMS_AutoManufacturer_DeleteByID";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", tagId);
                var result = cmd.ExecuteNonQuery();
                return result > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        #endregion

        /// <summary>
        /// Chuyển đổi DAL cho AutoManufacturer
        /// minhduongvan
        /// 2014/01/06
        /// </summary>

        #region Constructor

        private readonly CmsMainDb _db;

        protected AutoManufacturerDalBase(CmsMainDb db)
        {
            _db = db;
        }

        protected CmsMainDb Database
        {
            get { return _db; }
        }

        #endregion
    }
}
