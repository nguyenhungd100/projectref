﻿using ChannelVN.Auto.Entity;
using ChannelVN.Auto.MainDal.Databases;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ChannelVN.Auto.MainDal
{
    public abstract class AlbumInAutoManufacturerDalBase
    {
        #region Function GET

        public AlbumInAutoManufacturerEntity GetByAlbumId(int id)
        {
            const string commandText = "CMS_AlbumInAutoManufacturer_GetByAlbumId";
            try
            {
                AlbumInAutoManufacturerEntity data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", id);
                data = _db.Get<AlbumInAutoManufacturerEntity>(cmd);
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public AlbumInAutoManufacturerEntity GetAlbumAutoModelByModelId(int autoModelId)
        {
            const string commandText = "CMS_AlbumInAutoModel_GetByAutoModelId";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);

                _db.AddParameter(cmd, "AutoModelId", autoModelId);
                var result = _db.Get<AlbumInAutoManufacturerEntity>(cmd);
                return result;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        #endregion

        #region Funtion SET

        public bool Update(AlbumInAutoManufacturerEntity autoManufacturer)
        {
            const string commandText = "CMS_AlbumInAutoManufacturer_Update";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);

                _db.AddParameter(cmd, "AlbumId", autoManufacturer.AlbumId);
                _db.AddParameter(cmd, "AutoManufacturerId", autoManufacturer.AutoManufacturerId);
                var result = cmd.ExecuteNonQuery();
                return result > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public bool InsertAlbumAutoModel(int autoModelId, int albumId)
        {
            const string commandText = "CMS_AlbumInAutoModel_Insert";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);

                _db.AddParameter(cmd, "AutoModelId", autoModelId);
                _db.AddParameter(cmd, "AlbumId", albumId);
                var result = cmd.ExecuteNonQuery();
                return result > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        #endregion

        /// <summary>
        /// Chuyển đổi DAL cho AlbumInAutoManufacturer
        /// minhduongvan
        /// 2014/01/07
        /// </summary>

        #region Constructor

        private readonly CmsMainDb _db;

        protected AlbumInAutoManufacturerDalBase(CmsMainDb db)
        {
            _db = db;
        }

        protected CmsMainDb Database
        {
            get { return _db; }
        }

        #endregion
    }
}
