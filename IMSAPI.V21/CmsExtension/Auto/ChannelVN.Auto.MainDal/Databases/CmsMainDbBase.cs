﻿using System.Data;
using ChannelVN.CMS.Common.DalCommon;
using ChannelVN.Auto.MainDal.Databases;

namespace ChannelVN.Auto.MainDal.Databases
{
    /// <summary>
    /// The base class for the <see cref="CmsMainDb"/> class that 
    /// represents a connection to the <c>CmsMainDb</c> database. 
    /// </summary>
    /// <remarks>
    /// Do not change this source code. Modify the CmsMainDb class
    /// if you need to add or change some functionality.
    /// </remarks>
    public abstract class CmsMainDbBase : MainDbBase
    {
        #region Store procedures

        #region AutoModel
        private AutoModelDal _autoContentMainDal;
        public AutoModelDalBase AutoContentMainDal
        {
            get { return _autoContentMainDal ?? (_autoContentMainDal = new AutoModelDal((CmsMainDb)this)); }
        }
        #endregion

        #region Auto Manufacturer
        private AutoManufacturerDal _autoManuContentMainDal;
        public AutoManufacturerDalBase AutoManuContentMainDal
        {
            get { return _autoManuContentMainDal ?? (_autoManuContentMainDal = new AutoManufacturerDal((CmsMainDb)this)); }
        }
        #endregion

        #region Auto Type
        private AutoTypeDal _autoTypeMainDal;
        public AutoTypeDalBase AutoTypeMainDal
        {
            get { return _autoTypeMainDal ?? (_autoTypeMainDal = new AutoTypeDal((CmsMainDb)this)); }
        }
        #endregion

        #region Album In Auto Manufacturer
        private AlbumInAutoManufacturerDal _albumContentMainDal;
        public AlbumInAutoManufacturerDalBase AlbumContentMainDal
        {
            get { return _albumContentMainDal ?? (_albumContentMainDal = new AlbumInAutoManufacturerDal((CmsMainDb)this)); }
        }
        #endregion

        #region Box Embed
        private BoxAutoModelEmbedDal _boxAutoModelEmbedMainDal;
        public BoxAutoModelEmbedDalBase BoxAutoModelEmbedMainDal
        {
            get { return _boxAutoModelEmbedMainDal ?? (_boxAutoModelEmbedMainDal = new BoxAutoModelEmbedDal((CmsMainDb)this)); }
        }
        #endregion

        #endregion

        #region Constructors

        protected CmsMainDbBase()
        {
        }
        protected CmsMainDbBase(bool init)
        {
            if (init)
            {
                InitConnection();
            }
        }

        #endregion

        #region Protected members

        protected override sealed void InitConnection()
        {
            _connection = CreateConnection();
            _connection.Open();
        }

        /// <summary>
        /// Creates a new connection to the database.
        /// </summary>
        /// <returns>A reference to the <see cref="System.Data.IDbConnection"/> object.</returns>
        protected abstract IDbConnection CreateConnection();

        #endregion
    }
}