﻿using ChannelVN.Auto.Entity;
using ChannelVN.Auto.MainDal.Databases;
using ChannelVN.CMS.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChannelVN.Auto.MainDal
{
    public abstract class AutoModelDalBase
    {
        #region Function GET
        public List<AutoModelEntity> Search(string keyword, int pageIndex, int pageSize, ref int totalRow)
        {
            const string commandText = "CMS_AutoModel_Search";
            try
            {
                List<AutoModelEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "TotalRow", totalRow, System.Data.ParameterDirection.Output);
                _db.AddParameter(cmd, "Keyword", keyword);
                _db.AddParameter(cmd, "PageIndex", pageIndex);
                _db.AddParameter(cmd, "PageSize", pageSize);
                data = _db.GetList<AutoModelEntity>(cmd);
                totalRow = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 0));
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public AutoModelEntity GetById(long id)
        {
            const string commandText = "CMS_AutoModel_GetById";
            try
            {
                AutoModelEntity data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", id);
                data = _db.Get<AutoModelEntity>(cmd);
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public AutoModelEntity GetByName(string name)
        {
            const string commandText = "CMS_AutoModel_Search_By_Name";
            try
            {
                AutoModelEntity data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Keyword", name);
                data = _db.Get<AutoModelEntity>(cmd);
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public AutoModelEntity GetByTagId(long id)
        {
            const string commandText = "CMS_AutoModel_GetByTagId";
            try
            {
                AutoModelEntity data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", id);
                data = _db.Get<AutoModelEntity>(cmd);
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        #endregion

        #region Function SET
        public bool Insert(AutoModelEntity AutoModel)
        {
            const string commandText = "CMS_AutoModel_Insert";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "ModelName", AutoModel.ModelName);
                _db.AddParameter(cmd, "Price", AutoModel.Price);
                _db.AddParameter(cmd, "Origin", AutoModel.Origin);
                _db.AddParameter(cmd, "Type", AutoModel.Type);
                _db.AddParameter(cmd, "Passengers", AutoModel.Passengers);
                _db.AddParameter(cmd, "Engine", AutoModel.Engine);
                _db.AddParameter(cmd, "Power", AutoModel.Power);
                _db.AddParameter(cmd, "Torque", AutoModel.Torque);
                _db.AddParameter(cmd, "Transmission", AutoModel.Transmission);
                _db.AddParameter(cmd, "OutDoorEquipment", AutoModel.OutDoorEquipment);
                _db.AddParameter(cmd, "IndoorEquipment", AutoModel.IndoorEquipment);
                _db.AddParameter(cmd, "SafetyEquipment", AutoModel.SafetyEquipment);
                _db.AddParameter(cmd, "FuelCost", AutoModel.FuelCost);
                _db.AddParameter(cmd, "TagId", AutoModel.TagId);
                _db.AddParameter(cmd, "ManufactureId", AutoModel.ManufactureId);
                _db.AddParameter(cmd, "Avatar", AutoModel.Avatar);
                _db.AddParameter(cmd, "AutoTypeId", AutoModel.AutoTypeId);
                _db.AddParameter(cmd, "YearProduced", AutoModel.YearProduced);
                _db.AddParameter(cmd, "ValvePerXilanh", AutoModel.ValvePerXilanh);
                _db.AddParameter(cmd, "EngineCapacity", AutoModel.EngineCapacity);
                _db.AddParameter(cmd, "FuelType", AutoModel.FuelType);
                _db.AddParameter(cmd, "TransmissionLevel", AutoModel.TransmissionLevel);
                _db.AddParameter(cmd, "Length", AutoModel.Length);
                _db.AddParameter(cmd, "Width", AutoModel.Width);
                _db.AddParameter(cmd, "Height", AutoModel.Height);
                _db.AddParameter(cmd, "Weight", AutoModel.Weight);
                _db.AddParameter(cmd, "BaseLength", AutoModel.BaseLength);
                _db.AddParameter(cmd, "Color", AutoModel.Color);
                _db.AddParameter(cmd, "InteriorMaterials", AutoModel.InteriorMaterials);
                _db.AddParameter(cmd, "SoundSystem", AutoModel.SoundSystem);
                _db.AddParameter(cmd, "AirConditionSystem", AutoModel.AirConditionSystem);
                _db.AddParameter(cmd, "SizeOfTires", AutoModel.SizeOfTires);
                _db.AddParameter(cmd, "LastModifyDate", AutoModel.LastModifyDate);
                _db.AddParameter(cmd, "Description", AutoModel.Description);
                var numberOfRow = cmd.ExecuteNonQuery();

                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public bool Update(AutoModelEntity AutoModel)
        {
            const string commandText = "CMS_AutoModel_Update";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", AutoModel.Id);
                _db.AddParameter(cmd, "ModelName", AutoModel.ModelName);
                _db.AddParameter(cmd, "Price", AutoModel.Price);
                _db.AddParameter(cmd, "Origin", AutoModel.Origin);
                _db.AddParameter(cmd, "Type", AutoModel.Type);
                _db.AddParameter(cmd, "Passengers", AutoModel.Passengers);
                _db.AddParameter(cmd, "Engine", AutoModel.Engine);
                _db.AddParameter(cmd, "Power", AutoModel.Power);
                _db.AddParameter(cmd, "Torque", AutoModel.Torque);
                _db.AddParameter(cmd, "Transmission", AutoModel.Transmission);
                _db.AddParameter(cmd, "OutDoorEquipment", AutoModel.OutDoorEquipment);
                _db.AddParameter(cmd, "IndoorEquipment", AutoModel.IndoorEquipment);
                _db.AddParameter(cmd, "SafetyEquipment", AutoModel.SafetyEquipment);
                _db.AddParameter(cmd, "FuelCost", AutoModel.FuelCost);
                _db.AddParameter(cmd, "TagId", AutoModel.TagId);
                _db.AddParameter(cmd, "ManufactureId", AutoModel.ManufactureId);
                _db.AddParameter(cmd, "Avatar", AutoModel.Avatar);
                _db.AddParameter(cmd, "AutoTypeId", AutoModel.AutoTypeId);
                _db.AddParameter(cmd, "YearProduced", AutoModel.YearProduced);
                _db.AddParameter(cmd, "ValvePerXilanh", AutoModel.ValvePerXilanh);
                _db.AddParameter(cmd, "EngineCapacity", AutoModel.EngineCapacity);
                _db.AddParameter(cmd, "FuelType", AutoModel.FuelType);
                _db.AddParameter(cmd, "TransmissionLevel", AutoModel.TransmissionLevel);
                _db.AddParameter(cmd, "Length", AutoModel.Length);
                _db.AddParameter(cmd, "Width", AutoModel.Width);
                _db.AddParameter(cmd, "Height", AutoModel.Height);
                _db.AddParameter(cmd, "Weight", AutoModel.Weight);
                _db.AddParameter(cmd, "BaseLength", AutoModel.BaseLength);
                _db.AddParameter(cmd, "Color", AutoModel.Color);
                _db.AddParameter(cmd, "InteriorMaterials", AutoModel.InteriorMaterials);
                _db.AddParameter(cmd, "SoundSystem", AutoModel.SoundSystem);
                _db.AddParameter(cmd, "AirConditionSystem", AutoModel.AirConditionSystem);
                _db.AddParameter(cmd, "SizeOfTires", AutoModel.SizeOfTires);
                _db.AddParameter(cmd, "LastModifyDate", AutoModel.LastModifyDate);
                _db.AddParameter(cmd, "Description", AutoModel.Description);
                var numberOfRow = cmd.ExecuteNonQuery();

                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool UpdateAvatar(AutoModelEntity AutoModel)
        {
            const string commandText = "CMS_AutoModel_Update_Avatar";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", AutoModel.Id);
                _db.AddParameter(cmd, "Avatar", AutoModel.Avatar);
                var numberOfRow = cmd.ExecuteNonQuery();

                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public bool UpdateStatus(int id, int status)
        {
            const string commandText = "CMS_AutoModel_UpdateStatus";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", id);
                _db.AddParameter(cmd, "Status", status);
                var numberOfRow = cmd.ExecuteNonQuery();

                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public bool DeleteById(long tagId)
        {
            const string commandText = "CMS_AutoModel_DeleteByID";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", tagId);
                var numberOfRow = cmd.ExecuteNonQuery();

                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        #endregion

        /// <summary>
        /// Chuyển đổi DAL cho AutoModel
        /// minhduongvan
        /// 2014/01/06
        /// </summary>

        #region Constructor

        private readonly CmsMainDb _db;

        protected AutoModelDalBase(CmsMainDb db)
        {
            _db = db;
        }

        protected CmsMainDb Database
        {
            get { return _db; }
        }

        #endregion
    }
}
