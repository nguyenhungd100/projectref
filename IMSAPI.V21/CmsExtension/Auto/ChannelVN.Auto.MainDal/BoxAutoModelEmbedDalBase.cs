﻿using ChannelVN.Auto.Entity;
using ChannelVN.Auto.MainDal.Databases;
using ChannelVN.CMS.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChannelVN.Auto.MainDal
{
    public abstract class BoxAutoModelEmbedDalBase
    {
        #region Function GET

        public List<AutoModelEntity> Search(int type)
        {
            const string commandText = "CMS_BoxAutoModelEmbed_GetByType";
            try
            {
                List<AutoModelEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Type", type);
                data = _db.GetList<AutoModelEntity>(cmd);
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        #endregion

        #region Function SET

        public bool Update(int type, string listOfPriority)
        {
            const string commandText = "CMS_BoxAutoModelEmbed_Update";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Type", type);
                _db.AddParameter(cmd, "ListOfPriority", listOfPriority);
                var result = cmd.ExecuteNonQuery();
                return result > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        #endregion

        #region Constructor

        private readonly CmsMainDb _db;

        protected BoxAutoModelEmbedDalBase(CmsMainDb db)
        {
            _db = db;
        }

        protected CmsMainDb Database
        {
            get { return _db; }
        }
        #endregion
    }
}
