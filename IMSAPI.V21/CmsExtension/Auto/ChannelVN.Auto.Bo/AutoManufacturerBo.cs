﻿using ChannelVN.CMS.Common;
using ChannelVN.Auto.Bo.Common;
using ChannelVN.Auto.Dal;
using ChannelVN.Auto.Dal.Common;
using ChannelVN.Auto.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ChannelVN.Auto.Entity.ErrorCode;

namespace ChannelVN.Auto.Bo
{
    public class AutoManufacturerBo
    {
        #region AutoManufacturer

        public static List<AutoManufacturerEntity> SearchAutoManufacturer(string keyword, EnumAutoManufacturerStatus status, int pageIndex, int pageSize, ref int totalRow)
        {
            return AutoManufacturerDal.Search(keyword, (int)status, pageIndex, pageSize, ref totalRow);
        }

        public static AutoManufacturerEntity GetAutoManufacturerByAutoManufacturerId(int id)
        {
            return AutoManufacturerDal.GetById(id);
        }

        public static ErrorMapping.ErrorCodes InsertAutoManufacturer(AutoManufacturerEntity autoManufacturer)
        {
            if (string.IsNullOrEmpty(autoManufacturer.Name))
            {
                return ErrorMapping.ErrorCodes.UpdateAutoManufacturerInvalidTitle;
            }
            return AutoManufacturerDal.Insert(autoManufacturer)
                       ? ErrorMapping.ErrorCodes.Success
                       : ErrorMapping.ErrorCodes.BusinessError;
        }
        public static ErrorMapping.ErrorCodes UpdateAutoManufacturer(AutoManufacturerEntity autoManufacturer)
        {
            if (autoManufacturer.Id <= 0)
            {
                return ErrorMapping.ErrorCodes.UpdateAutoManufacturerNotFoundAutoManufacturer;
            }
            var autoManufacturerEntity = AutoManufacturerDal.GetById(autoManufacturer.Id);
            if (autoManufacturerEntity == null)
            {
                return ErrorMapping.ErrorCodes.UpdateAutoManufacturerNotFoundAutoManufacturer;
            }
            if (string.IsNullOrEmpty(autoManufacturer.Name))
            {
                return ErrorMapping.ErrorCodes.UpdateAutoManufacturerInvalidTitle;
            }
            autoManufacturerEntity.Name = autoManufacturer.Name;
            autoManufacturerEntity.Avatar = autoManufacturer.Avatar;
            autoManufacturerEntity.Status = autoManufacturer.Status;
            autoManufacturerEntity.Description = autoManufacturer.Description;
            autoManufacturerEntity.SortOrder = autoManufacturer.SortOrder;
            autoManufacturerEntity.UnsignName = autoManufacturer.UnsignName;
            autoManufacturerEntity.ParentId = autoManufacturer.ParentId;
            autoManufacturerEntity.TagId = autoManufacturer.TagId;
            return AutoManufacturerDal.Update(autoManufacturerEntity)
                       ? ErrorMapping.ErrorCodes.Success
                       : ErrorMapping.ErrorCodes.BusinessError;
        }
        public static ErrorMapping.ErrorCodes UpdateAutoManufacturerStatus(int id, EnumAutoManufacturerStatus status)
        {
            return AutoManufacturerDal.UpdateStatus(id, (int)status)
                       ? ErrorMapping.ErrorCodes.Success
                       : ErrorMapping.ErrorCodes.BusinessError;
        }
        public static ErrorMapping.ErrorCodes DeleteById(long tagId)
        {
            return AutoManufacturerDal.DeleteById(tagId) ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.UnknowError;
        }

        public static List<AutoManufacturerEntity> GetByParentId(int id)
        {
            return AutoManufacturerDal.GetByParentId(id);
        }
        public static List<AutoManufacturerEntity> SearchAutoManufacturerByParent(string keyword, EnumAutoManufacturerStatus status, int parentId, int pageIndex, int pageSize, ref int totalRow)
        {
            return AutoManufacturerDal.SearchByParent(keyword, (int)status, parentId, pageIndex, pageSize, ref totalRow);
        }

        #endregion

        #region AlbumInAutoManufacturer

        public static AlbumInAutoManufacturerEntity GetAlbumInAutoManufacturerByAlbumId(int id)
        {
            return AlbumInAutoManufacturerDal.GetByAlbumId(id);
        }

        public static ErrorMapping.ErrorCodes UpdateAlbumInAutoManufacturer(AlbumInAutoManufacturerEntity albumInAutoManufacturerEntity)
        {
            return AlbumInAutoManufacturerDal.Update(albumInAutoManufacturerEntity) ? ErrorMapping.ErrorCodes.Success
                       : ErrorMapping.ErrorCodes.BusinessError;
        }

        #endregion

        public static ErrorMapping.ErrorCodes UpdateAlbumInAutoModel(int autoModelId, int autoAlbumId)
        {
            return AlbumInAutoManufacturerDal.InsertAlbumAutoModel(autoModelId, autoAlbumId) ? ErrorMapping.ErrorCodes.Success
                       : ErrorMapping.ErrorCodes.BusinessError;
        }

        public static AlbumInAutoManufacturerEntity GetAlbumAutoModelByModelId(int id)
        {
            return AlbumInAutoManufacturerDal.GetAlbumAutoModelByModelId(id);
        }
    }
}
