﻿using ChannelVN.CMS.Common;
using ChannelVN.Auto.Bo.Common;
using ChannelVN.Auto.Dal;
using ChannelVN.Auto.Dal.Common;
using ChannelVN.Auto.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ChannelVN.Auto.Entity.ErrorCode;

namespace ChannelVN.Auto.Bo
{
    public class AutoModelBo
    {
        #region AutoModel

        public static List<AutoModelEntity> SearchAutoModel(string keyword, int pageIndex, int pageSize, ref int totalRow)
        {
            return AutoModelDal.Search(keyword, pageIndex, pageSize, ref totalRow);
        }
        public static AutoModelEntity GetAutoModelByAutoModelId(int id)
        {
            return AutoModelDal.GetById(id);
        }
        public static AutoModelEntity GetAutoModelByName(string name)
        {
            return AutoModelDal.GetByName(name);
        }
        public static AutoModelEntity GetAutoModelByTagId(long id)
        {
            return AutoModelDal.GetByTagId(id);
        }

        public static ErrorMapping.ErrorCodes InsertAutoModel(AutoModelEntity AutoModel)
        {
            return AutoModelDal.Insert(AutoModel)
                       ? ErrorMapping.ErrorCodes.Success
                       : ErrorMapping.ErrorCodes.BusinessError;
        }

        public static ErrorMapping.ErrorCodes UpdateAutoModel(AutoModelEntity AutoModel)
        {
            if (AutoModel.Id <= 0)
            {
                return ErrorMapping.ErrorCodes.UpdateAutoModelNotFoundAutoModel;
            }
            var AutoModelEntity = AutoModelDal.GetById(AutoModel.Id);
            if (AutoModelEntity == null)
            {
                return ErrorMapping.ErrorCodes.UpdateAutoModelNotFoundAutoModel;
            }
            AutoModelEntity.Engine = AutoModel.Engine;
            AutoModelEntity.FuelCost = AutoModel.FuelCost;
            AutoModelEntity.IndoorEquipment = AutoModel.IndoorEquipment;
            AutoModelEntity.ModelName = AutoModel.ModelName;
            AutoModelEntity.Origin = AutoModel.Origin;
            AutoModelEntity.OutDoorEquipment = AutoModel.OutDoorEquipment;
            AutoModelEntity.Passengers = AutoModel.Passengers;
            AutoModelEntity.Power = AutoModel.Power;
            AutoModelEntity.Price = AutoModel.Price;
            AutoModelEntity.SafetyEquipment = AutoModel.SafetyEquipment;
            AutoModelEntity.Torque = AutoModel.Torque;
            AutoModelEntity.Transmission = AutoModel.Transmission;
            AutoModelEntity.Type = AutoModel.Type;
            AutoModelEntity.ManufactureId = AutoModel.ManufactureId;
            AutoModelEntity.AirConditionSystem = AutoModel.AirConditionSystem;
            AutoModelEntity.AutoTypeId = AutoModel.AutoTypeId;
            AutoModelEntity.Avatar = AutoModel.Avatar;
            AutoModelEntity.BaseLength = AutoModel.BaseLength;
            AutoModelEntity.Length = AutoModel.Length;
            AutoModelEntity.Color = AutoModel.Color;
            AutoModelEntity.EngineCapacity = AutoModel.EngineCapacity;
            AutoModelEntity.FuelType = AutoModel.FuelType;
            AutoModelEntity.Height = AutoModel.Height;
            AutoModelEntity.InteriorMaterials = AutoModel.InteriorMaterials;
            AutoModelEntity.LastModifyDate = AutoModel.LastModifyDate;
            AutoModelEntity.SizeOfTires = AutoModel.SizeOfTires;
            AutoModelEntity.SoundSystem = AutoModel.SoundSystem;
            AutoModelEntity.TransmissionLevel = AutoModel.TransmissionLevel;
            AutoModelEntity.ValvePerXilanh = AutoModel.ValvePerXilanh;
            AutoModelEntity.Weight = AutoModel.Weight;
            AutoModelEntity.Width = AutoModel.Width;
            AutoModelEntity.YearProduced = AutoModel.YearProduced;
            AutoModelEntity.Description = AutoModel.Description;
            return AutoModelDal.Update(AutoModelEntity)
                       ? ErrorMapping.ErrorCodes.Success
                       : ErrorMapping.ErrorCodes.BusinessError;
        }
        public static ErrorMapping.ErrorCodes UpdateAvatarAutoModel(AutoModelEntity AutoModel)
        {
            if (AutoModel.Id <= 0)
            {
                return ErrorMapping.ErrorCodes.UpdateAutoModelNotFoundAutoModel;
            }
            var AutoModelEntity = AutoModelDal.GetById(AutoModel.Id);
            if (AutoModelEntity == null)
            {
                return ErrorMapping.ErrorCodes.UpdateAutoModelNotFoundAutoModel;
            }
            AutoModelEntity.Avatar = AutoModel.Avatar;
            return AutoModelDal.UpdateAvatar(AutoModelEntity)
                       ? ErrorMapping.ErrorCodes.Success
                       : ErrorMapping.ErrorCodes.BusinessError;
        }

        public static ErrorMapping.ErrorCodes DeleteById(long tagId)
        {
            return AutoModelDal.DeleteById(tagId) ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.UnknowError;
        }

        #endregion

        #region AlbumInAutoModel

        //public static AlbumInAutoModelEntity GetAlbumInAutoModelByAlbumId(int id)
        //{
        //    return AlbumInAutoModelDal.GetByAlbumId(id);
        //}

        //public static ErrorMapping.ErrorCodes UpdateAlbumInAutoModel(AlbumInAutoModelEntity albumInAutoModelEntity)
        //{
        //    return AlbumInAutoModelDal.Update(albumInAutoModelEntity) ? ErrorMapping.ErrorCodes.Success
        //               : ErrorMapping.ErrorCodes.BusinessError;
        //}

        #endregion
    }
}
