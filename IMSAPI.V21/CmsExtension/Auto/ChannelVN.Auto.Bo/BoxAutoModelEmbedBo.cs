﻿using ChannelVN.CMS.Common;
using ChannelVN.Auto.Bo.Common;
using ChannelVN.Auto.Dal;
using ChannelVN.Auto.Dal.Common;
using ChannelVN.Auto.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ChannelVN.Auto.Entity.ErrorCode;

namespace ChannelVN.Auto.Bo
{
    public class BoxAutoModelEmbedBo
    {
        #region BoxAutoModelEmbed

        public static List<AutoModelEntity> Search(int type)
        {
            return BoxAutoModelEmbedDal.Search(type);
        }

        public static ErrorMapping.ErrorCodes UpdateAutoModel(int type, string lst)
        {
            return BoxAutoModelEmbedDal.Update(type, lst)
                       ? ErrorMapping.ErrorCodes.Success
                       : ErrorMapping.ErrorCodes.BusinessError;
        }

        #endregion
    }
}
