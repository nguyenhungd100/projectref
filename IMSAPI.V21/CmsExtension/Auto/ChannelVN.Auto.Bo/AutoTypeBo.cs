﻿using ChannelVN.CMS.Common;
using ChannelVN.Auto.Bo.Common;
using ChannelVN.Auto.Dal;
using ChannelVN.Auto.Dal.Common;
using ChannelVN.Auto.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ChannelVN.Auto.Entity.ErrorCode;

namespace ChannelVN.Auto.Bo
{
    public class AutoTypeBo
    {
        #region AutoType

        public static List<AutoTypeEntity> SearchAutoType(string keyword,  int pageIndex, int pageSize, ref int totalRow)
        {
            return AutoTypeDal.Search(keyword,  pageIndex, pageSize, ref totalRow);
        }

        public static AutoTypeEntity GetAutoTypeByAutoTypeId(int id)
        {
            return AutoTypeDal.GetById(id);
        }

        public static ErrorMapping.ErrorCodes InsertAutoType(AutoTypeEntity autoType)
        {
            return AutoTypeDal.Insert(autoType)
                       ? ErrorMapping.ErrorCodes.Success
                       : ErrorMapping.ErrorCodes.BusinessError;
        }

        public static ErrorMapping.ErrorCodes UpdateAutoType(AutoTypeEntity autoType)
        {
            var autoTypeEntity = AutoTypeDal.GetById(autoType.Id);
            autoTypeEntity.AutoTypeName = autoType.AutoTypeName;
            autoTypeEntity.Avatar = autoType.Avatar;
            return AutoTypeDal.Update(autoTypeEntity)
                       ? ErrorMapping.ErrorCodes.Success
                       : ErrorMapping.ErrorCodes.BusinessError;
        }
        public static ErrorMapping.ErrorCodes UpdateAutoTypeStatus(int id)
        {
            return AutoTypeDal.UpdateStatus(id, 1)
                       ? ErrorMapping.ErrorCodes.Success
                       : ErrorMapping.ErrorCodes.BusinessError;
        }

        public static ErrorMapping.ErrorCodes DeleteById(long tagId)
        {
            return AutoTypeDal.DeleteById(tagId) ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.UnknowError;
        }

        public static List<AutoTypeEntity> GetByParentId(int id)
        {
            return AutoTypeDal.GetByParentId(id);
        }
        public static List<AutoTypeEntity> SearchAutoTypeByParent(string keyword,  int parentId, int pageIndex, int pageSize, ref int totalRow)
        {
            return AutoTypeDal.SearchByParent(keyword, 1, parentId, pageIndex, pageSize, ref totalRow);
        }

        #endregion
    }
}
