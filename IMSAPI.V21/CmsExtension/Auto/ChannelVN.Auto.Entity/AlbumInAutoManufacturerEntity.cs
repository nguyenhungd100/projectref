﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using ChannelVN.CMS.Common;

namespace ChannelVN.Auto.Entity
{    
    [DataContract]
    public class AlbumInAutoManufacturerEntity : EntityBase
    {
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public int AlbumId { get; set; }
        [DataMember]
        public int AutoManufacturerId { get; set; }
    }
}
