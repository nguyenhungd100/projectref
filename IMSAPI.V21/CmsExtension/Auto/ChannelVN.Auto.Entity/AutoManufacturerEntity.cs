﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using ChannelVN.CMS.Common;

namespace ChannelVN.Auto.Entity
{
    [DataContract]
    public enum EnumAutoManufacturerStatus
    {
        [EnumMember]
        AllStatus = -1,
        [EnumMember]
        Hidden = 0,
        [EnumMember]
        Show = 1
    }

    [DataContract]
    public class AutoManufacturerEntity : EntityBase
    {
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public string Name { get; set; }
        [DataMember]
        public string Avatar { get; set; }
        [DataMember]
        public string UnsignName { get; set; }
        [DataMember]
        public string Description { get; set; }
        [DataMember]
        public int Status { get; set; }
        [DataMember]
        public int SortOrder { get; set; }
        [DataMember]
        public int ParentId { get; set; }
        [DataMember]
        public long TagId { get; set; }
    }
}
