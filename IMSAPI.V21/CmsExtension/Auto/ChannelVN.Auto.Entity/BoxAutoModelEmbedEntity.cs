﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using ChannelVN.CMS.Common;

namespace ChannelVN.Auto.Entity
{
    [DataContract]
    public class BoxAutoModelEmbedEntity : EntityBase
    {
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public int AutoModelId { get; set; }
        [DataMember]
        public int SortOrder { get; set; }
        [DataMember]
        public int Type { get; set; } 
    }
}
