﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using ChannelVN.CMS.Common;

namespace ChannelVN.Auto.Entity
{
    [DataContract]
    public class AutoTypeEntity : EntityBase
    {
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public string AutoTypeName { get; set; }
        [DataMember]
        public string Avatar { get; set; }
        [DataMember]
        public DateTime CreatedDate { get; set; } 
    }
}
