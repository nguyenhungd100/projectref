﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using ChannelVN.CMS.Common;

namespace ChannelVN.Auto.Entity
{
    [DataContract]
    public class AutoModelEntity : EntityBase
    {
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public string ModelName { get; set; }
        [DataMember]
        public string Avatar { get; set; }
        [DataMember]
        public int AutoTypeId { get; set; }
        [DataMember]
        public int YearProduced { get; set; }
        [DataMember]
        public string ValvePerXilanh { get; set; }
        [DataMember]
        public string EngineCapacity { get; set; }
        [DataMember]
        public int FuelType { get; set; }
        [DataMember]
        public int TransmissionLevel { get; set; }
        [DataMember]
        public int Length { get; set; }
        [DataMember]
        public int Width { get; set; }
        [DataMember]
        public int Height { get; set; }
        [DataMember]
        public int Weight { get; set; }
        [DataMember]
        public int BaseLength { get; set; }
        [DataMember]
        public string Color { get; set; }
        [DataMember]
        public int InteriorMaterials { get; set; }
        [DataMember]
        public string SoundSystem { get; set; }
        [DataMember]
        public int AirConditionSystem { get; set; }
        [DataMember]
        public string SizeOfTires { get; set; }
        [DataMember]
        public decimal Price { get; set; }
        [DataMember]
        public string Origin { get; set; }
        [DataMember]
        public string Type { get; set; }
        [DataMember]
        public int Passengers { get; set; }
        [DataMember]
        public string Engine { get; set; }
        [DataMember]
        public string Power { get; set; }
        [DataMember]
        public string Torque { get; set; }
        [DataMember]
        public string Transmission { get; set; }
        [DataMember]
        public string OutDoorEquipment { get; set; }
        [DataMember]
        public string IndoorEquipment { get; set; }
        [DataMember]
        public string SafetyEquipment { get; set; }
        [DataMember]
        public string FuelCost { get; set; }
        [DataMember]
        public int ManufactureId { get; set; }
        [DataMember]
        public string ManufactureName { get; set; }
        [DataMember]
        public long TagId { get; set; }
        [DataMember]
        public DateTime LastModifyDate { get; set; }
        [DataMember]
        public string AutoTypeName { get; set; }
        [DataMember]
        public string Description { get; set; }

    }
}
