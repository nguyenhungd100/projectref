﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using ChannelVN.CMS.Common;
using ChannelVN.Auto.Dal.Common;
using ChannelVN.Auto.Entity;
using ChannelVN.Auto.MainDal.Databases;

namespace ChannelVN.Auto.Dal
{
    public class AlbumInAutoManufacturerDal
    {
        #region SET

        public static bool Update(AlbumInAutoManufacturerEntity autoManufacturer)
        {
            bool returnValue = false;
            using (var db = new CmsMainDb())
            {
                returnValue = db.AlbumContentMainDal.Update(autoManufacturer);
            }
            return returnValue;
        }

        public static bool InsertAlbumAutoModel(int autoModelId, int albumId)
        {
            bool returnValue = false;
            using (var db = new CmsMainDb())
            {
                returnValue = db.AlbumContentMainDal.InsertAlbumAutoModel(autoModelId, albumId);
            }
            return returnValue;
        }
        #endregion

        #region GET

        public static AlbumInAutoManufacturerEntity GetByAlbumId(int id)
        {
            AlbumInAutoManufacturerEntity returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.AlbumContentMainDal.GetByAlbumId(id);
            }
            return returnValue;
        }

        public static AlbumInAutoManufacturerEntity GetAlbumAutoModelByModelId(int id)
        {
            AlbumInAutoManufacturerEntity returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.AlbumContentMainDal.GetAlbumAutoModelByModelId(id);
            }
            return returnValue;
        }
        #endregion
    }
}
