﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using ChannelVN.CMS.Common;
using ChannelVN.Auto.Dal.Common;
using ChannelVN.Auto.Entity;
using ChannelVN.Auto.MainDal.Databases;

namespace ChannelVN.Auto.Dal
{
    public class AutoManufacturerDal
    {
        #region SET

        public static bool Insert(AutoManufacturerEntity autoManufacturer)
        {
            bool returnValue = false;
            using (var db = new CmsMainDb())
            {
                returnValue = db.AutoManuContentMainDal.Insert(autoManufacturer);
            }
            return returnValue;
        }
        public static bool Update(AutoManufacturerEntity autoManufacturer)
        {
            bool returnValue = false;
            using (var db = new CmsMainDb())
            {
                returnValue = db.AutoManuContentMainDal.Update(autoManufacturer);
            }
            return returnValue;
        }

        public static bool UpdateStatus(int id, int status)
        {
            bool returnValue = false;
            using (var db = new CmsMainDb())
            {
                returnValue = db.AutoManuContentMainDal.UpdateStatus(id, status);
            }
            return returnValue;
        }

        public static bool DeleteById(long tagId)
        {
            bool returnValue = false;
            using (var db = new CmsMainDb())
            {
                returnValue = db.AutoManuContentMainDal.DeleteById(tagId);
            }
            return returnValue;
        }
        #endregion

        #region GET

        public static List<AutoManufacturerEntity> Search(string keyword, int status, int pageIndex, int pageSize, ref int totalRow)
        {
            List<AutoManufacturerEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.AutoManuContentMainDal.Search(keyword, status, pageIndex, pageSize, ref totalRow);
            }
            return returnValue;
        }

        public static AutoManufacturerEntity GetById(long id)
        {
            AutoManufacturerEntity returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.AutoManuContentMainDal.GetById(id);
            }
            return returnValue;
        }

        public static List<AutoManufacturerEntity> GetByParentId(long id)
        {
            List<AutoManufacturerEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.AutoManuContentMainDal.GetByParentId(id);
            }
            return returnValue;
        }

        public static List<AutoManufacturerEntity> SearchByParent(string keyword, int status, int parentId, int pageIndex, int pageSize, ref int totalRow)
        {
            List<AutoManufacturerEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.AutoManuContentMainDal.SearchByParent(keyword, status, parentId, pageIndex, pageSize, ref totalRow);
            }
            return returnValue;
        }
        #endregion
    }
}
