﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using ChannelVN.CMS.Common;
using ChannelVN.Auto.Dal.Common;
using ChannelVN.Auto.Entity;
using ChannelVN.Auto.MainDal.Databases;

namespace ChannelVN.Auto.Dal
{
    public class AutoModelDal
    {
        #region SET

        public static bool Insert(AutoModelEntity AutoModel)
        {
            bool returnValue = false;
            using (var db = new CmsMainDb())
            {
                returnValue = db.AutoContentMainDal.Insert(AutoModel);
            }
            return returnValue;
        }

        public static bool Update(AutoModelEntity AutoModel)
        {
            bool returnValue = false;
            using (var db = new CmsMainDb())
            {
                returnValue = db.AutoContentMainDal.Update(AutoModel);
            }
            return returnValue;
        }

        public static bool UpdateAvatar(AutoModelEntity AutoModel)
        {
            bool returnValue = false;
            using (var db = new CmsMainDb())
            {
                returnValue = db.AutoContentMainDal.UpdateAvatar(AutoModel);
            }
            return returnValue;
        }

        public static bool UpdateStatus(int id, int status)
        {
            bool returnValue = false;
            using (var db = new CmsMainDb())
            {
                returnValue = db.AutoContentMainDal.UpdateStatus(id, status);
            }
            return returnValue;
        }

        public static bool DeleteById(long tagId)
        {
            bool returnValue = false;
            using (var db = new CmsMainDb())
            {
                returnValue = db.AutoContentMainDal.DeleteById(tagId);
            }
            return returnValue;
        }
        #endregion

        #region GET

        public static List<AutoModelEntity> Search(string keyword, int pageIndex, int pageSize, ref int totalRow)
        {
            List<AutoModelEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.AutoContentMainDal.Search(keyword, pageIndex, pageSize, ref totalRow);
            }
            return returnValue;
        }

        public static AutoModelEntity GetById(long id)
        {
            AutoModelEntity returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.AutoContentMainDal.GetById(id);
            }
            return returnValue;
        }

        public static AutoModelEntity GetByName(string name)
        {
            AutoModelEntity returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.AutoContentMainDal.GetByName(name);
            }
            return returnValue;
        }

        public static AutoModelEntity GetByTagId(long id)
        {
            AutoModelEntity returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.AutoContentMainDal.GetByTagId(id);
            }
            return returnValue;
        }
        #endregion
    }
}
