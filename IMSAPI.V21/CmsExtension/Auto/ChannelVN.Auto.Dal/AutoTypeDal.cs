﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using ChannelVN.CMS.Common;
using ChannelVN.Auto.Dal.Common;
using ChannelVN.Auto.Entity;
using ChannelVN.Auto.MainDal.Databases;

namespace ChannelVN.Auto.Dal
{
    public class AutoTypeDal
    {
        #region SET

        public static bool Insert(AutoTypeEntity autoType)
        {
            bool returnValue = false;
            using (var db = new CmsMainDb())
            {
                returnValue = db.AutoTypeMainDal.Insert(autoType);
            }
            return returnValue;
        }

        public static bool Update(AutoTypeEntity autoType)
        {
            bool returnValue = false;
            using (var db = new CmsMainDb())
            {
                returnValue = db.AutoTypeMainDal.Update(autoType);
            }
            return returnValue;
        }

        public static bool UpdateStatus(int id, int status)
        {
            bool returnValue = false;
            using (var db = new CmsMainDb())
            {
                returnValue = db.AutoTypeMainDal.UpdateStatus(id, status);
            }
            return returnValue;
        }

        public static bool DeleteById(long tagId)
        {
            bool returnValue = false;
            using (var db = new CmsMainDb())
            {
                returnValue = db.AutoTypeMainDal.DeleteById(tagId);
            }
            return returnValue;
        }
        #endregion

        #region GET

        public static List<AutoTypeEntity> Search(string keyword, int pageIndex, int pageSize, ref int totalRow)
        {
            List<AutoTypeEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.AutoTypeMainDal.Search(keyword, pageIndex, pageSize, ref totalRow);
            }
            return returnValue;
        }

        public static AutoTypeEntity GetById(long id)
        {
            AutoTypeEntity returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.AutoTypeMainDal.GetById(id);
            }
            return returnValue;
        }

        public static List<AutoTypeEntity> GetByParentId(long id)
        {
            List<AutoTypeEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.AutoTypeMainDal.GetByParentId(id);
            }
            return returnValue;
        }

        public static List<AutoTypeEntity> SearchByParent(string keyword, int status, int parentId, int pageIndex, int pageSize, ref int totalRow)
        {
            List<AutoTypeEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.AutoTypeMainDal.SearchByParent(keyword, status, parentId, pageIndex, pageSize, ref totalRow);
            }
            return returnValue;
        }
        #endregion
    }
}
