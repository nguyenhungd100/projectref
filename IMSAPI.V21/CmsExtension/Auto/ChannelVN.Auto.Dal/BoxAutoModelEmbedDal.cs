﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using ChannelVN.CMS.Common;
using ChannelVN.Auto.Dal.Common;
using ChannelVN.Auto.Entity;
using ChannelVN.Auto.MainDal.Databases;

namespace ChannelVN.Auto.Dal
{
    public class BoxAutoModelEmbedDal
    {
        #region SET

        public static bool Update(int type, string listOfPriority)
        {
            bool returnValue = false;
            using (var db = new CmsMainDb())
            {
                returnValue = db.BoxAutoModelEmbedMainDal.Update(type, listOfPriority);
            }
            return returnValue;
        }
        #endregion

        #region GET

        public static List<AutoModelEntity> Search(int type)
        {
            List<AutoModelEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.BoxAutoModelEmbedMainDal.Search(type);
            }
            return returnValue;
        }
        #endregion
    }
}
