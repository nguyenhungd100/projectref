﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using ChannelVN.CMS.Common;
using ChannelVN.Kenh14.Dal.Common;
using ChannelVN.Kenh14.Entity;
using ChannelVN.Kenh14.MainDal.Databases;

namespace ChannelVN.Kenh14.Dal
{
    public class StickerDal
    {
        #region Get methods
        public static List<StickerEntity> GetStickerByListOfId(string listStickerId)
        {
            List<StickerEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.StickerMainDal.GetStickerByListOfId(listStickerId);
            }
            return returnValue;
        }

        public static StickerEntity GetStickerByStickerId(long stickerId)
        {
            StickerEntity returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.StickerMainDal.GetStickerByStickerId(stickerId);
            }
            return returnValue;
        }

        public static List<StickerEntity> SearchSticker(string keyword, int pageIndex, int pageSize, ref int totalRow)
        {
            List<StickerEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.StickerMainDal.SearchSticker(keyword,  pageIndex, pageSize, ref  totalRow);
            }
            return returnValue;
        }

        public static List<StickerEntity> GetListStickerByNewsId(long newsId)
        {
            List<StickerEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.StickerMainDal.GetListStickerByNewsId(newsId);
            }
            return returnValue;
        }
        #endregion

        #region Update methods
        public static bool Insert(StickerEntity sticker, ref int stickerId)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.StickerMainDal.Insert(sticker, ref  stickerId);
            }
            return returnValue;
        }

        public static bool Update(StickerEntity sticker)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.StickerMainDal.Update(sticker);
            }
            return returnValue;
        }
        public static bool DeleteById(long stickerId)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.StickerMainDal.DeleteById(stickerId);
            }
            return returnValue;
        }
        public static bool DeleteStickerNewsById(string stickerName, long newsId)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.StickerMainDal.DeleteStickerNewsById(stickerName, newsId);
            }
            return returnValue;
        }
        /// <summary>
        /// Thêm mới thông tin sticker vào bảng stickerNews
        /// </summary>
        /// <param name="newsIds"></param>
        /// <param name="stickerId"></param>
        /// <param name="stickerMode"></param>
        /// <returns></returns>
        public static bool AddNews(string newsIds, long stickerId)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.StickerMainDal.AddNews(newsIds, stickerId);
            }
            return returnValue;
        }
        #endregion
    }
}
