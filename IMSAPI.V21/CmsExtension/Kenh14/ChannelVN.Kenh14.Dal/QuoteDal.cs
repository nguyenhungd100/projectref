﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using ChannelVN.CMS.Common;
using ChannelVN.Kenh14.Dal.Common;
using ChannelVN.Kenh14.Entity;
using ChannelVN.Kenh14.MainDal.Databases;

namespace ChannelVN.Kenh14.Dal
{
    public class QuoteDal
    {
        #region Quote
        #region SET
        public static bool Insert(QuoteEntity quoteEntity, ref int quoteId)
        {
            bool returnValue = false;
            using (var db = new CmsMainDb())
            {
                returnValue = db.QuoteMainDal.Insert(quoteEntity, ref quoteId);
            }
            return returnValue;
        }
        public static bool Update(QuoteEntity commentUser)
        {
            bool returnValue = false;
            using (var db = new CmsMainDb())
            {
                returnValue = db.QuoteMainDal.Update(commentUser);
            }
            return returnValue;
        }
        public static bool DeleteById(long tagId)
        {
            bool returnValue = false;
            using (var db = new CmsMainDb())
            {
                returnValue = db.QuoteMainDal.DeleteById(tagId);
            }
            return returnValue;
        }
        #endregion

        #region GET
        public static List<QuoteEntity> Search(string keyword, int pageIndex, int pageSize, ref int totalRow)
        {
            List<QuoteEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.QuoteMainDal.Search(keyword, pageIndex, pageSize, ref totalRow);
            }
            return returnValue;
        }        public static QuoteEntity GetById(long id)
        {
            QuoteEntity returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.QuoteMainDal.GetById(id);
            }
            return returnValue;
        }
        public static List<QuoteEntity> InitESAllQuote(int pageIndex, int pageSize, DateTime fromDate, DateTime toDate, ref int totalRow)
        {
            List<QuoteEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.QuoteMainDal.InitESAllQuote(pageIndex, pageSize, fromDate, toDate, ref totalRow);
            }
            return returnValue;
        }
        #endregion
        #endregion

        #region CharacterInfo
        #region SET
        public static bool InsertCharacterInfo(CharacterInfoEntity quoteEntity, ref int quoteId)
        {
            bool returnValue = false;
            using (var db = new CmsMainDb())
            {
                returnValue = db.QuoteMainDal.InsertCharacterInfo(quoteEntity, ref quoteId);
            }
            return returnValue;
        }
        public static bool UpdateCharacterInfo(CharacterInfoEntity commentUser)
        {
            bool returnValue = false;
            using (var db = new CmsMainDb())
            {
                returnValue = db.QuoteMainDal.UpdateCharacterInfo(commentUser);
            }
            return returnValue;
        }
        public static bool DeleteCharacterInfoById(int id)
        {
            bool returnValue = false;
            using (var db = new CmsMainDb())
            {
                returnValue = db.QuoteMainDal.DeleteCharacterInfoById(id);
            }
            return returnValue;
        }
        #endregion

        #region GET
        public static List<CharacterInfoEntity> SearchCharacterInfo(string keyword, int status, int pageIndex, int pageSize, ref int totalRow)
        {
            List<CharacterInfoEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.QuoteMainDal.SearchCharacterInfo(keyword, status, pageIndex, pageSize, ref totalRow);
            }
            return returnValue;
        }
        public static CharacterInfoEntity GetCharacterInfoById(int id)
        {
            CharacterInfoEntity returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.QuoteMainDal.GetCharacterInfoById(id);
            }
            return returnValue;
        }
        public static List<CharacterInfoEntity> InitESAllCharacterInfo(int pageIndex, int pageSize, DateTime fromDate, DateTime toDate, ref int totalRow)
        {
            List<CharacterInfoEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.QuoteMainDal.InitESAllCharacterInfo(pageIndex, pageSize, fromDate, toDate, ref totalRow);
            }
            return returnValue;
        }
        #endregion
        #endregion
    }
}
