﻿using System;
using System.Collections.Generic;
using System.Linq;
using ChannelVN.CMS.Common;
using ChannelVN.Kenh14.Entity;
using ChannelVN.Kenh14.Entity.ErrorCode;
using ChannelVN.Kenh14.MainDal;
using ChannelVN.Kenh14.MainDal.Databases;

namespace ChannelVN.Kenh14.Dal
{
    public class K14EmbedDal
    {
        public static ErrorMapping.ErrorCodes Insert(BoxBottomEmbedEntity entity, ref int newsAuthorId)
        {
            try
            {
                bool returnValue = false;
                using (var db = new CmsMainDb())
                {
                    returnValue = db.K14EmbedMainDal.Insert(entity, ref newsAuthorId);
                }
                if (returnValue)
                {
                    return ErrorMapping.ErrorCodes.Success;
                }
                return ErrorMapping.ErrorCodes.BusinessError;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return ErrorMapping.ErrorCodes.BusinessError;
            }
        }

        public static ErrorMapping.ErrorCodes Update(BoxBottomEmbedEntity entity)
        {
            try
            {
                bool returnValue = false;
                using (var db = new CmsMainDb())
                {
                    returnValue = db.K14EmbedMainDal.Update(entity);
                }
                if (returnValue)
                {
                    return ErrorMapping.ErrorCodes.Success;
                }
                return ErrorMapping.ErrorCodes.BusinessError;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return ErrorMapping.ErrorCodes.BusinessError;
            }
        }

        public static ErrorMapping.ErrorCodes Delete(int id)
        {
            try
            {
                bool returnValue = false;
                using (var db = new CmsMainDb())
                {
                    returnValue = db.K14EmbedMainDal.Delete(id);
                }
                if (returnValue)
                {
                    return ErrorMapping.ErrorCodes.Success;
                }
                return ErrorMapping.ErrorCodes.BusinessError;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return ErrorMapping.ErrorCodes.BusinessError;
            }
        }

        public static BoxBottomEmbedEntity GetById(int id)
        {
            try
            {
                BoxBottomEmbedEntity returnValue;
                using (var db = new CmsMainDb())
                {
                    returnValue = db.K14EmbedMainDal.GetById(id);
                }
                return returnValue;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return new BoxBottomEmbedEntity();
            }
        }

        public static List<BoxBottomEmbedEntity> GetAll()
        {
            try
            {
                List<BoxBottomEmbedEntity> returnValue;
                using (var db = new CmsMainDb())
                {
                    returnValue = db.K14EmbedMainDal.GetAll();
                }
                return returnValue;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return new List<BoxBottomEmbedEntity>();
            }
        }

        public static List<BoxBottomEmbedEntity> Search(string keyword)
        {
            try
            {
                List<BoxBottomEmbedEntity> returnValue;
                using (var db = new CmsMainDb())
                {
                    returnValue = db.K14EmbedMainDal.Search(keyword);
                }
                return returnValue;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return new List<BoxBottomEmbedEntity>();
            }
        }
    }
}
