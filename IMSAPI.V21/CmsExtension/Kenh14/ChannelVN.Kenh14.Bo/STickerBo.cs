﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ChannelVN.CMS.Common;
using ChannelVN.Kenh14.Dal;
using ChannelVN.Kenh14.Entity;
using ChannelVN.Kenh14.Entity.ErrorCode;
using ChannelVN.WcfExtensions;

namespace ChannelVN.Kenh14.Bo
{
    public class StickerBo
    {
        public static List<StickerEntity> SearchSticker(string keyword, int pageIndex, int pageSize, ref int totalRow)
        {
            try
            {
                return StickerDal.SearchSticker(keyword, pageIndex, pageSize, ref totalRow);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.ToString());
            }
            return new List<StickerEntity>();
        }

        public static StickerEntity GetStickerByStickerId(long stickerId)
        {
            var sticker = StickerDal.GetStickerByStickerId(stickerId);
            return sticker;
        }
        
        public static ErrorMapping.ErrorCodes InsertSticker(StickerEntity sticker, ref int newStickerId)
        {
            return StickerDal.Insert(sticker, ref newStickerId) ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.UnknowError;
        }
        
        public static ErrorMapping.ErrorCodes Update(StickerEntity sticker)
        {
            return StickerDal.Update(sticker) ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.UnknowError;
        }
        
        public static ErrorMapping.ErrorCodes DeleteById(long stickerId)
        {
            return StickerDal.DeleteById(stickerId) ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.UnknowError;
        }
        
        public static ErrorMapping.ErrorCodes AddNews(string newsIds, long stickerId)
        {
            return StickerDal.AddNews(newsIds, stickerId) ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.BusinessError;
        }
        
        public static List<StickerEntity> GetListStickerByNewsId(long newsId)
        {
            return StickerDal.GetListStickerByNewsId(newsId);
        }
    }
}
