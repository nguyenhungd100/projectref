﻿using System.Collections.Generic;
using System.Linq;
using ChannelVN.Kenh14.Dal;
using ChannelVN.Kenh14.Entity;
using ChannelVN.Kenh14.Entity.ErrorCode;

namespace ChannelVN.Kenh14.Bo
{
    public class MediaAlbumBo
    {
        public static List<MediaAlbumEntity> SearchMediaAlbum(string keyword, int zoneId, int status, int pageIndex, int pageSize, ref int totalRow)
        {
            return MediaAlbumDal.Search(keyword, zoneId, status, pageIndex, pageSize, ref totalRow);
        }
        public static MediaAlbumForEditEntity GetMediaAlbumForEditByMediaAlbumId(int MediaAlbumId, int topPhoto)
        {
            var MediaAlbum = MediaAlbumDal.GetById(MediaAlbumId);
            if (MediaAlbum != null)
            {
                return new MediaAlbumForEditEntity
                    {
                        MediaAlbum = MediaAlbum,
                        ListMediaAlbumDetail = MediaAlbumDetailDal.GetMediaAlbumDetailByAlbumId(MediaAlbumId)
                    };
            }
            return null;
        }

        public static ErrorMapping.ErrorCodes CreateMediaAlbum(MediaAlbumForEditEntity MediaAlbumForEdit)
        {
            if (MediaAlbumForEdit == null || MediaAlbumForEdit.MediaAlbum == null)
            {
                return ErrorMapping.ErrorCodes.BusinessError;
            }
            if (string.IsNullOrEmpty(MediaAlbumForEdit.MediaAlbum.Name))
            {
                return ErrorMapping.ErrorCodes.BusinessError;
            }
            //MediaAlbumForEdit.MediaAlbum.CreatedBy = WcfExtensions.WcfMessageHeader.Current.ClientUsername;
            MediaAlbumForEdit.MediaAlbum.Status = 1;

            var newMediaAlbumId = 0;
            if (MediaAlbumDal.Insert(MediaAlbumForEdit.MediaAlbum, ref newMediaAlbumId))
            {
                if (newMediaAlbumId > 0)
                {
                    foreach (var MediaAlbumDetail in MediaAlbumForEdit.ListMediaAlbumDetail)
                    {
                        MediaAlbumDetail.AlbumId = newMediaAlbumId;
                        MediaAlbumDetail.Status = 1;

                        var newMediaAlbumDetailId = 0;
                        MediaAlbumDetailDal.InsertMediaAlbumDetail(MediaAlbumDetail, "", ref newMediaAlbumDetailId);
                    }
                }
                return ErrorMapping.ErrorCodes.Success;
            }
            return ErrorMapping.ErrorCodes.BusinessError;
        }
        public static ErrorMapping.ErrorCodes UpdateMediaAlbum(MediaAlbumForEditEntity MediaAlbumForEdit)
        {
            if (MediaAlbumForEdit == null || MediaAlbumForEdit.MediaAlbum == null || MediaAlbumForEdit.MediaAlbum.Id <= 0)
            {
                return ErrorMapping.ErrorCodes.BusinessError;
            }
            var MediaAlbumId = MediaAlbumForEdit.MediaAlbum.Id;
            var currentMediaAlbum = MediaAlbumDal.GetById(MediaAlbumId);
            if (currentMediaAlbum == null)
            {
                return ErrorMapping.ErrorCodes.BusinessError;
            }
            if (string.IsNullOrEmpty(MediaAlbumForEdit.MediaAlbum.Name))
            {
                return ErrorMapping.ErrorCodes.BusinessError;
            }
            //MediaAlbumForEdit.MediaAlbum.LastModifiedBy = WcfExtensions.WcfMessageHeader.Current.ClientUsername;

            if (MediaAlbumDal.Update(MediaAlbumForEdit.MediaAlbum))
            {
                var listMediaAlbumDetailIds = "";
                foreach (var MediaAlbumDetail in MediaAlbumForEdit.ListMediaAlbumDetail)
                {
                    if (MediaAlbumDetail.Id > 0)
                    {
                        MediaAlbumDetail.AlbumId = MediaAlbumId;
                        MediaAlbumDetailDal.UpdateMediaAlbumDetail(MediaAlbumDetail, "");
                        listMediaAlbumDetailIds += ";" + MediaAlbumDetail.Id;
                    }
                    else
                    {
                        MediaAlbumDetail.AlbumId = MediaAlbumId;
                        MediaAlbumDetail.Status = 1;

                        var newMediaAlbumDetailId = 0;
                        if (MediaAlbumDetailDal.InsertMediaAlbumDetail(MediaAlbumDetail, "", ref newMediaAlbumDetailId))
                        {
                            listMediaAlbumDetailIds += ";" + newMediaAlbumDetailId;
                        }
                    }
                }
                //MediaAlbumDetailDal.DeleteByMediaAlbumIdExcudeListIds(MediaAlbumId, listMediaAlbumDetailIds);
                return ErrorMapping.ErrorCodes.Success;
            }
            return ErrorMapping.ErrorCodes.BusinessError;
        }
        public static ErrorMapping.ErrorCodes DeleteMediaAlbum(int MediaAlbumId)
        {
            if (MediaAlbumId <= 0)
            {
                return ErrorMapping.ErrorCodes.BusinessError;
            }
            var currentMediaAlbum = MediaAlbumDal.GetById(MediaAlbumId);
            if (currentMediaAlbum == null)
            {
                return ErrorMapping.ErrorCodes.BusinessError;
            }

            MediaAlbumDal.Delete(MediaAlbumId);
            return ErrorMapping.ErrorCodes.Success;
        }
        public static ErrorMapping.ErrorCodes DeleteMediaAlbumDetail(int id)
        {
            if (id <= 0)
            {
                return ErrorMapping.ErrorCodes.BusinessError;
            }

            MediaAlbumDetailDal.DeleteMediaAlbumDetail(id);
            return ErrorMapping.ErrorCodes.Success;
        }
        public static MediaAlbumDetailEntity GetMediaAlbumDetailForEditById(int MediaAlbumId)
        {
            var MediaAlbum = MediaAlbumDetailDal.GetMediaAlbumDetailById(MediaAlbumId);
            if (MediaAlbum != null)
            {
                return MediaAlbum;
            }
            return null;
        }
        public static ErrorMapping.ErrorCodes CreateMediaAlbumDetail(MediaAlbumDetailEntity MediaAlbumForEdit)
        {
            var newMediaAlbumId = 0;
            if (MediaAlbumDetailDal.InsertMediaAlbumDetail(MediaAlbumForEdit, string.Empty, ref newMediaAlbumId))
            {
                return ErrorMapping.ErrorCodes.Success;
            }
            return ErrorMapping.ErrorCodes.BusinessError;
        }
        public static ErrorMapping.ErrorCodes UpdateMediaAlbumDetail(MediaAlbumDetailEntity MediaAlbumForEdit)
        {

            if (MediaAlbumDetailDal.UpdateMediaAlbumDetail(MediaAlbumForEdit, string.Empty))
            {
                return ErrorMapping.ErrorCodes.Success;
            }
            return ErrorMapping.ErrorCodes.BusinessError;
        }
    }
}
