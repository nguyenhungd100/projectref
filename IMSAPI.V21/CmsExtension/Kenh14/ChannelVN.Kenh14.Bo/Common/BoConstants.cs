﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ChannelVN.CMS.Common.ChannelConfig;
using ChannelVN.WcfExtensions;

namespace ChannelVN.Kenh14.Bo.Common
{
    public class BoConstants
    {
        public const string TRANSFER_MARKET_NEWS_FORMAT_ID = "yyyyMMddHHmmssFFF";

        public static string TagFormatJson
        {
            get { return ServiceChannelConfiguration.GetAppSetting(WcfMessageHeader.Current.Namespace, "TagFormatJson"); }
        }

        public static string RelationFormatJson
        {
            get { return ServiceChannelConfiguration.GetAppSetting(WcfMessageHeader.Current.Namespace, "RelationFormatJson"); }
        }
    }
}
