﻿using System;
using System.Collections.Generic;
using System.Linq;
using ChannelVN.Kenh14.Entity;
using ChannelVN.Kenh14.Entity.ErrorCode;
using ChannelVN.CMS.BoSearch.Entity;
using ChannelVN.CMS.BoSearch.Extension.Kenh14Extension;
using ChannelVN.CMS.Common;

namespace ChannelVN.Kenh14.Bo
{
    public class QuoteBo
    {
        #region Quote
        public static List<QuoteSearchEntity> Search(string keyword, int position, int pageIndex, int pageSize, ref int totalRow)
        {
            try
            {
                return QuoteDalFactory.Search(keyword, position, pageIndex, pageSize, ref totalRow);
            }
            catch
            {
                return new List<QuoteSearchEntity>();
            }
        }
        public static QuoteEntity GetById(int id)
        {
            //return Dal.QuoteDal.GetById(id);
            var quote = ChannelVN.CMS.BoCached.Extension.Kenh14Extension.QuoteDalFactory.GetQuoteById(id);
            if (quote == null)
            {
                quote = Dal.QuoteDal.GetById(id);
                if (quote != null)
                {
                    //add to redis
                    ChannelVN.CMS.BoCached.Extension.Kenh14Extension.QuoteDalFactory.AddQuote(quote);
                }
            }
            return quote;
        }
        public static ErrorMapping.ErrorCodes InsertQuote(QuoteEntity quoteEntity, ref int quoteId)
        {
            try
            {
                var inserted = Dal.QuoteDal.Insert(quoteEntity, ref quoteId);
                if (inserted)
                {
                    quoteEntity.QuoteId = quoteId;
                    //add es
                    CMS.BoSearch.Extension.Kenh14Extension.QuoteDalFactory.AddQuote(quoteEntity);
                    //add redis
                    CMS.BoCached.Extension.Kenh14Extension.QuoteDalFactory.AddQuote(quoteEntity);

                }
                return inserted ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.BusinessError;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return ErrorMapping.ErrorCodes.BusinessError;
            }

            //return Dal.QuoteDal.Insert(quoteEntity)
            //           ? ErrorMapping.ErrorCodes.Success
            //           : ErrorMapping.ErrorCodes.BusinessError;
        }
        public static ErrorMapping.ErrorCodes UpdateQuote(QuoteEntity quoteEntity)
        {
            try
            {
                var inserted = Dal.QuoteDal.Update(quoteEntity);
                if (inserted)
                {
                    //add es
                    CMS.BoSearch.Extension.Kenh14Extension.QuoteDalFactory.UpdateQuote(quoteEntity);
                    //add redis
                    CMS.BoCached.Extension.Kenh14Extension.QuoteDalFactory.AddQuote(quoteEntity);
                }
                return inserted ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.UnknowError;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return ErrorMapping.ErrorCodes.BusinessError;
            }

            //return Dal.QuoteDal.Update(commentUser)
            //           ? ErrorMapping.ErrorCodes.Success
            //           : ErrorMapping.ErrorCodes.BusinessError;
        }
        public static ErrorMapping.ErrorCodes DeleteById(long tagId)
        {
            return Dal.QuoteDal.DeleteById(tagId) ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.UnknowError;
        }

        public static CMS.BoCached.Entity.Init.LogInitAsyncEntity InitEsByQuoteAsync(string name, DateTime startDate, DateTime endDate, int pageSize, string action = null)
        {
            var totalRows = 0;
            return CMS.BoCached.Common.Queue.InitSyncData(name, 1, startDate, endDate, () =>
            {
                var data = Dal.QuoteDal.InitESAllQuote(1, pageSize, startDate, endDate, ref totalRows);
                var totalPages = (int)Math.Ceiling((double)totalRows / pageSize);
                return totalPages;
            }, (page) =>
            {
                var data = Dal.QuoteDal.InitESAllQuote(page, pageSize, startDate, endDate, ref totalRows);
                var list = data.Select(s => new QuoteSearchEntity
                {
                    Id = s.QuoteId,
                    QuoteTitle = s.QuoteTitle,
                    Quote = s.Quote,
                    Avatar = s.Avatar,
                    Position=s.Position
                }).ToList();
                CMS.BoSearch.Extension.Kenh14Extension.QuoteDalFactory.InitAllQuote(list);
            }, action);
        }

        public static CMS.BoCached.Entity.Init.LogInitAsyncEntity InitRedisAllQuote(string name, DateTime startDate, DateTime endDate, int pageSize, string action = null)
        {
            var totalRows = 0;
            return CMS.BoCached.Common.Queue.InitSyncData(name, 1, startDate, endDate, () =>
            {
                var data = Dal.QuoteDal.InitESAllQuote(1, pageSize, startDate, endDate, ref totalRows);
                var totalPages = (int)Math.Ceiling((double)totalRows / pageSize);
                return totalPages;
            }, (page) =>
            {
                var data = Dal.QuoteDal.InitESAllQuote(page, pageSize, startDate, endDate, ref totalRows);
                CMS.BoCached.Extension.Kenh14Extension.QuoteDalFactory.InitAllQuote(data);
            }, action);
        }

        #endregion

        #region CharacterInfo
        public static List<CharacterInfoSearchEntity> SearchCharacterInfo(string keyword, int status, int pageIndex, int pageSize, ref int totalRow)
        {
            try
            {
                var data = QuoteDalFactory.SearchCharacterInfo(keyword, status, pageIndex, pageSize, ref totalRow);
                if (data!=null && data.Count == 0)
                {
                    var result= Dal.QuoteDal.SearchCharacterInfo(keyword, status, pageIndex, pageSize, ref totalRow);
                    data = new List<CharacterInfoSearchEntity>();
                    if (result != null)
                    {
                        data.AddRange(result.Select(s => new CharacterInfoSearchEntity
                        {
                            Id=s.Id,
                            Name=s.Name,
                            Avatar=s.Avatar,
                            Description=s.Description,
                            Quote=s.Quote,
                            BackgroundColor=s.BackgroundColor,
                            BorderColor=s.BorderColor,
                            Status=s.Status
                        }));
                    }
                }

                return data;
            }
            catch
            {
                return new List<CharacterInfoSearchEntity>();
            }
        }
        public static CharacterInfoEntity GetCharacterInfoById(int id)
        {
            //return Dal.QuoteDal.GetById(id);
            var quote = CMS.BoCached.Extension.Kenh14Extension.QuoteDalFactory.GetCharacterInfoById(id);
            if (quote == null)
            {
                quote = Dal.QuoteDal.GetCharacterInfoById(id);
                if (quote != null)
                {
                    //add to redis
                    CMS.BoCached.Extension.Kenh14Extension.QuoteDalFactory.AddCharacterInfo(quote);
                }
            }
            return quote;
        }
        public static ErrorMapping.ErrorCodes InsertCharacterInfo(CharacterInfoEntity quoteEntity, ref int quoteId)
        {
            try
            {
                var inserted = Dal.QuoteDal.InsertCharacterInfo(quoteEntity, ref quoteId);
                if (inserted)
                {
                    quoteEntity.Id = quoteId;
                    //add es
                    QuoteDalFactory.AddCharacterInfo(quoteEntity);
                    //add redis
                    CMS.BoCached.Extension.Kenh14Extension.QuoteDalFactory.AddCharacterInfo(quoteEntity);

                }
                return inserted ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.BusinessError;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return ErrorMapping.ErrorCodes.BusinessError;
            }

            //return Dal.QuoteDal.Insert(quoteEntity)
            //           ? ErrorMapping.ErrorCodes.Success
            //           : ErrorMapping.ErrorCodes.BusinessError;
        }
        public static ErrorMapping.ErrorCodes UpdateCharacterInfo(CharacterInfoEntity quoteEntity)
        {
            try
            {
                var inserted = Dal.QuoteDal.UpdateCharacterInfo(quoteEntity);
                if (inserted)
                {
                    //add es
                    QuoteDalFactory.UpdateCharacterInfo(quoteEntity);
                    //add redis
                    CMS.BoCached.Extension.Kenh14Extension.QuoteDalFactory.AddCharacterInfo(quoteEntity);
                }
                return inserted ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.UnknowError;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return ErrorMapping.ErrorCodes.BusinessError;
            }

            //return Dal.QuoteDal.Update(commentUser)
            //           ? ErrorMapping.ErrorCodes.Success
            //           : ErrorMapping.ErrorCodes.BusinessError;
        }
        public static ErrorMapping.ErrorCodes DeleteCharacterInfoById(int id)
        {
            var result= Dal.QuoteDal.DeleteCharacterInfoById(id) ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.UnknowError;
            if (result == ErrorMapping.ErrorCodes.Success)
            {
                CMS.BoSearch.Extension.Kenh14Extension.QuoteDalFactory.DeleteCharacterInfoById(id);
                CMS.BoCached.Extension.Kenh14Extension.QuoteDalFactory.DeleteCharacterInfoById(id);
            }
            return result;
        }

        public static CMS.BoCached.Entity.Init.LogInitAsyncEntity InitEsByCharacterInfoAsync(string name, DateTime startDate, DateTime endDate, int pageSize, string action = null)
        {
            var totalRows = 0;
            return CMS.BoCached.Common.Queue.InitSyncData(name, 1, startDate, endDate, () =>
            {
                var data = Dal.QuoteDal.InitESAllCharacterInfo(1, pageSize, startDate, endDate, ref totalRows);
                var totalPages = (int)Math.Ceiling((double)totalRows / pageSize);
                return totalPages;
            }, (page) =>
            {
                var data = Dal.QuoteDal.InitESAllCharacterInfo(page, pageSize, startDate, endDate, ref totalRows);
                var list = data.Select(s => new CharacterInfoSearchEntity
                {
                    Id = s.Id,
                    Name = s.Name,
                    Avatar = s.Avatar,
                    Description = s.Description,
                    Quote = s.Quote,
                    BackgroundColor = s.BackgroundColor,
                    BorderColor = s.BorderColor,
                    Status = s.Status
                }).ToList();
                CMS.BoSearch.Extension.Kenh14Extension.QuoteDalFactory.InitAllCharacterInfo(list);
            }, action);
        }

        public static CMS.BoCached.Entity.Init.LogInitAsyncEntity InitRedisAllCharacterInfo(string name, DateTime startDate, DateTime endDate, int pageSize, string action = null)
        {
            var totalRows = 0;
            return CMS.BoCached.Common.Queue.InitSyncData(name, 1, startDate, endDate, () =>
            {
                var data = Dal.QuoteDal.InitESAllCharacterInfo(1, pageSize, startDate, endDate, ref totalRows);
                var totalPages = (int)Math.Ceiling((double)totalRows / pageSize);
                return totalPages;
            }, (page) =>
            {
                var data = Dal.QuoteDal.InitESAllCharacterInfo(page, pageSize, startDate, endDate, ref totalRows);
                CMS.BoCached.Extension.Kenh14Extension.QuoteDalFactory.InitAllCharacterInfo(data);
            }, action);
        }

        #endregion
    }
}
