﻿using System;
using System.Collections.Generic;
using ChannelVN.CMS.Common;
using ChannelVN.Kenh14.Dal;
using ChannelVN.Kenh14.Entity;
using ChannelVN.Kenh14.Entity.ErrorCode;

namespace ChannelVN.Kenh14.Bo
{
    public class K14EmbedBo
    {
        public static ErrorMapping.ErrorCodes Insert(BoxBottomEmbedEntity entity, ref int k14EmbedId)
        {
            try
            {
                var inserted = K14EmbedDal.Insert(entity, ref k14EmbedId);
                return inserted;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return ErrorMapping.ErrorCodes.BusinessError;
            }
        }
        public static ErrorMapping.ErrorCodes Update(BoxBottomEmbedEntity entity)
        {
            try
            {
                var inserted = K14EmbedDal.Update(entity);
                return inserted;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return ErrorMapping.ErrorCodes.BusinessError;
            }
        }

        public static ErrorMapping.ErrorCodes Delete(int id)
        {
            try
            {
                var inserted = K14EmbedDal.Delete(id);
                return inserted;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return ErrorMapping.ErrorCodes.BusinessError;
            }
        }

        public static BoxBottomEmbedEntity GetById(int id)
        {
            try
            {
                return K14EmbedDal.GetById(id);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return new BoxBottomEmbedEntity();
            }
        }

        public static List<BoxBottomEmbedEntity> GetAll()
        {
            try
            {
                return K14EmbedDal.GetAll();
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return new List<BoxBottomEmbedEntity>();
            }
        }

        public static List<BoxBottomEmbedEntity> Search(string keyword)
        {
            try
            {
                return K14EmbedDal.Search(keyword);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return new List<BoxBottomEmbedEntity>();
            }
        }

    }
}
