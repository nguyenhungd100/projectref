﻿using System;
using System.Runtime.Serialization;
using ChannelVN.CMS.Common;

namespace ChannelVN.Kenh14.Entity
{
    [DataContract]
    public class QuoteEntity : EntityBase
    {
        [DataMember]
        public int QuoteId { set; get; }
        [DataMember]
        public string Quote { set; get; }
        [DataMember]
        public string QuoteTitle { set; get; }
        [DataMember]
        public string Avatar { set; get; }
        [DataMember]
        public int Position { set; get; }
    }

    [DataContract]
    public class CharacterInfoEntity : EntityBase
    {
        [DataMember]
        public int Id { set; get; }
        [DataMember]
        public string Name { set; get; }
        [DataMember]
        public string Avatar { set; get; }
        [DataMember]
        public string Description { set; get; }
        [DataMember]
        public string Quote { set; get; }
        [DataMember]
        public string BackgroundColor { set; get; }
        [DataMember]
        public string BorderColor { set; get; }
        [DataMember]
        public int Status { set; get; }
        [DataMember]
        public DateTime CreatedDate { set; get; }
        [DataMember]
        public string CreatedBy { set; get; }
        [DataMember]
        public DateTime ModifiedDate { set; get; }
        [DataMember]
        public string ModifiedBy { set; get; }        
    }
}
