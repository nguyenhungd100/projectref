﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using ChannelVN.CMS.Common;

namespace ChannelVN.Kenh14.Entity
{
    [DataContract]
    public class MediaAlbumEntity : EntityBase
    {
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public int ZoneId { get; set; }
        [DataMember]
        public string Name { get; set; }
        [DataMember]
        public string Avatar { get; set; }
        [DataMember]
        public string CreatedBy { get; set; }
        [DataMember]
        public DateTime CreatedDate { get; set; }
        [DataMember]
        public string LastModifiedBy { get; set; }
        [DataMember]
        public DateTime LastModifiedDate { get; set; }
        [DataMember]
        public int Status { get; set; }
        [DataMember]
        public DateTime StartDate { get; set; }
        [DataMember]
        public DateTime ExpireDate { get; set; }
    }

    [DataContract]
    public class MediaAlbumForEditEntity : EntityBase
    {
        [DataMember]
        public MediaAlbumEntity MediaAlbum { get; set; }
        [DataMember]
        public List<MediaAlbumDetailEntity> ListMediaAlbumDetail { get; set; }
    }
}
