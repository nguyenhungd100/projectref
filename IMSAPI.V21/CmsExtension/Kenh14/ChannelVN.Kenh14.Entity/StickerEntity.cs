﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using ChannelVN.CMS.Common;

namespace ChannelVN.Kenh14.Entity
{
    [DataContract]
    public class StickerEntity : EntityBase
    {
        [DataMember]
        public int Id { set; get; }
        [DataMember]
        public string StickerName { set; get; }
        [DataMember]
        public string Avatar { set; get; }
        [DataMember]
        public int Status { set; get; }
        [DataMember]
        public long NewsId { set; get; }
    }

    [DataContract]
    public class StickerInNewsEntity : EntityBase
    {
        [DataMember]
        public int Id { set; get; }
        [DataMember]
        public long NewsId { set; get; }
        [DataMember]
        public int StickerId { set; get; }
    }
}
