﻿using System;
using System.Runtime.Serialization;
using ChannelVN.CMS.Common;

namespace ChannelVN.Kenh14.Entity
{
    [DataContract]
    public class K14EmbedEntity : EntityBase
    {
        [DataMember]
        public int Id { set; get; }
        [DataMember]
        public string Name { set; get; }
        [DataMember]
        public string Code { set; get; }
        [DataMember]
        public string CategoryId { set; get; }
        [DataMember]
        public bool IsActive { set; get; }
        [DataMember]
        public DateTime EndDate { set; get; }
        [DataMember]
        public int Status { set; get; }
    }
    [DataContract]
    public class BoxBottomEmbedEntity : EntityBase
    {
        [DataMember]
        public int Id { set; get; }
        [DataMember]
        public string Name { set; get; }
        [DataMember]
        public string Code { set; get; }
        [DataMember]
        public string CategoryId { set; get; }
        [DataMember]
        public bool IsActive { set; get; }
        [DataMember]
        public DateTime EndDate { set; get; }
        [DataMember]
        public int Status { set; get; }
    }
}
