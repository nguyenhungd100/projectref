﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using ChannelVN.CMS.Common;
using ChannelVN.Kenh14.Entity;
using ChannelVN.Kenh14.MainDal.Databases;

namespace ChannelVN.Kenh14.MainDal
{
    public abstract class StickerDalBase
    {
        #region Get methods
        public List<StickerEntity> GetStickerByListOfId(string listStickerId)
        {
            const string commandText = "CMS_Sticker_GetStickerByIds";
            try
            {
                List<StickerEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Ids", listStickerId);
                data = _db.GetList<StickerEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public StickerEntity GetStickerByStickerId(long StickerId)
        {
            const string commandText = "CMS_Sticker_GetStickerByStickerID";
            try
            {
                StickerEntity data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", StickerId);
                data = _db.Get<StickerEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public List<StickerEntity> SearchSticker(string keyword,  int pageIndex, int pageSize, ref int totalRow)
        {
            const string commandText = "CMS_Sticker_Search";
            try
            {
                List<StickerEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "TotalRow", totalRow, ParameterDirection.Output);
                _db.AddParameter(cmd, "Keyword", keyword);
                _db.AddParameter(cmd, "PageIndex", pageIndex);
                _db.AddParameter(cmd, "PageSize", pageSize);
                data = _db.GetList<StickerEntity>(cmd);
                totalRow = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 0));
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public List<StickerEntity> GetListStickerByNewsId(long newsId)
        {
            const string commandText = "CMS_StickerNews_GetByNewsId";
            try
            {
                List<StickerEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "newsId", newsId);

                data = _db.GetList<StickerEntity>(cmd);
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        #endregion

        #region Update methods
        public bool Insert(StickerEntity Sticker, ref int StickerId)
        {

            const string commandText = "CMS_Sticker_Insert";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", 0, ParameterDirection.Output);
                _db.AddParameter(cmd, "StickerName", Sticker.StickerName);
                _db.AddParameter(cmd, "Avatar", Sticker.Avatar);
                _db.AddParameter(cmd, "Status", Sticker.Status);

                var numberOfRow = cmd.ExecuteNonQuery();

                StickerId = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 0));

                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }

        }

        public bool Update(StickerEntity Sticker)
        {

            const string commandText = "CMS_Sticker_Update";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", Sticker.Id);
                _db.AddParameter(cmd, "StickerName", Sticker.StickerName);
                _db.AddParameter(cmd, "Avatar", Sticker.Avatar);
                _db.AddParameter(cmd, "Status", Sticker.Status);

                var numberOfRow = cmd.ExecuteNonQuery();
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public bool DeleteById(long StickerId)
        {
            const string commandText = "CMS_Sticker_DeleteByStickerID";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", StickerId);
                var numberOfRow = cmd.ExecuteNonQuery();
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }

        }
        public bool DeleteStickerNewsById(string StickerName, long newsId)
        {
            const string commandText = "SYNCV2_CMS_Sticker_DeleteStickerNewByStickerID";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "StickerName", StickerName);
                _db.AddParameter(cmd, "newsId", newsId);
                var numberOfRow = cmd.ExecuteNonQuery();
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }

        }

        /// <summary>
        /// Thêm mới thông tin Sticker vào bảng StickerNews
        /// </summary>
        /// <param name="newsIds"></param>
        /// <param name="StickerId"></param>
        /// <param name="StickerMode"></param>
        /// <returns></returns>
        public bool AddNews(string newsIds, long StickerId)
        {
            const string commandText = "CMS_Sticker_AddNews";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "NewsIds", newsIds);
                _db.AddParameter(cmd, "StickerId", StickerId);
                var numberOfRow = cmd.ExecuteNonQuery();
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        #endregion

        #region Core members

        private readonly CmsMainDb _db;

        protected StickerDalBase(CmsMainDb db)
        {
            _db = db;
        }

        protected CmsMainDb Database
        {
            get { return _db; }
        }

        #endregion

    }
}
