﻿using System;
using System.Collections.Generic;
using System.Data;
using ChannelVN.CMS.Common;
using ChannelVN.Kenh14.Entity;
using ChannelVN.Kenh14.MainDal.Databases;

namespace ChannelVN.Kenh14.MainDal
{
    public abstract class K14EmbedDalBase
    {
        public bool Insert(BoxBottomEmbedEntity entity, ref int newsAuthorId)
        {
            const string commandText = "CMS_BoxBottomEmbed_Insert";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", 0, ParameterDirection.Output);
                _db.AddParameter(cmd, "CategoryId", entity.CategoryId);
                _db.AddParameter(cmd, "Code", entity.Code);
                _db.AddParameter(cmd, "EndDate", entity.EndDate);
                _db.AddParameter(cmd, "IsActive", entity.IsActive);
                _db.AddParameter(cmd, "Name", entity.Name);
                _db.AddParameter(cmd, "Status", entity.Status);
                bool data = _db.ExecuteNonQuery(cmd) > 0;
                newsAuthorId = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 0));
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public bool Update(BoxBottomEmbedEntity entity)
        {
            const string commandText = "CMS_BoxBottomEmbed_Update";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "CategoryId", entity.CategoryId);
                _db.AddParameter(cmd, "Code", entity.Code);
                _db.AddParameter(cmd, "EndDate", entity.EndDate);
                _db.AddParameter(cmd, "IsActive", entity.IsActive);
                _db.AddParameter(cmd, "Name", entity.Name);
                _db.AddParameter(cmd, "Status", entity.Status);
                _db.AddParameter(cmd, "Id", entity.Id);
                bool data = _db.ExecuteNonQuery(cmd) > 0;

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public bool Delete(int id)
        {
            const string commandText = "CMS_BoxBottomEmbed_DeleteById";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", id);
                bool data = _db.ExecuteNonQuery(cmd) > 0;

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        /// <summary>
        /// Delete All rows in NewsByAuthor table
        /// </summary>
        /// <param name="newsId"></param>
        /// <returns></returns>
        public bool DeleteAllNewsByAuthor(long newsId)
        {
            const string commandText = "CMS_NewsByAuthor_DeleteAll";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "NewsId", newsId);
                bool data = _db.ExecuteNonQuery(cmd) > 0;

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public BoxBottomEmbedEntity GetById(int id)
        {
            const string commandText = "CMS_BoxBottomEmbed_GetById";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", id);
                BoxBottomEmbedEntity data = _db.Get<BoxBottomEmbedEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.ToString());
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public List<BoxBottomEmbedEntity> GetAll()
        {
            const string commandText = "CMS_BoxBottomEmbed_GetAll";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                List<BoxBottomEmbedEntity> data = _db.GetList<BoxBottomEmbedEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public List<BoxBottomEmbedEntity> Search(string keyword)
        {
            const string commandText = "CMS_BoxBottomEmbed_Search";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Keyword", keyword);
                List<BoxBottomEmbedEntity> data = _db.GetList<BoxBottomEmbedEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        #region Core members

        private readonly CmsMainDb _db;

        protected K14EmbedDalBase(CmsMainDb db)
        {
            _db = db;
        }

        protected CmsMainDb Database
        {
            get { return _db; }
        }

        #endregion
    }
}
