﻿using System.Data;
using ChannelVN.CMS.Common.DalCommon;
using ChannelVN.Kenh14.MainDal;

namespace ChannelVN.Kenh14.MainDal.Databases
{
    /// <summary>
    /// The base class for the <see cref="CmsMainDb"/> class that 
    /// represents a connection to the <c>CmsMainDb</c> database. 
    /// </summary>
    /// <remarks>
    /// Do not change this source code. Modify the CmsMainDb class
    /// if you need to add or change some functionality.
    /// </remarks>
    public abstract class CmsMainDbBase : MainDbBase
    {
        #region Store procedures

        #region NewsRoyaltyAuthor
        private QuoteDal _quoteMainDal;
        public QuoteDalBase QuoteMainDal
        {
            get { return _quoteMainDal ?? (_quoteMainDal = new QuoteDal((CmsMainDb)this)); }
        }
        #endregion

        private MediaAlbumDal _mediaAlbumMainDal;
        public MediaAlbumDalBase MediaAlbumMainDal
        {
            get { return _mediaAlbumMainDal ?? (_mediaAlbumMainDal = new MediaAlbumDal((CmsMainDb)this)); }
        }

        private MediaAlbumDetailDal _mediaAlbumDetailMainDal;
        public MediaAlbumDetailDalBase MediaAlbumDetailMainDal
        {
            get { return _mediaAlbumDetailMainDal ?? (_mediaAlbumDetailMainDal = new MediaAlbumDetailDal((CmsMainDb)this)); }
        }

        private K14EmbedDal _m14EmbedMainDal;
        public K14EmbedDalBase K14EmbedMainDal
        {
            get { return _m14EmbedMainDal ?? (_m14EmbedMainDal = new K14EmbedDal((CmsMainDb)this)); }
        }

        private StickerDal _stickerMainDal;
        public StickerDalBase StickerMainDal
        {
            get { return _stickerMainDal ?? (_stickerMainDal = new StickerDal((CmsMainDb)this)); }
        }


        #endregion

        #region Constructors

        protected CmsMainDbBase()
        {
        }
        protected CmsMainDbBase(bool init)
        {
            if (init)
            {
                InitConnection();
            }
        }

        #endregion

        #region Protected members

        protected override sealed void InitConnection()
        {
            _connection = CreateConnection();
            _connection.Open();
        }

        /// <summary>
        /// Creates a new connection to the database.
        /// </summary>
        /// <returns>A reference to the <see cref="System.Data.IDbConnection"/> object.</returns>
        protected abstract IDbConnection CreateConnection();

        #endregion
    }
}