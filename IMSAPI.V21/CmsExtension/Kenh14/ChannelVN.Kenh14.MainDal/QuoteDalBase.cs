﻿using System.Data;
using ChannelVN.CMS.Common;
using System;
using System.Collections.Generic;
using ChannelVN.Kenh14.Entity;
using ChannelVN.Kenh14.MainDal.Databases;
using ChannelVN.Kenh14.MainDal.Common;

namespace ChannelVN.Kenh14.MainDal
{
    public abstract class QuoteDalBase
    {
        #region Quote
        #region function GET
        public List<QuoteEntity> Search(string keyword, int pageIndex, int pageSize, ref int totalRow)
        {
            const string commandText = "CMS_Quote_GetList";
            try
            {
                List<QuoteEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Keyword", keyword);
                _db.AddParameter(cmd, "PageIndex", pageIndex);
                _db.AddParameter(cmd, "PageSize", pageSize);
                _db.AddParameter(cmd, "TotalRows", totalRow, ParameterDirection.Output);
                data = _db.GetList<QuoteEntity>(cmd) ?? new List<QuoteEntity>();
                totalRow = Convert.ToInt32(_db.GetParameterValueFromCommand(cmd, cmd.Parameters.Count - 1));
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public QuoteEntity GetById(long id)
        {
            const string commandText = "CMS_Quote_GetById";
            try
            {
                QuoteEntity data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "quoteid", id);
                data = _db.Get<QuoteEntity>(cmd) ?? new QuoteEntity();
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public List<QuoteEntity> InitESAllQuote(int pageIndex, int pageSize, DateTime fromDate, DateTime toDate, ref int totalRow)
        {
            //const string commandText = "CMS_News_InitESAllNews";
            const string commandText = @"
                                        DECLARE @UpperBand int, @LowerBand int

                                        SELECT @totalRow = COUNT(*) FROM Quote q 

                                        SET @LowerBand  = (@pageIndex - 1) * @PageSize
                                        SET @UpperBand  = (@pageIndex * @PageSize)
                                        SELECT QuoteId,Quote,QuoteTitle,Avatar, Position FROM (
                                        SELECT q.*,ROW_NUMBER() OVER(ORDER BY q.QuoteId DESC) AS RowNumber 
                                        FROM Quote q 
                                        ) AS temp
                                        WHERE RowNumber > @LowerBand AND RowNumber <= @UpperBand";
            try
            {
                List<QuoteEntity> data = null;
                var cmd = _db.CreateCommand(commandText);
                _db.AddParameter(cmd, "TotalRow", totalRow, ParameterDirection.Output);
                _db.AddParameter(cmd, "PageIndex", pageIndex);
                _db.AddParameter(cmd, "PageSize", pageSize);
                if (fromDate <= Constants.MinDateTime)
                    _db.AddParameter(cmd, "DateFrom", DBNull.Value);
                else _db.AddParameter(cmd, "DateFrom", fromDate);
                if (toDate <= Constants.MinDateTime)
                    _db.AddParameter(cmd, "DateTo", DBNull.Value);
                else _db.AddParameter(cmd, "DateTo", toDate);
                data = _db.GetList<QuoteEntity>(cmd);

                totalRow = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 0));

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        #endregion

        #region Function SET
        public bool Insert(QuoteEntity quoteEntity, ref int quoteId)
        {
            const string commandText = "CMS_Quote_Insert";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", 0, ParameterDirection.Output);
                _db.AddParameter(cmd, "Quote", quoteEntity.Quote);
                _db.AddParameter(cmd, "QuoteTitle", quoteEntity.QuoteTitle);
                _db.AddParameter(cmd, "Avatar", quoteEntity.Avatar);
                _db.AddParameter(cmd, "Position", quoteEntity.Position);               

                bool data = _db.ExecuteNonQuery(cmd) > 0;
                quoteId = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 0));
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool Update(QuoteEntity quoteEntity)
        {
            const string commandText = "CMS_Quote_Update";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "QuoteId", quoteEntity.QuoteId);
                _db.AddParameter(cmd, "Quote", quoteEntity.Quote);
                _db.AddParameter(cmd, "QuoteTitle", quoteEntity.QuoteTitle);
                _db.AddParameter(cmd, "Avatar", quoteEntity.Avatar);
                _db.AddParameter(cmd, "Position", quoteEntity.Position);
                var result = cmd.ExecuteNonQuery();
                return result > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool DeleteById(long tagId)
        {
            const string commandText = "CMS_NewsRoyaltyAuthor_DeleteByID";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", tagId);
                var result = cmd.ExecuteNonQuery();
                return result > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        #endregion
        #endregion

        #region CharacterInfo
        #region function GET
        public List<CharacterInfoEntity> SearchCharacterInfo(string keyword, int status, int pageIndex, int pageSize, ref int totalRow)
        {
            const string commandText = "CMS_CharacterInfo_GetList";
            try
            {
                List<CharacterInfoEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Keyword", keyword);
                _db.AddParameter(cmd, "Status", status);
                _db.AddParameter(cmd, "PageIndex", pageIndex);
                _db.AddParameter(cmd, "PageSize", pageSize);
                _db.AddParameter(cmd, "TotalRows", totalRow, ParameterDirection.Output);

                data = _db.GetList<CharacterInfoEntity>(cmd) ?? new List<CharacterInfoEntity>();
                totalRow = Convert.ToInt32(_db.GetParameterValueFromCommand(cmd, cmd.Parameters.Count - 1));
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public CharacterInfoEntity GetCharacterInfoById(int id)
        {
            const string commandText = "CMS_CharacterInfo_GetById";
            try
            {
                CharacterInfoEntity data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", id);
                data = _db.Get<CharacterInfoEntity>(cmd);
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public List<CharacterInfoEntity> InitESAllCharacterInfo(int pageIndex, int pageSize, DateTime fromDate, DateTime toDate, ref int totalRow)
        {            
            const string commandText = @"
                                        DECLARE @UpperBand int, @LowerBand int

                                        SELECT @totalRow = COUNT(*) FROM CharacterInfo q 

                                        SET @LowerBand  = (@pageIndex - 1) * @PageSize
                                        SET @UpperBand  = (@pageIndex * @PageSize)
                                        SELECT * FROM (
                                        SELECT q.*,ROW_NUMBER() OVER(ORDER BY q.Id DESC) AS RowNumber 
                                        FROM CharacterInfo q 
                                        ) AS temp
                                        WHERE RowNumber > @LowerBand AND RowNumber <= @UpperBand";
            try
            {
                List<CharacterInfoEntity> data = null;
                var cmd = _db.CreateCommand(commandText);
                _db.AddParameter(cmd, "TotalRow", totalRow, ParameterDirection.Output);
                _db.AddParameter(cmd, "PageIndex", pageIndex);
                _db.AddParameter(cmd, "PageSize", pageSize);
                if (fromDate <= Constants.MinDateTime)
                    _db.AddParameter(cmd, "DateFrom", DBNull.Value);
                else _db.AddParameter(cmd, "DateFrom", fromDate);
                if (toDate <= Constants.MinDateTime)
                    _db.AddParameter(cmd, "DateTo", DBNull.Value);
                else _db.AddParameter(cmd, "DateTo", toDate);
                data = _db.GetList<CharacterInfoEntity>(cmd);

                totalRow = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 0));

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        #endregion

        #region Function SET
        public bool InsertCharacterInfo(CharacterInfoEntity quoteEntity, ref int quoteId)
        {
            const string commandText = "CMS_CharacterInfo_Insert";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", 0, ParameterDirection.Output);
                _db.AddParameter(cmd, "Name", quoteEntity.Name);
                _db.AddParameter(cmd, "Avatar", quoteEntity.Avatar);
                _db.AddParameter(cmd, "Description", quoteEntity.Description);
                _db.AddParameter(cmd, "Quote", quoteEntity.Quote);
                _db.AddParameter(cmd, "BackgroundColor", quoteEntity.BackgroundColor);
                _db.AddParameter(cmd, "BorderColor", quoteEntity.BorderColor);
                _db.AddParameter(cmd, "Status", quoteEntity.Status);
                _db.AddParameter(cmd, "CreatedBy", quoteEntity.CreatedBy);

                bool data = _db.ExecuteNonQuery(cmd) > 0;
                quoteId = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 0));
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool UpdateCharacterInfo(CharacterInfoEntity quoteEntity)
        {
            const string commandText = "CMS_CharacterInfo_Update";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", quoteEntity.Id);
                _db.AddParameter(cmd, "Name", quoteEntity.Name);
                _db.AddParameter(cmd, "Avatar", quoteEntity.Avatar);
                _db.AddParameter(cmd, "Description", quoteEntity.Description);
                _db.AddParameter(cmd, "Quote", quoteEntity.Quote);
                _db.AddParameter(cmd, "BackgroundColor", quoteEntity.BackgroundColor);
                _db.AddParameter(cmd, "BorderColor", quoteEntity.BorderColor);
                _db.AddParameter(cmd, "Status", quoteEntity.Status);
                _db.AddParameter(cmd, "ModifiedBy", quoteEntity.ModifiedBy);

                var result = cmd.ExecuteNonQuery();
                return result > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool DeleteCharacterInfoById(long id)
        {
            const string commandText = "CMS_CharacterInfo_DeleteById";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", id);
                var result = cmd.ExecuteNonQuery();
                return result > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        #endregion
        #endregion

        #region Constructor

        private readonly CmsMainDb _db;

        protected QuoteDalBase(CmsMainDb db)
        {
            _db = db;
        }

        protected CmsMainDb Database
        {
            get { return _db; }
        }

        #endregion
    }
}
