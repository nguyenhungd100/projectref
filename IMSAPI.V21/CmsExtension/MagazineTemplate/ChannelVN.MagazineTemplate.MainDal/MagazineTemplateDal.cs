﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ChannelVN.CMS.Common;
using ChannelVN.MagazineTemplate.Entity;
using ChannelVN.MagazineTemplate.MainDal.Databases;

namespace ChannelVN.MagazineTemplate.MainDal
{
    public class MagazineTemplateDal : MagazineTemplateDalBase
    {
        internal MagazineTemplateDal(CmsMainDb db)
            : base(db)
        {
        }
    }
}
