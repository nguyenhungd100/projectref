﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ChannelVN.CMS.Common;
using ChannelVN.MagazineTemplate.Entity;
using ChannelVN.MagazineTemplate.MainDal.Common;
using ChannelVN.MagazineTemplate.MainDal.Databases;

namespace ChannelVN.MagazineTemplate.MainDal
{
    public abstract class MagazineTemplateDalBase
    {
        public bool Insert(MagazineTemplateEntity magazine, ref int newMagazineTemplateId)
        {
            const string commandText = "CMS_MagazineTemplate_Insert";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", newMagazineTemplateId, ParameterDirection.Output);
                _db.AddParameter(cmd, "Title", magazine.Title);
                _db.AddParameter(cmd, "Avatar", magazine.Avatar);
                _db.AddParameter(cmd, "Type", magazine.Type);
                _db.AddParameter(cmd, "Cover", magazine.Cover);
                _db.AddParameter(cmd, "MobileCover", magazine.MobileCover);
                _db.AddParameter(cmd, "VideoCover", magazine.VideoCover);
                _db.AddParameter(cmd, "TextLayer", magazine.TextColor);
                _db.AddParameter(cmd, "BackgroundColor", magazine.BackgroundColor);
                _db.AddParameter(cmd, "TextColor", magazine.TextColor);
                _db.AddParameter(cmd, "DesktopZipUrl", magazine.DesktopZipUrl);
                _db.AddParameter(cmd, "MobileZipUrl", magazine.MobileZipUrl);
                _db.AddParameter(cmd, "CreatedBy", magazine.CreatedBy);
                _db.AddParameter(cmd, "ModifiedBy", magazine.ModifiedBy);
                _db.AddParameter(cmd, "Status", magazine.Status);
                _db.AddParameter(cmd, "Sapo", magazine.Sapo);
                _db.AddParameter(cmd, "Content", magazine.Content);                

                var numberOfRow = _db.ExecuteNonQuery(cmd);
                newMagazineTemplateId = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 0));
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public bool Update(MagazineTemplateEntity magazine)
        {
            const string commandText = "CMS_MagazineTemplate_Update";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Title", magazine.Title);
                _db.AddParameter(cmd, "Avatar", magazine.Avatar);
                _db.AddParameter(cmd, "Type", magazine.Type);
                _db.AddParameter(cmd, "Cover", magazine.Cover);
                _db.AddParameter(cmd, "MobileCover", magazine.MobileCover);
                _db.AddParameter(cmd, "VideoCover", magazine.VideoCover);
                _db.AddParameter(cmd, "TextLayer", magazine.TextLayer);
                _db.AddParameter(cmd, "BackgroundColor", magazine.BackgroundColor);
                _db.AddParameter(cmd, "TextColor", magazine.TextColor);
                _db.AddParameter(cmd, "DesktopZipUrl", magazine.DesktopZipUrl);
                _db.AddParameter(cmd, "MobileZipUrl", magazine.MobileZipUrl);                
                _db.AddParameter(cmd, "ModifiedBy", magazine.ModifiedBy);
                _db.AddParameter(cmd, "Status", magazine.Status);
                _db.AddParameter(cmd, "Sapo", magazine.Sapo);
                _db.AddParameter(cmd, "Content", magazine.Content);
                _db.AddParameter(cmd, "Id", magazine.Id);

                var numberOfRow = _db.ExecuteNonQuery(cmd);
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public bool Delete(int MagazineTemplateId)
        {
            const string commandText = "CMS_MagazineTemplate_Delete";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", MagazineTemplateId);
                var numberOfRow = _db.ExecuteNonQuery(cmd);
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public MagazineTemplateEntity GetMagazineTemplateById(int id)
        {
            const string commandText = "CMS_MagazineTemplate_GetbyId";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", id);
                var numberOfRow = _db.Get<MagazineTemplateEntity>(cmd);
                return numberOfRow;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public List<MagazineTemplateEntity> Search(string keyword, int type, int status, int pageIndex, int pageSize, ref int totalRow)
        {
            const string commandText = "CMS_MagazineTemplate_Search";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "TotalRow", totalRow, ParameterDirection.Output);
                _db.AddParameter(cmd, "Keyword", keyword);
                _db.AddParameter(cmd, "Status", status);
                _db.AddParameter(cmd, "Type", type);
                _db.AddParameter(cmd, "PageIndex", pageIndex);
                _db.AddParameter(cmd, "PageSize", pageSize);
                var numberOfRow = _db.GetList<MagazineTemplateEntity>(cmd);
                totalRow = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 0));
                return numberOfRow;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        #region Core members

        private readonly CmsMainDb _db;

        protected MagazineTemplateDalBase(CmsMainDb db)
        {
            _db = db;
        }

        protected CmsMainDb Database
        {
            get { return _db; }
        }

        #endregion

    }
}
