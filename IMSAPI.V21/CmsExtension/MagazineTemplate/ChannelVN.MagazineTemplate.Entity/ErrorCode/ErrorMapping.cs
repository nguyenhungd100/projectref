﻿using System.Runtime.Serialization;
using ChannelVN.CMS.Common;

namespace ChannelVN.MagazineTemplate.Entity.ErrorCode
{
    [DataContract]
    public class ErrorMapping : BoErrorMappingBase<ErrorMapping.ErrorCodes>
    {
        private static ErrorMapping _current;
        public static ErrorMapping Current
        {
            get { return _current ?? (_current = new ErrorMapping()); }
        }

        protected override void InitErrorMapping()
        {
            InnerHashtable[ErrorCodes.Success] = "Xử lý thành công";
            InnerHashtable[ErrorCodes.UnknowError] = "Lỗi không xác định";
            InnerHashtable[ErrorCodes.Exception] = "Lỗi trong quá trình xử lý";
            InnerHashtable[ErrorCodes.BusinessError] = "Lỗi nghiệp vụ";
            InnerHashtable[ErrorCodes.InvalidRequest] = "Yêu cầu không hợp lệ";
            InnerHashtable[ErrorCodes.TimeOutSession] = "Phiên làm việc của bạn đã hết.";            

            InnerHashtable[ErrorCodes.UpdateMagazineTemplateInvalidTitle] = "Invalid Title";
            InnerHashtable[ErrorCodes.UpdateMagazineTemplateNotFoundMagazineTemplate] = "Not Found MagazineTemplate";
            InnerHashtable[ErrorCodes.UpdateMagazineTemplateNewsNotFoundMagazineTemplateNews] = "Not Found MagazineTemplate News";
        }
        [DataContract]
        public enum ErrorCodes
        {
            #region General error

            [EnumMember]
            Success = ErrorCodeBase.Success,
            [EnumMember]
            UnknowError = ErrorCodeBase.UnknowError,
            [EnumMember]
            Exception = ErrorCodeBase.Exception,
            [EnumMember]
            BusinessError = ErrorCodeBase.BusinessError,
            [EnumMember]
            InvalidRequest = ErrorCodeBase.InvalidRequest,
            [EnumMember]
            TimeOutSession = ErrorCodeBase.TimeOutSession,

            UpdateMagazineTemplateInvalidTitle = 1001,
            UpdateMagazineTemplateNotFoundMagazineTemplate = 1002,
            UpdateMagazineTemplateNewsNotFoundMagazineTemplateNews = 1003
            #endregion
        }
    }
}
