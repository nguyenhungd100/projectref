﻿using System;
using System.Runtime.Serialization;
using ChannelVN.CMS.Common;

namespace ChannelVN.MagazineTemplate.Entity
{
    [DataContract]
    public enum EnumMagazineTemplateStatus: int
    {
        [EnumMember]
        AllStatus = -1,
        [EnumMember]
        Active = 1,
        [EnumMember]
        UnActive = 2
    }

    [DataContract]
    public class MagazineTemplateEntity : EntityBase
    {
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public string Title { get; set; }
        [DataMember]
        public string Avatar { get; set; }
        [DataMember]
        public int Type { get; set; }

        [DataMember]
        public string Cover { get; set; }
        [DataMember]
        public string MobileCover { get; set; }
        [DataMember]
        public string VideoCover { get; set; }
        [DataMember]
        public string TextLayer { get; set; }
        [DataMember]
        public string BackgroundColor { get; set; }
        [DataMember]
        public string TextColor { get; set; }

        [DataMember]
        public string DesktopZipUrl { get; set; }
        [DataMember]
        public string MobileZipUrl { get; set; }              
        [DataMember]
        public DateTime CreatedDate { get; set; }
        [DataMember]
        public string CreatedBy { get; set; }
        [DataMember]
        public DateTime ModifiedDate { get; set; }
        [DataMember]
        public string ModifiedBy { get; set; }
        [DataMember]
        public int Status { get; set; }
        [DataMember]
        public string Sapo { get; set; }

        [DataMember]
        public string Content { get; set; }
    }
}
