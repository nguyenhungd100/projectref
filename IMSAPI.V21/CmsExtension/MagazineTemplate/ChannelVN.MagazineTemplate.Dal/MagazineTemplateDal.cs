﻿using System.Collections.Generic;
using ChannelVN.MagazineTemplate.Entity;
using ChannelVN.MagazineTemplate.MainDal.Databases;

namespace ChannelVN.MagazineTemplate.Dal
{
    public class MagazineTemplateDal
    {
        public static bool Insert(MagazineTemplateEntity magazine, ref int newMagazineTemplateId)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.MagazineTemplateMainDal.Insert(magazine, ref newMagazineTemplateId);
            }
            return returnValue;
        }

        public static bool Update(MagazineTemplateEntity magazine)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.MagazineTemplateMainDal.Update(magazine);
            }
            return returnValue;
        }

        public static bool Delete(int MagazineTemplateId)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.MagazineTemplateMainDal.Delete(MagazineTemplateId);
            }
            return returnValue;
        }

        public static MagazineTemplateEntity GetMagazineTemplateById(int id)
        {
            MagazineTemplateEntity returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.MagazineTemplateMainDal.GetMagazineTemplateById(id);
            }
            return returnValue;
        }

        public static List<MagazineTemplateEntity> Search(string keyword, int type, int status, int pageIndex, int pageSize, ref int totalRow)
        {
            List<MagazineTemplateEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.MagazineTemplateMainDal.Search(keyword, type, status, pageIndex, pageSize, ref totalRow);
            }
            return returnValue;
        }
    }
}
