﻿using System;
using System.Collections.Generic;
using ChannelVN.MagazineTemplate.Dal;
using ChannelVN.MagazineTemplate.Entity;
using ChannelVN.MagazineTemplate.Entity.ErrorCode;

namespace ChannelVN.MagazineTemplate.Bo
{
    public class MagazineTemplateBo
    {
        #region MagazineTemplate
        
        public static ErrorMapping.ErrorCodes InsertMagazineTemplate(MagazineTemplateEntity entity, ref int newId)
        {
            if (string.IsNullOrEmpty(entity.Title))
            {
                return ErrorMapping.ErrorCodes.UpdateMagazineTemplateInvalidTitle;
            }
            
            return MagazineTemplateDal.Insert(entity, ref newId)
                       ? ErrorMapping.ErrorCodes.Success
                       : ErrorMapping.ErrorCodes.BusinessError;
        }

        public static ErrorMapping.ErrorCodes UpdateMagazineTemplate(MagazineTemplateEntity entity)
        {
            if (entity.Id <= 0)
            {
                return ErrorMapping.ErrorCodes.UpdateMagazineTemplateNotFoundMagazineTemplate;
            }
            var currentMagazineTemplate = MagazineTemplateDal.GetMagazineTemplateById(entity.Id);
            if (currentMagazineTemplate == null)
            {
                return ErrorMapping.ErrorCodes.UpdateMagazineTemplateNotFoundMagazineTemplate;
            }
            if (string.IsNullOrEmpty(entity.Title))
            {
                return ErrorMapping.ErrorCodes.UpdateMagazineTemplateInvalidTitle;
            }
            
            currentMagazineTemplate.Title = entity.Title;
            currentMagazineTemplate.Avatar = entity.Avatar;
            currentMagazineTemplate.Type = entity.Type;

            currentMagazineTemplate.Cover = entity.Cover;
            currentMagazineTemplate.MobileCover = entity.MobileCover;            
            currentMagazineTemplate.VideoCover = entity.VideoCover;
            currentMagazineTemplate.TextLayer = entity.TextLayer ;
            currentMagazineTemplate.BackgroundColor = entity.BackgroundColor;
            currentMagazineTemplate.TextColor = entity.TextColor;
            currentMagazineTemplate.DesktopZipUrl = entity.DesktopZipUrl;
            currentMagazineTemplate.MobileZipUrl = entity.MobileZipUrl;
            currentMagazineTemplate.ModifiedBy = entity.ModifiedBy;
            currentMagazineTemplate.Status = entity.Status;
            currentMagazineTemplate.Sapo = entity.Sapo;
            currentMagazineTemplate.Content = entity.Content;

            return MagazineTemplateDal.Update(currentMagazineTemplate)
                       ? ErrorMapping.ErrorCodes.Success
                       : ErrorMapping.ErrorCodes.BusinessError;
        }

        public static ErrorMapping.ErrorCodes DeleteMagazineTemplate(int magazineId)
        {
            if (magazineId <= 0)
            {
                return ErrorMapping.ErrorCodes.UpdateMagazineTemplateNotFoundMagazineTemplate;
            }
            var currentMagazineTemplate = MagazineTemplateDal.GetMagazineTemplateById(magazineId);
            if (currentMagazineTemplate == null)
            {
                return ErrorMapping.ErrorCodes.UpdateMagazineTemplateNotFoundMagazineTemplate;
            }
            return MagazineTemplateDal.Delete(magazineId)
                       ? ErrorMapping.ErrorCodes.Success
                       : ErrorMapping.ErrorCodes.BusinessError;
        }

        public static MagazineTemplateEntity GetMagazineTemplateById(int magazineId)
        {
            if (magazineId <= 0)
            {
                return null;
            }
            return MagazineTemplateDal.GetMagazineTemplateById(magazineId);
        }

        public static List<MagazineTemplateEntity> MagazineTemplateSearch(string keyword, int type, EnumMagazineTemplateStatus status, int pageIndex, int pageSize, ref int totalRow)
        {
            return MagazineTemplateDal.Search(keyword, type, (int)status, pageIndex, pageSize, ref totalRow);
        }

        #endregion
    }
}
