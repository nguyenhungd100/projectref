﻿using System;
using System.Collections.Generic;
using ChannelVN.CMS.Entity.ErrorCode;
using ChannelVN.SurveyManager.Dal;
using ChannelVN.SurveyManager.Entity;
using ChannelVN.WcfExtensions;

namespace ChannelVN.SurveyManager.Bo
{
    public class SurveyBo
    {
        public static List<SurveyEntity> GetList(string keyword,int zoneId,EnumSurveyType type, int status, int pageIndex, int pageSize, ref int totalRow)
        {
            return SurveyDal.GetList(keyword,zoneId, (int)type, status, pageIndex, pageSize, ref totalRow);
        }

        public static SurveyDetailEntity GetInfo(int id)
        {
            var surveyDetailEntity = new SurveyDetailEntity
            {
                SurveyInfo = (id < 0 ? null : SurveyDal.GetById(id))
            };
            if (id > 0)
            {
                try
                {
                    var listSurveyQuestionDetail = new List<SurveyQuestionDetailEntity>();
                    var getListSurveyQuestion = SurveyDal.GetListQuestionBySurveyId(id);
                    for (int i = 0; i < getListSurveyQuestion.Count; i++)
                    {
                        var surveyQuestionInfo = new SurveyQuestionEntity
                        {
                            Id = getListSurveyQuestion[i].Id,
                            SurveyId = getListSurveyQuestion[i].SurveyId,
                            Title = getListSurveyQuestion[i].Title,
                            SubTitle = getListSurveyQuestion[i].SubTitle,
                            Avatar = getListSurveyQuestion[i].Avatar,
                            Sapo = getListSurveyQuestion[i].Sapo,
                            Priority = getListSurveyQuestion[i].Priority,
                            DisplayStyle = getListSurveyQuestion[i].DisplayStyle,
                        };
                        var surveyQuestionDetailEntity = new SurveyQuestionDetailEntity
                        {
                            SurveyQuestionInfo = surveyQuestionInfo,
                            ListAnswers = SurveyDal.GetListAnswersByQuestionId(getListSurveyQuestion[i].Id)
                        };
                        listSurveyQuestionDetail.Add(surveyQuestionDetailEntity);
                    }
                    surveyDetailEntity.ListZoneInSurvey = SurveyDal.GetSurveyInZoneBySurveyId(id);
                    surveyDetailEntity.ListSurveyQuestionDetail = listSurveyQuestionDetail;
                }
                catch (Exception ex)
                {
                    surveyDetailEntity.ListSurveyQuestionDetail = null;
                }
            }
            else
            {
                surveyDetailEntity.ListSurveyQuestionDetail = null;
            }

            return surveyDetailEntity;
        }

        public static ErrorMapping.ErrorCodes Insert(SurveyDetailEntity surveyDetail, int zoneId, string zoneIdList)
        {
            try
            {
                if (surveyDetail == null)
                {
                    return ErrorMapping.ErrorCodes.InvalidRequest;
                }
                var surveyId = 0;
                var currentUser = WcfMessageHeader.Current.ClientUsername;
                var surveyEntity = surveyDetail.SurveyInfo;
                surveyEntity.CreatedBy = currentUser;
                surveyEntity.LastModifiedBy = currentUser;
                var listSurveyQuestionDetail = surveyDetail.ListSurveyQuestionDetail;
                SurveyDal.Insert(surveyEntity, zoneId, zoneIdList, ref surveyId);
                if (surveyId > 0)
                {
                    foreach (var t in listSurveyQuestionDetail)
                    {
                        var questionId = 0;
                        var questionEntity = t.SurveyQuestionInfo;
                        questionEntity.SurveyId = surveyId;
                        var listAnswers = t.ListAnswers;
                        SurveyDal.InsertQuestion(questionEntity, ref questionId);
                        if (questionId > 0)
                        {
                            foreach (var surveyAnswersEntity in listAnswers)
                            {
                                var answersId = 0;
                                surveyAnswersEntity.SurveyId = surveyId;
                                surveyAnswersEntity.SurveyQuestionId = questionId;
                                SurveyDal.InsertAnswers(surveyAnswersEntity, ref answersId);
                            }
                        }
                    }
                }
                return ErrorMapping.ErrorCodes.Success;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("SurveyDal.Insert:{0}", ex.Message));
            }
        }

        public static ErrorMapping.ErrorCodes Update(SurveyDetailEntity surveyDetail, int zoneId, string zoneIdList)
        {
            try
            {
                if (surveyDetail == null)
                {
                    return ErrorMapping.ErrorCodes.InvalidRequest;
                } 
                var currentUser = WcfMessageHeader.Current.ClientUsername;
                var surveyEntity = surveyDetail.SurveyInfo;
                surveyEntity.CreatedBy = currentUser;
                surveyEntity.LastModifiedBy = currentUser;
                var listSurveyQuestionDetail = surveyDetail.ListSurveyQuestionDetail;
                var surveyId = surveyEntity.Id;
                if (surveyId > 0)
                {
                    SurveyDal.Update(surveyEntity, zoneId, zoneIdList);
                    foreach (var t in listSurveyQuestionDetail)
                    {
                        var questionEntity = t.SurveyQuestionInfo;
                        var listAnswers = t.ListAnswers;
                        var questionId = questionEntity.Id;
                        if (questionId > 0)
                        {
                            SurveyDal.UpdateQuestion(questionEntity);
                            foreach (var surveyAnswersEntity in listAnswers)
                            {
                                surveyAnswersEntity.SurveyQuestionId = questionId;
                                if (surveyAnswersEntity.Id > 0)
                                {
                                    SurveyDal.UpdateAnswers(surveyAnswersEntity);
                                }
                                else
                                {
                                    var answersId = 0;
                                    SurveyDal.InsertAnswers(surveyAnswersEntity, ref answersId);
                                }
                            }
                        }
                        else
                        {
                            questionEntity.SurveyId = surveyId;
                            SurveyDal.InsertQuestion(questionEntity, ref questionId);
                            foreach (var surveyAnswersEntity in listAnswers)
                            {
                                surveyAnswersEntity.SurveyId = surveyId;
                                surveyAnswersEntity.SurveyQuestionId = questionId;
                                if (surveyAnswersEntity.Id > 0)
                                {
                                    SurveyDal.UpdateAnswers(surveyAnswersEntity);
                                }
                                else
                                {
                                    var answersId = 0;
                                    SurveyDal.InsertAnswers(surveyAnswersEntity, ref answersId);
                                }
                            }
                        }
                    }
                }
                return ErrorMapping.ErrorCodes.Success;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("SurveyDal.Insert:{0}", ex.Message));
            }
        }

        public static ErrorMapping.ErrorCodes Delete(int id)
        {
            return SurveyDal.Delete(id) ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.UnknowError;
        }
        public static ErrorMapping.ErrorCodes DeleteQuestion(string listid)
        {
            return SurveyDal.DeleteQuestion(listid) ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.UnknowError;
        }
        public static ErrorMapping.ErrorCodes DeleteAnswers(string listid)
        {
            return SurveyDal.DeleteAnswers(listid) ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.UnknowError;
        }
    }
}
