﻿using System.Collections.Generic;
using ChannelVN.SurveyManager.Entity;
using ChannelVN.SurveyManager.MainDal.Databases;

namespace ChannelVN.SurveyManager.Dal
{
    public class SurveyDal
    {
        #region Gets
        public static SurveyEntity GetById(int id)
        {
            SurveyEntity returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.SurveyMainDal.GetById(id);
            }
            return returnValue;
        }
        public static List<SurveyEntity> GetList(string keyword,int zoneId, int type, int status, int pageIndex, int pageSize, ref int totalRow)
        {
            List<SurveyEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.SurveyMainDal.GetList(keyword,zoneId, type, status, pageIndex, pageSize, ref totalRow);
            }
            return returnValue;
        }
        public static List<SurveyQuestionEntity> GetListQuestionBySurveyId(int surveyId)
        {
            List<SurveyQuestionEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.SurveyMainDal.GetListQuestionBySurveyId(surveyId);
            }
            return returnValue;
        }
        public static List<SurveyAnswersEntity> GetListAnswersByQuestionId(int surveyId)
        {
            List<SurveyAnswersEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.SurveyMainDal.GetListAnswersByQuestionId(surveyId);
            }
            return returnValue;
        }
        public static List<SurveyInZoneEntity> GetSurveyInZoneBySurveyId(int surveyId)
        {
            List<SurveyInZoneEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.SurveyMainDal.GetSurveyInZoneBySurveyId(surveyId);
            }
            return returnValue;
        }
        #endregion

        #region Sets
        public static bool Insert(SurveyEntity survey, int zoneId, string zoneIdList, ref int surveyId)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.SurveyMainDal.Insert(survey,zoneId,zoneIdList, ref surveyId);
            }
            return returnValue;
        }

        public static bool Update(SurveyEntity survey,int zoneId, string zoneIdList)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.SurveyMainDal.Update(survey, zoneId, zoneIdList);
            }
            return returnValue;
        }
        public static bool Delete(int id)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.SurveyMainDal.Delete(id);
            }
            return returnValue;
        }
        public static bool InsertQuestion(SurveyQuestionEntity surveyQuestion, ref int surveyQuestionId)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.SurveyMainDal.InsertQuestion(surveyQuestion, ref surveyQuestionId);
            }
            return returnValue;
        }

        public static bool UpdateQuestion(SurveyQuestionEntity surveyQuestion)
        {

            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.SurveyMainDal.UpdateQuestion(surveyQuestion);
            }
            return returnValue;
        }

        public static bool DeleteQuestion(string listid)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.SurveyMainDal.DeleteQuestion(listid);
            }
            return returnValue;

        }

        public static bool InsertAnswers(SurveyAnswersEntity surveyAnswers, ref int surveyAnswersId)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.SurveyMainDal.InsertAnswers(surveyAnswers, ref surveyAnswersId);
            }
            return returnValue;
        }

        public static bool UpdateAnswers(SurveyAnswersEntity surveyAnswers)
        {

            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.SurveyMainDal.UpdateAnswers(surveyAnswers);
            }
            return returnValue;
        }

        public static bool DeleteAnswers(string listid)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.SurveyMainDal.DeleteAnswers(listid);
            }
            return returnValue;

        }
        #endregion
    }
}
