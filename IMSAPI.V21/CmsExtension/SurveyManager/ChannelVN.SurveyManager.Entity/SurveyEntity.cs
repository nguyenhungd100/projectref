﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using ChannelVN.CMS.Common;

namespace ChannelVN.SurveyManager.Entity
{
    [DataContract]
    public enum EnumSurveyType
    {
        [EnumMember]
        All = -1,
        [EnumMember]
        Survey = 1,
        [EnumMember]
        Poll = 2,
    }
    [DataContract]
    public enum EnumSurveyQuestionDisplayStyle
    {
        [EnumMember]
        Radio = 1,
        [EnumMember]
        Checkbox = 2,
        [EnumMember]
        Scale = 3,
        [EnumMember]
        Photo = 4,
        [EnumMember]
        Video = 5,
    }
    [DataContract]
    public class SurveyEntity : EntityBase
    {
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public string Title { get; set; }
        [DataMember]
        public string Sapo { get; set; }
        [DataMember]
        public DateTime StartedDate { get; set; }
        [DataMember]
        public DateTime EndedDate { get; set; }
        [DataMember]
        public bool ShowInZone { get; set; }
        [DataMember]
        public bool ShowInFooter { get; set; }
        [DataMember]
        public int ViewCount { get; set; }
        [DataMember]
        public int RateCount { get; set; }
        [DataMember]
        public int Status { get; set; }
        [DataMember]
        public int Type { get; set; }
        [DataMember]
        public int Priority { get; set; }
        [DataMember]
        public DateTime CreatedDate { get; set; }
        [DataMember]
        public string CreatedBy { get; set; }
        [DataMember]
        public DateTime ModifiedDate { get; set; }
        [DataMember]
        public string LastModifiedBy { get; set; }
    }

    [DataContract]
    public class SurveyDetailEntity : EntityBase
    {
        [DataMember]
        public SurveyEntity SurveyInfo { get; set; }
        [DataMember]
        public List<SurveyQuestionDetailEntity> ListSurveyQuestionDetail { get; set; }
        [DataMember]
        public List<SurveyInZoneEntity> ListZoneInSurvey { get; set; }
    }


    [DataContract]
    public class SurveyQuestionEntity : EntityBase
    {
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public int SurveyId { get; set; }
        [DataMember]
        public string Title { get; set; }
        [DataMember]
        public string SubTitle { get; set; }
        [DataMember]
        public string Avatar { get; set; }
        [DataMember]
        public string Sapo { get; set; }
        [DataMember]
        public int Priority { get; set; }
        [DataMember]
        public int DisplayStyle { get; set; }
    }
    [DataContract]
    public class SurveyQuestionDetailEntity : EntityBase
    {
        [DataMember]
        public SurveyQuestionEntity SurveyQuestionInfo { get; set; }
        [DataMember]
        public List<SurveyAnswersEntity> ListAnswers { get; set; }
    }

    [DataContract]
    public class SurveyAnswersEntity : EntityBase
    {
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public int SurveyId { get; set; }
        [DataMember]
        public int SurveyQuestionId { get; set; }
        [DataMember]
        public string Value { get; set; }
        [DataMember]
        public double VoteRate { get; set; }
        [DataMember]
        public int Status { get; set; }
        [DataMember]
        public int Priority { get; set; }
    }

    [DataContract]
    public class SurveyInZoneEntity : EntityBase
    {
        [DataMember]
        public int SurveyId{ get; set; }
        [DataMember]
        public int ZoneId { get; set; }
        [DataMember]
        public bool IsPrimary { get; set; }
    }
}
