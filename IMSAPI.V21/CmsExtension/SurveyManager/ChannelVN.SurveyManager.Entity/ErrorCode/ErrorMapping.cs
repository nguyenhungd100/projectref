﻿using System.Runtime.Serialization;
using ChannelVN.CMS.Common;

namespace ChannelVN.SurveyManager.Entity.ErrorCode
{
    [DataContract]
    public class ErrorMapping : BoErrorMappingBase<ErrorMapping.ErrorCodes>
    {
        private static ErrorMapping _current;
        public static ErrorMapping Current
        {
            get { return _current ?? (_current = new ErrorMapping()); }
        }

        protected override void InitErrorMapping()
        {
            InnerHashtable[ErrorCodes.InvalidJob] = "Không tìm thấy công việc";
            InnerHashtable[ErrorCodes.InvalidJobName] = "Tên công việc không hợp lệ";
            InnerHashtable[ErrorCodes.InvalidParentJob] = "Không tìm thấy công việc gốc";
            InnerHashtable[ErrorCodes.JobMustHaveStartedDateForFinishedState] = "Công việc đã kết thúc cần có thời gian bắt đầu";
            InnerHashtable[ErrorCodes.FinishedDateMustGreaterThanStartedDate] = "Ngày kết thúc công việc phải lớn hơn ngày bắt đầu";
            InnerHashtable[ErrorCodes.InvalidUserForAssign] = "Không tìm thấy người được phân công công việc";
            InnerHashtable[ErrorCodes.InvalidZoneForJob] = "Không tìm thấy chuyên mục được phân chỉ tiêu";
        }
        [DataContract]
        public enum ErrorCodes
        {
            #region General error

            [EnumMember]
            Success = ErrorCodeBase.Success,
            [EnumMember]
            UnknowError = ErrorCodeBase.UnknowError,
            [EnumMember]
            Exception = ErrorCodeBase.Exception,
            [EnumMember]
            BusinessError = ErrorCodeBase.BusinessError,
            [EnumMember]
            InvalidRequest = ErrorCodeBase.InvalidRequest,
            [EnumMember]
            TimeOutSession = ErrorCodeBase.TimeOutSession,

            #endregion

            #region Job error

            InvalidJob = 10001,
            InvalidJobName = 10002,
            InvalidParentJob = 10003,
            JobMustHaveStartedDateForFinishedState = 10004,
            FinishedDateMustGreaterThanStartedDate = 10005,
            InvalidUserForAssign = 10006,
            InvalidJobAssignment = 10007,
            InvalidZoneForJob = 10008,


            #endregion
        }
    }
}
