﻿using System;
using System.Collections.Generic;
using System.Data;
using ChannelVN.CMS.Common;
using ChannelVN.SurveyManager.Entity;
using ChannelVN.SurveyManager.MainDal.Databases;

namespace ChannelVN.SurveyManager.MainDal
{
    public abstract class SurveyDalBase
    {
        #region Gets
        public SurveyEntity GetById(int id)
        {
            const string commandText = "CMS_Survey_SelectById";
            try
            {
                SurveyEntity data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", id);
                data = _db.Get<SurveyEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }

        }
        public List<SurveyEntity> GetList(string keyword,int zoneId, int type, int status, int pageIndex, int pageSize, ref int totalRow)
        {

            const string commandText = "CMS_Survey_Search";
            try
            {
                List<SurveyEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "TotalRow", totalRow, ParameterDirection.Output);
                _db.AddParameter(cmd, "Title", keyword);
                _db.AddParameter(cmd, "ZoneId", zoneId);
                _db.AddParameter(cmd, "Type", type);
                _db.AddParameter(cmd, "Status", status);
                _db.AddParameter(cmd, "PageIndex", pageIndex);
                _db.AddParameter(cmd, "PageSize", pageSize);
                data = _db.GetList<SurveyEntity>(cmd);
                totalRow = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 0));
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public List<SurveyQuestionEntity> GetListQuestionBySurveyId(int surveyId)
        {
            const string commandText = "CMS_SurveyQuestion_GetListBySurveyId";
            try
            {
                List<SurveyQuestionEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "SurveyId", surveyId);
                data = _db.GetList<SurveyQuestionEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public List<SurveyAnswersEntity> GetListAnswersByQuestionId(int surveyQuestionId)
        {
            const string commandText = "CMS_SurveyAnswers_GetListBySurveyQuestionId";
            try
            {
                List<SurveyAnswersEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "SurveyQuestionId", surveyQuestionId);
                data = _db.GetList<SurveyAnswersEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public List<SurveyInZoneEntity> GetSurveyInZoneBySurveyId(int surveyId)
        {
            const string commandText = "CMS_SurveyInZone_GetBySurveyId";
            try
            {
                List<SurveyInZoneEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "SurveyID", surveyId);
                data = _db.GetList<SurveyInZoneEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        #endregion

        #region Sets

        public bool Insert(SurveyEntity survey, int zoneId, string zoneIdList, ref int surveyId)
        {
            const string commandText = "CMS_Survey_Insert";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", surveyId, ParameterDirection.Output);
                _db.AddParameter(cmd, "ZoneId", zoneId);
                _db.AddParameter(cmd, "ZoneIdList", zoneIdList);
                _db.AddParameter(cmd, "Title", survey.Title);
                _db.AddParameter(cmd, "Sapo", survey.Sapo);
                _db.AddParameter(cmd, "StartedDate", survey.StartedDate != DateTime.MinValue ? survey.StartedDate : DateTime.Now);
                _db.AddParameter(cmd, "EndedDate", survey.EndedDate != DateTime.MinValue ? survey.EndedDate : DateTime.Now);
                _db.AddParameter(cmd, "ShowInZone", survey.ShowInZone);
                _db.AddParameter(cmd, "ShowInFooter", survey.ShowInFooter);
                _db.AddParameter(cmd, "ViewCount", survey.ViewCount);
                _db.AddParameter(cmd, "RateCount", survey.RateCount);
                _db.AddParameter(cmd, "Priority", survey.Priority);
                _db.AddParameter(cmd, "Status", survey.Status);
                _db.AddParameter(cmd, "Type", survey.Type);
                _db.AddParameter(cmd, "CreatedBy", survey.CreatedBy);
                _db.AddParameter(cmd, "LastModifiedBy", survey.LastModifiedBy);
                var numberOfRow = cmd.ExecuteNonQuery();

                surveyId = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 0));
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public bool Update(SurveyEntity survey, int zoneId, string zoneIdList)
        {
            const string commandText = "CMS_Survey_Update";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", survey.Id);
                _db.AddParameter(cmd, "ZoneId", zoneId);
                _db.AddParameter(cmd, "ZoneIdList", zoneIdList);
                _db.AddParameter(cmd, "Title", survey.Title);
                _db.AddParameter(cmd, "Sapo", survey.Sapo);
                _db.AddParameter(cmd, "StartedDate", survey.StartedDate != DateTime.MinValue ? survey.StartedDate : DateTime.Now);
                _db.AddParameter(cmd, "EndedDate", survey.EndedDate != DateTime.MinValue ? survey.EndedDate : DateTime.Now);
                _db.AddParameter(cmd, "ShowInZone", survey.ShowInZone);
                _db.AddParameter(cmd, "ShowInFooter", survey.ShowInFooter);
                _db.AddParameter(cmd, "ViewCount", survey.ViewCount);
                _db.AddParameter(cmd, "RateCount", survey.RateCount);
                _db.AddParameter(cmd, "Status", survey.Status);
                _db.AddParameter(cmd, "Type", survey.Type);
                _db.AddParameter(cmd, "Priority", survey.Priority);
                _db.AddParameter(cmd, "LastModifiedBy", survey.LastModifiedBy);

                var numberOfRow = cmd.ExecuteNonQuery();
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }

        }


        public bool Delete(int id)
        {
            const string commandText = "CMS_Survey_Delete";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", id);
                var numberOfRow = _db.ExecuteNonQuery(cmd);
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public bool InsertQuestion(SurveyQuestionEntity surveyQuestion, ref int surveyAnswersId)
        {

            const string commandText = "CMS_SurveyQuestion_Insert";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);

                _db.AddParameter(cmd, "Id", surveyAnswersId, ParameterDirection.Output);
                _db.AddParameter(cmd, "SurveyId", surveyQuestion.SurveyId);
                _db.AddParameter(cmd, "Title", surveyQuestion.Title);
                _db.AddParameter(cmd, "SubTitle", surveyQuestion.SubTitle);
                _db.AddParameter(cmd, "Avatar", surveyQuestion.Avatar);
                _db.AddParameter(cmd, "Sapo", surveyQuestion.Sapo);
                _db.AddParameter(cmd, "Priority", surveyQuestion.Priority);
                _db.AddParameter(cmd, "DisplayStyle", surveyQuestion.DisplayStyle);
                var numberOfRow = cmd.ExecuteNonQuery();

                surveyAnswersId = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 0));
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public bool UpdateQuestion(SurveyQuestionEntity surveyQuestion)
        {
            const string commandText = "CMS_SurveyQuestion_Update";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", surveyQuestion.Id);
                _db.AddParameter(cmd, "SurveyId", surveyQuestion.SurveyId);
                _db.AddParameter(cmd, "Title", surveyQuestion.Title);
                _db.AddParameter(cmd, "SubTitle", surveyQuestion.SubTitle);
                _db.AddParameter(cmd, "Avatar", surveyQuestion.Avatar);
                _db.AddParameter(cmd, "Sapo", surveyQuestion.Sapo);
                _db.AddParameter(cmd, "Priority", surveyQuestion.Priority);
                _db.AddParameter(cmd, "DisplayStyle", surveyQuestion.DisplayStyle);

                var numberOfRow = cmd.ExecuteNonQuery();
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public bool DeleteQuestion(string listId)
        {
            const string commandText = "CMS_SurveyQuestion_DeleteList";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "ListId", listId);
                var numberOfRow = cmd.ExecuteNonQuery();
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public bool InsertAnswers(SurveyAnswersEntity surveyAnswers, ref int surveyAnswersId)
        {

            const string commandText = "CMS_SurveyAnswers_Insert";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);

                _db.AddParameter(cmd, "Id", surveyAnswersId, ParameterDirection.Output);
                _db.AddParameter(cmd, "SurveyId", surveyAnswers.SurveyId);
                _db.AddParameter(cmd, "SurveyQuestionId", surveyAnswers.SurveyQuestionId);
                _db.AddParameter(cmd, "Value", surveyAnswers.Value);
                _db.AddParameter(cmd, "VoteRate", surveyAnswers.VoteRate);
                _db.AddParameter(cmd, "Status", surveyAnswers.Status);
                _db.AddParameter(cmd, "Priority", surveyAnswers.Priority);
                var numberOfRow = cmd.ExecuteNonQuery();

                surveyAnswersId = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 0));
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public bool UpdateAnswers(SurveyAnswersEntity surveyAnswers)
        {
            const string commandText = "CMS_SurveyAnswers_Update";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", surveyAnswers.Id);
                _db.AddParameter(cmd, "SurveyId", surveyAnswers.SurveyId);
                _db.AddParameter(cmd, "SurveyQuestionId", surveyAnswers.SurveyQuestionId);
                _db.AddParameter(cmd, "Value", surveyAnswers.Value);
                _db.AddParameter(cmd, "VoteRate", surveyAnswers.VoteRate);
                _db.AddParameter(cmd, "Status", surveyAnswers.Status);
                _db.AddParameter(cmd, "Priority", surveyAnswers.Priority);

                var numberOfRow = cmd.ExecuteNonQuery();
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public bool DeleteAnswers(string listId)
        {
            const string commandText = "CMS_SurveyAnswesr_DeleteList";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "ListId", listId);
                var numberOfRow = cmd.ExecuteNonQuery();
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        #endregion

        #region Core members

        private readonly CmsMainDb _db;

        protected SurveyDalBase(CmsMainDb db)
        {
            _db = db;
        }

        protected CmsMainDb Database
        {
            get { return _db; }
        }

        #endregion
    }
}
