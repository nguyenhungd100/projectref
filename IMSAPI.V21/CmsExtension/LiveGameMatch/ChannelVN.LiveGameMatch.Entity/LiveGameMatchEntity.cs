﻿using ChannelVN.CMS.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace ChannelVN.LiveGameMatch.Entity
{
     [DataContract]
    public class LiveGameMatchEntity:EntityBase
    {
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public int TeamIdA { get; set; }
        [DataMember]
        public int TeamIdB { get; set; }
        [DataMember]
        public int ResultA { get; set; }
        [DataMember]
        public int ResultB { get; set; }
        [DataMember]
        public DateTime DisplayTime { get; set; }
        [DataMember]
        public string Description { get; set; }
        [DataMember]
        public DateTime CreatedDate { get; set; }
        [DataMember]
        public long CreatedDateStamp { get; set; }
        [DataMember]
        public DateTime PublishDate { get; set; }
        [DataMember]
        public long PublishDateStamp { get; set; }
        [DataMember]
        public int GameId { get; set; }
        [DataMember]
        public int GameLeagueId { get; set; }
        [DataMember]
        public long NewsId { get; set; }
        [DataMember]
        public string TagItem { get; set; }
        [DataMember]
        public string TagUrl { get; set; }
        [DataMember]
        public int TagId { get; set; }
        [DataMember]
        public string EmbedReplay { get; set; }
        [DataMember]
        public string EmbedLive { get; set; }
        [DataMember]
        public string CaptionEmbedReplay { get; set; }
        [DataMember]
        public string CaptionEmbedLive { get; set; }
        [DataMember]
        public int LiveStatus { get; set; }
        [DataMember]
        public int CatId { get; set; }
        [DataMember]
        public string NewsTitle { get; set; }
        [DataMember]
        public string NewsImage { get; set; }
        [DataMember]
        public bool NewsIsFocus { get; set; }
        [DataMember]
        public int NewsStatus { get; set; }
    }
}
