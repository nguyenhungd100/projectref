﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ChannelVN.LiveGameMatch.Entity;
using System.Data.SqlClient;
using ChannelVN.CMS.Common;
using System.Data;
using ChannelVN.LiveGameMatch.Dal.Common;
using ChannelVN.LiveGameMatch.MainDal.Databases;

namespace ChannelVN.LiveGameMatch.Dal
{
    public class LiveGameMatchDal
    {
        public static bool Insert(LiveGameMatchEntity info, ref int id)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.LiveGameMatchMainDal.Insert(info, ref id);
            }
            return returnValue;
        }

        public static bool Update(LiveGameMatchEntity info)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.LiveGameMatchMainDal.Update(info);
            }
            return returnValue;
        }

        public static bool Delete(long newsId)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.LiveGameMatchMainDal.Delete(newsId);
            }
            return returnValue;
        }

        public static bool UpdatePublishDate(DateTime publishDate, long publishDateStamp, int gameMatchId)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.LiveGameMatchMainDal.UpdatePublishDate(publishDate, publishDateStamp, gameMatchId);
            }
            return returnValue;
        }

        public static LiveGameMatchEntity GetGameMatchById(int Id)
        {
            LiveGameMatchEntity returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.LiveGameMatchMainDal.GetGameMatchById(Id);
            }
            return returnValue;
        }

        public static LiveGameMatchEntity GetGameMatchByNewsId(long newsId)
        {
            LiveGameMatchEntity returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.LiveGameMatchMainDal.GetGameMatchByNewsId(newsId);
            }
            return returnValue;
        }
    }
}
