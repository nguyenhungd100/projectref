﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ChannelVN.LiveGameMatch.Entity;
using System.Data.SqlClient;
using ChannelVN.CMS.Common;
using System.Data;
using ChannelVN.LiveGameMatch.Dal.Common;
using ChannelVN.LiveGameMatch.MainDal.Databases;

namespace ChannelVN.LiveGameMatch.Dal
{
    public class LiveGameDal
    {
        /// <summary>
        /// Thêm mới dữ liệu vào bảng LiveGame
        /// </summary>
        /// <param name="info"></param>
        /// <param name="id"></param>
        /// <returns></returns>
        public static bool Insert(LiveGameEntity info, ref int id)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.LiveGameMainDal.Insert(info, ref id);
            }
            return returnValue;
        }
        /// <summary>
        /// Sửa dữ liệu cho bảng LiveGame
        /// </summary>
        /// <param name="info"></param>
        /// <returns></returns>
        public static bool Update(LiveGameEntity info)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.LiveGameMainDal.Update(info);
            }
            return returnValue;
        }
        /// <summary>
        /// Xóa dữ liệu trong bảng LiveGame theo Id
        /// </summary>
        /// <param name="liveGameId"></param>
        /// <returns></returns>
        public static bool Delete(int liveGameId)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.LiveGameMainDal.Delete(liveGameId);
            }
            return returnValue;
        }
        /// <summary>
        /// Lấy thông tin live game theo id
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        public static LiveGameEntity GetLiveGameById(int Id)
        {
            LiveGameEntity returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.LiveGameMainDal.GetLiveGameById(Id);
            }
            return returnValue;
        }
        /// <summary>
        /// Lấy danh sách LiveGame
        /// </summary>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <param name="totalRow"></param>
        /// <returns></returns>
        public static List<LiveGameEntity> ListLiveGameByPages(string search, int pageIndex, int pageSize, ref int totalRow)
        {
            List<LiveGameEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.LiveGameMainDal.ListLiveGameByPages(search, pageIndex, pageSize, ref totalRow);
            }
            return returnValue;
        }
    }
}
