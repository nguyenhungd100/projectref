﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using ChannelVN.CMS.Common;
using System.Data;
using ChannelVN.LiveGameMatch.Dal.Common;
using ChannelVN.LiveGameMatch.Entity;
using ChannelVN.LiveGameMatch.MainDal.Databases;

namespace ChannelVN.LiveGameMatch.Dal
{
    public class LiveTeamGameDal
    {
        /// <summary>
        /// Thêm mới dữ liệu vào bảng LiveTeamGame
        /// </summary>
        /// <param name="info"></param>
        /// <param name="id"></param>
        /// <returns></returns>
        public static bool Insert(LiveTeamGameEntity info, ref int id)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.LiveTeamGameMainDal.Insert(info, ref id);
            }
            return returnValue;
        }
        /// <summary>
        /// Sửa dữ liệu cho bảng LiveTeamGame
        /// </summary>
        /// <param name="info"></param>
        /// <returns></returns>
        public static bool Update(LiveTeamGameEntity info)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.LiveTeamGameMainDal.Update(info);
            }
            return returnValue;
        }
        /// <summary>
        /// Xóa dữ liệu trong bảng LiveTeamGame theo Id
        /// </summary>
        /// <param name="liveGameId"></param>
        /// <returns></returns>
        public static bool Delete(int liveGameId)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.LiveTeamGameMainDal.Delete(liveGameId);
            }
            return returnValue;
        }
        /// <summary>
        /// Lấy thông tin live team game theo id
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        public static LiveTeamGameEntity GetLiveTeamGameById(int Id)
        {
            LiveTeamGameEntity returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.LiveTeamGameMainDal.GetLiveTeamGameById(Id);
            }
            return returnValue;
        }
        /// <summary>
        /// Lấy danh sách LiveTeamGame
        /// </summary>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <param name="totalRow"></param>
        /// <returns></returns>
        public static List<LiveTeamGameEntity> ListLiveTeamGameByPages(string search, int pageIndex, int pageSize, ref int totalRow)
        {
            List<LiveTeamGameEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.LiveTeamGameMainDal.ListLiveTeamGameByPages(search, pageIndex, pageSize, ref totalRow);
            }
            return returnValue;
        }
        public static List<LiveTeamGameEntity> ListLiveTeamGameByMultiId(int teamA, int teamB)
        {
            List<LiveTeamGameEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.LiveTeamGameMainDal.ListLiveTeamGameByMultiId(teamA, teamB);
            }
            return returnValue;
        }
    }
}
