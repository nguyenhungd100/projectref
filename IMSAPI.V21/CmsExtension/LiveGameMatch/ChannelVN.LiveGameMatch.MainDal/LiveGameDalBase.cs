﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ChannelVN.LiveGameMatch.Entity;
using System.Data.SqlClient;
using ChannelVN.CMS.Common;
using System.Data;
using ChannelVN.LiveGameMatch.MainDal.Databases;

namespace ChannelVN.LiveGameMatch.MainDal
{
    public abstract class LiveGameDalBase
    {
        /// <summary>
        /// Thêm mới dữ liệu vào bảng LiveGame
        /// </summary>
        /// <param name="info"></param>
        /// <param name="id"></param>
        /// <returns></returns>
        public bool Insert(LiveGameEntity info, ref int id)
        {
            const string commandText = "CMS_LiveGame_Insert";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", id, ParameterDirection.Output);
                _db.AddParameter(cmd, "Name", info.Name);
                _db.AddParameter(cmd, "Description", !string.IsNullOrEmpty(info.Description) ? info.Description : null);
                _db.AddParameter(cmd, "TagItem", !string.IsNullOrEmpty(info.TagItem) ? info.TagItem : null);
                _db.AddParameter(cmd, "TagUrl", !string.IsNullOrEmpty(info.TagUrl) ? info.TagUrl : null);
                _db.AddParameter(cmd, "TagId", info.TagId);
                var numberOfRow = cmd.ExecuteNonQuery();
                id = Utility.ConvertToInt(cmd.Parameters[0]);
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        /// <summary>
        /// Sửa dữ liệu cho bảng LiveGame
        /// </summary>
        /// <param name="info"></param>
        /// <returns></returns>
        public bool Update(LiveGameEntity info)
        {
            const string commandText = "CMS_LiveGame_Update";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Name", info.Name);
                _db.AddParameter(cmd, "Description", !string.IsNullOrEmpty(info.Description) ? info.Description : null);
                _db.AddParameter(cmd, "TagItem", !string.IsNullOrEmpty(info.TagItem) ? info.TagItem : null);
                _db.AddParameter(cmd, "TagUrl", !string.IsNullOrEmpty(info.TagUrl) ? info.TagUrl : null);
                _db.AddParameter(cmd, "TagId", info.TagId);
                _db.AddParameter(cmd, "Id", info.Id);
                var numberOfRow = cmd.ExecuteNonQuery();

                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        /// <summary>
        /// Xóa dữ liệu trong bảng LiveGame theo Id
        /// </summary>
        /// <param name="liveGameId"></param>
        /// <returns></returns>
        public bool Delete(int liveGameId)
        {
            const string commandText = "CMS_LiveGame_Delete";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", liveGameId);
                var numberOfRow = cmd.ExecuteNonQuery();

                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        /// <summary>
        /// Lấy thông tin live game theo id
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        public LiveGameEntity GetLiveGameById(int Id)
        {
            const string commandText = "CMS_Livegame_SelectById";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", Id);
                var numberOfRow = _db.Get<LiveGameEntity>(cmd);

                return numberOfRow;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        /// <summary>
        /// Lấy danh sách LiveGame
        /// </summary>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <param name="totalRow"></param>
        /// <returns></returns>
        public List<LiveGameEntity> ListLiveGameByPages(string search, int pageIndex, int pageSize, ref int totalRow)
        {
            const string commandText = "CMS_LiveGame_Listdata";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "TotalRow", totalRow, ParameterDirection.Output);
                _db.AddParameter(cmd, "Search", search);
                _db.AddParameter(cmd, "PageIndex", pageIndex);
                _db.AddParameter(cmd, "PageSize", pageSize);
                var numberOfRow = _db.GetList<LiveGameEntity>(cmd);
                totalRow = Utility.ConvertToInt(cmd.Parameters[0]);
                return numberOfRow;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        #region Core members

        private readonly CmsMainDb _db;

        protected LiveGameDalBase(CmsMainDb db)
        {
            _db = db;
        }

        protected CmsMainDb Database
        {
            get { return _db; }
        }

        #endregion
    }
}
