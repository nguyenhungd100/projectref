﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ChannelVN.LiveGameMatch.Entity;
using System.Data.SqlClient;
using ChannelVN.CMS.Common;
using System.Data;
using ChannelVN.LiveGameMatch.MainDal.Databases;

namespace ChannelVN.LiveGameMatch.MainDal
{
    public abstract class LiveGameMatchDalBase
    {
        public bool Insert(LiveGameMatchEntity info, ref int id)
        {
            const string commandText = "CMS_LiveGameMatch_Insert";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", id, ParameterDirection.Output);
                _db.AddParameter(cmd, "TeamIdA", info.TeamIdA);
                _db.AddParameter(cmd, "TeamIdB", info.TeamIdB);
                _db.AddParameter(cmd, "ResultA", info.ResultA);
                _db.AddParameter(cmd, "ResultB", info.ResultB);
                _db.AddParameter(cmd, "DiplayTime", info.DisplayTime);
                _db.AddParameter(cmd, "Description", !string.IsNullOrEmpty(info.Description) ? info.Description : null);
                _db.AddParameter(cmd, "CreatedDate", info.CreatedDate);
                _db.AddParameter(cmd, "CreatedDateStamp", info.CreatedDateStamp);
                _db.AddParameter(cmd, "GameId", info.GameId);
                _db.AddParameter(cmd, "GameLeagueId", info.GameLeagueId);
                _db.AddParameter(cmd, "NewsId", info.NewsId);
                //_db.AddParameter(cmd, "TagItem", !string.IsNullOrEmpty(info.TagItem)?info.TagItem:null);
                //_db.AddParameter(cmd, "TagUrl", !string.IsNullOrEmpty(info.TagUrl)?info.TagUrl:null);
                //_db.AddParameter(cmd, "TagId", info.TagId);
                _db.AddParameter(cmd, "EmbedReplay", !string.IsNullOrEmpty(info.EmbedReplay) ? info.EmbedReplay : null);
                _db.AddParameter(cmd, "EmbedLive", !string.IsNullOrEmpty(info.EmbedLive) ? info.EmbedLive : null);
                _db.AddParameter(cmd, "CaptionEmbedReplay", !string.IsNullOrEmpty(info.CaptionEmbedReplay) ? info.CaptionEmbedReplay : null);
                _db.AddParameter(cmd, "CaptionEmbedLive", !string.IsNullOrEmpty(info.CaptionEmbedLive) ? info.CaptionEmbedLive : null);
                _db.AddParameter(cmd, "LiveStatus", info.LiveStatus);
                _db.AddParameter(cmd, "CatId", info.CatId);
                _db.AddParameter(cmd, "NewsTitle", info.NewsTitle);
                _db.AddParameter(cmd, "NewsImage", info.NewsImage);
                _db.AddParameter(cmd, "NewsIsFocus", info.NewsIsFocus);
                _db.AddParameter(cmd, "NewsStatus", info.NewsStatus);
                var numberOfRow = cmd.ExecuteNonQuery();
                id = Utility.ConvertToInt(cmd.Parameters[0]);
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public bool Update(LiveGameMatchEntity info)
        {
            const string commandText = "CMS_LiveGameMatch_Update";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "TeamIdA", info.TeamIdA);
                _db.AddParameter(cmd, "TeamIdB", info.TeamIdB);
                _db.AddParameter(cmd, "ResultA", info.ResultA);
                _db.AddParameter(cmd, "ResultB", info.ResultB);
                _db.AddParameter(cmd, "DiplayTime", info.DisplayTime);
                _db.AddParameter(cmd, "Description", !string.IsNullOrEmpty(info.Description) ? info.Description : null);
                _db.AddParameter(cmd, "GameId", info.GameId);
                _db.AddParameter(cmd, "GameLeagueId", info.GameLeagueId);
                //_db.AddParameter(cmd, "TagItem", !string.IsNullOrEmpty(info.TagItem)?info.TagItem:null);
                //_db.AddParameter(cmd, "TagUrl", !string.IsNullOrEmpty(info.TagUrl)?info.TagUrl:null);
                //_db.AddParameter(cmd, "TagId", info.TagId);
                _db.AddParameter(cmd, "EmbedReplay", !string.IsNullOrEmpty(info.EmbedReplay) ? info.EmbedReplay : null);
                _db.AddParameter(cmd, "EmbedLive", !string.IsNullOrEmpty(info.EmbedLive) ? info.EmbedLive : null);
                _db.AddParameter(cmd, "CaptionEmbedReplay", !string.IsNullOrEmpty(info.CaptionEmbedReplay) ? info.CaptionEmbedReplay : null);
                _db.AddParameter(cmd, "CaptionEmbedLive", !string.IsNullOrEmpty(info.CaptionEmbedLive) ? info.CaptionEmbedLive : null);
                _db.AddParameter(cmd, "LiveStatus", info.LiveStatus);
                _db.AddParameter(cmd, "Id", info.Id);
                var numberOfRow = cmd.ExecuteNonQuery();

                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public bool Delete(long newsId)
        {
            const string commandText = "CMS_LiveGameMatch_Delete";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Newsid", newsId);
                var numberOfRow = cmd.ExecuteNonQuery();

                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public bool UpdatePublishDate(DateTime publishDate, long publishDateStamp, int gameMatchId)
        {
            const string commandText = "CMS_LiveGameMatch_UpdatePublishDate";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "PublishDate", publishDate);
                _db.AddParameter(cmd, "PublishDateStamp", publishDateStamp);
                _db.AddParameter(cmd, "Id", gameMatchId);
                var numberOfRow = cmd.ExecuteNonQuery();

                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public LiveGameMatchEntity GetGameMatchById(int Id)
        {
            const string commandText = "CMS_LivegameMatch_SelectById";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", Id);
                var numberOfRow = _db.Get<LiveGameMatchEntity>(cmd);

                return numberOfRow;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public LiveGameMatchEntity GetGameMatchByNewsId(long newsId)
        {
            const string commandText = "CMS_LivegameMatch_SelectByNewsId";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "newsId", newsId);
                var numberOfRow = _db.Get<LiveGameMatchEntity>(cmd);

                return numberOfRow;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        #region Core members

        private readonly CmsMainDb _db;

        protected LiveGameMatchDalBase(CmsMainDb db)
        {
            _db = db;
        }

        protected CmsMainDb Database
        {
            get { return _db; }
        }

        #endregion
    }
}
