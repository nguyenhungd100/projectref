﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using ChannelVN.CMS.Common;
using System.Data;
using ChannelVN.LiveGameMatch.Entity;
using ChannelVN.LiveGameMatch.MainDal.Databases;

namespace ChannelVN.LiveGameMatch.MainDal
{
    public abstract class LiveTeamGameDalBase
    {
        /// <summary>
        /// Thêm mới dữ liệu vào bảng LiveTeamGame
        /// </summary>
        /// <param name="info"></param>
        /// <param name="id"></param>
        /// <returns></returns>
        public bool Insert(LiveTeamGameEntity info, ref int id)
        {
            const string commandText = "CMS_LiveTeamGame_Insert";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", id, ParameterDirection.Output);
                _db.AddParameter(cmd, "Name", info.Name);
                _db.AddParameter(cmd, "Description", !string.IsNullOrEmpty(info.Description) ? info.Description : null);
                _db.AddParameter(cmd, "Flag", !string.IsNullOrEmpty(info.Flag) ? info.Flag : null);
                _db.AddParameter(cmd, "TagItem", !string.IsNullOrEmpty(info.TagItem) ? info.TagItem : null);
                _db.AddParameter(cmd, "TagUrl", !string.IsNullOrEmpty(info.TagUrl) ? info.TagUrl : null);
                _db.AddParameter(cmd, "TagId", info.TagId);
                var numberOfRow = cmd.ExecuteNonQuery();
                id = Utility.ConvertToInt(cmd.Parameters[0]);
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        /// <summary>
        /// Sửa dữ liệu cho bảng LiveTeamGame
        /// </summary>
        /// <param name="info"></param>
        /// <returns></returns>
        public bool Update(LiveTeamGameEntity info)
        {
            const string commandText = "CMS_LiveTeamGame_Update";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Name", info.Name);
                _db.AddParameter(cmd, "Description", !string.IsNullOrEmpty(info.Description) ? info.Description : null);
                _db.AddParameter(cmd, "Flag", !string.IsNullOrEmpty(info.Flag) ? info.Flag : null);
                _db.AddParameter(cmd, "TagItem", !string.IsNullOrEmpty(info.TagItem) ? info.TagItem : null);
                _db.AddParameter(cmd, "TagUrl", !string.IsNullOrEmpty(info.TagUrl) ? info.TagUrl : null);
                _db.AddParameter(cmd, "TagId", info.TagId);
                _db.AddParameter(cmd, "Id", info.Id);
                var numberOfRow = cmd.ExecuteNonQuery();

                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        /// <summary>
        /// Xóa dữ liệu trong bảng LiveTeamGame theo Id
        /// </summary>
        /// <param name="liveGameId"></param>
        /// <returns></returns>
        public bool Delete(int liveGameId)
        {
            const string commandText = "CMS_LiveTeamGame_Delete";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", liveGameId);
                var numberOfRow = cmd.ExecuteNonQuery();

                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        /// <summary>
        /// Lấy thông tin live team game theo id
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        public LiveTeamGameEntity GetLiveTeamGameById(int Id)
        {
            const string commandText = "CMS_LiveTeamGame_SelectById";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", Id);
                var numberOfRow = _db.Get<LiveTeamGameEntity>(cmd);

                return numberOfRow;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        /// <summary>
        /// Lấy danh sách LiveTeamGame
        /// </summary>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <param name="totalRow"></param>
        /// <returns></returns>
        public List<LiveTeamGameEntity> ListLiveTeamGameByPages(string search, int pageIndex, int pageSize, ref int totalRow)
        {
            const string commandText = "CMS_LiveTeamGame_Listdata";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "TotalRow", totalRow, ParameterDirection.Output);
                _db.AddParameter(cmd, "Search", search);
                _db.AddParameter(cmd, "PageIndex", pageIndex);
                _db.AddParameter(cmd, "PageSize", pageSize);
                var numberOfRow = _db.GetList<LiveTeamGameEntity>(cmd);
                totalRow = Utility.ConvertToInt(cmd.Parameters[0]);
                return numberOfRow;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public List<LiveTeamGameEntity> ListLiveTeamGameByMultiId(int teamA, int teamB)
        {
            const string commandText = "CMS_LiveTeamGame_ListdataMultiId";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "TeamA", teamA);
                _db.AddParameter(cmd, "TeamB", teamB);
                var numberOfRow = _db.GetList<LiveTeamGameEntity>(cmd);

                return numberOfRow;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        #region Core members

        private readonly CmsMainDb _db;

        protected LiveTeamGameDalBase(CmsMainDb db)
        {
            _db = db;
        }

        protected CmsMainDb Database
        {
            get { return _db; }
        }

        #endregion
    }
}
