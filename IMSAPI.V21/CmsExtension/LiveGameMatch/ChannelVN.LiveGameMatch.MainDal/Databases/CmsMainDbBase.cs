﻿using System.Data;
using ChannelVN.CMS.Common.DalCommon;

namespace ChannelVN.LiveGameMatch.MainDal.Databases
{
    /// <summary>
    /// The base class for the <see cref="CmsMainDb"/> class that 
    /// represents a connection to the <c>CmsMainDb</c> database. 
    /// </summary>
    /// <remarks>
    /// Do not change this source code. Modify the CmsMainDb class
    /// if you need to add or change some functionality.
    /// </remarks>
    public abstract class CmsMainDbBase : MainDbBase
    {
        #region Store procedures

        #region LiveGame

        private LiveGameDal _liveGameMainDal;
        public LiveGameDal LiveGameMainDal
        {
            get { return _liveGameMainDal ?? (_liveGameMainDal = new LiveGameDal((CmsMainDb)this)); }
        }

        #endregion

        #region Live Game League

        private LiveGameLeagueDal _liveGameLeagueMainDal;
        public LiveGameLeagueDal LiveGameLeagueMainDal
        {
            get { return _liveGameLeagueMainDal ?? (_liveGameLeagueMainDal = new LiveGameLeagueDal((CmsMainDb)this)); }
        }

        #endregion

        #region Job Assignment

        private LiveGameMatchDal _liveGameMatchMainDal;
        public LiveGameMatchDal LiveGameMatchMainDal
        {
            get { return _liveGameMatchMainDal ?? (_liveGameMatchMainDal = new LiveGameMatchDal((CmsMainDb)this)); }
        }

        #endregion

        #region Job Assignment

        private LiveTeamGameDal _liveTeamGameMainDal;
        public LiveTeamGameDal LiveTeamGameMainDal
        {
            get { return _liveTeamGameMainDal ?? (_liveTeamGameMainDal = new LiveTeamGameDal((CmsMainDb)this)); }
        }

        #endregion

        #endregion

        #region Constructors

        protected CmsMainDbBase()
        {
        }
        protected CmsMainDbBase(bool init)
        {
            if (init)
            {
                InitConnection();
            }
        }

        #endregion

        #region Protected members

        protected override sealed void InitConnection()
        {
            _connection = CreateConnection();
            _connection.Open();
        }

        /// <summary>
        /// Creates a new connection to the database.
        /// </summary>
        /// <returns>A reference to the <see cref="System.Data.IDbConnection"/> object.</returns>
        protected abstract IDbConnection CreateConnection();

        #endregion
    }
}