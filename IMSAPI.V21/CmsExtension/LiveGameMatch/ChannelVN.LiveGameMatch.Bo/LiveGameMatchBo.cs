﻿using ChannelVN.LiveGameMatch.Dal;
using ChannelVN.LiveGameMatch.Entity;
using ChannelVN.LiveGameMatch.Entity.ErrorCode;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ChannelVN.LiveGameMatch.Bo
{
    public class LiveGameMatchBo
    {
        public static ErrorMapping.ErrorCodes Insert(LiveGameMatchEntity info, ref int id)
        {
            var result = ErrorMapping.ErrorCodes.BusinessError;
            var createSuccess = LiveGameMatchDal.Insert(info, ref id);
            if (createSuccess && id > 0)
                result = ErrorMapping.ErrorCodes.Success;
            return result;
        }

        public static ErrorMapping.ErrorCodes Update(LiveGameMatchEntity info)
        {
            var result = ErrorMapping.ErrorCodes.BusinessError;
            var createSuccess = LiveGameMatchDal.Update(info);
            if (createSuccess && info.Id > 0)
                result = ErrorMapping.ErrorCodes.Success;
            return result;
        }

        public static ErrorMapping.ErrorCodes Delete(long newsId)
        {
            var result = ErrorMapping.ErrorCodes.BusinessError;
            var createSuccess = LiveGameMatchDal.Delete(newsId);
            if (createSuccess)
                result = ErrorMapping.ErrorCodes.Success;
            return result;
        }

        public static LiveGameMatchEntity GetGameMatchById(int Id) {
            return LiveGameMatchDal.GetGameMatchById(Id);
        }

        public static LiveGameMatchEntity GetGameMatchByNewsId(long newsId) {
            return LiveGameMatchDal.GetGameMatchByNewsId(newsId);
        }

        public static ErrorMapping.ErrorCodes UpdatePublishDate(DateTime publishDate, long publishDateStamp, int gameMatchId) {
            var result = ErrorMapping.ErrorCodes.BusinessError;
            var createSuccess = LiveGameMatchDal.UpdatePublishDate(publishDate, publishDateStamp, gameMatchId);
            if (createSuccess)
                result = ErrorMapping.ErrorCodes.Success;
            return result;
        }
    }
}
