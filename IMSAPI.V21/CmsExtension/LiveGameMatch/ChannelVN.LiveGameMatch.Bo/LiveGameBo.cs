﻿using ChannelVN.LiveGameMatch.Dal;
using ChannelVN.LiveGameMatch.Entity;
using ChannelVN.LiveGameMatch.Entity.ErrorCode;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ChannelVN.LiveGameMatch.Bo
{
    public class LiveGameBo
    {
        public static ErrorMapping.ErrorCodes Insert(LiveGameEntity info, ref int id)
        {
            var result = ErrorMapping.ErrorCodes.BusinessError;
            var createSuccess = LiveGameDal.Insert(info, ref id);
            if (createSuccess && id > 0)
                result = ErrorMapping.ErrorCodes.Success;
            return result;
        }

        public static ErrorMapping.ErrorCodes Update(LiveGameEntity info)
        {
            var result = ErrorMapping.ErrorCodes.BusinessError;
            var createSuccess = LiveGameDal.Update(info);
            if (createSuccess && info.Id > 0)
                result = ErrorMapping.ErrorCodes.Success;
            return result;
        }

        public static ErrorMapping.ErrorCodes Delete(int id)
        {
            var result = ErrorMapping.ErrorCodes.BusinessError;
            var createSuccess = LiveGameDal.Delete(id);
            if (createSuccess && id > 0)
                result = ErrorMapping.ErrorCodes.Success;
            return result;
        }

        public static List<LiveGameEntity> ListLiveGameByPages(string search,int pageIndex, int pageSize, ref int totalRow) {
            return LiveGameDal.ListLiveGameByPages(search, pageIndex, pageSize, ref totalRow);
        }

        public static LiveGameEntity GetLiveGameById(int Id) {
            return LiveGameDal.GetLiveGameById(Id);
        }
    }
}
