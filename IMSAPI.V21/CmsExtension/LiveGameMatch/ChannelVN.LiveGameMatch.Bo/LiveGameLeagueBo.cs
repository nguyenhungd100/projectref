﻿using ChannelVN.LiveGameMatch.Dal;
using ChannelVN.LiveGameMatch.Entity;
using ChannelVN.LiveGameMatch.Entity.ErrorCode;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ChannelVN.LiveGameMatch.Bo
{
    public class LiveGameLeagueBo
    {
        public static ErrorMapping.ErrorCodes Insert(LiveGameLeagueEntity info, ref int id)
        {
            var result = ErrorMapping.ErrorCodes.BusinessError;
            var createSuccess = LiveGameLeagueDal.Insert(info, ref id);
            if (createSuccess && id > 0)
                result = ErrorMapping.ErrorCodes.Success;
            return result;
        }

        public static ErrorMapping.ErrorCodes Update(LiveGameLeagueEntity info)
        {
            var result = ErrorMapping.ErrorCodes.BusinessError;
            var createSuccess = LiveGameLeagueDal.Update(info);
            if (createSuccess && info.Id > 0)
                result = ErrorMapping.ErrorCodes.Success;
            return result;
        }

        public static ErrorMapping.ErrorCodes Delete(int id)
        {
            var result = ErrorMapping.ErrorCodes.BusinessError;
            var createSuccess = LiveGameLeagueDal.Delete(id);
            if (createSuccess && id > 0)
                result = ErrorMapping.ErrorCodes.Success;
            return result;
        }

        public static List<LiveGameLeagueEntity> ListLiveGameLeagueByPages(string search, int pageIndex, int pageSize, ref int totalRow)
        {
            return LiveGameLeagueDal.ListLiveGameLeagueByPages(search, pageIndex, pageSize, ref totalRow);
        }

        public static LiveGameLeagueEntity GetLiveGameLeagueById(int Id)
        {
            return LiveGameLeagueDal.GetLiveGameLeagueById(Id);
        }
    }
}
