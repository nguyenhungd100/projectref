﻿using System;
using System.Collections.Generic;
using ChannelVN.Dashboard.Entity;
using ChannelVN.Dashboard.MainDal.Databases;

namespace ChannelVN.Dashboard.MainDal
{
    public abstract class TrackingVisitDalBase
    {
        public List<PageViewEntity> ListPageViewByTime(int LoaiTk, string Db)
        {
            const string commandText = "CMS_Statistics_ViewVisit";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "LoaiTK", LoaiTk);
                _db.AddParameter(cmd, "Db", Db);
                var data = _db.GetList<PageViewEntity>(cmd);
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        #region Core members

        private readonly CmsTrackingVisitDb _db;

        protected TrackingVisitDalBase(CmsTrackingVisitDb db)
        {
            _db = db;
        }

        protected CmsTrackingVisitDb Database
        {
            get { return _db; }
        }

        #endregion

    }
}
