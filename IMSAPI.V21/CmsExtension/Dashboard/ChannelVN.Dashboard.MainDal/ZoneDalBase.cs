﻿using System;
using System.Collections.Generic;
using ChannelVN.Dashboard.Entity;
using ChannelVN.Dashboard.MainDal.Databases;

namespace ChannelVN.Dashboard.MainDal
{
    public abstract class ZoneDalBase
    {
        public List<ZoneEntity> ListZoneByUserName(string Username)
        {
            const string commandText = "CMS_Zone_GetByUsername";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "UserName", Username);
                var data = _db.GetList<ZoneEntity>(cmd);
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public List<ZoneParentEntity> ListZoneParentByUserName(string Username)
        {
            const string commandText = "CMS_ZoneParent_GetByUsername";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "UserName", Username);
                var data = _db.GetList<ZoneParentEntity>(cmd);
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public List<ZoneEntity> ListZoneById(int ZoneId)
        {
            const string commandText = "CMS_Zone_ListById";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "ZoneId", ZoneId);
                var data = _db.GetList<ZoneEntity>(cmd);
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public List<ZoneEntity> ListZone()
        {
            const string commandText = "CMS_Zone_GetAll";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                var data = _db.GetList<ZoneEntity>(cmd);
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        #region Core members

        private readonly CmsMainDb _db;

        protected ZoneDalBase(CmsMainDb db)
        {
            _db = db;
        }

        protected CmsMainDb Database
        {
            get { return _db; }
        }

        #endregion

    }
}
