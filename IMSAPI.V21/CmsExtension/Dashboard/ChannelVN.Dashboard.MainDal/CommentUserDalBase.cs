﻿using System;
using System.Data;
using ChannelVN.CMS.Common;
using ChannelVN.Dashboard.Entity;
using ChannelVN.Dashboard.MainDal.Databases;

namespace ChannelVN.Dashboard.MainDal
{
    public abstract class CommentUserDalBase
    {
        public CountUserEntity Counter_CommentUserByTime(int LoaiTK)
        {
            const string commandText = "CMS_STATISTICS_COMMENTUSER";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "LoaiTK", LoaiTK);
                _db.AddParameter(cmd, "CommentUserPending", SqlDbType.Int, ParameterDirection.Output);
                _db.AddParameter(cmd, "CommentUserApproved", SqlDbType.Int, ParameterDirection.Output);
                _db.AddParameter(cmd, "CommentUserBlock", SqlDbType.Int, ParameterDirection.Output);
                _db.AddParameter(cmd, "CountUserKyNay", SqlDbType.Int, ParameterDirection.Output);
                _db.AddParameter(cmd, "CountUserKyTruoc", SqlDbType.Int, ParameterDirection.Output);
                _db.AddParameter(cmd, "CountUserOldTime", SqlDbType.Float, ParameterDirection.Output);
                _db.AddParameter(cmd, "AverageUser", SqlDbType.Float, ParameterDirection.Output);
                _db.AddParameter(cmd, "GrowthUser", SqlDbType.Float, ParameterDirection.Output);
                var data = _db.GetList<ListTime>(cmd);
                var returnObj = new CountUserEntity();
                returnObj.ListTime = data;
                returnObj.UserPending = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 1));
                returnObj.UserApproved = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 2));
                returnObj.UserBlock = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 3));
                returnObj.CountUserKyNay = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 4));
                returnObj.CountUserKyTruoc = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 5));
                returnObj.Total = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 6));
                returnObj.AverageUser = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 7));
                returnObj.GrowthUser = Utility.ConvertToDouble(_db.GetParameterValueFromCommand(cmd, 8));
                return returnObj;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public StatisticsBoxTongHopEntity Counter_CommentUserPendingByTime(int LoaiTK)
        {
            const string commandText = "CMS_STATISTICS_BOXCOMMENTUSER";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "LoaiTK", LoaiTK);
                _db.AddParameter(cmd, "CommentUserPending", SqlDbType.Int, ParameterDirection.Output);
                _db.AddParameter(cmd, "CommentUserPendingT1", SqlDbType.Int, ParameterDirection.Output);
                _db.AddParameter(cmd, "CommentUserPendingT2", SqlDbType.Int, ParameterDirection.Output);
                _db.AddParameter(cmd, "CommentUserPendingT3", SqlDbType.Int, ParameterDirection.Output);
                _db.ExecuteReader(cmd);
                var data = new StatisticsBoxTongHopEntity();
                data.UserPending = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 1));
                data.UserPendingT1 = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 2));
                data.UserPendingT2 = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 3));
                data.UserPendingT3 = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 4));
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        #region Core members

        private readonly CmsCommentDb _db;

        protected CommentUserDalBase(CmsCommentDb db)
        {
            _db = db;
        }

        protected CmsCommentDb Database
        {
            get { return _db; }
        }

        #endregion

    }
}
