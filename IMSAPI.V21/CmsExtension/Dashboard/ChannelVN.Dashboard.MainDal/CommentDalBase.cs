﻿using System;
using System.Collections.Generic;
using System.Data;
using ChannelVN.CMS.Common;
using ChannelVN.Dashboard.Entity;
using ChannelVN.Dashboard.MainDal.Databases;

namespace ChannelVN.Dashboard.MainDal
{
    public abstract class CommentDalBase
    {
        public List<CommentEntity> Counter_Comment()
        {
            const string commandText = "CMS_STATISTICS_COMMENT";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                var data = _db.GetList<CommentEntity>(cmd);
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public List<CommentEntity> Counter_Comment_All()
        {
            const string commandText = "CMS_STATISTICS_COMMENT_ALL";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                var data = _db.GetList<CommentEntity>(cmd);
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public List<CommentEntity> ListCommentByZone(string listZone, string listZoneAll, int LoaiTK, ref int CountCommentOldTime)
        {
            const string commandText = "CMS_STATISTICS_COMMENTBYLISTZONE";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "LoaiTK", LoaiTK);
                _db.AddParameter(cmd, "listZoneAll", listZoneAll);
                _db.AddParameter(cmd, "listZone", listZone);
                _db.AddParameter(cmd, "CountCommentOldTime", SqlDbType.Int, ParameterDirection.Output);
                var data = _db.GetList<CommentEntity>(cmd);
                CountCommentOldTime = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 3));
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public List<CommentEntity> Counter_Comment_AllForever()
        {
            const string commandText = "CMS_STATISTICS_COMMENT_ALL_FOREVER";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                var data = _db.GetList<CommentEntity>(cmd);
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public List<CommentEntity> Counter_CommentByTime(CountCommentTemp ct, int LoaiTK)
        {
            const string commandText = "CMS_STATISTICS_CountCommentByTime";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "LoaiTK", LoaiTK);
                _db.AddParameter(cmd, "Total", SqlDbType.Int, ParameterDirection.Output);
                _db.AddParameter(cmd, "CountCommentCurrentTime", SqlDbType.Int, ParameterDirection.Output);
                _db.AddParameter(cmd, "CountCommentOldTime", SqlDbType.Int, ParameterDirection.Output);
                var data = _db.GetList<CommentEntity>(cmd);
                ct.Total = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd,1));
                ct.CountCommentCurrent = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 2));
                ct.CountCommentOld = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 3));
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        #region Core members

        private readonly CmsCommentDb _db;

        protected CommentDalBase(CmsCommentDb db)
        {
            _db = db;
        }

        protected CmsCommentDb Database
        {
            get { return _db; }
        }

        #endregion

    }
}
