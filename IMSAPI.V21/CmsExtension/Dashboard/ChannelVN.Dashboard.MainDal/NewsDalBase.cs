﻿using System;
using System.Collections.Generic;
using System.Data;
using ChannelVN.CMS.Common;
using ChannelVN.Dashboard.Entity;
using ChannelVN.Dashboard.MainDal.Databases;

namespace ChannelVN.Dashboard.MainDal
{
    public abstract class NewsDalBase
    {
        public StatisticsBoxTongHopEntity Counter_News(int LoaiTK, int ZoneId, string UserName)
        {
            const string commandText = "CMS_STATISTICS_NEWS";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "LoaiTK", LoaiTK);
                _db.AddParameter(cmd, "ZoneId", ZoneId);
                _db.AddParameter(cmd, "UserName", UserName);
                var data = _db.Get<StatisticsBoxTongHopEntity>(cmd);
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public StatisticsProductionNewsPublishEntity Counter_ProductionNews_Publish(int LoaiTK, int ZoneId, string UserName)
        {
            const string commandText = "CMS_STATISTICS_ProductionNews_Publish";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "LoaiTK", LoaiTK);
                _db.AddParameter(cmd, "ZoneId", ZoneId);
                _db.AddParameter(cmd, "UserName", UserName);
                _db.AddParameter(cmd, "CountPublishedNewsOldTime", SqlDbType.Int, ParameterDirection.Output);
                _db.AddParameter(cmd, "CountPublishedNewsCurrentTime", SqlDbType.Int, ParameterDirection.Output);
                _db.AddParameter(cmd, "TotalPublishedNewsKyTruoc", SqlDbType.Int, ParameterDirection.Output);
                _db.AddParameter(cmd, "GrowthNewsPublish", SqlDbType.Float, ParameterDirection.Output);
                var data = _db.GetList<ListTime>(cmd);
                var returnObj = new StatisticsProductionNewsPublishEntity();
                returnObj.ListTime = data;
                returnObj.CountPublishNewsOldTime = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 3));
                returnObj.CountPublishNewsCurrentTime = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 4));
                returnObj.TotalPublishNewsKyTruoc = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd,5));
                returnObj.GrowthNewsPublish = Utility.ConvertToDouble(_db.GetParameterValueFromCommand(cmd, 6));
                return returnObj;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public StatisticsProductionNewsSendEntity Counter_ProductionNews_Send(int LoaiTK, int ZoneId, string UserName)
        {
            const string commandText = "CMS_STATISTICS_ProductionNews_Send";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "LoaiTK", LoaiTK);
                _db.AddParameter(cmd, "ZoneId", ZoneId);
                _db.AddParameter(cmd, "UserName", UserName);
                _db.AddParameter(cmd, "CountSendNewsCurrentTime", SqlDbType.Int, ParameterDirection.Output);
                _db.AddParameter(cmd, "CountSendNewsOldTime", SqlDbType.Int, ParameterDirection.Output);
                _db.AddParameter(cmd, "TotalSendNewsKyTruoc", SqlDbType.Float, ParameterDirection.Output);
                _db.AddParameter(cmd, "GrowthNewsSend", SqlDbType.Float, ParameterDirection.Output);

                var data = _db.GetList<ListTime>(cmd);
                var returnObj = new StatisticsProductionNewsSendEntity();
                returnObj.ListTime = data;
                returnObj.CountSendNewsCurrentTime = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 3));
                returnObj.CountSendNewsOldTime = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 4));
                returnObj.TotalSendNewsKyTruoc = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 5));
                returnObj.GrowthNewsSend = Utility.ConvertToDouble(_db.GetParameterValueFromCommand(cmd, 6));
                return returnObj;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public List<ListPercent> Counter_NewsPercentByZone(int LoaiTK, string UserName)
        {
            const string commandText = "CMS_Statistic_Percent_NewsByZone";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "LoaiTK", LoaiTK);
                _db.AddParameter(cmd, "UserName", UserName);
                var data = _db.GetList<ListPercent>(cmd);
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public List<ListPercent> Counter_NewsPercentByCategory(int LoaiTK, int ZoneId, string UserName)
        {
            const string commandText = "CMS_Statistic_Percent_NewsByCategory";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "LoaiTK", LoaiTK);
                _db.AddParameter(cmd, "ZoneId", ZoneId);
                _db.AddParameter(cmd, "UserName", UserName);
                var data = _db.GetList<ListPercent>(cmd);
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public List<TempTopPVEntity> STATISTICS_TOPPV(int LoaiTK, int ZoneId, string UserName, int TopSize, string keyword)
        {
            const string commandText = "CMS_STATISTICS_TOPPV";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "LoaiTK", LoaiTK);
                _db.AddParameter(cmd, "ZoneId", ZoneId);
                _db.AddParameter(cmd, "UserName", UserName);
                _db.AddParameter(cmd, "TopSize", TopSize);
                _db.AddParameter(cmd, "ListUser", keyword);
                var data = _db.GetList<TempTopPVEntity>(cmd);
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        #region Core members

        private readonly CmsMainDb _db;

        protected NewsDalBase(CmsMainDb db)
        {
            _db = db;
        }

        protected CmsMainDb Database
        {
            get { return _db; }
        }

        #endregion

    }
}
