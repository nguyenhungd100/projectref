﻿using System;
using System.Collections.Generic;
using System.Data;
using ChannelVN.CMS.Common;
using ChannelVN.Dashboard.Entity;
using ChannelVN.Dashboard.MainDal.Databases;

namespace ChannelVN.Dashboard.MainDal
{
    public abstract class VideoDalBase
    {
        public StatisticsProductionVideoPublishEntity Counter_ProductionVideo_Publish(int LoaiTK, int ZoneId, string UserName)
        {
            const string commandText = "CMS_STATISTICS_ProductionVideo_Publish";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "LoaiTK", LoaiTK);
                _db.AddParameter(cmd, "ZoneId", ZoneId);
                _db.AddParameter(cmd, "UserName", UserName);
                _db.AddParameter(cmd, "CountPublishedVideoCurrentTime", SqlDbType.Int, ParameterDirection.Output);
                _db.AddParameter(cmd, "CountPublishedVideoOldTime", SqlDbType.Int, ParameterDirection.Output);
                _db.AddParameter(cmd, "TotalPublishedVideoKyTruoc", SqlDbType.Int, ParameterDirection.Output);
                _db.AddParameter(cmd, "GrowthVideoPublish", SqlDbType.Float, ParameterDirection.Output);
                var _list = _db.GetList<ListTime>(cmd);
                var returnObj = new StatisticsProductionVideoPublishEntity();
                returnObj.ListTime = _list;
                returnObj.CountPublishVideoCurrentTime = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 3));
                returnObj.CountPublishVideoOldTime = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 4));
                returnObj.TotalPublishVideoKytruoc = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 5));
                returnObj.GrowthVideoPublish = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 6));
                return returnObj;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public StatisticsProductionVideoSendEntity Counter_ProductionVideo_Send(int LoaiTK, int ZoneId, string UserName)
        {
            const string commandText = "CMS_STATISTICS_ProductionVideo_Send";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "LoaiTK", LoaiTK);
                _db.AddParameter(cmd, "ZoneId", ZoneId);
                _db.AddParameter(cmd, "UserName", UserName);
                _db.AddParameter(cmd, "CountSendVideoCurrentTime", SqlDbType.Int, ParameterDirection.Output);
                _db.AddParameter(cmd, "CountSendVideoOldTime", SqlDbType.Int, ParameterDirection.Output);
                _db.AddParameter(cmd, "TotalSendVideoKyTruoc", SqlDbType.Int, ParameterDirection.Output);
                _db.AddParameter(cmd, "GrowthVideoSend", SqlDbType.Float, ParameterDirection.Output);
                var _list = _db.GetList<ListTime>(cmd);
                var returnObj = new StatisticsProductionVideoSendEntity();
                returnObj.ListTime = _list;
                returnObj.CountSendVideoCurrentTime = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 3));
                returnObj.CountSendVideoOldTime = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 4));
                returnObj.TotalSendVideoKyTruoc = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 5));
                returnObj.GrowthVideoSend = Utility.ConvertToDouble(_db.GetParameterValueFromCommand(cmd, 6));
                return returnObj;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public List<ListPercent> Counter_VideoPercentByZone(int LoaiTK, string UserName)
        {
            const string commandText = "CMS_Statistic_Percent_VideoByZone";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "LoaiTK", LoaiTK);
                _db.AddParameter(cmd, "UserName", UserName);
                var data = _db.GetList<ListPercent>(cmd);
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        #region Core members

        private readonly CmsMainDb _db;

        protected VideoDalBase(CmsMainDb db)
        {
            _db = db;
        }

        protected CmsMainDb Database
        {
            get { return _db; }
        }

        #endregion

    }
}
