﻿using System.Data;
using System.Data.SqlClient;
using ChannelVN.CMS.Common.ChannelConfig;
using ChannelVN.Dashboard.MainDal.Common;
using ChannelVN.WcfExtensions;

namespace ChannelVN.Dashboard.MainDal.Databases
{
    public class CmsMainDb : CmsMainDbBase
    {
        private const string ConnectionStringName = "NewsConnect";

        /// <summary>
        /// Creates a new connection to the database.
        /// </summary>
        /// <returns>An <see cref="System.Data.IDbConnection"/> object.</returns>
        protected override IDbConnection CreateConnection()
        {
            string Namespace = "Kenh14";//WcfMessageHeader.Current.Namespace
            var strConn = ServiceChannelConfiguration.GetConnectionString(Namespace,
                ConnectionStringName, Constants.ConnectionDecryptKey);
            return new SqlConnection(strConn);
        }
    }
}