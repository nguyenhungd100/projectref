﻿using System.Data;
using ChannelVN.CMS.Common.DalCommon;

namespace ChannelVN.Dashboard.MainDal.Databases
{
    /// <summary>
    /// The base class for the <see cref="CmsMainDb"/> class that 
    /// represents a connection to the <c>CmsMainDb</c> database. 
    /// </summary>
    /// <remarks>
    /// Do not change this source code. Modify the CmsMainDb class
    /// if you need to add or change some functionality.
    /// </remarks>
    public abstract class CmsTrackingVisitDbBase : MainDbBase
    {
        #region Store procedures

        #region TrackingVisit

        private TrackingVisitDal _TrackingVisitDal;
        public TrackingVisitDal TrackingVisitDal
        {
            get { return _TrackingVisitDal ?? (_TrackingVisitDal = new TrackingVisitDal((CmsTrackingVisitDb)this)); }
        }

        #endregion


        #endregion

        #region Constructors

        protected CmsTrackingVisitDbBase()
        {
        }
        protected CmsTrackingVisitDbBase(bool init)
        {
            if (init)
            {
                InitConnection();
            }
        }

        #endregion

        #region Protected members

        protected override sealed void InitConnection()
        {
            _connection = CreateConnection();
            _connection.Open();
        }

        /// <summary>
        /// Creates a new connection to the database.
        /// </summary>
        /// <returns>A reference to the <see cref="System.Data.IDbConnection"/> object.</returns>
        protected abstract IDbConnection CreateConnection();

        #endregion
    }
}