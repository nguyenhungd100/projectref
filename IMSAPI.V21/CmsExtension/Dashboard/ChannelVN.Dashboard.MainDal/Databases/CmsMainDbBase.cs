﻿using System.Data;
using ChannelVN.CMS.Common.DalCommon;

namespace ChannelVN.Dashboard.MainDal.Databases
{
    /// <summary>
    /// The base class for the <see cref="CmsMainDb"/> class that 
    /// represents a connection to the <c>CmsMainDb</c> database. 
    /// </summary>
    /// <remarks>
    /// Do not change this source code. Modify the CmsMainDb class
    /// if you need to add or change some functionality.
    /// </remarks>
    public abstract class CmsMainDbBase : MainDbBase
    {
        #region Store procedures

        #region News

        private NewsDal _newsMainDal;
        public NewsDal NewsMainDal
        {
            get { return _newsMainDal ?? (_newsMainDal = new NewsDal((CmsMainDb)this)); }
        }

        private UsersDal _usersMainDal;
        public UsersDal UsersMainDal
        {
            get { return _usersMainDal ?? (_usersMainDal = new UsersDal((CmsMainDb)this)); }
        }

        #endregion

        #region Video

        private VideoDal _videoMainDal;
        public VideoDal VideoMainDal
        {
            get { return _videoMainDal ?? (_videoMainDal = new VideoDal((CmsMainDb)this)); }
        }

        #endregion
        #region Zone

        private ZoneDal _ZoneDal;
        public ZoneDal ZoneDal
        {
            get { return _ZoneDal ?? (_ZoneDal = new ZoneDal((CmsMainDb)this)); }
        }

        #endregion

        #endregion

        #region Constructors

        protected CmsMainDbBase()
        {
        }
        protected CmsMainDbBase(bool init)
        {
            if (init)
            {
                InitConnection();
            }
        }

        #endregion

        #region Protected members

        protected override sealed void InitConnection()
        {
            _connection = CreateConnection();
            _connection.Open();
        }

        /// <summary>
        /// Creates a new connection to the database.
        /// </summary>
        /// <returns>A reference to the <see cref="System.Data.IDbConnection"/> object.</returns>
        protected abstract IDbConnection CreateConnection();

        #endregion
    }
}