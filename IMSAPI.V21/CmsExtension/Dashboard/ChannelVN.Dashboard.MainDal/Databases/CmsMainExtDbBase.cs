﻿using System.Data;
using ChannelVN.CMS.Common.DalCommon;

namespace ChannelVN.Dashboard.MainDal.Databases
{
    /// <summary>
    /// The base class for the <see cref="CmsMainDb"/> class that 
    /// represents a connection to the <c>CmsMainDb</c> database. 
    /// </summary>
    /// <remarks>
    /// Do not change this source code. Modify the CmsMainDb class
    /// if you need to add or change some functionality.
    /// </remarks>
    public abstract class CmsMainExtDbBase : MainDbBase
    {
        #region Store procedures

        #region Magazine

        private NewsExtDal _newsExtMainDal;
        public NewsExtDal NewsExtMainDal
        {
            get { return _newsExtMainDal ?? (_newsExtMainDal = new NewsExtDal((CmsMainExtDb)this)); }
        }

        #endregion

        #region User

        private UsersExtDal _usersExtMainDal;
        public UsersExtDal UsersExtMainDal
        {
            get { return _usersExtMainDal ?? (_usersExtMainDal = new UsersExtDal((CmsMainExtDb)this)); }
        }

        #endregion

        #endregion

        #region Constructors

        protected CmsMainExtDbBase()
        {
        }
        protected CmsMainExtDbBase(bool init)
        {
            if (init)
            {
                InitConnection();
            }
        }

        #endregion

        #region Protected members

        protected override sealed void InitConnection()
        {
            _connection = CreateConnection();
            _connection.Open();
        }

        /// <summary>
        /// Creates a new connection to the database.
        /// </summary>
        /// <returns>A reference to the <see cref="System.Data.IDbConnection"/> object.</returns>
        protected abstract IDbConnection CreateConnection();

        #endregion
    }
}