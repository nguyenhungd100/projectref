﻿using System;
using System.Collections.Generic;
using ChannelVN.Dashboard.Entity;
using ChannelVN.Dashboard.MainDal.Databases;

namespace ChannelVN.Dashboard.MainDal
{
    public abstract class UsersExtDalBase
    {
        public List<UserEntity> ListExternalUser(string listUser)
        {
            const string commandText = "CMS_Statistics_GetUserByListUserName";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "listUser", listUser);
                var data = _db.GetList<UserEntity>(cmd);
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public List<UserEntity> ListExternalUser_Search(string KeyWord)
        {
            const string commandText = "CMS_User_Search";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "KeyWord", KeyWord);
                var data = _db.GetList<UserEntity>(cmd);
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
       
        #region Core members

        private readonly CmsMainExtDb _db;

        protected UsersExtDalBase(CmsMainExtDb db)
        {
            _db = db;
        }

        protected CmsMainExtDb Database
        {
            get { return _db; }
        }

        #endregion

    }
}
