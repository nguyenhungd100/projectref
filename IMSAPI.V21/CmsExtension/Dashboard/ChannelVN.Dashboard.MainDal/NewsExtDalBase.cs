﻿using System;
using System.Collections.Generic;
using System.Data;
using ChannelVN.CMS.Common;
using ChannelVN.Dashboard.Entity;
using ChannelVN.Dashboard.MainDal.Databases;

namespace ChannelVN.Dashboard.MainDal
{
    public abstract class NewsExtDalBase
    {
        public List<ExternalNewsEntity> ListCounter_ExternalNews(int LoaiTK, int ZoneId)
        {
            const string commandText = "CMS_STATISTICS_EXTERNALNEWS";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "LoaiTK", LoaiTK);
                _db.AddParameter(cmd, "ZoneId", ZoneId);
                var data = _db.GetList<ExternalNewsEntity>(cmd);
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public StatisticsBoxTongHopEntity Counter_ExternalNews(int LoaiTK, int ZoneId)
        {
            const string commandText = "CMS_STATISTICS_EXTERNALNEWS";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "LoaiTK", LoaiTK);
                _db.AddParameter(cmd, "ZoneId", ZoneId);
                _db.AddParameter(cmd, "Cooperator", SqlDbType.Int, ParameterDirection.Output);
                _db.AddParameter(cmd, "CooperatorT1", SqlDbType.Int, ParameterDirection.Output);
                _db.AddParameter(cmd, "CooperatorT2", SqlDbType.Int, ParameterDirection.Output);
                _db.AddParameter(cmd, "CooperatorT3", SqlDbType.Float, ParameterDirection.Output);
                var data = _db.Get<StatisticsBoxTongHopEntity>(cmd);
                data.Cooperator = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 2));
                data.CooperatorT1 = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 3));
                data.CooperatorT2 = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd,4));
                data.CooperatorT3 = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 5));
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        #region Core members

        private readonly CmsMainExtDb _db;

        protected NewsExtDalBase(CmsMainExtDb db)
        {
            _db = db;
        }

        protected CmsMainExtDb Database
        {
            get { return _db; }
        }

        #endregion

    }
}
