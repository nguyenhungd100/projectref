﻿using ChannelVN.CMS.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ChannelVN.Dashboard.Entity
{
    public class StatisticsView_VisitEntity : EntityBase
    {
        public int TotalViewKyNay { get; set; }
        public int TotalViewKyTruoc { get; set; }
        public int TotalVisitKyNay { get; set; }
        public int TotalVisitKyTruoc { get; set; }
        public double ViewGrowth { get; set; }
        public double VisitGrowth { get; set; }
        public List<PageViewEntity> ListViewKyNay { get; set; }
        public List<PageViewEntity> ListViewKyTruoc { get; set; }
        public List<PageViewEntity> ListVisitKyNay { get; set; }
        public List<PageViewEntity> ListVisitKyTruoc { get; set; }
    }
    public class StatisticsView_VisitEntityByZone : EntityBase
    {
        public List<ViewZoneEntity> ListViewKyNay { get; set; }
        public List<ViewZoneEntity> ListViewKyTruoc { get; set; }
        public List<VisitZoneEntity> ListVisitKyNay { get; set; }
        public List<VisitZoneEntity> ListVisitKyTruoc { get; set; }
    }
}
