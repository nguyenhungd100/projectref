﻿using ChannelVN.CMS.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ChannelVN.Dashboard.Entity
{
    public class StatisticsEntity : EntityBase
    {
        //Đợi biên tập status= 1
        public int WaitForEdit { get; set; }
        public int WaitForEditT1 { get; set; }
        public int WaitForEditT2 { get; set; }
        public int WaitForEditT3 { get; set; }
        //Trả lại phóng viên status=4
        public int ReturnedToReporter { get; set; }
        public int ReturnedToReporterT1 { get; set; }
        public int ReturnedToReporterT2 { get; set; }
        public int ReturnedToReporterT3 { get; set; }
        //Bài Cộng tác viên gửi lên status = 2
        public int Cooperator { get; set; }
        public int CooperatorT1 { get; set; }
        public int CooperatorT2 { get; set; }
        public int CooperatorT3 { get; set; }
        //Coment chưa duyệt status =1
        public int Comment { get; set; }
        public int CommentT1 { get; set; }
        public int CommentT2 { get; set; }
        public int CommentT3 { get; set; }
        //Đợi xuất bản status=5
        public int WaitForPublish { get; set; }
        public int WaitForPublishT1 { get; set; }
        public int WaitForPublishT2 { get; set; }
        public int WaitForPublishT3 { get; set; }
        //Trả lại biên tập viên status=7
        public int ReturnedToEditor { get; set; }
        public int ReturnedToEditorT1 { get; set; }
        public int ReturnedToEditorT2 { get; set; }
        public int ReturnedToEditorT3 { get; set; }
        ////Comment user chưa duyệt status = 0
        //public int CommentUserPending { get; set; }
        //public int CommentUserPendingT1 { get; set; }
        //public int CommentUserPendingT2 { get; set; }
        //public int CommentUserPendingT3 { get; set; }
        ////Comment user đã duyệt status = 1
        //public int CommentUserApproved { get; set; }
        //public int CommentUserApprovedT1 { get; set; }
        //public int CommentUserApprovedT2 { get; set; }
        //public int CommentUserApprovedT3 { get; set; }
        //User
        public CountUserEntity User { get; set; }
        //Comment
        public CountCommentEntity CountComment { get; set; }
        //ProductionNews
        public StatisticsProductionNewsPublishEntity ProductionNews { get; set; }
        public StatisticsProductionVideoPublishEntity ProductionVideo { get; set; }
        public List<ListPercent> ListNewsPublishByCategory { get; set; }
        public List<ListPercent> ListNewsPublishByZone { get; set; }
        public List<ListPercent> ListVideoPublishByZone { get; set; }
    }
}
