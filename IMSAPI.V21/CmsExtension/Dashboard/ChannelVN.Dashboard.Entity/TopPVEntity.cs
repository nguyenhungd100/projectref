﻿using ChannelVN.CMS.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ChannelVN.Dashboard.Entity
{
    public class TopPVEntity : EntityBase
    {
        public string Username { get; set; }
        public string Avatar { get; set; }
        public string FullName { get; set; }
        public int TotalPageView { get; set; }
        public int PageViewMobile { get; set; }
        public int PageViewWeb { get; set; }
        public string Hieusuat { get; set; }
        public string PercentHieusuat { get; set; }
        public string PercentGui { get; set; }
        public int TotalGui { get; set; }
        public string PercentXuatban { get; set; }
        public int TotalXuatban { get; set; }
        public List<ListTime> ListGuiKyNay { get; set; }
        public List<ListTime> ListXuatbanKyNay { get; set; }
        public List<ListTime> ListGuiKyTruoc { get; set; }
        public List<ListTime> ListXuatbanKyTruoc { get; set; }
    }
    public class TempTopPVEntity : EntityBase
    {
        public int Type { get; set; }
        public string createdby { get; set; }
        public int PageViewWeb { get; set; }
        public int PageViewMobile { get; set; }
        public string FullName { get; set; }
        public string Avatar { get; set; }
        public string Time { get; set; }
        public int CountItem { get; set; }
    }
}
