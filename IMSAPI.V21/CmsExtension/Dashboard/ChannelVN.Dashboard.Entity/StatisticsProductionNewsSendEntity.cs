﻿using ChannelVN.CMS.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ChannelVN.Dashboard.Entity
{
    public class StatisticsProductionNewsSendEntity : EntityBase
    {
        public int CountSendNewsCurrentTime { get; set; }
        public int CountSendNewsOldTime { get; set; }
        public int TotalSendNewsKyTruoc { get; set; }

        public double GrowthNewsSend { get; set; }
        public List<ListTime> ListTime { get; set; }
        public List<ListTime> ListSendNewsKyNay { get; set; }
        public List<ListTime> ListSendNewsKyTruoc { get; set; }

    }
}
