﻿using ChannelVN.CMS.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ChannelVN.Dashboard.Entity
{
    public class ZoneEntity : EntityBase
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int ParentId { get; set; }
        public int GroupId { get; set; }
        public int Count { get; set; }
        public double Percent { get; set; }
    }
    public class ViewZoneEntity : EntityBase
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int TotalView { get; set; }
    }
    public class VisitZoneEntity : EntityBase
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int TotalVisit { get; set; }
    }
    public class ZoneParentEntity : EntityBase
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
