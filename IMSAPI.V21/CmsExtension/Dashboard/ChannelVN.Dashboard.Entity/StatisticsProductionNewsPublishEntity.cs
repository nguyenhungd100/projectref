﻿using ChannelVN.CMS.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ChannelVN.Dashboard.Entity
{
    public class StatisticsProductionNewsPublishEntity : EntityBase
    {
        public int CountPublishNewsOldTime { get; set; }
        public int CountPublishNewsCurrentTime { get; set; }
        public int TotalPublishNewsKyTruoc { get; set; }

        public double GrowthNewsPublish { get; set; }
        public List<ListTime> ListTime { get; set; }
        public List<ListTime> ListPublishNewsKyNay { get; set; }
        public List<ListTime> ListPublishNewsKyTruoc { get; set; }

    }
}
