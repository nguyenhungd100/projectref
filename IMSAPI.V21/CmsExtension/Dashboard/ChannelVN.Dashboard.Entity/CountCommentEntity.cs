﻿using ChannelVN.CMS.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ChannelVN.Dashboard.Entity
{
    public class CountCommentEntity : EntityBase
    {
        public int CountCommentKyNay { get; set; }
        public int CountCommentKyTruoc { get; set; }
        public int AverageComment { get; set; }
        public double GrowthComment { get; set; }
        public List<ZoneEntity> ListCommentByZone { get; set; }
        public List<ListTime> ListCommentKyNay { get; set; }
        public List<ListTime> ListCommentKyTruoc { get; set; }
    }
    public class CountCommentTemp
    {
        public int CountCommentCurrent { get; set; }
        public int CountCommentOld { get; set; }
        public int Total { get; set; }
    }
}
