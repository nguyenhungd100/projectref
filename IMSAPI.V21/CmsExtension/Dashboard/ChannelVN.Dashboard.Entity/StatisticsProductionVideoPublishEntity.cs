﻿using ChannelVN.CMS.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ChannelVN.Dashboard.Entity
{
    public class StatisticsProductionVideoPublishEntity : EntityBase
    {
       
        public int CountPublishVideoOldTime { get; set; }
        public int CountPublishVideoCurrentTime { get; set; }
        public int TotalPublishVideoKytruoc { get; set; }
        public double GrowthVideoPublish { get; set; }
        public List<ListTime> ListTime { get; set; }
        public List<ListTime> ListPublishVideoKyNay { get; set; }
        public List<ListTime> ListPublishVideoKyTruoc { get; set; }
    }
}
