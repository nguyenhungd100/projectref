﻿using ChannelVN.CMS.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ChannelVN.Dashboard.Entity
{
    public class UserEntity : EntityBase
    {
        public string FullName { get; set; }
        public string Avatar { get; set; }
        public string Username { get; set; }
    }
}
