﻿using ChannelVN.CMS.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ChannelVN.Dashboard.Entity
{
    public class PageViewEntity : EntityBase
    {
        public int CatId { get; set; }
        public string Time { get; set; }
        public int Type { get; set; }
        public int TotalView { get; set; }
    }
}
