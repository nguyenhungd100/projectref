﻿using ChannelVN.CMS.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ChannelVN.Dashboard.Entity
{
    public class CountUserEntity : EntityBase
    {
        public int Total { get; set; }
        public int AverageUser { get; set; }
        public double GrowthUser { get; set; }
        public int CountUserKyNay { get; set; }
        public int CountUserKyTruoc { get; set; }
        //User chưa duyệt status = 0
        public int UserPending { get; set; }
        //User đã duyệt status = 0
        public int UserApproved { get; set; }
        //User Block status = 2
        public int UserBlock { get; set; }
        public List<ListTime> ListTime { get; set; }
        public List<ListTime> ListUserKyNay { get; set; }
        public List<ListTime> ListUserKyTruoc { get; set; }
       
    }
}
