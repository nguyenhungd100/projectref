﻿using ChannelVN.CMS.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ChannelVN.Dashboard.Entity
{
    public class ExternalNewsEntity : EntityBase
    {
        public long Id { get; set; }
        public int ZoneId { get; set; }
        public DateTime LastModifiedDate { get; set; }
    }
}
