﻿using ChannelVN.CMS.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ChannelVN.Dashboard.Entity
{
    public class StatisticsBoxTongHopEntity : EntityBase
    {
        //Đợi biên tập status= 1
        public int WaitForEdit { get; set; }
        public int WaitForEditT1 { get; set; }
        public int WaitForEditT2 { get; set; }
        public int WaitForEditT3 { get; set; }
        //Trả lại phóng viên status=4
        public int ReturnedToReporter { get; set; }
        public int ReturnedToReporterT1 { get; set; }
        public int ReturnedToReporterT2 { get; set; }
        public int ReturnedToReporterT3 { get; set; }
        //Bài Cộng tác viên gửi lên status = 2
        public int Cooperator { get; set; }
        public int CooperatorT1 { get; set; }
        public int CooperatorT2 { get; set; }
        public int CooperatorT3 { get; set; }
        //Coment chưa duyệt status =1
        public int Comment { get; set; }
        public int CommentT1 { get; set; }
        public int CommentT2 { get; set; }
        public int CommentT3 { get; set; }
        //Đợi xuất bản status=5
        public int WaitForPublish { get; set; }
        public int WaitForPublishT1 { get; set; }
        public int WaitForPublishT2 { get; set; }
        public int WaitForPublishT3 { get; set; }
        //Trả lại biên tập viên status=7
        public int ReturnedToEditor { get; set; }
        public int ReturnedToEditorT1 { get; set; }
        public int ReturnedToEditorT2 { get; set; }
        public int ReturnedToEditorT3 { get; set; }

        ////Comment user đã duyệt status = 1
        //public int UserApproved { get; set; }
        //public int UserApprovedT1 { get; set; }
        //public int UserApprovedT2 { get; set; }
        //public int UserApprovedT3 { get; set; }
        //User chưa duyệt
        public int UserPending { get; set; }
        public int UserPendingT1 { get; set; }
        public int UserPendingT2 { get; set; }
        public int UserPendingT3 { get; set; }

    }
}
