﻿using ChannelVN.CMS.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ChannelVN.Dashboard.Entity
{
    public class ListPercent : EntityBase
    {
        //public int Id { get; set; }
        public double Percent { get; set; }
        public int CountItem { get; set; }
        public string Name { get; set; }
    }
}
