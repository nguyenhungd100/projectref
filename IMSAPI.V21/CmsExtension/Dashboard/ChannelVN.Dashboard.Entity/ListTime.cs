﻿using ChannelVN.CMS.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ChannelVN.Dashboard.Entity
{
    public class ListTime : EntityBase
    {
        public string Time { get; set; }
        public int Type { get; set; }
        public int CountItem { get; set; }
    }
}
