﻿using ChannelVN.CMS.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ChannelVN.Dashboard.Entity
{
    public class StatisticsProductionVideoSendEntity : EntityBase
    {
        public int CountSendVideoOldTime { get; set; }
        public int CountSendVideoCurrentTime { get; set; }
        public int TotalSendVideoKyTruoc { get; set; }
        public double GrowthVideoSend { get; set; }

        public List<ListTime> ListTime { get; set; }
        public List<ListTime> ListSendVideoKyNay { get; set; }
        public List<ListTime> ListSendVideoKyTruoc { get; set; }
    }
}
