﻿using ChannelVN.CMS.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ChannelVN.Dashboard.Entity
{
    public class CommentEntity : EntityBase
    {
        public long Id { get; set; }
        public int ZoneId { get; set; }
        public DateTime CreatedDate { get; set; }
        public int CountItem { get; set; }
        public int Total { get; set; }
        public int Status { get; set; }
        public int Type { get; set; }
        public string Time { get; set; }
        public int TotalCommentOldTime { get; set; }
        public int TotalCommentKyNay { get; set; }
        public int TotalCommentKyTruoc { get; set; }
    }
}
