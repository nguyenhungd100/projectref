﻿using ChannelVN.CMS.Common;
using ChannelVN.Dashboard.Dal.Common;
using ChannelVN.Dashboard.Entity;
using ChannelVN.Dashboard.MainDal.Databases;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;

namespace ChannelVN.Dashboard.Dal
{
    public class StatisticsExternalNewsDal
    {
        public static List<ExternalNewsEntity> ListCounter_ExternalNews(int LoaiTK, int ZoneId)
        {
            List<ExternalNewsEntity> returnValue;
            using (var db = new CmsMainExtDb())
            {
                returnValue = db.NewsExtMainDal.ListCounter_ExternalNews(LoaiTK, ZoneId);
            }
            return returnValue;
        }
        public static StatisticsBoxTongHopEntity Counter_ExternalNews(int LoaiTK, int ZoneId)
        {
            StatisticsBoxTongHopEntity returnValue;
            using (var db = new CmsMainExtDb())
            {
                returnValue = db.NewsExtMainDal.Counter_ExternalNews(LoaiTK, ZoneId);
            }
            return returnValue;
        }
    }
}
