﻿using ChannelVN.CMS.Common;
using ChannelVN.Dashboard.Dal.Common;
using ChannelVN.Dashboard.Entity;
using ChannelVN.Dashboard.MainDal.Databases;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;

namespace ChannelVN.Dashboard.Dal
{
    public class StatisticsCommentDal
    {
        public static List<CommentEntity> Counter_Comment()
        {
            List<CommentEntity> returnValue;
            using (var db = new CmsCommentDb())
            {
                returnValue = db.CommentMainDal.Counter_Comment();
            }
            return returnValue;
        }
        public static List<CommentEntity> Counter_Comment_All()
        {
            List<CommentEntity> returnValue;
            using (var db = new CmsCommentDb())
            {
                returnValue = db.CommentMainDal.Counter_Comment_All();
            }
            return returnValue;
        }
        public static List<CommentEntity> ListCommentByZone(string listZone, string listZoneAll, int LoaiTk, ref int totalOdl)
        {
            List<CommentEntity> returnValue;
            using (var db = new CmsCommentDb())
            {
                returnValue = db.CommentMainDal.ListCommentByZone(listZone, listZoneAll, LoaiTk, ref totalOdl);
            }
            return returnValue;
        }
        public static List<CommentEntity> Counter_Comment_AllForever()
        {
            List<CommentEntity> returnValue;
            using (var db = new CmsCommentDb())
            {
                returnValue = db.CommentMainDal.Counter_Comment_AllForever();
            }
            return returnValue;
        }
        public static List<CommentEntity> Counter_CommentByTime(CountCommentTemp ct, int LoaiTK)
        {
            List<CommentEntity> returnValue;
            using (var db = new CmsCommentDb())
            {
                returnValue = db.CommentMainDal.Counter_CommentByTime(ct, LoaiTK);
            }
            return returnValue;
        }
    }
}
