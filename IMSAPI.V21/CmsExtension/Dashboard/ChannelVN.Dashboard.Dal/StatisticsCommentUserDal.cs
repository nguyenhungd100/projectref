﻿using ChannelVN.CMS.Common;
using ChannelVN.Dashboard.Dal.Common;
using ChannelVN.Dashboard.Entity;
using ChannelVN.Dashboard.MainDal.Databases;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;


namespace ChannelVN.Dashboard.Dal
{
    public class StatisticsCommentUserDal
    {
        public static CountUserEntity Counter_CommentUserByTime(int LoaiTK)
        {
            CountUserEntity returnValue;
            using (var db = new CmsCommentDb())
            {
                returnValue = db.CommentUserMainDal.Counter_CommentUserByTime(LoaiTK);
            }
            return returnValue;
        }
        public static StatisticsBoxTongHopEntity Counter_CommentUserPendingByTime(int LoaiTK)
        {
            StatisticsBoxTongHopEntity returnValue;
            using (var db = new CmsCommentDb())
            {
                returnValue = db.CommentUserMainDal.Counter_CommentUserPendingByTime(LoaiTK);
            }
            return returnValue;
        }
    }
}
