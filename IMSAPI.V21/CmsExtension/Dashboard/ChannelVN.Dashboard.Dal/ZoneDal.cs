﻿using ChannelVN.CMS.Common;
using ChannelVN.Dashboard.Dal.Common;
using ChannelVN.Dashboard.Entity;
using ChannelVN.Dashboard.MainDal.Databases;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;

namespace ChannelVN.Dashboard.Dal
{
    public class ZoneDal
    {
        public static List<ZoneParentEntity> ListZoneParentByUserName(string Username)
        {
            List<ZoneParentEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.ZoneDal.ListZoneParentByUserName(Username);
            }
            return returnValue;
        }
        public static List<ZoneEntity> ListZoneByUserName(string Username)
        {
            List<ZoneEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.ZoneDal.ListZoneByUserName(Username);
            }
            return returnValue;
        }
        public static List<ZoneEntity> ListZoneById(int ZoneId)
        {
            List<ZoneEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.ZoneDal.ListZoneById(ZoneId);
            }
            return returnValue;
        }
        public static List<ZoneEntity> ListZone()
        {
            List<ZoneEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.ZoneDal.ListZone();
            }
            return returnValue;
        }
    }
}
