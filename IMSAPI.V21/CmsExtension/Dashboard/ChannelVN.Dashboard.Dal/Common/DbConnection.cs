﻿using ChannelVN.CMS.Common;
using ChannelVN.CMS.Common.ChannelConfig;
using ChannelVN.WcfExtensions;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;

namespace ChannelVN.Dashboard.Dal.Common
{
    public class DbCommon
    {
            private const string _CONNECTION_DECRYPT_KEY = "nfvsMof35XnUdQEWuxgAZta";

            private static string GetConnectionName(Connection connection)
            {
                switch (connection)
                {
                    case Connection.CmsMainDb:
                        return "CmsMainDb";
                    case Connection.IMS_EXTERNAL_CMS:
                        return "ExternalCmsDb";
                    case Connection.IMS_DANTRI_EXT:
                        return "ExternalCommentDb";
                    case Connection.Chanel_TrackingVisit:
                        return "TrackingVisitConnect";
                    default:
                        return "";
                }
            }

            public enum Connection
            {
                CmsMainDb = 1,
                IMS_EXTERNAL_CMS = 2,
                IMS_DANTRI_EXT = 3,
                Chanel_TrackingVisit = 4
            }

            public static DateTime MinDateTime = new DateTime(1980, 1, 1);
            public static string DatabaseSchema = "[dbo].";

            public static string GetConnectionString(Connection connection)
            {
                //return ConfigurationManager.ConnectionStrings[GetConnectionName(connection)].ConnectionString;
                return ServiceChannelConfiguration.GetConnectionString(WcfMessageHeader.Current.Namespace,  GetConnectionName(connection),
                                                                _CONNECTION_DECRYPT_KEY);
            }

            public static bool IsUseMainDal
            {
                get
                {
                    //string Namespace = "ChannelVN.Dashboard.MainDal";//WcfMessageHeader.Current.Namespace
                    return Utility.ConvertToBoolean(ServiceChannelConfiguration.GetAppSetting(WcfMessageHeader.Current.Namespace, "IsUseMainDal"));
                }
            }
        
    }
}
