﻿using ChannelVN.CMS.Common;
using ChannelVN.Dashboard.Dal.Common;
using ChannelVN.Dashboard.Entity;
using ChannelVN.Dashboard.MainDal.Databases;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;


namespace ChannelVN.Dashboard.Dal
{
    public class StatisticsNewsDal
    {
        public static StatisticsBoxTongHopEntity Counter_News(int LoaiTK, int ZoneId, string UserName)
        {
            StatisticsBoxTongHopEntity returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsMainDal.Counter_News(LoaiTK, ZoneId, UserName);
            }
            return returnValue;
        }
        public static StatisticsProductionNewsPublishEntity Counter_ProductionNews_Publish(int LoaiTK, int ZoneId, string UserName)
        {
            StatisticsProductionNewsPublishEntity returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsMainDal.Counter_ProductionNews_Publish(LoaiTK, ZoneId, UserName);
            }
            return returnValue;
        }
        public static StatisticsProductionNewsSendEntity Counter_ProductionNews_Send(int LoaiTK, int ZoneId, string UserName)
        {
            StatisticsProductionNewsSendEntity returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsMainDal.Counter_ProductionNews_Send(LoaiTK, ZoneId, UserName);
            }
            return returnValue;
        }
        public static List<ListPercent> Counter_NewsPercentByZone(int LoaiTK, string UserName)
        {
            List<ListPercent> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsMainDal.Counter_NewsPercentByZone(LoaiTK, UserName);
            }
            return returnValue;
        }
        public static List<ListPercent> Counter_NewsPercentByCategory(int LoaiTK, int ZoneId, string UserName)
        {
            List<ListPercent> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsMainDal.Counter_NewsPercentByCategory(LoaiTK, ZoneId, UserName);
            }
            return returnValue;
        }
        public static List<TempTopPVEntity> STATISTICS_TOPPV(int LoaiTK, int ZoneId, string UserName, int TopSize, string keyword)
        {
            List<TempTopPVEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsMainDal.STATISTICS_TOPPV(LoaiTK, ZoneId, UserName, TopSize, keyword);
            }
            return returnValue;
        }
    }
}
