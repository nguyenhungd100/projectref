﻿using ChannelVN.CMS.Common;
using ChannelVN.Dashboard.Dal.Common;
using ChannelVN.Dashboard.Entity;
using ChannelVN.Dashboard.MainDal.Databases;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;

namespace ChannelVN.Dashboard.Dal
{
    public class TrackingVisitDal
    {
        public static List<PageViewEntity> ListPageViewByTime(int LoaiTk, string Db)
        {
            List<PageViewEntity> returnValue;
            using (var db = new CmsTrackingVisitDb())
            {
                returnValue = db.TrackingVisitDal.ListPageViewByTime(LoaiTk, Db);
            }
            return returnValue;
        }
    }
}
