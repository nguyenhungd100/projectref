﻿using ChannelVN.CMS.Common;
using ChannelVN.Dashboard.Dal.Common;
using ChannelVN.Dashboard.Entity;
using ChannelVN.Dashboard.MainDal.Databases;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;

namespace ChannelVN.Dashboard.Dal
{
    public class StatisticsExternalUsersDal
    {
        public static List<UserEntity> ListExternalUser(string listUsers)
        {
            List<UserEntity> returnValue;
            using (var db = new CmsMainExtDb())
            {
                returnValue = db.UsersExtMainDal.ListExternalUser(listUsers);
            }
            return returnValue;
        }
        public static List<UserEntity> ListExternalUser_Search(string keyword)
        {
            List<UserEntity> returnValue;
            using (var db = new CmsMainExtDb())
            {
                returnValue = db.UsersExtMainDal.ListExternalUser_Search(keyword);
            }
            return returnValue;
        }
    }
}
