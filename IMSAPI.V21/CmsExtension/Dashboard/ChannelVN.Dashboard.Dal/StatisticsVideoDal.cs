﻿using ChannelVN.CMS.Common;
using ChannelVN.Dashboard.Dal.Common;
using ChannelVN.Dashboard.Entity;
using ChannelVN.Dashboard.MainDal.Databases;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;


namespace ChannelVN.Dashboard.Dal
{
    public class StatisticsVideoDal
    {
        public static StatisticsProductionVideoPublishEntity Counter_ProductionVideo_Publish(int LoaiTK, int ZoneId, string UserName)
        {
            StatisticsProductionVideoPublishEntity returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.VideoMainDal.Counter_ProductionVideo_Publish(LoaiTK, ZoneId, UserName);
            }
            return returnValue;
        }
        public static StatisticsProductionVideoSendEntity Counter_ProductionVideo_Send(int LoaiTK, int ZoneId, string UserName)
        {
            StatisticsProductionVideoSendEntity returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.VideoMainDal.Counter_ProductionVideo_Send(LoaiTK, ZoneId, UserName);
            }
            return returnValue;
        }
        public static List<ListPercent> Counter_VideoPercentByZone(int LoaiTK, string UserName)
        {
            List<ListPercent> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.VideoMainDal.Counter_VideoPercentByZone(LoaiTK, UserName);
            }
            return returnValue;
        }
    }
}
