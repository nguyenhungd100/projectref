﻿using ChannelVN.CMS.Common;
using ChannelVN.Dashboard.Dal.Common;
using ChannelVN.Dashboard.Entity;
using ChannelVN.Dashboard.MainDal.Databases;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;


namespace ChannelVN.Dashboard.Dal
{
    public class UsersDal
    {
        public static List<UserEntity> User_Search(string KeyWord)
        {
            List<UserEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.UsersMainDal.User_Search(KeyWord);
            }
            return returnValue;
        }
    }
}
