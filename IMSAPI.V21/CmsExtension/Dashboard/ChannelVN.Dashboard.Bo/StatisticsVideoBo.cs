﻿using ChannelVN.Dashboard.Dal;
using ChannelVN.Dashboard.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Caching;

namespace ChannelVN.Dashboard.Bo
{
    public class StatisticsVideoBo
    {
        public static StatisticsProductionVideoPublishEntity Counter_ProductionVideo_Publish(int LoaiTK, int ZoneId, string UserName)
        {
            string[] tables = { Constants.TABLE.NEWSTB, Constants.TABLE.NEWSINZONETB, Constants.TABLE.USERPERMISSIONTB };
            string cacheName = string.Format(Constants.CACHE_NAME_FORMAT, Constants.CACHE_STATISTICS_NAME, "Counter_ProductionVideo_Publish", Constants.DATABASE.MAINDB, LoaiTK, ZoneId, UserName);
            var entity = HttpContext.Current.Cache[cacheName] as StatisticsProductionVideoPublishEntity;
            if (entity == null)
            {
                entity = StatisticsVideoDal.Counter_ProductionVideo_Publish(LoaiTK, ZoneId, UserName);
                StatisticsBo.CreateCacheStatistics(entity, Constants.DATABASE.MAINDB, cacheName, tables);
            }
            return entity;
        }
        public static StatisticsProductionVideoSendEntity Counter_ProductionVideo_Send(int LoaiTK, int ZoneId, string UserName)
        {
            string[] tables = { Constants.TABLE.NEWSTB, Constants.TABLE.NEWSINZONETB, Constants.TABLE.USERPERMISSIONTB };
            string cacheName = string.Format(Constants.CACHE_NAME_FORMAT, Constants.CACHE_STATISTICS_NAME, "Counter_ProductionVideo_Send", Constants.DATABASE.MAINDB, LoaiTK, ZoneId, UserName);
            var entity = HttpContext.Current.Cache[cacheName] as StatisticsProductionVideoSendEntity;
            if (entity == null)
            {
                entity = StatisticsVideoDal.Counter_ProductionVideo_Send(LoaiTK, ZoneId, UserName);
                StatisticsBo.CreateCacheStatistics(entity, Constants.DATABASE.MAINDB, cacheName, tables);
            }
            return entity;
        }
        public static List<ListPercent> Counter_VideoPercentByZone(int LoaiTK, string UserName)
        {
            string[] tables = { Constants.TABLE.NEWSTB, Constants.TABLE.NEWSINZONETB, Constants.TABLE.USERPERMISSIONTB };
            string cacheName = string.Format(Constants.CACHE_NAME_FORMAT, Constants.CACHE_STATISTICS_NAME, "Counter_VideoPercentByZone", Constants.DATABASE.MAINDB, LoaiTK, "NoneZoneId", UserName);
            var entity = HttpContext.Current.Cache[cacheName] as List<ListPercent>;
            if (entity == null)
            {
                entity = StatisticsVideoDal.Counter_VideoPercentByZone(LoaiTK, UserName);
                StatisticsBo.CreateCacheStatistics(entity, Constants.DATABASE.MAINDB, cacheName, tables);
            }
            return entity;
        }
    }
}
