﻿using ChannelVN.Dashboard.Dal;
using ChannelVN.Dashboard.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Caching;

namespace ChannelVN.Dashboard.Bo
{
    public class StatisticsCommentUserBo
    {
        public static CountUserEntity Counter_CommentUserByTime(int LoaiTK,string UserName)
        {
            string[] tables = { Constants.TABLE.COMMENTUSERTB};
            string cacheName = string.Format(Constants.CACHE_NAME_FORMAT, Constants.CACHE_STATISTICS_NAME, Constants.DATABASE.COMMENTDB, "Counter_CommentUserByTime", LoaiTK, "NoZoneId", UserName);
            var entity = HttpContext.Current.Cache[cacheName] as CountUserEntity;
            if (entity == null)
            {
                entity = StatisticsCommentUserDal.Counter_CommentUserByTime(LoaiTK);
                StatisticsBo.CreateCacheStatistics(entity, Constants.DATABASE.COMMENTDB, cacheName, tables);
                 var abc = HttpContext.Current.Cache[cacheName] as CountUserEntity;
            }
            return entity;
        }
        public static StatisticsBoxTongHopEntity Counter_CommentUserPendingByTime(int LoaiTK, string UserName)
        {
            string[] tables = { Constants.TABLE.COMMENTUSERTB };
            string cacheName = string.Format(Constants.CACHE_NAME_FORMAT, Constants.CACHE_STATISTICS_NAME, Constants.DATABASE.COMMENTDB, "Counter_CommentUserPendingByTime", LoaiTK, "NoZoneId", UserName);
            var entity = HttpContext.Current.Cache[cacheName] as StatisticsBoxTongHopEntity;
            if (entity == null)
            {
                entity = StatisticsCommentUserDal.Counter_CommentUserPendingByTime(LoaiTK);
                StatisticsBo.CreateCacheStatistics(entity, Constants.DATABASE.COMMENTDB, cacheName, tables);
                var abc = HttpContext.Current.Cache[cacheName] as CountUserEntity;
            }
            return entity;
        }
    }
}
