﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace ChannelVN.Dashboard.Bo
{
    public class Constants
    {
        public static class DATABASE
        {
            public const string MAINDB = "IMS_DANTRI_V1";
            public const string EXTERNALDB = "IMS_EXTERNAL_CMS";
            public const string COMMENTDB = "IMS_DANTRI_EXT";
            public const string TRACKINGVISITDB = "Chanel_TrackingVisit";
        }
        public static class TABLE
        {
            public const string NEWSTB = "News";
            public const string NEWSINZONETB = "NewsInZone";
            public const string EXTERNALNEWSTB = "ExternalNews";
            public const string ZONETB = "Zone";
            public const string USERPERMISSIONTB = "UserPermission";
            public const string EXTERNALNEWSINZONETB = "ExternalNewsInZone";
            public const string EXZoneTB = "Zone";
            public const string COMMENTTB = "Comment";
            public const string COMMENTUSERTB = "CommentUser";

            public const string _CategoryDaily = "_CategoryDaily";
        }
        public const string CACHE_NAME_FORMAT = "{0}_{1}_{2}_{3}_{4}_{5}";//{CacheName}_{DBName}_{method}_{LoaiTk}_{ZoneId}_{Username}
        public const string CACHE_ZONE_FORMAT = "{0}_{1}_{2}_{3}";//{DBName}_{TableName}_{method}_{Username}
        public const string CACHE_VISIT_FORMAT = "{0}_{1}_{2}_{3}_{4}";//{CacheName}_{DBName}_{method}_{LoaiTk}_{DBTB}
        public const string CACHE_STATISTICS_NAME = "Statistics";
        public const int TIME1 = -5;
        public const int TIME2 = -10; 
        public enum loaiThongKe
        {
           Ngay = 1,
           Tuan = 2,
           Thang = 3
        }
        public enum loaiThongKe2
        {
            Tuan = 2,
            Thang = 3
        }
        public enum Db
        {
            Kenh14 = 1,
            DanTri = 2,
            Soha = 3
        }
    }
}
