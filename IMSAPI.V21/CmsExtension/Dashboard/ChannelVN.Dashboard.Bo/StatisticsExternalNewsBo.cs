﻿using ChannelVN.Dashboard.Dal;
using ChannelVN.Dashboard.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Caching;

namespace ChannelVN.Dashboard.Bo
{
    public class StatisticsExternalNewsBo
    {
        public static List<ExternalNewsEntity> ListCounter_ExternalNews(int LoaiTK, int ZoneId, string UserName)
        {
            string[] tables = { Constants.TABLE.EXTERNALNEWSTB, Constants.TABLE.EXTERNALNEWSINZONETB };
            string cacheName = string.Format(Constants.CACHE_NAME_FORMAT, Constants.CACHE_STATISTICS_NAME, Constants.DATABASE.EXTERNALDB, "ListCounter_ExternalNews", LoaiTK, ZoneId, UserName);
            var entity = HttpContext.Current.Cache[cacheName] as List<ExternalNewsEntity>;
            if (entity==null)
            {
                entity = StatisticsExternalNewsDal.ListCounter_ExternalNews(LoaiTK,ZoneId);
                StatisticsBo.CreateCacheStatistics(entity,Constants.DATABASE.EXTERNALDB,cacheName,tables);
            }
            
            return entity;
        }
        public static StatisticsBoxTongHopEntity Counter_ExternalNews(int LoaiTK, int ZoneId, string UserName)
        {
            string[] tables = { Constants.TABLE.EXTERNALNEWSTB, Constants.TABLE.EXTERNALNEWSINZONETB };
            string cacheName = string.Format(Constants.CACHE_NAME_FORMAT, Constants.CACHE_STATISTICS_NAME, Constants.DATABASE.EXTERNALDB,"Counter_ExternalNews", LoaiTK, ZoneId, UserName);
            var entity = HttpContext.Current.Cache[cacheName] as StatisticsBoxTongHopEntity;
            if (entity == null)
            {
                entity = StatisticsExternalNewsDal.Counter_ExternalNews(LoaiTK, ZoneId);
                StatisticsBo.CreateCacheStatistics(entity, Constants.DATABASE.EXTERNALDB, cacheName, tables);
            }
            return entity;
        }
    }
}
