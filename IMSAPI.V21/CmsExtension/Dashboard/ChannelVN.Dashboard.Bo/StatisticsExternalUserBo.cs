﻿using ChannelVN.Dashboard.Dal;
using ChannelVN.Dashboard.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Caching;

namespace ChannelVN.Dashboard.Bo
{
    public class StatisticsExternalUserBo
    {
        public static List<UserEntity> ListExternalUser(string listUsers)
        {
            string[] tables = { Constants.TABLE.EXTERNALNEWSTB, Constants.TABLE.EXTERNALNEWSINZONETB };
            string cacheName = string.Format(Constants.CACHE_NAME_FORMAT, Constants.CACHE_STATISTICS_NAME, Constants.DATABASE.EXTERNALDB, "ListExternalUser", "l", "z", "u");
            var entity = HttpContext.Current.Cache[cacheName] as List<UserEntity>;
            if (entity==null)
            {
                entity = StatisticsExternalUsersDal.ListExternalUser(listUsers);
                StatisticsBo.CreateCacheStatistics(entity,Constants.DATABASE.EXTERNALDB,cacheName,tables);
            }
            
            return entity;
        }
        public static List<UserEntity> ListExternalUser_Search(string keyword)
        {
            return StatisticsExternalUsersDal.ListExternalUser_Search(keyword); ;
        }
    }
}
