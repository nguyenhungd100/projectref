﻿using ChannelVN.Dashboard.Dal;
using ChannelVN.Dashboard.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Caching;

namespace ChannelVN.Dashboard.Bo
{
    public class ZoneBo
    {
        public static List<ZoneEntity> ListZoneByUserName(string UserName)
        {
            string cacheName = string.Format(Constants.CACHE_NAME_FORMAT,Constants.CACHE_STATISTICS_NAME, Constants.DATABASE.MAINDB, "ListZoneByUserName","NoneLoaiTk","NoneZone", UserName);
            string[] tables = { Constants.TABLE.ZONETB, Constants.TABLE.USERPERMISSIONTB };
            var entity = HttpContext.Current.Cache[cacheName] as List<ZoneEntity>;
            if (entity==null)
            {
                entity = ZoneDal.ListZoneByUserName(UserName);
                StatisticsBo.CreateCacheStatistics(entity, Constants.DATABASE.MAINDB,cacheName, tables);
            }
            return entity;
        }
        public static List<ZoneEntity> ListZoneById(int zoneId)
        {
            string cacheName = string.Format(Constants.CACHE_NAME_FORMAT,Constants.CACHE_STATISTICS_NAME, Constants.DATABASE.MAINDB, "ListZoneById", "NoneLoaiTk", zoneId, "NoUserName");
            string[] tables = { Constants.TABLE.ZONETB, Constants.TABLE.USERPERMISSIONTB };
            var entity = HttpContext.Current.Cache[cacheName] as List<ZoneEntity>;
            if (entity == null)
            {
                entity = ZoneDal.ListZoneById(zoneId);
                StatisticsBo.CreateCacheStatistics(entity, Constants.DATABASE.MAINDB, cacheName, tables);
            }
            return entity;
        }
        public static List<ZoneEntity> ListZone(string UserName)
        {
            string cacheName = string.Format(Constants.CACHE_NAME_FORMAT, Constants.DATABASE.MAINDB, "ListZone", "NoneLoaiTk", "NoneZone", UserName);
            string[] tables = { Constants.TABLE.ZONETB, Constants.TABLE.USERPERMISSIONTB };
            var entity = HttpContext.Current.Cache[cacheName] as List<ZoneEntity>;
            if (entity == null)
            {
                entity = ZoneDal.ListZone();
                StatisticsBo.CreateCacheStatistics(entity, Constants.DATABASE.MAINDB, cacheName, tables);
            }
            return entity;
        }
        public static List<ZoneParentEntity> ListZoneParent(string UserName)
        {
            string cacheName = string.Format(Constants.CACHE_NAME_FORMAT, Constants.CACHE_STATISTICS_NAME, Constants.DATABASE.MAINDB, "ListZoneParent", "NoneLoaiTk", "NoneZone", UserName);
            string[] tables = { Constants.TABLE.ZONETB, Constants.TABLE.USERPERMISSIONTB };
            var returnData = HttpContext.Current.Cache[cacheName] as List<ZoneParentEntity>;
            if (returnData == null)
            {
                returnData = ZoneDal.ListZoneParentByUserName(UserName);
                StatisticsBo.CreateCacheStatistics(returnData, Constants.DATABASE.MAINDB, cacheName, tables);
            }
            return returnData;
        }
    }
}
