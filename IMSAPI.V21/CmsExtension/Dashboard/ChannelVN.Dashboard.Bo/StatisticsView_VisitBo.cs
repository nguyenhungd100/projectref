﻿using ChannelVN.Dashboard.Dal;
using ChannelVN.Dashboard.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Caching;

namespace ChannelVN.Dashboard.Bo
{
    public class StatisticsView_VisitBo
    {
        public static StatisticsView_VisitEntity Statistics_ViewVisitByTime(int LoaiTk, string UserName, int ZoneId, int Database)
        {
            string Db = "";
            double GAVisit = 0;
            switch (Database)
            {
                case (int)Constants.Db.DanTri:
                    Db = "DANTRI";
                    GAVisit = 3.4;
                    break;
                case (int)Constants.Db.Kenh14:
                    Db = "K14";
                    GAVisit = 4.7;
                    break;
                case (int)Constants.Db.Soha:
                    Db = "Soha";
                    GAVisit = 3.4;
                    break;
                default:
                    break;
            }
            var data = PageViewBo.ListPageViewByTime(LoaiTk, Db);
            var listZone = new List<ZoneEntity>();
            if (ZoneId > 0)
            {
                listZone = ZoneBo.ListZoneById(ZoneId);
            }
            else
                listZone = ZoneBo.ListZoneByUserName(UserName);
            data = (from d in data
                    where listZone.Any(k => k.Id == d.CatId)
                    select d).ToList();
            var ListViewKyNay = (from c in data
                                 where c.Type == 1
                                 group c by c.Time into g
                                 select new PageViewEntity
                                 {
                                     Time = g.Key,
                                     TotalView = g.Sum(k => k.TotalView)
                                 }).ToList();
            var ListViewKyTruoc = (from c in data
                                   where c.Type == 2
                                   group c by c.Time into g
                                   select new PageViewEntity
                                   {
                                       Time = g.Key,
                                       TotalView = g.Sum(k => k.TotalView)
                                   }).ToList();

            var ListVisitKyNay = (from c in ListViewKyNay
                                  select new PageViewEntity
                                  {
                                      Time = c.Time,
                                      TotalView = (int)(Math.Round(c.TotalView / GAVisit))
                                  }).ToList();
            var ListVisitKyTruoc = (from c in ListViewKyTruoc
                                    select new PageViewEntity
                                    {
                                        Time = c.Time,
                                        TotalView = (int)(Math.Round(c.TotalView / GAVisit))
                                    }).ToList();
            var listNewViewKyNay = new List<PageViewEntity>();
            var listNewViewKyTruoc = new List<PageViewEntity>();
            var listNewVisitKyNay = new List<PageViewEntity>();
            var listNewVisitKyTruoc = new List<PageViewEntity>();
            #region Add List
            if (LoaiTk == 1)
            {

            }
            else if (LoaiTk == 2)
            {
                for (int i = 1; i < 8; i++)
                {
                    bool check = true;
                    foreach (var item in ListViewKyNay) { if (item.Time == i.ToString()) { check = false; break; } }
                    if (check)
                    {
                        var additem = new PageViewEntity();
                        additem.Time= i.ToString();
                        additem.TotalView = 0;
                        listNewViewKyNay.Add(additem);
                    }
                    check = true;
                    foreach (var item in ListViewKyTruoc) { if (item.Time == i.ToString().ToString()) { check = false; break; } }
                    if (check)
                    {
                        var additem = new PageViewEntity();
                        additem.Time= i.ToString();
                        additem.TotalView = 0;
                        listNewViewKyTruoc.Add(additem);
                    }
                    check = true;
                    foreach (var item in ListVisitKyNay) { if (item.Time == i.ToString()) { check = false; break; } }
                    if (check)
                    {
                        var additem = new PageViewEntity();
                        additem.Time= i.ToString();
                        additem.TotalView = 0;
                        listNewVisitKyNay.Add(additem);
                    }
                    check = true;
                    foreach (var item in ListVisitKyTruoc) { if (item.Time == i.ToString()) { check = false; break; } }
                    if (check)
                    {
                        var additem = new PageViewEntity();
                        additem.Time= i.ToString();
                        additem.TotalView = 0;
                        listNewVisitKyTruoc.Add(additem);
                    }
                }
            }
            else
            {
                int cTruoc = DateTime.Parse(DateTime.Now.ToString("yyyy/MM/01")).AddDays(-1).Day;
                int cNay = DateTime.Parse(DateTime.Now.ToString("yyyy/MM/01")).AddMonths(1).AddDays(-1).Day;
                for (int i = 1; i < 32; i++)
                {
                    bool check = true;
                    if (i <= cNay)
                    {
                        // kỳ này
                        foreach (var item in ListViewKyNay) { if (item.Time == i.ToString()) { check = false; break; } }
                        if (check)
                        {
                            var additem = new PageViewEntity();
                            additem.Time= i.ToString();
                            additem.TotalView = 0;
                            listNewViewKyNay.Add(additem);
                        }
                        check = true;
                        foreach (var item in ListVisitKyNay) { if (item.Time == i.ToString()) { check = false; break; } }
                        if (check)
                        {
                            var additem = new PageViewEntity();
                            additem.Time= i.ToString();
                            additem.TotalView = 0;
                            listNewVisitKyNay.Add(additem);
                        }
                    }
                    check = true;
                    if (i <= cTruoc)
                    {
                        //Kỳ trước
                        check = true;
                        foreach (var item in ListViewKyTruoc) { if (item.Time == i.ToString()) { check = false; break; } }
                        if (check)
                        {
                            var additem = new PageViewEntity();
                            additem.Time= i.ToString();
                            additem.TotalView = 0;
                            listNewViewKyTruoc.Add(additem);
                        }
                        check = true;
                        foreach (var item in ListVisitKyTruoc) { if (item.Time == i.ToString()) { check = false; break; } }
                        if (check)
                        {
                            var additem = new PageViewEntity();
                            additem.Time= i.ToString();
                            additem.TotalView = 0;
                            listNewVisitKyTruoc.Add(additem);
                        }
                    }
                }
            }
            #endregion
            ListViewKyNay.AddRange(listNewViewKyNay);
            ListViewKyTruoc.AddRange(listNewViewKyTruoc);
            ListVisitKyNay.AddRange(listNewVisitKyNay);
            ListVisitKyTruoc.AddRange(listNewVisitKyTruoc);
            StatisticsView_VisitEntity objReturn = new StatisticsView_VisitEntity();
            if (LoaiTk == 2)
            {
                #region VIEW
                ListViewKyNay = (from f in ListViewKyNay
                                 orderby f.Time
                                 select new PageViewEntity
                                 {
                                     Time = f.Time == "1" ? "CN" : "T" + f.Time,
                                     TotalView = f.TotalView
                                 }).ToList();
                var additem = new PageViewEntity();
                additem.Time = ListViewKyNay[0].Time;
                additem.TotalView = ListViewKyNay[0].TotalView;
                ListViewKyNay.RemoveAt(0);
                ListViewKyNay.Add(additem);

                ListViewKyTruoc = (from f in ListViewKyTruoc
                                   orderby f.Time
                                   select new PageViewEntity
                                   {
                                       Time = f.Time == "1" ? "CN" : "T" + f.Time,
                                       TotalView = f.TotalView
                                   }).ToList();
                additem = new PageViewEntity();
                additem.Time = ListViewKyTruoc[0].Time;
                additem.TotalView = ListViewKyTruoc[0].TotalView;
                ListViewKyTruoc.RemoveAt(0);
                ListViewKyTruoc.Add(additem); 
                #endregion

                ListVisitKyNay = (from f in ListVisitKyNay
                                 orderby f.Time
                                 select new PageViewEntity
                                 {
                                     Time = f.Time == "1" ? "CN" : "T" + f.Time,
                                     TotalView = f.TotalView
                                 }).ToList();
                additem = new PageViewEntity();
                additem.Time = ListVisitKyNay[0].Time;
                additem.TotalView = ListVisitKyNay[0].TotalView;
                ListVisitKyNay.RemoveAt(0);
                ListVisitKyNay.Add(additem);

                ListVisitKyTruoc = (from f in ListVisitKyTruoc
                                   orderby f.Time
                                   select new PageViewEntity
                                   {
                                       Time = f.Time == "1" ? "CN" : "T" + f.Time,
                                       TotalView = f.TotalView
                                   }).ToList();
                additem = new PageViewEntity();
                additem.Time = ListVisitKyTruoc[0].Time;
                additem.TotalView = ListVisitKyTruoc[0].TotalView;
                ListVisitKyTruoc.RemoveAt(0);
                ListVisitKyTruoc.Add(additem);

                objReturn.ListViewKyNay = ListViewKyNay;
                objReturn.ListViewKyTruoc = ListViewKyTruoc;
                objReturn.ListVisitKyNay = ListVisitKyNay;
                objReturn.ListVisitKyTruoc = ListVisitKyTruoc;
            }
            else
            {
                objReturn.ListViewKyNay = ListViewKyNay.OrderBy(k => int.Parse(k.Time)).ToList();
                objReturn.ListViewKyTruoc = ListViewKyTruoc.OrderBy(k => int.Parse(k.Time)).ToList();
                objReturn.ListVisitKyNay = ListVisitKyNay.OrderBy(k => int.Parse(k.Time)).ToList();
                objReturn.ListVisitKyTruoc = ListVisitKyTruoc.OrderBy(k => int.Parse(k.Time)).ToList();
            }
           
            objReturn.TotalViewKyNay = ListViewKyNay.Sum(k => k.TotalView);
            objReturn.TotalViewKyTruoc = ListViewKyTruoc.Sum(k => k.TotalView);
            objReturn.TotalVisitKyNay = ListVisitKyNay.Sum(k => k.TotalView);
            objReturn.TotalVisitKyTruoc = ListVisitKyTruoc.Sum(k => k.TotalView);
            objReturn.ViewGrowth = objReturn.TotalViewKyTruoc > 0 ? Math.Round((double)objReturn.TotalViewKyNay / objReturn.TotalViewKyTruoc * 100 - 100, 1) : objReturn.TotalViewKyNay;
            objReturn.VisitGrowth = objReturn.TotalVisitKyTruoc > 0 ? Math.Round((double)objReturn.TotalVisitKyNay / objReturn.TotalVisitKyTruoc * 100 - 100, 1) : objReturn.TotalVisitKyNay;
            return objReturn;
        }

        public static StatisticsView_VisitEntityByZone Statistics_ViewVisitByZone(int LoaiTk, string UserName, int Database)
        {
            string Db = "";
            double GAVisit = 0;
            switch (Database)
            {
                case (int)Constants.Db.DanTri:
                    Db = "DANTRI";
                    GAVisit = 3.4;
                    break;
                case (int)Constants.Db.Kenh14:
                    Db = "K14";
                    GAVisit = 4.7;
                    break;
                case (int)Constants.Db.Soha:
                    Db = "Soha";
                    GAVisit = 3.4;
                    break;
                default:
                    break;
            }
            var data = PageViewBo.ListPageViewByTime(LoaiTk, Db);
            var listZone = new List<ZoneEntity>();
            //if (ZoneId > 0)
            //{
            //    listZone = ZoneBo.ListZoneById(ZoneId, UserName);
            //}
            //else
            listZone = ZoneBo.ListZoneByUserName(UserName);
            var dbKyNay = (from d in data
                    where listZone.Any(k => k.Id == d.CatId)
                    & d.Type ==1 
                    group d by new { d.CatId,d.Type } into g
                    select new PageViewEntity
                    {
                        CatId = g.Key.CatId,
                        Type =g.Key.Type,
                        TotalView = g.Sum(k => k.TotalView)
                    }).ToList();

            var dbKyTruoc= (from d in data
                           where listZone.Any(k => k.Id == d.CatId)
                           & d.Type == 2
                           group d by new { d.CatId, d.Type } into g
                           select new PageViewEntity
                           {
                               CatId = g.Key.CatId,
                               Type = g.Key.Type,
                               TotalView = g.Sum(k => k.TotalView)
                           }).ToList();

            var tempKN = (from z in listZone
                           join e in dbKyNay on z.Id equals e.CatId
                           into dt
                           from subpet in dt.DefaultIfEmpty()
                            select new ZoneEntity
                           {
                               Count = subpet == null ? 0 : subpet.TotalView,
                               GroupId = z.ParentId == 0 ? z.Id : z.ParentId,
                               Id = z.Id,
                               Name = z.Name,
                               ParentId = z.ParentId
                           }
                        ).OrderBy(k => k.ParentId).ToList();

            var tempKT = (from z in listZone
                          join e in dbKyTruoc on z.Id equals e.CatId
                          into dt
                          from subpet in dt.DefaultIfEmpty()
                          select new ZoneEntity
                          {
                              Count = subpet == null ? 0 : subpet.TotalView,
                              GroupId = z.ParentId == 0 ? z.Id : z.ParentId,
                              Id = z.Id,
                              Name = z.Name,
                              ParentId = z.ParentId
                          }
                       ).OrderBy(k => k.ParentId).ToList();

            var ListViewKyNay = tempKN.GroupBy(k => k.GroupId).Select(n => new ViewZoneEntity
                                                                                        {
                                                                                            Id =n.Key,
                                                                                            Name = n.First().Name,
                                                                                            TotalView = n.Sum(c => c.Count)
                                                                                        }).ToList();


            var ListViewKyTruoc = tempKT.GroupBy(k => k.GroupId).Select(n => new ViewZoneEntity
                                                                                        {
                                                                                            Id = n.Key,
                                                                                            Name = n.First().Name,
                                                                                            TotalView = n.Sum(c => c.Count)
                                                                                        }).ToList();

            var ListVisitKyNay = (from c in ListViewKyNay
                                  select new VisitZoneEntity
                                  {
                                      Id = c.Id,
                                      Name=c.Name,
                                      TotalVisit = (int)Math.Round(c.TotalView / GAVisit)
                                  }).ToList();
            var ListVisitKyTruoc = (from c in ListViewKyTruoc
                                    select new VisitZoneEntity
                                    {
                                        Id = c.Id,
                                        Name = c.Name,
                                        TotalVisit = (int)Math.Round(c.TotalView / GAVisit)
                                    }).ToList();

          
            StatisticsView_VisitEntityByZone objReturn = new StatisticsView_VisitEntityByZone();
            objReturn.ListViewKyNay = ListViewKyNay;
            objReturn.ListViewKyTruoc = ListViewKyTruoc;
            objReturn.ListVisitKyNay = ListVisitKyNay;
            objReturn.ListVisitKyTruoc = ListVisitKyTruoc;
            return objReturn;
        }
    }
}
