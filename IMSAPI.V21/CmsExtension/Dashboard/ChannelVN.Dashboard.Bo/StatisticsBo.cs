﻿using ChannelVN.Dashboard.Dal;
using ChannelVN.Dashboard.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Caching;

namespace ChannelVN.Dashboard.Bo
{
    public class StatisticsBo
    {
        public static CountCommentEntity Statistics_Comment(int LoaiTK, int ZoneId, string UserName)
        {
            var statistics = new CountCommentEntity();
            var listZoneUser = ZoneBo.ListZoneByUserName(UserName);
            var listZone = new List<ZoneEntity>();
            string strListZone = "";
            if (ZoneId > 0)
            {
                listZone = ZoneBo.ListZoneById(ZoneId);
                foreach (var item in listZone)
                {
                    if (strListZone.Equals("")) strListZone = item.Id.ToString();
                    else strListZone += "," + item.Id;
                }
            }
            else
            {
                listZone = listZoneUser;
            }
            #region SetTime
            DateTime df = DateTime.Parse(DateTime.Now.ToString("yyyy/MM/dd 00:00:00"));
            DateTime dt = DateTime.Now;
            DateTime OldTime = DateTime.Now;
            DateTime dateGrowth = df.AddDays(-1);
            DateTime dtavg = DateTime.Now;

            if (LoaiTK == 2)
            {
                while (df.DayOfWeek != DayOfWeek.Monday) df = df.AddDays(-1);
                OldTime = DateTime.Parse(df.ToString("yyyy/MM/01 00:00"));
                dateGrowth = df.AddDays(-7);
            }
            else if (LoaiTK == 3)
            {
                df = DateTime.Parse(df.ToString("yyyy/MM/01 00:00"));
                OldTime = DateTime.Parse(df.ToString("yyyy/01/01 00:00"));
                dateGrowth = df.AddMonths(-1);
                dtavg = df.AddMonths(1);
            }
            else
            {
                while (OldTime.DayOfWeek != DayOfWeek.Monday) OldTime = OldTime.AddDays(-1);
            }
            #endregion

            #region Comment All
            string strListZoneAll = "";
            foreach (var item in listZoneUser)
            {
                if (strListZoneAll.Equals("")) strListZoneAll = item.Id.ToString();
                else strListZoneAll += "," + item.Id;
            }
            int TotalOld = 0;
            var CommentAll = StatisticsCommentBo.ListCommentByZone(LoaiTK, strListZone, strListZoneAll, UserName, ref TotalOld);
            //var CmtAll = StatisticsCommentBo.Counter_Comment_All(LoaiTK, ZoneId, UserName);
            //var CommentForever = StatisticsCommentBo.Counter_Comment_AllForever(LoaiTK, ZoneId, UserName);
            //CommentAll.AddRange(CmtAll);
            //CommentAll.AddRange(CommentForever);
            var CommentByZone = new List<CommentEntity>();
            if (ZoneId > 0)
            {
                CommentByZone = (from c in CommentAll
                                 where listZone.Any(k => k.Id == c.ZoneId)
                                 select c).ToList();
            }
            else
                CommentByZone = CommentAll;
         

            //Coment
            var listNewCommentKyNay = new List<ListTime>();
            var listNewCommentKyTruoc = new List<ListTime>();
            var ListCmdKyNay = (from c in CommentAll
                            where c.Type == 1
                            select new ListTime
                            {
                                Time = string.Format("{0:0#}", c.Time),
                                CountItem = c.CountItem
                            }).ToList();
            var ListCmdKyTruoc = (from c in CommentAll
                              where c.Type == 2
                              select new ListTime
                              {
                                  Time = string.Format("{0:0#}", c.Time),
                                  CountItem = c.CountItem
                              }).ToList();
            double TotalCmt = ListCmdKyNay.Count();
            double avgComment = TotalCmt;
            #region Add List
            if (LoaiTK == 1)
            {
               
                for (int i = 0; i < 24; i++)
                {
                    bool check = true;
                    //Comment kỳ Này
                    foreach (var item in ListCmdKyNay) { if (int.Parse(item.Time) == i) { check = false; break; } }
                    if (check)
                    {
                        var additem = new ListTime();
                        additem.Time = string.Format("{0:0#}", i);
                        additem.CountItem = 0;
                        listNewCommentKyNay.Add(additem);
                    }
                    //Comment kỳ trước
                    check = true;
                    foreach (var item in ListCmdKyTruoc) { if (int.Parse(item.Time) == i) { check = false; break; } }
                    if (check)
                    {
                        var additem = new ListTime();
                        additem.Time = string.Format("{0:0#}", i);
                        additem.CountItem = 0;
                        listNewCommentKyTruoc.Add(additem);
                    }
                }
            }


            else if (LoaiTK == 2)
            {
                if (DateTime.Now.DayOfWeek == DayOfWeek.Sunday)
                    avgComment = Math.Round(TotalCmt / 7);
                else
                    avgComment = Math.Round(TotalCmt / (int)DateTime.Now.DayOfWeek);
                for (int i = 1; i < 8; i++)
                {
                    bool check = true;
                    foreach (var item in ListCmdKyNay) { if (int.Parse(item.Time) == i) { check = false; break; } }
                    if (check)
                    {
                        var additem = new ListTime();
                        additem.Time = i.ToString();
                        additem.CountItem = 0;
                        listNewCommentKyNay.Add(additem);
                    }
                    check = true;
                    foreach (var item in ListCmdKyTruoc) { if (int.Parse(item.Time) == i) { check = false; break; } }
                    if (check)
                    {
                        var additem = new ListTime();
                        additem.Time = i.ToString();
                        additem.CountItem = 0;
                        listNewCommentKyTruoc.Add(additem);
                    }
                }

            }
            else
            {
                avgComment = Math.Round(TotalCmt / (dtavg.AddDays(-1).Day-(dtavg.AddDays(-1).Day-DateTime.Now.Day)));

                int cTruoc = DateTime.Parse(DateTime.Now.ToString("yyyy/MM/01")).AddDays(-1).Day;
                int cNay = DateTime.Parse(DateTime.Now.ToString("yyyy/MM/01")).AddMonths(1).AddDays(-1).Day;
                for (int i = 1; i < 32; i++)
                {
                    bool check = true;
                    if (i <= cNay)
                    {
                        //Comment kỳ này
                        foreach (var item in ListCmdKyNay) { if (int.Parse(item.Time) == i) { check = false; break; } }
                        if (check)
                        {
                            var additem = new ListTime();
                            additem.Time = i.ToString();
                            additem.CountItem = 0;
                            listNewCommentKyNay.Add(additem);
                        }
                    }
                    check = true;
                    if (i <= cTruoc)
                    {
                        //Comment
                        foreach (var item in ListCmdKyTruoc) { if (int.Parse(item.Time) == i) { check = false; break; } }
                        if (check)
                        {
                            var additem = new ListTime();
                            additem.Time = i.ToString();
                            additem.CountItem = 0;
                            listNewCommentKyTruoc.Add(additem);
                        }
                    }
                }
            }
            #endregion

            #region List Comment By Zone
            var CommentByUser = CommentAll;
            var cmtData = (from c in CommentAll
                           where c.Type == 3
                           select new CommentEntity
                           {
                               ZoneId = c.ZoneId,
                               CountItem = c.CountItem
                           }).ToList(); 

            var tempCmt = (from z in listZoneUser
                           join e in cmtData on z.Id equals e.ZoneId
                           into data
                           from subpet in data.DefaultIfEmpty()
                           select new ZoneEntity
                           {
                               Id = z.Id,
                               Name = z.Name,
                               Count = subpet == null ? 0 : subpet.CountItem,
                               GroupId = z.ParentId == 0 ? z.Id : z.ParentId,
                               ParentId = z.ParentId
                           }
                         ).OrderBy(k => k.ParentId).ToList();

            var listCommentByZone = (from f in tempCmt
                                     group f by f.GroupId into z
                                     select new ZoneEntity
                                     {
                                         Id = z.Key,
                                         Count = z.Sum(k => k.Count),
                                         Name = z.First().Name,
                                         ParentId = z.First().ParentId,
                                         Percent = TotalCmt == 0 ? 0 : Math.Round((double)z.Sum(k => k.Count) / TotalCmt * 100, 1)
                                     }).Where(K => K.ParentId == 0).ToList();
            #endregion

            #endregion
            ListCmdKyNay.AddRange(listNewCommentKyNay);
            ListCmdKyTruoc.AddRange(listNewCommentKyTruoc);
            if (LoaiTK == 2)
            {
                ListCmdKyNay = (from f in ListCmdKyNay
                                orderby f.Time
                                select new ListTime
                                {
                                    Time = f.Time == "1" ? "CN" : "T" + f.Time,
                                    CountItem = f.CountItem
                                }).ToList();
                var additem = new ListTime();
                additem.Time = ListCmdKyNay[0].Time;
                additem.CountItem = ListCmdKyNay[0].CountItem;
                ListCmdKyNay.RemoveAt(0);
                ListCmdKyNay.Add(additem);

                ListCmdKyTruoc = (from f in ListCmdKyTruoc
                                  orderby f.Time
                                  select new ListTime
                                  {
                                      Time = f.Time == "1" ? "CN" : "T" + f.Time,
                                      CountItem = f.CountItem
                                  }).ToList();
                additem = new ListTime();
                additem.Time = ListCmdKyTruoc[0].Time;
                additem.CountItem = ListCmdKyTruoc[0].CountItem;
                ListCmdKyTruoc.RemoveAt(0);
                ListCmdKyTruoc.Add(additem);
                statistics.ListCommentKyNay = ListCmdKyNay;
                statistics.ListCommentKyTruoc = ListCmdKyTruoc;
            }
            else
            {
                statistics.ListCommentKyNay = ListCmdKyNay.OrderBy(k => int.Parse(k.Time)).ToList();
                statistics.ListCommentKyTruoc = ListCmdKyTruoc.OrderBy(k => int.Parse(k.Time)).ToList();
            }

            statistics.ListCommentByZone = new List<ZoneEntity>();
            statistics.ListCommentByZone.AddRange(listCommentByZone);
            statistics.CountCommentKyNay =(int)Math.Round(TotalCmt);
            statistics.CountCommentKyTruoc = TotalOld;
            statistics.AverageComment = Convert.ToInt32(avgComment);
            double dg = CommentByZone.Where(k => k.CreatedDate >= dateGrowth & k.CreatedDate < df).Count();
            if (dg > 0)
                statistics.GrowthComment = Math.Round(statistics.CountCommentKyNay / dg * 100 - 100, 1);
            else
                statistics.GrowthComment = statistics.CountCommentKyNay;
            return statistics;
        }
        public static StatisticsBoxTongHopEntity BoxTongHop(int LoaiTK, int ZoneId, string UserName)
        {
            var statistics = new StatisticsBoxTongHopEntity();
            statistics = StatisticsNewsBo.Counter_News(LoaiTK, ZoneId, UserName);
            var listZone = new List<ZoneEntity>();
            #region SetTime
            DateTime df = DateTime.Parse(DateTime.Now.ToString("yyyy/MM/dd 00:00:00"));
            DateTime dt = DateTime.Parse(DateTime.Now.ToString("yyyy/MM/dd 00:00:00"));
            if (LoaiTK == 2)
            {
                while (df.DayOfWeek != DayOfWeek.Monday) df = df.AddDays(-1);
            }
            else if (LoaiTK == 3)
            {
                df = DateTime.Parse(df.ToString("yyyy/MM/01 00:00"));
            }
            #endregion

            #region ExternalNews
            if (ZoneId > 0)
            {
                var ext = StatisticsExternalNewsBo.Counter_ExternalNews(LoaiTK, ZoneId, UserName);
                statistics.Cooperator = ext.Cooperator;
                statistics.CooperatorT1 = ext.CooperatorT1;
                statistics.CooperatorT2 = ext.CooperatorT2;
                statistics.CooperatorT3 = ext.CooperatorT3;
                listZone = ZoneBo.ListZoneById(ZoneId);
            }
            else
            {
                listZone = ZoneBo.ListZoneByUserName(UserName);
                var External = StatisticsExternalNewsBo.ListCounter_ExternalNews(LoaiTK, ZoneId, UserName);
                var objEx = from e in External
                            where listZone.Any(k => k.Id == e.ZoneId)
                            select e;
                statistics.Cooperator = objEx.Where(k => k.LastModifiedDate >= df & k.LastModifiedDate <= DateTime.Now).Count();
                statistics.CooperatorT1 = objEx.Where(k => k.LastModifiedDate < dt.AddDays(Constants.TIME2)).Count();
                statistics.CooperatorT2 = objEx.Where(k => k.LastModifiedDate >= dt.AddDays(Constants.TIME2) & k.LastModifiedDate < dt.AddDays(Constants.TIME1 - 1)).Count();
                statistics.CooperatorT3 = objEx.Where(k => k.LastModifiedDate >= dt.AddDays(Constants.TIME1) & k.LastModifiedDate <= DateTime.Now).Count();
            }
            #endregion

            #region Comment box
            var Comment = StatisticsCommentBo.Counter_Comment(LoaiTK, ZoneId, UserName);
            var objCmt = from e in Comment
                         where listZone.Any(k => k.Id == e.ZoneId)
                         select e;
            statistics.Comment = objCmt.Where(k => k.CreatedDate >= df & k.CreatedDate <= DateTime.Now).Count();
            statistics.CommentT1 = objCmt.Where(k => k.CreatedDate < dt.AddDays(Constants.TIME2)).Count();
            statistics.CommentT2 = objCmt.Where(k => k.CreatedDate >= dt.AddDays(Constants.TIME2) & k.CreatedDate < dt.AddDays(Constants.TIME1 - 1)).Count();
            statistics.CommentT3 = objCmt.Where(k => k.CreatedDate >= dt.AddDays(Constants.TIME1) & k.CreatedDate <= DateTime.Now).Count();
            #endregion
            //User
            var User = StatisticsCommentUserBo.Counter_CommentUserPendingByTime(LoaiTK, UserName);
            statistics.UserPending = User.UserPending;
            statistics.UserPendingT1 = User.UserPendingT1;
            statistics.UserPendingT2 = User.UserPendingT2;
            statistics.UserPendingT3 = User.UserPendingT3;
            return statistics;
        }
        public static CountUserEntity Statistics_User(int LoaiTK, string UserName)
        {
            var UserObj = StatisticsCommentUserBo.Counter_CommentUserByTime(LoaiTK, UserName);
            var ListUserKynay = UserObj.ListTime.Where(k => k.Type == 11).ToList();
            var ListUserKyTruoc = UserObj.ListTime.Where(k => k.Type == 12).ToList();
            var listNewUserKyNay = new List<ListTime>();
            var listNewUserKyTruoc = new List<ListTime>();

            #region Add List
            if (LoaiTK == 1)
            {
                for (int i = 0; i < 24; i++)
                {
                    bool check = true;
                    //User Kỳ này
                    check = true;
                    foreach (var item in ListUserKynay) { if (int.Parse(item.Time) == i) { check = false; break; } }
                    if (check)
                    {
                        var additem = new ListTime();
                        additem.Time = string.Format("{0:0#}", i);
                        additem.CountItem = 0;
                        listNewUserKyNay.Add(additem);
                    }
                    //User kỳ trước
                    check = true;
                    foreach (var item in ListUserKyTruoc) { if (int.Parse(item.Time) == i) { check = false; break; } }
                    if (check)
                    {
                        var additem = new ListTime();
                        additem.Time = string.Format("{0:0#}", i);
                        additem.CountItem = 0;
                        listNewUserKyTruoc.Add(additem);
                    }
                }
            }
            else if (LoaiTK == 2)
            {
                for (int i = 1; i < 8; i++)
                {
                    bool check = true;
                    //User
                    check = true;
                    foreach (var item in ListUserKynay) { if (int.Parse(item.Time) == i) { check = false; break; } }
                    if (check)
                    {
                        var additem = new ListTime();
                        additem.Time = i.ToString();
                        additem.CountItem = 0;
                        listNewUserKyNay.Add(additem);
                    }
                    check = true;
                    foreach (var item in ListUserKyTruoc) { if (int.Parse(item.Time) == i) { check = false; break; } }
                    if (check)
                    {
                        var additem = new ListTime();
                        additem.Time = i.ToString();
                        additem.CountItem = 0;
                        listNewUserKyTruoc.Add(additem);
                    }
                }
            }
            else
            {
                int cTruoc = DateTime.Parse(DateTime.Now.ToString("yyyy/MM/01")).AddDays(-1).Day;
                int cNay = DateTime.Parse(DateTime.Now.ToString("yyyy/MM/01")).AddMonths(1).AddDays(-1).Day;
                for (int i = 1; i < 32; i++)
                {
                    bool check = true;
                    if (i <= cNay)
                    {
                        check = true;
                        foreach (var item in ListUserKynay) { if (int.Parse(item.Time) == i) { check = false; break; } }
                        if (check)
                        {
                            var additem = new ListTime();
                            additem.Time = i.ToString();
                            additem.CountItem = 0;
                            listNewUserKyNay.Add(additem);
                        }
                    }
                    check = true;
                    if (i <= cTruoc)
                    {
                        check = true;
                        foreach (var item in ListUserKyTruoc) { if (int.Parse(item.Time) == i) { check = false; break; } }
                        if (check)
                        {
                            var additem = new ListTime();
                            additem.Time = i.ToString();
                            additem.CountItem = 0;
                            listNewUserKyTruoc.Add(additem);
                        }
                    }
                }
            }
            #endregion

            ListUserKynay.AddRange(listNewUserKyNay);
            ListUserKyTruoc.AddRange(listNewUserKyTruoc);
            if (LoaiTK == 2)
            {
                ListUserKynay = (from f in ListUserKynay
                                orderby f.Time
                                select new ListTime
                                {
                                    Time = f.Time == "1" ? "CN" : "T" + f.Time,
                                    CountItem = f.CountItem
                                }).ToList();
                var additem = new ListTime();
                additem.Time = ListUserKynay[0].Time;
                additem.CountItem = ListUserKynay[0].CountItem;
                ListUserKynay.RemoveAt(0);
                ListUserKynay.Add(additem);

                ListUserKyTruoc = (from f in ListUserKyTruoc
                                  orderby f.Time
                                  select new ListTime
                                  {
                                      Time = f.Time == "1" ? "CN" : "T" + f.Time,
                                      CountItem = f.CountItem
                                  }).ToList();
                additem = new ListTime();
                additem.Time = ListUserKyTruoc[0].Time;
                additem.CountItem = ListUserKyTruoc[0].CountItem;
                ListUserKyTruoc.RemoveAt(0);
                ListUserKyTruoc.Add(additem);
                UserObj.ListUserKyNay = ListUserKynay;
                UserObj.ListUserKyTruoc = ListUserKyTruoc;
            }
            else
            {
                UserObj.ListUserKyNay = ListUserKynay.OrderBy(k => int.Parse(k.Time)).ToList();
                UserObj.ListUserKyTruoc = ListUserKyTruoc.OrderBy(k => int.Parse(k.Time)).ToList();
            }
            UserObj.CountUserKyTruoc = UserObj.Total;
            return UserObj;
        }
        public static StatisticsProductionNewsPublishEntity Statistics_NewsPublish(int LoaiTK, int ZoneId, string UserName)
        {
            var ProductionNews = StatisticsNewsBo.Counter_ProductionNews_Publish(LoaiTK, ZoneId, UserName);
            var listPublistKyNay = ProductionNews.ListTime.Where(k => k.Type == 21).ToList();
            var listPublistKyTruoc = ProductionNews.ListTime.Where(k => k.Type == 22).ToList();
            var listNewPublistKyNay = new List<ListTime>();
            var listNewPublistKyTruoc = new List<ListTime>();

            #region Add List
            if (LoaiTK == 1)
            {
                for (int i = 0; i < 24; i++)
                {
                    bool check = true;

                    //News Kỳ này Publish
                    check = true;
                    foreach (var item in listPublistKyNay) { if (int.Parse(item.Time) == i) { check = false; break; } }
                    if (check)
                    {
                        var additem = new ListTime();
                        additem.Time = string.Format("{0:0#}", i);
                        additem.CountItem = 0;
                        listNewPublistKyNay.Add(additem);
                    }
                    //News Kỳ trước publish
                    check = true;
                    foreach (var item in listPublistKyTruoc) { if (int.Parse(item.Time) == i) { check = false; break; } }
                    if (check)
                    {
                        var additem = new ListTime();
                        additem.Time = string.Format("{0:0#}", i);
                        additem.CountItem = 0;
                        listNewPublistKyTruoc.Add(additem);
                    }
                }
            }
            else if (LoaiTK == 2)
            {
                for (int i = 1; i < 8; i++)
                {
                    bool check = true;
                    foreach (var item in listPublistKyNay) { if (int.Parse(item.Time) == i) { check = false; break; } }
                    if (check)
                    {
                        var additem = new ListTime();
                        additem.Time = i.ToString();
                        additem.CountItem = 0;
                        listNewPublistKyNay.Add(additem);
                    }
                    check = true;
                    foreach (var item in listPublistKyTruoc) { if (int.Parse(item.Time) == i) { check = false; break; } }
                    if (check)
                    {
                        var additem = new ListTime();
                        additem.Time = i.ToString();
                        additem.CountItem = 0;
                        listNewPublistKyTruoc.Add(additem);
                    }
                }
            }
            else
            {
                int cTruoc = DateTime.Parse(DateTime.Now.ToString("yyyy/MM/01")).AddDays(-1).Day;
                int cNay = DateTime.Parse(DateTime.Now.ToString("yyyy/MM/01")).AddMonths(1).AddDays(-1).Day;
                for (int i = 1; i < 32; i++)
                {
                    bool check = true;
                    if (i <= cNay)
                    {
                        foreach (var item in listPublistKyNay) { if (int.Parse(item.Time) == i) { check = false; break; } }
                        if (check)
                        {
                            var additem = new ListTime();
                            additem.Time = i.ToString();
                            additem.CountItem = 0;
                            listNewPublistKyNay.Add(additem);
                        }
                    }
                    check = true;
                    if (i <= cTruoc)
                    {
                        foreach (var item in listPublistKyTruoc) { if (int.Parse(item.Time) == i) { check = false; break; } }
                        if (check)
                        {
                            var additem = new ListTime();
                            additem.Time = i.ToString();
                            additem.CountItem = 0;
                            listNewPublistKyTruoc.Add(additem);
                        }
                    }
                }
            }
            #endregion
            listPublistKyNay.AddRange(listNewPublistKyNay);
            listPublistKyTruoc.AddRange(listNewPublistKyTruoc);
            if (LoaiTK == 2)
            {
                listPublistKyNay = (from f in listPublistKyNay
                                orderby f.Time
                                select new ListTime
                                {
                                    Time = f.Time == "1" ? "CN" : "T" + f.Time,
                                    CountItem = f.CountItem
                                }).ToList();
                var additem = new ListTime();
                additem.Time = listPublistKyNay[0].Time;
                additem.CountItem = listPublistKyNay[0].CountItem;
                listPublistKyNay.RemoveAt(0);
                listPublistKyNay.Add(additem);

                listPublistKyTruoc = (from f in listPublistKyTruoc
                                  orderby f.Time
                                  select new ListTime
                                  {
                                      Time = f.Time == "1" ? "CN" : "T" + f.Time,
                                      CountItem = f.CountItem
                                  }).ToList();
                additem = new ListTime();
                additem.Time = listPublistKyTruoc[0].Time;
                additem.CountItem = listPublistKyTruoc[0].CountItem;
                listPublistKyTruoc.RemoveAt(0);
                listPublistKyTruoc.Add(additem);
                ProductionNews.ListPublishNewsKyNay = listPublistKyNay;
                ProductionNews.ListPublishNewsKyTruoc = listPublistKyNay;
            }
            else
            {
                ProductionNews.ListPublishNewsKyNay = listPublistKyNay.OrderBy(k => int.Parse(k.Time)).ToList();
                ProductionNews.ListPublishNewsKyTruoc = listPublistKyNay.OrderBy(k => int.Parse(k.Time)).ToList();
            }

            return ProductionNews;
        }
        public static StatisticsProductionNewsSendEntity Statistics_NewsSend(int LoaiTK, int ZoneId, string UserName)
        {

            var ProductionNews = StatisticsNewsBo.Counter_ProductionNews_Send(LoaiTK, ZoneId, UserName);
            var listSendKyNay = ProductionNews.ListTime.Where(k => k.Type == 11).ToList();
            var listSendKyTruoc = ProductionNews.ListTime.Where(k => k.Type == 12).ToList();

            var listNewSendKyNay = new List<ListTime>();
            var listNewSendKyTruoc = new List<ListTime>();

            #region Add List
            if (LoaiTK == 1)
            {
                for (int i = 0; i < 24; i++)
                {
                    bool check = true;
                    //News kỳ này all
                    foreach (var item in listSendKyNay) { if (int.Parse(item.Time) == i) { check = false; break; } }
                    if (check)
                    {
                        var additem = new ListTime();
                        additem.Time = string.Format("{0:0#}", i);
                        additem.CountItem = 0;
                        listNewSendKyNay.Add(additem);
                    }
                    //News Kỳ trước all
                    check = true;
                    foreach (var item in listSendKyTruoc) { if (int.Parse(item.Time) == i) { check = false; break; } }
                    if (check)
                    {
                        var additem = new ListTime();
                        additem.Time = string.Format("{0:0#}", i);
                        additem.CountItem = 0;
                        listNewSendKyTruoc.Add(additem);
                    }
                }
            }
            else if (LoaiTK == 2)
            {
                for (int i = 1; i < 8; i++)
                {
                    bool check = true;
                    foreach (var item in listSendKyNay) { if (int.Parse(item.Time) == i) { check = false; break; } }
                    if (check)
                    {
                        var additem = new ListTime();
                        additem.Time = i.ToString();
                        additem.CountItem = 0;
                        listNewSendKyNay.Add(additem);
                    }
                    check = true;
                    foreach (var item in listSendKyTruoc) { if (int.Parse(item.Time) == i) { check = false; break; } }
                    if (check)
                    {
                        var additem = new ListTime();
                        additem.Time = i.ToString();
                        additem.CountItem = 0;
                        listNewSendKyTruoc.Add(additem);
                    }
                }
            }
            else
            {
                int cTruoc = DateTime.Parse(DateTime.Now.ToString("yyyy/MM/01")).AddDays(-1).Day;
                int cNay = DateTime.Parse(DateTime.Now.ToString("yyyy/MM/01")).AddMonths(1).AddDays(-1).Day;
                for (int i = 1; i < 32; i++)
                {
                    bool check = true;
                    if (i <= cNay)
                    {
                        foreach (var item in listSendKyNay) { if (int.Parse(item.Time) == i) { check = false; break; } }
                        if (check)
                        {
                            var additem = new ListTime();
                            additem.Time = i.ToString();
                            additem.CountItem = 0;
                            listNewSendKyNay.Add(additem);
                        }
                    }
                    check = true;
                    if (i <= cTruoc)
                    {
                        foreach (var item in listSendKyTruoc) { if (int.Parse(item.Time) == i) { check = false; break; } }
                        if (check)
                        {
                            var additem = new ListTime();
                            additem.Time = i.ToString();
                            additem.CountItem = 0;
                            listNewSendKyTruoc.Add(additem);
                        }
                    }
                }
            }
            #endregion

            listSendKyNay.AddRange(listNewSendKyNay);
            listSendKyTruoc.AddRange(listNewSendKyTruoc);
            if (LoaiTK == 2)
            {
                listSendKyNay = (from f in listSendKyNay
                                    orderby f.Time
                                    select new ListTime
                                    {
                                        Time = f.Time == "1" ? "CN" : "T" + f.Time,
                                        CountItem = f.CountItem
                                    }).ToList();
                var additem = new ListTime();
                additem.Time = listSendKyNay[0].Time;
                additem.CountItem = listSendKyNay[0].CountItem;
                listSendKyNay.RemoveAt(0);
                listSendKyNay.Add(additem);

                listSendKyTruoc = (from f in listSendKyTruoc
                                      orderby f.Time
                                      select new ListTime
                                      {
                                          Time = f.Time == "1" ? "CN" : "T" + f.Time,
                                          CountItem = f.CountItem
                                      }).ToList();
                additem = new ListTime();
                additem.Time = listSendKyTruoc[0].Time;
                additem.CountItem = listSendKyTruoc[0].CountItem;
                listSendKyTruoc.RemoveAt(0);
                listSendKyTruoc.Add(additem);
                ProductionNews.ListSendNewsKyNay = listSendKyNay;
                ProductionNews.ListSendNewsKyTruoc = listSendKyTruoc;
            }
            else
            {
                ProductionNews.ListSendNewsKyNay = listSendKyNay.OrderBy(k => int.Parse(k.Time)).ToList();
                ProductionNews.ListSendNewsKyTruoc = listSendKyTruoc.OrderBy(k => int.Parse(k.Time)).ToList();
            }
            return ProductionNews;
        }
        public static StatisticsProductionVideoPublishEntity Statistics_VideoPublish(int LoaiTK, int ZoneId, string UserName)
        {
            var ProductionVideo = StatisticsVideoBo.Counter_ProductionVideo_Publish(LoaiTK, ZoneId, UserName);
            var listPublistVideoKyNay = ProductionVideo.ListTime.Where(k => k.Type == 41).ToList();
            var listPublistVideoKyTruoc = ProductionVideo.ListTime.Where(k => k.Type == 42).ToList();
            var listNewPublistVideoKyNay = new List<ListTime>();
            var listNewPublistVideoKyTruoc = new List<ListTime>();

            #region Add List
            if (LoaiTK == 1)
            {
                for (int i = 0; i < 24; i++)
                {
                    bool check = true;
                    //Video Publish Kỳ này
                    foreach (var item in listPublistVideoKyNay) { if (int.Parse(item.Time) == i) { check = false; break; } }
                    if (check)
                    {
                        var additem = new ListTime();
                        additem.Time = string.Format("{0:0#}", i);
                        additem.CountItem = 0;
                        listNewPublistVideoKyNay.Add(additem);
                    }
                    //Video Phublish kỳ trước
                    check = true;
                    foreach (var item in listPublistVideoKyTruoc) { if (int.Parse(item.Time) == i) { check = false; break; } }
                    if (check)
                    {
                        var additem = new ListTime();
                        additem.Time = string.Format("{0:0#}", i);
                        additem.CountItem = 0;
                        listNewPublistVideoKyTruoc.Add(additem);
                    }
                }
            }
            else if (LoaiTK == 2)
            {
                for (int i = 1; i < 8; i++)
                {
                    bool check = true;

                    foreach (var item in listPublistVideoKyNay) { if (int.Parse(item.Time) == i) { check = false; break; } }
                    if (check)
                    {
                        var additem = new ListTime();
                        additem.Time = i.ToString();
                        additem.CountItem = 0;
                        listNewPublistVideoKyNay.Add(additem);
                    }
                    check = true;
                    foreach (var item in listPublistVideoKyTruoc) { if (int.Parse(item.Time) == i) { check = false; break; } }
                    if (check)
                    {
                        var additem = new ListTime();
                        additem.Time = i.ToString();
                        additem.CountItem = 0;
                        listNewPublistVideoKyTruoc.Add(additem);
                    }
                }
            }
            else
            {
                int cTruoc = DateTime.Parse(DateTime.Now.ToString("yyyy/MM/01")).AddDays(-1).Day;
                int cNay = DateTime.Parse(DateTime.Now.ToString("yyyy/MM/01")).AddMonths(1).AddDays(-1).Day;
                for (int i = 1; i < 32; i++)
                {
                    bool check = true;
                    if (i <= cNay)
                    {
                        check = true;
                        foreach (var item in listPublistVideoKyNay) { if (int.Parse(item.Time) == i) { check = false; break; } }
                        if (check)
                        {
                            var additem = new ListTime();
                            additem.Time = i.ToString();
                            additem.CountItem = 0;
                            listNewPublistVideoKyNay.Add(additem);
                        }
                    }
                    check = true;
                    if (i <= cTruoc)
                    {
                        foreach (var item in listPublistVideoKyTruoc) { if (int.Parse(item.Time) == i) { check = false; break; } }
                        if (check)
                        {
                            var additem = new ListTime();
                            additem.Time = i.ToString();
                            additem.CountItem = 0;
                            listNewPublistVideoKyTruoc.Add(additem);
                        }
                    }
                }
            }
            #endregion

            listPublistVideoKyNay.AddRange(listNewPublistVideoKyNay);
            listPublistVideoKyTruoc.AddRange(listNewPublistVideoKyTruoc);
            if (LoaiTK == 2)
            {
                listPublistVideoKyNay = (from f in listPublistVideoKyNay
                                 orderby f.Time
                                 select new ListTime
                                 {
                                     Time = f.Time == "1" ? "CN" : "T" + f.Time,
                                     CountItem = f.CountItem
                                 }).ToList();
                var additem = new ListTime();
                additem.Time = listPublistVideoKyNay[0].Time;
                additem.CountItem = listPublistVideoKyNay[0].CountItem;
                listPublistVideoKyNay.RemoveAt(0);
                listPublistVideoKyNay.Add(additem);

                listPublistVideoKyTruoc = (from f in listPublistVideoKyTruoc
                                   orderby f.Time
                                   select new ListTime
                                   {
                                       Time = f.Time == "1" ? "CN" : "T" + f.Time,
                                       CountItem = f.CountItem
                                   }).ToList();
                additem = new ListTime();
                additem.Time = listPublistVideoKyTruoc[0].Time;
                additem.CountItem = listPublistVideoKyTruoc[0].CountItem;
                listPublistVideoKyTruoc.RemoveAt(0);
                listPublistVideoKyTruoc.Add(additem);
                ProductionVideo.ListPublishVideoKyNay = listPublistVideoKyNay;
                ProductionVideo.ListPublishVideoKyTruoc = listPublistVideoKyTruoc;
            }
            else
            {
                ProductionVideo.ListPublishVideoKyNay = listPublistVideoKyNay.OrderBy(k => int.Parse(k.Time)).ToList();
                ProductionVideo.ListPublishVideoKyTruoc = listPublistVideoKyTruoc.OrderBy(k => int.Parse(k.Time)).ToList();
            }
            return ProductionVideo;
        }
        public static StatisticsProductionVideoSendEntity Statistics_VideoSend(int LoaiTK, int ZoneId, string UserName)
        {
            var ProductionVideo = StatisticsVideoBo.Counter_ProductionVideo_Send(LoaiTK, ZoneId, UserName);

            var listSendVideoKyNay = ProductionVideo.ListTime.Where(k => k.Type == 31).ToList();
            var listSendVideoKyTruoc = ProductionVideo.ListTime.Where(k => k.Type == 32).ToList();
            var listNewSendVideoKyNay = new List<ListTime>();
            var listNewSendVideoKyTruoc = new List<ListTime>();

            #region Add List
            if (LoaiTK == 1)
            {
                for (int i = 0; i < 24; i++)
                {
                    bool check = true;
                    //Video kỳ này all
                    foreach (var item in listSendVideoKyNay) { if (int.Parse(item.Time) == i) { check = false; break; } }
                    if (check)
                    {
                        var additem = new ListTime();
                        additem.Time = string.Format("{0:0#}", i);
                        additem.CountItem = 0;
                        listNewSendVideoKyNay.Add(additem);
                    }
                    //Video Kỳ trước all
                    check = true;
                    foreach (var item in listSendVideoKyTruoc) { if (int.Parse(item.Time) == i) { check = false; break; } }
                    if (check)
                    {
                        var additem = new ListTime();
                        additem.Time = string.Format("{0:0#}", i);
                        additem.CountItem = 0;
                        listNewSendVideoKyTruoc.Add(additem);
                    }
                }
            }
            else if (LoaiTK == 2)
            {
                for (int i = 1; i < 8; i++)
                {
                    bool check = true;
                    foreach (var item in listSendVideoKyNay) { if (int.Parse(item.Time) == i) { check = false; break; } }
                    if (check)
                    {
                        var additem = new ListTime();
                        additem.Time = i.ToString();
                        additem.CountItem = 0;
                        listNewSendVideoKyNay.Add(additem);
                    }
                    check = true;
                    foreach (var item in listSendVideoKyTruoc) { if (int.Parse(item.Time) == i) { check = false; break; } }
                    if (check)
                    {
                        var additem = new ListTime();
                        additem.Time = i.ToString();
                        additem.CountItem = 0;
                        listNewSendVideoKyTruoc.Add(additem);
                    }
                }
            }
            else
            {
                int cTruoc = DateTime.Parse(DateTime.Now.ToString("yyyy/MM/01")).AddDays(-1).Day;
                int cNay = DateTime.Parse(DateTime.Now.ToString("yyyy/MM/01")).AddMonths(1).AddDays(-1).Day;
                for (int i = 1; i < 32; i++)
                {
                    bool check = true;
                    if (i <= cNay)
                    {
                        check = true;
                        foreach (var item in listSendVideoKyNay) { if (int.Parse(item.Time) == i) { check = false; break; } }
                        if (check)
                        {
                            var additem = new ListTime();
                            additem.Time = i.ToString();
                            additem.CountItem = 0;
                            listNewSendVideoKyNay.Add(additem);
                        }
                    }
                    check = true;
                    if (i <= cTruoc)
                    {
                        check = true;
                        foreach (var item in listSendVideoKyTruoc) { if (int.Parse(item.Time) == i) { check = false; break; } }
                        if (check)
                        {
                            var additem = new ListTime();
                            additem.Time = i.ToString();
                            additem.CountItem = 0;
                            listNewSendVideoKyTruoc.Add(additem);
                        }
                    }
                }
            }
            #endregion

            listSendVideoKyNay.AddRange(listNewSendVideoKyNay);
            listSendVideoKyTruoc.AddRange(listNewSendVideoKyTruoc);
            if (LoaiTK == 2)
            {
                listSendVideoKyNay = (from f in listSendVideoKyNay
                                 orderby f.Time
                                 select new ListTime
                                 {
                                     Time = f.Time == "1" ? "CN" : "T" + f.Time,
                                     CountItem = f.CountItem
                                 }).ToList();
                var additem = new ListTime();
                additem.Time = listSendVideoKyNay[0].Time;
                additem.CountItem = listSendVideoKyNay[0].CountItem;
                listSendVideoKyNay.RemoveAt(0);
                listSendVideoKyNay.Add(additem);

                listSendVideoKyTruoc = (from f in listSendVideoKyTruoc
                                   orderby f.Time
                                   select new ListTime
                                   {
                                       Time = f.Time == "1" ? "CN" : "T" + f.Time,
                                       CountItem = f.CountItem
                                   }).ToList();
                additem = new ListTime();
                additem.Time = listSendVideoKyTruoc[0].Time;
                additem.CountItem = listSendVideoKyTruoc[0].CountItem;
                listSendVideoKyTruoc.RemoveAt(0);
                listSendVideoKyTruoc.Add(additem);
                ProductionVideo.ListSendVideoKyNay = listSendVideoKyNay;
                ProductionVideo.ListSendVideoKyTruoc = listSendVideoKyTruoc;
            }
            else
            {
                ProductionVideo.ListSendVideoKyNay = listSendVideoKyNay.OrderBy(k => int.Parse(k.Time)).ToList();
                ProductionVideo.ListSendVideoKyTruoc = listSendVideoKyTruoc.OrderBy(k => int.Parse(k.Time)).ToList();
            }
            return ProductionVideo;
        }
        public static List<ListPercent> Statistics_PercentNewsByCategory(int LoaiTK, int ZoneId, string UserName)
        {
            return StatisticsNewsBo.Counter_NewsPercentByCategory(LoaiTK, ZoneId, UserName);
        }
        public static List<ListPercent> Statistics_PercentNewsByZone(int LoaiTK, string UserName)
        {
            return StatisticsNewsBo.Counter_NewsPercentByZone(LoaiTK, UserName);
        }
        public static List<ListPercent> Statistics_PercentVideoByZone(int LoaiTK, string UserName)
        {
            return StatisticsVideoBo.Counter_VideoPercentByZone(LoaiTK, UserName);
        }

        public static List<TopPVEntity> Statistics_TopPV(int LoaiTK, int ZoneId, string UserName, int TopSize, string Keyword)
        {
            string ListUser = "";
            if(!string.IsNullOrEmpty(Keyword))
            {
                var u1 = UserBo.User_Search(Keyword);
                var u2 = StatisticsExternalUserBo.ListExternalUser_Search(Keyword);
                u1.AddRange(u2);
                ListUser = ",";
                foreach (var item in u1)
                {
                    ListUser += item.Username + ",";
                }
            }
            var data = StatisticsNewsBo.STATISTICS_TOPPV(LoaiTK, ZoneId, UserName, TopSize, ListUser);
            var listpv = data.Where(k => k.Type == 1).ToList();
            var listGuikynay = data.Where(k => k.Type == 31).ToList();
            var listGuikytruoc = data.Where(k => k.Type == 32).ToList();
            var listXuatkynay = data.Where(k => k.Type == 21).ToList();
            var listXuatkytruoc = data.Where(k => k.Type == 22).ToList();
            var objReturn = new List<TopPVEntity>();
            string listUsers = "";
            foreach (var pv in listpv)
            {
                var add = new TopPVEntity();
                add.FullName = pv.FullName;
                add.Avatar = pv.Avatar;
                add.Username = pv.createdby;
                var Guikynay = (from f in listGuikynay
                            where f.createdby.Equals(pv.createdby)
                            select new ListTime { Time = f.Time, CountItem = f.CountItem }).ToList();
                var Guikytruoc = (from f in listGuikytruoc
                              where f.createdby.Equals(pv.createdby)
                              select new ListTime { Time = f.Time, CountItem = f.CountItem }).ToList();
                var Xuatkynay = (from f in listXuatkynay
                             where f.createdby.Equals(pv.createdby)
                             select new ListTime { Time = f.Time, CountItem = f.CountItem }).ToList();
                var Xuatkytruoc = (from f in listXuatkytruoc
                               where f.createdby.Equals(pv.createdby)
                               select new ListTime { Time = f.Time, CountItem = f.CountItem }).ToList();
                var addGuiNay = new List<ListTime>();
                var addGuiTruoc = new List<ListTime>();
                var addXuatNay = new List<ListTime>();
                var addXuatTruoc = new List<ListTime>();
                #region Add List
                if (LoaiTK == 1)
                {
                    for (int i = 0; i < 24; i++)
                    {
                        bool check = true;
                        foreach (var item in Guikynay) { if (int.Parse(item.Time) == i) { check = false; break; } }
                        if (check)
                        {
                            var additem = new ListTime();
                            additem.Time = string.Format("{0:0#}", i);
                            additem.CountItem = 0;
                            addGuiNay.Add(additem);
                        }
                        check = true;
                        foreach (var item in Guikytruoc) { if (int.Parse(item.Time) == i) { check = false; break; } }
                        if (check)
                        {
                            var additem = new ListTime();
                            additem.Time = string.Format("{0:0#}", i);
                            additem.CountItem = 0;
                            addGuiTruoc.Add(additem);
                        }
                        check = true;
                        foreach (var item in Xuatkynay) { if (int.Parse(item.Time) == i) { check = false; break; } }
                        if (check)
                        {
                            var additem = new ListTime();
                            additem.Time = string.Format("{0:0#}", i);
                            additem.CountItem = 0;
                            addXuatNay.Add(additem);
                        }
                        check = true;
                        foreach (var item in Xuatkytruoc) { if (int.Parse(item.Time) == i) { check = false; break; } }
                        if (check)
                        {
                            var additem = new ListTime();
                            additem.Time = string.Format("{0:0#}", i);
                            additem.CountItem = 0;
                            addXuatTruoc.Add(additem);
                        }
                    }
                }
                else if (LoaiTK == 2)
                {
                    for (int i = 1; i < 8; i++)
                    {
                        bool check = true;
                        foreach (var item in Guikynay) { if (int.Parse(item.Time) == i) { check = false; break; } }
                        if (check)
                        {
                            var additem = new ListTime();
                            additem.Time = i.ToString();
                            additem.CountItem = 0;
                            addGuiNay.Add(additem);
                        }
                        check = true;
                        foreach (var item in Guikytruoc) { if (int.Parse(item.Time) == i) { check = false; break; } }
                        if (check)
                        {
                            var additem = new ListTime();
                            additem.Time = i.ToString();
                            additem.CountItem = 0;
                            addGuiTruoc.Add(additem);
                        }
                        check = true;
                        foreach (var item in Xuatkynay) { if (int.Parse(item.Time) == i) { check = false; break; } }
                        if (check)
                        {
                            var additem = new ListTime();
                            additem.Time = i.ToString();
                            additem.CountItem = 0;
                            addXuatNay.Add(additem);
                        }
                        check = true;
                        foreach (var item in Xuatkytruoc) { if (int.Parse(item.Time) == i) { check = false; break; } }
                        if (check)
                        {
                            var additem = new ListTime();
                            additem.Time = i.ToString();
                            additem.CountItem = 0;
                            addXuatTruoc.Add(additem);
                        }
                    }
                }
                else
                {
                    int cTruoc = DateTime.Parse(DateTime.Now.ToString("yyyy/MM/01")).AddDays(-1).Day;
                    int cNay = DateTime.Parse(DateTime.Now.ToString("yyyy/MM/01")).AddMonths(1).AddDays(-1).Day;
                    for (int i = 1; i < 32; i++)
                    {
                        bool check = true;
                        if (i <= cNay)
                        {
                            check = true;
                            foreach (var item in Guikynay) { if (int.Parse(item.Time) == i) { check = false; break; } }
                            if (check)
                            {
                                var additem = new ListTime();
                                additem.Time = i.ToString();
                                additem.CountItem = 0;
                                addGuiNay.Add(additem);
                            }
                            check = true;
                            foreach (var item in Guikytruoc) { if (int.Parse(item.Time) == i) { check = false; break; } }
                            if (check)
                            {
                                var additem = new ListTime();
                                additem.Time = i.ToString();
                                additem.CountItem = 0;
                                addGuiTruoc.Add(additem);
                            }
                        }
                        check = true;
                        if (i <= cTruoc)
                        {
                            check = true;
                            foreach (var item in Xuatkynay) { if (int.Parse(item.Time) == i) { check = false; break; } }
                            if (check)
                            {
                                var additem = new ListTime();
                                additem.Time = i.ToString();
                                additem.CountItem = 0;
                                addXuatNay.Add(additem);
                            }
                            check = true;
                            foreach (var item in Xuatkytruoc) { if (int.Parse(item.Time) == i) { check = false; break; } }
                            if (check)
                            {
                                var additem = new ListTime();
                                additem.Time = i.ToString();
                                additem.CountItem = 0;
                                addXuatTruoc.Add(additem);
                            }
                        }
                    }
                }
                #endregion
                Guikynay.AddRange(addGuiNay);
                Guikytruoc.AddRange(addGuiTruoc);
                Xuatkynay.AddRange(addXuatNay);
                Xuatkytruoc.AddRange(addXuatTruoc);
                if (LoaiTK == 2)
                {
                    Guikynay = (from f in Guikynay
                            orderby f.Time
                            select new ListTime
                            {
                                Time = f.Time == "1" ? "CN" : "T" + f.Time,
                                CountItem = f.CountItem
                            }).ToList();
                    var additem = new ListTime();
                    additem.Time = Guikynay[0].Time;
                    additem.CountItem = Guikynay[0].CountItem;
                    Guikynay.RemoveAt(0);
                    Guikynay.Add(additem);

                    Guikytruoc = (from f in Guikytruoc
                                orderby f.Time
                                select new ListTime
                                {
                                    Time = f.Time == "1" ? "CN" : "T" + f.Time,
                                    CountItem = f.CountItem
                                }).ToList();
                    additem = new ListTime();
                    additem.Time = Guikytruoc[0].Time;
                    additem.CountItem = Guikytruoc[0].CountItem;
                    Guikytruoc.RemoveAt(0);
                    Guikytruoc.Add(additem);

                    Xuatkynay = (from f in Xuatkynay
                                orderby f.Time
                                select new ListTime
                                {
                                    Time = f.Time == "1" ? "CN" : "T" + f.Time,
                                    CountItem = f.CountItem
                                }).ToList();
                    additem = new ListTime();
                    additem.Time = Xuatkynay[0].Time;
                    additem.CountItem = Xuatkynay[0].CountItem;
                    Xuatkynay.RemoveAt(0);
                    Xuatkynay.Add(additem);

                    Xuatkytruoc = (from f in Xuatkytruoc
                                  orderby f.Time
                                  select new ListTime
                                  {
                                      Time = f.Time == "1" ? "CN" : "T" + f.Time,
                                      CountItem = f.CountItem
                                  }).ToList();
                    additem = new ListTime();
                    additem.Time = Xuatkytruoc[0].Time;
                    additem.CountItem = Xuatkytruoc[0].CountItem;
                    Xuatkytruoc.RemoveAt(0);
                    Xuatkytruoc.Add(additem);
                }
                else
                {
                    Guikynay = Guikynay.OrderBy(k => int.Parse(k.Time)).ToList();
                    Guikytruoc = Guikytruoc.OrderBy(k => int.Parse(k.Time)).ToList();
                    Xuatkynay = Xuatkynay.OrderBy(k => int.Parse(k.Time)).ToList();
                    Xuatkytruoc = Xuatkytruoc.OrderBy(k => int.Parse(k.Time)).ToList();
                }
                add.ListGuiKyNay = Guikynay;
                add.ListGuiKyTruoc = Guikytruoc;
                add.ListXuatbanKyNay = Xuatkynay;
                add.ListXuatbanKyTruoc = Xuatkytruoc;
                add.PageViewMobile = pv.PageViewMobile < 0 ? 0 : pv.PageViewMobile;
                add.PageViewWeb = pv.PageViewWeb < 0 ? 0 : pv.PageViewWeb;
                add.TotalPageView = add.PageViewWeb + add.PageViewMobile;
                int Tongguikn = Guikynay.Sum(k=>k.CountItem);
                int Tongguikt = Guikytruoc.Sum(k=>k.CountItem);
                int Tongxuatkn = Xuatkynay.Sum(k => k.CountItem);
                int Tongxuatkt = Xuatkytruoc.Sum(k => k.CountItem);
                double gGui = Tongguikt > 0 ? Math.Round((double)Tongguikn / Tongguikt * 100 - 100, 1) : Tongguikn;
                double gXuat = Tongxuatkt > 0 ? Math.Round((double)Tongxuatkn / Tongxuatkt * 100 - 100, 1) : Tongxuatkn;
                add.TotalGui = Tongguikn;
                add.PercentGui = (gGui > 0 ? ("+"+ gGui.ToString()) : gGui.ToString()) + "%";
                add.TotalXuatban = Tongxuatkn;
                add.PercentXuatban = (gXuat > 0 ? ("+" + gXuat.ToString()) : gXuat.ToString()) + "%";
                double hx = Tongguikn > 0 ? Math.Round((double)Tongxuatkn / Tongguikn * 100, 1) : 0;
                double hxtruoc = Tongguikt > 0 ? Math.Round((double)Tongxuatkt / Tongguikt * 100, 1) : 0;
                add.Hieusuat = hx + "%";
                add.PercentHieusuat = Math.Round(hx - hxtruoc,1) + "%";
                objReturn.Add(add);
                if (listUsers.Equals(""))
                    listUsers = ","+ pv.createdby.Replace("pv_","") +",";
                else
                    listUsers += pv.createdby.Replace("pv_", "") + ",";
            }
            var ExtenalUsers = StatisticsExternalUserBo.ListExternalUser(listUsers);
            objReturn = (from f in objReturn
                        join j in ExtenalUsers on f.Username equals j.Username
                        into i
                        from sub in i.DefaultIfEmpty()
                        select new TopPVEntity
                        {
                            Avatar = string.IsNullOrEmpty(f.Avatar) ? (sub != null ? sub.Avatar : "") : f.Avatar,
                            FullName = string.IsNullOrEmpty(f.FullName) ? (sub != null ? sub.FullName : f.Username) : f.FullName,
                            Hieusuat = f.Hieusuat,
                            ListGuiKyNay = f.ListGuiKyNay,
                            ListGuiKyTruoc = f.ListGuiKyTruoc,
                            ListXuatbanKyNay = f.ListXuatbanKyNay,
                            ListXuatbanKyTruoc = f.ListXuatbanKyTruoc,
                            PageViewMobile = f.PageViewMobile,
                            TotalGui = f.TotalGui,
                            TotalPageView = f.TotalPageView,
                            TotalXuatban = f.TotalXuatban,
                            PageViewWeb = f.PageViewWeb,
                            PercentGui=f.PercentGui,
                            PercentHieusuat=f.PercentHieusuat,
                            PercentXuatban=f.PercentXuatban
                        }).ToList();
            return objReturn;
        }

        public static void CreateCacheStatistics(object data, string DB, string cacheName, string[] tables)
        {
            //AggregateCacheDependency AggCache = new AggregateCacheDependency();
            //SqlCacheDependency lSQLCacheDependency = null;
            //foreach (string tb in tables)
            //{
            //    lSQLCacheDependency = new SqlCacheDependency(DB, tb);
            //    AggCache.Add(lSQLCacheDependency);
            //}
            //HttpContext.Current.Cache.Insert(cacheName, data, AggCache);
        }
        public static void CreateCacheStatistics_NothingDependency(object data, string DB, string cacheName, string[] tables)
        {
            HttpContext.Current.Cache.Insert(cacheName, data, null, DateTime.Parse(DateTime.Now.ToString("yyyy/MM/dd 00:00:00")).AddDays(1), TimeSpan.Zero);
        }
        public static void XoaCacheStatistics(string UserName)
        {
            foreach (System.Collections.DictionaryEntry entry in HttpContext.Current.Cache)
            {
                string key = entry.Key.ToString();
                if (key.Substring(key.LastIndexOf("_") + 1) == UserName && key.Substring(0, key.IndexOf("_")) == Constants.CACHE_STATISTICS_NAME)
                    HttpContext.Current.Cache.Remove(key);
            }
        }
        public static void XoaCacheStatistics()
        {
            foreach (System.Collections.DictionaryEntry entry in HttpContext.Current.Cache)
            {
                string key = entry.Key.ToString();
                if (key.Substring(0, key.IndexOf("_")) == Constants.CACHE_STATISTICS_NAME)
                    HttpContext.Current.Cache.Remove(key);
            }
        }
        public static void XoaAllCache()
        {
            foreach (System.Collections.DictionaryEntry entry in HttpContext.Current.Cache)
            {
                string key = entry.Key.ToString();
                HttpContext.Current.Cache.Remove(key);
            }
        }
    }
}
