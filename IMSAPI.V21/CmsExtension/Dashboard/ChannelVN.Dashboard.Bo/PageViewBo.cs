﻿using ChannelVN.Dashboard.Dal;
using ChannelVN.Dashboard.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Caching;

namespace ChannelVN.Dashboard.Bo
{
    public class PageViewBo
    {
        public static List<PageViewEntity> ListPageViewByTime(int LoaiTk,string Db)
        {
            string cacheName = string.Format(Constants.CACHE_VISIT_FORMAT, Constants.CACHE_STATISTICS_NAME, Constants.DATABASE.TRACKINGVISITDB, "ListPageViewByTime", LoaiTk, Db);
            string[] tables = { Db + Constants.TABLE._CategoryDaily };
            var entity = HttpContext.Current.Cache[cacheName] as List<PageViewEntity>;
            if (entity == null)
            {
                entity = TrackingVisitDal.ListPageViewByTime(LoaiTk,Db);
                StatisticsBo.CreateCacheStatistics(entity, Constants.DATABASE.TRACKINGVISITDB, cacheName, tables);
            }
            return entity;
        }
    }
}
