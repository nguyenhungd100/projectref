﻿using ChannelVN.Dashboard.Dal;
using ChannelVN.Dashboard.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Caching;

namespace ChannelVN.Dashboard.Bo
{
    public class StatisticsNewsBo
    {
        public static StatisticsBoxTongHopEntity Counter_News(int LoaiTK, int ZoneId, string UserName)
        {
            string[] tables = { Constants.TABLE.NEWSTB, Constants.TABLE.NEWSINZONETB, Constants.TABLE.USERPERMISSIONTB};
            string cacheName = string.Format(Constants.CACHE_NAME_FORMAT, Constants.CACHE_STATISTICS_NAME,"Counter_News", Constants.DATABASE.MAINDB, LoaiTK, ZoneId, UserName);
            var entity = HttpContext.Current.Cache[cacheName] as StatisticsBoxTongHopEntity;
            if (entity == null)
            {
                entity = StatisticsNewsDal.Counter_News(LoaiTK, ZoneId, UserName);
                StatisticsBo.CreateCacheStatistics(entity, Constants.DATABASE.MAINDB, cacheName, tables);
            }
            return entity;
        }
        public static StatisticsProductionNewsPublishEntity Counter_ProductionNews_Publish(int LoaiTK, int ZoneId, string UserName)
        {
            string[] tables = { Constants.TABLE.NEWSTB, Constants.TABLE.NEWSINZONETB, Constants.TABLE.USERPERMISSIONTB };
            string cacheName = string.Format(Constants.CACHE_NAME_FORMAT, Constants.CACHE_STATISTICS_NAME, "Counter_ProductionNews_Publish", Constants.DATABASE.MAINDB, LoaiTK, ZoneId, UserName);
            var entity = HttpContext.Current.Cache[cacheName] as StatisticsProductionNewsPublishEntity;
            if (entity == null)
            {
                entity = StatisticsNewsDal.Counter_ProductionNews_Publish(LoaiTK, ZoneId, UserName);
                StatisticsBo.CreateCacheStatistics(entity, Constants.DATABASE.MAINDB, cacheName, tables);
            }
            return entity;
        }
        public static StatisticsProductionNewsSendEntity Counter_ProductionNews_Send(int LoaiTK, int ZoneId, string UserName)
        {
            string[] tables = { Constants.TABLE.NEWSTB, Constants.TABLE.NEWSINZONETB, Constants.TABLE.USERPERMISSIONTB };
            string cacheName = string.Format(Constants.CACHE_NAME_FORMAT, Constants.CACHE_STATISTICS_NAME, "Counter_ProductionNews_Send", Constants.DATABASE.MAINDB, LoaiTK, ZoneId, UserName);
            var entity = HttpContext.Current.Cache[cacheName] as StatisticsProductionNewsSendEntity;
            if (entity == null)
            {
                entity = StatisticsNewsDal.Counter_ProductionNews_Send(LoaiTK, ZoneId, UserName);
                StatisticsBo.CreateCacheStatistics(entity, Constants.DATABASE.MAINDB, cacheName, tables);
            }
            return entity;
        }
        public static List<ListPercent> Counter_NewsPercentByZone(int LoaiTK, string UserName)
        {
            string[] tables = { Constants.TABLE.NEWSTB, Constants.TABLE.NEWSINZONETB, Constants.TABLE.USERPERMISSIONTB };
            string cacheName = string.Format(Constants.CACHE_NAME_FORMAT, Constants.CACHE_STATISTICS_NAME, "Counter_NewsPercentByZone", Constants.DATABASE.MAINDB, LoaiTK, "NoneZoneId", UserName);
            var entity = HttpContext.Current.Cache[cacheName] as List<ListPercent>;
            if (entity == null)
            {
                entity = StatisticsNewsDal.Counter_NewsPercentByZone(LoaiTK, UserName);
                StatisticsBo.CreateCacheStatistics(entity, Constants.DATABASE.MAINDB, cacheName, tables);
            }
            return entity;
        }
        public static List<ListPercent> Counter_NewsPercentByCategory(int LoaiTK, int ZoneId, string UserName)
        {
            string[] tables = { Constants.TABLE.NEWSTB, Constants.TABLE.NEWSINZONETB, Constants.TABLE.USERPERMISSIONTB };
            string cacheName = string.Format(Constants.CACHE_NAME_FORMAT, Constants.CACHE_STATISTICS_NAME, "Counter_NewsPercentByCategory", Constants.DATABASE.MAINDB, LoaiTK, ZoneId, UserName);
            var entity = HttpContext.Current.Cache[cacheName] as List<ListPercent>;
            if (entity == null)
            {
                entity = StatisticsNewsDal.Counter_NewsPercentByCategory(LoaiTK, ZoneId, UserName);
                StatisticsBo.CreateCacheStatistics(entity, Constants.DATABASE.MAINDB, cacheName, tables);
            }
            return entity;
        }
        public static List<TempTopPVEntity> STATISTICS_TOPPV(int LoaiTK, int ZoneId, string UserName, int TopSize, string keyword)
        {
            string[] tables = { Constants.TABLE.NEWSTB, Constants.TABLE.NEWSINZONETB, Constants.TABLE.USERPERMISSIONTB };
            string cacheName = string.Format(Constants.CACHE_NAME_FORMAT, Constants.CACHE_STATISTICS_NAME, "STATISTICS_TOPPV", Constants.DATABASE.MAINDB, LoaiTK, ZoneId, UserName);
            var entity = HttpContext.Current.Cache[cacheName] as List<TempTopPVEntity>;
            if (entity == null)
            {
                entity = StatisticsNewsDal.STATISTICS_TOPPV(LoaiTK, ZoneId, UserName, TopSize,keyword);
                StatisticsBo.CreateCacheStatistics(entity, Constants.DATABASE.MAINDB, cacheName, tables);
            }
            return entity;
        }
    }
}
