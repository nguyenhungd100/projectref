﻿using ChannelVN.Dashboard.Dal;
using ChannelVN.Dashboard.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Caching;

namespace ChannelVN.Dashboard.Bo
{
    public class StatisticsCommentBo
    {
        public static List<CommentEntity> Counter_Comment(int LoaiTK,int ZoneId, string UserName)
        {
            string[] tables = { Constants.TABLE.COMMENTTB };
            string cacheName = string.Format(Constants.CACHE_NAME_FORMAT, Constants.CACHE_STATISTICS_NAME, Constants.DATABASE.COMMENTDB, "Counter_Comment", LoaiTK, ZoneId, UserName);
            var entity = HttpContext.Current.Cache[cacheName] as List<CommentEntity>;
            if (entity == null)
            {
                entity = StatisticsCommentDal.Counter_Comment();
                StatisticsBo.CreateCacheStatistics(entity, Constants.DATABASE.COMMENTDB,cacheName, tables);
            }
            return entity;
        }
        public static List<CommentEntity> ListCommentByZone(int LoaiTK, string ListZoneId, string listZoneAll, string UserName, ref int totalOdl)
        {
            string[] tables = { Constants.TABLE.COMMENTTB };
            string cacheName = string.Format(Constants.CACHE_NAME_FORMAT, Constants.CACHE_STATISTICS_NAME, Constants.DATABASE.COMMENTDB, "Counter_Comment_All", LoaiTK, ListZoneId.Replace(",",""), UserName);
            var entity = HttpContext.Current.Cache[cacheName] as List<CommentEntity>;
            if (entity == null)
            {
                entity = StatisticsCommentDal.ListCommentByZone(ListZoneId, listZoneAll, LoaiTK,ref totalOdl);
                StatisticsBo.CreateCacheStatistics(entity, Constants.DATABASE.COMMENTDB, cacheName, tables);
            }
            return entity;
        }
        public static List<CommentEntity> Counter_Comment_All(int LoaiTK, int ZoneId, string UserName)
        {
            string[] tables = { Constants.TABLE.COMMENTTB };
            string cacheName = string.Format(Constants.CACHE_NAME_FORMAT, Constants.CACHE_STATISTICS_NAME, Constants.DATABASE.COMMENTDB, "Counter_Comment_All", LoaiTK, ZoneId, UserName);
            var entity = HttpContext.Current.Cache[cacheName] as List<CommentEntity>;
            if (entity == null)
            {
                entity = StatisticsCommentDal.Counter_Comment_All();
                StatisticsBo.CreateCacheStatistics(entity, Constants.DATABASE.COMMENTDB, cacheName, tables);
            }
            return entity;
        }
        public static List<CommentEntity> Counter_Comment_AllForever(int LoaiTK, int ZoneId, string UserName)
        {
            string[] tables = { Constants.TABLE.COMMENTTB };
            string cacheName = string.Format(Constants.CACHE_NAME_FORMAT, Constants.CACHE_STATISTICS_NAME, Constants.DATABASE.COMMENTDB, "Counter_Comment_AllForever", LoaiTK, ZoneId, UserName);
            var entity = HttpContext.Current.Cache[cacheName] as List<CommentEntity>;
            if (entity == null)
            {
                entity = StatisticsCommentDal.Counter_Comment_AllForever();
                StatisticsBo.CreateCacheStatistics_NothingDependency(entity, Constants.DATABASE.COMMENTDB, cacheName, tables);
            }
            return entity;
        }
        public static List<CommentEntity> Counter_CommentByTime(CountCommentTemp ct,int LoaiTK, string UserName)
        {
            string[] tables = { Constants.TABLE.COMMENTTB };
            string cacheName = string.Format(Constants.CACHE_NAME_FORMAT, Constants.CACHE_STATISTICS_NAME, Constants.DATABASE.COMMENTDB,"Counter_CommentByTime", LoaiTK, "all", UserName);
            var entity = HttpContext.Current.Cache[cacheName] as List<CommentEntity>;
            if (entity == null)
            {
                entity = StatisticsCommentDal.Counter_CommentByTime(ct,LoaiTK);
                StatisticsBo.CreateCacheStatistics(entity, Constants.DATABASE.COMMENTDB, cacheName, tables);
            }
            return entity;
        }
    }
}
