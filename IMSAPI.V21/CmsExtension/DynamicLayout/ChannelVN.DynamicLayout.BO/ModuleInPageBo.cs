﻿using System.Collections.Generic;
using System.Linq;
using ChannelVN.DynamicLayout.DAL;
using ChannelVN.DynamicLayout.Entity;
using ChannelVN.DynamicLayout.Entity.ErrorCode;

namespace ChannelVN.DynamicLayout.BO
{
    public class ModuleInPageBo
    {
        public static List<ModuleInPageEntity> GetByPlaceHolderName(string _PlaceHolderName, int _PageID)
        {
            var moduleInPageEntities = new List<ModuleInPageEntity>();
            var entities = ModuleInPageDal.GetByPlaceHolderName(_PlaceHolderName, _PageID);
            if (null != entities && entities.Count > 0)
            {
                moduleInPageEntities.AddRange(
                    entities.Select(
                        page =>
                        new ModuleInPageEntity
                            {
                                Id = page.Id,
                                SkinSrc = page.SkinSrc,
                                ModuleId = page.ModuleId,
                                PageId = page.PageId,
                                PlaceHolderName = page.PlaceHolderName,
                                Priority = page.Priority
                            }));
            }
            return moduleInPageEntities;
        }
        public static ErrorMapping.ErrorCodes Insert(ModuleInPageEntity moduleInPageEntity, ref int moduleInPageID)
        {
            return ModuleInPageDal.Insert(moduleInPageEntity, ref moduleInPageID) ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.UnknowError;
        }
        public static ErrorMapping.ErrorCodes DeleteById(int moduleInPageID)
        {
            return ModuleInPageDal.DeleteById(moduleInPageID) ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.UnknowError;
        }
        public static ErrorMapping.ErrorCodes UpdateSkinSrc(int moduleInPageID, string skinSrc)
        {
            return ModuleInPageDal.UpdateSkinSrc(moduleInPageID, skinSrc) ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.UnknowError;
        }
        public static List<ModuleInPageWithConfigEntity> GetAllModuleWithConfigByPlaceHolder(string _PlaceHolderName, int _PageID)
        {
            var moduleInPageWithConfigEntity = new List<ModuleInPageWithConfigEntity>();
            var mainEntities = ModuleInPageDal.GetByPlaceHolderName(_PlaceHolderName, _PageID);
            for (int i = 0; i < mainEntities.Count; i++)
            {
                var moduleConfigEntities = ModuleConfigurationDal.GetByModuleInPageID(mainEntities[i].Id);
                var moduleEntity = ModuleDal.GetDetailByModuleID(mainEntities[i].ModuleId);
                var moduleSkin = ModuleSkinDal.GetByModuleId(mainEntities[i].ModuleId);
                moduleEntity.ModuleSkin = moduleSkin;
                moduleInPageWithConfigEntity.Add(new ModuleInPageWithConfigEntity
                                                     {
                                                         Configurations = moduleConfigEntities,
                                                         Id = mainEntities[i].Id,
                                                         PageId = mainEntities[i].PageId,
                                                         PlaceHolderName = mainEntities[i].PlaceHolderName,
                                                         SkinSrc = mainEntities[i].SkinSrc,
                                                         Priority = mainEntities[i].Priority,
                                                         Module = moduleEntity
                                                     });
            }
            return moduleInPageWithConfigEntity;
        }

        public static ModuleInPageWithConfigEntity GetModuleWithConfigByModuleInPageId(int moduleInPageId)
        {
            var moduleInPageWithConfigEntity = new ModuleInPageWithConfigEntity();
            var mainEntities = ModuleInPageDal.GetByModuleInPageId(moduleInPageId);
            var moduleConfigEntities = ModuleConfigurationDal.GetByModuleInPageID(mainEntities.Id);
            var moduleEntity = ModuleDal.GetDetailByModuleID(mainEntities.ModuleId);
            var moduleSkinEntity = ModuleSkinDal.GetByModuleId(mainEntities.ModuleId);
            moduleEntity.ModuleSkin = moduleSkinEntity;


            moduleInPageWithConfigEntity.Configurations = moduleConfigEntities;
            moduleInPageWithConfigEntity.Id = mainEntities.Id;
            moduleInPageWithConfigEntity.PageId = mainEntities.PageId;
            moduleInPageWithConfigEntity.PlaceHolderName = mainEntities.PlaceHolderName;
            moduleInPageWithConfigEntity.SkinSrc = mainEntities.SkinSrc;
            moduleInPageWithConfigEntity.Priority = mainEntities.Priority;
            moduleInPageWithConfigEntity.Module = moduleEntity;

            return moduleInPageWithConfigEntity;
        }
        public static FullModuleInPageWithConfigEntity GetFullModuleWithConfigByModuleInPageId(int moduleInPageId)
        {
            var fullModuleInPageWithConfigEntity = new FullModuleInPageWithConfigEntity();
            var mainEntities = ModuleInPageDal.GetByModuleInPageId(moduleInPageId);
            var moduleConfigEntities = ModuleConfigurationDal.GetByModuleInPageID(mainEntities.Id);
            var moduleDetailEntity = ModuleBo.GetModuleDetailByModuleId(mainEntities.ModuleId);
            var moduleSkinEntity = ModuleSkinDal.GetByModuleId(mainEntities.ModuleId);
            moduleDetailEntity.ModuleSkin = moduleSkinEntity;


            fullModuleInPageWithConfigEntity.Configurations = moduleConfigEntities;
            fullModuleInPageWithConfigEntity.Id = mainEntities.Id;
            fullModuleInPageWithConfigEntity.PageId = mainEntities.PageId;
            fullModuleInPageWithConfigEntity.PlaceHolderName = mainEntities.PlaceHolderName;
            fullModuleInPageWithConfigEntity.SkinSrc = mainEntities.SkinSrc;
            fullModuleInPageWithConfigEntity.Priority = mainEntities.Priority;
            fullModuleInPageWithConfigEntity.Module = moduleDetailEntity;

            return fullModuleInPageWithConfigEntity;
        }

        public static ErrorMapping.ErrorCodes UpdatePriority(string listOfPriority)
        {
            return ModuleInPageDal.UpdatePriority(listOfPriority) ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.UnknowError;
        }
    }
}
