﻿using System.Collections.Generic;
using ChannelVN.DynamicLayout.DAL;
using ChannelVN.DynamicLayout.Entity;
using ChannelVN.DynamicLayout.Entity.ErrorCode;

namespace ChannelVN.DynamicLayout.BO
{
    public class ModuleBo
    {
        public static List<ModuleEntity> GetAll()
        {
            return ModuleDal.GetAll();
            //var moduleEntities = new List<ModuleEntity>();
            //var entities = ModuleDal.GetAll();
            //if (null != entities && entities.Count > 0)
            //{
            //    moduleEntities.AddRange(
            //        entities.Select(
            //            page =>
            //            new ModuleEntity
            //                {
            //                    Id = page.Id,
            //                    ModuleName = page.ModuleName,
            //                    ModuleConfigSrc = page.ModuleConfigSrc,
            //                    Status = page.Status
            //                }));
            //}
            //return moduleEntities;
        }
        public static ErrorMapping.ErrorCodes Insert(ModuleEntity entity, ref int ModuleID)
        {
            return ModuleDal.Insert(entity, ref ModuleID) ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.UnknowError;
        }
        public static ModuleDetailEntity GetModuleDetailByModuleId(int moduleId)
        {
            var module = ModuleDal.GetByModuleID(moduleId);

            if (null != module)
            {
                return new ModuleDetailEntity()
                           {
                               Id = module.Id,
                               ModuleName = module.ModuleName,
                               ModuleConfigSrc = module.ModuleConfigSrc,
                               Status = module.Status,
                               ModuleSkin = ModuleSkinDal.GetByModuleId(moduleId)
                           };
            }
            return null;
        }
    }
}
