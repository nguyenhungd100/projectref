﻿using System.Collections.Generic;
using System.Linq;
using ChannelVN.DynamicLayout.DAL;
using ChannelVN.DynamicLayout.Entity;

namespace ChannelVN.DynamicLayout.BO
{
    public class PageBo
    {
        public static List<PageEntity> GetAll()
        {
            var pageEntities = new List<PageEntity>();
            var entities = PageDal.GetAll();
            if (null != entities && entities.Count>0)
            {
                pageEntities.AddRange(
                    entities.Select(
                        page =>
                        new PageEntity
                            {
                                Id = page.Id,
                                Name = page.Name,
                                PageSrc = page.PageSrc,
                                PageUrl = page.PageUrl,
                                Status = page.Status
                            }));
            }
            return pageEntities;
        }

        public static PageWithFullModuleEntity GetDetailById(int pageId)
        {
            var moduleInPages = ModuleInPageDal.GetByPageId(pageId);
            var moduleInPageDetail = moduleInPages.Select(moduleInPage => new FullModuleInPageWithConfigEntity()
                                                                              {
                                                                                  Id = moduleInPage.Id,
                                                                                  SkinSrc = moduleInPage.SkinSrc,
                                                                                  PageId = moduleInPage.PageId,
                                                                                  PlaceHolderName =
                                                                                      moduleInPage.PlaceHolderName,
                                                                                  Priority = moduleInPage.Priority,
                                                                                  Module = ModuleBo.GetModuleDetailByModuleId(moduleInPage.ModuleId),
                                                                                  Configurations =
                                                                                      ModuleConfigurationDal.
                                                                                      GetByModuleInPageID(
                                                                                          moduleInPage.Id)
                                                                              }).ToList();

            var page = PageDal.GetPageById(pageId);
            var pageDetail = new PageWithFullModuleEntity()
                                 {
                                     Id = page.Id,
                                     Name = page.Name,
                                     PageSrc = page.PageSrc,
                                     PageUrl = page.PageUrl,
                                     Status = page.Status,
                                     ListModuleInPage = moduleInPageDetail
                                 };
            return pageDetail;
        }
        public static PageWithFullModuleEntity GetDetailByPageSrc(string pageSrc)
        {

            var page = PageDal.GetPageByPageSrc(pageSrc);
            var pageDetail = new PageWithFullModuleEntity()
            {
                Id = page.Id,
                Name = page.Name,
                PageSrc = page.PageSrc,
                PageUrl = page.PageUrl,
                Status = page.Status,
                ListModuleInPage = new List<FullModuleInPageWithConfigEntity>()
            };

            var moduleInPages = ModuleInPageDal.GetByPageId(page.Id);
            var moduleInPageDetail = moduleInPages.Select(moduleInPage => new FullModuleInPageWithConfigEntity()
            {
                Id = moduleInPage.Id,
                SkinSrc = moduleInPage.SkinSrc,
                PageId = moduleInPage.PageId,
                PlaceHolderName =
                    moduleInPage.PlaceHolderName,
                Priority = moduleInPage.Priority,
                Module = ModuleBo.GetModuleDetailByModuleId(moduleInPage.ModuleId),
                Configurations =
                    ModuleConfigurationDal.
                    GetByModuleInPageID(
                        moduleInPage.Id)
            }).ToList();

            pageDetail.ListModuleInPage = moduleInPageDetail;

            return pageDetail;
        }
    }
}
