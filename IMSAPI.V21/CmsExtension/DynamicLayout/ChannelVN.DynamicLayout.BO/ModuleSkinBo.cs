﻿using System.Collections.Generic;
using System.Linq;
using ChannelVN.DynamicLayout.DAL;
using ChannelVN.DynamicLayout.Entity;

namespace ChannelVN.DynamicLayout.BO
{
    public class ModuleSkinBo
    {
        public static List<ModuleSkinEntity> GetByModuleId(int moduleId)
        {
            var moduleSkinEntities = new List<ModuleSkinEntity>();
            var entities = ModuleSkinDal.GetByModuleId(moduleId);
            if (null != entities && entities.Count > 0)
            {
                moduleSkinEntities.AddRange(
                    entities.Select(
                        entity =>
                        new ModuleSkinEntity
                        {
                            Id = entity.Id,
                            ModuleId = entity.ModuleId,
                            SkinName = entity.SkinName,
                            SkinSrc = entity.SkinSrc,
                            Status = entity.Status
                        }));
            }
            return moduleSkinEntities;
        }
    }
}
