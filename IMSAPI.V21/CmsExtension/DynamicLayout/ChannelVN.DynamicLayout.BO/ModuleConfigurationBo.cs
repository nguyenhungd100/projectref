﻿using System.Collections.Generic;
using System.Linq;
using ChannelVN.DynamicLayout.DAL;
using ChannelVN.DynamicLayout.Entity;
using ChannelVN.DynamicLayout.Entity.ErrorCode;

namespace ChannelVN.DynamicLayout.BO
{
    public class ModuleConfigurationBo
    {
        public static List<ModuleConfigurationEntity> GetByModuleInPageID(int ModuleInPageID)
        {
            var moduleConfigurationEntities = new List<ModuleConfigurationEntity>();
            var entities = ModuleConfigurationDal.GetByModuleInPageID(ModuleInPageID);
            if (null != entities && entities.Count > 0)
            {
                moduleConfigurationEntities.AddRange(
                    entities.Select(
                        page =>
                        new ModuleConfigurationEntity
                        {
                            Id = page.Id,
                            ConfigName = page.ConfigName,
                            ConfigValue = page.ConfigValue,
                            ModuleInPageId = page.ModuleInPageId
                        }));
            }
            return moduleConfigurationEntities;
        }
        public static ErrorMapping.ErrorCodes Insert(ModuleConfigurationEntity _ModuleConfigurationEntity, ref int _moduleConfigId)
        {
            return ModuleConfigurationDal.Insert(_ModuleConfigurationEntity, ref _moduleConfigId) ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.UnknowError;
        }
        public static ErrorMapping.ErrorCodes Update(ModuleConfigurationEntity _ModuleConfigurationEntity)
        {
            return ModuleConfigurationDal.Update(_ModuleConfigurationEntity) ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.UnknowError;
        }
    }
}
