﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using ChannelVN.CMS.Common;
using ChannelVN.DynamicLayout.DAL.Common;
using ChannelVN.DynamicLayout.Entity;
using ChannelVN.DynamicLayout.MainDal.Databases;

namespace ChannelVN.DynamicLayout.DAL
{
    public class ModuleDal
    {
        #region Get methods

        public static List<ModuleEntity> GetAll()
        {
            List<ModuleEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.ModuleMainDal.GetAll();
            }
            return returnValue;
        }
        public static ModuleEntity GetByModuleID(int _moduleID)
        {
            ModuleEntity returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.ModuleMainDal.GetByModuleID(_moduleID);
            }
            return returnValue;
        }
        public static ModuleDetailEntity GetDetailByModuleID(int _moduleID)
        {
            ModuleDetailEntity returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.ModuleMainDal.GetDetailByModuleID(_moduleID);
            }
            return returnValue;
        }
        #endregion

        #region SET method

        public static bool Insert(ModuleEntity module, ref int MofuleID)
        {
            bool returnValue = false;
            using (var db = new CmsMainDb())
            {
                returnValue = db.ModuleMainDal.Insert(module, ref MofuleID);
            }
            return returnValue;
        }
        #endregion
    }
}
