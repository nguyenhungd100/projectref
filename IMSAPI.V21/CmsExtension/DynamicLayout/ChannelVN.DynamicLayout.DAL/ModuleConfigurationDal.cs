﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using ChannelVN.CMS.Common;
using ChannelVN.DynamicLayout.DAL.Common;
using ChannelVN.DynamicLayout.Entity;
using ChannelVN.DynamicLayout.MainDal.Databases;

namespace ChannelVN.DynamicLayout.DAL
{
    public class ModuleConfigurationDal
    {
        #region Get methods

        public static List<ModuleConfigurationEntity> GetByModuleInPageID(int _ModuleInPageID)
        {
            List<ModuleConfigurationEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.ModuleConfigMainDal.GetByModuleInPageID(_ModuleInPageID);
            }
            return returnValue;
        }
        #endregion

        #region Set Methods

        public static bool Insert(ModuleConfigurationEntity moduleConfigurationEntity, ref int moduleConfigId)
        {
            bool returnValue = false;
            using (var db = new CmsMainDb())
            {
                returnValue = db.ModuleConfigMainDal.Insert(moduleConfigurationEntity, ref moduleConfigId);
            }
            return returnValue;
        }

        public static bool Update(ModuleConfigurationEntity moduleConfigurationEntity)
        {
            bool returnValue = false;
            using (var db = new CmsMainDb())
            {
                returnValue = db.ModuleConfigMainDal.Update(moduleConfigurationEntity);
            }
            return returnValue;
        }
        #endregion
    }
}
