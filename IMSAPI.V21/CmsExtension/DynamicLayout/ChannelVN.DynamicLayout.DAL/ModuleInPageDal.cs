﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using ChannelVN.CMS.Common;
using ChannelVN.DynamicLayout.DAL.Common;
using ChannelVN.DynamicLayout.Entity;
using ChannelVN.DynamicLayout.MainDal.Databases;

namespace ChannelVN.DynamicLayout.DAL
{
    public class ModuleInPageDal
    {
        #region Get methods
        public static List<ModuleInPageEntity> GetByPlaceHolderName(string _PlaceHolderName, int _PageID)
        {
            List<ModuleInPageEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.ModuleInPageMainDal.GetByPlaceHolderName(_PlaceHolderName, _PageID);
            }
            return returnValue;
        }
        public static ModuleInPageEntity GetByModuleInPageId(int moduleInPageId)
        {
            ModuleInPageEntity returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.ModuleInPageMainDal.GetByModuleInPageId(moduleInPageId);
            }
            return returnValue;
        }
        public static List<ModuleInPageEntity> GetByPageId(int pageId)
        {
            List<ModuleInPageEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.ModuleInPageMainDal.GetByPageId(pageId);
            }
            return returnValue;
        }
        #endregion

        #region Set Methods
        public static bool Insert(ModuleInPageEntity moduleInPage, ref int ModuleInPageID)
        {
            bool returnValue = false;
            using (var db = new CmsMainDb())
            {
                returnValue = db.ModuleInPageMainDal.Insert(moduleInPage, ref ModuleInPageID);
            }
            return returnValue;
        }
        public static bool DeleteById(int ModuleInPageID)
        {
            bool returnValue = false;
            using (var db = new CmsMainDb())
            {
                returnValue = db.ModuleInPageMainDal.DeleteById(ModuleInPageID);
            }
            return returnValue;
        }
        public static bool UpdateSkinSrc(int moduleInPageID, string skinSrc)
        {
            bool returnValue = false;
            using (var db = new CmsMainDb())
            {
                returnValue = db.ModuleInPageMainDal.UpdateSkinSrc(moduleInPageID, skinSrc);
            }
            return returnValue;
        }
        public static bool UpdatePriority(string listOfPriority)
        {
            bool returnValue = false;
            using (var db = new CmsMainDb())
            {
                returnValue = db.ModuleInPageMainDal.UpdatePriority(listOfPriority);
            }
            return returnValue;
        }
        #endregion
    }
}
