﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using ChannelVN.CMS.Common;
using ChannelVN.DynamicLayout.DAL.Common;
using ChannelVN.DynamicLayout.Entity;
using ChannelVN.DynamicLayout.MainDal.Databases;

namespace ChannelVN.DynamicLayout.DAL
{
    public class PageDal
    {
        #region Get methods
        public static List<PageEntity> GetAll()
        {
            List<PageEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.PageContentMainDal.GetAll();
            }
            return returnValue;
        }
        public static PageEntity GetPageById(int pageId)
        {
            PageEntity returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.PageContentMainDal.GetPageById(pageId);
            }
            return returnValue;
        }
        public static PageEntity GetPageByPageSrc(string pageSrc)
        {
            PageEntity returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.PageContentMainDal.GetPageByPageSrc(pageSrc);
            }
            return returnValue;
        }
        #endregion
    }
}
