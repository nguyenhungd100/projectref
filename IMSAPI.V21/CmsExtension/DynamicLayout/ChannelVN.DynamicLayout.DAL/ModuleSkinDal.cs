﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using ChannelVN.CMS.Common;
using ChannelVN.DynamicLayout.DAL.Common;
using ChannelVN.DynamicLayout.Entity;
using ChannelVN.DynamicLayout.MainDal.Databases;

namespace ChannelVN.DynamicLayout.DAL
{
    public class ModuleSkinDal
    {
        public static List<ModuleSkinEntity> GetByModuleId(int moduleID)
        {
            List<ModuleSkinEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.ModuleSkinMainDal.GetByModuleId(moduleID);
            }
            return returnValue;
        }
    }
}
