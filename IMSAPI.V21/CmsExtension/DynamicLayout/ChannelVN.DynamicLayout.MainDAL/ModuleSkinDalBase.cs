﻿using ChannelVN.DynamicLayout.Entity;
using ChannelVN.DynamicLayout.MainDal.Databases;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ChannelVN.DynamicLayout.MainDAL
{
    public abstract class ModuleSkinDalBase
    {
        #region function GET
        public List<ModuleSkinEntity> GetByModuleId(int moduleID) {
            const string commandText = "CmsDL_ModuleSkin_GetByModuleID";
            try
            {
                List<ModuleSkinEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "ModuleID", moduleID);
                data = _db.GetList<ModuleSkinEntity>(cmd);
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        #endregion

        /// <summary>
        /// Chuyển đổi DAL cho Module skin
        /// minhduongvan
        /// 2014/01/09
        /// </summary>

        #region Constructor

        private readonly CmsMainDb _db;

        protected ModuleSkinDalBase(CmsMainDb db)
        {
            _db = db;
        }

        protected CmsMainDb Database
        {
            get { return _db; }
        }

        #endregion
    }
}
